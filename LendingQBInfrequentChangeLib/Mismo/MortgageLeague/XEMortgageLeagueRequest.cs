using System;
using System.Xml;

namespace Mismo.MortgageLeague
{
	public class XEMortgageLeagueRequest : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="MORTGAGE_LEAGUE_REQUEST">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="CREDIT_REPORT"/>
        //              <xs:element ref="MCL_INSTANT_VIEW_ID"/>
        //          </xs:sequence>
        //          <xs:attribute name="_ActionType">
        //          <xs:attribute name="_LoanNumber" type="xs:string"/>
        //          <xs:attribute name="_LoanID" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        XECreditReport m_creditReport;
        XEMclInstantViewID m_mclInstantViewID;
        private E_XEMortgageLeagueRequestActionType m_actionType;
        private string m_loanNumber;
        private Guid m_loanID;
        #endregion

        #region Public Properties
        public XECreditReport CreditReport 
        {
            get { return m_creditReport; }
            set { m_creditReport = value; }
        }
        public XEMclInstantViewID MclInstantViewID 
        {
            get { return m_mclInstantViewID; }
            set { m_mclInstantViewID = value; }
        }
        public E_XEMortgageLeagueRequestActionType ActionType 
        {
            get { return m_actionType; }
            set { m_actionType = value; }
        }
        public string LoanNumber 
        {
            get { return m_loanNumber; }
            set { m_loanNumber = value; }
        }
        public Guid LoanID 
        {
            get { return m_loanID; }
            set { m_loanID = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MORTGAGE_LEAGUE_REQUEST");
            WriteAttribute(writer, "_ActionType", m_actionType);
            WriteAttribute(writer, "_LoanNumber", m_loanNumber);
            WriteAttribute(writer, "_LoanID", m_loanID.ToString());
            if (null != m_creditReport)
                m_creditReport.GenerateXml(writer);

            if (null != m_mclInstantViewID)
                m_mclInstantViewID.GenerateXml(writer);


            writer.WriteEndElement(); // </MORTGAGE_LEAGUE_REQUEST>
        }

        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_creditReport = null;
            XmlElement singleNode = (XmlElement) el.SelectSingleNode("CREDIT_REPORT");
            if (null != singleNode) 
            {
                m_creditReport = new XECreditReport();
                m_creditReport.Parse(singleNode);
            }

            m_mclInstantViewID = null;
            singleNode = (XmlElement) el.SelectSingleNode("MCL_INSTANT_VIEW_ID");
            if (null != singleNode) 
            {
                m_mclInstantViewID = new XEMclInstantViewID();
                m_mclInstantViewID.Parse(singleNode);
            }
            switch (el.GetAttribute("_ActionType")) 
            {
                case "SubmitNew":
                    m_actionType = E_XEMortgageLeagueRequestActionType.SubmitNew;
                    break;
                case "Retrieve":
                default:
                    m_actionType = E_XEMortgageLeagueRequestActionType.Retrieve;
                    break;
            }

            m_loanNumber = el.GetAttribute("_LoanNumber");
            m_loanID = Guid.Empty;
            try 
            {
                m_loanID = new Guid(el.GetAttribute("_LoanID"));
            } 
            catch {}

        }
        #endregion
	}
    public enum E_XEMortgageLeagueRequestActionType 
    {
        SubmitNew,
        Retrieve
    }
}
