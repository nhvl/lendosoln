/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEMIPremiumTax : Mismo.Common.AbstractXmlNode
    {
        public XEMIPremiumTax()
        {
        }
        #region Schema
        //  <xs:element name="MI_PREMIUM_TAX">
        //    <xs:complexType>
        //      <xs:attribute name="_CodeType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="County"/>
        //            <xs:enumeration value="Municipal"/>
        //            <xs:enumeration value="State"/>
        //            <xs:enumeration value="AllTaxes"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_CodePercent" type="xs:string"/>
        //      <xs:attribute name="_CodeAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>         
        #endregion

        #region Private Member Variables
        private E_MIPremiumTaxCodeType m_codeType;
        private string m_codePercent;
        private string m_codeAmount;
        #endregion

        #region Public Properties
        public E_MIPremiumTaxCodeType CodeType 
        {
            get { return m_codeType; }
            set { m_codeType = value; }
        }
        public string CodePercent 
        {
            get { return m_codePercent; }
            set { m_codePercent = value; }
        }
        public string CodeAmount 
        {
            get { return m_codeAmount; }
            set { m_codeAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MI_PREMIUM_TAX");
            WriteAttribute(writer, "_CodeType", m_codeType);
            WriteAttribute(writer, "_CodePercent", m_codePercent);
            WriteAttribute(writer, "_CodeAmount", m_codeAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_MIPremiumTaxCodeType 
    {
        Undefined = 0,
        County,
        Municipal,
        State,
        AllTaxes
    }
}
