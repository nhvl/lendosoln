/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEEmployer : Mismo.Common.AbstractXmlNode
	{
		public XEEmployer()
		{
		}
        #region Schema
        // <xs:element name="EMPLOYER">
        //   <xs:complexType>
        //     <xs:attribute name="_Name" type="xs:string"/>
        //     <xs:attribute name="_StreetAddress" type="xs:string"/>
        //     <xs:attribute name="_City" type="xs:string"/>
        //     <xs:attribute name="_State" type="xs:string"/>
        //     <xs:attribute name="_PostalCode" type="xs:string"/>
        //     <xs:attribute name="_TelephoneNumber" type="xs:string"/>
        //     <xs:attribute name="CurrentEmploymentMonthsOnJob" type="xs:string"/>
        //     <xs:attribute name="CurrentEmploymentTimeInLineOfWorkYears" type="xs:string"/>
        //     <xs:attribute name="CurrentEmploymentYearsOnJob" type="xs:string"/>
        //     <xs:attribute name="EmploymentBorrowerSelfEmployedIndicator">
        //     <xs:attribute name="EmploymentCurrentIndicator">
        //     <xs:attribute name="EmploymentPositionDescription" type="xs:string"/>
        //     <xs:attribute name="EmploymentPrimaryIndicator">
        //     <xs:attribute name="IncomeEmploymentMonthlyAmount" type="xs:string"/>
        //     <xs:attribute name="PreviousEmploymentEndDate" type="xs:string"/>
        //     <xs:attribute name="PreviousEmploymentStartDate" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_name;
        private string m_streetAddress;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_telephoneNumber;
        private string m_currentEmploymentMonthsOnJob;
        private string m_currentEmploymentTimeInLineOfWorkYears;
        private string m_currentEmploymentYearsOnJob;
        private E_YesNoIndicator m_employmentBorrowerSelfEmployedIndicator;
        private E_YesNoIndicator m_employmentCurrentIndicator;
        private string m_employmentPositionDescription;
        private E_YesNoIndicator m_employmentPrimaryIndicator;
        private string m_incomeEmploymentMonthlyAmount;
        private string m_previousEmploymentEndDate;
        private string m_previousEmploymentStartDate;


        #endregion

        #region Public Properties
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string TelephoneNumber 
        {
            get { return m_telephoneNumber; }
            set { m_telephoneNumber = value; }
        }
        public string CurrentEmploymentMonthsOnJob 
        {
            get { return m_currentEmploymentMonthsOnJob; }
            set { m_currentEmploymentMonthsOnJob = value; }
        }
        public string CurrentEmploymentTimeInLineOfWorkYears 
        {
            get { return m_currentEmploymentTimeInLineOfWorkYears; }
            set { m_currentEmploymentTimeInLineOfWorkYears = value; }
        }
        public string CurrentEmploymentYearsOnJob 
        {
            get { return m_currentEmploymentYearsOnJob; }
            set { m_currentEmploymentYearsOnJob = value; }
        }
        public E_YesNoIndicator EmploymentBorrowerSelfEmployedIndicator 
        {
            get { return m_employmentBorrowerSelfEmployedIndicator; }
            set { m_employmentBorrowerSelfEmployedIndicator = value; }
        }
        public E_YesNoIndicator EmploymentCurrentIndicator 
        {
            get { return m_employmentCurrentIndicator; }
            set { m_employmentCurrentIndicator = value; }
        }
        public string EmploymentPositionDescription 
        {
            get { return m_employmentPositionDescription; }
            set { m_employmentPositionDescription = value; }
        }
        public E_YesNoIndicator EmploymentPrimaryIndicator 
        {
            get { return m_employmentPrimaryIndicator; }
            set { m_employmentPrimaryIndicator = value; }
        }
        public string IncomeEmploymentMonthlyAmount 
        {
            get { return m_incomeEmploymentMonthlyAmount; }
            set { m_incomeEmploymentMonthlyAmount = value; }
        }
        public string PreviousEmploymentEndDate 
        {
            get { return m_previousEmploymentEndDate; }
            set { m_previousEmploymentEndDate = value; }
        }
        public string PreviousEmploymentStartDate 
        {
            get { return m_previousEmploymentStartDate; }
            set { m_previousEmploymentStartDate = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("EMPLOYER");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_TelephoneNumber", m_telephoneNumber);
            WriteAttribute(writer, "CurrentEmploymentMonthsOnJob", m_currentEmploymentMonthsOnJob);
            WriteAttribute(writer, "CurrentEmploymentTimeInLineOfWorkYears", m_currentEmploymentTimeInLineOfWorkYears);
            WriteAttribute(writer, "CurrentEmploymentYearsOnJob", m_currentEmploymentYearsOnJob);
            WriteAttribute(writer, "EmploymentBorrowerSelfEmployedIndicator", m_employmentBorrowerSelfEmployedIndicator);
            WriteAttribute(writer, "EmploymentCurrentIndicator", m_employmentCurrentIndicator);
            WriteAttribute(writer, "EmploymentPositionDescription", m_employmentPositionDescription);
            WriteAttribute(writer, "EmploymentPrimaryIndicator", m_employmentPrimaryIndicator);
            WriteAttribute(writer, "IncomeEmploymentMonthlyAmount", m_incomeEmploymentMonthlyAmount);
            WriteAttribute(writer, "PreviousEmploymentEndDate", m_previousEmploymentEndDate);
            WriteAttribute(writer, "PreviousEmploymentStartDate", m_previousEmploymentStartDate);
            writer.WriteEndElement(); // </EMPLOYER>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
