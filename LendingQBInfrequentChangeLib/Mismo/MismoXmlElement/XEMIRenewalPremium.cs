/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEMIRenewalPremium : Mismo.Common.AbstractXmlNode
    {
        public XEMIRenewalPremium()
        {
        }
        #region Schema
        //  <xs:element name="MI_RENEWAL_PREMIUM">
        //    <xs:complexType>
        //      <xs:attribute name="_Sequence">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="First"/>
        //            <xs:enumeration value="Second"/>
        //            <xs:enumeration value="Third"/>
        //            <xs:enumeration value="Fourth"/>
        //            <xs:enumeration value="Fifth"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_Rate" type="xs:string"/>
        //      <xs:attribute name="_RateDurationMonths" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private E_MIRenewalPremiumSequence m_sequence;
        private string m_rate;
        private string m_rateDurationMonths;
        #endregion

        #region Public Properties
        public E_MIRenewalPremiumSequence Sequence 
        {
            get { return m_sequence; }
            set { m_sequence = value; }
        }
        public string Rate 
        {
            get { return m_rate; }
            set { m_rate = value; }
        }
        public string RateDurationMonths 
        {
            get { return m_rateDurationMonths; }
            set { m_rateDurationMonths = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MI_RENEWAL_PREMIUM");
            WriteAttribute(writer, "_Sequence", m_sequence);
            WriteAttribute(writer, "_Rate", m_rate);
            WriteAttribute(writer, "_RateDurationMonths", m_rateDurationMonths);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_MIRenewalPremiumSequence 
    {
        Undefined = 0,
        First,
        Second,
        Third,
        Fourth,
        Fifth
    }
}
