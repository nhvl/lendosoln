/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEPaymentSchedule : Mismo.Common.AbstractXmlNode
    {
        public XEPaymentSchedule()
        {
        }
        #region Schema
        //  <xs:element name="PAYMENT_SCHEDULE">
        //    <xs:complexType>
        //      <xs:attribute name="_PaymentSequenceIdentifier" type="xs:string"/>
        //      <xs:attribute name="_TotalNumberOfPaymentsCount" type="xs:string"/>
        //      <xs:attribute name="_PaymentAmount" type="xs:string"/>
        //      <xs:attribute name="_PaymentStartDate" type="xs:string"/>
        //      <xs:attribute name="_PaymentVaryingToAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_paymentSequenceIdentifier;
        private string m_totalNumberOfPaymentsCount;
        private string m_paymentAmount;
        private string m_paymentStartDate;
        private string m_paymentVaryingToAmount;
        #endregion

        #region Public Properties
        public string PaymentSequenceIdentifier 
        {
            get { return m_paymentSequenceIdentifier; }
            set { m_paymentSequenceIdentifier = value; }
        }
        public string TotalNumberOfPaymentsCount 
        {
            get { return m_totalNumberOfPaymentsCount; }
            set { m_totalNumberOfPaymentsCount = value; }
        }
        public string PaymentAmount 
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }
        public string PaymentStartDate 
        {
            get { return m_paymentStartDate; }
            set { m_paymentStartDate = value; }
        }
        public string PaymentVaryingToAmount 
        {
            get { return m_paymentVaryingToAmount; }
            set { m_paymentVaryingToAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PAYMENT_SCHEDULE");
            WriteAttribute(writer, "_PaymentSequenceIdentifier", m_paymentSequenceIdentifier);
            WriteAttribute(writer, "_TotalNumberOfPaymentsCount", m_totalNumberOfPaymentsCount);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            WriteAttribute(writer, "_PaymentStartDate", m_paymentStartDate);
            WriteAttribute(writer, "_PaymentVaryingToAmount", m_paymentVaryingToAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
