/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XETransmittalData : Mismo.Common.AbstractXmlNode
    {
        public XETransmittalData()
        {
        }
        #region Schema
        //  <xs:element name="TRANSMITTAL_DATA">
        //    <xs:complexType>
        //      <xs:attribute name="ArmsLengthIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BelowMarketSubordinateFinancingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BuydownRatePercent" type="xs:string"/>
        //      <xs:attribute name="CaseStateType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Application"/>
        //            <xs:enumeration value="FinalDisposition"/>
        //            <xs:enumeration value="PostClosingQualityControl"/>
        //            <xs:enumeration value="Prequalification"/>
        //            <xs:enumeration value="Underwriting"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="CreditReportAuthorizationIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="CurrentFirstMortgageHolderType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FNM"/>
        //            <xs:enumeration value="FRE"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Unknown"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LenderBranchIdentifier" type="xs:string"/>
        //      <xs:attribute name="LenderRegistrationIdentifier" type="xs:string"/>
        //      <xs:attribute name="PropertyAppraisedValueAmount" type="xs:string"/>
        //      <xs:attribute name="PropertyEstimatedValueAmount" type="xs:string"/>
        //      <xs:attribute name="InvestorLoanIdentifier" type="xs:string"/>
        //      <xs:attribute name="InvestorInstitutionIdentifier" type="xs:string"/>
        //      <xs:attribute name="CommitmentReferenceIdentifier" type="xs:string"/>
        //      <xs:attribute name="ConcurrentOriginationIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="ConcurrentOriginationLenderIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="RateLockPeriodDays" type="xs:string"/>
        //      <xs:attribute name="RateLockType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="BestEfforts"/>
        //            <xs:enumeration value="Mandatory"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="RateLockRequestedExtensionDays" type="xs:string"/>
        //      <xs:attribute name="LoanOriginationSystemLoanIdentifier" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_armsLengthIndicator;
        private E_YesNoIndicator m_belowMarketSubordinateFinancingIndicator;
        private string m_buydownRatePercent;
        private E_TransmittalDataCaseStateType m_caseStateType;
        private E_YesNoIndicator m_creditReportAuthorizationIndicator;
        private E_TransmittalDataCurrentFirstMortgageHolderType m_currentFirstMortgageHolderType;
        private string m_lenderBranchIdentifier;
        private string m_lenderRegistrationIdentifier;
        private string m_propertyAppraisedValueAmount;
        private string m_propertyEstimatedValueAmount;
        private string m_investorLoanIdentifier;
        private string m_investorInstitutionIdentifier;
        private string m_commitmentReferenceIdentifier;
        private E_YesNoIndicator m_concurrentOriginationIndicator;
        private E_YesNoIndicator m_concurrentOriginationLenderIndicator;
        private string m_rateLockPeriodDays;
        private E_TransmittalDataRateLockType m_rateLockType;
        private string m_rateLockRequestedExtensionDays;
        private string m_loanOriginationSystemLoanIdentifier;
        #endregion

        #region Public Properties
        public E_YesNoIndicator ArmsLengthIndicator 
        {
            get { return m_armsLengthIndicator; }
            set { m_armsLengthIndicator = value; }
        }
        public E_YesNoIndicator BelowMarketSubordinateFinancingIndicator 
        {
            get { return m_belowMarketSubordinateFinancingIndicator; }
            set { m_belowMarketSubordinateFinancingIndicator = value; }
        }
        public string BuydownRatePercent 
        {
            get { return m_buydownRatePercent; }
            set { m_buydownRatePercent = value; }
        }
        public E_TransmittalDataCaseStateType CaseStateType 
        {
            get { return m_caseStateType; }
            set { m_caseStateType = value; }
        }
        public E_YesNoIndicator CreditReportAuthorizationIndicator 
        {
            get { return m_creditReportAuthorizationIndicator; }
            set { m_creditReportAuthorizationIndicator = value; }
        }
        public E_TransmittalDataCurrentFirstMortgageHolderType CurrentFirstMortgageHolderType 
        {
            get { return m_currentFirstMortgageHolderType; }
            set { m_currentFirstMortgageHolderType = value; }
        }
        public string LenderBranchIdentifier 
        {
            get { return m_lenderBranchIdentifier; }
            set { m_lenderBranchIdentifier = value; }
        }
        public string LenderRegistrationIdentifier 
        {
            get { return m_lenderRegistrationIdentifier; }
            set { m_lenderRegistrationIdentifier = value; }
        }
        public string PropertyAppraisedValueAmount 
        {
            get { return m_propertyAppraisedValueAmount; }
            set { m_propertyAppraisedValueAmount = value; }
        }
        public string PropertyEstimatedValueAmount 
        {
            get { return m_propertyEstimatedValueAmount; }
            set { m_propertyEstimatedValueAmount = value; }
        }
        public string InvestorLoanIdentifier 
        {
            get { return m_investorLoanIdentifier; }
            set { m_investorLoanIdentifier = value; }
        }
        public string InvestorInstitutionIdentifier 
        {
            get { return m_investorInstitutionIdentifier; }
            set { m_investorInstitutionIdentifier = value; }
        }
        public string CommitmentReferenceIdentifier 
        {
            get { return m_commitmentReferenceIdentifier; }
            set { m_commitmentReferenceIdentifier = value; }
        }
        public E_YesNoIndicator ConcurrentOriginationIndicator 
        {
            get { return m_concurrentOriginationIndicator; }
            set { m_concurrentOriginationIndicator = value; }
        }
        public E_YesNoIndicator ConcurrentOriginationLenderIndicator 
        {
            get { return m_concurrentOriginationLenderIndicator; }
            set { m_concurrentOriginationLenderIndicator = value; }
        }
        public string RateLockPeriodDays 
        {
            get { return m_rateLockPeriodDays; }
            set { m_rateLockPeriodDays = value; }
        }
        public E_TransmittalDataRateLockType RateLockType 
        {
            get { return m_rateLockType; }
            set { m_rateLockType = value; }
        }
        public string RateLockRequestedExtensionDays 
        {
            get { return m_rateLockRequestedExtensionDays; }
            set { m_rateLockRequestedExtensionDays = value; }
        }
        public string LoanOriginationSystemLoanIdentifier 
        {
            get { return m_loanOriginationSystemLoanIdentifier; }
            set { m_loanOriginationSystemLoanIdentifier = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("TRANSMITTAL_DATA");
            WriteAttribute(writer, "ArmsLengthIndicator", m_armsLengthIndicator);
            WriteAttribute(writer, "BelowMarketSubordinateFinancingIndicator", m_belowMarketSubordinateFinancingIndicator);
            WriteAttribute(writer, "BuydownRatePercent", m_buydownRatePercent);
            WriteAttribute(writer, "CaseStateType", m_caseStateType);
            WriteAttribute(writer, "CreditReportAuthorizationIndicator", m_creditReportAuthorizationIndicator);
            WriteAttribute(writer, "CurrentFirstMortgageHolderType", m_currentFirstMortgageHolderType);
            WriteAttribute(writer, "LenderBranchIdentifier", m_lenderBranchIdentifier);
            WriteAttribute(writer, "LenderRegistrationIdentifier", m_lenderRegistrationIdentifier);
            WriteAttribute(writer, "PropertyAppraisedValueAmount", m_propertyAppraisedValueAmount);
            WriteAttribute(writer, "PropertyEstimatedValueAmount", m_propertyEstimatedValueAmount);
            WriteAttribute(writer, "InvestorLoanIdentifier", m_investorLoanIdentifier);
            WriteAttribute(writer, "InvestorInstitutionIdentifier", m_investorInstitutionIdentifier);
            WriteAttribute(writer, "CommitmentReferenceIdentifier", m_commitmentReferenceIdentifier);
            WriteAttribute(writer, "ConcurrentOriginationIndicator", m_concurrentOriginationIndicator);
            WriteAttribute(writer, "ConcurrentOriginationLenderIndicator", m_concurrentOriginationLenderIndicator);
            WriteAttribute(writer, "RateLockPeriodDays", m_rateLockPeriodDays);
            WriteAttribute(writer, "RateLockType", m_rateLockType);
            WriteAttribute(writer, "RateLockRequestedExtensionDays", m_rateLockRequestedExtensionDays);
            WriteAttribute(writer, "LoanOriginationSystemLoanIdentifier", m_loanOriginationSystemLoanIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_TransmittalDataCaseStateType 
    {
        Undefined = 0,
        Application,
        FinalDisposition,
        PostClosingQualityControl,
        Prequalification,
        Underwriting

    }
    public enum E_TransmittalDataCurrentFirstMortgageHolderType 
    {
        Undefined = 0,
        FNM,
        FRE,
        Other,
        Unknown
    }
    public enum E_TransmittalDataRateLockType 
    {
        Undefined = 0,
        BestEfforts,
        Mandatory
    }
}
