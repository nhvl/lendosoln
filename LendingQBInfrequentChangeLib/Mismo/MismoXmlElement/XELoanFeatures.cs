/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XELoanFeatures : Mismo.Common.AbstractXmlNode
    {
        public XELoanFeatures()
        {
        }
        #region Schema
        //  <xs:element name="LOAN_FEATURES">
        //    <xs:complexType>
        //      <xs:attribute name="NameDocumentsDrawnInType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Broker"/>
        //            <xs:enumeration value="Lender"/>
        //            <xs:enumeration value="Investor"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="AssumabilityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BalloonIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BalloonLoanMaturityTermMonths" type="xs:string"/>
        //      <xs:attribute name="BuydownTemporarySubsidyIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="CounselingConfirmationIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="CounselingConfirmationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="AmericanHomeownerEducationInstituteApprovedCounseling"/>
        //            <xs:enumeration value="LenderTrainedCounseling"/>
        //            <xs:enumeration value="NoBorrowerCounseling"/>
        //            <xs:enumeration value="ThirdPartyCounseling"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="DownPaymentOptionType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="ThreeTwoOption"/>
        //            <xs:enumeration value="FivePercentOption"/>
        //            <xs:enumeration value="FNM97Option"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="EscrowAggregateAccountingAdjustmentAmount" type="xs:string"/>
        //      <xs:attribute name="EscrowWaiverIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="EscrowCushionNumberOfMonthsCount" type="xs:string"/>
        //      <xs:attribute name="FREOfferingIdentifier" type="xs:string"/>
        //      <xs:attribute name="FNMProductPlanIdentifier" type="xs:string"/>
        //      <xs:attribute name="FNMProductPlanIndentifier" type="xs:string"/>
        //      <xs:attribute name="GSEProjectClassificationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="A_IIICondominium"/>
        //            <xs:enumeration value="ApprovedFHA_VACondominiumProjectOrSpotLoan"/>
        //            <xs:enumeration value="B_IICondominium"/>
        //            <xs:enumeration value="C_ICondominium"/>
        //            <xs:enumeration value="OneCooperative"/>
        //            <xs:enumeration value="TwoCooperative"/>
        //            <xs:enumeration value="E_PUD"/>
        //            <xs:enumeration value="F_PUD"/>
        //            <xs:enumeration value="III_PUD"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="GSEPropertyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Attached"/>
        //            <xs:enumeration value="Condominium"/>
        //            <xs:enumeration value="Cooperative"/>
        //            <xs:enumeration value="Detached"/>
        //            <xs:enumeration value="HighRiseCondominium"/>
        //            <xs:enumeration value="ManufacturedHousing"/>
        //            <xs:enumeration value="Modular"/>
        //            <xs:enumeration value="PUD"/>
        //            <xs:enumeration value="ManufacturedHousingSingleWide"/>
        //            <xs:enumeration value="ManufacturedHousingDoubleWide"/>
        //            <xs:enumeration value="DetachedCondominium"/>
        //            <xs:enumeration value="ManufacturedHomeCondominium"/>
        //            <xs:enumeration value="ManufacturedHousingMultiWide"/>
        //            <xs:enumeration value="ManufacturedHomeCondominiumOrPUDOrCooperative"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="HELOCMaximumBalanceAmount" type="xs:string"/>
        //      <xs:attribute name="HELOCInitialAdvanceAmount" type="xs:string"/>
        //      <xs:attribute name="InterestOnlyTerm" type="xs:string"/>
        //      <xs:attribute name="LenderSelfInsuredIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LienPriorityType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FirstLien"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="SecondLien"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanDocumentationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Alternative"/>
        //            <xs:enumeration value="FullDocumentation"/>
        //            <xs:enumeration value="NoDepositVerification"/>
        //            <xs:enumeration value="NoDepositVerificationEmploymentVerificationOrIncomeVerification"/>
        //            <xs:enumeration value="NoDocumentation"/>
        //            <xs:enumeration value="NoEmploymentVerificationOrIncomeVerification"/>
        //            <xs:enumeration value="Reduced"/>
        //            <xs:enumeration value="StreamlineRefinance"/>
        //            <xs:enumeration value="NoRatio"/>
        //            <xs:enumeration value="NoIncomeNoEmploymentNoAssetsOn1003"/>
        //            <xs:enumeration value="NoIncomeOn1003"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncomeEmploymentOrAssets"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncomeOrAssests"/>
        //            <xs:enumeration value="NoVerificationOfStatedAssets"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncomeOrEmployment"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncome"/>
        //            <xs:enumeration value="VerbalVerificationOfEmployment"/>
        //            <xs:enumeration value="OnePaystub"/>
        //            <xs:enumeration value="OnePaystubAndVerbalVerificationOfEmployment"/>
        //            <xs:enumeration value="OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanRepaymentType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="InterestOnly"/>
        //            <xs:enumeration value="NoNegativeAmortization"/>
        //            <xs:enumeration value="PotentialNegativeAmortization"/>
        //            <xs:enumeration value="ScheduledAmortization"/>
        //            <xs:enumeration value="ScheduledNegativeAmortization"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanScheduledClosingDate" type="xs:string"/>
        //      <xs:attribute name="MICertificationStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="LenderToObtain"/>
        //            <xs:enumeration value="SellerOfLoanToObtain"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MICoveragePercent" type="xs:string"/>
        //      <xs:attribute name="MICompanyNameType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="AmerinGuaranteeCorporation"/>
        //            <xs:enumeration value="CMG_MICompany"/>
        //            <xs:enumeration value="CommonwealthMortgageAssuranceCompany"/>
        //            <xs:enumeration value="GECapitalMICorporation"/>
        //            <xs:enumeration value="MortgageGuarantyInsuranceCorporation"/>
        //            <xs:enumeration value="PMI_MICorporation"/>
        //            <xs:enumeration value="RadianGuarantyIncorporated"/>
        //            <xs:enumeration value="RepublicMICompany"/>
        //            <xs:enumeration value="TriadGuarantyInsuranceCorporation"/>
        //            <xs:enumeration value="UnitedGuarantyCorporation"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="NegativeAmortizationLimitPercent" type="xs:string"/>
        //      <xs:attribute name="PaymentFrequencyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Biweekly"/>
        //            <xs:enumeration value="Monthly"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PrepaymentPenaltyIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="FullPrepaymentPenaltyOptionType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Hard"/>
        //            <xs:enumeration value="Soft"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PrepaymentPenaltyTermMonths" type="xs:string"/>
        //      <xs:attribute name="PrepaymentRestrictionIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="ProductDescription" type="xs:string"/>
        //      <xs:attribute name="ProductName" type="xs:string"/>
        //      <xs:attribute name="ScheduledFirstPaymentDate" type="xs:string"/>
        //      <xs:attribute name="LoanClosingStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Closed"/>
        //            <xs:enumeration value="TableFunded"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="ServicingTransferStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Retained"/>
        //            <xs:enumeration value="Released"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="DemandFeatureIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="EstimatedPrepaidDays" type="xs:string"/>
        //      <xs:attribute name="EstimatedPrepaidDaysPaidByType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Buyer"/>
        //            <xs:enumeration value="Lender"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Seller"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="EstimatedPrepaidDaysPaidByOtherTypeDescription" type="xs:string"/>
        //      <xs:attribute name="InitialPaymentRatePercent" type="xs:string"/>
        //      <xs:attribute name="PrepaymentFinanceChargeRefundableIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="RefundableApplicationFeeIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="RequiredDepositIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="OriginalBalloonTermMonths" type="xs:string"/>
        //      <xs:attribute name="ConditionsToAssumabilityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_NameDocumentsDrawInType m_nameDocumentsDrawnInType;
        private E_YesNoIndicator m_assumabilityIndicator;
        private E_YesNoIndicator m_balloonIndicator;
        private string m_balloonLoanMaturityTermMonths;
        private E_YesNoIndicator m_buydownTemporarySubsidyIndicator;
        private E_YesNoIndicator m_counselingConfirmationIndicator;
        private E_CounselingConfirmationType m_counselingConfirmationType;
        private E_DownPaymentOptionType m_downPaymentOptionType;
        private string m_escrowAggregateAccountingAdjustmentAmount;
        private E_YesNoIndicator m_escrowWaiverIndicator;
        private string m_escrowCushionNumberOfMonthsCount;
        private string m_freOfferingIdentifier;
        private string m_fnmProductPlanIdentifier;
        private string m_fnmProductPlanIndentifier;
        private E_GSEProjectClassificationType m_GSEProjectClassificationType;
        private E_GSEPropertyType m_GSEPropertyType;
        private string m_helocMaximumBalanceAmount;
        private string m_helocInitialAdvanceAmount;
        private string m_interestOnlyTerm;
        private E_YesNoIndicator m_lenderSelfInsuredIndicator;
        private E_LienPriorityType m_lienPriorityType;
        private E_LoanDocumentationType m_loanDocumentationType;
        private E_LoanRepaymentType m_loanRepaymentType;
        private string m_loanScheduledClosingDate;
        private E_MICertificationStatusType m_MICertificationStatusType;
        private string m_MICoveragePercent;
        private E_MICompanyNameType m_MICompanyNameType;
        private string m_negativeAmortizationLimitPercent;
        private E_PaymentFrequencyType m_paymentFrequencyType;
        private E_YesNoIndicator m_prepaymentPenaltyIndicator;
        private E_FullPrepaymentPenaltyOptionType m_fullPrepaymentPenaltyOptionType;
        private string m_prepaymentPenaltyTermMonths;
        private E_YesNoIndicator m_prepaymentRestrictionIndicator;
        private string m_productDescription;
        private string m_productName;
        private string m_scheduledFirstPaymentDate;
        private E_LoanClosingStatusType m_loanClosingStatusType;
        private E_ServicingTransferStatusType m_servicingTransferStatusType;
        private E_YesNoIndicator m_demandFeatureIndicator;
        private string m_estimatedPrepaidDays;
        private E_EstimatedPrepaidDaysPaidByType m_EstimatedPrepaidDaysPaidByType;
        private string m_estimatedPrepaidDaysPaidByOtherTypeDescription;
        private string m_initialPaymentRatePercent;
        private E_YesNoIndicator m_prepaymentFinanceChargeRefundableIndicator;
        private E_YesNoIndicator m_refundableApplicationFeeIndicator;
        private E_YesNoIndicator m_requiredDepositIndicator;
        private string m_originalBalloonTermMonths;
        private E_YesNoIndicator m_conditionsToAssumabilityIndicator;
        #endregion

        #region Public Properties
        public E_NameDocumentsDrawInType NameDocumentsDrawnInType 
        {
            get { return m_nameDocumentsDrawnInType; }
            set { m_nameDocumentsDrawnInType = value; }
        }
        public E_YesNoIndicator AssumabilityIndicator 
        {
            get { return m_assumabilityIndicator; }
            set { m_assumabilityIndicator = value; }
        }
        public E_YesNoIndicator BalloonIndicator 
        {
            get { return m_balloonIndicator; }
            set { m_balloonIndicator = value; }
        }
        public string BalloonLoanMaturityTermMonths 
        {
            get { return m_balloonLoanMaturityTermMonths; }
            set { m_balloonLoanMaturityTermMonths = value; }
        }
        public E_YesNoIndicator BuydownTemporarySubsidyIndicator 
        {
            get { return m_buydownTemporarySubsidyIndicator; }
            set { m_buydownTemporarySubsidyIndicator = value; }
        }
        public E_YesNoIndicator CounselingConfirmationIndicator 
        {
            get { return m_counselingConfirmationIndicator; }
            set { m_counselingConfirmationIndicator = value; }
        }
        public E_CounselingConfirmationType CounselingConfirmationType 
        {
            get { return m_counselingConfirmationType; }
            set { m_counselingConfirmationType = value; }
        }
        public E_DownPaymentOptionType DownPaymentOptionType 
        {
            get { return m_downPaymentOptionType; }
            set { m_downPaymentOptionType = value; }
        }
        public string EscrowAggregateAccountingAdjustmentAmount 
        {
            get { return m_escrowAggregateAccountingAdjustmentAmount; }
            set { m_escrowAggregateAccountingAdjustmentAmount = value; }
        }
        public E_YesNoIndicator EscrowWaiverIndicator 
        {
            get { return m_escrowWaiverIndicator; }
            set { m_escrowWaiverIndicator = value; }
        }
        public string EscrowCushionNumberOfMonthsCount 
        {
            get { return m_escrowCushionNumberOfMonthsCount; }
            set { m_escrowCushionNumberOfMonthsCount = value; }
        }
        public string FreOfferingIdentifier 
        {
            get { return m_freOfferingIdentifier; }
            set { m_freOfferingIdentifier = value; }
        }
        public string FnmProductPlanIdentifier 
        {
            get { return m_fnmProductPlanIdentifier; }
            set { m_fnmProductPlanIdentifier = value; }
        }
        public string FnmProductPlanIndentifier 
        {
            get { return m_fnmProductPlanIndentifier; }
            set { m_fnmProductPlanIndentifier = value; }
        }
        public E_GSEProjectClassificationType GSEProjectClassificationType 
        {
            get { return m_GSEProjectClassificationType; }
            set { m_GSEProjectClassificationType = value; }
        }
        public E_GSEPropertyType GSEPropertyType 
        {
            get { return m_GSEPropertyType; }
            set { m_GSEPropertyType = value; }
        }
        public string HelocMaximumBalanceAmount 
        {
            get { return m_helocMaximumBalanceAmount; }
            set { m_helocMaximumBalanceAmount = value; }
        }
        public string HelocInitialAdvanceAmount 
        {
            get { return m_helocInitialAdvanceAmount; }
            set { m_helocInitialAdvanceAmount = value; }
        }
        public string InterestOnlyTerm 
        {
            get { return m_interestOnlyTerm; }
            set { m_interestOnlyTerm = value; }
        }
        public E_YesNoIndicator LenderSelfInsuredIndicator 
        {
            get { return m_lenderSelfInsuredIndicator; }
            set { m_lenderSelfInsuredIndicator = value; }
        }
        public E_LienPriorityType LienPriorityType 
        {
            get { return m_lienPriorityType; }
            set { m_lienPriorityType = value; }
        }
        public E_LoanDocumentationType LoanDocumentationType 
        {
            get { return m_loanDocumentationType; }
            set { m_loanDocumentationType = value; }
        }
        public E_LoanRepaymentType LoanRepaymentType 
        {
            get { return m_loanRepaymentType; }
            set { m_loanRepaymentType = value; }
        }
        public string LoanScheduledClosingDate 
        {
            get { return m_loanScheduledClosingDate; }
            set { m_loanScheduledClosingDate = value; }
        }
        public E_MICertificationStatusType MICertificationStatusType 
        {
            get { return m_MICertificationStatusType; }
            set { m_MICertificationStatusType = value; }
        }
        public string MICoveragePercent 
        {
            get { return m_MICoveragePercent; }
            set { m_MICoveragePercent = value; }
        }
        public E_MICompanyNameType MICompanyNameType 
        {
            get { return m_MICompanyNameType; }
            set { m_MICompanyNameType = value; }
        }
        public string NegativeAmortizationLimitPercent 
        {
            get { return m_negativeAmortizationLimitPercent; }
            set { m_negativeAmortizationLimitPercent = value; }
        }
        public E_PaymentFrequencyType PaymentFrequencyType 
        {
            get { return m_paymentFrequencyType; }
            set { m_paymentFrequencyType = value; }
        }
        public E_YesNoIndicator PrepaymentPenaltyIndicator 
        {
            get { return m_prepaymentPenaltyIndicator; }
            set { m_prepaymentPenaltyIndicator = value; }
        }
        public E_FullPrepaymentPenaltyOptionType FullPrepaymentPenaltyOptionType 
        {
            get { return m_fullPrepaymentPenaltyOptionType; }
            set { m_fullPrepaymentPenaltyOptionType = value; }
        }
        public string PrepaymentPenaltyTermMonths 
        {
            get { return m_prepaymentPenaltyTermMonths; }
            set { m_prepaymentPenaltyTermMonths = value; }
        }
        public E_YesNoIndicator PrepaymentRestrictionIndicator 
        {
            get { return m_prepaymentRestrictionIndicator; }
            set { m_prepaymentRestrictionIndicator = value; }
        }
        public string ProductDescription 
        {
            get { return m_productDescription; }
            set { m_productDescription = value; }
        }
        public string ProductName 
        {
            get { return m_productName; }
            set { m_productName = value; }
        }
        public string ScheduledFirstPaymentDate 
        {
            get { return m_scheduledFirstPaymentDate; }
            set { m_scheduledFirstPaymentDate = value; }
        }
        public E_LoanClosingStatusType LoanClosingStatusType 
        {
            get { return m_loanClosingStatusType; }
            set { m_loanClosingStatusType = value; }
        }
        public E_ServicingTransferStatusType ServicingTransferStatusType 
        {
            get { return m_servicingTransferStatusType; }
            set { m_servicingTransferStatusType = value; }
        }
        public E_YesNoIndicator DemandFeatureIndicator 
        {
            get { return m_demandFeatureIndicator; }
            set { m_demandFeatureIndicator = value; }
        }
        public string EstimatedPrepaidDays 
        {
            get { return m_estimatedPrepaidDays; }
            set { m_estimatedPrepaidDays = value; }
        }
        public E_EstimatedPrepaidDaysPaidByType EstimatedPrepaidDaysPaidByType 
        {
            get { return m_EstimatedPrepaidDaysPaidByType; }
            set { m_EstimatedPrepaidDaysPaidByType = value; }
        }
        public string EstimatedPrepaidDaysPaidByOtherTypeDescription 
        {
            get { return m_estimatedPrepaidDaysPaidByOtherTypeDescription; }
            set { m_estimatedPrepaidDaysPaidByOtherTypeDescription = value; }
        }
        public string InitialPaymentRatePercent 
        {
            get { return m_initialPaymentRatePercent; }
            set { m_initialPaymentRatePercent = value; }
        }
        public E_YesNoIndicator PrepaymentFinanceChargeRefundableIndicator 
        {
            get { return m_prepaymentFinanceChargeRefundableIndicator; }
            set { m_prepaymentFinanceChargeRefundableIndicator = value; }
        }
        public E_YesNoIndicator RefundableApplicationFeeIndicator 
        {
            get { return m_refundableApplicationFeeIndicator; }
            set { m_refundableApplicationFeeIndicator = value; }
        }
        public E_YesNoIndicator RequiredDepositIndicator 
        {
            get { return m_requiredDepositIndicator; }
            set { m_requiredDepositIndicator = value; }
        }
        public string OriginalBalloonTermMonths 
        {
            get { return m_originalBalloonTermMonths; }
            set { m_originalBalloonTermMonths = value; }
        }
        public E_YesNoIndicator ConditionsToAssumabilityIndicator 
        {
            get { return m_conditionsToAssumabilityIndicator; }
            set { m_conditionsToAssumabilityIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_FEATURES");
            WriteAttribute(writer, "NameDocumentsDrawnInType", m_nameDocumentsDrawnInType);
            WriteAttribute(writer, "AssumabilityIndicator", m_assumabilityIndicator);
            WriteAttribute(writer, "BalloonIndicator", m_balloonIndicator);
            WriteAttribute(writer, "BalloonLoanMaturityTermMonths", m_balloonLoanMaturityTermMonths);
            WriteAttribute(writer, "BuydownTemporarySubsidyIndicator", m_buydownTemporarySubsidyIndicator);
            WriteAttribute(writer, "CounselingConfirmationIndicator", m_counselingConfirmationIndicator);
            WriteAttribute(writer, "CounselingConfirmationType", m_counselingConfirmationType);
            WriteAttribute(writer, "DownPaymentOptionType", m_downPaymentOptionType);
            WriteAttribute(writer, "EscrowAggregateAccountingAdjustmentAmount", m_escrowAggregateAccountingAdjustmentAmount);
            WriteAttribute(writer, "EscrowWaiverIndicator", m_escrowWaiverIndicator);
            WriteAttribute(writer, "EscrowCushionNumberOfMonthsCount", m_escrowCushionNumberOfMonthsCount);
            WriteAttribute(writer, "FREOfferingIdentifier", m_freOfferingIdentifier);
            WriteAttribute(writer, "FNMProductPlanIdentifier", m_fnmProductPlanIdentifier);
            WriteAttribute(writer, "FNMProductPlanIndentifier", m_fnmProductPlanIndentifier);
            WriteAttribute(writer, "GSEProjectClassificationType", m_GSEProjectClassificationType);
            WriteAttribute(writer, "GSEPropertyType", m_GSEPropertyType);
            WriteAttribute(writer, "HELOCMaximumBalanceAmount", m_helocMaximumBalanceAmount);
            WriteAttribute(writer, "HELOCInitialAdvanceAmount", m_helocInitialAdvanceAmount);
            WriteAttribute(writer, "InterestOnlyTerm", m_interestOnlyTerm);
            WriteAttribute(writer, "LenderSelfInsuredIndicator", m_lenderSelfInsuredIndicator);
            WriteAttribute(writer, "LienPriorityType", m_lienPriorityType);
            WriteAttribute(writer, "LoanDocumentationType", m_loanDocumentationType);
            WriteAttribute(writer, "LoanRepaymentType", m_loanRepaymentType);
            WriteAttribute(writer, "LoanScheduledClosingDate", m_loanScheduledClosingDate);
            WriteAttribute(writer, "MICertificationStatusType", m_MICertificationStatusType);
            WriteAttribute(writer, "MICoveragePercent", m_MICoveragePercent);
            WriteAttribute(writer, "MICompanyNameType", m_MICompanyNameType);
            WriteAttribute(writer, "NegativeAmortizationLimitPercent", m_negativeAmortizationLimitPercent);
            WriteAttribute(writer, "PaymentFrequencyType", m_paymentFrequencyType);
            WriteAttribute(writer, "PrepaymentPenaltyIndicator", m_prepaymentPenaltyIndicator);
            WriteAttribute(writer, "FullPrepaymentPenaltyOptionType", m_fullPrepaymentPenaltyOptionType);
            WriteAttribute(writer, "PrepaymentPenaltyTermMonths", m_prepaymentPenaltyTermMonths);
            WriteAttribute(writer, "PrepaymentRestrictionIndicator", m_prepaymentRestrictionIndicator);
            WriteAttribute(writer, "ProductDescription", m_productDescription);
            WriteAttribute(writer, "ProductName", m_productName);
            WriteAttribute(writer, "ScheduledFirstPaymentDate", m_scheduledFirstPaymentDate);
            WriteAttribute(writer, "LoanClosingStatusType", m_loanClosingStatusType);
            WriteAttribute(writer, "ServicingTransferStatusType", m_servicingTransferStatusType);
            WriteAttribute(writer, "DemandFeatureIndicator", m_demandFeatureIndicator);
            WriteAttribute(writer, "EstimatedPrepaidDays", m_estimatedPrepaidDays);
            WriteAttribute(writer, "EstimatedPrepaidDaysPaidByType", m_EstimatedPrepaidDaysPaidByType);
            WriteAttribute(writer, "EstimatedPrepaidDaysPaidByOtherTypeDescription", m_estimatedPrepaidDaysPaidByOtherTypeDescription);
            WriteAttribute(writer, "InitialPaymentRatePercent", m_initialPaymentRatePercent);
            WriteAttribute(writer, "PrepaymentFinanceChargeRefundableIndicator", m_prepaymentFinanceChargeRefundableIndicator);
            WriteAttribute(writer, "RefundableApplicationFeeIndicator", m_refundableApplicationFeeIndicator);
            WriteAttribute(writer, "RequiredDepositIndicator", m_requiredDepositIndicator);
            WriteAttribute(writer, "OriginalBalloonTermMonths", m_originalBalloonTermMonths);
            WriteAttribute(writer, "ConditionsToAssumabilityIndicator", m_conditionsToAssumabilityIndicator);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_NameDocumentsDrawInType 
    {
        Undefined = 0,
        Broker,
        Lender,
        Investor
    }
    public enum E_CounselingConfirmationType 
    {
        Undefined = 0,
        AmericanHomeownerEducationInstituteApprovedCounseling,
        LenderTrainedCounseling,
        NoBorrowerCounseling,
        ThirdPartyCounseling
    }
    public enum E_DownPaymentOptionType 
    {
        Undefined = 0,
        ThreeTwoOption,
        FivePercentOption,
        FNM97Option
    }
    public enum E_GSEProjectClassificationType 
    {
        Undefined = 0,
        A_IIICondominium,
        ApprovedFHA_VACondominiumProjectOrSpotLoan,
        B_IICondominium,
        C_ICondominium,
        OneCooperative,
        TwoCooperative,
        E_PUD,
        F_PUD,
        III_PUD
    }
    public enum E_GSEPropertyType 
    {
        Undefined = 0,
        Attached,
        Condominium,
        Cooperative,
        Detached,
        HighRiseCondominium,
        ManufacturedHousing,
        Modular,
        PUD,
        ManufacturedHousingSingleWide,
        ManufacturedHousingDoubleWide,
        DetachedCondominium,
        ManufacturedHomeCondominium,
        ManufacturedHousingMultiWide,
        ManufacturedHomeCondominiumOrPUDOrCooperative
    }
    public enum E_LienPriorityType 
    {
        Undefined = 0,
        FirstLien,
        Other,
        SecondLien
    }
    public enum E_LoanDocumentationType 
    {
        Undefined = 0,
        Alternative,
        FullDocumentation,
        NoDepositVerification,
        NoDepositVerificationEmploymentVerificationOrIncomeVerification,
        NoDocumentation,
        NoEmploymentVerificationOrIncomeVerification,
        Reduced,
        StreamlineRefinance,
        NoRatio,
        NoIncomeNoEmploymentNoAssetsOn1003,
        NoIncomeOn1003,
        NoVerificationOfStatedIncomeEmploymentOrAssets,
        NoVerificationOfStatedIncomeOrAssests,
        NoVerificationOfStatedAssets,
        NoVerificationOfStatedIncomeOrEmployment,
        NoVerificationOfStatedIncome,
        VerbalVerificationOfEmployment,
        OnePaystub,
        OnePaystubAndVerbalVerificationOfEmployment,
        OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040
    }
    public enum E_LoanRepaymentType 
    {
        Undefined = 0,
        InterestOnly,
        NoNegativeAmortization,
        PotentialNegativeAmortization,
        ScheduledAmortization,
        ScheduledNegativeAmortization
    }
    public enum E_MICertificationStatusType 
    {
        Undefined = 0,
        LenderToObtain,
        SellerOfLoanToObtain
    }
    public enum E_MICompanyNameType 
    {
        Undefined = 0,
        AmerinGuaranteeCorporation,
        CMG_MICompany,
        CommonwealthMortgageAssuranceCompany,
        GECapitalMICorporation,
        MortgageGuarantyInsuranceCorporation,
        PMI_MICorporation,
        RadianGuarantyIncorporated,
        RepublicMICompany,
        TriadGuarantyInsuranceCorporation,
        UnitedGuarantyCorporation
    }
    public enum E_PaymentFrequencyType 
    {
        Undefined = 0,
        Biweekly,
        Monthly
    }
    public enum E_FullPrepaymentPenaltyOptionType 
    {
        Undefined = 0,
        Hard,
        Soft
    }
    public enum E_LoanClosingStatusType 
    {
        Undefined = 0,
        Closed,
        TableFunded
    }
    public enum E_ServicingTransferStatusType 
    {
        Undefined = 0,
        Retained,
        Released
    }
    public enum E_EstimatedPrepaidDaysPaidByType 
    {
        Undefined = 0,
        Buyer,
        Lender,
        Other,
        Seller
    }
}
