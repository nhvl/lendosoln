/// Author: David Dao

using System;
using System.Xml;
using System.Collections;
namespace Mismo.MismoXmlElement
{
    public class XEAdditionalCaseData : Mismo.Common.AbstractXmlNode
    {
        public XEAdditionalCaseData()
        {
        }
        #region Schema
        //<xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
        //  <xs:element name="ADDITIONAL_CASE_DATA">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="MORTGAGE_SCORE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="TRANSMITTAL_DATA" minOccurs="0"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private ArrayList m_mortgageScoreList = new ArrayList();
        private XETransmittalData m_transmittalData;
        #endregion

        #region Public Properties
        public void AddMortgageScore(XEMortgageScore mortgageScore) 
        {
            m_mortgageScoreList.Add(mortgageScore);
        }
        public void SetTransmittalData(XETransmittalData transmittalData) 
        {
            m_transmittalData = transmittalData;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ADDITIONAL_CASE_DATA");

            WriteElement(writer, m_mortgageScoreList);
            WriteElement(writer, m_transmittalData);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
