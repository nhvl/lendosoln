/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEParsedStreetAddress : Mismo.Common.AbstractXmlNode
    {
        public XEParsedStreetAddress()
        {
        }
        #region Schema
        //  <xs:element name="PARSED_STREET_ADDRESS">
        //    <xs:complexType>
        //      <xs:attribute name="_ApartmentOrUnit" type="xs:string"/>
        //      <xs:attribute name="_DirectionPrefix" type="xs:string"/>
        //      <xs:attribute name="_DirectionSuffix" type="xs:string"/>
        //      <xs:attribute name="_BuildingNumber" type="xs:string"/>
        //      <xs:attribute name="_HouseNumber" type="xs:string"/>
        //      <xs:attribute name="_Military_APO_FPO" type="xs:string"/>
        //      <xs:attribute name="_PostOfficeBox" type="xs:string"/>
        //      <xs:attribute name="_RuralRoute" type="xs:string"/>
        //      <xs:attribute name="_StreetName" type="xs:string"/>
        //      <xs:attribute name="_StreetSuffix" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_apartmentOrUnit;
        private string m_directionPrefix;
        private string m_directionSuffix;
        private string m_buildingNumber;
        private string m_houseNumber;
        private string m_militaryApoFpo;
        private string m_postOfficeBox;
        private string m_ruralRoute;
        private string m_streetName;
        private string m_streetSuffix;
        #endregion

        #region Public Properties
        public string ApartmentOrUnit 
        {
            get { return m_apartmentOrUnit; }
            set { m_apartmentOrUnit = value; }
        }
        public string DirectionPrefix 
        {
            get { return m_directionPrefix; }
            set { m_directionPrefix = value; }
        }
        public string DirectionSuffix 
        {
            get { return m_directionSuffix; }
            set { m_directionSuffix = value; }
        }
        public string BuildingNumber 
        {
            get { return m_buildingNumber; }
            set { m_buildingNumber = value; }
        }
        public string HouseNumber 
        {
            get { return m_houseNumber; }
            set { m_houseNumber = value; }
        }
        public string MilitaryApoFpo 
        {
            get { return m_militaryApoFpo; }
            set { m_militaryApoFpo = value; }
        }
        public string PostOfficeBox 
        {
            get { return m_postOfficeBox; }
            set { m_postOfficeBox = value; }
        }
        public string RuralRoute 
        {
            get { return m_ruralRoute; }
            set { m_ruralRoute = value; }
        }
        public string StreetName 
        {
            get { return m_streetName; }
            set { m_streetName = value; }
        }
        public string StreetSuffix 
        {
            get { return m_streetSuffix; }
            set { m_streetSuffix = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PARSED_STREET_ADDRESS");
            WriteAttribute(writer, "_ApartmentOrUnit", m_apartmentOrUnit);
            WriteAttribute(writer, "_DirectionPrefix", m_directionPrefix);
            WriteAttribute(writer, "_DirectionSuffix", m_directionSuffix);
            WriteAttribute(writer, "_BuildingNumber", m_buildingNumber);
            WriteAttribute(writer, "_HouseNumber", m_houseNumber);
            WriteAttribute(writer, "_Military_APO_FPO", m_militaryApoFpo);
            WriteAttribute(writer, "_PostOfficeBox", m_postOfficeBox);
            WriteAttribute(writer, "_RuralRoute", m_ruralRoute);
            WriteAttribute(writer, "_StreetName", m_streetName);
            WriteAttribute(writer, "_StreetSuffix", m_streetSuffix);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
