/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.MismoXmlElement
{
    public class XEAlias : Mismo.Common.AbstractXmlNode
    {
        public XEAlias()
        {
        }
        #region Schema
        //  <xs:element name="_ALIAS">
        //    <xs:complexType>
        //      <xs:attribute name="_FirstName" type="xs:string"/>
        //      <xs:attribute name="_LastName" type="xs:string"/>
        //      <xs:attribute name="_MiddleName" type="xs:string"/>
        //      <xs:attribute name="_AccountIdentifier" type="xs:string"/>
        //      <xs:attribute name="_CreditorName" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_firstName;
        private string m_lastName;
        private string m_middleName;
        private string m_accountIdentifier;
        private string m_creditorName;
        #endregion

        #region Public Properties
        public string FirstName 
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }
        public string LastName 
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }
        public string MiddleName 
        {
            get { return m_middleName; }
            set { m_middleName = value; }
        }
        public string AccountIdentifier 
        {
            get { return m_accountIdentifier; }
            set { m_accountIdentifier = value; }
        }
        public string CreditorName 
        {
            get { return m_creditorName; }
            set { m_creditorName = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_ALIAS");
            WriteAttribute(writer, "_Firstname", m_firstName);
            WriteAttribute(writer, "_LastName", m_lastName);
            WriteAttribute(writer, "_MiddleName", m_middleName);
            WriteAttribute(writer, "_AccountIdentifier", m_accountIdentifier);
            WriteAttribute(writer, "_CreditorName", m_creditorName);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
