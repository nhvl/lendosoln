/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEFhaLoan : Mismo.Common.AbstractXmlNode
	{
		public XEFhaLoan()
		{
		}
        #region Schema
        // <xs:element name="FHA_LOAN">
        //   <xs:complexType>
        //     <xs:attribute name="BorrowerFinancedFHADiscountPointsAmount" type="xs:string"/>
        //     <xs:attribute name="FHAAlimonyLiabilityTreatmentType">
        //     <xs:attribute name="FHACoverageRenewalRatePercent" type="xs:string"/>
        //     <xs:attribute name="FHA_MIPremiumRefundAmount" type="xs:string"/>
        //     <xs:attribute name="FHAUpfrontMIPremiumPercent" type="xs:string"/>
        //     <xs:attribute name="_LenderIdentifier" type="xs:string"/>
        //     <xs:attribute name="_SponsorIdentifier" type="xs:string"/>
        //     <xs:attribute name="SectionOfActType">
        //     <xs:attribute name="FHAEnergyRelatedRepairsOrImprovementsAmount" type="xs:string"/>
        //     <xs:attribute name="FHAGeneralServicesAdminstrationCodeIdentifier" type="xs:string"/>
        //     <xs:attribute name="FHALimitedDenialParticipationIdentifier" type="xs:string"/>
        //     <xs:attribute name="FHARefinanceInterestOnExistingLienAmount" type="xs:string"/>
        //     <xs:attribute name="FHARefinanceOriginalExistingFHACaseIdentifier" type="xs:string"/>
        //     <xs:attribute name="FHARefinanceOriginalExistingUpFrontMIPAmount" type="xs:string"/>
        //     <xs:attribute name="FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier" type="xs:string"/>
        //     <xs:attribute name="HUDAdequateAvailableAssetsIndicator">
        //     <xs:attribute name="HUDAdequateEffectiveIncomeIndicator">
        //     <xs:attribute name="HUDCreditCharacteristicsIndicator">
        //     <xs:attribute name="HUDStableEffectiveIncomeIndicator">
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_borrowerFinancedFhaDiscountPointsAmount;
        private E_FhaAlimonyLiabilityTreatmentType m_fhaAlimonyLiabilityTreatmentType;
        private string m_fhaCoverageRenewalRatePercent;
        private string m_fhaMIPremiumRefundAmount;
        private string m_fhaUpfrontMIPremiumPercent;
        private string m_lenderIdentifier;
        private string m_sponsorIdentifier;
        private E_FhaLoanSectionOfActType m_sectionOfActType;
        private string m_fhaEnergyRelatedRepairsOrImprovementsAmount;
        private string m_fhaGeneralServicesAdminstrationCodeIdentifier;
        private string m_fhaLimitedDenialParticipationIdentifier;
        private string m_fhaRefinanceInterestOnExistingLienAmount;
        private string m_fhaRefinanceOriginalExistingFhaCaseIdentifier;
        private string m_fhaRefinanceOriginalExistingUpFrontMIPAmount;
        private string m_fhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier;
        private E_YesNoIndicator m_hudAdequateAvailableAssetsIndicator;
        private E_YesNoIndicator m_hudAdequateEffectiveIncomeIndicator;
        private E_YesNoIndicator m_hudCreditCharacteristicsIndicator;
        private E_YesNoIndicator m_hudStableEffectiveIncomeIndicator;
        #endregion

        #region Public Properties
        public string BorrowerFinancedFhaDiscountPointsAmount 
        {
            get { return m_borrowerFinancedFhaDiscountPointsAmount; }
            set { m_borrowerFinancedFhaDiscountPointsAmount = value; }
        }
        public E_FhaAlimonyLiabilityTreatmentType FhaAlimonyLiabilityTreatmentType 
        {
            get { return m_fhaAlimonyLiabilityTreatmentType; }
            set { m_fhaAlimonyLiabilityTreatmentType = value; }
        }
        public string FhaCoverageRenewalRatePercent 
        {
            get { return m_fhaCoverageRenewalRatePercent; }
            set { m_fhaCoverageRenewalRatePercent = value; }
        }
        public string FhaMIPremiumRefundAmount 
        {
            get { return m_fhaMIPremiumRefundAmount; }
            set { m_fhaMIPremiumRefundAmount = value; }
        }
        public string FhaUpfrontMIPremiumPercent 
        {
            get { return m_fhaUpfrontMIPremiumPercent; }
            set { m_fhaUpfrontMIPremiumPercent = value; }
        }
        public string LenderIdentifier 
        {
            get { return m_lenderIdentifier; }
            set { m_lenderIdentifier = value; }
        }
        public string SponsorIdentifier 
        {
            get { return m_sponsorIdentifier; }
            set { m_sponsorIdentifier = value; }
        }
        public E_FhaLoanSectionOfActType SectionOfActType 
        {
            get { return m_sectionOfActType; }
            set { m_sectionOfActType = value; }
        }
        public string FhaEnergyRelatedRepairsOrImprovementsAmount 
        {
            get { return m_fhaEnergyRelatedRepairsOrImprovementsAmount; }
            set { m_fhaEnergyRelatedRepairsOrImprovementsAmount = value; }
        }
        public string FhaGeneralServicesAdminstrationCodeIdentifier 
        {
            get { return m_fhaGeneralServicesAdminstrationCodeIdentifier; }
            set { m_fhaGeneralServicesAdminstrationCodeIdentifier = value; }
        }
        public string FhaLimitedDenialParticipationIdentifier 
        {
            get { return m_fhaLimitedDenialParticipationIdentifier; }
            set { m_fhaLimitedDenialParticipationIdentifier = value; }
        }
        public string FhaRefinanceInterestOnExistingLienAmount 
        {
            get { return m_fhaRefinanceInterestOnExistingLienAmount; }
            set { m_fhaRefinanceInterestOnExistingLienAmount = value; }
        }
        public string FhaRefinanceOriginalExistingFhaCaseIdentifier 
        {
            get { return m_fhaRefinanceOriginalExistingFhaCaseIdentifier; }
            set { m_fhaRefinanceOriginalExistingFhaCaseIdentifier = value; }
        }
        public string FhaRefinanceOriginalExistingUpFrontMIPAmount 
        {
            get { return m_fhaRefinanceOriginalExistingUpFrontMIPAmount; }
            set { m_fhaRefinanceOriginalExistingUpFrontMIPAmount = value; }
        }
        public string FhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier 
        {
            get { return m_fhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier; }
            set { m_fhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier = value; }
        }
        public E_YesNoIndicator HudAdequateAvailableAssetsIndicator 
        {
            get { return m_hudAdequateAvailableAssetsIndicator; }
            set { m_hudAdequateAvailableAssetsIndicator = value; }
        }
        public E_YesNoIndicator HudAdequateEffectiveIncomeIndicator 
        {
            get { return m_hudAdequateEffectiveIncomeIndicator; }
            set { m_hudAdequateEffectiveIncomeIndicator = value; }
        }
        public E_YesNoIndicator HudCreditCharacteristicsIndicator 
        {
            get { return m_hudCreditCharacteristicsIndicator; }
            set { m_hudCreditCharacteristicsIndicator = value; }
        }
        public E_YesNoIndicator HudStableEffectiveIncomeIndicator 
        {
            get { return m_hudStableEffectiveIncomeIndicator; }
            set { m_hudStableEffectiveIncomeIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("FHA_LOAN");
            WriteAttribute(writer, "BorrowerFinancedFHADiscountPointsAmount", m_borrowerFinancedFhaDiscountPointsAmount);
            WriteAttribute(writer, "FHAAlimonyLiabilityTreatmentType", m_fhaAlimonyLiabilityTreatmentType);
            WriteAttribute(writer, "FHACoverageRenewalRatePercent", m_fhaCoverageRenewalRatePercent);
            WriteAttribute(writer, "FHA_MIPremiumRefundAmount", m_fhaMIPremiumRefundAmount);
            WriteAttribute(writer, "FHAUpfrontMIPremiumPercent", m_fhaUpfrontMIPremiumPercent);
            WriteAttribute(writer, "_LenderIdentifier", m_lenderIdentifier);
            WriteAttribute(writer, "_SponsorIdentifier", m_sponsorIdentifier);

            // Need to strip '_' in front of enum.
            if (m_sectionOfActType != E_FhaLoanSectionOfActType.Undefined) 
            {
                string sectionOfActType = m_sectionOfActType.ToString().Substring(1);
                WriteAttribute(writer, "SectionOfActType", sectionOfActType);
            }

            WriteAttribute(writer, "FHAEnergyRelatedRepairsOrImprovementsAmount", m_fhaEnergyRelatedRepairsOrImprovementsAmount);
            WriteAttribute(writer, "FHAGeneralServicesAdminstrationCodeIdentifier", m_fhaGeneralServicesAdminstrationCodeIdentifier);
            WriteAttribute(writer, "FHALimitedDenialParticipationIdentifier", m_fhaLimitedDenialParticipationIdentifier);
            WriteAttribute(writer, "FHARefinanceInterestOnExistingLienAmount", m_fhaRefinanceInterestOnExistingLienAmount);
            WriteAttribute(writer, "FHARefinanceOriginalExistingFHACaseIdentifier", m_fhaRefinanceOriginalExistingFhaCaseIdentifier);
            WriteAttribute(writer, "FHARefinanceOriginalExistingUpFrontMIPAmount", m_fhaRefinanceOriginalExistingUpFrontMIPAmount);
            WriteAttribute(writer, "FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier", m_fhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier);
            WriteAttribute(writer, "HUDAdequateAvailableAssetsIndicator", m_hudAdequateAvailableAssetsIndicator);
            WriteAttribute(writer, "HUDAdequateEffectiveIncomeIndicator", m_hudAdequateEffectiveIncomeIndicator);
            WriteAttribute(writer, "HUDCreditCharacteristicsIndicator", m_hudCreditCharacteristicsIndicator);
            WriteAttribute(writer, "HUDStableEffectiveIncomeIndicator", m_hudStableEffectiveIncomeIndicator);
            writer.WriteEndElement(); // </FHA_LOAN>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_FhaAlimonyLiabilityTreatmentType 
    {
        Undefined = 0,
        AdditionToDebt,
        ReductionToIncome
    }
    public enum E_FhaLoanSectionOfActType 
    {
        Undefined = 0,
        _203B,
        _203B251,
        _203B2,
        _203K,
        _203K251,
        _221D2,
        _221D2251,
        _234C,
        _234C251
    }

}
