/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XELoanPurpose : Mismo.Common.AbstractXmlNode
    {
        public XELoanPurpose()
        {
        }
        #region Schema
        //  <xs:element name="LOAN_PURPOSE">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="CONSTRUCTION_REFINANCE_DATA" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="GSETitleMannerHeldDescription" type="xs:string"/>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="ConstructionOnly"/>
        //            <xs:enumeration value="ConstructionToPermanent"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Purchase"/>
        //            <xs:enumeration value="Refinance"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="OtherLoanPurposeDescription" type="xs:string"/>
        //      <xs:attribute name="PropertyLeaseholdExpirationDate" type="xs:string"/>
        //      <xs:attribute name="PropertyRightsType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FeeSimple"/>
        //            <xs:enumeration value="Leasehold"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PropertyUsageType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Investor"/>
        //            <xs:enumeration value="PrimaryResidence"/>
        //            <xs:enumeration value="SecondHome"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private XEConstructionRefinanceData m_constructionRefinanceData;
        private string m_gseTitleMannerHeldDescription;
        private E_LoanPurposeType m_type;
        private string m_otherLoanPurposeDescription;
        private string m_propertyLeaseholdExpirationDate;
        private E_LoanPurposePropertyRightsType m_propertyRightsType;
        private E_LoanPurposePropertyUsageType m_propertyUsageType;
        #endregion

        #region Public Properties
        public void SetConstructionRefinanceData(XEConstructionRefinanceData constructionRefinanceData) 
        {
            m_constructionRefinanceData = constructionRefinanceData;
        }
        public string GseTitleMannerHeldDescription 
        {
            get { return m_gseTitleMannerHeldDescription; }
            set { m_gseTitleMannerHeldDescription = value; }
        }
        public E_LoanPurposeType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string OtherLoanPurposeDescription 
        {
            get { return m_otherLoanPurposeDescription; }
            set { m_otherLoanPurposeDescription = value; }
        }
        public string PropertyLeaseholdExpirationDate 
        {
            get { return m_propertyLeaseholdExpirationDate; }
            set { m_propertyLeaseholdExpirationDate = value; }
        }
        public E_LoanPurposePropertyRightsType PropertyRightsType 
        {
            get { return m_propertyRightsType; }
            set { m_propertyRightsType = value; }
        }
        public E_LoanPurposePropertyUsageType PropertyUsageType 
        {
            get { return m_propertyUsageType; }
            set { m_propertyUsageType = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_PURPOSE");
            WriteAttribute(writer, "GSETitleMannerHeldDescription", m_gseTitleMannerHeldDescription);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "OtherLoanPurposeDescription", m_otherLoanPurposeDescription);
            WriteAttribute(writer, "PropertyLeaseholdExpirationDate", m_propertyLeaseholdExpirationDate);
            WriteAttribute(writer, "PropertyRightsType", m_propertyRightsType);
            WriteAttribute(writer, "PropertyUsageType", m_propertyUsageType);
            WriteElement(writer, m_constructionRefinanceData);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_LoanPurposeType 
    {
        Undefined = 0,
        ConstructionOnly,
        ConstructionToPermanent,
        Other,
        Purchase,
        Refinance
    }
    public enum E_LoanPurposePropertyRightsType 
    {
        Undefined = 0,
        FeeSimple,
        Leasehold
    }
    public enum E_LoanPurposePropertyUsageType 
    {
        Undefined = 0,
        Investor,
        PrimaryResidence,
        SecondHome
    }
}
