/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XECurrentIncome : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="CURRENT_INCOME">
        //     <xs:complexType>
        //       <xs:attribute name="IncomeType">
        //       <xs:attribute name="_MonthlyTotalAmount" type="xs:string"/>
        //     </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_XECurrentIncomeType m_incomeType;
        private string m_monthlyTotalAmount;
        #endregion

        #region Public Properties
        public E_XECurrentIncomeType IncomeType 
        {
            get { return m_incomeType; }
            set { m_incomeType = value; }
        }
        public string MonthlyTotalAmount 
        {
            get { return m_monthlyTotalAmount; }
            set { m_monthlyTotalAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("CURRENT_INCOME");
            WriteAttribute(writer, "IncomeType", m_incomeType);
            WriteAttribute(writer, "_MonthlyTotalAmount", m_monthlyTotalAmount);
            writer.WriteEndElement(); // </CURRENT_INCOME>
        }

        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}

    public enum E_XECurrentIncomeType 
    {
        Undefined = 0,
        AlimonyChildSupport,
        AutomobileExpenseAccount,
        Base,
        Bonus,
        Commissions,
        DividendsInterest,
        FosterCare,
        NetRentalIncome,
        NotesReceivableInstallment,
        OtherTypesOfIncome,
        Overtime,
        Pension,
        SocialSecurity,
        SubjectPropertyNetCashFlow,
        Trust,
        Unemployment,
        PublicAssistance,
        VABenefitsNonEducational,
        MortgageDifferential,
        MilitaryBasePay,
        MilitaryRationsAllowance,
        MilitaryFlightPay,
        MilitaryHazardPay,
        MilitaryClothesAllowance,
        MilitaryQuartersAllowance,
        MilitaryPropPay,
        MilitaryOverseasPay,
        MilitaryCombatPay,
        MilitaryVariableHousingAllowance
    }
}
