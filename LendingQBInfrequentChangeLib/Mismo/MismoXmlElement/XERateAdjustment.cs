/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XERateAdjustment : Mismo.Common.AbstractXmlNode
    {
        public XERateAdjustment()
        {
        }
        #region Schema
        //  <xs:element name="RATE_ADJUSTMENT">
        //    <xs:complexType>
        //      <xs:attribute name="FirstRateAdjustmentMonths" type="xs:string"/>
        //      <xs:attribute name="_CalculationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="AddPercentToCurrentRate"/>
        //            <xs:enumeration value="AddPercentToOriginalRate"/>
        //            <xs:enumeration value="IndexPlusMargin"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_DurationMonths" type="xs:string"/>
        //      <xs:attribute name="_Percent" type="xs:string"/>
        //      <xs:attribute name="_PeriodNumber" type="xs:string"/>
        //      <xs:attribute name="_SubsequentCapPercent" type="xs:string"/>
        //      <xs:attribute name="SubsequentRateAdjustmentMonths" type="xs:string"/>
        //      <xs:attribute name="_InitialCapPercent" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_firstRateAdjustmentMonths;
        private E_RateAdjustmentCalculationType m_calculationType;
        private string m_durationMonths;
        private string m_percent;
        private string m_periodNumber;
        private string m_subsequentCapPercent;
        private string m_subsequentRateAdjustmentMonths;
        private string m_initialCapPercent;
        #endregion

        #region Public Properties
        public string FirstRateAdjustmentMonths 
        {
            get { return m_firstRateAdjustmentMonths; }
            set { m_firstRateAdjustmentMonths = value; }
        }
        public E_RateAdjustmentCalculationType CalculationType 
        {
            get { return m_calculationType; }
            set { m_calculationType = value; }
        }
        public string DurationMonths 
        {
            get { return m_durationMonths; }
            set { m_durationMonths = value; }
        }
        public string Percent 
        {
            get { return m_percent; }
            set { m_percent = value; }
        }
        public string PeriodNumber 
        {
            get { return m_periodNumber; }
            set { m_periodNumber = value; }
        }
        public string SubsequentCapPercent 
        {
            get { return m_subsequentCapPercent; }
            set { m_subsequentCapPercent = value; }
        }
        public string SubsequentRateAdjustmentMonths 
        {
            get { return m_subsequentRateAdjustmentMonths; }
            set { m_subsequentRateAdjustmentMonths = value; }
        }
        public string InitialCapPercent 
        {
            get { return m_initialCapPercent; }
            set { m_initialCapPercent = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RATE_ADJUSTMENT");
            WriteAttribute(writer, "FirstRateAdjustmentMonths", m_firstRateAdjustmentMonths);
            WriteAttribute(writer, "_CalculationType", m_calculationType);
            WriteAttribute(writer, "_DurationMonths", m_durationMonths);
            WriteAttribute(writer, "_Percent", m_percent);
            WriteAttribute(writer, "_PeriodNumber", m_periodNumber);
            WriteAttribute(writer, "_SubsequentCapPercent", m_subsequentCapPercent);
            WriteAttribute(writer, "SubsequentRateAdjustmentMonths", m_subsequentRateAdjustmentMonths);
            WriteAttribute(writer, "_InitialCapPercent", m_initialCapPercent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_RateAdjustmentCalculationType 
    {
        Undefined = 0,
        AddPercentToCurrentRate,
        AddPercentToOriginalRate,
        IndexPlusMargin

    }
}
