using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{

	public class XESubmittingParty : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="SUBMITTING_PARTY">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="CONTACT_DETAIL" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="PREFERRED_RESPONSE" minOccurs="0" maxOccurs="unbounded"/> // TODO
        //          </xs:sequence>
        //          <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //          <xs:attribute name="_Name" type="xs:string"/>
        //          <xs:attribute name="_StreetAddress" type="xs:string"/>
        //          <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //          <xs:attribute name="_City" type="xs:string"/>
        //          <xs:attribute name="_State" type="xs:string"/>
        //          <xs:attribute name="_PostalCode" type="xs:string"/>
        //          <xs:attribute name="LoginAccountIdentifier" type="xs:string"/>
        //          <xs:attribute name="LoginAccountPassword" type="xs:string"/>
        //          <xs:attribute name="_Identifier" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_contactDetailList = new ArrayList();
        private string m_name;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_identifier;
        private string m_sequenceIdentifier;
        private string m_loginAccountIdentifier;
        private string m_loginAccountPassword;
        #endregion

        #region Public Properties
        public void AddContactDetail(XEContactDetail o) 
        {
            m_contactDetailList.Add(o);
        }
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public string Name 
        { 
            get { return m_name; } 
            set { m_name = value; } 
        } 
        public string StreetAddress 
        { 
            get { return m_streetAddress; } 
            set { m_streetAddress = value; } 
        } 
        public string StreetAddress2 
        { 
            get { return m_streetAddress2; } 
            set { m_streetAddress2 = value; } 
        } 
        public string City 
        { 
            get { return m_city; } 
            set { m_city = value; } 
        } 
        public string State 
        { 
            get { return m_state; } 
            set { m_state = value; } 
        } 
        public string PostalCode 
        { 
            get { return m_postalCode; } 
            set { m_postalCode = value; } 
        } 
        public string Identifier 
        { 
            get { return m_identifier; } 
            set { m_identifier = value; } 
        } 
        public string LoginAccountIdentifier 
        {
            get { return m_loginAccountIdentifier; }
            set { m_loginAccountIdentifier = value; }
        }
        public string LoginAccountPassword 
        {
            get { return m_loginAccountPassword; }
            set { m_loginAccountPassword = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("SUBMITTING_PARTY");
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "LoginAccountIdentifier", m_loginAccountIdentifier);
            WriteAttribute(writer, "LoginAccountPassword", m_loginAccountPassword);
            WriteAttribute(writer, "_Identifier", m_identifier);

            foreach (XEContactDetail o in m_contactDetailList)
                o.GenerateXml(writer);

            writer.WriteEndElement(); // </SUBMITTING_PARTY>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_contactDetailList = new ArrayList();
            XmlNodeList nodeList = el.SelectNodes("CONTACT_DETAIL");
            foreach (XmlElement o in nodeList) 
            {
                XEContactDetail xe = new XEContactDetail();
                xe.Parse(o);
                m_contactDetailList.Add(xe);
            }

            m_sequenceIdentifier = el.GetAttribute("_SequenceIdentifier");
            m_name = el.GetAttribute("_Name");
            m_streetAddress = el.GetAttribute("_StreetAddress");
            m_streetAddress2 = el.GetAttribute("_StreetAddress2");
            m_city = el.GetAttribute("_City");
            m_state = el.GetAttribute("_State");
            m_postalCode = el.GetAttribute("_PostalCode");
            m_loginAccountIdentifier = el.GetAttribute("LoginAccountIdentifier");
            m_loginAccountPassword = el.GetAttribute("LoginAccountPassword");
            m_identifier = el.GetAttribute("_Identifier");

        }
        #endregion
	}
}
