/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
namespace Mismo.MismoXmlElement
{
    public class XEBorrower : Mismo.Common.AbstractXmlNode
    {
        public XEBorrower()
        {
        }
        #region Schema
        //  <xs:element name="BORROWER">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_ALIAS" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="_MAIL_TO" minOccurs="0"/>
        //        <xs:element ref="_RESIDENCE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="CURRENT_INCOME" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="DECLARATION" minOccurs="0"/>
        //        <xs:element ref="DEPENDENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="EMPLOYER" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="FHA_VA_BORROWER" minOccurs="0"/>
        //        <xs:element ref="GOVERNMENT_MONITORING" minOccurs="0"/>
        //        <xs:element ref="PRESENT_HOUSING_EXPENSE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="SUMMARY" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="VA_BORROWER" minOccurs="0"/>
        //        <xs:element ref="FHA_BORROWER" minOccurs="0"/>
        //        <xs:element ref="_NEAREST_LIVING_RELATIVE" minOccurs="0"/>
        //        <xs:element ref="CONTACT_POINT" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="BorrowerID" type="xs:ID"/>
        //      <xs:attribute name="JointAssetBorrowerID" type="xs:IDREF"/>
        //      <xs:attribute name="_FirstName" type="xs:string" use="required"/>
        //      <xs:attribute name="_MiddleName" type="xs:string"/>
        //      <xs:attribute name="_LastName" type="xs:string" use="required"/>
        //      <xs:attribute name="_NameSuffix" type="xs:string"/>
        //      <xs:attribute name="_AgeAtApplicationYears" type="xs:string"/>
        //      <xs:attribute name="_ApplicationSignedDate" type="xs:string"/>
        //      <xs:attribute name="_HomeTelephoneNumber" type="xs:string"/>
        //      <xs:attribute name="_PrintPositionType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Borrower"/>
        //            <xs:enumeration value="CoBorrower"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_SSN" type="xs:string" use="required"/>
        //      <xs:attribute name="_BirthDate" type="xs:string"/>
        //      <xs:attribute name="_UnparsedName" type="xs:string"/>
        //      <xs:attribute name="DependentCount" type="xs:string"/>
        //      <xs:attribute name="JointAssetLiabilityReportingType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Jointly"/>
        //            <xs:enumeration value="NotJointly"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MaritalStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Married"/>
        //            <xs:enumeration value="NotProvided"/>
        //            <xs:enumeration value="Separated"/>
        //            <xs:enumeration value="Unknown"/>
        //            <xs:enumeration value="Unmarried"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="SchoolingYears" type="xs:string"/>
        //      <xs:attribute name="CreditReportIdentifier" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private ArrayList m_aliasList = new ArrayList();
        private XEMailTo m_mailTo;
        private ArrayList m_residenceList = new ArrayList();
        private ArrayList m_currentIncomeList = new ArrayList();
        private XEDeclaration m_declaration;
        private ArrayList m_dependentList = new ArrayList();
        private ArrayList m_employerList = new ArrayList();
        private XEFhaVaBorrower m_fhaVaBorrower;
        private XEGovernmentMonitoring m_governmentMonitoring;
        private ArrayList m_presentHousingExpenseList = new ArrayList();
        private ArrayList m_summaryList = new ArrayList();
        private XEVaBorrower m_vaBorrower;
        private XEFhaBorrower m_fhaBorrower;
        private XENearestLivingRelative m_nearestLivingRelative;
        private ArrayList m_contactPointList = new ArrayList();
        private string m_borrowerId;
        private string m_jointAssetBorrowerId;
        private string m_firstName;
        private string m_middleName;
        private string m_lastName;
        private string m_nameSuffix;
        private string m_ageAtApplicationYears;
        private string m_applicationSignedDate;
        private string m_homeTelephoneNumber;
        private E_BorrowerPrintPositionType m_printPositionType;
        private string m_ssn;
        private string m_birthDate;
        private string m_unparsedName;
        private string m_dependentCount;
        private E_BorrowerJointAssetLiabilityReportingType m_jointAssetLiabilityReportType;
        private E_BorrowerMaritalStatusType m_maritalStatusType;
        private string m_schoolingYears;
        private string m_creditReportIdentifier;

        #endregion

        #region Public Properties
        public void AddAlias(XEAlias alias) 
        {
            if (null == alias)
                return;

            m_aliasList.Add(alias);
        }
        public void SetMailTo(XEMailTo mailTo) 
        {
            m_mailTo = mailTo;
        }
        public void AddResidence(XEResidence residence) 
        {
            if (null == residence)
                return;
            m_residenceList.Add(residence);
        }
        public void AddCurrentIncome(XECurrentIncome currentIncome) 
        {
            if (null == currentIncome)
                return;
            m_currentIncomeList.Add(currentIncome);
        }
        public void SetDeclaration(XEDeclaration declaration) 
        {
            m_declaration = declaration;
        }
        public void AddDependent(XEDependent dependent) 
        {
            if (null == dependent)
                return;
            m_dependentList.Add(dependent);
        }
        public void AddEmployer(XEEmployer employer) 
        {
            if (null == employer)
                return;
            m_employerList.Add(employer);
        }
        public void SetFhaVaBorrower(XEFhaVaBorrower fhaVaBorrower) 
        {
            m_fhaVaBorrower = fhaVaBorrower;
        }
        public void SetGovernmentMonitoring(XEGovernmentMonitoring governmentMonitoring) 
        {
            m_governmentMonitoring = governmentMonitoring;
        }
        public void AddPresentHousingExpense(XEPresentHousingExpense presentHousingExpense) 
        {
            if (null == presentHousingExpense)
                return;
            m_presentHousingExpenseList.Add(presentHousingExpense);
        }
        public void AddSummary(XESummary summary) 
        {
            if (null == summary)
                return;
            m_summaryList.Add(summary);
        }
        public void SetVaBorrower(XEVaBorrower vaBorrower) 
        {
            m_vaBorrower = vaBorrower;
        }
        public void SetFhaBorrower(XEFhaBorrower fhaBorrower) 
        {
            m_fhaBorrower = fhaBorrower;
        }
        public void SetNearestLivingRelative(XENearestLivingRelative nearestLivingRelative) 
        {
            m_nearestLivingRelative = nearestLivingRelative;
        }
        public void AddContactPoint(XEContactPoint contactPoint) 
        {
            if (null == contactPoint)
                return;
            m_contactPointList.Add(contactPoint);
        }
        public string BorrowerId 
        {
            get { return m_borrowerId; }
            set { m_borrowerId = value; }
        }
        public string JointAssetBorrowerId 
        {
            get { return m_jointAssetBorrowerId; }
            set { m_jointAssetBorrowerId = value; }
        }
        public string FirstName 
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }
        public string MiddleName 
        {
            get { return m_middleName; }
            set { m_middleName = value; }
        }
        public string LastName 
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }
        public string NameSuffix 
        {
            get { return m_nameSuffix; }
            set { m_nameSuffix = value; }
        }
        public string AgeAtApplicationYears 
        {
            get { return m_ageAtApplicationYears; }
            set { m_ageAtApplicationYears = value; }
        }
        public string ApplicationSignedDate 
        {
            get { return m_applicationSignedDate; }
            set { m_applicationSignedDate = value; }
        }
        public string HomeTelephoneNumber 
        {
            get { return m_homeTelephoneNumber; }
            set { m_homeTelephoneNumber = value; }
        }
        public E_BorrowerPrintPositionType PrintPositionType 
        {
            get { return m_printPositionType; }
            set { m_printPositionType = value; }
        }
        public string Ssn 
        {
            get { return m_ssn; }
            set { m_ssn = value; }
        }
        public string BirthDate 
        {
            get { return m_birthDate; }
            set { m_birthDate = value; }
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }
        public string DependentCount 
        {
            get { return m_dependentCount; }
            set { m_dependentCount = value; }
        }
        public E_BorrowerJointAssetLiabilityReportingType JointAssetLiabilityReportType 
        {
            get { return m_jointAssetLiabilityReportType; }
            set { m_jointAssetLiabilityReportType = value; }
        }
        public E_BorrowerMaritalStatusType MaritalStatusType 
        {
            get { return m_maritalStatusType; }
            set { m_maritalStatusType = value; }
        }
        public string SchoolingYears 
        {
            get { return m_schoolingYears; }
            set { m_schoolingYears = value; }
        }
        public string CreditReportIdentifier 
        {
            get { return m_creditReportIdentifier; }
            set { m_creditReportIdentifier = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("BORROWER");
            WriteAttribute(writer, "BorrowerID", m_borrowerId);
            WriteAttribute(writer, "JointAssetBorrowerID", m_jointAssetBorrowerId);
            WriteRequiredAttribute(writer, "_FirstName", m_firstName);
            WriteAttribute(writer, "_MiddleName", m_middleName);
            WriteRequiredAttribute(writer, "_LastName", m_lastName);
            WriteAttribute(writer, "_NameSuffix", m_nameSuffix);
            WriteAttribute(writer, "_AgeAtApplicationYears", m_ageAtApplicationYears);
            WriteAttribute(writer, "_ApplicationSignedDate", m_applicationSignedDate);
            WriteAttribute(writer, "_HomeTelephoneNumber", m_homeTelephoneNumber);
            WriteAttribute(writer, "_PrintPositionType", m_printPositionType);
            WriteRequiredAttribute(writer, "_SSN", m_ssn);
            WriteAttribute(writer, "_BirthDate", m_birthDate);
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);
            WriteAttribute(writer, "DependentCount", m_dependentCount);
            WriteAttribute(writer, "JointAssetLiabilityReportingType", m_jointAssetLiabilityReportType);
            WriteAttribute(writer, "MaritalStatusType", m_maritalStatusType);
            WriteAttribute(writer, "SchoolingYears", m_schoolingYears);
            WriteAttribute(writer, "CreditReportIdentifier", m_creditReportIdentifier);
            WriteElement(writer, m_aliasList);
            WriteElement(writer, m_mailTo);
            WriteElement(writer, m_residenceList);
            WriteElement(writer, m_currentIncomeList);
            WriteElement(writer, m_declaration);
            WriteElement(writer, m_dependentList);
            WriteElement(writer, m_employerList);
            WriteElement(writer, m_fhaVaBorrower);
            WriteElement(writer, m_governmentMonitoring);
            WriteElement(writer, m_presentHousingExpenseList);
            WriteElement(writer, m_summaryList);
            WriteElement(writer, m_vaBorrower);
            WriteElement(writer, m_fhaBorrower);
            WriteElement(writer, m_nearestLivingRelative);
            WriteElement(writer, m_contactPointList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_BorrowerPrintPositionType 
    {
        Undefined = 0,
        Borrower,
        CoBorrower
    }
    public enum E_BorrowerJointAssetLiabilityReportingType 
    {
        Undefined = 0,
        Jointly,
        NotJointly
    }
    public enum E_BorrowerMaritalStatusType 
    {
        Undefined = 0,
        Married,
        NotProvided,
        Separated,
        Unknown,
        Unmarried
    }
}
