using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{

	public class XERequest : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="REQUEST">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="KEY" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="REQUEST_DATA" minOccurs="0" maxOccurs="unbounded"/>
        //          </xs:sequence>
        //          <xs:attribute name="RequestDatetime" type="xs:string"/>
        //          <xs:attribute name="InternalAccountIdentifier" type="xs:string"/>
        //          <xs:attribute name="LoginAccountIdentifier" type="xs:string"/>
        //          <xs:attribute name="LoginAccountPassword" type="xs:string"/>
        //          <xs:attribute name="RequestingPartyBranchIdentifier" type="xs:string"/>
        //          <xs:attribute name="_ID" type="xs:ID"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_keyList = new ArrayList();
        private ArrayList m_requestDataList = new ArrayList();
        private DateTime m_requestDateTime;
        private string m_internalAccountIdentifier;
        private string m_loginAccountIdentifier;
        private string m_loginAccountPassword;
        private string m_requestingPartyBranchIdentifier;
        private string m_id;
        #endregion

        #region Public Properties
        public void AddKey(XEKey o) 
        {
            m_keyList.Add(o);
        }
        public void AddRequestData(XERequestData o) 
        {
            m_requestDataList.Add(o);
        }
        public XERequestData GetRequestData(int index) 
        {
            if (m_requestDataList == null || index < 0 || index >= m_requestDataList.Count)
                return null;
            return (XERequestData) m_requestDataList[index];
        }
        public DateTime RequestDateTime 
        { 
            get { return m_requestDateTime; } 
            set { m_requestDateTime = value; } 
        } 
        public string InternalAccountIdentifier 
        { 
            get { return m_internalAccountIdentifier; } 
            set { m_internalAccountIdentifier = value; } 
        } 
        public string LoginAccountIdentifier 
        { 
            get { return m_loginAccountIdentifier; } 
            set { m_loginAccountIdentifier = value; } 
        } 
        public string LoginAccountPassword 
        { 
            get { return m_loginAccountPassword; } 
            set { m_loginAccountPassword = value; } 
        } 
        public string RequestingPartyBranchIdentifier 
        { 
            get { return m_requestingPartyBranchIdentifier; } 
            set { m_requestingPartyBranchIdentifier = value; } 
        } 
        public string ID 
        { 
            get { return m_id; } 
            set { m_id = value; } 
        } 
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("REQUEST");
            WriteAttribute(writer, "RequestDateTime", Mismo.Common.Utilities.ToDateTimeString(m_requestDateTime));
            WriteAttribute(writer, "InternalAccountIdentifier", m_internalAccountIdentifier);
            WriteAttribute(writer, "LoginAccountIdentifier", m_loginAccountIdentifier);
            WriteAttribute(writer, "LoginAccountPassword", m_loginAccountPassword);
            WriteAttribute(writer, "RequestingPartyBranchIdentifier", m_requestingPartyBranchIdentifier);
            WriteAttribute(writer, "_ID", m_id);

            foreach (XEKey o in m_keyList)
                o.GenerateXml(writer);

            foreach (XERequestData o in m_requestDataList)
                o.GenerateXml(writer);


            writer.WriteEndElement(); // </REQUEST>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_keyList = new ArrayList();
            XmlNodeList nodeList = el.SelectNodes("KEY");
            foreach (XmlElement o in nodeList) 
            {
                XEKey xe = new XEKey();
                xe.Parse(o);
                m_keyList.Add(xe);
            }

            m_requestDataList = new ArrayList();
            nodeList = el.SelectNodes("REQUEST_DATA");
            foreach (XmlElement o in nodeList) 
            {
                XERequestData xe = new XERequestData();
                xe.Parse(o);
                m_requestDataList.Add(xe);
            }

            try 
            {
                m_requestDateTime = Mismo.Common.Utilities.ParseDateTime(el.GetAttribute("RequestDateTime"));
            } 
            catch 
            {
                m_requestDateTime = DateTime.MinValue;
            }
            m_internalAccountIdentifier = el.GetAttribute("InternalAccountIdentifier");
            m_loginAccountIdentifier = el.GetAttribute("LoginAccountIdentifier");
            m_loginAccountPassword = el.GetAttribute("LoginAccountPassword");
            m_requestingPartyBranchIdentifier = el.GetAttribute("RequestingPartyBranchIdentifier");
            m_id = el.GetAttribute("_ID");
        }
        #endregion
	}
}
