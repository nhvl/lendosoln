/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEPayments : Mismo.Common.AbstractXmlNode
	{
		public XEPayments()
		{
		}
        #region Schema
        //  <xs:element name="_PAYMENTS">
        //    <xs:complexType>
        //      <xs:attribute name="_DueDate" type="xs:string"/>
        //      <xs:attribute name="_PaymentAmount" type="xs:string"/>
        //      <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_dueDate;
        private string m_paymentAmount;
        private string m_sequenceIdentifier;
        #endregion

        #region Public Properties
        public string DueDate 
        {
            get { return m_dueDate; }
            set { m_dueDate = value; }
        }
        public string PaymentAmount 
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_PAYMENTS");
            WriteAttribute(writer, "_DueDate", m_dueDate);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
