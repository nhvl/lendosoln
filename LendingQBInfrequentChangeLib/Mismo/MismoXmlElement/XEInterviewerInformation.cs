/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEInterviewerInformation : Mismo.Common.AbstractXmlNode
	{
		public XEInterviewerInformation()
		{
		}
        #region Schema
        // <xs:element name="INTERVIEWER_INFORMATION">
        //   <xs:complexType>
        //     <xs:attribute name="InterviewersEmployerStreetAddress" type="xs:string"/>
        //     <xs:attribute name="InterviewersEmployerCity" type="xs:string"/>
        //     <xs:attribute name="InterviewersEmployerState" type="xs:string"/>
        //     <xs:attribute name="InterviewersEmployerPostalCode" type="xs:string"/>
        //     <xs:attribute name="InterviewersTelephoneNumber" type="xs:string"/>
        //     <xs:attribute name="ApplicationTakenMethodType">
        //     <xs:attribute name="InterviewerApplicationSignedDate" type="xs:string"/>
        //     <xs:attribute name="InterviewersEmployerName" type="xs:string"/>
        //     <xs:attribute name="InterviewersName" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_interviewersEmployerStreetAddress;
        private string m_interviewersEmployerCity;
        private string m_interviewersEmployerState;
        private string m_interviewersEmployerPostalCode;
        private string m_interviewersTelephoneNumber;
        private E_ApplicationTakenMethodType m_applicationTakenMethodType;
        private string m_interviewersApplicationSignedDate;
        private string m_interviewersEmployerName;
        private string m_interviewersName;
        #endregion

        #region Public Properties
        public string InterviewersEmployerStreetAddress 
        {
            get { return m_interviewersEmployerStreetAddress; }
            set { m_interviewersEmployerStreetAddress = value; }
        }
        public string InterviewersEmployerCity 
        {
            get { return m_interviewersEmployerCity; }
            set { m_interviewersEmployerCity = value; }
        }
        public string InterviewersEmployerState 
        {
            get { return m_interviewersEmployerState; }
            set { m_interviewersEmployerState = value; }
        }
        public string InterviewersEmployerPostalCode 
        {
            get { return m_interviewersEmployerPostalCode; }
            set { m_interviewersEmployerPostalCode = value; }
        }
        public string InterviewersTelephoneNumber 
        {
            get { return m_interviewersTelephoneNumber; }
            set { m_interviewersTelephoneNumber = value; }
        }
        public E_ApplicationTakenMethodType ApplicationTakenMethodType 
        {
            get { return m_applicationTakenMethodType; }
            set { m_applicationTakenMethodType = value; }
        }
        public string InterviewersApplicationSignedDate 
        {
            get { return m_interviewersApplicationSignedDate; }
            set { m_interviewersApplicationSignedDate = value; }
        }
        public string InterviewersEmployerName 
        {
            get { return m_interviewersEmployerName; }
            set { m_interviewersEmployerName = value; }
        }
        public string InterviewersName 
        {
            get { return m_interviewersName; }
            set { m_interviewersName = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("INTERVIEWER_INFORMATION");
            WriteAttribute(writer, "InterviewersEmployerStreetAddress", m_interviewersEmployerStreetAddress);
            WriteAttribute(writer, "InterviewersEmployerCity", m_interviewersEmployerCity);
            WriteAttribute(writer, "InterviewersEmployerState", m_interviewersEmployerState);
            WriteAttribute(writer, "InterviewersEmployerPostalCode", m_interviewersEmployerPostalCode);
            WriteAttribute(writer, "ApplicationTakenMethodType", m_applicationTakenMethodType);
            WriteAttribute(writer, "InterviewersEmployerName", m_interviewersEmployerName);
            WriteAttribute(writer, "InterviewersName", m_interviewersName);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_ApplicationTakenMethodType 
    {
        Undefined = 0,
        FaceToFace,
        Mail,
        Telephone,
        Internet
    }
}
