/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEPurchaseCredit : Mismo.Common.AbstractXmlNode
    {
        public XEPurchaseCredit()
        {
        }
        #region Schema
        //  <xs:element name="PURCHASE_CREDIT">
        //    <xs:complexType>
        //      <xs:attribute name="_Amount" type="xs:string"/>
        //      <xs:attribute name="_SourceType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="BorrowerPaidOutsideClosing"/>
        //            <xs:enumeration value="PropertySeller"/>
        //            <xs:enumeration value="Lender"/>
        //            <xs:enumeration value="NonParentRelative"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="EarnestMoney"/>
        //            <xs:enumeration value="RelocationFunds"/>
        //            <xs:enumeration value="EmployerAssistedHousing"/>
        //            <xs:enumeration value="LeasePurchaseFund"/>
        //            <xs:enumeration value="Other"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_amount;
        private E_PurchaseCreditSourceType m_sourceType;
        private E_PurchaseCreditType m_type;
        #endregion

        #region Public Properties
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public E_PurchaseCreditSourceType SourceType 
        {
            get { return m_sourceType; }
            set { m_sourceType = value; }
        }
        public E_PurchaseCreditType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PURCHASE_CREDIT");
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_SourceType", m_sourceType);
            WriteAttribute(writer, "_Type", m_type);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_PurchaseCreditSourceType 
    {
        Undefined = 0,
        BorrowerPaidOutsideClosing,
        PropertySeller,
        Lender,
        NonParentRelative,

    }
    public enum E_PurchaseCreditType 
    {
        Undefined = 0,
        EarnestMoney,
        RelocationFunds,
        EmployerAssistedHousing,
        LeasePurchaseFund,
        Other,
    }
}
