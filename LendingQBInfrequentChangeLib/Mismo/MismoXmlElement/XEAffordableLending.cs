/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.MismoXmlElement
{
	public class XEAffordableLending : Mismo.Common.AbstractXmlNode
	{
		public XEAffordableLending()
		{
		}
        #region Schema
        // <xs:element name="AFFORDABLE_LENDING">
        //   <xs:complexType>
        //     <xs:attribute name="FNMCommunityLendingProductName" type="xs:string"/>
        //     <xs:attribute name="FNMCommunityLendingProductType">
        //     <xs:attribute name="FNMCommunityLendingProductTypeOtherDescription" type="xs:string"/>
        //     <xs:attribute name="FNMCommunitySecondsIndicator">
        //     <xs:attribute name="FNMNeighborsMortgageEligibilityIndicator">
        //     <xs:attribute name="FREAffordableProgramIdentifier" type="xs:string"/>
        //     <xs:attribute name="HUDIncomeLimitAdjustmentFactor" type="xs:string"/>
        //     <xs:attribute name="HUDLendingIncomeLimitAmount" type="xs:string"/>
        //     <xs:attribute name="HUDMedianIncomeAmount" type="xs:string"/>
        //     <xs:attribute name="MSAIdentifier" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_FNMCommunityLendingProductName;
        private E_FNMCommunityLendingProductType m_FNMCommunityLendingProductType;
        private string m_FNMCommunityLendingProductTypeOtherDescription;
        private string m_FNMCommunitySecondsIndicator;
        private string m_FNMNeighborsMortgageEligibilityIndicator;
        private string m_FREAffordableProgramIdentifier;
        private string m_HUDIncomeLimitAdjustmentFactor;
        private string m_HUDLendingIncomeLimitAmount;
        private string m_HUDMedianIncomeAmount;
        private string m_MSAIdentifier;
        #endregion

        #region Public Properties
        public string FNMCommunityLendingProductName 
        {
            get { return m_FNMCommunityLendingProductName; }
            set { m_FNMCommunityLendingProductName = value; }
        }
        public E_FNMCommunityLendingProductType FNMCommunityLendingProductType 
        {
            get { return m_FNMCommunityLendingProductType; }
            set { m_FNMCommunityLendingProductType = value; }
        }
        public string FNMCommunityLendingProductTypeOtherDescription 
        {
            get { return m_FNMCommunityLendingProductTypeOtherDescription; }
            set { m_FNMCommunityLendingProductTypeOtherDescription = value; }
        }
        public string FNMCommunitySecondsIndicator 
        {
            get { return m_FNMCommunitySecondsIndicator; }
            set { m_FNMCommunitySecondsIndicator = value; }
        }
        public string FNMNeighborsMortgageEligibilityIndicator 
        {
            get { return m_FNMNeighborsMortgageEligibilityIndicator; }
            set { m_FNMNeighborsMortgageEligibilityIndicator = value; }
        }
        public string FREAffordableProgramIdentifier 
        {
            get { return m_FREAffordableProgramIdentifier; }
            set { m_FREAffordableProgramIdentifier = value; }
        }
        public string HUDIncomeLimitAdjustmentFactor 
        {
            get { return m_HUDIncomeLimitAdjustmentFactor; }
            set { m_HUDIncomeLimitAdjustmentFactor = value; }
        }
        public string HUDLendingIncomeLimitAmount 
        {
            get { return m_HUDLendingIncomeLimitAmount; }
            set { m_HUDLendingIncomeLimitAmount = value; }
        }
        public string HUDMedianIncomeAmount 
        {
            get { return m_HUDMedianIncomeAmount; }
            set { m_HUDMedianIncomeAmount = value; }
        }
        public string MSAIdentifier 
        {
            get { return m_MSAIdentifier; }
            set { m_MSAIdentifier = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("AFFORDABLE_LENDING");
            WriteAttribute(writer, "FNMCommunityLendingProductName", m_FNMCommunityLendingProductName);
            WriteAttribute(writer, "FNMCommunityLendingProductType", m_FNMCommunityLendingProductType);
            WriteAttribute(writer, "FNMCommunityLendingProductTypeOtherDescription", m_FNMCommunityLendingProductTypeOtherDescription);
            WriteAttribute(writer, "FNMCommunitySecondsIndicator", m_FNMCommunitySecondsIndicator);
            WriteAttribute(writer, "FNMNeighborsMortgageEligibilityIndicator", m_FNMNeighborsMortgageEligibilityIndicator);
            WriteAttribute(writer, "FREAffordableProgramIdentifier", m_FREAffordableProgramIdentifier);
            WriteAttribute(writer, "HUDIncomeLimitAdjustmentFactor", m_HUDIncomeLimitAdjustmentFactor);
            WriteAttribute(writer, "HUDLendingIncomeLimitAmount", m_HUDLendingIncomeLimitAmount);
            WriteAttribute(writer, "HUDMedianIncomeAmount", m_HUDMedianIncomeAmount);
            WriteAttribute(writer, "MSAIdentifier", m_MSAIdentifier);
            writer.WriteEndElement(); // </AFFORDABLE_LENDING>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_FNMCommunityLendingProductType 
    {
        Undefined = 0,
        CommunityHomeBuyerProgram,
        Fannie97,
        Fannie32,
        MyCommunityMortgage,
        Other
    }
}
