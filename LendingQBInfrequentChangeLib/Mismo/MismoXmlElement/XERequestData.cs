using System;
using System.Xml;

using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	/// <summary>
	/// Summary description for XERequestData.
	/// </summary>
	public class XERequestData : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="REQUEST_DATA">
        //      <xs:complexType mixed="true"/>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_contentInnerXml;
        private IXmlNode m_content;
        #endregion

        #region Public Properties
        public IXmlNode Content 
        {
            set { m_content = value; }
        }
        public string ContentInnerXml 
        {
            get { return m_contentInnerXml; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("REQUEST_DATA");
            
            if (null != m_content)
                m_content.GenerateXml(writer);

            writer.WriteEndElement(); // </REQUEST_DATA>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_contentInnerXml = el.InnerXml;
        }
        #endregion
	}
}
