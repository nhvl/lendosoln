/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEVaBorrower : Mismo.Common.AbstractXmlNode
    {
        public XEVaBorrower()
        {
        }
        #region Schema
        //  <xs:element name="VA_BORROWER">
        //    <xs:complexType>
        //      <xs:attribute name="VACoBorrowerNonTaxableIncomeAmount" type="xs:string"/>
        //      <xs:attribute name="VACoBorrowerTaxableIncomeAmount" type="xs:string"/>
        //      <xs:attribute name="VAFederalTaxAmount" type="xs:string"/>
        //      <xs:attribute name="VALocalTaxAmount" type="xs:string"/>
        //      <xs:attribute name="VAPrimaryBorrowerNonTaxableIncomeAmount" type="xs:string"/>
        //      <xs:attribute name="VAPrimaryBorrowerTaxableIncomeAmount" type="xs:string"/>
        //      <xs:attribute name="VASocialSecurityTaxAmount" type="xs:string"/>
        //      <xs:attribute name="VAStateTaxAmount" type="xs:string"/>
        //      <xs:attribute name="_CertificationOccupancyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="A"/>
        //            <xs:enumeration value="B"/>
        //            <xs:enumeration value="C"/>
        //            <xs:enumeration value="D"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private string m_vaCoBorrowerNonTaxableIncomeAmount;
        private string m_vaCoBorrowerTaxableIncomeAmount;
        private string m_vaFederalTaxAmount;
        private string m_vaLocalTaxAmount;
        private string m_vaPrimaryBorrowerNonTaxableIncomeAmount;
        private string m_vaPrimaryBorrowerTaxableIncomeAmount;
        private string m_vaSocialSecurityTaxAmount;
        private string m_vaStateTaxAmount;
        private E_VaBorrowerCertificationOccupancyType m_certificationOccupancyType;
        #endregion

        #region Public Properties
        public string VaCoBorrowerNonTaxableIncomeAmount 
        {
            get { return m_vaCoBorrowerNonTaxableIncomeAmount; }
            set { m_vaCoBorrowerNonTaxableIncomeAmount = value; }
        }
        public string VaCoBorrowerTaxableIncomeAmount 
        {
            get { return m_vaCoBorrowerTaxableIncomeAmount; }
            set { m_vaCoBorrowerTaxableIncomeAmount = value; }
        }
        public string VaFederalTaxAmount 
        {
            get { return m_vaFederalTaxAmount; }
            set { m_vaFederalTaxAmount = value; }
        }
        public string VaLocalTaxAmount 
        {
            get { return m_vaLocalTaxAmount; }
            set { m_vaLocalTaxAmount = value; }
        }
        public string VaPrimaryBorrowerNonTaxableIncomeAmount 
        {
            get { return m_vaPrimaryBorrowerNonTaxableIncomeAmount; }
            set { m_vaPrimaryBorrowerNonTaxableIncomeAmount = value; }
        }
        public string VaPrimaryBorrowerTaxableIncomeAmount 
        {
            get { return m_vaPrimaryBorrowerTaxableIncomeAmount; }
            set { m_vaPrimaryBorrowerTaxableIncomeAmount = value; }
        }
        public string VaSocialSecurityTaxAmount 
        {
            get { return m_vaSocialSecurityTaxAmount; }
            set { m_vaSocialSecurityTaxAmount = value; }
        }
        public string VaStateTaxAmount 
        {
            get { return m_vaStateTaxAmount; }
            set { m_vaStateTaxAmount = value; }
        }
        public E_VaBorrowerCertificationOccupancyType CertificationOccupancyType 
        {
            get { return m_certificationOccupancyType; }
            set { m_certificationOccupancyType = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("VA_BORROWER");
            WriteAttribute(writer, "VACoBorrowerNonTaxableIncomeAmount", m_vaCoBorrowerNonTaxableIncomeAmount);
            WriteAttribute(writer, "VACoBorrowerTaxableIncomeAmount", m_vaCoBorrowerTaxableIncomeAmount);
            WriteAttribute(writer, "VAFederalTaxAmount", m_vaFederalTaxAmount);
            WriteAttribute(writer, "VALocalTaxAmount", m_vaLocalTaxAmount);
            WriteAttribute(writer, "VAPrimaryBorrowerNonTaxableIncomeAmount", m_vaPrimaryBorrowerNonTaxableIncomeAmount);
            WriteAttribute(writer, "VAPrimaryBorrowerTaxableIncomeAmount", m_vaPrimaryBorrowerTaxableIncomeAmount);
            WriteAttribute(writer, "VASocialSecurityTaxAmount", m_vaSocialSecurityTaxAmount);
            WriteAttribute(writer, "VAStateTaxAmount", m_vaStateTaxAmount);
            WriteAttribute(writer, "_CertificationOccupancyType", m_certificationOccupancyType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_VaBorrowerCertificationOccupancyType 
    {
        Undefined = 0,
        A,
        B,
        C,
        D
    }
}
