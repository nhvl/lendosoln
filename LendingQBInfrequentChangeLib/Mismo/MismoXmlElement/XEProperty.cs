/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEProperty : Mismo.Common.AbstractXmlNode
    {
        public XEProperty()
        {
        }
        #region Schema
        //  <xs:element name="PROPERTY">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_LEGAL_DESCRIPTION" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="PARSED_STREET_ADDRESS" minOccurs="0"/>
        //        <xs:element ref="_VALUATION" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_County" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="BuildingStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Existing"/>
        //            <xs:enumeration value="Proposed"/>
        //            <xs:enumeration value="SubjectToAlterationImprovementRepairAndRehabilitation"/>
        //            <xs:enumeration value="SubstantiallyRehabilitated"/>
        //            <xs:enumeration value="UnderConstruction"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_FinancedNumberOfUnits" type="xs:string"/>
        //      <xs:attribute name="_StructureBuiltYear" type="xs:string"/>
        //      <xs:attribute name="_AcquiredDate" type="xs:string"/>
        //      <xs:attribute name="PlannedUnitDevelopmentIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_AcreageNumber" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_legalDescriptionList = new ArrayList();
        private XEParsedStreetAddress m_parsedStreetAddress;
        private ArrayList m_valuationList = new ArrayList();

        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_county;
        private string m_postalCode;
        private E_PropertyBuildingStatusType m_buildingStatusType;
        private string m_financedNumberOfUnits;
        private string m_structureBuiltYear;
        private string m_acquiredDate;
        private E_YesNoIndicator m_plannedUnitDevelopmentIndicator;
        private string m_acreageNumber;
        #endregion

        #region Public Properties
        public void AddLegalDescription(XELegalDescription legalDescription) 
        {
            if (null == legalDescription)
                return;
            m_legalDescriptionList.Add(legalDescription);
        }
        public void SetParsedStreetAddress(XEParsedStreetAddress parsedStreetAddress) 
        {
            m_parsedStreetAddress = parsedStreetAddress;
        }
        public void AddValuation(XEValuation valuation) 
        {
            if (null == valuation)
                return;
            m_valuationList.Add(valuation);
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string County 
        {
            get { return m_county; }
            set { m_county = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public E_PropertyBuildingStatusType BuildingStatusType 
        {
            get { return m_buildingStatusType; }
            set { m_buildingStatusType = value; }
        }
        public string FinancedNumberOfUnits 
        {
            get { return m_financedNumberOfUnits; }
            set { m_financedNumberOfUnits = value; }
        }
        public string StructureBuiltYear 
        {
            get { return m_structureBuiltYear; }
            set { m_structureBuiltYear = value; }
        }
        public string AcquiredDate 
        {
            get { return m_acquiredDate; }
            set { m_acquiredDate = value; }
        }
        public E_YesNoIndicator PlannedUnitDevelopmentIndicator 
        {
            get { return m_plannedUnitDevelopmentIndicator; }
            set { m_plannedUnitDevelopmentIndicator = value; }
        }
        public string AcreageNumber 
        {
            get { return m_acreageNumber; }
            set { m_acreageNumber = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PROPERTY");
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_County", m_county);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "BuildingStatusType", m_buildingStatusType);
            WriteAttribute(writer, "_FinancedNumberOfUnits", m_financedNumberOfUnits);
            WriteAttribute(writer, "_StructureBuiltYear", m_structureBuiltYear);
            WriteAttribute(writer, "_AcquiredDate", m_acquiredDate);
            WriteAttribute(writer, "PlannedUnitDevelopmentIndicator", m_plannedUnitDevelopmentIndicator);
            WriteAttribute(writer, "_AcreageNumber", m_acreageNumber);
            WriteElement(writer, m_legalDescriptionList);
            WriteElement(writer, m_parsedStreetAddress);
            WriteElement(writer, m_valuationList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_PropertyBuildingStatusType 
    {
        Undefined = 0,
        Existing,
        Proposed,
        SubjectToAlterationImprovementRepairAndRehabilitation,
        SubstantiallyRehabilitated,
        UnderConstruction
    }
}
