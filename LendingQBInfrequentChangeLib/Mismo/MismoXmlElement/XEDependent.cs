/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEDependent : Mismo.Common.AbstractXmlNode
	{
		public XEDependent()
		{
		}
        #region Schema
        // <xs:element name="DEPENDENT">
        //   <xs:complexType>
        //     <xs:attribute name="_AgeYears" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_ageYears;
        #endregion

        #region Public Properties
        public string AgeYears 
        {
            get { return m_ageYears; }
            set { m_ageYears = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("DEPENDENT");
            WriteAttribute(writer, "_AgeYears", m_ageYears);
            writer.WriteEndElement(); // </DEPENDENT>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
