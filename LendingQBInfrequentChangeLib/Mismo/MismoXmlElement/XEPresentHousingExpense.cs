/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEPresentHousingExpense : Mismo.Common.AbstractXmlNode
    {
        public XEPresentHousingExpense()
        {
        }
        #region Schema
        //  <xs:element name="PRESENT_HOUSING_EXPENSE">
        //    <xs:complexType>
        //      <xs:attribute name="HousingExpenseType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FirstMortgagePrincipalAndInterest"/>
        //            <xs:enumeration value="HazardInsurance"/>
        //            <xs:enumeration value="HomeownersAssociationDuesAndCondominiumFees"/>
        //            <xs:enumeration value="MI"/>
        //            <xs:enumeration value="OtherHousingExpense"/>
        //            <xs:enumeration value="OtherMortgageLoanPrincipalAndInterest"/>
        //            <xs:enumeration value="RealEstateTax"/>
        //            <xs:enumeration value="Rent"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaymentAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private E_PresentHousingExpenseType m_housingExpenseType;
        private string m_paymentAmount;
        #endregion

        #region Public Properties
        public E_PresentHousingExpenseType HousingExpenseType 
        {
            get { return m_housingExpenseType; }
            set { m_housingExpenseType = value; }
        }
        public string PaymentAmount 
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PRESENT_HOUSING_EXPENSE");
            WriteAttribute(writer, "HousingExpenseType", m_housingExpenseType);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_PresentHousingExpenseType 
    {
        Undefined = 0,
        FirstMortgagePrincipalAndInterest,
        HazardInsurance,
        HomeownersAssociationDuesAndCondominiumFees,
        MI,
        OtherHousingExpense,
        OtherMortgageLoanPrincipalAndInterest,
        RealEstateTax,
        Rent
    }
}
