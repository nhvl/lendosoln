/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEVaLoan : Mismo.Common.AbstractXmlNode
    {
        public XEVaLoan()
        {
        }
        #region Schema
        //  <xs:element name="VA_LOAN">
        //    <xs:complexType>
        //      <xs:attribute name="VABorrowerCoBorrowerMarriedIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BorrowerFundingFeePercent" type="xs:string"/>
        //      <xs:attribute name="VAEntitlementAmount" type="xs:string"/>
        //      <xs:attribute name="VAMaintenanceExpenseMonthlyAmount" type="xs:string"/>
        //      <xs:attribute name="VAUtilityExpenseMonthlyAmount" type="xs:string"/>
        //      <xs:attribute name="VAEntitlementCodeIdentifier" type="xs:string"/>
        //      <xs:attribute name="VAHouseholdSizeCount" type="xs:string"/>
        //      <xs:attribute name="VAResidualIncomeAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_vaBorrowerCoBorrowerMarriedIndicator;
        private string m_borrowerFundingFeePercent;
        private string m_vaEntitlementAmount;
        private string m_vaMaintenanceExpenseMonthlyAmount;
        private string m_vaUtilityExpenseMonthlyAmount;
        private string m_vaEntitlementCodeIdentifier;
        private string m_vaHouseholdSizeCount;
        private string m_vaResidualIncomeAmount;
        #endregion

        #region Public Properties
        public E_YesNoIndicator VaBorrowerCoBorrowerMarriedIndicator 
        {
            get { return m_vaBorrowerCoBorrowerMarriedIndicator; }
            set { m_vaBorrowerCoBorrowerMarriedIndicator = value; }
        }
        public string BorrowerFundingFeePercent 
        {
            get { return m_borrowerFundingFeePercent; }
            set { m_borrowerFundingFeePercent = value; }
        }
        public string VaEntitlementAmount 
        {
            get { return m_vaEntitlementAmount; }
            set { m_vaEntitlementAmount = value; }
        }
        public string VaMaintenanceExpenseMonthlyAmount 
        {
            get { return m_vaMaintenanceExpenseMonthlyAmount; }
            set { m_vaMaintenanceExpenseMonthlyAmount = value; }
        }
        public string VaUtilityExpenseMonthlyAmount 
        {
            get { return m_vaUtilityExpenseMonthlyAmount; }
            set { m_vaUtilityExpenseMonthlyAmount = value; }
        }
        public string VaEntitlementCodeIdentifier 
        {
            get { return m_vaEntitlementCodeIdentifier; }
            set { m_vaEntitlementCodeIdentifier = value; }
        }
        public string VaHouseholdSizeCount 
        {
            get { return m_vaHouseholdSizeCount; }
            set { m_vaHouseholdSizeCount = value; }
        }
        public string VaResidualIncomeAmount 
        {
            get { return m_vaResidualIncomeAmount; }
            set { m_vaResidualIncomeAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("VA_LOAN");
            WriteAttribute(writer, "VABorrowerCoBorrowerMarriedIndicator", m_vaBorrowerCoBorrowerMarriedIndicator);
            WriteAttribute(writer, "BorrowerFundingFeePercent", m_borrowerFundingFeePercent);
            WriteAttribute(writer, "VAEntitlementAmount", m_vaEntitlementAmount);
            WriteAttribute(writer, "VAMaintenanceExpenseMonthlyAmount", m_vaMaintenanceExpenseMonthlyAmount);
            WriteAttribute(writer, "VAUtilityExpenseMonthlyAmount", m_vaUtilityExpenseMonthlyAmount);
            WriteAttribute(writer, "VAEntitlementCodeIdentifier", m_vaEntitlementAmount);
            WriteAttribute(writer, "VAHouseholdSizeCount", m_vaHouseholdSizeCount);
            WriteAttribute(writer, "VAResidualIncomeAmount", m_vaResidualIncomeAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
