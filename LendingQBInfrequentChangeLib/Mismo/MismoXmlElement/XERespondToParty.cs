using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	/// <summary>
	/// Summary description for XERespondToParty.
	/// </summary>
	public class XERespondToParty : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="RESPOND_TO_PARTY">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="CONTACT_DETAIL" minOccurs="0" maxOccurs="unbounded"/>
        //          </xs:sequence>
        //          <xs:attribute name="_Name" type="xs:string"/>
        //          <xs:attribute name="_StreetAddress" type="xs:string"/>
        //          <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //          <xs:attribute name="_City" type="xs:string"/>
        //          <xs:attribute name="_State" type="xs:string"/>
        //          <xs:attribute name="_PostalCode" type="xs:string"/>
        //          <xs:attribute name="_Identifier" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_contactDetailList = new ArrayList();
        private string m_name;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_identifier;
        #endregion

        #region Public Properties
        public void AddContactDetail(XEContactDetail o) 
        {
            m_contactDetailList.Add(o);
        }
        public string Name 
        { 
            get { return m_name; } 
            set { m_name = value; } 
        } 
        public string StreetAddress 
        { 
            get { return m_streetAddress; } 
            set { m_streetAddress = value; } 
        } 
        public string StreetAddress2 
        { 
            get { return m_streetAddress2; } 
            set { m_streetAddress2 = value; } 
        } 
        public string City 
        { 
            get { return m_city; } 
            set { m_city = value; } 
        } 
        public string State 
        { 
            get { return m_state; } 
            set { m_state = value; } 
        } 
        public string PostalCode 
        { 
            get { return m_postalCode; } 
            set { m_postalCode = value; } 
        } 
        public string Identifier 
        { 
            get { return m_identifier; } 
            set { m_identifier = value; } 
        } 

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPOND_TO_PARTY");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_Identifier", m_identifier);

            foreach (XEContactDetail o in m_contactDetailList)
                o.GenerateXml(writer);

            writer.WriteEndElement(); // </RESPOND_TO_PARTY>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_contactDetailList = new ArrayList();
            XmlNodeList nodeList = el.SelectNodes("CONTACT_DETAIL");
            foreach (XmlElement o in nodeList) 
            {
                XEContactDetail xe = new XEContactDetail();
                xe.Parse(o);
                m_contactDetailList.Add(xe);
            }

            m_name = el.GetAttribute("_Name");
            m_streetAddress = el.GetAttribute("_StreetAddress");
            m_streetAddress2 = el.GetAttribute("_StreetAddress2");
            m_city = el.GetAttribute("_City");
            m_state = el.GetAttribute("_State");
            m_postalCode = el.GetAttribute("_PostalCode");
            m_identifier = el.GetAttribute("_Identifier");

        }
        #endregion
	}
}
