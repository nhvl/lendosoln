using System;
using System.Xml;

using Mismo.Common;
namespace Mismo.MismoXmlElement
{

	public class XEResponseData : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="RESPONSE_DATA">
        //      <xs:complexType mixed="true"/>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private IXmlNode m_content;
        private string m_contentInnerXml;
        #endregion

        #region Public Properties
        public IXmlNode Content 
        {
            set { m_content = value; }
        }
        public string ContentInnerXml 
        {
            get { return m_contentInnerXml; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPONSE_DATA");

            if (null != m_content)
                m_content.GenerateXml(writer);

            writer.WriteEndElement(); // </RESPONSE_DATA>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_contentInnerXml = el.InnerXml;
        }
        #endregion	

	}
}
