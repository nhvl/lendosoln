using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEContactPoint : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="CONTACT_POINT">
        //      <xs:complexType>
        //          <xs:attribute name="_RoleType">
        //          <xs:attribute name="_Type">
        //          <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //          <xs:attribute name="_Value" type="xs:string"/>
        //          <xs:attribute name="_PreferenceIndicator">
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_XEContactPointRoleType m_roleType;
        private string m_typeOtherDescription;
        private string m_value;
        private bool m_preferenceIndicator;
        #endregion

        #region Public Properties
        public E_XEContactPointRoleType RoleType 
        { 
            get { return m_roleType; } 
            set { m_roleType = value; } 
        } 

        private E_XEContactPointType m_type;
        public string TypeOtherDescription 
        { 
            get { return m_typeOtherDescription; } 
            set { m_typeOtherDescription = value; } 
        } 
        public string Value 
        { 
            get { return m_value; } 
            set { m_value = value; } 
        } 
        public bool PreferenceIndicator 
        { 
            get { return m_preferenceIndicator; } 
            set { m_preferenceIndicator = value; } 
        } 
        public E_XEContactPointType Type 
        { 
            get { return m_type; } 
            set { m_type = value; } 
        } 

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("CONTACT_POINT");
            WriteAttribute(writer, "_RoleType", m_roleType);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "_Value", m_value);
            WriteAttribute(writer, "_PreferenceIndicator", m_preferenceIndicator ? "Y" : "N");
            writer.WriteEndElement(); // </CONTACT_POINT>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;


            switch (el.GetAttribute("_RoleType")) 
            {
                case "Home": 
                    m_roleType = E_XEContactPointRoleType.Home;
                    break;
                case "Mobile":
                    m_roleType = E_XEContactPointRoleType.Mobile;
                    break;
                case "Work":
                    m_roleType = E_XEContactPointRoleType.Work;
                    break;
                default:
                    m_roleType = E_XEContactPointRoleType.Home;
                    break;
            }
            switch (el.GetAttribute("_Type")) 
            {
                case "Email":
                    m_type = E_XEContactPointType.Email;
                    break;
                case "Fax":
                    m_type = E_XEContactPointType.Fax;
                    break;
                case "Phone":
                    m_type = E_XEContactPointType.Phone;
                    break;
                case "Other":
                default:
                    m_type = E_XEContactPointType.Other;
                    break;
            }
            m_typeOtherDescription = el.GetAttribute("_TypeOtherDescription");
            m_value = el.GetAttribute("_Value");
            m_preferenceIndicator = el.GetAttribute("_PreferenceIndicator") == "Y";

        }
        #endregion
	}

    public enum E_XEContactPointRoleType 
    {
        Undefined = 0,
        Home,
        Mobile,
        Work
    }
    public enum E_XEContactPointType 
    {
        Undefined = 0,
        Email,
        Fax,
        Other,
        Phone
    }
}
