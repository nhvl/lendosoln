/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEGovernmentReporting : Mismo.Common.AbstractXmlNode
	{
		public XEGovernmentReporting()
		{
		}
        #region Schema
        // <xs:element name="GOVERNMENT_REPORTING">
        //   <xs:complexType>
        //     <xs:attribute name="HMDAPurposeOfLoanType">
        //     <xs:attribute name="HMDAPreapprovalType">
        //     <xs:attribute name="HMDA_HOEPALoanStatusIndicator">
        //     <xs:attribute name="HMDARateSpreadPercent" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private E_HmdaPurposeOfLoanType m_hmdaPurposeOfLoanType;
        private E_HmdaPreapprovalType m_hmdaPreapprovalType;
        private E_YesNoIndicator m_hmdaHoepaLoanStatusIndicator;
        private string m_hmdaRateSpreadPercent;
        #endregion

        #region Public Properties
        public E_HmdaPurposeOfLoanType HmdaPurposeOfLoanType 
        {
            get { return m_hmdaPurposeOfLoanType; }
            set { m_hmdaPurposeOfLoanType = value; }
        }
        public E_HmdaPreapprovalType HmdaPreapprovalType 
        {
            get { return m_hmdaPreapprovalType; }
            set { m_hmdaPreapprovalType = value; }
        }
        public E_YesNoIndicator HmdaHoepaLoanStatusIndicator 
        {
            get { return m_hmdaHoepaLoanStatusIndicator; }
            set { m_hmdaHoepaLoanStatusIndicator = value; }
        }
        public string HmdaRateSpreadPercent 
        {
            get { return m_hmdaRateSpreadPercent; }
            set { m_hmdaRateSpreadPercent = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("GOVERNMENT_REPORTING");
            WriteAttribute(writer, "HMDAPurposeOfLoanType", m_hmdaPurposeOfLoanType);
            WriteAttribute(writer, "HMDAPreapprovalType", m_hmdaPreapprovalType);
            WriteAttribute(writer, "HMDA_HOEPALoanStatusIndicator", m_hmdaHoepaLoanStatusIndicator);
            WriteAttribute(writer, "HMDARateSpreadPercent", m_hmdaRateSpreadPercent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}

    public enum E_HmdaPurposeOfLoanType 
    {
        Undefined = 0,
        HomePurchase,
        HomeImprovement,
        Refinancing
    }
    public enum E_HmdaPreapprovalType 
    {
        Undefined = 0,
        PreapprovalWasRequested,
        PreapprovalWasNotRequested,
        NotApplicable
    }
}
