/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEUnequalPayments : Mismo.Common.AbstractXmlNode
	{
		public XEUnequalPayments()
		{
		}
        #region Schema
        //  <xs:element name="_UNEQUAL_PAYMENTS">
        //    <xs:complexType>
        //      <xs:attribute name="_DueDate" type="xs:string"/>
        //      <xs:attribute name="_PaymentAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_dueDate;
        private string m_paymentAmount;
        #endregion

        #region Public Properties
        public string DueDate 
        {
            get { return m_dueDate; }
            set { m_dueDate = value; }
        }
        public string PaymentAmount 
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_UNEQUAL_PAYMENTS");
            WriteAttribute(writer, "_DueDate", m_dueDate);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
