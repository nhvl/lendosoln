using System;
using System.Xml;
using System.Collections;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{

	public class XERequestGroup : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="REQUEST_GROUP">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="REQUESTING_PARTY" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="RECEIVING_PARTY" minOccurs="0"/>
        //              <xs:element ref="SUBMITTING_PARTY" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="REQUEST" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="Signature" minOccurs="0"/> // TODO
        //          </xs:sequence>
        //          <xs:attribute name="MISMOVersionID" type="xs:string" fixed="2.3.1"/>
        //          <xs:attribute name="_ID" type="xs:ID"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_requestingPartyList = new ArrayList();
        private XEReceivingParty m_receivingParty;
        private ArrayList m_submittingPartyList = new ArrayList();
        private ArrayList m_requestList = new ArrayList();
        private string m_mismoVersionID = "2.3.1";
        private string m_id;
        #endregion

        #region Public Properties
        public void AddRequestingParty(XERequestingParty o) 
        {
            m_requestingPartyList.Add(o);
        }
        public XERequestingParty GetRequestingParty(int index) 
        {
            if (null == m_requestingPartyList || index < 0 || index >= m_requestingPartyList.Count)
                return null;

            return (XERequestingParty) m_requestingPartyList[index];
        }

        public XEReceivingParty ReceivingParty 
        {
            get { return m_receivingParty; }
            set { m_receivingParty = value; }
        }

        public void AddSubmittingParty(XESubmittingParty o) 
        {
            m_submittingPartyList.Add(o);
        }
        public XESubmittingParty GetSubmittingParty(int index) 
        {
            if (null == m_submittingPartyList || index < 0 || index >= m_submittingPartyList.Count)
                return null;
            return (XESubmittingParty) m_submittingPartyList[index];
        }

        public void AddRequest(XERequest o) 
        {
            m_requestList.Add(o);
        }
        public XERequest GetRequest(int index) 
        {
            if (m_requestList == null || index < 0 || index >= m_requestList.Count)
                return null;

            return (XERequest) m_requestList[index];
        }

        public XERequest CreateRequestHelper(string internalAccountIdentifier, string loginAccountIdentifier, string loginAccountPassword) 
        {
            XERequest request = new XERequest();
            request.InternalAccountIdentifier = internalAccountIdentifier;
            request.LoginAccountIdentifier = loginAccountIdentifier;
            request.LoginAccountPassword = loginAccountPassword;
            request.RequestDateTime = DateTime.Now;

            AddRequest(request);

            return request;
        }
        public string ID 
        {
            get { return m_id; }
            set { m_id = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("REQUEST_GROUP");
            WriteAttribute(writer, "MISMOVersionID", m_mismoVersionID);
            WriteAttribute(writer, "_ID", m_id);

            foreach (XERequestingParty o in m_requestingPartyList)
                o.GenerateXml(writer);

            if (null != m_receivingParty)
                m_receivingParty.GenerateXml(writer);

            foreach (XESubmittingParty o in m_submittingPartyList)
                o.GenerateXml(writer);

            foreach (XERequest o in m_requestList)
                o.GenerateXml(writer);

            writer.WriteEndElement(); // </REQUEST_GROUP>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            #region REQUESTING_PARTY
            m_requestingPartyList = new ArrayList();
            XmlNodeList nodeList = el.SelectNodes("REQUESTING_PARTY");
            foreach (XmlElement o in nodeList) 
            {
                XERequestingParty xe = new XERequestingParty();
                xe.Parse(o);
                m_requestingPartyList.Add(xe);
            }
            #endregion

            #region RECEIVING_PARTY
            XmlElement singleNode = (XmlElement) el.SelectSingleNode("RECEIVING_PARTY");
            m_receivingParty = null;
            if (null != singleNode) 
            {
                XEReceivingParty xe = new XEReceivingParty();
                xe.Parse(singleNode);
                m_receivingParty = xe;
            } 
            #endregion

            #region SUBMITTING_PARTY
            m_submittingPartyList = new ArrayList();
            nodeList = el.SelectNodes("SUBMITTING_PARTY");
            foreach (XmlElement o in nodeList) 
            {
                XESubmittingParty xe = new XESubmittingParty();
                xe.Parse(o);
                m_submittingPartyList.Add(xe);
            }
            #endregion

            #region REQUEST
            m_requestList = new ArrayList();
            nodeList = el.SelectNodes("REQUEST");
            foreach (XmlElement o in nodeList) 
            {
                XERequest xe = new XERequest();
                xe.Parse(o);
                m_requestList.Add(xe);
            }
            #endregion

            m_mismoVersionID = el.GetAttribute("MISMOVersionID");
            m_id = el.GetAttribute("_ID");

        }
        #endregion
	}
}
