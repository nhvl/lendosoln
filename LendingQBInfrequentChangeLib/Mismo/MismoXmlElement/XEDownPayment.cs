/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEDownPayment : Mismo.Common.AbstractXmlNode
	{
		public XEDownPayment()
		{
		}
        #region Schema
        // <xs:element name="DOWN_PAYMENT">
        //   <xs:complexType>
        //     <xs:attribute name="_Amount" type="xs:string"/>
        //     <xs:attribute name="_SourceDescription" type="xs:string"/>
        //     <xs:attribute name="_Type">
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_amount;
        private string m_sourceDescription;
        private E_DownPaymentType m_type;
        #endregion

        #region Public Properties
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public string SourceDescription 
        {
            get { return m_sourceDescription; }
            set { m_sourceDescription = value; }
        }
        public E_DownPaymentType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("DOWN_PAYMENT");
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_SourceDescription", m_sourceDescription);
            WriteAttribute(writer, "_Type", m_type);
            writer.WriteEndElement(); // </DOWN_PAYMENT>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_DownPaymentType 
    {
        Undefined = 0,
        BridgeLoan,
        CashOnHand,
        CheckingSavings,
        DepositOnSalesContract,
        EquityOnPendingSale,
        EquityOnSoldProperty,
        EquityOnSubjectProperty,
        GiftFunds,
        LifeInsuranceCashValue,
        LotEquity,
        OtherTypeOfDownPayment,
        RentWithOptionToPurchase,
        RetirementFunds,
        SaleOfChattel,
        SecuredBorrowedFunds,
        StocksAndBonds,
        SweatEquity,
        TradeEquity,
        TrustFunds,
        UnsecuredBorrowedFunds
    }
}
