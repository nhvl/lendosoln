using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEStatus : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="STATUS">
        //      <xs:complexType>
        //          <xs:attribute name="_Condition" type="xs:string"/>
        //          <xs:attribute name="_Code" type="xs:string"/>
        //          <xs:attribute name="_Name" type="xs:string"/>
        //          <xs:attribute name="_Description" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_condition;
        private string m_code;
        private string m_name;
        private string m_description;
        #endregion

        #region Public Properties
        public string Condition 
        { 
            get { return m_condition; } 
            set { m_condition = value; } 
        } 
        public string Code 
        { 
            get { return m_code; } 
            set { m_code = value; } 
        } 
        public string Name 
        { 
            get { return m_name; } 
            set { m_name = value; } 
        } 
        public string Description 
        { 
            get { return m_description; } 
            set { m_description = value; } 
        } 
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("STATUS");
            WriteAttribute(writer, "_Condition", m_condition);
            WriteAttribute(writer, "_Code", m_code);
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_Description", m_description);
            writer.WriteEndElement(); // </STATUS>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_condition = el.GetAttribute("_Condition");
            m_code = el.GetAttribute("_Code");
            m_name = el.GetAttribute("_Name");
            m_description = el.GetAttribute("_Description");
        }
        #endregion
	}
}
