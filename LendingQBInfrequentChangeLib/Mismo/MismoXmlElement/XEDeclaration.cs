/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEDeclaration : Mismo.Common.AbstractXmlNode
    {
        public XEDeclaration()
        {
        }
        #region Schema
        //  <xs:element name="DECLARATION">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_EXPLANATION" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="AlimonyChildSupportObligationIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BankruptcyIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BorrowedDownPaymentIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="CitizenshipResidencyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="USCitizen"/>
        //            <xs:enumeration value="PermanentResidentAlien"/>
        //            <xs:enumeration value="NonPermanentResidentAlien"/>
        //            <xs:enumeration value="NonResidentAlien"/>
        //            <xs:enumeration value="Unknown"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="CoMakerEndorserOfNoteIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="HomeownerPastThreeYearsType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Yes"/>
        //            <xs:enumeration value="No"/>
        //            <xs:enumeration value="Unknown"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="IntentToOccupyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Yes"/>
        //            <xs:enumeration value="No"/>
        //            <xs:enumeration value="Unknown"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanForeclosureOrJudgementIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="OutstandingJudgementsIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PartyToLawsuitIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PresentlyDelinquentIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PriorPropertyTitleType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Sole"/>
        //            <xs:enumeration value="JointWithSpouse"/>
        //            <xs:enumeration value="JointWithOtherThanSpouse"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PriorPropertyUsageType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Investment"/>
        //            <xs:enumeration value="PrimaryResidence"/>
        //            <xs:enumeration value="SecondaryResidence"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PropertyForeclosedPastSevenYearsIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BorrowerFirstTimeHomebuyerIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_explanationList = new ArrayList();
        private E_YesNoIndicator m_alimonyChildSupportObligationIndicator;
        private E_YesNoIndicator m_bankruptcyIndicator;
        private E_YesNoIndicator m_borrowedDownPaymentIndicator;
        private E_DeclarationCitizenshipResidencyType m_citizenshipResidencyType;
        private E_YesNoIndicator m_coMakerEndorserOfNoteIndicator;
        private E_DeclarationHomeownerPastThreeYearsType m_homeownerPastThreeYearsType;
        private E_DeclarationIntentToOccupyType m_intentToOccupyType;
        private E_YesNoIndicator m_loanForeclosureOrJudgementIndicator;
        private E_YesNoIndicator m_outstandingJudgementsIndicator;
        private E_YesNoIndicator m_partyToLawsuitIndicator;
        private E_YesNoIndicator m_presentlyDelinquentIndicator;
        private E_DeclarationPriorPropertyTitleType m_priorPropertyTitleType;
        private E_DeclarationPriorPropertyUsageType m_priorPropertyUsageType;
        private E_YesNoIndicator m_propertyForeclosedPastSevenYearsIndicator;
        private E_YesNoIndicator m_borrowerFirstTimeHomebuyerIndicator;
        #endregion

        #region Public Properties
        public void AddExplanation(XEExplanation explanation) 
        {
            if (null == explanation)
                return;

            m_explanationList.Add(explanation);
        }
        public E_YesNoIndicator AlimonyChildSupportObligationIndicator 
        {
            get { return m_alimonyChildSupportObligationIndicator; }
            set { m_alimonyChildSupportObligationIndicator = value; }
        }
        public E_YesNoIndicator BankruptcyIndicator 
        {
            get { return m_bankruptcyIndicator; }
            set { m_bankruptcyIndicator = value; }
        }
        public E_YesNoIndicator BorrowedDownPaymentIndicator 
        {
            get { return m_borrowedDownPaymentIndicator; }
            set { m_borrowedDownPaymentIndicator = value; }
        }
        public E_DeclarationCitizenshipResidencyType CitizenshipResidencyType 
        {
            get { return m_citizenshipResidencyType; }
            set { m_citizenshipResidencyType = value; }
        }
        public E_YesNoIndicator CoMakerEndorserOfNoteIndicator 
        {
            get { return m_coMakerEndorserOfNoteIndicator; }
            set { m_coMakerEndorserOfNoteIndicator = value; }
        }
        public E_DeclarationHomeownerPastThreeYearsType HomeownerPastThreeYearsType 
        {
            get { return m_homeownerPastThreeYearsType; }
            set { m_homeownerPastThreeYearsType = value; }
        }
        public E_DeclarationIntentToOccupyType IntentToOccupyType 
        {
            get { return m_intentToOccupyType; }
            set { m_intentToOccupyType = value; }
        }
        public E_YesNoIndicator LoanForeclosureOrJudgementIndicator 
        {
            get { return m_loanForeclosureOrJudgementIndicator; }
            set { m_loanForeclosureOrJudgementIndicator = value; }
        }
        public E_YesNoIndicator OutstandingJudgementsIndicator 
        {
            get { return m_outstandingJudgementsIndicator; }
            set { m_outstandingJudgementsIndicator = value; }
        }
        public E_YesNoIndicator PartyToLawsuitIndicator 
        {
            get { return m_partyToLawsuitIndicator; }
            set { m_partyToLawsuitIndicator = value; }
        }
        public E_YesNoIndicator PresentlyDelinquentIndicator 
        {
            get { return m_presentlyDelinquentIndicator; }
            set { m_presentlyDelinquentIndicator = value; }
        }
        public E_DeclarationPriorPropertyTitleType PriorPropertyTitleType 
        {
            get { return m_priorPropertyTitleType; }
            set { m_priorPropertyTitleType = value; }
        }
        public E_DeclarationPriorPropertyUsageType PriorPropertyUsageType 
        {
            get { return m_priorPropertyUsageType; }
            set { m_priorPropertyUsageType = value; }
        }
        public E_YesNoIndicator PropertyForeclosedPastSevenYearsIndicator 
        {
            get { return m_propertyForeclosedPastSevenYearsIndicator; }
            set { m_propertyForeclosedPastSevenYearsIndicator = value; }
        }
        public E_YesNoIndicator BorrowerFirstTimeHomebuyerIndicator 
        {
            get { return m_borrowerFirstTimeHomebuyerIndicator; }
            set { m_borrowerFirstTimeHomebuyerIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("DECLARATION");
            WriteAttribute(writer, "AlimonyChildSupportObligationIndicator", m_alimonyChildSupportObligationIndicator);
            WriteAttribute(writer, "BankruptcyIndicator", m_bankruptcyIndicator);
            WriteAttribute(writer, "BorrowedDownPaymentIndicator", m_borrowedDownPaymentIndicator);
            WriteAttribute(writer, "CitizenshipResidencyType", m_citizenshipResidencyType);
            WriteAttribute(writer, "CoMakerEndorserOfNoteIndicator", m_coMakerEndorserOfNoteIndicator);
            WriteAttribute(writer, "HomeownerPastThreeYearsType", m_homeownerPastThreeYearsType);
            WriteAttribute(writer, "IntentToOccupyType", m_intentToOccupyType);
            WriteAttribute(writer, "LoanForeclosureOrJudgementIndicator", m_loanForeclosureOrJudgementIndicator);
            WriteAttribute(writer, "OutstandingJudgementsIndicator", m_outstandingJudgementsIndicator);
            WriteAttribute(writer, "PartyToLawsuitIndicator", m_partyToLawsuitIndicator);
            WriteAttribute(writer, "PresentlyDelinquentIndicator", m_presentlyDelinquentIndicator);
            WriteAttribute(writer, "PriorPropertyTitleType", m_priorPropertyTitleType);
            WriteAttribute(writer, "PriorPropertyUsageType", m_priorPropertyUsageType);
            WriteAttribute(writer, "PropertyForeclosedPastSevenYearsIndicator", m_propertyForeclosedPastSevenYearsIndicator);
            WriteAttribute(writer, "BorrowerFirstTimeHomebuyerIndicator", m_borrowerFirstTimeHomebuyerIndicator);
            foreach (XEExplanation o in m_explanationList) 
            {
                o.GenerateXml(writer);
            }
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_DeclarationCitizenshipResidencyType 
    {
        Undefined = 0,
        USCitizen,
        PermanentResidentAlien,
        NonPermanentResidentAlien,
        NonResidentAlien,
        Unknown
    }
    public enum E_DeclarationHomeownerPastThreeYearsType 
    {
        Undefined = 0,
        Yes,
        No,
        Unknown

    }
    public enum E_DeclarationIntentToOccupyType 
    {
        Undefined = 0,
        Yes,
        No,
        Unknown

    }
    public enum E_DeclarationPriorPropertyTitleType 
    {
        Undefined = 0,
        Sole,
        JointWithSpouse,
        JointWithOtherThanSpouse

    }
    public enum E_DeclarationPriorPropertyUsageType 
    {
        Undefined = 0,
        Investment,
        PrimaryResidence,
        SecondaryResidence

    }
}
