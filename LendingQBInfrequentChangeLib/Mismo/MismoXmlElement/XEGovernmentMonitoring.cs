/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEGovernmentMonitoring : Mismo.Common.AbstractXmlNode
    {
        public XEGovernmentMonitoring()
        {
        }
        #region Schema
        //  <xs:element name="GOVERNMENT_MONITORING">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="HMDA_RACE" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="GenderType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Female"/>
        //            <xs:enumeration value="InformationNotProvidedUnknown"/>
        //            <xs:enumeration value="Male"/>
        //            <xs:enumeration value="NotApplicable"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="OtherRaceNationalOriginDescription" type="xs:string"/>
        //      <xs:attribute name="RaceNationalOriginRefusalIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="RaceNationalOriginType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="AmericanIndianOrAlaskanNative"/>
        //            <xs:enumeration value="AsianOrPacificIslander"/>
        //            <xs:enumeration value="BlackNotOfHispanicOrigin"/>
        //            <xs:enumeration value="Hispanic"/>
        //            <xs:enumeration value="InformationNotProvided"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="WhiteNotOfHispanicOrigin"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="HMDAEthnicityType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="HispanicOrLatino"/>
        //            <xs:enumeration value="NotHispanicOrLatino"/>
        //            <xs:enumeration value="InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication"/>
        //            <xs:enumeration value="NotApplicable"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private ArrayList m_hmdaRaceList = new ArrayList();
        private E_GovernmentMonitoringGenderType m_genderType;
        private string m_otherRaceNationalOriginDescription;
        private E_YesNoIndicator m_raceNationalOriginRefusalIndicator;
        private E_GovernmentMonitoringRaceNationalOriginType m_raceNationalOriginType;
        private E_GovernmentMonitoringHmdaEthnicityType m_hmdaEthnicityType;
        #endregion

        #region Public Properties
        public void AddHmdaRace(XEHmdaRace hmdaRace) 
        {
            if (null == hmdaRace)
                return;
            m_hmdaRaceList.Add(hmdaRace);
        }
        public E_GovernmentMonitoringGenderType GenderType 
        {
            get { return m_genderType; }
            set { m_genderType = value; }
        }
        public string OtherRaceNationalOriginDescription 
        {
            get { return m_otherRaceNationalOriginDescription; }
            set { m_otherRaceNationalOriginDescription = value; }
        }
        public E_YesNoIndicator RaceNationalOriginRefusalIndicator 
        {
            get { return m_raceNationalOriginRefusalIndicator; }
            set { m_raceNationalOriginRefusalIndicator = value; }
        }
        public E_GovernmentMonitoringRaceNationalOriginType RaceNationalOriginType 
        {
            get { return m_raceNationalOriginType; }
            set { m_raceNationalOriginType = value; }
        }
        public E_GovernmentMonitoringHmdaEthnicityType HmdaEthnicityType 
        {
            get { return m_hmdaEthnicityType; }
            set { m_hmdaEthnicityType = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("GOVERNMENT_MONITORING");
            WriteAttribute(writer, "GenderType", m_genderType);
            WriteAttribute(writer, "OtherRaceNationalOriginDescription", m_otherRaceNationalOriginDescription);
            WriteAttribute(writer, "RaceNationalOriginRefusalIndicator", m_raceNationalOriginRefusalIndicator);
            WriteAttribute(writer, "RaceNationalOriginType", m_raceNationalOriginType);
            WriteAttribute(writer, "HMDAEthnicityType", m_hmdaEthnicityType);
            WriteElement(writer, m_hmdaRaceList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_GovernmentMonitoringGenderType 
    {
        Undefined = 0,
        Female,
        InformationNotProvidedUnknown,
        Male,
        NotApplicable
    }
    public enum E_GovernmentMonitoringRaceNationalOriginType 
    {
        Undefined = 0,
        AmericanIndianOrAlaskanNative,
        AsianOrPacificIslander,
        BlackNotOfHispanicOrigin,
        Hispanic,
        InformationNotProvided,
        Other,
        WhiteNotOfHispanicOrigin
    }
    public enum E_GovernmentMonitoringHmdaEthnicityType 
    {
        Undefined = 0,
        HispanicOrLatino,
        NotHispanicOrLatino,
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        NotApplicable
    }
}
