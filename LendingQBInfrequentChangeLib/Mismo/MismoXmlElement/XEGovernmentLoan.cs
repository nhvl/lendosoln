/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEGovernmentLoan : Mismo.Common.AbstractXmlNode
    {
        public XEGovernmentLoan()
        {
        }
        #region Schema
        //  <xs:element name="GOVERNMENT_LOAN">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="FHA_LOAN" minOccurs="0"/>
        //        <xs:element ref="FHA_VA_LOAN" minOccurs="0"/>
        //        <xs:element ref="VA_LOAN" minOccurs="0"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private XEFhaLoan m_fhaLoan;
        private XEFhaVaLoan m_fhaVaLoan;
        private XEVaLoan m_vaLoan;
        #endregion

        #region Public Properties
        public void SetFhaLoan(XEFhaLoan fhaLoan) 
        {
            m_fhaLoan = fhaLoan;
        }
        public void SetFhaVaLoan(XEFhaVaLoan fhaVaLoan) 
        {
            m_fhaVaLoan = fhaVaLoan;
        }
        public void SetVaLoan(XEVaLoan vaLoan) 
        {
            m_vaLoan = vaLoan;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("GOVERNMENT_LOAN");
            WriteElement(writer, m_fhaLoan);
            WriteElement(writer, m_fhaVaLoan);
            WriteElement(writer, m_vaLoan);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
