/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEReoProperty : Mismo.Common.AbstractXmlNode
    {
        public XEReoProperty()
        {
        }
        #region Schema
        //  <xs:element name="REO_PROPERTY">
        //    <xs:complexType>
        //      <xs:attribute name="REO_ID" type="xs:ID"/>
        //      <xs:attribute name="BorrowerID" type="xs:IDREFS"/>
        //      <xs:attribute name="LiabilityID" type="xs:IDREFS"/>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="_CurrentResidenceIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_DispositionStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="PendingSale"/>
        //            <xs:enumeration value="RetainForRental"/>
        //            <xs:enumeration value="RetainForPrimaryOrSecondaryResidence"/>
        //            <xs:enumeration value="Sold"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_GSEPropertyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="SingleFamily"/>
        //            <xs:enumeration value="Condominium"/>
        //            <xs:enumeration value="Townhouse"/>
        //            <xs:enumeration value="Cooperative"/>
        //            <xs:enumeration value="TwoToFourUnitProperty"/>
        //            <xs:enumeration value="MultifamilyMoreThanFourUnits"/>
        //            <xs:enumeration value="ManufacturedMobileHome"/>
        //            <xs:enumeration value="CommercialNonResidential"/>
        //            <xs:enumeration value="MixedUseResidential"/>
        //            <xs:enumeration value="Farm"/>
        //            <xs:enumeration value="HomeAndBusinessCombined"/>
        //            <xs:enumeration value="Land"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_LienInstallmentAmount" type="xs:string"/>
        //      <xs:attribute name="_LienUPBAmount" type="xs:string"/>
        //      <xs:attribute name="_MaintenanceExpenseAmount" type="xs:string"/>
        //      <xs:attribute name="_MarketValueAmount" type="xs:string"/>
        //      <xs:attribute name="_RentalIncomeGrossAmount" type="xs:string"/>
        //      <xs:attribute name="_RentalIncomeNetAmount" type="xs:string"/>
        //      <xs:attribute name="_SubjectIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_reoId;
        private string m_borrowerId;
        private string m_liabilityId;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private E_YesNoIndicator m_currentResidenceIndicator;
        private E_ReoPropertyDispositionStatusType m_dispositionStatusType;
        private E_ReoPropertyGsePropertyType m_gsePropertyType;
        private string m_lienInstallmentAmount;
        private string m_lienUPBAmount;
        private string m_maintenanceExpenseAmount;
        private string m_marketValueAmount;
        private string m_rentalIncomeGrossAmount;
        private string m_rentalIncomeNetAmount;
        private E_YesNoIndicator m_subjectIndicator;
        #endregion

        #region Public Properties
        public string ReoId 
        {
            get { return m_reoId; }
            set { m_reoId = value; }
        }
        public string BorrowerId 
        {
            get { return m_borrowerId; }
            set { m_borrowerId = value; }
        }
        public string LiabilityId 
        {
            get { return m_liabilityId; }
            set { m_liabilityId = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public E_YesNoIndicator CurrentResidenceIndicator 
        {
            get { return m_currentResidenceIndicator; }
            set { m_currentResidenceIndicator = value; }
        }
        public E_ReoPropertyDispositionStatusType DispositionStatusType 
        {
            get { return m_dispositionStatusType; }
            set { m_dispositionStatusType = value; }
        }
        public E_ReoPropertyGsePropertyType GsePropertyType 
        {
            get { return m_gsePropertyType; }
            set { m_gsePropertyType = value; }
        }
        public string LienInstallmentAmount 
        {
            get { return m_lienInstallmentAmount; }
            set { m_lienInstallmentAmount = value; }
        }
        public string LienUPBAmount 
        {
            get { return m_lienUPBAmount; }
            set { m_lienUPBAmount = value; }
        }
        public string MaintenanceExpenseAmount 
        {
            get { return m_maintenanceExpenseAmount; }
            set { m_maintenanceExpenseAmount = value; }
        }
        public string MarketValueAmount 
        {
            get { return m_marketValueAmount; }
            set { m_marketValueAmount = value; }
        }
        public string RentalIncomeGrossAmount 
        {
            get { return m_rentalIncomeGrossAmount; }
            set { m_rentalIncomeGrossAmount = value; }
        }
        public string RentalIncomeNetAmount 
        {
            get { return m_rentalIncomeNetAmount; }
            set { m_rentalIncomeNetAmount = value; }
        }
        public E_YesNoIndicator SubjectIndicator 
        {
            get { return m_subjectIndicator; }
            set { m_subjectIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("REO_PROPERTY");
            WriteAttribute(writer, "REO_ID", m_reoId);
            WriteAttribute(writer, "BorrowerID", m_borrowerId);
            WriteAttribute(writer, "LiabilityID", m_liabilityId);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_CurrentResidenceIndicator", m_currentResidenceIndicator);
            WriteAttribute(writer, "_DispositionStatusType", m_dispositionStatusType);
            WriteAttribute(writer, "_GSEPropertyType", m_gsePropertyType);
            WriteAttribute(writer, "_LienInstallmentAmount", m_lienInstallmentAmount);
            WriteAttribute(writer, "_LienUPBAmount", m_lienUPBAmount);
            WriteAttribute(writer, "_MaintenanceExpenseAmount", m_maintenanceExpenseAmount);
            WriteAttribute(writer, "_MarketValueAmount", m_marketValueAmount);
            WriteAttribute(writer, "_RentalIncomeGrossAmount", m_rentalIncomeGrossAmount);
            WriteAttribute(writer, "_RentalIncomeNetAmount", m_rentalIncomeNetAmount);
            WriteAttribute(writer, "_SubjectIndicator", m_subjectIndicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ReoPropertyDispositionStatusType 
    {
        Undefined = 0,
        PendingSale,
        RetainForRental,
        RetainForPrimaryOrSecondaryResidence,
        Sold
    }
    public enum E_ReoPropertyGsePropertyType 
    {
        Undefined = 0,
        SingleFamily,
        Condominium,
        Townhouse,
        Cooperative,
        TwoToFourUnitProperty,
        MultifamilyMoreThanFourUnits,
        ManufacturedMobileHome,
        CommercialNonResidential,
        MixedUseResidential,
        Farm,
        HomeAndBusinessCombined,
        Land
    }
}
