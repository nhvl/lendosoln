/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.MismoXmlElement
{
	public class XEAsset : Mismo.Common.AbstractXmlNode
	{

		public XEAsset()
		{
		}
        #region Schema
        // <xs:element name="ASSET">
        //   <xs:complexType>
        //     <xs:attribute name="BorrowerID" type="xs:IDREFS"/>
        //     <xs:attribute name="_AccountIdentifier" type="xs:string"/>
        //     <xs:attribute name="_CashOrMarketValueAmount" type="xs:string"/>
        //     <xs:attribute name="_Type" />
        //     <xs:attribute name="_VerifiedIndicator">
        //     <xs:attribute name="_HolderName" type="xs:string"/>
        //     <xs:attribute name="_HolderStreetAddress" type="xs:string"/>
        //     <xs:attribute name="_HolderCity" type="xs:string"/>
        //     <xs:attribute name="_HolderState" type="xs:string"/>
        //     <xs:attribute name="_HolderPostalCode" type="xs:string"/>
        //     <xs:attribute name="AutomobileMakeDescription" type="xs:string"/>
        //     <xs:attribute name="AutomobileModelYear" type="xs:string"/>
        //     <xs:attribute name="LifeInsuranceFaceValueAmount" type="xs:string"/>
        //     <xs:attribute name="OtherAssetTypeDescription" type="xs:string"/>
        //     <xs:attribute name="StockBondMutualFundShareCount" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_borrowerID;
        private string m_accountIdentifier;
        private string m_cashOrMarketValueAmount;
        private E_XEAssetType m_type;
        private string m_verifiedIndicator;
        private string m_holderName;
        private string m_holderStreetAddress;
        private string m_holderCity;
        private string m_holderState;
        private string m_holderPostalCode;
        private string m_automobileMakeDescription;
        private string m_automobileModelYear;
        private string m_lifeInsuranceFaceValueAmount;
        private string m_otherAssetTypeDescription;
        private string m_stockBondMutualFundShareCount;

        #endregion

        #region Public Properties
        public string BorrowerID 
        {
            get { return m_borrowerID; }
            set { m_borrowerID = value; }
        }
        public string AccountIdentifier 
        {
            get { return m_accountIdentifier; }
            set { m_accountIdentifier = value; }
        }
        public string CashOrMarketValueAmount 
        {
            get { return m_cashOrMarketValueAmount; }
            set { m_cashOrMarketValueAmount = value; }
        }

        public E_XEAssetType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string VerifiedIndicator 
        {
            get { return m_verifiedIndicator; }
            set { m_verifiedIndicator = value; }
        }
        public string HolderName 
        {
            get { return m_holderName; }
            set { m_holderName = value; }
        }
        public string HolderStreetAddress 
        {
            get { return m_holderStreetAddress; }
            set { m_holderStreetAddress = value; }
        }
        public string HolderCity 
        {
            get { return m_holderCity; }
            set { m_holderCity = value; }
        }
        public string HolderState 
        {
            get { return m_holderState; }
            set { m_holderState = value; }
        }
        public string HolderPostalCode 
        {
            get { return m_holderPostalCode; }
            set { m_holderPostalCode = value; }
        }
        public string AutomobileMakeDescription 
        {
            get { return m_automobileMakeDescription; }
            set { m_automobileMakeDescription = value; }
        }
        public string AutomobileModelYear 
        {
            get { return m_automobileModelYear; }
            set { m_automobileModelYear = value; }
        }
        public string LifeInsuranceFaceValueAmount 
        {
            get { return m_lifeInsuranceFaceValueAmount; }
            set { m_lifeInsuranceFaceValueAmount = value; }
        }
        public string OtherAssetTypeDescription 
        {
            get { return m_otherAssetTypeDescription; }
            set { m_otherAssetTypeDescription = value; }
        }
        public string StockBondMutualFundShareCount 
        {
            get { return m_stockBondMutualFundShareCount; }
            set { m_stockBondMutualFundShareCount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ASSET");
            WriteAttribute(writer, "BorrowerID", m_borrowerID);
            WriteAttribute(writer, "_AccountIdentifier", m_accountIdentifier);
            WriteAttribute(writer, "_CashOrMarketValueAmount", m_cashOrMarketValueAmount);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_VerifiedIndicator", m_verifiedIndicator);
            WriteAttribute(writer, "_HolderName", m_holderName);
            WriteAttribute(writer, "_HolderStreetAddress", m_holderStreetAddress);
            WriteAttribute(writer, "_HolderCity", m_holderCity);
            WriteAttribute(writer, "_HolderState", m_holderState);
            WriteAttribute(writer, "_HolderPostalCode", m_holderPostalCode);
            WriteAttribute(writer, "AutomobileMakeDescription", m_automobileMakeDescription);
            WriteAttribute(writer, "AutomobileModelYear", m_automobileModelYear);
            WriteAttribute(writer, "LifeInsuranceFaceValueAmount", m_lifeInsuranceFaceValueAmount);
            WriteAttribute(writer, "OtherAssetTypeDescription", m_otherAssetTypeDescription);
            WriteAttribute(writer, "StockBondMutualFundShareCount", m_stockBondMutualFundShareCount);
            writer.WriteEndElement(); // </ASSET>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion

	}
    public enum E_XEAssetType 
    {
        Undefined = 0,
        Automobile,
        Bond,
        BridgeLoanNotDeposited,
        CashOnHand,
        CertificateOfDepositTimeDeposit,
        CheckingAccount,
        EarnestMoneyCashDepositTowardPurchase,
        GiftsTotal,
        GiftsNotDeposited,
        LifeInsurance,
        MoneyMarketFund,
        MutualFund,
        NetWorthOfBusinessOwned,
        OtherLiquidAssets,
        OtherNonLiquidAssets,
        PendingNetSaleProceedsFromRealEstateAssets,
        RelocationMoney,
        RetirementFund,
        SaleOtherAssets,
        SavingsAccount,
        SecuredBorrowedFundsNotDeposited,
        Stock,
        TrustAccount
    }

}
