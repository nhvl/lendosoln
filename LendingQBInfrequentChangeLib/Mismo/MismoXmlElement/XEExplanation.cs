/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEExplanation : Mismo.Common.AbstractXmlNode
    {
        public XEExplanation()
        {
        }
        #region Schema
        //  <xs:element name="_EXPLANATION">
        //    <xs:complexType>
        //      <xs:attribute name="_Description" type="xs:string"/>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="AlimonyChildSupport"/>
        //            <xs:enumeration value="BorrowedDownPayment"/>
        //            <xs:enumeration value="CoMakerEndorserOnNote"/>
        //            <xs:enumeration value="DeclaredBankruptcyPastSevenYears"/>
        //            <xs:enumeration value="DelinquencyOrDefault"/>
        //            <xs:enumeration value="DirectIndirectForeclosedPropertyPastSevenYears"/>
        //            <xs:enumeration value="ObligatedOnLoanForeclosedOrDeedInLieuOfJudgement"/>
        //            <xs:enumeration value="OutstandingJudgments"/>
        //            <xs:enumeration value="PartyToLawsuit"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_description;
        private E_ExplanationType m_type;
        #endregion

        #region Public Properties
        public string Description 
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public E_ExplanationType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_EXPLANATION");
            WriteAttribute(writer, "_Description", m_description);
            WriteAttribute(writer, "_Type", m_type);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ExplanationType 
    {
        Undefined = 0,
        AlimonyChildSupport,
        BorrowedDownPayment,
        CoMakerEndorserOnNote,
        DeclaredBankruptcyPastSevenYears,
        DelinquencyOrDefault,
        DirectIndirectForeclosedPropertyPastSevenYears,
        ObligatedOnLoanForeclosedOrDeedInLieuOfJudgement,
        OutstandingJudgments,
        PartyToLawsuit

    }
}
