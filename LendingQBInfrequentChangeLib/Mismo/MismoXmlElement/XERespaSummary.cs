/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XERespaSummary : Mismo.Common.AbstractXmlNode
    {
        public XERespaSummary()
        {
        }
        #region Schema
        //  <xs:element name="RESPA_SUMMARY">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_TOTAL_FEES_PAID_TO" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="_TOTAL_FEES_PAID_BY" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_DisclosedTotalSalesPriceAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalAmountFinancedAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalAPRFeesAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalNonAPRFeesAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalPaidToOthersAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalDepositedReservesAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalFinanceChargeAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalNetBorrowerFeesAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalNetProceedsForFundingAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalNetSellerFeesAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalOfAllPaymentsAmount" type="xs:string"/>
        //      <xs:attribute name="_TotalPrepaidFinanceChargeAmount" type="xs:string"/>
        //      <xs:attribute name="APR" type="xs:string"/>
        //      <xs:attribute name="_TotalFilingRecordingFeeAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private ArrayList m_totalFeesPaidToList = new ArrayList();
        private ArrayList m_totalFeesPaidByList = new ArrayList();
        private string m_disclosedTotalSalesPriceAmount;
        private string m_totalAmountFinancedAmount;
        private string m_totalAPRFeesAmount;
        private string m_totalNonAPRFeesAmount;
        private string m_totalPaidToOthersAmount;
        private string m_totalDepositedReservesAmount;
        private string m_totalFinanceChargeAmount;
        private string m_totalNetBorrowerFeesAmount;
        private string m_totalNetProceedsForFundingAmount;
        private string m_totalNetSellerFeesAmount;
        private string m_totalOfAllPaymentsAmount;
        private string m_totalPrepaidFinanceChargeAmount;
        private string m_apr;
        private string m_totalFilingRecordingFeeAmount;
        #endregion

        #region Public Properties
        public void AddTotalFeesPaidTo(XETotalFeesPaidTo totalFeesPaidTo) 
        {
            if (null == totalFeesPaidTo)
                return;

            m_totalFeesPaidToList.Add(totalFeesPaidTo);
        }
        public void AddTotalFeesPaidBy(XETotalFeesPaidBy totalFeesPaidBy) 
        {
            if (null == totalFeesPaidBy) 
                return;

            m_totalFeesPaidToList.Add(totalFeesPaidBy);

        }
        public string DisclosedTotalSalesPriceAmount 
        {
            get { return m_disclosedTotalSalesPriceAmount; }
            set { m_disclosedTotalSalesPriceAmount = value; }
        }
        public string TotalAmountFinancedAmount 
        {
            get { return m_totalAmountFinancedAmount; }
            set { m_totalAmountFinancedAmount = value; }
        }
        public string TotalAPRFeesAmount 
        {
            get { return m_totalAPRFeesAmount; }
            set { m_totalAPRFeesAmount = value; }
        }
        public string TotalNonAPRFeesAmount 
        {
            get { return m_totalNonAPRFeesAmount; }
            set { m_totalNonAPRFeesAmount = value; }
        }
        public string TotalPaidToOthersAmount 
        {
            get { return m_totalPaidToOthersAmount; }
            set { m_totalPaidToOthersAmount = value; }
        }
        public string TotalDepositedReservesAmount 
        {
            get { return m_totalDepositedReservesAmount; }
            set { m_totalDepositedReservesAmount = value; }
        }
        public string TotalFinanceChargeAmount 
        {
            get { return m_totalFinanceChargeAmount; }
            set { m_totalFinanceChargeAmount = value; }
        }
        public string TotalNetBorrowerFeesAmount 
        {
            get { return m_totalNetBorrowerFeesAmount; }
            set { m_totalNetBorrowerFeesAmount = value; }
        }
        public string TotalNetProceedsForFundingAmount 
        {
            get { return m_totalNetProceedsForFundingAmount; }
            set { m_totalNetProceedsForFundingAmount = value; }
        }
        public string TotalNetSellerFeesAmount 
        {
            get { return m_totalNetSellerFeesAmount; }
            set { m_totalNetSellerFeesAmount = value; }
        }
        public string TotalOfAllPaymentsAmount 
        {
            get { return m_totalOfAllPaymentsAmount; }
            set { m_totalOfAllPaymentsAmount = value; }
        }
        public string TotalPrepaidFinanceChargeAmount 
        {
            get { return m_totalPrepaidFinanceChargeAmount; }
            set { m_totalPrepaidFinanceChargeAmount = value; }
        }
        public string Apr 
        {
            get { return m_apr; }
            set { m_apr = value; }
        }
        public string TotalFilingRecordingFeeAmount 
        {
            get { return m_totalFilingRecordingFeeAmount; }
            set { m_totalFilingRecordingFeeAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPA_SUMMARY");
            WriteAttribute(writer, "_DisclosedTotalSalesPriceAmount", m_disclosedTotalSalesPriceAmount);
            WriteAttribute(writer, "_TotalAmountFinancedAmount", m_totalAmountFinancedAmount);
            WriteAttribute(writer, "_TotalAPRFeesAmount", m_totalAPRFeesAmount);
            WriteAttribute(writer, "_TotalNonAPRFeesAmount", m_totalNonAPRFeesAmount);
            WriteAttribute(writer, "_TotalPaidToOthersAmount", m_totalPaidToOthersAmount);
            WriteAttribute(writer, "_TotalDepositedReservesAmount", m_totalDepositedReservesAmount);
            WriteAttribute(writer, "_TotalFinanceChargeAmount", m_totalFinanceChargeAmount);
            WriteAttribute(writer, "_TotalNetBorrowerFeesAmount", m_totalNetBorrowerFeesAmount);
            WriteAttribute(writer, "_TotalNetProceedsForFundingAmount", m_totalNetProceedsForFundingAmount);
            WriteAttribute(writer, "_TotalNetSellerFeesAmount", m_totalNetSellerFeesAmount);
            WriteAttribute(writer, "_TotalOfAllPaymentsAmount", m_totalOfAllPaymentsAmount);
            WriteAttribute(writer, "_TotalPrepaidFinanceChargeAmount", m_totalPrepaidFinanceChargeAmount);
            WriteAttribute(writer, "APR", m_apr);
            WriteAttribute(writer, "_TotalFilingRecordingFeeAmount", m_totalFilingRecordingFeeAmount);
            WriteElement(writer, m_totalFeesPaidToList);
            WriteElement(writer, m_totalFeesPaidByList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
