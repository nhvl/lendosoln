/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.MismoXmlElement
{
	public class XEAppraiser : Mismo.Common.AbstractXmlNode
	{
		public XEAppraiser()
		{
		}
        #region Schema
        // <xs:element name="APPRAISER">
        //   <xs:complexType>
        //     <xs:attribute name="_Name" type="xs:string"/>
        //     <xs:attribute name="_CompanyName" type="xs:string"/>
        //     <xs:attribute name="_LicenseIdentifier" type="xs:string"/>
        //     <xs:attribute name="_LicenseState" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_name;
        private string m_companyName;
        private string m_licenseIdentifier;
        private string m_licenseState;
        #endregion

        #region Public Properties
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string CompanyName 
        {
            get { return m_companyName; }
            set { m_companyName = value; }
        }
        public string LicenseIdentifier 
        {
            get { return m_licenseIdentifier; }
            set { m_licenseIdentifier = value; }
        }
        public string LicenseState 
        {
            get { return m_licenseState; }
            set { m_licenseState = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("APPRAISER");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_CompanyName", m_companyName);
            WriteAttribute(writer, "_LicenseIdentifier", m_licenseIdentifier);
            WriteAttribute(writer, "_LicenseState", m_licenseState);
            writer.WriteEndElement(); // </APPRAISER>

        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
