/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XELegalDescription : Mismo.Common.AbstractXmlNode
    {
        public XELegalDescription()
        {
        }
        #region Schema
        //  <xs:element name="_LEGAL_DESCRIPTION">
        //    <xs:complexType>
        //      <xs:attribute name="_TextDescription" type="xs:string"/>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="MetesAndBounds"/>
        //            <xs:enumeration value="Other"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_textDescription;
        private E_LegalDescriptionType m_type;
        #endregion

        #region Public Properties
        public string TextDescription 
        {
            get { return m_textDescription; }
            set { m_textDescription = value; }
        }
        public E_LegalDescriptionType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_LEGAL_DESCRIPTION");
            WriteAttribute(writer, "_TextDescription", m_textDescription);
            WriteAttribute(writer, "_Type", m_type);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_LegalDescriptionType 
    {
        Undefined = 0,
        MetesAndBounds,
        Other
    }
}
