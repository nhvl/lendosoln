using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEContactDetail : Mismo.Common.AbstractXmlNode 
    {
        #region Schema
        //  <xs:element name="CONTACT_DETAIL">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="CONTACT_POINT" minOccurs="0" maxOccurs="unbounded"/>
        //          </xs:sequence>
        //          <xs:attribute name="_Name" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_contactPointList = new ArrayList();
        private string m_name;
        #endregion

        #region Public Properties
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public void AddContactPoint(XEContactPoint o) 
        {
            if (null == m_contactPointList)
                m_contactPointList = new ArrayList();

            m_contactPointList.Add(o);
        }
        public void AddWorkEmail(string email) 
        {
            XEContactPoint xe = new XEContactPoint();
            xe.Type = E_XEContactPointType.Email;
            xe.RoleType = E_XEContactPointRoleType.Work;
            xe.Value = email;
            AddContactPoint(xe);
        }
        public void AddWorkPhone(string phone) 
        {
            XEContactPoint xe = new XEContactPoint();
            xe.Type = E_XEContactPointType.Phone;
            xe.RoleType = E_XEContactPointRoleType.Work;
            xe.Value = phone;
            AddContactPoint(xe);

        }
        public void AddWorkFax(string fax) 
        {
            XEContactPoint xe = new XEContactPoint();
            xe.Type = E_XEContactPointType.Fax;
            xe.RoleType = E_XEContactPointRoleType.Work;
            xe.Value = fax;
            AddContactPoint(xe);

        }
        public string GetWorkEmail() 
        {
            if (null == m_contactPointList)
                return "";

            foreach (XEContactPoint o in m_contactPointList) 
            {
                if (o.Type == E_XEContactPointType.Email && o.RoleType == E_XEContactPointRoleType.Work)
                    return o.Value;
            }
            return "";
        }
        public string GetWorkPhone() 
        {
            if (null == m_contactPointList)
                return null;
            foreach (XEContactPoint o in m_contactPointList) 
            {
                if (o.Type == E_XEContactPointType.Phone && o.RoleType == E_XEContactPointRoleType.Work)
                    return o.Value;
            }
            return "";

        }
        public string GetWorkFax() 
        {
            if (null == m_contactPointList)
                return null;
            foreach (XEContactPoint o in m_contactPointList) 
            {
                if (o.Type == E_XEContactPointType.Fax && o.RoleType == E_XEContactPointRoleType.Work)
                    return o.Value;
            }
            return "";

        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("CONTACT_DETAIL");
            WriteAttribute(writer, "_Name", m_name);

            foreach (XEContactPoint o in m_contactPointList)
                o.GenerateXml(writer);

            writer.WriteEndElement(); // </CONTACT_DETAIL>
        }

        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_contactPointList = new ArrayList();
            XmlNodeList nodeList = el.SelectNodes("CONTACT_POINT");
            foreach (XmlElement o in nodeList) 
            {
                XEContactPoint xe = new XEContactPoint();
                xe.Parse(o);
                m_contactPointList.Add(xe);
            }

            m_name = el.GetAttribute("_Name");
        }
        #endregion
	}
}
