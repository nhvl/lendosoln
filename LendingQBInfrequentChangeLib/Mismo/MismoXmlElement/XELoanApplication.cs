/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XELoanApplication : Mismo.Common.AbstractXmlNode
    {
        public XELoanApplication()
        {
        }
        #region Schema
        //  <xs:element name="LOAN_APPLICATION">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_DATA_INFORMATION" minOccurs="0"/>
        //        <xs:element ref="ADDITIONAL_CASE_DATA" minOccurs="0"/>
        //        <xs:element ref="AFFORDABLE_LENDING" minOccurs="0"/>
        //        <xs:element ref="ASSET" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="DOWN_PAYMENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="GOVERNMENT_LOAN" minOccurs="0"/>
        //        <xs:element ref="GOVERNMENT_REPORTING" minOccurs="0"/>
        //        <xs:element ref="INTERVIEWER_INFORMATION" minOccurs="0"/>
        //        <xs:element ref="LIABILITY" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="LOAN_PRODUCT_DATA" minOccurs="0"/>
        //        <xs:element ref="LOAN_PURPOSE" minOccurs="0"/>
        //        <xs:element ref="LOAN_QUALIFICATION" minOccurs="0"/>
        //        <xs:element ref="MORTGAGE_TERMS" minOccurs="0"/>
        //        <xs:element ref="PROPERTY" minOccurs="0"/>
        //        <xs:element ref="PROPOSED_HOUSING_EXPENSE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="REO_PROPERTY" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="TITLE_HOLDER" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="TRANSACTION_DETAIL" minOccurs="0"/>
        //        <xs:element ref="BORROWER" maxOccurs="unbounded"/>
        //        <xs:element ref="ESCROW" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RESPA_FEE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="MI_DATA" minOccurs="0"/>
        //        <xs:element ref="DISCLOSURE_DATA" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="MISMOVersionID" type="xs:string" fixed="2.3.1"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_mismoVersionId = "2.3.1";
        private XEDataInformation m_dataInformation;
        private XEAdditionalCaseData m_additionalCaseData;
        private XEAffordableLending m_affordableLending;
        private ArrayList m_assetList = new ArrayList();
        private ArrayList m_downPaymentList = new ArrayList();
        private XEGovernmentLoan m_governmentLoan;
        private XEGovernmentReporting m_governmentReporting;
        private XEInterviewerInformation m_interviewerInformation;
        private ArrayList m_liabilityList = new ArrayList();
        private XELoanProductData m_loanProductData;
        private XELoanPurpose m_loanPurpose;
        private XELoanQualification m_loanQualification;
        private XEMortgageTerms m_mortgageTerms;
        private XEProperty m_property;
        private ArrayList m_proposedHousingExpenseList = new ArrayList();
        private ArrayList m_reoPropertyList = new ArrayList();
        private ArrayList m_titleHolderList = new ArrayList();
        private XETransactionDetail m_transactionDetail;
        private ArrayList m_borrowerList = new ArrayList();
        private ArrayList m_escrowList = new ArrayList();
        private ArrayList m_respaFeeList = new ArrayList();
        private XEMIData m_miData;
        private XEDisclosureData m_disclosureData;
        #endregion

        #region Public Properties
        public void SetDataInformation(XEDataInformation dataInformation) 
        {
            m_dataInformation = dataInformation;
        }
        public void SetAdditionalCaseData(XEAdditionalCaseData additionalCaseData) 
        {
            m_additionalCaseData = additionalCaseData;
        }
        public void SetAffordableLending(XEAffordableLending affordableLending) 
        {
            m_affordableLending = affordableLending;
        }
        public void AddAsset(XEAsset asset) 
        {
            if (null == asset)
                return;
            m_assetList.Add(asset);
        }
        public void AddDownPayment(XEDownPayment downPayment) 
        {
            if (null == downPayment)
                return;
            m_downPaymentList.Add(downPayment);
        }
        public void SetGovernmentLoan(XEGovernmentLoan governmentLoan) 
        {
            m_governmentLoan = governmentLoan;
        }
        public void SetGovernmentReporting(XEGovernmentReporting governmentReporting) 
        {
            m_governmentReporting = governmentReporting;
        }
        public void SetInterviewerInformation(XEInterviewerInformation interviewerInformation) 
        {
            m_interviewerInformation = interviewerInformation;
        }
        public void AddLiability(XELiability liability) 
        {
            if (null == liability)
                return;
            m_liabilityList.Add(liability);
        }
        public void SetLoanProductData(XELoanProductData loanProductData) 
        {
            m_loanProductData = loanProductData;
        }
        public void SetLoanPurpose(XELoanPurpose loanPurpose) 
        {
            m_loanPurpose = loanPurpose;
        }
        public void SetLoanQualification(XELoanQualification loanQualification) 
        {
            m_loanQualification = loanQualification;
        }
        public void SetMortgageTerms(XEMortgageTerms mortgageTerms) 
        {
            m_mortgageTerms = mortgageTerms;
        }
        public void SetProperty(XEProperty property) 
        {
            m_property = property;
        }
        public void AddProposedHousingExpense(XEProposedHousingExpense proposedHousingExpense) 
        {
            if (null == proposedHousingExpense)
                return;
            m_proposedHousingExpenseList.Add(proposedHousingExpense);
        }
        public void AddReoProperty(XEReoProperty reoProperty) 
        {
            if (null == reoProperty)
                return;
            m_reoPropertyList.Add(reoProperty);
        }
        public void AddTitleHolder(XETitleHolder titleHolder) 
        {
            if (null == titleHolder)
                return;
            m_titleHolderList.Add(titleHolder);
        }
        public void SetTransactionDetail(XETransactionDetail transactionDetail) 
        {
            m_transactionDetail = transactionDetail;
        }
        public void AddBorrower(XEBorrower borrower) 
        {
            if (null == borrower)
                return;
            m_borrowerList.Add(borrower);
        }
        public void AddEscrow(XEEscrow escrow) 
        {
            if (null == escrow)
                return;
            m_escrowList.Add(escrow);
        }
        public void AddRespaFee(XERespaFee respaFee) 
        {
            if (null == respaFee)
                return;
            m_respaFeeList.Add(respaFee);
        }
        public void SetMiData(XEMIData miData) 
        {
            m_miData = miData;
        }
        public void SetDisclosureData(XEDisclosureData disclosureData) 
        {
            m_disclosureData = disclosureData;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_APPLICATION");
            WriteAttribute(writer, "MISMOVersionID", m_mismoVersionId);
            WriteElement(writer, m_dataInformation);
            WriteElement(writer, m_additionalCaseData);
            WriteElement(writer, m_affordableLending);
            WriteElement(writer, m_assetList);
            WriteElement(writer, m_downPaymentList);
            WriteElement(writer, m_governmentLoan);
            WriteElement(writer, m_governmentReporting);
            WriteElement(writer, m_interviewerInformation);
            WriteElement(writer, m_liabilityList);
            WriteElement(writer, m_loanProductData);
            WriteElement(writer, m_loanPurpose);
            WriteElement(writer, m_loanQualification);
            WriteElement(writer, m_mortgageTerms);
            WriteElement(writer, m_property);
            WriteElement(writer, m_proposedHousingExpenseList);
            WriteElement(writer, m_reoPropertyList);
            WriteElement(writer, m_titleHolderList);
            WriteElement(writer, m_transactionDetail);
            WriteElement(writer, m_borrowerList);
            WriteElement(writer, m_escrowList);
            WriteElement(writer, m_respaFeeList);
            WriteElement(writer, m_miData);
            WriteElement(writer, m_disclosureData);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
