/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XELoanProductData : Mismo.Common.AbstractXmlNode
    {
        public XELoanProductData()
        {
        }
        #region Schema
        //  <xs:element name="LOAN_PRODUCT_DATA">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="ARM" minOccurs="0"/>
        //        <xs:element ref="BUYDOWN" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="LOAN_FEATURES" minOccurs="0"/>
        //        <xs:element ref="PAYMENT_ADJUSTMENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RATE_ADJUSTMENT" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private XEArm m_arm;
        private ArrayList m_buydownList = new ArrayList();
        private XELoanFeatures m_loanFeatures;
        private ArrayList m_paymentAdjustmentList = new ArrayList();
        private ArrayList m_rateAdjustmentList = new ArrayList();
        #endregion

        #region Public Properties
        public void SetArm(XEArm arm) 
        {
            m_arm = arm;
        }
        public void AddBuydown(XEBuyDown buydown) 
        {
            if (null == buydown)

                return;
            m_buydownList.Add(buydown);
        }
        public void SetLoanFeatures(XELoanFeatures loanFeatures) 
        {
            m_loanFeatures = loanFeatures;
        }
        public void AddPaymentAdjustment(XEPaymentAdjustment paymentAdjustment) 
        {
            if (null == paymentAdjustment) 
                return;
            m_paymentAdjustmentList.Add(paymentAdjustment);
        }
        public void AddRateAdjustment(XERateAdjustment rateAdjustment) 
        {
            if (null == rateAdjustment)
                return;

            m_rateAdjustmentList.Add(rateAdjustment);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_PRODUCT_DATA");
            WriteElement(writer, m_arm);
            WriteElement(writer, m_buydownList);
            WriteElement(writer, m_loanFeatures);
            WriteElement(writer, m_paymentAdjustmentList);
            WriteElement(writer, m_rateAdjustmentList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
