/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEHmdaRace : Mismo.Common.AbstractXmlNode
    {
        public XEHmdaRace()
        {
        }
        #region Schema
        // <xs:element name="HMDA_RACE">
        //   <xs:complexType>
        //     <xs:attribute name="_Type">
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private E_HmdaRaceType m_type;
        #endregion

        #region Public Properties
        public E_HmdaRaceType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("HMDA_RACE");
            WriteAttribute(writer, "_Type", m_type);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_HmdaRaceType 
    {
        Undefined = 0,
        AmericanIndianOrAlaskaNative,
        Asian,
        BlackOrAfricanAmerican,
        NativeHawaiianOrOtherPacificIslander,
        White,
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        NotApplicable
    }
}
