/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.MismoXmlElement
{
	public class XEArm : Mismo.Common.AbstractXmlNode
	{
		public XEArm()
		{
		}
        #region Schema
        // <xs:element name="ARM">
        //   <xs:complexType>
        //     <xs:attribute name="_IndexCurrentValuePercent" type="xs:string"/>
        //     <xs:attribute name="_IndexMarginPercent" type="xs:string"/>
        //     <xs:attribute name="_IndexType">
        //     <xs:attribute name="_QualifyingRatePercent" type="xs:string"/>
        //     <xs:attribute name="PaymentAdjustmentLifetimeCapAmount" type="xs:string"/>
        //     <xs:attribute name="PaymentAdjustmentLifetimeCapPercent" type="xs:string"/>
        //     <xs:attribute name="RateAdjustmentLifetimeCapPercent" type="xs:string"/>
        //     <xs:attribute name="_ConversionOptionIndicator">
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_indexCurrentValuePercent;
        private string m_indexMarginPercent;
        private E_ArmIndexType m_indexType;
        private string m_qualifyingRatePercent;
        private string m_paymentAdjustmentLifetimeCapAmount;
        private string m_paymentAdjustmentLifetimeCapPercent;
        private string m_rateAdjustmentLifetimeCapPercent;
        private string m_conversionOptionIndicator;
        #endregion

        #region Public Properties
        public string IndexCurrentValuePercent 
        {
            get { return m_indexCurrentValuePercent; }
            set { m_indexCurrentValuePercent = value; }
        }
        public string IndexMarginPercent 
        {
            get { return m_indexMarginPercent; }
            set { m_indexMarginPercent = value; }
        }
        public E_ArmIndexType IndexType 
        {
            get { return m_indexType; }
            set { m_indexType = value; }
        }
        public string QualifyingRatePercent 
        {
            get { return m_qualifyingRatePercent; }
            set { m_qualifyingRatePercent = value; }
        }
        public string PaymentAdjustmentLifetimeCapAmount 
        {
            get { return m_paymentAdjustmentLifetimeCapAmount; }
            set { m_paymentAdjustmentLifetimeCapAmount = value; }
        }
        public string PaymentAdjustmentLifetimeCapPercent 
        {
            get { return m_paymentAdjustmentLifetimeCapPercent; }
            set { m_paymentAdjustmentLifetimeCapPercent = value; }
        }
        public string RateAdjustmentLifetimeCapPercent 
        {
            get { return m_rateAdjustmentLifetimeCapPercent; }
            set { m_rateAdjustmentLifetimeCapPercent = value; }
        }
        public string ConversionOptionIndicator 
        {
            get { return m_conversionOptionIndicator; }
            set { m_conversionOptionIndicator = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ARM");
            WriteAttribute(writer, "_IndexCurrentValuePercent", m_indexCurrentValuePercent);
            WriteAttribute(writer, "_IndexMarginPercent", m_indexMarginPercent);
            WriteAttribute(writer, "_IndexType", m_indexType);
            WriteAttribute(writer, "_QualifyingRatePercent", m_qualifyingRatePercent);
            WriteAttribute(writer, "PaymentAdjustmentLifetimeCapAmount", m_paymentAdjustmentLifetimeCapAmount);
            WriteAttribute(writer, "PaymentAdjustmentLifetimeCapPercent", m_paymentAdjustmentLifetimeCapPercent);
            WriteAttribute(writer, "RateAdjustmentLifetimeCapPercent", m_rateAdjustmentLifetimeCapPercent);
            WriteAttribute(writer, "_ConversionOptionIndicator", m_conversionOptionIndicator);
            writer.WriteEndElement(); // </ARM>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_ArmIndexType 
    {
        Undefined = 0,
        EleventhDistrictCostOfFunds,
        OneYearTreasury,
        ThreeYearTreasury,
        SixMonthTreasury,
        DailyCertificateOfDepositRate,
        FNM60DayRequiredNetYield,
        FNM_LIBOR,
        FederalCostOfFunds,
        FRE60DayRequiredNetYield,
        FRE_LIBOR,
        LIBOR,
        MonthlyAverageConstantMaturingTreasury,
        NationalAverageContractRateFHLBB,
        NationalMonthlyMedianCostOfFunds,
        Other,
        TreasuryBillDailyValue,
        WallStreetJournalLIBOR,
        WeeklyAverageCertificateOfDepositRate,
        WeeklyAverageConstantMaturingTreasury,
        WeeklyAveragePrimeRate,
        WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield,
        WeeklyAverageTreasuryAuctionAverageBondDiscountYield,
        WeeklyAverageTreasuryAuctionAverageInvestmentYield
    }
}
