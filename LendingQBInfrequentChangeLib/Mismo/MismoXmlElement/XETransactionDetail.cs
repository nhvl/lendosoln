/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XETransactionDetail : Mismo.Common.AbstractXmlNode
    {
        public XETransactionDetail()
        {
        }
        #region Schema
        //  <xs:element name="TRANSACTION_DETAIL">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="PURCHASE_CREDIT" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="AlterationsImprovementsAndRepairsAmount" type="xs:string"/>
        //      <xs:attribute name="BorrowerPaidDiscountPointsTotalAmount" type="xs:string"/>
        //      <xs:attribute name="EstimatedClosingCostsAmount" type="xs:string"/>
        //      <xs:attribute name="MIAndFundingFeeFinancedAmount" type="xs:string"/>
        //      <xs:attribute name="MIAndFundingFeeTotalAmount" type="xs:string"/>
        //      <xs:attribute name="PrepaidItemsEstimatedAmount" type="xs:string"/>
        //      <xs:attribute name="PurchasePriceAmount" type="xs:string"/>
        //      <xs:attribute name="RefinanceIncludingDebtsToBePaidOffAmount" type="xs:string"/>
        //      <xs:attribute name="SalesConcessionAmount" type="xs:string"/>
        //      <xs:attribute name="SellerPaidClosingCostsAmount" type="xs:string"/>
        //      <xs:attribute name="SubordinateLienAmount" type="xs:string"/>
        //      <xs:attribute name="SubordinateLienHELOCAmount" type="xs:string"/>
        //      <xs:attribute name="FREReserveAmount" type="xs:string"/>
        //      <xs:attribute name="FREReservesAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private ArrayList m_purchaseCreditList = new ArrayList();
        private string m_alterationsImprovementsAndRepairsAmount;
        private string m_borrowerPaidDiscountPointsTotalAmount;
        private string m_estimatedClosingCostsAmount;
        private string m_MIAndFundingFeeFinancedAmount;
        private string m_MIAndFundingFeeTotalAmount;
        private string m_prepaidItemsEstimatedAmount;
        private string m_purchasePriceAmount;
        private string m_refinanceIncludingDebtsToBePaidOffAmount;
        private string m_salesConcessionAmount;
        private string m_sellerPaidClosingCostsAmount;
        private string m_subordinateLienAmount;
        private string m_subordinateLienHELOCAmount;
        private string m_FREReserveAmount;
        private string m_FREReservesAmount;

        #endregion

        #region Public Properties
        public void AddPurchaseCredit(XEPurchaseCredit purchaseCredit) 
        {
            m_purchaseCreditList.Add(purchaseCredit);
        }
        public string AlterationsImprovementsAndRepairsAmount 
        {
            get { return m_alterationsImprovementsAndRepairsAmount; }
            set { m_alterationsImprovementsAndRepairsAmount = value; }
        }
        public string BorrowerPaidDiscountPointsTotalAmount 
        {
            get { return m_borrowerPaidDiscountPointsTotalAmount; }
            set { m_borrowerPaidDiscountPointsTotalAmount = value; }
        }
        public string EstimatedClosingCostsAmount 
        {
            get { return m_estimatedClosingCostsAmount; }
            set { m_estimatedClosingCostsAmount = value; }
        }
        public string MIAndFundingFeeFinancedAmount 
        {
            get { return m_MIAndFundingFeeFinancedAmount; }
            set { m_MIAndFundingFeeFinancedAmount = value; }
        }
        public string MIAndFundingFeeTotalAmount 
        {
            get { return m_MIAndFundingFeeTotalAmount; }
            set { m_MIAndFundingFeeTotalAmount = value; }
        }
        public string PrepaidItemsEstimatedAmount 
        {
            get { return m_prepaidItemsEstimatedAmount; }
            set { m_prepaidItemsEstimatedAmount = value; }
        }
        public string PurchasePriceAmount 
        {
            get { return m_purchasePriceAmount; }
            set { m_purchasePriceAmount = value; }
        }
        public string RefinanceIncludingDebtsToBePaidOffAmount 
        {
            get { return m_refinanceIncludingDebtsToBePaidOffAmount; }
            set { m_refinanceIncludingDebtsToBePaidOffAmount = value; }
        }
        public string SalesConcessionAmount 
        {
            get { return m_salesConcessionAmount; }
            set { m_salesConcessionAmount = value; }
        }
        public string SellerPaidClosingCostsAmount 
        {
            get { return m_sellerPaidClosingCostsAmount; }
            set { m_sellerPaidClosingCostsAmount = value; }
        }
        public string SubordinateLienAmount 
        {
            get { return m_subordinateLienAmount; }
            set { m_subordinateLienAmount = value; }
        }
        public string SubordinateLienHELOCAmount 
        {
            get { return m_subordinateLienHELOCAmount; }
            set { m_subordinateLienHELOCAmount = value; }
        }
        public string FREReserveAmount 
        {
            get { return m_FREReserveAmount; }
            set { m_FREReserveAmount = value; }
        }
        public string FREReservesAmount 
        {
            get { return m_FREReservesAmount; }
            set { m_FREReservesAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("TRANSACTION_DETAIL");
            WriteAttribute(writer, "AlterationsImprovementsAndRepairsAmount", m_alterationsImprovementsAndRepairsAmount);
            WriteAttribute(writer, "BorrowerPaidDiscountPointsTotalAmount", m_borrowerPaidDiscountPointsTotalAmount);
            WriteAttribute(writer, "EstimatedClosingCostsAmount", m_estimatedClosingCostsAmount);
            WriteAttribute(writer, "MIAndFundingFeeFinancedAmount", m_MIAndFundingFeeFinancedAmount);
            WriteAttribute(writer, "MIAndFundingFeeTotalAmount", m_MIAndFundingFeeTotalAmount);
            WriteAttribute(writer, "PrepaidItemsEstimatedAmount", m_prepaidItemsEstimatedAmount);
            WriteAttribute(writer, "PurchasePriceAmount", m_purchasePriceAmount);
            WriteAttribute(writer, "RefinanceIncludingDebtsToBePaidOffAmount", m_refinanceIncludingDebtsToBePaidOffAmount);
            WriteAttribute(writer, "SalesConcessionAmount", m_salesConcessionAmount);
            WriteAttribute(writer, "SellerPaidClosingCostsAmount", m_sellerPaidClosingCostsAmount);
            WriteAttribute(writer, "SubordinateLienAmount", m_subordinateLienAmount);
            WriteAttribute(writer, "SubordinateLienHELOCAmount", m_subordinateLienHELOCAmount);
            WriteAttribute(writer, "FREReserveAmount", m_FREReserveAmount);
            WriteAttribute(writer, "FREReservesAmount", m_FREReservesAmount);

            WriteElement(writer, m_purchaseCreditList);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
