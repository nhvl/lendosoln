/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEResidence : Mismo.Common.AbstractXmlNode
	{
		public XEResidence()
		{
		}
        #region Schema
        //  <xs:element name="_RESIDENCE">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="LANDLORD" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="_Country" type="xs:string"/>
        //      <xs:attribute name="BorrowerResidencyBasisType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="LivingRentFree"/>
        //            <xs:enumeration value="Own"/>
        //            <xs:enumeration value="Rent"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BorrowerResidencyDurationMonths" type="xs:string"/>
        //      <xs:attribute name="BorrowerResidencyDurationYears" type="xs:string"/>
        //      <xs:attribute name="BorrowerResidencyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Current"/>
        //            <xs:enumeration value="Prior"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private XELandlord m_landLord;
        private string m_streetAddress;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_country;
        private E_ResidenceBorrowerResidencyBasisType m_borrowerResidencyBasisType;
        private string m_borrowerResidencyDurationMonths;
        private string m_borrowerResidencyDurationYears;
        private E_ResidenceBorrowerResidencyType m_borrowerResidencyType;
        #endregion

        #region Public Properties
        public void SetLandLord(XELandlord landLord) 
        {
            m_landLord = landLord;
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string Country 
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public E_ResidenceBorrowerResidencyBasisType BorrowerResidencyBasisType 
        {
            get { return m_borrowerResidencyBasisType; }
            set { m_borrowerResidencyBasisType = value; }
        }
        public string BorrowerResidencyDurationMonths 
        {
            get { return m_borrowerResidencyDurationMonths; }
            set { m_borrowerResidencyDurationMonths = value; }
        }
        public string BorrowerResidencyDurationYears 
        {
            get { return m_borrowerResidencyDurationYears; }
            set { m_borrowerResidencyDurationYears = value; }
        }
        public E_ResidenceBorrowerResidencyType BorrowerResidencyType 
        {
            get { return m_borrowerResidencyType; }
            set { m_borrowerResidencyType = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_RESIDENCE");
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "BorrowerResidencyBasisType", m_borrowerResidencyBasisType);
            WriteAttribute(writer, "BorrowerResidencyDurationMonths", m_borrowerResidencyDurationMonths);
            WriteAttribute(writer, "BorrowerResidencyDurationYears", m_borrowerResidencyDurationYears);
            WriteAttribute(writer, "BorrowerResidencyType", m_borrowerResidencyType);
            WriteElement(writer, m_landLord);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_ResidenceBorrowerResidencyBasisType 
    {
        Undefined = 0,
        LivingRentFree,
        Own,
        Rent
    }
    public enum E_ResidenceBorrowerResidencyType 
    {
        Undefined = 0,
        Current,
        Prior
    }
}
