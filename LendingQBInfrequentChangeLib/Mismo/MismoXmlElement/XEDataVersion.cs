/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEDataVersion : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="DATA_VERSION">
        //     <xs:complexType>
        //       <xs:attribute name="_Name" type="xs:string" use="required"/>
        //       <xs:attribute name="_Number" type="xs:string" use="required"/>
        //     </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_name;
        private string m_number;
        #endregion

        #region Public Properties
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string Number 
        {
            get { return m_number; }
            set { m_number = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("DATA_VERSION");
            WriteRequiredAttribute(writer, "_Name", m_name);
            WriteRequiredAttribute(writer, "_Number", m_number);
            writer.WriteEndElement(); // </DATA_VERSION>
        }

        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
		public XEDataVersion()
		{
		}
	}
}
