/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEPaymentDetails : Mismo.Common.AbstractXmlNode
	{
		public XEPaymentDetails()
		{
		}
        #region Schema
        //  <xs:element name="PAYMENT_DETAILS">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="PAYMENT_SCHEDULE" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private ArrayList m_paymentScheduleList = new ArrayList();
        #endregion

        #region Public Properties
        #endregion
        public void AddPaymentSchedule(XEPaymentSchedule paymentSchedule) 
        {
            m_paymentScheduleList.Add(paymentSchedule);
        }

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PAYMENT_DETAILS");
            foreach (XEPaymentSchedule paymentSchedule in m_paymentScheduleList) 
            {
                paymentSchedule.GenerateXml(writer);
            }
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
