/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XESummary : Mismo.Common.AbstractXmlNode
    {
        public XESummary()
        {
        }
        #region Schema
        //  <xs:element name="SUMMARY">
        //    <xs:complexType>
        //      <xs:attribute name="_Amount" type="xs:string"/>
        //      <xs:attribute name="_AmountType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="TotalMonthlyIncomeNotIncludingNetRentalIncome"/>
        //            <xs:enumeration value="SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance"/>
        //            <xs:enumeration value="TotalPresentHousingExpense"/>
        //            <xs:enumeration value="TotalLiabilitesBalance"/>
        //            <xs:enumeration value="SubtotalLiabilitesMonthlyPayment"/>
        //            <xs:enumeration value="SubtotalOmittedLiabilitesBalance"/>
        //            <xs:enumeration value="SubtotalOmittedLiabilitiesMonthlyPayment"/>
        //            <xs:enumeration value="SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment"/>
        //            <xs:enumeration value="SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty"/>
        //            <xs:enumeration value="SubtotalSubjectPropertyLiensPaidByClosingBalance"/>
        //            <xs:enumeration value="SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment"/>
        //            <xs:enumeration value="SubtotalLiabilitiesForRentalPropertyBalance"/>
        //            <xs:enumeration value="SubtotalLiabilitiesForRentalPropertyMonthlyPayment"/>
        //            <xs:enumeration value="SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty"/>
        //            <xs:enumeration value="SubtotalLiquidAssetsNotIncludingGift"/>
        //            <xs:enumeration value="SubtotalNonLiquidAssets"/>
        //            <xs:enumeration value="TotalLiabilitiesBalance"/>
        //            <xs:enumeration value="SubtotalLiabilitiesMonthlyPayment"/>
        //            <xs:enumeration value="SubtotalOmittedLiabilitiesBalance"/>
        //            <xs:enumeration value="SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty"/>
        //            <xs:enumeration value="UndrawnHELOC"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_amount;
        private E_SummaryAmountType m_amountType;
        #endregion

        #region Public Properties
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public E_SummaryAmountType AmountType 
        {
            get { return m_amountType; }
            set { m_amountType = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("SUMMARY");
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_AmountType", m_amountType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_SummaryAmountType 
    {
        Undefined = 0,
        TotalMonthlyIncomeNotIncludingNetRentalIncome,
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance,
        TotalPresentHousingExpense,
        TotalLiabilitesBalance,
        SubtotalLiabilitesMonthlyPayment,
        SubtotalOmittedLiabilitesBalance,
        SubtotalOmittedLiabilitiesMonthlyPayment,
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment,
        SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty,
        SubtotalSubjectPropertyLiensPaidByClosingBalance,
        SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment,
        SubtotalLiabilitiesForRentalPropertyBalance,
        SubtotalLiabilitiesForRentalPropertyMonthlyPayment,
        SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty,
        SubtotalLiquidAssetsNotIncludingGift,
        SubtotalNonLiquidAssets,
        TotalLiabilitiesBalance,
        SubtotalLiabilitiesMonthlyPayment,
        SubtotalOmittedLiabilitiesBalance,
        SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty,
        UndrawnHELOC
    }
}
