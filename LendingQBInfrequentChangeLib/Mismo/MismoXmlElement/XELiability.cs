/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XELiability : Mismo.Common.AbstractXmlNode
    {
        public XELiability()
        {
        }
        #region Schema
        // <xs:element name="LIABILITY">
        //   <xs:complexType>
        //     <xs:attribute name="_ID" type="xs:ID"/>
        //     <xs:attribute name="BorrowerID" type="xs:IDREFS"/>
        //     <xs:attribute name="REO_ID" type="xs:IDREF"/>
        //     <xs:attribute name="_HolderStreetAddress" type="xs:string"/>
        //     <xs:attribute name="_HolderCity" type="xs:string"/>
        //     <xs:attribute name="_HolderState" type="xs:string"/>
        //     <xs:attribute name="_HolderPostalCode" type="xs:string"/>
        //     <xs:attribute name="AlimonyOwedToName" type="xs:string"/>
        //     <xs:attribute name="_AccountIdentifier" type="xs:string"/>
        //     <xs:attribute name="_ExclusionIndicator">
        //     <xs:attribute name="_HolderName" type="xs:string"/>
        //     <xs:attribute name="_MonthlyPaymentAmount" type="xs:string"/>
        //     <xs:attribute name="_PayoffStatusIndicator">
        //     <xs:attribute name="_PayoffWithCurrentAssetsIndicator">
        //     <xs:attribute name="_RemainingTermMonths" type="xs:string"/>
        //     <xs:attribute name="_Type">
        //     <xs:attribute name="_UnpaidBalanceAmount" type="xs:string"/>
        //     <xs:attribute name="SubjectLoanResubordinationIndicator">
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_id;
        private string m_borrowerId;
        private string m_reoId;
        private string m_holderStreetAddress;
        private string m_holderCity;
        private string m_holderState;
        private string m_holderPostalCode;
        private string m_alimonyOwedToName;
        private string m_accountIdentifier;
        private E_YesNoIndicator m_exclusionIndicator;
        private string m_holderName;
        private string m_monthlyPaymentAmount;
        private E_YesNoIndicator m_payoffStatusIndicator;
        private E_YesNoIndicator m_payoffWithCurrentAssetsIndicator;
        private string m_remainingTermMonths;
        private E_LiabilityType m_type;
        private string m_unpaidBalanceAmount;
        private E_YesNoIndicator m_subjectLoanResubordinationIndicator;
        #endregion

        #region Public Properties
        public string Id 
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string BorrowerId 
        {
            get { return m_borrowerId; }
            set { m_borrowerId = value; }
        }
        public string ReoId 
        {
            get { return m_reoId; }
            set { m_reoId = value; }
        }
        public string HolderStreetAddress 
        {
            get { return m_holderStreetAddress; }
            set { m_holderStreetAddress = value; }
        }
        public string HolderCity 
        {
            get { return m_holderCity; }
            set { m_holderCity = value; }
        }
        public string HolderState 
        {
            get { return m_holderState; }
            set { m_holderState = value; }
        }
        public string HolderPostalCode 
        {
            get { return m_holderPostalCode; }
            set { m_holderPostalCode = value; }
        }
        public string AlimonyOwedToName 
        {
            get { return m_alimonyOwedToName; }
            set { m_alimonyOwedToName = value; }
        }
        public string AccountIdentifier 
        {
            get { return m_accountIdentifier; }
            set { m_accountIdentifier = value; }
        }
        public E_YesNoIndicator ExclusionIndicator 
        {
            get { return m_exclusionIndicator; }
            set { m_exclusionIndicator = value; }
        }
        public string HolderName 
        {
            get { return m_holderName; }
            set { m_holderName = value; }
        }
        public string MonthlyPaymentAmount 
        {
            get { return m_monthlyPaymentAmount; }
            set { m_monthlyPaymentAmount = value; }
        }
        public E_YesNoIndicator PayoffStatusIndicator 
        {
            get { return m_payoffStatusIndicator; }
            set { m_payoffStatusIndicator = value; }
        }
        public E_YesNoIndicator PayoffWithCurrentAssetsIndicator 
        {
            get { return m_payoffWithCurrentAssetsIndicator; }
            set { m_payoffWithCurrentAssetsIndicator = value; }
        }
        public string RemainingTermMonths 
        {
            get { return m_remainingTermMonths; }
            set { m_remainingTermMonths = value; }
        }
        public E_LiabilityType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string UnpaidBalanceAmount 
        {
            get { return m_unpaidBalanceAmount; }
            set { m_unpaidBalanceAmount = value; }
        }
        public E_YesNoIndicator SubjectLoanResubordinationIndicator 
        {
            get { return m_subjectLoanResubordinationIndicator; }
            set { m_subjectLoanResubordinationIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LIABILITY");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "BorrowerID", m_borrowerId);
            WriteAttribute(writer, "REO_ID", m_reoId);
            WriteAttribute(writer, "_HolderStreetAddress", m_holderStreetAddress);
            WriteAttribute(writer, "_HolderCity", m_holderCity);
            WriteAttribute(writer, "_HolderState", m_holderState);
            WriteAttribute(writer, "_HolderPostalCode", m_holderPostalCode);
            WriteAttribute(writer, "AlimonyOwedToName", m_alimonyOwedToName);
            WriteAttribute(writer, "_AccountIdentifier", m_accountIdentifier);
            WriteAttribute(writer, "_ExclusionIndicator", m_exclusionIndicator);
            WriteAttribute(writer, "_HolderName", m_holderName);
            WriteAttribute(writer, "_MonthlyPaymentAmount", m_monthlyPaymentAmount);
            WriteAttribute(writer, "_PayoffStatusIndicator", m_payoffStatusIndicator);
            WriteAttribute(writer, "_PayoffWithCurrentAssetsIndicator", m_payoffWithCurrentAssetsIndicator);
            WriteAttribute(writer, "_RemainingTermMonths", m_remainingTermMonths);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_UnpaidBalanceAmount", m_unpaidBalanceAmount);
            WriteAttribute(writer, "SubjectLoanResubordinationIndicator", m_subjectLoanResubordinationIndicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_LiabilityType 
    {
        Undefined = 0,
        Alimony,
        ChildCare,
        ChildSupport,
        CollectionsJudgementsAndLiens,
        HELOC,
        Installment,
        JobRelatedExpenses,
        LeasePayments,
        MortgageLoan,
        Open30DayChargeAccount,
        OtherLiability,
        Revolving,
        SeparateMaintenanceExpense,
        OtherExpense,
        Taxes
    }
}
