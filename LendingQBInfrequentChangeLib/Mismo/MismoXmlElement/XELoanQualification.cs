/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XELoanQualification : Mismo.Common.AbstractXmlNode
    {
        public XELoanQualification()
        {
        }
        #region Schema
        //  <xs:element name="LOAN_QUALIFICATION">
        //    <xs:complexType>
        //      <xs:attribute name="AdditionalBorrowerAssetsNotConsideredIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="AdditionalBorrowerAssetsConsideredIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_additionalBorrowerAssetsNotConsideredIndicator;
        private E_YesNoIndicator m_additionalBorrowerAssetsConsideredIndicator;
        #endregion

        #region Public Properties
        public E_YesNoIndicator AdditionalBorrowerAssetsNotConsideredIndicator 
        {
            get { return m_additionalBorrowerAssetsNotConsideredIndicator; }
            set { m_additionalBorrowerAssetsNotConsideredIndicator = value; }
        }
        public E_YesNoIndicator AdditionalBorrowerAssetsConsideredIndicator 
        {
            get { return m_additionalBorrowerAssetsConsideredIndicator; }
            set { m_additionalBorrowerAssetsConsideredIndicator = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_QUALIFICATION");
            WriteAttribute(writer, "AdditionalBorrowerAssetsNotConsideredIndicator", m_additionalBorrowerAssetsNotConsideredIndicator);
            WriteAttribute(writer, "AdditionalBorrowerAssetsConsideredIndicator", m_additionalBorrowerAssetsConsideredIndicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
