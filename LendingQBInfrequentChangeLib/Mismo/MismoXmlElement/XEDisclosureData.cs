/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEDisclosureData : Mismo.Common.AbstractXmlNode
	{
		public XEDisclosureData()
		{
		}
        #region Schema
        //  <xs:element name="DISCLOSURE_DATA">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="PAYMENT_DETAILS" minOccurs="0"/>
        //        <xs:element ref="RESPA_SERVICING_DATA" minOccurs="0"/>
        //        <xs:element ref="RESPA_SUMMARY" minOccurs="0"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private XEPaymentDetails m_paymentDetails;
        private XERespaServicingData m_respaServicingData;
        private XERespaSummary m_respaSummary;
        #endregion

        #region Public Properties
        public void SetPaymentDetails(XEPaymentDetails paymentDetails) 
        {
            m_paymentDetails = paymentDetails;
        }
        public void SetRespaServicingData(XERespaServicingData respaServicingData) 
        {
            m_respaServicingData = respaServicingData;
        }
        public void SetRespaSummary(XERespaSummary respaSummary) 
        {
            m_respaSummary = respaSummary;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("DISCLOSURE_DATA");
            WriteElement(writer, m_paymentDetails);
            WriteElement(writer, m_respaServicingData);
            WriteElement(writer, m_respaSummary);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
