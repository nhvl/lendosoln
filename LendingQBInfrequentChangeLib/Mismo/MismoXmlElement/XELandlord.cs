/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XELandlord : Mismo.Common.AbstractXmlNode
	{
		public XELandlord()
		{
		}
        #region Schema
        // <xs:element name="LANDLORD">
        //   <xs:complexType>
        //     <xs:sequence>
        //       <xs:element ref="CONTACT_DETAIL" minOccurs="0"/>
        //     </xs:sequence>
        //     <xs:attribute name="_Name" type="xs:string"/>
        //     <xs:attribute name="_StreetAddress" type="xs:string"/>
        //     <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //     <xs:attribute name="_City" type="xs:string"/>
        //     <xs:attribute name="_State" type="xs:string"/>
        //     <xs:attribute name="_PostalCode" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private XEContactDetail m_contactDetail;
        private string m_name;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        #endregion

        #region Public Properties
        public XEContactDetail ContactDetail 
        {
            get { return m_contactDetail; }
            set { m_contactDetail = value; }
        }
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LANDLORD");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);

            if (null != m_contactDetail)
                m_contactDetail.GenerateXml(writer);

            writer.WriteEndElement(); // </LANDLORD>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
