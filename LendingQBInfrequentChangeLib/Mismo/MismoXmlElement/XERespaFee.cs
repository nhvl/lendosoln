/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XERespaFee : Mismo.Common.AbstractXmlNode
    {
        public XERespaFee()
        {
        }
        #region Schema
        //  <xs:element name="RESPA_FEE">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_PAYMENT" maxOccurs="unbounded"/>
        //        <xs:element ref="_REQUIRED_SERVICE_PROVIDER" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="_PAID_TO" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="RESPASectionClassificationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="100:GrossAmountDueFromBorrower"/>
        //            <xs:enumeration value="1000:ReservesDepositedWithLender"/>
        //            <xs:enumeration value="1100:TitleCharges"/>
        //            <xs:enumeration value="1200:RecordingAndTransferCharges"/>
        //            <xs:enumeration value="1300:AdditionalSettlementFees"/>
        //            <xs:enumeration value="200:AmountsPaidByOrInBehalfOfBorrower"/>
        //            <xs:enumeration value="300:CashAtSettlementFromToBorrower"/>
        //            <xs:enumeration value="400:GrossAmountDueToSeller"/>
        //            <xs:enumeration value="500:ReductionsInAmountDueToSeller"/>
        //            <xs:enumeration value="600:CashAtSettlementToFromSeller"/>
        //            <xs:enumeration value="700:DivisionOfCommission"/>
        //            <xs:enumeration value="800:LoanFees"/>
        //            <xs:enumeration value="900:RequiredLenderPaidInAdvance"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaidToName" type="xs:string"/>
        //      <xs:attribute name="_RequiredProviderOfServiceIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ResponsiblePartyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Buyer"/>
        //            <xs:enumeration value="Seller"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_SpecifiedHUDLineNumber" type="xs:string"/>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="203KDiscountOnRepairs"/>
        //            <xs:enumeration value="203KPermits"/>
        //            <xs:enumeration value="203KArchitecturalAndEngineeringFee"/>
        //            <xs:enumeration value="203KInspectionFee"/>
        //            <xs:enumeration value="203KSupplementalOriginationFee"/>
        //            <xs:enumeration value="203KConsultantFee"/>
        //            <xs:enumeration value="203KTitleUpdate"/>
        //            <xs:enumeration value="AbstractOrTitleSearchFee"/>
        //            <xs:enumeration value="AmortizationFee"/>
        //            <xs:enumeration value="ApplicationFee"/>
        //            <xs:enumeration value="AppraisalFee"/>
        //            <xs:enumeration value="AssignmentFee"/>
        //            <xs:enumeration value="AssignmentRecordingFee"/>
        //            <xs:enumeration value="AssumptionFee"/>
        //            <xs:enumeration value="AttorneyFee"/>
        //            <xs:enumeration value="BondReviewFee"/>
        //            <xs:enumeration value="CityCountyDeedTaxStampFee"/>
        //            <xs:enumeration value="CityCountyMortgageTaxStampFee"/>
        //            <xs:enumeration value="CLOAccessFee"/>
        //            <xs:enumeration value="CommitmentFee"/>
        //            <xs:enumeration value="CopyFaxFee"/>
        //            <xs:enumeration value="CourierFee"/>
        //            <xs:enumeration value="CreditReportFee"/>
        //            <xs:enumeration value="DeedRecordingFee"/>
        //            <xs:enumeration value="DocumentPreparationFee"/>
        //            <xs:enumeration value="DocumentaryStampFee"/>
        //            <xs:enumeration value="EscrowWaiverFee"/>
        //            <xs:enumeration value="FloodCertification"/>
        //            <xs:enumeration value="GeneralCounselFee"/>
        //            <xs:enumeration value="InspectionFee"/>
        //            <xs:enumeration value="LoanDiscountPoints"/>
        //            <xs:enumeration value="LoanOriginationFee"/>
        //            <xs:enumeration value="ModificationFee"/>
        //            <xs:enumeration value="MortgageBrokerFee"/>
        //            <xs:enumeration value="MortgageRecordingFee"/>
        //            <xs:enumeration value="MunicipalLienCertificateFee"/>
        //            <xs:enumeration value="MunicipalLienCertificateRecordingFee"/>
        //            <xs:enumeration value="NewLoanAdministrationFee"/>
        //            <xs:enumeration value="NotaryFee"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="PestInspectionFee"/>
        //            <xs:enumeration value="ProcessingFee"/>
        //            <xs:enumeration value="RedrawFee"/>
        //            <xs:enumeration value="ReinspectionFee"/>
        //            <xs:enumeration value="ReleaseRecordingFee"/>
        //            <xs:enumeration value="RuralHousingFee"/>
        //            <xs:enumeration value="SettlementOrClosingFee"/>
        //            <xs:enumeration value="StateDeedTaxStampFee"/>
        //            <xs:enumeration value="StateMortgageTaxStampFee"/>
        //            <xs:enumeration value="SurveyFee"/>
        //            <xs:enumeration value="TaxRelatedServiceFee"/>
        //            <xs:enumeration value="TitleExaminationFee"/>
        //            <xs:enumeration value="TitleInsuranceBinderFee"/>
        //            <xs:enumeration value="TitleInsuranceFee"/>
        //            <xs:enumeration value="UnderwritingFee"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="_PaidToType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Broker"/>
        //            <xs:enumeration value="Lender"/>
        //            <xs:enumeration value="Investor"/>
        //            <xs:enumeration value="Other"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaidToTypeOtherDescription" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private ArrayList m_paymentList = new ArrayList();
        private ArrayList m_requiredServiceProviderList = new ArrayList();
        private XEPaidTo m_paidTo;
        private E_RespaFeeRespaSectionClassificationType m_respaSectionClassificationType;
        private string m_paidToName;
        private E_YesNoIndicator m_requiredProviderOfServiceIndicator;
        private E_RespaFeeResponsiblePartyType m_responsiblePartyType;
        private string m_specifiedHUDLineNumber;
        private E_RespaFeeType m_type;
        private string m_typeOtherDescription;
        private E_RespaFeePaidToType m_paidToType;
        private string m_paidToTypeOtherDescription;
        #endregion

        #region Public Properties
        public void AddPayment(XEPayment payment) 
        {
            if (null == payment)
                return;
            m_paymentList.Add(payment);
        }
        public void AddRequiredServiceProvider(XERequiredServiceProvider requiredServiceProvider) 
        {
            if (null == requiredServiceProvider)
                return;
            m_requiredServiceProviderList.Add(requiredServiceProvider);
        }
        public void SetPaidTo(XEPaidTo paidTo) 
        {
            m_paidTo = paidTo;
        }
        public E_RespaFeeRespaSectionClassificationType RespaSectionClassificationType 
        {
            get { return m_respaSectionClassificationType; }
            set { m_respaSectionClassificationType = value; }
        }
        public string PaidToName 
        {
            get { return m_paidToName; }
            set { m_paidToName = value; }
        }
        public E_YesNoIndicator RequiredProviderOfServiceIndicator 
        {
            get { return m_requiredProviderOfServiceIndicator; }
            set { m_requiredProviderOfServiceIndicator = value; }
        }
        public E_RespaFeeResponsiblePartyType ResponsiblePartyType 
        {
            get { return m_responsiblePartyType; }
            set { m_responsiblePartyType = value; }
        }
        public string SpecifiedHUDLineNumber 
        {
            get { return m_specifiedHUDLineNumber; }
            set { m_specifiedHUDLineNumber = value; }
        }
        public E_RespaFeeType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        public E_RespaFeePaidToType PaidToType 
        {
            get { return m_paidToType; }
            set { m_paidToType = value; }
        }
        public string PaidToTypeOtherDescription 
        {
            get { return m_paidToTypeOtherDescription; }
            set { m_paidToTypeOtherDescription = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPA_FEE");
            string str = m_respaSectionClassificationType.ToString();
            if (str != "Undefined") 
            {
                // Trim leading '_' and replace second '_' with ':'
                str = str.Substring(1).Replace("_", ":");
                WriteAttribute(writer, "RESPASectionClassificationType", str);
            }

            WriteAttribute(writer, "_PaidToName", m_paidToName);
            WriteAttribute(writer, "_RequiredProviderOfServiceIndicator", m_requiredProviderOfServiceIndicator);
            WriteAttribute(writer, "_ResponsiblePartyType", m_responsiblePartyType);
            WriteAttribute(writer, "_SpecifiedHUDLineNumber", m_specifiedHUDLineNumber);

            str = m_type.ToString();
            if (str != "Undefined") 
            {
                str = str.Replace("_", "");
                WriteAttribute(writer, "_Type", str);
            }

            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "_PaidToType", m_paidToType);
            WriteAttribute(writer, "_PaidToTypeOtherDescription", m_paidToTypeOtherDescription);
            WriteElement(writer, m_paymentList);
            WriteElement(writer, m_requiredServiceProviderList);
            WriteElement(writer, m_paidTo);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_RespaFeeRespaSectionClassificationType 
    {
        Undefined = 0,
        _100_GrossAmountDueFromBorrower,
        _1000_ReservesDepositedWithLender,
        _1100_TitleCharges,
        _1200_RecordingAndTransferCharges,
        _1300_AdditionalSettlementFees,
        _200_AmountsPaidByOrInBehalfOfBorrower,
        _300_CashAtSettlementFromToBorrower,
        _500__ReductionsInAmountDueToSeller,
        _600_CashAtSettlementToFromSeller,
        _700_DivisionOfCommission,
        _800_LoanFees,
        _900_RequiredLenderPaidInAdvance
    }
    public enum E_RespaFeeResponsiblePartyType 
    {
        Undefined = 0,
        Buyer,
        Seller
    }
    public enum E_RespaFeeType 
    {
        Undefined = 0,
        _203KDiscountOnRepairs,
        _203KPermits,
        _203KArchitecturalAndEngineeringFee,
        _203KInspectionFee,
        _203KSupplementalOriginationFee,
        _203KConsultantFee,
        _203KTitleUpdate,
        AbstractOrTitleSearchFee,
        AmortizationFee,
        ApplicationFee,
        AppraisalFee,
        AssignmentFee,
        AssignmentRecordingFee,
        AssumptionFee,
        AttorneyFee,
        BondReviewFee,
        CityCountyDeedTaxStampFee,
        CityCountyMortgageTaxStampFee,
        CLOAccessFee,
        CommitmentFee,
        CopyFaxFee,
        CourierFee,
        CreditReportFee,
        DeedRecordingFee,
        DocumentPreparationFee,
        DocumentaryStampFee,
        EscrowWaiverFee,
        FloodCertification,
        GeneralCounselFee,
        InspectionFee,
        LoanDiscountPoints,
        LoanOriginationFee,
        ModificationFee,
        MortgageBrokerFee,
        MortgageRecordingFee,
        MunicipalLienCertificateFee,
        MunicipalLienCertificateRecordingFee,
        NewLoanAdministrationFee,
        NotaryFee,
        Other,
        PestInspectionFee,
        ProcessingFee,
        RedrawFee,
        ReinspectionFee,
        ReleaseRecordingFee,
        RuralHousingFee,
        SettlementOrClosingFee,
        StateDeedTaxStampFee,
        StateMortgageTaxStampFee,
        SurveyFee,
        TaxRelatedServiceFee,
        TitleExaminationFee,
        TitleInsuranceBinderFee,
        TitleInsuranceFee,
        UnderwritingFee
    }
    public enum E_RespaFeePaidToType 
    {
        Undefined = 0,
        Broker,
        Lender,
        Investor,
        Other
    }
}
