/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEFhaBorrower : Mismo.Common.AbstractXmlNode
	{
		public XEFhaBorrower()
		{
		}
        #region Schema
        // <xs:element name="FHA_BORROWER">
        //   <xs:complexType>
        //     <xs:attribute name="_CertificationLeadPaintIndicator">
        //     <xs:attribute name="_CertificationOriginalMortgageAmount" type="xs:string"/>
        //     <xs:attribute name="_CertificationOwn4OrMoreDwellingsIndicator">
        //     <xs:attribute name="_CertificationOwnOtherPropertyIndicator">
        //     <xs:attribute name="_CertificationPropertySoldCity" type="xs:string"/>
        //     <xs:attribute name="_CertificationPropertySoldPostalCode" type="xs:string"/>
        //     <xs:attribute name="_CertificationPropertySoldState" type="xs:string"/>
        //     <xs:attribute name="_CertificationPropertySoldStreetAddress" type="xs:string"/>
        //     <xs:attribute name="_CertificationPropertyToBeSoldIndicator">
        //     <xs:attribute name="_CertificationRentalIndicator">
        //     <xs:attribute name="_CertificationSalesPriceAmount" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_certificationLeadPaintIndicator;
        private string m_certificationOriginalMortgageAmount;
        private E_YesNoIndicator m_certificationOwn4OrMoreDwellingsIndicator;
        private E_YesNoIndicator m_certificationOwnOtherPropertyIndicator;
        private string m_certificationPropertySoldCity;
        private string m_certificationPropertySoldPostalCode;
        private string m_certificationPropertySoldState;
        private string m_certificationPropertySoldStreetAddress;
        private E_YesNoIndicator m_certificationPropertyToBeSoldIndicator;
        private E_YesNoIndicator m_certificationRentalIndicator;
        private string m_certificationSalesPriceAmount;
        #endregion

        #region Public Properties
        public E_YesNoIndicator CertificationLeadPaintIndicator 
        {
            get { return m_certificationLeadPaintIndicator; }
            set { m_certificationLeadPaintIndicator = value; }
        }
        public string CertificationOriginalMortgageAmount 
        {
            get { return m_certificationOriginalMortgageAmount; }
            set { m_certificationOriginalMortgageAmount = value; }
        }
        public E_YesNoIndicator CertificationOwn4OrMoreDwellingsIndicator 
        {
            get { return m_certificationOwn4OrMoreDwellingsIndicator; }
            set { m_certificationOwn4OrMoreDwellingsIndicator = value; }
        }
        public E_YesNoIndicator CertificationOwnOtherPropertyIndicator 
        {
            get { return m_certificationOwnOtherPropertyIndicator; }
            set { m_certificationOwnOtherPropertyIndicator = value; }
        }
        public string CertificationPropertySoldCity 
        {
            get { return m_certificationPropertySoldCity; }
            set { m_certificationPropertySoldCity = value; }
        }
        public string CertificationPropertySoldPostalCode 
        {
            get { return m_certificationPropertySoldPostalCode; }
            set { m_certificationPropertySoldPostalCode = value; }
        }
        public string CertificationPropertySoldState 
        {
            get { return m_certificationPropertySoldState; }
            set { m_certificationPropertySoldState = value; }
        }
        public string CertificationPropertySoldStreetAddress 
        {
            get { return m_certificationPropertySoldStreetAddress; }
            set { m_certificationPropertySoldStreetAddress = value; }
        }
        public E_YesNoIndicator CertificationPropertyToBeSoldIndicator 
        {
            get { return m_certificationPropertyToBeSoldIndicator; }
            set { m_certificationPropertyToBeSoldIndicator = value; }
        }
        public E_YesNoIndicator CertificationRentalIndicator 
        {
            get { return m_certificationRentalIndicator; }
            set { m_certificationRentalIndicator = value; }
        }
        public string CertificationSalesPriceAmount 
        {
            get { return m_certificationSalesPriceAmount; }
            set { m_certificationSalesPriceAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("FHA_BORROWER");
            WriteAttribute(writer, "_CertificationLeadPaintIndicator", m_certificationLeadPaintIndicator);
            WriteAttribute(writer, "_CertificationOriginalMortgageAmount", m_certificationOriginalMortgageAmount);
            WriteAttribute(writer, "_CertificationOwn4OrMoreDwellingsIndicator", m_certificationOwn4OrMoreDwellingsIndicator);
            WriteAttribute(writer, "_CertificationOwnOtherPropertyIndicator", m_certificationOwnOtherPropertyIndicator);
            WriteAttribute(writer, "_CertificationPropertySoldCity", m_certificationPropertySoldCity);
            WriteAttribute(writer, "_CertificationPropertySoldPostalCode", m_certificationPropertySoldPostalCode);
            WriteAttribute(writer, "_CertificationPropertySoldState", m_certificationPropertySoldState);
            WriteAttribute(writer, "_CertificationPropertySoldStreetAddress", m_certificationPropertySoldStreetAddress);
            WriteAttribute(writer, "_CertificationPropertyToBeSoldIndicator", m_certificationPropertyToBeSoldIndicator);
            WriteAttribute(writer, "_CertificationRentalIndicator", m_certificationRentalIndicator);
            WriteAttribute(writer, "_CertificationSalesPriceAmount", m_certificationSalesPriceAmount);
            writer.WriteEndElement(); // </FHA_BORROWER>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
