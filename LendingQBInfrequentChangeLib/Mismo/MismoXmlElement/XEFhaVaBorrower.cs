/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEFhaVaBorrower : Mismo.Common.AbstractXmlNode
	{
		public XEFhaVaBorrower()
		{
		}
        #region Schema
        // <xs:element name="FHA_VA_BORROWER">
        //   <xs:complexType>
        //     <xs:attribute name="CAIVRSIdentifier" type="xs:string"/>
        //     <xs:attribute name="FNMBankruptcyCount" type="xs:string"/>
        //     <xs:attribute name="FNMBorrowerCreditRating" type="xs:string"/>
        //     <xs:attribute name="FNMForeclosureCount" type="xs:string"/>
        //     <xs:attribute name="VeteranStatusIndicator">
        //     <xs:attribute name="_CertificationSalesPriceExceeedsAppraisedValueType">
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_CAIVRSIdentifier;
        private string m_fnmBankruptcyCount;
        private string m_fnmBorrowerCreditRating;
        private string m_fnmForeclosureCount;
        private E_YesNoIndicator m_veteranStatusIndicator;
        private E_CertificationSalesPriceExceedsAppraisedValueType m_certificationSalesPriceExceeedsAppraisedValueType;
        #endregion

        #region Public Properties
        public string CAIVRSIdentifier 
        {
            get { return m_CAIVRSIdentifier; }
            set { m_CAIVRSIdentifier = value; }
        }
        public string FnmBankruptcyCount 
        {
            get { return m_fnmBankruptcyCount; }
            set { m_fnmBankruptcyCount = value; }
        }
        public string FnmBorrowerCreditRating 
        {
            get { return m_fnmBorrowerCreditRating; }
            set { m_fnmBorrowerCreditRating = value; }
        }
        public string FnmForeclosureCount 
        {
            get { return m_fnmForeclosureCount; }
            set { m_fnmForeclosureCount = value; }
        }
        public E_YesNoIndicator VeteranStatusIndicator 
        {
            get { return m_veteranStatusIndicator; }
            set { m_veteranStatusIndicator = value; }
        }
        public E_CertificationSalesPriceExceedsAppraisedValueType CertificationSalesPriceExceeedsAppraisedValueType 
        {
            get { return m_certificationSalesPriceExceeedsAppraisedValueType; }
            set { m_certificationSalesPriceExceeedsAppraisedValueType = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("FHA_VA_BORROWER");
            WriteAttribute(writer, "CAIVRSIdentifier", m_CAIVRSIdentifier);
            WriteAttribute(writer, "FNMBankruptcyCount", m_fnmBankruptcyCount);
            WriteAttribute(writer, "FNMBorrowerCreditRating", m_fnmBorrowerCreditRating);
            WriteAttribute(writer, "FNMForeclosureCount", m_fnmForeclosureCount);
            WriteAttribute(writer, "VeteranStatusIndicator", m_veteranStatusIndicator);
            WriteAttribute(writer, "_CertificationSalesPriceExceeedsAppraisedValueType", m_certificationSalesPriceExceeedsAppraisedValueType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_FnmCreditReportScoreType 
    {
        Undefined = 0,
        CreditQuote,
        FICO
    }
    public enum E_CertificationSalesPriceExceedsAppraisedValueType 
    {
        Undefined = 0,
        A,
        B
    }
}
