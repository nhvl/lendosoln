/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XETotalFeesPaidTo : Mismo.Common.AbstractXmlNode
	{
		public XETotalFeesPaidTo()
		{
		}
        #region Schema
        //  <xs:element name="_TOTAL_FEES_PAID_TO">
        //    <xs:complexType>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Broker"/>
        //            <xs:enumeration value="Lender"/>
        //            <xs:enumeration value="Investor"/>
        //            <xs:enumeration value="Other"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="_TypeAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private E_TotalFeesPaidToType m_type;
        private string m_typeOtherDescription;
        private string m_typeAmount;
        #endregion

        #region Public Properties
        public E_TotalFeesPaidToType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        public string TypeAmount 
        {
            get { return m_typeAmount; }
            set { m_typeAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_TOTAL_FEES_PAID_TO");
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "_TypeAmount", m_typeAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_TotalFeesPaidToType 
    {
        Undefined = 0,
        Broker,
        Lender,
        Investor,
        Other
    }
}
