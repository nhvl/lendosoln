using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEResponseGroup : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="RESPONSE_GROUP">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="RESPONDING_PARTY" minOccurs="0"/>
        //              <xs:element ref="RESPOND_TO_PARTY" minOccurs="0"/>
        //              <xs:element ref="RESPONSE" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="Signature" minOccurs="0"/>
        //          </xs:sequence>
        //          <xs:attribute name="MISMOVersionID" type="xs:string" fixed="2.3.1"/>
        //          <xs:attribute name="_ID" type="xs:ID"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private XERespondingParty m_respondingParty;
        private XERespondToParty m_respondToParty;
        private ArrayList m_responseList = new ArrayList();
        private string m_mismoVersionID = "2.3.1";
        private string m_id;
        #endregion

        #region Public Properties
        public XERespondingParty RespondingParty 
        {
            get { return m_respondingParty; }
            set { m_respondingParty = value; }
        }
        public XERespondToParty RespondToParty 
        {
            get { return m_respondToParty; }
            set { m_respondToParty = value; }
        }
        public void AddResponse(XEResponse o) 
        {
            m_responseList.Add(o);
        }
        public XEResponse GetResponse(int index) 
        {
            if (null == m_responseList || index < 0 || index >= m_responseList.Count)
                return null;
            return (XEResponse) m_responseList[index];
        }

        public string ID 
        {
            get { return m_id; }
            set { m_id = value; }
        }
            
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPONSE_GROUP");
            WriteAttribute(writer, "MISMOVersionID", m_mismoVersionID);
            WriteAttribute(writer, "_ID", m_id);
            
            if (m_respondingParty != null)
                m_respondingParty.GenerateXml(writer);

            if (m_respondToParty != null)
                m_respondToParty.GenerateXml(writer);

            foreach (XEResponse o in m_responseList)
                o.GenerateXml(writer);

            writer.WriteEndElement(); // </RESPONSE_GROUP>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el) 
                return;

            m_respondingParty = null;
            
            XmlElement singleNode = (XmlElement) el.SelectSingleNode("RESPONDING_PARTY");
            if (null != singleNode) 
            {
                m_respondingParty = new XERespondingParty();
                m_respondingParty.Parse(singleNode);
            }

            m_respondToParty = null;

            singleNode = (XmlElement) el.SelectSingleNode("RESPOND_TO_PARTY");
            if (null != singleNode) 
            {
                m_respondToParty = new XERespondToParty();
                m_respondToParty.Parse(singleNode);
            }

            m_responseList = new ArrayList();
            XmlNodeList nodeList = el.SelectNodes("RESPONSE");
            foreach (XmlElement o in nodeList) 
            {
                XEResponse xe = new XEResponse();
                xe.Parse(o);
                m_responseList.Add(xe);
            }

            m_mismoVersionID = el.GetAttribute("MISMOVersionID");
            m_id = el.GetAttribute("_ID");
        }
        #endregion	
	}
}
