/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEValuation : Mismo.Common.AbstractXmlNode
    {
        public XEValuation()
        {
        }
        #region Schema
        //  <xs:element name="_VALUATION">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="APPRAISER" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_MethodType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FNM1004"/>
        //            <xs:enumeration value="EmployeeRelocationCouncil2001"/>
        //            <xs:enumeration value="FNM1073"/>
        //            <xs:enumeration value="FNM1025"/>
        //            <xs:enumeration value="FNM2055Exterior"/>
        //            <xs:enumeration value="FNM2065"/>
        //            <xs:enumeration value="FRE2070Interior"/>
        //            <xs:enumeration value="FRE2070Exterior"/>
        //            <xs:enumeration value="FNM2075"/>
        //            <xs:enumeration value="BrokerPriceOpinion"/>
        //            <xs:enumeration value="AutomatedValuationModel"/>
        //            <xs:enumeration value="TaxValuation"/>
        //            <xs:enumeration value="DriveBy"/>
        //            <xs:enumeration value="FullAppraisal"/>
        //            <xs:enumeration value="None"/>
        //            <xs:enumeration value="FNM2055InteriorAndExterior"/>
        //            <xs:enumeration value="FNM2095Exterior"/>
        //            <xs:enumeration value="FNM2095InteriorAndExterior"/>
        //            <xs:enumeration value="PriorAppraisalUsed"/>
        //            <xs:enumeration value="Form261805"/>
        //            <xs:enumeration value="Form268712"/>
        //            <xs:enumeration value="Other"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_MethodTypeOtherDescription" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private XEAppraiser m_appraiser;
        private E_ValuationMethodType m_methodType;
        private string m_methodTypeOtherDescription;
        #endregion

        #region Public Properties
        public void SetAppraiser(XEAppraiser appraiser) 
        {
            m_appraiser = appraiser;
        }
        public E_ValuationMethodType MethodType 
        {
            get { return m_methodType; }
            set { m_methodType = value; }
        }
        public string MethodTypeOtherDescription 
        {
            get { return m_methodTypeOtherDescription; }
            set { m_methodTypeOtherDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_VALUATION");
            WriteAttribute(writer, "_MethodType", m_methodType);
            WriteAttribute(writer, "_MethodTypeOtherDescription", m_methodTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ValuationMethodType 
    {
        Undefined = 0,
        FNM1004,
        EmployeeRelocationCouncil2001,
        FNM1073,
        FNM1025,
        FNM2055Exterior,
        FNM2065,
        FRE2070Interior,
        FRE2070Exterior,
        FNM2075,
        BrokerPriceOpinion,
        AutomatedValuationModel,
        TaxValuation,
        DriveBy,
        FullAppraisal,
        None,
        FNM2055InteriorAndExterior,
        FNM2095Exterior,
        FNM2095InteriorAndExterior,
        PriorAppraisalUsed,
        Form261805,
        Form268712,
        Other
    }
}
