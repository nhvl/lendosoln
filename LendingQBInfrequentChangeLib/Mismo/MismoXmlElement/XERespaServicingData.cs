/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XERespaServicingData : Mismo.Common.AbstractXmlNode
    {
        public XERespaServicingData()
        {
        }
        #region Schema
        //  <xs:element name="RESPA_SERVICING_DATA">
        //    <xs:complexType>
        //      <xs:attribute name="_AreAbleToServiceIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_AssignSellOrTransferSomeServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_DoNotServiceIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ExpectToAssignSellOrTransferPercentOfServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ExpectToAssignSellOrTransferPercent" type="xs:string"/>
        //      <xs:attribute name="_ExpectToRetainAllServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ExpectToSellAllServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_FirstTransferYear" type="xs:string"/>
        //      <xs:attribute name="_FirstTransferYearValue" type="xs:string"/>
        //      <xs:attribute name="_HaveNotDecidedToServiceIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_HaveNotServicedInPastThreeYearsIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_HavePreviouslyAssignedServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_MayAssignServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PresentlyIntendToAssignSellOrTransferServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_SecondTransferYear" type="xs:string"/>
        //      <xs:attribute name="_SecondTransferYearValue" type="xs:string"/>
        //      <xs:attribute name="_ThirdTransferYear" type="xs:string"/>
        //      <xs:attribute name="_ThirdTransferYearValue" type="xs:string"/>
        //      <xs:attribute name="_ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ThisIsOurRecordOfTransferringServicingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_TwelveMonthPeriodTransferPercent" type="xs:string"/>
        //      <xs:attribute name="_WillNotServiceIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_WillServiceIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_areAbleToServiceIndicator;
        private E_YesNoIndicator m_assignSellOrTransferSomeServicingIndicator;
        private E_YesNoIndicator m_doNotServiceIndicator;
        private E_YesNoIndicator m_expectToAssignSellOrTransferPercentOfServicingIndicator;
        private string m_expectToAssignSellOrTransferPercent;
        private E_YesNoIndicator m_expectToRetainAllServicingIndicator;
        private E_YesNoIndicator m_expectToSellAllServicingIndicator;
        private string m_firstTransferYear;
        private string m_firstTransferYearValue;
        private E_YesNoIndicator m_haveNotDecidedToServiceIndicator;
        private E_YesNoIndicator m_haveNotServicedInPastThreeYearsIndicator;
        private E_YesNoIndicator m_havePreviouslyAssignedServicingIndicator;
        private E_YesNoIndicator m_mayAssignServicingIndicator;
        private E_YesNoIndicator m_presentlyIntendToAssignSellOrTransferServicingIndicator;
        private string m_secondTransferYear;
        private string m_secondTransferYearValue;
        private string m_thirdTransferYear;
        private string m_thirdTransferYearValue;
        private E_YesNoIndicator m_thisEstimateIncludesAssignmentsSalesOrTransfersIndicator;
        private E_YesNoIndicator m_thisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator;
        private E_YesNoIndicator m_thisIsOurRecordOfTransferringServicingIndicator;
        private string m_twelveMonthPeriodTransferPercent;
        private E_YesNoIndicator m_willNotServiceIndicator;
        private E_YesNoIndicator m_willServiceIndicator;
        #endregion

        #region Public Properties
        public E_YesNoIndicator AreAbleToServiceIndicator 
        {
            get { return m_areAbleToServiceIndicator; }
            set { m_areAbleToServiceIndicator = value; }
        }
        public E_YesNoIndicator AssignSellOrTransferSomeServicingIndicator 
        {
            get { return m_assignSellOrTransferSomeServicingIndicator; }
            set { m_assignSellOrTransferSomeServicingIndicator = value; }
        }
        public E_YesNoIndicator DoNotServiceIndicator 
        {
            get { return m_doNotServiceIndicator; }
            set { m_doNotServiceIndicator = value; }
        }
        public E_YesNoIndicator ExpectToAssignSellOrTransferPercentOfServicingIndicator 
        {
            get { return m_expectToAssignSellOrTransferPercentOfServicingIndicator; }
            set { m_expectToAssignSellOrTransferPercentOfServicingIndicator = value; }
        }
        public string ExpectToAssignSellOrTransferPercent 
        {
            get { return m_expectToAssignSellOrTransferPercent; }
            set { m_expectToAssignSellOrTransferPercent = value; }
        }
        public E_YesNoIndicator ExpectToRetainAllServicingIndicator 
        {
            get { return m_expectToRetainAllServicingIndicator; }
            set { m_expectToRetainAllServicingIndicator = value; }
        }
        public E_YesNoIndicator ExpectToSellAllServicingIndicator 
        {
            get { return m_expectToSellAllServicingIndicator; }
            set { m_expectToSellAllServicingIndicator = value; }
        }
        public string FirstTransferYear 
        {
            get { return m_firstTransferYear; }
            set { m_firstTransferYear = value; }
        }
        public string FirstTransferYearValue 
        {
            get { return m_firstTransferYearValue; }
            set { m_firstTransferYearValue = value; }
        }
        public E_YesNoIndicator HaveNotDecidedToServiceIndicator 
        {
            get { return m_haveNotDecidedToServiceIndicator; }
            set { m_haveNotDecidedToServiceIndicator = value; }
        }
        public E_YesNoIndicator HaveNotServicedInPastThreeYearsIndicator 
        {
            get { return m_haveNotServicedInPastThreeYearsIndicator; }
            set { m_haveNotServicedInPastThreeYearsIndicator = value; }
        }
        public E_YesNoIndicator HavePreviouslyAssignedServicingIndicator 
        {
            get { return m_havePreviouslyAssignedServicingIndicator; }
            set { m_havePreviouslyAssignedServicingIndicator = value; }
        }
        public E_YesNoIndicator MayAssignServicingIndicator 
        {
            get { return m_mayAssignServicingIndicator; }
            set { m_mayAssignServicingIndicator = value; }
        }
        public E_YesNoIndicator PresentlyIntendToAssignSellOrTransferServicingIndicator 
        {
            get { return m_presentlyIntendToAssignSellOrTransferServicingIndicator; }
            set { m_presentlyIntendToAssignSellOrTransferServicingIndicator = value; }
        }
        public string SecondTransferYear 
        {
            get { return m_secondTransferYear; }
            set { m_secondTransferYear = value; }
        }
        public string SecondTransferYearValue 
        {
            get { return m_secondTransferYearValue; }
            set { m_secondTransferYearValue = value; }
        }
        public string ThirdTransferYear 
        {
            get { return m_thirdTransferYear; }
            set { m_thirdTransferYear = value; }
        }
        public string ThirdTransferYearValue 
        {
            get { return m_thirdTransferYearValue; }
            set { m_thirdTransferYearValue = value; }
        }
        public E_YesNoIndicator ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator 
        {
            get { return m_thisEstimateIncludesAssignmentsSalesOrTransfersIndicator; }
            set { m_thisEstimateIncludesAssignmentsSalesOrTransfersIndicator = value; }
        }
        public E_YesNoIndicator ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator 
        {
            get { return m_thisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator; }
            set { m_thisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator = value; }
        }
        public E_YesNoIndicator ThisIsOurRecordOfTransferringServicingIndicator 
        {
            get { return m_thisIsOurRecordOfTransferringServicingIndicator; }
            set { m_thisIsOurRecordOfTransferringServicingIndicator = value; }
        }
        public string TwelveMonthPeriodTransferPercent 
        {
            get { return m_twelveMonthPeriodTransferPercent; }
            set { m_twelveMonthPeriodTransferPercent = value; }
        }
        public E_YesNoIndicator WillNotServiceIndicator 
        {
            get { return m_willNotServiceIndicator; }
            set { m_willNotServiceIndicator = value; }
        }
        public E_YesNoIndicator WillServiceIndicator 
        {
            get { return m_willServiceIndicator; }
            set { m_willServiceIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPA_SERVICING_DATA");
            WriteAttribute(writer, "_AreAbleToServiceIndicator", m_areAbleToServiceIndicator);
            WriteAttribute(writer, "_AssignSellOrTransferSomeServicingIndicator", m_assignSellOrTransferSomeServicingIndicator);
            WriteAttribute(writer, "_DoNotServiceIndicator", m_doNotServiceIndicator);
            WriteAttribute(writer, "_ExpectToAssignSellOrTransferPercentOfServicingIndicator", m_expectToAssignSellOrTransferPercentOfServicingIndicator);
            WriteAttribute(writer, "_ExpectToAssignSellOrTransferPercent", m_expectToAssignSellOrTransferPercent);
            WriteAttribute(writer, "_ExpectToRetainAllServicingIndicator", m_expectToRetainAllServicingIndicator);
            WriteAttribute(writer, "_ExpectToSellAllServicingIndicator", m_expectToSellAllServicingIndicator);
            WriteAttribute(writer, "_FirstTransferYear", m_firstTransferYear);
            WriteAttribute(writer, "_FirstTransferYearValue", m_firstTransferYearValue);
            WriteAttribute(writer, "_HaveNotDecidedToServiceIndicator", m_haveNotDecidedToServiceIndicator);
            WriteAttribute(writer, "_HaveNotServicedInPastThreeYearsIndicator", m_haveNotServicedInPastThreeYearsIndicator);
            WriteAttribute(writer, "_HavePreviouslyAssignedServicingIndicator", m_havePreviouslyAssignedServicingIndicator);
            WriteAttribute(writer, "_MayAssignServicingIndicator", m_mayAssignServicingIndicator);
            WriteAttribute(writer, "_PresentlyIntendToAssignSellOrTransferServicingIndicator", m_presentlyIntendToAssignSellOrTransferServicingIndicator);
            WriteAttribute(writer, "_SecondTransferYear", m_secondTransferYear);
            WriteAttribute(writer, "_SecondTransferYearValue", m_secondTransferYearValue);
            WriteAttribute(writer, "_ThirdTransferYear", m_thirdTransferYear);
            WriteAttribute(writer, "_ThirdTransferYearValue", m_thirdTransferYearValue);
            WriteAttribute(writer, "_ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator", m_thisEstimateIncludesAssignmentsSalesOrTransfersIndicator);
            WriteAttribute(writer, "_ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator", m_thisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator);
            WriteAttribute(writer, "_ThisIsOurRecordOfTransferringServicingIndicator", m_thisIsOurRecordOfTransferringServicingIndicator);
            WriteAttribute(writer, "_TwelveMonthPeriodTransferPercent", m_twelveMonthPeriodTransferPercent);
            WriteAttribute(writer, "_WillNotServiceIndicator", m_willNotServiceIndicator);
            WriteAttribute(writer, "_WillServiceIndicator", m_willServiceIndicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
