/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEConstructionRefinanceData : Mismo.Common.AbstractXmlNode
    {
        public XEConstructionRefinanceData()
        {
        }
        #region Schema
        // <xs:element name="CONSTRUCTION_REFINANCE_DATA">
        //   <xs:complexType>
        //     <xs:attribute name="ConstructionImprovementCostsAmount" type="xs:string"/>
        //     <xs:attribute name="ConstructionPurposeType">
        //     <xs:attribute name="FRECashOutAmount" type="xs:string"/>
        //     <xs:attribute name="GSERefinancePurposeType">
        //     <xs:attribute name="LandEstimatedValueAmount" type="xs:string"/>
        //     <xs:attribute name="LandOriginalCostAmount" type="xs:string"/>
        //     <xs:attribute name="PropertyAcquiredYear" type="xs:string"/>
        //     <xs:attribute name="PropertyExistingLienAmount" type="xs:string"/>
        //     <xs:attribute name="PropertyOriginalCostAmount" type="xs:string"/>
        //     <xs:attribute name="RefinanceImprovementCostsAmount" type="xs:string"/>
        //     <xs:attribute name="RefinanceImprovementsType">
        //     <xs:attribute name="RefinanceProposedImprovementsDescription" type="xs:string"/>
        //     <xs:attribute name="SecondaryFinancingRefinanceIndicator">
        //     <xs:attribute name="ConstructionPeriodInterestRatePercent" type="xs:string"/>
        //     <xs:attribute name="ConstructionPeriodNumberOfMonthsCount" type="xs:string"/>
        //     <xs:attribute name="FNMSecondMortgageFinancingOriginalPropertyIndicator">
        //     <xs:attribute name="StructuralAlterationsConventionalAmount" type="xs:string"/>
        //     <xs:attribute name="NonStructuralAlterationsConventionalAmount" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_constructionImprovementCostsAmount;
        private E_ConstructionPurposeType m_constructionPurposeType;
        private string m_FRECashOutAmount;
        private E_GSERefinancePurposeType m_GSERefinancePurposeType;
        private string m_landEstimatedValueAmount;
        private string m_landOriginalCostAmount;
        private string m_propertyAcquiredYear;
        private string m_propertyExistingLienAmount;
        private string m_propertyOriginalCostAmount;
        private string m_refinanceImprovementCostsAmount;
        private E_RefinanceImprovementsType m_refinanceImprovementsType;
        private string m_refinanceProposedImprovementsDescription;
        private E_YesNoIndicator m_secondaryFinancingRefinanceIndicator;
        private string m_constructionPeriodInterestRatePercent;
        private string m_constructionPeriodNumberOfMonthsCount;
        private E_YesNoIndicator m_FNMSecondMortgageFinancingOriginalPropertyIndicator;
        private string m_structuralAlterationsConventionalAmount;
        private string m_nonStructuralAlterationsConventionalAmount;

        #endregion

        #region Public Properties
        public string ConstructionImprovementCostsAmount 
        {
            get { return m_constructionImprovementCostsAmount; }
            set { m_constructionImprovementCostsAmount = value; }
        }
        public E_ConstructionPurposeType ConstructionPurposeType 
        {
            get { return m_constructionPurposeType; }
            set { m_constructionPurposeType = value; }
        }
        public string FRECashOutAmount 
        {
            get { return m_FRECashOutAmount; }
            set { m_FRECashOutAmount = value; }
        }
        public E_GSERefinancePurposeType GSERefinancePurposeType 
        {
            get { return m_GSERefinancePurposeType; }
            set { m_GSERefinancePurposeType = value; }
        }
        public string LandEstimatedValueAmount 
        {
            get { return m_landEstimatedValueAmount; }
            set { m_landEstimatedValueAmount = value; }
        }
        public string LandOriginalCostAmount 
        {
            get { return m_landOriginalCostAmount; }
            set { m_landOriginalCostAmount = value; }
        }
        public string PropertyAcquiredYear 
        {
            get { return m_propertyAcquiredYear; }
            set { m_propertyAcquiredYear = value; }
        }
        public string PropertyExistingLienAmount 
        {
            get { return m_propertyExistingLienAmount; }
            set { m_propertyExistingLienAmount = value; }
        }
        public string PropertyOriginalCostAmount 
        {
            get { return m_propertyOriginalCostAmount; }
            set { m_propertyOriginalCostAmount = value; }
        }
        public string RefinanceImprovementCostsAmount 
        {
            get { return m_refinanceImprovementCostsAmount; }
            set { m_refinanceImprovementCostsAmount = value; }
        }
        public E_RefinanceImprovementsType RefinanceImprovementsType 
        {
            get { return m_refinanceImprovementsType; }
            set { m_refinanceImprovementsType = value; }
        }
        public string RefinanceProposedImprovementsDescription 
        {
            get { return m_refinanceProposedImprovementsDescription; }
            set { m_refinanceProposedImprovementsDescription = value; }
        }
        public E_YesNoIndicator SecondaryFinancingRefinanceIndicator 
        {
            get { return m_secondaryFinancingRefinanceIndicator; }
            set { m_secondaryFinancingRefinanceIndicator = value; }
        }
        public string ConstructionPeriodInterestRatePercent 
        {
            get { return m_constructionPeriodInterestRatePercent; }
            set { m_constructionPeriodInterestRatePercent = value; }
        }
        public string ConstructionPeriodNumberOfMonthsCount 
        {
            get { return m_constructionPeriodNumberOfMonthsCount; }
            set { m_constructionPeriodNumberOfMonthsCount = value; }
        }
        public E_YesNoIndicator FNMSecondMortgageFinancingOriginalPropertyIndicator 
        {
            get { return m_FNMSecondMortgageFinancingOriginalPropertyIndicator; }
            set { m_FNMSecondMortgageFinancingOriginalPropertyIndicator = value; }
        }
        public string StructuralAlterationsConventionalAmount 
        {
            get { return m_structuralAlterationsConventionalAmount; }
            set { m_structuralAlterationsConventionalAmount = value; }
        }
        public string NonStructuralAlterationsConventionalAmount 
        {
            get { return m_nonStructuralAlterationsConventionalAmount; }
            set { m_nonStructuralAlterationsConventionalAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("CONSTRUCTION_REFINANCE_DATA");
            WriteAttribute(writer, "ConstructionImprovementCostsAmount", m_constructionImprovementCostsAmount);
            WriteAttribute(writer, "ConstructionPurposeType", m_constructionPurposeType);
            WriteAttribute(writer, "FRECashOutAmount", m_FRECashOutAmount);
            WriteAttribute(writer, "GSERefinancePurposeType", m_GSERefinancePurposeType);
            WriteAttribute(writer, "LandEstimatedValueAmount", m_landEstimatedValueAmount);
            WriteAttribute(writer, "LandOriginalCostAmount", m_landOriginalCostAmount);
            WriteAttribute(writer, "PropertyAcquiredYear", m_propertyAcquiredYear);
            WriteAttribute(writer, "PropertyExistingLienAmount", m_propertyExistingLienAmount);
            WriteAttribute(writer, "PropertyOriginalCostAmount", m_propertyOriginalCostAmount);
            WriteAttribute(writer, "RefinanceImprovementCostsAmount", m_refinanceImprovementCostsAmount);
            WriteAttribute(writer, "RefinanceImprovementsType", m_refinanceImprovementsType);
            WriteAttribute(writer, "RefinanceProposedImprovementsDescription", m_refinanceProposedImprovementsDescription);
            WriteAttribute(writer, "SecondaryFinancingRefinanceIndicator", m_secondaryFinancingRefinanceIndicator);
            WriteAttribute(writer, "ConstructionPeriodInterestRatePercent", m_constructionPeriodInterestRatePercent);
            WriteAttribute(writer, "ConstructionPeriodNumberOfMonthsCount", m_constructionPeriodNumberOfMonthsCount);
            WriteAttribute(writer, "FNMSecondMortgageFinancingOriginalPropertyIndicator", m_FNMSecondMortgageFinancingOriginalPropertyIndicator);
            WriteAttribute(writer, "StructuralAlterationsConventionalAmount", m_structuralAlterationsConventionalAmount);
            WriteAttribute(writer, "NonStructuralAlterationsConventionalAmount", m_nonStructuralAlterationsConventionalAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ConstructionPurposeType 
    {
        Undefined = 0,
        ConstructionOnly,
        ConstructionToPermanent
    }
    public enum E_GSERefinancePurposeType 
    {
        Undefined = 0,
        CashOutDebtConsolidation,
        CashOutHomeImprovement,
        CashOutLimited,
        CashOutOther,
        NoCashOutFHAStreamlinedRefinance,
        NoCashOutFREOwnedRefinance,
        NoCashOutOther,
        NoCashOutStreamlinedRefinance,
        ChangeInRateTerm
    }
    public enum E_RefinanceImprovementsType 
    {
        Undefined = 0,
        Made,
        ToBeMade,
        Unknown
    }
}
