/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XENearestLivingRelative : Mismo.Common.AbstractXmlNode
    {
        public XENearestLivingRelative()
        {
        }
        #region Schema
        //  <xs:element name="_NEAREST_LIVING_RELATIVE">
        //    <xs:complexType>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_Name" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="_RelationshipDescription" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_TelephoneNumber" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_city;
        private string m_name;
        private string m_postalCode;
        private string m_relationshipDescription;
        private string m_state;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_telephoneNumber;
        #endregion

        #region Public Properties
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string RelationshipDescription 
        {
            get { return m_relationshipDescription; }
            set { m_relationshipDescription = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string TelephoneNumber 
        {
            get { return m_telephoneNumber; }
            set { m_telephoneNumber = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_NEAREST_LIVING_RELATIVE");
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_RelationshipDescription", m_relationshipDescription);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_TelephoneNumber", m_telephoneNumber);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
