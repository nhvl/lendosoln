/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEProposedHousingExpense : Mismo.Common.AbstractXmlNode
    {
        public XEProposedHousingExpense()
        {
        }
        #region Schema
        //  <xs:element name="PROPOSED_HOUSING_EXPENSE">
        //    <xs:complexType>
        //      <xs:attribute name="HousingExpenseType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FirstMortgagePrincipalAndInterest"/>
        //            <xs:enumeration value="HazardInsurance"/>
        //            <xs:enumeration value="HomeownersAssociationDuesAndCondominiumFees"/>
        //            <xs:enumeration value="MI"/>
        //            <xs:enumeration value="OtherHousingExpense"/>
        //            <xs:enumeration value="OtherMortgageLoanPrincipalAndInterest"/>
        //            <xs:enumeration value="RealEstateTax"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaymentAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private E_ProposedHousingExpenseType m_housingExpenseType;
        private string m_paymentAmount;
        #endregion

        #region Public Properties
        public E_ProposedHousingExpenseType HousingExpenseType 
        {
            get { return m_housingExpenseType; }
            set { m_housingExpenseType = value; }
        }
        public string PaymentAmount 
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PROPOSED_HOUSING_EXPENSE");
            WriteAttribute(writer, "HousingExpenseType", m_housingExpenseType);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ProposedHousingExpenseType 
    {
        Undefined = 0,
        FirstMortgagePrincipalAndInterest,
        HazardInsurance,
        HomeownersAssociationDuesAndCondominiumFees,
        MI,
        OtherHousingExpense,
        OtherMortgageLoanPrincipalAndInterest,
        RealEstateTax
    }
}
