/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEMortgageScore : Mismo.Common.AbstractXmlNode
    {
        public XEMortgageScore()
        {
        }
        #region Schema
        //  <xs:element name="MORTGAGE_SCORE">
        //    <xs:complexType>
        //      <xs:attribute name="_Date" type="xs:string"/>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FraudFilterScore"/>
        //            <xs:enumeration value="GE_IQScore"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="PMIAuraAQIScore"/>
        //            <xs:enumeration value="UGIAccuscore"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_Value" type="xs:string"/>
        //      <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_date;
        private E_MortgageScoreType m_type;
        private string m_value;
        private string m_typeOtherDescription;
        #endregion

        #region Public Properties
        public string Date 
        {
            get { return m_date; }
            set { m_date = value; }
        }
        public E_MortgageScoreType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string Value 
        {
            get { return m_value; }
            set { m_value = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MORTGAGE_SCORE");
            WriteAttribute(writer, "_Date", m_date);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_Value", m_value);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_MortgageScoreType 
    {
        Undefined = 0,
        FraudFilterScore,
        GE_IQScore,
        Other,
        PMIAuraAQIScore,
        UGIAccuscore
    }
}
