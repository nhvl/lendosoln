using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEResponse : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="RESPONSE">
        //      <xs:complexType>
        //          <xs:sequence>
        //              <xs:element ref="KEY" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="RESPONSE_DATA" minOccurs="0" maxOccurs="unbounded"/>
        //              <xs:element ref="STATUS" minOccurs="0" maxOccurs="unbounded"/>
        //          </xs:sequence>
        //          <xs:attribute name="_ID" type="xs:ID"/>
        //          <xs:attribute name="ResponseDateTime" type="xs:string"/>
        //          <xs:attribute name="InternalAccountIdentifier" type="xs:string"/>
        //          <xs:attribute name="LoginAccountIdentifier" type="xs:string"/>
        //          <xs:attribute name="LoginAccountPassword" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_keyList = new ArrayList();
        private ArrayList m_responseDataList = new ArrayList();
        private ArrayList m_statusList = new ArrayList();
        private string m_id;
        private DateTime m_responseDateTime;
        private string m_internalAccountIdentifier;
        private string m_loginAccountIdentifier;
        private string m_loginAccountPassword;
        #endregion

        #region Public Properties
        public void AddKey(XEKey o) 
        {
            m_keyList.Add(o);
        }
        public void AddResponseData(XEResponseData o) 
        {
            m_responseDataList.Add(o);
        }
        public XEResponseData GetResponseData(int index) 
        {
            if (null == m_responseDataList || index < 0 || index >= m_responseDataList.Count)
                return null;
            return (XEResponseData) m_responseDataList[index];
        }
        public void AddStatus(XEStatus o) 
        {
            m_statusList.Add(o);
        }
        public string ID 
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public DateTime ResponseDateTime 
        { 
            get { return m_responseDateTime; } 
            set { m_responseDateTime = value; } 
        } 
        public string InternalAccountIdentifier 
        { 
            get { return m_internalAccountIdentifier; } 
            set { m_internalAccountIdentifier = value; } 
        } 
        public string LoginAccountIdentifier 
        { 
            get { return m_loginAccountIdentifier; } 
            set { m_loginAccountIdentifier = value; } 
        } 
        public string LoginAccountPassword 
        { 
            get { return m_loginAccountPassword; } 
            set { m_loginAccountPassword = value; } 
        } 

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPONSE");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "ResponseDateTime", Mismo.Common.Utilities.ToDateTimeString(m_responseDateTime));
            WriteAttribute(writer, "InternalAccountIdentifier", m_internalAccountIdentifier);
            WriteAttribute(writer, "LoginAccountIdentifier", m_loginAccountIdentifier);
            WriteAttribute(writer, "LoginAccountPassword", m_loginAccountPassword);

            foreach (XEKey o in m_keyList)
                o.GenerateXml(writer);

            foreach (XEResponseData o in m_responseDataList)
                o.GenerateXml(writer);

            foreach (XEStatus o in m_statusList)
                o.GenerateXml(writer);

            writer.WriteEndElement(); // </RESPONSE>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el) 
                return;

            m_keyList = new ArrayList();
            XmlNodeList nodeList = el.SelectNodes("KEY");
            foreach (XmlElement o in nodeList) 
            {
                XEKey xe = new XEKey();
                xe.Parse(o);
                m_keyList.Add(xe);
            }

            m_responseDataList = new ArrayList();
            nodeList = el.SelectNodes("RESPONSE_DATA");
            foreach (XmlElement o in nodeList) 
            {
                XEResponseData xe = new XEResponseData();
                xe.Parse(o);
                m_responseDataList.Add(xe);
            }

            m_statusList = new ArrayList();
            nodeList = el.SelectNodes("STATUS");
            foreach (XmlElement o in nodeList) 
            {
                XEStatus xe = new XEStatus();
                xe.Parse(o);
                m_statusList.Add(xe);
            }

            m_id = el.GetAttribute("_ID");
            try 
            {
                m_responseDateTime = Mismo.Common.Utilities.ParseDateTime(el.GetAttribute("ResponseDateTime"));
            } 
            catch 
            {
                m_responseDateTime = DateTime.MinValue;
            }
            m_internalAccountIdentifier = el.GetAttribute("InternalAccountIdentifier");
            m_loginAccountIdentifier = el.GetAttribute("LoginAccountIdentifier");
            m_loginAccountPassword = el.GetAttribute("LoginAccountPassword");

        }
        #endregion	
    }
}
