/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XETitleHolder : Mismo.Common.AbstractXmlNode
    {
        public XETitleHolder()
        {
        }
        #region Schema
        //  <xs:element name="TITLE_HOLDER">
        //    <xs:complexType>
        //      <xs:attribute name="_Name" type="xs:string"/>
        //      <xs:attribute name="LandTrustType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="IllinoisLandTrust"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_name;
        private E_TitleHolderLandTrustType m_landTrustType;
        #endregion

        #region Public Properties
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public E_TitleHolderLandTrustType LandTrustType 
        {
            get { return m_landTrustType; }
            set { m_landTrustType = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("TITLE_HOLDER");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "LandTrustType", m_landTrustType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_TitleHolderLandTrustType 
    {
        Undefined = 0,
        IllinoisLandTrust
    }
}
