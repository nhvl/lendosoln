using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEKey : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="KEY">
        //      <xs:complexType>
        //          <xs:attribute name="_Name" type="xs:string"/>
        //          <xs:attribute name="_Value" type="xs:string"/>
        //      </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_name;
        private string m_value;
        #endregion

        #region Public Properties
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string Value 
        {
            get { return m_value; }
            set { m_value = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("KEY");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_Value", m_value);
            writer.WriteEndElement(); // </KEY>
        }
        public override void Parse(XmlElement el) 
        {
            if (null == el)
                return;

            m_name = el.GetAttribute("_Name");
            m_value = el.GetAttribute("_Value");
        }
        #endregion
	}
}
