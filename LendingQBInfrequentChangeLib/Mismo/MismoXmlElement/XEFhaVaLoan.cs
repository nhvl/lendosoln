/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEFhaVaLoan : Mismo.Common.AbstractXmlNode
	{
		public XEFhaVaLoan()
		{
		}
        #region Schema
        // <xs:element name="FHA_VA_LOAN">
        //   <xs:complexType>
        //     <xs:attribute name="BorrowerPaidFHA_VAClosingCostsAmount" type="xs:string"/>
        //     <xs:attribute name="BorrowerPaidFHA_VAClosingCostsPercent" type="xs:string"/>
        //     <xs:attribute name="GovernmentMortgageCreditCertificateAmount" type="xs:string"/>
        //     <xs:attribute name="GovernmentRefinanceType">
        //     <xs:attribute name="OtherPartyPaidFHA_VAClosingCostsAmount" type="xs:string"/>
        //     <xs:attribute name="OtherPartyPaidFHA_VAClosingCostsPercent" type="xs:string"/>
        //     <xs:attribute name="PropertyEnergyEfficientHomeIndicator">
        //     <xs:attribute name="SellerPaidFHA_VAClosingCostsPercent" type="xs:string"/>
        //     <xs:attribute name="_OriginatorIdentifier" type="xs:string"/>
        //   </xs:complexType>
        // </xs:element>
        #endregion

        #region Private Member Variables
        private string m_borrowerPaidFhaVaClosingCostsAmount;
        private string m_borrowerPaidFhaVaClosingCostsPercent;
        private string m_governmentMortgageCreditCertificateAmount;
        private E_GovernmentRefinanceType m_governmentRefinanceType;
        private string m_otherPartyPaidFhaVaClosingCostsAmount;
        private string m_otherPartyPaidFhaVaClosingCostsPercent;
        private E_YesNoIndicator m_propertyEnergyEfficientHomeIndicator;
        private string m_sellerPaidFhaVaClosingCostsPercent;
        private string m_originatorIdentifier;
        #endregion

        #region Public Properties
        public string BorrowerPaidFhaVaClosingCostsAmount 
        {
            get { return m_borrowerPaidFhaVaClosingCostsAmount; }
            set { m_borrowerPaidFhaVaClosingCostsAmount = value; }
        }
        public string BorrowerPaidFhaVaClosingCostsPercent 
        {
            get { return m_borrowerPaidFhaVaClosingCostsPercent; }
            set { m_borrowerPaidFhaVaClosingCostsPercent = value; }
        }
        public string GovernmentMortgageCreditCertificateAmount 
        {
            get { return m_governmentMortgageCreditCertificateAmount; }
            set { m_governmentMortgageCreditCertificateAmount = value; }
        }
        public E_GovernmentRefinanceType GovernmentRefinanceType 
        {
            get { return m_governmentRefinanceType; }
            set { m_governmentRefinanceType = value; }
        }
        public string OtherPartyPaidFhaVaClosingCostsAmount 
        {
            get { return m_otherPartyPaidFhaVaClosingCostsAmount; }
            set { m_otherPartyPaidFhaVaClosingCostsAmount = value; }
        }
        public string OtherPartyPaidFhaVaClosingCostsPercent 
        {
            get { return m_otherPartyPaidFhaVaClosingCostsPercent; }
            set { m_otherPartyPaidFhaVaClosingCostsPercent = value; }
        }
        public E_YesNoIndicator PropertyEnergyEfficientHomeIndicator 
        {
            get { return m_propertyEnergyEfficientHomeIndicator; }
            set { m_propertyEnergyEfficientHomeIndicator = value; }
        }
        public string SellerPaidFhaVaClosingCostsPercent 
        {
            get { return m_sellerPaidFhaVaClosingCostsPercent; }
            set { m_sellerPaidFhaVaClosingCostsPercent = value; }
        }
        public string OriginatorIdentifier 
        {
            get { return m_originatorIdentifier; }
            set { m_originatorIdentifier = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("FHA_VA_LOAN");
            WriteAttribute(writer, "BorrowerPaidFHA_VAClosingCostsAmount", m_borrowerPaidFhaVaClosingCostsAmount);
            WriteAttribute(writer, "BorrowerPaidFHA_VAClosingCostsPercent", m_borrowerPaidFhaVaClosingCostsPercent);
            WriteAttribute(writer, "GovernmentMortgageCreditCertificateAmount", m_governmentMortgageCreditCertificateAmount);
            WriteAttribute(writer, "GovernmentRefinanceType", m_governmentRefinanceType);
            WriteAttribute(writer, "OtherPartyPaidFHA_VAClosingCostsAmount", m_otherPartyPaidFhaVaClosingCostsAmount);
            WriteAttribute(writer, "OtherPartyPaidFHA_VAClosingCostsPercent", m_otherPartyPaidFhaVaClosingCostsPercent);
            WriteAttribute(writer, "PropertyEnergyEfficientHomeIndicator", m_propertyEnergyEfficientHomeIndicator);
            WriteAttribute(writer, "SellerPaidFHA_VAClosingCostsPercent", m_sellerPaidFhaVaClosingCostsPercent);
            WriteAttribute(writer, "_OriginatorIdentifier", m_originatorIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion

	}
    public enum E_GovernmentRefinanceType 
    {
        Undefined = 0,
        FullDocumentation,
        InterestRateReductionRefinanceLoan,
        StreamlineWithAppraisal,
        StreamlineWithoutAppraisal
    }
}
