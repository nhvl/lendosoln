/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
	public class XEDataInformation : Mismo.Common.AbstractXmlNode
	{
		public XEDataInformation()
		{
		}
        #region Schema
        //  <xs:element name="_DATA_INFORMATION">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="DATA_VERSION" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private ArrayList m_dataVersionList = new ArrayList();
        #endregion

        #region Public Properties
        public void AddDataVersion(XEDataVersion version) 
        {
            if (null != version)
                return;

            m_dataVersionList.Add(version);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_DATA_INFORMATION");
            foreach (XEDataVersion version in m_dataVersionList) 
            {
                version.GenerateXml(writer);
            }
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
