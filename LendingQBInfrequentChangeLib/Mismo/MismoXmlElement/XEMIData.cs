/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEMIData : Mismo.Common.AbstractXmlNode
    {
        public XEMIData()
        {
        }
        #region Schema
        //  <xs:element name="MI_DATA">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="MI_RENEWAL_PREMIUM" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="MI_PREMIUM_TAX" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="MIPremiumFinancedIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIPremiumPaymentType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="BorrowerPaid"/>
        //            <xs:enumeration value="BothBorrowerAndLenderPaid"/>
        //            <xs:enumeration value="LenderPaid"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIPremiumRefundableType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="NotRefundable"/>
        //            <xs:enumeration value="Refundable"/>
        //            <xs:enumeration value="RefundableWithLimits"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIPremiumTermMonths" type="xs:string"/>
        //      <xs:attribute name="MIRenewalCalculationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Constant"/>
        //            <xs:enumeration value="Declining"/>
        //            <xs:enumeration value="NoRenewals"/>
        //            <xs:enumeration value="NotApplicable"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIDurationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Annual"/>
        //            <xs:enumeration value="NotApplicable"/>
        //            <xs:enumeration value="PeriodicMonthly"/>
        //            <xs:enumeration value="SingleLifeOfLoan"/>
        //            <xs:enumeration value="SingleSpecific"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIInitialPremiumRateDurationMonths" type="xs:string"/>
        //      <xs:attribute name="MIInitialPremiumRatePercent" type="xs:string"/>
        //      <xs:attribute name="MIEscrowIncludedInAggregateIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MI_LTVCutoffPercent" type="xs:string"/>
        //      <xs:attribute name="MI_LTVCutoffType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="AppraisedValue"/>
        //            <xs:enumeration value="SalesPrice"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MICollectedNumberOfMonthsCount" type="xs:string"/>
        //      <xs:attribute name="MICushionNumberOfMonthsCount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private ArrayList m_miRenewalPremiumList = new ArrayList();
        private ArrayList m_miPremiumTaxList = new ArrayList();
        private E_YesNoIndicator m_miPremiumFinancedIndicator;
        private E_MIPremiumPaymentType m_miPremiumPaymentType;
        private E_MIPremiumRefundableType m_miPremiumRefundableType;
        private string m_miPremiumTermMonths;
        private E_MIRenewalCalculationType m_miRenewalCalculationType;
        private E_MIDurationType m_miDurationType;
        private string m_miInitialPremiumRateDurationMonths;
        private string m_miInitialPremiumRatePercent;
        private E_YesNoIndicator m_miEscrowIncludedInAggregateIndicator;
        private string m_miLtvCutoffPercent;
        private E_MILtvCutoffType m_miLtvCutoffType;
        private string m_miCollectedNumberOfMonthsCount;
        private string m_miCushionNumberOfMonthsCount;
        #endregion

        #region Public Properties
        public void AddMiRenewalPremium(XEMIRenewalPremium miRenewalPremiumList) 
        {
            if (null == miRenewalPremiumList)
                return;
            m_miRenewalPremiumList.Add(miRenewalPremiumList);
        }
        public void AddMiPremiumTax(XEMIPremiumTax miPremiumTax) 
        {
            if (null == miPremiumTax)
                return;

            m_miPremiumTaxList.Add(miPremiumTax);
        }
        public E_YesNoIndicator MiPremiumFinancedIndicator 
        {
            get { return m_miPremiumFinancedIndicator; }
            set { m_miPremiumFinancedIndicator = value; }
        }
        public E_MIPremiumPaymentType MiPremiumPaymentType 
        {
            get { return m_miPremiumPaymentType; }
            set { m_miPremiumPaymentType = value; }
        }
        public E_MIPremiumRefundableType MiPremiumRefundableType 
        {
            get { return m_miPremiumRefundableType; }
            set { m_miPremiumRefundableType = value; }
        }
        public string MiPremiumTermMonths 
        {
            get { return m_miPremiumTermMonths; }
            set { m_miPremiumTermMonths = value; }
        }
        public E_MIRenewalCalculationType MiRenewalCalculationType 
        {
            get { return m_miRenewalCalculationType; }
            set { m_miRenewalCalculationType = value; }
        }
        public E_MIDurationType MiDurationType 
        {
            get { return m_miDurationType; }
            set { m_miDurationType = value; }
        }
        public string MiInitialPremiumRateDurationMonths 
        {
            get { return m_miInitialPremiumRateDurationMonths; }
            set { m_miInitialPremiumRateDurationMonths = value; }
        }
        public string MiInitialPremiumRatePercent 
        {
            get { return m_miInitialPremiumRatePercent; }
            set { m_miInitialPremiumRatePercent = value; }
        }
        public E_YesNoIndicator MiEscrowIncludedInAggregateIndicator 
        {
            get { return m_miEscrowIncludedInAggregateIndicator; }
            set { m_miEscrowIncludedInAggregateIndicator = value; }
        }
        public string MiLtvCutoffPercent 
        {
            get { return m_miLtvCutoffPercent; }
            set { m_miLtvCutoffPercent = value; }
        }
        public E_MILtvCutoffType MiLtvCutoffType 
        {
            get { return m_miLtvCutoffType; }
            set { m_miLtvCutoffType = value; }
        }
        public string MiCollectedNumberOfMonthsCount 
        {
            get { return m_miCollectedNumberOfMonthsCount; }
            set { m_miCollectedNumberOfMonthsCount = value; }
        }
        public string MiCushionNumberOfMonthsCount 
        {
            get { return m_miCushionNumberOfMonthsCount; }
            set { m_miCushionNumberOfMonthsCount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MI_DATA");
            WriteAttribute(writer, "MIPremiumFinancedIndicator", m_miPremiumFinancedIndicator);
            WriteAttribute(writer, "MIPremiumPaymentType", m_miPremiumPaymentType);
            WriteAttribute(writer, "MIPremiumRefundableType", m_miPremiumRefundableType);
            WriteAttribute(writer, "MIPremiumTermMonths", m_miPremiumTermMonths);
            WriteAttribute(writer, "MIRenewalCalculationType", m_miRenewalCalculationType);
            WriteAttribute(writer, "MIDurationType", m_miDurationType);
            WriteAttribute(writer, "MIInitialPremiumRateDurationMonths", m_miInitialPremiumRateDurationMonths);
            WriteAttribute(writer, "MIInitialPremiumRatePercent", m_miInitialPremiumRatePercent);
            WriteAttribute(writer, "MIEscrowIncludedInAggregateIndicator", m_miEscrowIncludedInAggregateIndicator);
            WriteAttribute(writer, "MI_LTVCutoffPercent", m_miLtvCutoffPercent);
            WriteAttribute(writer, "MI_LTVCutoffType", m_miLtvCutoffType);
            WriteAttribute(writer, "MICollectedNumberOfMonthsCount", m_miCollectedNumberOfMonthsCount);
            WriteAttribute(writer, "MICushionNumberOfMonthsCount", m_miCushionNumberOfMonthsCount);
            WriteElement(writer, m_miRenewalPremiumList);
            WriteElement(writer, m_miPremiumTaxList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_MIPremiumPaymentType 
    {
        Undefined = 0,
        BorrowerPaid,
        BothBorrowerAndLenderPaid,
        LenderPaid
    }
    public enum E_MIPremiumRefundableType 
    {
        Undefined = 0,
        NotRefundable,
        Refundable,
        RefundableWithLimits
    }
    public enum E_MIRenewalCalculationType 
    {
        Undefined = 0,
        Constant,
        Declining,
        NoRenewals,
        NotApplicable
    }
    public enum E_MIDurationType 
    {
        Undefined = 0,
        Annual,
        NotApplicable,
        PeriodicMonthly,
        SingleLifeOfLoan,
        SingleSpecific
    }
    public enum E_MILtvCutoffType 
    {
        Undefined = 0,
        AppraisedValue,
        SalesPrice
    }
}
