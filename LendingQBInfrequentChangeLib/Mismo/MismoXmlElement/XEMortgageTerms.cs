/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.MismoXmlElement
{
    public class XEMortgageTerms : Mismo.Common.AbstractXmlNode
    {
        public XEMortgageTerms()
        {
        }
        #region Schema
        //  <xs:element name="MORTGAGE_TERMS">
        //    <xs:complexType>
        //      <xs:attribute name="AgencyCaseIdentifier" type="xs:string"/>
        //      <xs:attribute name="ARMTypeDescription" type="xs:string"/>
        //      <xs:attribute name="BaseLoanAmount" type="xs:string"/>
        //      <xs:attribute name="BorrowerRequestedLoanAmount" type="xs:string"/>
        //      <xs:attribute name="LenderCaseIdentifier" type="xs:string"/>
        //      <xs:attribute name="LoanAmortizationTermMonths" type="xs:string"/>
        //      <xs:attribute name="LoanAmortizationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="AdjustableRate"/>
        //            <xs:enumeration value="Fixed"/>
        //            <xs:enumeration value="GraduatedPaymentMortgage"/>
        //            <xs:enumeration value="GrowingEquityMortgage"/>
        //            <xs:enumeration value="OtherAmortizationType"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MortgageType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Conventional"/>
        //            <xs:enumeration value="FarmersHomeAdministration"/>
        //            <xs:enumeration value="FHA"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="VA"/>
        //            <xs:enumeration value="HELOC"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="OtherAmortizationTypeDescription" type="xs:string"/>
        //      <xs:attribute name="OtherMortgageTypeDescription" type="xs:string"/>
        //      <xs:attribute name="RequestedInterestRatePercent" type="xs:string"/>
        //      <xs:attribute name="LendersContactPrimaryTelephoneNumber" type="xs:string"/>
        //      <xs:attribute name="LoanEstimatedClosingDate" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_agencyCaseIdentifier;
        private string m_armTypeDescription;
        private string m_baseLoanAmount;
        private string m_borrowerRequestedLoanAmount;
        private string m_lenderCaseIdentifier;
        private string m_loanAmortizationTermMonths;
        private E_LoanAmortizationType m_loanAmortizationType;
        private E_MortgageType m_mortgageType;
        private string m_otherAmortizationTypeDescription;
        private string m_otherMortgageTypeDescription;
        private string m_requestedInterestRatePercent;
        private string m_lendersContactPrimaryTelephoneNumber;
        private string m_loanEstimatedClosingDate;
        #endregion

        #region Public Properties
        public string AgencyCaseIdentifier 
        {
            get { return m_agencyCaseIdentifier; }
            set { m_agencyCaseIdentifier = value; }
        }
        public string ArmTypeDescription 
        {
            get { return m_armTypeDescription; }
            set { m_armTypeDescription = value; }
        }
        public string BaseLoanAmount 
        {
            get { return m_baseLoanAmount; }
            set { m_baseLoanAmount = value; }
        }
        public string BorrowerRequestedLoanAmount 
        {
            get { return m_borrowerRequestedLoanAmount; }
            set { m_borrowerRequestedLoanAmount = value; }
        }
        public string LenderCaseIdentifier 
        {
            get { return m_lenderCaseIdentifier; }
            set { m_lenderCaseIdentifier = value; }
        }
        public string LoanAmortizationTermMonths 
        {
            get { return m_loanAmortizationTermMonths; }
            set { m_loanAmortizationTermMonths = value; }
        }
        public E_LoanAmortizationType LoanAmortizationType 
        {
            get { return m_loanAmortizationType; }
            set { m_loanAmortizationType = value; }
        }
        public E_MortgageType MortgageType 
        {
            get { return m_mortgageType; }
            set { m_mortgageType = value; }
        }
        public string OtherAmortizationTypeDescription 
        {
            get { return m_otherAmortizationTypeDescription; }
            set { m_otherAmortizationTypeDescription = value; }
        }
        public string OtherMortgageTypeDescription 
        {
            get { return m_otherMortgageTypeDescription; }
            set { m_otherMortgageTypeDescription = value; }
        }
        public string RequestedInterestRatePercent 
        {
            get { return m_requestedInterestRatePercent; }
            set { m_requestedInterestRatePercent = value; }
        }
        public string LendersContactPrimaryTelephoneNumber 
        {
            get { return m_lendersContactPrimaryTelephoneNumber; }
            set { m_lendersContactPrimaryTelephoneNumber = value; }
        }
        public string LoanEstimatedClosingDate 
        {
            get { return m_loanEstimatedClosingDate; }
            set { m_loanEstimatedClosingDate = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MORTGAGE_TERMS");
            WriteAttribute(writer, "AgencyCaseIdentifier", m_agencyCaseIdentifier);
            WriteAttribute(writer, "ARMTypeDescription", m_armTypeDescription);
            WriteAttribute(writer, "BaseLoanAmount", m_baseLoanAmount);
            WriteAttribute(writer, "BorrowerRequestedLoanAmount", m_borrowerRequestedLoanAmount);
            WriteAttribute(writer, "LenderCaseIdentifier", m_lenderCaseIdentifier);
            WriteAttribute(writer, "LoanAmortizationTermMonths", m_loanAmortizationTermMonths);
            WriteAttribute(writer, "LoanAmortizationType", m_loanAmortizationType);
            WriteAttribute(writer, "MortgageType", m_mortgageType);
            WriteAttribute(writer, "OtherAmortizationTypeDescription", m_otherAmortizationTypeDescription);
            WriteAttribute(writer, "OtherMortgageTypeDescription", m_otherMortgageTypeDescription);
            WriteAttribute(writer, "RequestedInterestRatePercent", m_requestedInterestRatePercent);
            WriteAttribute(writer, "LendersContactPrimaryTelephoneNumber", m_lendersContactPrimaryTelephoneNumber);
            WriteAttribute(writer, "LoanEstimatedClosingDate", m_loanEstimatedClosingDate);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_LoanAmortizationType 
    {
        Undefined = 0,
        AdjustableRate,
        Fixed,
        GraduatedPaymentMortgage,
        GrowingEquityMortgage,
        OtherAmortizationType
    }
    public enum E_MortgageType 
    {
        Undefined = 0,
        Conventional,
        FarmersHomeAdministration,
        FHA,
        Other,
        VA,
        HELOC
    }
}
