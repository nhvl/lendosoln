using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Text;

using Mismo.MismoXmlElement;
using Mismo.Common;
namespace Mismo.Enveloping
{
	/// <summary>
	/// Summary description for MismoEnvelopingServer.
	/// </summary>
	public class MismoEnvelopingServer
	{
        private const string LOG_CONTEXT = "Mismo";
        public static XEResponseGroup Submit(string url, XERequestGroup request) 
        {
            XEResponseGroup response = null;
            try 
            {
                byte[] bytes = null;
                using (MemoryStream stream = new MemoryStream(5000)) 
                {
                    XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);
                                
                    request.GenerateXml(writer);
                    writer.Flush();
                
                    bytes = stream.GetBuffer();
                }


                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(url);
                webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
                webRequest.Method = "POST";

                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = bytes.Length;
                
                using (Stream stream = webRequest.GetRequestStream()) 
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                WebResponse webResponse = webRequest.GetResponse();

                StringBuilder sb = new StringBuilder();
                using (Stream stream = webResponse.GetResponseStream()) 
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);
                    while (size > 0) 
                    {
                        sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                    
                }
                webResponse.Close();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                response = new XEResponseGroup();
                response.Parse((XmlElement) doc.ChildNodes[0]);

            } 
            catch (Exception ) 
            {
            }

            return response;
        }
	}
}
