using System;
using System.IO;
using System.Xml;

using Mismo.MismoXmlElement;

namespace Mismo.Enveloping
{
	/// <summary>
	/// Summary description for BaseMismoEnvelopingPage.
	/// </summary>
	public class BaseMismoEnvelopingPage : System.Web.UI.Page
	{

        private void PageLoad(object sender, EventArgs e) 
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Request.InputStream);
//
//            Initialize();
//
            XERequestGroup requestGroup = new XERequestGroup();
            requestGroup.Parse((XmlElement) doc.ChildNodes[0]);

            XEResponseGroup responseGroup = Process(requestGroup);

            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "text/xml";

            if (null != responseGroup) 
            {
                XmlTextWriter writer = new XmlTextWriter(Response.Output);
                responseGroup.GenerateXml(writer);
                writer.Flush();
                
            }
            Response.End();
        }

        protected virtual void Initialize() 
        {
        }

        protected virtual XEResponseGroup Process(XERequestGroup requestGroup) 
        {
            throw new NotImplementedException("Need to implement Process() method.");
        }
        protected override void OnInit(EventArgs e) 
        {
            
            this.Load += new System.EventHandler(this.PageLoad);

            base.OnInit(e);
        }
	}
}
