﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEHelocOccurrences : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEHelocOccurrence m_helocOccurrence;

        #endregion

        #region Public Properties
        public void SetHelocOccurrence(XEHelocOccurrence helocOccurrence)
        {
            m_helocOccurrence = helocOccurrence;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("HELOC_OCCURRENCES");
            WriteElement(writer, m_helocOccurrence);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}