﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEGovernmentLoan : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string SectionOfActType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("GOVERNMENT_LOAN");
            WriteElement(writer, "SectionOfActType", SectionOfActType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}