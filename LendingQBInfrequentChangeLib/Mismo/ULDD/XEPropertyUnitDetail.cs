﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPropertyUnitDetail : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private string m_bedroomCount;
        private string m_propertyDwellingUnitEligibleRentAmount;
        #endregion

        #region Public Properties
        public string BedroomCount
        {
            get { return m_bedroomCount; }
            set { m_bedroomCount = value; }
        }
        public string PropertyDwellingUnitEligibleRentAmount
        {
            get { return m_propertyDwellingUnitEligibleRentAmount; }
            set { m_propertyDwellingUnitEligibleRentAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY_UNIT_DETAIL");
            WriteElement(writer, "BedroomCount", m_bedroomCount);
            WriteElement(writer, "PropertyDwellingUnitEligibleRentAmount", m_propertyDwellingUnitEligibleRentAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}