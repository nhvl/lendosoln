﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XESelectedLoanProduct : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEPriceLocks m_priceLocks;
        private XELoanProductDetail m_loanProductDetail;

        #endregion

        #region Public Properties
        public void SetPriceLocks(XEPriceLocks priceLocks)
        {
            m_priceLocks = priceLocks;
        }
        public void SetLoanProductDetail(XELoanProductDetail loanProductDetail)
        {
            m_loanProductDetail = loanProductDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("SELECTED_LOAN_PRODUCT");
            WriteElement(writer, m_priceLocks);
            WriteElement(writer, m_loanProductDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}