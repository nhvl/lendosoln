﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEMaturityRule : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string LoanMaturityDate { get; set; }
        public string LoanMaturityPeriodCount { get; set; }
        public string LoanMaturityPeriodType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("MATURITY_RULE");
            WriteElement(writer, "LoanMaturityDate", LoanMaturityDate);
            WriteElement(writer, "LoanMaturityPeriodCount", LoanMaturityPeriodCount);
            WriteElement(writer, "LoanMaturityPeriodType", LoanMaturityPeriodType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}