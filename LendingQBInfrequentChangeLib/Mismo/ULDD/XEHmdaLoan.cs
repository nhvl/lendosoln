﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEHmdaLoan : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string HMDA_HOEPALoanStatusIndicator { get; set; }
        public string HMDARateSpreadPercent { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("HMDA_LOAN");
            WriteElement(writer, "HMDA_HOEPALoanStatusIndicator", HMDA_HOEPALoanStatusIndicator);
            WriteElement(writer, "HMDARateSpreadPercent", HMDARateSpreadPercent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}