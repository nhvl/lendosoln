﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEParties : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_party = new ArrayList();

        #endregion

        #region Public Properties
        public void AddParty(XEParty party)
        {
            m_party.Add(party);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PARTIES");
            WriteElement(writer, m_party);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}