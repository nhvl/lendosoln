﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanIdentifier : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string InvestorCommitmentIdentifier { get; set; }
        public string InvestorContractIdentifier { get; set; }
        public string MERS_MINIdentifier { get; set; }
        public string SellerLoanIdentifier { get; set; }
        public string ServicerLoanIdentifier { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_IDENTIFIER");
            WriteElement(writer, "InvestorCommitmentIdentifier", InvestorCommitmentIdentifier);
            WriteElement(writer, "InvestorContractIdentifier", InvestorContractIdentifier);
            WriteElement(writer, "MERS_MINIdentifier", MERS_MINIdentifier);
            WriteElement(writer, "SellerLoanIdentifier", SellerLoanIdentifier);
            WriteElement(writer, "ServicerLoanIdentifier", ServicerLoanIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}