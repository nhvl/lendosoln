﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPartyRoleIdentifiers : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEPartyRoleIdentifier m_partyRoleIdentifier;

        #endregion

        #region Public Properties
        public void SetPartyRoleIdentifier(XEPartyRoleIdentifier partyRoleIdentifier)
        {
            m_partyRoleIdentifier = partyRoleIdentifier;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PARTY_ROLE_IDENTIFIERS");
            WriteElement(writer, m_partyRoleIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}