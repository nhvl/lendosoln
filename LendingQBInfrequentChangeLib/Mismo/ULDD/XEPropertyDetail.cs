﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPropertyDetail : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private E_AttachmentType m_attachmentType;
        private E_ConstructionMethodType m_constructionMethodType;
        private string m_financedUnitCount;
        private E_PropertyEstateType m_propertyEstateType;
        private bool m_propertyFloodInsuranceIndicator;
        private string m_propertyStructureBuiltYear;
        private E_PropertyUsageType m_propertyUsageType;

        #endregion

        #region Public Properties
        public E_AttachmentType AttachmentType
        {
            get { return m_attachmentType; }
            set { m_attachmentType = value; }
        }
        public E_ConstructionMethodType ConstructionMethodType
        {
            get { return m_constructionMethodType; }
            set { m_constructionMethodType = value; }
        }
        public string FinancedUnitCount
        {
            get { return m_financedUnitCount; }
            set { m_financedUnitCount = value; }
        }
        public E_PropertyEstateType PropertyEstateType
        {
            get { return m_propertyEstateType; }
            set { m_propertyEstateType = value; }
        }
        public bool PropertyFloodInsuranceIndicator
        {
            get { return m_propertyFloodInsuranceIndicator; }
            set { m_propertyFloodInsuranceIndicator = value; }
        }
        public string PropertyStructureBuiltYear
        {
            get { return m_propertyStructureBuiltYear; }
            set { m_propertyStructureBuiltYear = value; }
        }
        public E_PropertyUsageType PropertyUsageType
        {
            get { return m_propertyUsageType; }
            set { m_propertyUsageType = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY_DETAIL");
            WriteElement(writer, "AttachmentType", m_attachmentType);
            WriteElement(writer, "ConstructionMethodType", m_constructionMethodType);
            WriteElement(writer, "FinancedUnitCount", m_financedUnitCount);
            WriteElement(writer, "PropertyEstateType", m_propertyEstateType);
            WriteElement(writer, "PropertyFloodInsuranceIndicator", m_propertyFloodInsuranceIndicator.ToString().ToLower());
            WriteElement(writer, "PropertyStructureBuiltYear", m_propertyStructureBuiltYear);
            WriteElement(writer, "PropertyUsageType", m_propertyUsageType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_AttachmentType
    {
        None,
        Attached,
        Detached,
        SemiDetached
    }

    public enum E_ConstructionMethodType
    {
        None,
        Manufactured,
        Modular,
        SiteBuilt
    }

    public enum E_PropertyUsageType
    {
        None,
        Investment,
        PrimaryResidence,
        SecondHome
    }

    public enum E_PropertyEstateType
    {
        None,
        FeeSimple,
        Leasehold,
        Other
    }

}