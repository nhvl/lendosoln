﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEGovernmentMonitoringDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_HMDAEthnicityType HMDAEthnicityType { get; set; }
        public E_GenderType GenderType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("GOVERNMENT_MONITORING_DETAIL");
            WriteElement(writer, "GenderType", GenderType);
            WriteElement(writer, "HMDAEthnicityType", HMDAEthnicityType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_GenderType
    {
        None,
        Female,
        InformationNotProvidedUnknown,
        Male,
        NotApplicable
    }

    public enum E_HMDAEthnicityType
    {
        None,
        HispanicOrLatino,
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        NotApplicable,
        NotHispanicOrLatino
    }
}