﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEHudaRaces : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEHudaRace m_hudaRace;

        #endregion

        #region Public Properties
        public void SetHudaRace(XEHudaRace hudaRace)
        {
            m_hudaRace = hudaRace;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("HMDA_RACES");
            WriteElement(writer, m_hudaRace);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}