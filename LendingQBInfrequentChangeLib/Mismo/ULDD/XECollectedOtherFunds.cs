﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECollectedOtherFunds : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XECollectedOtherFund m_collectedOtherFund;

        #endregion

        #region Public Properties
        public void SetCollectedOtherFund(XECollectedOtherFund collectedOtherFund)
        {
            m_collectedOtherFund = collectedOtherFund;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("COLLECTED_OTHER_FUNDS");
            WriteElement(writer, m_collectedOtherFund);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}