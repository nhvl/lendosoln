﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEFormSpecificContents : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEFormSpecificContent m_formSpecificContent;

        #endregion

        #region Public Properties
        public void SetFormSpecificContent(XEFormSpecificContent formSpecificContent)
        {
            m_formSpecificContent = formSpecificContent;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("FORM_SPECIFIC_CONTENTS");
            WriteElement(writer, m_formSpecificContent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}