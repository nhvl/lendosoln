﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInterestRatePerChangeAdjustmentRule : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string AdjustmentRuleType { get; set; }
        public string PerChangeMaximumDecreaseRatePercent { get; set; }
        public string PerChangeMaximumIncreaseRatePercent { get; set; }
        public string PerChangeRateAdjustmentEffectiveDate { get; set; }
        public string PerChangeRateAdjustmentFrequencyMonthsCount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE");
            WriteElement(writer, "AdjustmentRuleType", AdjustmentRuleType);
            WriteElement(writer, "PerChangeMaximumDecreaseRatePercent", PerChangeMaximumDecreaseRatePercent);
            WriteElement(writer, "PerChangeMaximumIncreaseRatePercent", PerChangeMaximumIncreaseRatePercent);
            WriteElement(writer, "PerChangeRateAdjustmentEffectiveDate", PerChangeRateAdjustmentEffectiveDate);
            WriteElement(writer, "PerChangeRateAdjustmentFrequencyMonthsCount", PerChangeRateAdjustmentFrequencyMonthsCount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}