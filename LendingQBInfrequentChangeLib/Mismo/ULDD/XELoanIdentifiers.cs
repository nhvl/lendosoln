﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XELoanIdentifiers : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_loanIdentifier = new ArrayList();

        #endregion

        #region Public Properties
        public void AddLoanIdentifier(XELoanIdentifier loanIdentifier)
        {
            m_loanIdentifier.Add(loanIdentifier);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_IDENTIFIERS");
            WriteElement(writer, m_loanIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}