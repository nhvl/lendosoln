﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEHeloc : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEHelocOccurrences m_helocOccurrences;

        #endregion

        #region Public Properties
        public void SetHelocOccurrences(XEHelocOccurrences helocOccurrences)
        {
            m_helocOccurrences = helocOccurrences;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("HELOC");
            WriteElement(writer, m_helocOccurrences);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}