﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanProgram : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string LoanProgramIdentifier { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_PROGRAM");
            WriteElement(writer, "LoanProgramIdentifier", LoanProgramIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}