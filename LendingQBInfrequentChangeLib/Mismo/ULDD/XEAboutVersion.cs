﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAboutVersion : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private string m_aboutVersionIdentifier = "FNM 1.0";
        private string m_createdDateTime;
        #endregion

        #region Public Properties
        public string CreatedDateTime
        {
            get { return m_createdDateTime; }
            set { m_createdDateTime = value; }
        }
        public string AboutVersionIdentifier
        {
            get { return m_aboutVersionIdentifier; }
            set { m_aboutVersionIdentifier = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ABOUT_VERSION");
            WriteElement(writer, "AboutVersionIdentifier", m_aboutVersionIdentifier);
            WriteElement(writer, "CreatedDatetime", m_createdDateTime.ToString());
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}