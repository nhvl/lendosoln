﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEBorrowerDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string BorrowerAgeAtApplicationYearsCount { get; set; }
        public string BorrowerBirthDate { get; set; }
        public string BorrowerClassificationType { get; set; }
        public bool BorrowerMailToAddressSameasPropertyIndicator { get; set; }
        public string BorrowerQualifyingIncomeAmount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("BORROWER_DETAIL");
            WriteElement(writer, "BorrowerAgeAtApplicationYearsCount", BorrowerAgeAtApplicationYearsCount);
            WriteElement(writer, "BorrowerBirthDate", BorrowerBirthDate);
            WriteElement(writer, "BorrowerClassificationType", BorrowerClassificationType);
            WriteElement(writer, "BorrowerMailToAddressSameAsPropertyIndicator", BorrowerMailToAddressSameasPropertyIndicator.ToString().ToLower());
            WriteElement(writer, "BorrowerQualifyingIncomeAmount", BorrowerQualifyingIncomeAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}