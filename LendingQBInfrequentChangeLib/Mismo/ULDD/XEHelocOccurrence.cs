﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEHelocOccurrence : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string CurrentHELOCMaximumBalanceAmount { get; set; }
        public string HELOCBalanceAmount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("HELOC_OCCURRENCE");
            WriteElement(writer, "CurrentHELOCMaximumBalanceAmount", CurrentHELOCMaximumBalanceAmount);
            WriteElement(writer, "HELOCBalanceAmount", HELOCBalanceAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}