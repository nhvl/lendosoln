﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEQualification : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string TotalLiabilitiesMonthlyPaymentAmount { get; set; }
        public string TotalMonthlyIncomeAmount { get; set; }
        public string TotalMonthlyProposedHousingExpenseAmount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("QUALIFICATION");
            WriteElement(writer, "TotalLiabilitiesMonthlyPaymentAmount", TotalLiabilitiesMonthlyPaymentAmount);
            WriteElement(writer, "TotalMonthlyIncomeAmount", TotalMonthlyIncomeAmount);
            WriteElement(writer, "TotalMonthlyProposedHousingExpenseAmount", TotalMonthlyProposedHousingExpenseAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}