﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAboutVersions : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAboutVersion m_aboutVersion;

        #endregion

        #region Public Properties
        public void SetAboutVersion(XEAboutVersion aboutVersion)
        {
            m_aboutVersion = aboutVersion;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ABOUT_VERSIONS");
            WriteElement(writer, m_aboutVersion);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}