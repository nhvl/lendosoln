﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAutomatedUnderwriting : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string AutomatedUnderwritingCaseIdentifier { get; set; }
        public E_AutomatedUnderwritingRecommendationDescription AutomatedUnderwritingRecommendationDescription { get; set; }
        public E_AutomatedUnderwritingSystemType AutomatedUnderwritingSystemType { get; set; }
        public E_AutomatedUnderwritingSystemType AutomatedUnderwritingSystemTypeOtherDescription { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("AUTOMATED_UNDERWRITING");
            WriteElement(writer, "AutomatedUnderwritingCaseIdentifier", AutomatedUnderwritingCaseIdentifier);
            WriteElement(writer, "AutomatedUnderwritingRecommendationDescription", AutomatedUnderwritingRecommendationDescription);
            WriteElement(writer, "AutomatedUnderwritingSystemType", AutomatedUnderwritingSystemType);
            WriteElement(writer, "AutomatedUnderwritingSystemTypeOtherDescription", AutomatedUnderwritingSystemTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_AutomatedUnderwritingSystemType
    {
        None,
        Assetwise,
        Capstone,
        Clues,
        DesktopUnderwriter,
        ECS,
        LoanProspector,
        Other,
        Strategyware,
        Zippy,
        FirstMortgageCreditScore 
    }

    public enum E_AutomatedUnderwritingRecommendationDescription
    {
        None,
        ApproveEligible,
        ApproveIneligible,
        EAIEligible,
        EAIIEligible,
        EAIIIEligible,
        ReferEligible,
        ReferIneligible,
        ReferWithCautionIV,
        Accept,
        CautionEligibleForAMinus,
        C1Caution,
        Unknown,
        OutofScope
    }
}