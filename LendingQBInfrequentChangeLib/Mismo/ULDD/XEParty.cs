﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEParty : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XERoles m_roles;
        private XEAddresses m_addresses;
        private ArrayList m_individual = new ArrayList();
        private XETaxpayerIdentifiers m_taxpayerIdentifiers;
        private XELegalEntity m_legalEntity;

        #endregion

        #region Public Properties
        public void SetRoles(XERoles roles)
        {
            m_roles = roles;
        }
        public void SetAddresses(XEAddresses addresses)
        {
            m_addresses = addresses;
        }
        public void AddIndividual(XEIndividual individual)
        {
            m_individual.Add(individual);
        }
        public void SetTaxpayerIdentifiers(XETaxpayerIdentifiers taxpayerIdentifiers)
        {
            m_taxpayerIdentifiers = taxpayerIdentifiers;
        }
        public void SetLegalEntity(XELegalEntity legalEntity)
        {
            m_legalEntity = legalEntity;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PARTY");
            WriteElement(writer, m_individual);
            WriteElement(writer, m_addresses);
            WriteElement(writer, m_legalEntity);
            WriteElement(writer, m_roles);
            WriteElement(writer, m_taxpayerIdentifiers);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}