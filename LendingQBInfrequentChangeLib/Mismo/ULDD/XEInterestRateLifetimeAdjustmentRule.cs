﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInterestRateLifetimeAdjustmentRule : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string CeilingRatePercent { get; set; }
        public string FirstRateChangePaymentEffectiveDate { get; set; }
        public string FloorRatePercent { get; set; }
        public string InterestRateRoundingPercent { get; set; }
        public E_InterestRateRoundingType InterestRateRoundingType { get; set; }
        public string MarginRatePercent { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE");
            WriteElement(writer, "CeilingRatePercent", CeilingRatePercent);
            WriteElement(writer, "FirstRateChangePaymentEffectiveDate", FirstRateChangePaymentEffectiveDate);
            WriteElement(writer, "FloorRatePercent", FloorRatePercent);
            WriteElement(writer, "InterestRateRoundingPercent", InterestRateRoundingPercent);
            WriteElement(writer, "InterestRateRoundingType", InterestRateRoundingType);
            WriteElement(writer, "MarginRatePercent", MarginRatePercent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_InterestRateRoundingType
    {
        None,
        Down,
        Nearest,
        NoRounding,
        Up
    }
}