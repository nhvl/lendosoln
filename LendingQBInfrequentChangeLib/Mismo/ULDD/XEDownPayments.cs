﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEDownPayments : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_downPayment = new ArrayList();

        #endregion

        #region Public Properties
        public void AddDownPayment(XEDownPayment downPayment)
        {
            m_downPayment.Add(downPayment);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DOWN_PAYMENTS");
            WriteElement(writer, m_downPayment);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}