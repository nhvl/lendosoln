﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEName : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string SuffixName { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("NAME");
            WriteElement(writer, "FirstName", FirstName);
            WriteElement(writer, "LastName", LastName);
            WriteElement(writer, "MiddleName", MiddleName);
            WriteElement(writer, "SuffixName", SuffixName);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}