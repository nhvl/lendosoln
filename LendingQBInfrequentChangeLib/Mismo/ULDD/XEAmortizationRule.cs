﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAmortizationRule : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string AdjustmentRuleType { get; set; }
        public string LoanAmortizationPeriodCount { get; set; }
        public string LoanAmortizationPeriodType { get; set; }
        public E_LoanAmortizationType LoanAmortizationType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("AMORTIZATION_RULE");
            WriteElement(writer, "AdjustmentRuleType", AdjustmentRuleType);
            WriteElement(writer, "LoanAmortizationPeriodCount", LoanAmortizationPeriodCount);
            WriteElement(writer, "LoanAmortizationPeriodType", LoanAmortizationPeriodType);
            WriteElement(writer, "LoanAmortizationType", LoanAmortizationType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_LoanAmortizationType
    {
        None,
        AdjustableRate,
        Fixed,
        GraduatedPaymentARM,
        GraduatedPaymentMortgage,
        GrowingEquityMortgage,
        Step
    }
}