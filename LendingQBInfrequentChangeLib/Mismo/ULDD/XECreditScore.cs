﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XECreditScore : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_creditScoreDetail = new ArrayList();

        #endregion

        #region Public Properties
        public void AddCreditScoreDetail(XECreditScoreDetail creditScoreDetail)
        {
            m_creditScoreDetail.Add(creditScoreDetail);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("CREDIT_SCORE");
            WriteElement(writer, m_creditScoreDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}