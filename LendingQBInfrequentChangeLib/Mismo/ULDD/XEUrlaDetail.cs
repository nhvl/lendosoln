﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEUrlaDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string PurchasePriceAmount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("URLA_DETAIL");
            WriteElement(writer, "PurchasePriceAmount", PurchasePriceAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}