﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPaymentComponentBreakouts : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEPaymentComponentBreakout m_paymentComponentBreakout;

        #endregion

        #region Public Properties
        public void SetPaymentComponentBreakout(XEPaymentComponentBreakout paymentComponentBreakout)
        {
            m_paymentComponentBreakout = paymentComponentBreakout;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PAYMENT_COMPONENT_BREAKOUTS");
            WriteElement(writer, m_paymentComponentBreakout);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}