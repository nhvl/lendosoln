﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECollectedOtherFund : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string OtherFundsCollectedAtClosingAmount { get; set; }
        public string OtherFundsCollectedAtClosingType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("COLLECTED_OTHER_FUND");
            WriteElement(writer, "OtherFundsCollectedAtClosingAmount", OtherFundsCollectedAtClosingAmount);
            WriteElement(writer, "OtherFundsCollectedAtClosingType", OtherFundsCollectedAtClosingType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}