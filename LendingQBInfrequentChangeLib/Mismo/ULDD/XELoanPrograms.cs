﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanPrograms : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XELoanProgram m_loanProgram;

        #endregion

        #region Public Properties
        public void SetLoanProgram(XELoanProgram loanProgram)
        {
            m_loanProgram = loanProgram;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_PROGRAMS");
            WriteElement(writer, m_loanProgram);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}