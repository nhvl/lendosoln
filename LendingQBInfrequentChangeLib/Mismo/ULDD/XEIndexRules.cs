﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEIndexRules : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEIndexRule m_indexRule;

        #endregion

        #region Public Properties
        public void SetIndexRule(XEIndexRule indexRule)
        {
            m_indexRule = indexRule;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INDEX_RULES");
            WriteElement(writer, m_indexRule);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}