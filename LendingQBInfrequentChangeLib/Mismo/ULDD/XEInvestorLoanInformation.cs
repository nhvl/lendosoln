﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInvestorLoanInformation : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string RelatedInvestorLoanIdentifier { get; set; }
        public string RelatedLoanInvestorType { get; set; }

        public string BaseGuarantyFeePercent { get; set; }
        public string GuarantyFeeAfterAlternatePaymentMethodPercent { get; set; }
        public string GuarantyFeePercent { get; set; }
        public E_InvestorCollateralProgramIdentifier InvestorCollateralProgramIdentifier { get; set; }
        public string InvestorOwnershipPercent { get; set; }
        public string InvestorProductPlanIdentifier { get; set; }
        public string InvestorRemittanceDay { get; set; }
        public E_InvestorRemittanceType InvestorRemittanceType { get; set; }
        public string LoanAcquisitionScheduledUPBAmount { get; set; }
        public string LoanBuyupBuydownBasisPointNumber { get; set; }
        public E_LoanBuyupBuydownType LoanBuyupBuydownType { get; set; }
        public E_LoanDefaultLossPartyType LoanDefaultLossPartyType { get; set; }
        public E_REOMarketingPartyType REOMarketingPartyType { get; set; }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INVESTOR_LOAN_INFORMATION");
            WriteElement(writer, "BaseGuarantyFeePercent", BaseGuarantyFeePercent);
            WriteElement(writer, "GuarantyFeeAfterAlternatePaymentMethodPercent", GuarantyFeeAfterAlternatePaymentMethodPercent);
            WriteElement(writer, "GuarantyFeePercent", GuarantyFeePercent);
            WriteElement(writer, "InvestorCollateralProgramIdentifier", InvestorCollateralProgramIdentifier);
            WriteElement(writer, "InvestorOwnershipPercent", InvestorOwnershipPercent);
            WriteElement(writer, "InvestorProductPlanIdentifier", InvestorProductPlanIdentifier);
            WriteElement(writer, "InvestorRemittanceDay", InvestorRemittanceDay);
            WriteElement(writer, "InvestorRemittanceType", InvestorRemittanceType);
            WriteElement(writer, "RelatedInvestorLoanIdentifier", RelatedInvestorLoanIdentifier);
            WriteElement(writer, "RelatedLoanInvestorType", RelatedLoanInvestorType);
            WriteElement(writer, "LoanAcquisitionScheduledUPBAmount", LoanAcquisitionScheduledUPBAmount);
            WriteElement(writer, "LoanBuyupBuydownBasisPointNumber", LoanBuyupBuydownBasisPointNumber);
            WriteElement(writer, "LoanBuyupBuydownType", LoanBuyupBuydownType);
            WriteElement(writer, "LoanDefaultLossPartyType", LoanDefaultLossPartyType);
            WriteElement(writer, "REOMarketingPartyType", REOMarketingPartyType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_InvestorCollateralProgramIdentifier
    {
        None,
        PropertyInspectionAlternative,
        PropertyInspectionWaiver
    }

    public enum E_InvestorRemittanceType
    {
        None,
        ActualInterestActualPrincipal,
        ScheduledInterestActualPrincipal,
        ScheduledInterestScheduledPrincipal
    }

    public enum E_LoanBuyupBuydownType
    {
        None,
        Buyup,
        Buydown,
        BuyupBuydownDoesNotApply
    }

    public enum E_LoanDefaultLossPartyType
    {
        None,
        Investor,
        Lender,
        Shared
    }

    public enum E_REOMarketingPartyType
    {
        None,
        Investor,
        Lender,
        Other,
        Servicer,
        Unknown,
        Seller
    }
}