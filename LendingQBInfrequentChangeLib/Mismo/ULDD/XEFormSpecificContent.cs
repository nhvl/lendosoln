﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEFormSpecificContent : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEUrla m_urla;

        #endregion

        #region Public Properties
        public void SetUrla(XEUrla urla)
        {
            m_urla = urla;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("FORM_SPECIFIC_CONTENT");
            WriteElement(writer, m_urla);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}