﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XERateOrPaymentChangeOccurences : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XERateOrPaymentChangeOccurence m_rateOrPaymentChangeOccurence;

        #endregion

        #region Public Properties
        public void SetRateOrPaymentChangeOccurence(XERateOrPaymentChangeOccurence rateOrPaymentChangeOccurence)
        {
            m_rateOrPaymentChangeOccurence = rateOrPaymentChangeOccurence;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("RATE_OR_PAYMENT_CHANGE_OCCURRENCES");
            WriteElement(writer, m_rateOrPaymentChangeOccurence);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}