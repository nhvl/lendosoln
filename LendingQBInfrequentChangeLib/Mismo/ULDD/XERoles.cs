﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XERoles : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_role = new ArrayList();
        private XEPartyRoleIdentifiers m_partyRoleIdentifiers;

        #endregion

        #region Public Properties
        public void AddRole(XERole role)
        {
            m_role.Add(role);
        }
        public void SetPartyRoleIdentifiers(XEPartyRoleIdentifiers partyRoleIdentifiers)
        {
            m_partyRoleIdentifiers = partyRoleIdentifiers;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ROLES");
            WriteElement(writer, m_partyRoleIdentifiers);
            WriteElement(writer, m_role);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}