﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELtv : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string BaseLTVRatioPercent { get; set; }
        public string LTVRatioPercent { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LTV");
            WriteElement(writer, "BaseLTVRatioPercent", BaseLTVRatioPercent);
            WriteElement(writer, "LTVRatioPercent", LTVRatioPercent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}