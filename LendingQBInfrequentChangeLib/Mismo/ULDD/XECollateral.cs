﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECollateral : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEProperties m_properties;

        #endregion

        #region Public Properties
        public void SetProperties(XEProperties properties)
        {
            m_properties = properties;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("COLLATERAL");
            WriteElement(writer, m_properties);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}