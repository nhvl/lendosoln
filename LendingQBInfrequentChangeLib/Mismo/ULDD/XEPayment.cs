﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPayment : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEPaymentRule m_paymentRule;
        private XEPaymentSummary m_paymentSummary;
        private XEPaymentComponentBreakouts m_paymentComponentBreakouts;

        #endregion

        #region Public Properties
        public void SetPaymentRule(XEPaymentRule paymentRule)
        {
            m_paymentRule = paymentRule;
        }
        public void SetPaymentSummary(XEPaymentSummary paymentSummary)
        {
            m_paymentSummary = paymentSummary;
        }
        public void SetPaymentComponentBreakouts(XEPaymentComponentBreakouts paymentComponentBreakouts)
        {
            m_paymentComponentBreakouts = paymentComponentBreakouts;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PAYMENT");
            WriteElement(writer, m_paymentComponentBreakouts);
            WriteElement(writer, m_paymentRule);
            WriteElement(writer, m_paymentSummary);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}