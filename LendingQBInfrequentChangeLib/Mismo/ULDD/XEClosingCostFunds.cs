﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEClosingCostFunds : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_closingCostFund = new ArrayList();

        #endregion

        #region Public Properties
        public void AddClosingCostFund(XEClosingCostFund closingCostFund)
        {
            m_closingCostFund.Add(closingCostFund);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("CLOSING_COST_FUNDS");
            WriteElement(writer, m_closingCostFund);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}