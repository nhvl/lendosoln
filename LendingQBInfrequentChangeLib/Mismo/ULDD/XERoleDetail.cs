﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XERoleDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string FullName { get; set; }
        public string PartyRoleType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ROLE_DETAIL");
            WriteElement(writer, "FullName", FullName);
            WriteElement(writer, "PartyRoleType", PartyRoleType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}