﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInterestCalculation : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEInterestCalculationRules m_interestCalculationRules;

        #endregion

        #region Public Properties
        public void SetInterestCalculationRules(XEInterestCalculationRules interestCalculationRules)
        {
            m_interestCalculationRules = interestCalculationRules;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_CALCULATION");
            WriteElement(writer, m_interestCalculationRules);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}