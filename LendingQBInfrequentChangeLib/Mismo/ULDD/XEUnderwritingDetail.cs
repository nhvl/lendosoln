﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEUnderwritingDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string LoanManualUnderwritingIndicator { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("UNDERWRITING_DETAIL");
            WriteElement(writer, "LoanManualUnderwritingIndicator", LoanManualUnderwritingIndicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}