﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAvm : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private E_AVMModelNameType m_aVMModelNameType;
        private E_AVMModelNameType m_aVMModelNameTypeOtherDescription;
        #endregion

        #region Public Properties
        public E_AVMModelNameType AVMModelNameType
        {
            get { return m_aVMModelNameType; }
            set { m_aVMModelNameType = value; }
        }
        public E_AVMModelNameType AVMModelNameTypeOtherDescription
        {
            get { return m_aVMModelNameTypeOtherDescription; }
            set { m_aVMModelNameTypeOtherDescription = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("AVM");
            WriteElement(writer, "AVMModelNameType", m_aVMModelNameType);
            WriteElement(writer, "AVMModelNameTypeOtherDescription", m_aVMModelNameTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_AVMModelNameType
    {
        None,
        AutomatedPropertyService,
        Casa,
        FidelityHansen,
        HomePriceAnalyzer,
        HomePriceIndex,
        HomeValueExplorer,
        Indicator,
        NetValue,
        Other,
        Pass,
        PropertySurveyAnalysisReport,
        ValueFinder,
        ValuePoint,
        ValuePoint4,
        ValuePointPlus,
        ValueSure,
        ValueWizard,
        ValueWizardPlus,
        VeroIndexPlus,
        VeroValue,
        MTM 
    }
}