﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEProperties : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEProperty m_property;

        #endregion

        #region Public Properties
        public void SetProperty(XEProperty property)
        {
            m_property = property;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTIES");
            WriteElement(writer, m_property);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}