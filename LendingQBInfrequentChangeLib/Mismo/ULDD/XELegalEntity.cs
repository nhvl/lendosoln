﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XELegalEntity : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XELegalEntityDetail m_legalEntityDetail;

        #endregion

        #region Public Properties
        public void SetLegalEntityDetail(XELegalEntityDetail legalEntityDetail)
        {
            m_legalEntityDetail = legalEntityDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LEGAL_ENTITY");
            WriteElement(writer, m_legalEntityDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}