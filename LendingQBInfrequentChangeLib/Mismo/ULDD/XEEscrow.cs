﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEEscrow : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEEscrowItems m_escrowItems;

        #endregion

        #region Public Properties
        public void SetEscrowItems(XEEscrowItems escrowItems)
        {
            m_escrowItems = escrowItems;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ESCROW");
            WriteElement(writer, m_escrowItems);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}