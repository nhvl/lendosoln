﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPaymentRule : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string InitialPrincipalAndInterestPaymentAmount { get; set; }
        public string PaymentFrequencyType { get; set; }
        public string ScheduledFirstPaymentDate { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PAYMENT_RULE");
            WriteElement(writer, "InitialPrincipalAndInterestPaymentAmount", InitialPrincipalAndInterestPaymentAmount);
            WriteElement(writer, "PaymentFrequencyType", PaymentFrequencyType);
            WriteElement(writer, "ScheduledFirstPaymentDate", ScheduledFirstPaymentDate);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}