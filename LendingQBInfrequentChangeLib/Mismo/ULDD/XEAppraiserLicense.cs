﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAppraiserLicense : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string AppraiserLicenseIdentifier { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("APPRAISER_LICENSE");
            WriteElement(writer, "AppraiserLicenseIdentifier", AppraiserLicenseIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}