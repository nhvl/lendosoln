﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XELoanComments : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XELoanComment m_loanComment;

        #endregion

        #region Public Properties
        public void SetLoanComment(XELoanComment loanComment)
        {
            m_loanComment = loanComment;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_COMMENTS");
            WriteElement(writer, m_loanComment);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}