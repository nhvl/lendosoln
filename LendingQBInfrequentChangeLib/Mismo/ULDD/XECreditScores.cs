﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XECreditScores : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_creditScore = new ArrayList();

        #endregion

        #region Public Properties
        public void AddCreditScore(XECreditScore creditScore)
        {
            m_creditScore.Add(creditScore);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("CREDIT_SCORES");
            WriteElement(writer, m_creditScore);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}