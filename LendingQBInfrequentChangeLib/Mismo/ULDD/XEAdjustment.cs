﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAdjustment : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEInterestRateAdjustment m_interestRateAdjustment;
        private XERateOrPaymentChangeOccurences m_rateOrPaymentChangeOccurences;

        #endregion

        #region Public Properties
        public void SetInterestRateAdjustment(XEInterestRateAdjustment interestRateAdjustment)
        {
            m_interestRateAdjustment = interestRateAdjustment;
        }
        public void SetRateOrPaymentChangeOccurences(XERateOrPaymentChangeOccurences rateOrPaymentChangeOccurences)
        {
            m_rateOrPaymentChangeOccurences = rateOrPaymentChangeOccurences;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ADJUSTMENT");
            WriteElement(writer, m_interestRateAdjustment);
            WriteElement(writer, m_rateOrPaymentChangeOccurences);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}