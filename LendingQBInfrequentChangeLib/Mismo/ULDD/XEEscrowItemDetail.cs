﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEEscrowItemDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string EscrowItemType { get; set; }
        public string EscrowMonthlyPaymentAmount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ESCROW_ITEM_DETAIL");
            WriteElement(writer, "EscrowItemType", EscrowItemType);
            WriteElement(writer, "EscrowMonthlyPaymentAmount", EscrowMonthlyPaymentAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}