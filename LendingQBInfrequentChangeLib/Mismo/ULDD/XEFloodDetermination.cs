﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEFloodDetermination : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEFloodDeterminationDetail m_floodDeterminationDetail;

        #endregion

        #region Public Properties
        public void SetFloodDeterminationDetail(XEFloodDeterminationDetail floodDeterminationDetail)
        {
            m_floodDeterminationDetail = floodDeterminationDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("FLOOD_DETERMINATION");
            WriteElement(writer, m_floodDeterminationDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}