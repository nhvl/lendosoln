﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XERole : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XERoleDetail m_roleDetail;
        private ArrayList m_borrower = new ArrayList();
        private XEAppraiser m_appraiser;
        private XEAppraiserSupervisor m_appraiserSupervisor;
        private XELoanOriginator m_loanOriginator;

        #endregion

        #region Public Properties
        public void SetRoleDetail(XERoleDetail roleDetail)
        {
            m_roleDetail = roleDetail;
        }
        public void AddBorrower(XEBorrower borrower)
        {
            m_borrower.Add(borrower);
        }
        public void SetAppraiserSupervisor(XEAppraiserSupervisor appraiserSupervisor)
        {
            m_appraiserSupervisor = appraiserSupervisor;
        }
        public void SetAppraiser(XEAppraiser appraiser)
        {
            m_appraiser = appraiser;
        }
        public void SetLoanOriginator(XELoanOriginator loanOriginator)
        {
            m_loanOriginator = loanOriginator;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ROLE");
            WriteElement(writer, m_borrower);
            WriteElement(writer, m_appraiser);
            WriteElement(writer, m_appraiserSupervisor);
            WriteElement(writer, m_loanOriginator);
            WriteElement(writer, m_roleDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}