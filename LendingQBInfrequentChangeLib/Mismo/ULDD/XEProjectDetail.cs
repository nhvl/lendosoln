﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEProjectDetail : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private E_CondominiumProjectStatusType m_condominiumProjectStatusType;
        private E_ProjectAttachmentType m_projectAttachmentType;
        private string m_fNMCondominiumProjectManagerProjectIdentifier;
        private string m_projectClassificationIdentifier;
        private E_ProjectDesignType m_projectDesignType;
        private string m_projectDwellingUnitCount;
        private string m_projectDwellingUnitsSoldCount;
        private E_ProjectLegalStructureType m_projectLegalStructureType;
        private string m_projectName;
        private bool m_pUDIndicator;

        #endregion

        #region Public Properties
        public E_CondominiumProjectStatusType CondominiumProjectStatusType
        {
            get { return m_condominiumProjectStatusType; }
            set { m_condominiumProjectStatusType = value; }
        }
        public E_ProjectAttachmentType ProjectAttachmentType
        {
            get { return m_projectAttachmentType; }
            set { m_projectAttachmentType = value; }
        }
        public string FNMCondominiumProjectManagerProjectIdentifier
        {
            get { return m_fNMCondominiumProjectManagerProjectIdentifier; }
            set { m_fNMCondominiumProjectManagerProjectIdentifier = value; }
        }
        public string ProjectClassificationIdentifier
        {
            get { return m_projectClassificationIdentifier; }
            set { m_projectClassificationIdentifier = value; }
        }
        public E_ProjectDesignType ProjectDesignType
        {
            get { return m_projectDesignType; }
            set { m_projectDesignType = value; }
        }
        public string ProjectDwellingUnitCount
        {
            get { return m_projectDwellingUnitCount; }
            set { m_projectDwellingUnitCount = value; }
        }
        public string ProjectDwellingUnitsSoldCount
        {
            get { return m_projectDwellingUnitsSoldCount; }
            set { m_projectDwellingUnitsSoldCount = value; }
        }
        public E_ProjectLegalStructureType ProjectLegalStructureType
        {
            get { return m_projectLegalStructureType; }
            set { m_projectLegalStructureType = value; }
        }
        public string ProjectName
        {
            get { return m_projectName; }
            set { m_projectName = value; }
        }
        public bool PUDIndicator
        {
            get { return m_pUDIndicator; }
            set { m_pUDIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROJECT_DETAIL");
            WriteElement(writer, "CondominiumProjectStatusType", m_condominiumProjectStatusType);
            WriteElement(writer, "FNMCondominiumProjectManagerProjectIdentifier", m_fNMCondominiumProjectManagerProjectIdentifier);
            WriteElement(writer, "ProjectAttachmentType", m_projectAttachmentType);
            WriteElement(writer, "ProjectClassificationIdentifier", m_projectClassificationIdentifier);
            WriteElement(writer, "ProjectDesignType", m_projectDesignType);
            WriteElement(writer, "ProjectDwellingUnitCount", m_projectDwellingUnitCount);
            WriteElement(writer, "ProjectDwellingUnitsSoldCount", m_projectDwellingUnitsSoldCount);
            WriteElement(writer, "ProjectLegalStructureType", m_projectLegalStructureType);
            WriteElement(writer, "ProjectName", m_projectName);
            WriteElement(writer, "PUDIndicator", m_pUDIndicator.ToString().ToLower());
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_CondominiumProjectStatusType
    {
        None,
        Established,
        New
    }

    public enum E_ProjectAttachmentType
    {
        None,
        Attached,
        Detached
    }

    public enum E_ProjectClassificationIdentifier
    {
        None,
        StreamlinedReview,
        FullReview,
        CondominiumProjectManagerReview,
        ProjectEligibilityReviewService,
        FHA_Approved,
        ExemptFromReview,
        Blank
    }

    public enum E_ProjectDesignType
    {
        None,
        GardenProject,
        HighriseProject,
        MidriseProject,
        TownhouseRowhouse
    }

    public enum E_ProjectLegalStructureType
    {
        None,
        Condominium,
        Cooperative
    }
}