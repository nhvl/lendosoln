﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECounselingConfirmation : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_CounselingConfirmationType CounselingConfirmationType { get; set; }
        public E_CounselingConfirmationTypeOtherDescription CounselingConfirmationTypeOtherDescription { get; set; }
        public E_CounselingFormatType CounselingFormatType { get; set; }
        public E_CounselingFormatTypeOtherDescription CounselingFormatTypeOtherDescription { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("COUNSELING_CONFIRMATION");
            WriteElement(writer, "CounselingConfirmationType", CounselingConfirmationType);
            WriteElement(writer, "CounselingConfirmationTypeOtherDescription", CounselingConfirmationTypeOtherDescription);
            WriteElement(writer, "CounselingFormatType", CounselingFormatType);
            WriteElement(writer, "CounselingFormatTypeOtherDescription", CounselingFormatTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_CounselingConfirmationType
    {
        None,
        GovernmentAgency,
        HUDApprovedCounselingAgency,
        LenderTrainedCounseling,
        NoBorrowerCounseling,
        Other
    }

    public enum E_CounselingConfirmationTypeOtherDescription
    {
        None,
        BorrowerDidNotParticipate,
        MortgageInsuranceCompany,
        NonProfitOrganization
    }

    public enum E_CounselingFormatType
    {
        None,
        BorrowerEducationNotRequired,
        Classroom,
        HomeStudy,
        Individual,
        Other
    }

    public enum E_CounselingFormatTypeOtherDescription
    {
        None,
        BorrowerDidNotParticipate
    }
}