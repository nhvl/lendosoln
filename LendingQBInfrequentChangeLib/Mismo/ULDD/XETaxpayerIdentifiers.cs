﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XETaxpayerIdentifiers : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_taxpayerIdentifier = new ArrayList();

        #endregion

        #region Public Properties
        public void AddTaxpayerIdentifier(XETaxpayerIdentifier taxpayerIdentifier)
        {
            m_taxpayerIdentifier.Add(taxpayerIdentifier);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("TAXPAYER_IDENTIFIERS");
            WriteElement(writer, m_taxpayerIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}