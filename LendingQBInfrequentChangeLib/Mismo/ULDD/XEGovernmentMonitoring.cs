﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEGovernmentMonitoring : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEGovernmentMonitoringDetail m_governmentMonitoringDetail;
        private XEHudaRaces m_hudaRaces;

        #endregion

        #region Public Properties
        public void SetGovernmentMonitoringDetail(XEGovernmentMonitoringDetail governmentMonitoringDetail)
        {
            m_governmentMonitoringDetail = governmentMonitoringDetail;
        }
        public void SetHudaRaces(XEHudaRaces hudaRaces)
        {
            m_hudaRaces = hudaRaces;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("GOVERNMENT_MONITORING");
            WriteElement(writer, m_governmentMonitoringDetail);
            WriteElement(writer, m_hudaRaces);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}