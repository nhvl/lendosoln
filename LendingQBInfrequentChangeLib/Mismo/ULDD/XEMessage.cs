﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEMessage : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private string m_mismoReferenceModelIdentifier = "3.0.0.263.12";
        private XEAboutVersions m_aboutVersions;
        private XEDealSets m_dealSets;

        #endregion

        #region Public Properties
        public void SetAboutVersions(XEAboutVersions aboutVersions)
        {
            m_aboutVersions = aboutVersions;
        }
        public void SetDealSets(XEDealSets dealSets)
        {
            m_dealSets = dealSets;
        }
        public string xmlns
        {
            get
            {
                return "http://www.mismo.org/residential/2009/schemas";
            }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("MESSAGE");
            WriteAttribute(writer, "MISMOReferenceModelIdentifier", m_mismoReferenceModelIdentifier);
            WriteAttribute(writer, "xmlns", xmlns);
            WriteElement(writer, m_aboutVersions);
            WriteElement(writer, m_dealSets);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}