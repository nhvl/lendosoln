﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XETermsOfMortgage : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_LienPriorityType LienPriorityType { get; set; }
        public E_LoanPurposeType LoanPurposeType { get; set; }
        public E_MortgageType MortgageType { get; set; }
        public string NoteAmount { get; set; }
        public string NoteDate { get; set; }
        public string NoteRatePercent { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("TERMS_OF_MORTGAGE");
            WriteElement(writer, "LienPriorityType", LienPriorityType);
            WriteElement(writer, "LoanPurposeType", LoanPurposeType);
            WriteElement(writer, "MortgageType", MortgageType);
            WriteElement(writer, "NoteAmount", NoteAmount);
            WriteElement(writer, "NoteDate", NoteDate);
            WriteElement(writer, "NoteRatePercent", NoteRatePercent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_LienPriorityType
    {
        None,
        FirstLien,
        SecondLien
    }

    public enum E_LoanPurposeType
    {
        None,
        Purchase,
        Refinance
    }

    public enum E_MortgageType
    {
        None,
        Conventional,
        FHA,
        Other,
        USDARuralHousing,
        VA
    }
}