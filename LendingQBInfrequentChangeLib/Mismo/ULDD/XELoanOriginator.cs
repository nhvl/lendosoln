﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanOriginator : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_LoanOriginatorType LoanOriginatorType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_ORIGINATOR");
            WriteElement(writer, "LoanOriginatorType", LoanOriginatorType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_LoanOriginatorType
    {
        None,
        Broker,
        Correspondent,
        Lender
    }
}