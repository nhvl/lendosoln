﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDealSets : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEDealSet m_dealSet;
        private XEParties m_parties;

        #endregion

        #region Public Properties
        public void SetDealSet(XEDealSet dealSet)
        {
            m_dealSet = dealSet;
        }
        public void SetParties(XEParties parties)
        {
            m_parties = parties;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DEAL_SETS");
            WriteElement(writer, m_dealSet);
            WriteElement(writer, m_parties);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}