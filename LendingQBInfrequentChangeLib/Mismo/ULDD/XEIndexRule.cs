﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEIndexRule : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string IndexSourceType { get; set; }
        public string IndexSourceTypeOtherDescription { get; set; }
        public string InterestAndPaymentAdjustmentIndexLeadDaysCount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INDEX_RULE");
            WriteElement(writer, "IndexSourceType", IndexSourceType);
            WriteElement(writer, "IndexSourceTypeOtherDescription", IndexSourceTypeOtherDescription);
            WriteElement(writer, "InterestAndPaymentAdjustmentIndexLeadDaysCount", InterestAndPaymentAdjustmentIndexLeadDaysCount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}