﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEClosingInformation : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEClosingCostFunds m_closingCostFunds;
        private XECollectedOtherFunds m_collectedOtherFunds;

        #endregion

        #region Public Properties
        public void SetClosingCostFunds(XEClosingCostFunds closingCostFunds)
        {
            m_closingCostFunds = closingCostFunds;
        }
        public void SetCollectedOtherFunds(XECollectedOtherFunds collectedOtherFunds)
        {
            m_collectedOtherFunds = collectedOtherFunds;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("CLOSING_INFORMATION");
            WriteElement(writer, m_closingCostFunds);
            WriteElement(writer, m_collectedOtherFunds);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}