﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDeals : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEDeal m_deal;

        #endregion

        #region Public Properties
        public void SetDeal(XEDeal deal)
        {
            m_deal = deal;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DEALS");
            WriteElement(writer, m_deal);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}