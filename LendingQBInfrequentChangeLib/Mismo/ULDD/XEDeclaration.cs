﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDeclaration : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEDeclarationDetail m_declarationDetail;

        #endregion

        #region Public Properties
        public void SetDeclarationDetail(XEDeclarationDetail declarationDetail)
        {
            m_declarationDetail = declarationDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DECLARATION");
            WriteElement(writer, m_declarationDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}