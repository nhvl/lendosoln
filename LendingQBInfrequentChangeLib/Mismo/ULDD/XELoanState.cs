﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanState : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string LoanStateDate { get; set; }
        public string LoanStateType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_STATE");
            WriteElement(writer, "LoanStateDate", LoanStateDate);
            WriteElement(writer, "LoanStateType", LoanStateType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}