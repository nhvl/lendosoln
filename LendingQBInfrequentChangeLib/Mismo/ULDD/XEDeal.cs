﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDeal : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XECollaterals m_collaterals;
        private XELoans m_loans;
        private XEParties m_parties;

        #endregion

        #region Public Properties
        public void SetCollaterals(XECollaterals collaterals)
        {
            m_collaterals = collaterals;
        }
        public void SetLoans(XELoans loans)
        {
            m_loans = loans;
        }
        public void SetParties(XEParties parties)
        {
            m_parties = parties;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DEAL");
            WriteElement(writer, m_collaterals);
            WriteElement(writer, m_loans);
            WriteElement(writer, m_parties);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}