﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECombinedLtvs : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XECombinedLtv m_combinedLtv;

        #endregion

        #region Public Properties
        public void SetCombinedLtv(XECombinedLtv combinedLtv)
        {
            m_combinedLtv = combinedLtv;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("COMBINED_LTVS");
            WriteElement(writer, m_combinedLtv);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}