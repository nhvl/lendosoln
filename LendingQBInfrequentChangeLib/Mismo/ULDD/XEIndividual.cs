﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEIndividual : Mismo.Common.AbstractXmlNode
    {
        private XEName m_name;

        public void SetName(XEName name)
        {
            m_name = name;
        }

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INDIVIDUAL");
            WriteElement(writer, m_name);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}