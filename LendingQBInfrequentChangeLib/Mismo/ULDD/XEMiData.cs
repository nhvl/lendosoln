﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEMiData : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEMiDataDetail m_miDataDetail;

        #endregion

        #region Public Properties
        public void SetMiDataDetail(XEMiDataDetail miDataDetail)
        {
            m_miDataDetail = miDataDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("MI_DATA");
            WriteElement(writer, m_miDataDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}