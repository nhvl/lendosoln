﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEProject : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEProjectDetail m_projectDetail;

        #endregion

        #region Public Properties
        public void SetProjectDetail(XEProjectDetail projectDetail)
        {
            m_projectDetail = projectDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROJECT");
            WriteElement(writer, m_projectDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}