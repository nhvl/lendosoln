﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEUnderwriting : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAutomatedUnderwritings m_automatedUnderwritings;
        private XEUnderwritingDetail m_underwritingDetail;

        #endregion

        #region Public Properties
        public void SetAutomatedUnderwritings(XEAutomatedUnderwritings automatedUnderwritings)
        {
            m_automatedUnderwritings = automatedUnderwritings;
        }
        public void SetUnderwritingDetail(XEUnderwritingDetail underwritingDetail)
        {
            m_underwritingDetail = underwritingDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("UNDERWRITING");
            WriteElement(writer, m_automatedUnderwritings);
            WriteElement(writer, m_underwritingDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}