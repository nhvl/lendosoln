﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAddresses : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAddress m_address;

        #endregion

        #region Public Properties
        public void SetAddress(XEAddress address)
        {
            m_address = address;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ADDRESSES");
            WriteElement(writer, m_address);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}