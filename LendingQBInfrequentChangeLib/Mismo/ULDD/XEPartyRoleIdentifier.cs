﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPartyRoleIdentifier : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string PartyRoleIdentifier { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PARTY_ROLE_IDENTIFIER");
            WriteElement(writer, "PartyRoleIdentifier", PartyRoleIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}