﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanLevelCreditDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string LoanLevelCreditScoreSelectionMethodType { get; set; }
        public string LoanLevelCreditScoreValue { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_LEVEL_CREDIT_DETAIL");
            WriteElement(writer, "LoanLevelCreditScoreSelectionMethodType", LoanLevelCreditScoreSelectionMethodType);
            WriteElement(writer, "LoanLevelCreditScoreValue", LoanLevelCreditScoreValue);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}