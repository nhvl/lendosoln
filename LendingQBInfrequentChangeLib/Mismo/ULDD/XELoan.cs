﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoan : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAdjustment m_adjustment;
        private XEAmortization m_amortization;
        private XEClosingInformation m_closingInformation;
        private XEDownPayments m_downPayments;
        private XEFormSpecificContents m_formSpecificContents;
        private XEGovernmentLoan m_governmentLoan;
        private XEHeloc m_heloc;
        private XEHmdaLoan m_hmdaLoan;
        private XEInterestCalculation m_interestCalculation;
        private XEInterestOnly m_interestOnly;
        private XEInvestorLoanInformation m_investorLoanInformation;
        private XELoanDetail m_loanDetail;
        private XELoanLevelCredit m_loanLevelCredit;
        private XELoanState m_loanState;
        private XELtv m_ltv;
        private XEMaturity m_maturity;
        private XEPayment m_payment;
        private XEQualification m_qualification;
        private XERefinance m_refinance;
        private XESelectedLoanProduct m_selectedLoanProduct;
        private XETermsOfMortgage m_termsOfMortgage;
        private XEUnderwriting m_underwriting;
        private XEEscrow m_escrow;
        private XEInvestorFeatures m_investorFeatures;
        private XELoanIdentifiers m_loanIdentifiers;
        private XELoanPrograms m_loanPrograms;
        private XEMiData m_miData;
        private XEServicing m_servicing;
        private XELoanComments m_loanComments;

        #endregion

        #region Public Properties
        public void SetAdjustment(XEAdjustment adjustment)
        {
            m_adjustment = adjustment;
        }
        public void SetAmortization(XEAmortization amortization)
        {
            m_amortization = amortization;
        }
        public void SetClosingInformation(XEClosingInformation closingInformation)
        {
            m_closingInformation = closingInformation;
        }
        public void SetDownPayments(XEDownPayments downPayments)
        {
            m_downPayments = downPayments;
        }
        public void SetFormSpecificContents(XEFormSpecificContents formSpecificContents)
        {
            m_formSpecificContents = formSpecificContents;
        }
        public void SetGovernmentLoan(XEGovernmentLoan governmentLoan)
        {
            m_governmentLoan = governmentLoan;
        }
        public void SetHmdaLoan(XEHmdaLoan hmdaLoan)
        {
            m_hmdaLoan = hmdaLoan;
        }
        public void SetInterestCalculation(XEInterestCalculation interestCalculation)
        {
            m_interestCalculation = interestCalculation;
        }
        public void SetInterestOnly(XEInterestOnly interestOnly)
        {
            m_interestOnly = interestOnly;
        }
        public void SetInvestorLoanInformation(XEInvestorLoanInformation investorLoanInformation)
        {
            m_investorLoanInformation = investorLoanInformation;
        }
        public void SetLoanDetail(XELoanDetail loanDetail)
        {
            m_loanDetail = loanDetail;
        }
        public void SetLoanLevelCredit(XELoanLevelCredit loanLevelCredit)
        {
            m_loanLevelCredit = loanLevelCredit;
        }
        public void SetLoanState(XELoanState loanState)
        {
            m_loanState = loanState;
        }
        public void SetLtv(XELtv ltv)
        {
            m_ltv = ltv;
        }
        public void SetMaturity(XEMaturity maturity)
        {
            m_maturity = maturity;
        }
        public void SetPayment(XEPayment payment)
        {
            m_payment = payment;
        }
        public void SetQualification(XEQualification qualification)
        {
            m_qualification = qualification;
        }
        public void SetRefinance(XERefinance refinance)
        {
            m_refinance = refinance;
        }
        public void SetSelectedLoanProduct(XESelectedLoanProduct selectedLoanProduct)
        {
            m_selectedLoanProduct = selectedLoanProduct;
        }
        public void SetTermsOfMortgage(XETermsOfMortgage termsOfMortgage)
        {
            m_termsOfMortgage = termsOfMortgage;
        }
        public void SetUnderwriting(XEUnderwriting underwriting)
        {
            m_underwriting = underwriting;
        }
        public void SetEscrow(XEEscrow escrow)
        {
            m_escrow = escrow;
        }
        public void SetInvestorFeatures(XEInvestorFeatures investorFeatures)
        {
            m_investorFeatures = investorFeatures;
        }
        public void SetLoanIdentifiers(XELoanIdentifiers loanIdentifiers)
        {
            m_loanIdentifiers = loanIdentifiers;
        }
        public void SetLoanPrograms(XELoanPrograms loanPrograms)
        {
            m_loanPrograms = loanPrograms;
        }
        public void SetMiData(XEMiData miData)
        {
            m_miData = miData;
        }
        public void SetServicing(XEServicing servicing)
        {
            m_servicing = servicing;
        }
        public void SetHeloc(XEHeloc heloc)
        {
            m_heloc = heloc;
        }
        public void SetLoanComments(XELoanComments loanComments)
        {
            m_loanComments = loanComments;
        }
        public string LoanRoleType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN");
            WriteAttribute(writer, "LoanRoleType", LoanRoleType);
            WriteElement(writer, m_adjustment);
            WriteElement(writer, m_amortization);
            WriteElement(writer, m_closingInformation);
            WriteElement(writer, m_downPayments);
            WriteElement(writer, m_escrow);
            WriteElement(writer, m_formSpecificContents);
            WriteElement(writer, m_governmentLoan);
            WriteElement(writer, m_heloc);
            WriteElement(writer, m_hmdaLoan);
            WriteElement(writer, m_interestCalculation);
            WriteElement(writer, m_interestOnly);
            WriteElement(writer, m_investorFeatures);
            WriteElement(writer, m_investorLoanInformation);
            WriteElement(writer, m_loanComments);
            WriteElement(writer, m_loanDetail);
            WriteElement(writer, m_loanIdentifiers);
            WriteElement(writer, m_loanLevelCredit);
            WriteElement(writer, m_loanPrograms);
            WriteElement(writer, m_loanState);
            WriteElement(writer, m_ltv);
            WriteElement(writer, m_maturity);
            WriteElement(writer, m_miData);
            WriteElement(writer, m_payment);
            WriteElement(writer, m_qualification);
            WriteElement(writer, m_refinance);
            WriteElement(writer, m_selectedLoanProduct);
            WriteElement(writer, m_servicing);
            WriteElement(writer, m_termsOfMortgage);
            WriteElement(writer, m_underwriting);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}