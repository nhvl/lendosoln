﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPropertyValuationDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string AppraisalIdentifier { get; set; }
        public string PropertyValuationAmount { get; set; }
        public string PropertyValuationEffectiveDate { get; set; }
        public E_PropertyValuationMethodType PropertyValuationMethodType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY_VALUATION_DETAIL");
            WriteElement(writer, "AppraisalIdentifier", AppraisalIdentifier);
            WriteElement(writer, "PropertyValuationAmount", PropertyValuationAmount);
            WriteElement(writer, "PropertyValuationEffectiveDate", PropertyValuationEffectiveDate);
            WriteElement(writer, "PropertyValuationMethodType", PropertyValuationMethodType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_PropertyValuationMethodType
    {
        None,
        AutomatedValuationModel,
        DesktopAppraisal,
        DriveBy,
        FullAppraisal,
        PriorAppraisalUsed
    }
}