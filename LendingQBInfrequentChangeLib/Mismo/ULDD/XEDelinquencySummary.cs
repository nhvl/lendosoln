﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDelinquencySummary : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string DelinquentPaymentsOverPastTwelveMonthsCount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DELINQUENCY_SUMMARY");
            WriteElement(writer, "DelinquentPaymentsOverPastTwelveMonthsCount", DelinquentPaymentsOverPastTwelveMonthsCount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}