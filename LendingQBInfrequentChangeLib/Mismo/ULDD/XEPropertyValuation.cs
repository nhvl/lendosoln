﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPropertyValuation : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEPropertyValuationDetail m_propertyValuationDetail;
        private XEAvms m_avms;

        #endregion

        #region Public Properties
        public void SetPropertyValuationDetail(XEPropertyValuationDetail propertyValuationDetail)
        {
            m_propertyValuationDetail = propertyValuationDetail;
        }
        public void SetAvms(XEAvms avms)
        {
            m_avms = avms;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY_VALUATION");
            WriteElement(writer, m_avms);
            WriteElement(writer, m_propertyValuationDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}