﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEInterestRatePerChangeAdjustmentRules : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_interestRatePerChangeAdjustmentRule = new ArrayList();

        #endregion

        #region Public Properties
        public void AddInterestRatePerChangeAdjustmentRule(XEInterestRatePerChangeAdjustmentRule interestRatePerChangeAdjustmentRule)
        {
            m_interestRatePerChangeAdjustmentRule.Add(interestRatePerChangeAdjustmentRule);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES");
            WriteElement(writer, m_interestRatePerChangeAdjustmentRule);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}