﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInterestCalculationRule : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string InterestCalculationType { get; set; }
        public string LoanInterestAccrualStartDate { get; set; }
        public string InterestCalculationPeriodType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_CALCULATION_RULE");
            WriteElement(writer, "InterestCalculationPeriodType", InterestCalculationPeriodType);
            WriteElement(writer, "InterestCalculationType", InterestCalculationType);
            WriteElement(writer, "LoanInterestAccrualStartDate", LoanInterestAccrualStartDate);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}