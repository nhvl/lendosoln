﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDeclarationDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string BorrowerFirstTimeHomebuyerIndicator { get; set; }
        public E_CitizenshipResidencyType CitizenshipResidencyType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DECLARATION_DETAIL");
            WriteElement(writer, "BorrowerFirstTimeHomebuyerIndicator", BorrowerFirstTimeHomebuyerIndicator);
            WriteElement(writer, "CitizenshipResidencyType", CitizenshipResidencyType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_CitizenshipResidencyType
    {
        None,
        USCitizen,
        PermanentResidentAlien,
        NonPermanentResidentAlien
    }
}