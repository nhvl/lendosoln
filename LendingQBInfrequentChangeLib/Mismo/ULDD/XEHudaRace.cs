﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEHudaRace : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_HMDARaceType HMDARaceType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("HMDA_RACE");
            WriteElement(writer, "HMDARaceType", HMDARaceType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_HMDARaceType
    {
        None,
        AmericanIndianOrAlaskaNative,
        Asian,
        BlackOrAfricanAmerican,
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        NativeHawaiianOrOtherPacificIslander,
        NotApplicable,
        White
    }
}