﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPriceLock : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string PriceLockDatetime { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PRICE_LOCK");
            WriteElement(writer, "PriceLockDatetime", PriceLockDatetime);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}