﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEMaturity : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEMaturityRule m_maturityRule;

        #endregion

        #region Public Properties
        public void SetMaturityRule(XEMaturityRule maturityRule)
        {
            m_maturityRule = maturityRule;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("MATURITY");
            WriteElement(writer, m_maturityRule);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}