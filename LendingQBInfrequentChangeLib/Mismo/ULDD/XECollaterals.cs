﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECollaterals : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XECollateral m_collateral;

        #endregion

        #region Public Properties
        public void SetCollateral(XECollateral collateral)
        {
            m_collateral = collateral;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("COLLATERALS");
            WriteElement(writer, m_collateral);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}