﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XERefinance : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_RefinanceCashOutDeterminationType RefinanceCashOutDeterminationType { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("REFINANCE");
            WriteElement(writer, "RefinanceCashOutDeterminationType", RefinanceCashOutDeterminationType);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_RefinanceCashOutDeterminationType
    {
        None,
        CashOut,
        LimitedCashOut,
        NoCashOut
    }
}