﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECombinedLtv : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string CombinedLTVRatioPercent { get; set; }
        public string HomeEquityCombinedLTVRatioPercent { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("COMBINED_LTV");
            WriteElement(writer, "CombinedLTVRatioPercent", CombinedLTVRatioPercent);
            WriteElement(writer, "HomeEquityCombinedLTVRatioPercent", HomeEquityCombinedLTVRatioPercent);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}