﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAppraiser : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAppraiserLicense m_appraiserLicense;

        #endregion

        #region Public Properties
        public void SetAppraiserLicense(XEAppraiserLicense appraiserLicense)
        {
            m_appraiserLicense = appraiserLicense;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("APPRAISER");
            WriteElement(writer, m_appraiserLicense);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}