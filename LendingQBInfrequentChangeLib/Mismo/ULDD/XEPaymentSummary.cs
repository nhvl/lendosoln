﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPaymentSummary : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string AggregateLoanCurtailmentAmount { get; set; }
        public string LastPaidInstallmentDueDate { get; set; }
        public string LastPaymentReceivedDate { get; set; }
        public string UPBAmount { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PAYMENT_SUMMARY");
            WriteElement(writer, "AggregateLoanCurtailmentAmount", AggregateLoanCurtailmentAmount);
            WriteElement(writer, "LastPaidInstallmentDueDate", LastPaidInstallmentDueDate);
            WriteElement(writer, "LastPaymentReceivedDate", LastPaymentReceivedDate);
            WriteElement(writer, "UPBAmount", UPBAmount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}