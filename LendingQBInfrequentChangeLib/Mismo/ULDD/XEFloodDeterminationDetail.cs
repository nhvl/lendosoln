﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEFloodDeterminationDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string SpecialFloodHazardAreaIndicator
        {
            get;
            set;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("FLOOD_DETERMINATION_DETAIL");
            WriteElement(writer, "SpecialFloodHazardAreaIndicator", SpecialFloodHazardAreaIndicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}