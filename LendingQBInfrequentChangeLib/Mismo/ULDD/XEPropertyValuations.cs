﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPropertyValuations : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEPropertyValuation m_propertyValuation;

        #endregion

        #region Public Properties
        public void SetPropertyValuation(XEPropertyValuation propertyValuation)
        {
            m_propertyValuation = propertyValuation;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY_VALUATIONS");
            WriteElement(writer, m_propertyValuation);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}