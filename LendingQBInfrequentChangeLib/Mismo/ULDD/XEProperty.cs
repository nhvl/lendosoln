﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEProperty : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEProject m_project;
        private XEPropertyDetail m_propertyDetail;
        private XEPropertyUnits m_propertyUnits;
        private XEPropertyValuations m_propertyValuations;
        private XEAddress m_address;
        private XEFloodDetermination m_floodDetermination;

        #endregion

        #region Public Properties
        public void SetProject(XEProject project)
        {
            m_project = project;
        }
        public void SetPropertyDetail(XEPropertyDetail propertyDetail)
        {
            m_propertyDetail = propertyDetail;
        }
        public void SetPropertyUnits(XEPropertyUnits propertyUnits)
        {
            m_propertyUnits = propertyUnits;
        }
        public void SetPropertyValuations(XEPropertyValuations propertyValuations)
        {
            m_propertyValuations = propertyValuations;
        }
        public void SetAddress(XEAddress address)
        {
            m_address = address;
        }
        public void SetFloodDetermination(XEFloodDetermination floodDetermination)
        {
            m_floodDetermination = floodDetermination;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY");
            WriteElement(writer, m_address);
            WriteElement(writer, m_floodDetermination);
            WriteElement(writer, m_project);
            WriteElement(writer, m_propertyDetail);
            WriteElement(writer, m_propertyUnits);
            WriteElement(writer, m_propertyValuations);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}