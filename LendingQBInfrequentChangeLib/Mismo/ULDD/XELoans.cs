﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XELoans : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XECombinedLtvs m_combinedLtvs;
        private ArrayList m_loan = new ArrayList();

        #endregion

        #region Public Properties
        public void SetCombinedLtvs(XECombinedLtvs combinedLtvs)
        {
            m_combinedLtvs = combinedLtvs;
        }
        public void AddLoan(XELoan loan)
        {
            m_loan.Add(loan);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOANS");
            WriteElement(writer, m_combinedLtvs);
            WriteElement(writer, m_loan);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}