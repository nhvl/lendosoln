﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAutomatedUnderwritings : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAutomatedUnderwriting m_automatedUnderwriting;

        #endregion

        #region Public Properties
        public void SetAutomatedUnderwriting(XEAutomatedUnderwriting automatedUnderwriting)
        {
            m_automatedUnderwriting = automatedUnderwriting;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("AUTOMATED_UNDERWRITINGS");
            WriteElement(writer, m_automatedUnderwriting);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}