﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInterestRateAdjustment : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEIndexRules m_indexRules;
        private XEInterestRateLifetimeAdjustmentRule m_interestRateLifetimeAdjustmentRule;
        private XEInterestRatePerChangeAdjustmentRules m_interestRatePerChangeAdjustmentRules;

        #endregion

        #region Public Properties
        public void SetIndexRules(XEIndexRules indexRules)
        {
            m_indexRules = indexRules;
        }
        public void SetInterestRateLifetimeAdjustmentRule(XEInterestRateLifetimeAdjustmentRule interestRateLifetimeAdjustmentRule)
        {
            m_interestRateLifetimeAdjustmentRule = interestRateLifetimeAdjustmentRule;
        }
        public void SetInterestRatePerChangeAdjustmentRules(XEInterestRatePerChangeAdjustmentRules interestRatePerChangeAdjustmentRules)
        {
            m_interestRatePerChangeAdjustmentRules = interestRatePerChangeAdjustmentRules;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_RATE_ADJUSTMENT");
            WriteElement(writer, m_indexRules);
            WriteElement(writer, m_interestRateLifetimeAdjustmentRule);
            WriteElement(writer, m_interestRatePerChangeAdjustmentRules);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}