﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEPropertyUnit : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_propertyUnitDetail = new ArrayList();

        #endregion

        #region Public Properties
        public void AddPropertyUnitDetail(XEPropertyUnitDetail propertyUnitDetail)
        {
            m_propertyUnitDetail.Add(propertyUnitDetail);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY_UNIT");
            WriteElement(writer, m_propertyUnitDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}