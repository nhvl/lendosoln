﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XETaxpayerIdentifier : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string TaxpayerIdentifierType { get; set; }
        public string TaxpayerIdentifierValue { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("TAXPAYER_IDENTIFIER");
            WriteElement(writer, "TaxpayerIdentifierType", TaxpayerIdentifierType);
            WriteElement(writer, "TaxpayerIdentifierValue", TaxpayerIdentifierValue);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}