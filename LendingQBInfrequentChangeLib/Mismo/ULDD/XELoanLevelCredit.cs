﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanLevelCredit : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XELoanLevelCreditDetail m_loanLevelCreditDetail;

        #endregion

        #region Public Properties
        public void SetLoanLevelCreditDetail(XELoanLevelCreditDetail loanLevelCreditDetail)
        {
            m_loanLevelCreditDetail = loanLevelCreditDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_LEVEL_CREDIT");
            WriteElement(writer, m_loanLevelCreditDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}