﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanProductDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_RefinanceProgramIdentifier RefinanceProgramIdentifier { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_PRODUCT_DETAIL");
            WriteElement(writer, "RefinanceProgramIdentifier", RefinanceProgramIdentifier);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_RefinanceProgramIdentifier
    {
        None,
        FREOwnedStreamlinedRefinance,
        RefiPlus,
        ReliefRefinanceOpenAccess,
        ReliefRefinanceSameServicer,
        StreamlinedReliefRefinance,
        TexasEquity,
        DisasterResponse,
        DURefiPlus
    }
}