﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAddress : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string AddressLineText { get; set; }
        public string AddressType { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string StateCode { get; set; }
        public string CountryCode { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ADDRESS");
            WriteElement(writer, "AddressLineText", AddressLineText);
            WriteElement(writer, "AddressType", AddressType);
            WriteElement(writer, "CityName", CityName);
            WriteElement(writer, "CountryCode", CountryCode);
            WriteElement(writer, "PostalCode", PostalCode);
            WriteElement(writer, "StateCode", StateCode);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}