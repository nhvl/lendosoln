﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEInvestorFeatures : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_investorFeature = new ArrayList();

        #endregion

        #region Public Properties
        public void AddInvestorFeature(XEInvestorFeature investorFeature)
        {
            m_investorFeature.Add(investorFeature);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INVESTOR_FEATURES");
            WriteElement(writer, m_investorFeature);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}