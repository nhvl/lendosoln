﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEClosingCostFund : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string ClosingCostContributionAmount { get; set; }
        public E_ClosingCostFundsType ClosingCostFundsType { get; set; }
        public E_ClosingCostFundsType ClosingCostFundsTypeOtherDescription { get; set; }
        public E_ClosingCostSourceType ClosingCostSourceType { get; set; }
        public E_ClosingCostSourceType ClosingCostSourceTypeOtherDescription { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("CLOSING_COST_FUND");
            WriteElement(writer, "ClosingCostContributionAmount", ClosingCostContributionAmount);
            WriteElement(writer, "ClosingCostFundsType", ClosingCostFundsType);
            WriteElement(writer, "ClosingCostFundsTypeOtherDescription", ClosingCostFundsTypeOtherDescription);
            WriteElement(writer, "ClosingCostSourceType", ClosingCostSourceType);
            WriteElement(writer, "ClosingCostSourceTypeOtherDescription", ClosingCostSourceTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_ClosingCostFundsType
    {
        None,
        BridgeLoan,
        CashOnHand,
        CashOrOtherEquity,
        CheckingSavings,
        Contribution,
        CreditCard,
        DepositOnSalesContract,
        EquityOnPendingSale,
        EquityOnSoldProperty,
        EquityOnSubjectProperty,
        ForgivableSecuredLoan,
        GiftFunds,
        Grant,
        HousingRelocation,
        LifeInsuranceCashValue,
        LotEquity,
        MortgageCreditCertificates,
        Other,
        OtherEquity,
        PledgedCollateral,
        PremiumFunds,
        RentWithOptionToPurchase,
        RetirementFunds,
        SaleOfChattel,
        SalesPriceAdjustment,
        SecondaryFinancing,
        SecuredLoan,
        StocksAndBonds,
        SweatEquity,
        TradeEquity,
        TrustFunds,
        UnsecuredBorrowedFunds,
        AggregatedRemainingTypes,
        SecondaryFinancingClosedEnd,
        SecondaryFinancingHELOC
    }

    public enum E_ClosingCostSourceType
    {
        None,
        Borrower,
        Builder,
        CommunityNonProfit,
        Employer,
        FederalAgency,
        Lender,
        LocalAgency,
        MortgageCreditCertificate,
        Other,
        OwnerInvestor,
        PropertySeller,
        Relative,
        ReligiousNonProfit,
        StateAgency,
        AggregatedRemainingSourceTypes,
        FHLBAffordableHousingProgram,
        USDARuralHousing
    }
}