﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEPriceLocks : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEPriceLock m_priceLock;

        #endregion

        #region Public Properties
        public void SetPriceLock(XEPriceLock priceLock)
        {
            m_priceLock = priceLock;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PRICE_LOCKS");
            WriteElement(writer, m_priceLock);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}