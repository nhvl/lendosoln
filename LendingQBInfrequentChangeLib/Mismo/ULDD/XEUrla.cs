﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEUrla : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEUrlaDetail m_urlaDetail;

        #endregion

        #region Public Properties
        public void SetUrlaDetail(XEUrlaDetail urlaDetail)
        {
            m_urlaDetail = urlaDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("URLA");
            WriteElement(writer, m_urlaDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}