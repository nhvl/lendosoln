﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAmortization : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAmortizationRule m_amortizationRule;

        #endregion

        #region Public Properties
        public void SetAmortizationRule(XEAmortizationRule amortizationRule)
        {
            m_amortizationRule = amortizationRule;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("AMORTIZATION");
            WriteElement(writer, m_amortizationRule);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}