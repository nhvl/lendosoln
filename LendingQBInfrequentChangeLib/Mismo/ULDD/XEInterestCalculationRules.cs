﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInterestCalculationRules : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEInterestCalculationRule m_interestCalculationRule;

        #endregion

        #region Public Properties
        public void SetInterestCalculationRule(XEInterestCalculationRule interestCalculationRule)
        {
            m_interestCalculationRule = interestCalculationRule;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_CALCULATION_RULES");
            WriteElement(writer, m_interestCalculationRule);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}