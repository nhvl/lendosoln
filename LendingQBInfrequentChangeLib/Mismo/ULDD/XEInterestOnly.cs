﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEInterestOnly : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string InterestOnlyEndDate { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("INTEREST_ONLY");
            WriteElement(writer, "InterestOnlyEndDate", InterestOnlyEndDate);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}