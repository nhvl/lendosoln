﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDownPayment : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string DownPaymentAmount { get; set; }
        public E_DownPaymentSourceType DownPaymentSourceType { get; set; }
        public E_DownPaymentSourceType DownPaymentSourceTypeOtherDescription { get; set; }
        public E_DownPaymentType DownPaymentType { get; set; }
        public E_DownPaymentType DownPaymentTypeOtherDescription { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DOWN_PAYMENT");
            WriteElement(writer, "DownPaymentAmount", DownPaymentAmount);
            WriteElement(writer, "DownPaymentSourceType", DownPaymentSourceType);
            WriteElement(writer, "DownPaymentSourceTypeOtherDescription", DownPaymentSourceTypeOtherDescription);
            WriteElement(writer, "DownPaymentType", DownPaymentType);
            WriteElement(writer, "DownPaymentTypeOtherDescription", DownPaymentTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_DownPaymentSourceType
    {
        None,
        Borrower,
        CommunityNonProfit,
        Employer,
        FederalAgency,
        LocalAgency,
        Other,
        Relative,
        ReligiousNonProfit,
        StateAgency,
        AggregatedRemainingSourceTypes,
        FHLBAffordableHousingProgram,
        USDARuralHousing 
    }

    public enum E_DownPaymentType
    {
        None,
        BridgeLoan,
        CashOnHand,
        CheckingSavings,
        GiftFunds,
        OtherTypeOfDownPayment,
        SecuredBorrowedFunds,
        SweatEquity,
        UnsecuredBorrowedFunds,
        AggregatedRemainingTypes,
        SecondaryFinancingClosedEnd,
        SecondaryFinancingHELOC 
    }
}