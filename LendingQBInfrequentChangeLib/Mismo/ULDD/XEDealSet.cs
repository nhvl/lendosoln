﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEDealSet : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEDeals m_deals;
        private XEInvestorFeatures m_investorFeatures;

        #endregion

        #region Public Properties
        public void SetDeals(XEDeals deals)
        {
            m_deals = deals;
        }
        public void SetInvestorFeatures(XEInvestorFeatures investorFeatures)
        {
            m_investorFeatures = investorFeatures;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("DEAL_SET");
            WriteElement(writer, m_deals);
            WriteElement(writer, m_investorFeatures);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}