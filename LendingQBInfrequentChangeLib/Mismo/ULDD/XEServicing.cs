﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEServicing : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEDelinquencySummary m_delinquencySummary;

        #endregion

        #region Public Properties
        public void SetDelinquencySummary(XEDelinquencySummary delinquencySummary)
        {
            m_delinquencySummary = delinquencySummary;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("SERVICING");
            WriteElement(writer, m_delinquencySummary);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}