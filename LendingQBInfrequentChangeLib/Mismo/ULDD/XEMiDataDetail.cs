﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEMiDataDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string LenderPaidMIInterestRateAdjustmentPercent { get; set; }
        public string MICertificateIdentifier { get; set; }
        public string MICompanyNameType { get; set; }
        public string MICompanyNameTypeOtherDescription { get; set; }
        public string MICoveragePercent { get; set; }
        public string MIPremiumFinancedAmount { get; set; }
        public string MIPremiumFinancedIndicator { get; set; }
        public E_MIPremiumSourceType MIPremiumSourceType { get; set; }
        public E_PrimaryMIAbsenceReasonType PrimaryMIAbsenceReasonType { get; set; }
        public E_PrimaryMIAbsenceReasonType PrimaryMIAbsenceReasonTypeOtherDescription { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("MI_DATA_DETAIL");
            WriteElement(writer, "LenderPaidMIInterestRateAdjustmentPercent", LenderPaidMIInterestRateAdjustmentPercent);
            WriteElement(writer, "MICertificateIdentifier", MICertificateIdentifier);
            WriteElement(writer, "MICompanyNameType", MICompanyNameType);
            WriteElement(writer, "LoanStateDate", MICompanyNameTypeOtherDescription);
            WriteElement(writer, "MICoveragePercent", MICoveragePercent);
            WriteElement(writer, "MIPremiumFinancedAmount", MIPremiumFinancedAmount);
            WriteElement(writer, "MIPremiumFinancedIndicator", MIPremiumFinancedIndicator);
            WriteElement(writer, "MIPremiumSourceType", MIPremiumSourceType);
            WriteElement(writer, "PrimaryMIAbsenceReasonType", PrimaryMIAbsenceReasonType);
            WriteElement(writer, "PrimaryMIAbsenceReasonTypeOtherDescription", PrimaryMIAbsenceReasonTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_PrimaryMIAbsenceReasonType
    {
        None,
        MICanceledBasedOnCurrentLTV,
        NoMIBasedOnOriginalLTV,
        Other,
        IndemnificationInLieuOfMI,
        NoMIBasedonMortgageBeingRefinanced,
        RecourseInLieuOfMI
    }

    public enum E_MIPremiumSourceType
    {
        None,
        Borrower,
        Lender
    }
}