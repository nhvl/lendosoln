﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEBorrower : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEBorrowerDetail m_borrowerDetail;
        private XECounselingConfirmation m_counselingConfirmation;
        private ArrayList m_creditScores = new ArrayList();
        private XEDeclaration m_declaration;
        private XEGovernmentMonitoring m_governmentMonitoring;

        #endregion

        #region Public Properties
        public void SetBorrowerDetail(XEBorrowerDetail borrowerDetail)
        {
            m_borrowerDetail = borrowerDetail;
        }
        public void SetCounselingConfirmation(XECounselingConfirmation counselingConfirmation)
        {
            m_counselingConfirmation = counselingConfirmation;
        }
        public void AddCreditScores(XECreditScores creditScores)
        {
            m_creditScores.Add(creditScores);
        }
        public void SetDeclaration(XEDeclaration declaration)
        {
            m_declaration = declaration;
        }
        public void SetGovernmentMonitoring(XEGovernmentMonitoring governmentMonitoring)
        {
            m_governmentMonitoring = governmentMonitoring;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("BORROWER");
            WriteElement(writer, m_borrowerDetail);
            WriteElement(writer, m_counselingConfirmation);
            WriteElement(writer, m_creditScores);
            WriteElement(writer, m_declaration);
            WriteElement(writer, m_governmentMonitoring);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}