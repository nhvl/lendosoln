﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XECreditScoreDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public E_CreditRepositorySourceType CreditRepositorySourceType { get; set; }
        public string CreditScoreValue { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("CREDIT_SCORE_DETAIL");
            WriteElement(writer, "CreditRepositorySourceIndicator", "true");
            WriteElement(writer, "CreditRepositorySourceType", CreditRepositorySourceType);
            WriteElement(writer, "CreditScoreValue", CreditScoreValue);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public enum E_CreditRepositorySourceType
    {
        None,
        Equifax,
        Experian,
        TransUnion
    }
}