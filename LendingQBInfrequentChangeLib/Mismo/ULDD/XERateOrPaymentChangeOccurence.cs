﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XERateOrPaymentChangeOccurence : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string NextRateAdjustmentEffectiveDate { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("RATE_OR_PAYMENT_CHANGE_OCCURRENCE");
            WriteElement(writer, "NextRateAdjustmentEffectiveDate", NextRateAdjustmentEffectiveDate);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}