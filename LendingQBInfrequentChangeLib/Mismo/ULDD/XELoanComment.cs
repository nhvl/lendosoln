﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanComment : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string LoanCommentText { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_COMMENT");
            WriteElement(writer, "LoanCommentText", LoanCommentText);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}