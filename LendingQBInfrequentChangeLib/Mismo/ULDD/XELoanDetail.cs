﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XELoanDetail : Mismo.Common.AbstractXmlNode
    {
        #region Public Properties
        public string ApplicationReceivedDate { get; set; }
        public string AssumabilityIndicator { get; set; }
        public string BalloonIndicator { get; set; }
        public string BorrowerCount { get; set; }
        public string BuydownTemporarySubsidyIndicator { get; set; }
        public string CapitalizedLoanIndicator { get; set; }
        public string ConstructionLoanIndicator { get; set; }
        public string ConvertibleIndicator { get; set; }
        public string EscrowIndicator { get; set; }
        public string InitialFixedPeriodEffectiveMonthsCount { get; set; }
        public string InterestOnlyIndicator { get; set; }
        public string LoanAffordableIndicator { get; set; }
        public string PrepaymentPenaltyIndicator { get; set; }
        public string RelocationLoanIndicator { get; set; }
        public string SharedEquityIndicator { get; set; }
        public string HELOCIndicator { get; set; }
        public string BalloonResetIndicator { get; set; }
        public string CurrentInterestRatePercent { get; set; }
        public string MortgageModificationIndicator { get; set; }

        public string LoanStateDate { get; set; }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_DETAIL");
            WriteElement(writer, "ApplicationReceivedDate", ApplicationReceivedDate);
            WriteElement(writer, "AssumabilityIndicator", AssumabilityIndicator);
            WriteElement(writer, "BalloonIndicator", BalloonIndicator);
            WriteElement(writer, "BorrowerCount", BorrowerCount);
            WriteElement(writer, "BuydownTemporarySubsidyIndicator", BuydownTemporarySubsidyIndicator);
            WriteElement(writer, "CapitalizedLoanIndicator", CapitalizedLoanIndicator);
            WriteElement(writer, "ConstructionLoanIndicator", ConstructionLoanIndicator);
            WriteElement(writer, "ConvertibleIndicator", ConvertibleIndicator);
            WriteElement(writer, "EscrowIndicator", EscrowIndicator);
            WriteElement(writer, "HELOCIndicator", HELOCIndicator);
            WriteElement(writer, "InitialFixedPeriodEffectiveMonthsCount", InitialFixedPeriodEffectiveMonthsCount);
            WriteElement(writer, "InterestOnlyIndicator", InterestOnlyIndicator);
            WriteElement(writer, "LoanAffordableIndicator", LoanAffordableIndicator);
            WriteElement(writer, "PrepaymentPenaltyIndicator", PrepaymentPenaltyIndicator);
            WriteElement(writer, "RelocationLoanIndicator", RelocationLoanIndicator);
            WriteElement(writer, "SharedEquityIndicator", SharedEquityIndicator);

            WriteElement(writer, "BalloonResetIndicator", BalloonResetIndicator);
            WriteElement(writer, "CurrentInterestRatePercent", CurrentInterestRatePercent);
            WriteElement(writer, "MortgageModificationIndicator", MortgageModificationIndicator);

            WriteElement(writer, "LoanStateDate", LoanStateDate);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}