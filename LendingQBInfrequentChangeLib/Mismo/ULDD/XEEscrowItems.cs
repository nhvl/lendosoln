﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEEscrowItems : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_escrowItem = new ArrayList();

        #endregion

        #region Public Properties
        public void AddEscrowItem(XEEscrowItem escrowItem)
        {
            m_escrowItem.Add(escrowItem);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ESCROW_ITEMS");
            WriteElement(writer, m_escrowItem);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}