﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEEscrowItem : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEEscrowItemDetail m_escrowItemDetail;

        #endregion

        #region Public Properties
        public void SetEscrowItemDetail(XEEscrowItemDetail escrowItemDetail)
        {
            m_escrowItemDetail = escrowItemDetail;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("ESCROW_ITEM");
            WriteElement(writer, m_escrowItemDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}