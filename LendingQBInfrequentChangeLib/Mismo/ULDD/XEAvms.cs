﻿using System;
using System.Xml;

namespace Mismo.ULDD
{
    public class XEAvms : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private XEAvm m_avm;

        #endregion

        #region Public Properties
        public void SetAvm(XEAvm avm)
        {
            m_avm = avm;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("AVMS");
            WriteElement(writer, m_avm);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}