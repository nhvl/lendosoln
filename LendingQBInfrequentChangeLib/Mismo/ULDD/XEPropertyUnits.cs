﻿using System;
using System.Xml;
using System.Collections;

namespace Mismo.ULDD
{
    public class XEPropertyUnits : Mismo.Common.AbstractXmlNode
    {
        #region Private Member Variables
        private ArrayList m_propertyUnit = new ArrayList();

        #endregion

        #region Public Properties
        public void AddPropertyUnit(XEPropertyUnit propertyUnit)
        {
            m_propertyUnit.Add(propertyUnit);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer)
        {
            writer.WriteStartElement("PROPERTY_UNITS");
            WriteElement(writer, m_propertyUnit);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}