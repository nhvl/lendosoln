/// Author: David Dao

using System;
using System.Collections;
using System.Xml;

namespace Mismo.Closing
{
	public class XEPaymentDetails : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="PAYMENT_DETAILS">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="AMORTIZATION_SCHEDULE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="PAYMENT_SCHEDULE" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private ArrayList m_amortizationScheduleList = new ArrayList();
        private ArrayList m_paymentScheduleList = new ArrayList();
        #endregion

        #region Public Properties
        public void AddAmortizationSchedule(XEAmortizationSchedule amortizationSchedule) 
        {
            m_amortizationScheduleList.Add(amortizationSchedule);
        }
        public void AddPaymentSchedule(Mismo.MismoXmlElement.XEPaymentSchedule paymentSchedule) 
        {
            m_paymentScheduleList.Add(paymentSchedule);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PAYMENT_DETAILS");
            WriteElement(writer, m_amortizationScheduleList);
            WriteElement(writer, m_paymentScheduleList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
