/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEContributor : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="_CONTRIBUTOR">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_Amount" type="xs:string"/>
        //          <xs:attribute name="_Percent" type="xs:string"/>
        //          <xs:attribute name="_RoleType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="UnrelatedFriend"/>
        //                <xs:enumeration value="LenderPremiumFinanced"/>
        //                <xs:enumeration value="NonparentRelative"/>
        //                <xs:enumeration value="Unassigned"/>
        //                <xs:enumeration value="Employer"/>
        //                <xs:enumeration value="Borrower"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="Parent"/>
        //                <xs:enumeration value="Seller"/>
        //                <xs:enumeration value="Builder"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_RoleTypeOtherDescription" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_amount;
        private string m_percent;
        private E_ContributorRoleType m_roleType;
        private string m_roleTypeOtherDescription;
        #endregion

        #region Public Properties
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public string Percent 
        {
            get { return m_percent; }
            set { m_percent = value; }
        }
        public E_ContributorRoleType RoleType 
        {
            get { return m_roleType; }
            set { m_roleType = value; }
        }
        public string RoleTypeOtherDescription 
        {
            get { return m_roleTypeOtherDescription; }
            set { m_roleTypeOtherDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_CONTRIBUTOR");
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_Percent", m_percent);
            WriteAttribute(writer, "_RoleType", m_roleType);
            WriteAttribute(writer, "_RoleTypeOtherDescription", m_roleTypeOtherDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ContributorRoleType 
    {
        Undefined = 0,
        UnrelatedFriend,
        LenderPremiumFinanced,
        NonparentRelative,
        Unassigned,
        Employer,
        Borrower,
        Other,
        Parent,
        Seller,
        Builder
    }
}
