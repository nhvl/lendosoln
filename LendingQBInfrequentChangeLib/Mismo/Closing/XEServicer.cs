/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
    public class XEServicer : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="SERVICER">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="CONTACT_DETAIL" minOccurs="0"/>
        //        <xs:element ref="NON_PERSON_ENTITY_DETAIL" minOccurs="0"/>
        //        <xs:element ref="_QUALIFIED_WRITTEN_REQUEST_MAIL_TO" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_Name" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute  name="_Country"  type="xs:string"/>
        //      <xs:attribute name="_DaysOfTheWeekDescription" type="xs:string"/>
        //      <xs:attribute name="_DirectInquiryToDescription" type="xs:string"/>
        //      <xs:attribute name="_HoursOfTheDayDescription" type="xs:string"/>
        //      <xs:attribute name="_InquiryTelephoneNumber" type="xs:string"/>
        //      <xs:attribute name="_PaymentAcceptanceEndDate" type="xs:string"/>
        //      <xs:attribute name="_PaymentAcceptanceStartDate" type="xs:string"/>
        //      <xs:attribute name="_TransferEffectiveDate" type="xs:string"/>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="New"/>
        //            <xs:enumeration value="Investor"/>
        //            <xs:enumeration value="Present"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Lender"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="NonPersonEntityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private Mismo.MismoXmlElement.XEContactDetail m_contactDetail;
        private XENonPersonEntityDetail m_nonPersonEntityDetail;
        private XEQualifiedWrittenRequestMailTo m_qualifiedWrittenRequestMailTo;
        private string m_name;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_country;
        private string m_daysOfTheWeekDescription;
        private string m_directInquiryToDescription;
        private string m_hoursOfTheDayDescription;
        private string m_inquiryTelephoneNumber;
        private string m_paymentAcceptanceEndDate;
        private string m_paymentAcceptanceStartDate;
        private string m_transferEffectiveDate;
        private E_ServicerType m_type;
        private string m_typeOtherDescription;
        private E_YesNoIndicator m_nonPersonEntityIndicator;

        #endregion

        #region Public Properties
        public void SetContactDetail(Mismo.MismoXmlElement.XEContactDetail contactDetail) 
        {
            m_contactDetail = contactDetail;
        }
        public void SetNonPersonEntityDetail(XENonPersonEntityDetail nonPersonEntityDetail) 
        {
            m_nonPersonEntityDetail = nonPersonEntityDetail;
        }
        public void SetQualifiedWrittenRequestMailTo(XEQualifiedWrittenRequestMailTo qualifiedWrittenRequestMailTo) 
        {
            m_qualifiedWrittenRequestMailTo = qualifiedWrittenRequestMailTo;
        }
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string Country 
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string DaysOfTheWeekDescription 
        {
            get { return m_daysOfTheWeekDescription; }
            set { m_daysOfTheWeekDescription = value; }
        }
        public string DirectInquiryToDescription 
        {
            get { return m_directInquiryToDescription; }
            set { m_directInquiryToDescription = value; }
        }
        public string HoursOfTheDayDescription 
        {
            get { return m_hoursOfTheDayDescription; }
            set { m_hoursOfTheDayDescription = value; }
        }
        public string InquiryTelephoneNumber 
        {
            get { return m_inquiryTelephoneNumber; }
            set { m_inquiryTelephoneNumber = value; }
        }
        public string PaymentAcceptanceEndDate 
        {
            get { return m_paymentAcceptanceEndDate; }
            set { m_paymentAcceptanceEndDate = value; }
        }
        public string PaymentAcceptanceStartDate 
        {
            get { return m_paymentAcceptanceStartDate; }
            set { m_paymentAcceptanceStartDate = value; }
        }
        public string TransferEffectiveDate 
        {
            get { return m_transferEffectiveDate; }
            set { m_transferEffectiveDate = value; }
        }
        public E_ServicerType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        public E_YesNoIndicator NonPersonEntityIndicator 
        {
            get { return m_nonPersonEntityIndicator; }
            set { m_nonPersonEntityIndicator = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("SERVICER");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "_DaysOfTheWeekDescription", m_daysOfTheWeekDescription);
            WriteAttribute(writer, "_DirectInquiryToDescription", m_directInquiryToDescription);
            WriteAttribute(writer, "_HoursOfTheDayDescription", m_hoursOfTheDayDescription);
            WriteAttribute(writer, "_InquiryTelephoneNumber", m_inquiryTelephoneNumber);
            WriteAttribute(writer, "_PaymentAcceptanceEndDate", m_paymentAcceptanceEndDate);
            WriteAttribute(writer, "_PaymentAcceptanceStartDate", m_paymentAcceptanceStartDate);
            WriteAttribute(writer, "_TransferEffectiveDate", m_transferEffectiveDate);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "NonPersonEntityIndicator", m_nonPersonEntityIndicator);

            WriteElement(writer, m_contactDetail);
            WriteElement(writer, m_nonPersonEntityDetail);
            WriteElement(writer, m_qualifiedWrittenRequestMailTo);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ServicerType 
    {
        Undefined = 0,
        New,
        Investor,
        Present,
        Other,
        Lender
    }
}
