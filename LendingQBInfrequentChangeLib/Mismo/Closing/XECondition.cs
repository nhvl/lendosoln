/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
    public class XECondition : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="_CONDITION">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_Description" type="xs:string"/>
        //          <xs:attribute name="_MetIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //          <xs:attribute name="_WaivedIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private string m_description;
        private E_YesNoIndicator m_metIndicator;
        private string m_sequenceIdentifier;
        private E_YesNoIndicator m_waivedIndicator;
        #endregion

        #region Public Properties
        public string Description 
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public E_YesNoIndicator MetIndicator 
        {
            get { return m_metIndicator; }
            set { m_metIndicator = value; }
        }
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public E_YesNoIndicator WaivedIndicator 
        {
            get { return m_waivedIndicator; }
            set { m_waivedIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_CONDITION");
            WriteAttribute(writer, "_Description", m_description);
            WriteAttribute(writer, "_MetIndicator", m_metIndicator);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "_WaivedIndicator", m_waivedIndicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
