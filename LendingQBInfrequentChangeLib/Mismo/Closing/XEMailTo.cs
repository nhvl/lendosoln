/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
	public class XEMailTo : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_MAIL_TO">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_StreetAddress" type="xs:string"/>
        //          <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //          <xs:attribute name="_City" type="xs:string"/>
        //          <xs:attribute name="_State" type="xs:string"/>
        //          <xs:attribute name="_PostalCode" type="xs:string"/>
        //          <xs:attribute name="_Country" type="xs:string"/>
        //          <xs:attribute name="_AddressSameAsPropertyIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_country;
        private E_YesNoIndicator m_addressSameAsPropertyIndicator;
        #endregion

        #region Public Properties
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string Country 
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public E_YesNoIndicator AddressSameAsPropertyIndicator 
        {
            get { return m_addressSameAsPropertyIndicator; }
            set { m_addressSameAsPropertyIndicator = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_MAIL_TO");
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "_AddressSameAsPropertyIndicator", m_addressSameAsPropertyIndicator);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
