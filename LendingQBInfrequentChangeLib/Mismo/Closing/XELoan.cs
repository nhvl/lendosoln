/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XELoan : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="LOAN">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_APPLICATION" minOccurs="0"/>
        //        <xs:element ref="_CLOSING_DOCUMENTS" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="MISMOVersionIdentifier" fixed="2.3.1" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_mismoVersionIdentifier = "2.3.1";
        private XEApplication m_application;
        private XEClosingDocuments m_closingDocuments;
        
        #endregion

        #region Public Properties
        public void SetApplication(XEApplication application) 
        {
            m_application = application;
        }
        public void SetClosingDocuments(XEClosingDocuments closingDocuments) 
        {
            m_closingDocuments = closingDocuments;
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN");
            WriteAttribute(writer, "MISMOVersionIdentifier", m_mismoVersionIdentifier);
            WriteElement(writer, m_application);
            WriteElement(writer, m_closingDocuments);
            writer.WriteEndElement(); // </LOAN>
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
