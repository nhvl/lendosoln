/// Author: David Dao

using System;
using System.Xml;
namespace Mismo.Closing
{
    public class XECertificate : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="_CERTIFICATE">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_SIGNER_IDENTIFICATION" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_SigningDate" type="xs:string"/>
        //      <xs:attribute name="_SigningCounty" type="xs:string"/>
        //      <xs:attribute name="_SigningState" type="xs:string"/>
        //      <xs:attribute name="_SignerFirstName" type="xs:string"/>
        //      <xs:attribute name="_SignerMiddleName" type="xs:string"/>
        //      <xs:attribute name="_SignerLastName" type="xs:string"/>
        //      <xs:attribute name="_SignerNameSuffix" type="xs:string"/>
        //      <xs:attribute name="_SignerUnparsedName" type="xs:string"/>
        //      <xs:attribute name="_SignerTitleDescription" type="xs:string"/>
        //      <xs:attribute name="_SignerCompanyName" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private XESignerIdentification m_signerIdentification;
        private string m_signingDate;
        private string m_signingCounty;
        private string m_signingState;
        private string m_signerFirstName;
        private string m_signerMiddleName;
        private string m_signerLastName;
        private string m_signerNameSuffix;
        private string m_signerUnparsedName;
        private string m_signerTitleDescription;
        private string m_signerCompanyName;

        #endregion

        #region Public Properties
        public void SetSignerIdentification(XESignerIdentification signerIdentification) 
        {
            m_signerIdentification = signerIdentification;
        }
        public string SigningDate 
        {
            get { return m_signingDate; }
            set { m_signingDate = value; }
        }
        public string SigningCounty 
        {
            get { return m_signingCounty; }
            set { m_signingCounty = value; }
        }
        public string SigningState 
        {
            get { return m_signingState; }
            set { m_signingState = value; }
        }
        public string SignerFirstName 
        {
            get { return m_signerFirstName; }
            set { m_signerFirstName = value; }
        }
        public string SignerMiddleName 
        {
            get { return m_signerMiddleName; }
            set { m_signerMiddleName = value; }
        }
        public string SignerLastName 
        {
            get { return m_signerLastName; }
            set { m_signerLastName = value; }
        }
        public string SignerNameSuffix 
        {
            get { return m_signerNameSuffix; }
            set { m_signerNameSuffix = value; }
        }
        public string SignerUnparsedName 
        {
            get { return m_signerUnparsedName; }
            set { m_signerUnparsedName = value; }
        }
        public string SignerTitleDescription 
        {
            get { return m_signerTitleDescription; }
            set { m_signerTitleDescription = value; }
        }
        public string SignerCompanyName 
        {
            get { return m_signerCompanyName; }
            set { m_signerCompanyName = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_CERTIFICATE");
            WriteAttribute(writer, "_SigningDate", m_signingDate);
            WriteAttribute(writer, "_SigningCounty", m_signingCounty);
            WriteAttribute(writer, "_SigningState", m_signingState);
            WriteAttribute(writer, "_SignerFirstName", m_signerFirstName);
            WriteAttribute(writer, "_SignerMiddleName", m_signerMiddleName);
            WriteAttribute(writer, "_SignerLastName", m_signerLastName);
            WriteAttribute(writer, "_SignerNameSuffix", m_signerNameSuffix);
            WriteAttribute(writer, "_SignerUnparsedName", m_signerUnparsedName);
            WriteAttribute(writer, "_SignerTitleDescription", m_signerTitleDescription);
            WriteAttribute(writer, "_SignerCompanyName", m_signerCompanyName);

            WriteElement(writer, m_signerIdentification);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
