/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEPayOff : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="PAYOFF">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="PAYEE" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_AccountNumberIdentifier" type="xs:string"/>
        //      <xs:attribute name="_Amount" type="xs:string"/>
        //      <xs:attribute name="_PerDiemAmount" type="xs:string"/>
        //      <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //      <xs:attribute name="_SpecifiedHUDLineNumber" type="xs:string"/>
        //      <xs:attribute name="_ThroughDate" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private XEPayee m_payee;
        private string m_accountNumberIdentifier;
        private string m_amount;
        private string m_perDiemAmount;
        private string m_sequenceIdentifier;
        private string m_specifiedHUDLineNumber;
        private string m_throughDate;

        #endregion

        #region Public Properties
        public void SetPayee(XEPayee payee) 
        {
            m_payee = payee;
        }
        public string AccountNumberIdentifier 
        {
            get { return m_accountNumberIdentifier; }
            set { m_accountNumberIdentifier = value; }
        }
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public string PerDiemAmount 
        {
            get { return m_perDiemAmount; }
            set { m_perDiemAmount = value; }
        }
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public string SpecifiedHUDLineNumber 
        {
            get { return m_specifiedHUDLineNumber; }
            set { m_specifiedHUDLineNumber = value; }
        }
        public string ThroughDate 
        {
            get { return m_throughDate; }
            set { m_throughDate = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PAYOFF");
            WriteAttribute(writer, "_AccountNumberIdentifier", m_accountNumberIdentifier);
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_PerDiemAmount", m_perDiemAmount);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "_SpecifiedHUDLineNumber", m_specifiedHUDLineNumber);
            WriteAttribute(writer, "_ThroughDate", m_throughDate);

            WriteElement(writer, m_payee);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
