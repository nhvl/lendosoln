/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XECompensation : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="COMPENSATION">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_Amount" type="xs:string"/>
        //          <xs:attribute name="_PaidByType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Investor"/>
        //                <xs:enumeration value="Lender"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_PaidToType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Broker"/>
        //                <xs:enumeration value="Lender"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_Percent" type="xs:string"/>
        //          <xs:attribute name="_Type">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="YieldSpreadDifferential"/>
        //                <xs:enumeration value="Rebate"/>
        //                <xs:enumeration value="LenderCompensation"/>
        //                <xs:enumeration value="BrokerCompensation"/>
        //                <xs:enumeration value="ServiceReleasePremium"/>
        //                <xs:enumeration value="Other"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_amount;
        private E_CompensationPaidByType m_paidByType;
        private E_CompensationPaidToType m_paidToType;
        private string m_percent;
        private E_CompensationType m_type;
        private string m_typeOtherDescription;
        
        #endregion

        #region Public Properties
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public E_CompensationPaidByType PaidByType 
        {
            get { return m_paidByType; }
            set { m_paidByType = value; }
        }
        public E_CompensationPaidToType PaidToType 
        {
            get { return m_paidToType; }
            set { m_paidToType = value; }
        }
        public string Percent 
        {
            get { return m_percent; }
            set { m_percent = value; }
        }
        public E_CompensationType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("COMPENSATION");
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_PaidByType", m_paidByType);
            WriteAttribute(writer, "_PaidToType", m_paidToType);
            WriteAttribute(writer, "_Percent", m_percent);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_CompensationPaidByType 
    {
        Undefined = 0,
        Investor,
        Lender
    }
    public enum E_CompensationPaidToType 
    {
        Undefined = 0,
        Broker,
        Lender
    }
    public enum E_CompensationType 
    {
        Undefined = 0,
        YieldSpreadDifferential,
        Rebate,
        LenderCompensation,
        BrokerCompensation,
        ServiceReleasePremium,
        Other
    }
}
