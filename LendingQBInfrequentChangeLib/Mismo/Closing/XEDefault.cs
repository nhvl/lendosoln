/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
	public class XEDefault : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="DEFAULT">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_ApplicationFeesAmount" type="xs:string"/>
        //          <xs:attribute name="_ClosingPreparationFeesAmount" type="xs:string"/>
        //          <xs:attribute name="_LendingInstitutionPostOfficeBoxAddress" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator;
        private string m_applicationFeesAmount;
        private string m_closingPreparationFeesAmount;
        private string m_lendingInstitutionPostOfficeBoxAddress;

        #endregion

        #region Public Properties
        public E_YesNoIndicator AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator 
        {
            get { return m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator; }
            set { m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = value; }
        }
        public string ApplicationFeesAmount 
        {
            get { return m_applicationFeesAmount; }
            set { m_applicationFeesAmount = value; }
        }
        public string ClosingPreparationFeesAmount 
        {
            get { return m_closingPreparationFeesAmount; }
            set { m_closingPreparationFeesAmount = value; }
        }
        public string LendingInstitutionPostOfficeBoxAddress 
        {
            get { return m_lendingInstitutionPostOfficeBoxAddress; }
            set { m_lendingInstitutionPostOfficeBoxAddress = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("DEFAULT");
            WriteAttribute(writer, "_AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator", m_acknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator);
            WriteAttribute(writer, "_ApplicationFeesAmount", m_applicationFeesAmount);
            WriteAttribute(writer, "_ClosingPreparationFeesAmount", m_closingPreparationFeesAmount);
            WriteAttribute(writer, "_LendingInstitutionPostOfficeBoxAddress", m_lendingInstitutionPostOfficeBoxAddress);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
