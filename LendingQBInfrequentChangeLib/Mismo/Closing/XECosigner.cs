/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XECosigner : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="COSIGNER">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_TitleDescription" type="xs:string"/>
        //          <xs:attribute name="_UnparsedName" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_titleDescription;
        private string m_unparsedName;
        
        #endregion

        #region Public Properties
        public string TitleDescription 
        {
            get { return m_titleDescription; }
            set { m_titleDescription = value; }
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("COSIGNER");
            WriteAttribute(writer, "_TitleDescription", m_titleDescription);
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
