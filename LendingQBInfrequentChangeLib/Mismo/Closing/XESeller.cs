/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
	public class XESeller : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="SELLER">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="CONTACT_DETAIL" minOccurs="0"/>
        //        <xs:element ref="NON_PERSON_ENTITY_DETAIL" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_FirstName" type="xs:string"/>
        //      <xs:attribute name="_MiddleName" type="xs:string"/>
        //      <xs:attribute name="_LastName" type="xs:string"/>
        //      <xs:attribute name="_NameSuffix" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="_Country" type="xs:string"/>
        //      <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //      <xs:attribute name="NonPersonEntityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_UnparsedName" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private Mismo.MismoXmlElement.XEContactDetail m_contactDetail;
        private XENonPersonEntityDetail m_nonPersonEntityDetail;
        private string m_firstName;
        private string m_middleName;
        private string m_lastName;
        private string m_nameSuffix;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_country;
        private string m_sequenceIdentifier;
        private E_YesNoIndicator m_nonPersonEntityIndicator;
        private string m_unparsedName;

        #endregion

        #region Public Properties
        public void SetContactDetail(Mismo.MismoXmlElement.XEContactDetail contactDetail) 
        {
            m_contactDetail = contactDetail;
        }
        public void SetNonPersonEntityDetail(XENonPersonEntityDetail nonPersonEntityDetail) 
        {
            m_nonPersonEntityDetail = nonPersonEntityDetail;
        }
        public string FirstName 
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }
        public string MiddleName 
        {
            get { return m_middleName; }
            set { m_middleName = value; }
        }
        public string LastName 
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }
        public string NameSuffix 
        {
            get { return m_nameSuffix; }
            set { m_nameSuffix = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string Country 
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public E_YesNoIndicator NonPersonEntityIndicator 
        {
            get { return m_nonPersonEntityIndicator; }
            set { m_nonPersonEntityIndicator = value; }
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("SELLER");
            WriteAttribute(writer, "_FirstName", m_firstName);
            WriteAttribute(writer, "_MiddleName", m_middleName);
            WriteAttribute(writer, "_LastName", m_lastName);
            WriteAttribute(writer, "_NameSuffix", m_nameSuffix);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "NonPersonEntityIndicator", m_nonPersonEntityIndicator);
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);

            WriteElement(writer, m_contactDetail);
            WriteElement(writer, m_nonPersonEntityDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
