/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEConversionOption : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="_CONVERSION_OPTION">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_EndingChangeDatePeriodDescription" type="xs:string"/>
        //          <xs:attribute name="_FeeAmount" type="xs:string"/>
        //          <xs:attribute name="_NoteTermGreaterThanFifteenYearsAdditionalPercent" type="xs:string"/>
        //          <xs:attribute name="_NoteTermLessThanFifteenYearsAdditionalPercent" type="xs:string"/>
        //          <xs:attribute name="_PeriodEndDate" type="xs:string"/>
        //          <xs:attribute name="_PeriodStartDate" type="xs:string"/>
        //          <xs:attribute name="_StartingChangeDatePeriodDescription" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_endingChangeDatePeriodDescription;
        private string m_feeAmount;
        private string m_noteTermGreaterThanFifteenYearsAdditionalPercent;
        private string m_noteTermLessThanFifteenYearsAdditionalPercent;
        private string m_periodEndDate;
        private string m_periodStartDate;
        private string m_startingChangeDatePeriodDescription;
        #endregion

        #region Public Properties
        public string EndingChangeDatePeriodDescription 
        {
            get { return m_endingChangeDatePeriodDescription; }
            set { m_endingChangeDatePeriodDescription = value; }
        }
        public string FeeAmount 
        {
            get { return m_feeAmount; }
            set { m_feeAmount = value; }
        }
        public string NoteTermGreaterThanFifteenYearsAdditionalPercent 
        {
            get { return m_noteTermGreaterThanFifteenYearsAdditionalPercent; }
            set { m_noteTermGreaterThanFifteenYearsAdditionalPercent = value; }
        }
        public string NoteTermLessThanFifteenYearsAdditionalPercent 
        {
            get { return m_noteTermLessThanFifteenYearsAdditionalPercent; }
            set { m_noteTermLessThanFifteenYearsAdditionalPercent = value; }
        }
        public string PeriodEndDate 
        {
            get { return m_periodEndDate; }
            set { m_periodEndDate = value; }
        }
        public string PeriodStartDate 
        {
            get { return m_periodStartDate; }
            set { m_periodStartDate = value; }
        }
        public string StartingChangeDatePeriodDescription 
        {
            get { return m_startingChangeDatePeriodDescription; }
            set { m_startingChangeDatePeriodDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_CONVERSION_OPTION");
            WriteAttribute(writer, "_EndingChangeDatePeriodDescription", m_endingChangeDatePeriodDescription);
            WriteAttribute(writer, "_FeeAmount", m_feeAmount);
            WriteAttribute(writer, "_NoteTermGreaterThanFifteenYearsAdditionalPercent", m_noteTermGreaterThanFifteenYearsAdditionalPercent);
            WriteAttribute(writer, "_NoteTermLessThanFifteenYearsAdditionalPercent", m_noteTermLessThanFifteenYearsAdditionalPercent);
            WriteAttribute(writer, "_PeriodEndDate", m_periodEndDate);
            WriteAttribute(writer, "_PeriodStartDate", m_periodStartDate);
            WriteAttribute(writer, "_StartingChangeDatePeriodDescription", m_startingChangeDatePeriodDescription);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
