/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
    public class XERespaHudDetail : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="RESPA_HUD_DETAIL">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_SpecifiedHUDLineNumber" type="xs:string"/>
        //          <xs:attribute name="_LineItemAmount" type="xs:string"/>
        //          <xs:attribute name="_LineItemDescription" type="xs:string"/>
        //          <xs:attribute name="_LineItemPaidByType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="LenderPremium"/>
        //                <xs:enumeration value="Buyer"/>
        //                <xs:enumeration value="Seller"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="HUD1SettlementAgentUnparsedName" type="xs:string"/>
        //          <xs:attribute name="HUD1SettlementAgentUnparsedAddress" type="xs:string"/>
        //          <xs:attribute name="HUD1SettlementDate" type="xs:string"/>
        //          <xs:attribute name="HUD1LenderUnparsedName" type="xs:string"/>
        //          <xs:attribute name="HUD1LenderUnparsedAddress" type="xs:string"/>
        //          <xs:attribute name="HUD1CashToOrFromBorrowerIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="HUD1CashToOrFromSellerIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="HUD1ConventionalInsuredIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="HUD1FileNumberIdentifier" type="xs:string"/>
        //          <xs:attribute name="HUD1LineItemToDate" type="xs:string"/>
        //          <xs:attribute name="HUD1LineItemFromDate" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_specifiedHUDLineNumber;
        private string m_lineItemAmount;
        private string m_lineItemDescription;
        private E_RespaHudDetailLineItemPaidByType m_lineItemPaidByType;
        private string m_hud1SettlementAgentUnparsedName;
        private string m_hud1SettlementAgentUnparsedAddress;
        private string m_hud1SettlementDate;
        private string m_hud1LenderUnparsedName;
        private string m_hud1LenderUnparsedAddress;
        private E_YesNoIndicator m_hud1CashToOrFromBorrowerIndicator;
        private E_YesNoIndicator m_hud1CashToOrFromSellerIndicator;
        private E_YesNoIndicator m_hud1ConventionalInsuredIndicator;
        private string m_hud1FileNumberIdentifier;
        private string m_hud1LineItemToDate;
        private string m_hud1LineItemFromDate;
        #endregion

        #region Public Properties
        public string SpecifiedHUDLineNumber 
        {
            get { return m_specifiedHUDLineNumber; }
            set { m_specifiedHUDLineNumber = value; }
        }
        public string LineItemAmount 
        {
            get { return m_lineItemAmount; }
            set { m_lineItemAmount = value; }
        }
        public string LineItemDescription 
        {
            get { return m_lineItemDescription; }
            set { m_lineItemDescription = value; }
        }
        public E_RespaHudDetailLineItemPaidByType LineItemPaidByType 
        {
            get { return m_lineItemPaidByType; }
            set { m_lineItemPaidByType = value; }
        }
        public string Hud1SettlementAgentUnparsedName 
        {
            get { return m_hud1SettlementAgentUnparsedName; }
            set { m_hud1SettlementAgentUnparsedName = value; }
        }
        public string Hud1SettlementAgentUnparsedAddress 
        {
            get { return m_hud1SettlementAgentUnparsedAddress; }
            set { m_hud1SettlementAgentUnparsedAddress = value; }
        }
        public string Hud1SettlementDate 
        {
            get { return m_hud1SettlementDate; }
            set { m_hud1SettlementDate = value; }
        }
        public string Hud1LenderUnparsedName 
        {
            get { return m_hud1LenderUnparsedName; }
            set { m_hud1LenderUnparsedName = value; }
        }
        public string Hud1LenderUnparsedAddress 
        {
            get { return m_hud1LenderUnparsedAddress; }
            set { m_hud1LenderUnparsedAddress = value; }
        }
        public E_YesNoIndicator Hud1CashToOrFromBorrowerIndicator 
        {
            get { return m_hud1CashToOrFromBorrowerIndicator; }
            set { m_hud1CashToOrFromBorrowerIndicator = value; }
        }
        public E_YesNoIndicator Hud1CashToOrFromSellerIndicator 
        {
            get { return m_hud1CashToOrFromSellerIndicator; }
            set { m_hud1CashToOrFromSellerIndicator = value; }
        }
        public E_YesNoIndicator Hud1ConventionalInsuredIndicator 
        {
            get { return m_hud1ConventionalInsuredIndicator; }
            set { m_hud1ConventionalInsuredIndicator = value; }
        }
        public string Hud1FileNumberIdentifier 
        {
            get { return m_hud1FileNumberIdentifier; }
            set { m_hud1FileNumberIdentifier = value; }
        }
        public string Hud1LineItemToDate 
        {
            get { return m_hud1LineItemToDate; }
            set { m_hud1LineItemToDate = value; }
        }
        public string Hud1LineItemFromDate 
        {
            get { return m_hud1LineItemFromDate; }
            set { m_hud1LineItemFromDate = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RESPA_HUD_DETAIL");
            WriteAttribute(writer, "_SpecifiedHUDLineNumber", m_specifiedHUDLineNumber);
            WriteAttribute(writer, "_LineItemAmount", m_lineItemAmount);
            WriteAttribute(writer, "_LineItemDescription", m_lineItemDescription);
            WriteAttribute(writer, "_LineItemPaidByType", m_lineItemPaidByType);
            WriteAttribute(writer, "HUD1SettlementAgentUnparsedName", m_hud1SettlementAgentUnparsedName);
            WriteAttribute(writer, "HUD1SettlementAgentUnparsedAddress", m_hud1SettlementAgentUnparsedAddress);
            WriteAttribute(writer, "HUD1SettlementDate", m_hud1SettlementDate);
            WriteAttribute(writer, "HUD1LenderUnparsedName", m_hud1LenderUnparsedName);
            WriteAttribute(writer, "HUD1LenderUnparsedAddress", m_hud1LenderUnparsedAddress);
            WriteAttribute(writer, "HUD1CashToOrFromBorrowerIndicator", m_hud1CashToOrFromBorrowerIndicator);
            WriteAttribute(writer, "HUD1CashToOrFromSellerIndicator", m_hud1CashToOrFromSellerIndicator);
            WriteAttribute(writer, "HUD1ConventionalInsuredIndicator", m_hud1ConventionalInsuredIndicator);
            WriteAttribute(writer, "HUD1FileNumberIdentifier", m_hud1FileNumberIdentifier);
            WriteAttribute(writer, "HUD1LineItemToDate", m_hud1LineItemToDate);
            WriteAttribute(writer, "HUD1LineItemFromDate", m_hud1LineItemFromDate);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_RespaHudDetailLineItemPaidByType 
    {
        Undefined = 0,
        LenderPremium,
        Buyer,
        Seller
    }
}
