/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XEEscrowAccountSummary : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="ESCROW_ACCOUNT_SUMMARY">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="EscrowAggregateAccountingAdjustmentAmount" type="xs:string"/>
        //          <xs:attribute name="EscrowCushionNumberOfMonthsCount" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_escrowAggregateAccountingAdjustmentAmount;
        private string m_escrowCushionNumberOfMonthsCount;
        #endregion

        #region Public Properties
        public string EscrowAggregateAccountingAdjustmentAmount 
        {
            get { return m_escrowAggregateAccountingAdjustmentAmount; }
            set { m_escrowAggregateAccountingAdjustmentAmount = value; }
        }
        public string EscrowCushionNumberOfMonthsCount 
        {
            get { return m_escrowCushionNumberOfMonthsCount; }
            set { m_escrowCushionNumberOfMonthsCount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ESCROW_ACCOUNT_SUMMARY");
            WriteAttribute(writer, "EscrowAggregateAccountingAdjustmentAmount", m_escrowAggregateAccountingAdjustmentAmount);
            WriteAttribute(writer, "EscrowCushionNumberOfMonthsCount", m_escrowCushionNumberOfMonthsCount);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
