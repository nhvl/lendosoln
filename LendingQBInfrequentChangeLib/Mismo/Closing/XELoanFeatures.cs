/// Author: David Dao

using System;
using System.Xml;

using Mismo.Common;
namespace Mismo.Closing
{
    public class XELoanFeatures : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="LOAN_FEATURES">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="LATE_CHARGE" minOccurs="0"/>
        //        <xs:element ref="NOTE_PAY_TO" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="AssumabilityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BalloonIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="BalloonLoanMaturityTermMonths" type="xs:string"/>
        //      <xs:attribute name="BuydownTemporarySubsidyIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="CounselingConfirmationIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="DownPaymentOptionType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="FivePercentOption"/>
        //            <xs:enumeration value="FNM97Option"/>
        //            <xs:enumeration value="ThreeTwoOption"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="EscrowWaiverIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="FNMProductPlanIdentifier" type="xs:string"/>
        //      <xs:attribute name="FREOfferingIdentifier" type="xs:string"/>
        //      <xs:attribute name="GSEProjectClassificationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="OneCooperative"/>
        //            <xs:enumeration value="ApprovedFHA_VACondominiumProjectOrSpotLoan"/>
        //            <xs:enumeration value="F_PUD"/>
        //            <xs:enumeration value="C_ICondominium"/>
        //            <xs:enumeration value="E_PUD"/>
        //            <xs:enumeration value="A_IIICondominium"/>
        //            <xs:enumeration value="TwoCooperative"/>
        //            <xs:enumeration value="B_IICondominium"/>
        //            <xs:enumeration value="III_PUD"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="GSEPropertyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Condominium"/>
        //            <xs:enumeration value="HighRiseCondominium"/>
        //            <xs:enumeration value="Modular"/>
        //            <xs:enumeration value="ManufacturedHousing"/>
        //            <xs:enumeration value="Detached"/>
        //            <xs:enumeration value="ManufacturedHousingSingleWide"/>
        //            <xs:enumeration value="Cooperative"/>
        //            <xs:enumeration value="PUD"/>
        //            <xs:enumeration value="DetachedCondominium"/>
        //            <xs:enumeration value="ManufacturedHomeCondominiumOrPUDOrCooperative"/>
        //            <xs:enumeration value="ManufacturedHousingDoubleWide"/>
        //            <xs:enumeration value="ManufacturedHousingMultiWide"/>
        //            <xs:enumeration value="Attached"/>
        //            <xs:enumeration value="ManufacturedHomeCondominium"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="HELOCMaximumBalanceAmount" type="xs:string"/>
        //      <xs:attribute name="HELOCInitialAdvanceAmount" type="xs:string"/>
        //      <xs:attribute name="InterestOnlyTerm" type="xs:string"/>
        //      <xs:attribute name="LenderSelfInsuredIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LienPriorityType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="FirstLien"/>
        //            <xs:enumeration value="SecondLien"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanClosingStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="TableFunded"/>
        //            <xs:enumeration value="Closed"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanDocumentationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="OnePaystub"/>
        //            <xs:enumeration value="OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040"/>
        //            <xs:enumeration value="NoDepositVerification"/>
        //            <xs:enumeration value="VerbalVerificationOfEmployment"/>
        //            <xs:enumeration value="NoEmploymentVerificationOrIncomeVerification"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncomeOrEmployment"/>
        //            <xs:enumeration value="NoIncomeOn1003"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncome"/>
        //            <xs:enumeration value="NoDepositVerificationEmploymentVerificationOrIncomeVerification"/>
        //            <xs:enumeration value="NoRatio"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncomeEmploymentOrAssets"/>
        //            <xs:enumeration value="NoDocumentation"/>
        //            <xs:enumeration value="NoVerificationOfStatedAssets"/>
        //            <xs:enumeration value="FullDocumentation"/>
        //            <xs:enumeration value="NoIncomeNoEmploymentNoAssetsOn1003"/>
        //            <xs:enumeration value="OnePaystubAndVerbalVerificationOfEmployment"/>
        //            <xs:enumeration value="StreamlineRefinance"/>
        //            <xs:enumeration value="Alternative"/>
        //            <xs:enumeration value="NoVerificationOfStatedIncomeOrAssets"/>
        //            <xs:enumeration value="Reduced"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanRepaymentType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="PotentialNegativeAmortization"/>
        //            <xs:enumeration value="ScheduledNegativeAmortization"/>
        //            <xs:enumeration value="ScheduledAmortization"/>
        //            <xs:enumeration value="NoNegativeAmortization"/>
        //            <xs:enumeration value="InterestOnly"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LoanScheduledClosingDate" type="xs:string"/>
        //      <xs:attribute name="MICertificationStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="LenderToObtain"/>
        //            <xs:enumeration value="SellerOfLoanToObtain"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MICompanyNameType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="GECapitalMICorporation"/>
        //            <xs:enumeration value="MortgageGuarantyInsuranceCorporation"/>
        //            <xs:enumeration value="AmerinGuaranteeCorporation"/>
        //            <xs:enumeration value="PMI_MICorporation"/>
        //            <xs:enumeration value="RepublicMICompany"/>
        //            <xs:enumeration value="TriadGuarantyInsuranceCorporation"/>
        //            <xs:enumeration value="RadianGuarantyIncorporated"/>
        //            <xs:enumeration value="CMG_MICompany"/>
        //            <xs:enumeration value="UnitedGuarantyCorporation"/>
        //            <xs:enumeration value="CommonwealthMortgageAssuranceCompany"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MICoveragePercent" type="xs:string"/>
        //      <xs:attribute name="NameDocumentsDrawnInType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Broker"/>
        //            <xs:enumeration value="Investor"/>
        //            <xs:enumeration value="Lender"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="NegativeAmortizationLimitPercent" type="xs:string"/>
        //      <xs:attribute name="PaymentFrequencyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Monthly"/>
        //            <xs:enumeration value="AtMaturity"/>
        //            <xs:enumeration value="Annual"/>
        //            <xs:enumeration value="Biweekly"/>
        //            <xs:enumeration value="Weekly"/>
        //            <xs:enumeration value="Semiannual"/>
        //            <xs:enumeration value="Quarterly"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Semimonthly"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PrepaymentPenaltyIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="FullPrepaymentPenaltyOptionType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Hard"/>
        //            <xs:enumeration value="Soft"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PrepaymentPenaltyTermMonths" type="xs:string"/>
        //      <xs:attribute name="PrepaymentRestrictionIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="ProductDescription" type="xs:string"/>
        //      <xs:attribute name="ProductName" type="xs:string"/>
        //      <xs:attribute name="ScheduledFirstPaymentDate" type="xs:string"/>
        //      <xs:attribute name="ServicingTransferStatusType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Released"/>
        //            <xs:enumeration value="Retained"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="ConformingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="RequiredDepositIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="DemandFeatureIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="EstimatedPrepaidDays" type="xs:string"/>
        //      <xs:attribute name="EstimatedPrepaidDaysPaidByType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Buyer"/>
        //            <xs:enumeration value="Seller"/>
        //            <xs:enumeration value="Lender"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="EstimatedPrepaidDaysPaidByOtherTypeDescription" type="xs:string"/>
        //      <xs:attribute name="PrepaymentFinanceChargeRefundableIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="GraduatedPaymentMultiplierFactor" type="xs:string"/>
        //      <xs:attribute name="LoanMaturityDate" type="xs:string"/>
        //      <xs:attribute name="LoanOriginalMaturityTermMonths" type="xs:string"/>
        //      <xs:attribute name="PaymentFrequencyTypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="OriginalPrincipalAndInterestPaymentAmount" type="xs:string"/>
        //      <xs:attribute name="TimelyPaymentRateReductionIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="TimelyPaymentRateReductionPercent" type="xs:string"/>
        //      <xs:attribute name="CounselingConfirmationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="LenderTrainedCounseling"/>
        //            <xs:enumeration value="NoBorrowerCounseling"/>
        //            <xs:enumeration value="ThirdPartyCounseling"/>
        //            <xs:enumeration value="AmericanHomeownerEducationInstituteApprovedCounseling"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="InitialPaymentRatePercent" type="xs:string"/>
        //      <xs:attribute name="RefundableApplicationFeeIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="OriginalBalloonTermMonths" type="xs:string"/>
        //      <xs:attribute name="GrowingEquityLoanPayoffYearsCount" type="xs:string"/>
        //      <xs:attribute name="ConditionsToAssumabilityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private XELateCharge m_lateCharge;
        private XENotePayTo m_notePayTo;
        private E_YesNoIndicator m_assumabilityIndicator;
        private E_YesNoIndicator m_balloonIndicator;
        private string m_balloonLoanMaturityTermMonths;
        private E_YesNoIndicator m_buydownTemporarySubsidyIndicator;
        private E_YesNoIndicator m_counselingConfirmationIndicator;
        private E_LoanFeaturesDownPaymentOptionType m_downPaymentOptionType;
        private E_YesNoIndicator m_escrowWaiverIndicator;
        private string m_fnmProductPlanIdentifier;
        private string m_freOfferingIdentifier;
        private E_LoanFeaturesGseProjectClassificationType m_gseProjectClassificationType;
        private E_LoanFeaturesGsePropertyType m_gsePropertyType;
        private string m_helocMaximumBalanceAmount;
        private string m_helocInitialAdvanceAmount;
        private string m_interestOnlyTerm;
        private E_YesNoIndicator m_lenderSelfInsuredIndicator;
        private E_LoanFeaturesLienPriorityType m_lienPriorityType;
        private E_LoanFeaturesLoanClosingStatusType m_loanClosingStatusType;
        private E_LoanFeaturesLoanDocumentationType m_loanDocumentationType;
        private E_LoanFeaturesLoanRepaymentType m_loanRepaymentType;
        private string m_loanScheduledClosingDate;
        private E_LoanFeaturesMiCertificationStatusType m_miCertificationStatusType;
        private E_LoanFeaturesMiCompanyNameType m_miCompanyNameType;
        private string m_miCoveragePercent;
        private E_LoanFeaturesNameDocumentsDrawnInType m_nameDocumentsDrawnInType;
        private string m_negativeAmortizationLimitPercent;
        private E_LoanFeaturesPaymentFrequencyType m_paymentFrequencyType;
        private E_YesNoIndicator m_prepaymentPenaltyIndicator;
        private E_LoanFeaturesFullPaymentPenaltyOptionType m_fullPrepaymentPenaltyOptionType;
        private string m_prepaymentPenaltyTermMonths;
        private E_YesNoIndicator m_prepaymentRestrictionIndicator;
        private string m_productDescription;
        private string m_productName;
        private string m_scheduledFirstPaymentDate;
        private E_LoanFeaturesServicingTransferStatusType m_servicingTransferStatusType;
        private E_YesNoIndicator m_conformingIndicator;
        private E_YesNoIndicator m_requiredDepositIndicator;
        private E_YesNoIndicator m_demandFeatureIndicator;
        private string m_estimatedPrepaidDays;
        private E_LoanFeaturesEstimatedPrepaidDaysPaidByType m_estimatedPrepaidDaysPaidByType;
        private string m_estimatedPrepaidDaysPaidByOtherTypeDescription;
        private E_YesNoIndicator m_prepaymentFinanceChargeRefundableIndicator;
        private string m_graduatedPaymentMultiplierFactor;
        private string m_loanMaturityDate;
        private string m_loanOriginalMaturityTermMonths;
        private string m_paymentFrequencyTypeOtherDescription;
        private string m_originalPrincipalAndInterestPaymentAmount;
        private E_YesNoIndicator m_timelyPaymentRateReductionIndicator;
        private string m_timelyPaymentRateReductionPercent;
        private E_LoanFeaturesCounselingConfirmationType m_counselingConfirmationType;
        private string m_initialPaymentRatePercent;
        private E_YesNoIndicator m_refundableApplicationFeeIndicator;
        private string m_originalBalloonTermMonths;
        private string m_growingEquityLoanPayoffYearsCount;
        private E_YesNoIndicator m_conditionsToAssumabilityIndicator;
        
        #endregion

        #region Public Properties
        public void AddLateCharge(XELateCharge lateCharge) 
        {
            m_lateCharge = lateCharge;
        }
        public void AddNotePayTo(XENotePayTo notePayTo) 
        {
            m_notePayTo = notePayTo;
        }
        public E_YesNoIndicator AssumabilityIndicator 
        {
            get { return m_assumabilityIndicator; }
            set { m_assumabilityIndicator = value; }
        }
        public E_YesNoIndicator BalloonIndicator 
        {
            get { return m_balloonIndicator; }
            set { m_balloonIndicator = value; }
        }
        public string BalloonLoanMaturityTermMonths 
        {
            get { return m_balloonLoanMaturityTermMonths; }
            set { m_balloonLoanMaturityTermMonths = value; }
        }
        public E_YesNoIndicator BuydownTemporarySubsidyIndicator 
        {
            get { return m_buydownTemporarySubsidyIndicator; }
            set { m_buydownTemporarySubsidyIndicator = value; }
        }
        public E_YesNoIndicator CounselingConfirmationIndicator 
        {
            get { return m_counselingConfirmationIndicator; }
            set { m_counselingConfirmationIndicator = value; }
        }
        public E_LoanFeaturesDownPaymentOptionType DownPaymentOptionType 
        {
            get { return m_downPaymentOptionType; }
            set { m_downPaymentOptionType = value; }
        }
        public E_YesNoIndicator EscrowWaiverIndicator 
        {
            get { return m_escrowWaiverIndicator; }
            set { m_escrowWaiverIndicator = value; }
        }
        public string FnmProductPlanIdentifier 
        {
            get { return m_fnmProductPlanIdentifier; }
            set { m_fnmProductPlanIdentifier = value; }
        }
        public string FreOfferingIdentifier 
        {
            get { return m_freOfferingIdentifier; }
            set { m_freOfferingIdentifier = value; }
        }
        public E_LoanFeaturesGseProjectClassificationType GseProjectClassificationType 
        {
            get { return m_gseProjectClassificationType; }
            set { m_gseProjectClassificationType = value; }
        }
        public E_LoanFeaturesGsePropertyType GsePropertyType 
        {
            get { return m_gsePropertyType; }
            set { m_gsePropertyType = value; }
        }
        public string HelocMaximumBalanceAmount 
        {
            get { return m_helocMaximumBalanceAmount; }
            set { m_helocMaximumBalanceAmount = value; }
        }
        public string HelocInitialAdvanceAmount 
        {
            get { return m_helocInitialAdvanceAmount; }
            set { m_helocInitialAdvanceAmount = value; }
        }
        public string InterestOnlyTerm 
        {
            get { return m_interestOnlyTerm; }
            set { m_interestOnlyTerm = value; }
        }
        public E_YesNoIndicator LenderSelfInsuredIndicator 
        {
            get { return m_lenderSelfInsuredIndicator; }
            set { m_lenderSelfInsuredIndicator = value; }
        }
        public E_LoanFeaturesLienPriorityType LienPriorityType 
        {
            get { return m_lienPriorityType; }
            set { m_lienPriorityType = value; }
        }
        public E_LoanFeaturesLoanClosingStatusType LoanClosingStatusType 
        {
            get { return m_loanClosingStatusType; }
            set { m_loanClosingStatusType = value; }
        }
        public E_LoanFeaturesLoanDocumentationType LoanDocumentationType 
        {
            get { return m_loanDocumentationType; }
            set { m_loanDocumentationType = value; }
        }
        public E_LoanFeaturesLoanRepaymentType LoanRepaymentType 
        {
            get { return m_loanRepaymentType; }
            set { m_loanRepaymentType = value; }
        }
        public string LoanScheduledClosingDate 
        {
            get { return m_loanScheduledClosingDate; }
            set { m_loanScheduledClosingDate = value; }
        }
        public E_LoanFeaturesMiCertificationStatusType MiCertificationStatusType 
        {
            get { return m_miCertificationStatusType; }
            set { m_miCertificationStatusType = value; }
        }
        public E_LoanFeaturesMiCompanyNameType MiCompanyNameType 
        {
            get { return m_miCompanyNameType; }
            set { m_miCompanyNameType = value; }
        }
        public string MiCoveragePercent 
        {
            get { return m_miCoveragePercent; }
            set { m_miCoveragePercent = value; }
        }
        public E_LoanFeaturesNameDocumentsDrawnInType NameDocumentsDrawnInType 
        {
            get { return m_nameDocumentsDrawnInType; }
            set { m_nameDocumentsDrawnInType = value; }
        }
        public string NegativeAmortizationLimitPercent 
        {
            get { return m_negativeAmortizationLimitPercent; }
            set { m_negativeAmortizationLimitPercent = value; }
        }
        public E_LoanFeaturesPaymentFrequencyType PaymentFrequencyType 
        {
            get { return m_paymentFrequencyType; }
            set { m_paymentFrequencyType = value; }
        }
        public E_YesNoIndicator PrepaymentPenaltyIndicator 
        {
            get { return m_prepaymentPenaltyIndicator; }
            set { m_prepaymentPenaltyIndicator = value; }
        }
        public E_LoanFeaturesFullPaymentPenaltyOptionType FullPrepaymentPenaltyOptionType 
        {
            get { return m_fullPrepaymentPenaltyOptionType; }
            set { m_fullPrepaymentPenaltyOptionType = value; }
        }
        public string PrepaymentPenaltyTermMonths 
        {
            get { return m_prepaymentPenaltyTermMonths; }
            set { m_prepaymentPenaltyTermMonths = value; }
        }
        public E_YesNoIndicator PrepaymentRestrictionIndicator 
        {
            get { return m_prepaymentRestrictionIndicator; }
            set { m_prepaymentRestrictionIndicator = value; }
        }
        public string ProductDescription 
        {
            get { return m_productDescription; }
            set { m_productDescription = value; }
        }
        public string ProductName 
        {
            get { return m_productName; }
            set { m_productName = value; }
        }
        public string ScheduledFirstPaymentDate 
        {
            get { return m_scheduledFirstPaymentDate; }
            set { m_scheduledFirstPaymentDate = value; }
        }
        public E_LoanFeaturesServicingTransferStatusType ServicingTransferStatusType 
        {
            get { return m_servicingTransferStatusType; }
            set { m_servicingTransferStatusType = value; }
        }
        public E_YesNoIndicator ConformingIndicator 
        {
            get { return m_conformingIndicator; }
            set { m_conformingIndicator = value; }
        }
        public E_YesNoIndicator RequiredDepositIndicator 
        {
            get { return m_requiredDepositIndicator; }
            set { m_requiredDepositIndicator = value; }
        }
        public E_YesNoIndicator DemandFeatureIndicator 
        {
            get { return m_demandFeatureIndicator; }
            set { m_demandFeatureIndicator = value; }
        }
        public string EstimatedPrepaidDays 
        {
            get { return m_estimatedPrepaidDays; }
            set { m_estimatedPrepaidDays = value; }
        }
        public E_LoanFeaturesEstimatedPrepaidDaysPaidByType EstimatedPrepaidDaysPaidByType 
        {
            get { return m_estimatedPrepaidDaysPaidByType; }
            set { m_estimatedPrepaidDaysPaidByType = value; }
        }
        public string EstimatedPrepaidDaysPaidByOtherTypeDescription 
        {
            get { return m_estimatedPrepaidDaysPaidByOtherTypeDescription; }
            set { m_estimatedPrepaidDaysPaidByOtherTypeDescription = value; }
        }
        public E_YesNoIndicator PrepaymentFinanceChargeRefundableIndicator 
        {
            get { return m_prepaymentFinanceChargeRefundableIndicator; }
            set { m_prepaymentFinanceChargeRefundableIndicator = value; }
        }
        public string GraduatedPaymentMultiplierFactor 
        {
            get { return m_graduatedPaymentMultiplierFactor; }
            set { m_graduatedPaymentMultiplierFactor = value; }
        }
        public string LoanMaturityDate 
        {
            get { return m_loanMaturityDate; }
            set { m_loanMaturityDate = value; }
        }
        public string LoanOriginalMaturityTermMonths 
        {
            get { return m_loanOriginalMaturityTermMonths; }
            set { m_loanOriginalMaturityTermMonths = value; }
        }
        public string PaymentFrequencyTypeOtherDescription 
        {
            get { return m_paymentFrequencyTypeOtherDescription; }
            set { m_paymentFrequencyTypeOtherDescription = value; }
        }
        public string OriginalPrincipalAndInterestPaymentAmount 
        {
            get { return m_originalPrincipalAndInterestPaymentAmount; }
            set { m_originalPrincipalAndInterestPaymentAmount = value; }
        }
        public E_YesNoIndicator TimelyPaymentRateReductionIndicator 
        {
            get { return m_timelyPaymentRateReductionIndicator; }
            set { m_timelyPaymentRateReductionIndicator = value; }
        }
        public string TimelyPaymentRateReductionPercent 
        {
            get { return m_timelyPaymentRateReductionPercent; }
            set { m_timelyPaymentRateReductionPercent = value; }
        }
        public E_LoanFeaturesCounselingConfirmationType CounselingConfirmationType 
        {
            get { return m_counselingConfirmationType; }
            set { m_counselingConfirmationType = value; }
        }
        public string InitialPaymentRatePercent 
        {
            get { return m_initialPaymentRatePercent; }
            set { m_initialPaymentRatePercent = value; }
        }
        public E_YesNoIndicator RefundableApplicationFeeIndicator 
        {
            get { return m_refundableApplicationFeeIndicator; }
            set { m_refundableApplicationFeeIndicator = value; }
        }
        public string OriginalBalloonTermMonths 
        {
            get { return m_originalBalloonTermMonths; }
            set { m_originalBalloonTermMonths = value; }
        }
        public string GrowingEquityLoanPayoffYearsCount 
        {
            get { return m_growingEquityLoanPayoffYearsCount; }
            set { m_growingEquityLoanPayoffYearsCount = value; }
        }
        public E_YesNoIndicator ConditionsToAssumabilityIndicator 
        {
            get { return m_conditionsToAssumabilityIndicator; }
            set { m_conditionsToAssumabilityIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_FEATURES");
            WriteAttribute(writer, "AssumabilityIndicator", m_assumabilityIndicator);
            WriteAttribute(writer, "BalloonIndicator", m_balloonIndicator);
            WriteAttribute(writer, "BalloonLoanMaturityTermMonths", m_balloonLoanMaturityTermMonths);
            WriteAttribute(writer, "BuydownTemporarySubsidyIndicator", m_buydownTemporarySubsidyIndicator);
            WriteAttribute(writer, "CounselingConfirmationIndicator", m_counselingConfirmationIndicator);
            WriteAttribute(writer, "DownPaymentOptionType", m_downPaymentOptionType);
            WriteAttribute(writer, "EscrowWaiverIndicator", m_escrowWaiverIndicator);
            WriteAttribute(writer, "FNMProductPlanIdentifier", m_fnmProductPlanIdentifier);
            WriteAttribute(writer, "FREOfferingIdentifier", m_freOfferingIdentifier);
            WriteAttribute(writer, "GSEProjectClassificationType", m_gseProjectClassificationType);
            WriteAttribute(writer, "GSEPropertyType", m_gsePropertyType);
            WriteAttribute(writer, "HELOCMaximumBalanceAmount", m_helocMaximumBalanceAmount);
            WriteAttribute(writer, "HELOCInitialAdvanceAmount", m_helocInitialAdvanceAmount);
            WriteAttribute(writer, "InterestOnlyTerm", m_interestOnlyTerm);
            WriteAttribute(writer, "LenderSelfInsuredIndicator", m_lenderSelfInsuredIndicator);
            WriteAttribute(writer, "LienPriorityType", m_lienPriorityType);
            WriteAttribute(writer, "LoanClosingStatusType", m_loanClosingStatusType);
            WriteAttribute(writer, "LoanDocumentationType", m_loanDocumentationType);
            WriteAttribute(writer, "LoanRepaymentType", m_loanRepaymentType);
            WriteAttribute(writer, "LoanScheduledClosingDate", m_loanScheduledClosingDate);
            WriteAttribute(writer, "MICertificationStatusType", m_miCertificationStatusType);
            WriteAttribute(writer, "MICompanyNameType", m_miCompanyNameType);
            WriteAttribute(writer, "MICoveragePercent", m_miCoveragePercent);
            WriteAttribute(writer, "NameDocumentsDrawnInType", m_nameDocumentsDrawnInType);
            WriteAttribute(writer, "NegativeAmortizationLimitPercent", m_negativeAmortizationLimitPercent);
            WriteAttribute(writer, "PaymentFrequencyType", m_paymentFrequencyType);
            WriteAttribute(writer, "PrepaymentPenaltyIndicator", m_prepaymentPenaltyIndicator);
            WriteAttribute(writer, "FullPrepaymentPenaltyOptionType", m_fullPrepaymentPenaltyOptionType);
            WriteAttribute(writer, "PrepaymentPenaltyTermMonths", m_prepaymentPenaltyTermMonths);
            WriteAttribute(writer, "PrepaymentRestrictionIndicator", m_prepaymentRestrictionIndicator);
            WriteAttribute(writer, "ProductDescription", m_productDescription);
            WriteAttribute(writer, "ProductName", m_productName);
            WriteAttribute(writer, "ScheduledFirstPaymentDate", m_scheduledFirstPaymentDate);
            WriteAttribute(writer, "ServicingTransferStatusType", m_servicingTransferStatusType);
            WriteAttribute(writer, "ConformingIndicator", m_conformingIndicator);
            WriteAttribute(writer, "RequiredDepositIndicator", m_requiredDepositIndicator);
            WriteAttribute(writer, "DemandFeatureIndicator", m_demandFeatureIndicator);
            WriteAttribute(writer, "EstimatedPrepaidDays", m_estimatedPrepaidDays);
            WriteAttribute(writer, "EstimatedPrepaidDaysPaidByType", m_estimatedPrepaidDaysPaidByType);
            WriteAttribute(writer, "EstimatedPrepaidDaysPaidByOtherTypeDescription", m_estimatedPrepaidDaysPaidByOtherTypeDescription);
            WriteAttribute(writer, "PrepaymentFinanceChargeRefundableIndicator", m_prepaymentFinanceChargeRefundableIndicator);
            WriteAttribute(writer, "GraduatedPaymentMultiplierFactor", m_graduatedPaymentMultiplierFactor);
            WriteAttribute(writer, "LoanMaturityDate", m_loanMaturityDate);
            WriteAttribute(writer, "LoanOriginalMaturityTermMonths", m_loanOriginalMaturityTermMonths);
            WriteAttribute(writer, "PaymentFrequencyTypeOtherDescription", m_paymentFrequencyTypeOtherDescription);
            WriteAttribute(writer, "OriginalPrincipalAndInterestPaymentAmount", m_originalPrincipalAndInterestPaymentAmount);
            WriteAttribute(writer, "TimelyPaymentRateReductionIndicator", m_timelyPaymentRateReductionIndicator);
            WriteAttribute(writer, "TimelyPaymentRateReductionPercent", m_timelyPaymentRateReductionPercent);
            WriteAttribute(writer, "CounselingConfirmationType", m_counselingConfirmationType);
            WriteAttribute(writer, "InitialPaymentRatePercent", m_initialPaymentRatePercent);
            WriteAttribute(writer, "RefundableApplicationFeeIndicator", m_refundableApplicationFeeIndicator);
            WriteAttribute(writer, "OriginalBalloonTermMonths", m_originalBalloonTermMonths);
            WriteAttribute(writer, "GrowingEquityLoanPayoffYearsCount", m_growingEquityLoanPayoffYearsCount);
            WriteAttribute(writer, "ConditionsToAssumabilityIndicator", m_conditionsToAssumabilityIndicator);

            WriteElement(writer, m_lateCharge);
            WriteElement(writer, m_notePayTo);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_LoanFeaturesDownPaymentOptionType 
    {
        Undefined = 0,
        FivePercentOption,
        FNM97Option,
        ThreeTwoOption
    }
    public enum E_LoanFeaturesGseProjectClassificationType 
    {
        Undefined = 0,
        OneCooperative,
        ApprovedFHA_VACondominiumProjectOrSpotLoan,
        F_PUD,
        C_ICondominium,
        E_PUD,
        A_IIICondominium,
        TwoCooperative,
        B_IICondominium,
        III_PUD
    }
    public enum E_LoanFeaturesGsePropertyType 
    {
        Undefined = 0,
        Condominium,
        HighRiseCondominium,
        Modular,
        ManufacturedHousing,
        Detached,
        ManufacturedHousingSingleWide,
        Cooperative,
        PUD,
        DetachedCondominium,
        ManufacturedHomeCondominiumOrPUDOrCooperative,
        ManufacturedHousingDoubleWide,
        ManufacturedHousingMultiWide,
        Attached,
        ManufacturedHomeCondominium
    }
    public enum E_LoanFeaturesLienPriorityType 
    {
        Undefined = 0,
        Other,
        FirstLien,
        SecondLien
    }
    public enum E_LoanFeaturesLoanClosingStatusType 
    {
        Undefined = 0,
        TableFunded,
        Closed
    }
    public enum E_LoanFeaturesLoanDocumentationType 
    {
        Undefined = 0,
        OnePaystub,
        OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040,
        NoDepositVerification,
        VerbalVerificationOfEmployment,
        NoEmploymentVerificationOrIncomeVerification,
        NoVerificationOfStatedIncomeOrEmployment,
        NoIncomeOn1003,
        NoVerificationOfStatedIncome,
        NoDepositVerificationEmploymentVerificationOrIncomeVerification,
        NoRatio,
        NoVerificationOfStatedIncomeEmploymentOrAssets,
        NoDocumentation,
        NoVerificationOfStatedAssets,
        FullDocumentation,
        NoIncomeNoEmploymentNoAssetsOn1003,
        OnePaystubAndVerbalVerificationOfEmployment,
        StreamlineRefinance,
        Alternative,
        NoVerificationOfStatedIncomeOrAssets,
        Reduced
    }
    public enum E_LoanFeaturesLoanRepaymentType 
    {
        Undefined = 0,
        PotentialNegativeAmortization,
        ScheduledNegativeAmortization,
        ScheduledAmortization,
        NoNegativeAmortization,
        InterestOnly
    }
    public enum E_LoanFeaturesMiCertificationStatusType 
    {
        Undefined = 0,
        LenderToObtain,
        SellerOfLoanToObtain
    }
    public enum E_LoanFeaturesMiCompanyNameType 
    {
        Undefined = 0,
        GECapitalMICorporation,
        MortgageGuarantyInsuranceCorporation,
        AmerinGuaranteeCorporation,
        PMI_MICorporation,
        RepublicMICompany,
        TriadGuarantyInsuranceCorporation,
        RadianGuarantyIncorporated,
        CMG_MICompany,
        UnitedGuarantyCorporation,
        CommonwealthMortgageAssuranceCompany
    }
    public enum E_LoanFeaturesNameDocumentsDrawnInType 
    {
        Undefined = 0,
        Broker,
        Investor,
        Lender
    }
    public enum E_LoanFeaturesPaymentFrequencyType 
    {
        Undefined = 0,
        Monthly,
        AtMaturity,
        Annual,
        Biweekly,
        Weekly,
        Semiannual,
        Quarterly,
        Other,
        Semimonthly
    }
    public enum E_LoanFeaturesFullPaymentPenaltyOptionType 
    {
        Undefined = 0,
        Hard,
        Soft
    }
    public enum E_LoanFeaturesServicingTransferStatusType 
    {
        Undefined = 0,
        Released,
        Retained
    }
    public enum E_LoanFeaturesEstimatedPrepaidDaysPaidByType 
    {
        Undefined = 0,
        Other,
        Buyer,
        Seller,
        Lender
    }
    public enum E_LoanFeaturesCounselingConfirmationType 
    {
        Undefined = 0,
        LenderTrainedCounseling,
        NoBorrowerCounseling,
        ThirdPartyCounseling,
        AmericanHomeownerEducationInstituteApprovedCounseling
    }
}
