/// Author: David Dao

using System;
using System.Xml;
using System.Collections;
namespace Mismo.Closing
{
	public class XEClosingDocuments : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_CLOSING_DOCUMENTS">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="ALLONGE_TO_NOTE" minOccurs="0"/>
        //        <xs:element ref="BENEFICIARY" minOccurs="0"/>
        //        <xs:element ref="CLOSING_AGENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="CLOSING_INSTRUCTIONS" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="COMPENSATION" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="COSIGNER" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="ESCROW_ACCOUNT_DETAIL" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="EXECUTION" minOccurs="0"/>
        //        <xs:element ref="INVESTOR" minOccurs="0"/>
        //        <xs:element ref="LENDER" minOccurs="0"/>
        //        <xs:element ref="LENDER_BRANCH" minOccurs="0"/>
        //        <xs:element ref="LOAN_DETAILS" minOccurs="0"/>
        //        <xs:element ref="LOSS_PAYEE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="MORTGAGE_BROKER" minOccurs="0"/>
        //        <xs:element ref="PAYMENT_DETAILS" minOccurs="0"/>
        //        <xs:element ref="PAYOFF" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RECORDABLE_DOCUMENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RESPA_HUD_DETAIL" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RESPA_SERVICING_DATA" minOccurs="0"/>
        //        <xs:element ref="RESPA_SUMMARY" minOccurs="0"/>
        //        <xs:element ref="SELLER" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="SERVICER" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private XEAllongeToNote m_allongeToNote;
        private XEBeneficiary m_beneficiary;
        private ArrayList m_closingAgentList = new ArrayList();
        private ArrayList m_closingInstructionsList = new ArrayList();
        private ArrayList m_compensationList = new ArrayList();
        private ArrayList m_cosignerList = new ArrayList();
        private ArrayList m_escrowAccountDetailList = new ArrayList();
        private XEExecution m_execution;
        private XEInvestor m_investor;
        private XELender m_lender;
        private XELenderBranch m_lenderBranch;
        private XELoanDetails m_loanDetails;
        private ArrayList m_lossPayeeList = new ArrayList();
        private XEMortgageBroker m_mortgageBroker;
        private XEPaymentDetails m_paymentDetails;
        private ArrayList m_payoffList = new ArrayList();
        private ArrayList m_recordableDocumentList = new ArrayList();
        private ArrayList m_respaHudDetailList = new ArrayList();
        private Mismo.MismoXmlElement.XERespaServicingData m_respaServicingData;
        private Mismo.MismoXmlElement.XERespaSummary m_respaSummary;
        private ArrayList m_sellerList = new ArrayList();
        private ArrayList m_servicerList = new ArrayList();
        #endregion

        #region Public Properties
        public void SetAllongeToNote(XEAllongeToNote allongeToNote) 
        {
            m_allongeToNote = allongeToNote;
        }
        public void SetBeneficiary(XEBeneficiary beneficiary) 
        {
            m_beneficiary = beneficiary;
        }
        public void AddClosingAgent(XEClosingAgent closingAgent) 
        {
            m_closingAgentList.Add(closingAgent);
        }
        public void AddClosingInstructions(XEClosingInstructions closingInstructions) 
        {
            m_closingInstructionsList.Add(closingInstructions);
        }
        public void AddCompensation(XECompensation compensation) 
        {
            m_compensationList.Add(compensation);
        }
        public void AddCosigner(XECosigner cosigner) 
        {
            m_cosignerList.Add(cosigner);
        }
        public void AddEscrowAccountDetail(XEEscrowAccountDetail escrowAccountDetail) 
        {
            m_escrowAccountDetailList.Add(escrowAccountDetail);
        }
        public void SetExecution(XEExecution execution) 
        {
            m_execution = execution;
        }
        public void SetInvestor(XEInvestor investor) 
        {
            m_investor = investor;
        }
        public void SetLender(XELender lender) 
        {
            m_lender = lender;
        }
        public void SetLenderBranch(XELenderBranch lenderBranch) 
        {
            m_lenderBranch = lenderBranch;
        }
        public void SetLoanDetails(XELoanDetails loanDetails) 
        {
            m_loanDetails = loanDetails;
        }
        public void AddLossPayee(XELossPayee lossPayee) 
        {
            m_lossPayeeList.Add(lossPayee);
        }
        public void SetMortgageBroker(XEMortgageBroker mortgageBroker) 
        {
            m_mortgageBroker = mortgageBroker;
        }
        public void SetPaymentDetails(XEPaymentDetails paymentDetails) 
        {
            m_paymentDetails = paymentDetails;
        }
        public void AddPayoff(XEPayOff payoff) 
        {
            m_payoffList.Add(payoff);
        }
        public void AddRecordableDocument(XERecordableDocument recordableDocument) 
        {
            m_recordableDocumentList.Add(recordableDocument);
        }
        public void AddRespaHudDetail(XERespaHudDetail respaHudDetail) 
        {
            m_respaHudDetailList.Add(respaHudDetail);
        }
        public void SetRespaServicingData(Mismo.MismoXmlElement.XERespaServicingData respaServicingData) 
        {
            m_respaServicingData = respaServicingData;
        }
        public void SetRespaSummary(Mismo.MismoXmlElement.XERespaSummary respaSummary) 
        {
            m_respaSummary = respaSummary;
        }
        public void AddSeller(XESeller seller) 
        {
            m_sellerList.Add(seller);
        }
        public void AddServicer(XEServicer servicer) 
        {
            m_servicerList.Add(servicer);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_CLOSING_DOCUMENTS");
            WriteElement(writer, m_allongeToNote);
            WriteElement(writer, m_beneficiary);
            WriteElement(writer, m_closingAgentList);
            WriteElement(writer, m_closingInstructionsList);
            WriteElement(writer, m_compensationList);
            WriteElement(writer, m_cosignerList);
            WriteElement(writer, m_escrowAccountDetailList);
            WriteElement(writer, m_execution);
            WriteElement(writer, m_investor);
            WriteElement(writer, m_lender);
            WriteElement(writer, m_lenderBranch);
            WriteElement(writer, m_loanDetails);
            WriteElement(writer, m_lossPayeeList);
            WriteElement(writer, m_mortgageBroker);
            WriteElement(writer, m_paymentDetails);
            WriteElement(writer, m_payoffList);
            WriteElement(writer, m_recordableDocumentList);
            WriteElement(writer, m_respaHudDetailList);
            WriteElement(writer, m_respaServicingData);
            WriteElement(writer, m_respaSummary);
            WriteElement(writer, m_sellerList);
            WriteElement(writer, m_servicerList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
