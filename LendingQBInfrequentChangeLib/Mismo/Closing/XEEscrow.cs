/// Author: David Dao

using System;
using System.Collections;
using System.Xml;

namespace Mismo.Closing
{
    public class XEEscrow : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="ESCROW">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_ACCOUNT_SUMMARY" minOccurs="0"/>
        //        <xs:element ref="_PAYMENTS" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="_PAID_TO" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_AnnualPaymentAmount" type="xs:string"/>
        //      <xs:attribute name="_CollectedNumberOfMonthsCount" type="xs:string"/>
        //      <xs:attribute name="_DueDate" type="xs:string"/>
        //      <xs:attribute name="_ItemType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="HazardInsurance"/>
        //            <xs:enumeration value="Assessment"/>
        //            <xs:enumeration value="TownPropertyTax"/>
        //            <xs:enumeration value="EarthquakeInsurance"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="WindstormInsurance"/>
        //            <xs:enumeration value="VillagePropertyTax"/>
        //            <xs:enumeration value="CityPropertyTax"/>
        //            <xs:enumeration value="FloodInsurance"/>
        //            <xs:enumeration value="CountyPropertyTax"/>
        //            <xs:enumeration value="SchoolPropertyTax"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ItemTypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="_MonthlyPaymentAmount" type="xs:string"/>
        //      <xs:attribute name="_MonthlyPaymentRoundingType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Down"/>
        //            <xs:enumeration value="None"/>
        //            <xs:enumeration value="Up"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaidByType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="LenderPremium"/>
        //            <xs:enumeration value="Buyer"/>
        //            <xs:enumeration value="Seller"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaymentFrequencyType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Monthly"/>
        //            <xs:enumeration value="Annual"/>
        //            <xs:enumeration value="SemiAnnual"/>
        //            <xs:enumeration value="Quarterly"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Unequal"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaymentFrequencyTypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="_PremiumAmount" type="xs:string"/>
        //      <xs:attribute name="_PremiumPaidByType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Buyer"/>
        //            <xs:enumeration value="Seller"/>
        //            <xs:enumeration value="Lender"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PremiumPaymentType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="CollectAtClosing"/>
        //            <xs:enumeration value="Waived"/>
        //            <xs:enumeration value="PaidOutsideOfClosing"/>
        //            <xs:enumeration value="CollectedAtClosing"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PremiumDurationMonthsCount" type="xs:string"/>
        //      <xs:attribute name="_SpecifiedHUD1LineNumber" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private XEAccountSummary m_accountSummary;
        private ArrayList m_paymentsList = new ArrayList();
        private XEPaidTo m_paidTo;
        private string m_annualPaymentAmount;
        private string m_collectedNumberOfMonthsCount;
        private string m_dueDate;
        private E_EscrowItemType m_itemType;
        private string m_itemTypeOtherDescription;
        private string m_monthlyPaymentAmount;
        private E_EscrowMonthlyPaymentRoundingType m_monthlyPaymentRoundingType;
        private E_EscrowPaidByType m_paidByType;
        private E_EscrowPaymentFrequencyType m_paymentFrequencyType;
        private string m_paymentFrequencyTypeOtherDescription;
        private string m_premiumAmount;
        private E_EscrowPremiumPaidByType m_premiumPaidByType;
        private E_EscrowPremiumPaymentType m_premiumPaymentType;
        private string m_premiumDurationMonthsCount;
        private string m_specifiedHUD1LineNumber;

        #endregion

        #region Public Properties
        public void SetAccountSummary(XEAccountSummary accountSummary) 
        {
            m_accountSummary = accountSummary;
        }
        public void AddPayments(Mismo.MismoXmlElement.XEPayments payments) 
        {
            m_paymentsList.Add(payments);
        }
        public void SetPaidTo(XEPaidTo paidTo) 
        {
            m_paidTo = paidTo;
        }
        public string AnnualPaymentAmount 
        {
            get { return m_annualPaymentAmount; }
            set { m_annualPaymentAmount = value; }
        }
        public string CollectedNumberOfMonthsCount 
        {
            get { return m_collectedNumberOfMonthsCount; }
            set { m_collectedNumberOfMonthsCount = value; }
        }
        public string DueDate 
        {
            get { return m_dueDate; }
            set { m_dueDate = value; }
        }
        public E_EscrowItemType ItemType 
        {
            get { return m_itemType; }
            set { m_itemType = value; }
        }
        public string ItemTypeOtherDescription 
        {
            get { return m_itemTypeOtherDescription; }
            set { m_itemTypeOtherDescription = value; }
        }
        public string MonthlyPaymentAmount 
        {
            get { return m_monthlyPaymentAmount; }
            set { m_monthlyPaymentAmount = value; }
        }
        public E_EscrowMonthlyPaymentRoundingType MonthlyPaymentRoundingType 
        {
            get { return m_monthlyPaymentRoundingType; }
            set { m_monthlyPaymentRoundingType = value; }
        }
        public E_EscrowPaidByType PaidByType 
        {
            get { return m_paidByType; }
            set { m_paidByType = value; }
        }
        public E_EscrowPaymentFrequencyType PaymentFrequencyType 
        {
            get { return m_paymentFrequencyType; }
            set { m_paymentFrequencyType = value; }
        }
        public string PaymentFrequencyTypeOtherDescription 
        {
            get { return m_paymentFrequencyTypeOtherDescription; }
            set { m_paymentFrequencyTypeOtherDescription = value; }
        }
        public string PremiumAmount 
        {
            get { return m_premiumAmount; }
            set { m_premiumAmount = value; }
        }
        public E_EscrowPremiumPaidByType PremiumPaidByType 
        {
            get { return m_premiumPaidByType; }
            set { m_premiumPaidByType = value; }
        }
        public E_EscrowPremiumPaymentType PremiumPaymentType 
        {
            get { return m_premiumPaymentType; }
            set { m_premiumPaymentType = value; }
        }
        public string PremiumDurationMonthsCount 
        {
            get { return m_premiumDurationMonthsCount; }
            set { m_premiumDurationMonthsCount = value; }
        }
        public string SpecifiedHUD1LineNumber 
        {
            get { return m_specifiedHUD1LineNumber; }
            set { m_specifiedHUD1LineNumber = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ESCROW");
            WriteAttribute(writer, "_AnnualPaymentAmount", m_annualPaymentAmount);
            WriteAttribute(writer, "_CollectedNumberOfMonthsCount", m_collectedNumberOfMonthsCount);
            WriteAttribute(writer, "_DueDate", m_dueDate);
            WriteAttribute(writer, "_ItemType", m_itemType);
            WriteAttribute(writer, "_ItemTypeOtherDescription", m_itemTypeOtherDescription);
            WriteAttribute(writer, "_MonthlyPaymentAmount", m_monthlyPaymentAmount);
            WriteAttribute(writer, "_MonthlyPaymentRoundingType", m_monthlyPaymentRoundingType);
            WriteAttribute(writer, "_PaidByType", m_paidByType);
            WriteAttribute(writer, "_PaymentFrequencyType", m_paymentFrequencyType);
            WriteAttribute(writer, "_PaymentFrequencyTypeOtherDescription", m_paymentFrequencyTypeOtherDescription);
            WriteAttribute(writer, "_PremiumAmount", m_premiumAmount);
            WriteAttribute(writer, "_PremiumPaidByType", m_premiumPaidByType);
            WriteAttribute(writer, "_PremiumPaymentType", m_premiumPaymentType);
            WriteAttribute(writer, "_PremiumDurationMonthsCount", m_premiumDurationMonthsCount);
            WriteAttribute(writer, "_SpecifiedHUD1LineNumber", m_specifiedHUD1LineNumber);
            WriteElement(writer, m_accountSummary);
            WriteElement(writer, m_paymentsList);
            WriteElement(writer, m_paidTo);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_EscrowItemType 
    {
        Undefined = 0,
        HazardInsurance,
        Assessment,
        TownPropertyTax,
        EarthquakeInsurance,
        Other,
        WindstormInsurance,
        VillagePropertyTax,
        CityPropertyTax,
        FloodInsurance,
        CountyPropertyTax,
        SchoolPropertyTax
    }
    public enum E_EscrowMonthlyPaymentRoundingType 
    {
        Undefined = 0,
        Down,
        None,
        Up
    }
    public enum E_EscrowPaidByType 
    {
        Undefined = 0,
        LenderPremium,
        Buyer,
        Seller
    }
    public enum E_EscrowPaymentFrequencyType 
    {
        Undefined = 0,
        Monthly,
        Annual,
        SemiAnnual,
        Quarterly,
        Other,
        Unequal
    }
    public enum E_EscrowPremiumPaidByType 
    {
        Undefined = 0,
        Buyer,
        Seller,
        Lender
    }
    public enum E_EscrowPremiumPaymentType 
    {
        Undefined = 0,
        CollectAtClosing,
        Waived,
        PaidOutsideOfClosing,
        CollectedAtClosing
    }
}
