/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
    public class XESecurityInstrument : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="SECURITY_INSTRUMENT">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_AssumptionFeeAmount" type="xs:string"/>
        //          <xs:attribute name="_AttorneyFeeMinimumAmount" type="xs:string"/>
        //          <xs:attribute name="_AttorneyFeePercent" type="xs:string"/>
        //          <xs:attribute name="_MaximumPrincipalIndebtednessAmount" type="xs:string"/>
        //          <xs:attribute name="_MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_NoteHolderName" type="xs:string"/>
        //          <xs:attribute name="_OtherFeesAmount" type="xs:string"/>
        //          <xs:attribute name="_OtherFeesDescription" type="xs:string"/>
        //          <xs:attribute name="_OweltyOfPartitionIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="PropertyLongLegalPageNumberDescription" type="xs:string"/>
        //          <xs:attribute name="_PurchaseMoneyIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_RealPropertyImprovedOrToBeImprovedIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_RealPropertyImprovementsNotCoveredIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_RecordingRequestedByName" type="xs:string"/>
        //          <xs:attribute name="_RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_TaxSerialNumberIdentifier" type="xs:string"/>
        //          <xs:attribute name="_TrusteeFeePercent" type="xs:string"/>
        //          <xs:attribute name="_VendorsLienIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_VendorsLienDescription" type="xs:string"/>
        //          <xs:attribute name="_VestingDescription" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_assumptionFeeAmount;
        private string m_attorneyFeeMinimumAmount;
        private string m_attorneyFeePercent;
        private string m_maximumPrincipalIndebtednessAmount;
        private E_YesNoIndicator m_multipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator;
        private string m_noteHolderName;
        private string m_otherFeesAmount;
        private string m_otherFeesDescription;
        private E_YesNoIndicator m_oweltyOfPartitionIndicator;
        private string m_propertyLongLegalPageNumberDescription;
        private E_YesNoIndicator m_purchaseMoneyIndicator;
        private E_YesNoIndicator m_realPropertyImprovedOrToBeImprovedIndicator;
        private E_YesNoIndicator m_realPropertyImprovementsNotCoveredIndicator;
        private string m_recordingRequestedByName;
        private E_YesNoIndicator m_renewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator;
        private string m_taxSerialNumberIdentifier;
        private string m_trusteeFeePercent;
        private E_YesNoIndicator m_vendorsLienIndicator;
        private string m_vendorsLienDescription;
        private string m_vestingDescription;
        #endregion

        #region Public Properties
        public string AssumptionFeeAmount 
        {
            get { return m_assumptionFeeAmount; }
            set { m_assumptionFeeAmount = value; }
        }
        public string AttorneyFeeMinimumAmount 
        {
            get { return m_attorneyFeeMinimumAmount; }
            set { m_attorneyFeeMinimumAmount = value; }
        }
        public string AttorneyFeePercent 
        {
            get { return m_attorneyFeePercent; }
            set { m_attorneyFeePercent = value; }
        }
        public string MaximumPrincipalIndebtednessAmount 
        {
            get { return m_maximumPrincipalIndebtednessAmount; }
            set { m_maximumPrincipalIndebtednessAmount = value; }
        }
        public E_YesNoIndicator MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator 
        {
            get { return m_multipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator; }
            set { m_multipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = value; }
        }
        public string NoteHolderName 
        {
            get { return m_noteHolderName; }
            set { m_noteHolderName = value; }
        }
        public string OtherFeesAmount 
        {
            get { return m_otherFeesAmount; }
            set { m_otherFeesAmount = value; }
        }
        public string OtherFeesDescription 
        {
            get { return m_otherFeesDescription; }
            set { m_otherFeesDescription = value; }
        }
        public E_YesNoIndicator OweltyOfPartitionIndicator 
        {
            get { return m_oweltyOfPartitionIndicator; }
            set { m_oweltyOfPartitionIndicator = value; }
        }
        public string PropertyLongLegalPageNumberDescription 
        {
            get { return m_propertyLongLegalPageNumberDescription; }
            set { m_propertyLongLegalPageNumberDescription = value; }
        }
        public E_YesNoIndicator PurchaseMoneyIndicator 
        {
            get { return m_purchaseMoneyIndicator; }
            set { m_purchaseMoneyIndicator = value; }
        }
        public E_YesNoIndicator RealPropertyImprovedOrToBeImprovedIndicator 
        {
            get { return m_realPropertyImprovedOrToBeImprovedIndicator; }
            set { m_realPropertyImprovedOrToBeImprovedIndicator = value; }
        }
        public E_YesNoIndicator RealPropertyImprovementsNotCoveredIndicator 
        {
            get { return m_realPropertyImprovementsNotCoveredIndicator; }
            set { m_realPropertyImprovementsNotCoveredIndicator = value; }
        }
        public string RecordingRequestedByName 
        {
            get { return m_recordingRequestedByName; }
            set { m_recordingRequestedByName = value; }
        }
        public E_YesNoIndicator RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator 
        {
            get { return m_renewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator; }
            set { m_renewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator = value; }
        }
        public string TaxSerialNumberIdentifier 
        {
            get { return m_taxSerialNumberIdentifier; }
            set { m_taxSerialNumberIdentifier = value; }
        }
        public string TrusteeFeePercent 
        {
            get { return m_trusteeFeePercent; }
            set { m_trusteeFeePercent = value; }
        }
        public E_YesNoIndicator VendorsLienIndicator 
        {
            get { return m_vendorsLienIndicator; }
            set { m_vendorsLienIndicator = value; }
        }
        public string VendorsLienDescription 
        {
            get { return m_vendorsLienDescription; }
            set { m_vendorsLienDescription = value; }
        }
        public string VestingDescription 
        {
            get { return m_vestingDescription; }
            set { m_vestingDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("SECURITY_INSTRUMENT");
            WriteAttribute(writer, "_AssumptionFeeAmount", m_assumptionFeeAmount);
            WriteAttribute(writer, "_AttorneyFeeMinimumAmount", m_attorneyFeeMinimumAmount);
            WriteAttribute(writer, "_AttorneyFeePercent", m_attorneyFeePercent);
            WriteAttribute(writer, "_MaximumPrincipalIndebtednessAmount", m_maximumPrincipalIndebtednessAmount);
            WriteAttribute(writer, "_MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator", m_multipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator);
            WriteAttribute(writer, "_NoteHolderName", m_noteHolderName);
            WriteAttribute(writer, "_OtherFeesAmount", m_otherFeesAmount);
            WriteAttribute(writer, "_OtherFeesDescription", m_otherFeesDescription);
            WriteAttribute(writer, "_OweltyOfPartitionIndicator", m_oweltyOfPartitionIndicator);
            WriteAttribute(writer, "PropertyLongLegalPageNumberDescription", m_propertyLongLegalPageNumberDescription);
            WriteAttribute(writer, "_PurchaseMoneyIndicator", m_purchaseMoneyIndicator);
            WriteAttribute(writer, "_RealPropertyImprovedOrToBeImprovedIndicator", m_realPropertyImprovedOrToBeImprovedIndicator);
            WriteAttribute(writer, "_RealPropertyImprovementsNotCoveredIndicator", m_realPropertyImprovementsNotCoveredIndicator);
            WriteAttribute(writer, "_RecordingRequestedByName", m_recordingRequestedByName);
            WriteAttribute(writer, "_RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator", m_renewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator);
            WriteAttribute(writer, "_TaxSerialNumberIdentifier", m_taxSerialNumberIdentifier);
            WriteAttribute(writer, "_TrusteeFeePercent", m_trusteeFeePercent);
            WriteAttribute(writer, "_VendorsLienIndicator", m_vendorsLienIndicator);
            WriteAttribute(writer, "_VendorsLienDescription", m_vendorsLienDescription);
            WriteAttribute(writer, "_VestingDescription", m_vestingDescription);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
