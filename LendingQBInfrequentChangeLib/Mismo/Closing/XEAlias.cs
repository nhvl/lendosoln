/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XEAlias : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_ALIAS">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_FirstName" type="xs:string"/>
        //          <xs:attribute name="_LastName" type="xs:string"/>
        //          <xs:attribute name="_MiddleName" type="xs:string"/>
        //          <xs:attribute name="_NameSuffix" type="xs:string"/>
        //          <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //          <xs:attribute name="_Type">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="AlsoKnownAs"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="FormerlyKnownAs"/>
        //                <xs:enumeration value="NowKnownAs"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //          <xs:attribute name="_UnparsedName" type="xs:string"/>
        //          <xs:attribute name="_CreditorName" type="xs:string"/>
        //          <xs:attribute name="_AccountIdentifier" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_firstName;
        private string m_lastName;
        private string m_middleName;
        private string m_nameSuffix;
        private string m_sequenceIdentifier;
        private E_AliasType m_type;
        private string m_typeOtherDescription;
        private string m_unparsedName;
        private string m_creditorName;
        private string m_accountIdentifier;

        #endregion

        #region Public Properties
        public string FirstName 
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }
        public string LastName 
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }
        public string MiddleName 
        {
            get { return m_middleName; }
            set { m_middleName = value; }
        }
        public string NameSuffix 
        {
            get { return m_nameSuffix; }
            set { m_nameSuffix = value; }
        }
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public E_AliasType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }
        public string CreditorName 
        {
            get { return m_creditorName; }
            set { m_creditorName = value; }
        }
        public string AccountIdentifier 
        {
            get { return m_accountIdentifier; }
            set { m_accountIdentifier = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_ALIAS");
            WriteAttribute(writer, "_FirstName", m_firstName);
            WriteAttribute(writer, "_LastName", m_lastName);
            WriteAttribute(writer, "_MiddleName", m_middleName);
            WriteAttribute(writer, "_NameSuffix", m_nameSuffix);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);
            WriteAttribute(writer, "_CreditorName", m_creditorName);
            WriteAttribute(writer, "_AccountIdentifier", m_accountIdentifier);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_AliasType 
    {
        Undefined = 0,
        AlsoKnownAs,
        Other,
        FormerlyKnownAs,
        NowKnownAs
    }
}
