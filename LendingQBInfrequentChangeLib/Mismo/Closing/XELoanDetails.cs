/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XELoanDetails : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="LOAN_DETAILS">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="INTERIM_INTEREST" minOccurs="0"/>
        //        <xs:element ref="REQUEST_TO_RESCIND" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="ClosingDate" type="xs:string"/>
        //      <xs:attribute name="DisbursementDate" type="xs:string"/>
        //      <xs:attribute name="DocumentOrderClassificationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Final"/>
        //            <xs:enumeration value="Preliminary"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="DocumentPreparationDate" type="xs:string"/>
        //      <xs:attribute name="FundByDate" type="xs:string"/>
        //      <xs:attribute name="OriginalLTVRatioPercent" type="xs:string"/>
        //      <xs:attribute name="LockExpirationDate" type="xs:string"/>
        //      <xs:attribute name="RescissionDate" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private XEInterimInterest m_interimInterest;
        private XERequestToRescind m_requestToRescind;
        private string m_closingDate;
        private string m_disbursementDate;
        private E_LoanDetailsDocumentOrderClassificationType m_documentOrderClassificationType;
        private string m_documentPreparationDate;
        private string m_fundByDate;
        private string m_originalLTVRatioPercent;
        private string m_lockExpirationDate;
        private string m_rescissionDate;

        #endregion

        #region Public Properties
        public void SetInterimInterest(XEInterimInterest interimInterest) 
        {
            m_interimInterest = interimInterest;
        }
        public void SetRequestToRescind(XERequestToRescind requestToRescind)
        {
            m_requestToRescind = requestToRescind;
        }
        public string ClosingDate 
        {
            get { return m_closingDate; }
            set { m_closingDate = value; }
        }
        public string DisbursementDate 
        {
            get { return m_disbursementDate; }
            set { m_disbursementDate = value; }
        }
        public E_LoanDetailsDocumentOrderClassificationType DocumentOrderClassificationType 
        {
            get { return m_documentOrderClassificationType; }
            set { m_documentOrderClassificationType = value; }
        }
        public string DocumentPreparationDate 
        {
            get { return m_documentPreparationDate; }
            set { m_documentPreparationDate = value; }
        }
        public string FundByDate 
        {
            get { return m_fundByDate; }
            set { m_fundByDate = value; }
        }
        public string OriginalLTVRatioPercent 
        {
            get { return m_originalLTVRatioPercent; }
            set { m_originalLTVRatioPercent = value; }
        }
        public string LockExpirationDate 
        {
            get { return m_lockExpirationDate; }
            set { m_lockExpirationDate = value; }
        }
        public string RescissionDate 
        {
            get { return m_rescissionDate; }
            set { m_rescissionDate = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_DETAILS");
            WriteAttribute(writer, "ClosingDate", m_closingDate);
            WriteAttribute(writer, "DisbursementDate", m_disbursementDate);
            WriteAttribute(writer, "DocumentOrderClassificationType", m_documentOrderClassificationType);
            WriteAttribute(writer, "DocumentPreparationDate", m_documentPreparationDate);
            WriteAttribute(writer, "FundByDate", m_fundByDate);
            WriteAttribute(writer, "OriginalLTVRatioPercent", m_originalLTVRatioPercent);
            WriteAttribute(writer, "LockExpirationDate", m_lockExpirationDate);
            WriteAttribute(writer, "RescissionDate", m_rescissionDate);

            WriteElement(writer, m_interimInterest);
            WriteElement(writer, m_requestToRescind);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_LoanDetailsDocumentOrderClassificationType 
    {
        Undefined = 0,
        Final,
        Preliminary
    }
}
