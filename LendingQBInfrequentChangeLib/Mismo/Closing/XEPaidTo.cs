/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
	public class XEPaidTo : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_PAID_TO">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="CONTACT_DETAIL" minOccurs="0"/>
        //        <xs:element ref="NON_PERSON_ENTITY_DETAIL" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_Name" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="_Country" type="xs:string"/>
        //      <xs:attribute name="_County" type="xs:string"/>
        //      <xs:attribute name="NonPersonEntityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private Mismo.MismoXmlElement.XEContactDetail m_contactDetail;
        private XENonPersonEntityDetail m_nonPersonEntityDetail;
        private string m_name;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_country;
        private string m_county;
        private E_YesNoIndicator m_nonPersonEntityIndicator;
        #endregion

        #region Public Properties
        public void SetContactDetail(Mismo.MismoXmlElement.XEContactDetail contactDetail) 
        {
            m_contactDetail = contactDetail;
        }
        public void SetNonPersonEntityDetail(XENonPersonEntityDetail nonPersonEntityDetail) 
        {
            m_nonPersonEntityDetail = nonPersonEntityDetail;
        }
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string Country 
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string County 
        {
            get { return m_county; }
            set { m_county = value; }
        }
        public E_YesNoIndicator NonPersonEntityIndicator 
        {
            get { return m_nonPersonEntityIndicator; }
            set { m_nonPersonEntityIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_PAID_TO");
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "_County", m_county);
            WriteAttribute(writer, "NonPersonEntityIndicator", m_nonPersonEntityIndicator);
            WriteElement(writer, m_contactDetail);
            WriteElement(writer, m_nonPersonEntityDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
