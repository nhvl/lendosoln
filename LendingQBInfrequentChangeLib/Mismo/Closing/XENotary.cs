/// Author: David Dao

using System;
using System.Xml;
using System.Collections;

namespace Mismo.Closing
{
    public class XENotary : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="NOTARY">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_CERTIFICATE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="DATA_VERSION" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_AppearanceDate" type="xs:string"/>
        //      <xs:attribute name="_AppearedBeforeNamesDescription" type="xs:string"/>
        //      <xs:attribute name="_AppearedBeforeTitlesDescription" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_CommissionBondNumberIdentifier" type="xs:string"/>
        //      <xs:attribute name="_CommissionCity" type="xs:string"/>
        //      <xs:attribute name="_CommissionCounty" type="xs:string"/>
        //      <xs:attribute name="_CommissionExpirationDate" type="xs:string"/>
        //      <xs:attribute name="_CommissionNumberIdentifier" type="xs:string"/>
        //      <xs:attribute name="_CommissionState" type="xs:string"/>
        //      <xs:attribute name="_County" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_TitleDescription" type="xs:string"/>
        //      <xs:attribute name="_UnparsedName" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_certificateList = new ArrayList();
        private Mismo.MismoXmlElement.XEDataVersion m_dataVersion;
        private string m_appearanceDate;
        private string m_appearedBeforeNamesDescription;
        private string m_appearedBeforeTitlesDescription;
        private string m_city;
        private string m_commissionBondNumberIdentifier;
        private string m_commissionCity;
        private string m_commissionCounty;
        private string m_commissionExpirationDate;
        private string m_commissionNumberIdentifier; 
        private string m_commissionState;
        private string m_county;
        private string m_postalCode;
        private string m_state;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_titleDescription;
        private string m_unparsedName;

        #endregion

        #region Public Properties
        public void AddCertificate(XECertificate certificate) 
        {
            m_certificateList.Add(certificate);
        }
        public void SetDataVersion(Mismo.MismoXmlElement.XEDataVersion dataVersion) 
        {
            m_dataVersion = dataVersion;
        }
        public string AppearanceDate 
        {
            get { return m_appearanceDate; }
            set { m_appearanceDate = value; }
        }
        public string AppearedBeforeNamesDescription 
        {
            get { return m_appearedBeforeNamesDescription; }
            set { m_appearedBeforeNamesDescription = value; }
        }
        public string AppearedBeforeTitlesDescription 
        {
            get { return m_appearedBeforeTitlesDescription; }
            set { m_appearedBeforeTitlesDescription = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string CommissionBondNumberIdentifier 
        {
            get { return m_commissionBondNumberIdentifier; }
            set { m_commissionBondNumberIdentifier = value; }
        }
        public string CommissionCity 
        {
            get { return m_commissionCity; }
            set { m_commissionCity = value; }
        }
        public string CommissionCounty 
        {
            get { return m_commissionCounty; }
            set { m_commissionCounty = value; }
        }
        public string CommissionExpirationDate 
        {
            get { return m_commissionExpirationDate; }
            set { m_commissionExpirationDate = value; }
        }
        public string CommissionNumberIdentifier
        {
            get { return m_commissionNumberIdentifier; }
            set { m_commissionNumberIdentifier = value; }
        }
        public string CommissionState 
        {
            get { return m_commissionState; }
            set { m_commissionState = value; }
        }
        public string County 
        {
            get { return m_county; }
            set { m_county = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string TitleDescription 
        {
            get { return m_titleDescription; }
            set { m_titleDescription = value; }
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("NOTARY");
            WriteAttribute(writer, "_AppearanceDate", m_appearanceDate);
            WriteAttribute(writer, "_AppearedBeforeNamesDescription", m_appearedBeforeNamesDescription);
            WriteAttribute(writer, "_AppearedBeforeTitlesDescription", m_appearedBeforeTitlesDescription);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_CommissionBondNumberIdentifier", m_commissionBondNumberIdentifier);
            WriteAttribute(writer, "_CommissionCity", m_commissionCity);
            WriteAttribute(writer, "_CommissionCounty", m_commissionCounty);
            WriteAttribute(writer, "_CommissionExpirationDate", m_commissionExpirationDate);
            WriteAttribute(writer, "_CommissionNumberIdentifier", m_commissionNumberIdentifier);
            WriteAttribute(writer, "_CommissionState", m_commissionState);
            WriteAttribute(writer, "_County", m_county);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_TitleDescription", m_titleDescription);
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);

            WriteElement(writer, m_certificateList);
            WriteElement(writer, m_dataVersion);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
