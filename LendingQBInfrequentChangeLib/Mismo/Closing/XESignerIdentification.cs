/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XESignerIdentification : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_SIGNER_IDENTIFICATION">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_Type">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="ProvidedIdentification"/>
        //                <xs:enumeration value="PersonallyKnown"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_Description" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_SignerIdentificationType m_type;
        private string m_description;
        #endregion

        #region Public Properties
        public E_SignerIdentificationType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string Description 
        {
            get { return m_description; }
            set { m_description = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_SIGNER_IDENTIFICATION");
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_Description", m_description);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_SignerIdentificationType 
    {
        Undefined = 0,
        ProvidedIdentification,
        PersonallyKnown
    }
}
