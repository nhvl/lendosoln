/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XEExecution : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="EXECUTION">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_City" type="xs:string"/>
        //          <xs:attribute name="_State" type="xs:string"/>
        //          <xs:attribute name="_County" type="xs:string"/>
        //          <xs:attribute name="_Date" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_city;
        private string m_state;
        private string m_county;
        private string m_date;

        #endregion

        #region Public Properties
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string County 
        {
            get { return m_county; }
            set { m_county = value; }
        }
        public string Date 
        {
            get { return m_date; }
            set { m_date = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("EXECUTION");
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_County", m_county);
            WriteAttribute(writer, "_Date", m_date);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
