/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEAssociatedDocument : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="_ASSOCIATED_DOCUMENT">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_Type">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="SignatureAffidavit"/>
        //                <xs:enumeration value="WarrantyDeed"/>
        //                <xs:enumeration value="QuitClaimDeed"/>
        //                <xs:enumeration value="All"/>
        //                <xs:enumeration value="DeedOfTrust"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="Mortgage"/>
        //                <xs:enumeration value="SecurityInstrument"/>
        //                <xs:enumeration value="ReleaseOfLien"/>
        //                <xs:enumeration value="AssignmentOfMortgage"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //          <xs:attribute name="_BookNumber" type="xs:string"/>
        //          <xs:attribute name="_BookType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Plat"/>
        //                <xs:enumeration value="Maps"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="Mortgage"/>
        //                <xs:enumeration value="Deed"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_BookTypeOtherDescription" type="xs:string"/>
        //          <xs:attribute name="_Number" type="xs:string"/>
        //          <xs:attribute name="_TitleDescription" type="xs:string"/>
        //          <xs:attribute name="_InstrumentNumber" type="xs:string"/>
        //          <xs:attribute name="_PageNumber" type="xs:string"/>
        //          <xs:attribute name="_VolumeNumber" type="xs:string"/>
        //          <xs:attribute name="_RecordingDate" type="xs:string"/>
        //          <xs:attribute name="_RecordingJurisdictionName" type="xs:string"/>
        //          <xs:attribute name="_CountyOfRecordationName" type="xs:string"/>
        //          <xs:attribute name="_OfficeOfRecordationName" type="xs:string"/>
        //          <xs:attribute name="_StateOfRecordationName" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private E_AssociatedDocumentType m_type;
        private string m_typeOtherDescription;
        private string m_bookNumber;
        private E_AssociatedDocumentBookType m_bookType;
        private string m_bookTypeOtherDescription;
        private string m_number;
        private string m_titleDescription;
        private string m_instrumentNumber;
        private string m_pageNumber;
        private string m_volumeNumber;
        private string m_recordingDate;
        private string m_recordingJurisdictionName;
        private string m_countyOfRecordationName;
        private string m_officeOfRecordationName;
        private string m_stateOfRecordationName;

        #endregion

        #region Public Properties
        public E_AssociatedDocumentType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        public string BookNumber 
        {
            get { return m_bookNumber; }
            set { m_bookNumber = value; }
        }
        public E_AssociatedDocumentBookType BookType 
        {
            get { return m_bookType; }
            set { m_bookType = value; }
        }
        public string BookTypeOtherDescription 
        {
            get { return m_bookTypeOtherDescription; }
            set { m_bookTypeOtherDescription = value; }
        }
        public string Number 
        {
            get { return m_number; }
            set { m_number = value; }
        }
        public string TitleDescription 
        {
            get { return m_titleDescription; }
            set { m_titleDescription = value; }
        }
        public string InstrumentNumber 
        {
            get { return m_instrumentNumber; }
            set { m_instrumentNumber = value; }
        }
        public string PageNumber 
        {
            get { return m_pageNumber; }
            set { m_pageNumber = value; }
        }
        public string VolumeNumber 
        {
            get { return m_volumeNumber; }
            set { m_volumeNumber = value; }
        }
        public string RecordingDate 
        {
            get { return m_recordingDate; }
            set { m_recordingDate = value; }
        }
        public string RecordingJurisdictionName 
        {
            get { return m_recordingJurisdictionName; }
            set { m_recordingJurisdictionName = value; }
        }
        public string CountyOfRecordationName 
        {
            get { return m_countyOfRecordationName; }
            set { m_countyOfRecordationName = value; }
        }
        public string OfficeOfRecordationName 
        {
            get { return m_officeOfRecordationName; }
            set { m_officeOfRecordationName = value; }
        }
        public string StateOfRecordationName 
        {
            get { return m_stateOfRecordationName; }
            set { m_stateOfRecordationName = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_ASSOCIATED_DOCUMENT");
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "_BookNumber", m_bookNumber);
            WriteAttribute(writer, "_BookType", m_bookType);
            WriteAttribute(writer, "_BookTypeOtherDescription", m_bookTypeOtherDescription);
            WriteAttribute(writer, "_Number", m_number);
            WriteAttribute(writer, "_TitleDescription", m_titleDescription);
            WriteAttribute(writer, "_InstrumentNumber", m_instrumentNumber);
            WriteAttribute(writer, "_PageNumber", m_pageNumber);
            WriteAttribute(writer, "_VolumeNumber", m_volumeNumber);
            WriteAttribute(writer, "_RecordingDate", m_recordingDate);
            WriteAttribute(writer, "_RecordingJurisdictionName", m_recordingJurisdictionName);
            WriteAttribute(writer, "_CountyOfRecordationName", m_countyOfRecordationName);
            WriteAttribute(writer, "_OfficeOfRecordationName", m_officeOfRecordationName);
            WriteAttribute(writer, "_StateOfRecordationName", m_stateOfRecordationName);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_AssociatedDocumentType 
    {
        Undefined = 0,
        SignatureAffidavit,
        WarrantyDeed,
        QuitClaimDeed,
        All,
        DeedOfTrust,
        Other,
        Mortgage,
        SecurityInstrument,
        ReleaseOfLien,
        AssignmentOfMortgage
    }
    public enum E_AssociatedDocumentBookType 
    {
        Undefined = 0,
        Plat,
        Maps,
        Other,
        Mortgage,
        Deed
    }
}
