/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XEMortgageTerms : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="MORTGAGE_TERMS">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="AgencyCaseIdentifier" type="xs:string"/>
        //          <xs:attribute name="ARMTypeDescription" type="xs:string"/>
        //          <xs:attribute name="BaseLoanAmount" type="xs:string"/>
        //          <xs:attribute name="BorrowerRequestedLoanAmount" type="xs:string"/>
        //          <xs:attribute name="LenderCaseIdentifier" type="xs:string"/>
        //          <xs:attribute name="LoanAmortizationTermMonths" type="xs:string"/>
        //          <xs:attribute name="LoanAmortizationType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="OtherAmortizationType"/>
        //                <xs:enumeration value="Fixed"/>
        //                <xs:enumeration value="AdjustableRate"/>
        //                <xs:enumeration value="GraduatedPaymentMortgage"/>
        //                <xs:enumeration value="GrowingEquityMortgage"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="MortgageType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Conventional"/>
        //                <xs:enumeration value="VA"/>
        //                <xs:enumeration value="FarmersHomeAdministration"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="HELOC"/>
        //                <xs:enumeration value="FHA"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="OtherAmortizationTypeDescription" type="xs:string"/>
        //          <xs:attribute name="OtherMortgageTypeDescription" type="xs:string"/>
        //          <xs:attribute name="PaymentRemittanceDay" type="xs:string"/>
        //          <xs:attribute name="RequestedInterestRatePercent" type="xs:string"/>
        //          <xs:attribute name="NoteRatePercent" type="xs:string"/>
        //          <xs:attribute name="LenderLoanIdentifier" type="xs:string"/>
        //          <xs:attribute name="OriginalLoanAmount" type="xs:string"/>
        //          <xs:attribute name="LoanEstimatedClosingDate" type="xs:string"/>
        //          <xs:attribute name="LendersContactPrimaryTelephoneNumber" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_agencyCaseIdentifier;
        private string m_armTypeDescription;
        private string m_baseLoanAmount;
        private string m_borrowerRequestedLoanAmount;
        private string m_lenderCaseIdentifier;
        private string m_loanAmortizationTermMonths;
        private E_MortgageTermsLoanAmortizationType m_loanAmortizationType;
        private E_MortgageTermsMortgageType m_mortgageType;
        private string m_otherAmortizationTypeDescription;
        private string m_otherMortgageTypeDescription;
        private string m_paymentRemittanceDay;
        private string m_requestedInterestRatePercent;
        private string m_noteRatePercent;
        private string m_lenderLoanIdentifier;
        private string m_originalLoanAmount;
        private string m_loanEstimatedClosingDate;
        private string m_lendersContactPrimaryTelephoneNumber;
        #endregion

        #region Public Properties
        public string AgencyCaseIdentifier 
        {
            get { return m_agencyCaseIdentifier; }
            set { m_agencyCaseIdentifier = value; }
        }
        public string ArmTypeDescription 
        {
            get { return m_armTypeDescription; }
            set { m_armTypeDescription = value; }
        }
        public string BaseLoanAmount 
        {
            get { return m_baseLoanAmount; }
            set { m_baseLoanAmount = value; }
        }
        public string BorrowerRequestedLoanAmount 
        {
            get { return m_borrowerRequestedLoanAmount; }
            set { m_borrowerRequestedLoanAmount = value; }
        }
        public string LenderCaseIdentifier 
        {
            get { return m_lenderCaseIdentifier; }
            set { m_lenderCaseIdentifier = value; }
        }
        public string LoanAmortizationTermMonths 
        {
            get { return m_loanAmortizationTermMonths; }
            set { m_loanAmortizationTermMonths = value; }
        }
        public E_MortgageTermsLoanAmortizationType LoanAmortizationType 
        {
            get { return m_loanAmortizationType; }
            set { m_loanAmortizationType = value; }
        }
        public E_MortgageTermsMortgageType MortgageType 
        {
            get { return m_mortgageType; }
            set { m_mortgageType = value; }
        }
        public string OtherAmortizationTypeDescription 
        {
            get { return m_otherAmortizationTypeDescription; }
            set { m_otherAmortizationTypeDescription = value; }
        }
        public string OtherMortgageTypeDescription 
        {
            get { return m_otherMortgageTypeDescription; }
            set { m_otherMortgageTypeDescription = value; }
        }
        public string PaymentRemittanceDay 
        {
            get { return m_paymentRemittanceDay; }
            set { m_paymentRemittanceDay = value; }
        }
        public string RequestedInterestRatePercent 
        {
            get { return m_requestedInterestRatePercent; }
            set { m_requestedInterestRatePercent = value; }
        }
        public string NoteRatePercent 
        {
            get { return m_noteRatePercent; }
            set { m_noteRatePercent = value; }
        }
        public string LenderLoanIdentifier 
        {
            get { return m_lenderLoanIdentifier; }
            set { m_lenderLoanIdentifier = value; }
        }
        public string OriginalLoanAmount 
        {
            get { return m_originalLoanAmount; }
            set { m_originalLoanAmount = value; }
        }
        public string LoanEstimatedClosingDate 
        {
            get { return m_loanEstimatedClosingDate; }
            set { m_loanEstimatedClosingDate = value; }
        }
        public string LendersContactPrimaryTelephoneNumber 
        {
            get { return m_lendersContactPrimaryTelephoneNumber; }
            set { m_lendersContactPrimaryTelephoneNumber = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MORTGAGE_TERMS");
            WriteAttribute(writer, "AgencyCaseIdentifier", m_agencyCaseIdentifier);
            WriteAttribute(writer, "ARMTypeDescription", m_armTypeDescription);
            WriteAttribute(writer, "BaseLoanAmount", m_baseLoanAmount);
            WriteAttribute(writer, "BorrowerRequestedLoanAmount", m_borrowerRequestedLoanAmount);
            WriteAttribute(writer, "LenderCaseIdentifier", m_lenderCaseIdentifier);
            WriteAttribute(writer, "LoanAmortizationTermMonths", m_loanAmortizationTermMonths);
            WriteAttribute(writer, "LoanAmortizationType", m_loanAmortizationType);
            WriteAttribute(writer, "MortgageType", m_mortgageType);
            WriteAttribute(writer, "OtherAmortizationTypeDescription", m_otherAmortizationTypeDescription);
            WriteAttribute(writer, "OtherMortgageTypeDescription", m_otherMortgageTypeDescription);
            WriteAttribute(writer, "PaymentRemittanceDay", m_paymentRemittanceDay);
            WriteAttribute(writer, "RequestedInterestRatePercent", m_requestedInterestRatePercent);
            WriteAttribute(writer, "NoteRatePercent", m_noteRatePercent);
            WriteAttribute(writer, "LenderLoanIdentifier", m_lenderLoanIdentifier);
            WriteAttribute(writer, "OriginalLoanAmount", m_originalLoanAmount);
            WriteAttribute(writer, "LoanEstimatedClosingDate", m_loanEstimatedClosingDate);
            WriteAttribute(writer, "LendersContactPrimaryTelephoneNumber", m_lendersContactPrimaryTelephoneNumber);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_MortgageTermsLoanAmortizationType 
    {
        Undefined = 0,
        OtherAmortizationType,
        Fixed,
        AdjustableRate,
        GraduatedPaymentMortgage,
        GrowingEquityMortgage
    }
    public enum E_MortgageTermsMortgageType 
    {
        Undefined = 0,
        Conventional,
        VA,
        FarmersHomeAdministration,
        Other,
        HELOC,
        FHA
    }
}
