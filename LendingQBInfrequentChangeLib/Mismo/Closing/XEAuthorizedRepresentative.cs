/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
	public class XEAuthorizedRepresentative : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="AUTHORIZED_REPRESENTATIVE">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="CONTACT_DETAIL" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_UnparsedName" type="xs:string"/>
        //      <xs:attribute name="_TitleDescription" type="xs:string"/>
        //      <xs:attribute name="AuthorizedToSignIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private Mismo.MismoXmlElement.XEContactDetail m_contactDetail;
        private string m_unparsedName;
        private string m_titleDescription;
        private E_YesNoIndicator m_authorizedToSignIndicator;
        
        #endregion

        #region Public Properties
        public void SetContactDetail(Mismo.MismoXmlElement.XEContactDetail contactDetail) 
        {
            m_contactDetail = contactDetail;
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }
        public string TitleDescription 
        {
            get { return m_titleDescription; }
            set { m_titleDescription = value; }
        }
        public E_YesNoIndicator AuthorizedToSignIndicator 
        {
            get { return m_authorizedToSignIndicator; }
            set { m_authorizedToSignIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("AUTHORIZED_REPRESENTATIVE");
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);
            WriteAttribute(writer, "_TitleDescription", m_titleDescription);
            WriteAttribute(writer, "AuthorizedToSignIndicator", m_authorizedToSignIndicator);
            WriteElement(writer, m_contactDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
