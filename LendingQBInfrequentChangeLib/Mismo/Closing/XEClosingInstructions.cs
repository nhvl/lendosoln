/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
    public class XEClosingInstructions : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="CLOSING_INSTRUCTIONS">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_CONDITION" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_ConsolidatedClosingConditionsDescription" type="xs:string"/>
        //      <xs:attribute name="SpecialFloodHazardAreaIndictor">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="LeadBasedPaintCertificationRequiredIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PropertyTaxMessageDescription" type="xs:string"/>
        //      <xs:attribute name="PreliminaryTitleReportDate" type="xs:string"/>
        //      <xs:attribute name="TitleReportItemsDescription" type="xs:string"/>
        //      <xs:attribute name="TitleReportRequiredEndorsementsDescription" type="xs:string"/>
        //      <xs:attribute name="_TermiteReportRequiredIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="HoursDocumentsNeededPriorToDisbursementCount" type="xs:string"/>
        //      <xs:attribute name="FundingCutoffTime" type="xs:string"/>
        //      <xs:attribute name="SpecialFloodHazardAreaIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private ArrayList m_conditionList = new ArrayList();
        private string m_consolidatedClosingConditionsDescription;
        private E_YesNoIndicator m_specialFloodHazardAreaIndictor;
        private E_YesNoIndicator m_leadBasedPaintCertificationRequiredIndicator;
        private string m_propertyTaxMessageDescription;
        private string m_preliminaryTitleReportDate;
        private string m_titleReportItemsDescription;
        private string m_titleReportRequiredEndorsementsDescription;
        private E_YesNoIndicator m_termiteReportRequiredIndicator;
        private string m_hoursDocumentsNeededPriorToDisbursementCount;
        private string m_fundingCutoffTime;
        private E_YesNoIndicator m_specialFloodHazardAreaIndicator;
        
        #endregion

        #region Public Properties
        public void AddCondition(XECondition condition) 
        {
            m_conditionList.Add(condition);
        }
        public string ConsolidatedClosingConditionsDescription 
        {
            get { return m_consolidatedClosingConditionsDescription; }
            set { m_consolidatedClosingConditionsDescription = value; }
        }
        public E_YesNoIndicator SpecialFloodHazardAreaIndictor 
        {
            get { return m_specialFloodHazardAreaIndictor; }
            set { m_specialFloodHazardAreaIndictor = value; }
        }
        public E_YesNoIndicator LeadBasedPaintCertificationRequiredIndicator 
        {
            get { return m_leadBasedPaintCertificationRequiredIndicator; }
            set { m_leadBasedPaintCertificationRequiredIndicator = value; }
        }
        public string PropertyTaxMessageDescription 
        {
            get { return m_propertyTaxMessageDescription; }
            set { m_propertyTaxMessageDescription = value; }
        }
        public string PreliminaryTitleReportDate 
        {
            get { return m_preliminaryTitleReportDate; }
            set { m_preliminaryTitleReportDate = value; }
        }
        public string TitleReportItemsDescription 
        {
            get { return m_titleReportItemsDescription; }
            set { m_titleReportItemsDescription = value; }
        }
        public string TitleReportRequiredEndorsementsDescription 
        {
            get { return m_titleReportRequiredEndorsementsDescription; }
            set { m_titleReportRequiredEndorsementsDescription = value; }
        }
        public E_YesNoIndicator TermiteReportRequiredIndicator 
        {
            get { return m_termiteReportRequiredIndicator; }
            set { m_termiteReportRequiredIndicator = value; }
        }
        public string HoursDocumentsNeededPriorToDisbursementCount 
        {
            get { return m_hoursDocumentsNeededPriorToDisbursementCount; }
            set { m_hoursDocumentsNeededPriorToDisbursementCount = value; }
        }
        public string FundingCutoffTime 
        {
            get { return m_fundingCutoffTime; }
            set { m_fundingCutoffTime = value; }
        }
        public E_YesNoIndicator SpecialFloodHazardAreaIndicator 
        {
            get { return m_specialFloodHazardAreaIndicator; }
            set { m_specialFloodHazardAreaIndicator = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("CLOSING_INSTRUCTIONS");
            WriteAttribute(writer, "_ConsolidatedClosingConditionsDescription", m_consolidatedClosingConditionsDescription);
            WriteAttribute(writer, "SpecialFloodHazardAreaIndictor", m_specialFloodHazardAreaIndictor);
            WriteAttribute(writer, "LeadBasedPaintCertificationRequiredIndicator", m_leadBasedPaintCertificationRequiredIndicator);
            WriteAttribute(writer, "_PropertyTaxMessageDescription", m_propertyTaxMessageDescription);
            WriteAttribute(writer, "PreliminaryTitleReportDate", m_preliminaryTitleReportDate);
            WriteAttribute(writer, "TitleReportItemsDescription", m_titleReportItemsDescription);
            WriteAttribute(writer, "TitleReportRequiredEndorsementsDescription", m_titleReportRequiredEndorsementsDescription);
            WriteAttribute(writer, "_TermiteReportRequiredIndicator", m_termiteReportRequiredIndicator);
            WriteAttribute(writer, "HoursDocumentsNeededPriorToDisbursementCount", m_hoursDocumentsNeededPriorToDisbursementCount);
            WriteAttribute(writer, "FundingCutoffTime", m_fundingCutoffTime);
            WriteAttribute(writer, "SpecialFloodHazardAreaIndicator", m_specialFloodHazardAreaIndicator);

            WriteElement(writer, m_conditionList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
