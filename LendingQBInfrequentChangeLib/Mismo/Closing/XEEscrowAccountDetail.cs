/// Author: David Dao

using System;
using System.Collections;
using System.Xml;

namespace Mismo.Closing
{
	public class XEEscrowAccountDetail : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="ESCROW_ACCOUNT_DETAIL">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="ESCROW_ACCOUNT_ACTIVITY" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_InitialBalanceAmount" type="xs:string"/>
        //      <xs:attribute name="_MinimumBalanceAmount" type="xs:string"/>
        //      <xs:attribute name="TotalMonthlyPITIAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private ArrayList m_escrowAccountActivityList = new ArrayList();
        private string m_initialBalanceAmount;
        private string m_minimumBalanceAmount;
        private string m_totalMonthlyPITIAmount;
        
        #endregion

        #region Public Properties
        public void AddEscrowAccountActivity(XEEscrowAccountActivity escrowAccountActivity) 
        {
            m_escrowAccountActivityList.Add(escrowAccountActivity);
        }
        public string InitialBalanceAmount 
        {
            get { return m_initialBalanceAmount; }
            set { m_initialBalanceAmount = value; }
        }
        public string MinimumBalanceAmount 
        {
            get { return m_minimumBalanceAmount; }
            set { m_minimumBalanceAmount = value; }
        }
        public string TotalMonthlyPITIAmount 
        {
            get { return m_totalMonthlyPITIAmount; }
            set { m_totalMonthlyPITIAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ESCROW_ACCOUNT_DETAIL");
            WriteAttribute(writer, "_InitialBalanceAmount", m_initialBalanceAmount);
            WriteAttribute(writer, "_MinimumBalanceAmount", m_minimumBalanceAmount);
            WriteAttribute(writer, "TotalMonthlyPITIAmount", m_totalMonthlyPITIAmount);

            WriteElement(writer, m_escrowAccountActivityList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
