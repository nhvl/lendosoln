/// Author: David Dao

using System;
using System.Xml;
using System.Collections;

namespace Mismo.Closing
{
	public class XELoanProductData : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="LOAN_PRODUCT_DATA">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="ARM" minOccurs="0"/>
        //        <xs:element ref="BUYDOWN" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="LOAN_FEATURES" minOccurs="0"/>
        //        <xs:element ref="PAYMENT_ADJUSTMENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RATE_ADJUSTMENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="HELOC" minOccurs="0"/>
        //        <xs:element ref="INTEREST_ONLY" minOccurs="0"/>
        //        <xs:element ref="PREPAYMENT_PENALTY" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private XEArm m_arm;
        private ArrayList m_buyDownList = new ArrayList();
        private XELoanFeatures m_loanFeatures;
        private ArrayList m_paymentAdjustmentList = new ArrayList();
        private ArrayList m_rateAdjustmentList = new ArrayList();
        private XEHeloc m_heloc;
        private XEInterestOnly m_interestOnly;
        private ArrayList m_prepaymentPenaltyList = new ArrayList();
        #endregion

        #region Public Properties
        public void SetArm(XEArm arm) 
        {
            m_arm = arm;
        }
        public void AddBuyDown(XEBuyDown buyDown) 
        {
            m_buyDownList.Add(buyDown);
        }
        public void SetLoanFeatures(XELoanFeatures loanFeatures) 
        {
            m_loanFeatures = loanFeatures;
        }
        public void AddPaymentAdjustment(XEPaymentAdjustment paymentAdjustment) 
        {
            m_paymentAdjustmentList.Add(paymentAdjustment);
        }
        public void AddRateAdjustment(XERateAdjustment rateAdjustment) 
        {
            m_rateAdjustmentList.Add(rateAdjustment);
        }
        public void SetHeloc(XEHeloc heloc) 
        {
            m_heloc = heloc;
        }
        public void SetInterestOnly(XEInterestOnly interestOnly) 
        {
            m_interestOnly = interestOnly;
        }
        public void AddPrepaymentPenalty(XEPrepaymentPenalty prepaymentPenalty) 
        {
            m_prepaymentPenaltyList.Add(prepaymentPenalty);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LOAN_PRODUCT_DATA");
            WriteElement(writer, m_arm);
            WriteElement(writer, m_buyDownList);
            WriteElement(writer, m_loanFeatures);
            WriteElement(writer, m_paymentAdjustmentList);
            WriteElement(writer, m_rateAdjustmentList);
            WriteElement(writer, m_heloc);
            WriteElement(writer, m_interestOnly);
            WriteElement(writer, m_prepaymentPenaltyList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
