/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
    public class XEDetails : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="_DETAILS">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="CondominiumIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="CondominiumPUDDeclarationsDescription" type="xs:string"/>
        //          <xs:attribute name="NFIPFloodZoneIdentifier" type="xs:string"/>
        //          <xs:attribute name="JudicialDistrictName" type="xs:string"/>
        //          <xs:attribute name="JudicialDivisionName" type="xs:string"/>
        //          <xs:attribute name="ManufacturedHomeIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="OneToFourFamilyIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="ProjectName" type="xs:string"/>
        //          <xs:attribute name="RecordingJurisdictionName" type="xs:string"/>
        //          <xs:attribute name="RecordingJurisdictionType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Parish"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="County"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="RecordingJurisdictionTypeOtherDescription" type="xs:string"/>
        //          <xs:attribute name="PropertyUnincorporatedAreaName" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_condominiumIndicator;
        private string m_condominiumPUDDeclarationsDescription;
        private string m_nfipFloodZoneIdentifier;
        private string m_judicialDistrictName;
        private string m_judicialDivisionName;
        private E_YesNoIndicator m_manufacturedHomeIndicator;
        private E_YesNoIndicator m_oneToFourFamilyIndicator;
        private string m_projectName;
        private string m_recordingJurisdictionName;
        private E_DetailsRecordingJurisdictionType m_recordingJurisdictionType;
        private string m_recordingJurisdictionTypeOtherDescription;
        private string m_propertyUnincorporatedAreaName;
        
        #endregion

        #region Public Properties
        public E_YesNoIndicator CondominiumIndicator 
        {
            get { return m_condominiumIndicator; }
            set { m_condominiumIndicator = value; }
        }
        public string CondominiumPUDDeclarationsDescription 
        {
            get { return m_condominiumPUDDeclarationsDescription; }
            set { m_condominiumPUDDeclarationsDescription = value; }
        }
        public string NfipFloodZoneIdentifier 
        {
            get { return m_nfipFloodZoneIdentifier; }
            set { m_nfipFloodZoneIdentifier = value; }
        }
        public string JudicialDistrictName 
        {
            get { return m_judicialDistrictName; }
            set { m_judicialDistrictName = value; }
        }
        public string JudicialDivisionName 
        {
            get { return m_judicialDivisionName; }
            set { m_judicialDivisionName = value; }
        }
        public E_YesNoIndicator ManufacturedHomeIndicator 
        {
            get { return m_manufacturedHomeIndicator; }
            set { m_manufacturedHomeIndicator = value; }
        }
        public E_YesNoIndicator OneToFourFamilyIndicator 
        {
            get { return m_oneToFourFamilyIndicator; }
            set { m_oneToFourFamilyIndicator = value; }
        }
        public string ProjectName 
        {
            get { return m_projectName; }
            set { m_projectName = value; }
        }
        public string RecordingJurisdictionName 
        {
            get { return m_recordingJurisdictionName; }
            set { m_recordingJurisdictionName = value; }
        }
        public E_DetailsRecordingJurisdictionType RecordingJurisdictionType 
        {
            get { return m_recordingJurisdictionType; }
            set { m_recordingJurisdictionType = value; }
        }
        public string RecordingJurisdictionTypeOtherDescription 
        {
            get { return m_recordingJurisdictionTypeOtherDescription; }
            set { m_recordingJurisdictionTypeOtherDescription = value; }
        }
        public string PropertyUnincorporatedAreaName 
        {
            get { return m_propertyUnincorporatedAreaName; }
            set { m_propertyUnincorporatedAreaName = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_DETAILS");
            WriteAttribute(writer, "CondominiumIndicator", m_condominiumIndicator);
            WriteAttribute(writer, "CondominiumPUDDeclarationsDescription", m_condominiumPUDDeclarationsDescription);
            WriteAttribute(writer, "NFIPFloodZoneIdentifier", m_nfipFloodZoneIdentifier);
            WriteAttribute(writer, "JudicialDistrictName", m_judicialDistrictName);
            WriteAttribute(writer, "JudicialDivisionName", m_judicialDivisionName);
            WriteAttribute(writer, "ManufacturedHomeIndicator", m_manufacturedHomeIndicator);
            WriteAttribute(writer, "OneToFourFamilyIndicator", m_oneToFourFamilyIndicator);
            WriteAttribute(writer, "ProjectName", m_projectName);
            WriteAttribute(writer, "RecordingJurisdictionName", m_recordingJurisdictionName);
            WriteAttribute(writer, "RecordingJurisdictionType", m_recordingJurisdictionType);
            WriteAttribute(writer, "RecordingJurisdictionTypeOtherDescription", m_recordingJurisdictionTypeOtherDescription);
            WriteAttribute(writer, "PropertyUnincorporatedAreaName", m_propertyUnincorporatedAreaName);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_DetailsRecordingJurisdictionType 
    {
        Undefined = 0,
        Parish,
        Other,
        County
    }
}
