/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XERequestToRescind : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="REQUEST_TO_RESCIND">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_TransactionDate" type="xs:string"/>
        //          <xs:attribute name="_NotificationUnparsedName" type="xs:string"/>
        //          <xs:attribute name="_NotificationStreetAddress" type="xs:string"/>
        //          <xs:attribute name="_NotificationStreetAddress2" type="xs:string"/>
        //          <xs:attribute name="_NotificationCity" type="xs:string"/>
        //          <xs:attribute name="_NotificationState" type="xs:string"/>
        //          <xs:attribute name="_NotificationPostalCode" type="xs:string"/>
        //          <xs:attribute name="_NotificationCountry" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_transactionDate;
        private string m_notificationUnparsedName;
        private string m_notificationStreetAddress;
        private string m_notificationStreetAddress2;
        private string m_notificationCity;
        private string m_notificationState;
        private string m_notificationPostalCode;
        private string m_notificationCountry;

        #endregion

        #region Public Properties
        public string TransactionDate 
        {
            get { return m_transactionDate; }
            set { m_transactionDate = value; }
        }
        public string NotificationUnparsedName 
        {
            get { return m_notificationUnparsedName; }
            set { m_notificationUnparsedName = value; }
        }
        public string NotificationStreetAddress 
        {
            get { return m_notificationStreetAddress; }
            set { m_notificationStreetAddress = value; }
        }
        public string NotificationStreetAddress2 
        {
            get { return m_notificationStreetAddress2; }
            set { m_notificationStreetAddress2 = value; }
        }
        public string NotificationCity 
        {
            get { return m_notificationCity; }
            set { m_notificationCity = value; }
        }
        public string NotificationState 
        {
            get { return m_notificationState; }
            set { m_notificationState = value; }
        }
        public string NotificationPostalCode 
        {
            get { return m_notificationPostalCode; }
            set { m_notificationPostalCode = value; }
        }
        public string NotificationCountry 
        {
            get { return m_notificationCountry; }
            set { m_notificationCountry = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("REQUEST_TO_RESCIND");
            WriteAttribute(writer, "_TransactionDate", m_transactionDate);
            WriteAttribute(writer, "_NotificationUnparsedName", m_notificationUnparsedName);
            WriteAttribute(writer, "_NotificationStreetAddress", m_notificationStreetAddress);
            WriteAttribute(writer, "_NotificationStreetAddress2", m_notificationStreetAddress2);
            WriteAttribute(writer, "_NotificationCity", m_notificationCity);
            WriteAttribute(writer, "_NotificationState", m_notificationState);
            WriteAttribute(writer, "_NotificationPostalCode", m_notificationPostalCode);
            WriteAttribute(writer, "_NotificationCountry", m_notificationCountry);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
