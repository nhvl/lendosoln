/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEPrepaymentPenalty : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="PREPAYMENT_PENALTY">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_Percent" type="xs:string"/>
        //          <xs:attribute name="_PeriodSequenceIdentifier" type="xs:string"/>
        //          <xs:attribute name="_TermMonths" type="xs:string"/>
        //          <xs:attribute name="_TextDescription" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_percent;
        private string m_periodSequenceIdentifier;
        private string m_termMonths;
        private string m_textDescription;
        
        #endregion

        #region Public Properties
        public string Percent 
        {
            get { return m_percent; }
            set { m_percent = value; }
        }
        public string PeriodSequenceIdentifier 
        {
            get { return m_periodSequenceIdentifier; }
            set { m_periodSequenceIdentifier = value; }
        }
        public string TermMonths 
        {
            get { return m_termMonths; }
            set { m_termMonths = value; }
        }
        public string TextDescription 
        {
            get { return m_textDescription; }
            set { m_textDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PREPAYMENT_PENALTY");
            WriteAttribute(writer, "_Percent", m_percent);
            WriteAttribute(writer, "_PeriodSequenceIdentifier", m_periodSequenceIdentifier);
            WriteAttribute(writer, "_TermMonths", m_termMonths);
            WriteAttribute(writer, "_TextDescription", m_textDescription);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
