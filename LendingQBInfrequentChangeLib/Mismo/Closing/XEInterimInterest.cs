/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEInterimInterest : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="INTERIM_INTEREST">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_PaidFromDate" type="xs:string"/>
        //          <xs:attribute name="_PaidNumberOfDays" type="xs:string"/>
        //          <xs:attribute name="_PaidThroughDate" type="xs:string"/>
        //          <xs:attribute name="_PerDiemCalculationMethodType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="365"/>
        //                <xs:enumeration value="360"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_PerDiemPaymentOptionType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Closing"/>
        //                <xs:enumeration value="FirstPayment"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_SinglePerDiemAmount" type="xs:string"/>
        //          <xs:attribute name="_TotalPerDiemAmount" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_paidFromDate;
        private string m_paidNumberOfDays;
        private string m_paidThroughDate;
        private E_InterimInterestPerDiemCalculationMethodType m_perDiemCalculationMethodType;
        private E_InterimInterestPerDiemPaymentOptionType m_perDiemPaymentOptionType;
        private string m_singlePerDiemAmount;
        private string m_totalPerDiemAmount;

        #endregion

        #region Public Properties
        public string PaidFromDate 
        {
            get { return m_paidFromDate; }
            set { m_paidFromDate = value; }
        }
        public string PaidNumberOfDays 
        {
            get { return m_paidNumberOfDays; }
            set { m_paidNumberOfDays = value; }
        }
        public string PaidThroughDate 
        {
            get { return m_paidThroughDate; }
            set { m_paidThroughDate = value; }
        }
        public E_InterimInterestPerDiemCalculationMethodType PerDiemCalculationMethodType 
        {
            get { return m_perDiemCalculationMethodType; }
            set { m_perDiemCalculationMethodType = value; }
        }
        public E_InterimInterestPerDiemPaymentOptionType PerDiemPaymentOptionType 
        {
            get { return m_perDiemPaymentOptionType; }
            set { m_perDiemPaymentOptionType = value; }
        }
        public string SinglePerDiemAmount 
        {
            get { return m_singlePerDiemAmount; }
            set { m_singlePerDiemAmount = value; }
        }
        public string TotalPerDiemAmount 
        {
            get { return m_totalPerDiemAmount; }
            set { m_totalPerDiemAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("INTERIM_INTEREST");
            WriteAttribute(writer, "_PaidFromDate", m_paidFromDate);
            WriteAttribute(writer, "_PaidNumberOfDays", m_paidNumberOfDays);
            WriteAttribute(writer, "_PaidThroughDate", m_paidThroughDate);
            if (m_perDiemCalculationMethodType != E_InterimInterestPerDiemCalculationMethodType.Undefined) 
            {
                WriteAttribute(writer, "_PerDiemCalculationMethodType", m_perDiemCalculationMethodType.ToString().Replace("_", ""));
            }
            WriteAttribute(writer, "_PerDiemPaymentOptionType", m_perDiemPaymentOptionType);
            WriteAttribute(writer, "_SinglePerDiemAmount", m_singlePerDiemAmount);
            WriteAttribute(writer, "_TotalPerDiemAmount", m_totalPerDiemAmount);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_InterimInterestPerDiemCalculationMethodType 
    {
        Undefined = 0,
        _365,
        _360
    }
    public enum E_InterimInterestPerDiemPaymentOptionType 
    {
        Undefined = 0,
        Closing,
        FirstPayment
    }
}
