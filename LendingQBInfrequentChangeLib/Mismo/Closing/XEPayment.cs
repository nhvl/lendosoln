/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
    public class XEPayment : Mismo.Common.AbstractXmlNode
    {
        public XEPayment()
        {
        }
        #region Schema
        //  <xs:element name="_PAYMENT">
        //    <xs:complexType>
        //      <xs:attribute name="_AllowableFHAClosingCostIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_Amount" type="xs:string"/>
        //      <xs:attribute name="_CollectedByType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Broker"/>
        //            <xs:enumeration value="Lender"/>
        //            <xs:enumeration value="Investor"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_IncludedInAPRIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_NetDueAmount" type="xs:string"/>
        //      <xs:attribute name="_PaidByType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Buyer"/>
        //            <xs:enumeration value="Lender"/>
        //            <xs:enumeration value="Seller"/>
        //            <xs:enumeration value="ThirdParty"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_PaidByTypeThirdPartyName" type="xs:string"/>
        //      <xs:attribute name="_PaidOutsideOfClosingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_Percent" type="xs:string"/>
        //      <xs:attribute name="_ProcessType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Closing"/>
        //            <xs:enumeration value="Processing"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_Section32Indicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_amount;
        private E_YesNoIndicator m_allowableFHAClosingCostIndicator;
        private E_PaymentCollectedByType m_collectedByType;
        private E_YesNoIndicator m_includedInAPRIndicator;
        private E_PaymentPaidByType m_paidByType;
        private string m_paidByTypeThirdPartyName;
        private E_YesNoIndicator m_paidOutsideOfClosingIndicator;
        private string m_percent;
        private E_PaymentProcessType m_processType;
        private E_YesNoIndicator m_section32Indicator;
        private string m_netDueAmount;

        #endregion

        #region Public Properties
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public E_YesNoIndicator AllowableFHAClosingCostIndicator 
        {
            get { return m_allowableFHAClosingCostIndicator; }
            set { m_allowableFHAClosingCostIndicator = value; }
        }
        public E_PaymentCollectedByType CollectedByType 
        {
            get { return m_collectedByType; }
            set { m_collectedByType = value; }
        }
        public E_YesNoIndicator IncludedInAPRIndicator 
        {
            get { return m_includedInAPRIndicator; }
            set { m_includedInAPRIndicator = value; }
        }
        public E_PaymentPaidByType PaidByType 
        {
            get { return m_paidByType; }
            set { m_paidByType = value; }
        }
        public string PaidByTypeThirdPartyName 
        {
            get { return m_paidByTypeThirdPartyName; }
            set { m_paidByTypeThirdPartyName = value; }
        }
        public E_YesNoIndicator PaidOutsideOfClosingIndicator 
        {
            get { return m_paidOutsideOfClosingIndicator; }
            set { m_paidOutsideOfClosingIndicator = value; }
        }
        public string Percent 
        {
            get { return m_percent; }
            set { m_percent = value; }
        }
        public E_PaymentProcessType ProcessType 
        {
            get { return m_processType; }
            set { m_processType = value; }
        }
        public E_YesNoIndicator Section32Indicator 
        {
            get { return m_section32Indicator; }
            set { m_section32Indicator = value; }
        }
        public string NetDueAmount 
        {
            get { return m_netDueAmount; }
            set { m_netDueAmount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_PAYMENT");
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_AllowableFHAClosingCostIndicator", m_allowableFHAClosingCostIndicator);
            WriteAttribute(writer, "_CollectedByType", m_collectedByType);
            WriteAttribute(writer, "_IncludedInAPRIndicator", m_includedInAPRIndicator);
            WriteAttribute(writer, "_NetDueAmount", m_netDueAmount);
            WriteAttribute(writer, "_PaidByType", m_paidByType);
            WriteAttribute(writer, "_PaidByTypeThirdPartyName", m_paidByTypeThirdPartyName);
            WriteAttribute(writer, "_PaidOutsideOfClosingIndicator", m_paidOutsideOfClosingIndicator);
            WriteAttribute(writer, "_Percent", m_percent);
            WriteAttribute(writer, "_ProcessType", m_processType);
            WriteAttribute(writer, "_Section32Indicator", m_section32Indicator);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_PaymentCollectedByType 
    {
        Undefined = 0,
        Broker,
        Lender,
        Investor
    }
    public enum E_PaymentPaidByType 
    {
        Undefined = 0,
        Buyer,
        Lender,
        Seller,
        ThirdParty
    }
    public enum E_PaymentProcessType 
    {
        Undefined = 0,
        Closing,
        Processing
    }
}
