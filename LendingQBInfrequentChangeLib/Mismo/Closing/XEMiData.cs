/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
    public class XEMiData : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="MI_DATA">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="MI_PREMIUM_TAX" minOccurs="0"/>
        //        <xs:element ref="MI_RENEWAL_PREMIUM" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="_PAID_TO" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="BorrowerMITerminationDate" type="xs:string"/>
        //      <xs:attribute name="MIScheduledTerminationDate" type="xs:string"/>
        //      <xs:attribute name="MI_FHAUpfrontPremiumAmount" type="xs:string"/>
        //      <xs:attribute name="MICertificateIdentifier" type="xs:string"/>
        //      <xs:attribute name="MICollectedNumberOfMonthsCount" type="xs:string"/>
        //      <xs:attribute name="MICompanyName" type="xs:string"/>
        //      <xs:attribute name="MICushionNumberOfMonthsCount" type="xs:string"/>
        //      <xs:attribute name="ScheduledAmortizationMidpointDate" type="xs:string"/>
        //      <xs:attribute name="MIDurationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="SingleSpecific"/>
        //            <xs:enumeration value="PeriodicMonthly"/>
        //            <xs:enumeration value="Annual"/>
        //            <xs:enumeration value="SingleLifeOfLoan"/>
        //            <xs:enumeration value="NotApplicable"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIEscrowIncludedInAggregateIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIInitialPremiumAmount" type="xs:string"/>
        //      <xs:attribute name="MIInitialPremiumAtClosingType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Prepaid"/>
        //            <xs:enumeration value="Deferred"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIInitialPremiumRateDurationMonths" type="xs:string"/>
        //      <xs:attribute name="MIInitialPremiumRatePercent" type="xs:string"/>
        //      <xs:attribute name="MI_LTVCutoffPercent" type="xs:string"/>
        //      <xs:attribute name="MI_LTVCutoffType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="SalesPrice"/>
        //            <xs:enumeration value="AppraisedValue"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIPremiumFinancedIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIPremiumFromClosingAmount" type="xs:string"/>
        //      <xs:attribute name="MIPremiumPaymentType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Prepaid"/>
        //            <xs:enumeration value="PaidFromEscrow"/>
        //            <xs:enumeration value="Financed"/>
        //            <xs:enumeration value="BothBorrowerAndLenderPaid"/>
        //            <xs:enumeration value="LenderPaid"/>
        //            <xs:enumeration value="BorrowerPaid"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIPremiumRefundableType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="NotRefundable"/>
        //            <xs:enumeration value="Refundable"/>
        //            <xs:enumeration value="RefundableWithLimits"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MIPremiumTermMonths" type="xs:string"/>
        //      <xs:attribute name="MIRenewalCalculationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="NoRenewals"/>
        //            <xs:enumeration value="Constant"/>
        //            <xs:enumeration value="NotApplicable"/>
        //            <xs:enumeration value="Declining"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="MISourceType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="PMI"/>
        //            <xs:enumeration value="FHA"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private Mismo.MismoXmlElement.XEMIPremiumTax m_miPremiumTax;
        private ArrayList m_miRenewalPremiumList = new ArrayList();
        private XEPaidTo m_paidTo;
        private string m_borrowerMITerminationDate;
        private string m_miScheduledTerminationDate;
        private string m_miFhaUpfrontPremiumAmount;
        private string m_miCertificateIdentifier;
        private string m_miCollectedNumberOfMonthsCount;
        private string m_miCompanyName;
        private string m_miCushionNumberOfMonthsCount;
        private string m_scheduledAmortizationMidpointDate;
        private E_MiDurationType m_miDurationType;
        private E_YesNoIndicator m_miEscrowIncludedInAggregateIndicator;
        private string m_miInitialPremiumAmount;
        private E_MiInitialPremiumAtClosingType m_miInitialPremiumAtClosingType;
        private string m_miInitialPremiumRateDurationMonths;
        private string m_miInitialPremiumRatePercent;
        private string m_miLtvCutoffPercent;
        private E_MiLtvCutoffType m_miLtvCutoffType;
        private E_YesNoIndicator m_miPremiumFinancedIndicator;
        private string m_miPremiumFromClosingAmount;
        private E_MiPremiumPaymentType m_miPremiumPaymentType;
        private E_MiPremiumRefundableType m_miPremiumRefundableType;
        private string m_miPremiumTermMonths;
        private E_MiRenewalCalculationType m_miRenewalCalculationType;
        private E_MiSourceType m_miSourceType;
        
        #endregion

        #region Public Properties
        public void SetMiPremiumTax(Mismo.MismoXmlElement.XEMIPremiumTax miPremiumTax) 
        {
            m_miPremiumTax = miPremiumTax;
        }
        public void AddMiRenewalPremium(XEMiRenewalPremium miRenewalPremium) 
        {
            m_miRenewalPremiumList.Add(miRenewalPremium);
        }
        public void SetPaidTo(XEPaidTo paidTo) 
        {
            m_paidTo = paidTo;
        }
        public string BorrowerMITerminationDate 
        {
            get { return m_borrowerMITerminationDate; }
            set { m_borrowerMITerminationDate = value; }
        }
        public string MiScheduledTerminationDate 
        {
            get { return m_miScheduledTerminationDate; }
            set { m_miScheduledTerminationDate = value; }
        }
        public string MiFhaUpfrontPremiumAmount 
        {
            get { return m_miFhaUpfrontPremiumAmount; }
            set { m_miFhaUpfrontPremiumAmount = value; }
        }
        public string MiCertificateIdentifier 
        {
            get { return m_miCertificateIdentifier; }
            set { m_miCertificateIdentifier = value; }
        }
        public string MiCollectedNumberOfMonthsCount 
        {
            get { return m_miCollectedNumberOfMonthsCount; }
            set { m_miCollectedNumberOfMonthsCount = value; }
        }
        public string MiCompanyName 
        {
            get { return m_miCompanyName; }
            set { m_miCompanyName = value; }
        }
        public string MiCushionNumberOfMonthsCount 
        {
            get { return m_miCushionNumberOfMonthsCount; }
            set { m_miCushionNumberOfMonthsCount = value; }
        }
        public string ScheduledAmortizationMidpointDate 
        {
            get { return m_scheduledAmortizationMidpointDate; }
            set { m_scheduledAmortizationMidpointDate = value; }
        }
        public E_MiDurationType MiDurationType 
        {
            get { return m_miDurationType; }
            set { m_miDurationType = value; }
        }
        public E_YesNoIndicator MiEscrowIncludedInAggregateIndicator 
        {
            get { return m_miEscrowIncludedInAggregateIndicator; }
            set { m_miEscrowIncludedInAggregateIndicator = value; }
        }
        public string MiInitialPremiumAmount 
        {
            get { return m_miInitialPremiumAmount; }
            set { m_miInitialPremiumAmount = value; }
        }
        public E_MiInitialPremiumAtClosingType MiInitialPremiumAtClosingType 
        {
            get { return m_miInitialPremiumAtClosingType; }
            set { m_miInitialPremiumAtClosingType = value; }
        }
        public string MiInitialPremiumRateDurationMonths 
        {
            get { return m_miInitialPremiumRateDurationMonths; }
            set { m_miInitialPremiumRateDurationMonths = value; }
        }
        public string MiInitialPremiumRatePercent 
        {
            get { return m_miInitialPremiumRatePercent; }
            set { m_miInitialPremiumRatePercent = value; }
        }
        public string MiLtvCutoffPercent 
        {
            get { return m_miLtvCutoffPercent; }
            set { m_miLtvCutoffPercent = value; }
        }
        public E_MiLtvCutoffType MiLtvCutoffType 
        {
            get { return m_miLtvCutoffType; }
            set { m_miLtvCutoffType = value; }
        }
        public E_YesNoIndicator MiPremiumFinancedIndicator 
        {
            get { return m_miPremiumFinancedIndicator; }
            set { m_miPremiumFinancedIndicator = value; }
        }
        public string MiPremiumFromClosingAmount 
        {
            get { return m_miPremiumFromClosingAmount; }
            set { m_miPremiumFromClosingAmount = value; }
        }
        public E_MiPremiumPaymentType MiPremiumPaymentType 
        {
            get { return m_miPremiumPaymentType; }
            set { m_miPremiumPaymentType = value; }
        }
        public E_MiPremiumRefundableType MiPremiumRefundableType 
        {
            get { return m_miPremiumRefundableType; }
            set { m_miPremiumRefundableType = value; }
        }
        public string MiPremiumTermMonths 
        {
            get { return m_miPremiumTermMonths; }
            set { m_miPremiumTermMonths = value; }
        }
        public E_MiRenewalCalculationType MiRenewalCalculationType 
        {
            get { return m_miRenewalCalculationType; }
            set { m_miRenewalCalculationType = value; }
        }
        public E_MiSourceType MiSourceType 
        {
            get { return m_miSourceType; }
            set { m_miSourceType = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MI_DATA");
            WriteAttribute(writer, "BorrowerMITerminationDate", m_borrowerMITerminationDate);
            WriteAttribute(writer, "MIScheduledTerminationDate", m_miScheduledTerminationDate);
            WriteAttribute(writer, "MI_FHAUpfrontPremiumAmount", m_miFhaUpfrontPremiumAmount);
            WriteAttribute(writer, "MICertificateIdentifier", m_miCertificateIdentifier);
            WriteAttribute(writer, "MICollectedNumberOfMonthsCount", m_miCollectedNumberOfMonthsCount);
            WriteAttribute(writer, "MICompanyName", m_miCompanyName);
            WriteAttribute(writer, "MICushionNumberOfMonthsCount", m_miCushionNumberOfMonthsCount);
            WriteAttribute(writer, "ScheduledAmortizationMidpointDate", m_scheduledAmortizationMidpointDate);
            WriteAttribute(writer, "MIDurationType", m_miDurationType);
            WriteAttribute(writer, "MIEscrowIncludedInAggregateIndicator", m_miEscrowIncludedInAggregateIndicator);
            WriteAttribute(writer, "MIInitialPremiumAmount", m_miInitialPremiumAmount);
            WriteAttribute(writer, "MIInitialPremiumAtClosingType", m_miInitialPremiumAtClosingType);
            WriteAttribute(writer, "MIInitialPremiumRateDurationMonths", m_miInitialPremiumRateDurationMonths);
            WriteAttribute(writer, "MIInitialPremiumRatePercent", m_miInitialPremiumRatePercent);
            WriteAttribute(writer, "MI_LTVCutoffPercent", m_miLtvCutoffPercent);
            WriteAttribute(writer, "MI_LTVCutoffType", m_miLtvCutoffType);
            WriteAttribute(writer, "MIPremiumFinancedIndicator", m_miPremiumFinancedIndicator);
            WriteAttribute(writer, "MIPremiumFromClosingAmount", m_miPremiumFromClosingAmount);
            WriteAttribute(writer, "MIPremiumPaymentType", m_miPremiumPaymentType);
            WriteAttribute(writer, "MIPremiumRefundableType", m_miPremiumRefundableType);
            WriteAttribute(writer, "MIPremiumTermMonths", m_miPremiumTermMonths);
            WriteAttribute(writer, "MIRenewalCalculationType", m_miRenewalCalculationType);
            WriteAttribute(writer, "MISourceType", m_miSourceType);

            WriteElement(writer, m_miPremiumTax);
            WriteElement(writer, m_miRenewalPremiumList);
            WriteElement(writer, m_paidTo);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_MiDurationType 
    {
        Undefined = 0,
        SingleSpecific,
        PeriodicMonthly,
        Annual,
        SingleLifeOfLoan,
        NotApplicable
    }
    public enum E_MiInitialPremiumAtClosingType 
    {
        Undefined = 0,
        Prepaid,
        Deferred
    }
    public enum E_MiLtvCutoffType 
    {
        Undefined = 0,
        SalesPrice,
        AppraisedValue
    }
    public enum E_MiPremiumPaymentType 
    {
        Undefined = 0,
        Prepaid,
        PaidFromEscrow,
        Financed,
        BothBorrowerAndLenderPaid,
        LenderPaid,
        BorrowerPaid
    }
    public enum E_MiPremiumRefundableType 
    {
        Undefined = 0,
        NotRefundable,
        Refundable,
        RefundableWithLimits
    }
    public enum E_MiRenewalCalculationType 
    {
        Undefined = 0,
        NoRenewals,
        Constant,
        NotApplicable,
        Declining
    }
    public enum E_MiSourceType 
    {
        Undefined = 0,
        PMI,
        FHA
    }
}
