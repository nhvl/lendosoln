/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
    public class XEReturnTo : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="_RETURN_TO">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="CONTACT_DETAIL" minOccurs="0"/>
        //        <xs:element ref="NON_PERSON_ENTITY_DETAIL" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_UnparsedName" type="xs:string"/>
        //      <xs:attribute name="_TitleDescription" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress" type="xs:string"/>
        //      <xs:attribute name="_StreetAddress2" type="xs:string"/>
        //      <xs:attribute name="_City" type="xs:string"/>
        //      <xs:attribute name="_State" type="xs:string"/>
        //      <xs:attribute name="_PostalCode" type="xs:string"/>
        //      <xs:attribute name="_Country" type="xs:string"/>
        //      <xs:attribute name="_County" type="xs:string"/>
        //      <xs:attribute name="_ElectronicRoutingMethodType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="EMAIL"/>
        //            <xs:enumeration value="URI"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="URL"/>
        //            <xs:enumeration value="HTTP"/>
        //            <xs:enumeration value="FTP"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ElectronicRoutingAddress" type="xs:string"/>
        //      <xs:attribute name="_StateFIPSCode" type="xs:string"/>
        //      <xs:attribute name="_CountyFIPSCode" type="xs:string"/>
        //      <xs:attribute name="_CountryCode" type="xs:string"/>
        //      <xs:attribute name="NonPersonEntityIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private Mismo.MismoXmlElement.XEContactDetail m_contactDetail;
        private XENonPersonEntityDetail m_nonPersonEntityDetail;
        private string m_unparsedName;
        private string m_titleDescription;
        private string m_streetAddress;
        private string m_streetAddress2;
        private string m_city;
        private string m_state;
        private string m_postalCode;
        private string m_country;
        private string m_county;
        private E_ReturnToElectronicRoutingMethodType m_electronicRoutingMethodType;
        private string m_electronicRoutingAddress;
        private string m_stateFIPSCode;
        private string m_countyFIPSCode;
        private string m_countryCode;
        private E_YesNoIndicator m_nonPersonEntityIndicator;
        
        #endregion

        #region Public Properties
        public void SetContactDetail(Mismo.MismoXmlElement.XEContactDetail contactDetail) 
        {
            m_contactDetail = contactDetail;
        }
        public void SetNonPersonEntityDetail(XENonPersonEntityDetail nonPersonEntityDetail) 
        {
            m_nonPersonEntityDetail = nonPersonEntityDetail;
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }
        public string TitleDescription 
        {
            get { return m_titleDescription; }
            set { m_titleDescription = value; }
        }
        public string StreetAddress 
        {
            get { return m_streetAddress; }
            set { m_streetAddress = value; }
        }
        public string StreetAddress2 
        {
            get { return m_streetAddress2; }
            set { m_streetAddress2 = value; }
        }
        public string City 
        {
            get { return m_city; }
            set { m_city = value; }
        }
        public string State 
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public string PostalCode 
        {
            get { return m_postalCode; }
            set { m_postalCode = value; }
        }
        public string Country 
        {
            get { return m_country; }
            set { m_country = value; }
        }
        public string County 
        {
            get { return m_county; }
            set { m_county = value; }
        }
        public E_ReturnToElectronicRoutingMethodType ElectronicRoutingMethodType 
        {
            get { return m_electronicRoutingMethodType; }
            set { m_electronicRoutingMethodType = value; }
        }
        public string ElectronicRoutingAddress 
        {
            get { return m_electronicRoutingAddress; }
            set { m_electronicRoutingAddress = value; }
        }
        public string StateFIPSCode 
        {
            get { return m_stateFIPSCode; }
            set { m_stateFIPSCode = value; }
        }
        public string CountyFIPSCode 
        {
            get { return m_countyFIPSCode; }
            set { m_countyFIPSCode = value; }
        }
        public string CountryCode 
        {
            get { return m_countryCode; }
            set { m_countryCode = value; }
        }
        public E_YesNoIndicator NonPersonEntityIndicator 
        {
            get { return m_nonPersonEntityIndicator; }
            set { m_nonPersonEntityIndicator = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_RETURN_TO");
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);
            WriteAttribute(writer, "_TitleDescription", m_titleDescription);
            WriteAttribute(writer, "_StreetAddress", m_streetAddress);
            WriteAttribute(writer, "_StreetAddress2", m_streetAddress2);
            WriteAttribute(writer, "_City", m_city);
            WriteAttribute(writer, "_State", m_state);
            WriteAttribute(writer, "_PostalCode", m_postalCode);
            WriteAttribute(writer, "_Country", m_country);
            WriteAttribute(writer, "_County", m_county);
            WriteAttribute(writer, "_ElectronicRoutingMethodType", m_electronicRoutingMethodType);
            WriteAttribute(writer, "_ElectronicRoutingAddress", m_electronicRoutingAddress);
            WriteAttribute(writer, "_StateFIPSCode", m_stateFIPSCode);
            WriteAttribute(writer, "_CountyFIPSCode", m_countyFIPSCode);
            WriteAttribute(writer, "_CountryCode", m_countryCode);
            WriteAttribute(writer, "NonPersonEntityIndicator", m_nonPersonEntityIndicator);

            WriteElement(writer, m_contactDetail);
            WriteElement(writer, m_nonPersonEntityDetail);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_ReturnToElectronicRoutingMethodType 
    {
        Undefined = 0,
        EMAIL,
        URI,
        Other,
        URL,
        HTTP,
        FTP
    }
}
