/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
    public class XEHeloc : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="HELOC">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_AnnualFeeAmount" type="xs:string"/>
        //          <xs:attribute name="_CreditCardAccountIdentifier" type="xs:string"/>
        //          <xs:attribute name="_CreditCardIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_DailyPeriodicInterestRateCalculationType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="365"/>
        //                <xs:enumeration value="360"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_DailyPeriodicInterestRatePercent" type="xs:string"/>
        //          <xs:attribute name="_DrawPeriodMonthsCount" type="xs:string"/>
        //          <xs:attribute name="_FirstLienBookNumber" type="xs:string"/>
        //          <xs:attribute name="_FirstLienDate" type="xs:string"/>
        //          <xs:attribute name="_FirstLienHolderName" type="xs:string"/>
        //          <xs:attribute name="_FirstLienIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_FirstLienInstrumentNumber" type="xs:string"/>
        //          <xs:attribute name="_FirstLienPageNumber" type="xs:string"/>
        //          <xs:attribute name="_FirstLienPrincipalBalanceAmount" type="xs:string"/>
        //          <xs:attribute name="_FirstLienRecordedDate" type="xs:string"/>
        //          <xs:attribute name="_InitialAdvanceAmount" type="xs:string"/>
        //          <xs:attribute name="_MaximumAPRRate" type="xs:string"/>
        //          <xs:attribute name="_MinimumAdvanceAmount" type="xs:string"/>
        //          <xs:attribute name="_MinimumPaymentAmount" type="xs:string"/>
        //          <xs:attribute name="_MinimumPaymentPercent" type="xs:string"/>
        //          <xs:attribute name="_RepayPeriodMonthsCount" type="xs:string"/>
        //          <xs:attribute name="_ReturnedCheckChargeAmount" type="xs:string"/>
        //          <xs:attribute name="_StopPaymentChargeAmount" type="xs:string"/>
        //          <xs:attribute name="_TeaserTermEndDate" type="xs:string"/>
        //          <xs:attribute name="_TeaserTermMonthsCount" type="xs:string"/>
        //          <xs:attribute name="_TerminationFeeAmount" type="xs:string"/>
        //          <xs:attribute name="_TerminationPeriodMonthsCount" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_annualFeeAmount;
        private string m_creditCardAccountIdentifier;
        private E_YesNoIndicator m_creditCardIndicator;
        private E_HelocDailyPeriodicInterestRateCalculationType m_dailyPeriodicInterestRateCalculationType;
        private string m_dailyPeriodicInterestRatePercent;
        private string m_drawPeriodMonthsCount;
        private string m_firstLienBookNumber;
        private string m_firstLienDate;
        private string m_firstLienHolderName;
        private string m_firstLienIndicator;
        private string m_firstLienInstrumentNumber;
        private string m_firstLienPageNumber;
        private string m_firstLienPrincipalBalanceAmount;
        private string m_firstLienRecordedDate;
        private string m_initialAdvanceAmount;
        private string m_maximumAPRRate;
        private string m_minimumAdvanceAmount;
        private string m_minimumPaymentAmount;
        private string m_minimumPaymentPercent;
        private string m_repayPeriodMonthsCount;
        private string m_returnedCheckChargeAmount;
        private string m_stopPaymentChargeAmount;
        private string m_teaserTermEndDate;
        private string m_teaserTermMonthsCount;
        private string m_terminationFeeAmount;
        private string m_terminationPeriodMonthsCount;
        #endregion

        #region Public Properties
        public string AnnualFeeAmount 
        {
            get { return m_annualFeeAmount; }
            set { m_annualFeeAmount = value; }
        }
        public string CreditCardAccountIdentifier 
        {
            get { return m_creditCardAccountIdentifier; }
            set { m_creditCardAccountIdentifier = value; }
        }
        public E_YesNoIndicator CreditCardIndicator 
        {
            get { return m_creditCardIndicator; }
            set { m_creditCardIndicator = value; }
        }
        public E_HelocDailyPeriodicInterestRateCalculationType DailyPeriodicInterestRateCalculationType 
        {
            get { return m_dailyPeriodicInterestRateCalculationType; }
            set { m_dailyPeriodicInterestRateCalculationType = value; }
        }
        public string DailyPeriodicInterestRatePercent 
        {
            get { return m_dailyPeriodicInterestRatePercent; }
            set { m_dailyPeriodicInterestRatePercent = value; }
        }
        public string DrawPeriodMonthsCount 
        {
            get { return m_drawPeriodMonthsCount; }
            set { m_drawPeriodMonthsCount = value; }
        }
        public string FirstLienBookNumber 
        {
            get { return m_firstLienBookNumber; }
            set { m_firstLienBookNumber = value; }
        }
        public string FirstLienDate 
        {
            get { return m_firstLienDate; }
            set { m_firstLienDate = value; }
        }
        public string FirstLienHolderName 
        {
            get { return m_firstLienHolderName; }
            set { m_firstLienHolderName = value; }
        }
        public string FirstLienIndicator 
        {
            get { return m_firstLienIndicator; }
            set { m_firstLienIndicator = value; }
        }
        public string FirstLienInstrumentNumber 
        {
            get { return m_firstLienInstrumentNumber; }
            set { m_firstLienInstrumentNumber = value; }
        }
        public string FirstLienPageNumber 
        {
            get { return m_firstLienPageNumber; }
            set { m_firstLienPageNumber = value; }
        }
        public string FirstLienPrincipalBalanceAmount 
        {
            get { return m_firstLienPrincipalBalanceAmount; }
            set { m_firstLienPrincipalBalanceAmount = value; }
        }
        public string FirstLienRecordedDate 
        {
            get { return m_firstLienRecordedDate; }
            set { m_firstLienRecordedDate = value; }
        }
        public string InitialAdvanceAmount 
        {
            get { return m_initialAdvanceAmount; }
            set { m_initialAdvanceAmount = value; }
        }
        public string MaximumAPRRate 
        {
            get { return m_maximumAPRRate; }
            set { m_maximumAPRRate = value; }
        }
        public string MinimumAdvanceAmount 
        {
            get { return m_minimumAdvanceAmount; }
            set { m_minimumAdvanceAmount = value; }
        }
        public string MinimumPaymentAmount 
        {
            get { return m_minimumPaymentAmount; }
            set { m_minimumPaymentAmount = value; }
        }
        public string MinimumPaymentPercent 
        {
            get { return m_minimumPaymentPercent; }
            set { m_minimumPaymentPercent = value; }
        }
        public string RepayPeriodMonthsCount 
        {
            get { return m_repayPeriodMonthsCount; }
            set { m_repayPeriodMonthsCount = value; }
        }
        public string ReturnedCheckChargeAmount 
        {
            get { return m_returnedCheckChargeAmount; }
            set { m_returnedCheckChargeAmount = value; }
        }
        public string StopPaymentChargeAmount 
        {
            get { return m_stopPaymentChargeAmount; }
            set { m_stopPaymentChargeAmount = value; }
        }
        public string TeaserTermEndDate 
        {
            get { return m_teaserTermEndDate; }
            set { m_teaserTermEndDate = value; }
        }
        public string TeaserTermMonthsCount 
        {
            get { return m_teaserTermMonthsCount; }
            set { m_teaserTermMonthsCount = value; }
        }
        public string TerminationFeeAmount 
        {
            get { return m_terminationFeeAmount; }
            set { m_terminationFeeAmount = value; }
        }
        public string TerminationPeriodMonthsCount 
        {
            get { return m_terminationPeriodMonthsCount; }
            set { m_terminationPeriodMonthsCount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("HELOC");
            WriteAttribute(writer, "_AnnualFeeAmount", m_annualFeeAmount);
            WriteAttribute(writer, "_CreditCardAccountIdentifier", m_creditCardAccountIdentifier);
            WriteAttribute(writer, "_CreditCardIndicator", m_creditCardIndicator);

            if (m_dailyPeriodicInterestRateCalculationType != E_HelocDailyPeriodicInterestRateCalculationType.Undefined) 
            {
                string str = m_dailyPeriodicInterestRateCalculationType.ToString().Replace("_", "");
                WriteAttribute(writer, "_DailyPeriodicInterestRateCalculationType", str);
            }
            WriteAttribute(writer, "_DailyPeriodicInterestRatePercent", m_dailyPeriodicInterestRatePercent);
            WriteAttribute(writer, "_DrawPeriodMonthsCount", m_drawPeriodMonthsCount);
            WriteAttribute(writer, "_FirstLienBookNumber", m_firstLienBookNumber);
            WriteAttribute(writer, "_FirstLienDate", m_firstLienDate);
            WriteAttribute(writer, "_FirstLienHolderName", m_firstLienHolderName);
            WriteAttribute(writer, "_FirstLienIndicator", m_firstLienIndicator);
            WriteAttribute(writer, "_FirstLienInstrumentNumber", m_firstLienInstrumentNumber);
            WriteAttribute(writer, "_FirstLienPageNumber", m_firstLienPageNumber);
            WriteAttribute(writer, "_FirstLienPrincipalBalanceAmount", m_firstLienPrincipalBalanceAmount);
            WriteAttribute(writer, "_FirstLienRecordedDate", m_firstLienRecordedDate);
            WriteAttribute(writer, "_InitialAdvanceAmount", m_initialAdvanceAmount);
            WriteAttribute(writer, "_MaximumAPRRate", m_maximumAPRRate);
            WriteAttribute(writer, "_MinimumAdvanceAmount", m_minimumAdvanceAmount);
            WriteAttribute(writer, "_MinimumPaymentAmount", m_minimumPaymentAmount);
            WriteAttribute(writer, "_MinimumPaymentPercent", m_minimumPaymentPercent);
            WriteAttribute(writer, "_RepayPeriodMonthsCount", m_repayPeriodMonthsCount);
            WriteAttribute(writer, "_ReturnedCheckChargeAmount", m_returnedCheckChargeAmount);
            WriteAttribute(writer, "_StopPaymentChargeAmount", m_stopPaymentChargeAmount);
            WriteAttribute(writer, "_TeaserTermEndDate", m_teaserTermEndDate);
            WriteAttribute(writer, "_TeaserTermMonthsCount", m_teaserTermMonthsCount);
            WriteAttribute(writer, "_TerminationFeeAmount", m_terminationFeeAmount);
            WriteAttribute(writer, "_TerminationPeriodMonthsCount", m_terminationPeriodMonthsCount);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_HelocDailyPeriodicInterestRateCalculationType
    {
        Undefined = 0,
        _365,
        _360
    }
}
