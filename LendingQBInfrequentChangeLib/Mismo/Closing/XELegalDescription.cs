/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XELegalDescription : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_LEGAL_DESCRIPTION">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_TextDescription" type="xs:string"/>
        //          <xs:attribute name="_Type">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="MetesAndBounds"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_textDescription;
        private E_LegalDescriptionType m_type;
        private string m_typeOtherDescription;
        
        #endregion

        #region Public Properties
        public string TextDescription 
        {
            get { return m_textDescription; }
            set { m_textDescription = value; }
        }
        public E_LegalDescriptionType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_LEGAL_DESCRIPTION");
            WriteAttribute(writer, "_TextDescription", m_textDescription);
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_LegalDescriptionType 
    {
        Undefined = 0,
        Other,
        MetesAndBounds
    }
}
