/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEAmortizationSchedule : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="AMORTIZATION_SCHEDULE">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_EndingBalanceAmount" type="xs:string"/>
        //          <xs:attribute name="_InterestRatePercent" type="xs:string"/>
        //          <xs:attribute name="_LoanToValuePercent" type="xs:string"/>
        //          <xs:attribute name="_PaymentAmount" type="xs:string"/>
        //          <xs:attribute name="_PaymentNumber" type="xs:string"/>
        //          <xs:attribute name="_MIPaymentAmount" type="xs:string"/>
        //          <xs:attribute name="_PortionOfPaymentDistributedToInterestAmount" type="xs:string"/>
        //          <xs:attribute name="_PortionOfPaymentDistributedToPrincipalAmount" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_endingBalanceAmount;
        private string m_interestRatePercent;
        private string m_loanToValuePercent;
        private string m_paymentAmount;
        private string m_paymentNumber;
        private string m_miPaymentAmount;
        private string m_portionOfPaymentDistributedToInterestAmount;
        private string m_portionOfPaymentDistributedToPrincipalAmount;

        #endregion

        #region Public Properties
        public string EndingBalanceAmount 
        {
            get { return m_endingBalanceAmount; }
            set { m_endingBalanceAmount = value; }
        }
        public string InterestRatePercent 
        {
            get { return m_interestRatePercent; }
            set { m_interestRatePercent = value; }
        }
        public string LoanToValuePercent 
        {
            get { return m_loanToValuePercent; }
            set { m_loanToValuePercent = value; }
        }
        public string PaymentAmount 
        {
            get { return m_paymentAmount; }
            set { m_paymentAmount = value; }
        }
        public string PaymentNumber 
        {
            get { return m_paymentNumber; }
            set { m_paymentNumber = value; }
        }
        public string MiPaymentAmount 
        {
            get { return m_miPaymentAmount; }
            set { m_miPaymentAmount = value; }
        }
        public string PortionOfPaymentDistributedToInterestAmount 
        {
            get { return m_portionOfPaymentDistributedToInterestAmount; }
            set { m_portionOfPaymentDistributedToInterestAmount = value; }
        }
        public string PortionOfPaymentDistributedToPrincipalAmount 
        {
            get { return m_portionOfPaymentDistributedToPrincipalAmount; }
            set { m_portionOfPaymentDistributedToPrincipalAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("AMORTIZATION_SCHEDULE");
            WriteAttribute(writer, "_EndingBalanceAmount", m_endingBalanceAmount);
            WriteAttribute(writer, "_InterestRatePercent", m_interestRatePercent);
            WriteAttribute(writer, "_LoanToValuePercent", m_loanToValuePercent);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            WriteAttribute(writer, "_PaymentNumber", m_paymentNumber);
            WriteAttribute(writer, "_MIPaymentAmount", m_miPaymentAmount);
            WriteAttribute(writer, "_PortionOfPaymentDistributedToInterestAmount", m_portionOfPaymentDistributedToInterestAmount);
            WriteAttribute(writer, "_PortionOfPaymentDistributedToPrincipalAmount", m_portionOfPaymentDistributedToPrincipalAmount);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
