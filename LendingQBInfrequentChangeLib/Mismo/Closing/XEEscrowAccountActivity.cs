/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XEEscrowAccountActivity : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="ESCROW_ACCOUNT_ACTIVITY">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_CurrentBalanceAmount" type="xs:string"/>
        //          <xs:attribute name="_DisbursementMonth" type="xs:string"/>
        //          <xs:attribute name="_DisbursementSequenceIdentifier" type="xs:string"/>
        //          <xs:attribute name="_DisbursementYear" type="xs:string"/>
        //          <xs:attribute name="_PaymentDescriptionType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="HazardInsurance"/>
        //                <xs:enumeration value="Assessment"/>
        //                <xs:enumeration value="TownPropertyTax"/>
        //                <xs:enumeration value="EarthquakeInsurance"/>
        //                <xs:enumeration value="Windstorm"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="VillagePropertyTax"/>
        //                <xs:enumeration value="CityPropertyTax"/>
        //                <xs:enumeration value="MortgageInsurance"/>
        //                <xs:enumeration value="FloodInsurance"/>
        //                <xs:enumeration value="CountyPropertyTax"/>
        //                <xs:enumeration value="SchoolPropertyTax"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_PaymentFromEscrowAccountAmount" type="xs:string"/>
        //          <xs:attribute name="_PaymentToEscrowAccountAmount" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_currentBalanceAmount;
        private string m_disbursementMonth;
        private string m_disbursementSequenceIdentifier;
        private string m_disbursementYear;
        private E_EscrowAccountActivityPaymentDescriptionType m_paymentDescriptionType;
        private string m_paymentFromEscrowAccountAmount;
        private string m_paymentToEscrowAccountAmount;
        
        #endregion

        #region Public Properties
        public string CurrentBalanceAmount 
        {
            get { return m_currentBalanceAmount; }
            set { m_currentBalanceAmount = value; }
        }
        public string DisbursementMonth 
        {
            get { return m_disbursementMonth; }
            set { m_disbursementMonth = value; }
        }
        public string DisbursementSequenceIdentifier 
        {
            get { return m_disbursementSequenceIdentifier; }
            set { m_disbursementSequenceIdentifier = value; }
        }
        public string DisbursementYear 
        {
            get { return m_disbursementYear; }
            set { m_disbursementYear = value; }
        }
        public E_EscrowAccountActivityPaymentDescriptionType PaymentDescriptionType 
        {
            get { return m_paymentDescriptionType; }
            set { m_paymentDescriptionType = value; }
        }
        public string PaymentFromEscrowAccountAmount 
        {
            get { return m_paymentFromEscrowAccountAmount; }
            set { m_paymentFromEscrowAccountAmount = value; }
        }
        public string PaymentToEscrowAccountAmount 
        {
            get { return m_paymentToEscrowAccountAmount; }
            set { m_paymentToEscrowAccountAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ESCROW_ACCOUNT_ACTIVITY");
            WriteAttribute(writer, "_CurrentBalanceAmount", m_currentBalanceAmount);
            WriteAttribute(writer, "_DisbursementMonth", m_disbursementMonth);
            WriteAttribute(writer, "_DisbursementSequenceIdentifier", m_disbursementSequenceIdentifier);
            WriteAttribute(writer, "_DisbursementYear", m_disbursementYear);
            WriteAttribute(writer, "_PaymentDescriptionType", m_paymentDescriptionType);
            WriteAttribute(writer, "_PaymentFromEscrowAccountAmount", m_paymentFromEscrowAccountAmount);
            WriteAttribute(writer, "_PaymentToEscrowAccountAmount", m_paymentToEscrowAccountAmount);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_EscrowAccountActivityPaymentDescriptionType 
    {
        Undefined = 0,
        HazardInsurance,
        Assessment,
        TownPropertyTax,
        EarthquakeInsurance,
        Windstorm,
        Other,
        VillagePropertyTax,
        CityPropertyTax,
        MortgageInsurance,
        FloodInsurance,
        CountyPropertyTax,
        SchoolPropertyTax

    }
}
