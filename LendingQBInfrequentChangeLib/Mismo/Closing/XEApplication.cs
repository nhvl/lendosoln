/// Author: David Dao

using System;
using System.Collections;
using System.Xml;

namespace Mismo.Closing
{
	public class XEApplication : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_APPLICATION">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_DATA_INFORMATION" minOccurs="0"/>
        //        <xs:element ref="ADDITIONAL_CASE_DATA" minOccurs="0"/>
        //        <xs:element ref="AFFORDABLE_LENDING" minOccurs="0"/>
        //        <xs:element ref="ASSET" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="DOWN_PAYMENT" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="ESCROW" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="ESCROW_ACCOUNT_SUMMARY" minOccurs="0"/>
        //        <xs:element ref="GOVERNMENT_LOAN" minOccurs="0"/>
        //        <xs:element ref="GOVERNMENT_REPORTING" minOccurs="0"/>
        //        <xs:element ref="INTERVIEWER_INFORMATION" minOccurs="0"/>
        //        <xs:element ref="LIABILITY" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="LOAN_PRODUCT_DATA" minOccurs="0"/>
        //        <xs:element ref="LOAN_PURPOSE" minOccurs="0"/>
        //        <xs:element ref="LOAN_QUALIFICATION" minOccurs="0"/>
        //        <xs:element ref="MERS" minOccurs="0"/>
        //        <xs:element ref="MI_DATA" minOccurs="0"/>
        //        <xs:element ref="MORTGAGE_TERMS" minOccurs="0"/>
        //        <xs:element ref="PROPERTY" minOccurs="0"/>
        //        <xs:element ref="PROPOSED_HOUSING_EXPENSE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="REO_PROPERTY" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RESPA_FEE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="TITLE_HOLDER" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="TRANSACTION_DETAIL" minOccurs="0"/>
        //        <xs:element ref="BORROWER" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private Mismo.MismoXmlElement.XEDataInformation m_dataInformation;
        private Mismo.MismoXmlElement.XEAdditionalCaseData m_additionalCaseData;
        private Mismo.MismoXmlElement.XEAffordableLending m_affordableLending;
        private ArrayList m_assetList = new ArrayList();
        private ArrayList m_downPaymentList = new ArrayList();
        private ArrayList m_escrowList = new ArrayList();
        private XEEscrowAccountSummary m_escrowAccountSummary;
        private Mismo.MismoXmlElement.XEGovernmentLoan m_governmentLoan;
        private Mismo.MismoXmlElement.XEGovernmentReporting m_governmentReporting;
        private Mismo.MismoXmlElement.XEInterviewerInformation m_interviewerInformation;
        private ArrayList m_liabilityList = new ArrayList();
        private XELoanProductData m_loanProductData;
        private Mismo.MismoXmlElement.XELoanPurpose m_loanPurpose;
        private Mismo.MismoXmlElement.XELoanQualification m_loanQualification;
        private XEMers m_mers;
        private XEMiData  m_miData;
        private XEMortgageTerms m_mortgageTerms;
        private XEProperty m_property;
        private ArrayList m_proposedHousingExpenseList = new ArrayList();
        private ArrayList m_reoPropertyList = new ArrayList();
        private ArrayList m_respaFeeList = new ArrayList();
        private ArrayList m_titleHolderList = new ArrayList();
        private Mismo.MismoXmlElement.XETransactionDetail m_transactionDetail;
        private ArrayList m_borrowerList = new ArrayList();
        #endregion

        #region Public Properties
        public void SetDataInformation(Mismo.MismoXmlElement.XEDataInformation dataInformation) 
        {
            m_dataInformation = dataInformation;
        }
        public void SetAdditionalCaseData(Mismo.MismoXmlElement.XEAdditionalCaseData additionalCaseData) 
        {
            m_additionalCaseData = additionalCaseData;
        }
        public void SetAffordableLending(Mismo.MismoXmlElement.XEAffordableLending affordableLending) 
        {
            m_affordableLending = affordableLending;
        }
        public void AddAsset(Mismo.MismoXmlElement.XEAsset asset) 
        {
            m_assetList.Add(asset);
        }
        public void AddDownPayment(Mismo.MismoXmlElement.XEDownPayment downPayment) 
        {
            m_downPaymentList.Add(downPayment);
        }
        public void AddEscrow(XEEscrow escrow) 
        {
            m_escrowList.Add(escrow);
        }
        public void SetEscrowAccountSummary(XEEscrowAccountSummary escrowAccountSummary) 
        {
            m_escrowAccountSummary = escrowAccountSummary;
        }
        public void SetGovernmentLoan(Mismo.MismoXmlElement.XEGovernmentLoan governmentLoan) 
        {
            m_governmentLoan = governmentLoan;
        }
        public void SetGovernmentReporting(Mismo.MismoXmlElement.XEGovernmentReporting governmentReporting) 
        {
            m_governmentReporting = governmentReporting;
        }
        public void SetInterviewerInformation(Mismo.MismoXmlElement.XEInterviewerInformation interviewerInformation) 
        {
            m_interviewerInformation = interviewerInformation;
        }
        public void AddLiability(Mismo.MismoXmlElement.XELiability liability) 
        {
            m_liabilityList.Add(liability);
        }
        public void SetLoanProductData(XELoanProductData loanProductData) 
        {
            m_loanProductData = loanProductData;
        }
        public void SetLoanPurpose(Mismo.MismoXmlElement.XELoanPurpose loanPurpose) 
        {
            m_loanPurpose = loanPurpose;
        }
        public void SetLoanQualification(Mismo.MismoXmlElement.XELoanQualification loanQualification) 
        {
            m_loanQualification = loanQualification;
        }
        public void SetMers(XEMers mers) 
        {
            m_mers = mers;
        }
        public void SetMiData(XEMiData  miData) 
        {
            m_miData = miData;
        }
        public void SetMortgageTerms(XEMortgageTerms mortgageTerms) 
        {
            m_mortgageTerms = mortgageTerms;
        }
        public void SetProperty(XEProperty property) 
        {
            m_property = property;
        }
        public void AddProposedHousingExpense(Mismo.MismoXmlElement.XEProposedHousingExpense proposedHousingExpense) 
        {
            m_proposedHousingExpenseList.Add(proposedHousingExpense);
        }
        public void AddReoProperty(Mismo.MismoXmlElement.XEReoProperty reoProperty) 
        {
            m_reoPropertyList.Add(reoProperty);
        }
        public void AddRespaFee(XERespaFee respaFee) 
        {
            m_respaFeeList.Add(respaFee);
        }
        public void AddTitleHolder(Mismo.MismoXmlElement.XETitleHolder titleHolder) 
        {
            m_titleHolderList.Add(titleHolder);
        }
        public void SetTransactionDetail(Mismo.MismoXmlElement.XETransactionDetail transactionDetail) 
        {
            m_transactionDetail = transactionDetail;
        }
        public void AddBorrower(XEBorrower borrower) 
        {
            m_borrowerList.Add(borrower);
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_APPLICATION");
            WriteElement(writer, m_dataInformation);
            WriteElement(writer, m_additionalCaseData);
            WriteElement(writer, m_affordableLending);
            WriteElement(writer, m_assetList);
            WriteElement(writer, m_downPaymentList);
            WriteElement(writer, m_escrowList);
            WriteElement(writer, m_escrowAccountSummary);
            WriteElement(writer, m_governmentLoan);
            WriteElement(writer, m_governmentReporting);
            WriteElement(writer, m_interviewerInformation);
            WriteElement(writer, m_liabilityList);
            WriteElement(writer, m_loanProductData);
            WriteElement(writer, m_loanPurpose);
            WriteElement(writer, m_loanQualification);
            WriteElement(writer, m_mers);
            WriteElement(writer, m_miData);
            WriteElement(writer, m_mortgageTerms);
            WriteElement(writer, m_property);
            WriteElement(writer, m_proposedHousingExpenseList);
            WriteElement(writer, m_reoPropertyList);
            WriteElement(writer, m_respaFeeList);
            WriteElement(writer, m_titleHolderList);
            WriteElement(writer, m_transactionDetail);
            WriteElement(writer, m_borrowerList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
