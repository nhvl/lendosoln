/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEInterestOnly : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="INTEREST_ONLY">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_MonthlyPaymentAmount" type="xs:string"/>
        //          <xs:attribute name="_TermMonthsCount" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_monthlyPaymentAmount;
        private string m_termMonthsCount;
        #endregion

        #region Public Properties
        public string MonthlyPaymentAmount 
        {
            get { return m_monthlyPaymentAmount; }
            set { m_monthlyPaymentAmount = value; }
        }
        public string TermMonthsCount 
        {
            get { return m_termMonthsCount; }
            set { m_termMonthsCount = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("INTEREST_ONLY");
            WriteAttribute(writer, "_MonthlyPaymentAmount", m_monthlyPaymentAmount);
            WriteAttribute(writer, "_TermMonthsCount", m_termMonthsCount);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
