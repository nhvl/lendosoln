/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
    public class XEMers : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="MERS">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="MERS_MINNumber" type="xs:string"/>
        //          <xs:attribute name="MERSOriginalMortgageeOfRecordIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="MERSMortgageeOfRecordIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="MERSRegistrationDate" type="xs:string"/>
        //          <xs:attribute name="MERSRegistrationIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="MERSRegistrationStatusType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="NotRegistered"/>
        //                <xs:enumeration value="Other"/>
        //                <xs:enumeration value="Registered"/>
        //                <xs:enumeration value="Deactivated"/>
        //                <xs:enumeration value="Validated"/>
        //                <xs:enumeration value="PreRegistered"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="MERSRegistrationStatusTypeOtherDescription" type="xs:string"/>
        //          <xs:attribute name="MERSTaxNumberIdentifier" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private string m_mersMinNumber;
        private E_YesNoIndicator m_mersOriginalMortgageeOfRecordIndicator;
        private E_YesNoIndicator m_mersMortgageeOfRecordIndicator;
        private string m_mersRegistrationDate;
        private E_YesNoIndicator m_mersRegistrationIndicator;
        private E_MersRegistrationStatusType m_mersRegistrationStatusType;
        private string m_mersRegistrationStatusTypeOtherDescription;
        private string m_mersTaxNumberIdentifier;
        #endregion

        #region Public Properties
        public string MersMinNumber 
        {
            get { return m_mersMinNumber; }
            set { m_mersMinNumber = value; }
        }
        public E_YesNoIndicator MersOriginalMortgageeOfRecordIndicator 
        {
            get { return m_mersOriginalMortgageeOfRecordIndicator; }
            set { m_mersOriginalMortgageeOfRecordIndicator = value; }
        }
        public E_YesNoIndicator MersMortgageeOfRecordIndicator 
        {
            get { return m_mersMortgageeOfRecordIndicator; }
            set { m_mersMortgageeOfRecordIndicator = value; }
        }
        public string MersRegistrationDate 
        {
            get { return m_mersRegistrationDate; }
            set { m_mersRegistrationDate = value; }
        }
        public E_YesNoIndicator MersRegistrationIndicator 
        {
            get { return m_mersRegistrationIndicator; }
            set { m_mersRegistrationIndicator = value; }
        }
        public E_MersRegistrationStatusType MersRegistrationStatusType 
        {
            get { return m_mersRegistrationStatusType; }
            set { m_mersRegistrationStatusType = value; }
        }
        public string MersRegistrationStatusTypeOtherDescription 
        {
            get { return m_mersRegistrationStatusTypeOtherDescription; }
            set { m_mersRegistrationStatusTypeOtherDescription = value; }
        }
        public string MersTaxNumberIdentifier 
        {
            get { return m_mersTaxNumberIdentifier; }
            set { m_mersTaxNumberIdentifier = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MERS");
            WriteAttribute(writer, "MERS_MINNumber", m_mersMinNumber);
            WriteAttribute(writer, "MERSOriginalMortgageeOfRecordIndicator", m_mersOriginalMortgageeOfRecordIndicator);
            WriteAttribute(writer, "MERSMortgageeOfRecordIndicator", m_mersMortgageeOfRecordIndicator);
            WriteAttribute(writer, "MERSRegistrationDate", m_mersRegistrationDate);
            WriteAttribute(writer, "MERSRegistrationIndicator", m_mersRegistrationIndicator);
            WriteAttribute(writer, "MERSRegistrationStatusType", m_mersRegistrationStatusType);
            WriteAttribute(writer, "MERSRegistrationStatusTypeOtherDescription", m_mersRegistrationStatusTypeOtherDescription);
            WriteAttribute(writer, "MERSTaxNumberIdentifier", m_mersTaxNumberIdentifier);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_MersRegistrationStatusType 
    {
        Undefined = 0,
        NotRegistered,
        Other,
        Registered,
        Deactivated,
        Validated,
        PreRegistered
    }
}
