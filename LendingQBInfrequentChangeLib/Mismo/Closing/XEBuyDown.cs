/// Author: David Dao

using System;
using System.Xml;
using System.Collections;
using Mismo.Common;

namespace Mismo.Closing
{
    public class XEBuyDown : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="BUYDOWN">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_CONTRIBUTOR" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="_SUBSIDY_SCHEDULE" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_BaseDateType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="NoteDate"/>
        //            <xs:enumeration value="LastPaymentDate"/>
        //            <xs:enumeration value="FirstPaymentDate"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ChangeFrequencyMonths" type="xs:string"/>
        //      <xs:attribute name="_ContributorType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="UnrelatedFriend"/>
        //            <xs:enumeration value="LenderPremiumFinanced"/>
        //            <xs:enumeration value="NonparentRelative"/>
        //            <xs:enumeration value="Unassigned"/>
        //            <xs:enumeration value="Employer"/>
        //            <xs:enumeration value="Borrower"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Parent"/>
        //            <xs:enumeration value="Seller"/>
        //            <xs:enumeration value="Builder"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_ContributorTypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="_DurationMonths" type="xs:string"/>
        //      <xs:attribute name="_IncreaseRatePercent" type="xs:string"/>
        //      <xs:attribute name="_LenderFundingIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_OriginalBalanceAmount" type="xs:string"/>
        //      <xs:attribute name="_PermanentIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_SubsidyCalculationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="DecliningLoanBalance"/>
        //            <xs:enumeration value="OriginalLoanAmount"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_TotalSubsidyAmount" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private ArrayList m_contributorList = new ArrayList();
        private ArrayList m_subsidyScheduleList = new ArrayList();
        private E_BuyDownBaseDateType m_baseDateType;
        private string m_changeFrequencyMonths;
        private E_BuyDownContributorType m_contributorType;
        private string m_contributorTypeOtherDescription;
        private string m_durationMonths;
        private string m_increaseRatePercent;
        private E_YesNoIndicator m_lenderFundingIndicator;
        private string m_originalBalanceAmount;
        private E_YesNoIndicator m_permanentIndicator;
        private E_BuyDownSubsidyCalculationType m_subsidyCalculationType;
        private string m_totalSubsidyAmount;
        #endregion

        #region Public Properties
        public void AddContributor(XEContributor contributor) 
        {
            m_contributorList.Add(contributor);
        }
        public void AddSubsidySchedule(XESubsidySchedule subsidySchedule) 
        {
            m_subsidyScheduleList.Add(subsidySchedule);
        }
        public E_BuyDownBaseDateType BaseDateType 
        {
            get { return m_baseDateType; }
            set { m_baseDateType = value; }
        }
        public string ChangeFrequencyMonths 
        {
            get { return m_changeFrequencyMonths; }
            set { m_changeFrequencyMonths = value; }
        }
        public E_BuyDownContributorType ContributorType 
        {
            get { return m_contributorType; }
            set { m_contributorType = value; }
        }
        public string ContributorTypeOtherDescription 
        {
            get { return m_contributorTypeOtherDescription; }
            set { m_contributorTypeOtherDescription = value; }
        }
        public string DurationMonths 
        {
            get { return m_durationMonths; }
            set { m_durationMonths = value; }
        }
        public string IncreaseRatePercent 
        {
            get { return m_increaseRatePercent; }
            set { m_increaseRatePercent = value; }
        }
        public E_YesNoIndicator LenderFundingIndicator 
        {
            get { return m_lenderFundingIndicator; }
            set { m_lenderFundingIndicator = value; }
        }
        public string OriginalBalanceAmount 
        {
            get { return m_originalBalanceAmount; }
            set { m_originalBalanceAmount = value; }
        }
        public E_YesNoIndicator PermanentIndicator 
        {
            get { return m_permanentIndicator; }
            set { m_permanentIndicator = value; }
        }
        public E_BuyDownSubsidyCalculationType SubsidyCalculationType 
        {
            get { return m_subsidyCalculationType; }
            set { m_subsidyCalculationType = value; }
        }
        public string TotalSubsidyAmount 
        {
            get { return m_totalSubsidyAmount; }
            set { m_totalSubsidyAmount = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("BUYDOWN");
            WriteAttribute(writer, "_BaseDateType", m_baseDateType);
            WriteAttribute(writer, "_ChangeFrequencyMonths", m_changeFrequencyMonths);
            WriteAttribute(writer, "_ContributorType", m_contributorType);
            WriteAttribute(writer, "_ContributorTypeOtherDescription", m_contributorTypeOtherDescription);
            WriteAttribute(writer, "_DurationMonths", m_durationMonths);
            WriteAttribute(writer, "_IncreaseRatePercent", m_increaseRatePercent);
            WriteAttribute(writer, "_LenderFundingIndicator", m_lenderFundingIndicator);
            WriteAttribute(writer, "_OriginalBalanceAmount", m_originalBalanceAmount);
            WriteAttribute(writer, "_PermanentIndicator", m_permanentIndicator);
            WriteAttribute(writer, "_SubsidyCalculationType", m_subsidyCalculationType);
            WriteAttribute(writer, "_TotalSubsidyAmount", m_totalSubsidyAmount);
            WriteElement(writer, m_contributorList);
            WriteElement(writer, m_subsidyScheduleList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_BuyDownBaseDateType 
    {
        Undefined = 0,
        NoteDate,
        LastPaymentDate,
        FirstPaymentDate

    }
    public enum E_BuyDownContributorType 
    {
        Undefined = 0,
        UnrelatedFriend,
        LenderPremiumFinanced,
        NonparentRelative,
        Unassigned,
        Employer,
        Borrower,
        Other,
        Parent,
        Seller,
        Builder

    }
    public enum E_BuyDownSubsidyCalculationType 
    {
        Undefined = 0,
        DecliningLoanBalance,
        OriginalLoanAmount

    }
}
