/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEAllongeToNote : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="ALLONGE_TO_NOTE">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="AUTHORIZED_REPRESENTATIVE" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_Date" type="xs:string"/>
        //      <xs:attribute name="_ExecutedByDescription" type="xs:string"/>
        //      <xs:attribute name="_InFavorOfDescription" type="xs:string"/>
        //      <xs:attribute name="_PayToTheOrderOfDescription" type="xs:string"/>
        //      <xs:attribute name="_WithoutRecourseDescription" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private XEAuthorizedRepresentative m_authorizedRepresentative;
        private string m_date;
        private string m_executedByDescription;
        private string m_inFavorOfDescription;
        private string m_payToTheOrderOfDescription;
        private string m_withoutRecourseDescription;
        
        #endregion

        #region Public Properties
        public void SetAuthorizedRepresentative(XEAuthorizedRepresentative authorizedRepresentative) 
        {
            m_authorizedRepresentative = authorizedRepresentative;
        }
        public string Date 
        {
            get { return m_date; }
            set { m_date = value; }
        }
        public string ExecutedByDescription 
        {
            get { return m_executedByDescription; }
            set { m_executedByDescription = value; }
        }
        public string InFavorOfDescription 
        {
            get { return m_inFavorOfDescription; }
            set { m_inFavorOfDescription = value; }
        }
        public string PayToTheOrderOfDescription 
        {
            get { return m_payToTheOrderOfDescription; }
            set { m_payToTheOrderOfDescription = value; }
        }
        public string WithoutRecourseDescription 
        {
            get { return m_withoutRecourseDescription; }
            set { m_withoutRecourseDescription = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ALLONGE_TO_NOTE");
            WriteAttribute(writer, "_Date", m_date);
            WriteAttribute(writer, "_ExecutedByDescription", m_executedByDescription);
            WriteAttribute(writer, "_InFavorOfDescription", m_inFavorOfDescription);
            WriteAttribute(writer, "_PayToTheOrderOfDescription", m_payToTheOrderOfDescription);
            WriteAttribute(writer, "_WithoutRecourseDescription", m_withoutRecourseDescription);

            WriteElement(writer, m_authorizedRepresentative);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
