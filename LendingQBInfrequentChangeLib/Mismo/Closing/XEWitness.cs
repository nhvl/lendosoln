/// Author: David Dao

using System;
using System.Xml;
namespace Mismo.Closing
{
	public class XEWitness : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="WITNESS">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //          <xs:attribute name="_UnparsedName" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_sequenceIdentifier;
        private string m_unparsedName;

        #endregion

        #region Public Properties
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }
        public string UnparsedName 
        {
            get { return m_unparsedName; }
            set { m_unparsedName = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("WITNESS");
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);
            WriteAttribute(writer, "_UnparsedName", m_unparsedName);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
