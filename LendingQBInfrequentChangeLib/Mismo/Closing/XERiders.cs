/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;
namespace Mismo.Closing
{
    public class XERiders : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="RIDERS">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="AdjustableRateRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="BalloonRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="BiweeklyPaymentRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="CondominiumRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="GraduatedPaymentRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="GrowingEquityRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="NonOwnerOccupancyRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="OneToFourFamilyRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="OtherRiderDescription" type="xs:string"/>
        //          <xs:attribute name="OtherRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="PlannedUnitDevelopmentRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="RateImprovementRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="RehabilitationLoanRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="SecondHomeRiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="VARiderIndicator">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Y"/>
        //                <xs:enumeration value="N"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private E_YesNoIndicator m_adjustableRateRiderIndicator;
        private E_YesNoIndicator m_balloonRiderIndicator;
        private E_YesNoIndicator m_biweeklyPaymentRiderIndicator;
        private E_YesNoIndicator m_condominiumRiderIndicator;
        private E_YesNoIndicator m_graduatedPaymentRiderIndicator;
        private E_YesNoIndicator m_growingEquityRiderIndicator;
        private E_YesNoIndicator m_nonOwnerOccupancyRiderIndicator;
        private E_YesNoIndicator m_oneToFourFamilyRiderIndicator;
        private string m_otherRiderDescription;
        private E_YesNoIndicator m_otherRiderIndicator;
        private E_YesNoIndicator m_plannedUnitDevelopmentRiderIndicator;
        private E_YesNoIndicator m_rateImprovementRiderIndicator;
        private E_YesNoIndicator m_rehabilitationLoanRiderIndicator;
        private E_YesNoIndicator m_secondHomeRiderIndicator;
        private E_YesNoIndicator m_vaRiderIndicator;

        #endregion

        #region Public Properties
        public E_YesNoIndicator AdjustableRateRiderIndicator 
        {
            get { return m_adjustableRateRiderIndicator; }
            set { m_adjustableRateRiderIndicator = value; }
        }
        public E_YesNoIndicator BalloonRiderIndicator 
        {
            get { return m_balloonRiderIndicator; }
            set { m_balloonRiderIndicator = value; }
        }
        public E_YesNoIndicator BiweeklyPaymentRiderIndicator 
        {
            get { return m_biweeklyPaymentRiderIndicator; }
            set { m_biweeklyPaymentRiderIndicator = value; }
        }
        public E_YesNoIndicator CondominiumRiderIndicator 
        {
            get { return m_condominiumRiderIndicator; }
            set { m_condominiumRiderIndicator = value; }
        }
        public E_YesNoIndicator GraduatedPaymentRiderIndicator 
        {
            get { return m_graduatedPaymentRiderIndicator; }
            set { m_graduatedPaymentRiderIndicator = value; }
        }
        public E_YesNoIndicator GrowingEquityRiderIndicator 
        {
            get { return m_growingEquityRiderIndicator; }
            set { m_growingEquityRiderIndicator = value; }
        }
        public E_YesNoIndicator NonOwnerOccupancyRiderIndicator 
        {
            get { return m_nonOwnerOccupancyRiderIndicator; }
            set { m_nonOwnerOccupancyRiderIndicator = value; }
        }
        public E_YesNoIndicator OneToFourFamilyRiderIndicator 
        {
            get { return m_oneToFourFamilyRiderIndicator; }
            set { m_oneToFourFamilyRiderIndicator = value; }
        }
        public string OtherRiderDescription 
        {
            get { return m_otherRiderDescription; }
            set { m_otherRiderDescription = value; }
        }
        public E_YesNoIndicator OtherRiderIndicator 
        {
            get { return m_otherRiderIndicator; }
            set { m_otherRiderIndicator = value; }
        }
        public E_YesNoIndicator PlannedUnitDevelopmentRiderIndicator 
        {
            get { return m_plannedUnitDevelopmentRiderIndicator; }
            set { m_plannedUnitDevelopmentRiderIndicator = value; }
        }
        public E_YesNoIndicator RateImprovementRiderIndicator 
        {
            get { return m_rateImprovementRiderIndicator; }
            set { m_rateImprovementRiderIndicator = value; }
        }
        public E_YesNoIndicator RehabilitationLoanRiderIndicator 
        {
            get { return m_rehabilitationLoanRiderIndicator; }
            set { m_rehabilitationLoanRiderIndicator = value; }
        }
        public E_YesNoIndicator SecondHomeRiderIndicator 
        {
            get { return m_secondHomeRiderIndicator; }
            set { m_secondHomeRiderIndicator = value; }
        }
        public E_YesNoIndicator VaRiderIndicator 
        {
            get { return m_vaRiderIndicator; }
            set { m_vaRiderIndicator = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RIDERS");
            WriteAttribute(writer, "AdjustableRateRiderIndicator", m_adjustableRateRiderIndicator);
            WriteAttribute(writer, "BalloonRiderIndicator", m_balloonRiderIndicator);
            WriteAttribute(writer, "BiweeklyPaymentRiderIndicator", m_biweeklyPaymentRiderIndicator);
            WriteAttribute(writer, "CondominiumRiderIndicator", m_condominiumRiderIndicator);
            WriteAttribute(writer, "GraduatedPaymentRiderIndicator", m_graduatedPaymentRiderIndicator);
            WriteAttribute(writer, "GrowingEquityRiderIndicator", m_growingEquityRiderIndicator);
            WriteAttribute(writer, "NonOwnerOccupancyRiderIndicator", m_nonOwnerOccupancyRiderIndicator);
            WriteAttribute(writer, "OneToFourFamilyRiderIndicator", m_oneToFourFamilyRiderIndicator);
            WriteAttribute(writer, "OtherRiderDescription", m_otherRiderDescription);
            WriteAttribute(writer, "OtherRiderIndicator", m_otherRiderIndicator);
            WriteAttribute(writer, "PlannedUnitDevelopmentRiderIndicator", m_plannedUnitDevelopmentRiderIndicator);
            WriteAttribute(writer, "RateImprovementRiderIndicator", m_rateImprovementRiderIndicator);
            WriteAttribute(writer, "RehabilitationLoanRiderIndicator", m_rehabilitationLoanRiderIndicator);
            WriteAttribute(writer, "SecondHomeRiderIndicator", m_secondHomeRiderIndicator);
            WriteAttribute(writer, "VARiderIndicator", m_vaRiderIndicator);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
