/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEMiRenewalPremium : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="MI_RENEWAL_PREMIUM">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_MonthlyPaymentAmount" type="xs:string"/>
        //          <xs:attribute name="_MonthlyPaymentRoundingType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Down"/>
        //                <xs:enumeration value="None"/>
        //                <xs:enumeration value="Up"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_Rate" type="xs:string"/>
        //          <xs:attribute name="_RateDurationMonths" type="xs:string"/>
        //          <xs:attribute name="_Sequence">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="Second"/>
        //                <xs:enumeration value="Third"/>
        //                <xs:enumeration value="Fourth"/>
        //                <xs:enumeration value="First"/>
        //                <xs:enumeration value="Fifth"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_monthlyPaymentAmount;
        private E_MiRenewalPremiumMonthlyPaymentRoundingType m_monthlyPaymentRoundingType;
        private string m_rate;
        private string m_rateDurationMonths;
        private E_MiRenewalPremiumSequence m_sequence;
        #endregion

        #region Public Properties
        public string MonthlyPaymentAmount 
        {
            get { return m_monthlyPaymentAmount; }
            set { m_monthlyPaymentAmount = value; }
        }
        public E_MiRenewalPremiumMonthlyPaymentRoundingType MonthlyPaymentRoundingType 
        {
            get { return m_monthlyPaymentRoundingType; }
            set { m_monthlyPaymentRoundingType = value; }
        }
        public string Rate 
        {
            get { return m_rate; }
            set { m_rate = value; }
        }
        public string RateDurationMonths 
        {
            get { return m_rateDurationMonths; }
            set { m_rateDurationMonths = value; }
        }
        public E_MiRenewalPremiumSequence Sequence 
        {
            get { return m_sequence; }
            set { m_sequence = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("MI_RENEWAL_PREMIUM");
            WriteAttribute(writer, "_MonthlyPaymentAmount", m_monthlyPaymentAmount);
            WriteAttribute(writer, "_MonthlyPaymentRoundingType", m_monthlyPaymentRoundingType);
            WriteAttribute(writer, "_Rate", m_rate);
            WriteAttribute(writer, "_RateDurationMonths", m_rateDurationMonths);
            WriteAttribute(writer, "_Sequence", m_sequence);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_MiRenewalPremiumMonthlyPaymentRoundingType 
    {
        Undefined = 0,
        Down,
        None,
        Up
    }
    public enum E_MiRenewalPremiumSequence 
    {
        Undefined = 0,
        Second,
        Third,
        Fourth,
        First,
        Fifth
    }
}
