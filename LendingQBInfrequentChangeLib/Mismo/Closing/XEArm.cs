/// Author: David Dao

using System;
using System.Xml;
using Mismo.Common;

namespace Mismo.Closing
{
	public class XEArm : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="ARM">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="_CONVERSION_OPTION" minOccurs="0"/>
        //      </xs:sequence>
        //      <xs:attribute name="_IndexCurrentValuePercent" type="xs:string"/>
        //      <xs:attribute name="_IndexMarginPercent" type="xs:string"/>
        //      <xs:attribute name="_IndexType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="WeeklyAverageConstantMaturingTreasury"/>
        //            <xs:enumeration value="SixMonthTreasury"/>
        //            <xs:enumeration value="FRE_LIBOR"/>
        //            <xs:enumeration value="FederalCostOfFunds"/>
        //            <xs:enumeration value="WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield"/>
        //            <xs:enumeration value="NationalAverageContractRateFHLBB"/>
        //            <xs:enumeration value="LIBOR"/>
        //            <xs:enumeration value="FRE60DayRequiredNetYield"/>
        //            <xs:enumeration value="OneYearTreasury"/>
        //            <xs:enumeration value="TreasuryBillDailyValue"/>
        //            <xs:enumeration value="MonthlyAverageConstantMaturingTreasury"/>
        //            <xs:enumeration value="ThreeYearTreasury"/>
        //            <xs:enumeration value="WeeklyAveragePrimeRate"/>
        //            <xs:enumeration value="FNM_LIBOR"/>
        //            <xs:enumeration value="WeeklyAverageTreasuryAuctionAverageInvestmentYield"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="DailyCertificateOfDepositRate"/>
        //            <xs:enumeration value="WeeklyAverageTreasuryAuctionAverageBondDiscountYield"/>
        //            <xs:enumeration value="WallStreetJournalLIBOR"/>
        //            <xs:enumeration value="WeeklyAverageCertificateOfDepositRate"/>
        //            <xs:enumeration value="EleventhDistrictCostOfFunds"/>
        //            <xs:enumeration value="FNM60DayRequiredNetYield"/>
        //            <xs:enumeration value="NationalMonthlyMedianCostOfFunds"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_QualifyingRatePercent" type="xs:string"/>
        //      <xs:attribute name="_ConversionOptionIndicator">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Y"/>
        //            <xs:enumeration value="N"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="PaymentAdjustmentLifetimeCapAmount" type="xs:string"/>
        //      <xs:attribute name="PaymentAdjustmentLifetimeCapPercent" type="xs:string"/>
        //      <xs:attribute name="RateAdjustmentLifetimeCapPercent" type="xs:string"/>
        //      <xs:attribute name="_LifetimeCapRate" type="xs:string"/>
        //      <xs:attribute name="_LifetimeFloorPercent" type="xs:string"/>
        //      <xs:attribute name="_InterestRateRoundingFactor" type="xs:string"/>
        //      <xs:attribute name="_InterestRateRoundingType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="Down"/>
        //            <xs:enumeration value="None"/>
        //            <xs:enumeration value="Up"/>
        //            <xs:enumeration value="Nearest"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="FNMTreasuryYieldForCurrentIndexDivisorNumber" type="xs:string"/>
        //      <xs:attribute name="FNMTreasuryYieldForIndexDivisorNumber" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>
        
        #endregion

        #region Private Member Variables
        private XEConversionOption m_conversionOption;
        private string m_indexCurrentValuePercent;
        private string m_indexMarginPercent;
        private E_ArmIndexType m_indexType;
        private string m_qualifyingRatePercent;
        private E_YesNoIndicator m_conversionOptionIndicator;
        private string m_paymentAdjustmentLifetimeCapAmount;
        private string m_paymentAdjustmentLifetimeCapPercent;
        private string m_rateAdjustmentLifetimeCapPercent;
        private string m_lifetimeCapRate;
        private string m_lifetimeFloorPercent;
        private string m_interestRateRoundingFactor;
        private E_ArmInterestRateRoundingType m_interestRateRoundingType;
        private string m_fnmTreasuryYieldForCurrentIndexDivisorNumber;
        private string m_fnmTreasuryYieldForIndexDivisorNumber;

        #endregion

        #region Public Properties
        public void SetConversionOption(XEConversionOption conversionOption) 
        {
            m_conversionOption = conversionOption;
        }
        public string IndexCurrentValuePercent 
        {
            get { return m_indexCurrentValuePercent; }
            set { m_indexCurrentValuePercent = value; }
        }
        public string IndexMarginPercent 
        {
            get { return m_indexMarginPercent; }
            set { m_indexMarginPercent = value; }
        }
        public E_ArmIndexType IndexType 
        {
            get { return m_indexType; }
            set { m_indexType = value; }
        }
        public string QualifyingRatePercent 
        {
            get { return m_qualifyingRatePercent; }
            set { m_qualifyingRatePercent = value; }
        }
        public E_YesNoIndicator ConversionOptionIndicator 
        {
            get { return m_conversionOptionIndicator; }
            set { m_conversionOptionIndicator = value; }
        }
        public string PaymentAdjustmentLifetimeCapAmount 
        {
            get { return m_paymentAdjustmentLifetimeCapAmount; }
            set { m_paymentAdjustmentLifetimeCapAmount = value; }
        }
        public string PaymentAdjustmentLifetimeCapPercent 
        {
            get { return m_paymentAdjustmentLifetimeCapPercent; }
            set { m_paymentAdjustmentLifetimeCapPercent = value; }
        }
        public string RateAdjustmentLifetimeCapPercent 
        {
            get { return m_rateAdjustmentLifetimeCapPercent; }
            set { m_rateAdjustmentLifetimeCapPercent = value; }
        }
        public string LifetimeCapRate 
        {
            get { return m_lifetimeCapRate; }
            set { m_lifetimeCapRate = value; }
        }
        public string LifetimeFloorPercent 
        {
            get { return m_lifetimeFloorPercent; }
            set { m_lifetimeFloorPercent = value; }
        }
        public string InterestRateRoundingFactor 
        {
            get { return m_interestRateRoundingFactor; }
            set { m_interestRateRoundingFactor = value; }
        }
        public E_ArmInterestRateRoundingType InterestRateRoundingType 
        {
            get { return m_interestRateRoundingType; }
            set { m_interestRateRoundingType = value; }
        }
        public string FnmTreasuryYieldForCurrentIndexDivisorNumber 
        {
            get { return m_fnmTreasuryYieldForCurrentIndexDivisorNumber; }
            set { m_fnmTreasuryYieldForCurrentIndexDivisorNumber = value; }
        }
        public string FnmTreasuryYieldForIndexDivisorNumber 
        {
            get { return m_fnmTreasuryYieldForIndexDivisorNumber; }
            set { m_fnmTreasuryYieldForIndexDivisorNumber = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("ARM");
            WriteAttribute(writer, "_IndexCurrentValuePercent", m_indexCurrentValuePercent);
            WriteAttribute(writer, "_IndexMarginPercent", m_indexMarginPercent);
            WriteAttribute(writer, "_IndexType", m_indexType);
            WriteAttribute(writer, "_QualifyingRatePercent", m_qualifyingRatePercent);
            WriteAttribute(writer, "_ConversionOptionIndicator", m_conversionOptionIndicator);
            WriteAttribute(writer, "PaymentAdjustmentLifetimeCapAmount", m_paymentAdjustmentLifetimeCapAmount);
            WriteAttribute(writer, "PaymentAdjustmentLifetimeCapPercent", m_paymentAdjustmentLifetimeCapPercent);
            WriteAttribute(writer, "RateAdjustmentLifetimeCapPercent", m_rateAdjustmentLifetimeCapPercent);
            WriteAttribute(writer, "_LifetimeCapRate", m_lifetimeCapRate);
            WriteAttribute(writer, "_LifetimeFloorPercent", m_lifetimeFloorPercent);
            WriteAttribute(writer, "_InterestRateRoundingFactor", m_interestRateRoundingFactor);
            WriteAttribute(writer, "_InterestRateRoundingType", m_interestRateRoundingType);
            WriteAttribute(writer, "FNMTreasuryYieldForCurrentIndexDivisorNumber", m_fnmTreasuryYieldForCurrentIndexDivisorNumber);
            WriteAttribute(writer, "FNMTreasuryYieldForIndexDivisorNumber", m_fnmTreasuryYieldForIndexDivisorNumber);
            WriteElement(writer, m_conversionOption);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
    public enum E_ArmIndexType 
    {
        Undefined = 0,
        WeeklyAverageConstantMaturingTreasury,
        SixMonthTreasury,
        FRE_LIBOR,
        FederalCostOfFunds,
        WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield,
        NationalAverageContractRateFHLBB,
        LIBOR,
        FRE60DayRequiredNetYield,
        OneYearTreasury,
        TreasuryBillDailyValue,
        MonthlyAverageConstantMaturingTreasury,
        ThreeYearTreasury,
        WeeklyAveragePrimeRate,
        FNM_LIBOR,
        WeeklyAverageTreasuryAuctionAverageInvestmentYield,
        Other,
        DailyCertificateOfDepositRate,
        WeeklyAverageTreasuryAuctionAverageBondDiscountYield,
        WallStreetJournalLIBOR,
        WeeklyAverageCertificateOfDepositRate,
        EleventhDistrictCostOfFunds,
        FNM60DayRequiredNetYield,
        NationalMonthlyMedianCostOfFunds
    }
    public enum E_ArmInterestRateRoundingType 
    {
        Undefined = 0,
        Down,
        None,
        Up,
        Nearest
    }
}
