/// Author: David Dao

using System;
using System.Collections;
using System.Xml;

namespace Mismo.Closing
{
    public class XENonPersonEntityDetail : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="NON_PERSON_ENTITY_DETAIL">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="AUTHORIZED_REPRESENTATIVE" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_OrganizationType">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="SoleProprietorship"/>
        //            <xs:enumeration value="LimitedLiabilityCompany"/>
        //            <xs:enumeration value="Corporation"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Partnership"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_OrganizationTypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="_OrganizedUnderTheLawsOfJurisdictionName" type="xs:string"/>
        //      <xs:attribute name="_SuccessorClauseTextDescription" type="xs:string"/>
        //      <xs:attribute name="_TaxIdentificationNumberIdentifier" type="xs:string"/>
        //      <xs:attribute name="_OrganizationLicensingTypeDescription" type="xs:string"/>
        //      <xs:attribute name="MERSOrganizationIdentifier" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private ArrayList m_authorizedRepresentativeList = new ArrayList();
        private E_NonPersonEntityDetailOrganizationType m_organizationType;
        private string m_organizationTypeOtherDescription;
        private string m_organizedUnderTheLawsOfJurisdictionName;
        private string m_successorClauseTextDescription;
        private string m_taxIdentificationNumberIdentifier;
        private string m_organizationLicensingTypeDescription;
        private string m_mersOrganizationIdentifier;
        #endregion

        #region Public Properties
        public void AddAuthorizedRepresentative(XEAuthorizedRepresentative authorizedRepresentative) 
        {
            m_authorizedRepresentativeList.Add(authorizedRepresentative);
        }
        public E_NonPersonEntityDetailOrganizationType OrganizationType 
        {
            get { return m_organizationType; }
            set { m_organizationType = value; }
        }
        public string OrganizationTypeOtherDescription 
        {
            get { return m_organizationTypeOtherDescription; }
            set { m_organizationTypeOtherDescription = value; }
        }
        public string OrganizedUnderTheLawsOfJurisdictionName 
        {
            get { return m_organizedUnderTheLawsOfJurisdictionName; }
            set { m_organizedUnderTheLawsOfJurisdictionName = value; }
        }
        public string SuccessorClauseTextDescription 
        {
            get { return m_successorClauseTextDescription; }
            set { m_successorClauseTextDescription = value; }
        }
        public string TaxIdentificationNumberIdentifier 
        {
            get { return m_taxIdentificationNumberIdentifier; }
            set { m_taxIdentificationNumberIdentifier = value; }
        }
        public string OrganizationLicensingTypeDescription 
        {
            get { return m_organizationLicensingTypeDescription; }
            set { m_organizationLicensingTypeDescription = value; }
        }
        public string MersOrganizationIdentifier 
        {
            get { return m_mersOrganizationIdentifier; }
            set { m_mersOrganizationIdentifier = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("NON_PERSON_ENTITY_DETAIL");
            WriteAttribute(writer, "_OrganizationType", m_organizationType);
            WriteAttribute(writer, "_OrganizationTypeOtherDescription", m_organizationTypeOtherDescription);
            WriteAttribute(writer, "_OrganizedUnderTheLawsOfJurisdictionName", m_organizedUnderTheLawsOfJurisdictionName);
            WriteAttribute(writer, "_SuccessorClauseTextDescription", m_successorClauseTextDescription);
            WriteAttribute(writer, "_TaxIdentificationNumberIdentifier", m_taxIdentificationNumberIdentifier);
            WriteAttribute(writer, "_OrganizationLicensingTypeDescription", m_organizationLicensingTypeDescription);
            WriteAttribute(writer, "MERSOrganizationIdentifier", m_mersOrganizationIdentifier);
            WriteElement(writer, m_authorizedRepresentativeList);

            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_NonPersonEntityDetailOrganizationType 
    {
        Undefined = 0,
        SoleProprietorship,
        LimitedLiabilityCompany,
        Corporation,
        Other,
        Partnership
    }
}
