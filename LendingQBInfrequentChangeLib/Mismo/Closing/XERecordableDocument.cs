/// Author: David Dao

using System;
using System.Xml;
using System.Collections;

namespace Mismo.Closing
{
    public class XERecordableDocument : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="RECORDABLE_DOCUMENT">
        //    <xs:complexType>
        //      <xs:sequence>
        //        <xs:element ref="ASSIGN_FROM" minOccurs="0"/>
        //        <xs:element ref="ASSIGN_TO" minOccurs="0"/>
        //        <xs:element ref="DEFAULT" minOccurs="0"/>
        //        <xs:element ref="NOTARY" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="PREPARED_BY" minOccurs="0"/>
        //        <xs:element ref="_ASSOCIATED_DOCUMENT" minOccurs="0"/>
        //        <xs:element ref="_RETURN_TO" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="RIDERS" minOccurs="0"/>
        //        <xs:element ref="SECURITY_INSTRUMENT" minOccurs="0"/>
        //        <xs:element ref="TRUSTEE" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="WITNESS" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:sequence>
        //      <xs:attribute name="_Type">
        //        <xs:simpleType>
        //          <xs:restriction base="xs:NMTOKEN">
        //            <xs:enumeration value="SignatureAffidavit"/>
        //            <xs:enumeration value="WarrantyDeed"/>
        //            <xs:enumeration value="QuitClaimDeed"/>
        //            <xs:enumeration value="All"/>
        //            <xs:enumeration value="DeedOfTrust"/>
        //            <xs:enumeration value="Other"/>
        //            <xs:enumeration value="Mortgage"/>
        //            <xs:enumeration value="SecurityInstrument"/>
        //            <xs:enumeration value="ReleaseOfLien"/>
        //            <xs:enumeration value="AssignmentOfMortgage"/>
        //          </xs:restriction>
        //        </xs:simpleType>
        //      </xs:attribute>
        //      <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
        //      <xs:attribute name="_SequenceIdentifier" type="xs:string"/>
        //    </xs:complexType>
        //  </xs:element>

        #endregion

        #region Private Member Variables
        private XEAssignFrom m_assignFrom;
        private XEAssignTo m_assignTo;
        private XEDefault m_default;
        private ArrayList m_notaryList = new ArrayList();
        private XEPreparedBy m_preparedBy;
        private XEAssociatedDocument m_associatedDocument;
        private ArrayList m_returnToList = new ArrayList();
        private XERiders m_riders;
        private XESecurityInstrument m_securityInstrument;
        private ArrayList m_trusteeList = new ArrayList();
        private ArrayList m_witnessList = new ArrayList();
        private E_RecordableDocumentType m_type;
        private string m_typeOtherDescription;
        private string m_sequenceIdentifier;

        #endregion

        #region Public Properties
        public void SetAssignFrom(XEAssignFrom assignFrom) 
        {
            m_assignFrom = assignFrom;
        }
        public void SetAssignTo(XEAssignTo assignTo) 
        {
            m_assignTo = assignTo;
        }
        public void SetDefault(XEDefault defaultValue) 
        {
            m_default = defaultValue;
        }
        public void AddNotary(XENotary notary) 
        {
            m_notaryList.Add(notary);
        }
        public void SetPreparedBy(XEPreparedBy preparedBy) 
        {
            m_preparedBy = preparedBy;
        }
        public void SetAssociatedDocument(XEAssociatedDocument associatedDocument) 
        {
            m_associatedDocument = associatedDocument;
        }
        public void AddReturnTo(XEReturnTo returnTo) 
        {
            m_returnToList.Add(returnTo);
        }
        public void SetRiders(XERiders riders) 
        {
            m_riders = riders;
        }
        public void SetSecurityInstrument(XESecurityInstrument securityInstrument) 
        {
            m_securityInstrument = securityInstrument;
        }
        public void AddTrustee(XETrustee trustee) 
        {
            m_trusteeList.Add(trustee);
        }
        public void AddWitness(XEWitness witness) 
        {
            m_witnessList.Add(witness);
        }
        public E_RecordableDocumentType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public string TypeOtherDescription 
        {
            get { return m_typeOtherDescription; }
            set { m_typeOtherDescription = value; }
        }
        public string SequenceIdentifier 
        {
            get { return m_sequenceIdentifier; }
            set { m_sequenceIdentifier = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("RECORDABLE_DOCUMENT");
            WriteAttribute(writer, "_Type", m_type);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "_SequenceIdentifier", m_sequenceIdentifier);

            WriteElement(writer, m_assignFrom);
            WriteElement(writer, m_assignTo);
            WriteElement(writer, m_default);
            WriteElement(writer, m_notaryList);
            WriteElement(writer, m_preparedBy);
            WriteElement(writer, m_associatedDocument);
            WriteElement(writer, m_returnToList);
            WriteElement(writer, m_riders);
            WriteElement(writer, m_securityInstrument);
            WriteElement(writer, m_trusteeList);
            WriteElement(writer, m_witnessList);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_RecordableDocumentType 
    {
        Undefined = 0,
        SignatureAffidavit,
        WarrantyDeed,
        QuitClaimDeed,
        All,
        DeedOfTrust,
        Other,
        Mortgage,
        SecurityInstrument,
        ReleaseOfLien,
        AssignmentOfMortgage

    }
}
