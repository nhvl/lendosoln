/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
	public class XESubsidySchedule : Mismo.Common.AbstractXmlNode
	{
        #region Schema
        //  <xs:element name="_SUBSIDY_SCHEDULE">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_AdjustmentPercent" type="xs:string"/>
        //          <xs:attribute name="_PeriodIdentifier" type="xs:string"/>
        //          <xs:attribute name="_PeriodicPaymentEffectiveDate" type="xs:string"/>
        //          <xs:attribute name="_PeriodicPaymentSubsidyAmount" type="xs:string"/>
        //          <xs:attribute name="_PeriodicTerm" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_adjustmentPercent;
        private string m_periodIdentifier;
        private string m_periodicPaymentEffectiveDate;
        private string m_periodicPaymentSubsidyAmount;
        private string m_periodicTerm;
        #endregion

        #region Public Properties
        public string AdjustmentPercent 
        {
            get { return m_adjustmentPercent; }
            set { m_adjustmentPercent = value; }
        }
        public string PeriodIdentifier 
        {
            get { return m_periodIdentifier; }
            set { m_periodIdentifier = value; }
        }
        public string PeriodicPaymentEffectiveDate 
        {
            get { return m_periodicPaymentEffectiveDate; }
            set { m_periodicPaymentEffectiveDate = value; }
        }
        public string PeriodicPaymentSubsidyAmount 
        {
            get { return m_periodicPaymentSubsidyAmount; }
            set { m_periodicPaymentSubsidyAmount = value; }
        }
        public string PeriodicTerm 
        {
            get { return m_periodicTerm; }
            set { m_periodicTerm = value; }
        }

        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("_SUBSIDY_SCHEDULE");
            WriteAttribute(writer, "_AdjustmentPercent", m_adjustmentPercent);
            WriteAttribute(writer, "_PeriodIdentifier", m_periodIdentifier);
            WriteAttribute(writer, "_PeriodicPaymentEffectiveDate", m_periodicPaymentEffectiveDate);
            WriteAttribute(writer, "_PeriodicPaymentSubsidyAmount", m_periodicPaymentSubsidyAmount);
            WriteAttribute(writer, "_PeriodicTerm", m_periodicTerm);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
	}
}
