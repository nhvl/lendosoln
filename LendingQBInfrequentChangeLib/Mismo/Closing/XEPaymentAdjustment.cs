/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XEPaymentAdjustment : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="PAYMENT_ADJUSTMENT">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="FirstPaymentAdjustmentMonths" type="xs:string"/>
        //          <xs:attribute name="_Amount" type="xs:string"/>
        //          <xs:attribute name="_CalculationType">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="AddPercentToEffectivePaymentRate"/>
        //                <xs:enumeration value="AddFixedDollarAmountToTheCurrentPayment"/>
        //                <xs:enumeration value="AddPercentToCurrentPaymentAmount"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //          <xs:attribute name="_DurationMonths" type="xs:string"/>
        //          <xs:attribute name="_Percent" type="xs:string"/>
        //          <xs:attribute name="_PeriodNumber" type="xs:string"/>
        //          <xs:attribute name="_PeriodicCapAmount" type="xs:string"/>
        //          <xs:attribute name="_PeriodicCapPercent" type="xs:string"/>
        //          <xs:attribute name="SubsequentPaymentAdjustmentMonths" type="xs:string"/>
        //          <xs:attribute name="FirstPaymentAdjustmentDate" type="xs:string"/>
        //          <xs:attribute name="LastPaymentAdjustmentDate" type="xs:string"/>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>
        #endregion

        #region Private Member Variables
        private string m_firstPaymentAdjustmentMonths;
        private string m_amount;
        private E_PaymentAdjustmentCalculationType m_calculationType;
        private string m_durationMonths;
        private string m_percent;
        private string m_periodNumber;
        private string m_periodicCapAmount;
        private string m_periodicCapPercent;
        private string m_subsequentPaymentAdjustmentMonths;
        private string m_firstPaymentAdjustmentDate;
        private string m_lastPaymentAdjustmentDate;
        #endregion

        #region Public Properties
        public string FirstPaymentAdjustmentMonths 
        {
            get { return m_firstPaymentAdjustmentMonths; }
            set { m_firstPaymentAdjustmentMonths = value; }
        }
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public E_PaymentAdjustmentCalculationType CalculationType 
        {
            get { return m_calculationType; }
            set { m_calculationType = value; }
        }
        public string DurationMonths 
        {
            get { return m_durationMonths; }
            set { m_durationMonths = value; }
        }
        public string Percent 
        {
            get { return m_percent; }
            set { m_percent = value; }
        }
        public string PeriodNumber 
        {
            get { return m_periodNumber; }
            set { m_periodNumber = value; }
        }
        public string PeriodicCapAmount 
        {
            get { return m_periodicCapAmount; }
            set { m_periodicCapAmount = value; }
        }
        public string PeriodicCapPercent 
        {
            get { return m_periodicCapPercent; }
            set { m_periodicCapPercent = value; }
        }
        public string SubsequentPaymentAdjustmentMonths 
        {
            get { return m_subsequentPaymentAdjustmentMonths; }
            set { m_subsequentPaymentAdjustmentMonths = value; }
        }
        public string FirstPaymentAdjustmentDate 
        {
            get { return m_firstPaymentAdjustmentDate; }
            set { m_firstPaymentAdjustmentDate = value; }
        }
        public string LastPaymentAdjustmentDate 
        {
            get { return m_lastPaymentAdjustmentDate; }
            set { m_lastPaymentAdjustmentDate = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("PAYMENT_ADJUSTMENT");
            WriteAttribute(writer, "FirstPaymentAdjustmentMonths", m_firstPaymentAdjustmentMonths);
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_CalculationType", m_calculationType);
            WriteAttribute(writer, "_DurationMonths", m_durationMonths);
            WriteAttribute(writer, "_Percent", m_percent);
            WriteAttribute(writer, "_PeriodNumber", m_periodNumber);
            WriteAttribute(writer, "_PeriodicCapAmount", m_periodicCapAmount);
            WriteAttribute(writer, "_PeriodicCapPercent", m_periodicCapPercent);
            WriteAttribute(writer, "SubsequentPaymentAdjustmentMonths", m_subsequentPaymentAdjustmentMonths);
            WriteAttribute(writer, "FirstPaymentAdjustmentDate", m_firstPaymentAdjustmentDate);
            WriteAttribute(writer, "LastPaymentAdjustmentDate", m_lastPaymentAdjustmentDate);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_PaymentAdjustmentCalculationType 
    {
        Undefined = 0,
        AddPercentToEffectivePaymentRate,
        AddFixedDollarAmountToTheCurrentPayment,
        AddPercentToCurrentPaymentAmount

    }
}
