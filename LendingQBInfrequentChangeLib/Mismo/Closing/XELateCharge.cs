/// Author: David Dao

using System;
using System.Xml;

namespace Mismo.Closing
{
    public class XELateCharge : Mismo.Common.AbstractXmlNode
    {
        #region Schema
        //  <xs:element name="LATE_CHARGE">
        //    <xs:complexType>
        //      <xs:complexContent>
        //        <xs:restriction base="xs:anyType">
        //          <xs:attribute name="_Amount" type="xs:string"/>
        //          <xs:attribute name="_GracePeriod" type="xs:string"/>
        //          <xs:attribute name="_LoanPaymentAmount" type="xs:string"/>
        //          <xs:attribute name="_MaximumAmount" type="xs:string"/>
        //          <xs:attribute name="_MinimumAmount" type="xs:string"/>
        //          <xs:attribute name="_Rate" type="xs:string"/>
        //          <xs:attribute name="_Type">
        //            <xs:simpleType>
        //              <xs:restriction base="xs:NMTOKEN">
        //                <xs:enumeration value="PercentageOfNetPayment"/>
        //                <xs:enumeration value="PercentageOfTotalPayment"/>
        //                <xs:enumeration value="PercentOfPrincipalAndInterest"/>
        //                <xs:enumeration value="PercentageOfPrincipalBalance"/>
        //                <xs:enumeration value="PercentageOfDelinquentInterest"/>
        //                <xs:enumeration value="PercentageOfPrinicipalBalance"/>
        //                <xs:enumeration value="FlatDollarAmount"/>
        //                <xs:enumeration value="NoLateCharges"/>
        //              </xs:restriction>
        //            </xs:simpleType>
        //          </xs:attribute>
        //        </xs:restriction>
        //      </xs:complexContent>
        //    </xs:complexType>
        //  </xs:element>        
        #endregion

        #region Private Member Variables
        private string m_amount;
        private string m_gracePeriod;
        private string m_loanPaymentAmount;
        private string m_maximumAmount;
        private string m_minimumAmount;
        private string m_rate;
        private E_LateChargeType m_type;
        #endregion

        #region Public Properties
        public string Amount 
        {
            get { return m_amount; }
            set { m_amount = value; }
        }
        public string GracePeriod 
        {
            get { return m_gracePeriod; }
            set { m_gracePeriod = value; }
        }
        public string LoanPaymentAmount 
        {
            get { return m_loanPaymentAmount; }
            set { m_loanPaymentAmount = value; }
        }
        public string MaximumAmount 
        {
            get { return m_maximumAmount; }
            set { m_maximumAmount = value; }
        }
        public string MinimumAmount 
        {
            get { return m_minimumAmount; }
            set { m_minimumAmount = value; }
        }
        public string Rate 
        {
            get { return m_rate; }
            set { m_rate = value; }
        }
        public E_LateChargeType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        #endregion

        #region Implement IXmlNode
        public override void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement("LATE_CHARGE");
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_GracePeriod", m_gracePeriod);
            WriteAttribute(writer, "_LoanPaymentAmount", m_loanPaymentAmount);
            WriteAttribute(writer, "_MaximumAmount", m_maximumAmount);
            WriteAttribute(writer, "_MinimumAmount", m_minimumAmount);
            WriteAttribute(writer, "_Rate", m_rate);
            WriteAttribute(writer, "_Type", m_type);
            writer.WriteEndElement();
        }
        public override void Parse(XmlElement el) 
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    public enum E_LateChargeType 
    {
        Undefined = 0,
        PercentageOfNetPayment,
        PercentageOfTotalPayment,
        PercentOfPrincipalAndInterest,
        PercentageOfPrincipalBalance,
        PercentageOfDelinquentInterest,
        PercentageOfPrinicipalBalance,
        FlatDollarAmount,
        NoLateCharges

    }
}
