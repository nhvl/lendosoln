/// Author: David Dao

using System;

namespace Mismo.Common
{
    public enum E_YesNoIndicator 
    {
        Undefined = 0,
        Yes,
        No
    }
}
