using System;

namespace Mismo.Common
{
	/// <summary>
	/// Summary description for Utilities.
	/// </summary>
	public class Utilities
	{
        /// <summary>
        /// Return CCYY-MM-DD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDateString(DateTime dt) 
        {
            return dt.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// Return CCYY-MM-DDTHH:MM:SS
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDateTimeString(DateTime dt) 
        {
            return dt.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        /// <summary>
        /// Return CCYY-MM
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToMonthYearString(DateTime dt) 
        {
            return dt.ToString("yyyy-MM");
        }
        public static DateTime ParseDateTime(string s) 
        {
            DateTime dt = DateTime.MinValue;
            try 
            {
                dt = DateTime.ParseExact(s, new string[] {"yyyy-MM-dd", "yyyy-MM", "yyyy-MM-ddTHH:mm:ss"},null, System.Globalization.DateTimeStyles.None);
            } 
            catch 
            {
            }
            return dt;
        }
	}
}
