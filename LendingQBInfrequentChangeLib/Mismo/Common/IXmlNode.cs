using System;
using System.Collections;
using System.Xml;

namespace Mismo.Common
{
    public interface IXmlNode
    {
        void GenerateXml(XmlWriter writer);
        void Parse(XmlElement el);
    }

    public abstract class AbstractXmlNode : IXmlNode
    {

        /// <summary>
        /// If value is null or empty then attribute will not generate.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="attr"></param>
        /// <param name="value"></param>
        protected void WriteAttribute(XmlWriter writer, string attr, string value) 
        {
            if (null != value && "" != value)
                writer.WriteAttributeString(attr, value);
        }

        protected void WriteRequiredAttribute(XmlWriter writer, string attr, string value) 
        {
            writer.WriteAttributeString(attr, value);
        }
        protected void WriteAttribute(XmlWriter writer, string attr, Enum value) 
        {
            
            
            if (value.ToString("D") == "0")
                return; // Do not generate attribute if it is undefined.

            writer.WriteAttributeString(attr, value.ToString());
        }
        protected void WriteAttribute(XmlWriter writer, string attr, E_YesNoIndicator value) 
        {
            switch (value) 
            {
                case E_YesNoIndicator.Undefined:
                    break; // Don't generate attribute.
                case E_YesNoIndicator.Yes:
                    writer.WriteAttributeString(attr, "Y");
                    break;
                case E_YesNoIndicator.No:
                    writer.WriteAttributeString(attr, "N");
                    break;
                default:
                    break;
            }
        }
        protected void WriteElement(XmlWriter writer, string elName, string value) 
        {
            if (null != value && "" != value)
                writer.WriteElementString(elName, value);
        }
        protected void WriteRequiredElement(XmlWriter writer, string elName, string value) 
        {
            writer.WriteElementString(elName, value);
        }
        protected void WriteElement(XmlWriter writer, IXmlNode node) 
        {
            if (null != node)
                node.GenerateXml(writer);
        }
        protected void WriteElement(XmlWriter writer, string elName, Enum value)
        {
            if (value.ToString("D") == "0")
                return; // Do not generate attribute if it is undefined.

            if (null != value)
                writer.WriteElementString(elName, value.ToString());
        }
        protected void WriteElement(XmlWriter writer, ArrayList list) 
        {
            foreach (IXmlNode o in list) 
            {
                if (null != o)
                    o.GenerateXml(writer);
            }
        }
        protected void LoadXml(string xml) 
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            
            Parse((XmlElement) doc.ChildNodes[0]);
        }
        public abstract void GenerateXml(XmlWriter writer);
        public abstract void Parse(XmlElement el);
    }
}
