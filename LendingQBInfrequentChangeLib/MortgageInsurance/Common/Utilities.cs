﻿// <copyright file="Utilities.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/17/2014 5:35:57 PM
// </summary>
namespace Mismo231.MI
{
    using System;
    using System.Reflection;
    using System.Xml.Serialization;

    /// <summary>
    /// Utility classes for the MI request and response.
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Allows declaration of enumerated types that specify both an in-code name and an XML string name, for use with XML serialization.
        /// Copied from LendersOffice.Common.Utilities
        /// Example:
        /// [XmlEnum("")]
        /// None,
        /// [XmlEnum("Y")]
        /// Yes,
        /// [XmlEnum("N")]
        /// No,
        /// </summary>
        /// <param name="e"></param>
        /// <returns>A string representing the enum value</returns>
        public static string ConvertEnumToString(Enum e)
        {
            Type t = e.GetType();

            FieldInfo info = t.GetField(e.ToString("G"));

            if (!info.IsDefined(typeof(XmlEnumAttribute), false))
            {
                return e.ToString("G");
            }

            object[] o = info.GetCustomAttributes(typeof(XmlEnumAttribute), false);
            var att = (XmlEnumAttribute)o[0];
            return att.Name;
        }
    }
}
