﻿// <copyright file="Enums.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 10:57:03 AM
// </summary>
namespace Mismo231.MI
{
    using System.Xml.Serialization;

    /// <summary>
    /// An LQB defined enum with names of key-value pairs included in the request from LQB and/or expected in the response from the MI vendor.
    /// </summary>
    public enum MIKey
    {
        /// <summary>
        /// Key with no name. Used to handle unexpected keys.
        /// </summary>
        [XmlEnum("")]
        None = 0,

        /// <summary>
        /// Each MI order is assigned an order number by LQB.
        /// </summary>
        [XmlEnum("OrderNumber")]
        OrderNumber = 1,

        /// <summary>
        /// This loan ID is the sLId of the loan file.
        /// </summary>
        [XmlEnum("LendingQBLoanID")]
        LendingQBLoanID = 2,

        /// <summary>
        /// Indicates that the user requested a split premium.
        /// </summary>
        [XmlEnum("SplitPremium")]
        SplitPremium = 3,

        /// <summary>
        /// Indicates that the loan is a business relocation loan.
        /// </summary>
        [XmlEnum("MIRelocationLoanIndicator")]
        MIRelocationLoanIndicator = 4,

        /// <summary>
        /// The quote disclaimer is accepted from the MI vendor and displayed to the LQB user.
        /// </summary>
        [XmlEnum("RateQuoteDisclaimer")]
        RateQuoteDisclaimer = 5,

        /// <summary>
        /// The debt-to-income or back-end ratio.
        /// </summary>
        [XmlEnum("TotalDebtExpenseRatioPercent")]
        DTI = 6,

        /// <summary>
        /// Identifies the type of entity that originated the loan file (Broker | Correspondent | Lender | Other).
        /// </summary>
        [XmlEnum("LoanOriginatorType")]
        LoanOriginatorType = 7,

        /// <summary>
        /// The XML element name for the LQB GENERIC_FRAMEWORK_USER_TICKET.
        /// </summary>
        [XmlEnum("GenericFrameworkUserTicketName")]
        GenericFrameworkUserTicketName = 8,

        /// <summary>
        /// The encrypted ticket value for the LQB GENERIC_FRAMEWORK_USER_TICKET.
        /// </summary>
        [XmlEnum("GenericFrameworkUserTicketEncryptedTicket")]
        GenericFrameworkUserTicketEncryptedTicket = 9,
        
        /// <summary>
        /// Indicates an error.
        /// </summary>
        [XmlEnum("Error")]
        Error = 10,

        /// <summary>
        /// Indicates an abort due to an error.
        /// </summary>
        [XmlEnum("Abort")]
        Abort = 11,

        /// <summary>
        /// Indicates a fatal error.
        /// </summary>
        [XmlEnum("Fatal")]
        Fatal = 12,
        
        /// <summary>
        /// The quote number that is originated by MI Provider.
        /// </summary>
        [XmlEnum("QuoteNumber")]
        QuoteNumber = 13,

        /// <summary>
        /// The index type as a string.
        /// </summary>
        [XmlEnum("IndexTypeOtherDescription")]
        IndexTypeOtherDescription = 14,

        /// <summary>
        /// Indicates whether the property inspection was waived.
        /// </summary>
        [XmlEnum("PropertyInspectionWaiver")]
        PropertyInspectionWaiver = 15,

        /// <summary>
        /// A unique identifier for the appraisal.
        /// </summary>
        [XmlEnum("AppraisalIdentifier")]
        AppraisalIdentifier = 16,

        /// <summary>
        /// The Collateral Underwriter risk score.
        /// </summary>
        [XmlEnum("CURiskScore")]
        CURiskScore = 17,

        /// <summary>
        /// The web service domain for the LQB GENERIC_FRAMEWORK_USER_TICKET.
        /// </summary>
        [XmlEnum("GenericFrameworkUserTicketWebServiceDomain")]
        GenericFrameworkUserTicketWebServiceDomain = 18,


        /// <summary>
        /// The site code indicating where the authentication ticket is valid.
        /// </summary>
        [XmlEnum("GenericFrameworkUserTicketSiteCode")]
        GenericFrameworkUserTicketSiteCode = 19,
    }

    public enum E_YNIndicator
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Y")]
        Y,
        [XmlEnum("N")]
        N,
    }

    public enum MI_AVMModelNameTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("AutomatedPropertyService")]
        AutomatedPropertyService,
        [XmlEnum("Casa")]
        Casa,
        [XmlEnum("HomePriceAnalyzer")]
        HomePriceAnalyzer,
        [XmlEnum("HomePriceIndex")]
        HomePriceIndex,
        [XmlEnum("HomeValueExplorer")]
        HomeValueExplorer,
        [XmlEnum("Indicator")]
        Indicator,
        [XmlEnum("NetValue")]
        NetValue,
        [XmlEnum("Pass")]
        Pass,
        [XmlEnum("PropertySurveyAnalysisReport")]
        PropertySurveyAnalysisReport,
        [XmlEnum("SiteXValue")]
        SiteXValue,
        [XmlEnum("ValueFinder")]
        ValueFinder,
        [XmlEnum("ValuePoint")]
        ValuePoint,
        [XmlEnum("ValuePointPlus")]
        ValuePointPlus,
        [XmlEnum("ValuePoint4")]
        ValuePoint4,
        [XmlEnum("ValueSure")]
        ValueSure,
        [XmlEnum("ValueWizard")]
        ValueWizard,
        [XmlEnum("ValueWizardPlus")]
        ValueWizardPlus,
        [XmlEnum("VeroIndexPlus")]
        VeroIndexPlus,
        [XmlEnum("VeroValue")]
        VeroValue,
        [XmlEnum("Other")]
        Other,
    }

    public enum MI_BorrowerPrintPositionTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Borrower")]
        Borrower,
        [XmlEnum("CoBorrower")]
        CoBorrower,
    }

    public enum MI_ContactPointRoleTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Home")]
        Home,
        [XmlEnum("Mobile")]
        Mobile,
        [XmlEnum("Work")]
        Work,
    }

    public enum MI_ContactPointTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Email")]
        Email,
        [XmlEnum("Fax")]
        Fax,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("Phone")]
        Phone,
    }

    public enum MI_CreditReportTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Error")]
        Error,
        [XmlEnum("Merge")]
        Merge,
        [XmlEnum("MergePlus")]
        MergePlus,
        [XmlEnum("NonTraditional")]
        NonTraditional,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("RiskScoresOnly")]
        RiskScoresOnly,
        [XmlEnum("RMCR")]
        RMCR,
        [XmlEnum("RMCRForeign")]
        RMCRForeign,
        [XmlEnum("Status")]
        Status,
    }

    public enum MI_CreditRepositorySourceTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Equifax")]
        Equifax,
        [XmlEnum("Experian")]
        Experian,
        [XmlEnum("MergedData")]
        MergedData,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("TransUnion")]
        TransUnion,
    }

    public enum MI_CreditScoreExclusionReasonTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("InvalidScoreRequest")]
        InvalidScoreRequest,
        [XmlEnum("NotScoredCreditDataNotAvailable")]
        NotScoredCreditDataNotAvailable,
        [XmlEnum("NotScoredFileIsUnderReview")]
        NotScoredFileIsUnderReview,
        [XmlEnum("NotScoredFileTooLong")]
        NotScoredFileTooLong,
        [XmlEnum("NotScoredInsufficientCredit")]
        NotScoredInsufficientCredit,
        [XmlEnum("NotScoredNoQualifyingAccount")]
        NotScoredNoQualifyingAccount,
        [XmlEnum("NotScoredNoRecentAccountInformation")]
        NotScoredNoRecentAccountInformation,
        [XmlEnum("NotScoredSubjectDeceased")]
        NotScoredSubjectDeceased,
        [XmlEnum("ScoringNotAvailable")]
        ScoringNotAvailable,
        [XmlEnum("UnauthorizedScoreRequest")]
        UnauthorizedScoreRequest,
        [XmlEnum("NotScoredRequirementsNotMet")]
        NotScoredRequirementsNotMet,
        [XmlEnum("NotScoredFileCannotBeScored")]
        NotScoredFileCannotBeScored,
    }

    public enum MI_CreditScoreModelNameTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("EquifaxBeacon")]
        EquifaxBeacon,
        [XmlEnum("EquifaxBeacon5.0")]
        EquifaxBeacon5,
        [XmlEnum("EquifaxBeacon5.0Auto")]
        EquifaxBeacon5Auto,
        [XmlEnum("EquifaxBeacon5.0BankCard")]
        EquifaxBeacon5BankCard,
        [XmlEnum("EquifaxBeacon5.0Installment")]
        EquifaxBeacon5Installment,
        [XmlEnum("EquifaxBeacon5.0PersonalFinance")]
        EquifaxBeacon5PersonalFinance,
        [XmlEnum("EquifaxBeaconAuto")]
        EquifaxBeaconAuto,
        [XmlEnum("EquifaxBeaconBankcard")]
        EquifaxBeaconBankcard,
        [XmlEnum("EquifaxBeaconInstallment")]
        EquifaxBeaconInstallment,
        [XmlEnum("EquifaxBeaconPersonalFinance")]
        EquifaxBeaconPersonalFinance,
        [XmlEnum("EquifaxDAS")]
        EquifaxDAS,
        [XmlEnum("EquifaxEnhancedBeacon")]
        EquifaxEnhancedBeacon,
        [XmlEnum("EquifaxEnhancedDAS")]
        EquifaxEnhancedDAS,
        [XmlEnum("EquifaxMarketMax")]
        EquifaxMarketMax,
        [XmlEnum("EquifaxMortgageScore")]
        EquifaxMortgageScore,
        [XmlEnum("EquifaxPinnacle")]
        EquifaxPinnacle,
        [XmlEnum("EquifaxPinnacle2.0")]
        EquifaxPinnacle2,
        [XmlEnum("ExperianFairIsaac")]
        ExperianFairIsaac,
        [XmlEnum("ExperianFairIsaacAdvanced")]
        ExperianFairIsaacAdvanced,
        [XmlEnum("ExperianFairIsaacAuto")]
        ExperianFairIsaacAuto,
        [XmlEnum("ExperianFairIsaacBankcard")]
        ExperianFairIsaacBankcard,
        [XmlEnum("ExperianFairIsaacInstallment")]
        ExperianFairIsaacInstallment,
        [XmlEnum("ExperianFairIsaacPersonalFinance")]
        ExperianFairIsaacPersonalFinance,
        [XmlEnum("ExperianMDSBankruptcyII")]
        ExperianMDSBankruptcyII,
        [XmlEnum("ExperianNewNationalEquivalency")]
        ExperianNewNationalEquivalency,
        [XmlEnum("ExperianNewNationalRisk")]
        ExperianNewNationalRisk,
        [XmlEnum("ExperianOldNationalRisk")]
        ExperianOldNationalRisk,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("TransUnionDelphi")]
        TransUnionDelphi,
        [XmlEnum("TransUnionEmpirica")]
        TransUnionEmpirica,
        [XmlEnum("TransUnionEmpiricaAuto")]
        TransUnionEmpiricaAuto,
        [XmlEnum("TransUnionEmpiricaBankcard")]
        TransUnionEmpiricaBankcard,
        [XmlEnum("TransUnionEmpiricaInstallment")]
        TransUnionEmpiricaInstallment,
        [XmlEnum("TransUnionEmpiricaPersonalFinance")]
        TransUnionEmpiricaPersonalFinance,
        [XmlEnum("TransUnionNewDelphi")]
        TransUnionNewDelphi,
        [XmlEnum("TransUnionPrecision")]
        TransUnionPrecision,
    }

    public enum MI_DesktopUnderwriterRecommendationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("ReferEligible")]
        ReferEligible,
        [XmlEnum("OutOfScope")]
        OutOfScope,
        [XmlEnum("ApproveEligible")]
        ApproveEligible,
        [XmlEnum("ReferIneligible")]
        ReferIneligible,
        [XmlEnum("Unknown")]
        Unknown,
        [XmlEnum("ApproveIneligible")]
        ApproveIneligible,
        [XmlEnum("ReferWithCaution")]
        ReferWithCaution,
        [XmlEnum("Refer")]
        Refer,
        [XmlEnum("Deny")]
        Deny,
        [XmlEnum("Error")]
        Error,
        [XmlEnum("Complete")]
        Complete,
        [XmlEnum("Approve")]
        Approve,
        [XmlEnum("Ineligible")]
        Ineligible,
        [XmlEnum("CompleteWithCaution")]
        CompleteWithCaution,
        [XmlEnum("ReferWithCaution1")]
        ReferWithCaution1,
        [XmlEnum("ReferWithCaution2")]
        ReferWithCaution2,
        [XmlEnum("ReferWithCaution3")]
        ReferWithCaution3,
        [XmlEnum("ReferWithCaution4")]
        ReferWithCaution4,
        [XmlEnum("Resubmit")]
        Resubmit,
        [XmlEnum("ExpandedApproval1")]
        ExpandedApproval1,
        [XmlEnum("ExpandedApproval2")]
        ExpandedApproval2,
        [XmlEnum("ExpandedApproval3")]
        ExpandedApproval3,
        [XmlEnum("ExpandedApproval4")]
        ExpandedApproval4,
        [XmlEnum("ExpandedApproval1Eligible")]
        ExpandedApproval1Eligible,
        [XmlEnum("ExpandedApproval1Ineligible")]
        ExpandedApproval1Ineligible,
        [XmlEnum("ExpandedApproval2Eligible")]
        ExpandedApproval2Eligible,
        [XmlEnum("ExpandedApproval2Ineligible")]
        ExpandedApproval2Ineligible,
        [XmlEnum("ExpandedApproval3Eligible")]
        ExpandedApproval3Eligible,
        [XmlEnum("ExpandedApproval3Ineligible")]
        ExpandedApproval3Ineligible,
    }

    public enum MI_FreddieMacPurchaseEligibilityTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("FreddieMacEligible")]
        FreddieMacEligible,
        [XmlEnum("FreddieMacIneligible")]
        FreddieMacIneligible,
        [XmlEnum("FreddieMacEligibleLPAMinusOffering")]
        FreddieMacEligibleLPAMinusOffering,
    }

    public enum MI_InvestorProgramNameTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Calpers")]
        Calpers,
        [XmlEnum("PaymentPower")]
        PaymentPower,
        [XmlEnum("PrimeRatePlus")]
        PrimeRatePlus,
        [XmlEnum("SettleAmerica")]
        SettleAmerica,
        [XmlEnum("Other")]
        Other,
    }

    public enum MI_LoanProspectorCreditRiskClassificationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Accept")]
        Accept,
        [XmlEnum("Caution")]
        Caution,
        [XmlEnum("Refer")]
        Refer,
        [XmlEnum("NA")]
        NA,
    }

    public enum MI_LoanProspectorDocumentationClassificationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Accept")]
        Accept,
        [XmlEnum("AcceptPlus")]
        AcceptPlus,
        [XmlEnum("Caution")]
        Caution,
        [XmlEnum("Refer")]
        Refer,
        [XmlEnum("NA")]
        NA,
    }

    public enum MI_LoanProspectorRiskGradeAssignedTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("RG1")]
        RG1,
        [XmlEnum("RG2")]
        RG2,
        [XmlEnum("RG3")]
        RG3,
        [XmlEnum("RG4")]
        RG4,
        [XmlEnum("RG5")]
        RG5,
        [XmlEnum("RG6")]
        RG6,
        [XmlEnum("RG7")]
        RG7,
        [XmlEnum("RG8")]
        RG8,
        [XmlEnum("RG9")]
        RG9,
        [XmlEnum("RG10")]
        RG10,
    }

    public enum MI_MIApplicationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Delegated")]
        Delegated,
        [XmlEnum("Prequalification")]
        Prequalification,
        [XmlEnum("Standard")]
        Standard,
        [XmlEnum("RateQuote")]
        RateQuote,
    }

    public enum MI_MIAssetDocumentationConfirmationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("NotVerified")]
        NotVerified,
        [XmlEnum("Verified")]
        Verified,
    }

    public enum MI_MIAssetDocumentationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("FullDocumentation")]
        FullDocumentation,
        [XmlEnum("Stated")]
        Stated,
        [XmlEnum("NotStatedOnLoanApplication")]
        NotStatedOnLoanApplication,
        [XmlEnum("Other")]
        Other,
    }

    public enum MI_MICertificateTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("BothPrimaryAndPool")]
        BothPrimaryAndPool,
        [XmlEnum("NoCoverage")]
        NoCoverage,
        [XmlEnum("Pool")]
        Pool,
        [XmlEnum("Primary")]
        Primary,
    }
    
    public enum MI_MICoveragePlanTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Pool")]
        Pool,
        [XmlEnum("RiskSharing")]
        RiskSharing,
        [XmlEnum("SecondLayer")]
        SecondLayer,
        [XmlEnum("StandardPrimary")]
        StandardPrimary,
    }

    public enum MI_MIDecisionTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Approved")]
        Approved,
        [XmlEnum("ApprovedAfterReevaluation")]
        ApprovedAfterReevaluation,
        [XmlEnum("ConditionedApproval")]
        ConditionedApproval,
        [XmlEnum("Declined")]
        Declined,
        [XmlEnum("Suspended")]
        Suspended,
    }

    public enum MI_MIDurationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Annual")]
        Annual,
        [XmlEnum("NotApplicable")]
        NotApplicable,
        [XmlEnum("PeriodicMonthly")]
        PeriodicMonthly,
        [XmlEnum("SingleLifeOfLoan")]
        SingleLifeOfLoan,
        [XmlEnum("SingleSpecific")]
        SingleSpecific,
    }

    public enum MI_MIEmploymentDocumentationConfirmationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("NotVerified")]
        NotVerified,
        [XmlEnum("Verified")]
        Verified,
    }

    public enum MI_MIEmploymentDocumentationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("FullDocumentation")]
        FullDocumentation,
        [XmlEnum("Stated")]
        Stated,
        [XmlEnum("NotStatedOnLoanApplication")]
        NotStatedOnLoanApplication,
        [XmlEnum("Other")]
        Other,
    }

    public enum MI_MIIncomeDocumentationConfirmationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("NotVerified")]
        NotVerified,
        [XmlEnum("Verified")]
        Verified,
    }

    public enum MI_MIIncomeDocumentationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("FullDocumentation")]
        FullDocumentation,
        [XmlEnum("StatedWithIRSForm4506")]
        StatedWithIRSForm4506,
        [XmlEnum("StatedWithoutIRSForm4506")]
        StatedWithoutIRSForm4506,
        [XmlEnum("NotStatedOnLoanApplication")]
        NotStatedOnLoanApplication,
        [XmlEnum("Other")]
        Other,
    }

    public enum MI_MIInitialPremiumAtClosingTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Deferred")]
        Deferred,
        [XmlEnum("Prepaid")]
        Prepaid,
    }

    public enum MI_MIPremiumPaymentTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("BorrowerPaid")]
        BorrowerPaid,
        [XmlEnum("BothBorrowerAndLenderPaid")]
        BothBorrowerAndLenderPaid,
        [XmlEnum("LenderPaid")]
        LenderPaid,
    }

    public enum MI_MIPremiumRatePlanTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("BackLoaded")]
        BackLoaded,
        [XmlEnum("Level")]
        Level,
        [XmlEnum("ModifiedFrontLoaded")]
        ModifiedFrontLoaded,
        [XmlEnum("StandardFrontLoaded")]
        StandardFrontLoaded,
        [XmlEnum("SplitPremium1")]
        SplitPremium1,
        [XmlEnum("SplitPremium2")]
        SplitPremium2,
        [XmlEnum("SplitPremium3")]
        SplitPremium3,
    }

    public enum MI_MIPremiumRefundableTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("NotRefundable")]
        NotRefundable,
        [XmlEnum("Refundable")]
        Refundable,
        [XmlEnum("RefundableWithLimits")]
        RefundableWithLimits,
    }

    public enum MI_MIPremiumTaxCodeTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("County")]
        County,
        [XmlEnum("Municipal")]
        Municipal,
        [XmlEnum("State")]
        State,
        [XmlEnum("AllTaxes")]
        AllTaxes,
    }

    public enum MI_MIReducedLoanDocumentationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("NoIncomeNoAsset")]
        NoIncomeNoAsset,
        [XmlEnum("NoIncomeNoRatio")]
        NoIncomeNoRatio,
        [XmlEnum("StatedIncomeWithIRSForm4506")]
        StatedIncomeWithIRSForm4506,
        [XmlEnum("StatedIncomeWithoutIRSForm4506")]
        StatedIncomeWithoutIRSForm4506,
        [XmlEnum("StatedIncomeStatedAsset")]
        StatedIncomeStatedAsset,
        [XmlEnum("StatedAssets")]
        StatedAssets,
    }

    public enum MI_MIRenewalCalculationTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Constant")]
        Constant,
        [XmlEnum("Declining")]
        Declining,
        [XmlEnum("NoRenewals")]
        NoRenewals,
        [XmlEnum("NotApplicable")]
        NotApplicable,
    }

    public enum MI_MIRenewalPremiumSequenceEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("First")]
        First,
        [XmlEnum("Second")]
        Second,
        [XmlEnum("Third")]
        Third,
        [XmlEnum("Fourth")]
        Fourth,
        [XmlEnum("Fifth")]
        Fifth,
    }

    public enum MI_MIRequestTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("OriginalRequest")]
        OriginalRequest,
        [XmlEnum("Resubmission")]
        Resubmission,
    }

    public enum MI_MISubPrimeProgramTypeEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("AMinus")]
        AMinus,
    }

    public enum MI_PreferredResponseFormatEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("PCL")]
        PCL,
        [XmlEnum("PDF")]
        PDF,
        [XmlEnum("Text")]
        Text,
        [XmlEnum("XML")]
        XML,
    }

    public enum MI_PreferredResponseMethodEnumerated
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Fax")]
        Fax,
        [XmlEnum("File")]
        File,
        [XmlEnum("FTP")]
        FTP,
        [XmlEnum("HTTP")]
        HTTP,
        [XmlEnum("HTTPS")]
        HTTPS,
        [XmlEnum("Mail")]
        Mail,
        [XmlEnum("MessageQueue")]
        MessageQueue,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("SMTP")]
        SMTP,
        [XmlEnum("VAN")]
        VAN,
    }

    /// <summary>
    /// The type of entity that originated the loan file.
    /// </summary>
    public enum LoanOriginatorType
    {
        /// <summary>
        /// Not specified.
        /// </summary>
        [XmlEnum("")]
        None,
        
        /// <summary>
        /// A broker, i.e. wholesale channel.
        /// </summary>
        [XmlEnum("Broker")]
        Broker,
        
        /// <summary>
        /// A correspondent.
        /// </summary>
        [XmlEnum("Correspondent")]
        Correspondent,
        
        /// <summary>
        /// A lender.
        /// </summary>
        [XmlEnum("Lender")]
        Lender,
        
        /// <summary>
        /// Any other type.
        /// </summary>
        [XmlEnum("Other")]
        Other
    }
}
