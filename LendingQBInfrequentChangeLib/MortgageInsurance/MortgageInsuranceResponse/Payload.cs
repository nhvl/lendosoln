﻿// <copyright file="Payload.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   11/09/2015 2:34:30 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Custom data class for Genworth to capture the PDF document in the MISMO 2.3.1 EMBEDDED_FILE element.
    /// </summary>
    public class Payload
    {
        /// <summary>
        /// The PDF from the vendor is base64 encoded and included in a CDATA block.
        /// </summary>
        [XmlText(Type = typeof(string))]
        public string PDF;
    }
}