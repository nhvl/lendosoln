﻿// <copyright file="RespondToParty.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:09:23 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPOND_TO_PARTY element. 
    /// </summary>
    public class RespondToParty
    {
        [XmlAttribute()]
        public string _Name;

        [XmlAttribute()]
        public string _StreetAddress;

        [XmlAttribute()]
        public string _StreetAddress2;

        [XmlAttribute()]
        public string _City;

        [XmlAttribute()]
        public string _State;

        [XmlAttribute()]
        public string _PostalCode;

        [XmlAttribute()]
        public string _Identifier;

        [XmlElement("CONTACT_DETAIL")]
        public List<ContactDetail> ContactDetailList = new List<ContactDetail>();
    }
}