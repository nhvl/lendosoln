﻿// <copyright file="Status.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:36:50 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 STATUS element. 
    /// </summary>
    public class Status
    {
        [XmlAttribute()]
        public string _Condition;

        [XmlAttribute()]
        public string _Code;

        [XmlAttribute()]
        public string _Name;

        [XmlAttribute()]
        public string _Description;
    }
}
