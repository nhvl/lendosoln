﻿// <copyright file="Key.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:24:00 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 KEY element.
    /// </summary>
    public class Key
    {
        [XmlAttribute()]
        public string _Name;

        [XmlAttribute()]
        public string _Value;
    }
}
