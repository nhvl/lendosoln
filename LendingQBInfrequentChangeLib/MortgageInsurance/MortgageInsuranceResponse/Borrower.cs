﻿// <copyright file="Borrower.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 6:11:16 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 BORROWER element.
    /// </summary>
    public class Borrower
    {
        [XmlAttribute()]
        public string BorrowerID;

        [XmlAttribute()]
        public string _FirstName;

        [XmlAttribute()]
        public string _MiddleName;

        [XmlAttribute()]
        public string _LastName;

        [XmlAttribute()]
        public MI_BorrowerPrintPositionTypeEnumerated _PrintPositionType;

        [XmlAttribute()]
        public string _SSN;
    }
}