﻿// <copyright file="Document.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 6:23:05 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the PDF document in the MISMO 2.3.1 EMBEDDED_FILE element.
    /// </summary>
    public class Document
    {
        [XmlText(Type = typeof(string))]
        public string PDF;

        /// <summary>
        /// A custom element for Genworth. Their standard policy response includes a Payload sub-container with the CDATA block for the PDF.
        /// </summary>
        [XmlElement("Payload")]
        public Payload Payload;
    }
}
