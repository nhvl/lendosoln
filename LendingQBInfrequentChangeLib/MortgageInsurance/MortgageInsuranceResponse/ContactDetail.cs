﻿// <copyright file="ContactDetail.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:27:50 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 CONTACT_DETAIL element. 
    /// </summary>
    public class ContactDetail
    {
        [XmlAttribute()]
        public string _Name;

        [XmlElement("CONTACT_POINT")]
        public List<ContactPoint> ContactPointList = new List<ContactPoint>();
    }
}
