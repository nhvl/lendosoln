﻿// <copyright file="ContactPoint.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:31:17 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 CONTACT_POINT element. 
    /// </summary>
    public class ContactPoint
    {
        [XmlAttribute()]
        public MI_ContactPointRoleTypeEnumerated _RoleType;

        [XmlAttribute()]
        public MI_ContactPointTypeEnumerated _Type;

        [XmlAttribute()]
        public string _TypeOtherDescription;

        [XmlAttribute()]
        public string _Value;

        [XmlAttribute()]
        public E_YNIndicator _PreferenceIndicator;
    }
}
