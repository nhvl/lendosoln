﻿// <copyright file="MIResponse.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:42:14 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 MI_RESPONSE element.
    /// </summary>
    public class MIResponse
    {
        [XmlAttribute()]
        public string MISMOVersionID;

        [XmlAttribute()]
        public string LenderCaseIdentifier;

        [XmlAttribute()]
        public string MI_LTVPercent;

        [XmlAttribute()]
        public MI_MIApplicationTypeEnumerated MIApplicationType;

        [XmlAttribute()]
        public string MICertificateExpirationDate;

        [XmlAttribute()]
        public string MIRateQuoteExpirationDate;

        [XmlAttribute()]
        public string MICertificateIdentifier;

        [XmlAttribute()]
        public MI_MICertificateTypeEnumerated MICertificateType;

        [XmlAttribute()]
        public string MICommentDescription;

        [XmlAttribute()]
        public string MICompanyName;

        [XmlAttribute()]
        public string MICoveragePercent;

        [XmlAttribute()]
        public MI_MIDecisionTypeEnumerated MIDecisionType;

        [XmlAttribute()]
        public MI_MIDurationTypeEnumerated MIDurationType;

        [XmlAttribute()]
        public string MIInitialPremiumAmount;

        [XmlAttribute()]
        public MI_MIInitialPremiumAtClosingTypeEnumerated MIInitialPremiumAtClosingType;

        [XmlAttribute()]
        public string MIInitialPremiumRateDurationMonths;

        [XmlAttribute()]
        public string MIInitialPremiumRatePercent;

        [XmlAttribute()]
        public string MILenderIdentifier;

        [XmlAttribute()]
        public string MIPremiumFromClosingAmount;

        [XmlAttribute()]
        public MI_MIPremiumPaymentTypeEnumerated MIPremiumPaymentType;

        [XmlAttribute()]
        public MI_MIPremiumRatePlanTypeEnumerated MIPremiumRatePlanType;

        [XmlAttribute()]
        public MI_MIRenewalCalculationTypeEnumerated MIRenewalCalculationType;

        [XmlAttribute()]
        public string MITransactionIdentifier;

        [XmlElement("MI_RENEWAL_PREMIUM")]
        public List<MIRenewalPremium> MIRenewalPremiumList = new List<MIRenewalPremium>();

        [XmlElement("MI_PREMIUM_TAX")]
        public List<MIPremiumTax> MIPremiumTaxList = new List<MIPremiumTax>();

        [XmlElement("BORROWER")]
        public Borrower Borrower = new Borrower();

        [XmlElement("CONTACT_DETAIL")]
        public List<ContactDetail> ContactDetailList = new List<ContactDetail>();

        [XmlElement("STATUS")]
        public Status Status = new Status();

        [XmlElement("KEY")]
        public List<Key> KeyList = new List<Key>();

        [XmlElement("EMBEDDED_FILE")]
        public List<EmbeddedFile> EmbeddedFileList = new List<EmbeddedFile>();
    }
}
