﻿// <copyright file="EmbeddedFile.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 6:15:40 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 EMBEDDED_FILE element.
    /// </summary>
    public class EmbeddedFile
    {
        [XmlAttribute()]
        public string MISMOVersionID;

        [XmlAttribute()]
        public string _ID;

        [XmlAttribute()]
        public string _Type;

        [XmlAttribute()]
        public string _Version;

        [XmlAttribute()]
        public string _Name;

        [XmlAttribute()]
        public string _EncodingType;

        [XmlAttribute()]
        public string _Description;

        [XmlAttribute()]
        public string MIMEType;

        [XmlElement("DOCUMENT")]
        public Document Document;
    }
}
