﻿// <copyright file="MIPremiumTax.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 6:09:02 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 MI_PREMIUM_TAX element.
    /// </summary>
    public class MIPremiumTax
    {
        [XmlAttribute()]
        public MI_MIPremiumTaxCodeTypeEnumerated _CodeType;

        [XmlAttribute()]
        public string _CodePercent;

        [XmlAttribute()]
        public string _CodeAmount;
    }
}
