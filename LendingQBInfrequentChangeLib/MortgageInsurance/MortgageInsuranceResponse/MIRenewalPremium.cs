﻿// <copyright file="MIRenewalPremium.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:59:14 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 MI_RENEWAL_PREMIUM element.
    /// </summary>
    public class MIRenewalPremium
    {
        [XmlAttribute()]
        public MI_MIRenewalPremiumSequenceEnumerated _Sequence;

        [XmlAttribute()]
        public string _Rate;

        [XmlAttribute()]
        public string _RateDurationMonths;
    }
}
