﻿// <copyright file="Response.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:14:07 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPONSE element.
    /// </summary>
    public class Response
    {
        [XmlAttribute()]
        public string _ID;

        [XmlAttribute()]
        public string ResponseDateTime;

        [XmlAttribute()]
        public string InternalAccountIdentifier;

        [XmlAttribute()]
        public string LoginAccountIdentifier;

        [XmlAttribute()]
        public string LoginAccountPassword;

        [XmlElement("KEY")]
        public List<Key> KeyList = new List<Key>();

        [XmlElement("RESPONSE_DATA")]
        public List<ResponseData> ResponseDataList = new List<ResponseData>();

        [XmlElement("STATUS")]
        public List<Status> StatusList = new List<Status>();
    }
}
