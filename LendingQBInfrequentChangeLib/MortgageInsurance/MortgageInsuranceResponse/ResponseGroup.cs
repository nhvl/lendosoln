﻿// <copyright file="ResponseGroup.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 3:57:16 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPONSE_GROUP element.
    /// </summary>
    [XmlRoot("RESPONSE_GROUP")]
    public class ResponseGroup
    {
        [XmlAttribute()]
        public string MISMOVersionID;

        [XmlAttribute()]
        public string _ID;

        [XmlElement("RESPONDING_PARTY")]
        public RespondingParty RespondingParty = new RespondingParty();

        [XmlElement("RESPOND_TO_PARTY")]
        public RespondToParty RespondToParty = new RespondToParty();

        [XmlElement("RESPONSE")]
        public List<Response> ResponseList = new List<Response>();
    }
}
