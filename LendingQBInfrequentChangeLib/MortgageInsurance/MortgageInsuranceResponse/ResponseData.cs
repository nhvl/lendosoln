﻿// <copyright file="ResponseData.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/19/2014 5:38:55 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceResponse
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPONSE_DATA element.
    /// </summary>
    public class ResponseData
    {
        [XmlElement("MI_RESPONSE")]
        public MIResponse MIResponse = new MIResponse();
    }
}
