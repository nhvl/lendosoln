﻿// <copyright file="ResponseGroup.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 10:56:59 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPONSE_GROUP element.
    /// </summary>
    [XmlRoot("RESPONSE_GROUP")]
    public class ResponseGroup
    {
        [XmlAttribute()]
        public string MISMOVersionID = "2.3.1";
        public bool ShouldSerializeMISMOVersionID() { return !string.IsNullOrEmpty(this.MISMOVersionID); }

        [XmlAttribute()]
        public string _ID;
        public bool ShouldSerialize_ID() { return !string.IsNullOrEmpty(this._ID); }

        [XmlElement(Order = 1, ElementName = "RESPONDING_PARTY")]
        public RespondingParty RespondingParty;
        [XmlIgnore()]
        public bool RespondingPartySpecified { get { return this.RespondingParty != null; } set { } }

        [XmlElement(Order = 2, ElementName = "RESPOND_TO_PARTY")]
        public RespondToParty RespondToParty;
        [XmlIgnore()]
        public bool RespondToPartySpecified { get { return this.RespondToParty != null; } set { } }

        [XmlElement(Order = 3, ElementName = "RESPONSE")]
        public Response Response;
        [XmlIgnore()]
        public bool ResponseSpecified { get { return this.Response != null; } set { } }
    }
}
