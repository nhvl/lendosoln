﻿// <copyright file="Response.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 11:25:21 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPONSE element.
    /// </summary>
    public class Response
    {
        [XmlAttribute()]
        public string _ID;
        public bool ShouldSerialize_ID() { return !string.IsNullOrEmpty(this._ID); }

        [XmlAttribute()]
        public string ResponseDateTime;
        public bool ShouldSerializeResponseDateTime() { return !string.IsNullOrEmpty(this.ResponseDateTime); }

        [XmlAttribute()]
        public string InternalAccountIdentifier;
        public bool ShouldSerializeInternalAccountIdentifier() { return !string.IsNullOrEmpty(this.InternalAccountIdentifier); }

        [XmlAttribute()]
        public string LoginAccountIdentifier;
        public bool ShouldSerializeLoginAccountIdentifier() { return !string.IsNullOrEmpty(this.LoginAccountIdentifier); }

        [XmlAttribute()]
        public string LoginAccountPassword;
        public bool ShouldSerializeLoginAccountPassword() { return !string.IsNullOrEmpty(this.LoginAccountPassword); }

        [XmlElement(Order = 1, ElementName = "KEY")]
        public List<Key> KeyList = new List<Key>();

        [XmlElement(Order = 2, ElementName = "RESPONSE_DATA")]
        public ResponseData ResponseData;
        [XmlIgnore()]
        public bool ResponseDataSpecified { get { return this.ResponseData != null; } set { } }

        [XmlElement(Order = 3, ElementName = "STATUS")]
        public List<Status> StatusList = new List<Status>();
    }
}
