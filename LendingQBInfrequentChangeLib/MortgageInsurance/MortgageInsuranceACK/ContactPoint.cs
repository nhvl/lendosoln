﻿// <copyright file="ContactPoint.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 11:33:44 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 CONTACT_POINT element. 
    /// </summary>
    public class ContactPoint
    {
        [XmlIgnore()]
        public MI_ContactPointRoleTypeEnumerated roleType;
        [XmlAttribute(AttributeName = "_RoleType")]
        public string _RoleTypeSerialized { get { return Utilities.ConvertEnumToString(this.roleType); } set { } }
        public bool ShouldSerialize_RoleTypeSerialized() { return !string.IsNullOrEmpty(this._RoleTypeSerialized); }

        [XmlIgnore()]
        public MI_ContactPointTypeEnumerated type;
        [XmlAttribute(AttributeName = "_Type")]
        public string _TypeSerialized { get { return Utilities.ConvertEnumToString(this.type); } set { } }
        public bool ShouldSerialize_TypeSerialized() { return !string.IsNullOrEmpty(this._TypeSerialized); }

        [XmlAttribute()]
        public string _TypeOtherDescription;
        public bool ShouldSerialize_TypeOtherDescription() { return !string.IsNullOrEmpty(this._TypeOtherDescription); }

        [XmlAttribute()]
        public string _Value;
        public bool ShouldSerialize_Value() { return !string.IsNullOrEmpty(this._Value); }

        [XmlIgnore()]
        public E_YNIndicator preferenceIndicator;
        [XmlAttribute(AttributeName = "_PreferenceIndicator")]
        public string _PreferenceIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.preferenceIndicator); } set { } }
        public bool ShouldSerialize_PreferenceIndicatorSerialized() { return !string.IsNullOrEmpty(this._PreferenceIndicatorSerialized); }
    }
}
