﻿// <copyright file="Key.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 11:35:35 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 KEY element.
    /// </summary>
    public class Key
    {
        [XmlAttribute()]
        public string _Name;
        public bool ShouldSerialize_Name() { return !string.IsNullOrEmpty(this._Name); }

        [XmlAttribute()]
        public string _Value;
        public bool ShouldSerialize_Value() { return !string.IsNullOrEmpty(this._Value); }
    }
}
