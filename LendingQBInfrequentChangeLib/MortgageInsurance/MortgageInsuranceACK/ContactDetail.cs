﻿// <copyright file="ContactDetail.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 11:31:53 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 CONTACT_DETAIL element. 
    /// </summary>
    public class ContactDetail
    {
        [XmlAttribute()]
        public string _Name;
        public bool ShouldSerialize_Name() { return !string.IsNullOrEmpty(this._Name); }

        [XmlElement(Order = 1, ElementName = "CONTACT_POINT")]
        public List<ContactPoint> ContactPointList = new List<ContactPoint>();
    }
}
