﻿// <copyright file="RespondingParty.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 11:11:48 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPONDING_PARTY element. 
    /// </summary>
    public class RespondingParty
    {
        [XmlAttribute()]
        public string _Name;
        public bool ShouldSerialize_Name() { return !string.IsNullOrEmpty(this._Name); }

        [XmlAttribute()]
        public string _StreetAddress;
        public bool ShouldSerialize_StreetAddress() { return !string.IsNullOrEmpty(this._StreetAddress); }

        [XmlAttribute()]
        public string _StreetAddress2;
        public bool ShouldSerialize_StreetAddress2() { return !string.IsNullOrEmpty(this._StreetAddress2); }

        [XmlAttribute()]
        public string _City;
        public bool ShouldSerialize_City() { return !string.IsNullOrEmpty(this._City); }

        [XmlAttribute()]
        public string _State;
        public bool ShouldSerialize_State() { return !string.IsNullOrEmpty(this._State); }

        [XmlAttribute()]
        public string _PostalCode;
        public bool ShouldSerialize_PostalCode() { return !string.IsNullOrEmpty(this._PostalCode); }

        [XmlAttribute()]
        public string _Identifier;
        public bool ShouldSerialize_Identifier() { return !string.IsNullOrEmpty(this._Identifier); }

        [XmlElement(Order = 1, ElementName = "CONTACT_DETAIL")]
        public List<ContactDetail> ContactDetailList = new List<ContactDetail>();
    }
}
