﻿// <copyright file="Status.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 11:37:13 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 STATUS element. 
    /// </summary>
    public class Status
    {
        [XmlAttribute()]
        public string _Condition;
        public bool ShouldSerialize_Condition() { return !string.IsNullOrEmpty(this._Condition); }

        [XmlAttribute()]
        public string _Code;
        public bool ShouldSerialize_Code() { return !string.IsNullOrEmpty(this._Code); }

        [XmlAttribute()]
        public string _Name;
        public bool ShouldSerialize_Name() { return !string.IsNullOrEmpty(this._Name); }

        [XmlAttribute()]
        public string _Description;
        public bool ShouldSerialize_Description() { return !string.IsNullOrEmpty(this._Description); }
    }
}
