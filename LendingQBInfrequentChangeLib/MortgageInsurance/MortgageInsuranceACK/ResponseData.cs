﻿// <copyright file="ResponseData.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 11:39:38 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceACK
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 RESPONSE_DATA element.
    /// </summary>
    public class ResponseData
    {
        [XmlElement(Order = 1, ElementName = "MI_RESPONSE")]
        public MIResponse MIResponse;
        [XmlIgnore()]
        public bool MIResponseSpecified { get { return this.MIResponse != null; } set { } }
    }
}
