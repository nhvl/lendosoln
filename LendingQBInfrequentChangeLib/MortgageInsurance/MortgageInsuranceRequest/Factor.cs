﻿// <copyright file="Factor.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 6:59:26 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 _FACTOR element.
    /// </summary>
    public class Factor
    {
        [XmlAttribute()]
        public string _Code;
        public bool ShouldSerialize_Code() { return !string.IsNullOrEmpty(this._Code); }

        [XmlAttribute()]
        public string _Text;
        public bool ShouldSerialize_Text() { return !string.IsNullOrEmpty(this._Text); }
    }
}
