﻿// <copyright file="PreferredResponse.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 10:44:07 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 PREFERRED_RESPONSE element. 
    /// </summary>
    public class PreferredResponse
    {
        [XmlIgnore()]
        public MI_PreferredResponseFormatEnumerated format;
        [XmlAttribute(AttributeName = "_Format")]
        public string _FormatSerialized { get { return Utilities.ConvertEnumToString(this.format); } set { } }
        public bool ShouldSerialize_FormatSerialized() { return !string.IsNullOrEmpty(this._FormatSerialized); }

        [XmlIgnore()]
        public MI_PreferredResponseMethodEnumerated method;
        [XmlAttribute(AttributeName = "_Method")]
        public string _MethodSerialized { get { return Utilities.ConvertEnumToString(this.method); } set { } }
        public bool ShouldSerialize_MethodSerialized() { return !string.IsNullOrEmpty(this._MethodSerialized); }

        [XmlAttribute()]
        public string _Destination;
        public bool ShouldSerialize_Destination() { return !string.IsNullOrEmpty(this._Destination); }

        [XmlAttribute()]
        public string _FormatOtherDescription;
        public bool ShouldSerialize_FormatOtherDescription() { return !string.IsNullOrEmpty(this._FormatOtherDescription); }

        [XmlAttribute()]
        public string _MethodOther;
        public bool ShouldSerialize_MethodOther() { return !string.IsNullOrEmpty(this._MethodOther); }

        [XmlIgnore()]
        public E_YNIndicator useEmbeddedFileIndicator;
        [XmlAttribute(AttributeName = "_UseEmbeddedFileIndicator")]
        public string _UseEmbeddedFileIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.useEmbeddedFileIndicator); } set { } }
        public bool ShouldSerialize_UseEmbeddedFileIndicatorSerialized() { return !string.IsNullOrEmpty(this._UseEmbeddedFileIndicatorSerialized); }

        [XmlAttribute()]
        public string MIMEType;
        public bool ShouldSerializeMIMEType() { return !string.IsNullOrEmpty(this.MIMEType); }

        [XmlAttribute()]
        public string _VersionIdentifier;
        public bool ShouldSerialize_VersionIdentifier() { return !string.IsNullOrEmpty(this._VersionIdentifier); }
    }
}
