﻿// <copyright file="RequestGroup.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/17/2014 4:41:03 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 REQUEST_GROUP element.
    /// </summary>
    [XmlRoot("REQUEST_GROUP")]
    public class RequestGroup
    {
        [XmlAttribute()]
        public string MISMOVersionID = "2.3.1";
        public bool ShouldSerializeMISMOVersionID() { return !string.IsNullOrEmpty(this.MISMOVersionID); }

        [XmlAttribute()]
        public string _ID;
        public bool ShouldSerialize_ID() { return !string.IsNullOrEmpty(this._ID); }
        
        [XmlElement(Order = 1, ElementName = "REQUESTING_PARTY")]
        public RequestingParty RequestingParty;
        [XmlIgnore()]
        public bool RequestingPartySpecified { get { return this.RequestingParty != null; } set { } }

        [XmlElement(Order = 2, ElementName = "RECEIVING_PARTY")]
        public ReceivingParty ReceivingParty;
        [XmlIgnore()]
        public bool ReceivingPartySpecified { get { return this.ReceivingParty != null; } set { } }
        
        [XmlElement(Order = 3, ElementName = "SUBMITTING_PARTY")]
        public SubmittingParty SubmittingParty;
        [XmlIgnore()]
        public bool SubmittingPartySpecified { get { return this.SubmittingParty != null; } set { } }

        [XmlElement(Order = 4, ElementName = "REQUEST")]
        public Request Request;
        [XmlIgnore()]
        public bool RequestSpecified { get { return this.Request != null; } set { } }
    }
}