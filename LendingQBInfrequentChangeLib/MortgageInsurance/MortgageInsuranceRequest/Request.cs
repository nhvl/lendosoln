﻿// <copyright file="Request.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 10:26:41 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 REQUEST element.
    /// </summary>
    public class Request
    {
        [XmlAttribute()]
        public string RequestDatetime;
        public bool ShouldSerializeRequestDatetime() { return !string.IsNullOrEmpty(this.RequestDatetime); }

        [XmlAttribute()]
        public string InternalAccountIdentifier;
        public bool ShouldSerializeInternalAccountIdentifier() { return !string.IsNullOrEmpty(this.InternalAccountIdentifier); }

        [XmlAttribute()]
        public string LoginAccountIdentifier;
        public bool ShouldSerializeLoginAccountIdentifier() { return !string.IsNullOrEmpty(this.LoginAccountIdentifier); }

        [XmlAttribute()]
        public string LoginAccountPassword;
        public bool ShouldSerializeLoginAccountPassword() { return !string.IsNullOrEmpty(this.LoginAccountPassword); }

        [XmlAttribute()]
        public string RequestingPartyBranchIdentifier;
        public bool ShouldSerializeRequestingPartyBranchIdentifier() { return !string.IsNullOrEmpty(this.RequestingPartyBranchIdentifier); }

        [XmlAttribute()]
        public string _ID;
        public bool ShouldSerialize_ID() { return !string.IsNullOrEmpty(this._ID); }

        [XmlElement(Order = 1, ElementName = "KEY")]
        public List<Key> KeyList = new List<Key>();

        [XmlElement(Order = 2, ElementName = "REQUEST_DATA")]
        public RequestData RequestData;
        [XmlIgnore()]
        public bool RequestDataSpecified { get { return this.RequestData != null; } set { } }
    }
}
