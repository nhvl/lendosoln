﻿// <copyright file="MIRequest.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 2:24:29 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 MI_REQUEST element.
    /// </summary>
    public class MIRequest
    {
        [XmlIgnore()]
        public MI_MIApplicationTypeEnumerated MIApplicationType;
        [XmlAttribute(AttributeName = "MIApplicationType")]
        public string MIApplicationTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIApplicationType); } set { } }
        public bool ShouldSerializeMIApplicationTypeSerialized() { return !string.IsNullOrEmpty(this.MIApplicationTypeSerialized); }

        [XmlIgnore()]
        public E_YNIndicator MICaptiveReinsuranceIndicator;
        [XmlAttribute(AttributeName = "MICaptiveReinsuranceIndicator")]
        public string MICaptiveReinsuranceIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.MICaptiveReinsuranceIndicator); } set { } }
        public bool ShouldSerializeMICaptiveReinsuranceIndicatorSerialized() { return !string.IsNullOrEmpty(this.MICaptiveReinsuranceIndicatorSerialized); }

        [XmlAttribute()]
        public string MICertificateIdentifier;
        public bool ShouldSerializeMICertificateIdentifier() { return !string.IsNullOrEmpty(this.MICertificateIdentifier); }

        [XmlIgnore()]
        public MI_MICertificateTypeEnumerated MICertificateType;
        [XmlAttribute(AttributeName = "MICertificateType")]
        public string MICertificateTypeSerialized { get { return Utilities.ConvertEnumToString(this.MICertificateType); } set { } }
        public bool ShouldSerializeMICertificateTypeSerialized() { return !string.IsNullOrEmpty(this.MICertificateTypeSerialized); }

        [XmlAttribute()]
        public string MICompanyName;
        public bool ShouldSerializeMICompanyName() { return !string.IsNullOrEmpty(this.MICompanyName); }

        [XmlIgnore()]
        public MI_MICoveragePlanTypeEnumerated MICoveragePlanType;
        [XmlAttribute(AttributeName = "MICoveragePlanType")]
        public string MICoveragePlanTypeSerialized { get { return Utilities.ConvertEnumToString(this.MICoveragePlanType); } set { } }
        public bool ShouldSerializeMICoveragePlanTypeSerialized() { return !string.IsNullOrEmpty(this.MICoveragePlanTypeSerialized); }

        [XmlAttribute()]
        public string MILoanLevelCreditScoreValue;
        public bool ShouldSerializeMILoanLevelCreditScoreValue() { return !string.IsNullOrEmpty(this.MILoanLevelCreditScoreValue); }

        [XmlIgnore()]
        public MI_MIDurationTypeEnumerated MIDurationType;
        [XmlAttribute(AttributeName = "MIDurationType")]
        public string MIDurationTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIDurationType); } set { } }
        public bool ShouldSerializeMIDurationTypeSerialized() { return !string.IsNullOrEmpty(this.MIDurationTypeSerialized); }

        [XmlIgnore()]
        public E_YNIndicator MIEmployeeLoanIndicator;
        [XmlAttribute(AttributeName = "MIEmployeeLoanIndicator")]
        public string MIEmployeeLoanIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.MIEmployeeLoanIndicator); } set { } }
        public bool ShouldSerializeMIEmployeeLoanIndicatorSerialized() { return !string.IsNullOrEmpty(this.MIEmployeeLoanIndicatorSerialized); }

        [XmlIgnore()]
        public MI_MIInitialPremiumAtClosingTypeEnumerated MIInitialPremiumAtClosingType;
        [XmlAttribute(AttributeName = "MIInitialPremiumAtClosingType")]
        public string MIInitialPremiumAtClosingTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIInitialPremiumAtClosingType); } set { } }
        public bool ShouldSerializeMIInitialPremiumAtClosingTypeSerialized() { return !string.IsNullOrEmpty(this.MIInitialPremiumAtClosingTypeSerialized); }

        [XmlAttribute()]
        public string MILenderIdentifier;
        public bool ShouldSerializeMILenderIdentifier() { return !string.IsNullOrEmpty(this.MILenderIdentifier); }

        [XmlAttribute()]
        public string MILenderSpecialProgramType;
        public bool ShouldSerializeMILenderSpecialProgramType() { return !string.IsNullOrEmpty(this.MILenderSpecialProgramType); }

        [XmlIgnore()]
        public E_YNIndicator MIPremiumFinancedIndicator;
        [XmlAttribute(AttributeName = "MIPremiumFinancedIndicator")]
        public string MIPremiumFinancedIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.MIPremiumFinancedIndicator); } set { } }
        public bool ShouldSerializeMIPremiumFinancedIndicatorSerialized() { return !string.IsNullOrEmpty(this.MIPremiumFinancedIndicatorSerialized); }

        [XmlIgnore()]
        public MI_MIPremiumPaymentTypeEnumerated MIPremiumPaymentType;
        [XmlAttribute(AttributeName = "MIPremiumPaymentType")]
        public string MIPremiumPaymentTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIPremiumPaymentType); } set { } }
        public bool ShouldSerializeMIPremiumPaymentTypeSerialized() { return !string.IsNullOrEmpty(this.MIPremiumPaymentTypeSerialized); }

        [XmlIgnore()]
        public MI_MIPremiumRatePlanTypeEnumerated MIPremiumRatePlanType;
        [XmlAttribute(AttributeName = "MIPremiumRatePlanType")]
        public string MIPremiumRatePlanTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIPremiumRatePlanType); } set { } }
        public bool ShouldSerializeMIPremiumRatePlanTypeSerialized() { return !string.IsNullOrEmpty(this.MIPremiumRatePlanTypeSerialized); }

        [XmlIgnore()]
        public MI_MIPremiumRefundableTypeEnumerated MIPremiumRefundableType;
        [XmlAttribute(AttributeName = "MIPremiumRefundableType")]
        public string MIPremiumRefundableTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIPremiumRefundableType); } set { } }
        public bool ShouldSerializeMIPremiumRefundableTypeSerialized() { return !string.IsNullOrEmpty(this.MIPremiumRefundableTypeSerialized); }

        [XmlAttribute()]
        public string MIPremiumTermMonths;
        public bool ShouldSerializeMIPremiumTermMonths() { return !string.IsNullOrEmpty(this.MIPremiumTermMonths); }

        [XmlIgnore()]
        public MI_MIReducedLoanDocumentationTypeEnumerated MIReducedLoanDocumentationType;
        [XmlAttribute(AttributeName = "MIReducedLoanDocumentationType")]
        public string MIReducedLoanDocumentationTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIReducedLoanDocumentationType); } set { } }
        public bool ShouldSerializeMIReducedLoanDocumentationTypeSerialized() { return !string.IsNullOrEmpty(this.MIReducedLoanDocumentationTypeSerialized); }

        [XmlIgnore()]
        public E_YNIndicator MIRelocationLoanIndicator;
        [XmlAttribute(AttributeName = "MIRelocationLoanIndicator")]
        public string MIRelocationLoanIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.MIRelocationLoanIndicator); } set { } }
        public bool ShouldSerializeMIRelocationLoanIndicatorSerialized() { return !string.IsNullOrEmpty(this.MIRelocationLoanIndicatorSerialized); }

        [XmlIgnore()]
        public MI_MIRenewalCalculationTypeEnumerated MIRenewalCalculationType;
        [XmlAttribute(AttributeName = "MIRenewalCalculationType")]
        public string MIRenewalCalculationTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIRenewalCalculationType); } set { } }
        public bool ShouldSerializeMIRenewalCalculationTypeSerialized() { return !string.IsNullOrEmpty(this.MIRenewalCalculationTypeSerialized); }

        [XmlIgnore()]
        public MI_MIRequestTypeEnumerated MIRequestType;
        [XmlAttribute(AttributeName = "MIRequestType")]
        public string MIRequestTypeSerialized { get { return Utilities.ConvertEnumToString(this.MIRequestType); } set { } }
        public bool ShouldSerializeMIRequestTypeSerialized() { return !string.IsNullOrEmpty(this.MIRequestTypeSerialized); }

        [XmlAttribute()]
        public string MISpecialPricingType;
        public bool ShouldSerializeMISpecialPricingType() { return !string.IsNullOrEmpty(this.MISpecialPricingType); }

        [XmlIgnore()]
        public MI_MISubPrimeProgramTypeEnumerated MISubPrimeProgramType;
        [XmlAttribute(AttributeName = "MISubPrimeProgramType")]
        public string MISubPrimeProgramTypeSerialized { get { return Utilities.ConvertEnumToString(this.MISubPrimeProgramType); } set { } }
        public bool ShouldSerializeMISubPrimeProgramTypeSerialized() { return !string.IsNullOrEmpty(this.MISubPrimeProgramTypeSerialized); }

        [XmlAttribute()]
        public string MITransactionIdentifier;
        public bool ShouldSerializeMITransactionIdentifier() { return !string.IsNullOrEmpty(this.MITransactionIdentifier); }

        [XmlAttribute()]
        public string DesktopUnderwriterCaseFileIdentifier;
        public bool ShouldSerializeDesktopUnderwriterCaseFileIdentifier() { return !string.IsNullOrEmpty(this.DesktopUnderwriterCaseFileIdentifier); }

        [XmlIgnore()]
        public MI_DesktopUnderwriterRecommendationTypeEnumerated DesktopUnderwriterRecommendationType;
        [XmlAttribute(AttributeName = "DesktopUnderwriterRecommendationType")]
        public string DesktopUnderwriterRecommendationTypeSerialized { get { return Utilities.ConvertEnumToString(this.DesktopUnderwriterRecommendationType); } set { } }
        public bool ShouldSerializeDesktopUnderwriterRecommendationTypeSerialized() { return !string.IsNullOrEmpty(this.DesktopUnderwriterRecommendationTypeSerialized); }

        [XmlAttribute()]
        public string LoanProspectorKeyIdentifier;
        public bool ShouldSerializeLoanProspectorKeyIdentifier() { return !string.IsNullOrEmpty(this.LoanProspectorKeyIdentifier); }

        [XmlIgnore()]
        public E_YNIndicator LoanProspectorAcceptPlusEligibleIndicator;
        [XmlAttribute(AttributeName = "LoanProspectorAcceptPlusEligibleIndicator")]
        public string LoanProspectorAcceptPlusEligibleIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.LoanProspectorAcceptPlusEligibleIndicator); } set { } }
        public bool ShouldSerializeLoanProspectorAcceptPlusEligibleIndicatorSerialized() { return !string.IsNullOrEmpty(this.LoanProspectorAcceptPlusEligibleIndicatorSerialized); }

        [XmlIgnore()]
        public MI_LoanProspectorCreditRiskClassificationTypeEnumerated LoanProspectorCreditRiskClassificationType;
        [XmlAttribute(AttributeName = "LoanProspectorCreditRiskClassificationType")]
        public string LoanProspectorCreditRiskClassificationTypeSerialized { get { return Utilities.ConvertEnumToString(this.LoanProspectorCreditRiskClassificationType); } set { } }
        public bool ShouldSerializeLoanProspectorCreditRiskClassificationTypeSerialized() { return !string.IsNullOrEmpty(this.LoanProspectorCreditRiskClassificationTypeSerialized); }

        [XmlAttribute()]
        public string LoanProspectorCreditRiskClassificationDescription;
        public bool ShouldSerializeLoanProspectorCreditRiskClassificationDescription() { return !string.IsNullOrEmpty(this.LoanProspectorCreditRiskClassificationDescription); }

        [XmlIgnore()]
        public MI_LoanProspectorDocumentationClassificationTypeEnumerated LoanProspectorDocumentationClassificationType;
        [XmlAttribute(AttributeName = "LoanProspectorDocumentationClassificationType")]
        public string LoanProspectorDocumentationClassificationTypeSerialized { get { return Utilities.ConvertEnumToString(this.LoanProspectorDocumentationClassificationType); } set { } }
        public bool ShouldSerializeLoanProspectorDocumentationClassificationTypeSerialized() { return !string.IsNullOrEmpty(this.LoanProspectorDocumentationClassificationTypeSerialized); }

        [XmlAttribute()]
        public string LoanProspectorDocumentationClassificationDescription;
        public bool ShouldSerializeLoanProspectorDocumentationClassificationDescription() { return !string.IsNullOrEmpty(this.LoanProspectorDocumentationClassificationDescription); }

        [XmlIgnore()]
        public MI_LoanProspectorRiskGradeAssignedTypeEnumerated LoanProspectorRiskGradeAssignedType;
        [XmlAttribute(AttributeName = "LoanProspectorRiskGradeAssignedType")]
        public string LoanProspectorRiskGradeAssignedTypeSerialized { get { return Utilities.ConvertEnumToString(this.LoanProspectorRiskGradeAssignedType); } set { } }
        public bool ShouldSerializeLoanProspectorRiskGradeAssignedTypeSerialized() { return !string.IsNullOrEmpty(this.LoanProspectorRiskGradeAssignedTypeSerialized); }

        [XmlIgnore()]
        public MI_FreddieMacPurchaseEligibilityTypeEnumerated FreddieMacPurchaseEligibilityType;
        [XmlAttribute(AttributeName = "FreddieMacPurchaseEligibilityType")]
        public string FreddieMacPurchaseEligibilityTypeSerialized { get { return Utilities.ConvertEnumToString(this.FreddieMacPurchaseEligibilityType); } set { } }
        public bool ShouldSerializeFreddieMacPurchaseEligibilityTypeSerialized() { return !string.IsNullOrEmpty(this.FreddieMacPurchaseEligibilityTypeSerialized); }

        [XmlAttribute()]
        public string AutomatedUnderwritingSystemName;
        public bool ShouldSerializeAutomatedUnderwritingSystemName() { return !string.IsNullOrEmpty(this.AutomatedUnderwritingSystemName); }

        [XmlAttribute()]
        public string AutomatedUnderwritingSystemResultValue;
        public bool ShouldSerializeAutomatedUnderwritingSystemResultValue() { return !string.IsNullOrEmpty(this.AutomatedUnderwritingSystemResultValue); }

        [XmlIgnore()]
        public MI_InvestorProgramNameTypeEnumerated InvestorProgramNameType;
        [XmlAttribute(AttributeName = "InvestorProgramNameType")]
        public string InvestorProgramNameTypeSerialized { get { return Utilities.ConvertEnumToString(this.InvestorProgramNameType); } set { } }
        public bool ShouldSerializeInvestorProgramNameTypeSerialized() { return !string.IsNullOrEmpty(this.InvestorProgramNameTypeSerialized); }

        [XmlAttribute()]
        public string InvestorProgramNameTypeOtherDescription;
        public bool ShouldSerializeInvestorProgramNameTypeOtherDescription() { return !string.IsNullOrEmpty(this.InvestorProgramNameTypeOtherDescription); }

        [XmlElement(Order = 1, ElementName = "THIRD_PARTY_ORIGINATOR")]
        public ThirdPartyOriginator ThirdPartyOriginator;
        [XmlIgnore()]
        public bool ThirdPartyOriginatorSpecified { get { return this.ThirdPartyOriginator != null; } set { } }
    }
}
