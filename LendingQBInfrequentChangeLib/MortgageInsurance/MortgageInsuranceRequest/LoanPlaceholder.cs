﻿// <copyright file="LoanPlaceholder.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 12:15:27 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    /// <summary>
    /// This placeholder facilitates the insertion of a MISMO loan into the MI request. The MISMO 2.3.1 loan is the LOAN_APPLICATION block from LendersOffice.Conversions.Mismo23::LoanMismo23Exporter.
    /// </summary>
    public class LoanPlaceholder
    {
    }
}
