﻿// <copyright file="SubmittingParty.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/17/2014 6:12:33 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 SUBMITTING_PARTY element. 
    /// </summary>
    public class SubmittingParty
    {
        [XmlIgnore()]
        private int sequenceIdentifier;
        [XmlIgnore()]
        public int SequenceIdentifier { get { return this.sequenceIdentifier; } set { this.sequenceIdentifier = value; this.SequenceIdentifierProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceIdentifierProvided = false;
        [XmlAttribute(AttributeName = "_SequenceIdentifier")]
        public string SequenceIdentifierSerialized { get { return this.SequenceIdentifier.ToString(); } set { } }
        public bool ShouldSerializeSequenceIdentifierSerialized() { return this.SequenceIdentifierProvided; }

        [XmlAttribute()]
        public string _Name;
        public bool ShouldSerialize_Name() { return !string.IsNullOrEmpty(this._Name); }

        [XmlAttribute()]
        public string _StreetAddress;
        public bool ShouldSerialize_StreetAddress() { return !string.IsNullOrEmpty(this._StreetAddress); }

        [XmlAttribute()]
        public string _StreetAddress2;
        public bool ShouldSerialize_StreetAddress2() { return !string.IsNullOrEmpty(this._StreetAddress2); }

        [XmlAttribute()]
        public string _City;
        public bool ShouldSerialize_City() { return !string.IsNullOrEmpty(this._City); }

        [XmlAttribute()]
        public string _State;
        public bool ShouldSerialize_State() { return !string.IsNullOrEmpty(this._State); }

        [XmlAttribute()]
        public string _PostalCode;
        public bool ShouldSerialize_PostalCode() { return !string.IsNullOrEmpty(this._PostalCode); }

        [XmlAttribute()]
        public string LoginAccountIdentifier;
        public bool ShouldSerializeLoginAccountIdentifier() { return !string.IsNullOrEmpty(this.LoginAccountIdentifier); }

        [XmlAttribute()]
        public string LoginAccountPassword;
        public bool ShouldSerializeLoginAccountPassword() { return !string.IsNullOrEmpty(this.LoginAccountPassword); }

        [XmlAttribute()]
        public string _Identifier;
        public bool ShouldSerialize_Identifier() { return !string.IsNullOrEmpty(this._Identifier); }

        [XmlElement(Order = 1, ElementName = "CONTACT_DETAIL")]
        public List<ContactDetail> ContactDetailList = new List<ContactDetail>();

        [XmlElement(Order = 2, ElementName = "PREFERRED_RESPONSE")]
        public List<PreferredResponse> PreferredResponseList = new List<PreferredResponse>();
    }
}