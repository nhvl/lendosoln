﻿// <copyright file="MIApplication.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 11:51:19 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 MI_APPLICATION element.
    /// </summary>
    public class MIApplication
    {
        [XmlAttribute()]
        public string MISMOVersionID = "2.3.1";
        public bool ShouldSerializeMISMOVersionID() { return !string.IsNullOrEmpty(this.MISMOVersionID); }

        [XmlElement(Order = 1, ElementName = "MI_REQUEST")]
        public MIRequest MIRequest;
        [XmlIgnore()]
        public bool MIRequestSpecified { get { return this.MIRequest != null; } set { } }

        [XmlElement(Order = 2, ElementName = "REQUESTING_PARTY")]
        public RequestingParty RequestingParty;
        [XmlIgnore()]
        public bool RequestingPartySpecified { get { return this.RequestingParty != null; } set { } }

        [XmlElement(Order = 3, ElementName = "SUBMITTING_PARTY")]
        public SubmittingParty SubmittingParty;
        [XmlIgnore()]
        public bool SubmittingPartySpecified { get { return this.SubmittingParty != null; } set { } }

        [XmlElement(Order = 4, ElementName = "CREDIT_SCORE")]
        public List<CreditScore> CreditScoreList = new List<CreditScore>();

        [XmlElement(Order = 5, ElementName = "KEY")]
        public List<Key> KeyList = new List<Key>();

        //// Placeholder for the LOAN_APPLICATION from the MISMO 2.3.1 loan exporter (LendersOffice.Conversions.Mismo23::LoanMismo23Exporter).
        [XmlElement(Order = 6, ElementName = "LOAN_PLACEHOLDER")]
        public LoanPlaceholder LoanPlaceholder = new LoanPlaceholder();
        [XmlIgnore()]
        public bool LoanPlaceholderSpecified { get { return this.LoanPlaceholder != null; } set { } }
    }
}
