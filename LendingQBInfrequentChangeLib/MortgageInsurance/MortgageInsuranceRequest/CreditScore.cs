﻿// <copyright file="CreditScore.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 6:16:47 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 CREDIT_SCORE element. 
    /// </summary>
    public class CreditScore
    {
        [XmlAttribute()]
        public string MISMOVersionID = "2.3";
        public bool ShouldSerializeMISMOVersionID() { return !string.IsNullOrEmpty(this.MISMOVersionID); }

        [XmlAttribute()]
        public string CreditScoreID;
        public bool ShouldSerializeCreditScoreID() { return !string.IsNullOrEmpty(this.CreditScoreID); }

        [XmlAttribute()]
        public string BorrowerID;
        public bool ShouldSerializeBorrowerID() { return !string.IsNullOrEmpty(this.BorrowerID); }

        [XmlAttribute()]
        public string CreditFileID;
        public bool ShouldSerializeCreditFileID() { return !string.IsNullOrEmpty(this.CreditFileID); }

        [XmlAttribute()]
        public string CreditReportIdentifier;
        public bool ShouldSerializeCreditReportIdentifier() { return !string.IsNullOrEmpty(this.CreditReportIdentifier); }

        [XmlIgnore()]
        public MI_CreditRepositorySourceTypeEnumerated creditRepositorySourceType;
        [XmlAttribute(AttributeName = "CreditRepositorySourceType")]
        public string CreditRepositorySourceTypeSerialized { get { return Utilities.ConvertEnumToString(this.creditRepositorySourceType); } set { } }
        public bool ShouldSerializeCreditRepositorySourceTypeSerialized() { return !string.IsNullOrEmpty(this.CreditRepositorySourceTypeSerialized); }

        [XmlAttribute()]
        public string _Date;
        public bool ShouldSerialize_Date() { return !string.IsNullOrEmpty(this._Date); }

        [XmlIgnore()]
        public MI_CreditScoreExclusionReasonTypeEnumerated exclusionReasonType;
        [XmlAttribute(AttributeName = "_ExclusionReasonType")]
        public string _ExclusionReasonTypeSerialized { get { return Utilities.ConvertEnumToString(this.exclusionReasonType); } set { } }
        public bool ShouldSerialize_ExclusionReasonTypeSerialized() { return !string.IsNullOrEmpty(this._ExclusionReasonTypeSerialized); }

        [XmlIgnore()]
        public MI_CreditScoreModelNameTypeEnumerated modelNameType;
        [XmlAttribute(AttributeName = "_ModelNameType")]
        public string _ModelNameTypeSerialized { get { return Utilities.ConvertEnumToString(this.modelNameType); } set { } }
        public bool ShouldSerialize_ModelNameTypeSerialized() { return !string.IsNullOrEmpty(this._ModelNameTypeSerialized); }

        [XmlAttribute()]
        public string _ModelNameTypeOtherDescription;
        public bool ShouldSerialize_ModelNameTypeOtherDescription() { return !string.IsNullOrEmpty(this._ModelNameTypeOtherDescription); }

        [XmlAttribute()]
        public string _Value;
        public bool ShouldSerialize_Value() { return !string.IsNullOrEmpty(this._Value); }

        [XmlIgnore()]
        public MI_CreditReportTypeEnumerated creditReportType;
        [XmlAttribute(AttributeName = "CreditReportType")]
        public string CreditReportTypeSerialized { get { return Utilities.ConvertEnumToString(this.creditReportType); } set { } }
        public bool ShouldSerializeCreditReportTypeSerialized() { return !string.IsNullOrEmpty(this.CreditReportTypeSerialized); }

        [XmlAttribute()]
        public string CreditReportTypeOtherDescription;
        public bool ShouldSerializeCreditReportTypeOtherDescription() { return !string.IsNullOrEmpty(this.CreditReportTypeOtherDescription); }

        [XmlElement(Order = 1, ElementName = "_FACTOR")]
        public List<Factor> FactorList = new List<Factor>();
    }
}
