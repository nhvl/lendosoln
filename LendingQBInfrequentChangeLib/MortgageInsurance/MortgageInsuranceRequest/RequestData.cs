﻿// <copyright file="RequestData.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 11:46:30 AM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 REQUEST_DATA element.
    /// </summary>
    public class RequestData
    {
        [XmlElement(Order = 1, ElementName = "MI_APPLICATION")]
        public MIApplication MIApplication;
        [XmlIgnore()]
        public bool MIApplicationSpecified { get { return this.MIApplication != null; } set { } }
    }
}
