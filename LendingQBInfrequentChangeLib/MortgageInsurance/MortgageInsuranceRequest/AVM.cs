﻿// <copyright file="AVM.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/18/2014 5:58:01 PM
// </summary>
namespace Mismo231.MI.MortgageInsuranceRequest
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Data class for the MISMO 2.3.1 AVM element. 
    /// </summary>
    public class AVM
    {
        [XmlAttribute()]
        public string _Date;
        public bool ShouldSerialize_Date() { return !string.IsNullOrEmpty(this._Date); }

        [XmlIgnore()]
        public MI_AVMModelNameTypeEnumerated modelNameType;
        [XmlAttribute(AttributeName = "_ModelNameType")]
        public string _ModelNameTypeSerialized { get { return Utilities.ConvertEnumToString(this.modelNameType); } set { } }
        public bool ShouldSerialize_ModelNameTypeSerialized() { return !string.IsNullOrEmpty(this._ModelNameTypeSerialized); }

        [XmlAttribute()]
        public string _ModelNameTypeOtherDescription;
        public bool ShouldSerialize_ModelNameTypeOtherDescription() { return !string.IsNullOrEmpty(this._ModelNameTypeOtherDescription); }

        [XmlAttribute()]
        public string _IndicatedValueAmount;
        public bool ShouldSerialize_IndicatedValueAmount() { return !string.IsNullOrEmpty(this._IndicatedValueAmount); }

        [XmlAttribute()]
        public string _HighValueRangeAmount;
        public bool ShouldSerialize_HighValueRangeAmount() { return !string.IsNullOrEmpty(this._HighValueRangeAmount); }

        [XmlAttribute()]
        public string _LowValueRangeAmount;
        public bool ShouldSerialize_LowValueRangeAmount() { return !string.IsNullOrEmpty(this._LowValueRangeAmount); }

        [XmlAttribute()]
        public string _ConfidenceScoreIdentifier;
        public bool ShouldSerialize_ConfidenceScoreIdentifier() { return !string.IsNullOrEmpty(this._ConfidenceScoreIdentifier); }

        [XmlIgnore()]
        public E_YNIndicator confidenceScoreIndicator;
        [XmlAttribute(AttributeName = "_ConfidenceScoreIndicator")]
        public string _ConfidenceScoreIndicatorSerialized { get { return Utilities.ConvertEnumToString(this.confidenceScoreIndicator); } set { } }
        public bool ShouldSerialize_ConfidenceScoreIndicatorSerialized() { return !string.IsNullOrEmpty(this._ConfidenceScoreIndicatorSerialized); }
    }
}
