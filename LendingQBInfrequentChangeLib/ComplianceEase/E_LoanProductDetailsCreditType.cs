﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplianceEase
{
    public enum E_LoanProductDetailsCreditType
    {
        Undefined = 0,
        HELOC,
        MortgageLoan,
        ReverseMortgage
    }
}
