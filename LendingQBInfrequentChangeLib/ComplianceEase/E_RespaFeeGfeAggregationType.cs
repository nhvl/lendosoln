﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplianceEase
{
    public enum E_RespaFeeGfeAggregationType
    {
        Undefined = 0,
        ChosenInterestRateCreditOrCharge,
        GovernmentRecordingCharges,
        InitialDepositForYourEscrowAccount,
        LendersTitleInsurance,
        None,
        OurOriginationCharge,
        OwnersTitleInsurance,
        RequiredServicesYouCanShopFor,
        TitleServices,
        TransferTaxes,
    }
}
