﻿// <copyright file="E_DocumentType.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   April 28, 2015
// </summary>

namespace ComplianceEase
{
    /// <summary>
    /// Specifies how the disclosure was delivered.
    /// </summary>
    public enum E_DeliveryMethodType
    {
        /// <summary>
        /// Delivery by email or fax.
        /// </summary>
        ElectronicDelivery = 0,

        /// <summary>
        /// Delivery in person.
        /// </summary>
        InPerson,

        /// <summary>
        /// Delivery by mail.
        /// </summary>
        USPSFirstClassMail
    }
}