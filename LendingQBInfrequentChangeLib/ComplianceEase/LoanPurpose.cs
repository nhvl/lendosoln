// Generated by CodeMonkey on 11/15/2010 4:56:01 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace ComplianceEase
{
    public sealed class LoanPurpose : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "LOAN_PURPOSE"; }
        }

        public E_LoanPurposePropertyUsageType PropertyUsageType;
        private ConstructionRefinanceData m_ConstructionRefinanceData;
        public ConstructionRefinanceData ConstructionRefinanceData
        {
            get
            {
                if (null == m_ConstructionRefinanceData)
                {
                    m_ConstructionRefinanceData = new ConstructionRefinanceData();
                }
                return m_ConstructionRefinanceData;
            }
            set { m_ConstructionRefinanceData = value; }
        }

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "PropertyUsageType", out PropertyUsageType);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CONSTRUCTION_REFINANCE_DATA": ReadElement(reader, ConstructionRefinanceData); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "PropertyUsageType", PropertyUsageType);
            WriteElement(writer, m_ConstructionRefinanceData);
        }
        private void ReadAttribute(XmlReader reader, string attrName, out E_LoanPurposePropertyUsageType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Investment")
            {
                result = E_LoanPurposePropertyUsageType.Investment;
            }
            else if (s == "PrimaryResidence")
            {
                result = E_LoanPurposePropertyUsageType.PrimaryResidence;
            }
            else if (s == "SecondHome")
            {
                result = E_LoanPurposePropertyUsageType.SecondHome;
            }
            else
            {
                result = E_LoanPurposePropertyUsageType.Undefined;
            }
        }
        private void WriteAttribute(XmlWriter writer, string attrName, E_LoanPurposePropertyUsageType value)
        {
            switch (value)
            {
                case E_LoanPurposePropertyUsageType.Investment:
                    writer.WriteAttributeString(attrName, "Investment");
                    break;
                case E_LoanPurposePropertyUsageType.PrimaryResidence:
                    writer.WriteAttributeString(attrName, "PrimaryResidence");
                    break;
                case E_LoanPurposePropertyUsageType.SecondHome:
                    writer.WriteAttributeString(attrName, "SecondHome");
                    break;
            }
        }
    }
}
