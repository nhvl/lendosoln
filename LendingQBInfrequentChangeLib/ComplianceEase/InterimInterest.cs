namespace ComplianceEase
{
    using System;
    using System.Xml;

    public sealed class InterimInterest : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "INTERIM_INTEREST"; }
        }

        public string PaidNumberOfDays;

        public E_PerDiemCalculationMethodType PerDiemCalculationMethodType;

        public E_PerDiemCalculationScaleType PerDiemCalculationScaleType;

        public string AdditionalInterestDaysRequiringConsentCount;

        protected override void ReadAttributes(XmlReader reader)
        {
            this.ReadAttribute(reader, $"_{nameof(this.PaidNumberOfDays)}", out this.PaidNumberOfDays);
            this.ReadAttribute(reader, $"_{nameof(this.PerDiemCalculationMethodType)}", out this.PerDiemCalculationMethodType);
            this.ReadAttribute(reader, $"_{nameof(this.PerDiemCalculationScaleType)}", out this.PerDiemCalculationScaleType);
            this.ReadAttribute(reader, $"{nameof(this.AdditionalInterestDaysRequiringConsentCount)}", out this.AdditionalInterestDaysRequiringConsentCount);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            this.WriteAttribute(writer, $"_{nameof(this.PaidNumberOfDays)}", this.PaidNumberOfDays);
            this.WriteAttribute(writer, $"_{nameof(this.PerDiemCalculationMethodType)}", this.PerDiemCalculationMethodType);
            this.WriteAttribute(writer, $"_{nameof(this.PerDiemCalculationScaleType)}", this.PerDiemCalculationScaleType);
            this.WriteAttribute(writer, $"{nameof(this.AdditionalInterestDaysRequiringConsentCount)}", this.AdditionalInterestDaysRequiringConsentCount);            
        }

        private void ReadAttribute(XmlReader reader, string attrName, out E_PerDiemCalculationMethodType calculationMethod)
        {
            string value = this.ReadAttribute(reader, attrName);

            switch (value)
            {
                case "Default":
                    calculationMethod = E_PerDiemCalculationMethodType.Default;
                    break;
                case "360":
                    calculationMethod = E_PerDiemCalculationMethodType._360;
                    break;
                case "364":
                    calculationMethod = E_PerDiemCalculationMethodType._364;
                    break;
                case "365":
                    calculationMethod = E_PerDiemCalculationMethodType._365;
                    break;
                case "365.25":
                    calculationMethod = E_PerDiemCalculationMethodType._365_25;
                    break;
                case "365Or366":
                    calculationMethod = E_PerDiemCalculationMethodType._365Or366;
                    break;
                case "366":
                    calculationMethod = E_PerDiemCalculationMethodType._366;
                    break;
                default:
                    throw new ArgumentException($"PerDiemCalculationMethodType value \"{value}\" returned by ComplianceEase is not handled.");
                        
            }
        }

        private void ReadAttribute(XmlReader reader, string attrName, out E_PerDiemCalculationScaleType calculationScale)
        {
            string value = this.ReadAttribute(reader, attrName);

            switch (value)
            {
                case "Default":
                    calculationScale = E_PerDiemCalculationScaleType.Default;
                    break;
                case "2":
                    calculationScale = E_PerDiemCalculationScaleType._2;
                    break;
                case "4":
                    calculationScale = E_PerDiemCalculationScaleType._4;
                    break;
                default:
                    throw new ArgumentException($"PerDiemCalculationScaleType value \"{value}\" returned by ComplianceEase is not handled.");
            }
        }

        private void WriteAttribute(XmlWriter writer, string attrName, E_PerDiemCalculationMethodType value)
        {
            switch (value)
            {
                case E_PerDiemCalculationMethodType.Default:
                    writer.WriteAttributeString(attrName, "Default");
                    break;
                case E_PerDiemCalculationMethodType._360:
                    writer.WriteAttributeString(attrName, "360");
                    break;
                case E_PerDiemCalculationMethodType._364:
                    writer.WriteAttributeString(attrName, "364");
                    break;
                case E_PerDiemCalculationMethodType._365:
                    writer.WriteAttributeString(attrName, "365");
                    break;
                case E_PerDiemCalculationMethodType._365_25:
                    writer.WriteAttributeString(attrName, "365.25");
                    break;
                case E_PerDiemCalculationMethodType._365Or366:
                    writer.WriteAttributeString(attrName, "365Or366");
                    break;
                case E_PerDiemCalculationMethodType._366:
                    writer.WriteAttributeString(attrName, "366");
                    break;
                default:
                    throw new ArgumentException($"The PerDiemCalculationMethodType value \"{value}\" is not handled.");
            }
        }

        private void WriteAttribute(XmlWriter writer, string attrName, E_PerDiemCalculationScaleType value)
        {
            switch (value)
            {
                case E_PerDiemCalculationScaleType.Default:
                    writer.WriteAttributeString(attrName, "Default");
                    break;
                case E_PerDiemCalculationScaleType._2:
                    writer.WriteAttributeString(attrName, "2");
                    break;
                case E_PerDiemCalculationScaleType._4:
                    writer.WriteAttributeString(attrName, "4");
                    break;
                default:
                    throw new ArgumentException($"The PerDiemCalculationScaleType value \"{value}\" is not handled.");
            }
        }
    }
}
