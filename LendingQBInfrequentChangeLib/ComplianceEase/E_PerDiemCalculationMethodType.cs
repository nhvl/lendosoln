﻿namespace ComplianceEase
{
    /// <summary>
    /// The number of days in a year, used for calculating the daily dollar
    /// amount of interim or prepaid interest due.
    /// </summary>
    public enum E_PerDiemCalculationMethodType
    {
        /// <summary>
        /// Use the ComplianceEase default.
        /// </summary>
        Default,

        /// <summary>
        /// Use a 360-day year.
        /// </summary>
        _360,

        /// <summary>
        /// Use a 364-day year.
        /// </summary>
        _364,

        /// <summary>
        /// Use a 365-day year.
        /// </summary>
        _365,

        /// <summary>
        /// Use a 365.25-day year.
        /// </summary>
        _365_25,

        /// <summary>
        /// Use a 365 or 366-day year.
        /// </summary>
        _365Or366,

        /// <summary>
        /// Use a 366-day year.
        /// </summary>
        _366
    }
}
