﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplianceEase
{
    public enum E_LoanFeaturesRespaConformingYearType
    {
        Undefined = 0,
        Pre2010,
        January2010,
        October2015
    }
}
