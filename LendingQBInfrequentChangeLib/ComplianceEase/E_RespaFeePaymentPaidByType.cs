﻿namespace ComplianceEase
{
    public enum E_RespaFeePaymentPaidByType
    {
        Undefined,
        Buyer,
        Seller,
        Lender,
        Broker,
        Default,
    }
}