﻿namespace ComplianceEase
{
    using System.Xml;

    /// <summary>
    /// Represents a ComplianceEase LENDER_CREDITS element.
    /// </summary>
    public sealed class LenderCredits : AbstractXmlSerializable
    {
        /// <summary>
        /// The credit amount.
        /// </summary>
        private string amount;

        /// <summary>
        /// The date of the CoC triggering a lender credit..
        /// </summary>
        private string changedCircumstanceDate;

        /// <summary>
        /// The reason for the CoC triggering a lender credit.
        /// </summary>
        private string changedCircumstanceReason;

        /// <summary>
        /// Gets or sets the lender credit amount.
        /// </summary>
        /// <value>The credit amount.</value>
        public string Amount
        {
            get
            {
                return this.amount;
            }

            set
            {
                this.amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the date of the CoC.
        /// </summary>
        /// <value>The date of the CoC.</value>
        public string ChangedCircumstanceDate
        {
            get
            {
                return this.changedCircumstanceDate;
            }

            set
            {
                this.changedCircumstanceDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the reason for the CoC.
        /// </summary>
        /// <value>The reason for the CoC.</value>
        public string ChangedCircumstanceReason
        {
            get
            {
                return this.changedCircumstanceReason;
            }

            set
            {
                this.changedCircumstanceReason = value;
            }
        }

        /// <summary>
        /// Gets the name of the element.
        /// </summary>
        /// <value>The name of the element.</value>
        public override string XmlElementName
        {
            get
            {
                return "LENDER_CREDITS";
            }
        }

        /// <summary>
        /// Reads a LENDER_CREDITS element.
        /// </summary>
        /// <param name="reader">An XML reader.</param>
        protected override void ReadAttributes(XmlReader reader)
        {
            this.ReadAttribute(reader, "_Amount", out this.amount);
            this.ReadAttribute(reader, "ChangedCircumstanceDate", out this.changedCircumstanceDate);
            this.ReadAttribute(reader, "ChangedCircumstanceReason", out this.changedCircumstanceReason);
        }

        /// <summary>
        /// Writes a LENDER_CREDITS element.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            this.WriteAttribute(writer, "_Amount", this.amount);
            this.WriteAttribute(writer, "ChangedCircumstanceDate", this.changedCircumstanceDate);
            this.WriteAttribute(writer, "ChangedCircumstanceReason", this.changedCircumstanceReason);
        }
    }
}
