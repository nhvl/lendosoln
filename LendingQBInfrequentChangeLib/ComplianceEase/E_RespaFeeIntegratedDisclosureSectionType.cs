﻿// <copyright file="E_DocumentType.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   April 30, 2015
// </summary>

namespace ComplianceEase
{
    /// <summary>
    /// The title or description used to identify a primary section of the integrated disclosure document.
    /// </summary>
    public enum E_RespaFeeIntegratedDisclosureSectionType
    {
        /// <summary>
        /// An initial escrow payment made at closing.
        /// </summary>
        InitialEscrowPaymentAtClosing,

        /// <summary>
        /// Loan origination charges.
        /// </summary>
        OriginationCharges,

        /// <summary>
        /// Another option not listed here.
        /// </summary>
        Other,

        /// <summary>
        /// Prepaid items.
        /// </summary>
        Prepaids,

        /// <summary>
        /// Services the borrower chose not to shop for.
        /// </summary>
        ServicesBorrowerDidNotShopFor,

        /// <summary>
        /// Services the borrower chose to shop for.
        /// </summary>
        ServicesBorrowerDidShopFor,

        /// <summary>
        /// Services that cannot be shopped for.
        /// </summary>
        ServicesYouCannotShopFor,

        /// <summary>
        /// Services that can be shopped for.
        /// </summary>
        ServicesYouCanShopFor,

        /// <summary>
        /// Taxes and other government fees.
        /// </summary>
        TaxesAndOtherGovernmentFees
    }
}