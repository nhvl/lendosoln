﻿// <copyright file="E_DocumentType.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   April 28, 2015
// </summary>

namespace ComplianceEase
{
    using System;
    using System.Xml;

    /// <summary>
    /// Serializes information on the TILA-RESPA Integrated Disclosure documents.
    /// </summary>
    public sealed class IntegratedDisclosure : AbstractXmlSerializable
    {
        /// <summary>
        /// Gets the name of the XML element.
        /// </summary>
        public override string XmlElementName
        {
            get { return "INTEGRATED_DISCLOSURE"; }
        }

        /// <summary>
        /// The delivery date of the initial Closing Disclosure.
        /// </summary>
        public string InitialClosingDisclosureDeliveryDate;

        /// <summary>
        /// The delivery method of the initial Closing Disclosure.
        /// </summary>
        public E_DeliveryMethodType? InitialClosingDisclosureDeliveryMethodType;

        /// <summary>
        /// The receipt date of the initial Closing Disclosure.
        /// </summary>
        public string InitialClosingDisclosureReceiptDate;

        /// <summary>
        /// The delivery date of the initial Loan Estimate.
        /// </summary>
        public string InitialLoanEstimateDeliveryDate;

        /// <summary>
        /// The delivery date of the revised Loan Estimate.
        /// </summary>
        public string RevisedLoanEstimateDeliveryDate;

        /// <summary>
        /// The delivery date of the post-consummation Closing Disclosure, if any.
        /// </summary>
        public string PostConsummationClosingDisclosureDeliveryDate;

        /// <summary>
        /// The reason for the post-consummation Closing Disclosure, if any.
        /// </summary>
        public E_PostConsummationRedisclosureReasonType? PostConsummationRedisclosureReasonType;

        /// <summary>
        /// The date on which the event requiring post-consummation disclosure occurred.
        /// </summary>
        public string PostConsummationRedisclosureReasonDate;

        /// <summary>
        /// The date on which the lender received knowledge of the event requiring post-consummation disclosure.
        /// </summary>
        public string PostConsummationKnowledgeOfEventDate;

        /// <summary>
        /// The delivery date of an Integrated Disclosure document.
        /// </summary>
        public string DeliveryDate;

        /// <summary>
        /// The delivery method of an Integrated Disclosure Document.
        /// </summary>
        public E_DeliveryMethodType? DeliveryMethodType;

        /// <summary>
        /// The issuance type of an Integrated Disclosure document.
        /// </summary>
        public E_DocumentIssuanceType? DocumentIssuanceType;

        /// <summary>
        /// The type of an Integrated Disclosure document.
        /// </summary>
        public E_DocumentType? DocumentType;

        /// <summary>
        /// Distinguishes between multiple Integrated Disclosure documents if they were
        /// disclosed on the same day.
        /// </summary>
        public string SequenceNumber;

        /// <summary>
        /// The receipt date of an Integrated Disclosure document.
        /// </summary>
        public string ReceiptDate;

        /// <summary>
        /// Lender credit information.
        /// </summary>
        public LenderCredits LenderCredits;

        /// <summary>
        /// Reads in the attributes from an INTEGRATED_DISCLOSURE element in a ComplianceEase XML request.
        /// </summary>
        /// <param name="reader">An XML reader.</param>
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "InitialClosingDisclosureDeliveryDate", out InitialClosingDisclosureDeliveryDate);
            ReadAttribute(reader, "InitialClosingDisclosureDeliveryMethodType", out InitialClosingDisclosureDeliveryMethodType);
            ReadAttribute(reader, "InitialClosingDisclosureReceiptDate", out InitialClosingDisclosureReceiptDate);
            ReadAttribute(reader, "InitialLoanEstimateDeliveryDate", out InitialLoanEstimateDeliveryDate);
            ReadAttribute(reader, "RevisedLoanEstimateDeliveryDate", out RevisedLoanEstimateDeliveryDate);
            ReadAttribute(reader, "PostConsummationClosingDisclosureDeliveryDate", out PostConsummationClosingDisclosureDeliveryDate);
            ReadAttribute(reader, "PostConsummationRedisclosureReasonType", out PostConsummationRedisclosureReasonType);
            ReadAttribute(reader, "PostConsummationRedisclosureReasonDate", out PostConsummationRedisclosureReasonDate);
            ReadAttribute(reader, "PostConsummationKnowledgeOfEventDate", out PostConsummationKnowledgeOfEventDate);
            ReadAttribute(reader, "_DeliveryDate", out DeliveryDate);
            ReadAttribute(reader, "_DeliveryMethodType", out DeliveryMethodType);
            ReadAttribute(reader, "_DocumentIssuanceType", out DocumentIssuanceType);
            ReadAttribute(reader, "_DocumentType", out DocumentType);
            ReadAttribute(reader, "_SequenceNumber", out SequenceNumber);
            ReadAttribute(reader, "_ReceiptDate", out ReceiptDate);
        }

        /// <summary>
        /// Reads a child element.
        /// </summary>
        /// <param name="reader">An XML reader.</param>
        protected override void ReadElement(XmlReader reader)
        {
            if (reader.Name.Equals("LENDER_CREDITS"))
            {
                ReadElement(reader, LenderCredits);
            }
        }

        /// <summary>
        /// Writes the attributes for an INTEGRATED_DISCLOSURE element in a ComplianceEase XML request.
        /// </summary>
        /// <param name="writer">An XML Writer.</param>
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "InitialClosingDisclosureDeliveryDate", InitialClosingDisclosureDeliveryDate);
            WriteAttribute(writer, "InitialClosingDisclosureDeliveryMethodType", InitialClosingDisclosureDeliveryMethodType);
            WriteAttribute(writer, "InitialClosingDisclosureReceiptDate", InitialClosingDisclosureReceiptDate);
            WriteAttribute(writer, "InitialLoanEstimateDeliveryDate", InitialLoanEstimateDeliveryDate);
            WriteAttribute(writer, "RevisedLoanEstimateDeliveryDate", RevisedLoanEstimateDeliveryDate);
            WriteAttribute(writer, "PostConsummationClosingDisclosureDeliveryDate", PostConsummationClosingDisclosureDeliveryDate);
            WriteAttribute(writer, "PostConsummationRedisclosureReasonType", PostConsummationRedisclosureReasonType);
            WriteAttribute(writer, "PostConsummationRedisclosureReasonDate", PostConsummationRedisclosureReasonDate);
            WriteAttribute(writer, "PostConsummationKnowledgeOfEventDate", PostConsummationKnowledgeOfEventDate);
            WriteAttribute(writer, "_DeliveryDate", DeliveryDate);
            WriteAttribute(writer, "_DeliveryMethodType", DeliveryMethodType);
            WriteAttribute(writer, "_DocumentIssuanceType", DocumentIssuanceType);
            WriteAttribute(writer, "_DocumentType", DocumentType);
            WriteAttribute(writer, "_SequenceNumber", SequenceNumber);
            WriteAttribute(writer, "_ReceiptDate", ReceiptDate);
            WriteElement(writer, LenderCredits);
        }

        /// <summary>
        /// Reads in a DeliveryMethodType attribute from an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="reader">An XML reader.</param>
        /// <param name="attributeName">The name of the attribute to be read.</param>
        /// <param name="deliveryMethod">An <see cref="E_DeliveryMethodType" /> to be populated.</param>
        /// <exception cref="ArgumentException">Thrown if the attribute value does not map to <see cref="E_DeliveryMethodType /></exception>
        private void ReadAttribute(XmlReader reader, string attributeName, out E_DeliveryMethodType? deliveryMethod)
        {
            string attributeValue = ReadAttribute(reader, attributeName);
            switch (attributeValue)
            {
                case "ElectronicDelivery":
                    deliveryMethod = E_DeliveryMethodType.ElectronicDelivery;
                    break;
                case "InPerson":
                    deliveryMethod = E_DeliveryMethodType.InPerson;
                    break;
                case "USPSFirstClassMail":
                    deliveryMethod = E_DeliveryMethodType.USPSFirstClassMail;
                    break;
                default:
                    throw new ArgumentException("The value '" + attributeValue + "' does not map to E_DeliveryMethodType.");
            }
        }

        /// <summary>
        /// Reads in a PostConsummationRedisclosureReasonType attribute from an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="reader">An XML reader.</param>
        /// <param name="attributeName">The name of the attribute to be read.</param>
        /// <param name="deliveryMethod">An <see cref="E_PostConsummationRedisclosureReasonType" /> to be populated.</param>
        /// <exception cref="ArgumentException">Thrown if the attribute value does not map to <see cref="E_PostConsummationRedisclosureReasonType /></exception>
        private void ReadAttribute(XmlReader reader, string attributeName, out E_PostConsummationRedisclosureReasonType? postConsummationRedisclosureReasonType)
        {
            string attributeValue = ReadAttribute(reader, attributeName);
            switch (attributeValue)
            {
                case "ChangeToAmountPaidByBorrower":
                    postConsummationRedisclosureReasonType = E_PostConsummationRedisclosureReasonType.ChangeToAmountPaidByBorrower;
                    break;
                case "CureForViolationOfToleranceOrVariation":
                    postConsummationRedisclosureReasonType = E_PostConsummationRedisclosureReasonType.CureForViolationOfToleranceOrVariation;
                    break;
                case "NonNumericClericalError":
                    postConsummationRedisclosureReasonType = E_PostConsummationRedisclosureReasonType.NonNumericClericalError;
                    break;
                default:
                    throw new ArgumentException("The value '" + attributeValue + "' does not map to E_PostConsummationRedisclosureReasonType.");
            }
        }

        /// <summary>
        /// Writes a DeliveryMethodType attribute to an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        /// <param name="attributeName">The name of the attribute to be written.</param>
        /// <param name="deliveryMethod">The delivery method to serialize.</param>
        /// <exception cref="ArgumentException">Thrown if the delivery method is not a valid ComplianceEase value.</exception>
        private void WriteAttribute(XmlWriter writer, string attributeName, E_DeliveryMethodType? deliveryMethod)
        {
            switch (deliveryMethod)
            {
                case null:
                    break;
                case E_DeliveryMethodType.ElectronicDelivery:
                    writer.WriteAttributeString(attributeName, "ElectronicDelivery");
                    break;
                case E_DeliveryMethodType.InPerson:
                    writer.WriteAttributeString(attributeName, "InPerson");
                    break;
                case E_DeliveryMethodType.USPSFirstClassMail:
                    writer.WriteAttributeString(attributeName, "USPSFirstClassMail");
                    break;
                default:
                    throw new ArgumentException("Unhandled enum value '" + deliveryMethod + "' for E_DeliveryMethodType");
            }
        }

        /// <summary>
        /// Writes a PostConsummationRedisclosureReasonType attribute to an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        /// <param name="attributeName">The name of the attribute to be written.</param>
        /// <param name="deliveryMethod">The delivery method to serialize.</param>
        /// <exception cref="ArgumentException">Thrown if the redisclosure reason is not a valid ComplianceEase value.</exception>
        private void WriteAttribute(XmlWriter writer, string attributeName, E_PostConsummationRedisclosureReasonType? postConsummationRedisclosureReasonType)
        {
            switch (postConsummationRedisclosureReasonType)
            {
                case null:
                    break;
                case E_PostConsummationRedisclosureReasonType.ChangeToAmountPaidByBorrower:
                    writer.WriteAttributeString(attributeName, "ChangeToAmountPaidByBorrower");
                    break;
                case E_PostConsummationRedisclosureReasonType.CureForViolationOfToleranceOrVariation:
                    writer.WriteAttributeString(attributeName, "CureForViolationOfToleranceOrVariation");
                    break;
                case E_PostConsummationRedisclosureReasonType.NonNumericClericalError:
                    writer.WriteAttributeString(attributeName, "NonNumericClericalError");
                    break;
                default:
                    throw new ArgumentException("Unhandled enum value '" + postConsummationRedisclosureReasonType + "' for E_PostConsummationRedisclosureReasonType");
            }
        }

        /// <summary>
        /// Reads in a DocumentIssuanceType attribute from an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="reader">An XML reader.</param>
        /// <param name="attributeName">The name of the attribute to be read.</param>
        /// <param name="deliveryMethod">An <see cref="E_DocumentIssuanceType" /> to be populated.</param>
        /// <exception cref="ArgumentException">Thrown if the attribute value does not map to <see cref="E_DocumentIssuanceType /></exception>
        private void ReadAttribute(XmlReader reader, string attributeName, out E_DocumentIssuanceType? issuanceType)
        {
            string attributeValue = ReadAttribute(reader, attributeName);
            switch (attributeValue)
            {
                case "Initial":
                    issuanceType = E_DocumentIssuanceType.Initial;
                    break;
                case "Revised":
                    issuanceType = E_DocumentIssuanceType.Revised;
                    break;
                default:
                    throw new ArgumentException("The value '" + attributeValue + "' does not map to E_DocumentIssuanceType.");
            }
        }

        /// <summary>
        /// Writes a DocumentIssuanceType attribute to an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        /// <param name="attributeName">The name of the attribute to be written.</param>
        /// <param name="deliveryMethod">The issuance type to serialize.</param>
        /// <exception cref="ArgumentException">Thrown if the issuance type is not a valid ComplianceEase value.</exception>
        private void WriteAttribute(XmlWriter writer, string attributeName, E_DocumentIssuanceType? issuanceType)
        {
            switch (issuanceType)
            {
                case null:
                    break;
                case E_DocumentIssuanceType.Initial:
                    writer.WriteAttributeString(attributeName, "Initial");
                    break;
                case E_DocumentIssuanceType.Revised:
                    writer.WriteAttributeString(attributeName, "Revised");
                    break;
                default:
                    throw new ArgumentException("Unhandled enum value '" + issuanceType + "' for E_DocumentIssuanceType");
            }
        }

        /// <summary>
        /// Reads in a DocumentType attribute from an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="reader">An XML reader.</param>
        /// <param name="attributeName">The name of the attribute to be read.</param>
        /// <param name="deliveryMethod">An <see cref="E_DocumentType" /> to be populated.</param>
        /// <exception cref="ArgumentException">Thrown if the attribute value does not map to <see cref="E_DocumentType /></exception>
        private void ReadAttribute(XmlReader reader, string attributeName, out E_DocumentType? documentType)
        {
            string attributeValue = ReadAttribute(reader, attributeName);
            switch (attributeValue)
            {
                case "LoanEstimate":
                    documentType = E_DocumentType.LoanEstimate;
                    break;
                case "ClosingDisclosure":
                    documentType = E_DocumentType.ClosingDisclosure;
                    break;
                case "PostConsummationRevisedClosingDisclosure":
                    documentType = E_DocumentType.PostConsummationRevisedClosingDisclosure;
                    break;
                default:
                    throw new ArgumentException("The value '" + attributeValue + "' does not map to E_DocumentType.");
            }
        }

        /// <summary>
        /// Writes a DocumentType attribute to an INTEGRATED_DISCLOSURE element.
        /// </summary>
        /// <param name="writer">An XML writer.</param>
        /// <param name="attributeName">The name of the attribute to be written.</param>
        /// <param name="deliveryMethod">The document type to serialize.</param>
        /// <exception cref="ArgumentException">Thrown if the document type is not a valid ComplianceEase value.</exception>
        private void WriteAttribute(XmlWriter writer, string attributeName, E_DocumentType? documentType)
        {
            switch (documentType)
            {
                case null:
                    break;
                case E_DocumentType.LoanEstimate:
                    writer.WriteAttributeString(attributeName, "LoanEstimate");
                    break;
                case E_DocumentType.ClosingDisclosure:
                    writer.WriteAttributeString(attributeName, "ClosingDisclosure");
                    break;
                case E_DocumentType.PostConsummationRevisedClosingDisclosure:
                    writer.WriteAttributeString(attributeName, "PostConsummationRevisedClosingDisclosure");
                    break;
                default:
                    throw new ArgumentException("Unhandled enum value '" + documentType + "' for E_DocumentType");
            }
        }
    }
}