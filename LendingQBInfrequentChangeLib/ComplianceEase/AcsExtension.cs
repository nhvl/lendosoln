// Generated by CodeMonkey on 11/15/2010 4:56:01 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace ComplianceEase
{
    public sealed class AcsExtension : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "ACS_EXTENSION"; }
        }

        private AdditionalCaseData m_AdditionalCaseData;
        public AdditionalCaseData AdditionalCaseData
        {
            get
            {
                if (null == m_AdditionalCaseData)
                {
                    m_AdditionalCaseData = new AdditionalCaseData();
                }
                return m_AdditionalCaseData;
            }
            set { m_AdditionalCaseData = value; }
        }

        private ConstructionLoanDetails m_ConstructionLoanDetails;
        public ConstructionLoanDetails ConstructionLoanDetails
        {
            get
            {
                if (null == m_ConstructionLoanDetails)
                {
                    m_ConstructionLoanDetails = new ConstructionLoanDetails();
                }
                return m_ConstructionLoanDetails;
            }
            set { m_ConstructionLoanDetails = value; }
        }

        private IntegratedDisclosure m_integratedDisclosure;
        public IntegratedDisclosure IntegratedDisclosure
        {
            get
            {
                if (null == m_integratedDisclosure)
                {
                    m_integratedDisclosure = new IntegratedDisclosure();
                }
                return m_integratedDisclosure;
            }
            set
            {
                m_integratedDisclosure = value;
            }
        }

        private List<Investor> m_InvestorList;
        public List<Investor> InvestorList
        {
            get
            {
                if (null == m_InvestorList)
                {
                    m_InvestorList = new List<Investor>();
                }
                return m_InvestorList;
            }
        }

        private LenderDetails m_LenderDetails;
        public LenderDetails LenderDetails
        {
            get
            {
                if (null == m_LenderDetails)
                {
                    m_LenderDetails = new LenderDetails();
                }
                return m_LenderDetails;
            }
            set { m_LenderDetails = value; }
        }

        private LoanOriginator m_LoanOriginator;
        public LoanOriginator LoanOriginator
        {
            get
            {
                if (null == m_LoanOriginator)
                {
                    m_LoanOriginator = new LoanOriginator();
                }
                return m_LoanOriginator;
            }
            set { m_LoanOriginator = value; }
        }

        private LoanProductDetails m_LoanProductDetails;
        public LoanProductDetails LoanProductDetails
        {
            get
            {
                if (null == m_LoanProductDetails)
                {
                    m_LoanProductDetails = new LoanProductDetails();
                }
                return m_LoanProductDetails;
            }
            set { m_LoanProductDetails = value; }
        }

        private LoanPurposeDetails m_LoanPurposeDetails;
        public LoanPurposeDetails LoanPurposeDetails
        {
            get
            {
                if (null == m_LoanPurposeDetails)
                {
                    m_LoanPurposeDetails = new LoanPurposeDetails();
                }
                return m_LoanPurposeDetails;
            }
            set { m_LoanPurposeDetails = value; }
        }

        private Mi m_Mi;
        public Mi Mi
        {
            get
            {
                if (null == m_Mi)
                {
                    m_Mi = new Mi();
                }
                return m_Mi;
            }
            set { m_Mi = value; }
        }

        private MortgageLoanDetails m_MortgageLoanDetails;
        public MortgageLoanDetails MortgageLoanDetails
        {
            get
            {
                if (null == m_MortgageLoanDetails)
                {
                    m_MortgageLoanDetails = new MortgageLoanDetails();
                }
                return m_MortgageLoanDetails;
            }
            set { m_MortgageLoanDetails = value; }
        }

        private PrepaymentPenaltyProgram m_PrepaymentPenaltyProgram;
        public PrepaymentPenaltyProgram PrepaymentPenaltyProgram
        {
            get
            {
                if (null == m_PrepaymentPenaltyProgram)
                {
                    m_PrepaymentPenaltyProgram = new PrepaymentPenaltyProgram();
                }
                return m_PrepaymentPenaltyProgram;
            }
            set { m_PrepaymentPenaltyProgram = value; }
        }

        private List<RespaFee> m_RespaFeeList;
        public List<RespaFee> RespaFeeList
        {
            get
            {
                if (null == m_RespaFeeList)
                {
                    m_RespaFeeList = new List<RespaFee>();
                }
                return m_RespaFeeList;
            }
        }

        private SubjectProperty m_SubjectProperty;
        public SubjectProperty SubjectProperty
        {
            get
            {
                if (null == m_SubjectProperty)
                {
                    m_SubjectProperty = new SubjectProperty();
                }
                return m_SubjectProperty;
            }
            set { m_SubjectProperty = value; }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ADDITIONAL_CASE_DATA": ReadElement(reader, AdditionalCaseData); break;
                case "CONSTRUCTION_LOAN_DETAILS": ReadElement(reader, ConstructionLoanDetails); break;
                case "INTEGRATED_DISCLOSURE": ReadElement(reader, IntegratedDisclosure); break;
                case "INVESTOR": ReadElement(reader, InvestorList); break;
                case "LENDER_DETAILS": ReadElement(reader, LenderDetails); break;
                case "LOAN_ORIGINATOR": ReadElement(reader, LoanOriginator); break;
                case "LOAN_PRODUCT_DETAILS": ReadElement(reader, LoanProductDetails); break;
                case "LOAN_PURPOSE_DETAILS": ReadElement(reader, LoanPurposeDetails); break;
                case "MI": ReadElement(reader, Mi); break;
                case "MORTGAGE_LOAN_DETAILS": ReadElement(reader, MortgageLoanDetails); break;
                case "PREPAYMENT_PENALTY_PROGRAM": ReadElement(reader, PrepaymentPenaltyProgram); break;
                case "RESPA_FEE": ReadElement(reader, RespaFeeList); break;
                case "SUBJECT_PROPERTY": ReadElement(reader, SubjectProperty); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_AdditionalCaseData);
            WriteElement(writer, m_ConstructionLoanDetails);
            WriteElement(writer, m_integratedDisclosure);
            WriteElement(writer, m_InvestorList);
            WriteElement(writer, m_LenderDetails);
            WriteElement(writer, m_LoanOriginator);
            WriteElement(writer, m_LoanProductDetails);
            WriteElement(writer, m_LoanPurposeDetails);
            WriteElement(writer, m_Mi);
            WriteElement(writer, m_MortgageLoanDetails);
            WriteElement(writer, m_PrepaymentPenaltyProgram);
            WriteElement(writer, m_RespaFeeList);
            WriteElement(writer, m_SubjectProperty);
        }
    }
}
