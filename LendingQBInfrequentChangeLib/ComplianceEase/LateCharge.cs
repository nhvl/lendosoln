// Generated by CodeMonkey on 11/15/2010 4:56:01 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace ComplianceEase
{
    public sealed class LateCharge : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "LATE_CHARGE"; }
        }

        public string GracePeriod;
        public string Rate;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_GracePeriod", out GracePeriod);
            ReadAttribute(reader, "_Rate", out Rate);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_GracePeriod", GracePeriod);
            WriteAttribute(writer, "_Rate", Rate);
        }
    }
}
