﻿namespace ComplianceEase
{
    /// <summary>
    /// The number of decimal places used when rounding the single per diem amount.
    /// </summary>
    public enum E_PerDiemCalculationScaleType
    {
        /// <summary>
        /// Use the default setting.
        /// </summary>
        Default,

        /// <summary>
        /// Round to two decimal places.
        /// </summary>
        _2,

        /// <summary>
        /// Round to four decimal places.
        /// </summary>
        _4,
    }
}
