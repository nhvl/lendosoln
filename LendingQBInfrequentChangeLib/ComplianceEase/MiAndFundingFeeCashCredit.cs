﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ComplianceEase
{
    public class MiAndFundingFeeCashCredit : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "MI_AND_FUNDING_FEE_CASH_CREDIT"; }
        }

        public string Amount;
        public E_DefaultAbleIndicator IncludedInAPRIndicator;
        public E_RespaFeePaymentPaidByType PaidByType;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Amount", out Amount);
            ReadAttribute(reader, "_IncludedInAPRIndicator", out IncludedInAPRIndicator);
            ReadAttribute(reader, "_PaidByType", out PaidByType);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Amount", Amount);
            WriteAttribute(writer, "_IncludedInAPRIndicator",IncludedInAPRIndicator);
            WriteAttribute(writer, "_PaidByType", PaidByType);
        }
    }
}
