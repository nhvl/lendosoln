﻿namespace ComplianceEase
{
    public enum E_TransmittalDataNMLSVerificationType
    {
        Undefined = 0,
        Default,
        FederalRegistration,
        StateMortgageLicenses,
        StateMortgageLicensesAndFederalRegistration,
    }
}