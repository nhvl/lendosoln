﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace ComplianceEase
{
    public sealed class ServiceFault : AbstractXmlSerializable
    {
        public ServiceFault()
        {
        }
        public ServiceFault(string xmlContent)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(xmlContent)))
            {
                ReadXml(reader);
            }
        }
        public override string XmlElementName
        {
            get { return "ServiceFault"; }
        }

        public string Description;

        private FaultCause m_faultCause;
        public FaultCause FaultCause
        {
            get
            {
                if (m_faultCause == null)
                {
                    m_faultCause = new FaultCause();
                }
                return m_faultCause;
            }
            set { m_faultCause = value; }
        }

        protected override void ReadElement(System.Xml.XmlReader reader)
        {
            switch (reader.Name)
            {
                case "Description": Description = ReadElementString(reader); break;
                case "FaultCause": ReadElement(reader, FaultCause); break;
            }
        }
        protected override void WriteXmlImpl(System.Xml.XmlWriter writer)
        {
            WriteElementString(writer, "Description", Description);
            WriteElement(writer, FaultCause);
        }
    }
}
