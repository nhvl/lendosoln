﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ComplianceEase
{
    public sealed class GfeDetail : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "GFE_DETAIL"; }
        }
        public string GfeDisclosureDate;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "GFEDisclosureDate", out GfeDisclosureDate);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "GFEDisclosureDate", GfeDisclosureDate);
        }
    }
}
