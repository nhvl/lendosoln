// Generated by CodeMonkey on 11/15/2010 4:56:01 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace ComplianceEase
{
    public sealed class PrepaymentPenalty : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "PREPAYMENT_PENALTY"; }
        }

        public string TermMonths;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_TermMonths", out TermMonths);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_TermMonths", TermMonths);
        }
    }
}
