﻿// <copyright file="E_DocumentType.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   May 1, 2015
// </summary>

namespace ComplianceEase
{
    /// <summary>
    /// The general class of a TRID fee.
    /// </summary>
    public enum E_RespaFeeClassificationType
    {
        /// <summary>
        /// A fee related to escrow or reserves.
        /// </summary>
        EscrowOrReserves = 0,

        /// <summary>
        /// A fee related to the cost of the loan
        /// </summary>
        LoanCosts,

        /// <summary>
        /// A loan origination charge.
        /// </summary>
        OriginationCharges,

        /// <summary>
        /// A former HUD-1 custom 1300 fee.
        /// </summary>
        OtherFormerHUD1Custom1300,

        /// <summary>
        /// A former HUD-1 custom 1301 fee.
        /// </summary>
        OtherFormerHUD1Custom1301,

        /// <summary>
        /// Prepaid fees.
        /// </summary>
        Prepaids,

        /// <summary>
        /// Recording fees.
        /// </summary>
        RecordingFees,

        /// <summary>
        /// Title fees related to insurance and services.
        /// </summary>
        Title_InsuranceAndServices,

        /// <summary>
        /// Title insurance for the lender.
        /// </summary>
        Title_LendersTitleInsurance,

        /// <summary>
        /// Title insurance for the owner.
        /// </summary>
        Title_OwnersTitleInsurance,

        /// <summary>
        /// Transfer taxes.
        /// </summary>
        TransferTaxes
    }
}