﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplianceEase
{
    public sealed class FaultCause : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "FaultCause"; }
        }

        private List<Error> m_errorList;
        public List<Error> ErrorList
        {
            get
            {
                if (m_errorList == null)
                {
                    m_errorList = new List<Error>();
                }
                return m_errorList;
            }
        }

        protected override void ReadElement(System.Xml.XmlReader reader)
        {
            switch (reader.Name)
            {
                case "Error": ReadElement(reader, ErrorList); break;
            }
        }
        protected override void WriteXmlImpl(System.Xml.XmlWriter writer)
        {
            WriteElement(writer, m_errorList);
        }
    }
}
