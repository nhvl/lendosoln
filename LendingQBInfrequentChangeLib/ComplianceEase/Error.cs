﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplianceEase
{
    public sealed class Error : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "Error"; }
        }

        public string ErrorCode;
        public string Description;

        protected override void ReadElement(System.Xml.XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ErrorCode": ErrorCode = ReadElementString(reader); break;
                case "Description": Description = ReadElementString(reader); break;
            }
        }
        protected override void WriteXmlImpl(System.Xml.XmlWriter writer)
        {
            WriteElementString(writer, "ErrorCode", ErrorCode);
            WriteElementString(writer, "Description", Description);
        }
    }
}
