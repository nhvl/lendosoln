﻿namespace ComplianceEase
{
    /// <summary>
    /// The reason for the post-consummation redisclosure.
    /// </summary>
    public enum E_PostConsummationRedisclosureReasonType
    {
        /// <summary>
        /// There was a change to the amount paid by the borrower.
        /// </summary>
        ChangeToAmountPaidByBorrower = 0,

        /// <summary>
        /// There was a cure cure for a violation of tolerance or some variation.
        /// </summary>
        CureForViolationOfToleranceOrVariation = 1,

        /// <summary>
        /// There was a non-numerical clerical error.
        /// </summary>
        NonNumericClericalError = 2
    }
}
