﻿namespace ComplianceEase
{
    public enum E_TransmittalDataQualifiedMortgageType
    {
        Undefined = 0,
        AgencyQualifiedMortgage,
        GeneralQualifiedMortgage,
        None,
    }
}