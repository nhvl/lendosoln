﻿// <copyright file="E_DocumentType.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   April 28, 2015
// </summary>

namespace ComplianceEase
{
    /// <summary>
    /// Specifies which instance of an integrated disclosure is represented.
    /// </summary>
    public enum E_DocumentIssuanceType
    {
        /// <summary>
        /// The first instance of the Loan Estimate or Closing Disclosure.
        /// </summary>
        Initial = 0,

        /// <summary>
        /// A subsequent instance of the Loan Estimate or Closing Disclosure.
        /// </summary>
        Revised
    }
}