﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace ComplianceEase
{
    public sealed class Indicators : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "INDICATORS"; }
        }
        public List<Indicator> m_IndicatorList;
        public List<Indicator> IndicatorList
        {
            get
            {
                if (null == m_IndicatorList)
                {
                    m_IndicatorList = new List<Indicator>();
                }
                return m_IndicatorList;
            }
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "INDICATOR": ReadElement(reader, IndicatorList); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_IndicatorList);
        }

    }
}
