﻿// <copyright file="E_DocumentType.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   April 28, 2015
// </summary>

namespace ComplianceEase
{
    /// <summary>
    /// Specifies the type of integrated disclosure.
    /// </summary>
    public enum E_DocumentType
    {
        /// <summary>
        /// The Loan Estimate.
        /// </summary>
        LoanEstimate = 0,

        /// <summary>
        /// The Closing Disclosure.
        /// </summary>
        ClosingDisclosure,

        /// <summary>
        /// A post-consummation revision of the Closing Disclosure.
        /// </summary>
        PostConsummationRevisedClosingDisclosure
    }
}