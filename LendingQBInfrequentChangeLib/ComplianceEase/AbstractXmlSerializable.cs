﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace ComplianceEase
{

    public abstract class AbstractXmlSerializable : IXmlSerializable
    {
        protected virtual string Prefix { get { return string.Empty; } }

        protected virtual string Namespace { get { return "http://www.complianceease.com/2017/11/ComplianceAnalyzer"; } }

        public abstract string XmlElementName { get; }

        protected string ReadElementString(XmlReader reader)
        {
            string str = reader.ReadString();
            if (str == null)
            {
                str = string.Empty;
            }
            return str;
        }

        protected string ReadAttribute(XmlReader reader, string attrName)
        {
            string value = reader.GetAttribute(attrName);
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value;
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteAttributeString(attrName, value);
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out string result)
        {
            result = ReadAttribute(reader, attrName);
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_YesNoIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Y")
            {
                result = E_YesNoIndicator.Y;
            }
            else if (s == "N")
            {
                result = E_YesNoIndicator.N;
            }
            else
            {
                result = E_YesNoIndicator.Undefined;
            }
            
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, E_YesNoIndicator value)
        {
            switch (value)
            {
                case E_YesNoIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
                case E_YesNoIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaFeePaidToType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AffiliateOfBroker")
            {
                result = E_RespaFeePaidToType.AffiliateOfBroker;
            }
            else if (s == "AffiliateOfLender")
            {
                result = E_RespaFeePaidToType.AffiliateOfLender;
            }
            else if (s == "Broker")
            {
                result = E_RespaFeePaidToType.Broker;
            }
            else if (s == "Default")
            {
                result = E_RespaFeePaidToType.Default;
            }
            else if (s == "Lender")
            {
                result = E_RespaFeePaidToType.Lender;
            }
            else if (s == "Other")
            {
                result = E_RespaFeePaidToType.Other;
            }
            else
            {
                result = E_RespaFeePaidToType.Undefined;
            }
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaFeePaidToType value)
        {
            switch (value)
            {
                case E_RespaFeePaidToType.AffiliateOfBroker:
                    writer.WriteAttributeString(attrName, "AffiliateOfBroker");
                    break;
                case E_RespaFeePaidToType.AffiliateOfLender:
                    writer.WriteAttributeString(attrName, "AffiliateOfLender");
                    break;
                case E_RespaFeePaidToType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_RespaFeePaidToType.Default:
                    writer.WriteAttributeString(attrName, "Default");
                    break;
                case E_RespaFeePaidToType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_RespaFeePaidToType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out E_DefaultAbleIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Y")
            {
                result = E_DefaultAbleIndicator.Y;
            }
            else if (s == "N")
            {
                result = E_DefaultAbleIndicator.N;
            }
            else if (s == "Default")
            {
                result = E_DefaultAbleIndicator.Default;
            }
            else
            {
                result = E_DefaultAbleIndicator.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DefaultAbleIndicator value)
        {
            switch (value)
            {
                case E_DefaultAbleIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
                case E_DefaultAbleIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_DefaultAbleIndicator.Default:
                    writer.WriteAttributeString(attrName, "Default");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaFeePaymentPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Buyer")
            {
                result = E_RespaFeePaymentPaidByType.Buyer;
            }
            else if (s == "Seller")
            {
                result = E_RespaFeePaymentPaidByType.Seller;
            }
            else if (s == "Lender")
            {
                result = E_RespaFeePaymentPaidByType.Lender;
            }
            else if (s == "Broker")
            {
                result = E_RespaFeePaymentPaidByType.Broker;
            }
            else if (s == "Default")
            {
                result = E_RespaFeePaymentPaidByType.Default;
            }
            else
            {
                result = E_RespaFeePaymentPaidByType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaFeePaymentPaidByType value)
        {
            switch (value)
            {
                case E_RespaFeePaymentPaidByType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_RespaFeePaymentPaidByType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
                case E_RespaFeePaymentPaidByType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_RespaFeePaymentPaidByType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_RespaFeePaymentPaidByType.Default:
                    writer.WriteAttributeString(attrName, "Default");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_VarianceValueType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Days")
            {
                result = E_VarianceValueType.Days;
            }
            else if (s == "Money")
            {
                result = E_VarianceValueType.Money;
            }
            else if (s == "Months")
            {
                result = E_VarianceValueType.Months;
            }
            else if (s == "Percentage")
            {
                result = E_VarianceValueType.Percentage;
            }
            else
            {
                result = E_VarianceValueType.Undefined;
            }

        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_VarianceValueType value)
        {
            switch (value)
            {
                case E_VarianceValueType.Days:
                    writer.WriteAttributeString(attrName, "Days");
                    break;
                case E_VarianceValueType.Money:
                    writer.WriteAttributeString(attrName, "Money");
                    break;
                case E_VarianceValueType.Months:
                    writer.WriteAttributeString(attrName, "Months");
                    break;
                case E_VarianceValueType.Percentage:
                    writer.WriteAttributeString(attrName, "Percentage");
                    break;
            }
        }
        protected void WriteElementString(XmlWriter writer, string name, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteElementString(name, value);
            }
        }

        protected void WriteString(XmlWriter writer, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteString(value);
            }
        }
        protected void WriteElement(XmlWriter writer, IXmlSerializable el)
        {
            if (null != el)
            {
                el.WriteXml(writer);
            }
        }
        protected void WriteElement<T>(XmlWriter writer, List<T> list) where T : IXmlSerializable
        {
            if (null == list)
                return;

            foreach (IXmlSerializable o in list)
            {
                WriteElement(writer, o);
            }
        }

        protected void ReadElement(XmlReader reader, IXmlSerializable el)
        {
            if (null == el)
            {
                return;
            }
            el.ReadXml(reader);
        }
        protected void ReadElement<T>(XmlReader reader, List<T> list) where T : IXmlSerializable, new()
        {
            if (null == list)
                return;

            T _o = new T();
            _o.ReadXml(reader);
            list.Add(_o);
        }

        protected virtual void ReadAttributes(XmlReader reader)
        {
        }

        protected virtual void ReadElement(XmlReader reader)
        {
        }
        protected virtual void ReadTextContent(XmlReader reader)
        {
        }
        protected virtual void WriteXmlImpl(XmlWriter writer)
        {
        }
        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.LocalName != XmlElementName)
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }
            ReadAttributes(reader);

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    ReadTextContent(reader);
                    continue;
                }
                if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == XmlElementName)
                {
                    return;
                }
                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                ReadElement(reader);
            }
        }


        public void WriteXml(System.Xml.XmlWriter writer)
        {
            if (string.IsNullOrEmpty(Prefix) == false && string.IsNullOrEmpty(Namespace) == false)
            {
                writer.WriteStartElement(Prefix, XmlElementName, Namespace);
            }
            else if (string.IsNullOrEmpty(Namespace) == false)
            {
                writer.WriteStartElement(XmlElementName, Namespace);
            }
            else
            {
                writer.WriteStartElement(XmlElementName);
            }

            WriteXmlImpl(writer);
            writer.WriteEndElement();
        }


        #endregion
    }
}
