// Generated by CodeMonkey on 02/28/2008 11:21
// Generated from LoanProspector_Request_4.0.xsd
namespace LoanProspectorRequest
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoLoanOriginationSystem : AbstractXmlSerializable
    {
        private string m_id = "";
        private string m_loanIdentifier = "";
        private string m_name = "";
        private string m_vendorIdentifier = "";
        private string m_versionIdentifier = "";
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public string LoanIdentifier
        {
            get
            {
                return m_loanIdentifier;
            }
            set
            {
                m_loanIdentifier = value;
            }
        }
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }
        public string VendorIdentifier
        {
            get
            {
                return m_vendorIdentifier;
            }
            set
            {
                m_vendorIdentifier = value;
            }
        }
        public string VersionIdentifier
        {
            get
            {
                return m_versionIdentifier;
            }
            set
            {
                m_versionIdentifier = value;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("LOAN_ORIGINATION_SYSTEM");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_LoanIdentifier", m_loanIdentifier);
            WriteAttribute(writer, "_Name", m_name);
            WriteAttribute(writer, "_VendorIdentifier", m_vendorIdentifier);
            WriteAttribute(writer, "_VersionIdentifier", m_versionIdentifier);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "LOAN_ORIGINATION_SYSTEM" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_loanIdentifier = ReadAttribute(reader, "_LoanIdentifier");
                m_name = ReadAttribute(reader, "_Name");
                m_vendorIdentifier = ReadAttribute(reader, "_VendorIdentifier");
                m_versionIdentifier = ReadAttribute(reader, "_VersionIdentifier");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LOAN_ORIGINATION_SYSTEM")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                    }
                }
            }
        }
    }
}
