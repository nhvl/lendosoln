// Generated by CodeMonkey on 02/28/2008 11:21
// Generated from LoanProspector_Request_4.0.xsd
namespace LoanProspectorRequest
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoProposedHousingExpense : AbstractXmlSerializable
    {
        private string m_id = "";
        private E_MismoProposedHousingExpenseHousingExpenseType m_housingExpenseType;
        private string m_housingExpenseTypeOtherDescription = "";
        private string m_paymentAmount = "";
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public E_MismoProposedHousingExpenseHousingExpenseType HousingExpenseType
        {
            get
            {
                return m_housingExpenseType;
            }
            set
            {
                m_housingExpenseType = value;
            }
        }
        public string HousingExpenseTypeOtherDescription
        {
            get
            {
                return m_housingExpenseTypeOtherDescription;
            }
            set
            {
                m_housingExpenseTypeOtherDescription = value;
            }
        }
        public string PaymentAmount
        {
            get
            {
                return m_paymentAmount;
            }
            set
            {
                m_paymentAmount = value;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("PROPOSED_HOUSING_EXPENSE");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "HousingExpenseType", m_housingExpenseType, E_MismoProposedHousingExpenseHousingExpenseTypeTable.Table);
            WriteAttribute(writer, "HousingExpenseTypeOtherDescription", m_housingExpenseTypeOtherDescription);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "PROPOSED_HOUSING_EXPENSE" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_housingExpenseType = (E_MismoProposedHousingExpenseHousingExpenseType) ConvertEnum(reader.GetAttribute("HousingExpenseType"), E_MismoProposedHousingExpenseHousingExpenseTypeTable.Table);
                m_housingExpenseTypeOtherDescription = ReadAttribute(reader, "HousingExpenseTypeOtherDescription");
                m_paymentAmount = ReadAttribute(reader, "_PaymentAmount");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PROPOSED_HOUSING_EXPENSE")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                    }
                }
            }
        }
    }
    public enum E_MismoProposedHousingExpenseHousingExpenseType
    {
        Undefined = 0,
        HazardInsurance,
        Utilities,
        GroundRent,
        HomeownersAssociationDuesAndCondominiumFees,
        RealEstateTax,
        FirstMortgagePITI,
        MaintenanceAndMiscellaneous,
        LeaseholdPayments,
        OtherMortgageLoanPrincipalAndInterest,
        MI,
        OtherMortgageLoanPrincipalInterestTaxesAndInsurance,
        OtherHousingExpense,
        FirstMortgagePrincipalAndInterest,
        Rent,
    }
    class E_MismoProposedHousingExpenseHousingExpenseTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoProposedHousingExpenseHousingExpenseType.Undefined, ""},
{E_MismoProposedHousingExpenseHousingExpenseType.HazardInsurance, "HazardInsurance"},
{E_MismoProposedHousingExpenseHousingExpenseType.Utilities, "Utilities"},
{E_MismoProposedHousingExpenseHousingExpenseType.GroundRent, "GroundRent"},
{E_MismoProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, "HomeownersAssociationDuesAndCondominiumFees"},
{E_MismoProposedHousingExpenseHousingExpenseType.RealEstateTax, "RealEstateTax"},
{E_MismoProposedHousingExpenseHousingExpenseType.FirstMortgagePITI, "FirstMortgagePITI"},
{E_MismoProposedHousingExpenseHousingExpenseType.MaintenanceAndMiscellaneous, "MaintenanceAndMiscellaneous"},
{E_MismoProposedHousingExpenseHousingExpenseType.LeaseholdPayments, "LeaseholdPayments"},
{E_MismoProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, "OtherMortgageLoanPrincipalAndInterest"},
{E_MismoProposedHousingExpenseHousingExpenseType.MI, "MI"},
{E_MismoProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalInterestTaxesAndInsurance, "OtherMortgageLoanPrincipalInterestTaxesAndInsurance"},
{E_MismoProposedHousingExpenseHousingExpenseType.OtherHousingExpense, "OtherHousingExpense"},
{E_MismoProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, "FirstMortgagePrincipalAndInterest"},
{E_MismoProposedHousingExpenseHousingExpenseType.Rent, "Rent"},
};
}
}
