﻿namespace LoanProspectorRequest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class MismoExtensionValuation : AbstractXmlSerializable
    {
        public string AppraisalIdentifier { get; set; } = string.Empty;

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EXTENSION_VALUATION");
            WriteAttribute(writer, nameof(AppraisalIdentifier), AppraisalIdentifier);
            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            if (reader.Name == "EXTENSION_VALUATION" && reader.NodeType == XmlNodeType.Element)
            {
                this.AppraisalIdentifier = ReadAttribute(reader, nameof(AppraisalIdentifier));
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "APPRAISER")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                    }
                }
            }
        }
    }
}
