// Generated by CodeMonkey on 02/28/2008 11:21
// Generated from LoanProspector_Request_4.0.xsd
namespace LoanProspectorRequest
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoPresentHousingExpense : AbstractXmlSerializable
    {
        private string m_id = "";
        private E_MismoPresentHousingExpenseHousingExpenseType m_housingExpenseType;
        private string m_housingExpenseTypeOtherDescription = "";
        private string m_paymentAmount = "";
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public E_MismoPresentHousingExpenseHousingExpenseType HousingExpenseType
        {
            get
            {
                return m_housingExpenseType;
            }
            set
            {
                m_housingExpenseType = value;
            }
        }
        public string HousingExpenseTypeOtherDescription
        {
            get
            {
                return m_housingExpenseTypeOtherDescription;
            }
            set
            {
                m_housingExpenseTypeOtherDescription = value;
            }
        }
        public string PaymentAmount
        {
            get
            {
                return m_paymentAmount;
            }
            set
            {
                m_paymentAmount = value;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("PRESENT_HOUSING_EXPENSE");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "HousingExpenseType", m_housingExpenseType, E_MismoPresentHousingExpenseHousingExpenseTypeTable.Table);
            WriteAttribute(writer, "HousingExpenseTypeOtherDescription", m_housingExpenseTypeOtherDescription);
            WriteAttribute(writer, "_PaymentAmount", m_paymentAmount);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "PRESENT_HOUSING_EXPENSE" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_housingExpenseType = (E_MismoPresentHousingExpenseHousingExpenseType) ConvertEnum(reader.GetAttribute("HousingExpenseType"), E_MismoPresentHousingExpenseHousingExpenseTypeTable.Table);
                m_housingExpenseTypeOtherDescription = ReadAttribute(reader, "HousingExpenseTypeOtherDescription");
                m_paymentAmount = ReadAttribute(reader, "_PaymentAmount");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PRESENT_HOUSING_EXPENSE")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                    }
                }
            }
        }
    }
    public enum E_MismoPresentHousingExpenseHousingExpenseType
    {
        Undefined = 0,
        HazardInsurance,
        Utilities,
        GroundRent,
        HomeownersAssociationDuesAndCondominiumFees,
        RealEstateTax,
        FirstMortgagePITI,
        MaintenanceAndMiscellaneous,
        LeaseholdPayments,
        OtherMortgageLoanPrincipalAndInterest,
        MI,
        OtherMortgageLoanPrincipalInterestTaxesAndInsurance,
        OtherHousingExpense,
        FirstMortgagePrincipalAndInterest,
        Rent,
    }
    class E_MismoPresentHousingExpenseHousingExpenseTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoPresentHousingExpenseHousingExpenseType.Undefined, ""},
{E_MismoPresentHousingExpenseHousingExpenseType.HazardInsurance, "HazardInsurance"},
{E_MismoPresentHousingExpenseHousingExpenseType.Utilities, "Utilities"},
{E_MismoPresentHousingExpenseHousingExpenseType.GroundRent, "GroundRent"},
{E_MismoPresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, "HomeownersAssociationDuesAndCondominiumFees"},
{E_MismoPresentHousingExpenseHousingExpenseType.RealEstateTax, "RealEstateTax"},
{E_MismoPresentHousingExpenseHousingExpenseType.FirstMortgagePITI, "FirstMortgagePITI"},
{E_MismoPresentHousingExpenseHousingExpenseType.MaintenanceAndMiscellaneous, "MaintenanceAndMiscellaneous"},
{E_MismoPresentHousingExpenseHousingExpenseType.LeaseholdPayments, "LeaseholdPayments"},
{E_MismoPresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, "OtherMortgageLoanPrincipalAndInterest"},
{E_MismoPresentHousingExpenseHousingExpenseType.MI, "MI"},
{E_MismoPresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalInterestTaxesAndInsurance, "OtherMortgageLoanPrincipalInterestTaxesAndInsurance"},
{E_MismoPresentHousingExpenseHousingExpenseType.OtherHousingExpense, "OtherHousingExpense"},
{E_MismoPresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, "FirstMortgagePrincipalAndInterest"},
{E_MismoPresentHousingExpenseHousingExpenseType.Rent, "Rent"},
};
}
}
