// Generated by CodeMonkey on 02/28/2008 11:21
// Generated from LoanProspector_Request_4.0.xsd
namespace LoanProspectorRequest
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoPaymentAdjustment : AbstractXmlSerializable
    {
        private string m_id = "";
        private string m_firstPaymentAdjustmentMonths = "";
        private string m_amount = "";
        private E_MismoPaymentAdjustmentCalculationType m_calculationType;
        private string m_calculationTypeOtherDescription = "";
        private string m_durationMonths = "";
        private string m_percent = "";
        private string m_periodicCapAmount = "";
        private string m_periodicCapPercent = "";
        private string m_periodNumber = "";
        private string m_subsequentPaymentAdjustmentMonths = "";
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public string FirstPaymentAdjustmentMonths
        {
            get
            {
                return m_firstPaymentAdjustmentMonths;
            }
            set
            {
                m_firstPaymentAdjustmentMonths = value;
            }
        }
        public string Amount
        {
            get
            {
                return m_amount;
            }
            set
            {
                m_amount = value;
            }
        }
        public E_MismoPaymentAdjustmentCalculationType CalculationType
        {
            get
            {
                return m_calculationType;
            }
            set
            {
                m_calculationType = value;
            }
        }
        public string CalculationTypeOtherDescription
        {
            get
            {
                return m_calculationTypeOtherDescription;
            }
            set
            {
                m_calculationTypeOtherDescription = value;
            }
        }
        public string DurationMonths
        {
            get
            {
                return m_durationMonths;
            }
            set
            {
                m_durationMonths = value;
            }
        }
        public string Percent
        {
            get
            {
                return m_percent;
            }
            set
            {
                m_percent = value;
            }
        }
        public string PeriodicCapAmount
        {
            get
            {
                return m_periodicCapAmount;
            }
            set
            {
                m_periodicCapAmount = value;
            }
        }
        public string PeriodicCapPercent
        {
            get
            {
                return m_periodicCapPercent;
            }
            set
            {
                m_periodicCapPercent = value;
            }
        }
        public string PeriodNumber
        {
            get
            {
                return m_periodNumber;
            }
            set
            {
                m_periodNumber = value;
            }
        }
        public string SubsequentPaymentAdjustmentMonths
        {
            get
            {
                return m_subsequentPaymentAdjustmentMonths;
            }
            set
            {
                m_subsequentPaymentAdjustmentMonths = value;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("PAYMENT_ADJUSTMENT");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "FirstPaymentAdjustmentMonths", m_firstPaymentAdjustmentMonths);
            WriteAttribute(writer, "_Amount", m_amount);
            WriteAttribute(writer, "_CalculationType", m_calculationType, E_MismoPaymentAdjustmentCalculationTypeTable.Table);
            WriteAttribute(writer, "_CalculationTypeOtherDescription", m_calculationTypeOtherDescription);
            WriteAttribute(writer, "_DurationMonths", m_durationMonths);
            WriteAttribute(writer, "_Percent", m_percent);
            WriteAttribute(writer, "_PeriodicCapAmount", m_periodicCapAmount);
            WriteAttribute(writer, "_PeriodicCapPercent", m_periodicCapPercent);
            WriteAttribute(writer, "_PeriodNumber", m_periodNumber);
            WriteAttribute(writer, "SubsequentPaymentAdjustmentMonths", m_subsequentPaymentAdjustmentMonths);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "PAYMENT_ADJUSTMENT" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_firstPaymentAdjustmentMonths = ReadAttribute(reader, "FirstPaymentAdjustmentMonths");
                m_amount = ReadAttribute(reader, "_Amount");
                m_calculationType = (E_MismoPaymentAdjustmentCalculationType) ConvertEnum(reader.GetAttribute("_CalculationType"), E_MismoPaymentAdjustmentCalculationTypeTable.Table);
                m_calculationTypeOtherDescription = ReadAttribute(reader, "_CalculationTypeOtherDescription");
                m_durationMonths = ReadAttribute(reader, "_DurationMonths");
                m_percent = ReadAttribute(reader, "_Percent");
                m_periodicCapAmount = ReadAttribute(reader, "_PeriodicCapAmount");
                m_periodicCapPercent = ReadAttribute(reader, "_PeriodicCapPercent");
                m_periodNumber = ReadAttribute(reader, "_PeriodNumber");
                m_subsequentPaymentAdjustmentMonths = ReadAttribute(reader, "SubsequentPaymentAdjustmentMonths");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PAYMENT_ADJUSTMENT")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                    }
                }
            }
        }
    }
    public enum E_MismoPaymentAdjustmentCalculationType
    {
        Undefined = 0,
        AddPercentToEffectivePaymentRate,
        Other,
        AddFixedDollarAmountToTheCurrentPayment,
        AddPercentToCurrentPaymentAmount,
    }
    class E_MismoPaymentAdjustmentCalculationTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoPaymentAdjustmentCalculationType.Undefined, ""},
{E_MismoPaymentAdjustmentCalculationType.AddPercentToEffectivePaymentRate, "AddPercentToEffectivePaymentRate"},
{E_MismoPaymentAdjustmentCalculationType.Other, "Other"},
{E_MismoPaymentAdjustmentCalculationType.AddFixedDollarAmountToTheCurrentPayment, "AddFixedDollarAmountToTheCurrentPayment"},
{E_MismoPaymentAdjustmentCalculationType.AddPercentToCurrentPaymentAmount, "AddPercentToCurrentPaymentAmount"},
};
}
}
