// Generated by CodeMonkey on 02/28/2008 11:21
// Generated from LoanProspector_Request_4.0.xsd
namespace LoanProspectorRequest
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoAffordableLending : AbstractXmlSerializable
    {
        private string m_id = "";
        private string m_fnmCommunityLendingProductName = "";
        private E_MismoAffordableLendingFnmCommunityLendingProductType m_fnmCommunityLendingProductType;
        private string m_fnmCommunityLendingProductTypeOtherDescription = "";
        private E_YesNoIndicator m_fnmCommunitySecondsIndicator;
        private E_YesNoIndicator m_fnmNeighborsMortgageEligibilityIndicator;
        private string m_freAffordableProgramIdentifier = "";
        private string m_hudIncomeLimitAdjustmentFactor = "";
        private string m_hudLendingIncomeLimitAmount = "";
        private string m_hudMedianIncomeAmount = "";
        private string m_msaIdentifier = "";
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public string FnmCommunityLendingProductName
        {
            get
            {
                return m_fnmCommunityLendingProductName;
            }
            set
            {
                m_fnmCommunityLendingProductName = value;
            }
        }
        public E_MismoAffordableLendingFnmCommunityLendingProductType FnmCommunityLendingProductType
        {
            get
            {
                return m_fnmCommunityLendingProductType;
            }
            set
            {
                m_fnmCommunityLendingProductType = value;
            }
        }
        public string FnmCommunityLendingProductTypeOtherDescription
        {
            get
            {
                return m_fnmCommunityLendingProductTypeOtherDescription;
            }
            set
            {
                m_fnmCommunityLendingProductTypeOtherDescription = value;
            }
        }
        public E_YesNoIndicator FnmCommunitySecondsIndicator
        {
            get
            {
                return m_fnmCommunitySecondsIndicator;
            }
            set
            {
                m_fnmCommunitySecondsIndicator = value;
            }
        }
        public E_YesNoIndicator FnmNeighborsMortgageEligibilityIndicator
        {
            get
            {
                return m_fnmNeighborsMortgageEligibilityIndicator;
            }
            set
            {
                m_fnmNeighborsMortgageEligibilityIndicator = value;
            }
        }
        public string FreAffordableProgramIdentifier
        {
            get
            {
                return m_freAffordableProgramIdentifier;
            }
            set
            {
                m_freAffordableProgramIdentifier = value;
            }
        }
        public string HudIncomeLimitAdjustmentFactor
        {
            get
            {
                return m_hudIncomeLimitAdjustmentFactor;
            }
            set
            {
                m_hudIncomeLimitAdjustmentFactor = value;
            }
        }
        public string HudLendingIncomeLimitAmount
        {
            get
            {
                return m_hudLendingIncomeLimitAmount;
            }
            set
            {
                m_hudLendingIncomeLimitAmount = value;
            }
        }
        public string HudMedianIncomeAmount
        {
            get
            {
                return m_hudMedianIncomeAmount;
            }
            set
            {
                m_hudMedianIncomeAmount = value;
            }
        }
        public string MsaIdentifier
        {
            get
            {
                return m_msaIdentifier;
            }
            set
            {
                m_msaIdentifier = value;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("AFFORDABLE_LENDING");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "FNMCommunityLendingProductName", m_fnmCommunityLendingProductName);
            WriteAttribute(writer, "FNMCommunityLendingProductType", m_fnmCommunityLendingProductType, E_MismoAffordableLendingFnmCommunityLendingProductTypeTable.Table);
            WriteAttribute(writer, "FNMCommunityLendingProductTypeOtherDescription", m_fnmCommunityLendingProductTypeOtherDescription);
            WriteAttribute(writer, "FNMCommunitySecondsIndicator", m_fnmCommunitySecondsIndicator, E_YesNoIndicatorTable.Table);
            WriteAttribute(writer, "FNMNeighborsMortgageEligibilityIndicator", m_fnmNeighborsMortgageEligibilityIndicator, E_YesNoIndicatorTable.Table);
            WriteAttribute(writer, "FREAffordableProgramIdentifier", m_freAffordableProgramIdentifier);
            WriteAttribute(writer, "HUDIncomeLimitAdjustmentFactor", m_hudIncomeLimitAdjustmentFactor);
            WriteAttribute(writer, "HUDLendingIncomeLimitAmount", m_hudLendingIncomeLimitAmount);
            WriteAttribute(writer, "HUDMedianIncomeAmount", m_hudMedianIncomeAmount);
            WriteAttribute(writer, "MSAIdentifier", m_msaIdentifier);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "AFFORDABLE_LENDING" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_fnmCommunityLendingProductName = ReadAttribute(reader, "FNMCommunityLendingProductName");
                m_fnmCommunityLendingProductType = (E_MismoAffordableLendingFnmCommunityLendingProductType) ConvertEnum(reader.GetAttribute("FNMCommunityLendingProductType"), E_MismoAffordableLendingFnmCommunityLendingProductTypeTable.Table);
                m_fnmCommunityLendingProductTypeOtherDescription = ReadAttribute(reader, "FNMCommunityLendingProductTypeOtherDescription");
                m_fnmCommunitySecondsIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("FNMCommunitySecondsIndicator"), E_YesNoIndicatorTable.Table);
                m_fnmNeighborsMortgageEligibilityIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("FNMNeighborsMortgageEligibilityIndicator"), E_YesNoIndicatorTable.Table);
                m_freAffordableProgramIdentifier = ReadAttribute(reader, "FREAffordableProgramIdentifier");
                m_hudIncomeLimitAdjustmentFactor = ReadAttribute(reader, "HUDIncomeLimitAdjustmentFactor");
                m_hudLendingIncomeLimitAmount = ReadAttribute(reader, "HUDLendingIncomeLimitAmount");
                m_hudMedianIncomeAmount = ReadAttribute(reader, "HUDMedianIncomeAmount");
                m_msaIdentifier = ReadAttribute(reader, "MSAIdentifier");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "AFFORDABLE_LENDING")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                    }
                }
            }
        }
    }
    public enum E_MismoAffordableLendingFnmCommunityLendingProductType
    {
        Undefined = 0,
        MyCommunityMortgage,
        Fannie32,
        Other,
        Fannie97,
        CommunityHomeBuyersProgram,
        CommunityHomeBuyerProgram,
    }
    class E_MismoAffordableLendingFnmCommunityLendingProductTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoAffordableLendingFnmCommunityLendingProductType.Undefined, ""},
{E_MismoAffordableLendingFnmCommunityLendingProductType.MyCommunityMortgage, "MyCommunityMortgage"},
{E_MismoAffordableLendingFnmCommunityLendingProductType.Fannie32, "Fannie32"},
{E_MismoAffordableLendingFnmCommunityLendingProductType.Other, "Other"},
{E_MismoAffordableLendingFnmCommunityLendingProductType.Fannie97, "Fannie97"},
{E_MismoAffordableLendingFnmCommunityLendingProductType.CommunityHomeBuyersProgram, "CommunityHomeBuyersProgram"},
{E_MismoAffordableLendingFnmCommunityLendingProductType.CommunityHomeBuyerProgram, "CommunityHomeBuyerProgram"},
};
}
}
