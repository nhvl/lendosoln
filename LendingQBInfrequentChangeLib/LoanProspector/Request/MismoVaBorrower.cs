// Generated by CodeMonkey on 02/28/2008 11:21
// Generated from LoanProspector_Request_4.0.xsd
namespace LoanProspectorRequest
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoVaBorrower : AbstractXmlSerializable
    {
        private string m_id = "";
        private string m_vaCoBorrowerNonTaxableIncomeAmount = "";
        private string m_vaCoBorrowerTaxableIncomeAmount = "";
        private string m_vaFederalTaxAmount = "";
        private string m_vaLocalTaxAmount = "";
        private string m_vaPrimaryBorrowerNonTaxableIncomeAmount = "";
        private string m_vaPrimaryBorrowerTaxableIncomeAmount = "";
        private string m_vaSocialSecurityTaxAmount = "";
        private string m_vaStateTaxAmount = "";
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public string VaCoBorrowerNonTaxableIncomeAmount
        {
            get
            {
                return m_vaCoBorrowerNonTaxableIncomeAmount;
            }
            set
            {
                m_vaCoBorrowerNonTaxableIncomeAmount = value;
            }
        }
        public string VaCoBorrowerTaxableIncomeAmount
        {
            get
            {
                return m_vaCoBorrowerTaxableIncomeAmount;
            }
            set
            {
                m_vaCoBorrowerTaxableIncomeAmount = value;
            }
        }
        public string VaFederalTaxAmount
        {
            get
            {
                return m_vaFederalTaxAmount;
            }
            set
            {
                m_vaFederalTaxAmount = value;
            }
        }
        public string VaLocalTaxAmount
        {
            get
            {
                return m_vaLocalTaxAmount;
            }
            set
            {
                m_vaLocalTaxAmount = value;
            }
        }
        public string VaPrimaryBorrowerNonTaxableIncomeAmount
        {
            get
            {
                return m_vaPrimaryBorrowerNonTaxableIncomeAmount;
            }
            set
            {
                m_vaPrimaryBorrowerNonTaxableIncomeAmount = value;
            }
        }
        public string VaPrimaryBorrowerTaxableIncomeAmount
        {
            get
            {
                return m_vaPrimaryBorrowerTaxableIncomeAmount;
            }
            set
            {
                m_vaPrimaryBorrowerTaxableIncomeAmount = value;
            }
        }
        public string VaSocialSecurityTaxAmount
        {
            get
            {
                return m_vaSocialSecurityTaxAmount;
            }
            set
            {
                m_vaSocialSecurityTaxAmount = value;
            }
        }
        public string VaStateTaxAmount
        {
            get
            {
                return m_vaStateTaxAmount;
            }
            set
            {
                m_vaStateTaxAmount = value;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("VA_BORROWER");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "VACoBorrowerNonTaxableIncomeAmount", m_vaCoBorrowerNonTaxableIncomeAmount);
            WriteAttribute(writer, "VACoBorrowerTaxableIncomeAmount", m_vaCoBorrowerTaxableIncomeAmount);
            WriteAttribute(writer, "VAFederalTaxAmount", m_vaFederalTaxAmount);
            WriteAttribute(writer, "VALocalTaxAmount", m_vaLocalTaxAmount);
            WriteAttribute(writer, "VAPrimaryBorrowerNonTaxableIncomeAmount", m_vaPrimaryBorrowerNonTaxableIncomeAmount);
            WriteAttribute(writer, "VAPrimaryBorrowerTaxableIncomeAmount", m_vaPrimaryBorrowerTaxableIncomeAmount);
            WriteAttribute(writer, "VASocialSecurityTaxAmount", m_vaSocialSecurityTaxAmount);
            WriteAttribute(writer, "VAStateTaxAmount", m_vaStateTaxAmount);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "VA_BORROWER" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_vaCoBorrowerNonTaxableIncomeAmount = ReadAttribute(reader, "VACoBorrowerNonTaxableIncomeAmount");
                m_vaCoBorrowerTaxableIncomeAmount = ReadAttribute(reader, "VACoBorrowerTaxableIncomeAmount");
                m_vaFederalTaxAmount = ReadAttribute(reader, "VAFederalTaxAmount");
                m_vaLocalTaxAmount = ReadAttribute(reader, "VALocalTaxAmount");
                m_vaPrimaryBorrowerNonTaxableIncomeAmount = ReadAttribute(reader, "VAPrimaryBorrowerNonTaxableIncomeAmount");
                m_vaPrimaryBorrowerTaxableIncomeAmount = ReadAttribute(reader, "VAPrimaryBorrowerTaxableIncomeAmount");
                m_vaSocialSecurityTaxAmount = ReadAttribute(reader, "VASocialSecurityTaxAmount");
                m_vaStateTaxAmount = ReadAttribute(reader, "VAStateTaxAmount");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "VA_BORROWER")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                    }
                }
            }
        }
    }
}
