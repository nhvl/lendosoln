// Generated by CodeMonkey on 02/28/2008 11:24
// Generated from LoanProspector_Response_4.0.xsd
namespace LoanProspectorResponse
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoCategory : AbstractXmlSerializable
    {
        private string m_id = "";
        private E_MismoCategoryType m_type;
        private string m_typeOtherDescription = "";
        private MismoErrorCollection m_error;
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public E_MismoCategoryType Type
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
            }
        }
        public string TypeOtherDescription
        {
            get
            {
                return m_typeOtherDescription;
            }
            set
            {
                m_typeOtherDescription = value;
            }
        }
        public MismoErrorCollection Error
        {
            get
            {
                if ((null == m_error))
                {
                    m_error = new MismoErrorCollection();
                }
                return m_error;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("_CATEGORY");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_Type", m_type, E_MismoCategoryTypeTable.Table);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteElement(writer, m_error);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "_CATEGORY" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_type = (E_MismoCategoryType) ConvertEnum(reader.GetAttribute("_Type"), E_MismoCategoryTypeTable.Table);
                m_typeOtherDescription = ReadAttribute(reader, "_TypeOtherDescription");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "_CATEGORY")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "ERROR":
                                MismoError _m_error = new MismoError();
                                _m_error.ReadXml(reader);
                                Error.Add(_m_error);
                                break;
                        }
                    }
                }
            }
        }
    }
    public enum E_MismoCategoryType
    {
        Undefined = 0,
        CommercialResidential,
        Modular,
        Church,
        Attached,
        LandAndLots,
        Farm,
        TownhouseRowhouse,
        Other,
        HighRise,
        ManufacturedMultiWide,
        MobileHome,
        SiteBuilt,
        ManufacturedSingleWide,
        Detached,
        LowRise,
        Manufactured,
        Stacked,
        CommercialNonResidential,
    }
    class E_MismoCategoryTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoCategoryType.Undefined, ""},
{E_MismoCategoryType.CommercialResidential, "CommercialResidential"},
{E_MismoCategoryType.Modular, "Modular"},
{E_MismoCategoryType.Church, "Church"},
{E_MismoCategoryType.Attached, "Attached"},
{E_MismoCategoryType.LandAndLots, "LandAndLots"},
{E_MismoCategoryType.Farm, "Farm"},
{E_MismoCategoryType.TownhouseRowhouse, "TownhouseRowhouse"},
{E_MismoCategoryType.Other, "Other"},
{E_MismoCategoryType.HighRise, "HighRise"},
{E_MismoCategoryType.ManufacturedMultiWide, "ManufacturedMultiWide"},
{E_MismoCategoryType.MobileHome, "MobileHome"},
{E_MismoCategoryType.SiteBuilt, "SiteBuilt"},
{E_MismoCategoryType.ManufacturedSingleWide, "ManufacturedSingleWide"},
{E_MismoCategoryType.Detached, "Detached"},
{E_MismoCategoryType.LowRise, "LowRise"},
{E_MismoCategoryType.Manufactured, "Manufactured"},
{E_MismoCategoryType.Stacked, "Stacked"},
{E_MismoCategoryType.CommercialNonResidential, "CommercialNonResidential"},
};
}
}
