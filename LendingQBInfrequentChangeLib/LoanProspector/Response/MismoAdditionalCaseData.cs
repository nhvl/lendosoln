// Generated by CodeMonkey on 02/28/2008 11:24
// Generated from LoanProspector_Response_4.0.xsd
namespace LoanProspectorResponse
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoAdditionalCaseData : AbstractXmlSerializable
    {
        private string m_id = "";
        private MismoErrorCollection m_error;
        private MismoMortgageScoreCollection m_mortgageScore;
        private MismoTransmittalData m_transmittalData;
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public MismoErrorCollection Error
        {
            get
            {
                if ((null == m_error))
                {
                    m_error = new MismoErrorCollection();
                }
                return m_error;
            }
        }
        public MismoMortgageScoreCollection MortgageScore
        {
            get
            {
                if ((null == m_mortgageScore))
                {
                    m_mortgageScore = new MismoMortgageScoreCollection();
                }
                return m_mortgageScore;
            }
        }
        public MismoTransmittalData TransmittalData
        {
            get
            {
                if ((null == m_transmittalData))
                {
                    m_transmittalData = new MismoTransmittalData();
                }
                return m_transmittalData;
            }
            set
            {
                m_transmittalData = value;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("ADDITIONAL_CASE_DATA");
            WriteAttribute(writer, "_ID", m_id);
            WriteElement(writer, m_error);
            WriteElement(writer, m_mortgageScore);
            WriteElement(writer, m_transmittalData);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "ADDITIONAL_CASE_DATA" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ADDITIONAL_CASE_DATA")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "ERROR":
                                MismoError _m_error = new MismoError();
                                _m_error.ReadXml(reader);
                                Error.Add(_m_error);
                                break;
                            case "MORTGAGE_SCORE":
                                MismoMortgageScore _m_mortgageScore = new MismoMortgageScore();
                                _m_mortgageScore.ReadXml(reader);
                                MortgageScore.Add(_m_mortgageScore);
                                break;
                            case "TRANSMITTAL_DATA":
                                m_transmittalData = new MismoTransmittalData();
                                m_transmittalData.ReadXml(reader);
                                break;
                        }
                    }
                }
            }
        }
    }
    public class MismoMortgageScoreCollection : CollectionBase
    {
        public int Add(MismoMortgageScore value)
        {
            if (null == value) return 0;
            return List.Add(value);
        }
    }
}
