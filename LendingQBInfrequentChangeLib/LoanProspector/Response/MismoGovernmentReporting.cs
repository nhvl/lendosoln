// Generated by CodeMonkey on 02/28/2008 11:24
// Generated from LoanProspector_Response_4.0.xsd
namespace LoanProspectorResponse
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoGovernmentReporting : AbstractXmlSerializable
    {
        private string m_id = "";
        private E_YesNoIndicator m_hmdaHoepaLoanStatusIndicator;
        private E_MismoGovernmentReportingHmdaPreapprovalType m_hmdaPreapprovalType;
        private E_MismoGovernmentReportingHmdaPurposeOfLoanType m_hmdaPurposeOfLoanType;
        private string m_hmdaRateSpreadPercent = "";
        private MismoErrorCollection m_error;
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public E_YesNoIndicator HmdaHoepaLoanStatusIndicator
        {
            get
            {
                return m_hmdaHoepaLoanStatusIndicator;
            }
            set
            {
                m_hmdaHoepaLoanStatusIndicator = value;
            }
        }
        public E_MismoGovernmentReportingHmdaPreapprovalType HmdaPreapprovalType
        {
            get
            {
                return m_hmdaPreapprovalType;
            }
            set
            {
                m_hmdaPreapprovalType = value;
            }
        }
        public E_MismoGovernmentReportingHmdaPurposeOfLoanType HmdaPurposeOfLoanType
        {
            get
            {
                return m_hmdaPurposeOfLoanType;
            }
            set
            {
                m_hmdaPurposeOfLoanType = value;
            }
        }
        public string HmdaRateSpreadPercent
        {
            get
            {
                return m_hmdaRateSpreadPercent;
            }
            set
            {
                m_hmdaRateSpreadPercent = value;
            }
        }
        public MismoErrorCollection Error
        {
            get
            {
                if ((null == m_error))
                {
                    m_error = new MismoErrorCollection();
                }
                return m_error;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("GOVERNMENT_REPORTING");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "HMDA_HOEPALoanStatusIndicator", m_hmdaHoepaLoanStatusIndicator, E_YesNoIndicatorTable.Table);
            WriteAttribute(writer, "HMDAPreapprovalType", m_hmdaPreapprovalType, E_MismoGovernmentReportingHmdaPreapprovalTypeTable.Table);
            WriteAttribute(writer, "HMDAPurposeOfLoanType", m_hmdaPurposeOfLoanType, E_MismoGovernmentReportingHmdaPurposeOfLoanTypeTable.Table);
            WriteAttribute(writer, "HMDARateSpreadPercent", m_hmdaRateSpreadPercent);
            WriteElement(writer, m_error);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "GOVERNMENT_REPORTING" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_hmdaHoepaLoanStatusIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("HMDA_HOEPALoanStatusIndicator"), E_YesNoIndicatorTable.Table);
                m_hmdaPreapprovalType = (E_MismoGovernmentReportingHmdaPreapprovalType) ConvertEnum(reader.GetAttribute("HMDAPreapprovalType"), E_MismoGovernmentReportingHmdaPreapprovalTypeTable.Table);
                m_hmdaPurposeOfLoanType = (E_MismoGovernmentReportingHmdaPurposeOfLoanType) ConvertEnum(reader.GetAttribute("HMDAPurposeOfLoanType"), E_MismoGovernmentReportingHmdaPurposeOfLoanTypeTable.Table);
                m_hmdaRateSpreadPercent = ReadAttribute(reader, "HMDARateSpreadPercent");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "GOVERNMENT_REPORTING")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "ERROR":
                                MismoError _m_error = new MismoError();
                                _m_error.ReadXml(reader);
                                Error.Add(_m_error);
                                break;
                        }
                    }
                }
            }
        }
    }
    public enum E_MismoGovernmentReportingHmdaPreapprovalType
    {
        Undefined = 0,
        NotApplicable,
        PreapprovalWasRequested,
        PreapprovalWasNotRequested,
    }
    class E_MismoGovernmentReportingHmdaPreapprovalTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoGovernmentReportingHmdaPreapprovalType.Undefined, ""},
{E_MismoGovernmentReportingHmdaPreapprovalType.NotApplicable, "NotApplicable"},
{E_MismoGovernmentReportingHmdaPreapprovalType.PreapprovalWasRequested, "PreapprovalWasRequested"},
{E_MismoGovernmentReportingHmdaPreapprovalType.PreapprovalWasNotRequested, "PreapprovalWasNotRequested"},
};
}
    public enum E_MismoGovernmentReportingHmdaPurposeOfLoanType
    {
        Undefined = 0,
        Refinancing,
        HomePurchase,
        HomeImprovement,
    }
    class E_MismoGovernmentReportingHmdaPurposeOfLoanTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoGovernmentReportingHmdaPurposeOfLoanType.Undefined, ""},
{E_MismoGovernmentReportingHmdaPurposeOfLoanType.Refinancing, "Refinancing"},
{E_MismoGovernmentReportingHmdaPurposeOfLoanType.HomePurchase, "HomePurchase"},
{E_MismoGovernmentReportingHmdaPurposeOfLoanType.HomeImprovement, "HomeImprovement"},
};
}
}
