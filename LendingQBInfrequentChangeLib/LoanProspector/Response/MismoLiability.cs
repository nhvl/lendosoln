// Generated by CodeMonkey on 02/28/2008 11:24
// Generated from LoanProspector_Response_4.0.xsd
namespace LoanProspectorResponse
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoLiability : AbstractXmlSerializable
    {
        private string m_id = "";
        private string m_borrowerId = "";
        private string m_reoId = "";
        private string m_alimonyOwedToName = "";
        private string m_accountIdentifier = "";
        private E_YesNoIndicator m_exclusionIndicator;
        private string m_holderCity = "";
        private string m_holderName = "";
        private string m_holderPostalCode = "";
        private string m_holderState = "";
        private string m_holderStreetAddress = "";
        private string m_monthlyPaymentAmount = "";
        private E_YesNoIndicator m_payoffStatusIndicator;
        private E_YesNoIndicator m_payoffWithCurrentAssetsIndicator;
        private string m_remainingTermMonths = "";
        private E_MismoLiabilityType m_type;
        private string m_typeOtherDescription = "";
        private string m_unpaidBalanceAmount = "";
        private E_YesNoIndicator m_subjectLoanResubordinationIndicator;
        private MismoErrorCollection m_error;
        private EXTENSION_LIABILITY extensionLiability;

        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public string BorrowerId
        {
            get
            {
                return m_borrowerId;
            }
            set
            {
                m_borrowerId = value;
            }
        }
        public string ReoId
        {
            get
            {
                return m_reoId;
            }
            set
            {
                m_reoId = value;
            }
        }
        public string AlimonyOwedToName
        {
            get
            {
                return m_alimonyOwedToName;
            }
            set
            {
                m_alimonyOwedToName = value;
            }
        }
        public string AccountIdentifier
        {
            get
            {
                return m_accountIdentifier;
            }
            set
            {
                m_accountIdentifier = value;
            }
        }
        public E_YesNoIndicator ExclusionIndicator
        {
            get
            {
                return m_exclusionIndicator;
            }
            set
            {
                m_exclusionIndicator = value;
            }
        }
        public string HolderCity
        {
            get
            {
                return m_holderCity;
            }
            set
            {
                m_holderCity = value;
            }
        }
        public string HolderName
        {
            get
            {
                return m_holderName;
            }
            set
            {
                m_holderName = value;
            }
        }
        public string HolderPostalCode
        {
            get
            {
                return m_holderPostalCode;
            }
            set
            {
                m_holderPostalCode = value;
            }
        }
        public string HolderState
        {
            get
            {
                return m_holderState;
            }
            set
            {
                m_holderState = value;
            }
        }
        public string HolderStreetAddress
        {
            get
            {
                return m_holderStreetAddress;
            }
            set
            {
                m_holderStreetAddress = value;
            }
        }
        public string MonthlyPaymentAmount
        {
            get
            {
                return m_monthlyPaymentAmount;
            }
            set
            {
                m_monthlyPaymentAmount = value;
            }
        }
        public E_YesNoIndicator PayoffStatusIndicator
        {
            get
            {
                return m_payoffStatusIndicator;
            }
            set
            {
                m_payoffStatusIndicator = value;
            }
        }
        public E_YesNoIndicator PayoffWithCurrentAssetsIndicator
        {
            get
            {
                return m_payoffWithCurrentAssetsIndicator;
            }
            set
            {
                m_payoffWithCurrentAssetsIndicator = value;
            }
        }
        public string RemainingTermMonths
        {
            get
            {
                return m_remainingTermMonths;
            }
            set
            {
                m_remainingTermMonths = value;
            }
        }
        public E_MismoLiabilityType Type
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
            }
        }
        public string TypeOtherDescription
        {
            get
            {
                return m_typeOtherDescription;
            }
            set
            {
                m_typeOtherDescription = value;
            }
        }
        public string UnpaidBalanceAmount
        {
            get
            {
                return m_unpaidBalanceAmount;
            }
            set
            {
                m_unpaidBalanceAmount = value;
            }
        }
        public E_YesNoIndicator SubjectLoanResubordinationIndicator
        {
            get
            {
                return m_subjectLoanResubordinationIndicator;
            }
            set
            {
                m_subjectLoanResubordinationIndicator = value;
            }
        }
        public MismoErrorCollection Error
        {
            get
            {
                if ((null == m_error))
                {
                    m_error = new MismoErrorCollection();
                }
                return m_error;
            }
        }
        public EXTENSION_LIABILITY ExtensionLiability
        {
            get
            {
                if (this.extensionLiability == null)
                {
                    this.extensionLiability = new EXTENSION_LIABILITY();
                }

                return this.extensionLiability;
            }

            set
            {
                this.extensionLiability = value;
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("LIABILITY");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "BorrowerID", m_borrowerId);
            WriteAttribute(writer, "REO_ID", m_reoId);
            WriteAttribute(writer, "AlimonyOwedToName", m_alimonyOwedToName);
            WriteAttribute(writer, "_AccountIdentifier", m_accountIdentifier);
            WriteAttribute(writer, "_ExclusionIndicator", m_exclusionIndicator, E_YesNoIndicatorTable.Table);
            WriteAttribute(writer, "_HolderCity", m_holderCity);
            WriteAttribute(writer, "_HolderName", m_holderName);
            WriteAttribute(writer, "_HolderPostalCode", m_holderPostalCode);
            WriteAttribute(writer, "_HolderState", m_holderState);
            WriteAttribute(writer, "_HolderStreetAddress", m_holderStreetAddress);
            WriteAttribute(writer, "_MonthlyPaymentAmount", m_monthlyPaymentAmount);
            WriteAttribute(writer, "_PayoffStatusIndicator", m_payoffStatusIndicator, E_YesNoIndicatorTable.Table);
            WriteAttribute(writer, "_PayoffWithCurrentAssetsIndicator", m_payoffWithCurrentAssetsIndicator, E_YesNoIndicatorTable.Table);
            WriteAttribute(writer, "_RemainingTermMonths", m_remainingTermMonths);
            WriteAttribute(writer, "_Type", m_type, E_MismoLiabilityTypeTable.Table);
            WriteAttribute(writer, "_TypeOtherDescription", m_typeOtherDescription);
            WriteAttribute(writer, "_UnpaidBalanceAmount", m_unpaidBalanceAmount);
            WriteAttribute(writer, "SubjectLoanResubordinationIndicator", m_subjectLoanResubordinationIndicator, E_YesNoIndicatorTable.Table);
            WriteElement(writer, m_error);
            WriteElement(writer, this.extensionLiability);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "LIABILITY" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_borrowerId = ReadAttribute(reader, "BorrowerID");
                m_reoId = ReadAttribute(reader, "REO_ID");
                m_alimonyOwedToName = ReadAttribute(reader, "AlimonyOwedToName");
                m_accountIdentifier = ReadAttribute(reader, "_AccountIdentifier");
                m_exclusionIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("_ExclusionIndicator"), E_YesNoIndicatorTable.Table);
                m_holderCity = ReadAttribute(reader, "_HolderCity");
                m_holderName = ReadAttribute(reader, "_HolderName");
                m_holderPostalCode = ReadAttribute(reader, "_HolderPostalCode");
                m_holderState = ReadAttribute(reader, "_HolderState");
                m_holderStreetAddress = ReadAttribute(reader, "_HolderStreetAddress");
                m_monthlyPaymentAmount = ReadAttribute(reader, "_MonthlyPaymentAmount");
                m_payoffStatusIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("_PayoffStatusIndicator"), E_YesNoIndicatorTable.Table);
                m_payoffWithCurrentAssetsIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("_PayoffWithCurrentAssetsIndicator"), E_YesNoIndicatorTable.Table);
                m_remainingTermMonths = ReadAttribute(reader, "_RemainingTermMonths");
                m_type = (E_MismoLiabilityType) ConvertEnum(reader.GetAttribute("_Type"), E_MismoLiabilityTypeTable.Table);
                m_typeOtherDescription = ReadAttribute(reader, "_TypeOtherDescription");
                m_unpaidBalanceAmount = ReadAttribute(reader, "_UnpaidBalanceAmount");
                m_subjectLoanResubordinationIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("SubjectLoanResubordinationIndicator"), E_YesNoIndicatorTable.Table);
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LIABILITY")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case nameof(EXTENSION_LIABILITY):
                                EXTENSION_LIABILITY extensionLiability = new EXTENSION_LIABILITY();
                                extensionLiability.ReadXml(reader);
                                this.ExtensionLiability = extensionLiability;
                                break;
                            case "ERROR":
                                MismoError _m_error = new MismoError();
                                _m_error.ReadXml(reader);
                                Error.Add(_m_error);
                                break;
                        }
                    }
                }
            }
        }
    }
    public enum E_MismoLiabilityType
    {
        Undefined = 0,
        SeparateMaintenanceExpense,
        Taxes,
        BorrowerEstimatedTotalMonthlyLiabilityPayment,
        DelinquentTaxes,
        ChildCare,
        LeasePayments,
        CollectionsJudgementsAndLiens,
        FirstMortgageBeingFinanced,
        Garnishments,
        UnionDues,
        UnsecuredHomeImprovementLoanInstallment,
        ChildSupport,
        Alimony,
        HELOC,
        Revolving,
        Open30DayChargeAccount,
        OtherLiability,
        OtherExpense,
        Installment,
        UnsecuredHomeImprovementLoanRevolving,
        DeferredStudentLoan,
        JobRelatedExpenses,
        MortgageLoan,
    }
    class E_MismoLiabilityTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoLiabilityType.Undefined, ""},
{E_MismoLiabilityType.SeparateMaintenanceExpense, "SeparateMaintenanceExpense"},
{E_MismoLiabilityType.Taxes, "Taxes"},
{E_MismoLiabilityType.BorrowerEstimatedTotalMonthlyLiabilityPayment, "BorrowerEstimatedTotalMonthlyLiabilityPayment"},
{E_MismoLiabilityType.DelinquentTaxes, "DelinquentTaxes"},
{E_MismoLiabilityType.ChildCare, "ChildCare"},
{E_MismoLiabilityType.LeasePayments, "LeasePayments"},
{E_MismoLiabilityType.CollectionsJudgementsAndLiens, "CollectionsJudgementsAndLiens"},
{E_MismoLiabilityType.FirstMortgageBeingFinanced, "FirstMortgageBeingFinanced"},
{E_MismoLiabilityType.Garnishments, "Garnishments"},
{E_MismoLiabilityType.UnionDues, "UnionDues"},
{E_MismoLiabilityType.UnsecuredHomeImprovementLoanInstallment, "UnsecuredHomeImprovementLoanInstallment"},
{E_MismoLiabilityType.ChildSupport, "ChildSupport"},
{E_MismoLiabilityType.Alimony, "Alimony"},
{E_MismoLiabilityType.HELOC, "HELOC"},
{E_MismoLiabilityType.Revolving, "Revolving"},
{E_MismoLiabilityType.Open30DayChargeAccount, "Open30DayChargeAccount"},
{E_MismoLiabilityType.OtherLiability, "OtherLiability"},
{E_MismoLiabilityType.OtherExpense, "OtherExpense"},
{E_MismoLiabilityType.Installment, "Installment"},
{E_MismoLiabilityType.UnsecuredHomeImprovementLoanRevolving, "UnsecuredHomeImprovementLoanRevolving"},
{E_MismoLiabilityType.DeferredStudentLoan, "DeferredStudentLoan"},
{E_MismoLiabilityType.JobRelatedExpenses, "JobRelatedExpenses"},
{E_MismoLiabilityType.MortgageLoan, "MortgageLoan"},
};
}
}
