// Generated by CodeMonkey on 02/28/2008 11:24
// Generated from LoanProspector_Response_4.0.xsd
namespace LoanProspectorResponse
{
    using System.Xml;
    using System.Collections;
	using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoFhaVaLoan : AbstractXmlSerializable
    {
        private string m_id = "";
        private string m_borrowerPaidFhaVaClosingCostsAmount = "";
        private string m_borrowerPaidFhaVaClosingCostsPercent = "";
        private string m_governmentMortgageCreditCertificateAmount = "";
        private E_MismoFhaVaLoanGovernmentRefinanceType m_governmentRefinanceType;
        private string m_governmentRefinanceTypeOtherDescription = "";
        private string m_otherPartyPaidFhaVaClosingCostsAmount = "";
        private string m_otherPartyPaidFhaVaClosingCostsPercent = "";
        private E_YesNoIndicator m_propertyEnergyEfficientHomeIndicator;
        private string m_sellerPaidFhaVaClosingCostsPercent = "";
        private MismoErrorCollection m_error;
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public string BorrowerPaidFhaVaClosingCostsAmount
        {
            get
            {
                return m_borrowerPaidFhaVaClosingCostsAmount;
            }
            set
            {
                m_borrowerPaidFhaVaClosingCostsAmount = value;
            }
        }
        public string BorrowerPaidFhaVaClosingCostsPercent
        {
            get
            {
                return m_borrowerPaidFhaVaClosingCostsPercent;
            }
            set
            {
                m_borrowerPaidFhaVaClosingCostsPercent = value;
            }
        }
        public string GovernmentMortgageCreditCertificateAmount
        {
            get
            {
                return m_governmentMortgageCreditCertificateAmount;
            }
            set
            {
                m_governmentMortgageCreditCertificateAmount = value;
            }
        }
        public E_MismoFhaVaLoanGovernmentRefinanceType GovernmentRefinanceType
        {
            get
            {
                return m_governmentRefinanceType;
            }
            set
            {
                m_governmentRefinanceType = value;
            }
        }
        public string GovernmentRefinanceTypeOtherDescription
        {
            get
            {
                return m_governmentRefinanceTypeOtherDescription;
            }
            set
            {
                m_governmentRefinanceTypeOtherDescription = value;
            }
        }
        public string OtherPartyPaidFhaVaClosingCostsAmount
        {
            get
            {
                return m_otherPartyPaidFhaVaClosingCostsAmount;
            }
            set
            {
                m_otherPartyPaidFhaVaClosingCostsAmount = value;
            }
        }
        public string OtherPartyPaidFhaVaClosingCostsPercent
        {
            get
            {
                return m_otherPartyPaidFhaVaClosingCostsPercent;
            }
            set
            {
                m_otherPartyPaidFhaVaClosingCostsPercent = value;
            }
        }
        public E_YesNoIndicator PropertyEnergyEfficientHomeIndicator
        {
            get
            {
                return m_propertyEnergyEfficientHomeIndicator;
            }
            set
            {
                m_propertyEnergyEfficientHomeIndicator = value;
            }
        }
        public string SellerPaidFhaVaClosingCostsPercent
        {
            get
            {
                return m_sellerPaidFhaVaClosingCostsPercent;
            }
            set
            {
                m_sellerPaidFhaVaClosingCostsPercent = value;
            }
        }
        public MismoErrorCollection Error
        {
            get
            {
                if ((null == m_error))
                {
                    m_error = new MismoErrorCollection();
                }
                return m_error;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("FHA_VA_LOAN");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "BorrowerPaidFHA_VAClosingCostsAmount", m_borrowerPaidFhaVaClosingCostsAmount);
            WriteAttribute(writer, "BorrowerPaidFHA_VAClosingCostsPercent", m_borrowerPaidFhaVaClosingCostsPercent);
            WriteAttribute(writer, "GovernmentMortgageCreditCertificateAmount", m_governmentMortgageCreditCertificateAmount);
            WriteAttribute(writer, "GovernmentRefinanceType", m_governmentRefinanceType, E_MismoFhaVaLoanGovernmentRefinanceTypeTable.Table);
            WriteAttribute(writer, "GovernmentRefinanceTypeOtherDescription", m_governmentRefinanceTypeOtherDescription);
            WriteAttribute(writer, "OtherPartyPaidFHA_VAClosingCostsAmount", m_otherPartyPaidFhaVaClosingCostsAmount);
            WriteAttribute(writer, "OtherPartyPaidFHA_VAClosingCostsPercent", m_otherPartyPaidFhaVaClosingCostsPercent);
            WriteAttribute(writer, "PropertyEnergyEfficientHomeIndicator", m_propertyEnergyEfficientHomeIndicator, E_YesNoIndicatorTable.Table);
            WriteAttribute(writer, "SellerPaidFHA_VAClosingCostsPercent", m_sellerPaidFhaVaClosingCostsPercent);
            WriteElement(writer, m_error);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "FHA_VA_LOAN" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_borrowerPaidFhaVaClosingCostsAmount = ReadAttribute(reader, "BorrowerPaidFHA_VAClosingCostsAmount");
                m_borrowerPaidFhaVaClosingCostsPercent = ReadAttribute(reader, "BorrowerPaidFHA_VAClosingCostsPercent");
                m_governmentMortgageCreditCertificateAmount = ReadAttribute(reader, "GovernmentMortgageCreditCertificateAmount");
                m_governmentRefinanceType = (E_MismoFhaVaLoanGovernmentRefinanceType) ConvertEnum(reader.GetAttribute("GovernmentRefinanceType"), E_MismoFhaVaLoanGovernmentRefinanceTypeTable.Table);
                m_governmentRefinanceTypeOtherDescription = ReadAttribute(reader, "GovernmentRefinanceTypeOtherDescription");
                m_otherPartyPaidFhaVaClosingCostsAmount = ReadAttribute(reader, "OtherPartyPaidFHA_VAClosingCostsAmount");
                m_otherPartyPaidFhaVaClosingCostsPercent = ReadAttribute(reader, "OtherPartyPaidFHA_VAClosingCostsPercent");
                m_propertyEnergyEfficientHomeIndicator = (E_YesNoIndicator) ConvertEnum(reader.GetAttribute("PropertyEnergyEfficientHomeIndicator"), E_YesNoIndicatorTable.Table);
                m_sellerPaidFhaVaClosingCostsPercent = ReadAttribute(reader, "SellerPaidFHA_VAClosingCostsPercent");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "FHA_VA_LOAN")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "ERROR":
                                MismoError _m_error = new MismoError();
                                _m_error.ReadXml(reader);
                                Error.Add(_m_error);
                                break;
                        }
                    }
                }
            }
        }
    }
    public enum E_MismoFhaVaLoanGovernmentRefinanceType
    {
        Undefined = 0,
        InterestRateReductionRefinanceLoan,
        Other,
        FullDocumentation,
        StreamlineWithAppraisal,
        StreamlineWithoutAppraisal,
    }
    class E_MismoFhaVaLoanGovernmentRefinanceTypeTable
    {
        public static readonly object[,] Table =
{
{E_MismoFhaVaLoanGovernmentRefinanceType.Undefined, ""},
{E_MismoFhaVaLoanGovernmentRefinanceType.InterestRateReductionRefinanceLoan, "InterestRateReductionRefinanceLoan"},
{E_MismoFhaVaLoanGovernmentRefinanceType.Other, "Other"},
{E_MismoFhaVaLoanGovernmentRefinanceType.FullDocumentation, "FullDocumentation"},
{E_MismoFhaVaLoanGovernmentRefinanceType.StreamlineWithAppraisal, "StreamlineWithAppraisal"},
{E_MismoFhaVaLoanGovernmentRefinanceType.StreamlineWithoutAppraisal, "StreamlineWithoutAppraisal"},
};
}
}
