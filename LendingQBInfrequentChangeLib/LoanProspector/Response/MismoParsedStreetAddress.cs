// Generated by CodeMonkey on 02/28/2008 11:24
// Generated from LoanProspector_Response_4.0.xsd
namespace LoanProspectorResponse
{
    using System.Xml;
    using System.Collections;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;
    
    public class MismoParsedStreetAddress : AbstractXmlSerializable
    {
        private string m_id = "";
        private string m_apartmentOrUnit = "";
        private string m_buildingNumber = "";
        private string m_directionPrefix = "";
        private string m_directionSuffix = "";
        private string m_houseNumber = "";
        private string m_militaryApoFpo = "";
        private string m_postOfficeBox = "";
        private string m_ruralRoute = "";
        private string m_streetName = "";
        private string m_streetSuffix = "";
        private MismoErrorCollection m_error;
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }
        public string ApartmentOrUnit
        {
            get
            {
                return m_apartmentOrUnit;
            }
            set
            {
                m_apartmentOrUnit = value;
            }
        }
        public string BuildingNumber
        {
            get
            {
                return m_buildingNumber;
            }
            set
            {
                m_buildingNumber = value;
            }
        }
        public string DirectionPrefix
        {
            get
            {
                return m_directionPrefix;
            }
            set
            {
                m_directionPrefix = value;
            }
        }
        public string DirectionSuffix
        {
            get
            {
                return m_directionSuffix;
            }
            set
            {
                m_directionSuffix = value;
            }
        }
        public string HouseNumber
        {
            get
            {
                return m_houseNumber;
            }
            set
            {
                m_houseNumber = value;
            }
        }
        public string MilitaryApoFpo
        {
            get
            {
                return m_militaryApoFpo;
            }
            set
            {
                m_militaryApoFpo = value;
            }
        }
        public string PostOfficeBox
        {
            get
            {
                return m_postOfficeBox;
            }
            set
            {
                m_postOfficeBox = value;
            }
        }
        public string RuralRoute
        {
            get
            {
                return m_ruralRoute;
            }
            set
            {
                m_ruralRoute = value;
            }
        }
        public string StreetName
        {
            get
            {
                return m_streetName;
            }
            set
            {
                m_streetName = value;
            }
        }
        public string StreetSuffix
        {
            get
            {
                return m_streetSuffix;
            }
            set
            {
                m_streetSuffix = value;
            }
        }
        public MismoErrorCollection Error
        {
            get
            {
                if ((null == m_error))
                {
                    m_error = new MismoErrorCollection();
                }
                return m_error;
            }
        }
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("PARSED_STREET_ADDRESS");
            WriteAttribute(writer, "_ID", m_id);
            WriteAttribute(writer, "_ApartmentOrUnit", m_apartmentOrUnit);
            WriteAttribute(writer, "_BuildingNumber", m_buildingNumber);
            WriteAttribute(writer, "_DirectionPrefix", m_directionPrefix);
            WriteAttribute(writer, "_DirectionSuffix", m_directionSuffix);
            WriteAttribute(writer, "_HouseNumber", m_houseNumber);
            WriteAttribute(writer, "_Military_APO_FPO", m_militaryApoFpo);
            WriteAttribute(writer, "_PostOfficeBox", m_postOfficeBox);
            WriteAttribute(writer, "_RuralRoute", m_ruralRoute);
            WriteAttribute(writer, "_StreetName", m_streetName);
            WriteAttribute(writer, "_StreetSuffix", m_streetSuffix);
            WriteElement(writer, m_error);
            writer.WriteEndElement();
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.Name == "PARSED_STREET_ADDRESS" && reader.NodeType == XmlNodeType.Element)
            {
                m_id = ReadAttribute(reader, "_ID");
                m_apartmentOrUnit = ReadAttribute(reader, "_ApartmentOrUnit");
                m_buildingNumber = ReadAttribute(reader, "_BuildingNumber");
                m_directionPrefix = ReadAttribute(reader, "_DirectionPrefix");
                m_directionSuffix = ReadAttribute(reader, "_DirectionSuffix");
                m_houseNumber = ReadAttribute(reader, "_HouseNumber");
                m_militaryApoFpo = ReadAttribute(reader, "_Military_APO_FPO");
                m_postOfficeBox = ReadAttribute(reader, "_PostOfficeBox");
                m_ruralRoute = ReadAttribute(reader, "_RuralRoute");
                m_streetName = ReadAttribute(reader, "_StreetName");
                m_streetSuffix = ReadAttribute(reader, "_StreetSuffix");
            }
            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while(reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "PARSED_STREET_ADDRESS")
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "ERROR":
                                MismoError _m_error = new MismoError();
                                _m_error.ReadXml(reader);
                                Error.Add(_m_error);
                                break;
                        }
                    }
                }
            }
        }
    }
}
