﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;

    public class EXTENSION_LIABILITY : AbstractXmlSerializable
    {
        public E_YesNoIndicator LiabilityPartialPayoffIndicator
        {
            get;
            set;
        }

        public string LiabilityPartialPayoffAmount
        {
            get;
            set;
        }

        public bool ReadFromXml
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(EXTENSION_LIABILITY));

            this.WriteAttribute(writer, nameof(this.LiabilityPartialPayoffIndicator), this.LiabilityPartialPayoffIndicator, E_YesNoIndicatorTable.Table);
            this.WriteAttribute(writer, nameof(this.LiabilityPartialPayoffAmount), this.LiabilityPartialPayoffAmount);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(EXTENSION_LIABILITY))
            {
                reader.Read();
                if (reader.EOF)
                {

                    return;
                }
            }

            this.ReadFromXml = true;
            this.LiabilityPartialPayoffAmount = this.ReadAttribute(reader, nameof(LiabilityPartialPayoffAmount));
            this.LiabilityPartialPayoffIndicator = (E_YesNoIndicator)this.ConvertEnum(this.ReadAttribute(reader, nameof(LiabilityPartialPayoffIndicator)), E_YesNoIndicatorTable.Table);
        }
    }
}
