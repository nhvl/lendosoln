﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class GOVERNMENT_MONITORING_DETAIL : AbstractXmlSerializable
    {
        private static readonly object[,] GovernmentMonitoringDetailHmdaGenderTypeTable =
        {
            { GovernmentMonitoringDetailHmdaGenderType.ApplicantSelectedBothMaleAndFemale, "ApplicantSelectedBothMaleAndFemale" },
            { GovernmentMonitoringDetailHmdaGenderType.Female, "Female" },
            { GovernmentMonitoringDetailHmdaGenderType.Male, "Male" }
        };

        public ApplicationTakenMethodType ApplicationTakenMethodType
        {
            get;
            set;
        }

        public E_YesNoIndicator HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator
        {
            get;
            set;
        }

        public E_YesNoIndicator HMDAEthnicityRefusalIndicator
        {
            get;
            set;
        }

        public E_YesNoIndicator HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator
        {
            get;
            set;
        }

        public E_YesNoIndicator HMDAGenderRefusalIndicator
        {
            get;
            set;
        }

        public GovernmentMonitoringDetailHmdaGenderType HMDAGenderType
        {
            get;
            set;
        }

        public E_YesNoIndicator HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator
        {
            get;
            set;
        }

        public E_YesNoIndicator HMDARaceRefusalIndicator
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(GOVERNMENT_MONITORING_DETAIL));

            this.WriteAttribute(writer, nameof(this.ApplicationTakenMethodType), this.ApplicationTakenMethodType, Common.ApplicationTakenMethodTypeTable.Table);
            this.WriteAttribute(writer, nameof(this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator), this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator, E_YesNoIndicatorTable.Table);
            this.WriteAttribute(writer, nameof(this.HMDAEthnicityRefusalIndicator), this.HMDAEthnicityRefusalIndicator, E_YesNoIndicatorTable.Table);
            this.WriteAttribute(writer, nameof(this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator), this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator, E_YesNoIndicatorTable.Table);
            this.WriteAttribute(writer, nameof(this.HMDAGenderRefusalIndicator), this.HMDAGenderRefusalIndicator, E_YesNoIndicatorTable.Table);
            this.WriteAttribute(writer, nameof(this.HMDAGenderType), this.HMDAGenderType, GovernmentMonitoringDetailHmdaGenderTypeTable);
            this.WriteAttribute(writer, nameof(this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator), this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator, E_YesNoIndicatorTable.Table);
            this.WriteAttribute(writer, nameof(this.HMDARaceRefusalIndicator), this.HMDARaceRefusalIndicator, E_YesNoIndicatorTable.Table);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(GOVERNMENT_MONITORING_DETAIL))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.ApplicationTakenMethodType = (ApplicationTakenMethodType)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.ApplicationTakenMethodType)), Common.ApplicationTakenMethodTypeTable.Table);
            this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator = (E_YesNoIndicator)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator)), E_YesNoIndicatorTable.Table);
            this.HMDAEthnicityRefusalIndicator = (E_YesNoIndicator)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDARaceRefusalIndicator)), E_YesNoIndicatorTable.Table);
            this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator = (E_YesNoIndicator)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator)), E_YesNoIndicatorTable.Table);
            this.HMDAGenderRefusalIndicator = (E_YesNoIndicator)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDAGenderRefusalIndicator)), E_YesNoIndicatorTable.Table);
            this.HMDAGenderType = (GovernmentMonitoringDetailHmdaGenderType)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDAGenderType)), GovernmentMonitoringDetailHmdaGenderTypeTable);
            this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator = (E_YesNoIndicator)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator)), E_YesNoIndicatorTable.Table);
            this.HMDARaceRefusalIndicator = (E_YesNoIndicator)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDARaceRefusalIndicator)), E_YesNoIndicatorTable.Table);
        }
    }

    public enum GovernmentMonitoringDetailHmdaGenderType
    {
        Undefined = 0,
        ApplicantSelectedBothMaleAndFemale,
        Female,
        Male
    }
}
