﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System.ComponentModel;

    public enum E_YesNoIndicator
    {
        Undefined = 0,
        Y,
        N,
    }

    public class E_YesNoIndicatorTable
    {
        public static readonly object[,] Table = { { E_YesNoIndicator.Undefined, "" }, { E_YesNoIndicator.Y, "Y" }, { E_YesNoIndicator.N, "N" } };
    }

    public enum ApplicationTakenMethodType
    {
        Undefined = 0,
        Mail,
        FaceToFace,
        Internet,
        Telephone,
    }

    public class ApplicationTakenMethodTypeTable
    {
        public static readonly object[,] Table =
        {
            {ApplicationTakenMethodType.Undefined, ""},
            {ApplicationTakenMethodType.Mail, "Mail"},
            {ApplicationTakenMethodType.FaceToFace, "FaceToFace"},
            {ApplicationTakenMethodType.Internet, "Internet"},
            {ApplicationTakenMethodType.Telephone, "Telephone"},
        };
    }
}
