﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;
    public class HMDA_ETHNICITY : AbstractXmlSerializable
    {
        private static readonly object[,] HmdaEthnicityHmdaEthnicityTypeTable =
        {
            { HmdaEthnicityHmdaEthnicityType.HispanicOrLatino, "HispanicOrLatino" },
            { HmdaEthnicityHmdaEthnicityType.NotHispanicOrLatino, "NotHispanicOrLatino" }
        };

        public HmdaEthnicityHmdaEthnicityType HMDAEthnicityType
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_ETHNICITY));

            this.WriteAttribute(writer, nameof(this.HMDAEthnicityType), this.HMDAEthnicityType, HmdaEthnicityHmdaEthnicityTypeTable);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_ETHNICITY))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.HMDAEthnicityType = (HmdaEthnicityHmdaEthnicityType)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDAEthnicityType)), HmdaEthnicityHmdaEthnicityTypeTable);
        }
    }

    public enum HmdaEthnicityHmdaEthnicityType
    {
        Undefined = 0,
        HispanicOrLatino,
        NotHispanicOrLatino
    }
}
