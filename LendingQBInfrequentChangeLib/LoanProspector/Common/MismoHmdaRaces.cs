﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class HMDA_RACES : AbstractXmlSerializable
    {
        private List<HMDA_RACE_EXTENSION> hmdaRaceExtensionList;

        public List<HMDA_RACE_EXTENSION> HmdaRaceExtensionList
        {
            get
            {
                if (this.hmdaRaceExtensionList == null)
                {
                    this.hmdaRaceExtensionList = new List<HMDA_RACE_EXTENSION>();
                }

                return this.hmdaRaceExtensionList;
            }

            set
            {
                this.hmdaRaceExtensionList = value;
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_RACES));

            this.WriteElement(writer, this.hmdaRaceExtensionList);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_RACES))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(HMDA_RACES))
                    {
                        break;
                    }

                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case nameof(HMDA_RACE_EXTENSION):
                                HMDA_RACE_EXTENSION hmdaRaceExtension = new HMDA_RACE_EXTENSION();
                                hmdaRaceExtension.ReadXml(reader);
                                this.HmdaRaceExtensionList.Add(hmdaRaceExtension);
                                break;
                            default:
                                throw new ArgumentException($"Element name {reader.Name} is not handled.");
                        }
                    }
                }
            }
        }
    }
}
