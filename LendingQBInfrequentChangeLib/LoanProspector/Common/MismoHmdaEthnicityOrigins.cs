﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class HMDA_ETHNICITY_ORIGINS : AbstractXmlSerializable
    {
        private List<HMDA_ETHNICITY_ORIGIN> hmdaEthnicityOriginList;

        public List<HMDA_ETHNICITY_ORIGIN> HmdaEthnicityOriginList
        {
            get
            {
                if (this.hmdaEthnicityOriginList == null)
                {
                    this.hmdaEthnicityOriginList = new List<HMDA_ETHNICITY_ORIGIN>();
                }

                return this.hmdaEthnicityOriginList;
            }

            set
            {
                this.hmdaEthnicityOriginList = value;
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_ETHNICITY_ORIGINS));

            this.WriteElement(writer, this.hmdaEthnicityOriginList);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_ETHNICITY_ORIGINS))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(HMDA_ETHNICITY_ORIGINS))
                    {
                        break;
                    }

                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case nameof(HMDA_ETHNICITY_ORIGIN):
                                HMDA_ETHNICITY_ORIGIN hmdaEthnicityOrigin = new HMDA_ETHNICITY_ORIGIN();
                                hmdaEthnicityOrigin.ReadXml(reader);
                                this.HmdaEthnicityOriginList.Add(hmdaEthnicityOrigin);
                                break;
                            default:
                                throw new ArgumentException($"Element name {reader.Name} is not handled.");
                        }
                    }
                }
            }
        }
    }
}
