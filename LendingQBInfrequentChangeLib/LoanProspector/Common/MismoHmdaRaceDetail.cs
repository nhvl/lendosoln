﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class HMDA_RACE_DETAIL : AbstractXmlSerializable
    {
        private static readonly object[,] HmdaRaceDetailHmdaRaceTypeTable =
        {
            { HmdaRaceDetailHmdaRaceType.AmericanIndianOrAlaskaNative, "AmericanIndianOrAlaskaNative" },
            { HmdaRaceDetailHmdaRaceType.Asian, "Asian" },
            { HmdaRaceDetailHmdaRaceType.BlackOrAfricanAmerican, "BlackOrAfricanAmerican" },
            { HmdaRaceDetailHmdaRaceType.NativeHawaiianOrOtherPacificIslander, "NativeHawaiianOrOtherPacificIslander" },
            { HmdaRaceDetailHmdaRaceType.White, "White" },
            { HmdaRaceDetailHmdaRaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication, "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication" },
            { HmdaRaceDetailHmdaRaceType.NotApplicable, "NotApplicable" }
        };

        public string HMDARaceTypeAdditionalDescription
        {
            get; set;
        }

        public HmdaRaceDetailHmdaRaceType HMDARaceType
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_RACE_DETAIL));

            this.WriteAttribute(writer, nameof(this.HMDARaceType), this.HMDARaceType, HmdaRaceDetailHmdaRaceTypeTable);
            this.WriteAttribute(writer, nameof(this.HMDARaceTypeAdditionalDescription), this.HMDARaceTypeAdditionalDescription);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_RACE_DETAIL))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.HMDARaceTypeAdditionalDescription = this.ReadAttribute(reader, nameof(this.HMDARaceTypeAdditionalDescription));
            this.HMDARaceType = (HmdaRaceDetailHmdaRaceType)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDARaceType)), HmdaRaceDetailHmdaRaceTypeTable);
        }
    }

    public enum HmdaRaceDetailHmdaRaceType
    {
        Undefined = 0,
        AmericanIndianOrAlaskaNative,
        Asian,
        BlackOrAfricanAmerican,
        NativeHawaiianOrOtherPacificIslander,
        White,
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        NotApplicable
    }
}
