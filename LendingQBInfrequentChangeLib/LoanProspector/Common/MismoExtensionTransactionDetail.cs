﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System.Xml;

    public class EXTENSION_TRANSACTION_DETAIL : AbstractXmlSerializable
    {
        public string TotalDiscountPointsAmount
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(EXTENSION_TRANSACTION_DETAIL));

            this.WriteAttribute(writer, nameof(this.TotalDiscountPointsAmount), this.TotalDiscountPointsAmount);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(EXTENSION_TRANSACTION_DETAIL))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.TotalDiscountPointsAmount = this.ReadAttribute(reader, nameof(this.TotalDiscountPointsAmount));
        }
    }
}
