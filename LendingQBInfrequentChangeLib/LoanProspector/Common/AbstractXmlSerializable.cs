﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Xml.Serialization;
    using System.Xml;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class AbstractXmlSerializable : IXmlSerializable
    {

        protected void WriteAttribute(XmlWriter writer, string attrName, string value)
        {
            if (null != value && "" != value)
                writer.WriteAttributeString(attrName, value);
        }
        protected void WriteAttributeOverrideNull(XmlWriter writer, string attrName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                writer.WriteAttributeString(attrName, string.Empty);
            }
            else
            {
                writer.WriteAttributeString(attrName, value);
            }
        }


        protected string ReadAttribute(XmlReader reader, string attrName)
        {
            string value = reader.GetAttribute(attrName);
            if (null == value)
                return "";
            return value;
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, Enum value, object[,] mapTable)
        {
            string strValue = "";
            int length = mapTable.Length / 2;
            for (int i = 0; i < length; i++)
            {
                if (value.Equals(mapTable[i, 0]))
                {
                    strValue = (string)mapTable[i, 1];
                    break;
                }
            }
            WriteAttribute(writer, attrName, strValue);
        }

        protected int ConvertEnum(string value, object[,] mapTable)
        {
            int ret = 0;
            int length = mapTable.Length / 2;
            for (int i = 0; i < length; i++)
            {
                if (value == (string)mapTable[i, 1])
                    return (int)mapTable[i, 0];
            }
            return ret;
        }

        protected void WriteElement(XmlWriter writer, IXmlSerializable el)
        {
            if (null != el)
                el.WriteXml(writer);
        }

        protected void WriteElement(XmlWriter writer, CollectionBase list)
        {
            if (null != list)
            {
                foreach (IXmlSerializable el in list)
                {
                    if (null != el)
                        el.WriteXml(writer);
                }
            }
        }

        protected void WriteElement(XmlWriter writer, IEnumerable<IXmlSerializable> elCollection)
        {
            if (elCollection != null && elCollection.Any())
            {
                foreach (var el in elCollection)
                {
                    if (el != null)
                    {
                        el.WriteXml(writer);
                    }
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public virtual void WriteXml(XmlWriter writer)
        {
        }
        public virtual void ReadXml(XmlReader reader)
        {
        }
    }
}
