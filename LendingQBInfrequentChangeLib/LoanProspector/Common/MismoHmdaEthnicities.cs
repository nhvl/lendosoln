﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class HMDA_ETHNICITIES : AbstractXmlSerializable
    {
        private List<HMDA_ETHNICITY> hmdaEthnicityList;

        public List<HMDA_ETHNICITY> HmdaEthnicityList
        {
            get
            {
                if (this.hmdaEthnicityList == null)
                {
                    this.hmdaEthnicityList = new List<HMDA_ETHNICITY>();
                }

                return this.hmdaEthnicityList;
            }

            set
            {
                this.hmdaEthnicityList = value;
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_ETHNICITIES));

            this.WriteElement(writer, this.hmdaEthnicityList);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_ETHNICITIES))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(HMDA_ETHNICITIES))
                    {
                        break;
                    }

                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case nameof(HMDA_ETHNICITY):
                                HMDA_ETHNICITY hmdaEthnicity = new HMDA_ETHNICITY();
                                hmdaEthnicity.ReadXml(reader);
                                this.HmdaEthnicityList.Add(hmdaEthnicity);
                                break;
                            default:
                                throw new ArgumentException($"Element name {reader.Name} is not handled.");
                        }
                    }
                }
            }
        }
    }
}
