﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class GOVERNMENT_MONITORING_EXTENSION : AbstractXmlSerializable
    {
        private GOVERNMENT_MONITORING_DETAIL governmentMonitoringDetail;

        private HMDA_ETHNICITIES hmdaEthnicities;

        private HMDA_ETHNICITY_ORIGINS hmdaEthnicityOrigins;

        private HMDA_RACES hmdaRaces;

        public GOVERNMENT_MONITORING_DETAIL GovernmentMonitoringDetail
        {
            get
            {
                if (this.governmentMonitoringDetail == null)
                {
                    this.governmentMonitoringDetail = new GOVERNMENT_MONITORING_DETAIL();
                }

                return this.governmentMonitoringDetail;
            }

            set
            {
                this.governmentMonitoringDetail = value;
            }
        }

        public HMDA_ETHNICITIES HmdaEthnicities
        {
            get
            {
                if (this.hmdaEthnicities == null)
                {
                    this.hmdaEthnicities = new HMDA_ETHNICITIES();
                }

                return this.hmdaEthnicities;
            }

            set
            {
                this.hmdaEthnicities = value;
            }
        }

        public HMDA_ETHNICITY_ORIGINS HmdaEthnicityOrigins
        {
            get
            {
                if (this.hmdaEthnicityOrigins == null)
                {
                    this.hmdaEthnicityOrigins = new HMDA_ETHNICITY_ORIGINS();
                }

                return this.hmdaEthnicityOrigins;
            }

            set
            {
                this.hmdaEthnicityOrigins = value;
            }
        }

        public HMDA_RACES HmdaRaces
        {
            get
            {
                if (this.hmdaRaces == null)
                {
                    this.hmdaRaces = new HMDA_RACES();
                }

                return this.hmdaRaces;
            }

            set
            {
                this.hmdaRaces = value;
            }
        }

        public bool WasReadFromXml
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(GOVERNMENT_MONITORING_EXTENSION));

            this.WriteElement(writer, this.governmentMonitoringDetail);
            this.WriteElement(writer, this.hmdaEthnicities);
            this.WriteElement(writer, this.hmdaEthnicityOrigins);
            this.WriteElement(writer, this.hmdaRaces);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            if (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(GOVERNMENT_MONITORING_EXTENSION))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(GOVERNMENT_MONITORING_EXTENSION))
                    {
                        break;
                    }

                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            
                            case nameof(GOVERNMENT_MONITORING_DETAIL):
                                GOVERNMENT_MONITORING_DETAIL governmentMonitoringDetail = new GOVERNMENT_MONITORING_DETAIL();
                                governmentMonitoringDetail.ReadXml(reader);
                                this.GovernmentMonitoringDetail = governmentMonitoringDetail;
                                break;
                            case nameof(HMDA_ETHNICITIES):
                                HMDA_ETHNICITIES hmdaEthnicities = new HMDA_ETHNICITIES();
                                hmdaEthnicities.ReadXml(reader);
                                this.HmdaEthnicities = hmdaEthnicities;
                                break;
                            case nameof(HMDA_ETHNICITY_ORIGINS):
                                HMDA_ETHNICITY_ORIGINS hmdaEthnicityOrigins = new HMDA_ETHNICITY_ORIGINS();
                                hmdaEthnicityOrigins.ReadXml(reader);
                                this.hmdaEthnicityOrigins = hmdaEthnicityOrigins;
                                break;
                            case nameof(HMDA_RACES):
                                HMDA_RACES hmdaRaces = new HMDA_RACES();
                                hmdaRaces.ReadXml(reader);
                                this.HmdaRaces = hmdaRaces;
                                break;
                            default:
                                throw new ArgumentException($"Element name {reader.Name} is not handled.");
                        }
                    }
                }
            }
        }
    }
}
