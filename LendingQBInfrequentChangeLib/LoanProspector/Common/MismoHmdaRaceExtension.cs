﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class HMDA_RACE_EXTENSION : AbstractXmlSerializable
    {
        private HMDA_RACE_DETAIL hmdaRaceDetail;

        private HMDA_RACE_DESIGNATIONS hmdaRaceDesignations;

        public HMDA_RACE_DETAIL HmdaRaceDetail
        {
            get
            {
                if (this.hmdaRaceDetail == null)
                {
                    this.hmdaRaceDetail = new HMDA_RACE_DETAIL();
                }

                return this.hmdaRaceDetail;
            }

            set
            {
                this.hmdaRaceDetail = value;
            }
        }

        public HMDA_RACE_DESIGNATIONS HmdaRaceDesignations
        {
            get
            {
                if (this.hmdaRaceDesignations == null)
                {
                    this.hmdaRaceDesignations = new HMDA_RACE_DESIGNATIONS();
                }

                return this.hmdaRaceDesignations;
            }

            set
            {
                this.hmdaRaceDesignations = value;
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_RACE_EXTENSION));

            this.WriteElement(writer, this.hmdaRaceDetail);
            this.WriteElement(writer, this.hmdaRaceDesignations);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_RACE_EXTENSION))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(HMDA_RACE_EXTENSION))
                    {
                        break;
                    }

                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case nameof(HMDA_RACE_DETAIL):
                                HMDA_RACE_DETAIL hmdaRaceDetail = new HMDA_RACE_DETAIL();
                                hmdaRaceDetail.ReadXml(reader);
                                this.HmdaRaceDetail = hmdaRaceDetail;
                                break;
                            case nameof(HMDA_RACE_DESIGNATIONS):
                                HMDA_RACE_DESIGNATIONS hmdaRaceDesignations = new HMDA_RACE_DESIGNATIONS();
                                hmdaRaceDesignations.ReadXml(reader);
                                this.HmdaRaceDesignations = hmdaRaceDesignations;
                                break;
                            default:
                                throw new ArgumentException($"Element name {reader.Name} is not handled.");
                        }
                    }
                }
            }
        }
    }
}
