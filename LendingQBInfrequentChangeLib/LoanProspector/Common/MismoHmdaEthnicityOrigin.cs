﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;
    public class HMDA_ETHNICITY_ORIGIN : AbstractXmlSerializable
    {
        private static readonly object[,] HmdaEthnicityOriginHmdaEthnicityOriginTypeTable =
        {
            { HmdaEthnicityOriginHmdaEthnicityOriginType.Cuban, "Cuban" },
            { HmdaEthnicityOriginHmdaEthnicityOriginType.Mexican, "Mexican" },
            { HmdaEthnicityOriginHmdaEthnicityOriginType.OtherHispanicOrLatino, "Other" },
            { HmdaEthnicityOriginHmdaEthnicityOriginType.PuertoRican, "PuertoRican" }
        };

        public HmdaEthnicityOriginHmdaEthnicityOriginType HMDAEthnicityOriginType
        {
            get;
            set;
        }

        public string HMDAEthnicityOriginTypeOtherDescription
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_ETHNICITY_ORIGIN));

            this.WriteAttribute(writer, nameof(this.HMDAEthnicityOriginType), this.HMDAEthnicityOriginType, HmdaEthnicityOriginHmdaEthnicityOriginTypeTable);
            this.WriteAttribute(writer, nameof(this.HMDAEthnicityOriginTypeOtherDescription), this.HMDAEthnicityOriginTypeOtherDescription);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_ETHNICITY_ORIGIN))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.HMDAEthnicityOriginType = (HmdaEthnicityOriginHmdaEthnicityOriginType)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDAEthnicityOriginType)), HmdaEthnicityOriginHmdaEthnicityOriginTypeTable);
            this.HMDAEthnicityOriginTypeOtherDescription = this.ReadAttribute(reader, nameof(this.HMDAEthnicityOriginTypeOtherDescription));
        }
    }

    public enum HmdaEthnicityOriginHmdaEthnicityOriginType
    {
        Undefined = 0,
        Mexican,
        PuertoRican,
        Cuban,
        OtherHispanicOrLatino
    }
}
