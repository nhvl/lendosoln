﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class HMDA_RACE_DESIGNATIONS : AbstractXmlSerializable
    {
        private List<HMDA_RACE_DESIGNATION> hmdaRaceDesignationList;

        public List<HMDA_RACE_DESIGNATION> HmdaRaceDesignationList
        {
            get
            {
                if (this.hmdaRaceDesignationList == null)
                {
                    this.hmdaRaceDesignationList = new List<HMDA_RACE_DESIGNATION>();
                }

                return this.hmdaRaceDesignationList;
            }

            set
            {
                this.hmdaRaceDesignationList = value;
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_RACE_DESIGNATIONS));

            this.WriteElement(writer, this.hmdaRaceDesignationList);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_RACE_DESIGNATIONS))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            if (reader.IsEmptyElement)
            {
                return;
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == nameof(HMDA_RACE_DESIGNATIONS))
                    {
                        break;
                    }

                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case nameof(HMDA_RACE_DESIGNATION):
                                HMDA_RACE_DESIGNATION hmdaRaceDesignation = new HMDA_RACE_DESIGNATION();
                                hmdaRaceDesignation.ReadXml(reader);
                                this.HmdaRaceDesignationList.Add(hmdaRaceDesignation);
                                break;
                            default:
                                throw new ArgumentException($"Element name {reader.Name} is not handled.");
                        }
                    }
                }
            }
        }
    }
}
