﻿namespace LendingQBInfrequentChangeLib.LoanProspector.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using LendingQBInfrequentChangeLib.LoanProspector.Common;

    public class HMDA_RACE_DESIGNATION : AbstractXmlSerializable
    {
        private static readonly object[,] HmdaRaceDesignationHmdaRaceDesignationTypeTable =
        {
            { HmdaRaceDesignationHmdaRaceDesignationType.AsianIndian, "AsianIndian" },
            { HmdaRaceDesignationHmdaRaceDesignationType.Chinese, "Chinese" },
            { HmdaRaceDesignationHmdaRaceDesignationType.Filipino, "Filipino" },
            { HmdaRaceDesignationHmdaRaceDesignationType.GuamanianOrChamorro, "GuamanianOrChamorro" },
            { HmdaRaceDesignationHmdaRaceDesignationType.Japanese, "Japanese" },
            { HmdaRaceDesignationHmdaRaceDesignationType.Korean, "Korean" },
            { HmdaRaceDesignationHmdaRaceDesignationType.NativeHawaiian, "NativeHawaiian" },
            { HmdaRaceDesignationHmdaRaceDesignationType.OtherAsian, "OtherAsian" },
            { HmdaRaceDesignationHmdaRaceDesignationType.OtherPacificIslander, "OtherPacificIslander" },
            { HmdaRaceDesignationHmdaRaceDesignationType.Samoan, "Samoan" },
            { HmdaRaceDesignationHmdaRaceDesignationType.Vietnamese, "Vietnamese" }
        };

        public string HMDARaceDesignationOtherAsianDescription
        {
            get;
            set;
        }

        public string HMDARaceDesignationOtherPacificIslanderDescription
        {
            get;
            set;
        }

        public HmdaRaceDesignationHmdaRaceDesignationType HMDARaceDesignationType
        {
            get;
            set;
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(nameof(HMDA_RACE_DESIGNATION));

            this.WriteAttribute(writer, nameof(this.HMDARaceDesignationOtherAsianDescription), this.HMDARaceDesignationOtherAsianDescription);
            this.WriteAttribute(writer, nameof(this.HMDARaceDesignationOtherPacificIslanderDescription), this.HMDARaceDesignationOtherPacificIslanderDescription);
            this.WriteAttribute(writer, nameof(this.HMDARaceDesignationType), this.HMDARaceDesignationType, HmdaRaceDesignationHmdaRaceDesignationTypeTable);

            writer.WriteEndElement();
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != nameof(HMDA_RACE_DESIGNATION))
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }

            this.HMDARaceDesignationOtherAsianDescription = this.ReadAttribute(reader, nameof(this.HMDARaceDesignationOtherAsianDescription));
            this.HMDARaceDesignationOtherPacificIslanderDescription = this.ReadAttribute(reader, nameof(this.HMDARaceDesignationOtherPacificIslanderDescription));
            this.HMDARaceDesignationType = (HmdaRaceDesignationHmdaRaceDesignationType)this.ConvertEnum(this.ReadAttribute(reader, nameof(this.HMDARaceDesignationType)), HmdaRaceDesignationHmdaRaceDesignationTypeTable);
        }
    }

    public enum HmdaRaceDesignationHmdaRaceDesignationType
    {
        Undefined = 0,
        AsianIndian,
        Chinese,
        Filipino,
        GuamanianOrChamorro,
        Japanese,
        Korean,
        NativeHawaiian,
        OtherAsian,
        OtherPacificIslander,
        Samoan,
        Vietnamese
    }
}
