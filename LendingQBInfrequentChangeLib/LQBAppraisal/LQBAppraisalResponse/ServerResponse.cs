// Generated by CodeMonkey on 2/20/2014 11:41:53 AM
using System;
using System.Collections.Generic;
using System.Xml;
namespace LQBAppraisal.LQBAppraisalResponse
{
    public class ServerResponse : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_ServerResponseRequestStatus m_requestStatus = E_ServerResponseRequestStatus.Undefined;
        private List<ServerMessage> m_serverMessageList = null;
        #endregion

        #region Public Properties
        public E_ServerResponseRequestStatus RequestStatus
        {
            get { return m_requestStatus; }
            set { m_requestStatus = value; }
        }
        public List<ServerMessage> ServerMessageList
        {
            get
            {
                if (null == m_serverMessageList) m_serverMessageList = new List<ServerMessage>();
                return m_serverMessageList;
            }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "ServerResponse")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_requestStatus = (E_ServerResponseRequestStatus) ReadAttribute(reader, "RequestStatus", EnumMappings.E_ServerResponseRequestStatus);
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "ServerResponse") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "ServerMessage":
                        ReadElement(reader, ServerMessageList);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ServerResponse");
            WriteAttribute(writer, "RequestStatus", EnumMappings.E_ServerResponseRequestStatus[(int) m_requestStatus]);
            WriteElement(writer, m_serverMessageList);
            writer.WriteEndElement();
        }
    }

    public enum E_ServerResponseRequestStatus
    {
        Undefined
        , Success
        , Failure
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_ServerResponseRequestStatus = {
                        ""
                         , "Success"
                         , "Failure"
                         };
    }
}
