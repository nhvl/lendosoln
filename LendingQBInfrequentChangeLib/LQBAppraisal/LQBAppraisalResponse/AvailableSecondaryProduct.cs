// Generated by CodeMonkey on 2/20/2014 11:41:53 AM
using System;
using System.Collections.Generic;
using System.Xml;
namespace LQBAppraisal.LQBAppraisalResponse
{
    public class AvailableSecondaryProduct : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_productName = string.Empty;
        #endregion

        #region Public Properties
        public string ProductName
        {
            get { return m_productName; }
            set { m_productName = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "AvailableSecondaryProduct")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_productName = ReadAttribute(reader, "ProductName");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "AvailableSecondaryProduct") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("AvailableSecondaryProduct");
            WriteAttribute(writer, "ProductName", m_productName);
            writer.WriteEndElement();
        }
    }
}
