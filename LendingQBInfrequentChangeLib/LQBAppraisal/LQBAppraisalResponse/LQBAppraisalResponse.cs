// Generated by CodeMonkey on 2/20/2014 11:41:53 AM
using System;
using System.Collections.Generic;
using System.Xml;
namespace LQBAppraisal.LQBAppraisalResponse
{
    public class LQBAppraisalResponse : AbstractXmlSerializable
    {
        #region Private Member Variables
        private ServerResponse m_serverResponse = null;
        private AccountInfoResponse m_accountInfoResponse = null;
        private OrderConfirmation m_orderConfirmation = null;
        #endregion

        #region Public Properties
        public ServerResponse ServerResponse
        {
            get
            {
                if (null == m_serverResponse) m_serverResponse = new ServerResponse();
                return m_serverResponse;
            }
            set { m_serverResponse = value; }
        }
        public AccountInfoResponse AccountInfoResponse
        {
            get
            {
                if (null == m_accountInfoResponse) m_accountInfoResponse = new AccountInfoResponse();
                return m_accountInfoResponse;
            }
            set { m_accountInfoResponse = value; }
        }
        public OrderConfirmation OrderConfirmation
        {
            get
            {
                if (null == m_orderConfirmation) m_orderConfirmation = new OrderConfirmation();
                return m_orderConfirmation;
            }
            set { m_orderConfirmation = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "LQBAppraisalResponse")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LQBAppraisalResponse") return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                #region Read Elements
                switch (reader.Name)
                {
                    case "ServerResponse":
                        ReadElement(reader, ServerResponse);
                        break;
                    case "AccountInfoResponse":
                        ReadElement(reader, AccountInfoResponse);
                        break;
                    case "OrderConfirmation":
                        ReadElement(reader, OrderConfirmation);
                        break;
                }
                #endregion
            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("LQBAppraisalResponse");
            WriteElement(writer, m_serverResponse);
            WriteElement(writer, m_accountInfoResponse);
            WriteElement(writer, m_orderConfirmation);
            writer.WriteEndElement();
        }
    }
}
