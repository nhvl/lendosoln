using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace LQBAppraisal
{
 
    public abstract class AbstractXmlSerializable : IXmlSerializable
    {
        protected string ReadAttribute(XmlReader reader, string attrName)
        {
            string value = reader.GetAttribute(attrName);
            
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            return value;
        }
        protected int ReadAttribute(XmlReader reader, string attrName, string[] enumMappings)
        {
            string value = reader.GetAttribute(attrName);

            if (!string.IsNullOrEmpty(value))
            {
                for (int i = 1; i < enumMappings.Length; i++)
                {
                    if (enumMappings[i] == value)
                        return i;
                }
            }
            
            return 0;
        }

        protected void ReadElement(XmlReader reader, IXmlSerializable el)
        {
            if (null == el)
                return;

            el.ReadXml(reader);
        }
        protected void ReadElement<T>(XmlReader reader, List<T> list) where T : IXmlSerializable, new()
        {
            if (null == list)
                return;

            T _o = new T();
            _o.ReadXml(reader);
            list.Add(_o);
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;

            writer.WriteAttributeString(attrName, value);
        }

        protected void WriteElement<T>(XmlWriter writer, List<T> list) where T : IXmlSerializable
        {
            if (null == list)
                return;

            foreach (IXmlSerializable o in list)
            {
                WriteElement(writer, o);
            }
        }

        protected void WriteElement(XmlWriter writer, IXmlSerializable item)
        {
            if (null == item)
                return;

            item.WriteXml(writer);

        }
        protected void WriteElementString(XmlWriter writer, string name, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;

            if (value.IndexOf('<') >= 0)
                writer.WriteCData(value);
            else
                writer.WriteElementString(name, value);
        }
        protected void WriteElementString(XmlWriter writer, string name, List<string> list)
        {
            if (null == list)
                return;

            foreach (string value in list)
            {
                WriteElementString(writer, name, value);
            }
        }
        protected void WriteString(XmlWriter writer, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;

            writer.WriteString(value);
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public abstract void ReadXml(System.Xml.XmlReader reader);

        public abstract void WriteXml(System.Xml.XmlWriter writer);

        #endregion
    }

    public enum E_YNIndicator
    {
        Undefined,
        Y,
        N
    }
    public enum E_YesNo
    {
        Undefined,
        Yes,
        No
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_YNIndicator = { "", "Y", "N" };
        public static readonly string[] E_YesNo = { "", "Yes", "No" };
    }
}