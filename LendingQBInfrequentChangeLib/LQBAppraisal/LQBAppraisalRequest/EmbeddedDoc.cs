// Generated by CodeMonkey on 2/20/2014 11:41:53 AM
using System;
using System.Collections.Generic;
using System.Xml;
namespace LQBAppraisal.LQBAppraisalRequest
{
    public class EmbeddedDoc : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_documentName = string.Empty;
        private E_EmbeddedDocDocumentFormat m_documentFormat = E_EmbeddedDocDocumentFormat.Undefined;
        private string m_content = string.Empty;
        #endregion

        #region Public Properties
        public string DocumentName
        {
            get { return m_documentName; }
            set { m_documentName = value; }
        }
        public E_EmbeddedDocDocumentFormat DocumentFormat
        {
            get { return m_documentFormat; }
            set { m_documentFormat = value; }
        }
        public string Content
        {
            get { return m_content; }
            set { m_content = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "EmbeddedDoc")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_documentName = ReadAttribute(reader, "DocumentName");
            m_documentFormat = (E_EmbeddedDocDocumentFormat) ReadAttribute(reader, "DocumentFormat", EnumMappings.E_EmbeddedDocDocumentFormat);
            #endregion

            if (reader.IsEmptyElement) return;
            m_content = reader.ReadString();
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("EmbeddedDoc");
            WriteAttribute(writer, "DocumentName", m_documentName);
            WriteAttribute(writer, "DocumentFormat", EnumMappings.E_EmbeddedDocDocumentFormat[(int) m_documentFormat]);
            WriteString(writer, m_content);
            writer.WriteEndElement();
        }
    }

    public enum E_EmbeddedDocDocumentFormat
    {
        Undefined
        , PDF
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_EmbeddedDocDocumentFormat = {
                        ""
                         , "PDF"
                         };
    }
}
