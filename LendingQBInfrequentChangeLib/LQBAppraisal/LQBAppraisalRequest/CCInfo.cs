// Generated by CodeMonkey on 2/20/2014 11:41:53 AM
using System;
using System.Collections.Generic;
using System.Xml;
namespace LQBAppraisal.LQBAppraisalRequest
{
    public class CCInfo : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_cCType = string.Empty;
        private string m_cCNumber = string.Empty;
        private string m_cCExpMonth = string.Empty;
        private string m_cCExpYear = string.Empty;
        private string m_cCSecurityCode = string.Empty;
        #endregion

        #region Public Properties
        public string CCType
        {
            get { return m_cCType; }
            set { m_cCType = value; }
        }
        public string CCNumber
        {
            get { return m_cCNumber; }
            set { m_cCNumber = value; }
        }
        public string CCExpMonth
        {
            get { return m_cCExpMonth; }
            set { m_cCExpMonth = value; }
        }
        public string CCExpYear
        {
            get { return m_cCExpYear; }
            set { m_cCExpYear = value; }
        }
        public string CCSecurityCode
        {
            get { return m_cCSecurityCode; }
            set { m_cCSecurityCode = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "CCInfo")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            #region Read Attributes
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "CCInfo") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("CCInfo");
            WriteAttribute(writer, "CCType", m_cCType);
            WriteAttribute(writer, "CCNumber", m_cCNumber);
            WriteAttribute(writer, "CCExpMonth", m_cCExpMonth);
            WriteAttribute(writer, "CCExpYear", m_cCExpYear);
            WriteAttribute(writer, "CCSecurityCode", m_cCSecurityCode);
            writer.WriteEndElement();
        }
    }
}
