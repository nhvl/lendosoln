// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum LoanMarginCalculationMethodType
    {
        Undefined = 0,
        BasedOnProduct,
        Constant,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, LoanMarginCalculationMethodType value)
        {
            if (value == LoanMarginCalculationMethodType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case LoanMarginCalculationMethodType.BasedOnProduct:
                    _v = "BasedOnProduct"; break;
                case LoanMarginCalculationMethodType.Constant:
                    _v = "Constant"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected LoanMarginCalculationMethodType ReadElementString_LoanMarginCalculationMethodType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return LoanMarginCalculationMethodType.Undefined;
            }
            switch (str)
            {
                case "BasedOnProduct":
                    return LoanMarginCalculationMethodType.BasedOnProduct;
                case "Constant":
                    return LoanMarginCalculationMethodType.Constant;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
