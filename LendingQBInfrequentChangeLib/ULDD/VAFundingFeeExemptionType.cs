// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum VAFundingFeeExemptionType
    {
        Undefined = 0,
        CompletelyExempt,
        NotExempt,
        PartiallyExempt,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, VAFundingFeeExemptionType value)
        {
            if (value == VAFundingFeeExemptionType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case VAFundingFeeExemptionType.CompletelyExempt:
                    _v = "CompletelyExempt"; break;
                case VAFundingFeeExemptionType.NotExempt:
                    _v = "NotExempt"; break;
                case VAFundingFeeExemptionType.PartiallyExempt:
                    _v = "PartiallyExempt"; break;
                case VAFundingFeeExemptionType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected VAFundingFeeExemptionType ReadElementString_VAFundingFeeExemptionType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return VAFundingFeeExemptionType.Undefined;
            }
            switch (str)
            {
                case "CompletelyExempt":
                    return VAFundingFeeExemptionType.CompletelyExempt;
                case "NotExempt":
                    return VAFundingFeeExemptionType.NotExempt;
                case "PartiallyExempt":
                    return VAFundingFeeExemptionType.PartiallyExempt;
                case "Other":
                    return VAFundingFeeExemptionType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
