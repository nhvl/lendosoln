// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class FipsInformation : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "FIPS_INFORMATION"; } }

        #region Property List
        public string FIPSCountryCode { get; set; }
        public string FIPSCountyCode { get; set; }
        public string FIPSCountySubdivisionCode { get; set; }
        public string FIPSCountySubdivisionName { get; set; }
        public FIPSCountySubdivisionType FIPSCountySubdivisionType { get; set; }
        public string FIPSPlaceCode { get; set; }
        public string FIPSPlaceName { get; set; }
        public string FIPSStateAlphaCode { get; set; }
        public string FIPSStateNumericCode { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "FIPSCountryCode":
                    FIPSCountryCode = ReadElementString(reader); break;
                case "FIPSCountyCode":
                    FIPSCountyCode = ReadElementString(reader); break;
                case "FIPSCountySubdivisionCode":
                    FIPSCountySubdivisionCode = ReadElementString(reader); break;
                case "FIPSCountySubdivisionName":
                    FIPSCountySubdivisionName = ReadElementString(reader); break;
                case "FIPSCountySubdivisionType":
                    FIPSCountySubdivisionType = ReadElementString_FIPSCountySubdivisionType(reader); break;
                case "FIPSPlaceCode":
                    FIPSPlaceCode = ReadElementString(reader); break;
                case "FIPSPlaceName":
                    FIPSPlaceName = ReadElementString(reader); break;
                case "FIPSStateAlphaCode":
                    FIPSStateAlphaCode = ReadElementString(reader); break;
                case "FIPSStateNumericCode":
                    FIPSStateNumericCode = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "FIPSCountryCode", FIPSCountryCode);
            WriteElementString(writer, "FIPSCountyCode", FIPSCountyCode);
            WriteElementString(writer, "FIPSCountySubdivisionCode", FIPSCountySubdivisionCode);
            WriteElementString(writer, "FIPSCountySubdivisionName", FIPSCountySubdivisionName);
            WriteElementString(writer, "FIPSCountySubdivisionType", FIPSCountySubdivisionType);
            WriteElementString(writer, "FIPSPlaceCode", FIPSPlaceCode);
            WriteElementString(writer, "FIPSPlaceName", FIPSPlaceName);
            WriteElementString(writer, "FIPSStateAlphaCode", FIPSStateAlphaCode);
            WriteElementString(writer, "FIPSStateNumericCode", FIPSStateNumericCode);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
