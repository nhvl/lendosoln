// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class AmortizationScheduleItem : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "AMORTIZATION_SCHEDULE_ITEM"; } }

        #region Property List
        public string AmortizationScheduleEndingBalanceAmount { get; set; }
        public string AmortizationScheduleInterestPortionAmount { get; set; }
        public string AmortizationScheduleInterestRatePercent { get; set; }
        public string AmortizationScheduleLoanToValuePercent { get; set; }
        public string AmortizationScheduleMIPaymentAmount { get; set; }
        public string AmortizationSchedulePaymentAmount { get; set; }
        public string AmortizationSchedulePaymentDueDate { get; set; }
        public string AmortizationSchedulePaymentNumber { get; set; }
        public string AmortizationSchedulePrincipalPortionAmount { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AmortizationScheduleEndingBalanceAmount":
                    AmortizationScheduleEndingBalanceAmount = ReadElementString(reader); break;
                case "AmortizationScheduleInterestPortionAmount":
                    AmortizationScheduleInterestPortionAmount = ReadElementString(reader); break;
                case "AmortizationScheduleInterestRatePercent":
                    AmortizationScheduleInterestRatePercent = ReadElementString(reader); break;
                case "AmortizationScheduleLoanToValuePercent":
                    AmortizationScheduleLoanToValuePercent = ReadElementString(reader); break;
                case "AmortizationScheduleMIPaymentAmount":
                    AmortizationScheduleMIPaymentAmount = ReadElementString(reader); break;
                case "AmortizationSchedulePaymentAmount":
                    AmortizationSchedulePaymentAmount = ReadElementString(reader); break;
                case "AmortizationSchedulePaymentDueDate":
                    AmortizationSchedulePaymentDueDate = ReadElementString(reader); break;
                case "AmortizationSchedulePaymentNumber":
                    AmortizationSchedulePaymentNumber = ReadElementString(reader); break;
                case "AmortizationSchedulePrincipalPortionAmount":
                    AmortizationSchedulePrincipalPortionAmount = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "AmortizationScheduleEndingBalanceAmount", AmortizationScheduleEndingBalanceAmount);
            WriteElementString(writer, "AmortizationScheduleInterestPortionAmount", AmortizationScheduleInterestPortionAmount);
            WriteElementString(writer, "AmortizationScheduleInterestRatePercent", AmortizationScheduleInterestRatePercent);
            WriteElementString(writer, "AmortizationScheduleLoanToValuePercent", AmortizationScheduleLoanToValuePercent);
            WriteElementString(writer, "AmortizationScheduleMIPaymentAmount", AmortizationScheduleMIPaymentAmount);
            WriteElementString(writer, "AmortizationSchedulePaymentAmount", AmortizationSchedulePaymentAmount);
            WriteElementString(writer, "AmortizationSchedulePaymentDueDate", AmortizationSchedulePaymentDueDate);
            WriteElementString(writer, "AmortizationSchedulePaymentNumber", AmortizationSchedulePaymentNumber);
            WriteElementString(writer, "AmortizationSchedulePrincipalPortionAmount", AmortizationSchedulePrincipalPortionAmount);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
