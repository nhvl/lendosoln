// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Borrower : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "BORROWER"; } }

        #region Property List
        private Bankruptcy m_bankruptcy;
        public Bankruptcy Bankruptcy
        {
            get
            {
                if (m_bankruptcy == null)
                {
                    m_bankruptcy = new Bankruptcy();
                }
                return m_bankruptcy;
            }
            set { m_bankruptcy = value; }
        }
        private BorrowerDetail m_borrowerDetail;
        public BorrowerDetail BorrowerDetail
        {
            get
            {
                if (m_borrowerDetail == null)
                {
                    m_borrowerDetail = new BorrowerDetail();
                }
                return m_borrowerDetail;
            }
            set { m_borrowerDetail = value; }
        }
        private CounselingConfirmation m_counselingConfirmation;
        public CounselingConfirmation CounselingConfirmation
        {
            get
            {
                if (m_counselingConfirmation == null)
                {
                    m_counselingConfirmation = new CounselingConfirmation();
                }
                return m_counselingConfirmation;
            }
            set { m_counselingConfirmation = value; }
        }
        private CreditScores m_creditScores;
        public CreditScores CreditScores
        {
            get
            {
                if (m_creditScores == null)
                {
                    m_creditScores = new CreditScores();
                }
                return m_creditScores;
            }
            set { m_creditScores = value; }
        }
        private CurrentIncome m_currentIncome;
        public CurrentIncome CurrentIncome
        {
            get
            {
                if (m_currentIncome == null)
                {
                    m_currentIncome = new CurrentIncome();
                }
                return m_currentIncome;
            }
            set { m_currentIncome = value; }
        }
        private Declaration m_declaration;
        public Declaration Declaration
        {
            get
            {
                if (m_declaration == null)
                {
                    m_declaration = new Declaration();
                }
                return m_declaration;
            }
            set { m_declaration = value; }
        }
        private Dependents m_dependents;
        public Dependents Dependents
        {
            get
            {
                if (m_dependents == null)
                {
                    m_dependents = new Dependents();
                }
                return m_dependents;
            }
            set { m_dependents = value; }
        }
        private Employers m_employers;
        public Employers Employers
        {
            get
            {
                if (m_employers == null)
                {
                    m_employers = new Employers();
                }
                return m_employers;
            }
            set { m_employers = value; }
        }
        private GovernmentBorrower m_governmentBorrower;
        public GovernmentBorrower GovernmentBorrower
        {
            get
            {
                if (m_governmentBorrower == null)
                {
                    m_governmentBorrower = new GovernmentBorrower();
                }
                return m_governmentBorrower;
            }
            set { m_governmentBorrower = value; }
        }
        private GovernmentMonitoring m_governmentMonitoring;
        public GovernmentMonitoring GovernmentMonitoring
        {
            get
            {
                if (m_governmentMonitoring == null)
                {
                    m_governmentMonitoring = new GovernmentMonitoring();
                }
                return m_governmentMonitoring;
            }
            set { m_governmentMonitoring = value; }
        }
        private PresentHousingExpenses m_presentHousingExpenses;
        public PresentHousingExpenses PresentHousingExpenses
        {
            get
            {
                if (m_presentHousingExpenses == null)
                {
                    m_presentHousingExpenses = new PresentHousingExpenses();
                }
                return m_presentHousingExpenses;
            }
            set { m_presentHousingExpenses = value; }
        }
        private Residences m_residences;
        public Residences Residences
        {
            get
            {
                if (m_residences == null)
                {
                    m_residences = new Residences();
                }
                return m_residences;
            }
            set { m_residences = value; }
        }
        private NearestLivingRelative m_nearestLivingRelative;
        public NearestLivingRelative NearestLivingRelative
        {
            get
            {
                if (m_nearestLivingRelative == null)
                {
                    m_nearestLivingRelative = new NearestLivingRelative();
                }
                return m_nearestLivingRelative;
            }
            set { m_nearestLivingRelative = value; }
        }
        private SolicitationPreferences m_solicitationPreferences;
        public SolicitationPreferences SolicitationPreferences
        {
            get
            {
                if (m_solicitationPreferences == null)
                {
                    m_solicitationPreferences = new SolicitationPreferences();
                }
                return m_solicitationPreferences;
            }
            set { m_solicitationPreferences = value; }
        }
        private Summaries m_summaries;
        public Summaries Summaries
        {
            get
            {
                if (m_summaries == null)
                {
                    m_summaries = new Summaries();
                }
                return m_summaries;
            }
            set { m_summaries = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "BANKRUPTCY":
                    ReadElement(reader, Bankruptcy); break;
                case "BORROWER_DETAIL":
                    ReadElement(reader, BorrowerDetail); break;
                case "COUNSELING_CONFIRMATION":
                    ReadElement(reader, CounselingConfirmation); break;
                case "CREDIT_SCORES":
                    ReadElement(reader, CreditScores); break;
                case "CURRENT_INCOME":
                    ReadElement(reader, CurrentIncome); break;
                case "DECLARATION":
                    ReadElement(reader, Declaration); break;
                case "DEPENDENTS":
                    ReadElement(reader, Dependents); break;
                case "EMPLOYERS":
                    ReadElement(reader, Employers); break;
                case "GOVERNMENT_BORROWER":
                    ReadElement(reader, GovernmentBorrower); break;
                case "GOVERNMENT_MONITORING":
                    ReadElement(reader, GovernmentMonitoring); break;
                case "PRESENT_HOUSING_EXPENSES":
                    ReadElement(reader, PresentHousingExpenses); break;
                case "RESIDENCES":
                    ReadElement(reader, Residences); break;
                case "NEAREST_LIVING_RELATIVE":
                    ReadElement(reader, NearestLivingRelative); break;
                case "SOLICITATION_PREFERENCES":
                    ReadElement(reader, SolicitationPreferences); break;
                case "SUMMARIES":
                    ReadElement(reader, Summaries); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_bankruptcy);
            WriteElement(writer, m_borrowerDetail);
            WriteElement(writer, m_counselingConfirmation);
            WriteElement(writer, m_creditScores);
            WriteElement(writer, m_currentIncome);
            WriteElement(writer, m_declaration);
            WriteElement(writer, m_dependents);
            WriteElement(writer, m_employers);
            WriteElement(writer, m_governmentBorrower);
            WriteElement(writer, m_governmentMonitoring);
            WriteElement(writer, m_presentHousingExpenses);
            WriteElement(writer, m_residences);
            WriteElement(writer, m_nearestLivingRelative);
            WriteElement(writer, m_solicitationPreferences);
            WriteElement(writer, m_summaries);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
