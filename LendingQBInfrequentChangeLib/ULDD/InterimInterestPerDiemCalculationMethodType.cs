// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum InterimInterestPerDiemCalculationMethodType
    {
        Undefined = 0,
        _360,
        _365,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, InterimInterestPerDiemCalculationMethodType value)
        {
            if (value == InterimInterestPerDiemCalculationMethodType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case InterimInterestPerDiemCalculationMethodType._360:
                    _v = "360"; break;
                case InterimInterestPerDiemCalculationMethodType._365:
                    _v = "365"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected InterimInterestPerDiemCalculationMethodType ReadElementString_InterimInterestPerDiemCalculationMethodType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return InterimInterestPerDiemCalculationMethodType.Undefined;
            }
            switch (str)
            {
                case "360":
                    return InterimInterestPerDiemCalculationMethodType._360;
                case "365":
                    return InterimInterestPerDiemCalculationMethodType._365;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
