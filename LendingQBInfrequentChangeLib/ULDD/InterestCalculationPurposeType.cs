// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum InterestCalculationPurposeType
    {
        Undefined = 0,
        Draw,
        Other,
        Payoff,
        ServicerAdvance,
        Standard,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, InterestCalculationPurposeType value)
        {
            if (value == InterestCalculationPurposeType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case InterestCalculationPurposeType.Draw:
                    _v = "Draw"; break;
                case InterestCalculationPurposeType.Other:
                    _v = "Other"; break;
                case InterestCalculationPurposeType.Payoff:
                    _v = "Payoff"; break;
                case InterestCalculationPurposeType.ServicerAdvance:
                    _v = "ServicerAdvance"; break;
                case InterestCalculationPurposeType.Standard:
                    _v = "Standard"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected InterestCalculationPurposeType ReadElementString_InterestCalculationPurposeType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return InterestCalculationPurposeType.Undefined;
            }
            switch (str)
            {
                case "Draw":
                    return InterestCalculationPurposeType.Draw;
                case "Other":
                    return InterestCalculationPurposeType.Other;
                case "Payoff":
                    return InterestCalculationPurposeType.Payoff;
                case "ServicerAdvance":
                    return InterestCalculationPurposeType.ServicerAdvance;
                case "Standard":
                    return InterestCalculationPurposeType.Standard;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
