// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class PurchaseCredit : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PURCHASE_CREDIT"; } }

        #region Property List
        public string PurchaseCreditAmount { get; set; }
        public PurchaseCreditSourceType PurchaseCreditSourceType { get; set; }
        public string PurchaseCreditSourceTypeOtherDescription { get; set; }
        public PurchaseCreditType PurchaseCreditType { get; set; }
        public string PurchaseCreditTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "PurchaseCreditAmount":
                    PurchaseCreditAmount = ReadElementString(reader); break;
                case "PurchaseCreditSourceType":
                    PurchaseCreditSourceType = ReadElementString_PurchaseCreditSourceType(reader); break;
                case "PurchaseCreditSourceTypeOtherDescription":
                    PurchaseCreditSourceTypeOtherDescription = ReadElementString(reader); break;
                case "PurchaseCreditType":
                    PurchaseCreditType = ReadElementString_PurchaseCreditType(reader); break;
                case "PurchaseCreditTypeOtherDescription":
                    PurchaseCreditTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "PurchaseCreditAmount", PurchaseCreditAmount);
            WriteElementString(writer, "PurchaseCreditSourceType", PurchaseCreditSourceType);
            WriteElementString(writer, "PurchaseCreditSourceTypeOtherDescription", PurchaseCreditSourceTypeOtherDescription);
            WriteElementString(writer, "PurchaseCreditType", PurchaseCreditType);
            WriteElementString(writer, "PurchaseCreditTypeOtherDescription", PurchaseCreditTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
