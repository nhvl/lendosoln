// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class TitleResponse : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "TITLE_RESPONSE"; } }

        #region Property List
        private Executions m_executions;
        public Executions Executions
        {
            get
            {
                if (m_executions == null)
                {
                    m_executions = new Executions();
                }
                return m_executions;
            }
            set { m_executions = value; }
        }
        private LegalAndVestings m_legalAndVestings;
        public LegalAndVestings LegalAndVestings
        {
            get
            {
                if (m_legalAndVestings == null)
                {
                    m_legalAndVestings = new LegalAndVestings();
                }
                return m_legalAndVestings;
            }
            set { m_legalAndVestings = value; }
        }
        private Loans m_loans;
        public Loans Loans
        {
            get
            {
                if (m_loans == null)
                {
                    m_loans = new Loans();
                }
                return m_loans;
            }
            set { m_loans = value; }
        }
        private Parties m_parties;
        public Parties Parties
        {
            get
            {
                if (m_parties == null)
                {
                    m_parties = new Parties();
                }
                return m_parties;
            }
            set { m_parties = value; }
        }
        private Property m_property;
        public Property Property
        {
            get
            {
                if (m_property == null)
                {
                    m_property = new Property();
                }
                return m_property;
            }
            set { m_property = value; }
        }
        private RecordingEndorsementInformation m_recordingEndorsementInformation;
        public RecordingEndorsementInformation RecordingEndorsementInformation
        {
            get
            {
                if (m_recordingEndorsementInformation == null)
                {
                    m_recordingEndorsementInformation = new RecordingEndorsementInformation();
                }
                return m_recordingEndorsementInformation;
            }
            set { m_recordingEndorsementInformation = value; }
        }
        private TitleAdditionalExceptions m_titleAdditionalExceptions;
        public TitleAdditionalExceptions TitleAdditionalExceptions
        {
            get
            {
                if (m_titleAdditionalExceptions == null)
                {
                    m_titleAdditionalExceptions = new TitleAdditionalExceptions();
                }
                return m_titleAdditionalExceptions;
            }
            set { m_titleAdditionalExceptions = value; }
        }
        private TitleEndorsements m_titleEndorsements;
        public TitleEndorsements TitleEndorsements
        {
            get
            {
                if (m_titleEndorsements == null)
                {
                    m_titleEndorsements = new TitleEndorsements();
                }
                return m_titleEndorsements;
            }
            set { m_titleEndorsements = value; }
        }
        private TitleResponseDetail m_titleResponseDetail;
        public TitleResponseDetail TitleResponseDetail
        {
            get
            {
                if (m_titleResponseDetail == null)
                {
                    m_titleResponseDetail = new TitleResponseDetail();
                }
                return m_titleResponseDetail;
            }
            set { m_titleResponseDetail = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "EXECUTIONS":
                    ReadElement(reader, Executions); break;
                case "LEGAL_AND_VESTINGS":
                    ReadElement(reader, LegalAndVestings); break;
                case "LOANS":
                    ReadElement(reader, Loans); break;
                case "PARTIES":
                    ReadElement(reader, Parties); break;
                case "PROPERTY":
                    ReadElement(reader, Property); break;
                case "RECORDING_ENDORSEMENT_INFORMATION":
                    ReadElement(reader, RecordingEndorsementInformation); break;
                case "TITLE_ADDITIONAL_EXCEPTIONS":
                    ReadElement(reader, TitleAdditionalExceptions); break;
                case "TITLE_ENDORSEMENTS":
                    ReadElement(reader, TitleEndorsements); break;
                case "TITLE_RESPONSE_DETAIL":
                    ReadElement(reader, TitleResponseDetail); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_executions);
            WriteElement(writer, m_legalAndVestings);
            WriteElement(writer, m_loans);
            WriteElement(writer, m_parties);
            WriteElement(writer, m_property);
            WriteElement(writer, m_recordingEndorsementInformation);
            WriteElement(writer, m_titleAdditionalExceptions);
            WriteElement(writer, m_titleEndorsements);
            WriteElement(writer, m_titleResponseDetail);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
