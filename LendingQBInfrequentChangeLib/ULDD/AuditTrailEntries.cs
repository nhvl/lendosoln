// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class AuditTrailEntries : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "AUDIT_TRAIL_ENTRIES"; } }

        #region Property List
        private List<AuditTrailEntry> m_auditTrailEntryList;
        public List<AuditTrailEntry> AuditTrailEntryList
        {
            get
            {
                if (m_auditTrailEntryList == null)
                {
                    m_auditTrailEntryList = new List<AuditTrailEntry>();
                }
                return m_auditTrailEntryList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AUDIT_TRAIL_ENTRY":
                    ReadElement(reader, AuditTrailEntryList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_auditTrailEntryList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
