// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum PayoffActionType
    {
        Undefined = 0,
        LenderOrderBothPayoffSubordinate,
        LenderOrderPayoff,
        LenderOrderSubordinate,
        Other,
        TitleCompanyOrderBothPayoffSubordinate,
        TitleCompanyOrderPayoff,
        TitleCompanyOrderSubordinate,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, PayoffActionType value)
        {
            if (value == PayoffActionType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case PayoffActionType.LenderOrderBothPayoffSubordinate:
                    _v = "LenderOrderBothPayoffSubordinate"; break;
                case PayoffActionType.LenderOrderPayoff:
                    _v = "LenderOrderPayoff"; break;
                case PayoffActionType.LenderOrderSubordinate:
                    _v = "LenderOrderSubordinate"; break;
                case PayoffActionType.Other:
                    _v = "Other"; break;
                case PayoffActionType.TitleCompanyOrderBothPayoffSubordinate:
                    _v = "TitleCompanyOrderBothPayoffSubordinate"; break;
                case PayoffActionType.TitleCompanyOrderPayoff:
                    _v = "TitleCompanyOrderPayoff"; break;
                case PayoffActionType.TitleCompanyOrderSubordinate:
                    _v = "TitleCompanyOrderSubordinate"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected PayoffActionType ReadElementString_PayoffActionType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return PayoffActionType.Undefined;
            }
            switch (str)
            {
                case "LenderOrderBothPayoffSubordinate":
                    return PayoffActionType.LenderOrderBothPayoffSubordinate;
                case "LenderOrderPayoff":
                    return PayoffActionType.LenderOrderPayoff;
                case "LenderOrderSubordinate":
                    return PayoffActionType.LenderOrderSubordinate;
                case "Other":
                    return PayoffActionType.Other;
                case "TitleCompanyOrderBothPayoffSubordinate":
                    return PayoffActionType.TitleCompanyOrderBothPayoffSubordinate;
                case "TitleCompanyOrderPayoff":
                    return PayoffActionType.TitleCompanyOrderPayoff;
                case "TitleCompanyOrderSubordinate":
                    return PayoffActionType.TitleCompanyOrderSubordinate;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
