// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Compensations : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "COMPENSATIONS"; } }

        #region Property List
        private List<Compensation> m_compensationList;
        public List<Compensation> CompensationList
        {
            get
            {
                if (m_compensationList == null)
                {
                    m_compensationList = new List<Compensation>();
                }
                return m_compensationList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "COMPENSATION":
                    ReadElement(reader, CompensationList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_compensationList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
