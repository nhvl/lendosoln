// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class AboutVersions : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "ABOUT_VERSIONS"; } }

        #region Property List
        private List<AboutVersion> m_aboutVersionList;
        public List<AboutVersion> AboutVersionList
        {
            get
            {
                if (m_aboutVersionList == null)
                {
                    m_aboutVersionList = new List<AboutVersion>();
                }
                return m_aboutVersionList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ABOUT_VERSION":
                    ReadElement(reader, AboutVersionList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_aboutVersionList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
