// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class DocumentSets : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "DOCUMENT_SETS"; } }

        #region Property List
        private List<DocumentSet> m_documentSetList;
        public List<DocumentSet> DocumentSetList
        {
            get
            {
                if (m_documentSetList == null)
                {
                    m_documentSetList = new List<DocumentSet>();
                }
                return m_documentSetList;
            }
        }
        private ForeignObjects m_foreignObjects;
        public ForeignObjects ForeignObjects
        {
            get
            {
                if (m_foreignObjects == null)
                {
                    m_foreignObjects = new ForeignObjects();
                }
                return m_foreignObjects;
            }
            set { m_foreignObjects = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DOCUMENT_SET":
                    ReadElement(reader, DocumentSetList); break;
                case "FOREIGN_OBJECTS":
                    ReadElement(reader, ForeignObjects); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_documentSetList);
            WriteElement(writer, m_foreignObjects);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
