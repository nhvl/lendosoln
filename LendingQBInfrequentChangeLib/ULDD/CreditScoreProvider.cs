// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class CreditScoreProvider : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "CREDIT_SCORE_PROVIDER"; } }

        #region Property List
        private Address m_address;
        public Address Address
        {
            get
            {
                if (m_address == null)
                {
                    m_address = new Address();
                }
                return m_address;
            }
            set { m_address = value; }
        }
        private CreditScoreProviderDetail m_creditScoreProviderDetail;
        public CreditScoreProviderDetail CreditScoreProviderDetail
        {
            get
            {
                if (m_creditScoreProviderDetail == null)
                {
                    m_creditScoreProviderDetail = new CreditScoreProviderDetail();
                }
                return m_creditScoreProviderDetail;
            }
            set { m_creditScoreProviderDetail = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ADDRESS":
                    ReadElement(reader, Address); break;
                case "CREDIT_SCORE_PROVIDER_DETAIL":
                    ReadElement(reader, CreditScoreProviderDetail); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_address);
            WriteElement(writer, m_creditScoreProviderDetail);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
