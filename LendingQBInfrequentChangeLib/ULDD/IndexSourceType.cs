// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum IndexSourceType
    {
        Undefined = 0,
        FHLBEleventhDistrictMonthlyCostOfFundsIndex,
        LIBOROneMonthWSJ25thDayOfMonth,
        LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay,
        LIBOROneMonthWSJDaily,
        LIBOROneMonthWSJFifteenthDayOfMonth,
        LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay,
        LIBOROneYearWSJDaily,
        LIBORSixMonthWSJ25thDayOfMonth,
        LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay,
        LIBORSixMonthWSJFifteenthDayOfMonth,
        LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay,
        LIBORSixMonthWSJLastBusinessDayOfMonth,
        MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15,
        MonthlyNationalAverageContractRateForThePurchaseOfPreviouslyOccupiedHomesByCombinedLen,
        MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15,
        MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15,
        NationalMonthlyMedianCostOfFundsIndexOTS,
        Other,
        PrimeRateWSJEffectiveDate,
        PrimeRateWSJPublicationDate,
        SixMonthLIBOR_WSJDaily,
        SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth,
        SixMonthUSTBillMonthlyAuctionDiscountRateCalculated,
        SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated,
        SixMonthUSTBillWeeklyAuctionDiscountRateUST,
        SixMonthUSTBillWeeklyAuctionInvestmentYieldUST,
        WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15,
        WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15,
        WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15,
        WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15,
        WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, IndexSourceType value)
        {
            if (value == IndexSourceType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case IndexSourceType.FHLBEleventhDistrictMonthlyCostOfFundsIndex:
                    _v = "FHLBEleventhDistrictMonthlyCostOfFundsIndex"; break;
                case IndexSourceType.LIBOROneMonthWSJ25thDayOfMonth:
                    _v = "LIBOROneMonthWSJ25thDayOfMonth"; break;
                case IndexSourceType.LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay:
                    _v = "LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay"; break;
                case IndexSourceType.LIBOROneMonthWSJDaily:
                    _v = "LIBOROneMonthWSJDaily"; break;
                case IndexSourceType.LIBOROneMonthWSJFifteenthDayOfMonth:
                    _v = "LIBOROneMonthWSJFifteenthDayOfMonth"; break;
                case IndexSourceType.LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay:
                    _v = "LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay"; break;
                case IndexSourceType.LIBOROneYearWSJDaily:
                    _v = "LIBOROneYearWSJDaily"; break;
                case IndexSourceType.LIBORSixMonthWSJ25thDayOfMonth:
                    _v = "LIBORSixMonthWSJ25thDayOfMonth"; break;
                case IndexSourceType.LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay:
                    _v = "LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay"; break;
                case IndexSourceType.LIBORSixMonthWSJFifteenthDayOfMonth:
                    _v = "LIBORSixMonthWSJFifteenthDayOfMonth"; break;
                case IndexSourceType.LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay:
                    _v = "LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay"; break;
                case IndexSourceType.LIBORSixMonthWSJLastBusinessDayOfMonth:
                    _v = "LIBORSixMonthWSJLastBusinessDayOfMonth"; break;
                case IndexSourceType.MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15:
                    _v = "MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15"; break;
                case IndexSourceType.MonthlyNationalAverageContractRateForThePurchaseOfPreviouslyOccupiedHomesByCombinedLen:
                    _v = "MonthlyNationalAverageContractRateForThePurchaseOfPreviouslyOccupiedHomesByCombinedLen"; break;
                case IndexSourceType.MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15:
                    _v = "MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15"; break;
                case IndexSourceType.MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15:
                    _v = "MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15"; break;
                case IndexSourceType.NationalMonthlyMedianCostOfFundsIndexOTS:
                    _v = "NationalMonthlyMedianCostOfFundsIndexOTS"; break;
                case IndexSourceType.Other:
                    _v = "Other"; break;
                case IndexSourceType.PrimeRateWSJEffectiveDate:
                    _v = "PrimeRateWSJEffectiveDate"; break;
                case IndexSourceType.PrimeRateWSJPublicationDate:
                    _v = "PrimeRateWSJPublicationDate"; break;
                case IndexSourceType.SixMonthLIBOR_WSJDaily:
                    _v = "SixMonthLIBOR_WSJDaily"; break;
                case IndexSourceType.SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth:
                    _v = "SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth"; break;
                case IndexSourceType.SixMonthUSTBillMonthlyAuctionDiscountRateCalculated:
                    _v = "SixMonthUSTBillMonthlyAuctionDiscountRateCalculated"; break;
                case IndexSourceType.SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated:
                    _v = "SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated"; break;
                case IndexSourceType.SixMonthUSTBillWeeklyAuctionDiscountRateUST:
                    _v = "SixMonthUSTBillWeeklyAuctionDiscountRateUST"; break;
                case IndexSourceType.SixMonthUSTBillWeeklyAuctionInvestmentYieldUST:
                    _v = "SixMonthUSTBillWeeklyAuctionInvestmentYieldUST"; break;
                case IndexSourceType.WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15:
                    _v = "WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15"; break;
                case IndexSourceType.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15:
                    _v = "WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15"; break;
                case IndexSourceType.WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15:
                    _v = "WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15"; break;
                case IndexSourceType.WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15:
                    _v = "WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15"; break;
                case IndexSourceType.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15:
                    _v = "WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected IndexSourceType ReadElementString_IndexSourceType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return IndexSourceType.Undefined;
            }
            switch (str)
            {
                case "FHLBEleventhDistrictMonthlyCostOfFundsIndex":
                    return IndexSourceType.FHLBEleventhDistrictMonthlyCostOfFundsIndex;
                case "LIBOROneMonthWSJ25thDayOfMonth":
                    return IndexSourceType.LIBOROneMonthWSJ25thDayOfMonth;
                case "LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay":
                    return IndexSourceType.LIBOROneMonthWSJ25thDayOfMonthOrNextBusinessDay;
                case "LIBOROneMonthWSJDaily":
                    return IndexSourceType.LIBOROneMonthWSJDaily;
                case "LIBOROneMonthWSJFifteenthDayOfMonth":
                    return IndexSourceType.LIBOROneMonthWSJFifteenthDayOfMonth;
                case "LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay":
                    return IndexSourceType.LIBOROneMonthWSJFifteenthDayOfMonthOrNextBusinessDay;
                case "LIBOROneYearWSJDaily":
                    return IndexSourceType.LIBOROneYearWSJDaily;
                case "LIBORSixMonthWSJ25thDayOfMonth":
                    return IndexSourceType.LIBORSixMonthWSJ25thDayOfMonth;
                case "LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay":
                    return IndexSourceType.LIBORSixMonthWSJ25thDayOfMonthOrNextBusinessDay;
                case "LIBORSixMonthWSJFifteenthDayOfMonth":
                    return IndexSourceType.LIBORSixMonthWSJFifteenthDayOfMonth;
                case "LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay":
                    return IndexSourceType.LIBORSixMonthWSJFifteenthDayOfMonthOrNextBusinessDay;
                case "LIBORSixMonthWSJLastBusinessDayOfMonth":
                    return IndexSourceType.LIBORSixMonthWSJLastBusinessDayOfMonth;
                case "MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15":
                    return IndexSourceType.MonthlyFiveYearTreasurySecuritiesConstantMaturityFRBH15;
                case "MonthlyNationalAverageContractRateForThePurchaseOfPreviouslyOccupiedHomesByCombinedLen":
                    return IndexSourceType.MonthlyNationalAverageContractRateForThePurchaseOfPreviouslyOccupiedHomesByCombinedLen;
                case "MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15":
                    return IndexSourceType.MonthlyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                case "MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15":
                    return IndexSourceType.MonthlyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                case "NationalMonthlyMedianCostOfFundsIndexOTS":
                    return IndexSourceType.NationalMonthlyMedianCostOfFundsIndexOTS;
                case "Other":
                    return IndexSourceType.Other;
                case "PrimeRateWSJEffectiveDate":
                    return IndexSourceType.PrimeRateWSJEffectiveDate;
                case "PrimeRateWSJPublicationDate":
                    return IndexSourceType.PrimeRateWSJPublicationDate;
                case "SixMonthLIBOR_WSJDaily":
                    return IndexSourceType.SixMonthLIBOR_WSJDaily;
                case "SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth":
                    return IndexSourceType.SixMonthLIBOR_WSJFirstBusinessDayOfTheMonth;
                case "SixMonthUSTBillMonthlyAuctionDiscountRateCalculated":
                    return IndexSourceType.SixMonthUSTBillMonthlyAuctionDiscountRateCalculated;
                case "SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated":
                    return IndexSourceType.SixMonthUSTBillMonthlyAuctionInvestmentYieldCalculated;
                case "SixMonthUSTBillWeeklyAuctionDiscountRateUST":
                    return IndexSourceType.SixMonthUSTBillWeeklyAuctionDiscountRateUST;
                case "SixMonthUSTBillWeeklyAuctionInvestmentYieldUST":
                    return IndexSourceType.SixMonthUSTBillWeeklyAuctionInvestmentYieldUST;
                case "WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15":
                    return IndexSourceType.WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15;
                case "WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15":
                    return IndexSourceType.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                case "WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15":
                    return IndexSourceType.WeeklySixMonthCertificateOfDepositSecondaryMarketFRBH15;
                case "WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15":
                    return IndexSourceType.WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15;
                case "WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15":
                    return IndexSourceType.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
