// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum InvestorReportingAdditionalChargeAssessedToPartyType
    {
        Undefined = 0,
        Borrower,
        Investor,
        Other,
        Servicer,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, InvestorReportingAdditionalChargeAssessedToPartyType value)
        {
            if (value == InvestorReportingAdditionalChargeAssessedToPartyType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case InvestorReportingAdditionalChargeAssessedToPartyType.Borrower:
                    _v = "Borrower"; break;
                case InvestorReportingAdditionalChargeAssessedToPartyType.Investor:
                    _v = "Investor"; break;
                case InvestorReportingAdditionalChargeAssessedToPartyType.Other:
                    _v = "Other"; break;
                case InvestorReportingAdditionalChargeAssessedToPartyType.Servicer:
                    _v = "Servicer"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected InvestorReportingAdditionalChargeAssessedToPartyType ReadElementString_InvestorReportingAdditionalChargeAssessedToPartyType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return InvestorReportingAdditionalChargeAssessedToPartyType.Undefined;
            }
            switch (str)
            {
                case "Borrower":
                    return InvestorReportingAdditionalChargeAssessedToPartyType.Borrower;
                case "Investor":
                    return InvestorReportingAdditionalChargeAssessedToPartyType.Investor;
                case "Other":
                    return InvestorReportingAdditionalChargeAssessedToPartyType.Other;
                case "Servicer":
                    return InvestorReportingAdditionalChargeAssessedToPartyType.Servicer;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
