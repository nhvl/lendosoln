﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class HmdaRaceDesignations : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "HMDA_RACE_DESIGNATIONS"; } }

        #region Property List
        public List<HmdaRaceDesignation> hmdaRaceDesignationList;
        /// <summary>
        /// This is the value that will show up in the GOVERNMENT_MONITORING_DETAIL/EXTENSION/OTHER/GOVERNMENT_MONITORING_DETAIL_EXTENSION/HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator element.
        /// </summary>
        public List<HmdaRaceDesignation> HMDARaceDesignationList
        {
            get
            {
                if (hmdaRaceDesignationList == null)
                {
                    hmdaRaceDesignationList = new List<HmdaRaceDesignation>();
                }
                return hmdaRaceDesignationList;
            }
        }
        #endregion

        #region Read/Write XML

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "HMDA_RACE_DESIGNATION":
                    ReadElement(reader, HMDARaceDesignationList);
                    break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, hmdaRaceDesignationList);
        }
        #endregion
    }
}