// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class ServicePaymentDetail : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "SERVICE_PAYMENT_DETAIL"; } }

        #region Property List
        public string InternalAccountIdentifier { get; set; }
        public string ServicePaymentAccountIdentifier { get; set; }
        public string ServicePaymentAmount { get; set; }
        public string ServicePaymentCreditAccountExpirationDate { get; set; }
        public ServicePaymentMethodType ServicePaymentCreditMethodType { get; set; }
        public string ServicePaymentCreditMethodTypeOtherDescription { get; set; }
        public PaymentMethodType ServicePaymentMethodType { get; set; }
        public string ServicePaymentMethodTypeOtherDescription { get; set; }
        public string ServicePaymentOnAccountIdentifier { get; set; }
        public string ServicePaymentOnAccountMaximumDebitAmount { get; set; }
        public string ServicePaymentOnAccountMinimumBalanceAmount { get; set; }
        public string ServicePaymentReferenceIdentifier { get; set; }
        public string ServicePaymentSecondaryCreditAccountIdentifier { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "InternalAccountIdentifier":
                    InternalAccountIdentifier = ReadElementString(reader); break;
                case "ServicePaymentAccountIdentifier":
                    ServicePaymentAccountIdentifier = ReadElementString(reader); break;
                case "ServicePaymentAmount":
                    ServicePaymentAmount = ReadElementString(reader); break;
                case "ServicePaymentCreditAccountExpirationDate":
                    ServicePaymentCreditAccountExpirationDate = ReadElementString(reader); break;
                case "ServicePaymentCreditMethodType":
                    ServicePaymentCreditMethodType = ReadElementString_ServicePaymentMethodType(reader); break;
                case "ServicePaymentCreditMethodTypeOtherDescription":
                    ServicePaymentCreditMethodTypeOtherDescription = ReadElementString(reader); break;
                case "ServicePaymentMethodType":
                    ServicePaymentMethodType = ReadElementString_PaymentMethodType(reader); break;
                case "ServicePaymentMethodTypeOtherDescription":
                    ServicePaymentMethodTypeOtherDescription = ReadElementString(reader); break;
                case "ServicePaymentOnAccountIdentifier":
                    ServicePaymentOnAccountIdentifier = ReadElementString(reader); break;
                case "ServicePaymentOnAccountMaximumDebitAmount":
                    ServicePaymentOnAccountMaximumDebitAmount = ReadElementString(reader); break;
                case "ServicePaymentOnAccountMinimumBalanceAmount":
                    ServicePaymentOnAccountMinimumBalanceAmount = ReadElementString(reader); break;
                case "ServicePaymentReferenceIdentifier":
                    ServicePaymentReferenceIdentifier = ReadElementString(reader); break;
                case "ServicePaymentSecondaryCreditAccountIdentifier":
                    ServicePaymentSecondaryCreditAccountIdentifier = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "InternalAccountIdentifier", InternalAccountIdentifier);
            WriteElementString(writer, "ServicePaymentAccountIdentifier", ServicePaymentAccountIdentifier);
            WriteElementString(writer, "ServicePaymentAmount", ServicePaymentAmount);
            WriteElementString(writer, "ServicePaymentCreditAccountExpirationDate", ServicePaymentCreditAccountExpirationDate);
            WriteElementString(writer, "ServicePaymentCreditMethodType", ServicePaymentCreditMethodType);
            WriteElementString(writer, "ServicePaymentCreditMethodTypeOtherDescription", ServicePaymentCreditMethodTypeOtherDescription);
            WriteElementString(writer, "ServicePaymentMethodType", ServicePaymentMethodType);
            WriteElementString(writer, "ServicePaymentMethodTypeOtherDescription", ServicePaymentMethodTypeOtherDescription);
            WriteElementString(writer, "ServicePaymentOnAccountIdentifier", ServicePaymentOnAccountIdentifier);
            WriteElementString(writer, "ServicePaymentOnAccountMaximumDebitAmount", ServicePaymentOnAccountMaximumDebitAmount);
            WriteElementString(writer, "ServicePaymentOnAccountMinimumBalanceAmount", ServicePaymentOnAccountMinimumBalanceAmount);
            WriteElementString(writer, "ServicePaymentReferenceIdentifier", ServicePaymentReferenceIdentifier);
            WriteElementString(writer, "ServicePaymentSecondaryCreditAccountIdentifier", ServicePaymentSecondaryCreditAccountIdentifier);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
