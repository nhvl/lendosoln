// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class AutomatedUnderwriting : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "AUTOMATED_UNDERWRITING"; } }

        #region Property List
        public string AutomatedUnderwritingCaseIdentifier { get; set; }
        public string AutomatedUnderwritingDecisionDatetime { get; set; }
        public string AutomatedUnderwritingEvaluationStatusDescription { get; set; }
        public string AutomatedUnderwritingMethodVersionIdentifier { get; set; }
        public string AutomatedUnderwritingProcessDescription { get; set; }
        public string AutomatedUnderwritingRecommendationDescription { get; set; }
        public bool? AutomatedUnderwritingSystemDocumentWaiverIndicator { get; set; }
        public string AutomatedUnderwritingSystemResultValue { get; set; }
        public AutomatedUnderwritingSystemType AutomatedUnderwritingSystemType { get; set; }
        public string AutomatedUnderwritingSystemTypeOtherDescription { get; set; }
        public DesktopUnderwriterRecommendationType DesktopUnderwriterRecommendationType { get; set; }
        public FREPurchaseEligibilityType FREPurchaseEligibilityType { get; set; }
        public string LoanProspectorClassificationDescription { get; set; }
        public string LoanProspectorDocumentationClassificationDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AutomatedUnderwritingCaseIdentifier":
                    AutomatedUnderwritingCaseIdentifier = ReadElementString(reader); break;
                case "AutomatedUnderwritingDecisionDatetime":
                    AutomatedUnderwritingDecisionDatetime = ReadElementString(reader); break;
                case "AutomatedUnderwritingEvaluationStatusDescription":
                    AutomatedUnderwritingEvaluationStatusDescription = ReadElementString(reader); break;
                case "AutomatedUnderwritingMethodVersionIdentifier":
                    AutomatedUnderwritingMethodVersionIdentifier = ReadElementString(reader); break;
                case "AutomatedUnderwritingProcessDescription":
                    AutomatedUnderwritingProcessDescription = ReadElementString(reader); break;
                case "AutomatedUnderwritingRecommendationDescription":
                    AutomatedUnderwritingRecommendationDescription = ReadElementString(reader); break;
                case "AutomatedUnderwritingSystemDocumentWaiverIndicator":
                    AutomatedUnderwritingSystemDocumentWaiverIndicator = ReadElementString_Bool(reader); break;
                case "AutomatedUnderwritingSystemResultValue":
                    AutomatedUnderwritingSystemResultValue = ReadElementString(reader); break;
                case "AutomatedUnderwritingSystemType":
                    AutomatedUnderwritingSystemType = ReadElementString_AutomatedUnderwritingSystemType(reader); break;
                case "AutomatedUnderwritingSystemTypeOtherDescription":
                    AutomatedUnderwritingSystemTypeOtherDescription = ReadElementString(reader); break;
                case "DesktopUnderwriterRecommendationType":
                    DesktopUnderwriterRecommendationType = ReadElementString_DesktopUnderwriterRecommendationType(reader); break;
                case "FREPurchaseEligibilityType":
                    FREPurchaseEligibilityType = ReadElementString_FREPurchaseEligibilityType(reader); break;
                case "LoanProspectorClassificationDescription":
                    LoanProspectorClassificationDescription = ReadElementString(reader); break;
                case "LoanProspectorDocumentationClassificationDescription":
                    LoanProspectorDocumentationClassificationDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "AutomatedUnderwritingCaseIdentifier", AutomatedUnderwritingCaseIdentifier);
            WriteElementString(writer, "AutomatedUnderwritingDecisionDatetime", AutomatedUnderwritingDecisionDatetime);
            WriteElementString(writer, "AutomatedUnderwritingEvaluationStatusDescription", AutomatedUnderwritingEvaluationStatusDescription);
            WriteElementString(writer, "AutomatedUnderwritingMethodVersionIdentifier", AutomatedUnderwritingMethodVersionIdentifier);
            WriteElementString(writer, "AutomatedUnderwritingProcessDescription", AutomatedUnderwritingProcessDescription);
            WriteElementString(writer, "AutomatedUnderwritingRecommendationDescription", AutomatedUnderwritingRecommendationDescription);
            WriteElementString(writer, "AutomatedUnderwritingSystemDocumentWaiverIndicator", AutomatedUnderwritingSystemDocumentWaiverIndicator);
            WriteElementString(writer, "AutomatedUnderwritingSystemResultValue", AutomatedUnderwritingSystemResultValue);
            WriteElementString(writer, "AutomatedUnderwritingSystemType", AutomatedUnderwritingSystemType);
            WriteElementString(writer, "AutomatedUnderwritingSystemTypeOtherDescription", AutomatedUnderwritingSystemTypeOtherDescription);
            WriteElementString(writer, "DesktopUnderwriterRecommendationType", DesktopUnderwriterRecommendationType);
            WriteElementString(writer, "FREPurchaseEligibilityType", FREPurchaseEligibilityType);
            WriteElementString(writer, "LoanProspectorClassificationDescription", LoanProspectorClassificationDescription);
            WriteElementString(writer, "LoanProspectorDocumentationClassificationDescription", LoanProspectorDocumentationClassificationDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
