// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum InterestCalculationBasisDaysInPeriodType
    {
        Undefined = 0,
        _30Days,
        DaysBetweenPayments,
        DaysInCalendarMonth,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, InterestCalculationBasisDaysInPeriodType value)
        {
            if (value == InterestCalculationBasisDaysInPeriodType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case InterestCalculationBasisDaysInPeriodType._30Days:
                    _v = "30Days"; break;
                case InterestCalculationBasisDaysInPeriodType.DaysBetweenPayments:
                    _v = "DaysBetweenPayments"; break;
                case InterestCalculationBasisDaysInPeriodType.DaysInCalendarMonth:
                    _v = "DaysInCalendarMonth"; break;
                case InterestCalculationBasisDaysInPeriodType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected InterestCalculationBasisDaysInPeriodType ReadElementString_InterestCalculationBasisDaysInPeriodType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return InterestCalculationBasisDaysInPeriodType.Undefined;
            }
            switch (str)
            {
                case "30Days":
                    return InterestCalculationBasisDaysInPeriodType._30Days;
                case "DaysBetweenPayments":
                    return InterestCalculationBasisDaysInPeriodType.DaysBetweenPayments;
                case "DaysInCalendarMonth":
                    return InterestCalculationBasisDaysInPeriodType.DaysInCalendarMonth;
                case "Other":
                    return InterestCalculationBasisDaysInPeriodType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
