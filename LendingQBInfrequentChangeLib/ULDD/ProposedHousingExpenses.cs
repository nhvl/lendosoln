// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class ProposedHousingExpenses : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PROPOSED_HOUSING_EXPENSES"; } }

        #region Property List
        private List<ProposedHousingExpense> m_proposedHousingExpenseList;
        public List<ProposedHousingExpense> ProposedHousingExpenseList
        {
            get
            {
                if (m_proposedHousingExpenseList == null)
                {
                    m_proposedHousingExpenseList = new List<ProposedHousingExpense>();
                }
                return m_proposedHousingExpenseList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "PROPOSED_HOUSING_EXPENSE":
                    ReadElement(reader, ProposedHousingExpenseList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_proposedHousingExpenseList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
