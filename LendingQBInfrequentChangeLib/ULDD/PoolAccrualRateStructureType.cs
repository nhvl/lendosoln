// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum PoolAccrualRateStructureType
    {
        Undefined = 0,
        StatedStructure,
        WeightedAverageStructure,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, PoolAccrualRateStructureType value)
        {
            if (value == PoolAccrualRateStructureType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case PoolAccrualRateStructureType.StatedStructure:
                    _v = "StatedStructure"; break;
                case PoolAccrualRateStructureType.WeightedAverageStructure:
                    _v = "WeightedAverageStructure"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected PoolAccrualRateStructureType ReadElementString_PoolAccrualRateStructureType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return PoolAccrualRateStructureType.Undefined;
            }
            switch (str)
            {
                case "StatedStructure":
                    return PoolAccrualRateStructureType.StatedStructure;
                case "WeightedAverageStructure":
                    return PoolAccrualRateStructureType.WeightedAverageStructure;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
