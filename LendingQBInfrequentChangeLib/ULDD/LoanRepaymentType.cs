// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum LoanRepaymentType
    {
        Undefined = 0,
        ConstantPrincipal,
        Other,
        PrincipalPaymentOption,
        ScheduledAmortization,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, LoanRepaymentType value)
        {
            if (value == LoanRepaymentType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case LoanRepaymentType.ConstantPrincipal:
                    _v = "ConstantPrincipal"; break;
                case LoanRepaymentType.Other:
                    _v = "Other"; break;
                case LoanRepaymentType.PrincipalPaymentOption:
                    _v = "PrincipalPaymentOption"; break;
                case LoanRepaymentType.ScheduledAmortization:
                    _v = "ScheduledAmortization"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected LoanRepaymentType ReadElementString_LoanRepaymentType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return LoanRepaymentType.Undefined;
            }
            switch (str)
            {
                case "ConstantPrincipal":
                    return LoanRepaymentType.ConstantPrincipal;
                case "Other":
                    return LoanRepaymentType.Other;
                case "PrincipalPaymentOption":
                    return LoanRepaymentType.PrincipalPaymentOption;
                case "ScheduledAmortization":
                    return LoanRepaymentType.ScheduledAmortization;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
