// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class LocationIdentifier : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "LOCATION_IDENTIFIER"; } }

        #region Property List
        private CensusInformation m_censusInformation;
        public CensusInformation CensusInformation
        {
            get
            {
                if (m_censusInformation == null)
                {
                    m_censusInformation = new CensusInformation();
                }
                return m_censusInformation;
            }
            set { m_censusInformation = value; }
        }
        private FipsInformation m_fipsInformation;
        public FipsInformation FipsInformation
        {
            get
            {
                if (m_fipsInformation == null)
                {
                    m_fipsInformation = new FipsInformation();
                }
                return m_fipsInformation;
            }
            set { m_fipsInformation = value; }
        }
        private GeneralIdentifier m_generalIdentifier;
        public GeneralIdentifier GeneralIdentifier
        {
            get
            {
                if (m_generalIdentifier == null)
                {
                    m_generalIdentifier = new GeneralIdentifier();
                }
                return m_generalIdentifier;
            }
            set { m_generalIdentifier = value; }
        }
        private GeospatialInformation m_geospatialInformation;
        public GeospatialInformation GeospatialInformation
        {
            get
            {
                if (m_geospatialInformation == null)
                {
                    m_geospatialInformation = new GeospatialInformation();
                }
                return m_geospatialInformation;
            }
            set { m_geospatialInformation = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CENSUS_INFORMATION":
                    ReadElement(reader, CensusInformation); break;
                case "FIPS_INFORMATION":
                    ReadElement(reader, FipsInformation); break;
                case "GENERAL_IDENTIFIER":
                    ReadElement(reader, GeneralIdentifier); break;
                case "GEOSPATIAL_INFORMATION":
                    ReadElement(reader, GeospatialInformation); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_censusInformation);
            WriteElement(writer, m_fipsInformation);
            WriteElement(writer, m_generalIdentifier);
            WriteElement(writer, m_geospatialInformation);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
