// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class DataItemChangeContexts : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "DATA_ITEM_CHANGE_CONTEXTS"; } }

        #region Property List
        private List<DataItemChangeContext> m_dataItemChangeContextList;
        public List<DataItemChangeContext> DataItemChangeContextList
        {
            get
            {
                if (m_dataItemChangeContextList == null)
                {
                    m_dataItemChangeContextList = new List<DataItemChangeContext>();
                }
                return m_dataItemChangeContextList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DATA_ITEM_CHANGE_CONTEXT":
                    ReadElement(reader, DataItemChangeContextList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_dataItemChangeContextList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
