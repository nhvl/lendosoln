// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum ConversionOptionPeriodFeeBalanceCalculationType
    {
        Undefined = 0,
        CurrentBalanceAtConversion,
        OriginalBalance,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, ConversionOptionPeriodFeeBalanceCalculationType value)
        {
            if (value == ConversionOptionPeriodFeeBalanceCalculationType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case ConversionOptionPeriodFeeBalanceCalculationType.CurrentBalanceAtConversion:
                    _v = "CurrentBalanceAtConversion"; break;
                case ConversionOptionPeriodFeeBalanceCalculationType.OriginalBalance:
                    _v = "OriginalBalance"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected ConversionOptionPeriodFeeBalanceCalculationType ReadElementString_ConversionOptionPeriodFeeBalanceCalculationType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return ConversionOptionPeriodFeeBalanceCalculationType.Undefined;
            }
            switch (str)
            {
                case "CurrentBalanceAtConversion":
                    return ConversionOptionPeriodFeeBalanceCalculationType.CurrentBalanceAtConversion;
                case "OriginalBalance":
                    return ConversionOptionPeriodFeeBalanceCalculationType.OriginalBalance;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
