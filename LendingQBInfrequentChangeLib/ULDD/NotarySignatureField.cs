// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class NotarySignatureField : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "NOTARY_SIGNATURE_FIELD"; } }

        #region Property List
        private FieldReference m_notarialCertificateLanguageFieldReference;
        public FieldReference NotarialCertificateLanguageFieldReference
        {
            get
            {
                if (m_notarialCertificateLanguageFieldReference == null)
                {
                    m_notarialCertificateLanguageFieldReference = new FieldReference();
                }
                return m_notarialCertificateLanguageFieldReference;
            }
            set { m_notarialCertificateLanguageFieldReference = value; }
        }
        private FieldReference m_notarialSealFieldReference;
        public FieldReference NotarialSealFieldReference
        {
            get
            {
                if (m_notarialSealFieldReference == null)
                {
                    m_notarialSealFieldReference = new FieldReference();
                }
                return m_notarialSealFieldReference;
            }
            set { m_notarialSealFieldReference = value; }
        }
        private NotarySignatureFieldDetail m_notarySignatureFieldDetail;
        public NotarySignatureFieldDetail NotarySignatureFieldDetail
        {
            get
            {
                if (m_notarySignatureFieldDetail == null)
                {
                    m_notarySignatureFieldDetail = new NotarySignatureFieldDetail();
                }
                return m_notarySignatureFieldDetail;
            }
            set { m_notarySignatureFieldDetail = value; }
        }
        private FieldReference m_signatureFieldReference;
        public FieldReference SignatureFieldReference
        {
            get
            {
                if (m_signatureFieldReference == null)
                {
                    m_signatureFieldReference = new FieldReference();
                }
                return m_signatureFieldReference;
            }
            set { m_signatureFieldReference = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "NOTARIAL_CERTIFICATE_LANGUAGE_FIELD_REFERENCE":
                    ReadElement(reader, NotarialCertificateLanguageFieldReference); break;
                case "NOTARIAL_SEAL_FIELD_REFERENCE":
                    ReadElement(reader, NotarialSealFieldReference); break;
                case "NOTARY_SIGNATURE_FIELD_DETAIL":
                    ReadElement(reader, NotarySignatureFieldDetail); break;
                case "SIGNATURE_FIELD_REFERENCE":
                    ReadElement(reader, SignatureFieldReference); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_notarialCertificateLanguageFieldReference);
            WriteElement(writer, m_notarialSealFieldReference);
            WriteElement(writer, m_notarySignatureFieldDetail);
            WriteElement(writer, m_signatureFieldReference);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
