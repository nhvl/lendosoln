// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class DataItemChange : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "DATA_ITEM_CHANGE"; } }

        #region Property List
        public string DataItemChangeEffectiveDate { get; set; }
        public string DataItemChangeExpirationDate { get; set; }
        public string DataItemChangePriorValue { get; set; }
        public DataItemChangeType DataItemChangeType { get; set; }
        public string DataItemChangeValue { get; set; }
        public string DataItemChangeXPath { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DataItemChangeEffectiveDate":
                    DataItemChangeEffectiveDate = ReadElementString(reader); break;
                case "DataItemChangeExpirationDate":
                    DataItemChangeExpirationDate = ReadElementString(reader); break;
                case "DataItemChangePriorValue":
                    DataItemChangePriorValue = ReadElementString(reader); break;
                case "DataItemChangeType":
                    DataItemChangeType = ReadElementString_DataItemChangeType(reader); break;
                case "DataItemChangeValue":
                    DataItemChangeValue = ReadElementString(reader); break;
                case "DataItemChangeXPath":
                    DataItemChangeXPath = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "DataItemChangeEffectiveDate", DataItemChangeEffectiveDate);
            WriteElementString(writer, "DataItemChangeExpirationDate", DataItemChangeExpirationDate);
            WriteElementString(writer, "DataItemChangePriorValue", DataItemChangePriorValue);
            WriteElementString(writer, "DataItemChangeType", DataItemChangeType);
            WriteElementString(writer, "DataItemChangeValue", DataItemChangeValue);
            WriteElementString(writer, "DataItemChangeXPath", DataItemChangeXPath);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
