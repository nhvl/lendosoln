// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class TaxpayerIdentifier : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "TAXPAYER_IDENTIFIER"; } }

        #region Property List
        public TaxpayerIdentifierType TaxpayerIdentifierType { get; set; }
        public string TaxpayerIdentifierValue { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "TaxpayerIdentifierType":
                    TaxpayerIdentifierType = ReadElementString_TaxpayerIdentifierType(reader); break;
                case "TaxpayerIdentifierValue":
                    TaxpayerIdentifierValue = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "TaxpayerIdentifierType", TaxpayerIdentifierType);
            WriteElementString(writer, "TaxpayerIdentifierValue", TaxpayerIdentifierValue);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
