// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class EscrowItems : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "ESCROW_ITEMS"; } }

        #region Property List
        private List<EscrowItem> m_escrowItemList;
        public List<EscrowItem> EscrowItemList
        {
            get
            {
                if (m_escrowItemList == null)
                {
                    m_escrowItemList = new List<EscrowItem>();
                }
                return m_escrowItemList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ESCROW_ITEM":
                    ReadElement(reader, EscrowItemList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_escrowItemList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
