// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum SummaryAmountType
    {
        Undefined = 0,
        SubtotalLiabilitiesForRentalPropertyBalance,
        SubtotalLiabilitiesForRentalPropertyMonthlyPayment,
        SubtotalLiabilitiesMonthlyPayment,
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance,
        SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment,
        SubtotalLiquidAssetsNotIncludingGift,
        SubtotalNonLiquidAssets,
        SubtotalOmittedLiabilitiesBalance,
        SubtotalOmittedLiabilitiesMonthlyPayment,
        SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty,
        SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty,
        SubtotalSubjectPropertyLiensPaidByClosingBalance,
        SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment,
        TotalLiabilitiesBalance,
        TotalMonthlyIncomeNotIncludingNetRentalIncome,
        TotalPresentHousingExpense,
        UndrawnHELOC,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, SummaryAmountType value)
        {
            if (value == SummaryAmountType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case SummaryAmountType.SubtotalLiabilitiesForRentalPropertyBalance:
                    _v = "SubtotalLiabilitiesForRentalPropertyBalance"; break;
                case SummaryAmountType.SubtotalLiabilitiesForRentalPropertyMonthlyPayment:
                    _v = "SubtotalLiabilitiesForRentalPropertyMonthlyPayment"; break;
                case SummaryAmountType.SubtotalLiabilitiesMonthlyPayment:
                    _v = "SubtotalLiabilitiesMonthlyPayment"; break;
                case SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance:
                    _v = "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance"; break;
                case SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment:
                    _v = "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment"; break;
                case SummaryAmountType.SubtotalLiquidAssetsNotIncludingGift:
                    _v = "SubtotalLiquidAssetsNotIncludingGift"; break;
                case SummaryAmountType.SubtotalNonLiquidAssets:
                    _v = "SubtotalNonLiquidAssets"; break;
                case SummaryAmountType.SubtotalOmittedLiabilitiesBalance:
                    _v = "SubtotalOmittedLiabilitiesBalance"; break;
                case SummaryAmountType.SubtotalOmittedLiabilitiesMonthlyPayment:
                    _v = "SubtotalOmittedLiabilitiesMonthlyPayment"; break;
                case SummaryAmountType.SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty:
                    _v = "SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty"; break;
                case SummaryAmountType.SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty:
                    _v = "SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty"; break;
                case SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingBalance:
                    _v = "SubtotalSubjectPropertyLiensPaidByClosingBalance"; break;
                case SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment:
                    _v = "SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment"; break;
                case SummaryAmountType.TotalLiabilitiesBalance:
                    _v = "TotalLiabilitiesBalance"; break;
                case SummaryAmountType.TotalMonthlyIncomeNotIncludingNetRentalIncome:
                    _v = "TotalMonthlyIncomeNotIncludingNetRentalIncome"; break;
                case SummaryAmountType.TotalPresentHousingExpense:
                    _v = "TotalPresentHousingExpense"; break;
                case SummaryAmountType.UndrawnHELOC:
                    _v = "UndrawnHELOC"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected SummaryAmountType ReadElementString_SummaryAmountType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return SummaryAmountType.Undefined;
            }
            switch (str)
            {
                case "SubtotalLiabilitiesForRentalPropertyBalance":
                    return SummaryAmountType.SubtotalLiabilitiesForRentalPropertyBalance;
                case "SubtotalLiabilitiesForRentalPropertyMonthlyPayment":
                    return SummaryAmountType.SubtotalLiabilitiesForRentalPropertyMonthlyPayment;
                case "SubtotalLiabilitiesMonthlyPayment":
                    return SummaryAmountType.SubtotalLiabilitiesMonthlyPayment;
                case "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance":
                    return SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance;
                case "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment":
                    return SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment;
                case "SubtotalLiquidAssetsNotIncludingGift":
                    return SummaryAmountType.SubtotalLiquidAssetsNotIncludingGift;
                case "SubtotalNonLiquidAssets":
                    return SummaryAmountType.SubtotalNonLiquidAssets;
                case "SubtotalOmittedLiabilitiesBalance":
                    return SummaryAmountType.SubtotalOmittedLiabilitiesBalance;
                case "SubtotalOmittedLiabilitiesMonthlyPayment":
                    return SummaryAmountType.SubtotalOmittedLiabilitiesMonthlyPayment;
                case "SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty":
                    return SummaryAmountType.SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty;
                case "SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty":
                    return SummaryAmountType.SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty;
                case "SubtotalSubjectPropertyLiensPaidByClosingBalance":
                    return SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingBalance;
                case "SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment":
                    return SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment;
                case "TotalLiabilitiesBalance":
                    return SummaryAmountType.TotalLiabilitiesBalance;
                case "TotalMonthlyIncomeNotIncludingNetRentalIncome":
                    return SummaryAmountType.TotalMonthlyIncomeNotIncludingNetRentalIncome;
                case "TotalPresentHousingExpense":
                    return SummaryAmountType.TotalPresentHousingExpense;
                case "UndrawnHELOC":
                    return SummaryAmountType.UndrawnHELOC;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
