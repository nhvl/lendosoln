// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class AssetDocumentation : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "ASSET_DOCUMENTATION"; } }

        #region Property List
        public AssetDocumentationType AssetDocumentationType { get; set; }
        public string AssetDocumentationTypeOtherDescription { get; set; }
        public string AssetVerificationRangeCount { get; set; }
        public VerificationRangeType AssetVerificationRangeType { get; set; }
        public string AssetVerificationRangeTypeOtherDescription { get; set; }
        public DocumentationStateType DocumentationStateType { get; set; }
        public string DocumentationStateTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AssetDocumentationType":
                    AssetDocumentationType = ReadElementString_AssetDocumentationType(reader); break;
                case "AssetDocumentationTypeOtherDescription":
                    AssetDocumentationTypeOtherDescription = ReadElementString(reader); break;
                case "AssetVerificationRangeCount":
                    AssetVerificationRangeCount = ReadElementString(reader); break;
                case "AssetVerificationRangeType":
                    AssetVerificationRangeType = ReadElementString_VerificationRangeType(reader); break;
                case "AssetVerificationRangeTypeOtherDescription":
                    AssetVerificationRangeTypeOtherDescription = ReadElementString(reader); break;
                case "DocumentationStateType":
                    DocumentationStateType = ReadElementString_DocumentationStateType(reader); break;
                case "DocumentationStateTypeOtherDescription":
                    DocumentationStateTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "AssetDocumentationType", AssetDocumentationType);
            WriteElementString(writer, "AssetDocumentationTypeOtherDescription", AssetDocumentationTypeOtherDescription);
            WriteElementString(writer, "AssetVerificationRangeCount", AssetVerificationRangeCount);
            WriteElementString(writer, "AssetVerificationRangeType", AssetVerificationRangeType);
            WriteElementString(writer, "AssetVerificationRangeTypeOtherDescription", AssetVerificationRangeTypeOtherDescription);
            WriteElementString(writer, "DocumentationStateType", DocumentationStateType);
            WriteElementString(writer, "DocumentationStateTypeOtherDescription", DocumentationStateTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
