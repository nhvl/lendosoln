// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class SalesContract : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "SALES_CONTRACT"; } }

        #region Property List
        public string SalesConcessionDescription { get; set; }
        public bool? SalesConcessionIndicator { get; set; }
        public string SalesConditionsOfSaleDescription { get; set; }
        public string SalesContractAmount { get; set; }
        public string SalesContractDate { get; set; }
        public string SalesContractInvoiceRevewCommentDescription { get; set; }
        public bool? SalesContractInvoiceReviewedIndicator { get; set; }
        public string SalesContractRetailerName { get; set; }
        public string SalesContractReviewCommentDescription { get; set; }
        public bool? SalesContractReviewedIndicator { get; set; }
        public bool? SalesContractTermsAndConditionsConsistentWithTheLocalMarketIndicator { get; set; }
        public bool? SellerIsOwnerIndicator { get; set; }
        public string SubjectDataSourceDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "SalesConcessionDescription":
                    SalesConcessionDescription = ReadElementString(reader); break;
                case "SalesConcessionIndicator":
                    SalesConcessionIndicator = ReadElementString_Bool(reader); break;
                case "SalesConditionsOfSaleDescription":
                    SalesConditionsOfSaleDescription = ReadElementString(reader); break;
                case "SalesContractAmount":
                    SalesContractAmount = ReadElementString(reader); break;
                case "SalesContractDate":
                    SalesContractDate = ReadElementString(reader); break;
                case "SalesContractInvoiceRevewCommentDescription":
                    SalesContractInvoiceRevewCommentDescription = ReadElementString(reader); break;
                case "SalesContractInvoiceReviewedIndicator":
                    SalesContractInvoiceReviewedIndicator = ReadElementString_Bool(reader); break;
                case "SalesContractRetailerName":
                    SalesContractRetailerName = ReadElementString(reader); break;
                case "SalesContractReviewCommentDescription":
                    SalesContractReviewCommentDescription = ReadElementString(reader); break;
                case "SalesContractReviewedIndicator":
                    SalesContractReviewedIndicator = ReadElementString_Bool(reader); break;
                case "SalesContractTermsAndConditionsConsistentWithTheLocalMarketIndicator":
                    SalesContractTermsAndConditionsConsistentWithTheLocalMarketIndicator = ReadElementString_Bool(reader); break;
                case "SellerIsOwnerIndicator":
                    SellerIsOwnerIndicator = ReadElementString_Bool(reader); break;
                case "SubjectDataSourceDescription":
                    SubjectDataSourceDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "SalesConcessionDescription", SalesConcessionDescription);
            WriteElementString(writer, "SalesConcessionIndicator", SalesConcessionIndicator);
            WriteElementString(writer, "SalesConditionsOfSaleDescription", SalesConditionsOfSaleDescription);
            WriteElementString(writer, "SalesContractAmount", SalesContractAmount);
            WriteElementString(writer, "SalesContractDate", SalesContractDate);
            WriteElementString(writer, "SalesContractInvoiceRevewCommentDescription", SalesContractInvoiceRevewCommentDescription);
            WriteElementString(writer, "SalesContractInvoiceReviewedIndicator", SalesContractInvoiceReviewedIndicator);
            WriteElementString(writer, "SalesContractRetailerName", SalesContractRetailerName);
            WriteElementString(writer, "SalesContractReviewCommentDescription", SalesContractReviewCommentDescription);
            WriteElementString(writer, "SalesContractReviewedIndicator", SalesContractReviewedIndicator);
            WriteElementString(writer, "SalesContractTermsAndConditionsConsistentWithTheLocalMarketIndicator", SalesContractTermsAndConditionsConsistentWithTheLocalMarketIndicator);
            WriteElementString(writer, "SellerIsOwnerIndicator", SellerIsOwnerIndicator);
            WriteElementString(writer, "SubjectDataSourceDescription", SubjectDataSourceDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
