// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class ServicingComment : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "SERVICING_COMMENT"; } }

        #region Property List
        public string ServicingCommentDatetime { get; set; }
        public string ServicingCommentSourceDescription { get; set; }
        public string ServicingCommentText { get; set; }
        public ServicingCommentType ServicingCommentType { get; set; }
        public string ServicingCommentTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ServicingCommentDatetime":
                    ServicingCommentDatetime = ReadElementString(reader); break;
                case "ServicingCommentSourceDescription":
                    ServicingCommentSourceDescription = ReadElementString(reader); break;
                case "ServicingCommentText":
                    ServicingCommentText = ReadElementString(reader); break;
                case "ServicingCommentType":
                    ServicingCommentType = ReadElementString_ServicingCommentType(reader); break;
                case "ServicingCommentTypeOtherDescription":
                    ServicingCommentTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "ServicingCommentDatetime", ServicingCommentDatetime);
            WriteElementString(writer, "ServicingCommentSourceDescription", ServicingCommentSourceDescription);
            WriteElementString(writer, "ServicingCommentText", ServicingCommentText);
            WriteElementString(writer, "ServicingCommentType", ServicingCommentType);
            WriteElementString(writer, "ServicingCommentTypeOtherDescription", ServicingCommentTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
