// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class ProjectDetail : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PROJECT_DETAIL"; } }

        #region Property List
        public AdditionalProjectConsiderationsType AdditionalProjectConsiderationsType { get; set; }
        public string AdditionalProjectConsiderationsTypeOtherDescription { get; set; }
        public CondominiumProjectStatusType CondominiumProjectStatusType { get; set; }
        public string FNMCondominiumProjectManagerProjectIdentifier { get; set; }
        public bool? ProjectAmenityImprovementsCompletedIndicator { get; set; }
        public ProjectAttachmentType ProjectAttachmentType { get; set; }
        public string ProjectClassificationIdentifier { get; set; }
        public ProjectDesignType ProjectDesignType { get; set; }
        public string ProjectDesignTypeOtherDescription { get; set; }
        public string ProjectDwellingUnitCount { get; set; }
        public string ProjectDwellingUnitsSoldCount { get; set; }
        public ProjectLegalStructureType ProjectLegalStructureType { get; set; }
        public string ProjectName { get; set; }
        public bool? PUDIndicator { get; set; }
        public bool? TwoToFourUnitCondominiumProjectIndicator { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AdditionalProjectConsiderationsType":
                    AdditionalProjectConsiderationsType = ReadElementString_AdditionalProjectConsiderationsType(reader); break;
                case "AdditionalProjectConsiderationsTypeOtherDescription":
                    AdditionalProjectConsiderationsTypeOtherDescription = ReadElementString(reader); break;
                case "CondominiumProjectStatusType":
                    CondominiumProjectStatusType = ReadElementString_CondominiumProjectStatusType(reader); break;
                case "FNMCondominiumProjectManagerProjectIdentifier":
                    FNMCondominiumProjectManagerProjectIdentifier = ReadElementString(reader); break;
                case "ProjectAmenityImprovementsCompletedIndicator":
                    ProjectAmenityImprovementsCompletedIndicator = ReadElementString_Bool(reader); break;
                case "ProjectAttachmentType":
                    ProjectAttachmentType = ReadElementString_ProjectAttachmentType(reader); break;
                case "ProjectClassificationIdentifier":
                    ProjectClassificationIdentifier = ReadElementString(reader); break;
                case "ProjectDesignType":
                    ProjectDesignType = ReadElementString_ProjectDesignType(reader); break;
                case "ProjectDesignTypeOtherDescription":
                    ProjectDesignTypeOtherDescription = ReadElementString(reader); break;
                case "ProjectDwellingUnitCount":
                    ProjectDwellingUnitCount = ReadElementString(reader); break;
                case "ProjectDwellingUnitsSoldCount":
                    ProjectDwellingUnitsSoldCount = ReadElementString(reader); break;
                case "ProjectLegalStructureType":
                    ProjectLegalStructureType = ReadElementString_ProjectLegalStructureType(reader); break;
                case "ProjectName":
                    ProjectName = ReadElementString(reader); break;
                case "PUDIndicator":
                    PUDIndicator = ReadElementString_Bool(reader); break;
                case "TwoToFourUnitCondominiumProjectIndicator":
                    TwoToFourUnitCondominiumProjectIndicator = ReadElementString_Bool(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "AdditionalProjectConsiderationsType", AdditionalProjectConsiderationsType);
            WriteElementString(writer, "AdditionalProjectConsiderationsTypeOtherDescription", AdditionalProjectConsiderationsTypeOtherDescription);
            WriteElementString(writer, "CondominiumProjectStatusType", CondominiumProjectStatusType);
            WriteElementString(writer, "FNMCondominiumProjectManagerProjectIdentifier", FNMCondominiumProjectManagerProjectIdentifier);
            WriteElementString(writer, "ProjectAmenityImprovementsCompletedIndicator", ProjectAmenityImprovementsCompletedIndicator);
            WriteElementString(writer, "ProjectAttachmentType", ProjectAttachmentType);
            WriteElementString(writer, "ProjectClassificationIdentifier", ProjectClassificationIdentifier);
            WriteElementString(writer, "ProjectDesignType", ProjectDesignType);
            WriteElementString(writer, "ProjectDesignTypeOtherDescription", ProjectDesignTypeOtherDescription);
            WriteElementString(writer, "ProjectDwellingUnitCount", ProjectDwellingUnitCount);
            WriteElementString(writer, "ProjectDwellingUnitsSoldCount", ProjectDwellingUnitsSoldCount);
            WriteElementString(writer, "ProjectLegalStructureType", ProjectLegalStructureType);
            WriteElementString(writer, "ProjectName", ProjectName);
            WriteElementString(writer, "PUDIndicator", PUDIndicator);
            WriteElementString(writer, "TwoToFourUnitCondominiumProjectIndicator", TwoToFourUnitCondominiumProjectIndicator);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
