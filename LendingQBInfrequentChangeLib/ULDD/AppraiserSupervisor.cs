// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class AppraiserSupervisor : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "APPRAISER_SUPERVISOR"; } }

        #region Property List
        private AppraiserLicense m_appraiserLicense;
        public AppraiserLicense AppraiserLicense
        {
            get
            {
                if (m_appraiserLicense == null)
                {
                    m_appraiserLicense = new AppraiserLicense();
                }
                return m_appraiserLicense;
            }
            set { m_appraiserLicense = value; }
        }
        private AppraiserSupervisorDetail m_appraiserSupervisorDetail;
        public AppraiserSupervisorDetail AppraiserSupervisorDetail
        {
            get
            {
                if (m_appraiserSupervisorDetail == null)
                {
                    m_appraiserSupervisorDetail = new AppraiserSupervisorDetail();
                }
                return m_appraiserSupervisorDetail;
            }
            set { m_appraiserSupervisorDetail = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "APPRAISER_LICENSE":
                    ReadElement(reader, AppraiserLicense); break;
                case "APPRAISER_SUPERVISOR_DETAIL":
                    ReadElement(reader, AppraiserSupervisorDetail); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_appraiserLicense);
            WriteElement(writer, m_appraiserSupervisorDetail);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
