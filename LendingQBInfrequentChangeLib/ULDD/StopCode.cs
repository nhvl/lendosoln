// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class StopCode : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "STOP_CODE"; } }

        #region Property List
        public StopCodeActionType StopCodeActionType { get; set; }
        public string StopCodeActionTypeOtherDescription { get; set; }
        public string StopCodeExpirationDate { get; set; }
        public StopCodeConditionType StopCodeConditionType { get; set; }
        public string StopCodeConditionTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "StopCodeActionType":
                    StopCodeActionType = ReadElementString_StopCodeActionType(reader); break;
                case "StopCodeActionTypeOtherDescription":
                    StopCodeActionTypeOtherDescription = ReadElementString(reader); break;
                case "StopCodeExpirationDate":
                    StopCodeExpirationDate = ReadElementString(reader); break;
                case "StopCodeConditionType":
                    StopCodeConditionType = ReadElementString_StopCodeConditionType(reader); break;
                case "StopCodeConditionTypeOtherDescription":
                    StopCodeConditionTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "StopCodeActionType", StopCodeActionType);
            WriteElementString(writer, "StopCodeActionTypeOtherDescription", StopCodeActionTypeOtherDescription);
            WriteElementString(writer, "StopCodeExpirationDate", StopCodeExpirationDate);
            WriteElementString(writer, "StopCodeConditionType", StopCodeConditionType);
            WriteElementString(writer, "StopCodeConditionTypeOtherDescription", StopCodeConditionTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
