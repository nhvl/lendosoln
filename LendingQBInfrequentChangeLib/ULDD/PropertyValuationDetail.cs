// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class PropertyValuationDetail : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PROPERTY_VALUATION_DETAIL"; } }

        #region Property List
        public string AppraisalIdentifier { get; set; }
        public string PropertyEstimatedValueAmount { get; set; }
        public string PropertyReplacementValueAmount { get; set; }
        public string PropertyValuationAmount { get; set; }
        public PropertyValuationConclusionDependencyType PropertyValuationConclusionDependencyType { get; set; }
        public string PropertyValuationConclusionDependencyTypeOtherDescription { get; set; }
        public string PropertyValuationEffectiveDate { get; set; }
        public PropertyValuationFormType PropertyValuationFormType { get; set; }
        public string PropertyValuationFormTypeOtherDescription { get; set; }
        public string PropertyValuationFormVersionIdentifier { get; set; }
        public AppraisalInspectionType PropertyValuationInspectionType { get; set; }
        public string PropertyValuationInspectionTypeOtherDescription { get; set; }
        public PropertyValuationMethodType PropertyValuationMethodType { get; set; }
        public string PropertyValuationMethodTypeOtherDescription { get; set; }
        public PropertyValuationType PropertyValuationType { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AppraisalIdentifier":
                    AppraisalIdentifier = ReadElementString(reader); break;
                case "PropertyEstimatedValueAmount":
                    PropertyEstimatedValueAmount = ReadElementString(reader); break;
                case "PropertyReplacementValueAmount":
                    PropertyReplacementValueAmount = ReadElementString(reader); break;
                case "PropertyValuationAmount":
                    PropertyValuationAmount = ReadElementString(reader); break;
                case "PropertyValuationConclusionDependencyType":
                    PropertyValuationConclusionDependencyType = ReadElementString_PropertyValuationConclusionDependencyType(reader); break;
                case "PropertyValuationConclusionDependencyTypeOtherDescription":
                    PropertyValuationConclusionDependencyTypeOtherDescription = ReadElementString(reader); break;
                case "PropertyValuationEffectiveDate":
                    PropertyValuationEffectiveDate = ReadElementString(reader); break;
                case "PropertyValuationFormType":
                    PropertyValuationFormType = ReadElementString_PropertyValuationFormType(reader); break;
                case "PropertyValuationFormTypeOtherDescription":
                    PropertyValuationFormTypeOtherDescription = ReadElementString(reader); break;
                case "PropertyValuationFormVersionIdentifier":
                    PropertyValuationFormVersionIdentifier = ReadElementString(reader); break;
                case "PropertyValuationInspectionType":
                    PropertyValuationInspectionType = ReadElementString_AppraisalInspectionType(reader); break;
                case "PropertyValuationInspectionTypeOtherDescription":
                    PropertyValuationInspectionTypeOtherDescription = ReadElementString(reader); break;
                case "PropertyValuationMethodType":
                    PropertyValuationMethodType = ReadElementString_PropertyValuationMethodType(reader); break;
                case "PropertyValuationMethodTypeOtherDescription":
                    PropertyValuationMethodTypeOtherDescription = ReadElementString(reader); break;
                case "PropertyValuationType":
                    PropertyValuationType = ReadElementString_PropertyValuationType(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "AppraisalIdentifier", AppraisalIdentifier);
            WriteElementString(writer, "PropertyEstimatedValueAmount", PropertyEstimatedValueAmount);
            WriteElementString(writer, "PropertyReplacementValueAmount", PropertyReplacementValueAmount);
            WriteElementString(writer, "PropertyValuationAmount", PropertyValuationAmount);
            WriteElementString(writer, "PropertyValuationConclusionDependencyType", PropertyValuationConclusionDependencyType);
            WriteElementString(writer, "PropertyValuationConclusionDependencyTypeOtherDescription", PropertyValuationConclusionDependencyTypeOtherDescription);
            WriteElementString(writer, "PropertyValuationEffectiveDate", PropertyValuationEffectiveDate);
            WriteElementString(writer, "PropertyValuationFormType", PropertyValuationFormType);
            WriteElementString(writer, "PropertyValuationFormTypeOtherDescription", PropertyValuationFormTypeOtherDescription);
            WriteElementString(writer, "PropertyValuationFormVersionIdentifier", PropertyValuationFormVersionIdentifier);
            WriteElementString(writer, "PropertyValuationInspectionType", PropertyValuationInspectionType);
            WriteElementString(writer, "PropertyValuationInspectionTypeOtherDescription", PropertyValuationInspectionTypeOtherDescription);
            WriteElementString(writer, "PropertyValuationMethodType", PropertyValuationMethodType);
            WriteElementString(writer, "PropertyValuationMethodTypeOtherDescription", PropertyValuationMethodTypeOtherDescription);
            WriteElementString(writer, "PropertyValuationType", PropertyValuationType);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
