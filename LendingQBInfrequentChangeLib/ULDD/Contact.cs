// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Contact : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "CONTACT"; } }

        #region Property List
        private ContactDetail m_contactDetail;
        public ContactDetail ContactDetail
        {
            get
            {
                if (m_contactDetail == null)
                {
                    m_contactDetail = new ContactDetail();
                }
                return m_contactDetail;
            }
            set { m_contactDetail = value; }
        }
        private ContactPoints m_contactPoints;
        public ContactPoints ContactPoints
        {
            get
            {
                if (m_contactPoints == null)
                {
                    m_contactPoints = new ContactPoints();
                }
                return m_contactPoints;
            }
            set { m_contactPoints = value; }
        }
        private Name m_name;
        public Name Name
        {
            get
            {
                if (m_name == null)
                {
                    m_name = new Name();
                }
                return m_name;
            }
            set { m_name = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CONTACT_DETAIL":
                    ReadElement(reader, ContactDetail); break;
                case "CONTACT_POINTS":
                    ReadElement(reader, ContactPoints); break;
                case "NAME":
                    ReadElement(reader, Name); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElement(writer, m_contactDetail);
            WriteElement(writer, m_contactPoints);
            WriteElement(writer, m_name);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
