// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum HELOCTeaserRateType
    {
        Undefined = 0,
        Adjustable,
        Fixed,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, HELOCTeaserRateType value)
        {
            if (value == HELOCTeaserRateType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case HELOCTeaserRateType.Adjustable:
                    _v = "Adjustable"; break;
                case HELOCTeaserRateType.Fixed:
                    _v = "Fixed"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected HELOCTeaserRateType ReadElementString_HELOCTeaserRateType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return HELOCTeaserRateType.Undefined;
            }
            switch (str)
            {
                case "Adjustable":
                    return HELOCTeaserRateType.Adjustable;
                case "Fixed":
                    return HELOCTeaserRateType.Fixed;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
