// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class EmploymentDocumentation : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "EMPLOYMENT_DOCUMENTATION"; } }

        #region Property List
        public DocumentationStateType DocumentationStateType { get; set; }
        public string DocumentationStateTypeOtherDescription { get; set; }
        public EmploymentDocumentationType EmploymentDocumentationType { get; set; }
        public string EmploymentDocumentationTypeOtherDescription { get; set; }
        public string EmploymentVerificationRangeCount { get; set; }
        public VerificationRangeType EmploymentVerificationRangeType { get; set; }
        public string EmploymentVerificationRangeTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DocumentationStateType":
                    DocumentationStateType = ReadElementString_DocumentationStateType(reader); break;
                case "DocumentationStateTypeOtherDescription":
                    DocumentationStateTypeOtherDescription = ReadElementString(reader); break;
                case "EmploymentDocumentationType":
                    EmploymentDocumentationType = ReadElementString_EmploymentDocumentationType(reader); break;
                case "EmploymentDocumentationTypeOtherDescription":
                    EmploymentDocumentationTypeOtherDescription = ReadElementString(reader); break;
                case "EmploymentVerificationRangeCount":
                    EmploymentVerificationRangeCount = ReadElementString(reader); break;
                case "EmploymentVerificationRangeType":
                    EmploymentVerificationRangeType = ReadElementString_VerificationRangeType(reader); break;
                case "EmploymentVerificationRangeTypeOtherDescription":
                    EmploymentVerificationRangeTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "DocumentationStateType", DocumentationStateType);
            WriteElementString(writer, "DocumentationStateTypeOtherDescription", DocumentationStateTypeOtherDescription);
            WriteElementString(writer, "EmploymentDocumentationType", EmploymentDocumentationType);
            WriteElementString(writer, "EmploymentDocumentationTypeOtherDescription", EmploymentDocumentationTypeOtherDescription);
            WriteElementString(writer, "EmploymentVerificationRangeCount", EmploymentVerificationRangeCount);
            WriteElementString(writer, "EmploymentVerificationRangeType", EmploymentVerificationRangeType);
            WriteElementString(writer, "EmploymentVerificationRangeTypeOtherDescription", EmploymentVerificationRangeTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
