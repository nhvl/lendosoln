// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Notary : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "NOTARY"; } }

        #region Property List
        private NotaryCertificates m_notaryCertificates;
        public NotaryCertificates NotaryCertificates
        {
            get
            {
                if (m_notaryCertificates == null)
                {
                    m_notaryCertificates = new NotaryCertificates();
                }
                return m_notaryCertificates;
            }
            set { m_notaryCertificates = value; }
        }
        private NotaryCommission m_notaryCommission;
        public NotaryCommission NotaryCommission
        {
            get
            {
                if (m_notaryCommission == null)
                {
                    m_notaryCommission = new NotaryCommission();
                }
                return m_notaryCommission;
            }
            set { m_notaryCommission = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "NOTARY_CERTIFICATES":
                    ReadElement(reader, NotaryCertificates); break;
                case "NOTARY_COMMISSION":
                    ReadElement(reader, NotaryCommission); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_notaryCertificates);
            WriteElement(writer, m_notaryCommission);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
