// Generated By CodeMonkey 8/27/2012 12:58:44 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class UrlaDetail : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "URLA_DETAIL"; } }

        #region Property List
        public bool? AdditionalBorrowerAssetsConsideredIndicator { get; set; }
        public bool? AdditionalBorrowerAssetsNotConsideredIndicator { get; set; }
        public string AlterationsImprovementsAndRepairsAmount { get; set; }
        public string ARMTypeDescription { get; set; }
        public string BaseLoanAmount { get; set; }
        public string BorrowerPaidDiscountPointsTotalAmount { get; set; }
        public string BorrowerRequestedInterestRatePercent { get; set; }
        public string BorrowerRequestedLoanAmount { get; set; }
        public string EstimatedClosingCostsAmount { get; set; }
        public string LenderRegistrationIdentifier { get; set; }
        public string MIAndFundingFeeFinancedAmount { get; set; }
        public string MIAndFundingFeeTotalAmount { get; set; }
        public string PrepaidItemsEstimatedAmount { get; set; }
        public string PurchasePriceAmount { get; set; }
        public string PurchasePriceNetAmount { get; set; }
        public string RefinanceImprovementCostsAmount { get; set; }
        public RefinanceImprovementsType RefinanceImprovementsType { get; set; }
        public string RefinanceIncludingDebtsToBePaidOffAmount { get; set; }
        public string RefinanceProposedImprovementsDescription { get; set; }
        public bool? RefundableApplicationFeeIndicator { get; set; }
        public bool? RequiredDepositIndicator { get; set; }
        public string SellerPaidClosingCostsAmount { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AdditionalBorrowerAssetsConsideredIndicator":
                    AdditionalBorrowerAssetsConsideredIndicator = ReadElementString_Bool(reader); break;
                case "AdditionalBorrowerAssetsNotConsideredIndicator":
                    AdditionalBorrowerAssetsNotConsideredIndicator = ReadElementString_Bool(reader); break;
                case "AlterationsImprovementsAndRepairsAmount":
                    AlterationsImprovementsAndRepairsAmount = ReadElementString(reader); break;
                case "ARMTypeDescription":
                    ARMTypeDescription = ReadElementString(reader); break;
                case "BaseLoanAmount":
                    BaseLoanAmount = ReadElementString(reader); break;
                case "BorrowerPaidDiscountPointsTotalAmount":
                    BorrowerPaidDiscountPointsTotalAmount = ReadElementString(reader); break;
                case "BorrowerRequestedInterestRatePercent":
                    BorrowerRequestedInterestRatePercent = ReadElementString(reader); break;
                case "BorrowerRequestedLoanAmount":
                    BorrowerRequestedLoanAmount = ReadElementString(reader); break;
                case "EstimatedClosingCostsAmount":
                    EstimatedClosingCostsAmount = ReadElementString(reader); break;
                case "LenderRegistrationIdentifier":
                    LenderRegistrationIdentifier = ReadElementString(reader); break;
                case "MIAndFundingFeeFinancedAmount":
                    MIAndFundingFeeFinancedAmount = ReadElementString(reader); break;
                case "MIAndFundingFeeTotalAmount":
                    MIAndFundingFeeTotalAmount = ReadElementString(reader); break;
                case "PrepaidItemsEstimatedAmount":
                    PrepaidItemsEstimatedAmount = ReadElementString(reader); break;
                case "PurchasePriceAmount":
                    PurchasePriceAmount = ReadElementString(reader); break;
                case "PurchasePriceNetAmount":
                    PurchasePriceNetAmount = ReadElementString(reader); break;
                case "RefinanceImprovementCostsAmount":
                    RefinanceImprovementCostsAmount = ReadElementString(reader); break;
                case "RefinanceImprovementsType":
                    RefinanceImprovementsType = ReadElementString_RefinanceImprovementsType(reader); break;
                case "RefinanceIncludingDebtsToBePaidOffAmount":
                    RefinanceIncludingDebtsToBePaidOffAmount = ReadElementString(reader); break;
                case "RefinanceProposedImprovementsDescription":
                    RefinanceProposedImprovementsDescription = ReadElementString(reader); break;
                case "RefundableApplicationFeeIndicator":
                    RefundableApplicationFeeIndicator = ReadElementString_Bool(reader); break;
                case "RequiredDepositIndicator":
                    RequiredDepositIndicator = ReadElementString_Bool(reader); break;
                case "SellerPaidClosingCostsAmount":
                    SellerPaidClosingCostsAmount = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "AdditionalBorrowerAssetsConsideredIndicator", AdditionalBorrowerAssetsConsideredIndicator);
            WriteElementString(writer, "AdditionalBorrowerAssetsNotConsideredIndicator", AdditionalBorrowerAssetsNotConsideredIndicator);
            WriteElementString(writer, "AlterationsImprovementsAndRepairsAmount", AlterationsImprovementsAndRepairsAmount);
            WriteElementString(writer, "ARMTypeDescription", ARMTypeDescription);
            WriteElementString(writer, "BaseLoanAmount", BaseLoanAmount);
            WriteElementString(writer, "BorrowerPaidDiscountPointsTotalAmount", BorrowerPaidDiscountPointsTotalAmount);
            WriteElementString(writer, "BorrowerRequestedInterestRatePercent", BorrowerRequestedInterestRatePercent);
            WriteElementString(writer, "BorrowerRequestedLoanAmount", BorrowerRequestedLoanAmount);
            WriteElementString(writer, "EstimatedClosingCostsAmount", EstimatedClosingCostsAmount);
            WriteElementString(writer, "LenderRegistrationIdentifier", LenderRegistrationIdentifier);
            WriteElementString(writer, "MIAndFundingFeeFinancedAmount", MIAndFundingFeeFinancedAmount);
            WriteElementString(writer, "MIAndFundingFeeTotalAmount", MIAndFundingFeeTotalAmount);
            WriteElementString(writer, "PrepaidItemsEstimatedAmount", PrepaidItemsEstimatedAmount);
            WriteElementString(writer, "PurchasePriceAmount", PurchasePriceAmount);
            WriteElementString(writer, "PurchasePriceNetAmount", PurchasePriceNetAmount);
            WriteElementString(writer, "RefinanceImprovementCostsAmount", RefinanceImprovementCostsAmount);
            WriteElementString(writer, "RefinanceImprovementsType", RefinanceImprovementsType);
            WriteElementString(writer, "RefinanceIncludingDebtsToBePaidOffAmount", RefinanceIncludingDebtsToBePaidOffAmount);
            WriteElementString(writer, "RefinanceProposedImprovementsDescription", RefinanceProposedImprovementsDescription);
            WriteElementString(writer, "RefundableApplicationFeeIndicator", RefundableApplicationFeeIndicator);
            WriteElementString(writer, "RequiredDepositIndicator", RequiredDepositIndicator);
            WriteElementString(writer, "SellerPaidClosingCostsAmount", SellerPaidClosingCostsAmount);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
