// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class PriceLocks : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PRICE_LOCKS"; } }

        #region Property List
        private List<PriceLock> m_priceLockList;
        public List<PriceLock> PriceLockList
        {
            get
            {
                if (m_priceLockList == null)
                {
                    m_priceLockList = new List<PriceLock>();
                }
                return m_priceLockList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "PRICE_LOCK":
                    ReadElement(reader, PriceLockList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_priceLockList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
