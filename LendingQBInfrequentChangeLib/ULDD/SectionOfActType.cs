// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum SectionOfActType
    {
        Undefined = 0,
        _203B,
        _203B251,
        _203B2,
        _203K,
        _203K251,
        _221D2,
        _221D2251,
        _234C,
        _234C251,
        _184,
        _201S,
        _201SD,
        _201U,
        _201UD,
        _203B241,
        _203H,
        _203I,
        _203K241,
        _213,
        _220,
        _221,
        _222,
        _223E,
        _233,
        _235,
        _237,
        _240,
        _245,
        _247,
        _248,
        _251,
        _255,
        _256,
        _257,
        _26101,
        _26102,
        _26201,
        _26202,
        _27001,
        _27002,
        _27003,
        _27004,
        _27005,
        _27101,
        _27102,
        _27103,
        _27104,
        _27105,
        _3703,
        _3703D,
        _3703D2,
        _3710,
        _3711,
        _502,
        _729,
        _8,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, SectionOfActType value)
        {
            if (value == SectionOfActType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case SectionOfActType._203B:
                    _v = "203B"; break;
                case SectionOfActType._203B251:
                    _v = "203B251"; break;
                case SectionOfActType._203B2:
                    _v = "203B2"; break;
                case SectionOfActType._203K:
                    _v = "203K"; break;
                case SectionOfActType._203K251:
                    _v = "203K251"; break;
                case SectionOfActType._221D2:
                    _v = "221D2"; break;
                case SectionOfActType._221D2251:
                    _v = "221D2251"; break;
                case SectionOfActType._234C:
                    _v = "234C"; break;
                case SectionOfActType._234C251:
                    _v = "234C251"; break;
                case SectionOfActType._184:
                    _v = "184"; break;
                case SectionOfActType._201S:
                    _v = "201S"; break;
                case SectionOfActType._201SD:
                    _v = "201SD"; break;
                case SectionOfActType._201U:
                    _v = "201U"; break;
                case SectionOfActType._201UD:
                    _v = "201UD"; break;
                case SectionOfActType._203B241:
                    _v = "203B241"; break;
                case SectionOfActType._203H:
                    _v = "203H"; break;
                case SectionOfActType._203I:
                    _v = "203I"; break;
                case SectionOfActType._203K241:
                    _v = "203K241"; break;
                case SectionOfActType._213:
                    _v = "213"; break;
                case SectionOfActType._220:
                    _v = "220"; break;
                case SectionOfActType._221:
                    _v = "221"; break;
                case SectionOfActType._222:
                    _v = "222"; break;
                case SectionOfActType._223E:
                    _v = "223E"; break;
                case SectionOfActType._233:
                    _v = "233"; break;
                case SectionOfActType._235:
                    _v = "235"; break;
                case SectionOfActType._237:
                    _v = "237"; break;
                case SectionOfActType._240:
                    _v = "240"; break;
                case SectionOfActType._245:
                    _v = "245"; break;
                case SectionOfActType._247:
                    _v = "247"; break;
                case SectionOfActType._248:
                    _v = "248"; break;
                case SectionOfActType._251:
                    _v = "251"; break;
                case SectionOfActType._255:
                    _v = "255"; break;
                case SectionOfActType._256:
                    _v = "256"; break;
                case SectionOfActType._257:
                    _v = "257"; break;
                case SectionOfActType._26101:
                    _v = "26101"; break;
                case SectionOfActType._26102:
                    _v = "26102"; break;
                case SectionOfActType._26201:
                    _v = "26201"; break;
                case SectionOfActType._26202:
                    _v = "26202"; break;
                case SectionOfActType._27001:
                    _v = "27001"; break;
                case SectionOfActType._27002:
                    _v = "27002"; break;
                case SectionOfActType._27003:
                    _v = "27003"; break;
                case SectionOfActType._27004:
                    _v = "27004"; break;
                case SectionOfActType._27005:
                    _v = "27005"; break;
                case SectionOfActType._27101:
                    _v = "27101"; break;
                case SectionOfActType._27102:
                    _v = "27102"; break;
                case SectionOfActType._27103:
                    _v = "27103"; break;
                case SectionOfActType._27104:
                    _v = "27104"; break;
                case SectionOfActType._27105:
                    _v = "27105"; break;
                case SectionOfActType._3703:
                    _v = "3703"; break;
                case SectionOfActType._3703D:
                    _v = "3703D"; break;
                case SectionOfActType._3703D2:
                    _v = "3703D2"; break;
                case SectionOfActType._3710:
                    _v = "3710"; break;
                case SectionOfActType._3711:
                    _v = "3711"; break;
                case SectionOfActType._502:
                    _v = "502"; break;
                case SectionOfActType._729:
                    _v = "729"; break;
                case SectionOfActType._8:
                    _v = "8"; break;
                case SectionOfActType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected SectionOfActType ReadElementString_SectionOfActType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return SectionOfActType.Undefined;
            }
            switch (str)
            {
                case "203B":
                    return SectionOfActType._203B;
                case "203B251":
                    return SectionOfActType._203B251;
                case "203B2":
                    return SectionOfActType._203B2;
                case "203K":
                    return SectionOfActType._203K;
                case "203K251":
                    return SectionOfActType._203K251;
                case "221D2":
                    return SectionOfActType._221D2;
                case "221D2251":
                    return SectionOfActType._221D2251;
                case "234C":
                    return SectionOfActType._234C;
                case "234C251":
                    return SectionOfActType._234C251;
                case "184":
                    return SectionOfActType._184;
                case "201S":
                    return SectionOfActType._201S;
                case "201SD":
                    return SectionOfActType._201SD;
                case "201U":
                    return SectionOfActType._201U;
                case "201UD":
                    return SectionOfActType._201UD;
                case "203B241":
                    return SectionOfActType._203B241;
                case "203H":
                    return SectionOfActType._203H;
                case "203I":
                    return SectionOfActType._203I;
                case "203K241":
                    return SectionOfActType._203K241;
                case "213":
                    return SectionOfActType._213;
                case "220":
                    return SectionOfActType._220;
                case "221":
                    return SectionOfActType._221;
                case "222":
                    return SectionOfActType._222;
                case "223E":
                    return SectionOfActType._223E;
                case "233":
                    return SectionOfActType._233;
                case "235":
                    return SectionOfActType._235;
                case "237":
                    return SectionOfActType._237;
                case "240":
                    return SectionOfActType._240;
                case "245":
                    return SectionOfActType._245;
                case "247":
                    return SectionOfActType._247;
                case "248":
                    return SectionOfActType._248;
                case "251":
                    return SectionOfActType._251;
                case "255":
                    return SectionOfActType._255;
                case "256":
                    return SectionOfActType._256;
                case "257":
                    return SectionOfActType._257;
                case "26101":
                    return SectionOfActType._26101;
                case "26102":
                    return SectionOfActType._26102;
                case "26201":
                    return SectionOfActType._26201;
                case "26202":
                    return SectionOfActType._26202;
                case "27001":
                    return SectionOfActType._27001;
                case "27002":
                    return SectionOfActType._27002;
                case "27003":
                    return SectionOfActType._27003;
                case "27004":
                    return SectionOfActType._27004;
                case "27005":
                    return SectionOfActType._27005;
                case "27101":
                    return SectionOfActType._27101;
                case "27102":
                    return SectionOfActType._27102;
                case "27103":
                    return SectionOfActType._27103;
                case "27104":
                    return SectionOfActType._27104;
                case "27105":
                    return SectionOfActType._27105;
                case "3703":
                    return SectionOfActType._3703;
                case "3703D":
                    return SectionOfActType._3703D;
                case "3703D2":
                    return SectionOfActType._3703D2;
                case "3710":
                    return SectionOfActType._3710;
                case "3711":
                    return SectionOfActType._3711;
                case "502":
                    return SectionOfActType._502;
                case "729":
                    return SectionOfActType._729;
                case "8":
                    return SectionOfActType._8;
                case "Other":
                    return SectionOfActType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
