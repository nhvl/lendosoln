// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum PrepaymentPenaltyWaiverType
    {
        Undefined = 0,
        HardshipSale,
        None,
        Other,
        ThirdPartySale,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, PrepaymentPenaltyWaiverType value)
        {
            if (value == PrepaymentPenaltyWaiverType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case PrepaymentPenaltyWaiverType.HardshipSale:
                    _v = "HardshipSale"; break;
                case PrepaymentPenaltyWaiverType.None:
                    _v = "None"; break;
                case PrepaymentPenaltyWaiverType.Other:
                    _v = "Other"; break;
                case PrepaymentPenaltyWaiverType.ThirdPartySale:
                    _v = "ThirdPartySale"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected PrepaymentPenaltyWaiverType ReadElementString_PrepaymentPenaltyWaiverType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return PrepaymentPenaltyWaiverType.Undefined;
            }
            switch (str)
            {
                case "HardshipSale":
                    return PrepaymentPenaltyWaiverType.HardshipSale;
                case "None":
                    return PrepaymentPenaltyWaiverType.None;
                case "Other":
                    return PrepaymentPenaltyWaiverType.Other;
                case "ThirdPartySale":
                    return PrepaymentPenaltyWaiverType.ThirdPartySale;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
