// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum AliasType
    {
        Undefined = 0,
        FormerlyKnownAs,
        NowKnownAs,
        AlsoKnownAs,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, AliasType value)
        {
            if (value == AliasType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case AliasType.FormerlyKnownAs:
                    _v = "FormerlyKnownAs"; break;
                case AliasType.NowKnownAs:
                    _v = "NowKnownAs"; break;
                case AliasType.AlsoKnownAs:
                    _v = "AlsoKnownAs"; break;
                case AliasType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected AliasType ReadElementString_AliasType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return AliasType.Undefined;
            }
            switch (str)
            {
                case "FormerlyKnownAs":
                    return AliasType.FormerlyKnownAs;
                case "NowKnownAs":
                    return AliasType.NowKnownAs;
                case "AlsoKnownAs":
                    return AliasType.AlsoKnownAs;
                case "Other":
                    return AliasType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
