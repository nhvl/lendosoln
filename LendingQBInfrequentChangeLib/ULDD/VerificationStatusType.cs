// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum VerificationStatusType
    {
        Undefined = 0,
        NotVerified,
        ToBeVerified,
        Verified,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, VerificationStatusType value)
        {
            if (value == VerificationStatusType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case VerificationStatusType.NotVerified:
                    _v = "NotVerified"; break;
                case VerificationStatusType.ToBeVerified:
                    _v = "ToBeVerified"; break;
                case VerificationStatusType.Verified:
                    _v = "Verified"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected VerificationStatusType ReadElementString_VerificationStatusType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return VerificationStatusType.Undefined;
            }
            switch (str)
            {
                case "NotVerified":
                    return VerificationStatusType.NotVerified;
                case "ToBeVerified":
                    return VerificationStatusType.ToBeVerified;
                case "Verified":
                    return VerificationStatusType.Verified;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
