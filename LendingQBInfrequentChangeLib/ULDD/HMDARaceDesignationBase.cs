﻿namespace ULDD
{
    public enum HMDARaceDesignationBase
    {
        Blank = 0,
        NativeHawaiian,
        GuamanianOrChamorro,
        Samoan,
        AsianIndian,
        Chinese,
        Filipino,
        Japanese,
        Korean,
        Vietnamese,
        OtherAsian,
        OtherPacificIslander,
    }
}
