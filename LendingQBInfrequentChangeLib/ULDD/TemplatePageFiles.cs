// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class TemplatePageFiles : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "TEMPLATE_PAGE_FILES"; } }

        #region Property List
        private List<TemplatePageFile> m_templatePageFileList;
        public List<TemplatePageFile> TemplatePageFileList
        {
            get
            {
                if (m_templatePageFileList == null)
                {
                    m_templatePageFileList = new List<TemplatePageFile>();
                }
                return m_templatePageFileList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "TEMPLATE_PAGE_FILE":
                    ReadElement(reader, TemplatePageFileList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_templatePageFileList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
