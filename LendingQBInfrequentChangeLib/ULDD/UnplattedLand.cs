// Generated By CodeMonkey 8/27/2012 12:58:44 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class UnplattedLand : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "UNPLATTED_LAND"; } }

        #region Property List
        public string AbstractIdentifier { get; set; }
        public string BaseIdentifier { get; set; }
        public string LegalTractIdentifier { get; set; }
        public string MeridianIdentifier { get; set; }
        public string MetesAndBoundsRemainingDescription { get; set; }
        public string QuarterSectionIdentifier { get; set; }
        public string RangeIdentifier { get; set; }
        public string SectionIdentifier { get; set; }
        public string TownshipIdentifier { get; set; }
        public UnplattedLandType UnplattedLandType { get; set; }
        public string UnplattedLandTypeOtherDescription { get; set; }
        public string UnplattedLandTypeIdentifier { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AbstractIdentifier":
                    AbstractIdentifier = ReadElementString(reader); break;
                case "BaseIdentifier":
                    BaseIdentifier = ReadElementString(reader); break;
                case "LegalTractIdentifier":
                    LegalTractIdentifier = ReadElementString(reader); break;
                case "MeridianIdentifier":
                    MeridianIdentifier = ReadElementString(reader); break;
                case "MetesAndBoundsRemainingDescription":
                    MetesAndBoundsRemainingDescription = ReadElementString(reader); break;
                case "QuarterSectionIdentifier":
                    QuarterSectionIdentifier = ReadElementString(reader); break;
                case "RangeIdentifier":
                    RangeIdentifier = ReadElementString(reader); break;
                case "SectionIdentifier":
                    SectionIdentifier = ReadElementString(reader); break;
                case "TownshipIdentifier":
                    TownshipIdentifier = ReadElementString(reader); break;
                case "UnplattedLandType":
                    UnplattedLandType = ReadElementString_UnplattedLandType(reader); break;
                case "UnplattedLandTypeOtherDescription":
                    UnplattedLandTypeOtherDescription = ReadElementString(reader); break;
                case "UnplattedLandTypeIdentifier":
                    UnplattedLandTypeIdentifier = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "AbstractIdentifier", AbstractIdentifier);
            WriteElementString(writer, "BaseIdentifier", BaseIdentifier);
            WriteElementString(writer, "LegalTractIdentifier", LegalTractIdentifier);
            WriteElementString(writer, "MeridianIdentifier", MeridianIdentifier);
            WriteElementString(writer, "MetesAndBoundsRemainingDescription", MetesAndBoundsRemainingDescription);
            WriteElementString(writer, "QuarterSectionIdentifier", QuarterSectionIdentifier);
            WriteElementString(writer, "RangeIdentifier", RangeIdentifier);
            WriteElementString(writer, "SectionIdentifier", SectionIdentifier);
            WriteElementString(writer, "TownshipIdentifier", TownshipIdentifier);
            WriteElementString(writer, "UnplattedLandType", UnplattedLandType);
            WriteElementString(writer, "UnplattedLandTypeOtherDescription", UnplattedLandTypeOtherDescription);
            WriteElementString(writer, "UnplattedLandTypeIdentifier", UnplattedLandTypeIdentifier);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
