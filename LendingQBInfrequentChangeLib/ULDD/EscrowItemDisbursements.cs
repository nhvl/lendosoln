// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class EscrowItemDisbursements : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "ESCROW_ITEM_DISBURSEMENTS"; } }

        #region Property List
        private List<EscrowItemDisbursement> m_escrowItemDisbursementList;
        public List<EscrowItemDisbursement> EscrowItemDisbursementList
        {
            get
            {
                if (m_escrowItemDisbursementList == null)
                {
                    m_escrowItemDisbursementList = new List<EscrowItemDisbursement>();
                }
                return m_escrowItemDisbursementList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ESCROW_ITEM_DISBURSEMENT":
                    ReadElement(reader, EscrowItemDisbursementList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_escrowItemDisbursementList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
