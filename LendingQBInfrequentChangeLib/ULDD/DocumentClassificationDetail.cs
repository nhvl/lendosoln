// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class DocumentClassificationDetail : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "DOCUMENT_CLASSIFICATION_DETAIL"; } }

        #region Property List
        public AcceptableSigningMethodType AcceptableSigningMethodType { get; set; }
        public string DocumentCommentDescription { get; set; }
        public string DocumentFormIssuingEntityNumberIdentifier { get; set; }
        public string DocumentFormIssuingEntityVersionIdentifier { get; set; }
        public DocumentFormIssuingEntityNameType DocumentFormIssuingEntityNameType { get; set; }
        public string DocumentFormIssuingEntityNameTypeOtherDescription { get; set; }
        public string DocumentFormPublisherEntityName { get; set; }
        public string DocumentFormPublisherNumberIdentifier { get; set; }
        public string DocumentFormPublisherVersionIdentifier { get; set; }
        public bool? DocumentHasBeenRecordedIndicator { get; set; }
        public string DocumentName { get; set; }
        public bool? DocumentNegotiableInstrumentIndicator { get; set; }
        public string DocumentPeriodEndDate { get; set; }
        public string DocumentPeriodStartDate { get; set; }
        public string DocumentReceiptDatetime { get; set; }
        public DocumentRecordationProcessingType DocumentRecordationProcessingType { get; set; }
        public string PageCount { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AcceptableSigningMethodType":
                    AcceptableSigningMethodType = ReadElementString_AcceptableSigningMethodType(reader); break;
                case "DocumentCommentDescription":
                    DocumentCommentDescription = ReadElementString(reader); break;
                case "DocumentFormIssuingEntityNumberIdentifier":
                    DocumentFormIssuingEntityNumberIdentifier = ReadElementString(reader); break;
                case "DocumentFormIssuingEntityVersionIdentifier":
                    DocumentFormIssuingEntityVersionIdentifier = ReadElementString(reader); break;
                case "DocumentFormIssuingEntityNameType":
                    DocumentFormIssuingEntityNameType = ReadElementString_DocumentFormIssuingEntityNameType(reader); break;
                case "DocumentFormIssuingEntityNameTypeOtherDescription":
                    DocumentFormIssuingEntityNameTypeOtherDescription = ReadElementString(reader); break;
                case "DocumentFormPublisherEntityName":
                    DocumentFormPublisherEntityName = ReadElementString(reader); break;
                case "DocumentFormPublisherNumberIdentifier":
                    DocumentFormPublisherNumberIdentifier = ReadElementString(reader); break;
                case "DocumentFormPublisherVersionIdentifier":
                    DocumentFormPublisherVersionIdentifier = ReadElementString(reader); break;
                case "DocumentHasBeenRecordedIndicator":
                    DocumentHasBeenRecordedIndicator = ReadElementString_Bool(reader); break;
                case "DocumentName":
                    DocumentName = ReadElementString(reader); break;
                case "DocumentNegotiableInstrumentIndicator":
                    DocumentNegotiableInstrumentIndicator = ReadElementString_Bool(reader); break;
                case "DocumentPeriodEndDate":
                    DocumentPeriodEndDate = ReadElementString(reader); break;
                case "DocumentPeriodStartDate":
                    DocumentPeriodStartDate = ReadElementString(reader); break;
                case "DocumentReceiptDatetime":
                    DocumentReceiptDatetime = ReadElementString(reader); break;
                case "DocumentRecordationProcessingType":
                    DocumentRecordationProcessingType = ReadElementString_DocumentRecordationProcessingType(reader); break;
                case "PageCount":
                    PageCount = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "AcceptableSigningMethodType", AcceptableSigningMethodType);
            WriteElementString(writer, "DocumentCommentDescription", DocumentCommentDescription);
            WriteElementString(writer, "DocumentFormIssuingEntityNumberIdentifier", DocumentFormIssuingEntityNumberIdentifier);
            WriteElementString(writer, "DocumentFormIssuingEntityVersionIdentifier", DocumentFormIssuingEntityVersionIdentifier);
            WriteElementString(writer, "DocumentFormIssuingEntityNameType", DocumentFormIssuingEntityNameType);
            WriteElementString(writer, "DocumentFormIssuingEntityNameTypeOtherDescription", DocumentFormIssuingEntityNameTypeOtherDescription);
            WriteElementString(writer, "DocumentFormPublisherEntityName", DocumentFormPublisherEntityName);
            WriteElementString(writer, "DocumentFormPublisherNumberIdentifier", DocumentFormPublisherNumberIdentifier);
            WriteElementString(writer, "DocumentFormPublisherVersionIdentifier", DocumentFormPublisherVersionIdentifier);
            WriteElementString(writer, "DocumentHasBeenRecordedIndicator", DocumentHasBeenRecordedIndicator);
            WriteElementString(writer, "DocumentName", DocumentName);
            WriteElementString(writer, "DocumentNegotiableInstrumentIndicator", DocumentNegotiableInstrumentIndicator);
            WriteElementString(writer, "DocumentPeriodEndDate", DocumentPeriodEndDate);
            WriteElementString(writer, "DocumentPeriodStartDate", DocumentPeriodStartDate);
            WriteElementString(writer, "DocumentReceiptDatetime", DocumentReceiptDatetime);
            WriteElementString(writer, "DocumentRecordationProcessingType", DocumentRecordationProcessingType);
            WriteElementString(writer, "PageCount", PageCount);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
