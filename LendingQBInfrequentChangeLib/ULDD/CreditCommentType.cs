// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum CreditCommentType
    {
        Undefined = 0,
        BureauRemarks,
        ConsumerStatement,
        Instruction,
        Other,
        PublicRecordText,
        StatusCode,
        Warning,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, CreditCommentType value)
        {
            if (value == CreditCommentType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case CreditCommentType.BureauRemarks:
                    _v = "BureauRemarks"; break;
                case CreditCommentType.ConsumerStatement:
                    _v = "ConsumerStatement"; break;
                case CreditCommentType.Instruction:
                    _v = "Instruction"; break;
                case CreditCommentType.Other:
                    _v = "Other"; break;
                case CreditCommentType.PublicRecordText:
                    _v = "PublicRecordText"; break;
                case CreditCommentType.StatusCode:
                    _v = "StatusCode"; break;
                case CreditCommentType.Warning:
                    _v = "Warning"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected CreditCommentType ReadElementString_CreditCommentType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return CreditCommentType.Undefined;
            }
            switch (str)
            {
                case "BureauRemarks":
                    return CreditCommentType.BureauRemarks;
                case "ConsumerStatement":
                    return CreditCommentType.ConsumerStatement;
                case "Instruction":
                    return CreditCommentType.Instruction;
                case "Other":
                    return CreditCommentType.Other;
                case "PublicRecordText":
                    return CreditCommentType.PublicRecordText;
                case "StatusCode":
                    return CreditCommentType.StatusCode;
                case "Warning":
                    return CreditCommentType.Warning;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
