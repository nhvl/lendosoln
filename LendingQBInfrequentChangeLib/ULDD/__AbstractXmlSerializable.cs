﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace ULDD
{
    public abstract partial class AbstractXmlSerializable : IXmlSerializable
    {
        public string Prefix
        {
            get;
            set;
        }
        public string Namespace
        {
            get;
            set;
        }
        public abstract string XmlElementName { get; }

        protected string ReadElementString(XmlReader reader)
        {
            string str = reader.ReadString();
            if (str == null)
            {
                str = string.Empty;
            }
            return str;
        }
        protected bool? ReadElementString_Bool(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }
            else if (str == "true")
            {
                return true;
            }
            else if (str == "false")
            {
                return false;
            }
            else
            {
                return null;
            }
        }

        protected TEnum? ReadElementString_Enum<TEnum>(XmlReader reader, bool ignoreCase = true) where TEnum : struct
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            TEnum result;
            return Enum.TryParse(str, ignoreCase, out result) && Enum.IsDefined(typeof(TEnum), result) ? result : (TEnum?)null;
        }

        protected string ReadAttribute(XmlReader reader, string attrName)
        {
            string value = reader.GetAttribute(attrName);
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value;
        }


        protected void WriteAttribute(XmlWriter writer, string attrName, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteAttributeString(attrName, value);
            }
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, bool? value)
        {
            if (value.HasValue)
            {
                string str = value.Value ? "true" : "false";
                writer.WriteAttributeString(attrName, str);
            }
        }
        protected void WriteElementString(XmlWriter writer, string name, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteElementString(name, value);
            }
        }
        protected void WriteElementString(XmlWriter writer, string name, bool? value)
        {
            if (value.HasValue)
            {
                writer.WriteElementString(name, value.Value ? "true" : "false");
            }
        }

        /// <summary>
        /// Writes an enum using the default ToString() for the enum.
        /// Treats the 0 (default) value as blank and skips writing.
        /// </summary>
        /// <typeparam name="TEnum">The enum type to write.</typeparam>
        /// <param name="writer">The xml writer to write to.</param>
        /// <param name="name">The name of the element to write</param>
        /// <param name="value">The enum value to write.</param>
        protected void WriteElementEnumString<TEnum>(XmlWriter writer, string name, TEnum? value) where TEnum : struct
        {
            if (value == null || (value as int?) == 0 || !typeof(TEnum).IsEnum || !Enum.IsDefined(typeof(TEnum), value))
            {
                return;
            }
            else
            {
                WriteElementString(writer, name, (value.Value as Enum).ToString("G"));
            }
        }

        protected void WriteString(XmlWriter writer, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteString(value);
            }
        }
        protected void WriteElement(XmlWriter writer, IXmlSerializable el)
        {
            if (null != el)
            {
                el.WriteXml(writer);
            }
        }
        protected void WriteElement<T>(XmlWriter writer, List<T> list) where T : IXmlSerializable
        {
            if (null == list)
                return;

            foreach (IXmlSerializable o in list)
            {
                WriteElement(writer, o);
            }
        }

        protected void ReadElement(XmlReader reader, IXmlSerializable el)
        {
            if (null == el)
            {
                return;
            }
            el.ReadXml(reader);
        }
        protected void ReadElement<T>(XmlReader reader, List<T> list) where T : IXmlSerializable, new()
        {
            if (null == list)
                return;

            T _o = new T();
            _o.ReadXml(reader);
            list.Add(_o);
        }
        protected virtual void ReadAttributes(XmlReader reader)
        {
        }

        protected virtual void ReadElement(XmlReader reader)
        {
        }
        protected virtual void ReadTextContent(XmlReader reader)
        {
        }
        protected virtual void WriteXmlImpl(XmlWriter writer)
        {
        }
        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.LocalName != XmlElementName)
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }
            ReadAttributes(reader);

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    ReadTextContent(reader);
                    continue;
                }
                if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == XmlElementName)
                {
                    return;
                }
                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                ReadElement(reader);
            }
        }


        public void WriteXml(System.Xml.XmlWriter writer)
        {
            if (string.IsNullOrEmpty(Prefix) == false && string.IsNullOrEmpty(Namespace) == false)
            {
                writer.WriteStartElement(Prefix, XmlElementName, Namespace);
            }
            else
            {
                writer.WriteStartElement(XmlElementName);
            }

            WriteXmlImpl(writer);
            writer.WriteEndElement();
        }


        #endregion
    }
}
