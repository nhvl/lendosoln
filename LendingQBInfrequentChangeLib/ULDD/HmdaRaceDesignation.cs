﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class HmdaRaceDesignation : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "HMDA_RACE_DESIGNATION"; } }

        #region Property List
        public HMDARaceDesignationBase? HMDARaceDesignationType { get; set; }

        public string HMDARaceDesignationOtherAsianDescription { get; set; }

        public string HMDARaceDesignationOtherPacificIslanderDescription { get; set; }
        #endregion

        #region Read/Write XML

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "HMDARaceDesignationType":
                    this.HMDARaceDesignationType = ReadElementString_Enum<HMDARaceDesignationBase>(reader);
                    break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementEnumString(writer, "HMDARaceDesignationType", this.HMDARaceDesignationType);
            WriteElementString(writer, "HMDARaceDesignationOtherAsianDescription", this.HMDARaceDesignationOtherAsianDescription);
            WriteElementString(writer, "HMDARaceDesignationOtherPacificIslanderDescription", this.HMDARaceDesignationOtherPacificIslanderDescription);
        }
        #endregion
    }
}