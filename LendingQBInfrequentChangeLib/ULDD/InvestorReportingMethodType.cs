// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum InvestorReportingMethodType
    {
        Undefined = 0,
        ExceptionReporting,
        FullReporting,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, InvestorReportingMethodType value)
        {
            if (value == InvestorReportingMethodType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case InvestorReportingMethodType.ExceptionReporting:
                    _v = "ExceptionReporting"; break;
                case InvestorReportingMethodType.FullReporting:
                    _v = "FullReporting"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected InvestorReportingMethodType ReadElementString_InvestorReportingMethodType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return InvestorReportingMethodType.Undefined;
            }
            switch (str)
            {
                case "ExceptionReporting":
                    return InvestorReportingMethodType.ExceptionReporting;
                case "FullReporting":
                    return InvestorReportingMethodType.FullReporting;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
