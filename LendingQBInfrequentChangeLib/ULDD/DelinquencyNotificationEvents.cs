// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class DelinquencyNotificationEvents : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "DELINQUENCY_NOTIFICATION_EVENTS"; } }

        #region Property List
        private List<DelinquencyNotificationEvent> m_delinquencyNotificationEventList;
        public List<DelinquencyNotificationEvent> DelinquencyNotificationEventList
        {
            get
            {
                if (m_delinquencyNotificationEventList == null)
                {
                    m_delinquencyNotificationEventList = new List<DelinquencyNotificationEvent>();
                }
                return m_delinquencyNotificationEventList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DELINQUENCY_NOTIFICATION_EVENT":
                    ReadElement(reader, DelinquencyNotificationEventList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_delinquencyNotificationEventList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
