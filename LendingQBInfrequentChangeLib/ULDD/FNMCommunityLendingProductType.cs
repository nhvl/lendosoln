// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum FNMCommunityLendingProductType
    {
        Undefined = 0,
        Fannie97,
        Fannie32,
        MyCommunityMortgage,
        Other,
        CommunityHomeBuyersProgram,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, FNMCommunityLendingProductType value)
        {
            if (value == FNMCommunityLendingProductType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case FNMCommunityLendingProductType.Fannie97:
                    _v = "Fannie97"; break;
                case FNMCommunityLendingProductType.Fannie32:
                    _v = "Fannie32"; break;
                case FNMCommunityLendingProductType.MyCommunityMortgage:
                    _v = "MyCommunityMortgage"; break;
                case FNMCommunityLendingProductType.Other:
                    _v = "Other"; break;
                case FNMCommunityLendingProductType.CommunityHomeBuyersProgram:
                    _v = "CommunityHomeBuyersProgram"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected FNMCommunityLendingProductType ReadElementString_FNMCommunityLendingProductType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return FNMCommunityLendingProductType.Undefined;
            }
            switch (str)
            {
                case "Fannie97":
                    return FNMCommunityLendingProductType.Fannie97;
                case "Fannie32":
                    return FNMCommunityLendingProductType.Fannie32;
                case "MyCommunityMortgage":
                    return FNMCommunityLendingProductType.MyCommunityMortgage;
                case "Other":
                    return FNMCommunityLendingProductType.Other;
                case "CommunityHomeBuyersProgram":
                    return FNMCommunityLendingProductType.CommunityHomeBuyersProgram;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
