// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class RespaSummaryTotalFeesPaidBy : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "RESPA_SUMMARY_TOTAL_FEES_PAID_BY"; } }

        #region Property List
        public RESPASummaryTotalFeesPaidByType RESPASummaryTotalFeesPaidByType { get; set; }
        public string RESPASummaryTotalFeesPaidByTypeAmount { get; set; }
        public string RESPASummaryTotalFeesPaidByTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "RESPASummaryTotalFeesPaidByType":
                    RESPASummaryTotalFeesPaidByType = ReadElementString_RESPASummaryTotalFeesPaidByType(reader); break;
                case "RESPASummaryTotalFeesPaidByTypeAmount":
                    RESPASummaryTotalFeesPaidByTypeAmount = ReadElementString(reader); break;
                case "RESPASummaryTotalFeesPaidByTypeOtherDescription":
                    RESPASummaryTotalFeesPaidByTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "RESPASummaryTotalFeesPaidByType", RESPASummaryTotalFeesPaidByType);
            WriteElementString(writer, "RESPASummaryTotalFeesPaidByTypeAmount", RESPASummaryTotalFeesPaidByTypeAmount);
            WriteElementString(writer, "RESPASummaryTotalFeesPaidByTypeOtherDescription", RESPASummaryTotalFeesPaidByTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
