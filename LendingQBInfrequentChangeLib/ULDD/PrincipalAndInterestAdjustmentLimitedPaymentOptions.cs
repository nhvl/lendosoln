// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class PrincipalAndInterestAdjustmentLimitedPaymentOptions : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS"; } }

        #region Property List
        private List<PrincipalAndInterestAdjustmentLimitedPaymentOption> m_principalAndInterestAdjustmentLimitedPaymentOptionList;
        public List<PrincipalAndInterestAdjustmentLimitedPaymentOption> PrincipalAndInterestAdjustmentLimitedPaymentOptionList
        {
            get
            {
                if (m_principalAndInterestAdjustmentLimitedPaymentOptionList == null)
                {
                    m_principalAndInterestAdjustmentLimitedPaymentOptionList = new List<PrincipalAndInterestAdjustmentLimitedPaymentOption>();
                }
                return m_principalAndInterestAdjustmentLimitedPaymentOptionList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION":
                    ReadElement(reader, PrincipalAndInterestAdjustmentLimitedPaymentOptionList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_principalAndInterestAdjustmentLimitedPaymentOptionList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
