// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum SignatureFieldMarkType
    {
        Undefined = 0,
        FullSignature,
        Initials,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, SignatureFieldMarkType value)
        {
            if (value == SignatureFieldMarkType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case SignatureFieldMarkType.FullSignature:
                    _v = "FullSignature"; break;
                case SignatureFieldMarkType.Initials:
                    _v = "Initials"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected SignatureFieldMarkType ReadElementString_SignatureFieldMarkType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return SignatureFieldMarkType.Undefined;
            }
            switch (str)
            {
                case "FullSignature":
                    return SignatureFieldMarkType.FullSignature;
                case "Initials":
                    return SignatureFieldMarkType.Initials;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
