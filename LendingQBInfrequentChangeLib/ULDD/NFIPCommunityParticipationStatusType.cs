// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum NFIPCommunityParticipationStatusType
    {
        Undefined = 0,
        Emergency,
        NonParticipating,
        _Non_Participating,
        Other,
        Probation,
        Regular,
        Suspended,
        Unknown,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, NFIPCommunityParticipationStatusType value)
        {
            if (value == NFIPCommunityParticipationStatusType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case NFIPCommunityParticipationStatusType.Emergency:
                    _v = "Emergency"; break;
                case NFIPCommunityParticipationStatusType.NonParticipating:
                    _v = "NonParticipating"; break;
                case NFIPCommunityParticipationStatusType._Non_Participating:
                    _v = "Non-Participating"; break;
                case NFIPCommunityParticipationStatusType.Other:
                    _v = "Other"; break;
                case NFIPCommunityParticipationStatusType.Probation:
                    _v = "Probation"; break;
                case NFIPCommunityParticipationStatusType.Regular:
                    _v = "Regular"; break;
                case NFIPCommunityParticipationStatusType.Suspended:
                    _v = "Suspended"; break;
                case NFIPCommunityParticipationStatusType.Unknown:
                    _v = "Unknown"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected NFIPCommunityParticipationStatusType ReadElementString_NFIPCommunityParticipationStatusType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return NFIPCommunityParticipationStatusType.Undefined;
            }
            switch (str)
            {
                case "Emergency":
                    return NFIPCommunityParticipationStatusType.Emergency;
                case "NonParticipating":
                    return NFIPCommunityParticipationStatusType.NonParticipating;
                case "Non-Participating":
                    return NFIPCommunityParticipationStatusType._Non_Participating;
                case "Other":
                    return NFIPCommunityParticipationStatusType.Other;
                case "Probation":
                    return NFIPCommunityParticipationStatusType.Probation;
                case "Regular":
                    return NFIPCommunityParticipationStatusType.Regular;
                case "Suspended":
                    return NFIPCommunityParticipationStatusType.Suspended;
                case "Unknown":
                    return NFIPCommunityParticipationStatusType.Unknown;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
