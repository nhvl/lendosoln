// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum UniqueDwellingType
    {
        Undefined = 0,
        EarthShelterHome,
        GeodesicDome,
        Houseboat,
        LogHome,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, UniqueDwellingType value)
        {
            if (value == UniqueDwellingType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case UniqueDwellingType.EarthShelterHome:
                    _v = "EarthShelterHome"; break;
                case UniqueDwellingType.GeodesicDome:
                    _v = "GeodesicDome"; break;
                case UniqueDwellingType.Houseboat:
                    _v = "Houseboat"; break;
                case UniqueDwellingType.LogHome:
                    _v = "LogHome"; break;
                case UniqueDwellingType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected UniqueDwellingType ReadElementString_UniqueDwellingType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return UniqueDwellingType.Undefined;
            }
            switch (str)
            {
                case "EarthShelterHome":
                    return UniqueDwellingType.EarthShelterHome;
                case "GeodesicDome":
                    return UniqueDwellingType.GeodesicDome;
                case "Houseboat":
                    return UniqueDwellingType.Houseboat;
                case "LogHome":
                    return UniqueDwellingType.LogHome;
                case "Other":
                    return UniqueDwellingType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
