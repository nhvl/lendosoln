// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum LoanAmortizationPeriodType
    {
        Undefined = 0,
        Biweekly,
        Day,
        Month,
        Quarter,
        Semimonthly,
        Week,
        Year,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, LoanAmortizationPeriodType value)
        {
            if (value == LoanAmortizationPeriodType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case LoanAmortizationPeriodType.Biweekly:
                    _v = "Biweekly"; break;
                case LoanAmortizationPeriodType.Day:
                    _v = "Day"; break;
                case LoanAmortizationPeriodType.Month:
                    _v = "Month"; break;
                case LoanAmortizationPeriodType.Quarter:
                    _v = "Quarter"; break;
                case LoanAmortizationPeriodType.Semimonthly:
                    _v = "Semimonthly"; break;
                case LoanAmortizationPeriodType.Week:
                    _v = "Week"; break;
                case LoanAmortizationPeriodType.Year:
                    _v = "Year"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected LoanAmortizationPeriodType ReadElementString_LoanAmortizationPeriodType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return LoanAmortizationPeriodType.Undefined;
            }
            switch (str)
            {
                case "Biweekly":
                    return LoanAmortizationPeriodType.Biweekly;
                case "Day":
                    return LoanAmortizationPeriodType.Day;
                case "Month":
                    return LoanAmortizationPeriodType.Month;
                case "Quarter":
                    return LoanAmortizationPeriodType.Quarter;
                case "Semimonthly":
                    return LoanAmortizationPeriodType.Semimonthly;
                case "Week":
                    return LoanAmortizationPeriodType.Week;
                case "Year":
                    return LoanAmortizationPeriodType.Year;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
