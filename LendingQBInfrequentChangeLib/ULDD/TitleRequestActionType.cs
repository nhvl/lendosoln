// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum TitleRequestActionType
    {
        Undefined = 0,
        Cancellation,
        Change,
        Original,
        PriceQuote,
        StatusQuery,
        Update,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, TitleRequestActionType value)
        {
            if (value == TitleRequestActionType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case TitleRequestActionType.Cancellation:
                    _v = "Cancellation"; break;
                case TitleRequestActionType.Change:
                    _v = "Change"; break;
                case TitleRequestActionType.Original:
                    _v = "Original"; break;
                case TitleRequestActionType.PriceQuote:
                    _v = "PriceQuote"; break;
                case TitleRequestActionType.StatusQuery:
                    _v = "StatusQuery"; break;
                case TitleRequestActionType.Update:
                    _v = "Update"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected TitleRequestActionType ReadElementString_TitleRequestActionType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return TitleRequestActionType.Undefined;
            }
            switch (str)
            {
                case "Cancellation":
                    return TitleRequestActionType.Cancellation;
                case "Change":
                    return TitleRequestActionType.Change;
                case "Original":
                    return TitleRequestActionType.Original;
                case "PriceQuote":
                    return TitleRequestActionType.PriceQuote;
                case "StatusQuery":
                    return TitleRequestActionType.StatusQuery;
                case "Update":
                    return TitleRequestActionType.Update;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
