// Generated By CodeMonkey 8/27/2012 12:58:44 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class ViewFiles : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "VIEW_FILES"; } }

        #region Property List
        private List<ViewFile> m_viewFileList;
        public List<ViewFile> ViewFileList
        {
            get
            {
                if (m_viewFileList == null)
                {
                    m_viewFileList = new List<ViewFile>();
                }
                return m_viewFileList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "VIEW_FILE":
                    ReadElement(reader, ViewFileList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_viewFileList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
