// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class HmdaRaces : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "HMDA_RACES"; } }

        #region Property List
        private List<HmdaRace> m_hmdaRaceList;
        public List<HmdaRace> HmdaRaceList
        {
            get
            {
                if (m_hmdaRaceList == null)
                {
                    m_hmdaRaceList = new List<HmdaRace>();
                }
                return m_hmdaRaceList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "HMDA_RACE":
                    ReadElement(reader, HmdaRaceList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_hmdaRaceList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
