// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class FieldReference : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "FIELD_REFERENCE"; } }

        #region Property List
        public string FieldHeightNumber { get; set; }
        public string FieldPlaceholderIdentifier { get; set; }
        public string FieldWidthNumber { get; set; }
        public MeasurementUnitType MeasurementUnitType { get; set; }
        public string OffsetFromLeftNumber { get; set; }
        public string OffsetFromTopNumber { get; set; }
        public string PageNumber { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "FieldHeightNumber":
                    FieldHeightNumber = ReadElementString(reader); break;
                case "FieldPlaceholderIdentifier":
                    FieldPlaceholderIdentifier = ReadElementString(reader); break;
                case "FieldWidthNumber":
                    FieldWidthNumber = ReadElementString(reader); break;
                case "MeasurementUnitType":
                    MeasurementUnitType = ReadElementString_MeasurementUnitType(reader); break;
                case "OffsetFromLeftNumber":
                    OffsetFromLeftNumber = ReadElementString(reader); break;
                case "OffsetFromTopNumber":
                    OffsetFromTopNumber = ReadElementString(reader); break;
                case "PageNumber":
                    PageNumber = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "FieldHeightNumber", FieldHeightNumber);
            WriteElementString(writer, "FieldPlaceholderIdentifier", FieldPlaceholderIdentifier);
            WriteElementString(writer, "FieldWidthNumber", FieldWidthNumber);
            WriteElementString(writer, "MeasurementUnitType", MeasurementUnitType);
            WriteElementString(writer, "OffsetFromLeftNumber", OffsetFromLeftNumber);
            WriteElementString(writer, "OffsetFromTopNumber", OffsetFromTopNumber);
            WriteElementString(writer, "PageNumber", PageNumber);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
