// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class ParsedLegalDescription : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PARSED_LEGAL_DESCRIPTION"; } }

        #region Property List
        private PlattedLands m_plattedLands;
        public PlattedLands PlattedLands
        {
            get
            {
                if (m_plattedLands == null)
                {
                    m_plattedLands = new PlattedLands();
                }
                return m_plattedLands;
            }
            set { m_plattedLands = value; }
        }
        private UnplattedLands m_unplattedLands;
        public UnplattedLands UnplattedLands
        {
            get
            {
                if (m_unplattedLands == null)
                {
                    m_unplattedLands = new UnplattedLands();
                }
                return m_unplattedLands;
            }
            set { m_unplattedLands = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "PLATTED_LANDS":
                    ReadElement(reader, PlattedLands); break;
                case "UNPLATTED_LANDS":
                    ReadElement(reader, UnplattedLands); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_plattedLands);
            WriteElement(writer, m_unplattedLands);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
