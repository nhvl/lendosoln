// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum LoanDelinquencyStatusType
    {
        Undefined = 0,
        AssignmentPursued,
        AssumptionPursued,
        ChapterElevenBankruptcy,
        ChapterSevenBankruptcy,
        ChapterThirteenBankruptcy,
        ChapterTwelveBankruptcy,
        ChargeOffPursued,
        ContestedForeclosure,
        DrugSeizurePending,
        Forbearance,
        Foreclosure,
        GovernmentSeizurePending,
        MilitaryIndulgence,
        ModificationPursued,
        Other,
        PreforeclosureSalePlanned,
        Probate,
        RefinancePursued,
        RepaymentPlan,
        SecondLienConsiderations,
        VeteransAffairsBuydown,
        VeteransAffairsNoBids,
        VeteransAffairsRefundPursued,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, LoanDelinquencyStatusType value)
        {
            if (value == LoanDelinquencyStatusType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case LoanDelinquencyStatusType.AssignmentPursued:
                    _v = "AssignmentPursued"; break;
                case LoanDelinquencyStatusType.AssumptionPursued:
                    _v = "AssumptionPursued"; break;
                case LoanDelinquencyStatusType.ChapterElevenBankruptcy:
                    _v = "ChapterElevenBankruptcy"; break;
                case LoanDelinquencyStatusType.ChapterSevenBankruptcy:
                    _v = "ChapterSevenBankruptcy"; break;
                case LoanDelinquencyStatusType.ChapterThirteenBankruptcy:
                    _v = "ChapterThirteenBankruptcy"; break;
                case LoanDelinquencyStatusType.ChapterTwelveBankruptcy:
                    _v = "ChapterTwelveBankruptcy"; break;
                case LoanDelinquencyStatusType.ChargeOffPursued:
                    _v = "ChargeOffPursued"; break;
                case LoanDelinquencyStatusType.ContestedForeclosure:
                    _v = "ContestedForeclosure"; break;
                case LoanDelinquencyStatusType.DrugSeizurePending:
                    _v = "DrugSeizurePending"; break;
                case LoanDelinquencyStatusType.Forbearance:
                    _v = "Forbearance"; break;
                case LoanDelinquencyStatusType.Foreclosure:
                    _v = "Foreclosure"; break;
                case LoanDelinquencyStatusType.GovernmentSeizurePending:
                    _v = "GovernmentSeizurePending"; break;
                case LoanDelinquencyStatusType.MilitaryIndulgence:
                    _v = "MilitaryIndulgence"; break;
                case LoanDelinquencyStatusType.ModificationPursued:
                    _v = "ModificationPursued"; break;
                case LoanDelinquencyStatusType.Other:
                    _v = "Other"; break;
                case LoanDelinquencyStatusType.PreforeclosureSalePlanned:
                    _v = "PreforeclosureSalePlanned"; break;
                case LoanDelinquencyStatusType.Probate:
                    _v = "Probate"; break;
                case LoanDelinquencyStatusType.RefinancePursued:
                    _v = "RefinancePursued"; break;
                case LoanDelinquencyStatusType.RepaymentPlan:
                    _v = "RepaymentPlan"; break;
                case LoanDelinquencyStatusType.SecondLienConsiderations:
                    _v = "SecondLienConsiderations"; break;
                case LoanDelinquencyStatusType.VeteransAffairsBuydown:
                    _v = "VeteransAffairsBuydown"; break;
                case LoanDelinquencyStatusType.VeteransAffairsNoBids:
                    _v = "VeteransAffairsNoBids"; break;
                case LoanDelinquencyStatusType.VeteransAffairsRefundPursued:
                    _v = "VeteransAffairsRefundPursued"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected LoanDelinquencyStatusType ReadElementString_LoanDelinquencyStatusType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return LoanDelinquencyStatusType.Undefined;
            }
            switch (str)
            {
                case "AssignmentPursued":
                    return LoanDelinquencyStatusType.AssignmentPursued;
                case "AssumptionPursued":
                    return LoanDelinquencyStatusType.AssumptionPursued;
                case "ChapterElevenBankruptcy":
                    return LoanDelinquencyStatusType.ChapterElevenBankruptcy;
                case "ChapterSevenBankruptcy":
                    return LoanDelinquencyStatusType.ChapterSevenBankruptcy;
                case "ChapterThirteenBankruptcy":
                    return LoanDelinquencyStatusType.ChapterThirteenBankruptcy;
                case "ChapterTwelveBankruptcy":
                    return LoanDelinquencyStatusType.ChapterTwelveBankruptcy;
                case "ChargeOffPursued":
                    return LoanDelinquencyStatusType.ChargeOffPursued;
                case "ContestedForeclosure":
                    return LoanDelinquencyStatusType.ContestedForeclosure;
                case "DrugSeizurePending":
                    return LoanDelinquencyStatusType.DrugSeizurePending;
                case "Forbearance":
                    return LoanDelinquencyStatusType.Forbearance;
                case "Foreclosure":
                    return LoanDelinquencyStatusType.Foreclosure;
                case "GovernmentSeizurePending":
                    return LoanDelinquencyStatusType.GovernmentSeizurePending;
                case "MilitaryIndulgence":
                    return LoanDelinquencyStatusType.MilitaryIndulgence;
                case "ModificationPursued":
                    return LoanDelinquencyStatusType.ModificationPursued;
                case "Other":
                    return LoanDelinquencyStatusType.Other;
                case "PreforeclosureSalePlanned":
                    return LoanDelinquencyStatusType.PreforeclosureSalePlanned;
                case "Probate":
                    return LoanDelinquencyStatusType.Probate;
                case "RefinancePursued":
                    return LoanDelinquencyStatusType.RefinancePursued;
                case "RepaymentPlan":
                    return LoanDelinquencyStatusType.RepaymentPlan;
                case "SecondLienConsiderations":
                    return LoanDelinquencyStatusType.SecondLienConsiderations;
                case "VeteransAffairsBuydown":
                    return LoanDelinquencyStatusType.VeteransAffairsBuydown;
                case "VeteransAffairsNoBids":
                    return LoanDelinquencyStatusType.VeteransAffairsNoBids;
                case "VeteransAffairsRefundPursued":
                    return LoanDelinquencyStatusType.VeteransAffairsRefundPursued;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
