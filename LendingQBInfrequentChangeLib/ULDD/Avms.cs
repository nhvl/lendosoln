// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Avms : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "AVMS"; } }

        #region Property List
        private List<Avm> m_avmList;
        public List<Avm> AvmList
        {
            get
            {
                if (m_avmList == null)
                {
                    m_avmList = new List<Avm>();
                }
                return m_avmList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AVM":
                    ReadElement(reader, AvmList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_avmList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
