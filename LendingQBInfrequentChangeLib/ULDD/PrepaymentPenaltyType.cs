// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum PrepaymentPenaltyType
    {
        Undefined = 0,
        Assumption,
        EarlyPayoff,
        Other,
        PrincipalCurtailment,
        Refinance,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, PrepaymentPenaltyType value)
        {
            if (value == PrepaymentPenaltyType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case PrepaymentPenaltyType.Assumption:
                    _v = "Assumption"; break;
                case PrepaymentPenaltyType.EarlyPayoff:
                    _v = "EarlyPayoff"; break;
                case PrepaymentPenaltyType.Other:
                    _v = "Other"; break;
                case PrepaymentPenaltyType.PrincipalCurtailment:
                    _v = "PrincipalCurtailment"; break;
                case PrepaymentPenaltyType.Refinance:
                    _v = "Refinance"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected PrepaymentPenaltyType ReadElementString_PrepaymentPenaltyType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return PrepaymentPenaltyType.Undefined;
            }
            switch (str)
            {
                case "Assumption":
                    return PrepaymentPenaltyType.Assumption;
                case "EarlyPayoff":
                    return PrepaymentPenaltyType.EarlyPayoff;
                case "Other":
                    return PrepaymentPenaltyType.Other;
                case "PrincipalCurtailment":
                    return PrepaymentPenaltyType.PrincipalCurtailment;
                case "Refinance":
                    return PrepaymentPenaltyType.Refinance;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
