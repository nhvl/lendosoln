// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum TitleEndorsementSourceType
    {
        Undefined = 0,
        ALTA,
        CLTA,
        Other,
        TLTA,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, TitleEndorsementSourceType value)
        {
            if (value == TitleEndorsementSourceType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case TitleEndorsementSourceType.ALTA:
                    _v = "ALTA"; break;
                case TitleEndorsementSourceType.CLTA:
                    _v = "CLTA"; break;
                case TitleEndorsementSourceType.Other:
                    _v = "Other"; break;
                case TitleEndorsementSourceType.TLTA:
                    _v = "TLTA"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected TitleEndorsementSourceType ReadElementString_TitleEndorsementSourceType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return TitleEndorsementSourceType.Undefined;
            }
            switch (str)
            {
                case "ALTA":
                    return TitleEndorsementSourceType.ALTA;
                case "CLTA":
                    return TitleEndorsementSourceType.CLTA;
                case "Other":
                    return TitleEndorsementSourceType.Other;
                case "TLTA":
                    return TitleEndorsementSourceType.TLTA;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
