// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum NegativeAmortizationRecastType
    {
        Undefined = 0,
        BothScheduledAndMaximumPercent,
        MaximumPercent,
        Scheduled,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, NegativeAmortizationRecastType value)
        {
            if (value == NegativeAmortizationRecastType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case NegativeAmortizationRecastType.BothScheduledAndMaximumPercent:
                    _v = "BothScheduledAndMaximumPercent"; break;
                case NegativeAmortizationRecastType.MaximumPercent:
                    _v = "MaximumPercent"; break;
                case NegativeAmortizationRecastType.Scheduled:
                    _v = "Scheduled"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected NegativeAmortizationRecastType ReadElementString_NegativeAmortizationRecastType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return NegativeAmortizationRecastType.Undefined;
            }
            switch (str)
            {
                case "BothScheduledAndMaximumPercent":
                    return NegativeAmortizationRecastType.BothScheduledAndMaximumPercent;
                case "MaximumPercent":
                    return NegativeAmortizationRecastType.MaximumPercent;
                case "Scheduled":
                    return NegativeAmortizationRecastType.Scheduled;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
