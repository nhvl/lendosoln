// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Individual : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "INDIVIDUAL"; } }

        #region Property List
        private Aliases m_aliases;
        public Aliases Aliases
        {
            get
            {
                if (m_aliases == null)
                {
                    m_aliases = new Aliases();
                }
                return m_aliases;
            }
            set { m_aliases = value; }
        }
        private ContactPoints m_contactPoints;
        public ContactPoints ContactPoints
        {
            get
            {
                if (m_contactPoints == null)
                {
                    m_contactPoints = new ContactPoints();
                }
                return m_contactPoints;
            }
            set { m_contactPoints = value; }
        }
        private IdentificationVerification m_identificationVerification;
        public IdentificationVerification IdentificationVerification
        {
            get
            {
                if (m_identificationVerification == null)
                {
                    m_identificationVerification = new IdentificationVerification();
                }
                return m_identificationVerification;
            }
            set { m_identificationVerification = value; }
        }
        private Name m_name;
        public Name Name
        {
            get
            {
                if (m_name == null)
                {
                    m_name = new Name();
                }
                return m_name;
            }
            set { m_name = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ALIASES":
                    ReadElement(reader, Aliases); break;
                case "CONTACT_POINTS":
                    ReadElement(reader, ContactPoints); break;
                case "IDENTIFICATION_VERIFICATION":
                    ReadElement(reader, IdentificationVerification); break;
                case "NAME":
                    ReadElement(reader, Name); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_aliases);
            WriteElement(writer, m_contactPoints);
            WriteElement(writer, m_identificationVerification);
            WriteElement(writer, m_name);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
