﻿namespace ULDD
{
    public enum HMDAEthnicityBase
    {
        Blank = 0,
        HispanicOrLatino,
        InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication,
        NotApplicable,
        NotHispanicOrLatino,
    }
}
