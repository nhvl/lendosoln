// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class TermsOfMortgage : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "TERMS_OF_MORTGAGE"; } }

        #region Property List
        public string DisclosedFullyIndexedRatePercent { get; set; }
        public string DisclosedIndexRatePercent { get; set; }
        public string DisclosedMarginRatePercent { get; set; }
        public LienPriorityType LienPriorityType { get; set; }
        public string LienPriorityTypeOtherDescription { get; set; }
        public LoanPurposeType LoanPurposeType { get; set; }
        public string LoanPurposeTypeOtherDescription { get; set; }
        public MortgageType MortgageType { get; set; }
        public string MortgageTypeOtherDescription { get; set; }
        public string NoteAmount { get; set; }
        public string NoteDate { get; set; }
        public string NoteRatePercent { get; set; }
        public string OriginalInterestRateDiscountPercent { get; set; }
        public UPBChangeFrequencyType UPBChangeFrequencyType { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DisclosedFullyIndexedRatePercent":
                    DisclosedFullyIndexedRatePercent = ReadElementString(reader); break;
                case "DisclosedIndexRatePercent":
                    DisclosedIndexRatePercent = ReadElementString(reader); break;
                case "DisclosedMarginRatePercent":
                    DisclosedMarginRatePercent = ReadElementString(reader); break;
                case "LienPriorityType":
                    LienPriorityType = ReadElementString_LienPriorityType(reader); break;
                case "LienPriorityTypeOtherDescription":
                    LienPriorityTypeOtherDescription = ReadElementString(reader); break;
                case "LoanPurposeType":
                    LoanPurposeType = ReadElementString_LoanPurposeType(reader); break;
                case "LoanPurposeTypeOtherDescription":
                    LoanPurposeTypeOtherDescription = ReadElementString(reader); break;
                case "MortgageType":
                    MortgageType = ReadElementString_MortgageType(reader); break;
                case "MortgageTypeOtherDescription":
                    MortgageTypeOtherDescription = ReadElementString(reader); break;
                case "NoteAmount":
                    NoteAmount = ReadElementString(reader); break;
                case "NoteDate":
                    NoteDate = ReadElementString(reader); break;
                case "NoteRatePercent":
                    NoteRatePercent = ReadElementString(reader); break;
                case "OriginalInterestRateDiscountPercent":
                    OriginalInterestRateDiscountPercent = ReadElementString(reader); break;
                case "UPBChangeFrequencyType":
                    UPBChangeFrequencyType = ReadElementString_UPBChangeFrequencyType(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "DisclosedFullyIndexedRatePercent", DisclosedFullyIndexedRatePercent);
            WriteElementString(writer, "DisclosedIndexRatePercent", DisclosedIndexRatePercent);
            WriteElementString(writer, "DisclosedMarginRatePercent", DisclosedMarginRatePercent);
            WriteElementString(writer, "LienPriorityType", LienPriorityType);
            WriteElementString(writer, "LienPriorityTypeOtherDescription", LienPriorityTypeOtherDescription);
            WriteElementString(writer, "LoanPurposeType", LoanPurposeType);
            WriteElementString(writer, "LoanPurposeTypeOtherDescription", LoanPurposeTypeOtherDescription);
            WriteElementString(writer, "MortgageType", MortgageType);
            WriteElementString(writer, "MortgageTypeOtherDescription", MortgageTypeOtherDescription);
            WriteElementString(writer, "NoteAmount", NoteAmount);
            WriteElementString(writer, "NoteDate", NoteDate);
            WriteElementString(writer, "NoteRatePercent", NoteRatePercent);
            WriteElementString(writer, "OriginalInterestRateDiscountPercent", OriginalInterestRateDiscountPercent);
            WriteElementString(writer, "UPBChangeFrequencyType", UPBChangeFrequencyType);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
