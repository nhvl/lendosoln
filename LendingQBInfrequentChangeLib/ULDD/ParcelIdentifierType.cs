// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum ParcelIdentifierType
    {
        Undefined = 0,
        ParcelIdentificationNumber,
        TaxMapIdentifier,
        TaxParcelIdentifier,
        AssessorUnformattedIdentifier,
        TorrensCertificateIdentifier,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, ParcelIdentifierType value)
        {
            if (value == ParcelIdentifierType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case ParcelIdentifierType.ParcelIdentificationNumber:
                    _v = "ParcelIdentificationNumber"; break;
                case ParcelIdentifierType.TaxMapIdentifier:
                    _v = "TaxMapIdentifier"; break;
                case ParcelIdentifierType.TaxParcelIdentifier:
                    _v = "TaxParcelIdentifier"; break;
                case ParcelIdentifierType.AssessorUnformattedIdentifier:
                    _v = "AssessorUnformattedIdentifier"; break;
                case ParcelIdentifierType.TorrensCertificateIdentifier:
                    _v = "TorrensCertificateIdentifier"; break;
                case ParcelIdentifierType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected ParcelIdentifierType ReadElementString_ParcelIdentifierType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return ParcelIdentifierType.Undefined;
            }
            switch (str)
            {
                case "ParcelIdentificationNumber":
                    return ParcelIdentifierType.ParcelIdentificationNumber;
                case "TaxMapIdentifier":
                    return ParcelIdentifierType.TaxMapIdentifier;
                case "TaxParcelIdentifier":
                    return ParcelIdentifierType.TaxParcelIdentifier;
                case "AssessorUnformattedIdentifier":
                    return ParcelIdentifierType.AssessorUnformattedIdentifier;
                case "TorrensCertificateIdentifier":
                    return ParcelIdentifierType.TorrensCertificateIdentifier;
                case "Other":
                    return ParcelIdentifierType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
