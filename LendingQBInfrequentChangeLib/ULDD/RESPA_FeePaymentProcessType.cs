// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum RESPA_FeePaymentProcessType
    {
        Undefined = 0,
        Closing,
        Processing,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, RESPA_FeePaymentProcessType value)
        {
            if (value == RESPA_FeePaymentProcessType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case RESPA_FeePaymentProcessType.Closing:
                    _v = "Closing"; break;
                case RESPA_FeePaymentProcessType.Processing:
                    _v = "Processing"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected RESPA_FeePaymentProcessType ReadElementString_RESPA_FeePaymentProcessType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return RESPA_FeePaymentProcessType.Undefined;
            }
            switch (str)
            {
                case "Closing":
                    return RESPA_FeePaymentProcessType.Closing;
                case "Processing":
                    return RESPA_FeePaymentProcessType.Processing;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
