// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Employers : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "EMPLOYERS"; } }

        #region Property List
        private List<Employer> m_employerList;
        public List<Employer> EmployerList
        {
            get
            {
                if (m_employerList == null)
                {
                    m_employerList = new List<Employer>();
                }
                return m_employerList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "EMPLOYER":
                    ReadElement(reader, EmployerList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_employerList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
