// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class ClosingInformation : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "CLOSING_INFORMATION"; } }

        #region Property List
        private ClosingCostFunds m_closingCostFunds;
        public ClosingCostFunds ClosingCostFunds
        {
            get
            {
                if (m_closingCostFunds == null)
                {
                    m_closingCostFunds = new ClosingCostFunds();
                }
                return m_closingCostFunds;
            }
            set { m_closingCostFunds = value; }
        }
        private ClosingInformationDetail m_closingInformationDetail;
        public ClosingInformationDetail ClosingInformationDetail
        {
            get
            {
                if (m_closingInformationDetail == null)
                {
                    m_closingInformationDetail = new ClosingInformationDetail();
                }
                return m_closingInformationDetail;
            }
            set { m_closingInformationDetail = value; }
        }
        private ClosingInstruction m_closingInstruction;
        public ClosingInstruction ClosingInstruction
        {
            get
            {
                if (m_closingInstruction == null)
                {
                    m_closingInstruction = new ClosingInstruction();
                }
                return m_closingInstruction;
            }
            set { m_closingInstruction = value; }
        }
        private CollectedOtherFunds m_collectedOtherFunds;
        public CollectedOtherFunds CollectedOtherFunds
        {
            get
            {
                if (m_collectedOtherFunds == null)
                {
                    m_collectedOtherFunds = new CollectedOtherFunds();
                }
                return m_collectedOtherFunds;
            }
            set { m_collectedOtherFunds = value; }
        }
        private Compensations m_compensations;
        public Compensations Compensations
        {
            get
            {
                if (m_compensations == null)
                {
                    m_compensations = new Compensations();
                }
                return m_compensations;
            }
            set { m_compensations = value; }
        }
        private InterimInterests m_interimInterests;
        public InterimInterests InterimInterests
        {
            get
            {
                if (m_interimInterests == null)
                {
                    m_interimInterests = new InterimInterests();
                }
                return m_interimInterests;
            }
            set { m_interimInterests = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CLOSING_COST_FUNDS":
                    ReadElement(reader, ClosingCostFunds); break;
                case "CLOSING_INFORMATION_DETAIL":
                    ReadElement(reader, ClosingInformationDetail); break;
                case "CLOSING_INSTRUCTION":
                    ReadElement(reader, ClosingInstruction); break;
                case "COLLECTED_OTHER_FUNDS":
                    ReadElement(reader, CollectedOtherFunds); break;
                case "COMPENSATIONS":
                    ReadElement(reader, Compensations); break;
                case "INTERIM_INTERESTS":
                    ReadElement(reader, InterimInterests); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_closingCostFunds);
            WriteElement(writer, m_closingInformationDetail);
            WriteElement(writer, m_closingInstruction);
            WriteElement(writer, m_collectedOtherFunds);
            WriteElement(writer, m_compensations);
            WriteElement(writer, m_interimInterests);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
