// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum ConversionOptionPeriodType
    {
        Undefined = 0,
        OnDemand,
        OnDemandAtInterestRateChangeDates,
        OnDemandMonthly,
        Scheduled,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, ConversionOptionPeriodType value)
        {
            if (value == ConversionOptionPeriodType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case ConversionOptionPeriodType.OnDemand:
                    _v = "OnDemand"; break;
                case ConversionOptionPeriodType.OnDemandAtInterestRateChangeDates:
                    _v = "OnDemandAtInterestRateChangeDates"; break;
                case ConversionOptionPeriodType.OnDemandMonthly:
                    _v = "OnDemandMonthly"; break;
                case ConversionOptionPeriodType.Scheduled:
                    _v = "Scheduled"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected ConversionOptionPeriodType ReadElementString_ConversionOptionPeriodType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return ConversionOptionPeriodType.Undefined;
            }
            switch (str)
            {
                case "OnDemand":
                    return ConversionOptionPeriodType.OnDemand;
                case "OnDemandAtInterestRateChangeDates":
                    return ConversionOptionPeriodType.OnDemandAtInterestRateChangeDates;
                case "OnDemandMonthly":
                    return ConversionOptionPeriodType.OnDemandMonthly;
                case "Scheduled":
                    return ConversionOptionPeriodType.Scheduled;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
