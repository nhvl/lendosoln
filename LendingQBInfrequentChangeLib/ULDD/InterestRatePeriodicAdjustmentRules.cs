// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class InterestRatePeriodicAdjustmentRules : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES"; } }

        #region Property List
        private List<InterestRatePeriodicAdjustmentRule> m_interestRatePeriodicAdjustmentRuleList;
        public List<InterestRatePeriodicAdjustmentRule> InterestRatePeriodicAdjustmentRuleList
        {
            get
            {
                if (m_interestRatePeriodicAdjustmentRuleList == null)
                {
                    m_interestRatePeriodicAdjustmentRuleList = new List<InterestRatePeriodicAdjustmentRule>();
                }
                return m_interestRatePeriodicAdjustmentRuleList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE":
                    ReadElement(reader, InterestRatePeriodicAdjustmentRuleList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_interestRatePeriodicAdjustmentRuleList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
