// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class InterestOnly : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "INTEREST_ONLY"; } }

        #region Property List
        public string InterestOnlyEndDate { get; set; }
        public string InterestOnlyMonthlyPaymentAmount { get; set; }
        public string InterestOnlyTermPaymentsCount { get; set; }
        public string InterestOnlyTermMonthsCount { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "InterestOnlyEndDate":
                    InterestOnlyEndDate = ReadElementString(reader); break;
                case "InterestOnlyMonthlyPaymentAmount":
                    InterestOnlyMonthlyPaymentAmount = ReadElementString(reader); break;
                case "InterestOnlyTermPaymentsCount":
                    InterestOnlyTermPaymentsCount = ReadElementString(reader); break;
                case "InterestOnlyTermMonthsCount":
                    InterestOnlyTermMonthsCount = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "InterestOnlyEndDate", InterestOnlyEndDate);
            WriteElementString(writer, "InterestOnlyMonthlyPaymentAmount", InterestOnlyMonthlyPaymentAmount);
            WriteElementString(writer, "InterestOnlyTermPaymentsCount", InterestOnlyTermPaymentsCount);
            WriteElementString(writer, "InterestOnlyTermMonthsCount", InterestOnlyTermMonthsCount);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
