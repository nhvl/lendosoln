// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Executions : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "EXECUTIONS"; } }

        #region Property List
        private List<Execution> m_executionList;
        public List<Execution> ExecutionList
        {
            get
            {
                if (m_executionList == null)
                {
                    m_executionList = new List<Execution>();
                }
                return m_executionList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "EXECUTION":
                    ReadElement(reader, ExecutionList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_executionList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
