// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum ClosingInstructionsConditionSatisfactionResponsiblePartyType
    {
        Undefined = 0,
        Attorney,
        Broker,
        Closer,
        ClosingAgent,
        EscrowCompany,
        LoanOfficer,
        Originator,
        Other,
        Processor,
        SettlementAgent,
        TitleCompany,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, ClosingInstructionsConditionSatisfactionResponsiblePartyType value)
        {
            if (value == ClosingInstructionsConditionSatisfactionResponsiblePartyType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.Attorney:
                    _v = "Attorney"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.Broker:
                    _v = "Broker"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.Closer:
                    _v = "Closer"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.ClosingAgent:
                    _v = "ClosingAgent"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.EscrowCompany:
                    _v = "EscrowCompany"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.LoanOfficer:
                    _v = "LoanOfficer"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.Originator:
                    _v = "Originator"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.Other:
                    _v = "Other"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.Processor:
                    _v = "Processor"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.SettlementAgent:
                    _v = "SettlementAgent"; break;
                case ClosingInstructionsConditionSatisfactionResponsiblePartyType.TitleCompany:
                    _v = "TitleCompany"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected ClosingInstructionsConditionSatisfactionResponsiblePartyType ReadElementString_ClosingInstructionsConditionSatisfactionResponsiblePartyType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return ClosingInstructionsConditionSatisfactionResponsiblePartyType.Undefined;
            }
            switch (str)
            {
                case "Attorney":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.Attorney;
                case "Broker":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.Broker;
                case "Closer":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.Closer;
                case "ClosingAgent":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.ClosingAgent;
                case "EscrowCompany":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.EscrowCompany;
                case "LoanOfficer":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.LoanOfficer;
                case "Originator":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.Originator;
                case "Other":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.Other;
                case "Processor":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.Processor;
                case "SettlementAgent":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.SettlementAgent;
                case "TitleCompany":
                    return ClosingInstructionsConditionSatisfactionResponsiblePartyType.TitleCompany;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
