// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum ConstructionStatusType
    {
        Undefined = 0,
        Complete,
        Existing,
        Incomplete,
        Other,
        Proposed,
        SubjectToAlteration,
        SubjectToAlterationImprovementRepairAndRehabilitation,
        SubstantiallyRehabilitated,
        UnderConstruction,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, ConstructionStatusType value)
        {
            if (value == ConstructionStatusType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case ConstructionStatusType.Complete:
                    _v = "Complete"; break;
                case ConstructionStatusType.Existing:
                    _v = "Existing"; break;
                case ConstructionStatusType.Incomplete:
                    _v = "Incomplete"; break;
                case ConstructionStatusType.Other:
                    _v = "Other"; break;
                case ConstructionStatusType.Proposed:
                    _v = "Proposed"; break;
                case ConstructionStatusType.SubjectToAlteration:
                    _v = "SubjectToAlteration"; break;
                case ConstructionStatusType.SubjectToAlterationImprovementRepairAndRehabilitation:
                    _v = "SubjectToAlterationImprovementRepairAndRehabilitation"; break;
                case ConstructionStatusType.SubstantiallyRehabilitated:
                    _v = "SubstantiallyRehabilitated"; break;
                case ConstructionStatusType.UnderConstruction:
                    _v = "UnderConstruction"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected ConstructionStatusType ReadElementString_ConstructionStatusType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return ConstructionStatusType.Undefined;
            }
            switch (str)
            {
                case "Complete":
                    return ConstructionStatusType.Complete;
                case "Existing":
                    return ConstructionStatusType.Existing;
                case "Incomplete":
                    return ConstructionStatusType.Incomplete;
                case "Other":
                    return ConstructionStatusType.Other;
                case "Proposed":
                    return ConstructionStatusType.Proposed;
                case "SubjectToAlteration":
                    return ConstructionStatusType.SubjectToAlteration;
                case "SubjectToAlterationImprovementRepairAndRehabilitation":
                    return ConstructionStatusType.SubjectToAlterationImprovementRepairAndRehabilitation;
                case "SubstantiallyRehabilitated":
                    return ConstructionStatusType.SubstantiallyRehabilitated;
                case "UnderConstruction":
                    return ConstructionStatusType.UnderConstruction;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
