// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class CounselingConfirmation : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "COUNSELING_CONFIRMATION"; } }

        #region Property List
        public bool? CounselingConfirmationIndicator { get; set; }
        public CounselingConfirmationType CounselingConfirmationType { get; set; }
        public string CounselingConfirmationTypeOtherDescription { get; set; }
        public CounselingFormatType CounselingFormatType { get; set; }
        public string CounselingFormatTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CounselingConfirmationIndicator":
                    CounselingConfirmationIndicator = ReadElementString_Bool(reader); break;
                case "CounselingConfirmationType":
                    CounselingConfirmationType = ReadElementString_CounselingConfirmationType(reader); break;
                case "CounselingConfirmationTypeOtherDescription":
                    CounselingConfirmationTypeOtherDescription = ReadElementString(reader); break;
                case "CounselingFormatType":
                    CounselingFormatType = ReadElementString_CounselingFormatType(reader); break;
                case "CounselingFormatTypeOtherDescription":
                    CounselingFormatTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "CounselingConfirmationIndicator", CounselingConfirmationIndicator);
            WriteElementString(writer, "CounselingConfirmationType", CounselingConfirmationType);
            WriteElementString(writer, "CounselingConfirmationTypeOtherDescription", CounselingConfirmationTypeOtherDescription);
            WriteElementString(writer, "CounselingFormatType", CounselingFormatType);
            WriteElementString(writer, "CounselingFormatTypeOtherDescription", CounselingFormatTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
