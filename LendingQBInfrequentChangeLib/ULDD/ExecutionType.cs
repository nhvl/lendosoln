// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum ExecutionType
    {
        Undefined = 0,
        Cash,
        Guarantor,
        Multilender,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, ExecutionType value)
        {
            if (value == ExecutionType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case ExecutionType.Cash:
                    _v = "Cash"; break;
                case ExecutionType.Guarantor:
                    _v = "Guarantor"; break;
                case ExecutionType.Multilender:
                    _v = "Multilender"; break;
                case ExecutionType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected ExecutionType ReadElementString_ExecutionType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return ExecutionType.Undefined;
            }
            switch (str)
            {
                case "Cash":
                    return ExecutionType.Cash;
                case "Guarantor":
                    return ExecutionType.Guarantor;
                case "Multilender":
                    return ExecutionType.Multilender;
                case "Other":
                    return ExecutionType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
