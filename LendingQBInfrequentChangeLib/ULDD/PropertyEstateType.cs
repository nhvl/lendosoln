// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum PropertyEstateType
    {
        Undefined = 0,
        FeeSimple,
        Fractional,
        Leasehold,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, PropertyEstateType value)
        {
            if (value == PropertyEstateType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case PropertyEstateType.FeeSimple:
                    _v = "FeeSimple"; break;
                case PropertyEstateType.Fractional:
                    _v = "Fractional"; break;
                case PropertyEstateType.Leasehold:
                    _v = "Leasehold"; break;
                case PropertyEstateType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected PropertyEstateType ReadElementString_PropertyEstateType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return PropertyEstateType.Undefined;
            }
            switch (str)
            {
                case "FeeSimple":
                    return PropertyEstateType.FeeSimple;
                case "Fractional":
                    return PropertyEstateType.Fractional;
                case "Leasehold":
                    return PropertyEstateType.Leasehold;
                case "Other":
                    return PropertyEstateType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
