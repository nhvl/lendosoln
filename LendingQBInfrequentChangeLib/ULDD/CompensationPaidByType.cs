// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum CompensationPaidByType
    {
        Undefined = 0,
        Investor,
        Lender,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, CompensationPaidByType value)
        {
            if (value == CompensationPaidByType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case CompensationPaidByType.Investor:
                    _v = "Investor"; break;
                case CompensationPaidByType.Lender:
                    _v = "Lender"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected CompensationPaidByType ReadElementString_CompensationPaidByType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return CompensationPaidByType.Undefined;
            }
            switch (str)
            {
                case "Investor":
                    return CompensationPaidByType.Investor;
                case "Lender":
                    return CompensationPaidByType.Lender;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
