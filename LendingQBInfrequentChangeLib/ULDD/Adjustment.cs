// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Adjustment : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "ADJUSTMENT"; } }

        #region Property List
        private ConversionAdjustment m_conversionAdjustment;
        public ConversionAdjustment ConversionAdjustment
        {
            get
            {
                if (m_conversionAdjustment == null)
                {
                    m_conversionAdjustment = new ConversionAdjustment();
                }
                return m_conversionAdjustment;
            }
            set { m_conversionAdjustment = value; }
        }
        private InterestRateAdjustment m_interestRateAdjustment;
        public InterestRateAdjustment InterestRateAdjustment
        {
            get
            {
                if (m_interestRateAdjustment == null)
                {
                    m_interestRateAdjustment = new InterestRateAdjustment();
                }
                return m_interestRateAdjustment;
            }
            set { m_interestRateAdjustment = value; }
        }
        private PrincipalAndInterestPaymentAdjustment m_principalAndInterestPaymentAdjustment;
        public PrincipalAndInterestPaymentAdjustment PrincipalAndInterestPaymentAdjustment
        {
            get
            {
                if (m_principalAndInterestPaymentAdjustment == null)
                {
                    m_principalAndInterestPaymentAdjustment = new PrincipalAndInterestPaymentAdjustment();
                }
                return m_principalAndInterestPaymentAdjustment;
            }
            set { m_principalAndInterestPaymentAdjustment = value; }
        }
        private RateOrPaymentChangeOccurrences m_rateOrPaymentChangeOccurrences;
        public RateOrPaymentChangeOccurrences RateOrPaymentChangeOccurrences
        {
            get
            {
                if (m_rateOrPaymentChangeOccurrences == null)
                {
                    m_rateOrPaymentChangeOccurrences = new RateOrPaymentChangeOccurrences();
                }
                return m_rateOrPaymentChangeOccurrences;
            }
            set { m_rateOrPaymentChangeOccurrences = value; }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CONVERSION_ADJUSTMENT":
                    ReadElement(reader, ConversionAdjustment); break;
                case "INTEREST_RATE_ADJUSTMENT":
                    ReadElement(reader, InterestRateAdjustment); break;
                case "PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT":
                    ReadElement(reader, PrincipalAndInterestPaymentAdjustment); break;
                case "RATE_OR_PAYMENT_CHANGE_OCCURRENCES":
                    ReadElement(reader, RateOrPaymentChangeOccurrences); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_conversionAdjustment);
            WriteElement(writer, m_interestRateAdjustment);
            WriteElement(writer, m_principalAndInterestPaymentAdjustment);
            WriteElement(writer, m_rateOrPaymentChangeOccurrences);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
