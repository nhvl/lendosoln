// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum RESPAFeeType
    {
        Undefined = 0,
        _203KArchitecturalAndEngineeringFee,
        _203KConsultantFee,
        _203KDiscountOnRepairs,
        _203KInspectionFee,
        _203KPermits,
        _203KSupplementalOriginationFee,
        _203KTitleUpdate,
        AbstractOrTitleSearchFee,
        AmortizationFee,
        ApplicationFee,
        AppraisalFee,
        AppraisalDeskReviewFee,
        AppraisalFieldReviewFee,
        AssignmentFee,
        AssignmentRecordingFee,
        AssumptionFee,
        AttorneyFee,
        BankruptcyMonitoringFee,
        BondFee,
        BondReviewFee,
        CertificationFee,
        CityCountyDeedTaxStampFee,
        CityCountyMortgageTaxStampFee,
        CLOAccessFee,
        ClosingProtectionLetterFee,
        CommitmentFee,
        CopyOrFaxFee,
        CourierFee,
        CreditReportFee,
        DeedRecordingFee,
        DocumentPreparationFee,
        DocumentaryStampFee,
        ElectronicDocumentDeliveryFee,
        EscrowServiceFee,
        EscrowWaiverFee,
        FloodCertification,
        GeneralCounselFee,
        InspectionFee,
        LoanDiscountPoints,
        LoanOriginationFee,
        MERSRegistrationFee,
        ModificationFee,
        MortgageBrokerFee,
        MortgageRecordingFee,
        MunicipalLienCertificateFee,
        MunicipalLienCertificateRecordingFee,
        NewLoanAdministrationFee,
        NotaryFee,
        Other,
        PayoffRequestFee,
        PestInspectionFee,
        ProcessingFee,
        RealEstateCommission,
        RedrawFee,
        ReinspectionFee,
        ReleaseRecordingFee,
        RuralHousingFee,
        SettlementOrClosingFee,
        SigningAgentFee,
        StateDeedTaxStampFee,
        StateMortgageTaxStampFee,
        SubordinationFee,
        SurveyFee,
        TaxRelatedServiceFee,
        TitleEndorsementFee,
        TitleExaminationFee,
        TitleInsuranceBinderFee,
        TitleInsuranceFee,
        UnderwritingFee,
        WireTransferFee,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, RESPAFeeType value)
        {
            if (value == RESPAFeeType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case RESPAFeeType._203KArchitecturalAndEngineeringFee:
                    _v = "203KArchitecturalAndEngineeringFee"; break;
                case RESPAFeeType._203KConsultantFee:
                    _v = "203KConsultantFee"; break;
                case RESPAFeeType._203KDiscountOnRepairs:
                    _v = "203KDiscountOnRepairs"; break;
                case RESPAFeeType._203KInspectionFee:
                    _v = "203KInspectionFee"; break;
                case RESPAFeeType._203KPermits:
                    _v = "203KPermits"; break;
                case RESPAFeeType._203KSupplementalOriginationFee:
                    _v = "203KSupplementalOriginationFee"; break;
                case RESPAFeeType._203KTitleUpdate:
                    _v = "203KTitleUpdate"; break;
                case RESPAFeeType.AbstractOrTitleSearchFee:
                    _v = "AbstractOrTitleSearchFee"; break;
                case RESPAFeeType.AmortizationFee:
                    _v = "AmortizationFee"; break;
                case RESPAFeeType.ApplicationFee:
                    _v = "ApplicationFee"; break;
                case RESPAFeeType.AppraisalFee:
                    _v = "AppraisalFee"; break;
                case RESPAFeeType.AppraisalDeskReviewFee:
                    _v = "AppraisalDeskReviewFee"; break;
                case RESPAFeeType.AppraisalFieldReviewFee:
                    _v = "AppraisalFieldReviewFee"; break;
                case RESPAFeeType.AssignmentFee:
                    _v = "AssignmentFee"; break;
                case RESPAFeeType.AssignmentRecordingFee:
                    _v = "AssignmentRecordingFee"; break;
                case RESPAFeeType.AssumptionFee:
                    _v = "AssumptionFee"; break;
                case RESPAFeeType.AttorneyFee:
                    _v = "AttorneyFee"; break;
                case RESPAFeeType.BankruptcyMonitoringFee:
                    _v = "BankruptcyMonitoringFee"; break;
                case RESPAFeeType.BondFee:
                    _v = "BondFee"; break;
                case RESPAFeeType.BondReviewFee:
                    _v = "BondReviewFee"; break;
                case RESPAFeeType.CertificationFee:
                    _v = "CertificationFee"; break;
                case RESPAFeeType.CityCountyDeedTaxStampFee:
                    _v = "CityCountyDeedTaxStampFee"; break;
                case RESPAFeeType.CityCountyMortgageTaxStampFee:
                    _v = "CityCountyMortgageTaxStampFee"; break;
                case RESPAFeeType.CLOAccessFee:
                    _v = "CLOAccessFee"; break;
                case RESPAFeeType.ClosingProtectionLetterFee:
                    _v = "ClosingProtectionLetterFee"; break;
                case RESPAFeeType.CommitmentFee:
                    _v = "CommitmentFee"; break;
                case RESPAFeeType.CopyOrFaxFee:
                    _v = "CopyOrFaxFee"; break;
                case RESPAFeeType.CourierFee:
                    _v = "CourierFee"; break;
                case RESPAFeeType.CreditReportFee:
                    _v = "CreditReportFee"; break;
                case RESPAFeeType.DeedRecordingFee:
                    _v = "DeedRecordingFee"; break;
                case RESPAFeeType.DocumentPreparationFee:
                    _v = "DocumentPreparationFee"; break;
                case RESPAFeeType.DocumentaryStampFee:
                    _v = "DocumentaryStampFee"; break;
                case RESPAFeeType.ElectronicDocumentDeliveryFee:
                    _v = "ElectronicDocumentDeliveryFee"; break;
                case RESPAFeeType.EscrowServiceFee:
                    _v = "EscrowServiceFee"; break;
                case RESPAFeeType.EscrowWaiverFee:
                    _v = "EscrowWaiverFee"; break;
                case RESPAFeeType.FloodCertification:
                    _v = "FloodCertification"; break;
                case RESPAFeeType.GeneralCounselFee:
                    _v = "GeneralCounselFee"; break;
                case RESPAFeeType.InspectionFee:
                    _v = "InspectionFee"; break;
                case RESPAFeeType.LoanDiscountPoints:
                    _v = "LoanDiscountPoints"; break;
                case RESPAFeeType.LoanOriginationFee:
                    _v = "LoanOriginationFee"; break;
                case RESPAFeeType.MERSRegistrationFee:
                    _v = "MERSRegistrationFee"; break;
                case RESPAFeeType.ModificationFee:
                    _v = "ModificationFee"; break;
                case RESPAFeeType.MortgageBrokerFee:
                    _v = "MortgageBrokerFee"; break;
                case RESPAFeeType.MortgageRecordingFee:
                    _v = "MortgageRecordingFee"; break;
                case RESPAFeeType.MunicipalLienCertificateFee:
                    _v = "MunicipalLienCertificateFee"; break;
                case RESPAFeeType.MunicipalLienCertificateRecordingFee:
                    _v = "MunicipalLienCertificateRecordingFee"; break;
                case RESPAFeeType.NewLoanAdministrationFee:
                    _v = "NewLoanAdministrationFee"; break;
                case RESPAFeeType.NotaryFee:
                    _v = "NotaryFee"; break;
                case RESPAFeeType.Other:
                    _v = "Other"; break;
                case RESPAFeeType.PayoffRequestFee:
                    _v = "PayoffRequestFee"; break;
                case RESPAFeeType.PestInspectionFee:
                    _v = "PestInspectionFee"; break;
                case RESPAFeeType.ProcessingFee:
                    _v = "ProcessingFee"; break;
                case RESPAFeeType.RealEstateCommission:
                    _v = "RealEstateCommission"; break;
                case RESPAFeeType.RedrawFee:
                    _v = "RedrawFee"; break;
                case RESPAFeeType.ReinspectionFee:
                    _v = "ReinspectionFee"; break;
                case RESPAFeeType.ReleaseRecordingFee:
                    _v = "ReleaseRecordingFee"; break;
                case RESPAFeeType.RuralHousingFee:
                    _v = "RuralHousingFee"; break;
                case RESPAFeeType.SettlementOrClosingFee:
                    _v = "SettlementOrClosingFee"; break;
                case RESPAFeeType.SigningAgentFee:
                    _v = "SigningAgentFee"; break;
                case RESPAFeeType.StateDeedTaxStampFee:
                    _v = "StateDeedTaxStampFee"; break;
                case RESPAFeeType.StateMortgageTaxStampFee:
                    _v = "StateMortgageTaxStampFee"; break;
                case RESPAFeeType.SubordinationFee:
                    _v = "SubordinationFee"; break;
                case RESPAFeeType.SurveyFee:
                    _v = "SurveyFee"; break;
                case RESPAFeeType.TaxRelatedServiceFee:
                    _v = "TaxRelatedServiceFee"; break;
                case RESPAFeeType.TitleEndorsementFee:
                    _v = "TitleEndorsementFee"; break;
                case RESPAFeeType.TitleExaminationFee:
                    _v = "TitleExaminationFee"; break;
                case RESPAFeeType.TitleInsuranceBinderFee:
                    _v = "TitleInsuranceBinderFee"; break;
                case RESPAFeeType.TitleInsuranceFee:
                    _v = "TitleInsuranceFee"; break;
                case RESPAFeeType.UnderwritingFee:
                    _v = "UnderwritingFee"; break;
                case RESPAFeeType.WireTransferFee:
                    _v = "WireTransferFee"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected RESPAFeeType ReadElementString_RESPAFeeType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return RESPAFeeType.Undefined;
            }
            switch (str)
            {
                case "203KArchitecturalAndEngineeringFee":
                    return RESPAFeeType._203KArchitecturalAndEngineeringFee;
                case "203KConsultantFee":
                    return RESPAFeeType._203KConsultantFee;
                case "203KDiscountOnRepairs":
                    return RESPAFeeType._203KDiscountOnRepairs;
                case "203KInspectionFee":
                    return RESPAFeeType._203KInspectionFee;
                case "203KPermits":
                    return RESPAFeeType._203KPermits;
                case "203KSupplementalOriginationFee":
                    return RESPAFeeType._203KSupplementalOriginationFee;
                case "203KTitleUpdate":
                    return RESPAFeeType._203KTitleUpdate;
                case "AbstractOrTitleSearchFee":
                    return RESPAFeeType.AbstractOrTitleSearchFee;
                case "AmortizationFee":
                    return RESPAFeeType.AmortizationFee;
                case "ApplicationFee":
                    return RESPAFeeType.ApplicationFee;
                case "AppraisalFee":
                    return RESPAFeeType.AppraisalFee;
                case "AppraisalDeskReviewFee":
                    return RESPAFeeType.AppraisalDeskReviewFee;
                case "AppraisalFieldReviewFee":
                    return RESPAFeeType.AppraisalFieldReviewFee;
                case "AssignmentFee":
                    return RESPAFeeType.AssignmentFee;
                case "AssignmentRecordingFee":
                    return RESPAFeeType.AssignmentRecordingFee;
                case "AssumptionFee":
                    return RESPAFeeType.AssumptionFee;
                case "AttorneyFee":
                    return RESPAFeeType.AttorneyFee;
                case "BankruptcyMonitoringFee":
                    return RESPAFeeType.BankruptcyMonitoringFee;
                case "BondFee":
                    return RESPAFeeType.BondFee;
                case "BondReviewFee":
                    return RESPAFeeType.BondReviewFee;
                case "CertificationFee":
                    return RESPAFeeType.CertificationFee;
                case "CityCountyDeedTaxStampFee":
                    return RESPAFeeType.CityCountyDeedTaxStampFee;
                case "CityCountyMortgageTaxStampFee":
                    return RESPAFeeType.CityCountyMortgageTaxStampFee;
                case "CLOAccessFee":
                    return RESPAFeeType.CLOAccessFee;
                case "ClosingProtectionLetterFee":
                    return RESPAFeeType.ClosingProtectionLetterFee;
                case "CommitmentFee":
                    return RESPAFeeType.CommitmentFee;
                case "CopyOrFaxFee":
                    return RESPAFeeType.CopyOrFaxFee;
                case "CourierFee":
                    return RESPAFeeType.CourierFee;
                case "CreditReportFee":
                    return RESPAFeeType.CreditReportFee;
                case "DeedRecordingFee":
                    return RESPAFeeType.DeedRecordingFee;
                case "DocumentPreparationFee":
                    return RESPAFeeType.DocumentPreparationFee;
                case "DocumentaryStampFee":
                    return RESPAFeeType.DocumentaryStampFee;
                case "ElectronicDocumentDeliveryFee":
                    return RESPAFeeType.ElectronicDocumentDeliveryFee;
                case "EscrowServiceFee":
                    return RESPAFeeType.EscrowServiceFee;
                case "EscrowWaiverFee":
                    return RESPAFeeType.EscrowWaiverFee;
                case "FloodCertification":
                    return RESPAFeeType.FloodCertification;
                case "GeneralCounselFee":
                    return RESPAFeeType.GeneralCounselFee;
                case "InspectionFee":
                    return RESPAFeeType.InspectionFee;
                case "LoanDiscountPoints":
                    return RESPAFeeType.LoanDiscountPoints;
                case "LoanOriginationFee":
                    return RESPAFeeType.LoanOriginationFee;
                case "MERSRegistrationFee":
                    return RESPAFeeType.MERSRegistrationFee;
                case "ModificationFee":
                    return RESPAFeeType.ModificationFee;
                case "MortgageBrokerFee":
                    return RESPAFeeType.MortgageBrokerFee;
                case "MortgageRecordingFee":
                    return RESPAFeeType.MortgageRecordingFee;
                case "MunicipalLienCertificateFee":
                    return RESPAFeeType.MunicipalLienCertificateFee;
                case "MunicipalLienCertificateRecordingFee":
                    return RESPAFeeType.MunicipalLienCertificateRecordingFee;
                case "NewLoanAdministrationFee":
                    return RESPAFeeType.NewLoanAdministrationFee;
                case "NotaryFee":
                    return RESPAFeeType.NotaryFee;
                case "Other":
                    return RESPAFeeType.Other;
                case "PayoffRequestFee":
                    return RESPAFeeType.PayoffRequestFee;
                case "PestInspectionFee":
                    return RESPAFeeType.PestInspectionFee;
                case "ProcessingFee":
                    return RESPAFeeType.ProcessingFee;
                case "RealEstateCommission":
                    return RESPAFeeType.RealEstateCommission;
                case "RedrawFee":
                    return RESPAFeeType.RedrawFee;
                case "ReinspectionFee":
                    return RESPAFeeType.ReinspectionFee;
                case "ReleaseRecordingFee":
                    return RESPAFeeType.ReleaseRecordingFee;
                case "RuralHousingFee":
                    return RESPAFeeType.RuralHousingFee;
                case "SettlementOrClosingFee":
                    return RESPAFeeType.SettlementOrClosingFee;
                case "SigningAgentFee":
                    return RESPAFeeType.SigningAgentFee;
                case "StateDeedTaxStampFee":
                    return RESPAFeeType.StateDeedTaxStampFee;
                case "StateMortgageTaxStampFee":
                    return RESPAFeeType.StateMortgageTaxStampFee;
                case "SubordinationFee":
                    return RESPAFeeType.SubordinationFee;
                case "SurveyFee":
                    return RESPAFeeType.SurveyFee;
                case "TaxRelatedServiceFee":
                    return RESPAFeeType.TaxRelatedServiceFee;
                case "TitleEndorsementFee":
                    return RESPAFeeType.TitleEndorsementFee;
                case "TitleExaminationFee":
                    return RESPAFeeType.TitleExaminationFee;
                case "TitleInsuranceBinderFee":
                    return RESPAFeeType.TitleInsuranceBinderFee;
                case "TitleInsuranceFee":
                    return RESPAFeeType.TitleInsuranceFee;
                case "UnderwritingFee":
                    return RESPAFeeType.UnderwritingFee;
                case "WireTransferFee":
                    return RESPAFeeType.WireTransferFee;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
