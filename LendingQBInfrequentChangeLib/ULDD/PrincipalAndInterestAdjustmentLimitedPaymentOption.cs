// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class PrincipalAndInterestAdjustmentLimitedPaymentOption : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION"; } }

        #region Property List
        public string LimitedPrincipalAndInterestPaymentActivationAmount { get; set; }
        public bool? LimitedPrincipalAndInterestPaymentActivationIndicator { get; set; }
        public string LimitedPrincipalAndInterestPaymentActivationPercent { get; set; }
        public string LimitedPrincipalAndInterestPaymentEffectiveDate { get; set; }
        public string LimitedPrincipalAndInterestPaymentOptionDescription { get; set; }
        public bool? LimitedPrincipalAndInterestPaymentPullTheNoteIndicator { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "LimitedPrincipalAndInterestPaymentActivationAmount":
                    LimitedPrincipalAndInterestPaymentActivationAmount = ReadElementString(reader); break;
                case "LimitedPrincipalAndInterestPaymentActivationIndicator":
                    LimitedPrincipalAndInterestPaymentActivationIndicator = ReadElementString_Bool(reader); break;
                case "LimitedPrincipalAndInterestPaymentActivationPercent":
                    LimitedPrincipalAndInterestPaymentActivationPercent = ReadElementString(reader); break;
                case "LimitedPrincipalAndInterestPaymentEffectiveDate":
                    LimitedPrincipalAndInterestPaymentEffectiveDate = ReadElementString(reader); break;
                case "LimitedPrincipalAndInterestPaymentOptionDescription":
                    LimitedPrincipalAndInterestPaymentOptionDescription = ReadElementString(reader); break;
                case "LimitedPrincipalAndInterestPaymentPullTheNoteIndicator":
                    LimitedPrincipalAndInterestPaymentPullTheNoteIndicator = ReadElementString_Bool(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "LimitedPrincipalAndInterestPaymentActivationAmount", LimitedPrincipalAndInterestPaymentActivationAmount);
            WriteElementString(writer, "LimitedPrincipalAndInterestPaymentActivationIndicator", LimitedPrincipalAndInterestPaymentActivationIndicator);
            WriteElementString(writer, "LimitedPrincipalAndInterestPaymentActivationPercent", LimitedPrincipalAndInterestPaymentActivationPercent);
            WriteElementString(writer, "LimitedPrincipalAndInterestPaymentEffectiveDate", LimitedPrincipalAndInterestPaymentEffectiveDate);
            WriteElementString(writer, "LimitedPrincipalAndInterestPaymentOptionDescription", LimitedPrincipalAndInterestPaymentOptionDescription);
            WriteElementString(writer, "LimitedPrincipalAndInterestPaymentPullTheNoteIndicator", LimitedPrincipalAndInterestPaymentPullTheNoteIndicator);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
