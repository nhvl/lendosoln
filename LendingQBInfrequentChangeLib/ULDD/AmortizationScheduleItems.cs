// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class AmortizationScheduleItems : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "AMORTIZATION_SCHEDULE_ITEMS"; } }

        #region Property List
        private List<AmortizationScheduleItem> m_amortizationScheduleItemList;
        public List<AmortizationScheduleItem> AmortizationScheduleItemList
        {
            get
            {
                if (m_amortizationScheduleItemList == null)
                {
                    m_amortizationScheduleItemList = new List<AmortizationScheduleItem>();
                }
                return m_amortizationScheduleItemList;
            }
        }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "AMORTIZATION_SCHEDULE_ITEM":
                    ReadElement(reader, AmortizationScheduleItemList); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_amortizationScheduleItemList);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
