using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class LoanIdentifierExtension : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "EXTENSION"; } }

        #region Property List
        /// <summary>
        /// This is the value that will show up in the LOAN_IDENTIFIER/EXTENSION/OTHER/LOAN_IDENTIFIER_EXTENSION/LoanIdentifier element.
        /// </summary>
        public string OtherLoanIdentifier { get; set; }

        /// <summary>
        /// Will show up in LOAN_IDENTIFIER/EXTENSION/OTHER/LOAN_IDENTIFIER_EXTENSION/LoanIdentifierType.
        /// </summary>
        public string OtherLoanIdentifierType { get; set; }
        #endregion

        #region Read/Write XML

        protected override void ReadElement(XmlReader reader)
        {
            if (reader.Name == "OTHER")
            {
                while (reader.NodeType != XmlNodeType.Element || reader.LocalName != XmlElementName)
                {
                    reader.Read();
                    if (reader.EOF)
                    {
                        return;
                    }
                }

                if (reader.Name == "LOAN_IDENTIFIER_EXTENSION")
                {
                    while (reader.NodeType != XmlNodeType.Element || reader.LocalName != XmlElementName)
                    {
                        reader.Read();
                        if (reader.EOF)
                        {
                            return;
                        }
                    }

                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Text)
                        {
                            ReadTextContent(reader);
                            continue;
                        }
                        if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == XmlElementName)
                        {
                            return;
                        }
                        if (reader.NodeType != XmlNodeType.Element)
                        {
                            continue;
                        }

                        ReadProperty(reader);
                    }
                }
            }
        }

        private void ReadProperty(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "LoanIdentifier":
                    OtherLoanIdentifier = ReadElementString(reader);
                    break;
                case "LoanIdentifierType":
                    OtherLoanIdentifierType = ReadElementString(reader);
                    break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            writer.WriteStartElement("OTHER");
            writer.WriteStartElement("LOAN_IDENTIFIER_EXTENSION");
            WriteElementString(writer, "LoanIdentifier", this.OtherLoanIdentifier);
            WriteElementString(writer, "LoanIdentifierType", this.OtherLoanIdentifierType);
            writer.WriteEndElement(); // LOAN_IDENTIFIER_EXTENSION
            writer.WriteEndElement(); // OTHER
        }
        #endregion
    }
}
