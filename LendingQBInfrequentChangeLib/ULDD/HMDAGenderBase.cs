﻿namespace ULDD
{
    public enum HMDAGenderBase
    {
        LeaveBlank = 0,
        ApplicantHasSelectedBothMaleAndFemale,
        Female,
        InformationNotProvidedUnknown,
        Male,
        NotApplicable,
    }
}
