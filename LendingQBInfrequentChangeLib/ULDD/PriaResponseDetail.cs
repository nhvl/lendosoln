// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class PriaResponseDetail : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "PRIA_RESPONSE_DETAIL"; } }

        #region Property List
        public bool? PRIAResponseRelatedDocumentsIndicator { get; set; }
        public PRIAResponseType PRIAResponseType { get; set; }
        public string PRIAResponseTypeOtherDescription { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "PRIAResponseRelatedDocumentsIndicator":
                    PRIAResponseRelatedDocumentsIndicator = ReadElementString_Bool(reader); break;
                case "PRIAResponseType":
                    PRIAResponseType = ReadElementString_PRIAResponseType(reader); break;
                case "PRIAResponseTypeOtherDescription":
                    PRIAResponseTypeOtherDescription = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "PRIAResponseRelatedDocumentsIndicator", PRIAResponseRelatedDocumentsIndicator);
            WriteElementString(writer, "PRIAResponseType", PRIAResponseType);
            WriteElementString(writer, "PRIAResponseTypeOtherDescription", PRIAResponseTypeOtherDescription);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
