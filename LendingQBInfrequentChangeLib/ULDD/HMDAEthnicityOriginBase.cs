﻿namespace ULDD
{
    public enum HMDAEthnicityOriginBase
    {
        LeaveBlank = 0,
        Cuban,
        Mexican,
        Other,
        PuertoRican,
    }
}
