// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class Ach : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "ACH"; } }

        #region Property List
        public string ACH_ABARoutingAndTransitIdentifier { get; set; }
        public ACHAccountType ACHAccountType { get; set; }
        public string ACHAdditionalEscrowAmount { get; set; }
        public string ACHAdditionalPrincipalAmount { get; set; }
        public string ACHBankAccountIdentifier { get; set; }
        public string ACHDraftAfterDueDateDayCount { get; set; }
        public ACHDraftFrequencyType ACHDraftFrequencyType { get; set; }
        public string ACHDraftingPartyName { get; set; }
        public string ACHInstitutionTelegraphicAbbreviationName { get; set; }
        public string ACHPendingDraftEffectiveDate { get; set; }
        public string ACHReceiverSubaccountName { get; set; }
        public ACHType ACHType { get; set; }
        public string ACHWireAmount { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        #endregion

        #region Read/Write XML
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "ACH_ABARoutingAndTransitIdentifier":
                    ACH_ABARoutingAndTransitIdentifier = ReadElementString(reader); break;
                case "ACHAccountType":
                    ACHAccountType = ReadElementString_ACHAccountType(reader); break;
                case "ACHAdditionalEscrowAmount":
                    ACHAdditionalEscrowAmount = ReadElementString(reader); break;
                case "ACHAdditionalPrincipalAmount":
                    ACHAdditionalPrincipalAmount = ReadElementString(reader); break;
                case "ACHBankAccountIdentifier":
                    ACHBankAccountIdentifier = ReadElementString(reader); break;
                case "ACHDraftAfterDueDateDayCount":
                    ACHDraftAfterDueDateDayCount = ReadElementString(reader); break;
                case "ACHDraftFrequencyType":
                    ACHDraftFrequencyType = ReadElementString_ACHDraftFrequencyType(reader); break;
                case "ACHDraftingPartyName":
                    ACHDraftingPartyName = ReadElementString(reader); break;
                case "ACHInstitutionTelegraphicAbbreviationName":
                    ACHInstitutionTelegraphicAbbreviationName = ReadElementString(reader); break;
                case "ACHPendingDraftEffectiveDate":
                    ACHPendingDraftEffectiveDate = ReadElementString(reader); break;
                case "ACHReceiverSubaccountName":
                    ACHReceiverSubaccountName = ReadElementString(reader); break;
                case "ACHType":
                    ACHType = ReadElementString_ACHType(reader); break;
                case "ACHWireAmount":
                    ACHWireAmount = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElementString(writer, "ACH_ABARoutingAndTransitIdentifier", ACH_ABARoutingAndTransitIdentifier);
            WriteElementString(writer, "ACHAccountType", ACHAccountType);
            WriteElementString(writer, "ACHAdditionalEscrowAmount", ACHAdditionalEscrowAmount);
            WriteElementString(writer, "ACHAdditionalPrincipalAmount", ACHAdditionalPrincipalAmount);
            WriteElementString(writer, "ACHBankAccountIdentifier", ACHBankAccountIdentifier);
            WriteElementString(writer, "ACHDraftAfterDueDateDayCount", ACHDraftAfterDueDateDayCount);
            WriteElementString(writer, "ACHDraftFrequencyType", ACHDraftFrequencyType);
            WriteElementString(writer, "ACHDraftingPartyName", ACHDraftingPartyName);
            WriteElementString(writer, "ACHInstitutionTelegraphicAbbreviationName", ACHInstitutionTelegraphicAbbreviationName);
            WriteElementString(writer, "ACHPendingDraftEffectiveDate", ACHPendingDraftEffectiveDate);
            WriteElementString(writer, "ACHReceiverSubaccountName", ACHReceiverSubaccountName);
            WriteElementString(writer, "ACHType", ACHType);
            WriteElementString(writer, "ACHWireAmount", ACHWireAmount);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
