// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum VerificationMethodType
    {
        Undefined = 0,
        Verbal,
        Written,
        Other,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, VerificationMethodType value)
        {
            if (value == VerificationMethodType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case VerificationMethodType.Verbal:
                    _v = "Verbal"; break;
                case VerificationMethodType.Written:
                    _v = "Written"; break;
                case VerificationMethodType.Other:
                    _v = "Other"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected VerificationMethodType ReadElementString_VerificationMethodType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return VerificationMethodType.Undefined;
            }
            switch (str)
            {
                case "Verbal":
                    return VerificationMethodType.Verbal;
                case "Written":
                    return VerificationMethodType.Written;
                case "Other":
                    return VerificationMethodType.Other;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
