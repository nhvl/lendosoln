// Generated By CodeMonkey 8/27/2012 12:58:43 AM

using System;
using System.Collections.Generic;
using System.Xml;

namespace ULDD
{
    public class LoanStatus : AbstractXmlSerializable
    {
        public override string XmlElementName { get { return "LOAN_STATUS"; } }

        #region Property List
        public string LoanStatusDate { get; set; }
        public string LoanStatusIdentifier { get; set; }
        private Extension m_extension;
        public Extension Extension
        {
            get
            {
                if (m_extension == null)
                {
                    m_extension = new Extension();
                }
                return m_extension;
            }
            set { m_extension = value; }
        }
        public string SequenceNumber { get; set; }
        #endregion

        #region Read/Write XML
        protected override void ReadAttributes(XmlReader reader)
        {
            SequenceNumber = ReadAttribute(reader, "SequenceNumber");
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "LoanStatusDate":
                    LoanStatusDate = ReadElementString(reader); break;
                case "LoanStatusIdentifier":
                    LoanStatusIdentifier = ReadElementString(reader); break;
                case "EXTENSION":
                    ReadElement(reader, Extension); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "SequenceNumber", SequenceNumber);
            WriteElementString(writer, "LoanStatusDate", LoanStatusDate);
            WriteElementString(writer, "LoanStatusIdentifier", LoanStatusIdentifier);
            WriteElement(writer, m_extension);
        }
        #endregion
    }
}
