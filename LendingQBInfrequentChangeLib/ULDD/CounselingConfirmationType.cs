// Generated By CodeMonkey 8/27/2012 12:58:42 AM

using System;
using System.Xml;

namespace ULDD
{
    public enum CounselingConfirmationType
    {
        Undefined = 0,
        AmericanHomeownerEducationInstituteApprovedCounseling,
        GovernmentAgency,
        HUDApprovedCounselingAgency,
        LenderTrainedCounseling,
        NoBorrowerCounseling,
        Other,
        ThirdPartyCounseling,
        Unknown,
    }

    public partial class AbstractXmlSerializable
    {
        protected void WriteElementString(XmlWriter writer, string name, CounselingConfirmationType value)
        {
            if (value == CounselingConfirmationType.Undefined)
            {
                return;
            }
            string _v = string.Empty;
            switch (value)
            {
                case CounselingConfirmationType.AmericanHomeownerEducationInstituteApprovedCounseling:
                    _v = "AmericanHomeownerEducationInstituteApprovedCounseling"; break;
                case CounselingConfirmationType.GovernmentAgency:
                    _v = "GovernmentAgency"; break;
                case CounselingConfirmationType.HUDApprovedCounselingAgency:
                    _v = "HUDApprovedCounselingAgency"; break;
                case CounselingConfirmationType.LenderTrainedCounseling:
                    _v = "LenderTrainedCounseling"; break;
                case CounselingConfirmationType.NoBorrowerCounseling:
                    _v = "NoBorrowerCounseling"; break;
                case CounselingConfirmationType.Other:
                    _v = "Other"; break;
                case CounselingConfirmationType.ThirdPartyCounseling:
                    _v = "ThirdPartyCounseling"; break;
                case CounselingConfirmationType.Unknown:
                    _v = "Unknown"; break;
                default:
                    throw new Exception("Unhandled " + value);
            }
            WriteElementString(writer, name, _v);
        }

        protected CounselingConfirmationType ReadElementString_CounselingConfirmationType(XmlReader reader)
        {
            string str = reader.ReadString();
            if (string.IsNullOrEmpty(str))
            {
                return CounselingConfirmationType.Undefined;
            }
            switch (str)
            {
                case "AmericanHomeownerEducationInstituteApprovedCounseling":
                    return CounselingConfirmationType.AmericanHomeownerEducationInstituteApprovedCounseling;
                case "GovernmentAgency":
                    return CounselingConfirmationType.GovernmentAgency;
                case "HUDApprovedCounselingAgency":
                    return CounselingConfirmationType.HUDApprovedCounselingAgency;
                case "LenderTrainedCounseling":
                    return CounselingConfirmationType.LenderTrainedCounseling;
                case "NoBorrowerCounseling":
                    return CounselingConfirmationType.NoBorrowerCounseling;
                case "Other":
                    return CounselingConfirmationType.Other;
                case "ThirdPartyCounseling":
                    return CounselingConfirmationType.ThirdPartyCounseling;
                case "Unknown":
                    return CounselingConfirmationType.Unknown;
                default:
                    throw new Exception("Unhandled " + str);
            }
        }
    }
}
