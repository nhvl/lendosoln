using System;
using System.Runtime.Serialization;
using CommonLib;

namespace MeridianLink.CCGateway
{
	/// <summary>
	/// Superclass for credit card gateways, which handle a single credit card transaction.
	/// </summary>
	[Serializable]
	abstract public class CreditCardGateway2
	{
		/// <summary>
		/// Tells what happened during the last call to a gateway.
		/// </summary>
		/// <remarks>
		/// None				State set at beginning of each call to gateway
		/// Success				Call was successful
		/// BadData				Call failed because of bad input data
		/// BadComm				Call failed because of a communication error within the gateway
		/// Failure				Call failed within the gateway
		/// Timeout				Post to gateway timed out
		/// UnknownReturn		Call failed to gateway for unknown reason
		/// UnknownException	An exception was thrown
		/// </remarks>
		public enum ResultType {None = 0, Success, BadData, BadComm, Failure, Timeout, UnknownReturn, UnknownException}

		/// <summary>
		/// The state of the transaction, either deduced from last call or gotten directly be querying gateway.
		/// </summary>
		public enum TransactionState {None = 0, Authorized, Settled, Cancelled, Declined, ReFunded}

		/// <summary>
		/// The last method called on the gateway.
		/// </summary>
		public enum CallType {None = 0, Authorize, ReAuthorize, Settle, Cancel, ReFund}

		public const double Version = 2.0;
		private const string s_sEncryptKey = "7&4d0nhYTrg.n,_+49qZuharEVcz"; // WARNING: changing will make earlier versions unreadable

		[NonSerialized] private bool m_bTrace = false;
		protected double m_dAmount;

#if DEBUG
		// AD - Added debug-only code that permits testing different scenarios without actually calling Skipjack
		[NonSerialized] public bool DoFakeResult = false;
		[NonSerialized] public ResultType fakeAuthResultType = ResultType.Success;
		[NonSerialized] public ResultType fakeReAuthResultType = ResultType.Success;
		[NonSerialized] public ResultType fakeDeleteResultType = ResultType.Success;
		[NonSerialized] public ResultType fakeSettleResultType = ResultType.Success;
		[NonSerialized] public ResultType fakeRefundResultType = ResultType.Success;
#endif

		// Input data
		private string m_sType;
		private string m_sLogin1;
		private string m_sLogin2;
		private string m_sPassword;
		private bool m_bIsTest;
		private CommonLib.CreditCardHolder m_card;
		private string m_sEmail; // card holder or admin email
		private string m_sPhone; // ship-to or admin phone
		private string m_sShortDesc;
		private int m_nMLTransactionID; // id in ml system
		private string m_sMLTransactionID; // id in ml system (with perhaps some decoration)

		// Return data set by the derived classes
		protected ResultType m_result = ResultType.None;
		protected TransactionState m_state = TransactionState.None;
		protected CallType m_lastCall = CallType.None;
		protected string m_sProviderTransactionID;
		protected string m_sAuthCode;
		protected string m_sResponseText;
		protected string m_sErrorInfo;
		protected string m_sReturnInfo;

		private CreditCardGateway2(){} // prevent subclasses from being created directly

		static CreditCardGateway2()
		{
		}

		protected CreditCardGateway2(string type)
		{
			this.m_sType = type;
		}

		protected CreditCardGateway2(SerializationInfo info, StreamingContext context)
		{
			double dVersion = info.GetDouble("gatewayVersion");

			if (dVersion < 2.0)
			{
				CommonLib.CreditCard card = (CommonLib.CreditCard)info.GetValue("card", typeof(CommonLib.CreditCard));
				this.m_card = new CommonLib.CreditCardHolder(card, null);
			}
			else
			{
				this.m_card = (CommonLib.CreditCardHolder)info.GetValue("card", typeof(CommonLib.CreditCardHolder));
			}

			this.m_dAmount = info.GetDouble("amount");
			this.m_sType = info.GetString("type");
			this.m_sLogin1 = Decrypt(info.GetString("login1"));
			this.m_sLogin2 = Decrypt(info.GetString("login2"));
			this.m_sPassword = Decrypt(info.GetString("password"));
			this.m_bIsTest = info.GetBoolean("isTest");
			this.m_sEmail = Decrypt(info.GetString("email"));
			this.m_sPhone = Decrypt(info.GetString("phone"));
			this.m_sShortDesc = info.GetString("shortDesc");
			this.m_sMLTransactionID = info.GetString("mlID");
			this.m_nMLTransactionID = info.GetInt32("ccid");
			this.m_result = (ResultType)Enum.Parse(typeof(ResultType), info.GetString("result"), false);
			this.m_state = (TransactionState)Enum.Parse(typeof(TransactionState), info.GetString("state"), false);
			this.m_lastCall = (CallType)Enum.Parse(typeof(CallType), info.GetString("lastCall"), false);
			this.m_sProviderTransactionID = info.GetString("provID");
			this.m_sAuthCode = info.GetString("authCode");
			this.m_sResponseText = info.GetString("responseText");
			this.m_sErrorInfo = info.GetString("errorInfo");
			this.m_sReturnInfo = info.GetString("returnInfo");
		}

		protected void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("gatewayVersion", Version);
			info.AddValue("amount", this.m_dAmount);
			info.AddValue("type", this.m_sType);
			info.AddValue("login1", Encrypt(this.m_sLogin1));
			info.AddValue("login2", Encrypt(this.m_sLogin2));
			info.AddValue("password", Encrypt(this.m_sPassword));
			info.AddValue("isTest", this.m_bIsTest);
			info.AddValue("card", this.m_card);
			info.AddValue("email", Encrypt(this.m_sEmail));
			info.AddValue("phone", Encrypt(this.m_sPhone));
			info.AddValue("shortDesc", this.m_sShortDesc);
			info.AddValue("mlID", this.m_sMLTransactionID);
			info.AddValue("ccid", this.m_nMLTransactionID);
			info.AddValue("result", this.m_result.ToString());
			info.AddValue("state", this.m_state.ToString());
			info.AddValue("lastCall", this.m_lastCall.ToString());
			info.AddValue("provID", this.m_sProviderTransactionID);
			info.AddValue("authCode", this.m_sAuthCode);
			info.AddValue("responseText", this.m_sResponseText);
			info.AddValue("errorInfo", this.m_sErrorInfo);
			info.AddValue("returnInfo", this.m_sReturnInfo);
		}

		/// <summary>
		/// Gets True if extra tracing is configured.
		/// </summary>
		public bool DoTrace
		{
			get { return this.m_bTrace ; }
		}

		public string Type
		{
			get {return this.m_sType;}
		}

		// virtual so that base classes can perform any format checks on the input strings
		virtual public void SetLoginInfo(string login1, string login2, string password, bool isTest)
		{
			this.m_sLogin1 = login1;
			this.m_sLogin2 = login2;
			this.m_sPassword = password;
			this.m_bIsTest = isTest;
		}

		public string Login1
		{
			get {return this.m_sLogin1;}
		}

		public string Login2
		{
			get {return this.m_sLogin2;}
		}

		public string Password
		{
			get {return this.m_sPassword;}
		}

		public bool IsTest
		{
			get {return this.m_bIsTest;}
		}

		public CommonLib.CreditCardHolder CreditCard
		{
			get {return this.m_card;}
			set {this.m_card = value;}
		}

		public string ContactEmail
		{
			get {return this.m_sEmail;}
			set {this.m_sEmail = value;}
		}

		public string ContactPhone
		{
			get {return this.m_sPhone;}
			set {this.m_sPhone = value;}
		}

		public string ShortDescription
		{
			get {return this.m_sShortDesc;}
			set {this.m_sShortDesc = value;}
		}

		/// <summary>
		/// CCID in CC_Charges table
		/// </summary>
		public int MLID
		{
			get {return this.m_nMLTransactionID;}
			set {this.m_nMLTransactionID = value;}
		}

		/// <summary>
		/// ID that will be sent to Gateway
		/// </summary>
		public string MLTransactionID
		{
			get {return this.m_sMLTransactionID;}
			set {this.m_sMLTransactionID = value;}
		}

		public ResultType Result
		{
			get {return this.m_result;}
		}

		// virtual because some gateways may provide a method to query status,
		// so that the implementation can generate an up-to-date value
		virtual public TransactionState State
		{
			get {return this.m_state;}
		}

		public CallType LastCall
		{
			get {return this.m_lastCall;}
		}

		public string ProviderTransactionID
		{
			get {return this.m_sProviderTransactionID;}
		}

		public string AuthorizationCode
		{
			get {return this.m_sAuthCode;}
		}

		public string ResponseText
		{
			get {return this.m_sResponseText;}
		}

		public string ErrorInfo
		{
			get {return this.m_sErrorInfo;}
		}

		public string ReturnInfo
		{
			get {return this.m_sReturnInfo;}
		}

		public double Amount
		{
			get {return this.m_dAmount;}
		}

		virtual protected void ClearVolatileData()
		{
			this.m_sResponseText = "";
			this.m_sErrorInfo = "";
			this.m_sReturnInfo = "";
		}

		virtual protected void CheckOrderData(int quantity, double charge)
		{
			if (quantity < 1)
				throw CreateException("CheckOrderData", "Order Quantity must be > 0");
			if (quantity >= 10000)
				throw CreateException("CheckOrderData", "Order Quantity must be < 10,000");
			if (charge >= 100000)
				throw CreateException("CheckOrderData", "Charge per item must be < $100,000");
		}

		/// <summary>
		/// Adds an individual order description to the Skipjack transaction.
		/// </summary>
		/// <param name="itemID">Item number</param>
		/// <param name="quantity">Number of items, four-digit max</param>
		/// <param name="charge">Charge per item, 99999.99 max</param>
		/// <param name="description">Item description</param>
		virtual public void AddOrder(int itemID, int quantity, double charge, string description)
		{
			// Not all gateways support this...in which case this provides a null implementation
		}

		/// <summary>
		/// Authorize an amount on the credit card
		/// </summary>
		abstract public void Authorize(double amount);

		/// <summary>
		/// Change the authorization amount.
		/// </summary>
		abstract public void ReAuthorize(double amount);

		/// <summary>
		/// Settle a previously Authorized transaction.
		/// </summary>
		abstract public void Settle();

		/// <summary>
		/// Cancel a previously Authorized transaction
		/// </summary>
		abstract public void Cancel();

		/// <summary>
		/// Refund a settled amount to the user.
		/// </summary>
		abstract public void Refund();

		private string Encrypt(string inString)
		{
			using (CryptoUtil crypto = new CryptoUtil(CreditCardGateway2.s_sEncryptKey))
			{
				return crypto.Encrypt(inString);
			}
		}

		private string Decrypt(string inString)
		{
			using (CryptoUtil crypto = new CryptoUtil(CreditCardGateway2.s_sEncryptKey))
			{
				return crypto.Decrypt(inString);
			}
		}

		static private void LogError(string method, string msg)
		{
		}

		static protected ApplicationException CreateException(string method, string msg)
		{
			LogError(method, msg);
			return new ApplicationException(msg);
		}
	}
}
