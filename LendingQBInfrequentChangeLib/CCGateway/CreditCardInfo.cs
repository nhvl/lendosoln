// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using CommonLib;

namespace MeridianLink.CCGateway
{
	public class CreditCardInfo
	{
		private string m_sName;
		private string m_sAddress;
		private string m_sCity;
		private string m_sState;
		private string m_sZip;
		private string m_sNumber;
		private string m_sEncryptedNumber;
		private string m_sCVV;
		private int m_nExpMonth;
		private int m_nExpYear;

		public CreditCardInfo(string name, string address, string city, string state, string zip, string number, string encryptedNumber, string cvv, int expMonth, int expYear)
		{
			if (!SafeString.HasValue(name)) throw new ApplicationException("Empty Card Holder Name");
			/* -- 03/31/06-bd-these fields are not required
			if (!SafeString.HasValue(a_sAddress)) throw new ApplicationException("Empty Billing Address");
			if (!SafeString.HasValue(a_sCity)) throw new ApplicationException("Empty Billing City");
			if (!SafeString.HasValue(a_sState)) throw new ApplicationException("Empty Billing State");
			*/

			if (!SafeString.HasValue(zip)) throw new ApplicationException("Empty Billing Zipcode");
			if (!SafeString.HasValue(number) && !SafeString.HasValue(encryptedNumber)) throw new ApplicationException("Empty Card Number");
			if ((expMonth < 1) || (expMonth > 12)) throw new ApplicationException("Invalid Expiration Month");
			int nThisYear = DateTime.Now.Year;
			if ((expYear < nThisYear) || (expYear > nThisYear + 100)) throw new ApplicationException("Invalid Expiration Year");

			this.m_sName = name;
			this.m_sAddress = address;
			this.m_sCity = city;
			this.m_sState = state;
			this.m_sZip = zip;
			this.m_sNumber = number;
			this.m_sEncryptedNumber = encryptedNumber;
			this.m_sCVV = cvv;
			this.m_nExpMonth = expMonth;
			this.m_nExpYear = expYear;
		}

		public CreditCardInfo(CreditCardHolder cardHolder)
		{
			if (cardHolder.UnEncryptedCard != null) PopulateFromCreditCard(cardHolder.UnEncryptedCard);
			else PopulateFromEncryptedCreditCard(cardHolder.EncryptedCard);
		}

		public CreditCardInfo(CreditCard card)
		{
			PopulateFromCreditCard(card);
		}

		public CreditCardInfo(CreditCardEncrypted card)
		{
			PopulateFromEncryptedCreditCard(card);
		}

		public string Name {get{return this.m_sName;}}
		public string Address {get{if (SafeString.HasValue(this.m_sAddress)) return this.m_sAddress; else return "" ;}}
		public string City {get{if (SafeString.HasValue(this.m_sCity)) return this.m_sCity; else return "" ;}}
		public string State {get{if (SafeString.HasValue(this.m_sState)) return this.m_sState; else return "" ;}}
		public string Zip {get{return this.m_sZip;}}
		public string Number {get{return this.m_sNumber;}}
		public string EncryptedNumber {get{return this.m_sEncryptedNumber;}}
		public string CVV {get{return this.m_sCVV;}}
		public int ExpMonth {get{return this.m_nExpMonth;}}
		public int ExpYear {get{return this.m_nExpYear;}}

		public bool UseEncrypted()
		{
			return (!CommonLib.SafeString.HasValue(this.Number));
		}

		private void PopulateFromCreditCard(CreditCard card)
		{
			this.m_sName = card.Name;
			if (SafeString.HasValue(card.Address.StreetAddress)) this.m_sAddress = card.Address.StreetAddress;
			if (SafeString.HasValue(card.Address.City)) this.m_sCity = card.Address.City;
			if (SafeString.HasValue(card.Address.State)) this.m_sState = card.Address.State;
			this.m_sZip = card.Address.Zipcode;
			this.m_sNumber = card.Number;
			if (SafeString.HasValue(card.CVV)) this.m_sCVV = card.CVV;
			this.m_nExpMonth = card.ExpMonth;
			this.m_nExpYear = card.ExpYear;
		}

		private void PopulateFromEncryptedCreditCard(CreditCardEncrypted card)
		{
			this.m_sName = card.Name;
			if (SafeString.HasValue(card.Address.StreetAddress)) this.m_sAddress = card.Address.StreetAddress;
			if (SafeString.HasValue(card.Address.City)) this.m_sCity = card.Address.City;
			if (SafeString.HasValue(card.Address.State)) this.m_sState = card.Address.State;
			this.m_sZip = card.Address.Zipcode;
			this.m_sEncryptedNumber = card.Number;
			if (SafeString.HasValue(card.CVV)) this.m_sCVV = card.CVV;
			this.m_nExpMonth = card.ExpMonth;
			this.m_nExpYear = card.ExpYear;
		}
	}
}
