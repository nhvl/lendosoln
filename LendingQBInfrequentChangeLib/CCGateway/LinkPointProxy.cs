using System;
using System.Configuration;
using CommonLib;

namespace MeridianLink.CCGateway.LinkPointProxy
{
	/// <summary>
	/// Interface for the LinkPoint Proxy service
	/// </summary>
	public interface ILinkPointProxy
	{
		/// <summary>
		/// Executes a LinkPoint transaction.
		/// </summary>
		/// <param name="storeName">LinkPoint Merchant Store Name (usually numeric).</param>
		/// <param name="host">Host to send the request to.</param>
		/// <param name="port">Port to send the request to.</param>
		/// <param name="request">LinkPoint request, in XML format.</param>
		/// <returns>The transaction from LinkPoint.</returns>
		/// <remarks>
		/// The existing components already find the certificate path;
		/// that has been delayed until the requests reach this service.
		/// </remarks>
		string SendTransaction(string storeName, string host, int port, string request) ; 
	}

	/// <summary>
	/// Intermediary object for interactive communication with the LinkPointProxy service.
	/// </summary>
	public class LinkPointGatewayProxy
	{
		private ILinkPointProxy m_iService ;

		string m_sStoreName, m_sHost ;
		int m_nPort ;

		/// <summary>
		/// Initializes this gateway proxy.
		/// </summary>
		/// <param name="storeName">LinkPoint merchant store name</param>
		/// <param name="host">LinkPoint host name</param>
		/// <param name="port">LinkPoint host port</param>
		public LinkPointGatewayProxy(string storeName, string host, int port)
		{
			m_sStoreName = storeName ;
			m_sHost = host ;
			m_nPort = port ;
/*
			string sProxy = String.Format("tcp://{0}:{1}/{2}",
				Configuration.HostName, Configuration.HostPort, Configuration.ServiceName) ;

			m_iService = (ILinkPointProxy) Activator.GetObject(typeof(ILinkPointProxy), sProxy) ;
*/
		}

		/// <summary>
		/// Sends a proxied transaction request to LinkPoint.
		/// </summary>
		/// <param name="request">Request to send, as an XML string</param>
		/// <returns>Response, as a text string</returns>
		public string SendRequest(string request)
		{
			string sProxy = String.Format("tcp://{0}:{1}/{2}",
				Configuration.HostName, Configuration.HostPort, Configuration.ServiceName) ;

			m_iService = (ILinkPointProxy) Activator.GetObject(typeof(ILinkPointProxy), sProxy) ;

			return m_iService.SendTransaction(m_sStoreName, m_sHost, m_nPort, request) ;
		}
	}

	public struct Configuration
	{
		/// <summary>
		/// Gets the machine name for the LinkPoint Proxy service.
		/// </summary>
		public static string HostName
		{
			get
			{
				string sHost = SafeConvert.ToString(AppSettings.LinkPointProxyHost) ;
				return ( "" == sHost ? "localhost" : sHost ) ;
			}
		}

		/// <summary>
		/// Gets the machine name for the LinkPoint Proxy service.
		/// </summary>
		public static int HostPort
		{
			get 
			{
				int nPort = SafeConvert.ToInt(AppSettings.LinkPointProxyPort) ;
				return ( 0 >= nPort || 65535 < nPort ? 61129 : nPort ) ;
			}
		}

		/// <summary>
		/// Gets the service name used in the service URI.
		/// </summary>
		public static string ServiceName
		{
			get 
			{
				string sName = SafeConvert.ToString(AppSettings.LinkPointProxySvcName) ;
				return ( "" == sName ? "lpproxysvc" : sName ) ;
			}
		}
	}
}
