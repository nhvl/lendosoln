using System;
using System.Xml;
using CommonLib;

namespace MeridianLink.CCGateway
{
	/// <summary>
	/// Abstract superclass for credit card gateways.
	/// </summary>
	/// <remarks>
	/// Split off into a separate project - 12/20/02, Lawrence.
	/// </remarks>
	abstract public class CreditCardGateway
	{
		private string m_sDSN = null;

		protected enum CallResultType {SUCCESS, TIMEOUT, ERROR}

		public CreditCardGateway()
		{
		}

		abstract public string GatewayName {get;}

		/// <summary>
		/// MCL Application wants logging in CC_Submissions table so needs to set this property
		/// </summary>
		public string CC_Submissions_DSN
		{
			set {this.m_sDSN = value;}
		}

		/// <summary>
		/// Set the cardholder's name.
		/// </summary>
		/// <param name="firstName">Cardholder's first name.</param>
		/// <param name="lastName">Cardholder's last name.</param>
		abstract public void SetName( string firstName, string lastName ) ;

		/// <summary>
		/// Sets the card's billing address.
		/// </summary>
		/// <param name="streetAddress">The street address, including house number, street name, and street type.</param>
		/// <param name="city">City name.</param>
		/// <param name="state">State abbreviation.</param>
		/// <param name="zip">ZIP code.</param>
		abstract public void SetAddress( string streetAddress, string city, string state, string zip ) ;

		/// <summary>
		/// Specifies other contact information.
		/// </summary>
		/// <param name="sEmail">Cardholder's e-mail address.</param>
		/// <param name="sPhone">"Ship To" or admin phone number</param>
		abstract public void SetContactInfo( string sEmail, string sPhone ) ;

		/// <summary>
		/// Sets the basic credit-card info.  (CVV is not included.)
		/// </summary>
		/// <param name="cardNumber">Credit card number.</param>
		/// <param name="expMonth">Month of expiration.</param>
		/// <param name="expYear">Year of expiration.</param>
		abstract public void SetCardInfo( string cardNumber, string expMonth, string expYear ) ;

		/// <summary>
		/// Sets basic credit-card info, including a CVV/CVV2/CVM.
		/// </summary>
		/// <param name="cardNumber">Credit card number.</param>
		/// <param name="expMonth">Month of expiration.</param>
		/// <param name="expYear">Year of expiration.</param>
		/// <param name="sCVV">Card Value code, typically 3-4 digits.</param>
		abstract public void SetCardInfo( string cardNumber, string expMonth, string expYear, string sCVV ) ;
		
		/// <summary>
		/// Sets the total amount to be charged.
		/// </summary>
		/// <param name="totalCharge">Total charge amount.</param>
		abstract public void SetAmount( double totalCharge ) ;

		/// <summary>
		/// Retrieve the amount that was charged.
		/// </summary>
		abstract public double GetAmount();

		/// <summary>
		/// Specifies a brief product description that may be passed onto a billing statement.
		/// This is typically short - 16 characters or less.
		/// </summary>
		/// <param name="sDesc">A short product description.</param>
		abstract public void SetShortDesc( string sDesc ) ;

		/// <summary>
		/// Adds an order to a transaction.  Not fully implemented by some gateways.
		/// </summary>
		/// <param name="quantity">Number of items</param>
		/// <param name="cost">Cost per item</param>
		/// <param name="description">Item description</param>
		abstract public void AddOrder( int itemID, int quantity, double cost, string description ) ;

		/// <summary>
		/// Associate a transaction ID with an order.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction identifier.</param>
		abstract public void SetMLTransactionID( int MLTransactionID ) ;

		/// <summary>
		/// Send an authorization request and a sale request.
		/// The order must be constructed (using other methods) before calling this function.
		/// </summary>
		abstract public void AuthAndSale() ;

		/// <summary>
		/// Authorizes a transaction, but does not actually process it.
		/// The order must be constructed (using other methods) before calling this function.
		/// </summary>
		abstract public void Authorize() ;

		/// <summary>
		/// Processes a previously validated transaction.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction ID</param>
		/// <param name="providerTransactionID">The gateway's transaction ID</param>
		abstract public void Capture(string MLTransactionID, string providerTransactionID) ;

		/// <summary>
		/// Processes a previously validated transaction.
		/// Specifies the amount input - but this may be ignored by some gateways.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction ID</param>
		/// <param name="providerTransactionID">The gateway's transaction ID</param>
		abstract public void Capture(string MLTransactionID, string providerTransactionID, double amount) ;

		/// <summary>
		/// Credits the account for a previous transaction.
		/// The original amount will be refunded.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction ID</param>
		/// <param name="providerTransactionID">The gateway's transaction ID</param>
		abstract public void Refund(string MLTransactionID, string providerTransactionID) ;

		/// <summary>
		/// Credits the account a given amount on a transaction.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction ID</param>
		/// <param name="providerTransactionID">The gateway's transaction ID</param>
		/// <param name="amount">Non-negative amount to refund.</param>
		abstract public void Refund(string MLTransactionID, string providerTransactionID, double amount) ;

		/// <summary>
		/// Voids a previous transaction.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction ID</param>
		/// <param name="providerTransactionID">The gateway's transaction ID</param>
		abstract public void VoidTransaction(string MLTransactionID, string providerTransactionID) ;

		/// <summary>
		/// Query a previous transaction.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction ID</param>
		/// <param name="providerTransactionID">The gateway's transaction ID</param>
		abstract public void RequestStatus(string MLTransactionID, string providerTransactionID ) ; 
		
		/// <summary>
		/// Query a previous transaction.
		/// </summary>
		/// <param name="MLTransactionID">Our transaction ID</param>
		/// <param name="providerTransactionID">The gateway's transaction ID</param>
		/// <param name="date">Date to search, usually optional.</param>
		abstract public void RequestStatus(string MLTransactionID, string providerTransactionID, DateTime date ) ; 

		/// <summary>
		/// Queries whether or not a transaction was successful.
		/// </summary>
		/// <returns>True on success, False on failure.</returns>
		abstract public bool IsSuccess() ;

		/// <summary>
		/// Gets the authorization code.  Read-only property.
		/// </summary>
		/// <returns>Authorization code, as a string.</returns>
		abstract public string GetAuthCode() ;
		
		/// <summary>
		/// Gets miscellaneous response text.
		/// </summary>
		/// <returns>Response text, as a string.</returns>		
		abstract public string GetResponseText() ;

		/// <summary>
		/// Gets extra error information.
		/// </summary>
		/// <returns>Error info, as a string.</returns>
		abstract public string GetErrorInfo() ;

		/// <summary>
		/// Gets the returned information.
		/// </summary>
		/// <returns>Returned info, as a string.</returns>
		abstract public string GetReturnInfo() ;

		/// <summary>
		/// Returns the provider's transaction ID or file number.
		/// This ID is required for successful AUTH/CAPTURE usage.
		/// </summary>
		/// <returns></returns>
		abstract public string GetTransactionID() ;

		/// <summary>
		/// Determines whether or not a non-authorization action succeeded.
		/// </summary>
		/// <returns>True on success, false otherwise.</returns>
		abstract public bool IsActionSuccess() ;

		/// <summary>
		/// Return a description of what happened in a non-authorization action.
		/// </summary>
		abstract public string ActionDescription();
		
		/// <summary>
		/// Validates a credit-card number using the Lunh-10 algorithm.
		/// </summary>
		/// <param name="cardNumber">The credit card number.</param>
		/// <returns>A numeric card number if valid.
		/// Throws an exception if invalid.</returns>
		protected string ValidateCardNumber( string cardNumber )
		{
			int idx, sum ;
			string sWorking = "" ;
			int[] validator ;

			for ( idx = 0; idx < cardNumber.Length ; idx++ )
			{
				if ( cardNumber[idx] >= '0' && cardNumber[idx] <= '9' )
					sWorking += cardNumber[idx] ;
			}

			if ( sWorking.Length < 13 || sWorking.Length > 16 )
				throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Invalid credit card number: Wrong number of digits.") ;

			# region Determine card type and digit length

			switch( CreditCardType( CommonLib.SafeConvert.ToLong( CommonLib.SafeString.Left(sWorking, 6) ) ) )
			{
				case "VI" :
					if ( !( sWorking.Length == 13 || sWorking.Length == 16 ) )
						throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Invalid credit card number: Wrong number of digits.") ;
					break ;
				case "MC" :
					if (!(sWorking.Length == 16))
						throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Invalid credit card number: Wrong number of digits.") ;
					break ;
				case "AE" :
					if (!(sWorking.Length == 15))
						throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Invalid credit card number: Wrong number of digits.") ;
					break ;
				case "DI" :
					if (!(sWorking.Length == 16))
						throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Invalid credit card number: Wrong number of digits.") ;
					break ;
				case "DC" :
					if (!(sWorking.Length == 14 || sWorking.Length == 16))
						throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Invalid credit card number: Wrong number of digits.") ;
					break ;
				case "JC" :
					if (!(sWorking.Length == 16))
						throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Invalid credit card number: Wrong number of digits.") ;
					break ;
				default :
					throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Unrecognized credit card type." ) ;
			}
			#endregion

			#region Apply Luhn-10 algorithm
			// The rather unwieldy statement CommonLib.SafeConvert.ToLong(sWorking[idx].ToString())
			// is used because Convert.ToInt32(char) returns the ASCII value rather than the digit.
			// [java.lang.Character.ToInt(), anyone?]

			validator = new int[sWorking.Length-1] ;

			sum = 1 ;
			for ( idx = sWorking.Length-3 ; idx < sWorking.Length-1 ; idx++ )
			{
				validator[idx] = sum * CommonLib.SafeConvert.ToLong( sWorking[idx].ToString() ) ;
				sum = sum == 1 ? 2 : 1 ;
			}

			sum = 2 ;
			for ( idx = sWorking.Length-4; idx >= 0; idx-- )
			{
				validator[idx] = sum * CommonLib.SafeConvert.ToLong( sWorking[idx].ToString() ) ;
				sum = sum == 1 ? 2 : 1 ;
			}

			sum = 0;
			for ( idx = 0 ; idx < sWorking.Length-1; idx++ )
			{
				if ( validator[idx] >= 10 )
					sum += ( validator[idx] / 10 ) + ( validator[idx] % 10 ) ;
				else
					sum += validator[idx] ;
			}

			sum += CommonLib.SafeConvert.ToLong( sWorking[sWorking.Length-1].ToString() ) ;

			// m_oLog.Log("VER", "CC#: " + sWorking + "; checksum: " + sum, 0) ;

			if ( sum % 10 == 0 )
				return sWorking ;
			else
				throw new CreditCardException(ERROR_TYPE.USER_ERROR, "Credit card number is invalid.") ;

			#endregion
                
		}

		
		/// <summary>
		/// Determines a type of credit card based on the first six digits.
		/// </summary>
		/// <param name="firstSix">First six digits of the credit card number.</param>
		/// <returns></returns>
		protected string CreditCardType( int firstSix )
		{
			if ( firstSix >= 400000 && firstSix < 500000 )
				return "VI" ;	// VISA
			if ( firstSix >= 510000 && firstSix < 560000 )
				return "MC" ;	// MasterCard
			if ( ( firstSix >= 340000 && firstSix < 350000 )
				|| ( firstSix >= 370000 && firstSix < 380000 ) )
				return "AE" ;	// American Express
			if ( firstSix >= 601100 && firstSix < 601200 )
				return "DI" ;	// Discover / NOVUS
			if ( ( firstSix >= 300000 && firstSix < 306000 )
				|| ( firstSix >= 309500 && firstSix < 309600 )
				|| ( firstSix >= 360000 && firstSix < 370000 )
				|| ( firstSix >= 380000 && firstSix < 390000 ) )
				return "DC" ;	// Diners Club
			if ( firstSix >= 352800 && firstSix < 359000 )
				return "JC" ;

			return "" ;	// Unknown
		}
	}

	/// <summary>
	/// Superclass for timeout exceptions.
	/// </summary>
	public abstract class CCTimeoutException : ApplicationException
	{
		protected XmlDocument m_docTimeout ;

		public CCTimeoutException(string gatewayType) : base(gatewayType + " timeout")
		{
			m_docTimeout = new XmlDocument() ;
			XmlElement root = m_docTimeout.CreateElement(String.Format("{0}_TIMEOUT", gatewayType.ToUpper())) ;
			m_docTimeout.AppendChild(root) ;
		}

		public CCTimeoutException(string gatewayType, Exception exc) : base(gatewayType + " timeout", exc)
		{
			m_docTimeout = new XmlDocument() ;
			XmlElement root = m_docTimeout.CreateElement(String.Format("{0}_TIMEOUT", gatewayType.ToUpper())) ;
			m_docTimeout.AppendChild(root) ;
		}

		public XmlElement Login
		{
			get
			{
				XmlElement elem = (XmlElement) m_docTimeout.DocumentElement.SelectSingleNode("LOGIN") ; 
				if ( elem == null )
				{
					elem = m_docTimeout.CreateElement("LOGIN") ;
					m_docTimeout.DocumentElement.AppendChild(elem) ;
				}
				return elem ;
			}
		}

		public XmlElement Transaction
		{
			get
			{
				XmlElement elem = (XmlElement) m_docTimeout.DocumentElement.SelectSingleNode("TRANSACTION") ; 
				if ( elem == null )
				{
					elem = m_docTimeout.CreateElement("TRANSACTION") ;
					m_docTimeout.DocumentElement.AppendChild(elem) ;
				}
				return elem ;
			}		
		}

		public string DSN
		{
			get { return m_docTimeout.DocumentElement.GetAttribute("dsn") ; }
			set { m_docTimeout.DocumentElement.SetAttribute("dsn", value) ; }
		}

		public string StringValue
		{
			get { return m_docTimeout.OuterXml ; }
		}

		public string Name
		{
			get { return m_docTimeout.DocumentElement.LocalName ; }
		}

	}
}
