using System;

namespace MeridianLink.CCGateway
{
	/// <exception>
	/// Provides an exception type for credit card entry errors.
	/// </exception>
	public class CreditCardException : Exception
	{
		ERROR_TYPE m_eErrorType ;
		public CreditCardException(ERROR_TYPE eErrorType, string sErrorMessage) : base(sErrorMessage)
		{
			m_eErrorType = eErrorType ;
		}
		public ERROR_TYPE ErrorType
		{
			get { return m_eErrorType ; }
		}
		public string ErrorMessage // kept in to avoid breaking any clients
		{
			get { return this.Message ; }
		}
	}

	public enum ERROR_TYPE { NONE = 0, SYSTEM_ERROR = -2147220504, USER_ERROR = -2147220503, NO_RECORD_FOUND = -2147220502 } ;

}
