// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Configuration;

namespace MeridianLink.CCGateway
{
	public abstract class AppSettings
	{
		private static string KEYWORD__ClientCertificatePath = "ClientCertificatePath";
		private static string KEYWORD__Linkpoint_Test_Url = "Linkpoint_Test_Url";
		private static string KEYWORD__Linkpoint_Prod_Url = "Linkpoint_Prod_Url";
		private static string KEYWORD__Linkpoint_Test_Port = "Linkpoint_Test_Port";
		private static string KEYWORD__Linkpoint_Prod_Port = "Linkpoint_Prod_Port";
		private static string KEYWORD__LinkPointProxyHost = "LinkPointProxyHost";
		private static string KEYWORD__LinkPointProxyPort = "LinkPointProxyPort";
		private static string KEYWORD__LinkPointProxySvcName = "LinkPointProxySvcName";
		private static string KEYWORD__Skipjack_Test_Url = "Skipjack_Test_Url";
		private static string KEYWORD__Skipjack_Prod_Url = "Skipjack_Prod_Url";
		private static string KEYWORD__MERIDIANLINK_ENCRYPTION_SERVICE_URL = "MERIDIANLINK_ENCRYPTION_SERVICE_URL";
		private static string KEYWORD__CC_Trace_CRAs = "CC_Trace_CRAs";

		// Required for Version 3
		//private static string KEYWORD__SKIPJACK_WEBSERVICE_URL = "SKIPJACK_WEBSERVICE_URL";
		//private static string KEYWORD__LINKPOINT_WEBSERVICE_URL = "LINKPOINT_WEBSERVICE_URL";

		public static string ClientCertificatePath
		{
			get {return GetValue(KEYWORD__ClientCertificatePath);}
		}
		public static string Linkpoint_Test_Url
		{
			get {return GetValue(KEYWORD__Linkpoint_Test_Url);}
		}
		public static string Linkpoint_Prod_Url
		{
			get {return GetValue(KEYWORD__Linkpoint_Prod_Url);}
		}
		public static string Linkpoint_Test_Port
		{
			get {return GetValue(KEYWORD__Linkpoint_Test_Port);}
		}
		public static string Linkpoint_Prod_Port
		{
			get {return GetValue(KEYWORD__Linkpoint_Prod_Port);}
		}
		public static string LinkPointProxyHost
		{
			get {return GetValue(KEYWORD__LinkPointProxyHost);}
		}
		public static string LinkPointProxyPort
		{
			get {return GetValue(KEYWORD__LinkPointProxyPort);}
		}
		public static string LinkPointProxySvcName
		{
			get {return GetValue(KEYWORD__LinkPointProxySvcName);}
		}
		public static string Skipjack_Test_Url
		{
			get {return GetValue(KEYWORD__Skipjack_Test_Url);}
		}
		public static string Skipjack_Prod_Url
		{
			get {return GetValue(KEYWORD__Skipjack_Prod_Url);}
		}
		public static string MERIDIANLINK_ENCRYPTION_SERVICE_URL
		{
			get {return GetValue(KEYWORD__MERIDIANLINK_ENCRYPTION_SERVICE_URL);}
		}
		public static string CC_Trace_CRAs
		{
			get { return GetValue(KEYWORD__CC_Trace_CRAs);}
		}
		public static string Skipjack_Webservice_Url
		{
			//get {return GetValue(KEYWORD__SKIPJACK_WEBSERVICE_URL);}
			get { return "https://ccs.meridianlink.com/Skipjack.asmx" ; }
		}
		public static string Linkpoint_Webservice_Url
		{
			//get {return GetValue(KEYWORD__LINKPOINT_WEBSERVICE_URL);}
			// 04/10/06-bd-This value isn't going to change. Therefore, it's not necessary to store it in app.config
			get { return "https://ccs.meridianlink.com/Linkpoint.asmx" ; }
		}

		public static string GetValue(string key)
		{
			string value = ConfigurationManager.AppSettings[key] ;
			if (value == null || value.Length == 0)
				throw new ApplicationException(string.Format("Key {0} is not configured in the exe.config file", key) ) ;

			return value ;
		}
	}
}
