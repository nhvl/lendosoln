// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Xml;

namespace MeridianLink.CCGateway
{
	public abstract class LinkPointAPIGateway3
	{
		public class TransactionInfo
		{
			private string m_sStoreName;
			private bool m_bIsTest;
			private string m_sOID;
			private string m_sLinkpointID;
			private double m_dCharge;

			/// <summary>
			/// Container for the details of the transaction
			/// </summary>
			/// <param name="storeName">Assigned by Linkpoint to the merchant account...will map to a pem file.</param>
			/// <param name="a_bIsTest">True if using the Test gateway, false if production.</param>
			/// <param name="oid">An ID that the client (MeridianLink) assigns to the transaction. This ID must be unique or LinkPoint will return a DUPLICATE error.</param>
			/// <param name="linkpointID">
			/// The Transaction ID that Linkpoint returns for the original authorization.
			/// This should be set to null when making the original authorization. This Transactio ID is only required for making voids.
			/// This is the tdate field in the LinkPoint manual.
			/// </param>
			/// <param name="charge">The quantity to be charged.</param>
			public TransactionInfo(string storeName, bool isTest, string oid, string linkpointID, double charge)
			{
				if (!CommonLib.SafeString.HasValue(storeName)) throw new ApplicationException("Invalid Store Name");
				if (!CommonLib.SafeString.HasValue(oid)) throw new ApplicationException("Invalid OID");
				if (charge <= 0.0) throw new ApplicationException("Invalid Charge amount");

				this.m_sStoreName = storeName;
				this.m_bIsTest = isTest;
				this.m_sOID = oid;
				this.m_sLinkpointID = linkpointID;
				this.m_dCharge = charge;
			}

			public string StoreName {get{return this.m_sStoreName;}}
			public bool IsTest {get{return this.m_bIsTest;}}
			public string OID {get{return this.m_sOID;}}
			public string LinkpointID {get{ return this.m_sLinkpointID;}}
			public double Charge {get{return this.m_dCharge;}}
		}

		public class OrderItem
		{
			private string m_sID;
			private string m_sDescription;
			private double m_dPrice;
			private int m_nQuantity;

			/// <summary>
			/// An itemized statement can be obtained containing these items.
			/// The total charge for a transaction will be
			/// Total = sum over items (price x quantity)
			/// </summary>
			/// <param name="id">An ID assigned by the client (MeridianLink).  This can be something simple like 1, 2, 3,...</param>
			/// <param name="description">A description of what the item is.</param>
			/// <param name="price">The price/per object to which this item refers.</param>
			/// <param name="quantity">The number of objects that this item represents.</param>
			public OrderItem(string id, string description, double price, int quantity)
			{
				if (!CommonLib.SafeString.HasValue(id)) throw new ApplicationException("Invalid Item ID");
				//if (!CommonLib.SafeString.HasValue(a_sDescription)) throw new ApplicationException("Invalid Item Description");
				if (price <= 0.0) throw new ApplicationException("Invalid Item Price amount");
				if (quantity <= 0) throw new ApplicationException("Invalid Quantity amount");

				this.m_sID = id;
				this.m_sDescription = (description == null) ? string.Empty : description;
				this.m_dPrice = price;
				this.m_nQuantity = quantity;
			}

			public string ID {get{return this.m_sID;}}
			public string Description {get{return this.m_sDescription;}}
			public double Price {get{return this.m_dPrice;}}
			public int Quantity {get{return this.m_nQuantity;}}
		}

		public class DescriptiveInfo
		{
			private string m_sShortDescription;
			private OrderItem[] m_arrItems;

			/// <summary>
			/// Used to send descriptive information to the credit card gateway.
			/// </summary>
			/// <param name="shortDescription">
			/// A short description that would appear on the monthly statement.  This is an OPTIONAL field.
			/// </param>
			/// <param name="items">
			/// The items would appear in a more detailed statement where the total charge
			/// is broken down...probably viewable on the gateway's website or by special request.
			/// This is an OPTIONAL field.
			/// </param>
			public DescriptiveInfo(string shortDescription, OrderItem[] items)
			{
				this.m_sShortDescription = shortDescription;
				this.m_arrItems = items;
			}

			public string ShortDescription {get{return this.m_sShortDescription;}}
			public OrderItem[] OrderItems {get{return this.m_arrItems;}}
		}

		public class ParsedResponse
		{
			private XmlDocument m_docResponse;
			private bool m_bSuccess;
			private string m_sCode;
			//private string m_sLinkpointID;

			public ParsedResponse(XmlDocument response)
			{
				this.m_docResponse = response;
				ParseResponse();
			}

			public XmlDocument ResponseXml {get{return this.m_docResponse;}}
			public bool Success {get{return this.m_bSuccess;}}
			public string AuthCode {get{return this.m_sCode;}}

			// r_csp, r_score, r_authresponse in our DB but undocumented in LPIntGuide.pdf !!
			public string AVS
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_avs");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			public string OrderNumber
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_ordernum");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			public string Error
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_error");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			/// <summary>
			/// Can return APPROVED | DECLINED | FRAUD
			/// </summary>
			public string Approved
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_approved");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			public string ApprovalCode
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_code");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			public string Message
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_message");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			/// <summary>
			/// Example: Thu Mar 23 17:00:28 2006 
			/// </summary>
			public string TimeString
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_time");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			public DateTime DateTime
			{
				get
				{
					string sTime = this.TimeString;
					if (sTime.Length == 0) return DateTime.MinValue;

					try
					{
						DateTime dt = DateTime.ParseExact(sTime, "ddd MMM dd HH:mm:ss yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
						return dt;
					}
					catch
					{
						CommonLib.Tracer.Trace("CCGateway", "LinkPointAPIGateway3", "DateTime", "war", "Failed to parser " + sTime, 0);
						return DateTime.MinValue;
					}
				}
			}

			public string ReferenceNumber
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_ref");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			public string LinkpointID
			{
				get
				{
					XmlElement elem = (XmlElement)this.m_docResponse.DocumentElement.SelectSingleNode("r_tdate");
					return (elem == null) ? string.Empty : elem.InnerText;
				}
			}

			private void ParseResponse()
			{
				this.m_bSuccess = (this.Approved == "APPROVED");

				if (this.ApprovalCode.Length > 0)
				{
					// The authorization code returned by LinkPoint is long.
					// For brevity, only show the first segment of the code.
					// If necessary, the merchant can still see the whole thing at LinkPoint Central.
					int nPos = this.ApprovalCode.IndexOf(':');
					this.m_sCode = (nPos > 0) ? this.ApprovalCode.Substring(0, nPos) : this.ApprovalCode;
				}
			}
		}

		public static bool CreditCardIsEncrypted(CreditCardInfo ccInfo)
		{
			if (ccInfo == null) throw new ApplicationException("Credit Card information missing");
			return ccInfo.UseEncrypted();
		}

		public static string FormulateGatewayInfo(TransactionInfo tranInfo)
		{
			string sTest = (tranInfo.IsTest) ? "T" : "F";
			return string.Format("<LinkPointComm store='{0}' test='{1}'/>", tranInfo.StoreName, sTest);
		}

		public static ParsedResponse GetParsedResponse(XmlDocument docResponse)
		{
			return new ParsedResponse(docResponse);
		}

		/*
		 * When the client chooses to directly call the webservice that communicates with Skipjack,
		 * use the following helper methods:
		 * 
		 * bool bEncrypted = CreditCardIsEncrypted(creditcardInfo);
		 * string sGatewayInfo = FormulateGatewayInfo(transactionInfo);
		 * XmlDocument docRequest = FormulateAuthorizeRequest(creditcardInfo, transactionInfo, descriptiveInfo);
		 * ...client calls webservice and gets sResponse
		 * ParsedResponse parsedResponse = GetParsedResponse(sResponse);
		*/
		public static XmlDocument FormulateAuthorizeRequest(CreditCardInfo ccInfo, TransactionInfo tranInfo, DescriptiveInfo descInfo)
		{
			if (tranInfo == null) throw new ApplicationException("Transaction information missing");
			if (ccInfo == null) throw new ApplicationException("Credit Card information missing");

			return FormulateRequest("PREAUTH", ccInfo, tranInfo, descInfo);
		}

		/// <summary>
		/// Authorize.
		/// </summary>
		public static ParsedResponse Authorize(CreditCardInfo ccInfo, TransactionInfo tranInfo, DescriptiveInfo descInfo)
		{
			bool bEncrypted = CreditCardIsEncrypted(ccInfo);
			string sGatewayInfo = FormulateGatewayInfo(tranInfo);
			XmlDocument docRequest = FormulateAuthorizeRequest(ccInfo, tranInfo, descInfo);
			XmlDocument docResponse = SendRequest(docRequest, sGatewayInfo, bEncrypted);
			return GetParsedResponse(docResponse);
		}

		/*
		 * When the client chooses to directly call the webservice that communicates with Skipjack,
		 * use the following helper methods:
		 * 
		 * bool bEncrypted = CreditCardIsEncrypted(creditcardInfo);
		 * string sGatewayInfo = FormulateGatewayInfo(transactionInfo);
		 * XmlDocument docRequest = FormulateSettleRequest(creditcardInfo, transactionInfo, descriptiveInfo);
		 * ...client calls webservice and gets sResponse
		 * ParsedResponse parsedResponse = GetParsedResponse(sResponse);
		*/
		public static XmlDocument FormulateSettleRequest(CreditCardInfo ccInfo, TransactionInfo tranInfo, DescriptiveInfo descInfo)
		{
			if (tranInfo == null) throw new ApplicationException("Transaction information missing");
			if (ccInfo == null) throw new ApplicationException("Credit Card information missing");

			return FormulateRequest("POSTAUTH", ccInfo, tranInfo, descInfo);
		}

		/// <summary>
		/// Settle.
		/// </summary>
		public static ParsedResponse Settle(CreditCardInfo ccInfo, TransactionInfo atranInfo, DescriptiveInfo descInfo)
		{
			bool bEncrypted = CreditCardIsEncrypted(ccInfo);
			string sGatewayInfo = FormulateGatewayInfo(atranInfo);
			XmlDocument docRequest = FormulateSettleRequest(ccInfo, atranInfo, descInfo);
			XmlDocument docResponse = SendRequest(docRequest, sGatewayInfo, bEncrypted);
			return GetParsedResponse(docResponse);
		}

		/*
		 * When the client chooses to directly call the webservice that communicates with Skipjack,
		 * use the following helper methods:
		 * 
		 * bool bEncrypted = CreditCardIsEncrypted(creditcardInfo);
		 * string sGatewayInfo = FormulateGatewayInfo(transactionInfo);
		 * XmlDocument docRequest = FormulateCancelRequest(creditcardInfo, transactionInfo, descriptiveInfo);
		 * ...client calls webservice and gets sResponse
		 * ParsedResponse parsedResponse = GetParsedResponse(sResponse);
		*/
		public static XmlDocument FormulateCancelRequest(CreditCardInfo ccInfo, TransactionInfo tranInfo, DescriptiveInfo descInfo)
		{
			if (tranInfo == null) throw new ApplicationException("Transaction information missing");
			if (ccInfo == null) throw new ApplicationException("Credit Card information missing");

			return FormulateRequest("VOID", ccInfo, tranInfo, descInfo);
		}

		/// <summary>
		/// Cancel.
		/// </summary>
		public static ParsedResponse Cancel(CreditCardInfo ccInfo, TransactionInfo tranInfo, DescriptiveInfo descInfo)
		{
			bool bEncrypted = CreditCardIsEncrypted(ccInfo);
			string sGatewayInfo = FormulateGatewayInfo(tranInfo);
			XmlDocument docRequest = FormulateCancelRequest(ccInfo, tranInfo, descInfo);
			XmlDocument docResponse = SendRequest(docRequest, sGatewayInfo, bEncrypted);
			return GetParsedResponse(docResponse);
		}

		private static void LogRequest(XmlDocument docRequest)
		{
			XmlNode copy = docRequest.Clone() ;

			// strip out confidential information
			string[] HiddenFields = { "//cardnumber", "//cvmvalue" } ;
			foreach(string hfield in HiddenFields)
			{
				XmlElement xe = (XmlElement)copy.SelectSingleNode(hfield) ;
				if (xe != null)
					xe.InnerText = "[HIDDEN]" ;
			}

			CommonLib.Tracer.Trace("LinkPointAPIGateway3", "RequestPacket", "VER", copy.OuterXml) ;
		}
		public static XmlDocument SendRequest(XmlDocument docRequest, string gatewayInfo, bool cardNumberIsEncrypted)
		{
			string sResponse = null;
			try
			{
#if LQB_NET45
                global::LendingQBInfrequentChangeLib.com.meridianlink.ccs1.Linkpoint commAgent = new global::LendingQBInfrequentChangeLib.com.meridianlink.ccs1.Linkpoint();
#else
				CCGateway.com.meridianlink.ccs1.Linkpoint commAgent = new CCGateway.com.meridianlink.ccs1.Linkpoint();
#endif
				commAgent.Url = AppSettings.Linkpoint_Webservice_Url;

				LogRequest(docRequest) ;
				sResponse = commAgent.Transmit(gatewayInfo, docRequest.DocumentElement.OuterXml, cardNumberIsEncrypted);
			}
			catch (Exception ex)
			{
				int nTimeIndex = ex.Message.IndexOf("CCGATEWAYNETSERVICE TIMEOUT");
				if (nTimeIndex >= 0)
					throw new System.Net.WebException("TIMEOUT", System.Net.WebExceptionStatus.Timeout);
				else
					throw ex;
			}

			// The response is in an XML-like format, but doesn't include a top element for some reason!
			XmlDocument doc = new XmlDocument();
			if (sResponse.IndexOf("<response>") < 0)
			{
				XmlElement elemResponse = doc.CreateElement("response");
				doc.AppendChild(elemResponse);
				elemResponse.InnerXml = sResponse;
			}
			else
			{ // 04/10/06-For some responses, it's missing the response tag, for others, it has them. Stupid LinkPoint
				doc.LoadXml(sResponse) ;
			}
			return doc;
		}

		private static XmlDocument FormulateRequest(string type, CreditCardInfo ccInfo, TransactionInfo tranInfo, DescriptiveInfo descInfo)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml("<order/>");

			AddBilling(doc, ccInfo);
			AddTransactionDetails(doc, tranInfo);
			AddOrderOptions(doc, type, tranInfo);
			AddPayment(doc, tranInfo);
			AddCreditCard(doc, ccInfo);
			AddMerchantInfo(doc, tranInfo);
			AddItems(doc, descInfo);
			AddNotes(doc, descInfo);

			return doc;
		}

		private static void AddBilling(XmlDocument doc, CreditCardInfo ccInfo)
		{
			XmlElement billing = AddChild(doc.DocumentElement, "billing");
			AddChild(billing, "name", ccInfo.Name);
			AddChild(billing, "address1", ccInfo.Address);
			AddChild(billing, "city", ccInfo.City);
			AddChild(billing, "state", ccInfo.State);
			AddChild(billing, "zip", ccInfo.Zip);

			System.Text.StringBuilder builder = new System.Text.StringBuilder(ccInfo.Address.Length);
			foreach (char ch in ccInfo.Address)
			{
				if (char.IsDigit(ch))
					builder.Append(ch);
				else if (ch == ' ')
					break;
			}
			AddChild(billing, "addrnum", builder.ToString());
		}

		private static void AddTransactionDetails(XmlDocument doc, TransactionInfo tranInfo)
		{
			XmlElement tran = AddChild(doc.DocumentElement, "transactiondetails");
			AddChild(tran, "oid", tranInfo.OID);
			AddChild(tran, "taxexempt", "Y");
		}

		private static void AddOrderOptions(XmlDocument doc, string type, TransactionInfo tranInfo)
		{
			XmlElement options = AddChild(doc.DocumentElement, "orderoptions");
			AddChild(options, "ordertype", type);
			string sResult = (tranInfo.IsTest) ? "GOOD" : "LIVE";
			AddChild(options, "result", sResult);
		}

		private static void AddPayment(XmlDocument doc, TransactionInfo tranInfo)
		{
			XmlElement payment = AddChild(doc.DocumentElement, "payment");
			AddChild(payment, "tax", "0");
			AddChild(payment, "chargetotal", tranInfo.Charge.ToString("F2"));
		}

		private static void AddCreditCard(XmlDocument doc, CreditCardInfo ccInfo)
		{
			XmlElement cc = AddChild(doc.DocumentElement, "creditcard");
			string sCardNumber = CreditCardIsEncrypted(ccInfo) ? ccInfo.EncryptedNumber : ccInfo.Number;
			AddChild(cc, "cardnumber", sCardNumber);
			AddChild(cc, "cardexpmonth", ccInfo.ExpMonth.ToString("D2"));
			AddChild(cc, "cardexpyear", ccInfo.ExpYear.ToString().Substring(2));
			if (ccInfo.CVV != null)
			{
				AddChild(cc, "cvmvalue", ccInfo.CVV);
				AddChild(cc, "cvmindicator", "provided");
			}
		}

		private static void AddMerchantInfo(XmlDocument doc, TransactionInfo tranInfo)
		{
			XmlElement merchant = AddChild(doc.DocumentElement, "merchantinfo");
			AddChild(merchant, "configfile", tranInfo.StoreName);
		}

		private static void AddItems(XmlDocument doc, DescriptiveInfo descInfo)
		{
			if (descInfo == null) return;
			if (descInfo.OrderItems == null) return;
			if (descInfo.OrderItems.Length == 0) return;

			XmlElement items = AddChild(doc.DocumentElement, "items");
			foreach (OrderItem oItem in descInfo.OrderItems)
			{
				XmlElement item = AddChild(items, "item");
				AddChild(item, "id", oItem.ID);
				AddChild(item, "description", oItem.Description);
				AddChild(item, "price", oItem.Price.ToString("F2"));
				AddChild(item, "quantity", oItem.Quantity.ToString());
			}
		}

		private static void AddNotes(XmlDocument doc, DescriptiveInfo descInfo)
		{
			if (descInfo == null) return;
			if (!CommonLib.SafeString.HasValue(descInfo.ShortDescription)) return;

			XmlElement notes = AddChild(doc.DocumentElement, "notes");
			AddChild(notes, "comments", descInfo.ShortDescription);
		}

		private static XmlElement AddChild(XmlElement parent, string childName)
		{
			XmlElement child = parent.OwnerDocument.CreateElement(childName);
			parent.AppendChild(child);
			return child;
		}

		private static void AddChild(XmlElement parent, string childName, string data)
		{
			XmlElement child = AddChild(parent, childName);
			child.InnerText = data;
		}
	}
}
