﻿using System;
using System.Collections.Generic;
using System.IO;
namespace LookUpTableLib
{
    public class ComboTableLUBase
    {
        //! these members used to be static.
        protected List<NewTableVariable> schema;
        protected Dictionary<string, string> actionStrs;

         protected string pLookUp(List<string> varNames, List<string> vals)
        {
            // first turn range values into their appropriate range.
            string[] newVals = new string[vals.Count];
            for (int i = 0; i < vals.Count; i++)
            {
                if (schema[i].Name != varNames[i])
                    throw new Exception("name mismatch: " + schema[i].Name + " and " + varNames[i]);

                Type type;
                string errorMessages;
                if(!HelperFxns.isValidTypeName(schema[i].TypeName, out errorMessages, out type))
                    throw new Exception("unrecognized type" + schema[i].TypeName);

                if (type == typeof(Range))
                {
                    if (!hasRangeWithValueStr(schema[i].TypeName, schema[i].Values, vals[i], out newVals[i]))
                        throw new Exception("unable to find " + vals[i] + " in variable named: " + schema[i].Name);
                }
                else
                    newVals[i] = vals[i];
            }
            string uniqId = genUniqId(newVals);
            
            if (!actionStrs.ContainsKey(uniqId))
                throw new InvalidCTLookupException("actions dictionary does not contain an action for key: " + uniqId);

            return actionStrs[uniqId];
        }

        public bool hasRangeWithValueStr(string typeName, IEnumerable<string> rangeStrs, string value, out string rAsString)
        {
            rAsString = "";
            foreach (string rstr in rangeStrs)
            {
                Range r = new Range(typeName, rstr);
                if (r.containsValueStr(value))  // throws exception if invalid type.
                {
                    rAsString = r.ToString();
                    return true;
                }
            }
            return false;
        }

        public bool hasRangeWithValue(IEnumerable<Range> ranges, object value, out string rAsString)
        {
            rAsString = "";
            foreach (Range r in ranges)
            {
                if (r.containsValue(value))  // throws exception if invalid type.
                {
                    rAsString = r.ToString();
                    return true;
                }
            }
            return false;
        }

        public bool hasRange(IEnumerable<Range> ranges, Range range)
        {
            foreach (Range r in ranges)
            {
                if (r.isEqualTo(range))  // throws exception if invalid type.
                {
                    return true;
                }
            }
            return false;
        }

        private void AssertTrue(bool b)
        {
            if (!b)
                throw new Exception();
        }

        private void AssertTrue(bool p, string p_2)
        {
            if (!p)
                throw new Exception(p_2);
        }

        private string genUniqId(string[] splitLine)
        {
            string uniqId = "";

            for(int i = 0; i < schema.Count; i++)
            {
                var v = schema[i];
                uniqId += v.Name + ":" + splitLine[i].Trim() + ";";
            }

            return uniqId;
        }

    }

    public class InvalidCTLookupException : Exception
    {
        public InvalidCTLookupException(string msg)
            : base(msg)
        {
        }
    }

}