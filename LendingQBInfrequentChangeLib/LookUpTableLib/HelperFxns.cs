﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Text.RegularExpressions;
using System.CodeDom.Compiler;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;

namespace LookUpTableLib
{

    public class HelperFxns
    {


        public static void convertKeysToLowerCase(Dictionary<string,string> d)
        {
            if (d == null)
                return;
            string[] oldkeys = d.Keys.ToArray();
            foreach (var key in oldkeys)
            {
                string lowerkey = key.ToLower();
                string action = d[key];
                d.Remove(key);
                try
                {
                    d.Add(lowerkey, action);
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("table had too many keys.");
                }
            }
        }

        public static void convertKeysToLowerCaseForTableRows(Dictionary<string, TableRow> d)
        {
            if (d == null)
                return;
            string[] oldkeys = d.Keys.ToArray();
            foreach (var key in oldkeys)
            {
                string lowerkey = key.ToLower();
                TableRow action = d[key];
                d.Remove(key);
                d.Add(lowerkey, action);
            }
        }

        public static bool hasQuotes(string s)
        {
            if (s.Contains('\''))
                return true;
            if (s.Contains('"'))
                return true;
            return false;
        }

        public static string replaceCommas(string s)
        {
            return s.Replace(',', '_');
        }

        public static string getEnumName(string typeName)
        {
            //! assumes typeName is valid.
            typeName = typeName.Trim();
            string typeNameParam = "";
            if (typeName.StartsWith("Enum", StringComparison.InvariantCultureIgnoreCase))
            {
                string typeNameNoEnum = typeName.Substring(4).Trim();
                if (typeNameNoEnum.StartsWith(":"))
                {
                    typeNameParam = typeNameNoEnum.Substring(1).Trim();
                    if (!string.IsNullOrEmpty(typeNameParam))
                    {

                        return typeNameParam;
                    }
                    else
                    {
                        throw new Exception("invalid (empty) typeName" + typeName);
                    }
                }
                else
                {
                    throw new Exception(typeName + " starts with 'Enum', but is not followed by a ':'" +
                        " which indicates the start of the enumType.\n");
                }
            }

            return typeNameParam;
        }

        public static bool containsBackSlash(string inQuestion)
        {
            return inQuestion.Contains('\\');
        }


        

        public static bool canParseAs(Type type, string typeName, string val, out string errorMessages,
            out object parsed)
        {
            // any input reference variables will be obliterated, regardless of the outcome.
            // returns true iff can parse as the type specified.  
            // typeName is only used for ranges, otherwise it's value is ignored.

            errorMessages = "";
            parsed = null;

            if (type == typeof(Range))
            {
                try
                {
                    parsed = new Range(typeName, val);
                }
                catch (Exception e)
                {
                    errorMessages = e.Message;
                    return false;
                }
            }
            else if (type == typeof(Enum))
            {
                if(val.Length == 0)
                {
                    errorMessages = "vals must have length > 0";
                    return false;
                }
                if (char.IsLetter(val[0]) || val[0]=='_')
                    parsed = val;
                else
                {
                    errorMessages = "enums may not begin with a digit as did: " + val;
                    return false;
                }
            }
            else if ((type == typeof(string)))
            {
                parsed = val;
            }
            else // primitive non-string non-char
            {
                try
                {
                    //MethodInfo mi = type.GetMethod("Parse", new Type[] { typeof(string) });
                    // parsed = mi.Invoke(null, new object[] { val });

                    parsed = Convert.ChangeType(val, type);
                }
                catch (Exception)
                {
                    errorMessages = val + " is not a valid " + type.Name;
                    return false;
                }
            }

            return true;
        }
        // 3/6/2012 dd - DO NOT USE x_connectionStringDictionary directly.
        private static Dictionary<string, string> ConnectionStringDictionary = new Dictionary<string, string>();
        private static object ConnectionStringLock = new object();
        private static string DecryptSqlConnection(string encryptConnectionString)
        {
            // 3/6/2012 dd - This method will decrypt sql connection string.
            // DO NOT MAKE THIS METHOD PUBLIC. This method should only be use by this class.

            // 3/6/2012 dd - To avoid having decrypt connection everytime, I cached the decrypt string
            // in memory.
            string decrypt = string.Empty;
            lock (ConnectionStringLock)
            {
                if (ConnectionStringDictionary.TryGetValue(encryptConnectionString, out decrypt) == false)
                {
                    var builder = new SqlConnectionStringBuilder(encryptConnectionString);
                    builder.UserID = Decrypt(builder.UserID, Key, IV);
                    builder.Password = Decrypt(builder.Password, Key, IV);

                    decrypt = builder.ConnectionString;
                    ConnectionStringDictionary.Add(encryptConnectionString, decrypt);
                }
            }
            return decrypt;
        }

        // DO NOT CHANGE THESE!!!
        static readonly byte[] Key = { 27, 13, 172, 49, 204, 204, 123, 148, 9, 187, 19, 192, 245, 251, 50, 203, 19, 197, 179, 60, 81, 87, 201, 52, 171, 63, 27, 241, 33, 207, 39, 17 };
        static readonly byte[] IV = { 181, 113, 140, 201, 207, 18, 81, 193, 187, 102, 60, 213, 21, 121, 13, 105 };

        public static string Decrypt(string data, byte[] key, byte[] iv)
        {
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }
            byte[] inputBuffer = System.Convert.FromBase64String(data);
            using (SymmetricAlgorithm cryptoService = new RijndaelManaged())
            {
                cryptoService.Key = key;
                cryptoService.IV = iv;

                return DecryptImpl(inputBuffer, cryptoService);
            }

        }

        private static string DecryptImpl(byte[] inputBuffer, SymmetricAlgorithm cryptoService)
        {
            MemoryStream memoryStream = new MemoryStream(inputBuffer, 0, inputBuffer.Length);

            ICryptoTransform decryptor = cryptoService.CreateDecryptor();

            string ret;

            using(CryptoStream csStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
            using (StreamReader streamReader = new StreamReader(csStream))
            {
                ret = streamReader.ReadToEnd();
            }
            
            return ret;
        }

        public static bool canParseAsPrimitive(Type type, string val, out string errorMessages, out object parsed)
        {
            errorMessages = "";
            parsed = null;

            if (type == typeof(Range))
            {
                errorMessages = "Range type is not primitive.";
                return false;
            }
            else if ((type == typeof(string)) || (type == typeof(Enum)))
            {
                parsed = val;
            }
            else // primitive non-string non-char
            {
                try
                {
                    //MethodInfo mi = type.GetMethod("Parse", new Type[] { typeof(string) });
                    // parsed = mi.Invoke(null, new object[] { val });

                    parsed = Convert.ChangeType(val, type);
                }
                catch (Exception)
                {
                    errorMessages = val + " is not a valid " + type.Name;
                    return false;
                }
            }

            return true;
        }

        public static bool isValidTypeName(string typeName, out string errorMessages, out Type type)
        {
            errorMessages = "";
            type = null;
            typeName = typeName.Trim();

            List<string> allowedTypes = new string[]{"bool","char", "decimal", "double", "float", "int", "uint", 
                                        "long", "ulong", "short", "ushort", "string"}.ToList();

            // disallow bool, char, and string from range types.
            List<string> allowedRangeTypes = new string[]{"decimal", "double", "float", "int", "uint", 
                                        "long", "ulong", "short", "ushort"}.ToList();


            string[] allowedSystemTypes = {"Boolean", "Char", "Decimal", "Double", "Single", "Int32", "UInt32", 
                                              "Int64", "UInt64", "Int16", "UInt16", "String"};
            string[] allowedSystemRangeTypes = {"Decimal", "Double", "Single", "Int32", "UInt32", 
                                              "Int64", "UInt64", "Int16", "UInt16"};

            bool isAllowedType = allowedTypes.Contains(typeName, StringComparer.InvariantCultureIgnoreCase);
            if (isAllowedType)
            {
                type = Type.GetType("System." + allowedSystemTypes[allowedTypes.IndexOf(typeName.ToLower())]);
                return true;
            }

            bool isAllowedRangeType = false;
            string typeNameMinusRange = "";
            if (typeName.EndsWith("range", StringComparison.InvariantCultureIgnoreCase))
            {
                typeNameMinusRange = typeName.Remove(typeName.LastIndexOf("range", StringComparison.InvariantCultureIgnoreCase));
                typeNameMinusRange = typeNameMinusRange.Trim();
                if (allowedRangeTypes.Contains(typeNameMinusRange, StringComparer.InvariantCultureIgnoreCase))
                    isAllowedRangeType = true;
                else
                    errorMessages += "typeName: " + typeName + " ends with 'range', but is not a valid numeric type.\n";
            }
            if (isAllowedRangeType)
            {
                //?
                //!
                errorMessages = typeNameMinusRange.ToLower();
                type = typeof(Range);
                return true;
            }

            bool isAllowedEnumType = false;
            string typeNameParam = "";
            if (typeName.StartsWith("Enum", StringComparison.InvariantCultureIgnoreCase))
            {
                string typeNameNoEnum = typeName.Substring(4).Trim();
                if (typeNameNoEnum.StartsWith(":"))
                {
                    typeNameParam = typeNameNoEnum.Substring(1).Trim();
                    if (!string.IsNullOrEmpty(typeNameParam))
                    {
                        isAllowedEnumType = true;
                    }
                    else
                    {
                        errorMessages = "can't have Enum with empty name.";
                        type = null;
                    }
                }
                else
                {
                    errorMessages += typeName + " starts with 'Enum', but is not followed by a ':'" +
                        " which indicates the start of the enumType.\n";
                }
            }
            if (isAllowedEnumType)
            {
                
                type = typeof(Enum); //! ignore the typeNameParameter.
                return true;
                /*Type enumType = Type.GetType("ComboTableApp." + typeNameParam);
                if (enumType != null)
                {
                    type = enumType;
                    return true;
                }
                else
                {
                    errorMessages += "Unrecognized enum type: ComboTableApp." + typeNameParam + "\n";
                    errorMessages += "Perhaps your enum is in a class instead of freely floating in the Assembly\n";
                }*/
            }

            // if have made it this far, didn't recognize the type
            errorMessages += "Unrecognized typeName: " + typeName;
            return false;
        }

        public static bool areEqualOrIntersect(Type type, object o1, object o2,
            out string explanation)
        {
            //! assumes o1 and o2 are same type, given by type.
            explanation = "";
            if (Type.Equals(type, typeof(Range)))
            {
                bool intersect = ((Range)o1).intersects((Range)o2);
                if (intersect)
                {
                    explanation = "overlap b/t " + o1 + " and " + o2;
                    return true;
                }
            }
            else if ((Type.Equals(type, typeof(string))) || Type.Equals(type, typeof(Enum)))
            {
                //! treat strings and enums equivalently.
                if (((string)o1).Equals((string)o2, StringComparison.InvariantCultureIgnoreCase))
                {
                    explanation = "identical values: " + o1 + " and " + o2;
                    return true;
                    
                }
            }
            else if (Type.Equals(type, typeof(char)))
            {
                string sLeft = ((char)(o1)).ToString();
                string sRight = ((char)(o2)).ToString();
                if (sLeft.Equals(sRight, StringComparison.InvariantCultureIgnoreCase))
                {
                    explanation = "identical values: " + o1 + " and " + o2;
                    return true;
                }
            }
            else // type is aor primitive type (not string / char)
            {
                IComparable i1 = (IComparable)o1;
                if (i1.CompareTo(o2) == 0)
                {
                    explanation = "identical values: " + o1 + " and " + o2;
                    return true;
                }                
            }
            return false;
        
        }

        public static bool areEqual(Type type, object o1, object o2, out string explanation)
        {
            //! assumes o1 and o2 are same type, given by type.
            explanation = "";
            if (Type.Equals(type, typeof(Range)))
            {
                bool areEqual = ((Range)o1).isEqualTo((Range)o2);
                if (areEqual)
                {
                    explanation = "identical (Range) values: " + o1 + " and " + o2;
                    return true;
                }
            }
            else if ((Type.Equals(type, typeof(string))) ||(Type.Equals(type, typeof(Enum))))
            {
                //! treats enums and strings equivalently.
                if (((string)o1).Equals((string)o2, StringComparison.InvariantCultureIgnoreCase))
                {
                    explanation = "identical (string) values: " + o1 + " and " + o2;
                    return true;

                }
            }
            else if (Type.Equals(type, typeof(char)))
            {
                string sLeft = ((char)(o1)).ToString();
                string sRight = ((char)(o2)).ToString();
                if (sLeft.Equals(sRight, StringComparison.InvariantCultureIgnoreCase))
                {
                    explanation = "identical (char) values: " + o1 + " and " + o2;
                    return true;
                }
            }
            else // type is an enum or primitive type (not string / char)
            {
                IComparable i1 = (IComparable)o1;
                if (i1.CompareTo(o2) == 0)
                {
                    explanation = "identical ("+type.Name+") values: " + o1 + " and " + o2;
                    return true;
                }
            }
            return false;
        }

        public static bool areEquivalent(
            Type type1, string typeName1, string val1,
            Type type2, string typeName2, string val2,
            out string explanation, out Type matchedType)
        {
            object oVal1, oVal2;
            string eM1, eM2;
            if (canParseAs(type1, typeName1, val1, out eM1, out oVal1) && canParseAs(type1, typeName1, val2, out eM2, out oVal2))
            {
                matchedType = type1;
                if (areEqual(type1, oVal1, oVal2, out explanation))
                    return true;
            }
            if (canParseAs(type2, typeName2, val1, out eM1, out oVal1) && canParseAs(type2, typeName2, val2, out eM2, out oVal2))
            {
                matchedType = type2;
                if (areEqual(type2, oVal1, oVal2, out explanation))
                    return true;
            }

            matchedType = null;
            explanation = "cannot parse both vals: " +val1 + ", "+ val2 + " as either both "+typeName1+ " or "+typeName2; 
            return false;
        }


        public static bool LessThan(Type type, object o1, object o2)
        {
            //! assumes o1 and o2 are same type, given by type.
            if (Type.Equals(type, typeof(Range)))
            {
                bool isLessThan = Range.isFullyToTheLeftOf(((Range)o1),((Range)o2));
                if (isLessThan)
                {
                    return true;
                }
            }
            else if ((Type.Equals(type, typeof(string))) || (Type.Equals(type, typeof(Enum))))
            {
                if (string.Compare((string)o1, (string)o2, true) < 0)
                {
                    return true;
                }
            }
            else if (Type.Equals(type, typeof(char)))
            {
                string sLeft = ((char)(o1)).ToString().ToLower();
                string sRight = ((char)(o2)).ToString().ToLower();
                if (string.Compare(sLeft, sRight, true) < 0)
                {
                    return true;
                }
            }
            else // type is a primitive type (not string / char)
            {
                IComparable i1 = (IComparable)o1;
                if (i1.CompareTo(o2) < 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsEnumeration(Type t)
        {
            return t.IsEnum;
        }

        public static bool isValidIdentifierTester(string id)
        {
            // this disallows the @keyword identifier.  So @if and @bool would not be valid identifiers.
            return codeDomProvider.IsValidIdentifier(id);
        }

        //! taken (and adapted) from: 
        // http://stackoverflow.com/questions/1904252/is-there-a-method-in-c-to-check-if-a-string-is-a-valid-identifier
        static string startCharTypes = @"(\p{Lu}|\p{Ll}|\p{Lt}|\p{Lm}|\p{Lo}|\p{Nl})";
        static string extendCharTypes = @"(\p{Mn}|\p{Mc}|\p{Nd}|\p{Pc}|\p{Cf})";
        private static Regex validator = new Regex(string.Format("({0}|{1})", startCharTypes, extendCharTypes));

        private static CodeDomProvider codeDomProvider = CodeDomProvider.CreateProvider("C#");

        public static bool isValidIdentifier(string id)
        {
            // this disallows the @keyword identifier.  So @if and @bool would not be valid identifiers.

            return codeDomProvider.IsValidIdentifier(id);

        }
        public static bool isValidNamespaceIdentifier(string userNamespace)
        {
            if (userNamespace == null)
                return false;

            if (userNamespace.Length >= 3)
            {
                char firstChar = userNamespace[0];
                char lastChar = userNamespace[userNamespace.Length - 1];
                string middleString = userNamespace.Substring(1, userNamespace.Length - 2);
                string middleStringMinusPeriods = middleString.Replace(".", "");

                return isValidIdentifier(firstChar + middleStringMinusPeriods + lastChar);

            }
            else if (userNamespace.Length == 0)
                return false;
            else if (userNamespace.Length == 1 || userNamespace.Length == 2)
                return isValidIdentifier(userNamespace);
            else
                throw new Exception("invalid length of user provided namespace" + userNamespace);
            

        }
        

    }
}
