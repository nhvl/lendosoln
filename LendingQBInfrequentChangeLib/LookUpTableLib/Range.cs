﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Xml.Serialization;

namespace LookUpTableLib
{
    [Serializable]
    public class Range
    {
        private string typeName;
        [XmlIgnore]public Type type;

        //? add original string that created this?

        public bool firstIsMinusInf;    // (-Inf if true
        public bool secondIsInf;        // Inf) if true;
        
        public bool includesFirst;      // [ if true, ( if false.
        public bool includesSecond;     // ] if true, ) if false.

        public string sval1;
        public string sval2;

        public object val1;
        public object val2;

        public Range()
        {
        }

        public Range(string typeIn, string val)
        {
            typeName = typeIn.Trim();
            
            // disallow bool, char, and string from range types.
            List<string> allowedRangeTypes = new string[]{"decimal", "double", "float", "int", "uint", 
                                        "long", "ulong", "short", "ushort"}.ToList();

            string[] allowedSystemRangeTypes = {"Decimal", "Double", "Single", "Int32", "UInt32", 
                                              "Int64", "UInt64", "Int16", "UInt16"};

            bool isAllowedRangeType = false;
            string typeNameMinusRange = "";
            if (typeName.EndsWith("range", StringComparison.InvariantCultureIgnoreCase))
            {
                typeNameMinusRange = typeName.Remove(typeName.LastIndexOf("range", StringComparison.InvariantCultureIgnoreCase));
                typeNameMinusRange = typeNameMinusRange.Trim();
                if (allowedRangeTypes.Contains(typeNameMinusRange, StringComparer.InvariantCultureIgnoreCase))
                    isAllowedRangeType = true;

            }
            if (!isAllowedRangeType)
            {
                throw new Exception("unable to discern range type from: " + typeName);
            }
            else
            {
                string rangeValue = val;
                string systemTypeName = allowedSystemRangeTypes[allowedRangeTypes.IndexOf(typeNameMinusRange.ToLower())];
                type = Type.GetType("System." + systemTypeName);
                if ((!rangeValue.StartsWith("[")) && (!rangeValue.StartsWith("(")))
                {
                    throw new Exception("value: " + rangeValue + " did not start with [ or ( as it should.");
                }
                includesFirst = rangeValue.StartsWith("[");
                if ((!rangeValue.EndsWith("]")) && (!rangeValue.EndsWith(")")))
                {
                    throw new Exception("value: " + rangeValue + " did not end with ] or ) as it should.");
                }
                includesSecond = rangeValue.EndsWith("]");
                string innerRange = rangeValue.Substring(1, rangeValue.Length - 2);
                string[] rangeValues = innerRange.Split(new string[]{" "},
                    2000, 
                    StringSplitOptions.RemoveEmptyEntries); //! figure out what this delimiter should be.
                if (rangeValues.Length != 2)
                {
                    throw new Exception("value: " + rangeValue + " should have two values only.");
                }
                rangeValues[0] = rangeValues[0].Trim();
                sval1 = rangeValues[0];
                rangeValues[1] = rangeValues[1].Trim();
                sval2 = rangeValues[1];

                string errorMessages1;
                firstIsMinusInf = false;
                if (string.Compare(rangeValues[0], "Inf", true) == 0)
                {
                    throw new Exception("first val in range must be the smaller number, so cannot be Inf.  Perhaps you meant -Inf?");
                }
                if (string.Compare(rangeValues[0], "-Inf", true) == 0)
                {
                    firstIsMinusInf = true;
                }
                if (firstIsMinusInf && includesFirst)
                {
                    throw new Exception("first val is minus inf, but it was included.  Cannot include infinity since it is not a number."
                        + " Try (-Inf ... instead of [-Inf ... ");
                }
                if (!firstIsMinusInf)
                {
                    if (!HelperFxns.canParseAs(type, typeNameMinusRange, rangeValues[0], out errorMessages1, out val1))
                    {
                        throw new Exception("the first value of the range is: " + rangeValues[0]
                            + " which is not a valid " + typeNameMinusRange);
                    }
                }

                string errorMessages2;
                secondIsInf = false;
                if (string.Compare(rangeValues[1], "-Inf", true) == 0)
                {
                    throw new Exception( "second val in range must be the larger number, so cannot be -Inf.  Perhaps you meant Inf?");
                }
                if (string.Compare(rangeValues[1], "Inf", true) == 0)
                {
                    secondIsInf = true;
                }
                if (secondIsInf && includesSecond)
                {
                    throw new Exception("second val is inf, but it was included.  Cannot include infinity since it is not a number."
                        + " Try ... Inf) instead of ... Inf]");
                }
                if (!secondIsInf)
                {
                    if (!HelperFxns.canParseAs(type, typeNameMinusRange, rangeValues[1], out errorMessages2, out val2))
                    {
                       throw new Exception("the second value of the range is: " + rangeValues[1]
                            + " which is not a valid " + typeNameMinusRange);
                    }
                }
                if (!firstIsMinusInf && !secondIsInf)
                {
                    int comparison = ((IComparable)val1).CompareTo(val2);
                    if (comparison > 0)
                    {
                        throw new Exception(rangeValues[0] + " is greater than " + rangeValues[1]);
                    }
                    if (comparison == 0)
                    {
                        if (!includesSecond && !includesFirst)
                        {
                            string errorMessages= "";
                            errorMessages += "Should not have noninclusive single valued ranges." +
                                "\nval1 = " + rangeValues[0] + " which equals " + " val2 = " + rangeValues[1];
                            errorMessages += "\nTry instead [" + rangeValues[0] + ", " + rangeValues[1]
                                + "] to include just that number.";

                            throw new Exception(errorMessages);
                        }
                    }
                }
            }
        }

        public string TypeName
        {
            get
            {
                return typeName;
            }
            set
            {
                typeName = value.Trim();

                // disallow bool, char, and string from range types.
                List<string> allowedRangeTypes = new string[]{"decimal", "double", "float", "int", "uint", 
                                        "long", "ulong", "short", "ushort"}.ToList();

                string[] allowedSystemRangeTypes = {"Decimal", "Double", "Single", "Int32", "UInt32", 
                                              "Int64", "UInt64", "Int16", "UInt16"};

                bool isAllowedRangeType = false;
                string typeNameMinusRange = "";
                if (typeName.EndsWith("range", StringComparison.InvariantCultureIgnoreCase))
                {
                    typeNameMinusRange = typeName.Remove(typeName.LastIndexOf("range", StringComparison.InvariantCultureIgnoreCase));
                    typeNameMinusRange = typeNameMinusRange.Trim();
                    if (allowedRangeTypes.Contains(typeNameMinusRange, StringComparer.InvariantCultureIgnoreCase))
                    {
                        isAllowedRangeType = true;
                        string systemTypeName = allowedSystemRangeTypes[allowedRangeTypes.IndexOf(typeNameMinusRange.ToLower())];
                        type = Type.GetType("System." + systemTypeName);
                    }

                }
                if (!isAllowedRangeType)
                {
                    throw new Exception("unable to discern range type from: " + typeName);
                }
            }
        }

        public bool intersects(Range range2)
        {
            if (!this.type.Equals(range2.type))
            {
                throw new Exception("ranges must have same type in order to be compared.");
            }
            if (Range.isFullyToTheLeftOf(this, range2) || Range.isFullyToTheLeftOf(range2, this))
                return false;
            return true;
        }

        public static bool isFullyToTheLeftOf(Range r1, Range r2)
        {
            // returns if the first range (the "left" one in the parameter list) is fully to the 
            // left of  the second or "right" range. 
            /*
             * Note that for the below, all comparisons must handle Inf value cases.
             * 
             * if(range1.val1 < range2.val1) and (range1.val2 < range2.val1)
             *  return true;
             *  
             * if(range1.val1 < range2.val1) and (range1.val2 == range2.val1)
             *  if(range1.includesVal2 and range2.includesVal1) // test for pt intersection.
             *   return false  
             *  else
             *   return true
             *   
             * return false
             * 
             * False:
             *   r1:              ========
             *   r2: ==========
             * True:
             *   r1: =========
             *   r2:            =========
             * False:
             *   r1: ========
             *   r2:      ============
             *
             */

            //MethodInfo mi = r1.type.GetMethod("CompareTo", new Type[] { r1.type });
            IComparable r1v1comparer = (IComparable)r1.val1;
            IComparable r1v2comparer = (IComparable)r1.val2;
            int r1v1MinusR2v1, r1v2MinusR2v1;
            if (!r2.firstIsMinusInf)
            {
                if (!r1.firstIsMinusInf)
                    r1v1MinusR2v1 = r1v1comparer.CompareTo(r2.val1); //(int)mi.Invoke(r1.val1, new object[] { r2.val1 });
                else
                    r1v1MinusR2v1 = -1;

                if (!r1.secondIsInf)
                    r1v2MinusR2v1 = r1v2comparer.CompareTo(r2.val1); //(int)mi.Invoke(r1.val2, new object[] { r2.val1 });
                else
                    r1v2MinusR2v1 = 1;
                
            }
            else // if r2.firstIsMinusInf
            {
                if (r1.firstIsMinusInf)
                    r1v1MinusR2v1 = 0;
                else
                    r1v1MinusR2v1 = 1;

                r1v2MinusR2v1 = 1;
            }

            if ((r1v1MinusR2v1 < 0) && (r1v2MinusR2v1 < 0))  // definitely no leftIntersection.
            {
                return true;
            }
            if ((r1v1MinusR2v1 < 0) && (r1v2MinusR2v1 == 0))
            {
                if (r1.includesSecond && r2.includesFirst)    
                {
                    return false;        // pt intersection.
                }
                else
                    return true;
            }

            return false;
        }

        public override string ToString()
        {
            string ret = "";
            if (includesFirst)
                ret += "[";
            else
                ret += "(";

            ret += sval1;
            ret += " ";
            ret += sval2;

            if (includesSecond)
                ret += "]";
            else
                ret += ")";

            return ret;

        }

        public bool isEqualTo(Range range2)
        {
            // ignores if the typeName string are different.
            if (firstIsMinusInf != range2.firstIsMinusInf)
                return false;
            if (secondIsInf != range2.secondIsInf)
                return false;
            if (includesFirst != range2.includesFirst)
                return false;
            if (includesSecond != range2.includesSecond)
                return false;

            if (type != range2.type)
                return false;
            
            if (!firstIsMinusInf)
            {
                string explanation;
                if (!HelperFxns.areEqualOrIntersect(type, this.val1, range2.val1, out explanation))
                    return false;
            }
       
            if (!secondIsInf)
            {
                string explanation;
                if (!HelperFxns.areEqualOrIntersect(type, this.val2, range2.val2, out explanation))
                    return false;
            }
            return true;
        }

        public bool containsValue(object value)
        {
            // throws an exception if the value isn't the same type as the range.

            if (value.GetType() != type)
                throw new Exception("value: " + value + " is of type: " + value.GetType() +
                "\n but Range: " + this + " is of type: " + type);

            Range ptRange = new Range(typeName, "[" + value + " " + value + "]");
            return this.intersects(ptRange);
        }

        public bool containsValueStr(string value)
        {
            string errorMessages;
            object oValue;
            if (!HelperFxns.canParseAsPrimitive(type, value, out errorMessages, out oValue))
                throw new Exception("could not parse " + value + " as " + type.Name);

            Range ptRange = new Range(typeName, "[" + value + " " + value + "]");
            return this.intersects(ptRange);
        }

        //! would like to make signature identical to Int32.Parse("1"),
        //! but each Range must have a type associated with it, which makes that difficult/impossible.
        /*
        public static Range Parse(string val)
        {

        }*/

    }
}
