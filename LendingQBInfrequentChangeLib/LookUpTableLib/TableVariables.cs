﻿using System;
using System.Collections;  // without this, compiler will ask for IEnumerable<T> and it won't work at all.
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace LookUpTableLib
{
    [Serializable][XmlInclude(typeof(Range))]
    public class NewTableVariables : IEnumerable
    {
        private List<NewTableVariable> tvVars;

        public NewTableVariables()
        {
            tvVars = new List<NewTableVariable>();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public List<NewTableVariable>.Enumerator GetEnumerator()
        {
            return tvVars.GetEnumerator();
        }

        //? for both the constructor and the method Add, should the local copies be copies, or references
        // to the same varaible?
        public NewTableVariables(IEnumerable<NewTableVariable> tvs)
        {
            tvVars = new List<NewTableVariable>();
            
            foreach (var tv in tvs)
            {
                this.Add(tv);
            }
        }

        public TableVariablesNumerable ListAllPerms()
        {
            // Lists all permutations of the values of the variables, with the first variable values
            // changing the least often.
            return new TableVariablesNumerable(this);
        }

        private void checkValidity(NewTableVariable tv)
        {
            if (tv == null)
                throw new Exception("can't add null tableVariable to collection.");
            if (tv.Values == null)
                throw new Exception("can't add tableVariable with null values.");
            if (tv.Values.Length == 0)
                throw new Exception("can't add tableVariable with empty values list.");
        }

        public void undoDecimalCommas()
        {
            foreach(var v in this)
            {
                v.undoDecimalCommas();
            }
        }

        public bool increment()
        {
            // returns false if shouldn't increment the variables again.
            int i;
            for(i = tvVars.Count-1; i >= 0; i--)
            {
                bool continueLoop = false;
                var v = tvVars[i];
                v.CurrValueIndex = v.CurrValueIndex+1;
                if (v.CurrValueIndex == 0)
                {
                    continueLoop = true;
                }

                if (!continueLoop)
                    break;

            }
            if (i == -1)
                return false;
            return true;

        }

        public bool decrement()
        {
            // returns false if shouldn't increment the variables again.
            int i;
            for (i = tvVars.Count - 1; i >= 0; i--)
            {
                bool continueLoop = false;
                var v = tvVars[i];
                v.CurrValueIndex = v.CurrValueIndex-1;
                if (v.CurrValueIndex == v.Values.Length-1)
                {
                    continueLoop = true;
                }

                if (!continueLoop)
                    break;

            }
            if (i == -1)
                return false;

            return true;
        }

        public string uniqueId()
        {
            string s = "";
            for(int i = 0; i < tvVars.Count; i++)
            {
                var v = tvVars[i];
                s += v.Name.ToLower() + ":" + v.getCurrValue().ToLower() + ";";
            }

            return s;
        }

        public void Add(NewTableVariable tv)
        {
            checkValidity(tv);
            tvVars.Add(tv);
        }

        public void AddAt(int index, NewTableVariable tv)
        {
            checkValidity(tv);

            tvVars.Insert(index, tv);
        }

        public void Remove(NewTableVariable tv)
        {
            int ind = tvVars.IndexOf(tv);
            tvVars.RemoveAt(ind);
        }

        public void RemoveAt(int ind)
        {
            tvVars.RemoveAt(ind);
        }

        public int Count
        {
            get
            {
                return tvVars.Count;
            }
        }

        public NewTableVariable this[int index] 
        {
            get
            {
                return tvVars[index];
            }
            set
            {
                checkValidity(value);
                tvVars[index] = value;
            }
        }

        public bool IsDeeplyEqualTo(NewTableVariables tvVarsIn)
        {
            if (tvVarsIn == null)
                return false;

            if (this.Count != tvVarsIn.Count)
                return false;

            for(int i = 0; i < tvVarsIn.Count; i++)
            {
                if (!tvVars[i].IsDeeplyEqualToNoRecurse(tvVarsIn[i])) //! rename to ShallowlyEqual?
                    return false;
            }

            return true;
            
        }

        public NewTableVariables swapVars(int[] newpositions)
        {
            // note this has side effects.

            if (this.Count != newpositions.Length)
                throw new Exception("positions array length is too long/short to swap vars");
            
            NewTableVariable[] tvArr = tvVars.ToArray();
            NewTableVariable[] newTVArr = new NewTableVariable[tvArr.Length];

            for (int i = 0; i < tvArr.Length; i++)
            {
                newTVArr[newpositions[i]] = tvArr[i];
            }

            tvVars.Clear();
            for (int i = 0; i < tvArr.Length; i++ )
            {
                tvVars.Add(newTVArr[i]);
            }

            return this;
        }

        public NewTableVariable variableWithName(string variablename)
        {
            foreach(var currVar in tvVars)
            {
                if (currVar.isNamed(variablename))
                    return currVar;
            }
            return null;
        }

        //? for both rename and renameValue:
        //      should I add a check to make sure newVarName isn't already one of the variable names?
        public void rename(string oldVarName, string newVarName)
        {
            foreach (var v in tvVars)
            {
                if (v.isNamed(oldVarName))
                {
                    v.Name = newVarName;
                    break;
                }
            }
        }

        public void renameValue(string varName, string oldValName, string newValName)
        {
            foreach (var v in tvVars)
            {
                if (v.isNamed(varName))
                {
                    v.renameValue(oldValName, newValName);
                    break;
                }
            }
        }

        public void forceTypes()
        {
            //? should this even need to be called publicly?
            //? shouldn't this be called essentially upon creation?
            foreach (var v in tvVars)
            {
                v.forceType();
            }
            
        }

        public NewTableVariable[] asArray() //? should i change all references to this?
        {
            return tvVars.ToArray();
        }

        public NewTableVariables deepClone()
        {
            NewTableVariables y = new NewTableVariables();
            foreach (var v in this)
            {
                NewTableVariable x = v.deepClone();
                y.Add(x);
            }
            return y;

        }

        public IEnumerable<string> missingVariableNamesWRT(NewTableVariables oldvariables)
        {
            List<string> theMissings = new List<string>();
            foreach (var oldvar in oldvariables)
            {
                bool isMissing = true;
                foreach (var newvar in this)
                {
                    if (newvar.isNamed(oldvar.Name))
                    {
                        isMissing = false;
                        break;
                    }
                }
                if (isMissing)
                {
                    theMissings.Add(oldvar.Name);
                }
            }
            return theMissings;  
        }

        public IEnumerable<string> commonVariableNamesWRT(NewTableVariables oldVars)
        {
            List<string> theCommons = new List<string>();
            foreach (var oldvar in oldVars)
            {
                bool isCommon = false;
                foreach (var newvar in this)
                {
                    if (newvar.isNamed(oldvar.Name))
                    {
                        isCommon = true;
                        break;
                    }
                }
                if (isCommon)
                {
                    theCommons.Add(oldvar.Name);
                }
            }
            return theCommons; 
        }

        public IEnumerable<string> extraVariableNamesWRT(NewTableVariables oldVars)
        {
            List<string> theExtras = new List<string>();
            foreach (var newvar in this)
            {
                bool isExtra = true;
                foreach (var oldvar in oldVars)
                {
                    if (newvar.isNamed(oldvar.Name))
                    {
                        isExtra = false;
                        break;
                    }
                }
                if (isExtra)
                {
                    theExtras.Add(newvar.Name);
                }
            }
            return theExtras; 
        }

        public int findPositionOfNewVarWRT(NewTableVariables oldVars, string newVarName)
        {
            //! note this only works after all removes detected.
            int j = 0;
            int offset = 0;

            for(int i = 0; i < this.Count; i++)
            {
                
                var v = this[i];

                if (v.isNamed(newVarName))
                    return i-offset;
                if (j < oldVars.Count)
                {
                    var oldVar = oldVars[j];
                    if (!v.isNamed(oldVar.Name))
                        offset++;
                    else
                        j++;
                }
                else
                    return j+1; //?
            }
            throw new Exception("woops.  developer should see TableVariable's findPositionOfNewVarWRT");
        }

        public int[] permutationWRT(NewTableVariables newVar)
        {
            if (this.Count != newVar.Count)
                throw new Exception("error in TableVariable.obtainPermutationWRT(). mismatch variable lengths.");
            int[] permutation = new int[this.Count];
            for (int i = 0; i < permutation.Length; i++)
            {
                bool hasCommon = false;
                for (int j = 0; j < permutation.Length; j++)
                {
                    if (this[i].isNamed(newVar[j].Name))
                    {
                        permutation[i] = j;
                        hasCommon = true;
                        break;
                    }
                }
                if (!hasCommon)
                    throw new Exception("error in TableVariable.obtainPermutationWRT().  uncommon variable name.");
            }
            return permutation;
        }


        public bool isEmpty()
        {
            return (tvVars.Count == 0);
        }

        public bool IsDeeplyEqualToCaseSensitive(NewTableVariables newVars)
        {
            if (newVars == null)
                return false;

            if (this.Count != newVars.Count)
                return false;

            for (int i = 0; i < tvVars.Count; i++)
            {
                var otherTV = newVars[i];
                if (otherTV == null)
                    return false;
                if (!this[i].IsDeeplyEqualToNoRecurseCaseSensitive(otherTV))
                    return false;
            }
            return true;

        }

        public bool hasVariable(string varName)
        {
            // case insensitive.
            foreach (var v in this)
            {
                if (v.isNamed(varName))
                    return true;
            }
            return false;
        }

        public bool hasVariableCaseSensitive(string varName)
        {
            foreach (var v in this)
            {
                if (v.isNamedCaseSensitive(varName))
                    return true;
            }
            return false;
        }

        public bool hasValueVariable(string varName, string value)
        {
            // case insensitive.
            foreach (var v in this)
            {
                if (v.isNamed(varName))
                    return v.hasValue(value);
            }
            return false;
        }

        public bool hasValueCaseSensitiveVariable(string varName, string value)
        {
            // case sensitive only on the values.  variables are matched insensitively.
            foreach (var v in this)
            {
                if (v.isNamed(varName))
                    return v.hasValueCaseSensitive(value);
            }
            return false;
        }

        public void Reset()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].CurrValueIndex = 0;
            }
        }

        public string asMultiLineText()
        {
            string ret = "";
            foreach (var v in this)
            {
                ret += v.asLine() + "\n";
            }
            return ret;
        }

        public bool hasVariable(NewTableVariable tv1)
        {
            if(tv1 == null)
                return false;
            
            NewTableVariable candidate = this.variableWithName(tv1.Name);
            if (candidate == null)
                return false;

            if (!candidate.IsDeeplyEqualToIgnoringOrderAndSubVariables(tv1))
                return false;

            return true;            
        }
    }

    public class TableVariablesNumerable : IEnumerable
    {
        private NewTableVariables tvs;
        public TableVariablesNumerable(NewTableVariables tvsIn)
        {
            tvs = tvsIn;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public TableVariablesNumerator GetEnumerator()
        {
            return new TableVariablesNumerator(tvs);
        }
    }

    public class TableVariablesNumerator : IEnumerator
    {
        private NewTableVariables tvs;
        private bool hasSeenBefore;

        public TableVariablesNumerator(NewTableVariables tvsIn)
        {
            hasSeenBefore = false;
            tvs = tvsIn;
            tvs.decrement();
        }

        public bool MoveNext()
        {
            if (!tvs.increment())
                if (hasSeenBefore)
                    return false;
                else
                    hasSeenBefore = true;
            return true;
        }

        public void Reset()
        {
            hasSeenBefore = false;
            tvs.Reset();
            tvs.decrement();
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public NewTableVariables Current
        {
            get
            {
                return tvs;
            }
        }


    }
}
