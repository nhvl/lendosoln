﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LookUpTableLib
{
    [Serializable]  
    public class NewTableVariable
    {
        //? what should become properties?
        public string name = ""; 
        private string typeName;  // accessed as a property...
        [XmlIgnore] public Type type;
        private string[] values;
        [XmlIgnore] private object[] oValues;  
        public int currValueIndex = 0;
        
        private static CodeDomProvider codeDomProvider = CodeDomProvider.CreateProvider("C#");

        public string TypeName
        {
            //? should this solely be concerned w/ TypeName?
            get{ return typeName; }
            set
            {
                if (value == null)
                {
                    type = typeof(Enum);
                }
                else
                {
                    typeName = value;
                    //? change this to get the type?
                }
                checkSelfValidityAndAssignOtherVariables();
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                this.updateType();
            }
        }

        public string[] Values
        {
            get
            {
                return values;
            }
            set
            {
                values = (string[])value.Clone();
                checkSelfValidityAndAssignOtherVariables();
            }
        }

        //?
        /*public string Values[int ind]
        {
        }*/


        public int CurrValueIndex
        {
            get
            {
                return currValueIndex;
            }
            set
            {
                currValueIndex = value;
                if (values != null)
                {
                    if (values.Length > 1)
                    {
                        if (value >= 0)
                        {
                            currValueIndex = value % values.Length;
                        }
                        else // if (value < 0)
                        {
                            currValueIndex = values.Length + (value % values.Length);
                        }

                    }
                    else if (values.Length == 1)
                    {
                        currValueIndex = 0;
                    }
                }
                //! checkSelfValidityAndAssignOtherVariables();
            }
        }

        public NewTableVariable()  // need this for XMLSerialization.
        {
        }

        public NewTableVariable(string nameIn, string[] vals)
        {
            name = nameIn;
            typeName = "Enum:" + nameIn;
            values = (string[])vals.Clone();  // note shallow copy, not copying reference to list.

            checkSelfValidityAndAssignOtherVariables();
        }

        public NewTableVariable(string nameIn, string typeIn, string[] vals)
        {
            name = nameIn;
            typeName = typeIn;
            values = (string[])vals.Clone();  // note shallow copy, not copying pointer to list.

            checkSelfValidityAndAssignOtherVariables();
        }

        private void checkSelfValidityAndAssignOtherVariables()
        {
            // checks name, type, values.
            // throws exception if definition isn't valid.

            // check the name (is it a valid ddentifier?)
            if (!codeDomProvider.IsValidIdentifier(name))
                throw new Exception("invalid identifier: " + name);

            // check the type validity.
            Type newType;
            string errorMessages;
            if (HelperFxns.isValidTypeName(typeName, out errorMessages, out newType))
                type = newType;
            else
                throw new Exception(errorMessages);

            if (values == null)
                return;

            //? add something about currValueIndex?

            // set the object values, given the type and value strings.
            oValues = new object[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                var val = values[i];
                string errorMessages2;
                object oVal;
                if (!HelperFxns.canParseAs(type, typeName, val, out errorMessages2, out oVal))
                    throw new Exception(errorMessages2);
                oValues[i] = oVal;
            }

            // check that no redundancy of values exist within the oValues
            for (int i = 0; i < oValues.Length; i++)
            {
                object oVal1 = oValues[i];
                for (int j = i + 1; j < oValues.Length; j++)
                {
                    object oVal2 = oValues[j];
                    string explanation;
                    if (HelperFxns.areEqual(type, oVal1, oVal2, out explanation))
                        throw new Exception(explanation);
                }
            }

            // if the variable is of type Enum, make sure that the values are valid identifers as well.
            if (type == typeof(Enum))
            {
                foreach (var val in values)
                {
                    if (!codeDomProvider.IsValidIdentifier(val))
                        throw new Exception("Enums must have valid identifiers as values.  variable named: " + name +
                            " has a value: " + val + " that is not a valid identifier.");
                }
            }
        }

        public string getCurrValue()
        {
            return Values[CurrValueIndex];
        }

        public void addValue(string val)
        {
            if (values != null)
            {
                string[] valuestmp = new string[values.Length + 1];
                values.CopyTo(valuestmp, 0);
                valuestmp[valuestmp.Length - 1] = val;
                Values = valuestmp;
            }
            else
            {
                Values = new string[1] { val };
            }
        }

        public void addValue(string val, int position)
        {
            List<string> vals = Values.ToList<string>();
            vals.Insert(position, val);
            Values = vals.ToArray();
        }

        public void removeValue(string val)
        {
            List<string> vals = Values.ToList<string>();
            vals.Remove(val);
            Values = vals.ToArray();
        }

        public override string ToString()
        {
            string asString = "";
            asString += "name: " + Name + "<br/>";
            asString += "curValueIndex: " + CurrValueIndex + "<br/>";
            asString += "values: <br/>";
            foreach (var val in Values)
            {
                asString += val + "<br/>";
            }
            asString += "<br/>";
            /*if (varToIncrement != null)
            {
                asString += varToIncrement.ToString();
            }*/
            return asString;
            
        }

        public string asLine()
        {
            //! note, this will give an error if values is not set.
            string ret = "";
            ret += Name + "(";
            ret += TypeName;
            ret += "):";
            for (int i = 0; i < Values.Length-1; i++)
            {
                ret += Values[i] + " "; //! used to be a comma here.
            }
            ret += Values[Values.Length - 1];
            return ret;
        }

        public string asTypelessLine()
        {
            //! note, this will give an error if values is not set.
            string ret = "";
            ret += Name + ":";
            for (int i = 0; i < Values.Length - 1; i++)
            {
                ret += Values[i] + " "; //! used to be a comma here.
            }
            ret += Values[Values.Length - 1];
            return ret;
        }

        public bool isNamedCaseSensitive(string varname)
        {
            if (Name == varname)
                return true;
            return false;
        }

        public bool hasValue(string value)
        {
            // insensitive (2.0 = 2 for floats, CAPPED = capped for strings, etc.)
            string errorMessages, errorMessages2;
            Type type;
            HelperFxns.isValidTypeName(this.TypeName, out errorMessages, out type);
            object oValue;
            if (!HelperFxns.canParseAs(type, this.TypeName, value, out errorMessages2, out oValue))
                return false;
            for (int i = 0; i < Values.Length; i++)
            {
                object oValI;
                string errorMessages3;
                HelperFxns.canParseAs(type, this.TypeName, Values[i], out errorMessages3, out oValI);

                string explanation;
                if (HelperFxns.areEqual(type, oValue, oValI, out explanation))
                    return true;
            }
            return false;
        }

        public bool hasValueCaseSensitive(string value)
        {
            for (int i = 0; i < Values.Length; i++)
            {
                if (Values[i] == value)
                    return true;
            }
            return false;
        }

        public bool isNamed(string otherName)
        {
            return this.Name.Equals(otherName, StringComparison.InvariantCultureIgnoreCase);
        }

        public void updateType()
        {
            string eM1;
            Type type;
            if (this.TypeName != null)
            {
                if (!HelperFxns.isValidTypeName(this.TypeName, out eM1, out type))
                    this.TypeName = "Enum:" + this.Name;
                //! if (typeof(Enum).Equals(type))
                //!    this.TypeName = "Enum:" + this.Name;
            }
            else
                this.TypeName = "Enum:" + this.Name;
            
        }

        public void renameValue(string oldValName, string newValName)
        {
            // this version of this method doesn't do recursion.

            string  eM1, eM2;
            object oOldVal, oNewVal;
            //? would it be better to do the below via searching for the oldValName to get the index?
            HelperFxns.canParseAs(type, this.TypeName, oldValName, out eM1, out oOldVal); 
            HelperFxns.canParseAs(type, this.TypeName, newValName, out eM2, out oNewVal);


            bool foundVal = false;
            for (int i = 0; i < Values.Length; i++)
            {
                string explanation;
                if (HelperFxns.areEqual(type, oValues[i], oOldVal, out explanation))
                {
                    Values[i] = newValName;
                    oValues[i] = oNewVal;
                    foundVal = true;
                    break;
                }
            }
            if (!foundVal)
                throw new Exception("value: " + newValName + " not found in variable named: "+ Name);
        }

        public List<string> extraValuesWRT(NewTableVariable oldvariable)
        {
            object[] newOArr = this.oValues;
            object[] oldOArr = oldvariable.oValues;
            List<string> theExtras = new List<string>();

            for(int i = 0; i < newOArr.Length; i++)//each (var newval in newArr)
            {
                bool isExtra = true;
                object oNewVal = newOArr[i];
                string val1 = this.Values[i];

                for(int j = 0; j < oldOArr.Length; j++)// each (var oldval in oldOArr)
                {
                    object oOldVal = oldOArr[j];
                    string val2 = oldvariable.Values[j];
                    string explanation;
                    Type matchedType;
                    // if(HelperFxns.areEqual(type, oOldVal, oNewVal, out explanation))
                    if (HelperFxns.areEquivalent(
                        type, typeName, val1,
                        oldvariable.type, oldvariable.TypeName, val2,
                        out explanation, out matchedType)) 
                    {
                        isExtra = false;
                        break;
                    }
                }
                if (isExtra)
                {
                    string newVal = this.Values[i];
                    theExtras.Add(newVal);
                }
            }
            return theExtras; 
        }

        public List<string> missingValuesWRT(NewTableVariable oldvariable)
        {
            object[] newOArr = this.oValues;
            object[] oldOArr = oldvariable.oValues;
            
            List<string> theMissings = new List<string>();
            
            for(int i = 0; i < oldOArr.Length; i++)
            {
                bool isMissing = true;
                object o1 = oldOArr[i];
                string val1 = oldvariable.Values[i];

                for(int j = 0; j < newOArr.Length; j++)
                {
                    object o2 = newOArr[j];
                    string val2 = this.values[j];
                    string explanation;
                    Type matchedType;
                    // if(HelperFxns.areEqual(type, o1, o2, out explanation))
                    if(HelperFxns.areEquivalent(
                        type, typeName, val1, 
                        oldvariable.type, oldvariable.TypeName, val2,
                        out explanation, out matchedType))
                    {
                        isMissing = false;
                        break;
                    }
                }
                if (isMissing)
                {
                    string oldVal = oldvariable.Values[i];
                    theMissings.Add(oldVal);
                }
            }
            return theMissings; 
        }

        public List<string> commonValuesWRT(NewTableVariable oldvariable)
        {
            object[] newOArr = this.oValues;
            object[] oldOArr = oldvariable.oValues;
            List<string> theCommons = new List<string>();

            for(int i = 0; i < oldOArr.Length; i++)
            {
                bool isCommon = false;
                object oOldVal = oldOArr[i];
                string val1 = oldvariable.Values[i];
                
                for(int j = 0; j < newOArr.Length; j++)
                {
                    object oNewVal = newOArr[j];
                    string val2 = this.Values[j];
                    string explanation;
                    Type matchedType;
                    //if(HelperFxns.areEqual(type, oOldVal, oNewVal, out explanations))
                    if(HelperFxns.areEquivalent(
                        type, typeName, val1, 
                        oldvariable.type, oldvariable.TypeName, val2,
                        out explanation, out matchedType))
                    {
                        isCommon = true;
                        break;
                    }
                }
                if (isCommon)
                {
                    string oldVal = oldvariable.values[i];
                    theCommons.Add(oldVal);
                }
            }
            return theCommons; 
        }

        public int findPositionOfNewValWRT(NewTableVariable oldVar, string newVal)
        {
            string[] oldvals = oldVar.Values;
            string[] newvals = Values;
            int position = 0;
            string explanation;
            Type matchedType;
            for (int ni = 0; ni < newvals.Length; ni++)
            {
                if (position == oldvals.Length)
                {
                    break;
                }
                //if // (newvals[ni].Equals(newVal, StringComparison.InvariantCultureIgnoreCase))
                if(HelperFxns.areEquivalent(type, typeName, newvals[ni],
                    oldVar.type, oldVar.TypeName, newVal, out explanation, out matchedType))
                {
                    return position;
                }
                //if (newvals[ni].Equals(oldvals[position], StringComparison.InvariantCultureIgnoreCase))
                if(HelperFxns.areEquivalent(type, typeName, newvals[ni],
                    oldVar.type, oldVar.TypeName, oldvals[position], out explanation, out matchedType))
                {
                    position++;
                }

            }
            return position;
        }

        public int findIndexOfVal(NewTableVariable oldVar, string newVal)
        {
            string[] newvals = Values;
            string explanation;
            Type matchedType;
            for (int ni = 0; ni < newvals.Length; ni++)
            {
                if (HelperFxns.areEquivalent(type, typeName, newvals[ni],
                    oldVar.type, oldVar.TypeName, newVal, out explanation, out matchedType))
                {
                    return ni;
                }
            }
            return -1;
        }

        public NewTableVariable deepClone()
        {
            //! note that this will have to be updated if any changes are made to the class fields.
            NewTableVariable x = new NewTableVariable();
            x.Name = Name;
            x.TypeName = TypeName;
            x.type = type;
            x.CurrValueIndex = CurrValueIndex;
            if (Values == null)
                x.Values = null;
            else
                x.Values = (string[])Values.Clone();
            if (oValues == null)
                x.oValues = null;
            else
                x.oValues = (object[])oValues.Clone();
            return x;

        }

        //! shoud probably rename this to something to do w/ top node.
        public bool IsDeeplyEqualToNoRecurse(NewTableVariable otherTV)
        {
            if (otherTV == null)
                return false;
            if (!(this.Name.Equals(otherTV.Name, StringComparison.InvariantCultureIgnoreCase)))
                return false;
            if (this.CurrValueIndex != otherTV.CurrValueIndex)
                return false;
            if (this.Values == null)
                if (otherTV.Values == null)
                    return true;
                else
                    return false;
            if (otherTV.Values == null)
                return false;
            if (this.Values.Length != otherTV.Values.Length)
                return false;
            for(int i = 0; i < Values.Length; i++)
            {
                if (!Values[i].Equals(otherTV.Values[i], StringComparison.InvariantCultureIgnoreCase))
                    return false;
            }
            return true;
        }

        public bool IsDeeplyEqualToNoRecurseCaseSensitive(NewTableVariable otherTV)
        {
            if (otherTV == null)
                return false;
            if (this.Name != otherTV.Name)
                return false;
            if (this.TypeName != otherTV.TypeName)
                return false;
            if (this.CurrValueIndex != otherTV.CurrValueIndex)
                return false;
            if (this.Values == null)
                if (otherTV.Values == null)
                    return true;
                else
                    return false;
            if (otherTV.Values == null)
                return false;
            if (this.Values.Length != otherTV.Values.Length)
                return false;
            for (int i = 0; i < Values.Length; i++)
            {
                if (Values[i] != otherTV.Values[i])
                    return false;
            }
            return true;
        }

        public int indexOfVal(string valToFind)
        {
            if (string.IsNullOrEmpty(valToFind))
                throw new Exception("can't find the index of a value that is null or empty.");

            string errorMessages1, errorMessages2;
            Type type;
            if (!HelperFxns.isValidTypeName(this.TypeName, out errorMessages1, out type))
                throw new Exception("invalid type " + this.TypeName);
            object oValToFind;
            if (!HelperFxns.canParseAs(type, this.TypeName, valToFind, out errorMessages2, out oValToFind))
                throw new Exception("can't parse " + valToFind + " as " + this.TypeName);
            for(int i = 0; i < Values.Length; i++)
            {
                object oValI;
                string errorMessages3;
                if(!HelperFxns.canParseAs(type, this.TypeName, Values[i], out errorMessages3, out oValI))
                    throw new Exception("can't parse " + Values[i] + " as " + this.TypeName);
                string explanation;
                if (HelperFxns.areEqual(type, oValI, oValToFind, out explanation))
                    return i;
            }
            throw new Exception("couldn't find " + valToFind + ".");
        }

        private static bool isValidValueArrayString(string valsstr, Type type, out string[] trimmedVals, out string errorMessages)
        {
            string[] vals;
            bool isValid = true;
            errorMessages = "";
            if (Type.Equals(type, typeof(Range)))
            {
                List<string> lVals = new List<string>();
                string dispValsStr = valsstr;

                while (dispValsStr.Length > 0)
                {
                    // split off [/( and ]/) substrings.
                    if (!dispValsStr.StartsWith("[") && !dispValsStr.StartsWith("("))
                    {
                        errorMessages = "invalid values list: '" + dispValsStr + "'.  Should have started with [ or (";
                        isValid = false;
                        break;
                    }
                    int valRightInd = dispValsStr.IndexOf(')');
                    if (valRightInd < 0)
                    {
                        valRightInd = dispValsStr.IndexOf(']');
                    }
                    if (valRightInd < 0)
                    {
                        errorMessages = "invalid values list: '" + dispValsStr + "'. No closing ]/) character.";
                        isValid = false;
                        break;
                    }
                    lVals.Add(dispValsStr.Substring(0, valRightInd + 1));
                    dispValsStr = dispValsStr.Substring(valRightInd + 1).Trim();
                }
                vals = lVals.ToArray();
            }
            else
            {
                vals = valsstr.Split(' ');
            }

            trimmedVals = new string[vals.Length];
            for (int i = 0; i < vals.Length; i++)
            {
                string val = vals[i].Trim();
                if (val == "")
                {
                    errorMessages = "value " + i + " is empty";
                    isValid = false;
                }
                trimmedVals[i] = val;
            }
            return isValid;
        }

        //! rename the below.  It should just be getVariableFromLine.
        public static NewTableVariable getVariableFromRuleLine(string line)
        {
            //! Problem is that these are probably coming from the user,
            //! which means that exceptions should not be thrown,
            //! but rather the user should be nicely warned.

            if (string.IsNullOrEmpty(line))
                throw new Exception();
            if (!line.Contains(':'))
                throw new Exception("does not have enough semicolons. ");
            string[] splitline = line.Split(':');
            if (splitline.Length != 2)
                throw new Exception("has too many semicolons. ");
            if ((string.IsNullOrEmpty(splitline[0].Trim())))
                throw new Exception("has no variable name.");
            if (string.IsNullOrEmpty(splitline[1].Trim()))
                throw new Exception("has no values.");

            string[] splitvals = splitline[1].Split(' ');
            NewTableVariable tv = new NewTableVariable();
            string[] trimmedValues = new string[splitvals.Length];
            
            for (int i = 0; i < splitvals.Length; i++)
            {
                if (string.IsNullOrEmpty(splitvals[i].Trim()))
                    throw new Exception("has extra comma @ position " + (i + 1));
                trimmedValues[i] = splitvals[i].Trim();
            }
            tv.Values = trimmedValues;
            for (int i = 0; i < splitvals.Length; i++)
            {
                for (int j = i + 1; j < splitvals.Length; j++)
                {
                    if (tv.Values[i].Equals(tv.Values[j], StringComparison.InvariantCultureIgnoreCase))//string.Compare(tv.values[i],tv.values[j],true)==0)
                    {
                        string message = "has redundant value " + tv.Values[i] + " @ positions ";
                        message += (i + 1) + " and " + (j + 1);
                        throw new Exception(message);
                    }
                }
            }
            tv.Name = splitline[0].Trim();
            return tv;
        }

        public static NewTableVariable getVariableFromRuleLineWRTVars(string line, NewTableVariable[] variables)
        {
            if (string.IsNullOrEmpty(line))
                throw new Exception();
            if (!line.Contains(':'))
                throw new Exception("does not have enough semicolons. ");
            string[] splitline = line.Split(':');
            if (splitline.Length != 2)
                throw new Exception("has too many semicolons. ");
            if ((string.IsNullOrEmpty(splitline[0].Trim())))
                throw new Exception("has no variable name.");
            if (string.IsNullOrEmpty(splitline[1].Trim()))
                throw new Exception("has no values.");

            //! the error is here.  
            //! Need to first get they type, then parse the value line based on that type:
            //! then check that values are redundant based on that type as well.
            //! the code should not be unlike EditTable. getVariableFromLine!
            NewTableVariable tv = new NewTableVariable();

            tv.Name = splitline[0].Trim();

            // check that the name from the rule line is in the names of variables.
            // and assign the typeName based on the variable that matches.
            bool foundIt = false;
            NewTableVariable matchedVar = null;
            foreach (var v in variables)
            {
                if (v.Name.Replace(" ", "").ToLower() == tv.Name.Replace(" ", "").ToLower())
                {
                    tv.TypeName = v.TypeName;
                    foundIt = true;
                    matchedVar = v;
                }
            }
            if (!foundIt)
            {
                string message = "has undefined variablename: " + tv.Name;
                throw new Exception(message);
            }

            
            string errorMessages;
            Type type;
            HelperFxns.isValidTypeName(tv.TypeName, out errorMessages, out type);
            string valsstr = splitline[1].Trim();
            string[] vals;
            string errorMessages2;
            if (!isValidValueArrayString(valsstr, type, out vals, out errorMessages2))  //? overkill to get vals?
                throw new Exception(errorMessages2);

            // string[] splitvals = splitline[1].Split(' '); //! Regex.Split(splitline[1], ", ");

            //tv.Values = new string[vals.Length];
            string[] trimmedValues = new string[vals.Length];
            // check that there aren't extra commas.
            for (int i = 0; i < vals.Length; i++)
            {
                if (string.IsNullOrEmpty(vals[i].Trim()))
                    throw new Exception("has extra comma @ position " + (i + 1));
                trimmedValues[i] = vals[i].Trim();
            }
            tv.Values = trimmedValues;
            // check that there are no redundant values.
            for (int i = 0; i < vals.Length; i++)
            {
                for (int j = i + 1; j < vals.Length; j++)
                {
                    if (tv.Values[i].Equals(tv.Values[j], StringComparison.InvariantCultureIgnoreCase))//string.Compare(tv.values[i],tv.values[j],true)==0)
                    {
                        string message = "has redundant value " + tv.Values[i] + " @ positions ";
                        message += (i + 1) + " and " + (j + 1);
                        throw new Exception(message);
                    }
                }
            }
            

            // check that the new rule variable doesn't have any extra values relative to the original.
            if (tv.extraValuesWRT(matchedVar).Count != 0)
            {
                string message = "has extra values wrt its definition:";
                message += string.Join(", ", tv.extraValuesWRT(matchedVar).ToArray());
                throw new Exception(message);
            }

            
            
            return tv;
        }

        public int GetTopNodeHashCode()
        {
            // this method will ignore the tablevariable list that this table variable points to.
            // thus if two variables differ only by the list they point to, this hashcode won't distinguish
            return this.asLine().GetHashCode();
        }

        public bool hasCommonValsWRT(NewTableVariable otherTV)
        {
            foreach (var val1 in this.Values)
                foreach (var val2 in otherTV.Values)  // this will throw exception if otherTV is null.
                    if (val1.Equals(val2, StringComparison.InvariantCultureIgnoreCase))
                        return true;
            return false;
        }

        public bool IsDeeplyEqualToIgnoringOrderAndSubVariables(NewTableVariable var2)
        {
            //! note, also ignores the currValueIndex.
            if (!(this.Name.Equals(var2.Name, StringComparison.InvariantCultureIgnoreCase)))
                return false;
            if (this.Values.Length != var2.Values.Length) // will prompt error if empty.
                return false;
            foreach (var val2 in var2.Values)
                if (!this.hasValue(val2))
                    return false;
            return true;
        }

        public void forceType()
        {
            if (this.TypeName == null)
            {
                if (!string.IsNullOrEmpty(this.Name))
                {
                    this.TypeName = "Enum:" + this.Name;
                }
            }
        }

        public bool allowsNewValueByType(string value)
        {
            string errorMessages, errorMessages2;
            object parsed;
            Type type;
            HelperFxns.isValidTypeName(this.TypeName, out errorMessages, out type);
            return HelperFxns.canParseAs(type, this.TypeName, value, out errorMessages2, out parsed);
        }

        public void ReplaceInto(NewTableVariables cvs, int position)
        {
            if (cvs.isEmpty())
                throw new Exception();
            if (cvs.Count <= position)
                throw new Exception();
            NewTableVariable v = cvs[position];

            if (!this.isNamed(v.Name))
            {
                this.ReplaceInto(cvs, position + 1);
            }
            else
            {
                if (this.type != v.type)
                    throw new Exception();

                List<string> valsToKeep = new List<string>();   
                List<object> oValsToKeep = new List<object>();  
                int valsKeptSoFar = 0;

                // find the vals in v that correspond to the vals in this (every val in this should have
                // a corresponding val in v) and only keep the ones that are in this.values.
                foreach (var oRuleVal in this.oValues)
                {
                    for (int i = 0; i < v.Values.Length; i++)
                    {
                        string headVal = v.Values[i];
                        object oHeadVal = v.oValues[i];

                        string explanation;
                        if (HelperFxns.areEqual(type, oHeadVal, oRuleVal, out explanation))
                        {
                            valsToKeep.Add(headVal);
                            oValsToKeep.Add(oHeadVal);
                            valsKeptSoFar++;
                        }
                    }
                }
                v.Values = valsToKeep.ToArray();
                v.oValues = oValsToKeep.ToArray();
            }          
        }

        public void undoDecimalCommas()
        {
            if (this.type == typeof(decimal) || this.type == typeof(Range))
            {
                string[] newValues = new string[values.Length];
                for(int i = 0; i < newValues.Length; i++) //var value in newValues)
                {
                    var value = values[i];
                    newValues[i] = value.Replace(",", "");
                }
                Values = newValues;
            }
        }

        public static bool EqualByValue(NewTableVariable tv1, string value1, NewTableVariable tv2, string value2)
        {
            // Requires that variables are same type.
            // Requires that values are of that type.
            // Requires that values belong to variables.

            //if((tv1.type == typeof(Range)) || (tv2.type == typeof(Range)))
            //    throw new NotImplementedException();  //! implement...


            if (tv1.type != tv2.type)
                throw new ArgumentException("variables must have the same type.");

            if (!tv1.hasValue(value1))
                throw new ArgumentException("var1 '"+tv1.Name + "' does not have value '" + value1+"'.");
            if (!tv2.hasValue(value2))
                throw new ArgumentException("var2 '"+tv1.Name + "' does not have value '" + value2+"'.");

            string errorMessages1;
            string errorMessages2;
            object ovalue1;
            object ovalue2;
            if(!HelperFxns.canParseAs(tv1.type, tv1.typeName,value1, out errorMessages1, out ovalue1))
                throw new ArgumentException("couldn't parse "+ value1+ " as a "+tv1.typeName);
            if(!HelperFxns.canParseAs(tv2.type, tv1.typeName, value2, out errorMessages2, out ovalue2))
                throw new ArgumentException("couldn't parse "+ value1+ " as a "+tv1.typeName);

            string explanation;
            
            return HelperFxns.areEqual(tv1.type, ovalue1, ovalue2, out explanation);

            /*Type typeTest = typeof(bool);
            if((tv1.type == typeTest) || (tv2.type == typeTest))
            {
                if((tv1.type != typeTest) || (tv2.type != typeTest))
                {
                    throw new ArgumentException("can't compare bool to nonbool");
                }
                return ((bool)ovalue1 == (bool)ovalue2);
            }

            typeTest = typeof(Enum);
            throw new NotImplementedException(); //! add string case below.
            if((tv1.type == typeTest) || (tv2.type == typeTest))
            {
                if((tv1.type != typeTest) || (tv2.type != typeTest))
                {
                    throw new ArgumentException("can't compare enum to non enum");
                }
                return ((Enum)ovalue1 == (Enum)ovalue2);
            }
            //! add char and string cases.
            throw new NotImplementedException();
            
            // not a bool, a range, or an enum at this point.
            IComparable v1comparer = (IComparable)ovalue1;
            return (v1comparer.CompareTo(ovalue2) == 0);*/
        
        }

        public static bool LessThan(NewTableVariable tv1, string value1, NewTableVariable tv2, string value2)
        {
            // Requires that variables are same type.
            // Requires that values are of that type.
            // Requires that values belong to variables.

            if (tv1.type != tv2.type)
                throw new ArgumentException("variables must have the same type.");

            if (tv1.type == typeof(bool))
                throw new ArgumentException("cannot do lt/gt comparisons on bools.");

            if (!tv1.hasValue(value1))
                throw new ArgumentException("var1 '" + tv1.Name + "' does not have value '" + value1 + "'.");
            if (!tv2.hasValue(value2))
                throw new ArgumentException("var2 '" + tv1.Name + "' does not have value '" + value2 + "'.");

            string errorMessages1;
            string errorMessages2;
            object ovalue1;
            object ovalue2;
            if (!HelperFxns.canParseAs(tv1.type, tv1.typeName, value1, out errorMessages1, out ovalue1))
                throw new ArgumentException("couldn't parse " + value1 + " as a " + tv1.typeName);
            if (!HelperFxns.canParseAs(tv2.type, tv1.typeName, value2, out errorMessages2, out ovalue2))
                throw new ArgumentException("couldn't parse " + value1 + " as a " + tv1.typeName);

            return HelperFxns.LessThan(tv1.type, ovalue1, ovalue2);
        }

        public static bool OverlapValues(NewTableVariable tv1, string value1, NewTableVariable tv2, string value2)
        {
            if (tv1.type != tv2.type)
                throw new ArgumentException("variables must have the same type.");
            if (tv1.type != typeof(Range))
                throw new ArgumentException("cannot use overlap comparisons on non-range types.");


            if (!tv1.hasValue(value1))
                throw new ArgumentException("var1 '" + tv1.Name + "' does not have value '" + value1 + "'.");
            if (!tv2.hasValue(value2))
                throw new ArgumentException("var2 '" + tv1.Name + "' does not have value '" + value2 + "'.");

            string errorMessages1;
            string errorMessages2;
            object ovalue1;
            object ovalue2;
            if (!HelperFxns.canParseAs(tv1.type, tv1.typeName, value1, out errorMessages1, out ovalue1))
                throw new ArgumentException("couldn't parse " + value1 + " as a " + tv1.typeName);
            if (!HelperFxns.canParseAs(tv2.type, tv1.typeName, value2, out errorMessages2, out ovalue2))
                throw new ArgumentException("couldn't parse " + value1 + " as a " + tv1.typeName);

            Range rvalue1 = (Range)ovalue1;
            Range rvalue2 = (Range)ovalue2;
            return rvalue1.intersects(rvalue2);
        }
    }
}
