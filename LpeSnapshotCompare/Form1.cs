using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace LpeSnapshotCompare
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
    public class LpeSnapshotCompareMain : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.TextBox tbResults;
        private System.Windows.Forms.OpenFileDialog ofdOpenFileDialog;
        private System.Windows.Forms.Button btnBrowseFile1;
        private System.Windows.Forms.Button btnBrowseFile2;
        private System.Windows.Forms.TextBox tbFile1;
        private System.Windows.Forms.TextBox tbFile2;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public LpeSnapshotCompareMain()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //

            tbFile1.Text = @"c:\t.xml";
            tbFile2.Text = @"c:\f.xml";
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if (components != null) 
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

		#region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbFile1 = new System.Windows.Forms.TextBox();
            this.tbFile2 = new System.Windows.Forms.TextBox();
            this.btnCompare = new System.Windows.Forms.Button();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.ofdOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowseFile1 = new System.Windows.Forms.Button();
            this.btnBrowseFile2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "File #1";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "File #2";
            // 
            // tbFile1
            // 
            this.tbFile1.Location = new System.Drawing.Point(64, 8);
            this.tbFile1.Name = "tbFile1";
            this.tbFile1.Size = new System.Drawing.Size(568, 20);
            this.tbFile1.TabIndex = 2;
            this.tbFile1.Text = "c:\\temp\\result1.xml";
            // 
            // tbFile2
            // 
            this.tbFile2.Location = new System.Drawing.Point(64, 40);
            this.tbFile2.Name = "tbFile2";
            this.tbFile2.Size = new System.Drawing.Size(568, 20);
            this.tbFile2.TabIndex = 3;
            this.tbFile2.Text = "c:\\temp\\result2.xml";
            // 
            // btnCompare
            // 
            this.btnCompare.Location = new System.Drawing.Point(272, 64);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.TabIndex = 4;
            this.btnCompare.Text = "Compare";
            this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
            // 
            // tbResults
            // 
            this.tbResults.Location = new System.Drawing.Point(8, 96);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbResults.Size = new System.Drawing.Size(712, 544);
            this.tbResults.TabIndex = 5;
            this.tbResults.Text = "";
            // 
            // ofdOpenFileDialog
            // 
            this.ofdOpenFileDialog.Filter = "XML File|*.xml";
            // 
            // btnBrowseFile1
            // 
            this.btnBrowseFile1.Location = new System.Drawing.Point(640, 8);
            this.btnBrowseFile1.Name = "btnBrowseFile1";
            this.btnBrowseFile1.TabIndex = 6;
            this.btnBrowseFile1.Text = "Browse ...";
            this.btnBrowseFile1.Click += new System.EventHandler(this.btnBrowseFile1_Click);
            // 
            // btnBrowseFile2
            // 
            this.btnBrowseFile2.Location = new System.Drawing.Point(640, 40);
            this.btnBrowseFile2.Name = "btnBrowseFile2";
            this.btnBrowseFile2.TabIndex = 7;
            this.btnBrowseFile2.Text = "Browse ...";
            this.btnBrowseFile2.Click += new System.EventHandler(this.btnBrowseFile2_Click);
            // 
            // LpeSnapshotCompareMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(728, 654);
            this.Controls.AddRange(new System.Windows.Forms.Control[] {
                                                                          this.btnBrowseFile2,
                                                                          this.btnBrowseFile1,
                                                                          this.tbResults,
                                                                          this.btnCompare,
                                                                          this.tbFile2,
                                                                          this.tbFile1,
                                                                          this.label2,
                                                                          this.label1});
            this.Name = "LpeSnapshotCompareMain";
            this.Text = "LPE Snapshot Compare";
            this.ResumeLayout(false);

        }
		#endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() 
        {
            Application.Run(new LpeSnapshotCompareMain());
        }

        private void btnCompare_Click(object sender, System.EventArgs e)
        {
            tbResults.Text     = "";
            btnCompare.Enabled = false;

            string report;
            LendersOfficeApp.TestSuite.LpeSnapshotCompareTools.Compare(tbFile1.Text, tbFile2.Text, out report);

            tbResults.Text = report;
            btnCompare.Enabled = true;
        }

        private void btnBrowseFile1_Click(object sender, System.EventArgs e)
        {
            DialogResult result = ofdOpenFileDialog.ShowDialog();
            if (result == DialogResult.OK) 
            {
                tbFile1.Text = ofdOpenFileDialog.FileName;
            }

        }

        private void btnBrowseFile2_Click(object sender, System.EventArgs e)
        {
            DialogResult result = ofdOpenFileDialog.ShowDialog();
            if (result == DialogResult.OK) 
            {
                tbFile2.Text = ofdOpenFileDialog.FileName;
            }
        }
	}

}
