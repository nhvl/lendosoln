// Note :   C:\LendOSoln\LendOSoln\LendOSolnTest\LpeSnapshotCompareTools.cs and 
//          C:\LendOSoln\LendOSoln\LpeSnapshotCompare\LpeSnapshotCompareTools.cs are same.
//        Please, try to synchronize those files.  

using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Xml;

namespace LendersOfficeApp.TestSuite
{
    // Related file C:\LendOSoln\LendOSoln\LendersOfficeApp\los\LoanFindService.aspx.cs

    public class LpeSnapshotCompareTools
    {
        static public bool Compare( string xmlSnapshotFileName1, string xmlSnapshotFileName2, out string report)
        {
            StringBuilder sb = new StringBuilder();

            XmlDocument doc1 = new XmlDocument();
            doc1.Load(xmlSnapshotFileName1);

            XmlDocument doc2 = new XmlDocument();
            doc2.Load(xmlSnapshotFileName2);

            bool result = Compare(doc1, doc2, sb);
            report = sb.ToString();

            return result;

        }

        static public bool Compare(XmlDocument doc1, XmlDocument doc2, out string report)
        {
            StringBuilder sb = new StringBuilder();

            bool result = Compare(doc1, doc2, sb);
            report = sb.ToString();

            return result;
        }

        static bool Compare(XmlDocument doc1, XmlDocument doc2, StringBuilder sb) 
        {
            bool isEqual = true;

            double duration1, duration2;
            sb.AppendFormat("    [1] : {0}{1}"    , GetSnapshotInfo(doc1, out duration1), Environment.NewLine);
            sb.AppendFormat("    [2] : {0}{1}{1}" , GetSnapshotInfo(doc2, out duration2), Environment.NewLine);

            if( duration1 != double.MaxValue && duration2 != double.MaxValue && duration1 != 0 && duration2 != 0 )
            {
                sb.AppendFormat("            duration1/ duration2 = {0:N2} %    ***  duration2 / duration1 = {1:N2}%{2}{2}",
                    duration1*100/duration2, duration2*100/duration1,  Environment.NewLine );

            }


            Hashtable hash1 = new Hashtable(10);
            Hashtable hash2 = new Hashtable(10);

            XmlNodeList nodeList = doc1.SelectNodes("/Snapshot/Loan");
            foreach (XmlElement el in nodeList) 
            {
                hash1.Add(el.GetAttribute("sLId"), el);
            }

            nodeList = doc2.SelectNodes("/Snapshot/Loan");
            foreach (XmlElement el in nodeList) 
            {
                hash2.Add(el.GetAttribute("sLId"), el);
            }

            foreach (string key in hash1.Keys) 
            {
                if (!hash2.ContainsKey(key)) 
                {
                    isEqual = false;
                    sb.AppendFormat("sLId={0} - MISSING FROM FILE #2.{1}{1}", key, Environment.NewLine);
                    continue;
                }

                XmlElement loanElement1 = (XmlElement) hash1[key];
                XmlElement loanElement2 = (XmlElement) hash2[key];

                if( CompareLoan(loanElement1, loanElement2, sb) == false )
                    isEqual = false;
                hash2.Remove(key);

            }

            foreach (string key in hash2.Keys) 
            {
                isEqual = false;
                sb.AppendFormat("sLId={0} - MISSING FROM FILE #1.{1}{1}", key, Environment.NewLine);
            }

            return isEqual;

        }

        static bool CompareLoan(XmlElement loanElement1, XmlElement loanElement2, StringBuilder sb) 
        {
            bool isSame = true;
            string indentString = "     ";
            StringBuilder subSb = new StringBuilder();

            Hashtable hash1 = new Hashtable(50);
            Hashtable hash2 = new Hashtable(50);

            XmlNodeList nodeList = loanElement1.SelectNodes("./Results/CApplicantPriceXml");
            foreach (XmlElement el in nodeList) 
                hash1.Add(el.GetAttribute("lLpTemplateId"), el);

            nodeList = loanElement2.SelectNodes("./Results/CApplicantPriceXml");
            foreach (XmlElement el in nodeList) 
                hash2.Add(el.GetAttribute("lLpTemplateId"), el);


            foreach (string key in hash1.Keys) 
            {
                XmlElement applicantPriceElement1 = (XmlElement) hash1[key];
                if (!hash2.ContainsKey(key)) 
                {
                    subSb.AppendFormat("{0}{1} - MISSING FROM FILE #2.{2}", indentString, applicantPriceElement1.GetAttribute("lLpTemplateNm"), Environment.NewLine);
                    isSame = false;
                    continue;
                }
                XmlElement applicantPriceElement2 = (XmlElement) hash2[key];

                bool ret = CompareLoanApplicantPrice(applicantPriceElement1, applicantPriceElement2, subSb, indentString);
                if( isSame )
                    isSame = ret;

                hash2.Remove(key);
            }

            foreach (string key in hash2.Keys) 
            {
                XmlElement el = (XmlElement) hash2[key];
                isSame = false;
                subSb.AppendFormat("{0}{1} - MISSING APPLICANT FROM FILE #1.{2}", indentString, el.GetAttribute("lLpTemplateNm"), Environment.NewLine);                
            }

            sb.AppendFormat("sLId={0} - {3}.{1}{2}{1}", loanElement1.GetAttribute("sLId"), Environment.NewLine, subSb, isSame ? "SAME" : "DIFFERENCE" );
            return isSame; 
        }

        static bool CompareLoanApplicantPrice(XmlElement applicantPriceElement1, XmlElement applicantPriceElement2, StringBuilder sb, string indentString) 
        {
            bool isDiff = false;

            string header = string.Format("{3}{0}{1} *** lLpTemplateId (aka prgId) = {2} - DIFFERENCE.{3}", indentString, applicantPriceElement1.GetAttribute("lLpTemplateNm"), applicantPriceElement1.GetAttribute("lLpTemplateId"), Environment.NewLine);
            bool bFirstDiff = true;

            #region Compare Attributes
            foreach (XmlAttribute attr in applicantPriceElement1.Attributes) 
            {
                string value1 = attr.Value;
                string value2 = applicantPriceElement2.GetAttribute(attr.Name);

                if (value1 != value2) 
                {
                    isDiff = true;
                    if (bFirstDiff) 
                    {
                        sb.Append(header);
                        bFirstDiff = false;
                    }
                    sb.AppendFormat("{0}{0}Attr[{1}]:: File #1 = [{2}], File #2 = [{3}]{4}", indentString, attr.Name, value1, value2, Environment.NewLine);
                }
            }
            #endregion

            #region Compare Errors

            XmlNodeList errors1 = applicantPriceElement1.SelectNodes("./Errors");
            XmlNodeList errors2 = applicantPriceElement2.SelectNodes("./Errors");

            if (errors1.Count != errors2.Count) 
            {
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}Errors have different count.{1}", indentString, Environment.NewLine);
            } 
            else 
            {
                Hashtable errTable1 = BuildErrorTable(applicantPriceElement1);

                for (int i = 0; i < errors1.Count; i++) 
                {

                    string errStr2 = RemoveReferenceInErrorMsg(errors2[i].InnerText);
                    if( errTable1.Contains( errStr2 ) )
                        continue;

                    string errStr1 = RemoveReferenceInErrorMsg(errors1[i].InnerText);

                    if(  errStr1 != errStr2) 
                    {
                        isDiff = true;
                        if (bFirstDiff) 
                        {
                            sb.Append(header);
                            bFirstDiff = false;
                        }
                        sb.AppendFormat("{0}{0}Errors are difference.{1}[Err 1] : {2}{1}[Err 2] : {3}{1}", 
                            indentString, Environment.NewLine, errStr1,  errStr2);
                    }
                    break;
                }
            }
            #endregion

            #region Compare Denial Reasons

            bool isDenialReasonsSame = true;
            XmlNodeList denialReasons1 = applicantPriceElement1.SelectNodes("./DenialReasons/DenialReason");
            XmlNodeList denialReasons2 = applicantPriceElement2.SelectNodes("./DenialReasons/DenialReason");
            if (denialReasons1.Count != denialReasons2.Count) 
            {
                isDenialReasonsSame = false;
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}Denial Reasons count are difference : {1} vs {2} {3}", 
                    indentString, denialReasons1.Count, denialReasons2.Count, Environment.NewLine);
            } 
            else 
            {
                Hashtable  denialReasonsTable = new Hashtable(denialReasons1.Count);
                for (int i = 0; i < denialReasons1.Count; i++) 
                {
                    XmlElement el1 = (XmlElement) denialReasons1[i];
                    denialReasonsTable[el1.InnerText] = null;
                }

                for (int i = 0; i < denialReasons1.Count; i++) 
                {
                    XmlElement el1 = (XmlElement) denialReasons1[i];
                    XmlElement el2 = (XmlElement) denialReasons2[i];

                    if( denialReasonsTable.Contains(el2.InnerText) )
                        continue;
                    if (el1.InnerText != el2.InnerText) 
                    {
                        isDenialReasonsSame = false;
                        isDiff = true;
                        if (bFirstDiff) 
                        {
                            sb.Append(header);
                            bFirstDiff = false;
                        }
                        sb.AppendFormat("{0}{0}Denial Reasons text are difference.{1}", indentString, Environment.NewLine);
                        break;
                    }
                }
            }

            #endregion

            #region Compare Disqualified Rules
            XmlElement disqualifiedRules1 = (XmlElement) applicantPriceElement1.SelectSingleNode("./DisqualifiedRules");
            XmlElement disqualifiedRules2 = (XmlElement) applicantPriceElement2.SelectSingleNode("./DisqualifiedRules");

            // With current implement, disqualifiedRules is text form of DenialReasons
            if (disqualifiedRules1.InnerText != disqualifiedRules2.InnerText && isDenialReasonsSame == false ) 
            {
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}Disqualified Rules are difference.{1}", indentString, Environment.NewLine);

            }
            #endregion

            #region Compare ApplicantRateOptions
            XmlNodeList applicantRateOptions1 = applicantPriceElement1.SelectNodes("./ApplicantRateOptions/CApplicantRateOption");
            XmlNodeList applicantRateOptions2 = applicantPriceElement2.SelectNodes("./ApplicantRateOptions/CApplicantRateOption");

            if (applicantRateOptions1.Count != applicantRateOptions2.Count) 
            {
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}ApplicantRateOptions have different count : {1} vs {2} {3}", 
                    indentString, applicantRateOptions1.Count, applicantRateOptions2.Count, Environment.NewLine);
            } 
            else 
            {
                for (int i = 0; i < applicantRateOptions1.Count; i++) 
                {
                    XmlElement el1 = (XmlElement) applicantRateOptions1[i];
                    XmlElement el2 = (XmlElement) applicantRateOptions2[i];
                    bool b = false;
                    foreach (XmlAttribute attr in el1.Attributes) 
                    {
                        string value1 = attr.Value;
                        string value2 = el2.GetAttribute(attr.Name);
                        if (value1 != value2) 
                        {
                            isDiff = true;
                            if (bFirstDiff) 
                            {
                                sb.Append(header);
                                bFirstDiff = false;
                            }
                            sb.AppendFormat("{0}{0}ApplicantRateOptions are difference.{1}", indentString, Environment.NewLine);
                            b = true;
                            break;
                        }
                    }
                    if (b) break;

                }
            }
            #endregion

            #region Compare RepresentativeRateOptions
            XmlNodeList representativeRateOptions1 = applicantPriceElement1.SelectNodes("./RepresentativeRateOption/CApplicantRateOption");
            XmlNodeList representativeRateOptions2 = applicantPriceElement2.SelectNodes("./RepresentativeRateOption/CApplicantRateOption");

            if (representativeRateOptions1.Count != representativeRateOptions2.Count) 
            {
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}RepresentativeRateOptions have different count.{1}", indentString, Environment.NewLine);
            } 
            else 
            {
                for (int i = 0; i < representativeRateOptions1.Count; i++) 
                {
                    XmlElement el1 = (XmlElement) representativeRateOptions1[i];
                    XmlElement el2 = (XmlElement) representativeRateOptions2[i];
                    bool b = false;
                    foreach (XmlAttribute attr in el1.Attributes) 
                    {
                        string value1 = attr.Value;
                        string value2 = el2.GetAttribute(attr.Name);
                        if (value1 != value2) 
                        {
                            isDiff = true;
                            if (bFirstDiff) 
                            {
                                sb.Append(header);
                                bFirstDiff = false;
                            }
                            sb.AppendFormat("{0}{0}RepresentativeRateOptions are difference.{1}", indentString, Environment.NewLine);
                            b = true;
                            break;
                        }
                    }
                    if (b) break;

                }
            }

            #endregion

            #region Compare Stips
            XmlNodeList stips1 = applicantPriceElement1.SelectNodes("./Stips/CSortedListOfGroups/item");
            XmlNodeList stips2 = applicantPriceElement2.SelectNodes("./Stips/CSortedListOfGroups/item");
            int []arrIdx = new int[] { 0, 1};
            if (stips1.Count != stips2.Count) 
            {
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}Stipulations have different count : {1} vs {2} {3}", 
                    indentString, stips1.Count, stips2.Count, Environment.NewLine);
                Swap( ref stips1, ref stips2, arrIdx );

            } 
            //else 
        {
            // 7/12/2006 dd - Assume the stipulations are in the same order.
            Hashtable stipTalbe = new Hashtable(stips1.Count);
            for (int i = 0; i < stips1.Count; i++) 
                stipTalbe[ AttributesToString( (XmlElement) stips1[i] ) ] = null;


            for (int i = 0; i < stips2.Count; i++) 
            {
                XmlElement el2 = (XmlElement) stips2[i];

                if( stipTalbe.Contains( AttributesToString( el2 )  ) == true )
                    continue;

                /*
                    XmlElement el1 = (XmlElement) stips1[i];
                    if ((el1.GetAttribute("item") != el2.GetAttribute("item")) ||
                        (el1.GetAttribute("singleItem") != el2.GetAttribute("singleItem"))) 
                    {
                        isDiff = true;
                        if (bFirstDiff) 
                        {
                            sb.Append(header);
                            bFirstDiff = false;
                        }
                        sb.AppendFormat("{0}{0}Stipulation texts are difference.{1}", indentString, Environment.NewLine);
                        break;

                    }
                    */
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}Stipulation texts are difference. For example only {1} file contains{2}{3}{2}", 
                    indentString, arrIdx[1] == 0 ? "first" : "second",
                    Environment.NewLine, AttributesToStringWithOrgOrder( el2 ) );
                break;

            }
        }

            #endregion

            #region Compare AdjustDescs
            XmlNodeList adjustDesc1 = applicantPriceElement1.SelectNodes("./AdjustDescs/Item");
            XmlNodeList adjustDesc2 = applicantPriceElement2.SelectNodes("./AdjustDescs/Item");
            if (adjustDesc1.Count != adjustDesc2.Count) 
            {
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}AdjustDescs have different count : {1} vs {2} {3}", 
                    indentString, adjustDesc1.Count, adjustDesc2.Count, Environment.NewLine);
            } 
            else 
            {
                Hashtable adjTable = new Hashtable(adjustDesc1.Count);
                for (int i = 0; i < adjustDesc1.Count; i++) 
                    adjTable[  AttributesToString( (XmlElement) adjustDesc1[i] ) ] = null;

                for (int i = 0; i < adjustDesc1.Count; i++) 
                {
                    XmlElement el1 = (XmlElement) adjustDesc1[i];
                    XmlElement el2 = (XmlElement) adjustDesc2[i];

                    if( adjTable.Contains( AttributesToString( el2 ) ) )
                        continue;

                    bool b = false;
                    foreach (XmlAttribute attr in el1.Attributes) 
                    {
                        string value1 = attr.Value;
                        string value2 = el2.GetAttribute(attr.Name);
                        if (value1 != value2) 
                        {
                            isDiff = true;
                            if (bFirstDiff) 
                            {
                                sb.Append(header);
                                bFirstDiff = false;
                            }
                            sb.AppendFormat("{0}{0}AdjustDescs are difference.{1}", indentString, Environment.NewLine)
                                .AppendFormat("{0}{0}{0}Value1 = {1}{2}", indentString, value1, Environment.NewLine)
                                .AppendFormat("{0}{0}{0}Value2 = {1}{2}", indentString, value2, Environment.NewLine);
                            b = true;
                            break;
                        }
                    }
                    if (b) break;

                }
            }
            #endregion

            #region Compare HiddenAdjustDescs
            XmlNodeList hiddenAdjustDesc1 = applicantPriceElement1.SelectNodes("./HiddenAdjustDescs/Item");
            XmlNodeList hiddenAdjustDesc2 = applicantPriceElement2.SelectNodes("./HiddenAdjustDescs/Item");
            if (hiddenAdjustDesc1.Count != hiddenAdjustDesc2.Count) 
            {
                isDiff = true;
                if (bFirstDiff) 
                {
                    sb.Append(header);
                    bFirstDiff = false;
                }
                sb.AppendFormat("{0}{0}HiddenAdjustDescs have different count : {1} vs {2} {3}", 
                    indentString, hiddenAdjustDesc1.Count, hiddenAdjustDesc2.Count, Environment.NewLine);
            } 
            else 
            {
                Hashtable adjTable = new Hashtable(hiddenAdjustDesc1.Count);
                for (int i = 0; i < hiddenAdjustDesc1.Count; i++) 
                    adjTable[  AttributesToString( (XmlElement) hiddenAdjustDesc1[i] ) ] = null;

                for (int i = 0; i < hiddenAdjustDesc1.Count; i++) 
                {
                    XmlElement el1 = (XmlElement) hiddenAdjustDesc1[i];
                    XmlElement el2 = (XmlElement) hiddenAdjustDesc2[i];
                    if( adjTable.Contains( AttributesToString( el2 ) ) )
                        continue;

                    bool b = false;
                    foreach (XmlAttribute attr in el1.Attributes) 
                    {
                        string value1 = attr.Value;
                        string value2 = el2.GetAttribute(attr.Name);


                        if (value1 != value2) 
                        {
                            isDiff = true;
                            if (bFirstDiff) 
                            {
                                sb.Append(header);
                                bFirstDiff = false;
                            }
                            sb.AppendFormat("{0}{0}HiddenAdjustDescs are difference. Only the second file contains {1}{2}{1}", 
                                indentString, Environment.NewLine, AttributesToStringWithOrgOrder(el2));
                            b = true;
                            break;
                        }
                    }
                    if (b) break;

                }
            }
            #endregion

            return isDiff ? false : true;
        }


        static string GetSnapshotInfo(XmlDocument doc, out double duration)
        {
            duration= double.MaxValue;

            XmlNode snapshot   = doc.SelectSingleNode("/Snapshot");
            string usr         = snapshot.Attributes["LoginName"].Value;
            string timeStamp   = snapshot.Attributes["Timestamp"].Value;
            string durationStr = snapshot.Attributes["ExecutionTime"].Value.Replace("[0 hr ", "[");

            int start = durationStr.LastIndexOf("[");
            int end   = durationStr.LastIndexOf(" ms.]");

            if( start > 0 &&  end > start )
            {
                try
                {
                    duration = double.Parse( durationStr.Substring( start+1, end - start) );
                }
                finally
                {
                }

            }

            int appCount     = doc.SelectNodes("//CApplicantPriceXml").Count;

            return usr + "   " + timeStamp + "   #app = " + appCount + "  *** " + durationStr;
        }

        static Hashtable BuildErrorTable(XmlElement applicantPriceElement)
        {
            Hashtable table = new Hashtable();
            foreach(XmlElement error in applicantPriceElement.SelectNodes("./Errors") )
            {
                table[RemoveReferenceInErrorMsg(error.InnerText)] = null;
            }
            return table;
        }

        // input  : Evaluation error: Reference #:W9L-77JP-C4GE UserMessage
        // output : Evaluation error: UserMessage
        static string RemoveReferenceInErrorMsg(string str)
        {
            const string keyword = "Reference #:";

            str = str.Trim();

            while( true )
            {
                int idx = str.IndexOf(keyword);
                if( idx  < 0 )
                    return str;
                str = str.Remove(idx, keyword.Length + "W9L-77JP-C4GE".Length);
            }

        }

        static string AttributesToString(XmlElement ele)
        {
            if( ele == null )
                return "";

            StringBuilder sb = new StringBuilder();

            Hashtable table = new Hashtable();
            foreach (XmlAttribute attr in ele.Attributes) 
                table[attr.Name] = attr.Value.Trim();

            foreach (String name in table.Keys)             
                sb.AppendFormat("{0}=\"{1}\" ", name, (string)table[name]);
            return sb.ToString().Trim();
        }

        static string AttributesToStringWithOrgOrder(XmlElement ele)
        {
            if( ele == null )
                return "";

            StringBuilder sb = new StringBuilder();

            Hashtable table = new Hashtable();
            foreach (XmlAttribute attr in ele.Attributes) 
                sb.AppendFormat("{0}=\"{1}\" ", attr.Name, attr.Value.Trim() );

            return sb.ToString();
        }

        static void Swap( ref XmlNodeList x, ref XmlNodeList y, int []arrIdx )
        {
            if( x.Count > y.Count ) 
            {
                XmlNodeList tmp = x;
                x = y;
                y = tmp;

                int tmpIdx = arrIdx[0];
                arrIdx[0] = arrIdx[1];
                arrIdx[1] = tmpIdx;

            }
        }

    }
}
