﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RetailLogin.aspx.cs" Inherits="PriceMyLoan.RetailLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LendingQB</title>
    <style type="text/css">
        #LoginTable {
            width: 420px;
            margin: 60px auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="m_ValidBrowser" value="true" runat="server" name="index_data" />

        <table id="LoginTable" class="login-page">
            <tr>
                <td class="label-login text-right">
                    <strong>Login</strong>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LoginName" AutoCompleteType="None" NoHighlight="true"></asp:TextBox>
                </td>
                <td class="text-left">
                    <a href="https://support.lendingqb.com/index.php?//Knowledgebase/Article/View/185" target="_blank">Forgot login?</a>
                </td>
            </tr>
            <tr>
                <td class="label-login text-right">
                    <strong>Password</strong>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Password" TextMode="Password" AutoCompleteType="None" NoHighlight="True"></asp:TextBox>
                </td>
                <td class="text-left">
                    <a onclick="RedirectToResetPassword()">Forgot password?</a>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="text-center">
                    <strong>
                        <ml:EncodedLabel id="ErrorMessage" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel>
                        <ml:EncodedLabel id="ErrorMessage2" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel>
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="text-center">
                    <asp:Button ID="Login" runat="server" Text="Login" CssClass="btn btn-default" OnClick="Login_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="3" class="text-center">
                    <span runat="server" id="CopyrightFooter"></span>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
