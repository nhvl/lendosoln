﻿namespace PriceMyLoan
{
    using System;
    using Common;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using Security;

    /// <summary>
    /// Provides a means for retail users to log in to the portal.
    /// </summary>
    public partial class RetailLogin : UI.BasePage
    {
        /// <summary>
        /// Gets a value indicating whether principal-specific 
        /// styling is enabled for the page.
        /// </summary>
        protected override bool IncludePrincipalSpecificStyling => false;

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.CopyrightFooter.InnerHtml = ConstAppDavid.CopyrightMessage;

            this.RegisterJsScript("common.js");
            this.RegisterJsScript("login.js");

            var compatibilityNotification = ConstApp.GetPmlBrowserIncompatibleNotification(
                    Request.Browser.Browser,
                    Request.Browser.Version,
                    Request.UserAgent);

            this.RegisterJsGlobalVariables("CompatNotification", compatibilityNotification);
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }
            
            if (!this.IsPostBack && RequestHelper.GetSafeQueryString("errorcode") != null)
            {
                PriceMyLoanRequestHelper.SetRequestErrorMessage(
                    RequestHelper.GetSafeQueryString("errorcode"), 
                    this.ErrorMessage, 
                    this.ErrorMessage2);
            }
        }

        /// <summary>
        /// Handles the login button click.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Login_Click(object sender, EventArgs e)
        {
            if (string.Equals("false", this.m_ValidBrowser.Value, StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            if (WebsiteUtilities.IsDatabaseOffline || !this.IsPostBack)
            {
                return;
            }

            PriceMyLoanRequestHelper.IsEncompassIntegration = false; // If user can log in then it is not an Encompass integration
            // 6/2/2010 dd - Base on Citi request (M7). We only accept POST request for authentication page.
            if (this.Request.HttpMethod != "POST")
            {
                return;
            }

            PrincipalFactory.E_LoginProblem loginProblem;
            var principal = (PriceMyLoanPrincipal)PrincipalFactory.CreateRetailWithFailureType(
                this.LoginName.Text, 
                this.Password.Text, 
                isStoreToCookie: true, 
                loginProblem: out loginProblem);

            if (principal != null)
            {
                if (string.Equals("P", principal.Type, StringComparison.OrdinalIgnoreCase) ||
                    !principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
                {
                    this.ErrorMessage.Text = ErrorMessages.GenericAccessDenied;
                    this.ErrorMessage2.Text = string.Empty;
                    return;
                }

                if (!principal.BrokerDB.EnableRetailTpoPortalMode)
                {
                    this.ErrorMessage.Text = ErrorMessages.RetailPortalNotEnabled;
                    this.ErrorMessage2.Text = string.Empty;
                    return;
                }

                bool didLog = RequestHelper.EnforceMultiFactorAuthentication();

                // Only insert an audit record if we don't need MFA.  The code below the above line won't run if MFA is being enforced.
                if (!didLog)
                {
                    RequestHelper.InsertAuditRecord(principal, brokerPmlSiteId: Guid.Empty);
                }

                if (!BrokerUserDB.AreSecurityAnswersSetForTheUser(principal.BrokerId, principal.UserId))
                {
                    this.Response.Redirect(PriceMyLoanConstants.SetSecurityQuestionsUrl);
                }

                this.Response.Redirect(PriceMyLoanConstants.PipelineUrl + "?isLoadingDashboard=true");

            }
            else
            {
                // 02/13/08 mf. OPM 19614 We need to redirect even on a failed login.
                var errorCode = PriceMyLoanRequestHelper.GetErrorCodeFromLoginProblem(loginProblem);
                this.Response.Redirect(Request.Url.AbsolutePath + "?errorcode=" + errorCode);
            }
        }
    }
}