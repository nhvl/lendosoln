using System;
using System.Web.UI;
using LendersOffice.Admin;
using PriceMyLoan.Security;

namespace PriceMyLoan.help
{
	public partial class HousingHistory : PriceMyLoan.UI.BasePage
	{
		protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            // Load the lender description of FTHB

            BrokerDB broker = BrokerDB.RetrieveById(((PriceMyLoanPrincipal)Page.User).BrokerId);

            FthbCustomDefinitionLabel.Text = broker.FthbCustomDefinition;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
