﻿<%@ Page language="c#" Codebehind="NumberOfFinancedProperties.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.help.NumberOfFinancedProperties" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
	<head runat="server">
		<title>Is My Credit Provider Supported</title><link href="css/style.css" rel="stylesheet" text="text/css" /></head>
	<body>
	    <form id="HousingHistory" method="post" runat="server">
		    <div class="Title">Number of Financed Properties</div>
	        <ol style="list-style-type: lower-alpha">
	            <li>Include all properties where mortgage financing will remain in place.</li>
	            <li>Include the subject property.</li>
	            <li>This number should be cumulative across all borrowers in this loan file.</li>
	            <li>Each property should be counted only once</li>
	            <li>Must be at least one.</li>
	        </ol>
		</form>
	</body>
</html>
