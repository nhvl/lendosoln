using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PriceMyLoan.Security;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.AntiXss;

namespace PriceMyLoan.help
{
	/// <summary>
	/// Summary description for Q00001.
	/// </summary>
	public partial class Q00001 : PriceMyLoan.UI.BasePage
	{

		protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
			BrokerDB broker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
			
			string email = broker.EmailAddrSendFromForNotif.ToLower();
			
			string label = (broker.NameSendFromForNotif.TrimWhitespaceAndBOM().Equals(""))?email:broker.NameSendFromForNotif;
			
			
			if(email.TrimWhitespaceAndBOM().Equals(""))
			{
				m_CreditProviderSupported.Text = AspxTools.HtmlString("our administrator or your account executive"); 
			}
			else
			{
                m_CreditProviderSupported.Text = $"<a href={AspxTools.HtmlAttribute("mailto:" + email)}>{AspxTools.HtmlString(label)}</a>";
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
