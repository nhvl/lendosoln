<%@ Register TagPrefix="ml" TagName="ModelessDlg" Src="../Common/ModalDlg/ModelessDlg.ascx" %>
<%@ Page language="c#" Codebehind="Q00001.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.help.Q00001" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>CreditProviderSupported</title>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto">
		<form id="CreditProviderSupported" method="post" runat="server">
			<table width="100%">
				<tr>
					<td class="SectionTitle" style="COLOR: black; FONT-WEIGHT: bold; font-size:18px">Is 
						My Credit Provider Supported?</td>
				</tr>
				<tr>
					<td valign="top" style="COLOR: black">
						<br><br>
						Check the drop-down list to the left to determine if your provider is 
						supported. If you do not see them on the list, please have your <STRONG>credit 
						reporting company</STRONG> email 
						<ML:PassthroughLabel id="m_CreditProviderSupported" runat="server"></ML:PassthroughLabel>
						with information regarding that credit vendor and we will see if we can get 
						them integrated.
					</td>
				</tr>
				
			</table>
		</form>
	</body>
</HTML>
