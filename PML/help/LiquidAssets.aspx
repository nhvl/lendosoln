<%@ Page Language="c#" CodeBehind="LiquidAssets.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.help.LiquidAssets" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Liquid Assets</title>
</head>
<body ms_positioning="FlowLayout" style="overflow-y: auto;">
    <form id="HousingHistory" method="post" runat="server">
    <table>
        <tr>
            <td class="SectionTitle" style="color: black; font-weight: bold">
                Liquid Assets
            </td>
        </tr>
        <tr>
            <td valign="top" style="color: black">
                Total Liquid Assets include cash and other assets that are easily converted to cash by all the borrowers on application:
                <ul>
                    <li>drafting or withdrawing funds from an account;</li>
                    <li>selling an asset; </li>
                    <li>redeeming vested funds; or</li>
                    <li>obtaining a loan secured by assets from a fund administrator or an insurance company.</li>
                </ul>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
