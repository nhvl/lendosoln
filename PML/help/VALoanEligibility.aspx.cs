using System;
using LendersOffice.Common;

namespace PriceMyLoan.help
{
    public partial class VALoanEligibility : PriceMyLoan.UI.BasePage
    {
        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isMigrated = RequestHelper.GetInt("isMigrated", -1) == 1;

            if (isMigrated)
            {
                bool isVeteran = RequestHelper.GetBool("isVeteran");
                bool isSurvivingSpouseOfVeteran = RequestHelper.GetBool("isSSpouseOfVet");

                IsVeteran.Checked = isVeteran;
                IsSurvivingSpouseOfVeteran.Checked = isSurvivingSpouseOfVeteran;
                ClientScript.RegisterHiddenField("bType", RequestHelper.GetSafeQueryString("bType"));
            }
            else
            {
                VAOptions.Style["display"] = "none";
                Buttons.Style["display"] = "none";
            }
		}
	}
}
