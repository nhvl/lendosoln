<%@ Page Language="c#" CodeBehind="VALoanEligibility.aspx.cs" AutoEventWireup="true" Inherits="PriceMyLoan.help.VALoanEligibility" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>VA Loan Eligibility</title>
    <style type="text/css">
        .Bold
        {
            font-weight: bold;
        }
    </style>
</head>
<body ms_positioning="FlowLayout" style="overflow-y: auto;">
    <script type="text/javascript">
        $j(function () {
            $j('#CancelBtn').click(function () {
                parent.LQBPopup.Hide();
            });

            $j('#OKBtn').click(function () {
                var args = {};
                var isVet = $j("#IsVeteran").is(":checked");
                var isSurvivingSpouseOfVeteran = $j("#IsSurvivingSpouseOfVeteran").is(":checked");
                
                args.isVeteran = isVet;
                args.IsSurvivingSpouseOfVeteran = isSurvivingSpouseOfVeteran;
                args.bType = $j("#bType").val();

                parent.LQBPopup.Return(args);
            });
        });
    </script>
    <form id="HousingHistory" method="post" runat="server">
    <table>
        <tr>
            <td class="SectionTitle Bold" colspan="2">
                VA Loan Eligibility
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                To be VA loan eligible, the borrower must be a veteran,
                an active duty personnel, a reservist/national guard
                member, or (in some cases) the surviving spouse of one
                of the prior three.
                Borrower must obtain a 'Certificate of Eligibility' from
                the Department of Veteran Affairs in order to be eligible.
            </td>
        </tr>
        <tr align="center" runat="server" id="VAOptions">
            <td>
                Is Veteran? 
                <asp:CheckBox runat="server" ID="IsVeteran" /> Yes
            </td>
            <td>
                Is Surviving Spouse of Veteran?
                <asp:CheckBox runat="server" ID="IsSurvivingSpouseOfVeteran" /> Yes
            </td>
        </tr>
        <tr align="center" runat="server" id="Buttons">
            <td colspan="2">
                <input type="button" value="OK" id="OKBtn"/>
                &nbsp;
                <input type="button" value="Close" id="CancelBtn"/>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
