﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using Integration.Encompass;
using PriceMyLoan.Common;
using LendersOffice.Security;
using System.Text.RegularExpressions;

namespace PriceMyLoan
{
    /// <summary>
    /// This page is not accessed through our system. It's only accessed through Encompass.
    /// </summary>
    public partial class Encompass : PriceMyLoan.UI.BasePage
    {
        protected override bool IsCrossSiteForgeryCheck 
        {
            get { return false; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod == "POST")
            {
                ReportRequest();
                
                PriceMyLoanRequestHelper.IsEncompassIntegration = true; // 7/17/2009 dd - This page is only access through Encompass.
                EncompassServer eServer = new EncompassServer(GetEPassData());

                bool bAuthenticated = false;
                if (eServer.AuthenticateUser())
                {
                    bAuthenticated = true;
                }
                else
                {
                    Tools.LogWarning("User could not be authenticated");
                }

                string sLoanURL = "";
                bool bSuccess = false;
                if (bAuthenticated)
                {
                    string[] remnUsers = new string[] { "REMN_HOMEBRIDGE_ENCOMPASS_TOTAL", "REMN_ENCOMPASS_TOTAL", "david_acc", "david_pml" };

                    if (remnUsers.Contains(eServer.Principal.LoginNm, StringComparer.OrdinalIgnoreCase))
                    {
                        Tools.LogInfo("Performing special REMN integration");
                        // 6/13/2012 dd - Perform special REMN (PML0200) integration between
                        // Encompass 360 + TOTAL Score.
                        // 6/13/2012 dd - Cache EpassData for 4 hours.
                        string key = AutoExpiredTextCache.AddToCache(GetEPassData(), TimeSpan.FromSeconds(14400));
                        if (eServer.IsPUser)
                        {
                            sLoanURL = string.Format("~/EncompassRemnSearchScreen.aspx?cmd=initialmatch&key={0}", key);
                        }
                        else
                        {
                            sLoanURL = string.Format("~/embeddedpml/EncompassRemnSearchScreen.aspx?userid={1}&cmd=initialmatch&key={0}&secret={2}", key, eServer.UserID, HttpUtility.UrlEncode(Tools.SecretHashForPml(eServer.UserID)));
                        }
                        bSuccess = true;
                    }
                    else if (eServer.ImportLoanFile())
                    {
                        Tools.LogInfo("Imported loan file");
                        PriceMyLoanRequestHelper.EncompassRequestType = eServer.RequestType;
                        bool bCanRunLPE = eServer.HasLPEAccess();

                        if (eServer.IsPUser)
                        {
                            if (bCanRunLPE)
                                if (eServer.Principal.BrokerDB.IsNewPmlUIEnabled)
                                {
                                    sLoanURL = string.Format("~/webapp/pml.aspx?loanid={0}&source=PML", eServer.LoanID.ToString());
                                }
                                else
                                {
                                    sLoanURL = string.Format("~/main/agents.aspx?authid=&loanid={0}", eServer.LoanID.ToString());
                                }
                            else
                                sLoanURL = string.Format("~/EncompassLoginFailure.aspx?errorcode={0}", "LPEIsLocked");
                        }
                        else
                        {
                            if (bCanRunLPE)
                                if (eServer.Principal.BrokerDB.IsNewPmlUIEnabled)
                                {

                                    sLoanURL = string.Format("{0}/embeddedpml/webapp/pml.aspx?userid={1}&loanid={2}&secret={3}&source=PML", VirtualRoot, eServer.UserID.ToString(), eServer.LoanID.ToString(), HttpUtility.UrlEncode(Tools.SecretHashForPml(eServer.UserID)));
                                }
                                else
                                {
                                    sLoanURL = string.Format("{0}/embeddedpml/main/agents.aspx?userid={1}&loanid={2}&secret={3}", VirtualRoot, eServer.UserID.ToString(), eServer.LoanID.ToString(), HttpUtility.UrlEncode(Tools.SecretHashForPml(eServer.UserID)));
                                }
                            else
                            {
                                sLoanURL = string.Format("{0}/embeddedpml/EncompassLoginFailure.aspx?errorcode={1}", VirtualRoot, "LPEIsLocked");
                            }
                        }
                        bSuccess = true;
                    }
                }
                
                if(!bSuccess)
                {
                    if(eServer.IsPUser)
                        sLoanURL = string.Format("~/EncompassLoginFailure.aspx?errorcode={0}", eServer.ErrorCode);
                    else
                        sLoanURL = string.Format("{0}/embeddedpml/EncompassLoginFailure.aspx?errorcode={1}", VirtualRoot, eServer.ErrorCode);
                }
                Tools.LogInfo("Redirecting to " + sLoanURL);
                Response.Redirect(sLoanURL);
            }
        }

        // Returns a string containing the xml data request from Encompass
        private string GetEPassData()
        {
            return Request.Form["epass"];
        }

        // A debug-only method that can be used to report the request to PB
        private void ReportRequest()
        {
            NameValueCollection nvColl;
            nvColl = Request.Headers;

            StringBuilder sb = new StringBuilder(string.Format("This is the request from Encompass:{0}Headers:{0}", Environment.NewLine));
            foreach(string sKey in nvColl.AllKeys)
            {
                sb.Append(string.Format("{0} --{1}", sKey, Environment.NewLine));
                foreach(string sVal in nvColl.GetValues(sKey))
                {
                    sb.Append(sVal + Environment.NewLine);
                }
            }
            using (StreamReader sReader = new StreamReader(Request.InputStream, Encoding.ASCII))
            {
                sb.Append(string.Format("Body:{0}{1}", Environment.NewLine, sReader.ReadToEnd()));
            }
            string s = Server.UrlDecode(sb.ToString());

            // 6/15/2012 dd - Mask out a password.
            s = Regex.Replace(s, "password=\"[^\"]+\"", "password=\"******\"");
            Tools.LogInfo(s);
        }
    }
}
