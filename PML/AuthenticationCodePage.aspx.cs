﻿// <copyright file="AuthenticationCodePage.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/4/2014 2:47:01 AM 
// </summary>
namespace PriceMyLoan
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using PriceMyLoan.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Users are directed to this page after logging in if they have MultiFactorAuthentication enabled.
    /// </summary>
    public partial class AuthenticationCodePage : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// The broker's id.
        /// </summary>
        private Guid brokerId = Guid.Empty;
        
        /// <summary>
        /// The user's id.
        /// </summary>
        private Guid userId = Guid.Empty;

        /// <summary>
        /// The lender's pml site id.
        /// </summary>
        private Guid lenderPmlSiteID = Guid.Empty;

        /// <summary>
        /// Loads the web page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            EmployeeDB employee = EmployeeDB.RetrieveByUserId(this.brokerId, this.userId);
            string step1AuthenticationMode;
            if (string.IsNullOrEmpty(employee.PrivateCellPhone))
            {
                step1AuthenticationMode = "email address on file.";
                btnSendAuthenticationCode.Text = "Email Me An Authentication Code";
            }
            else
            {
                step1AuthenticationMode = "cell phone on file via SMS.";
                btnSendAuthenticationCode.Text = "Text Me An Authentication Code";
                CellphoneContainer.Visible = false;
            }
            if (employee.EnableAuthCodeViaAuthenticator)
            {
                Step1Text.InnerText = $"Please get your one time code from your configured authenticator app.";

                if (employee.EnableAuthCodeViaSms)
                {
                    Step1Text.InnerText += $" You may still request a one time code be sent to your {step1AuthenticationMode}";
                }
                else
                {
                    btnSendAuthenticationCode.Visible = false;
                    CellphoneContainer.Visible = false;
                }
            }

            else if (employee.EnableAuthCodeViaSms)
            {
                Step1Text.InnerText = $"A code was sent to the {step1AuthenticationMode}. If you do not receive the code within the next few minutes, the button directly below can be used to request a new one. ";
            }
            else
            {
                ErrorUtilities.DisplayErrorPage(new AccessDenied(ErrorMessages.SmsAuthenticationNotEnabled, $"User <{userId}> does not have EnableAuthCodeViaSms == true"), false, this.brokerId, this.userId);
            }

            string displayName;
            BrokerDB broker = BrokerDB.RetrieveById(employee.BrokerID);
            if (!broker.AllowDeviceRegistrationViaAuthCodePage)
            {
                m_registerCb.Visible = false;
            }


            if (!MultiFactorAuthCodeUtilities.UseBranchAsSource(employee, out displayName))
            {
                CompanyName.Visible = false;
                Guid branchId;
                var portalMode = employee.PortalMode == E_PortalMode.Blank ? Tools.DeterminePortalModeFirstLogin(employee) : employee.PortalMode;

                switch (portalMode)
                {
                    case E_PortalMode.Blank:
                        branchId = Guid.Empty;
                        break;
                    case E_PortalMode.Broker:
                    case E_PortalMode.Retail:
                        branchId = employee.BranchID;
                        break;
                    case E_PortalMode.Correspondent:
                        branchId = employee.CorrespondentBranchID;
                        break;
                    case E_PortalMode.MiniCorrespondent:
                        branchId = employee.MiniCorrespondentBranchID;
                        break;
                    default:
                        throw new UnhandledEnumException(portalMode);
                }

                string pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogo(broker.PmlSiteID, branchId);

                if (FileDBTools.DoesFileExist(E_FileDB.Normal, pmlLenderSiteId.ToLower() + ".logo.gif"))
                {
                    CompanyLogo.Src = "LogoPL.aspx?id=" + pmlLenderSiteId;
                }
            }
            else
            {
                CompanyLogo.Visible = false;
                CompanyName.InnerText = displayName;
            }

         
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = false;
            this.EnableJqueryMigrate = false;
            this.btnSubmit.Click += new EventHandler(this.BtnSubmit_Click);
            this.btnCancel.Click += new EventHandler(this.BtnCancel_Click);
            this.btnSendAuthenticationCode.Click += new EventHandler(this.BtnSendAuthenticationCode_Click);

            this.RegisterJsScript("mask.js");
            this.RegisterCSS("jquery-ui-1.11.css");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");

            this.InitializeDataFromCookie();
        }

        /// <summary>
        /// Generates a new authentication code and sends it to the user.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void BtnSendAuthenticationCode_Click(object sender, EventArgs e)
        {
            MultiFactorAuthCodeUtilities.GenerateAndSendNewCode(this.brokerId, this.userId, true);
        }

        /// <summary>
        /// Cancels the users attempt.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            PriceMyLoanRequestHelper.Signout(LendersOffice.Security.PrincipalFactory.CurrentPrincipal);
        }

        /// <summary>
        /// The user's code and I.P. address are verified.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (MultiFactorAuthCodeUtilities.Verify(this.brokerId, this.userId, SecurityPin.Text))
            {
                PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal)PriceMyLoan.Security.PrincipalFactory.CreateByPmlUserId(this.brokerId, this.userId);
                BrokerDB broker = principal.BrokerDB;
                if (broker == null)
                {
                    broker = BrokerDB.RetrieveById(this.brokerId);
                }

                if (broker.AllowDeviceRegistrationViaAuthCodePage && this.m_registerCb.Visible)
                {
                    UserRegisteredIp.AddKnownIpAddress(this.brokerId, this.userId, RequestHelper.ClientIP, m_registerCb.Checked);
                }

                RequestHelper.StoreToCookie(ConstApp.CookieMultiFactorAuthentication, string.Empty, false);
                RequestHelper.InsertAuditRecord(principal, this.lenderPmlSiteID);

                EmployeeDB employee = EmployeeDB.RetrieveByUserId(principal.BrokerId, principal.UserId);
                if (string.IsNullOrEmpty(employee.PrivateCellPhone) && !string.IsNullOrEmpty(Cellphone.Text))
                {
                    employee.PrivateCellPhone = Cellphone.Text;
                    employee.IsCellphoneForMultiFactorOnly = true;
                    employee.Save(principal);
                }

                if (!BrokerUserDB.AreSecurityAnswersSetForTheUser(this.brokerId, this.userId))
                {
                    Response.Redirect(PriceMyLoanConstants.SetSecurityQuestionsUrl);
                }

                Response.Redirect(PriceMyLoanConstants.PipelineUrl + "?isLoadingDashboard=true");
            }
            else
            {
                this.DisplayErrorMessage("Invalid authentication code");
                SecurityPin.Text = string.Empty;
            }
        }

        /// <summary>
        /// If the verification failed, an error message is displayed.
        /// </summary>
        /// <param name="errorMessage">The message to be displayed.</param>
        private void DisplayErrorMessage(string errorMessage)
        {
            ErrorMessagePanel.Visible = true;
            ErrorMessage.Text = errorMessage;
        }

        /// <summary>
        /// This method shuffles the list of integers to create a random keypad.
        /// </summary>
        /// <param name="list">The list of items being shuffled.</param>
        private void Shuffle(IList<int> list)
        {
            // http://stackoverflow.com/questions/273313/randomize-a-listt-in-c-sharp
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                int value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Retrieves the data from the cookie so that we can retrieve a reference to the user and broker attempting to log-in.
        /// </summary>
        private void InitializeDataFromCookie()
        {
            HttpCookie cookie = Request.Cookies.Get(ConstApp.CookieMultiFactorAuthentication);
            if (cookie == null)
            {
                // 4/28/2014 dd - This mean someone access this page without go through login control.
                RequestHelper.Signout();
            }

            string encryptBrokerIdUserId = cookie.Value;
            string[] parts = EncryptionHelper.Decrypt(encryptBrokerIdUserId).Split(':');

            this.brokerId = new Guid(parts[0]);
            this.userId = new Guid(parts[1]);

            // PML site ID cookie will be null for retail users.
            HttpCookie siteIdCookie = Request.Cookies.Get("LenderPmlSiteId");
            if (siteIdCookie != null)
            {
                this.lenderPmlSiteID = new Guid(siteIdCookie.Value);
            }
        }
    }
}
