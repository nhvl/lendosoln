using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.CreditReport.Mismo2_1;
using LendersOffice.Drivers.Gateways;
using LendersOffice.Security;
using Mismo.Enveloping;
using Mismo.MismoXmlElement;
using Mismo.MortgageLeague;
using PriceMyLoan.Security;
using PriceMyLoan.DataAccess;

namespace PriceMyLoan.inetapi
{
	public partial class MortgageLeague : BaseMismoEnvelopingPage
	{
        private string m_loanNumber;

        private string m_brokerName = "";
        private string m_loanOfficerName = "";
        private string m_loanOfficerEmail = "";
        private string m_loanOfficerPhone = "";

        private void ParseRequestParty(XERequestingParty requestingParty) 
        {
            if (null == requestingParty)
                return;

            m_brokerName = requestingParty.Name;
            
            XEContactDetail contactDetail = requestingParty.GetContactDetail(0);
            if (null != contactDetail) 
            {
                m_loanOfficerName = contactDetail.Name;
                m_loanOfficerEmail = contactDetail.GetWorkEmail();
                m_loanOfficerPhone = contactDetail.GetWorkPhone();
            }
            
        }
        protected override XEResponseGroup Process(XERequestGroup requestGroup) 
        {
            

            XEMortgageLeagueResponse mortgageLeagueResponse = new XEMortgageLeagueResponse();
            XERequest request = requestGroup.GetRequest(0);
            if (AuthenticateRequest(request)) 
            {
                ParseRequestParty(requestGroup.GetRequestingParty(0));
                XERequestData requestData = request.GetRequestData(0);
                if (null != requestData) 
                {
                    try 
                    {
                        

                        Guid loanID = ProcessMortgageLeagueRequest(requestData.ContentInnerXml);

                        mortgageLeagueResponse.Status = E_XEMortgageLeagueResponseStatusType.OK;
                        mortgageLeagueResponse.Url = BuildUrl(loanID);
                        mortgageLeagueResponse.LoanNumber = m_loanNumber;
                        mortgageLeagueResponse.LoanID = loanID;
                    } 
                    catch (CBaseException exc) 
                    {
                        Tools.LogError(exc);
                        mortgageLeagueResponse.SetErrorMessage(MortgageLeagueErrorCode.UnexpectedError, exc.UserMessage);
                    }
                } 
                else 
                {
                    mortgageLeagueResponse.SetErrorMessage(MortgageLeagueErrorCode.MissingData, "Missing Request Data");
                }
            } 
            else 
            {
                mortgageLeagueResponse.SetErrorMessage(MortgageLeagueErrorCode.InvalidAuthentication, "Invalid Login.");
            }

            XEResponseGroup responseGroup = new XEResponseGroup();

            XEResponse response = new XEResponse();
            response.ResponseDateTime = DateTime.Now;
            responseGroup.AddResponse(response);

            XEResponseData responseData = new XEResponseData();
            responseData.Content = mortgageLeagueResponse;
            response.AddResponseData(responseData);

            return responseGroup;
        }

        private bool AuthenticateRequest(XERequest request) 
        {
            if (null != request) 
            {
                string loginName = request.LoginAccountIdentifier;
                Guid lenderPmlSiteID = new Guid(request.LoginAccountPassword);
                IPrincipal principal = PriceMyLoan.Security.PrincipalFactory.CreateUsingMLAuthentication(loginName, lenderPmlSiteID);
                if (null != principal)
                {
                    Context.User = principal;
                    System.Threading.Thread.CurrentPrincipal = principal;
                    return true;
                }

            }

            return false;
        }

        private Guid ProcessMortgageLeagueRequest(string xml) 
        {
            XEMortgageLeagueRequest mortgageLeagueRequest = new XEMortgageLeagueRequest();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            
            mortgageLeagueRequest.Parse((XmlElement) doc.ChildNodes[0]);

            Guid loanID = Guid.Empty;
            if (mortgageLeagueRequest.ActionType == E_XEMortgageLeagueRequestActionType.SubmitNew) 
            {
                // Create new Loan.
                loanID = CreateNewLoan();
                if (null != mortgageLeagueRequest.CreditReport) 
                {
                    // 1/19/2005 dd - MCL entire credit report is available, therefore no need to retrieve credit report again.
                    // TODO.
                } 
                else 
                {
                    XEMclInstantViewID mclInstantViewID = mortgageLeagueRequest.MclInstantViewID;
                    PullMclInstantViewCreditReport(loanID, mclInstantViewID.InstantViewPassword, mclInstantViewID.ReportID);

                }

                AddAgentInformation(loanID);
            } 
            else if (mortgageLeagueRequest.ActionType == E_XEMortgageLeagueRequestActionType.Retrieve) 
            {
                m_loanNumber = mortgageLeagueRequest.LoanNumber;
                loanID = mortgageLeagueRequest.LoanID;

                // Try to load a light dataobject with this loan id to verify loan existed.
                CPageData dataLoan = new CLoanNameData(loanID);
                dataLoan.InitLoad();
                m_loanNumber = dataLoan.sLNm;
            }
            return loanID;
        }
        private void AddAgentInformation(Guid sLId) 
        {
            CPageData dataLoan = new CPreparerOnlyData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = m_loanOfficerName;
            agent.CompanyName = m_brokerName;
            agent.Phone = m_loanOfficerPhone;
            agent.EmailAddr = m_loanOfficerEmail;
            agent.Update();

            dataLoan.Save();

        }
        private string BuildUrl(Guid sLId) 
        {
            string urlFormat = "";
            string host = Request.Url.Scheme + "://" + Request.Url.Host + Tools.VRoot;

            PriceMyLoanPrincipal currentUser = (PriceMyLoanPrincipal) User;

            // 1/26/2005 dd - Check to see if user have permission to view loan file.
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(currentUser.BrokerId, sLId, WorkflowOperations.WriteLoanOrTemplate, WorkflowOperations.ReadLoanOrTemplate);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(currentUser));

            bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
            bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);

            if (canWrite) 
            {
                if (FileOperationHelper.Exists(Tools.GetServerMapPath("~/custom/website/edit.asp")))
                {
                    urlFormat = "{0}/custom/website/edit.asp?loanid={1}&gmlid={2}";

                } 
                else 
                {
                    urlFormat = "{0}/main/main.aspx?tabkey=APPINFO&loanid={1}&gmlid={2}";
                }
            } 
            else if (canRead) 
            {
                if (FileOperationHelper.Exists(Tools.GetServerMapPath("~/custom/website/view.asp"))) 
                {
                    urlFormat = "{0}/custom/website/view.asp?loanid={1}&gmlid={2}";
                } 
                else 
                {
                    urlFormat = "{0}/main/LoanSummary.aspx?loanid={1}&gmlid={2}";
                }

            } 
            else 
            {
                // TODO: What should happen here
            }

            SqlParameter[] parameters = { new SqlParameter("@UserID", currentUser.UserId) };

            Guid pmlSiteID = Guid.Empty;
            Guid gmlID = Guid.Empty;
            try 
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(currentUser.BrokerId, "Pml_RetrievePmlSiteIdByUserID", parameters)) 
                {
                    if (reader.Read()) 
                    {
                        // TODO: If user doesn't have PmlSiteID report better error.
                        pmlSiteID = (Guid) reader["PmlSiteId"];
                    }

                }
            } 
            catch (Exception exc)
            {
                throw new CBaseException(PriceMyLoan.UI.Common.PmlErrorMessages.NeedsAllowLoginFromMCL, exc);
            }
            if (pmlSiteID != Guid.Empty) 
            {
                gmlID = Guid.NewGuid();
                // Add to server cache for 5 minutes
                AutoExpiredTextCache.AddToCache(pmlSiteID.ToString(), new TimeSpan(0, 1, 0), "MortgageLeague_" + gmlID.ToString());
//                Cache.Add("MortgageLeague_" + gmlID.ToString(), pmlSiteID, null, DateTime.Now.AddMinutes(5), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
            }
            return string.Format(urlFormat, host, sLId, gmlID);
        }
        private Guid RetrievePmlLoanTemplateID() 
        {
            LendersOffice.Admin.BrokerDB brokerDB = LendersOffice.Admin.BrokerDB.RetrieveById(((PriceMyLoanPrincipal) User).BrokerId);

            return brokerDB.PmlLoanTemplateID;

        }
        private Guid CreateNewLoan() 
        {

            Guid pmlLoanTemplateID = RetrievePmlLoanTemplateID();

            LendersOffice.Audit.E_LoanCreationSource source = LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank;
            if (Guid.Empty != pmlLoanTemplateID)
                source = LendersOffice.Audit.E_LoanCreationSource.FromTemplate;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator( ((PriceMyLoanPrincipal) User), source );
            Guid loanID = Guid.Empty;

            if (Guid.Empty == pmlLoanTemplateID) 
            {
                loanID = creator.CreateBlankLoanFile();
            } 
            else 
            {
                loanID = creator.CreateLoanFromTemplate(templateId: pmlLoanTemplateID);
            }
            m_loanNumber = creator.LoanName;

            return loanID;

        }
        private void PullMclInstantViewCreditReport(Guid sLId, string instantViewID, string reportID) 
        {
            MclCreditReportRequest creditRequest = new MclCreditReportRequest();
            creditRequest.RequestType = MclRequestType.Get;
            creditRequest.InstantViewID = instantViewID;
            creditRequest.ReportID = reportID;
            creditRequest.AppendOutputFormat(MclOutputFormat.XML);
            creditRequest.AppendOutputFormat(MclOutputFormat.HTML);

            bool isReady = false;
            while (!isReady) 
            {
                // TODO: When this page actually gets implemented, we'll want to have this use the Background Job Processor to make the request.
                string temp;
                ICreditReportResponse creditResponse = CreditReportServer.OrderCreditReport(sLId, creditRequest, CreditReportProtocol.Mcl, LendersOffice.Security.PrincipalFactory.CurrentPrincipal, out temp);
                if (null == creditResponse) 
                {
                    throw new CBaseException(ErrorMessages.Generic, "TODO");
                }
                if (creditResponse.HasError) 
                {
                    throw new CBaseException(ErrorMessages.Generic, "TODO");
                }
                if (creditResponse.IsReady) 
                {
                    Guid dbFileKey = Guid.NewGuid(); // 7/28/2004 dd - TODO: Recycle old key if there is one.

                    if (Request.Form["DBFileKey"] != "") 
                    {
                        dbFileKey = new Guid(Request.Form["DBFileKey"]);
                    }

//                    CreditReportUtilities.SaveXmlCreditReport(creditResponse, (PriceMyLoanPrincipal) User, false, sLId, Guid.Empty, comId,crAccProxyId);

                    CPageData dataLoan = new CPmlImportLiabilityData(sLId);
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    CAppData dataApp = dataLoan.GetAppData(0);
                    dataApp.ImportLiabilitiesFromCreditReport(true, true);

					// 2/27/2007 nw - OPM 10630 - import public records
					dataApp.aPublicRecordCollection.ClearAll();
					dataApp.aPublicRecordCollection.Flush();
					dataApp.ImportPublicRecordsFromCreditReport();

                    dataLoan.Save();

                    
                    isReady = true;
                    throw new NotImplementedException();
                }
            }
        }

	}
}
