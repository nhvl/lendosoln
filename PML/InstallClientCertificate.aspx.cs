﻿// <copyright file="InstallClientCertificate.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/4/2014 2:47:01 AM 
// </summary>
namespace PriceMyLoan
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Installs the client certificate.
    /// </summary>
    public partial class InstallClientCertificate : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// Gets the current principal from the Principal Factory.
        /// </summary>
        private AbstractUserPrincipal CurrentPrincipal
        {
            get { return PrincipalFactory.CurrentPrincipal; }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                string cmd = RequestHelper.GetSafeQueryString("cmd");
                if (cmd == "download")
                {
                    this.DownloadCertificate();
                    return;
                }
            }
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.m_continueBtn.ServerClick += new EventHandler(this.ContinueBtn_Click);
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            this.RegisterCSS("InstallClientCertificate.css");
        }

        /// <summary>
        /// Generates the certificate's password.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        private void ContinueBtn_Click(object sender, EventArgs e)
        {
            this.Panel0.Visible = false;
            this.Panel1.Visible = true;

            this.m_certificatePassword.Text = MultiFactorAuthCodeUtilities.GenerateRandom(6);

            string str = EncryptionHelper.Encrypt(this.m_certificatePassword.Text + this.m_description.Text);

            str = str.Replace("+", ".").Replace("=", "-");

            this.RegisterJsGlobalVariables("EncryptedString", str);
        }

        /// <summary>
        /// Returns the certificate.
        /// </summary>
        private void DownloadCertificate()
        {
            string encryptedString = RequestHelper.GetSafeQueryString("arg");
            encryptedString = encryptedString.Replace(".", "+").Replace("-", "=");
            string str = EncryptionHelper.Decrypt(encryptedString);

            string password = str.Substring(0, 6);
            string description = str.Substring(6);

            byte[] bytes = ClientCertificate.CreateClientCertificate(this.CurrentPrincipal, description, password);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=certificate.p12");
            Response.ContentType = "application/octet-stream";

            Response.OutputStream.Write(bytes, 0, bytes.Length);
            Response.Flush();
            Response.End();
        }
    }
}
