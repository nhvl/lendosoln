﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="PriceMyLoan.ResetPassword" %>
<%@Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Change Password</title>

    <style type="text/css">
        .ErrorMessage
        {
            color:#EA1C0D;
        }
    </style>

    <script type="text/javascript">
        <!--

        function VerifyPassword() {
            HideAllRuleIcons();
            var password = document.getElementById("txtPassword").value;
            VerifyPasswordOnServer(password);
        }

        function HideAllRuleIcons()
        {
            var numRules = 7;
            for (var i = 1; i <= numRules; i++) {
                var imgElement = document.getElementById("imgRule" + i);
                if (imgElement != null) imgElement.style.display = "none";
            }
        }

        function VerifyPasswordRetype() {
            var password = document.getElementById("txtPassword").value;
            var password2 = document.getElementById("txtVerifyPass").value;

            if (password != password2) {
                SetImageIcon("imgVerifyRetypePass", "F")
            }
            else {
                SetImageIcon("imgVerifyRetypePass", "T")
            }
        }

        function SetImageIcon(imgElementName, success) {
           var imgElement = document.getElementById(imgElementName);
           imgElement.style.display = "";
           if(success == "T")
           {
                imgElement.innerHTML='&#xE86C;';
           }
           else
           {
                imgElement.innerHTML='&#xE147;';
           }

           $j(imgElement).toggleClass("red rotate-45",success != "T");
           $j(imgElement).toggleClass("green",success == "T");
        }
        function ReceivePasswordVerification(arg, context) {
            var numRules = 7;
            var idx = parseInt(arg.substring(0, 1));
            if (idx == 0) {
                for (var i = 1; i <= numRules; i++) {
                    SetImageIcon("imgRule" + i, "T");
                }
            }
            else {
                if (idx > 1) {
                    for (var i = 1; i < idx; i++) {
                        SetImageIcon("imgRule" + i, "T");
                    }
                }
                SetImageIcon("imgRule" + idx, "F");
            }
        }
        //-->
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <center>
        <div class="login-page password-reset-page">
            <h3 class="title">Reset Your Password</h3>
            <% if (!QuestionsVerified)
                {%>

            <p><strong>Security Questions</strong></p>
            <div class="table">
                <div><div class="text-bold label-login nowrap w-92">Question 1</div><div class="text-bold label-login nowrap"><ml:EncodedLabel ID="lblQuestion1" runat="server"></ml:EncodedLabel></div></div>
                <div><div class="w-92">Answer 1</div><div><asp:TextBox ID="txtBoxAnswer1" runat="server" Width="250px"> </asp:TextBox></div><div><asp:RequiredFieldValidator ID="rfvAnswer1" runat="server" ControlToValidate="txtBoxAnswer1" ErrorMessage="Answer 1 cannot be blank"><i runat="server" class="material-icons red rotate-45">&#xE147;</i></asp:RequiredFieldValidator></div></div>

                <div><div class="text-bold label-login nowrap w-92">Question 2</div><div class="text-bold label-login nowrap"><ml:EncodedLabel ID="lblQuestion2" runat="server"></ml:EncodedLabel></div></div>
                <div><div class="w-92">Answer 2</div><div><asp:TextBox ID="txtBoxAnswer2" runat="server" Width="250px"> </asp:TextBox></div><div><asp:RequiredFieldValidator ID="rfvAnswer2" runat="server" ControlToValidate="txtBoxAnswer2" ErrorMessage="Answer 2 cannot be blank"><i runat="server" class="material-icons red rotate-45">&#xE147;</i></asp:RequiredFieldValidator></div></div>

                <div><div class="text-bold label-login nowrap w-92">Question 3</div><div class="text-bold label-login nowrap"><ml:EncodedLabel ID="lblQuestion3" runat="server"></ml:EncodedLabel></div></div>
                <div><div class="w-92">Answer 3</div><div><asp:TextBox ID="txtBoxAnswer3" runat="server" Width="250px"> </asp:TextBox></div><div><asp:RequiredFieldValidator ID="rfvAnswer3" runat="server" ControlToValidate="txtBoxAnswer3" ErrorMessage="Answer 3 cannot be blank"><i runat="server" class="material-icons red rotate-45">&#xE147;</i></asp:RequiredFieldValidator></div></div>
            </div>
        </div>
        <table>
            <tr>
                <td><asp:ValidationSummary ID="validationSummary" runat="server" /></td> <td class="ErrorMessage"><ml:PassthroughLiteral ID="lblSecurityQuestionsMsg" runat="server" /> </td>
            </tr>
        </table>
        <div class="login-page password-reset-page">
            <div class="table reduce-margin-top">
                <div>
                    <div class="w-92"></div>
                    <div>
                        <button type="button" onclick="javascript:window.location=<%=AspxTools.JsString("../simple_login.aspx?lenderpmlsiteid=" + m_guidLenderSiteId.ToString())%>">Cancel</button>
                        <button id="btnVerifyAnswers" runat="server" type="button" OnServerClick="btnVerifyAnswers_OnClick">Verify Answers</button>
                    </div>
                </div>
            </div>
        </div>
    <%} %>

    <%if (QuestionsVerified && !PasswordChanged)
      {%>
        </td>
        </tr>

        <tr>
            <td style="text-align:left;">
                <p><strong>Set New Password</strong></p>
                <div class="table">
                    <div>
                        <div class="label-login text-right w-171"><strong>New Password</strong></div>
                        <div class="padding-right-p8">
                            <asp:textbox id="txtPassword" TextMode="Password" runat="server" autocomplete="off" ></asp:textbox>
                        </div>
                        <div>
                            <i class="material-icons" id="imgPasswordCheck" style="display:none"></i>
                        </div>
                    </div>
                    <div>
                        <div class="label-login text-right w-171"><strong>Retype New Password</strong></div>
                        <div class="padding-right-p8">
                            <asp:textbox id="txtVerifyPass" TextMode="Password" runat="server" autocomplete="off"></asp:textbox>
                        </div>
                        <div>
                            <i class="material-icons" id="imgVerifyRetypePass" style="display:none"></i>
                        </div>
                    </div>
                </div>
                <p class="ErrorMessage"><ml:PassthroughLiteral ID="lblPasswordError" runat="server" /></p>
                <div class="padding-left-p171 padding-top-p16">
                    <button type="button" onclick='javascript:window.history.back();'>Cancel</button>
                    <button id="btnChangePass" runat="server" type="button" OnServerClick="btnChangePass_OnClick">Change</button>
                </div>
                <div class="padding-left-p171 padding-top-p16">
                    <!-- <b>Your password must follow these guidelines:</b> -->
                    <ul class="passwordRules">
                        <li><i class="material-icons" id="imgRule2" style="display:none" visible="false"></i>Password must have a minimum of 6 characters.</li>
                        <li><i class="material-icons" id="imgRule1" style="display:none" visible="false"></i>Password must contain at least one numeric and one alphabetic character.</li>
                        <li><i class="material-icons" id="imgRule7" style="display:none"></i>Password cannot contain first name, last name, or login name.</li>
                        <li><i class="material-icons" id="imgRule4" style="display:none"></i>Password cannot contain 3 identical characters in a row (Example: AAAsample1, sample1111).</li>
                        <li><i class="material-icons" id="imgRule5" style="display:none"></i>Password cannot contain 3 consecutive alphabetical or numeric characters in a row (Example: sampleABC, MNOsample, sampleOPQsample, sample123sample).</li>
                        <li><i class="material-icons" id="imgRule6" style="display:none"></i>Password cannot be the same as the previous password.</li>
                        <li><i class="material-icons" id="imgRule3" style="display:none"></i>Passwords can only contain safe characters. Not Allowed: < > \" ' % ; ) ( & + - =</li>
                    </ul>
                </div>
              </td>
        </tr>
    </table>
    <%} %>

     <%if (PasswordChanged)
       {%>
      <b>Successfully Changed Password </b>
      <br/>
      Your password may now be used to <a href='#' onclick='javascript:window.location=<%=AspxTools.SafeUrl(this.SignInUrl)%>'>sign in</a>      
      <% }%>
    </center>
    </form>
</body>
</html>
