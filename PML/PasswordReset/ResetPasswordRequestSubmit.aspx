﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPasswordRequestSubmit.aspx.cs" Inherits="PriceMyLoan.ResetPasswordRequestSuccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Password Reset Instructions Sent</title>
</head>
<body>
    <form id="form1" runat="server">
    <center>
    <table width="50%">
        <tr>
            <td align="left" style="font-size:20px">Password Reset Instructions Sent</td>
        </tr>
        <tr>
            <td align="left" style="font-size:12px"><asp:Label ID="m_lblEmail" Text="Password reset instructions were sent to: " runat="server" /></td>
        </tr>
        <tr>
            <td><br /><br /></td>
        </tr>
        <tr>
            <td align="left" style="font-size:12px"><strong>If you don't see the e-mail message in the inbox, check your junk mail folder or check again in a few minutes</strong></td>
        </tr>
    </table>
    </center>
    </form>
</body>
</html>
