﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetSecurityQuestions.aspx.cs"
    Inherits="PriceMyLoan.PasswordReset.SetSecurityQuestions" %>

<%@ Register Src="../PasswordReset/SetUserSecurityQuestions.ascx" TagName="SecurityQuestions"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Set Security Information</title>
</head>
<body>

    <script type="text/javascript">
    function f_logout()
    {
      self.location = gVirtualRoot + "/logout.aspx";
    }
    $j(document).ready(function () {
        TPOStyleUnification.Components();
    });
    </script>
    <form id="form1" runat="server">
    <table width="100%" id="LogOffPanel" runat="server">
        <tr>
            <td align="right">
                <input type="button" value="Log Off" onclick="f_logout();" value="LOG OFF" style="font-family: Verdana, Arial, Helvetica, sans-serif;
                    font-size: 11px; font-weight: bold; color: Black; margin-top: 5; margin-right: 15;" />
            </td>
        </tr>
    </table>
    <div style="width: 50em; margin: 2em">
        <header class="page-header">Set Security Questions</header>
        <div>
            <b>Please choose three different questions and provide a different answer for each one.
                <br/>These questions will be asked if you forget your password.</b>
        </div>
        <br />
        <table>
            <tr>
                <td>
                    <uc:SecurityQuestions ID="SecurityQuestions" runat="server" EditMode="Create" />
                </td>
            </tr>
        </table>
        <div class="text-danger">
            <ml:EncodedLiteral ID="lblStatus" runat="server" EnableViewState="false" />
        </div>
        <br />
        <div width="100%" style="text-align: center">
            <button ID="btnContinue" runat="server" onserverclick="btnContinue_Click">Not Now</button>
            <button ID="btnSubmit" runat="server" onserverclick="btnSubmit_Click">Submit</button>
        </div>
    </div>
    </form>
</body>
</html>
