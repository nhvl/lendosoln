﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PriceMyLoan.Security;
using PriceMyLoan.Common;
using LendersOffice.Common;

namespace PriceMyLoan.PasswordReset
{
    public partial class SetSecurityQuestions : PriceMyLoan.UI.BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            LogOffPanel.Visible = false;
            lblStatus.Visible = false;
            string msg = RequestHelper.GetSafeQueryString("msg");
            if (!string.IsNullOrEmpty(msg))
            {
                lblStatus.Visible = true;
                lblStatus.Text = msg;
            }
            SecurityQuestions.SetUserInfo(PriceMyLoanUser.BrokerId, PriceMyLoanUser.UserId);
        }

        protected void btnSubmit_Click(object sender, System.EventArgs e)
        {
            bool bRet = SecurityQuestions.Save();
            if (!bRet)
            {
                Response.Redirect("SetSecurityQuestions.aspx?msg=" + Server.HtmlEncode(SecurityQuestions.StatusMessage));
            }
            else
            {
                DisplayMessage("Your security answers have been saved.");                
                Response.Redirect(PriceMyLoanConstants.PipelineUrl + "?isLoadingDashboard=true");
            }
        }

        protected void btnContinue_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(PriceMyLoanConstants.PipelineUrl + "?isLoadingDashboard=true");
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        private void DisplayMessage(string sError)
        {
            lblStatus.Visible = true;
            lblStatus.Text = sError;
        }
    }
}
