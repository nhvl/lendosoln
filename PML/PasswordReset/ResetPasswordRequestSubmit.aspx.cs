﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.AntiXss;

namespace PriceMyLoan
{
    public partial class ResetPasswordRequestSuccess : PriceMyLoan.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            m_lblEmail.Text += Request.QueryString["Email"];
        }
    }
}
