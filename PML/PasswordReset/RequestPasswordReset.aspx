﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestPasswordReset.aspx.cs" Inherits="PriceMyLoan.RequestPasswordReset" %>
<%@Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reset Password</title>
    <script type="text/javascript">
    <!--
        function OnInit() {
            var loginBox = document.getElementById("txtLogin");
            if (loginBox != null) {
                loginBox.focus();
            }
        }

        jQuery(function($) {

            var elementIsValid =
                {
                    email: false,
                    login: false
                };
            var formWasValid = false;
            function validateForm() {
                var valid = true;
                $.each(elementIsValid, function(idx, val) {
                    return valid = valid && val;
                });

                if (valid && $('span[class=error]:visible').length == 0) {
                    $('#btnSendInstructions').removeAttr('disabled');
                    if (!formWasValid) $('#btnSendInstructions').focus();
                }
                else {
                    $('#btnSendInstructions').attr('disabled', 'disabled');
                }

                formWasValid = valid;
            }

            function makeValidator(element, regex, errorMessage, which) {
                var errorField = $('<span class="error">' + errorMessage + '<i class="material-icons red required">&#xE147;</i></span>');
                $(element).after(errorField);
                errorField.hide();
                return function() {
                    if (regex.test($(element).val())) {
                        errorField.hide();
                        elementIsValid[which] = true;
                    }
                    else {
                        errorField.show();
                        elementIsValid[which] = false;
                    }

                    validateForm();
                }
            }

            $('#txtLogin').blur(makeValidator($('#txtLogin'), /.+/, "Login is required", 'login'));
            $('#txtEmail').blur(makeValidator($('#txtEmail'), /.+/, "Email is required", 'email'));
            $('#txtEmail').blur(makeValidator($('#txtEmail'), /^([\w._%+-]+@[\w.-]+\.[A-Za-z]{2,6}|)$/, "Invalid email address", 'email'));
            validateForm();
        });
    //-->
    </script>
</head>

<body onload="OnInit();">
    <form id="form1" runat="server">
    <center>
    <div id="divResetForm" runat="server" style="overflow:auto; min-width:500px">
        <div id="main" class="login-page password-reset-page">
            <h3 class="title">Reset Your Password</h3>
            <p class="padding-bottom-p8"><strong>Receive Instructions By Email</strong></p>
            <p>Please enter your login and e-mail address registered with <span runat="server" id="RegisteredPlatform">PriceMyLoan</span>.</p>
            <p>
                <asp:ValidationSummary ID="errSummary" Runat="server" CssClass="ErrorMsg" DisplayMode="BulletList"/>
            </p>
            <div class="table">
                <div>
                    <div class="label-login text-right"><strong>Login</strong></div>
                    <div><asp:textbox id="txtLogin" Width="200px" runat="server" autocomplete="off" NoHighlight="true"></asp:textbox></div>
                </div>
                <div>
                    <div class="label-login text-right"><strong>E-mail</strong></div>
                    <div><asp:textbox id="txtEmail" Width="200px" runat="server" autocomplete="off" NoHighlight="true"></asp:textbox></div>
                </div>
                <div>
                    <div></div>
                    <div>
                        <button type="button" onclick='javascript:window.history.back();'>Cancel</button>
                        <button id="btnSendInstructions" type="button" OnServerClick="btnSendInstructions_OnClick" runat="server">Submit</button>
                    </div>
                </div>
            </div>
        </div>
      </div>

      <div id="divSubmitMessage" runat="server" style=" width:50%; text-align:left">
          <br />
          <br />
          <label style="font-size:14px"> <b>Password Reset Instructions Sent</b></label>
          <br />If an account exists with that login/email combination, you will receive an email shortly. If you do not receive an email, try again or contact your administrator. 
      </div>
    </center>
    </form>
</body>
</html>
