﻿using System;
using System.Web.UI;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.UI.Themes;
using LendersOffice.Security;

namespace PriceMyLoan
{
    public partial class RequestPasswordReset : PriceMyLoan.UI.BasePage
    {
        private const string m_sSenderEmailId = "Do_Not_Reply@pricemyloan.com";  

        public override string StyleSheet
        {
            get { return VirtualRoot + "/css/style.css"; }
        }

        private Guid BrokerPmlSiteId
        {
            get { return RequestHelper.GetGuid("LenderPmlSiteId", Guid.Empty); }
        }

        protected override bool IncludePrincipalSpecificStyling => false;

        private void DisplaySuccessful()
        {
            divResetForm.Visible = false;
            divSubmitMessage.Visible = true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string cmd = RequestHelper.GetSafeQueryString("cmd");

            if (!Page.IsPostBack && cmd == "success")
            {
                DisplaySuccessful();
            }
            else
            {
                divSubmitMessage.Visible = false;
            }

            if (this.BrokerPmlSiteId == Guid.Empty)
            {
                this.RegisteredPlatform.InnerText = "LendingQB";
            }
        }

        protected void btnSendInstructions_OnClick(object sender, EventArgs e)
        {
            string login = txtLogin.Text.TrimWhitespaceAndBOM().ToLower();
            string email = txtEmail.Text.TrimWhitespaceAndBOM().ToLower();

            if (this.BrokerPmlSiteId == Guid.Empty)
            {
                this.SendRetailInstructions(login, email);
            }
            else
            {
                this.SendPmlInstructions(login, email);
            }
        }

        private void SendRetailInstructions(string login, string email)
        {
            string userFullName = string.Empty;
            Guid brokerId;

            Guid guidPasswordReset = BrokerUserDB.CreatePasswordResetRequest_Broker(login, email, out userFullName, out brokerId);
            if (guidPasswordReset == Guid.Empty)
            {
                // OPM 246520. Unsuccessful attempt to create a request.  
                // Update the failure counts and do appropriate locking.
                BrokerUserDB.IncrementPasswordResetFailureCount(login, "B", Guid.Empty, email);
            }
            else
            {
                // 6/2/2010 dd - Due to Citi Requirement, we must always perform Redirect after POST.
                SendPasswordResetEmail(brokerId, guidPasswordReset, email, userFullName, isRetail: true);
            }
            
            Response.Redirect("RequestPasswordReset.aspx?cmd=success");
        }

        private void SendPmlInstructions(string login, string email)
        {
            string sUserFullName = string.Empty;
            Guid brokerId;

            Guid guidPasswordReset = BrokerUserDB.CreatePasswordResetRequest_Pml(login, email, this.BrokerPmlSiteId, out sUserFullName, out brokerId);
            if (guidPasswordReset == Guid.Empty)
            {
                // OPM 246520. Unsuccessful attempt to create a request.  
                // Update the failure counts and do appropriate locking.
                BrokerUserDB.IncrementPasswordResetFailureCount(login, "P", this.BrokerPmlSiteId, email);
            }
            else
            {
                // 6/2/2010 dd - Due to Citi Requirement, we must always perform Redirect after POST.
                SendPasswordResetEmail(brokerId, guidPasswordReset, email, sUserFullName, isRetail: false);
            }
            
            Response.Redirect("RequestPasswordReset.aspx?LenderPmlSiteId=" + this.BrokerPmlSiteId + "&cmd=success");
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        private void SendPasswordResetEmail(Guid brokerId, Guid requestId, string recipientEmail, string recipientName, bool isRetail)
        {
            string sUrl = Request.Url.Scheme + "://" + Request.Url.Host + Tools.VRoot + "/PasswordReset/ResetPassword.aspx?g=" + requestId;

            if (!isRetail)
            {
                sUrl += "&lenderpmlsiteid=" + BrokerPmlSiteId;
            }

            string sSubject = $"Reset your {(isRetail ? "LendingQB" : "PriceMyLoan")} password";
            string sBody = @"Dear " + recipientName + @",<br /><br />To reset your password, click on the link below to complete the process.  If you are having trouble accessing the link, copy and paste the link into the address bar of Internet Explorer.
<br /><br /><a href='" + sUrl + @"'>" + sUrl + @"</a> <br /><br />Thank you";

            var cbe = new LendersOffice.Email.CBaseEmail(brokerId)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = m_sSenderEmailId,
                To = recipientEmail,
                Subject = sSubject,
                Message = sBody,
                IsHtmlEmail = true,
                BccToSupport = false
            };
            cbe.Send();
        }        
    }
}
