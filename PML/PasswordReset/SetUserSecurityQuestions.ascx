﻿<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetUserSecurityQuestions.ascx.cs"
    Inherits="PriceMyLoan.PasswordReset.SetUserSecurityQuestions" %>

<script type="text/javascript">
  function f_changeAnswer(link, i)
  {
      $j(link).css('display', 'none');
      $j('#' + 'Answer' + i + 'Modified').val('1');
      var id = <%=AspxTools.JsString(ClientID) %> + '_txtBoxAnswer' + i;
      var tb = $j('#'+id);
      tb.css('display', '');
  }
</script>

<div class="table">
    <div>
        <div>
            <label>Question 1</label>
            <asp:DropDownList ID="dropDownQuestion1" runat="server" />
        </div>
        <div>
            <label>Answer 1</label>
            <asp:TextBox ID="txtBoxAnswer1" runat="server" Style="display: none" /><a href="#"
                onclick="f_changeAnswer(this, 1);">change answer</a>
        </div>
    </div>
    <div>
        <div>
            <label>Question 2</label>
            <asp:DropDownList ID="dropDownQuestion2" runat="server" />
        </div>
        <div>
            <label>Answer 2</label>
            <asp:TextBox ID="txtBoxAnswer2" runat="server" Style="display: none" /><a href="#"
                onclick="f_changeAnswer(this, 2);">change answer</a>
        </div>
    </div>
    <div>
        <div>
            <label>Question 3</label>
            <asp:DropDownList ID="dropDownQuestion3" runat="server" />
        </div>
        <div>
            <label>Answer 3</label>
            <asp:TextBox ID="txtBoxAnswer3" runat="server" Style="display: none" /><a href="#"
                onclick="f_changeAnswer(this, 3);">change answer</a>
        </div>
    </div>
</div>

