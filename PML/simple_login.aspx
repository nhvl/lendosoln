<!DOCTYPE html>

<%@ Page language="c#" Codebehind="index.aspx.cs" AutoEventWireup="false" ValidateRequest="false" Inherits="PriceMyLoan.UI.index" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>

<html>
  <head runat="server">
    <title>PriceMyLoan</title>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE, NO-STORE">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
</head>

<body MS_POSITIONING="FlowLayout" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 id="LP_f6d1d39dee3c435d9d14f1dc245e316d">
    <style type="text/css">
    #MessageLabel p
    {
        margin-bottom: 0px;
    }
    #MessageLabel ul
    {
        text-align: left;
        margin-top: 0px;
    }
    </style>
    <form id="index" method="post" runat="server">
    <input type="hidden" id="m_ValidBrowser" value=true runat=server name="index_data" />

<br /><br /><br />
<table width="100%">

  <tr><td align="center">  
  <table width="420" class="login-page">
      <tr>
        <td align="right" class="label-login"><strong>Login</strong></td>  
        <td align="right">
            <asp:textbox id="LoginName" runat="server" autocomplete="off" TabIndex="1" NoHighlight="true"></asp:textbox>
        </td>
        <td>
            <a href="help/loginHelp.htm" id="A1" style="PADDING-LEFT:10px" target="_blank">Forgot login?</a>
        </td>
      </tr>
      <tr>
        <td align="right" class="label-login"><strong>Password</strong></td>
        <td align="right">
            <asp:textbox id=Password runat="server" textmode="Password" autocomplete="off" TabIndex="2" NoHighlight="true"></asp:textbox>
        </td>
        <td>
            <a href="#" onclick="RedirectToResetPassword()" id="A2" style="PADDING-LEFT:10px">Forgot password?</a>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="3">
            <b>
            <ml:EncodedLabel id="m_ErrMsg" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel>
            <ml:EncodedLabel id="m_ErrMsg2" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel>
            </b>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="3"><span id="MessageLabel"></span></td>
      </tr>
      <tr>
        <td colspan="3" class="text-center">
		    <asp:Button id="m_login" runat="server" Text="Login" TabIndex="3" class="btn btn-default" onclick="OnLoginClick"></asp:Button>
		</td>
		<td></td>
      </tr>
  </table>
  </td></tr>
  <tr><td align="center">&nbsp;</td></tr>
  <tr><td align="center">&nbsp;</td></tr>
  <tr><td align="center">&nbsp;</td></tr>  
  <tr><td align="center">&copy;<%= AspxTools.HtmlString(DateTime.Today.Year.ToString()) %> PriceMyLoan, All Rights Reserved.</td></tr>
</table>

<br /><br /><br />
     </form>
  </body>
</html>
