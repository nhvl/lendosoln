﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuthenticationCodePage.aspx.cs" Inherits="PriceMyLoan.AuthenticationCodePage" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Verification</title>
    <style type="text/css">
        body {
            padding: 10px;
        }

        .company-display {
            height: 38px;
            width: 185px;
            border: none;
            font-weight: bold;
            font-size: 16px;
            font-family: Arial;
        }
    </style>
</head>
<body>
    <script type="text/javascript">

        var g_pin = "";

        $(function () {

            $("#HelpLink").click(function () { displayDialog(); });

        });

        function displayDialog() {
            $("#HelpDialog").dialog({
                resizable: true,
                modal: true,
                width: "350px",
                height: 'auto',
                position: { my: "top", at: "top", of: window },
                close: function () {
                }
            }).css({
                minHeight: 400
            });
        }


    </script>
    <form id="form1" runat="server">
        <img runat="server" id="CompanyLogo" />
        <span runat="server" id="CompanyName" class="company-display"></span>
        <br />
        <br />
        <h2>Site Security: Authentication Code Required</h2>
        <div>An authentication code is required to proceed. Please follow the steps below to complete the login process.</div>
        <hr />
        <div>
            <div id="ErrorMessagePanel" runat="server" class="errorPanel" visible="false">
                <ml:EncodedLiteral ID="ErrorMessage" runat="server" />
            </div>
            <table width="800" border="0">
                <tr>
                    <td valign="top" style="padding-left: 10px">
                        <div style="font-weight: bold">Step 1.  Get an authentication code.</div>
                           <span runat="server" id="Step1Text"></span>
		        <br />
                        <br />
                        <asp:Button ID="btnSendAuthenticationCode" runat="server" />
                        <br />
                        <br />
                       <div style="font-weight:bold">Step 2. Enter your authentication code below.</div>

                        <asp:TextBox runat="server" ID="SecurityPin" onkeyup="this.value=this.value.replace(/[^\d]+/,'')"  autocomplete="off" >  </asp:TextBox>
                        <br />
                        <br />
                        <asp:CheckBox ID="m_registerCb" Text="Register this computer to login without an authentication code for the next 30 days. Please leave this unchecked if you are accessing from someone else's computer." runat="server" />
                        <br />
                        <br />
                        <span runat="server" id="CellphoneContainer">
                            <label class="FieldLabel">Cell Phone Number</label>
                            <asp:TextBox preset="phone" ID="Cellphone" runat="server"></asp:TextBox>
                            <br />
                            <a href="#" id="HelpLink">why is this needed?</a>
                            <br />
                            <br />
                        </span>
                        <asp:Button ID="btnSubmit" Text="Continue" runat="server" />
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="HelpDialog" class="dialog" style="display: none;">
            <div style="border-top: solid 2px black; border-bottom: solid 2px black;">
                <label class="FieldLabel">Why are we asking for your cell phone number?</label></div>
            <br />
            <div>
                If you ever use a computer/network that is not registered in the system, you will need an authentication code to bypass the security restriction.  By recording your cell phone number in your account, you will be able to conveniently and securely get an authentication code sent to you via text message.  Text message rates may apply from your cellphone carrier.
            
            <br />
                <br />
                <label class="FieldLabel">Privacy Policy</label>
                <br />
                <span>We will not share or sell your cell phone number with any third party companies or use it for telephone solicitation.  The sole purpose of recording your cell phone number is to provide you with a way to access the system from computer/networks that are not registered.
                </span>
            </div>
        </div>
    </form>
</body>
</html>
