﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.Security;
using PriceMyLoan.Common;
using LendersOffice.Common;
using System.Web.Services;
using LendersOffice.ObjLib.QuickPricer;
using LendersOffice.Security;
using DataAccess;
using LqbGrammar.DataTypes;

namespace PriceMyLoan
{
    public partial class QuickPricer2RateMonitors : PriceMyLoan.UI.BasePage
    {
        private PriceMyLoanPrincipal CurrentPMLPrincipal
        {
            get
            {
                return this.User as PriceMyLoanPrincipal;
            }
        }
        private AbstractUserPrincipal CurrentAbstractPrincipal
        {
            get
            {
                return this.User as PriceMyLoanPrincipal;
            }
        }

        private bool IsEmbeddedPML
        {
            get
            {
                return PriceMyLoanConstants.IsEmbeddedPML;
            }
        }

        private E_PortalMode portalMode;

        protected override void OnInit(EventArgs e)
        {
            this.Init += new EventHandler(this.PageInit);
            base.OnInit(e);
        }

        protected void PageInit(object sender, EventArgs e)
        {
            if (false == CurrentPMLPrincipal.IsActuallyUsePml2AsQuickPricer)
            {
                throw new AccessDenied("Can not access rate monitors without new quickpricer enabled.");
            }
            if (false == CurrentPMLPrincipal.BrokerDB.AllowRateMonitorForQP2FilesInTpoPml)
            {
                throw new AccessDenied("Your lender has not enabled rate monitors scenario tracking.");
            }

            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            RegisterJsScript("LQBNestedFrameSupport.js");
            RegisterJsScript("jquery.tmpl.js");
            RegisterJsScript("tableUpdater.js");
            RegisterJsScript("rateMonitorSet.js");


            portalMode = PriceMyLoanUser.PortalMode;
            if (false == Page.IsPostBack)
            {
                var scenarios = QuickPricer2LoanPoolManager.GetRateMonitoredLoansByUserID(
                    CurrentPMLPrincipal.BrokerId,
                    CurrentPMLPrincipal.UserId);

                var scenariosAndUrls = from s in scenarios
                                       where 
                                       ((s.BranchChannelT == E_BranchChannelT.Wholesale || s.BranchChannelT == E_BranchChannelT.Blank) && portalMode.Equals(E_PortalMode.Broker)) ||
                                       (s.BranchChannelT == E_BranchChannelT.Correspondent && s.CorrespondentProcessT.Equals(E_sCorrespondentProcessT.MiniCorrespondent) && portalMode.Equals(E_PortalMode.MiniCorrespondent)) ||
                                       (s.BranchChannelT == E_BranchChannelT.Correspondent && (s.CorrespondentProcessT.Equals(E_sCorrespondentProcessT.PriorApproved) || s.CorrespondentProcessT.Equals(E_sCorrespondentProcessT.Delegated) || s.CorrespondentProcessT.Equals(E_sCorrespondentProcessT.Blank)) && portalMode.Equals(E_PortalMode.Correspondent)) ||
                                       ((s.BranchChannelT == E_BranchChannelT.Retail || s.BranchChannelT == E_BranchChannelT.Blank) && portalMode.Equals(E_PortalMode.Retail))
                                       orderby s.Name
                                       select new
                                       {
                                           LoanID = s.LoanID,
                                           Name = s.Name,
                                           URL = ResolveClientUrl(@"~\webapp\pml.aspx?source=PML&loanid=" + s.LoanID)
                                       };

                RegisterJsObjectWithJavascriptSerializer(
                    "rateMonitorSets",
                    scenariosAndUrls);
            }
        }

        [WebMethod]
        public static void DeleteSet(Guid loanID)
        {
            try
            {
                QuickPricer2LoanPoolManager.DeleteMonitoredScenario((AbstractUserPrincipal)LendersOffice.Security.PrincipalFactory.CurrentPrincipal, loanID);
            }
            catch (Exception e)
            {
                var msg = "DeleteSet failed for loan" + loanID;
                Tools.LogError(msg, e);
                throw;
            }
        }
    }
}
