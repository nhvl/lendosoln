﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PriceMyLoan
{
    public partial class _MaskTestPage : PriceMyLoan.UI.BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.ComboBoxControl.UseJqueryScript = true; // 2/7/2012 dd - This is require to make control cross browser compatible.
            for (int i = 0; i < 5; i++)
            {
                this.ComboBoxControl.Items.Add("Item " + i + " with <html> tag");
            }

            this.smartzipcodeControl.SmartZipcode(CityControl, StateControl, countyControl);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
