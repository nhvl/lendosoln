﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_MaskTestPage.aspx.cs" Inherits="PriceMyLoan._MaskTestPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Common Control Test Pages</title>
    <script type="text/javascript" src="/PML/inc/SyntaxHighlighter/shCore.js" ></script>
    <script type="text/javascript" src="/PML/inc/SyntaxHighlighter/shBrushJScript.js"></script>
    <script type="text/javascript" src="/PML/inc/SyntaxHighlighter/shBrushCSharp.js"></script>
    <script type="text/javascript" src="/PML/inc/SyntaxHighlighter/shBrushXml.js"></script>
    
    <link href="/PML/inc/SyntaxHighlighter/shCore.css" runat="server" rel="Stylesheet" type="text/css" />
    <link href="/PML/inc/SyntaxHighlighter/shThemeDefault.css" runat="server" rel="Stylesheet" type="text/css" />
</head>
<body>
    <script src="/PML/inc/mask.js" type="text/javascript"></script>
    <script type="text/javascript">
      jQuery(function()
      {
        SyntaxHighlighter.defaults['gutter'] = false;
        SyntaxHighlighter.defaults['toolbar'] = false;
        SyntaxHighlighter.all();
        
        $j('.Popup').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            LQBPopup.Show(ML.VirtualRoot + '/help/Q00001.html');
        });

      });
    </script>
    <script type="text/javascript">
        $j(document).ready(function() {
            $j('#taReadOnly').readOnly(true);
            $j('#iReadOnly').readOnly(true);
        });
    </script>
    <form id="form1" runat="server">
    <div>
    <h1>Common Control Demo Pages</h1>
    <table border="1">
    <tr>
      <td>Control</td>
      <td>&nbsp;</td>
      <td>Code</td>
      <td>Notes</td>
    </tr>
    <tr>
        <td>ReadOnly textarea</td>
        <td><textarea id='taReadOnly' >cant change me</textarea></td>
        <td><pre class="brush: xml;">
        &lt;textarea id='taReadOnly'  &gt;cant change me&lt;/textarea&gt;
        </pre>
        <pre class="brush: javascript">$j('#taReadOnly').readOnly(true);
        </pre></td>
        <td></td>
    </tr>
    <tr>
        <td>ReadOnly input</td>
        <td><input type="text" id="iReadOnly" value='cant change me'/></td>
        <td><pre class="brush: xml;">
        &lt;input id='iReadOnly'  &gt;cant change me&lt;/textarea&gt;
        </pre>
        <pre class="brush: javascript">$j('#iReadOnly').readOnly(true);
        </pre</td>
        <td></td>
    </tr>
    <tr>
        <td><i>nonstandard</i></td>
        <td><button type="button" onclick="thisfunctionisnotdefined();">Cause Error</button></td>
        <td><pre class="brush: xml;">
        &lt;button type="button" onclick="thisfunctionisnotdefined();"&gt;Cause Error&lt;/button&gt;
        </pre></td>
        <td>Causes an error, which should also be logged as a bug in paul bunyan</td>
    </tr>
    <tr>
      <td>Zipcode</td>
      <td><ml:ZipcodeTextBox ID="ZipcodeControl" runat="server" width="50" preset="zipcode"/></td>
      <td>
        <pre class="brush: xml;">
        &lt;ml:ZipcodeTextBox ID="ZipcodeControl" runat="server" width="50" preset="zipcode"/&gt;
        </pre>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>Zipcode with auto populate city/county/state</td>
      <td valign="top">
      Zip: <ml:ZipcodeTextBox ID="smartzipcodeControl" runat="server" Width="50" preset="zipcode" />
      <br />
      County: <asp:TextBox ID="countyControl" runat="server" />
      <br />
      City: <asp:TextBox ID="CityControl" runat="server" />
      <br />
      State: <ml:StateDropDownList id="StateControl" runat="server" />
      </td>
      <td valign="top">
      ASPX
      <pre  class="brush: xml">
      Zip: &lt;ml:ZipcodeTextBox ID="smartzipcodeControl" runat="server" Width="50" preset="zipcode" /&gt;
      &lt;br /&gt;
      County: &lt;asp:TextBox ID="countyControl" runat="server" /&gt;
      &lt;br /&gt;
      City: &lt;asp:TextBox ID="CityControl" runat="server" /&gt;
      &lt;br /&gt;
      State: &lt;ml:StateDropDownList id="StateControl" runat="server" /&gt;      
      </pre>
      Server Size C#
      <pre class="brush: csharp">
        protected void Page_Init(object sender, EventArgs e)
        {
            smartzipcodeControl.SmartZipcode(CityControl, StateControl, countyControl);
        }
      </pre>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>SSN</td>
      <td><ml:SSNTextBox ID="SsnControl" runat="server" width="75" preset="ssn"/></td>
      <td>
        <pre class="brush: xml">
        &lt;ml:SSNTextBox ID="SsnControl" runat="server" width="75" preset="ssn"/&gt;
        </pre>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>Phone</td>
      <td><ml:PhoneTextBox ID="PhoneControl" runat="server"  with="120" preset="phone"/></td>
      <td>
        <pre class="brush: xml">
        &lt;ml:PhoneTextBox ID="PhoneControl" runat="server" with="120" preset="phone"/&gt;
        </pre>
      </td>
      <td></td>
      
    </tr>
    <tr>
      <td>Date</td>
      <td><ml:DateTextBox ID="DateTextControl" runat="server" with="50" preset="date" /></td>
      <td>
        <pre class="brush: xml">
        &lt;ml:DateTextBox ID="DateTextControl" runat="server" with="50" preset="date"/&gt;
        </pre>
      </td>
      <td></td>
      
    </tr>
    <tr>
      <td>Percent</td>
      <td><ml:PercentTextBox ID="PercentControl" runat="server" with="70" preset="percent"/></td>
      <td>
        <pre class="brush: xml">
        &lt;ml:PercentTextBox ID="PercentControl" runat="server" with="70" preset="percent"/&gt;
        </pre>
      </td>
      <td></td>
      
    </tr>
    <tr>
      <td>Money</td>
      <td><ml:MoneyTextBox ID="MoneyControl" runat="server" with="75" preset="money"/></td>
      <td>
        <pre class="brush: xml">
        &ltml:MoneyTextBox ID="MoneyControl" runat="server" with="75" preset="money"/&gt;
        </pre>
      </td>
      <td></td>
      
    </tr>
    <tr>
      <td>Combo Box</td>
      <td><ml:ComboBox ID="ComboBoxControl" runat="server" /></td>
      <td>
        ASPX
        <pre class="brush: xml">
        &lt;ml:ComboBox ID="ComboBoxControl" runat="server" /&gt;
        </pre>
        Server Side C#
        <pre class="brush: csharp">
        protected void Page_Init(object sender, EventArgs e)
        {
			//This is require to make control cross browser compatible.
            this.ComboBoxControl.UseJqueryScript = true; 
            for (int i = 0; i &lt; 5; i++)
            {
                this.ComboBoxControl.Items.Add("Item " + i + " with &lt;html&gt; tag");
            }
        }			
        </pre>
        
      </td>
      <td>

      </td>
      
    </tr>
    <tr>
        <td>IFrame Popup</td>
        <td>
            <a href="#" class="Popup">Open Help Page</a>
        </td>
        <td>
            Opener   ASPX
<pre class="brush: xml">
&lt;a href=&quot;#&quot; class=&quot;Popup&quot;&gt;Open Help Page&lt;/a&gt;
</pre>
<pre class="brush: js">
$j(&#39;.Popup&#39;).click(function(){
    e.stopPropagation();
    e.preventDefault();
    LQBPopup.Show(ML.VirtualRoot + &#39;/help/Q00001.aspx&#39;);
});
</pre>

        </td>
        <td>&nbsp;</td>
    </tr>
    
    
    </table>
    </div>
    </form>

</body>
</html>
