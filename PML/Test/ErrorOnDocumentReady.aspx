﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorOnDocumentReady.aspx.cs" Inherits="PriceMyLoan.Test.ErrorOnDocumentReady" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        $j(document).ready(function() {
            alertanundefinedfunction();
        });</script>
    <div>
    This page should have logged the error that was caused on document being ready.
    </div>
    </form>
</body>
</html>
