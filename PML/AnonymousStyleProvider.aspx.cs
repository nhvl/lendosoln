﻿namespace PriceMyLoan
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.UI.Themes;

    /// <summary>
    /// Provides a means for sending customized CSS content to a client
    /// when no principal-related data is available.
    /// </summary>
    public partial class AnonymousStyleProvider : StyleProvider
    {
        /// <summary>
        /// Gets the style content.
        /// </summary>
        /// <returns>
        /// The style content.
        /// </returns>
        protected override string GetStyleContent()
        {
            var lenderPmlSiteId = RequestHelper.GetGuid("lenderpmlsiteid", Guid.Empty);
            var colorTheme = SassUtilities.GetColorThemeFromLenderPmlSiteId(lenderPmlSiteId);
            return SassUtilities.CompileColorTheme(colorTheme);
        }
    }
}