//
// Show modeless dialogs.  This simple api is used to
// provide a single implementation so that one change
// quickly propogates throughout the application.
// Note that we return the window, not the args.
//

var g_CurrentModelessDlg = null;

function showSoloModeless( sUrl , sOpts, sTitle ) 
{ 
    if (null == sTitle)
      sTitle = "";
      
	  var w = null , a = new Object;
  	
	  a.opener = window;
	  a.OK     = false;

	  if( g_CurrentModelessDlg != null )
	  {
		  g_CurrentModelessDlg.close();
		  g_CurrentModelessDlg = null;
	  }

	  if( sOpts == null )
	  {
		  w = g_CurrentModelessDlg = window.showModelessDialog
			  ( VRoot + "/Common/ModalDlg/DialogFrame.aspx?url=" + escape( VRoot + sUrl ) + "&title=" + escape(sTitle)
			  , a
			  , "dialogWidth: 200px; dialogHeight: 200px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;"
			  );
	  }
	  else
	  {
		  w = g_CurrentModelessDlg = window.showModelessDialog
			  ( VRoot + "/Common/ModalDlg/DialogFrame.aspx?url=" + escape( VRoot + sUrl ) + "&title=" + escape(sTitle)
			  , a
			  , sOpts
			  );
	  }

	  w.opener = window;

	  return w;
}

function showModeless( sUrl , sOpts, sTitle ) { try
{
    if (null == sTitle)
      sTitle = "";

	var w = null , a = new Object;
	
	a.opener = window;
	a.OK     = false;

	if( g_CurrentModelessDlg != null )
	{
		g_CurrentModelessDlg.close();
		g_CurrentModelessDlg = null;
	}

	if( sOpts == null )
	{
		w = window.showModelessDialog
			( VRoot + "/Common/ModalDlg/DialogFrame.aspx?url=" + escape( VRoot + sUrl ) + "&title=" + escape(sTitle)
			, a
			, "dialogWidth: 200px; dialogHeight: 200px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;"
			);
	}
	else
	{
		w = window.showModelessDialog
			( VRoot + "/Common/ModalDlg/DialogFrame.aspx?url=" + escape( VRoot + sUrl ) + "&title=" + escape(sTitle)
			, a
			, sArgs
			);
	}
	
	w.opener = window;

	return w;
}
catch( e )
{
	// Oops!
    logJSException(e, 'problem showing Modeless Dialog.')
	throw e;
}}
