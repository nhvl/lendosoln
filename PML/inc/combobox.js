(function($)
{
    var globalIdIndex = 0;

    var globalSelCurrentSelectedTextBoxId = null;
    var globalIsDocumentEventBinded = false;
    var containerCss = { 'background-color': 'white', 'border': '1px solid black', 'position': 'absolute', 'cursor': 'default', 'font-size': '11px', 'font-family': 'Arial, Helvetica, sans-serif' };
    var itemCss = { 'padding': '5px' };
    var mouseoverCss = { 'background-color': '#4169E1', 'color': 'white' };
    var mouseoutCss = { 'background-color': '', 'color': '' };
    var dropdownCss = { 'cursor': 'default' };
    var DATA_KEY = 'lendingqb_combobox_data';

    function hideCurrentCombobox()
    {
        if (globalSelCurrentSelectedTextBoxId == null)
            return;

        var data = $(globalSelCurrentSelectedTextBoxId).data(DATA_KEY);

        $(data.SelContainerId).hide();
        globalSelCurrentSelectedTextBoxId = null;

    }

    function navigateByKey(event)
    {

        if (globalSelCurrentSelectedTextBoxId != null)
        {
            var data = $(globalSelCurrentSelectedTextBoxId).data(DATA_KEY);
            var currentIndex = data.CurrentIndex;

            if (event.which == 38) // Up Arrow
            {
                __selectItem(currentIndex - 1);
            }
            else if (event.which == 40) // Down Arrow
            {
                __selectItem(currentIndex + 1);
            }
            else if (event.which == 13) // Enter
            {
                __chooseCurrentItem();
            }
            else if (event.which == 27) // ESC
            {
                hideCurrentCombobox();
            }
        }
    }
    function __selectItem(index)
    {
        if (globalSelCurrentSelectedTextBoxId == null)
            return;

        var data = $(globalSelCurrentSelectedTextBoxId).data(DATA_KEY);
        var previousIndex = data.CurrentIndex;

        if (previousIndex != null && previousIndex >= 0)
        {
            $(data.SelContainerId).children().eq(previousIndex).css(mouseoutCss);
        }
        var numOfItems = data.Items.length;
        if (index >= numOfItems)
        {
            index = 0;
        } else if (index < 0)
        {
            index = numOfItems - 1;
        }
        data.CurrentIndex = index;
        $(data.SelContainerId).children().eq(index).css(mouseoverCss);
    }
    function __chooseCurrentItem()
    {
        if (globalSelCurrentSelectedTextBoxId == null)
            return;

        var data = $(globalSelCurrentSelectedTextBoxId).data(DATA_KEY);
        var index = data.CurrentIndex;

        $(globalSelCurrentSelectedTextBoxId).val(data.Items[index]).trigger('change');
        hideCurrentCombobox();

    }
    function __createComboboxContainer(data)
    {
        // Create Container
        var containerId = 'lendingqb_comboboxcontainer_' + (globalIdIndex++);

        data.SelContainerId = '#' + containerId;

        var items = data.Items;
        var numOfItems = items.length;

        var divContainer = $('<div/>', { id: containerId }).css(containerCss);

        for (var index = 0; index < numOfItems; index++)
        {
            var itemData = { index: index };
            $('<div/>', { text: items[index] }).css(itemCss).data(DATA_KEY, itemData).appendTo(divContainer);

        }

        $(divContainer).appendTo('body');
        $(divContainer).on({
            mouseover: function()
            {
                __selectItem($(this).data(DATA_KEY).index);
            },
            click: function()
            {
                __chooseCurrentItem();
            }

        }, 'div');



    }

    $.fn.combobox = function(items)
    {
        return this.each(function()
        {
            // Lazy Instantiate.
            var selTextBoxId = '#' + $(this).prop('id');
            $(this).data(DATA_KEY, { Items: items, IsInit: false, CurrentIndex: 0 });

            var $combodropdown = $('<span>&#9660;</span>').css(dropdownCss).data(DATA_KEY, { IsCombobox: true });
            $(this).after($combodropdown);
            $($combodropdown).click(function(event)
            {
                hideCurrentCombobox();

                // Display Combobox
                var $textbox = $(selTextBoxId);

                var textbox_data = $textbox.data(DATA_KEY);
                if (!textbox_data.IsInit)
                {
                    __createComboboxContainer(textbox_data);
                    textbox_data.IsInit = true;
                }

                var p = $textbox.offset();
                var h = $textbox.outerHeight(true);
                var w = $textbox.outerWidth();
                $(textbox_data.SelContainerId).css({ 'top': p.top + h, 'left': p.left, 'width': w }).show();
                globalSelCurrentSelectedTextBoxId = selTextBoxId;
                __selectItem(0);



            });

            if (globalIsDocumentEventBinded == false)
            {
                // Initialize global onclick that for combobox.
                $(document).on({
                    click: function(event)
                    {
                        var eventTargetData = $(event.target).data(DATA_KEY);
                        if (eventTargetData != null && eventTargetData.IsCombobox)
                        {
                            return;
                        }

                        hideCurrentCombobox();
                    },
                    keydown: function(event) { navigateByKey(event); }
                });

                globalIsDocumentEventBinded = true;
            }

        });
    };
})(jQuery);
