﻿var resetVoa;
var resetVoe;
var addOrdersToTable;
jQuery(function ($) {
    var voaSection = $('#secVoa');
    var voeSection = $('#secVoe');

    $('.VOXTable').on('click', '.EDocLink', function () {
        var docId = $(this).data('docid');
        window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=" + docId, '_self');
    });

    $('.VOXTable').on('click', '.ErrorLink', function () {
        var error = $(this).data('error');
        simpleDialog.alert(error, "Error");
    });

    $('.VOXTable').on('click', '.PollLink', function () {
        RequestLinkClick(this, true, true);
    });

    $('.VOXTable').on('click', '.RefreshLink', function () {
        var row = this;
        simpleDialog.confirm("You are about to refresh this order. Proceed?").then(function (isConfirmed) {
            if (isConfirmed) {
                RequestLinkClick(row, false, false);
            }
        });
    });

    $('.VOXTable').on('click', '.NoConfirmRefreshLink', function () {
        RequestLinkClick(this, true, false);
    });

    $('body').on('click', '#Credentials #OkCredentialsBtn', function () {
        var popup = $(this).closest('#Credentials');
        var accountId = popup.find('#AccountId').val().trim();
        var userName = popup.find('#Username').val().trim();
        var password = popup.find('#Password').val().trim();
        if ((popup.find('#AccountIdRow').is(':visible') && popup.find('#AccountId').hasClass('RequiredInput') && accountId === '') ||
            (popup.find('#UsernameRow').is(':visible') && userName === '') ||
            (popup.find('#PasswordRow').is(':visible') && password === '')) {
            simpleDialog.alert("Please enter values for all required fields.", "Error");
        }
        else {
            LQBPopup.Return({ AccountId: accountId, Username: userName, Password: password });
        }
    });

    $('body').on('change', '#Credentials .RequiredInput', function () {
        $(this).siblings('.RequiredSpan').toggle(this.value.trim() === '');
    });

    function RequestLinkClick(link, usePollMsg, usePollMethod) {
        var requiresAccountId = $(link).siblings('.RequiresAccountId').val().toLowerCase() === 'true';
        var usesAccountId = $(link).siblings('.UsesAccountId').val().toLowerCase() === 'true';
        var serviceCredentialId = $(link).siblings('.ServiceCredentialId').val();
        var serviceCredentialHasAccountId = $(link).siblings('.ServiceCredentialHasAccountId').val().toLowerCase() === 'true';

        if (serviceCredentialId !== '-1' && (serviceCredentialHasAccountId || !usesAccountId)) {
            var data = {
                OrderId: $(link).siblings('.OrderId').val(),
                LoanId: ML.sLId
            };

            SubmitRequest(data, $(link).closest('.OrderRow'), usePollMsg, usePollMethod);
        }
        else {
            var popup = $('#CredentialsPopup');
            popup.find('#AccountIdRow').toggle(usesAccountId);
            popup.find('#AccountIdRequiredSpan').toggle(requiresAccountId);
            popup.find('#AccountId').toggleClass('RequiredInput', requiresAccountId);
            popup.find('#UsernameRow, #PasswordRow').toggle(serviceCredentialId === '-1');
            popup.find('#Username, #Password').toggleClass('RequiredInput', serviceCredentialId === '-1');
            popup.find('#UsernameRequiredSpan, #PasswordRequiredSpan').toggle(serviceCredentialId === '-1');

            LQBPopup.ShowElement(popup, {
                width: 350,
                height: 200,
                hideCloseButton: true,
                elementClasses: 'FullWidthHeight',
                onReturn: function (returnArgs) {
                    if (returnArgs) {
                        var data = {
                            AccountId: returnArgs.AccountId,
                            Username: returnArgs.Username,
                            Password: returnArgs.Password,
                            OrderId: $(link).siblings('.OrderId').val(),
                            LoanId: ML.sLId
                        };

                        SubmitRequest(data, $(link).closest('.OrderRow'), usePollMsg, usePollMethod);
                    }
                }
            });
        }
    }

    function SubmitRequest(data, associatedRow, usePollMsg, usePollMethod) {
        var serviceObject;
        var rowCreator;
        var header;
        var serviceType = $(associatedRow).find('.ServiceType').val();

        // Add new order types here.
        if (serviceType === "0") { // VOA
            serviceObject = gService.vod;
            rowCreator = VOAOrderRow.CreateVoaRow;
            header = "VOA";
        }
        else if (serviceType === '1') { // VOE
            serviceObject = gService.voe;
            rowCreator = VOEOrderRow.CreateVOERow;
            header = "VOE";
        }
        else {
            simpleDialog.alert("Invalid service type.", header);
            return;
        }

        var popupMessage = usePollMsg ? "Getting " + header + " Results..." : "Refreshing " + header + " Order...";
        var methodName = usePollMethod ? "PollOrder" : "RefreshOrder";

        var loading = $('<loading-icon id="LQBPopupUWaitTable">' + popupMessage + '</loading-icon>');
        TPOStyleUnification.UnifyLoadingIcons(loading);

        LQBPopup.ShowElement(loading, {
            width: 350,
            height: 200,
            hideCloseButton: true
        });

        window.setTimeout(function () {
            serviceObject.callAsyncSimple(methodName, data,
                function (results) {
                    HandleRequestResults(results, associatedRow, usePollMethod, rowCreator, header, serviceObject);
                }, ML.VOXTimeoutInMilliseconds);
        });
    }

    function HandleRequestResults(results, associatedRow, usePollMethod, rowCreatorFunction, header, serviceObject) {
        if (results.error) {
            LQBPopup.Return();
            simpleDialog.alert(results.UserMessage, header).then(function () { LQBPopup.Return(null) });
        }
        else if (results.value["Success"].toLowerCase() !== 'true') {
            LQBPopup.Return();
            var requestErrors = JSON.parse(results.value["Errors"]);
            simpleDialog.multilineAlert(requestErrors, header).then(function () { LQBPopup.Return(null) });
        }
        else if (results.value.PublicJobId) {
            var publicJobId = results.value.PublicJobId;
            var pollingIntervalInMilliseconds = parseInt(results.value.PollingIntervalInSeconds) * 1000;
            var attemptNumber = 1;
            window.setTimeout(function () {
                PollForSubsequentResult(publicJobId, pollingIntervalInMilliseconds, attemptNumber, results.value, serviceObject, associatedRow, usePollMethod, rowCreatorFunction, header);
            }, pollingIntervalInMilliseconds);
        }
        else {
            LQBPopup.Return();
            HandleSuccessResult(results, associatedRow, usePollMethod, rowCreatorFunction, header);
        }
    }

    function PollForSubsequentResult(publicJobId, pollingIntervalInMilliseconds, attemptNumber, previousResult, serviceObject, associatedRow, usePollMethod, rowCreatorFunction, header) {
        var args = {};
        args.LoanId = ML.sLId;
        args.PublicJobId = publicJobId;
        args.AttemptNumber = attemptNumber;
        for (var key in previousResult) {
            if (previousResult.hasOwnProperty(key)) {
                args[key] = previousResult[key];
            }
        }

        serviceObject.callAsyncBypassOverlay('PollForSubsequentResults', args,
            function (result) {
                if (result.error) {
                    LQBPopup.Return(null);
                    simpleDialog.alert(result.UserMessage, header).then(function () { LQBPopup.Return(null) });
                }
                else if (result.value.Success.toLowerCase() !== 'true') {
                    LQBPopup.Return(null);

                    var requestErrors = JSON.parse(result.value["Errors"]);
                    simpleDialog.multilineAlert(requestErrors, header).then(function () { LQBPopup.Return(null) });
                }
                else if (result.value.PublicJobId) {
                    publicJobId = result.value.PublicJobId;
                    window.setTimeout(function () {
                        PollForSubsequentResult(publicJobId, pollingIntervalInMilliseconds, ++attemptNumber, previousResult, serviceObject, associatedRow, usePollMethod, rowCreatorFunction, header);
                    }, pollingIntervalInMilliseconds);
                }
                else {
                    LQBPopup.Return(null);
                    HandleSuccessResult(result, associatedRow, usePollMethod, rowCreatorFunction, header);
                }
            });
    }

    function HandleSuccessResult(results, associatedRow, usePollMethod, rowCreatorFunction, header) {
        var updatedOrder = JSON.parse(results.value["UpdatedOrder"]);
        var newRow = rowCreatorFunction(updatedOrder);

        if (updatedOrder.Status !== 0 && updatedOrder.Status !== 1 && updatedOrder.Status !== 5) {
            simpleDialog.alert(updatedOrder.Error, header)
                .then(function () {
                    // VOE/I behavior only
                    if (updatedOrder.ServiceType == 1) {
                        $(associatedRow).closest('tbody').append(newRow);
                    }
                    else {
                        $(associatedRow).replaceWith(newRow);
                    }

                    LQBPopup.Return(null);
                });
        }
        else if (updatedOrder.Status == 0) {
            simpleDialog.multilineAlert(["Service order is pending.", updatedOrder.StatusDescription || "The vendor has received your order."], header)
                .then(function () {
                    if (!usePollMethod) {
                        $(associatedRow).closest('tbody').append(newRow);
                    }
                    else {
                        $(associatedRow).replaceWith(newRow);
                    }

                    if (results.value['Key']) {
                        window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + results.value['Key'], '_self')
                    }

                    LQBPopup.Return(null);
                });
        }
        else {
            simpleDialog.alert("Order results have been received.", header).
                then(function () {
                    if (!usePollMethod && updatedOrder.ServiceType == 1) {
                        // Special behavior for VOE/I.
                        $(associatedRow).closest('tbody').append(newRow);
                    }
                    else {
                        $(associatedRow).replaceWith(newRow);
                    }

                    if (results.value['HasReturnedAssets'].toLowerCase() === 'true') {
                        var pdfKey = results.value['Key'];
                        IframePopup.show(ML.VirtualRoot + '/webapp/ImportUpdateAssets.aspx?loanid=' + ML.sLId + '&order=' + results.value['OrderId'], {}).then(function (pair) {
                            var error = pair[0];
                            var result = pair[1];

                            var messages = [];
                            if (result.NoAccess) {
                                messages.push("Cannot import assets.");
                            }
                            else {
                                messages.push(result.Success ? "Verified Assets Were Imported" : "No Assets Were Imported");
                                messages.push(result.Success ? (result.Added + " assets were added, " + result.Modified + " assets were modified, and " + result.Removed + " assets were removed.") : "");
                            }

                            simpleDialog.multilineAlert(messages, header).then(function () {
                                if (pdfKey) {
                                    window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + pdfKey, '_self')
                                }
                            });
                        });
                    }
                    else {
                        if (results.value['Key']) {
                            window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + results.value['Key'], '_self')
                        }
                    }

                    LQBPopup.Return(null);
                });
        }
    }

    function OpenOrderingDiv(voxSection) {
        voxSection.find('.VOXOrderBtn').hide();
        voxSection.find('.VOXOrderingDiv').show();
    }

    function AddOrdersToTable(orders, stitchedCacheKey) {
        $.each(orders, function (index, order) {
            var table;
            var rowCreationMethod;
            if (order.ServiceType == 0) { // VOXServiceT.VOA_VOD
                table = voaSection.find('.OrdersTable');
                rowCreationMethod = VOAOrderRow.CreateVoaRow;
                ResetVOA();
            }
            else if (order.ServiceType == 1) { // VOXServiceT.VOE
                table = voeSection.find('.OrdersTable');
                rowCreationMethod = VOEOrderRow.CreateVOERow;
                ResetVOE();
            }

            var row = rowCreationMethod(order);
            table.find('tbody').append(row);
        });

        if (stitchedCacheKey) {
            window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + stitchedCacheKey, '_self')
        }
    }

    // ===== VOA ====
    voaSection.on('click', '.VOXOrderBtn', function () {
        OpenOrderingDiv(voaSection);
        VOAOrdering.OnTabOpen();
    });

    voaSection.on('click', '.VOXCancelBtn', function () {
        ResetVOA();
    });

    function ResetVOA() {
        if (ML.VoaTabEnabled) {
            voaSection.find('.VOXOrderBtn').show();
            voaSection.find('.VOXOrderingDiv').hide();
            VOAOrdering.ClearSection();
        }
    }

    function LoadVOATable() {
        if (ML.VoaTabEnabled) {
            $.each(VOAOrders, function (index, value) {
                var row = VOAOrderRow.CreateVoaRow(value);
                voaSection.find('.OrdersTable tbody').append(row);
            });
        }
    }

    // ==== VOE ====
    voeSection.on('click', '.VOXOrderBtn', function () {
        OpenOrderingDiv(voeSection);
        VOEOrdering.OnTabOpen();
    });

    voeSection.on('click', '.VOXCancelBtn', function () {
        ResetVOE();
    });

    function ResetVOE() {
        if (ML.VoeTabEnabled) {
            voeSection.find('.VOXOrderBtn').show();
            voeSection.find('.VOXOrderingDiv').hide();
            VOEOrdering.ClearSection();
        }
    }

    function LoadVOETable() {
        if (ML.VoeTabEnabled) {
            $.each(VOEOrders, function (index, value) {
                var row = VOEOrderRow.CreateVOERow(value);
                voeSection.find('.OrdersTable tbody').append(row);
            });
        }
    }

    // ==== ON LOAD ====
    resetVoa = ResetVOA;
    resetVoe = ResetVOE;
    VOAOrderRow.PostTemplateLoadCallback(LoadVOATable);
    VOEOrderRow.PostTemplateLoadCallback(LoadVOETable);

    addOrdersToTable = AddOrdersToTable;
});


var VOAOrderRow = (function () {
    var voaRowTemplate;
    var voaPostTemplateLoadCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/webapp/Partials/VOX/VOAOrderRow.html", function (data) {
            voaRowTemplate = $.template(data);
            if (typeof (voaPostTemplateLoadCallback) == 'function') {
                voaPostTemplateLoadCallback();
            }
        });
    });

    function _createVoaRow(data) {
        return $.tmpl(voaRowTemplate, data);
    }

    return {
        CreateVoaRow: _createVoaRow,
        PostTemplateLoadCallback: function (callback) { voaPostTemplateLoadCallback = callback; }
    };
})();

var VOEOrderRow = (function () {
    var voeRowTemplate;
    var voePostTemplateLoadCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/webapp/Partials/VOX/VOEOrderRow.html", function (data) {
            voeRowTemplate = $.template(data);
            if (typeof (voePostTemplateLoadCallback) == 'function') {
                voePostTemplateLoadCallback();
            }
        });
    });

    function _createVoeRow(data) {
        return $.tmpl(voeRowTemplate, data);
    }

    return {
        CreateVOERow: _createVoeRow,
        PostTemplateLoadCallback: function (callback) { voePostTemplateLoadCallback = callback; }
    };
})();

