var gCurrencyNegativeColor = 'red';
var gCurrencyColor = 'black';



function _initMask(o) {
    if (o.hasClass('mask-initialized')) {
        return;
    }

    o.addClass('mask-initialized');

    switch (o.attr('preset')) {
        case "zipcode" : 
            o.attr({'mask': '#####', 'maxLength':  5, 'width': 50} ).keyup(mask_keyup);          
            break;
        case "ssn" : 
            // #=digit, ?=alphanumeric, *=digit or '*'
            o.attr( {'maxLength' : 11, 'mask' : '***-**-####'}).css('width', 90).keyup(mask_keyup);
            break;
        case "ssn-last-four":
            // #=digit, ?=alphanumeric, *=digit or '*'
            o.attr({ 'maxLength': 4, 'mask': '####' }).keyup(mask_keyup);
            break;
        case "phone" :
            o.attr({'mask': '(###) ###-####???????', 'maxLength' : 21}).css('width', '120').keyup(mask_keyup);
            break;
        case "date" :
            o.attr({'maxLength' : 10, 'width' :  75}).keyup(date_keyup).blur(date_onblur);
            if (o.val() != '') o.trigger('blur');
            break;
        case "percent" :
            o.css({ 'text-align' : 'right', 'width': 70 }).change(percent_onblur).blur(percent_onblur);
            break;
        case "percent-allowblank":
            o.css({ 'text-align': 'right', 'width': 70 }).change(percent_allowblank_onblur).blur(percent_allowblank_onblur);
            break;
        case "percent-nocss" :
            o.change(percent_onblur).blur(percent_onblur);
            break;
        case "money" :
            o.css({'width' : 90, 'text-align' : "right" }).change(money_onblur).blur(money_onblur);
            break;
        case "money-allowblank" :
            o.css({ 'width': 90, 'text-align': "right" }).change(money_allowblank_onblur).blur(money_allowblank_onblur);
            break;
        case "money-allowblank-nocss" :
            o.change(money_allowblank_onblur).blur(money_allowblank_onblur);
            break;
        case "money-nocss" :
            o.change(money_onblur).blur(money_onblur);
            break;
        case "numeric":
            o.css({ 'width': 90, 'text-align': "right" }).change(numeric_onblur).blur(numeric_onblur);
            break;
        case "numeric-nocss":
            o.css({'text-align': "right" }).change(numeric_onblur).blur(numeric_onblur);
            break;
        case "employerIdentificationNumber" : 
            o.attr( {'maxLength' : 10, 'mask' : '##-#######'}).css('width', 90).keyup(mask_keyup);
            break;
    }
}

function date_keyup(e) {
    var srcElement = this;
    if (e.which == 84) { // enter 'T'
        if (!jQuery(e.target).prop('readOnly')) {
            srcElement.value = dtToString(new Date());
            // 2/11/2004 dd - set value of textbox through script does not trigger onchange event. Therefore explicit action requires.
            jQuery(srcElement).change(); 
            if (typeof(updateDirtyBit) == 'function') updateDirtyBit();
        }
    }
}

function dtToString(dt) {
    return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
}

function mask_keyup(e){
    // Validate the event throwing object.  We choose th global
    // arguments set if the event source element is null.

    var o = this; key = e.which;

    if( key == 9 || key == 16 || key == 8 || key == 37 || key == 39 ){
        // Skip tab, delete, and cursor keys.
        return;
    }

    if (o.value == ''){
        return;
    }
    
    // Get the current cursor position.  This is definitely
    // jscript voodoo, but it seems to work when the text
    // box has focus.

    var pos = 0; 
    
    if (document.selection){
        var selection = document.selection.createRange(); 
        selection.moveStart('character', -o.value.length);
        pos = selection.text.length;
    }else{
        pos = o.selectionStart;
    }
    
    // Walk the string and apply the mask.

    var maskIndex = 0;
    var index     = 0;
    var ret       = "";
    var value     = o.value;
    var mask      = jQuery(o).attr('mask');

    while(true){
        m = mask.charAt(maskIndex);
        c = value.charAt(index);

        if(m == '#'){
            if(isDigit(c)){ 
                ret += c;

                if(++maskIndex > mask.length)
                    break;
            }

            if(++index > value.length)
                break;
        }
        else if (m == '*'){
            if (isDigit(c) || c == '*'){
                ret += c;

                if (++maskIndex > mask.length)
                    break;
            }

            if (++index > value.length)
                break;
        }
        else if(m == '?'){
            if(isAlphaNumeric(c)){
                ret += c;

                if(++maskIndex > mask.length)
                    break;
            }

            if(++index > value.length)
                break;
        }
        else{
            ret += m;

            if(maskIndex <= pos && c != m){
                ++pos;
            }

            if(c == m){
                ++index;
            }

            if(++maskIndex > mask.length)
                break;
        }
    }

    o.value    = ret;
    if(typeof(updateDirtyBit) == 'function'){
        updateDirtyBit();
    }

    // Update the text box with the final cursor position.
    // We use the previously discovered position, which may
    // have been updated during masking, and move the cursor
    // into that.  The final select will trigger the caret
    // to appear at the start of the moved text range, which
    // has no width.

   // setCaretPosition(o, pos);
}

function isDigit(ch) { 
  return ch.match(/[0-9]/) != null;
}
function isAlphaNumeric(ch) {
  return ch.match(/[a-z0-9]/i) != null;
}

function numeric_onblur() {
    var e = this;
    if (e.value === '') {
        return;
    }
    format_numeric(e);
}

// Format a generic number that is allowed to have decimals.
function format_numeric(e) {
    var index = 0;
    var ret = '';
    var isNegative = false;
    var isLeadingZero = true;
    var hasDecimal = false;
    var str = e.value.replace(/[^0-9-\(.]/g, '');
    var decimalDigits = 0;

    for (index = 0; index < str.length; index++) {
        ch = str.charAt(index);
        if (ch == '-' && index == 0 && !isNegative) {
            isNegative = true;
            continue;
        }
        if (isLeadingZero && ch == '0') continue;
        if (isDigit(ch)) {
            ret += ch;
            isLeadingZero = false;
            if (hasDecimal) decimalDigits++;
        } else if (ch == '.' && !hasDecimal) {
            if (isLeadingZero) ret = '0.';
            else ret += '.';
            hasDecimal = true;
            isLeadingZero = false;
        }
    }

    if (isLeadingZero || ret === '0.') {
        e.value = '0';
        return;
    }

    // Place thousand separator.
    var p = ret.length - (hasDecimal ? (decimalDigits + 1) : 0);
    while (p > 3) {
        ret = ret.substr(0, p - 3) + ',' + ret.substr(p - 3);
        p -= 3;
    }

    if (isNegative) {
        e.value = '-' + ret;
    } else {
        e.value = ret;
    }
}

function money_onblur() {
  var e = this;
  format_money(e);
  if (typeof(format_money_callback) === 'function') {
      format_money_callback(e);
  }
}

function money_allowblank_onblur() {
  var e = this;
  if (e.value === '') {
      return;
  }
  format_money(e);
}

function format_money(e) {
    // If initial inputbox is empty then DO NOT format this input. dd 9/8/03
    // 10/20/2009 dd - Add support for 4 decimal digits
    if (e.value == '') return;
    var index = 0;
    var ret = '';
    var isNegative = false;
    var isLeadingZero = true;
    var hasDecimal = false;
    var precision = 0;
    var str = e.value.replace(/[^0-9-\(.]/g, '');
    var decimalDigits = e.getAttribute("decimalDigits") == null ? 2 : e.getAttribute("decimalDigits");
    var precisionLimit = decimalDigits >= 2 ? decimalDigits - 1 : 1;

    for (index = 0; index < str.length; index++) {
        ch = str.charAt(index);
        if ((ch == '-' || ch == '(') && index == 0 && !isNegative) {
            isNegative = true;
            continue;
        }
        if (isLeadingZero && ch == '0') continue;
        if (isDigit(ch)) {
            ret += ch;
            isLeadingZero = false;
            if (hasDecimal) precision++;
            if (precision > precisionLimit) break;
        } else if (ch == '.' && decimalDigits == 0) {
            break;  // truncate
        } else if (ch == '.' && !hasDecimal) {
            if (isLeadingZero) ret = '0.';
            else ret += '.';
            hasDecimal = true;
            isLeadingZero = false;
        }
    }

    if (!hasDecimal) {
        switch (decimalDigits) {
            case 0:
                if (isLeadingZero)
                    ret += '0';
                break;
            case 4:
                if (isLeadingZero)
                    ret = '0.0000';
                else
                    ret += '.0000';
            default:
                if (isLeadingZero)
                    ret = '0.00';
                else
                    ret += '.00';
        }
    }
    else if (decimalDigits == 0) {
        // shouldn't be able to get here.
        // DO NOTHING
    }
    else
    {
        var leftOverDecimalPlaces = decimalDigits - precision;
        for(leftOverDecimalPlaces; leftOverDecimalPlaces > 0; --leftOverDecimalPlaces)
        {
            ret += '0';
        }
    }

    // Place thousand separator.
    var decimalCharacters = decimalDigits <= 0 ? 0 : parseInt(decimalDigits) + 1;

    var p = ret.length - decimalCharacters;
    while (p > 3) {
        ret = ret.substr(0, p - 3) + ',' + ret.substr(p - 3);
        p -= 3;
    }

    // 9/5/03 - For performance reason, I don't reformat when value pass back from server.
    // Therefore color code on money field will not work consistencely. 
    if (isNegative) {
        e.value = '($' + ret + ')';
        //  e.style.color = gCurrencyNegativeColor;
    } else {
        e.value = '$' + ret;
        //  e.style.color = gCurrencyColor;
    }
}

function percent_onblur() {
    format_percent(this);
    if(typeof(format_percent_callback) === 'function'){
        format_percent_callback.call(this);
    }
}

function percent_allowblank_onblur() {
    if (this.value === '') {
        return;
    }
    format_percent(this);
}

function format_percent(thisObj) {
  // Remove leading 0.
  // Append 0 if first character is .
  var index = 0, ret = '', isLeadingZero = true, hasDecimal = false, precision = 0, isNegative = false, value = thisObj.value;
  var decimalDigits = thisObj.getAttribute("decimalDigits") == null ? 3 : thisObj.getAttribute("decimalDigits");
  var precisionLimit;
  if (decimalDigits == 6) {
      precisionLimit = 5;
  } else if (decimalDigits == 4) {
      precisionLimit = 3;
  } else {
      precisionLimit = 2;
  }

  for (index = 0; index < value.length; index++){
    ch = value.charAt(index);
    if ((ch == '-' || ch == '(') && index == 0 && !isNegative){
      isNegative = true;
      continue;
    }    
    if (isLeadingZero && ch == '0') continue;
    if (isDigit(ch)) {
      ret += ch;
      isLeadingZero = false;
      if (hasDecimal) precision++;
      if (precision > precisionLimit) break;
    } else if (ch == '.' && !hasDecimal) {
      if (isLeadingZero) ret = '0.';
      else ret += '.';
      hasDecimal = true;
      isLeadingZero = false;
    }
  }

  if (!hasDecimal) {
      if (decimalDigits == 6) {
          if (isLeadingZero) ret = '0.000000';
          else ret += '.000000';
      } else if (decimalDigits == 4) {
          if (isLeadingZero) ret = '0.0000';
          else ret += '.0000';
      } else {
          if (isLeadingZero) ret = '0.000';
          else ret += '.000';
      }
  } else if (precision == 0) // pad with zeroes
  {
      ret += decimalDigits == 6 ? '000000' : '000';
  } else if (precision == 1) {
      ret += decimalDigits == 6 ? '00000' : '00';
  } else if (precision == 2) {
      ret += decimalDigits == 6 ? '0000' : '0';
  } else if (precision == 3) {
      ret += decimalDigits == 6 ? '000' : '';
  }
  else if (precision == 4) {
      ret += decimalDigits == 6 ? '00' : '';
  }
  else if (precision == 5) {
      ret += decimalDigits == 6 ? '0' : '';
  }

  if (isNegative)
    thisObj.value = '-' + ret + '%';
  else
    thisObj.value = ret + '%';
}

// Enter 't' will fill in today date
// Max length = 10.
// Enter single digit will use current month and year.
// Enter mm/dd will use current year.
// Enter mm/dd/yy if year is between 30 and 99 then yyyy = 19yy else 20yy
// Support format mmddyyyy, mm-dd-yyyy, mm/dd/yyyy
function date_onblur(e) {

  if (this.value == '') return;
  var value = this.value.replace(/[^0-9\/-]/g, '').replace(/-/g,'/'), dt = new Date(), mm, dd, yyyy; 
  
  if (value != '' && !isNaN(value) && parseInt(value, 10) <= 31){
    dt.setDate(parseInt(value, 10));
  } else {
    var str = value;
   
    var parts = str.split('/');
    if (parts.length == 1){
        // must be either mmddyy or mmddyyyy
        if (str.length == 6 || str.length == 8){
            yyyy = getYear(parseInt(str.substr(4)));
            mm = parseInt(str.substr(0, 2), 10);
            dd = parseInt(str.substr(2, 2), 10);
            if (mm < 1 || mm > 12){
                errorMsg('Invalid month. Month need to be from 1 to 12.', this);
                    return;
            }
            if (dd < 1 || dd > 31){
                errorMsg('Invalid date. Date need to be from 1 to 31.', this);
                return;
            } 
            dt.setFullYear(yyyy, mm - 1, dd);
        } 
        else{
            errorMsg('Invalid date format.\n\nYou can enter dates in the following formats: mm/dd/yy, mm/dd/yyyy, mm/dd, mmddyy, mmddyyyy.', this);
            return;
        }
    
    }
    else if (parts.length == 2 || parts.length == 3){
        for (var i = 0; i < parts.length; i++){
            if (parts[i] == '' || isNaN(parts[i])){
                errorMsg('Invalid date format.\n\nYou can enter dates in the following formats: mm/dd/yy, mm/dd/yyyy, mm/dd, mmddyy, mmddyyyy.', this);
                return;
            }
        }
        mm = parseInt(parts[0], 10);
        dd = parseInt(parts[1], 10);
        yyyy = parts.length == 3 ? getYear(parseInt(parts[2])) : dt.getFullYear();
        
        if (mm < 1 || mm > 12){
          errorMsg('Invalid month. Month need to be from 1 to 12.', this);
          return;
        }
        if (dd < 1 || dd > 31) {
          errorMsg('Invalid date. Date need to be from 1 to 31.', this);
          return;
        } 
        if (!isValidDate(yyyy, mm, dd)){      
          return;
        }
        dt.setFullYear(yyyy, mm - 1, dd);
        
    } 
    else{
      errorMsg('Invalid date format.\n\nYou can enter dates in the following formats: mm/dd/yy, mm/dd/yyyy, mm/dd, mmddyy, mmddyyyy.', this);
      return;
    }
  }

  this.value = dtToString(dt);

  if (isDirty()) {
      if (typeof (doAfterDateFormat) == 'function') {
          doAfterDateFormat(e);
      }
  }
}


function isValidDate(yyyy, mm, dd) {
  var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && dd > 30) {
    errorMsg(months[mm - 1] + ' has maximum of 30 days.');
    return false;
  } else if (mm == 2 && ((yyyy % 4) > 0) && dd > 28) {
    errorMsg('February of ' + yyyy + ' has maximum of 28 days.');
    return false;
  } else if (mm == 2 && dd > 29) {
    errorMsg('February of ' + yyyy + ' has maximum of 29 days.');
    return false;
  }
  return true;
}

function getYear(y) {
  if (y < 30)
    return y += 2000;
  else if (y < 100)
    return y += 1900;
  else
    return y;
}
function errorMsg(msg, o) {
  alert(msg);
}

// --- Functions for time control.
function time_onminuteblur(o) {
  var a = parseInt(o.value, 10);
  if (a < 10) o.value = "0" + a;
  else if (a < 60) o.value = a;
  else o.value = "00";
}

function time_onhourkeyup(o, m) {
  // skip tab & delete
  if (event.keyCode == 9 || event.keyCode == 16 || event.keyCode == 8 || 
event.keyCode == 39 || event.keyCode == 37) return;

  var v = parseInt(o.value, 10);
  var prefix = o.value.length == 2 ? o.value.charAt(0) : "0";
  //alert(o.value + " | " + parseInt(o.value));

  if (isNaN(v)) o.value = "";
  else {
    if (v > 1 && v < 10) {
      o.value = prefix + v;
      document.getElementById(m).select();
      document.getElementById(m).focus();
    } else if (v > 12) {
      o.value = prefix;
    } else if (v > 9 && v < 13) {
      document.getElementById(m).select();
      document.getElementById(m).focus();
    }
  }
}

function time_onhourblur(o) {
  var a = parseInt(o.value, 10);
  if (a < 10) o.value = "0" + a;
  else if (a < 13) o.value = a;
  else o.value = "12";
}

