var LQBPopup = (function($) {
    var _popupStack = [], $iframe, $element,$elementDiv,$controlDiv, $closeButton, $waitTable, returnCallback = null, cancelCallback = null, $args = {},  popupClasses;
    /*popupItem{
        .popup,
        .setting, //temporari don't need

        .iframe,
        .element,
        .elementDiv,
        .controlDiv,
        .closeButton
        .waittable,

        .popupClasses, // check with setting

        .returnCallback,
        .cancenCallbak,
        .args,
    }*/

    lqbPopupV2DefaultSetting = {
        'width': null,//450,
        'height': null,//350,
        'closeText': 'Close',
        'onClose': null,
        'onReturn': null,
        'hideXButton': false,
        'hideCloseButton': false,
        'modifyOverflow': false,//true,
        'popupClasses': null,
        'headerText': '',
        'buttons':null,
        'backdrop':'static',
        'draggable':false,
        'resizable':false,
        'minHeight':100,//use for resizable = true
        'minWidth':100,//use for resizable = true
        'onShown': null
    };
// no more popup v2
    function _getPopupConfig(){
        return _popupStack.length ? _popupStack[_popupStack.length - 1] : null;
    }

    function _pushPopup(_popup){
        _popupStack.push(_popup);
        return _getPopupConfig();
    }

    function _popPopup(){
        return _popupStack.pop();
    }

    var getPopupHtml =    function(componentHtml, key){
                                return '<div class="modal fade" id="' + key +'" role="dialog">' +
                                            '<div class="modal-dialog">'+
                                                '<div class="modal-content">'+
                                                    componentHtml +
                                                '</div>'+
                                            '</div>'+
                                        '</div>';
                            }

    const getElementHtml = function(componentHtml)
                            {
                                return '<div id="LQBPopupUElementDiv">' + componentHtml + '</div>';
                            }
    
    
    const popUpFooterHtml       = '<div class="modal-footer">'+
                                      '<button id="LQBPopupUCloseBtn" type="button" class="btn btn-flat" data-dismiss="modal">Cancel</button>'+
                                  '</div>';

    const popupHeaderHtml      = '<div class="modal-header">'+
                                      '<button type="button" class="close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></button>'+
                                      '<h4 class="modal-title"></h4>'+
                                  '</div>';

    const popupBodyHtml        =  '<div class="modal-body">'+
                                      '<div id="LQBPopupUElement">' + '</div>' +
                                  '</div>';
    const popupLoadingHtml     =  '<loading-icon id="LQBPopupUWaitTable" style="display: none">Please Wait ...</loading-icon>';
    const popupIframeDivHtml   =  '<iframe id="LQBPopupUDivFrame" src="about:blank"></iframe>';
    const popupControlDivHtml  =  '<div id="LQBPopupUControlDiv"></div>';

    function getPopupTemplate (key){
        return getPopupHtml(
                    popupLoadingHtml +
                    popupIframeDivHtml +
                    popupControlDivHtml +
                    getElementHtml( popupHeaderHtml + popupBodyHtml + popUpFooterHtml)
                    , key);
    }

    function getPopupWrapperTemplate(key) { return  getPopupHtml( getElementHtml( popupBodyHtml), key); };                                        

    const wrapAttribute = "UsingWrap";

    function _loadPopup(keyInput){
        var key = keyInput;
        if (!$.trim(key).length){
            key = "LQBPopupUDiv" + _popupStack.length;
            $('body').first().append(getPopupTemplate(key));
        }
       
        var popupConfig = _createPopupConfigFromKey(key);
        
        popupConfig.popup.on('hidden.bs.modal', function () {
            _cleanUpTopPopup();
        });

        return _pushPopup(popupConfig);
    }

    function _createPopupConfigFromKey(key){
        var popupConfig = {};
        popupConfig.popup = $("#" + key);
        popupConfig.iframe = popupConfig.popup.find('#LQBPopupUDivFrame').hide();
        popupConfig.element = popupConfig.popup.find('#LQBPopupUElement');
        popupConfig.elementDiv = popupConfig.popup.find('#LQBPopupUElementDiv').hide();
        popupConfig.controlDiv = popupConfig.popup.find('#LQBPopupUControlDiv').hide();
        popupConfig.waitTable =  popupConfig.popup.find('#LQBPopupUWaitTable').hide();

        popupConfig.closeButton = popupConfig.popup.find('#LQBPopupUCloseBtn').hide();
        return popupConfig;
    }

    function _configModal(_popupConfig, _settings){
        _loadCloseButton(_popupConfig, _settings);
        _loadBottomButtons(_popupConfig, _settings);

        _popupConfig.popup.find('.modal-title').text(_settings.headerText);
        _setDragAndResize(_popupConfig.popup,_settings);

        TPOStyleUnification.UnifyLoadingIcons($waitTable);
        _popupConfig.popupClasses = _settings.popupClasses;
        _popupConfig.popup.find('.modal-dialog').addClass(_popupConfig.popupClasses);
        
        if (_settings && typeof (_settings.onShown) === 'function') {
            _settings.onShown(_popupConfig.popup);
        }
        
        if (_settings.width != null) _popupConfig.popup.find('.modal-dialog').width(adjustWidth(_settings.width,_settings.minWidth));
        if (_settings.height != null) _popupConfig.popup.find('.modal-body').height(adjustHeight(_settings.height,_settings.minHeight));

        _popupConfig.height = _settings.height;
        _popupConfig.minHeight = _settings.minHeight;

        _popupConfig.popup.find('.modal-header button').toggle(!_settings.hideXButton);
        _popupConfig.returnCallback = _settings.onReturn;
        _popupConfig.args = _settings.args;
        _popupConfig.closeButton.click(function() {
            if (_settings && typeof (_settings.onClose) === 'function') {
                _settings.onClose();
            }

            _hide();
        });
    }

    function _showPopupModal(_popup, _settings){
        _popup.modal({
            backdrop: _settings.backdrop,
            keyboard: false,
        });
    }

    function _hidePopupModal(_popup){
        _popup.find('.modal-dialog').width('');
        _popup.find('.modal-body').height('');
    }

    function _isPopupShown(_popup) {return _popup && _popup.hasClass('in');}

    function _loadBottomButtons(_popupConfig, _settings){
        var buttonPadding = _settings.hideCloseButton ? 0 : 25;
        if(_settings.buttons){
             $.each(_settings.buttons, function(index, button) {
                var $buttonName=Object.keys(button)[0];
                if (_popupConfig.popup.find("button:contains('"+$buttonName+"')").length == 0) {
                    var btn = $('<button type="button" name="' + $buttonName + '" class="btn btn-flat">'+$buttonName+'</button>');
                    btn.click(function(){
                        button[$buttonName]();
                        _hide();
                    });
                    _popupConfig.popup.find('.modal-footer').append(btn);
                }
            });
        }
    }
    
    function _loadCloseButton(_popupConfig, _settings, _closeButton){
        var _closeButton = _popupConfig.closeButton;
        if (!_closeButton.length){
            return;
        }

        if (_settings.hideCloseButton) {
            _closeButton.detach();
            _closeButton.hide();
        }
        else {
            if (!jQuery.contains(document.documentElement, _closeButton[0])) {
                _popupConfig.popup.find('.modal-footer').append(_closeButton);
            }
           _closeButton.show();
        }
        _closeButton.text(_settings.closeText);
    }

    function _updateLayout(_popup, _settings){
        if(_isPopupShown(_popup)){ 
            _hidePopupModal(_popup);
        }

        _setDragAndResize(_popup, _settings);
    }

    function _setDragAndResize(_popup, _settings){
        if(_settings.draggable) _popup.find('.modal-dialog').draggable();

        if(_settings.resizable){
            _popup.find('.modal-content').resizable({
                minHeight: _settings.minHeight,
                minWidth: _settings.minWidth
            });
            _popup.on('show.bs.modal', function () {
                $(this).find('.modal-body').css({
                    'max-height':'100%'
                });
            });    
        }
    }

    function adjustHeight(height, minHeight){
        height = Math.max(height, minHeight);
        return Math.min($(window).height() - 120, height);
    }

    function adjustWidth(width, minWidth){
        width = Math.max(width, minWidth);
        return Math.min($(window).width(), width);
    }

    function generatePopupClass(url){
        return "modal-" + url.substring(url.lastIndexOf('/')+1).split('.')[0];
    }

    function _jquerifyElement(_el){
        if(typeof _el === "string")
        {
            return /<[a-z][\s\S]*>/i.test(_el) ? $(_el) : $('<div>'+_el+'</div>');
        }

        return _el;
    }

    function _showPopupWithIframe(url, settings) {
        var popupConfig = _loadPopup();
        settings = $.extend({}, lqbPopupV2DefaultSetting, settings);
        _configModal(popupConfig, settings);

        popupConfig.waitTable.parent().width((settings.width != null) ? adjustWidth(settings.width,settings.minWidth) : settings.minWidth);
        popupConfig.waitTable.parent().height((settings.height != null) ? adjustHeight(settings.height,settings.minHeight) : settings.minHeight);
        popupConfig.waitTable.show();    

        popupClasses = settings.popupClasses || generatePopupClass(url);
        popupConfig.popup.find('.modal-dialog').addClass(popupClasses);

        popupConfig.iframe.get(0).contentWindow.location.replace(url);
        popupConfig.iframe.on('load',function(){
            setTimeout(function(){
                popupConfig.waitTable.hide();
                popupConfig.waitTable.parent().width('');
                popupConfig.waitTable.parent().height('');
                popupConfig.iframe.show();
                popupConfig.iframe.get(0).style.width='100%';    

                popupConfig.iframe.height(adjustHeight(((settings.height != null) ? settings.height: popupConfig.iframe.contents().height()),settings.minHeight));
            },500);
        })

        _showPopupModal(popupConfig.popup, settings);
    }

    function showWaitTable(isShow) {
        var popupConfig = _getPopupConfig();
        popupConfig.waitTable.toggle(isShow);
        popupConfig.iframe.css("visibility", !isShow ? "visible" : "hidden");
    }

    function _autoResizeIframeModal(){
        var popupConfig = _getPopupConfig();
        if (popupConfig.iframe.length) {
            popupConfig.iframe.height(0);
            setTimeout(function(){
                popupConfig.iframe.height(adjustHeight(((popupConfig.height != null) ? popupConfig.height : popupConfig.iframe.contents().height()), popupConfig.minHeight));
            },0);

        }
    }

    function _showElement($el, settings) // show an element (such as a div or just plain text)
    {
        var popupConfig = _loadPopup();
        _showElementComponents( popupConfig, $el, settings);
    }

    function _showWrapper($el, settings) // show an element (such as a div or just plain text)
    {
        $el.attr(wrapAttribute, 1);
        var key = "LQBPopupUDiv" + _popupStack.length;

        $el.wrap(getPopupWrapperTemplate(key));
        var $popup =$("#" + key);

        $popup.find('.modal-body').before(popupHeaderHtml);
        $popup.find('.modal-body').after(popUpFooterHtml);
        $popup.find('.modal-content').prepend(popupLoadingHtml +popupIframeDivHtml + popupControlDivHtml);

        var popupConfig = _loadPopup(key);

        _showElementComponents(popupConfig, $el, settings);
    }

    function _showElementComponents(popupConfig, $el, settings){
        settings = $.extend({}, lqbPopupV2DefaultSetting, settings);
        _configModal(popupConfig, settings);
        
        $el = _jquerifyElement($el);
        if(!$.contains(popupConfig.popup[0], $el[0])){
            popupConfig.element.html(TPOStyleUnification.Components($el).html());
        } else {
            TPOStyleUnification.Components($el);
        }

        popupConfig.elementDiv.show();
        $el.show();

        _showPopupModal(popupConfig.popup, settings);
    }

    function _showDivPopup($el, settings) // show a div popup with header and footer itself. Events are handled by itself.
    {
        var popupConfig = _loadPopup();
        settings = $.extend({}, lqbPopupV2DefaultSetting, settings);

        $el = $el.clone(true,true);
        $el.removeClass('hidden Hidden HiddenDiv');
        $el.show();

        _configModal(popupConfig, settings);

        popupConfig.controlDiv.html(TPOStyleUnification.Components($el));         
        popupConfig.controlDiv.show();
        _showPopupModal(popupConfig.popup, settings);
    }

	function _hide() {
		var $popup;

		if (_getPopupConfig() != null) {
			$popup = _getPopupConfig().popup;
		}        

        if(!_isPopupShown($popup)){
            return; 
        }
        
        $popup.modal("hide");
    }

    function _removeWrapPopup(){
        var  popupConfig = _getPopupConfig();
        if (!popupConfig || !popupConfig.popup) return;
        var $wrappedObjs = popupConfig.popup.find('[' +wrapAttribute + "= '1'" + ']');
        if ($wrappedObjs.length > 0){
            if ($wrappedObjs.length > 1) {
                console.error("More than one popup-wrapped component in this page.");
            }

            while ($.contains(popupConfig.popup[0], $wrappedObjs[0]))
            {
                var siblings = $wrappedObjs.siblings();
                var $parent = $wrappedObjs.parent();
                $wrappedObjs.unwrap();
                $wrappedObjs.after($parent);
                siblings.appendTo($parent);
            }

            $wrappedObjs.hide();
        };
    }

    //Remove the current 
    function _cleanUpTopPopup(){
        var  popupConfig = _getPopupConfig();
        var  $popup = popupConfig.popup;
        var  $iframe = popupConfig.iframe;
        settings = null;
        _hidePopupModal($popup);

        $popup.find('.modal-footer button').not("#LQBPopupUCloseBtn").remove();
        $popup.find('.modal-dialog').removeClass(popupClasses);

        returnCallback = null;
        cancelCallback = null;
        if ($iframe.length && $iframe.get(0).contentWindow){
            $iframe.get(0).contentWindow.location.replace('about:blank');
            $iframe.attr("style","");
        }

        _removeWrapPopup();
        _popPopup($popup);
        $popup.remove();
        $popup = null;
    }

    function _showPopupIframe(url, settings, args) {
        settings.args = args;
        _showPopupWithIframe(url, settings);
    }

    function _showPopupWithWrapper(url, settings, args) {
        settings.args = args;
        _showWrapper(url, settings);
    }

    function _showPopupElement(url, settings, args) {
        settings.args = args;
        _showElement(url, settings);
    }

    function _showString(s, settings) {
        _showElement($('<div>').html(s), settings);
    }

    function _showPopupControl(url, settings, args){
        settings.args = args;
        _showDivPopup(url, settings);
    }

    function _return() {
        var popupConfig = _getPopupConfig();
        if (popupConfig)
        {
            if (typeof (popupConfig.returnCallback) === 'function') {
                var args = [].slice.call(arguments);
                _getPopupConfig().returnCallback.apply(this, args);
            }

            _hide();
        }
    }

    function _getCurrentlyDisplayedFilename() {
        if ($iframe == null) {
            return '';
        }
        
        return $iframe.get(0).contentWindow.location.pathname.split('/').pop();
    }

    return {
        Show: _showPopupIframe,
        ShowElement: _showPopupElement,
        ShowDivPopup: _showPopupControl,
        ShowElementWithWrapper: _showPopupWithWrapper,
        ShowString: _showString,
        showWaitTable: showWaitTable,
        Hide: _hide,
        Return: _return,
        AutoResizeIframeModal: _autoResizeIframeModal,
        GetArguments: function() { return _getPopupConfig().args; },
        GetCurrentlyDisplayedFilename: _getCurrentlyDisplayedFilename,
    };
})(jQuery);

function isLqbPopup(window) {
    return (
        window.frameElement != null &&
        jQuery(window.frameElement).closest("#LQBPopupDiv").length > 0
    );
}