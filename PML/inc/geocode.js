﻿function importGeoCodes(args) {
    if (!args) {
        args = getAllFormValues();
    }
    gService.geocode.acall('ImportGeoCodes', args, importGeoCodes_processResults, function (results) { simpleDialog.alert(results.UserMessage).then(lookupGeoCode); }, /*keepQuiet*/true, /*bUseParentForError*/false, /*dontHandleError*/true);

    return false; // prevent postback
}

function importGeoCodes_processResults(results) {
    if (!results.error) {
        var currentFipsControl = document.getElementById('sFannieFips');
        var fipsLocked = document.getElementById('sFannieFipsLckd');
        var newFipsCode = getFipsCode(
            results.value.sHmdaStateCode,
            results.value.sHmdaCountyCode,
            results.value.sHmdaCensusTract)

        var sHmdaMsaNum = document.getElementById('sHmdaMsaNum');
        sHmdaMsaNum.value = results.value.sHmdaMsaNum;

        var sHmdaCountyCode = document.getElementById('sHmdaCountyCode');
        sHmdaCountyCode.value = results.value.sHmdaCountyCode;

        var sHmdaStateCode = document.getElementById('sHmdaStateCode');
        sHmdaStateCode.value = results.value.sHmdaStateCode;

        var sHmdaCensusTract = document.getElementById('sHmdaCensusTract');
        sHmdaCensusTract.value = results.value.sHmdaCensusTract;

        updateDirtyBit();

        if (fipsLocked.checked && currentFipsControl.value !== newFipsCode) {
            getPermissionToOverrideFips(currentFipsControl.value, newFipsCode);
        } else {
            overrideFipsCode(newFipsCode);
        }
    }
    else {
        simpleDialog.alert(results.UserMessage).then(lookupGeoCode);
    }
}

function getFipsCode(stateCode, countyCode, censusTract) {
    return stateCode + countyCode + censusTract.replace(/\./, '');
}

function overrideFipsCode(newFipsCode) {
    var currentFipsControl = document.getElementById('sFannieFips');
    currentFipsControl.text = newFipsCode;

    var fipsLocked = document.getElementById('sFannieFipsLckd');
    fipsLocked.checked = false;

    if (typeof onFipsLocked == 'function') {
        onFipsLocked();
    }
}

function getPermissionToOverrideFips(currentFips, newFipsCode) {
    var message = "The Fannie Mae FIPS Code is manually overridden with a value of " + currentFips
        + ". The Imported GeoCodes create a FIPS Code of " + newFipsCode
        + ". Would you like to override the current FIPS with the values from the imported GeoCodes?";

    jQuery("#LoanHeaderPopup").dialog({
        closeOnEscape: false,
        draggable: false,
        modal: true,
        resizable: false,
        height: 'auto',
        width: 425,
        open: function () {
            jQuery('.ui-dialog-titlebar').hide();
            jQuery('.ui-dialog :button').blur();
            jQuery("#LoanHeaderPopupText").html(message);
        },
        buttons: {
            'Yes': function () { jQuery(this).dialog('close'); overrideFipsCode(newFipsCode); },
            'No': function () { jQuery(this).dialog('close'); }
        }
    });
}

function lookupGeoCode() {
    var url = 'http://www.ffiec.gov/Geocode/default.aspx';
    window.open(url, 'Geocode', 'width=750,height=490,menu=no,status=yes,location=no,scrollbars=yes,resizable=yes');
}
