﻿;(function($, window, document, undefined) {
    $.widget("lqb.downpayments", {
        options: {
            data: [],
            downpaymentSources: [],
            readonly: false
        },

        _create: function() {
            var e = this.element,
                r = $('<tr class="header"/>'),
                d = this.options.data;

            $(e).delegate('input', 'change', function() {

            });

            this.f = $('<tfoot><tr><td colspan="4"><a><i class="material-icons large-material-icons">&#xE146;</i></a></td></tr></tfoot>').appendTo(e);
            
            var thead = $('<thead>').appendTo(e);
            thead.append(r);
            
            r.append('<td class="FieldLabel">Amount</td>');
            r.append('<td class="FieldLabel">(Source of Down Payment)</td>');
            r.append('<td class="FieldLabel">(Explain on this line)</td>');
            r.append('<td class="FieldLabel">&nbsp;</td>');

            this.t = $('<tbody>').appendTo(e);


            var that = this;
            $('a', this.f).click(function() {
                if (!that.options.readonly) {
                    that.addRow();
                    updateDirtyBit();
                }
            });

            createCombobox('sDownPmtSrc', this.options.downpaymentSources);
            this.comboIDSeed = 0;

            while (this.options.data.length < 3) {
                this.options.data.push({ b: '$0.00' });
            }

            this.addRow(this.options.data);
            this.setReadOnly(this.options.readonly);
        },

        setReadOnly: function(isReadOnly) {
            this.options.readonly = isReadOnly;
            $('input', this.t).prop({ 'readonly': isReadOnly, 'disabled': isReadOnly });
            $('input', this.f).prop('disabled', isReadOnly);
            $('button', this.t).prop({ 'readonly': isReadOnly, 'disabled': isReadOnly });
            $('a', this.f).toggleClass('disabled', isReadOnly);
            $('a', this.t).toggleClass('disabled', isReadOnly);
            $('i', this.t).toggleClass('disabled', isReadOnly);
        },

        initialize: function(data) {
            this.t.children('tr:not(.header)').remove();
            this.addRow(data);
            this.setReadOnly(this.options.readonly);
        },

        addRow: function(data) {
            var e = this.element,

                tr = null,
                t = null,
                i = null,
                comboID = null;



            if (!data) {
                data = [{ b: '$0.00', s: '', e: ''}];
            }

            for (var y = 0; y < data.length; y++) {
                var r = data[y];
                tr = $('<tr>').appendTo(this.t);
                t = $('<td>').appendTo(tr);
                i = $('<input>', {
                    'type': 'text',
                    'preset': 'money',
                    'value': r.b,
                    'class': 'amount',
                    'onChange' : 'updateDirtyBit();'
                }).appendTo(t);

                _initMask(i);

                t = $('<td>').appendTo(tr);

                comboID = 'sDownPmtSrc_' + this.comboIDSeed;

                d = $('<div>').appendTo(t);

                i = $('<input>', {
                    'type': 'text',
                    'data-cbl': 'sDownPmtSrc',
                    'id': comboID,
                    'class': 'source form-control-combo',
                    'value': r.s,
                    'onChange': 'updateDirtyBit();'
                }).appendTo(d);

                this.comboIDSeed += 1;

                i.keydown(function(e) {
                    onComboboxKeypress(e.currentTarget);
                }).keypress(function(e) {
                    onComboboxKeypress(e.currentTarget);
                });

                i = $('<i>', {
                    'class': 'combobox_img',
                    'data-cbid': comboID,
                    'html': '&#xE5C5;'
                }).appendTo(d);
                i.click(function() {
                    onSelect($(this).attr('data-cbid'));
                });

                this.downpaymentIdSeed += 1;
                t = $('<td>').appendTo(tr);
                $('<input>', {
                    'type': 'text',
                    'class': 'explain',
                    'value': r.e,
                    'onChange': 'updateDirtyBit();'
                }).appendTo(t);

                t = $('<td>').appendTo(tr);
                $('<a>', {
                    'href': 'javascript:void(0);',
                    'html': '<i class="material-icons large-material-icons">&#xE909;</i>'
                }).appendTo(t).click(function(e) {
                    if($(this).hasClass('disabled')){
                        e.preventDefault();
                        return;
                    }
                    $(this).closest('tr').remove();
                    updateDirtyBit();
                });
                if (typeof TPOStyleUnification === 'object' && typeof TPOStyleUnification.Components === 'function') {
                    TPOStyleUnification.Components(tr);
                }
            }
        },


        serialize: function() {

            var items = [];

            this.t.children('tr').each(function(i, o) {
                if ($(o).is('.header')) {
                    return;
                }
                items.push({
                    b: $('input.amount, input[preset="money"]', o).val(),
                    s: $('input.source', o).val(),
                    e: $('input.explain', o).val()
                });
            });
            return items;
        },


        // Destroy an instantiated plugin and clean up
        // modifications the widget has made to the DOM
        destroy: function() {

            // this.element.removeStuff();
            // For UI 1.8, destroy must be invoked from the
            // base widget
            $.Widget.prototype.destroy.call(this);
            // For UI 1.9, define _destroy instead and don't
            // worry about
            // calling the base widget
        }
    });

})(jQuery, window, document);
