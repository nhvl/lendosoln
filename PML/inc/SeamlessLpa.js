﻿var seamlessLpa = angular.module('SeamlessLPA', ['ngRoute', 'LqbForms', 'LqbAudit'])

.controller('SeamlessLPAController', ['$scope', '$route', '$routeParams', '$location', 'LqbPopupService',
    function SeamlessLPAController($scope, $route, $routeParams, $location, LqbPopupService) {
        this.$route = $route;
        this.$location = $location;
        this.$routeParams = $routeParams;
        $scope.$on('$routeChangeStart', function (event, next, current) {
            $scope.loaded = false;
            $scope.loadSuccess = false;
            $scope.errorMessage = 'Unknown Error.';
        });
        $scope.$on('$routeChangeSuccess', function (event, next, current) {
            $scope.loaded = true;
            $scope.loadSuccess = true;
            $scope.errorMessage = '';
            parent && parent.jQuery("#LQBPopupDiv").dialog('option', { title: next.locals.pageName });
        });
        $scope.$on('$routeChangeError', function (event, next, current, response) {
            $scope.loaded = true;
            $scope.loadSuccess = false;
            $scope.errorMessage = response.data.UserMessage + ' Reference Number: ' + response.data.ErrorReferenceNumber;
        });
        $scope.closeWindow = function()
        {
            return LqbPopupService.popup.Hide();
        }
    }])
.config(function ($compileProvider, $routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');

    $routeProvider
    .when('/audit', {
        templateUrl: seamlessFolder + '/SeamlessLpaAudit.html',
        controller: 'SeamlessLPAAuditController',
        resolve: { 
            auditResult: function ($http, $route, LqbUrl) {
                return $http.post(LqbUrl.seamlessFolder + '/SeamlessLpaService.aspx?method=RunAudit',
                    {
                        LoanId: $route.current.params.loanid,
                        IsEmbeddedPml: !!window.parent.pmlData && window.parent.pmlData.IsEmbeddedPML
                    })
                    .then(function successCallback(response) {
                        var responseData = angular.fromJson(response.data.d.AuditResult);
                        responseData.LoanErrors.auditSuccess = responseData.LoanErrors.length === 0;
                        for (var i = 0; i < responseData.BorrowerAudits.length; i++) {
                            responseData.BorrowerAudits[i].auditSuccess = responseData.BorrowerAudits[i].Errors.length === 0;
                        }
                        return {
                            success: true,
                            loanErrors: responseData.LoanErrors,
                            borrowers: responseData.BorrowerAudits,
                            auditSuccess:
                                responseData.LoanErrors.auditSuccess
                                && responseData.BorrowerAudits.every(function (borrower) { return borrower.auditSuccess; })
                        };
                    });
            },
            pageName: function ($rootScope) { $rootScope.pageTitle = 'LPA Submission Data Audit'; return $rootScope.pageTitle; }
        }
    })

    .when('/login', {
        templateUrl: seamlessFolder + '/SeamlessLpaLogin.html',
        controller: 'SeamlessLPALoginContoller',
        resolve: {
            loginSettings: function ($http, $route, LqbUrl) {
                return $http.post(LqbUrl.seamlessFolder + '/SeamlessLpaService.aspx?method=GetLoginModel', { LoanId: $route.current.params.loanid })
                    .then(function successCallback(response) {
                        var responseData = angular.fromJson(response.data.d.LoginModel);
                        return {
                            success: true,
                            creditProviders: responseData.creditProviderOptions,
                            creditReportOptions: responseData.creditReportOptions,
                            pagemodel: responseData.pagemodel,
                            auditSuccess: responseData.auditSuccess,
                            ignoreAudit: responseData.ignoreAudit,
                            lpaCaseStates: responseData.lpaCaseStates
                        };
                    });
            },
            pageName: function ($rootScope) { $rootScope.pageTitle = 'LPA Submission'; return $rootScope.pageTitle; }
        }
    })

    .when('/summary', {
        templateUrl: seamlessFolder + '/SeamlessLpaSummary.html',
        controller: 'SeamlessLPASummaryController',
        resolve: {
            pageName: function ($rootScope) { $rootScope.pageTitle = 'Casefile Status Summary'; return $rootScope.pageTitle; }
        }
    })

    .otherwise({ redirectTo: '/audit' });
})

.controller('SeamlessLPAAuditController', function ($scope, $location, $routeParams, LqbPopupService, LqbUrl, auditResult) {
    $scope.closeWindow = LqbPopupService.popup.Hide;
    $scope.loanErrors = [{ Message: "Loading LPA Seamless audit results..." }];
    $scope.auditResult = auditResult;
    $scope.virtualRoot = LqbUrl.root;
    $scope.navigateParent = LqbPopupService.navigateParent;

    $scope.nextPage = function () {
        $location.url('/login?loanid=' + encodeURIComponent($routeParams.loanid));
    };
})

.controller('SeamlessLPALoginContoller', function ($rootScope, $scope, $http, $location, $routeParams, $timeout, $window, LqbPopupService, LqbUrl, loginSettings) {
    $scope.closeWindow = LqbPopupService.popup.Hide;
    $scope.creditProviders = loginSettings.creditProviders;
    $scope.creditReportOptions = loginSettings.creditReportOptions;
    $scope.lpaCaseStates = loginSettings.lpaCaseStates;
    $scope.model = loginSettings.pagemodel;
    $scope.auditError = false;

    var dialogArguments = LqbPopupService.getArguments();
    if (dialogArguments && (dialogArguments.selectedCra || Array.isArray(dialogArguments.referenceNumbers))) {
        if (dialogArguments.selectedCra) {
            $scope.model.CreditInfo.CraProviderId = dialogArguments.selectedCra;
        }
        if (Array.isArray(dialogArguments.referenceNumbers)) {
            for (var i = 0; i < $scope.model.CreditInfo.Applications.length; i++) {
                var reportId = dialogArguments.referenceNumbers[i];
                if (reportId) {
                    $scope.model.CreditInfo.Applications[i].CraResubmitId = reportId;
                }
            }
        }
        $scope.model.CreditInfo.CreditReportOption =
            $scope.model.CreditInfo.CraProviderId && $scope.model.CreditInfo.Applications.some(function(application) { return application && application.CraResubmitId; })
                ? $scope.creditReportOptions.UsePrevious.value
                : $scope.creditReportOptions.OrderNew.value;

    }
    $scope.model.CreditInfo.hasCreditReport = $scope.model.CreditInfo.CraProviderId && $scope.model.CreditInfo.Applications.some(function(app) { return app && app.CraResubmitId != ''; });

    $scope.maxPolls = 20;
    $scope.pollCount = 0;

    $scope.previousPage = function () {
        $location.url('/audit?loanid=' + encodeURIComponent($routeParams.loanid));
    };

    $scope.openLpaResultsWindow = function (url) {
        $scope.lpaResponseWindow = $window.open(url, 'LpaResponseWindow', "toolbar=no,menubar=no,location=no,status=no,resizable=yes");
    };

    $scope.ErrorListDetails = {};
    $scope.ViewErrors = function (errors) {
        $scope.ErrorListDetails = {
            ErrorList: errors
        };

        $timeout(function () {
            LQBPopup.ShowElement(angular.element('#ErrorListDetails'), {
                width: 500,
                height: 300,
                elementClasses: 'FullWidthHeight',
                hideCloseButton: true
            });
        }, 100);
    }

    $scope.setErrorStatus = function (message, responseUrl, auditError, errorList) {
        $scope.pollCount = 0;
        $scope.submitting = false;
        $scope.submitMessageClass = 'Error';
        $scope.submitMessageSymbol = 'fa-warning';
        $scope.submitMessage = message;
        $scope.auditError = !!auditError; // Coerce to boolean, defaulting to false
        if (responseUrl)
        {
            $scope.responseLogUrl = LqbUrl.root + responseUrl;
            if ($scope.lpaResponseWindow)
            {
                $scope.lpaResponseWindow.location = $scope.responseLogUrl;
            }
        }

        if(errorList && errorList.length > 1)
        {
            $scope.Errors = errorList;
        }
    };

    $scope.bottomButtonPlaceholderHeight = function () {
        return { height: angular.element('#bottomButtons')[0].clientHeight + 'px' }
    };

    $scope.submit = function () {
        if ($scope.responseLogUrl)
            $window.URL.revokeObjectURL($scope.responseLogUrl);
        $scope.responseLogUrl = null;

        if (this.LpaSubmissionForm.$invalid)
        {
            $scope.submitMessage = 'Could not submit. Form is invalid.';
            $scope.submitMessageClass = 'Error';
            $timeout(function () { $scope.submitMessage = ''; $scope.submitMessageClass = ''; }, 3000);
            return;
        }
        $scope.submitting = true;
        $scope.submitMessageClass = '';
        $scope.submitMessageSymbol = 'fa-spin fa-spinner';
        $scope.submitMessage = 'Submitting to Freddie Mac Loan Product Advisor...';

        $http.post(LqbUrl.seamlessFolder + '/SeamlessLpaService.aspx?method=Submit',
            {
                model: angular.toJson($scope.model),
                LoanId: $routeParams.loanid
            }).then(
            $scope.pollingSuccessCallback,
            function failureCallback(response) {
                var statusMessage = response.data.statusMessage ? response.data.statusMessage + ': ' : '';
                $scope.setErrorStatus(statusMessage
                    + response.data.UserMessage + '\n'
                    + '(Reference Number: ' + response.data.ErrorReferenceNumber + ')');
            });
    };

    $scope.poll = function()
    {
        if ($scope.pollCount >= $scope.maxPolls)
        {
            $scope.setErrorStatus('Did not receive a LPA underwriting response after ' + $scope.maxPolls
                        + ' tries. Please try again later. If the problem persists, please contact your administrator.');
            return;
        }
        $http.post(LqbUrl.seamlessFolder + '/SeamlessLpaService.aspx?method=Poll',
            {
                PublicJobId: $scope.publicJobId,
                AsyncId: $scope.asyncId,
                LpaUserId: $scope.model.LpaCredentials.LpaUserId,
                LpaPassword: $scope.model.LpaCredentials.LpaPassword,
                LoanId: $routeParams.loanid
            }).then($scope.pollingSuccessCallback);
    }

    $scope.pollingSuccessCallback = function (response) {
        var responseData = angular.fromJson(response.data.d.LpaResultsModel);
        if (responseData.Status === 'Done') {
            $scope.pollCount = 0;
            $rootScope.cannotImportCreditReport = !responseData.HasCreditReport;

            $rootScope.resultsPageModel = responseData;
            if ($scope.lpaResponseWindow)
                $scope.lpaResponseWindow.close();
            $location.url('/summary?loanid=' + encodeURIComponent($routeParams.loanid));
        }
        else if (responseData.Status === 'Processing') {
            $scope.publicJobId = responseData.PublicJobId;
            $scope.pollCount++;
            $scope.submitMessageSymbol = 'fa-spin fa-spinner';
            $scope.submitMessage = 'Freddie Mac is processing your request.';
            $scope.asyncId = responseData.AsyncIdentifier;
            if (responseData.MaxRetries)
                $scope.maxPolls = responseData.MaxRetries;
            $timeout($scope.poll, responseData.PollingPeriodMs);
            return;
        }
        else if (responseData.Status === 'LoginError'
                || responseData.Status === 'Error') {
            if (responseData.Errors && responseData.Errors.length > 1)
            {
                $scope.setErrorStatus("Something went wrong", responseData.LpaResponseUrl, undefined, responseData.Errors);
            }
            else
            {
                $scope.setErrorStatus(responseData.ErrorMessage, responseData.LpaResponseUrl);
            }
        }
        else if (responseData.Status === 'AuditFailure') {
            $scope.setErrorStatus(responseData.ErrorMessage, undefined, true);
        }
        else {
            $scope.setErrorStatus('Unexpected Status from the server: "'
                + responseData.Status + '" : ' + responseData.ErrorMessage,
                responseData.LpaResponseUrl);
        }
    }
})
.directive('errorDetailsPopup', function (LqbUrl) {
    return {
        restrict: 'A',
        templateUrl: LqbUrl.angularModules + '/ErrorDetails.html',
        scope: {
            ErrorListDetails: '<errorDetails'
        }
    };
})
.controller('SeamlessLPASummaryController', function ($rootScope, $scope, $http, $location, $routeParams, $window, LqbPopupService, LqbUrl) {
    $scope.closeWindow = LqbPopupService.popup.Hide;
    $scope.updating = false;
    $scope.cannotImportCreditReport = $rootScope.cannotImportCreditReport;
    $scope.resultsPageModel = $rootScope.resultsPageModel;

    $scope.openLpaResultsWindow = function (url) {
        $scope.lpaResponseWindow = $window.open(url, 'LpaResponseWindow', "toolbar=no,menubar=no,location=no,status=no,resizable=yes");
    };

    if ($scope.resultsPageModel) {
        $scope.lpaResults = $scope.resultsPageModel.LpaResults;
    }
    else
    {
        $scope.lpaResults = {};
        $location.url('/login?loanid=' + encodeURIComponent($routeParams.loanid));
    }

    if ($scope.lpaResults && $scope.lpaResults['AUS Status']=== 'Error')
    {
        $scope.responseLogUrl = $scope.resultsPageModel.LpaResponseUrl;
    }

    if ($scope.cannotImportCreditReport)
    {
        $scope.resultsPageModel.ImportCreditReport = false;
        $scope.resultsPageModel.ImportLiabilities = false;
        $scope.creditReportMessage = "No Credit Report was returned from the AUS."
    }

    $scope.importCreditReport_change = function(importCreditReport) {
        if (!importCreditReport) {
            $scope.resultsPageModel.ImportLiabilities = false;
        }
    };

    $scope.displayLPFeedback = function () {
        var report_url = LqbUrl.root + $scope.resultsPageModel.LpaResponseUrl;
        var reportWindow = window.open(report_url, 'report', "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=1200,height=600");
        reportWindow.focus();
    };

    $scope.updateLoan = function () {
        $scope.updating = true;
        $http.post(LqbUrl.seamlessFolder + '/SeamlessLpaService.aspx?method=UpdateLoan',
            {
                LoanId: $routeParams.loanid,
                ImportFindings: $scope.resultsPageModel.ImportFindings ? true : false,
                ImportCreditReport: $scope.resultsPageModel.ImportCreditReport ? true : false,
                ImportLiabilities: $scope.resultsPageModel.ImportLiabilities ? true : false
            }).then(function successCallback(response) {
                $scope.updating = false;
                $scope.displayLPFeedback();
                $scope.closeWindow();
                LqbPopupService.refreshParent();
            }, function errorCallback(response) {
                $scope.updating = false;
                $scope.errorMessage = response.data.UserMessage + '\n'
                    + '(Reference Number: ' + response.data.ErrorReferenceNumber + ')';
            });
    }
});
