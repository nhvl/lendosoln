var UnloadMessage = "Leave site?  Changes you made may not be saved.";

var gHighlightRowColor = "";
// Color of highlighted row.
var gReadonlyBackgroundColor = '#EEEEEE';
var gHighlightColor = 'transparent';
var oldTabIndex = 0;
if (typeof(gVirtualRoot) == 'undefined') {
	gVirtualRoot = document.getElementById("VirtualRoot") == null ? "" : document.getElementById("VirtualRoot").value;
}
//---- Put a transparent layer on top of the current screen.
function copyStringToClipboard(stringData) {
    if (window.clipboardData) {  // Internet Explorer
        window.clipboardData.setData('Text', stringData);
    } else if (document.body) {
        var input = document.createElement("textarea");
        input.value = stringData;
        document.body.appendChild(input);

        input.select();
        document.execCommand("copy");
        input.parentNode.removeChild(input);
    } else {
        window.prompt("Copy to clipboard: Ctrl+C, Enter", stringData);
    }
}
function debugObject(o) {
	var str = '';
	var i = 0;
	for (p in o)
		if (p != 'outerText' && p != 'innerHTML' && p != 'innerText' && p != 'outerHTML')
			str += 'o.' + p + ' = ' + o[p] + (i++ % 10 == 0 ? '\n\r' : ';');
	alert(str);
}

function f_disabledInputForBlockMask() {
	var coll = document.getElementsByTagName('select');
	for (var i = 0; i < coll.length; i++) {
		var o = coll[i];
		if (typeof o.attributes['data-mask-ignore'] === 'undefined') {
		    o.style.display = 'none';
		    o._blockTabIndex = o.tabIndex;
		    o.tabIndex = -1;
		}
	}
	var oTaggableTags = new Array("INPUT", "A", "BUTTON", "TEXTAREA", "IFRAME");
	for (var i = 0; i < oTaggableTags.length; i++) {
		var oList = document.getElementsByTagName(oTaggableTags[i]);
		for (var j = 0; j < oList.length; j++) {
			var o = oList[j];
			o._blockTabIndex = o.tabIndex;
			o.tabIndex = -1;
		}
	}
}

function f_enabledInputForBlockMask() {
	var coll = document.getElementsByTagName('select');
	for (var i = 0; i < coll.length; i++) {
		var o = coll[i];
		o.style.display = '';
		o.tabIndex = o._blockTabIndex;
	}
	var oTaggableTags = new Array("INPUT", "A", "BUTTON", "TEXTAREA", "IFRAME");
	for (var i = 0; i < oTaggableTags.length; i++) {
		var oList = document.getElementsByTagName(oTaggableTags[i]);
		for (var j = 0; j < oList.length; j++) {
			var o = oList[j];
			o.tabIndex = o._blockTabIndex;
		}
	}
}

function f_getViewportHeight() {
	if (window.innerHeight != window.undefined) return window.innerHeight;
	if (document.compatMode == 'CSS1Compat') return document.documentElement.clientHeight;
	if (document.body) return document.body.clientHeight;
	return window.undefined;
}

function f_getViewportWidth() {
	if (window.innerWidth != window.undefined) return window.innerWidth;
	if (document.compatMode == 'CSS1Compat') return document.documentElement.clientWidth;
	if (document.body) return document.body.clientWidth;
	return window.undefined;
}

function f_getScrollLeft() {
	return document.body.scrollLeft;
}

function f_getScrollTop() {
	return document.body.scrollTop;
}

function f_displayBlockMask(bDisplay) {
	var sUniqueId = '__BlockMask210587__';
	var oBlockMaskContent = document.getElementById(sUniqueId);
	if (null == oBlockMaskContent && bDisplay) {

		var oBody = document.getElementsByTagName("BODY")[0];
		oBlockMaskContent = document.createElement("div");
		oBlockMaskContent.id = sUniqueId;
		oBlockMaskContent.className = 'BlockMask';
		oBody.appendChild(oBlockMaskContent);

	} else if (null != oBlockMaskContent && !bDisplay) {
		oBlockMaskContent.style.display = 'none';
	}

	if (bDisplay) {
		f_disabledInputForBlockMask();
		f_resizeBlockMask();
	} else {
		f_enabledInputForBlockMask();
	}


}

function f_resizeBlockMask() {
	var sUniqueId = '__BlockMask210587__';
	var oBlockMaskContent = document.getElementById(sUniqueId);

	oBlockMaskContent.style.display = '';

}

//---- END Put a transparent layer on top of the current screen.

// ------------------- Zero - One CheckBoxList

function _registerCBL(name, c) {
	for (var i = 0; i < c; i++) {
		var o = document.getElementById(name + "_" + i);

		o.onclick = function() { onZeroOneCheckboxClick(this, name, c); };
	}
}

// The hidden variables use to store the value of checkbox list will have prefix of 'cbl_'.
// If there is no prefix then error will occur when normal postback occur on CheckBoxList. dd 12/8/03

function onZeroOneCheckboxClick(cb, name, count) {
	//alert('Name = ' + name + ', count = ' + count);
	var b = cb.checked;
	for (var i = 0; i < count; i++) {
		document.getElementById(name + "_" + i).checked = false;
	}
	cb.checked = b;
	var index = cb.name.substr(name.length + 1, cb.name.length - name.length);
	if (document.forms[0]["cbl_" + name] != null)
		document.forms[0]["cbl_" + name].value = b ? eval(name + "_items[" + index + "]") : "";
//  alert(name + ' = ' + document.forms[0][name].value);
}

// ---- End Zero - One CheckBoxList

function TextAreaMaxLength(fieldObj, maxChars) {
	var diff = maxChars - fieldObj.value.length;
	if (diff < 0) fieldObj.value = fieldObj.value.substring(0, maxChars);
}

function parseMoneyFloat(str) {
	var a = str.indexOf("(") >= 0 ? -1 : 1;
	str = str.replace(/[,$%()]/g, "");
	if (str == "") str = "0";
	return parseFloat(str) * a;
}

function displayHelp() {
    var helpUrl = 'https://support.lendingqb.com/imgstor/imagestore/KBSCPDF_Accessing the LendingQB Support Center PDF.pdf';
	if (handle != null) handle.focus();
}

// 2/2/2012 dd - Crossbrowser Compatible.

function populateForm(obj, clientID) {
	var $inputList = jQuery('form :input');

	var prefix = (clientID == null || clientID == "") ? "" : clientID + '$';
	for (property in obj) {
		var val = obj[property];
		$inputList.filter('[name="' + prefix + property + '"]').each(function() {
			var $this = jQuery(this);
			var type = $this.prop('type');
			if (type == 'radio') {
				if ($this.val() == val) {
					$this.prop('checked', true);
				}
			} else if (type == 'checkbox') {
				$this.prop('checked', val == 'True');
			} else {
				$this.val(val);
			}
		});
	}
}

// Usage:
// getAllFormValues() will collect all input on current page and pack into an object.
// This could lead to performance problem if there are many input on the page. To skip
// unnessary input then add the attribute SkipMe to input tag (ie. <input type=text SkipMe>) and invoke
// getAllFormValues('', true); dd 7/25/2003
// // 1/30/2012 dd - Updated to cross browser compatible. Require JQuery.

function getAllFormValues(clientID, skipIgnore) {
	var skipNameRegEx = /^(IsUpdate|__VIEWSTATE|__EVENTTARGET|__EVENTARGUMENT|__EVENTVALIDATION)$/;
	var skipInputTypeRegEx = /^(submit|button)$/;
	var obj = { };

	jQuery('form :input').each(function() {
		var $el = jQuery(this);

		var name = $el.prop('name');
		var type = $el.prop('type');

		if (skipNameRegEx.test(name) || skipInputTypeRegEx.test(type)) {
			return true; // Continue
		}

		if (!!skipIgnore && $el.attr('SkipMe') != null) {
			return true;
		}

		if (typeof name === 'undefined') {
            // This input is most likely generated from a library.
		    return true;
		}

		if (clientID != null && clientID != '' && !name.match(clientID + '\\$') && !name.match(clientID + '_')) {
			return true; // Skip name if it does not belong to this control's clientID
		}
		if (clientID != null && clientID != '') {
			name = name.replace(clientID + '$', ''); // Remove clientID_ in front of input name.
		}
		if (type == 'radio') {
			if ($el.prop('checked')) {
				obj[name] = $el.val();
			}
		} else if (type == 'checkbox') {
			obj[name] = $el.prop('checked') ? 'True' : 'False';
		} else {
			obj[name] = $el.val();
		}
	}
	);

	return obj;
}

function highlightRowByCheckbox(o, bgColor) {
	// Highlight row if checkbox is check.
	if (o.checked != null && o.checked) {
		highlightRow(o, bgColor);
	} else {
		unhighlightRow(o, "");
	}
}

function highlightRow(o, bgColor) {
	// Highlight the inner most <tr> tag.
	if (null == o) return;
	var e = o;
	while (e != null && e.nodeName.toLowerCase() != "body") {
		if (e.nodeName.toLowerCase() == "tr") {
			e.previousBackgroundColor = e.style.backgroundColor;
			bgColor = bgColor == null ? gHighlightRowColor : bgColor;
			e.style.backgroundColor = bgColor;
			break;
		}
		e = e.parentElement;
	}
}

function unhighlightRow(o, bgColor) {
	// unhighlight the inner most <tr> tag.
	if (null == o) return;
	var e = o;


	while (e != null && e.nodeName.toLowerCase() != "body") {
		if (e.nodeName.toLowerCase() == "tr") {
			if (null != bgColor)
				e.style.backgroundColor = bgColor;
			else
				e.style.backgroundColor = e.previousBackgroundColor == null ? "" : e.previousBackgroundColor;
			break;
		}
		e = e.parentElement;
	}

}

function smartZipcodeAuto(event) {
    var zipcode = event.srcElement || event.target;
    if (zipcode == null) {
        return;
    }

    var retrieveLinkedInputForZipCode = function (zipcode, inputType) {
        var inputId = zipcode.getAttribute(inputType);
        if (inputId) {
            return document.getElementById(inputId);
        }

        return null;
    }

    var city = retrieveLinkedInputForZipCode(zipcode, "city");
    var state = retrieveLinkedInputForZipCode(zipcode, "state");
    var county = retrieveLinkedInputForZipCode(zipcode, "county");

    smartZipcode(zipcode, city, state, county, event);
}

function smartZipcode(zipcode, city, state, county, event) {
    if (gService == null || gService.utils == null || zipcode == null) return;
    // 3/3/04 dd - Don't perform lookup if zipcode does not change from previous value.
    if (typeof (zipcode.oldValue) != null && zipcode.oldValue == zipcode.value) {
        if (county != null && typeof (county.value) != null && county.value != "")
            return;
        else if (county == null)
            return;


    }

    updateDirtyBit();

    var args = new Object();
    args.GetCounties = county != null && county.nodeName.toLowerCase() == "select" ? "true" : "false";

    args.zipcode = zipcode.value;
    var result;
    gService.utils.callAsyncSimple("smartzipcode", args, function (result) { smartZipcode_postSeviceCall(result, zipcode, city, state, county) });
}

function smartZipcode_postSeviceCall(result, zipcode, city, state, county) {
	if (!result.error) {
		var val = result.value;
		if (val.IsValid == "1") {
			if (state != null) {
				state.oldValue = val.State;
				state.value = val.State;
			}
			if (city != null) {
				if (state.selectedIndex != -1) {
					city.value = val.City;

				}
			}

			if (county != null && county.nodeName.toLowerCase() != "select") {
				county.value = val.County;
			} else if (county != null) {

				county.options.length = 0;
				if (state.selectedIndex != -1) {
					for (i = 0; i < val.CountyCount; i++) {
						p = "val.County" + i;
						countyName = eval(p);
						selected = countyName.toLowerCase() == val.County.toLowerCase();
						county.options[i] = new Option(countyName, countyName, false, selected);
					}
				}

			}


			//      zipcode.onchange();
		} else if (county != null && county.nodeName.toLowerCase() == "select") {
			county.selectedIndex = -1;
		}

		$j(zipcode).trigger("change"); // 3/30/2006 mf - So the control doesn't have to define onchange handler; 2/7/12 mp - cross-browser compat
	}


}

function UpdateCounties(state, county, callback) {
	if (gService == null || gService.utils == null || state == null || county == null) return;
	if (typeof(state.oldValue) != null && state.oldValue == state.selectedIndex) return;


	updateDirtyBit();
	var args = new Object();
	args.state = state.options[state.selectedIndex].value;
	county.options.length = 0;

	if (args.state == "") {
	    return;
	}
	else {
	    gService.utils.callAsyncSimple("GetCountiesForState", args, function (result) {
	        if (result.error)
	            return;

	        val = result.value;
	        county.options[0] = new Option("", "");
	        for (i = 0; i < val.CountyCount; i++) {
	            p = "val.County" + i;
	            countyName = eval(p);
	            county.options[i + 1] = new Option(countyName, countyName, false, false);
	        }

	        if (typeof (callback) == 'function') {
	            callback();
	        }
	    });
	}
}

function parentHasInfo() {
    try {
        return ((typeof (parent.info) != 'undefined') && (parent.info != null));
    }
    catch (e) {
        return false;
    }
}
function updateDirtyBit(e) {
	if (null != e) {
		var src = e.srcElement ? e.srcElement : e.target;
		var key = (e.keyCode ? e.keyCode : e.which);
		if (null != src && (src.readOnly || src.disabled)) return; // DO NOT update dirty bit if field is readonly.
		if (null != key && (key == 9 || key == 16 || key == 17)) return; // DO NOT update when user tabs. 09/12/06 mf OPM 7310. Updated 11/10/09 for opm 32915 vm - DO NOT update on ctrl
		if (null != key && e.ctrlKey && key == 67) return; // DO NOT update on Ctrl + C. 11/10/09 vm opm 32915
	}

	// Enable save button on the upper left frame. This only apply to /los/loanapp.aspx.
	if (parentHasInfo() && typeof (parent.info.f_enableSave) == 'function')
		parent.info.f_enableSave(true);

	if (document.getElementById("IsUpdate") != null) document.getElementById("IsUpdate").value = "1";

	if (typeof(updateDirtyBit_callback) == 'function') {
	    // Define this method on page when you want notify when dirty bit is set.
	    updateDirtyBit_callback();	
    }

    var saveBtn = document.getElementById("saveBtn");
    if (saveBtn != null)
        saveBtn.disabled = false;

    if (!disableExplicitSavingPage) {
        $j("#save-button-with-tooltip").hide();
        $j("#save-button-without-tooltip").show();
    }    
}

function isDirty() {
	if (document.getElementById("IsUpdate") != null) return document.getElementById("IsUpdate").value == "1";
	else return true;
}

function isReadOnly() {
	if (document.getElementById("_ReadOnly") != null) return document.getElementById("_ReadOnly").value == "True";
	else return false;
}

function clearDirty() {
    // Enable save button on the upper left frame. This only apply to /los/loanapp.aspx.
    if (parentHasInfo() && typeof (parent.info.f_enableSave) == 'function')
		parent.info.f_enableSave(false);

    if (document.getElementById("IsUpdate") != null) document.getElementById("IsUpdate").value = "0";
	
	if(document.getElementById("saveBtn") != null)
	    document.getElementById("saveBtn").disabled = true;

	if (!disableExplicitSavingPage) {
	    $j("#save-button-with-tooltip").show();
	    $j("#save-button-without-tooltip").hide();
	}
}

function formatReadonlyField(o) {
	o.style.backgroundColor = '';
	o.oldTabIndex = o.tabIndex;
	o.tabIndex = -1;
}

function highlightField(ev) {
	var e = this, $e = $j(this);
	if ($e.attr('NoHighlight') != null) return; // 5/26/2004 dd - Explicitly request by component to not highlight
	if (!$e.prop('readonly') && $e.prop('tagName') != 'SELECT') {
		var type = $e.prop('type');
		if (typeof(e.select) != "undefined" && type != 'button' && type != 'submit') {
			e.select();
		}
	}
}

// Before, the mouseup event was always calling preventDefault.
// Now, if it is a textbox, make sure that you are
// supposed to disable it. This will allow the user to put the caret
// to a certain position without only allowing selection of the entire
// input value. GF OPM 91979

function f_mouseup(ev) {
	var e = this, $e = $j(this);
	var type = $e.prop('type');
	if (type == 'text' || type == 'textarea') {
		if ($e.data('DoNotDeselectWithMouseup') == true) {
			$e.data('DoNotDeselectWithMouseup', false);
			ev.preventDefault();
		}
	} else {
		ev.preventDefault();
	}
}

// If mouse down and the input does not have focus, we want to prevent the default
// behavior, which would deselect the text in webkit and cause issues with FF.

function f_mousedown(ev) {
	var e = this;
	$e = $j(this);
	var type = $e.prop('type');
	if (type == 'text' && !$e.is(':focus')) {
		$e.data('DoNotDeselectWithMouseup', true);
	}
}

function attachCommonEvents(o) {
	$o = $j(o);
	if (isReadOnly()) {
		if ($o.attr('type') == 'button') $o.attr('disabled', 'disabled');
		return;
	}
	// 10/26/2007 dd - In IE7, if you highlight the background of drop down list, you will need to click twice to expand the list
	// In Webkit browsers (Safari and Chrome), the field will become unselected due to the mouseup event - so we need to prevent that.
	$o.on("focus.highlightField", highlightField).on("mouseup.highlightField", f_mouseup).on("mousedown.highlightField", f_mousedown);

	// Change from 'onchange' to 'onkeyup'. As soon as user enter text in input  
	// modify dirty bit.
	var editable = !$o.attr('NotForEdit'), tag = $o.prop('tagName').toUpperCase(), type = inputType = $o.prop('type');
	//new update 2014
	if (type == null) return;
	if (editable) {
		inputType = inputType.toUpperCase();
		if (tag == 'INPUT' && (inputType == 'CHECKBOX' || inputType == 'RADIO')) {
			$o.on("click.updateDirtyBit", updateDirtyBit);
		} else if (tag == 'SELECT') {
			$o.on("change.updateDirtyBit", updateDirtyBit);
		} else {
			$o.on("keydown.updateDirtyBit paste.updateDirtyBit", updateDirtyBit);
		}
	}
}

function event_keydown(e) {
	//debugObject(event.srcElement);
	// Because IE will convert Backspace into a Back browse button. Therefore I will disable the backspace key.
	// Backspace only enable on INPUT type=text/password and TEXTAREA. dd
	var c = (e.keyCode ? e.keyCode : e.which);
	var o = e.srcElement ? e.srcElement : e.target, nodeName = o.nodeName.toLowerCase();

	if (c === 8) { // Backspace key press


		if (o != null && ((nodeName == "input" && (o.type == "text" || o.type == "password" || o.type == "file")) || (nodeName == "textarea"))) {
			if (o.readOnly || o.disabled) return false; // If textbox is readonly or disable then also disable backspace
			else return true; // Don't disable backspace
		} else {
			return false; // Disable backspace key
		}

	} else if (e.ctrlKey && e.altKey && c == 81 && window.clipboardData) { //only for ie
		// Ctrl + Alt + Q - Copy object id to clipboard
		if (null != o) {
			var id = o.id;
			if (document.forms[0]["_ClientID"] != null && document.forms[0]["_ClientID"].value != "") {
				id = id.replace(document.forms[0]["_ClientID"].value + "_", "");
			}
			window.clipboardData.setData("Text", id);
			alert('Field id: ' + id + ' is copied to your windows clipboard.');
			return false;
		}
	}

}

function _initInput() {
	$j(document).bind("keydown", event_keydown);
	var coll = document.getElementsByTagName("INPUT");
	var length = coll.length;
	for (i = 0; i < length; i++) {
		var o = coll[i], $o = $j(o);
		if ($o.prop('readonly') && !$o.attr('NotEditable')) formatReadonlyField(o);
		if ($o.attr('preset') != null && typeof(_initMask) == 'function') _initMask($o);
		attachCommonEvents(o);
		//   o.attachEvent("onpropertychange", event_onpropertychange); FIX
	}
	coll = document.getElementsByTagName("SELECT");
	length = coll.length;
	if (length > 0) {
		for (i = 0; i < length; i++) {
			var o = coll[i];
			$o = $j(o);
			if ($o.prop('disabled') && !$o.attr('NotEditable')) disableDDL(o, true);
			attachCommonEvents(o);

		}
	}
	coll = document.getElementsByTagName("TEXTAREA");
	length = coll.length;
	if (length > 0) {
		for (i = 0; i < length; i++) {
			var o = coll[i];
			if (o.readOnly) formatReadonlyField(o);
			attachCommonEvents(o);
		}
	}
	// While init some input set dirtybit.
	// clear dirty bit after finish init. dd 6/4/2003
	clearDirty();
}

function event_onpropertychange() {
	var propertyName = event.propertyName;
	var e = event.srcElement;
	if (propertyName == "readOnly") {
		if (isReadOnly()) e.readOnly = true;
		if (e.readOnly) formatReadonlyField(e);
		else {
			e.style.backgroundColor = "";
			e.tabIndex = e.oldTabIndex != null ? e.oldTabIndex : 0;
		}
	}
}

function disableDDL(ddl, b) {
	ddl.disabled = b;
	//ddl.style.backgroundColor = b ? gReadonlyBackgroundColor : "";
}

function event_onkeyup(event) {

	if (typeof pml_action == 'function') pml_action('event_onkeyup');

	if (event.ctrlKey && event.keyCode == 83) {
		// Ctrl + S - Save loan information.
		if (typeof(f_saveMe) == 'function') f_saveMe();
	} else {
		if (typeof(f_customEventKeyup) == 'function') f_customEventKeyup(event);
	}
}

function displayObnoxiousColor() {
	var pattern = /(TextBox|SSNTextBox|DropDownList|DateTextBox|CheckBox|CheckBoxList|PhoneTextBox|ZipcodeTextBox|ComboBox|PercentTextBox|MoneyTextBox|PhoneTextBox)\d/;
	var coll = document.getElementsByTagName("INPUT");
	var length = coll.length;
	if (length > 0) {
		for (i = 0; i < length; i++) {
			var o = coll[i];
			if (o.id.match(pattern))
				o.style.backgroundColor = 'blue';
		}
	}
	coll = document.getElementsByTagName("SELECT");
	length = coll.length;
	if (length > 0) {
		for (i = 0; i < length; i++) {
			var o = coll[i];
			if (o.id.match(pattern))
				o.style.backgroundColor = 'blue';
		}
	}
	coll = document.getElementsByTagName("TEXTAREA");
	length = coll.length;
	if (length > 0) {
		for (i = 0; i < length; i++) {
			var o = coll[i];
			if (o.id.match(pattern))
				o.style.backgroundColor = 'blue';
		}
	}

}

// Define rolodex here. 
//

function cRolodex() {
	this.chooseFromRolodex = cRolodex_ChooseFromRolodex;
	this.addToRolodex = cRolodex_AddToRolodex;
	this.populateFromOfficialContactList = cRolodex_PopulateFromOfficialContactList;
}

// Display a list from rolodex. If 'type' is define then rolodex will set the filter
// on the tab by default.
// Return:
//
//     var rolodex = new cRolodex();
//     var args = rolodex.chooseFromRolodex();
//
//        args.OK
//        args.DisplayName // Name use to display in alert.
//        args.AgentName
//        args.AgentType
//        args.AgentStreetAddr
//        args.AgentCity
//        args.AgentState
//        args.AgentZip
//        args.AgentAltPhone
//        args.AgentCompanyName
//        args.AgentEmail
//        args.AgentFax
//        args.AgentPhone
//        args.AgentTitle 
//        args.AgentDepartmentName
//        args.AgentPager 
//        args.AgentLicenseNumber
//        args.AgentNote
//        args.PhoneOfCompany;
//        args.FaxOfCompany;

function cRolodex_ChooseFromRolodex(type) {
	if (typeof(showModal) != "function") {
		alert("showModal is not define. Please include /common/ModalDlg/CModalDlg.js in your page.");
		var args = new Object();
		args.OK = false;
		return args;
	}
	return showModal('/webapp/RolodexList.aspx?type=' + type);
}

function cRolodex_PopulateFromOfficialContactList(sLId, agentRoleT) {
    if (gService == null || gService.utils == null) {
        alert("Your page need to inherit from LendersOffice.Common.BaseServicePage");
        return;
    }
    var args = new Object();
    args["LoanID"] = sLId;
    args["AgentRoleT"] = agentRoleT;
    gService.utils.callAsyncSimple("PopulateFromOfficialContactList", args, function (result) { PopulateFromOfficialContactList_postServiceCall(result, callback) });
}

function PopulateFromOfficialContactList_postServiceCall(result, callback) {
    var ret = new Object();
    if (!result.error) {

        ret.AgentName = result.value["AgentName"];
        ret.AgentStreetAddr = result.value["AgentStreetAddr"];
        ret.AgentCity = result.value["AgentCity"];
        ret.AgentState = result.value["AgentState"];
        ret.AgentZip = result.value["AgentZip"];
        ret.AgentCompanyName = result.value["AgentCompanyName"];
        ret.AgentEmail = result.value["AgentEmail"];
        ret.AgentFax = result.value["AgentFax"];
        ret.AgentPhone = result.value["AgentPhone"];
        ret.AgentTitle = result.value["AgentTitle"];
        ret.AgentDepartmentName = result.value["AgentDepartmentName"];
        ret.AgentPager = result.value["AgentPager"];
        ret.AgentLicenseNumber = result.value["AgentLicenseNumber"];
        ret.PhoneOfCompany = result.value["PhoneOfCompany"];
        ret.FaxOfCompany = result.value["FaxOfCompany"];
        ret.CompanyLicenseNumber = result.value["CompanyLicenseNumber"];
        ret.LoanOriginatorIdentifier = result.value["LoanOriginatorIdentifier"];
        ret.CompanyLoanOriginatorIdentifier = result.value["CompanyLoanOriginatorIdentifier"];
        ret.TaxID = result.value["TaxID"];
        ret.AgentSourceT = result.value["AgentSourceT"];
        ret.BrokerLevelAgentID = result.value["BrokerLevelAgentID"];

        // OPM 150537 - Added fields for funding page
        ret.BranchName = result.value["BranchName"];
        ret.PayToBankName = result.value["PayToBankName"];
        ret.PayToBankCityState = result.value["PayToBankCityState"];
        ret.PayToABANumber = result.value["PayToABANumber"];
        ret.PayToAccountName = result.value["PayToAccountName"];
        ret.PayToAccountNumber = result.value["PayToAccountNumber"];
        ret.FurtherCreditToAccountNumber = result.value["FurtherCreditToAccountNumber"];
        ret.FurtherCreditToAccountName = result.value["FurtherCreditToAccountName"];
    }

    if (typeof (callback) == 'function') {
        callback(ret);
    }
}
function cRolodex_PackageAddToRolodexResult(result) {
    var ret = new Object;
    if (!result.error) {
        ret.BrokerLevelAgentID = result.value["BrokerLevelAgentID"];
    }
    return ret;
}

function hasAttr(tag, attrName) {
    return tag.hasAttribute ? tag.hasAttribute(attrName)
                            : !!(tag.attributes[attrName] && tag.attributes[attrName].specified);
}

function cRolodex_AddToRolodex(args) {
	if (gService == null || gService.utils == null) {
		alert("Your page need to inherit from LendersOffice.Common.BaseServicePage");
		return;
	}

	args.AddCommand = "AddIfUnique";

	gService.utils.callAsyncSimple("AddToRolodex", args, function (result) { AddToRolodex_postServiceCall(result, args); });
}

function AddToRolodex_postServiceCall(result, args) {
    var name, comp, disp;

	name = args.AgentName;
	comp = args.AgentCompanyName;

	if (name != null && name != "" && comp != null && comp != "") {
		disp = "Agent: " + name + " of company " + comp;
	} else if (name != null && name != "") {
		disp = "Agent: " + name;
	} else if (comp != null && comp != "") {
		disp = "Entry: " + comp;
	} else {
		disp = "Entry";
	}

	if (!result.error) {
		if (result.value.AddResult == "Collided") {
			var o = new Object();
			if (result.value.ReasonWhy != null && result.value.ReasonWhy != "") {
				o.Msg = disp + " is already in Contacts.<br><br>" + result.value.ReasonWhy;
			} else {
				o.Msg = disp + " is already in the Contacts.";
			}
			window.showModal("/inc/customdia.htm", o, "dialogHeight:200px;dialogwidth:450px;", null, function(o){
				var ret = o.ret;

				if (ret == 6) {
				args.AddCommand = "Overwrite";
				} else if (ret == 7) {
				args.AddCommand = "Add";
				} else {
				return false; 
				}

				gService.utils.callAsyncSimple("AddToRolodex", args, function (result2) {
				    if (!result2.error && result2.value.AddResult == "Added") {
				        if (args.AddCommand == "Overwrite") {
				            alert(disp + " has been replaced in Contacts.");
				        } else {
				            alert(disp + " added to Contacts.");
				        }
				    } else if (!result2.error && result2.value.AddResult == "Denied") {
				        alert("Access denied.  You do not have permission to add"
	                        + " contact entries.  Please consult your account"
	                        + " administrator if you have any questions."
	                    );
				    } else {
				        alert("Unable to add " + disp + " to Contacts.");
				    }
				});
			});
		} else if (result.value.AddResult == "Denied") {
			alert("Access denied.  You do not have permission to add"
				+ " contact entries.  Please consult your account"
				+ " administrator if you have any questions."
			);
		} else {
			alert(disp + " added to Contacts.");
		}
	} else {
		alert("Unable to add " + disp + " to Contacts.");
	}
}

function pageY(elem) {
	return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
}

// calendar
var gCalendar;

function selectedDate(cal, date) {

	{
		cal.sel.value = date;
		cal.callCloseHandler();
		if (typeof(updateDirtyBit) == 'function') updateDirtyBit();
		$j(cal.sel).trigger('change');
	}
}

function closeCalHandler(cal) {
	cal.hide();
}

function displayCalendar(field) {
	var el = document.getElementById(field);
	if (el.readOnly || el.disabled)
		return false;

	if (gCalendar != null) {
		gCalendar.hide();
	} else {
		var cal = new Calendar(false, null, selectedDate, closeCalHandler);
		gCalendar = cal;
		cal.setRange(1900, 2070);
		cal.setDateFormat('mm/dd/y');
		cal.create();
	}
	gCalendar.sel = el;
	var dateVal = gCalendar.sel.value;
	if (dateVal != '') {
		$j(gCalendar.sel).trigger('blur');
		gCalendar._init(false, new Date(dateVal));
	}
	gCalendar.showAtElement(el);
	if (correctCalendarPos)
	    correctCalendarPos(el);
	return false;
}

function lockField(cb, tbID) {
    var clientID = "";
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value + "_";

    var item = document.getElementById(clientID + tbID);
    item.readOnly = !cb.checked;
}
function isInternetExplorer() {
    // Modified from https://codepen.io/gapcode/pen/vEJNZN
    if (window.navigator.appName === 'Microsoft Internet Explorer') {
	   return true;
	}

    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE');
    if (msie >= 0) {
        // IE 10 or older
        return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident >= 0) {
        // IE 11
        return true;
    }

    var edge = ua.indexOf('Edge/');
    if (edge >= 0) {
        // Edge (IE 12+)
        return true;
    }

    return false;
}

var currentUrl;
var disableExplicitSavingPage;

(function ($) {
    currentUrl = window.location.href;

    // Keep this list in sync with LoanNavigationHeader.ascx
    var explicitlyDisabledSaveButtonPages = [
        "/webapp/pml.aspx?source=PML",
        "/main/EDocs/Edocs.aspx",
        "/main/Tasks/TaskList.aspx",
        "/webapp/OrderServices.aspx",
        "/webapp/StatusAndAgents.aspx",
        "/webapp/RateLock.aspx"
    ];

    var isExplicitlyDisabled = false;
    for (var i = 0; i < explicitlyDisabledSaveButtonPages.length; ++i) {
        if (currentUrl.indexOf(explicitlyDisabledSaveButtonPages[i]) !== -1) {
            isExplicitlyDisabled = true;
            break;
        }
    }

    disableExplicitSavingPage = isExplicitlyDisabled;

    if (disableExplicitSavingPage) {
        $j("#save-button-with-tooltip").show();
        $j("#save-button-without-tooltip").hide();
    }

	$.fn.readOnly = function() {
		if (arguments.length == 0 && this.length > 0) {
			var e = this.first(), tagName = e.prop('tagName');
			switch (tagName) {
			case 'INPUT':
				if (e.is('[type=checkbox]')) {
					return e.prop('disabled');
				} else {
					return e.prop('readonly');
				}
			case 'SELECT':
				return e.prop('disabled');
			case 'TEXTAREA':
				return e.prop('readonly');
			default:
				return false;
			}
		}
		var isReadOnly = arguments[0], readOnlyFn = formatReadonlyField, disableDDLFn = disableDDL;
		return this.each(function() {
			var $this = $(this), tagName = $this.prop('tagName');
			if (tagName == 'INPUT' || tagName == 'TEXTAREA') {
				if (isReadOnly) {
					readOnlyFn(this);
				} else {
					$this.css('background-color', '');
					this.tabIndex = this.oldTabIndex != null ? this.oldTabIndex : 0;
				}

				$this.prop('readonly', isReadOnly);
				if ($this.is('[type=checkbox]')) {
					$this.prop('disabled', isReadOnly);
				}
			} else if (tagName == 'SELECT') {
				disableDDLFn(this, isReadOnly);
			}
		});
	};
})(jQuery);

function getTopFrameThisDomain() {
    var topThisDomain = window; // initialize to current frame.

    try {
        // Attempt to use top frame. Will throw if top belongs to a different domain.
        var topLoc = top.location.href;
        topThisDomain = top;
    } catch (e) {
        // Climb up from current frame until parent is null or belongs to another domain (throws).
        while (1) {
            try {
                if (topThisDomain.parent == null) {
                    break;
                }

                var parLoc = topThisDomain.parent.location.href; // throws if parent in different domain.
                topThisDomain = topThisDomain.parent;
            } catch (e) {
                break;
            }
        }
    }

    return topThisDomain;
}

window.topThisDomain = getTopFrameThisDomain();

function promptExplicitSaveCallback(resultMessage, navigationFunction) {
    if (typeof resultMessage === "string") {
        simpleDialog.alert(resultMessage);
    }
    else {
        toastr.options = {
            "positionClass": "toast-bottom-center",
            "timeOut": 2000
        }
        toastr.success("Your changes have been saved.");
        navigationFunction();
    }

    f_displayBlockMask(false);
}

function normalSaveCallback(resultMessage) {
    if (typeof resultMessage === "string") {
        simpleDialog.alert(resultMessage);
        return resultMessage;
    }
}

function promptExplicitSave(isSaveButtonEnabled, wasDirty, saveFunction, navigationFunction) {
    if (wasDirty && isSaveButtonEnabled) {
        simpleDialog.confirm("Changes you made have not been saved.", "Leave This Page?", "SAVE & PROCEED", "DISCARD & PROCEED", null).then(function (isConfirmed) {
            if (isConfirmed === null) { // cancel
                return;
            }
            else if (isConfirmed) {
                f_displayBlockMask(true);
                saveFunction(navigationFunction);
            }
            else { // Discard & Proceed
                clearDirty();
                navigationFunction();
            }
        });
    }
    else if (wasDirty && !isSaveButtonEnabled) {
        var errorMessage = saveFunction();
        if (typeof errorMessage !== "string") {
            navigationFunction();
        }
    }
    else {
        navigationFunction();
    }
}
var _postGetAllFormValuesCallbacks = [];
var _postRefreshCalculationCallbacks = [];
function registerPostGetAllFormValuesCallback(funcToRegister) {
    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (_postGetAllFormValuesCallbacks[i] == funcToRegister)
            return false;
    }
    _postGetAllFormValuesCallbacks.push(funcToRegister);
    return true;
}
function registerPostRefreshCalculationCallback(funcToRegister) {
    for (var i = 0; i < _postRefreshCalculationCallbacks.length; i++) {
        if (_postRefreshCalculationCallbacks[i] == funcToRegister)
            return false;
    }
    _postRefreshCalculationCallbacks.push(funcToRegister);
    return true;
}
function f_getWindowDimensions(currentWindow) {
    var $doc = jQuery(currentWindow.document.documentElement);
    var $body = jQuery(currentWindow.document.body);
    var $win = jQuery(currentWindow);
    return {
        height: currentWindow.innerHeight || $body.innerHeight(),
        width: currentWindow.innerWidth || $body.innerWidth(),
        left: ($doc.scrollLeft() || $body.scrollLeft()),
        top: ($doc.scrollTop() || $body.scrollTop())
    };
}

function addTrimmingToAspRegExValidator(validator) {
    validator.evaluationfunction = TrimmingRegularExpressionValidatorEvaluateIsValid;
}

function TrimmingRegularExpressionValidatorEvaluateIsValid(validator) {
    var $control = jQuery("#" + validator.controltovalidate);
    $control.val(jQuery.trim($control.val()));

    return RegularExpressionValidatorEvaluateIsValid(validator);
}

//show block mask when calculating. Being used in inc\src\TPOPortalClosingCosts\index.ts
function toggleBlockMask(isShow, id){
	id = id || 'temp_block_mask';
	if(isShow){
		var d = jQuery('<div></div>');
	    jQuery("body").after(d[0]);
	    d[0].id = id;
	    d.css({
	        position:'absolute',
	        top: jQuery("body").position().top, 
	        left:0,
	        zIndex:1000});
	    d.height(jQuery(document).height());
	    d.width('100%');	
	}
	else{
		jQuery('#'+id).remove();
	}
	
}
