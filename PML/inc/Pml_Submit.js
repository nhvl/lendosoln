﻿
if (typeof $j != 'undefined')
{
    $j(document).on("change", "input, select", maskResults);
}

var IsPricing2nd = false;
var isCurrentlyPricing = false;

function maskResults() {
    $j("#ResultsContainer").toggleClass("mask", true);
}

function requestPricing(extraArgs) {
    $j("#LoadingMessage").show();

    var args = {
        loanid : ML.sLId,
        sLienQualifyModeT: 0, // By default, this value corresponds to the ThisLoan enum (for first liens).  Override it with the extra args if needed.
        RenderResultModeT: ML.RenderResultModeT,
    };

    if (typeof (extraArgs) != "undefined") {
        $j.extend(args, extraArgs);
    }

    if (typeof (PML) != "undefined" && PML != null) {
        PML.setSessionStorage("ClientPriceTiming_BeforeSubmit", (new Date()).getTime());
    }

    isCurrentlyPricing = true;
    var result = gService.main.acall(IsPricing2nd ? "GetResult_SubmitUnderwriting" : "GetResult_SubmitUnderwritingByGrouping", args, displayResultsIfAvailable);
}

function displayResultsIfAvailable(result) {
    if(!result.error)
    {
        if (typeof (result.value["ErrorMessage"]) != "undefined") {
            displayError(result);
        }
        else {
            if (result.value["IsAvailable"] == "1") {
                displayResults(result.value.RequestID);
            }
        }
    }
    else {
        displayError(result);
    }

    $j("#ResultsContainer").show();
    $j("#LoadingMessage").hide();

    if (typeof(PML) != "undefined" && typeof(PML.triggerPricingDone) != "undefined") {
        PML.triggerPricingDone();
    }
}

function displayResults(requestId) {
    var args = {
        CurrentRequestId: requestId,
        LoanId: ML.sLId,
        IsCheckEligibility: false,
        PricingOption: "PML",
        GetResultsUsing: "0",
        sProdFilterDisplayrateMerge: $j("#sProdFilterDisplayrateMerge").prop("checked"),
        IsPricing2ndLoan: IsPricing2nd,
        mlJson : JSON.stringify(ML)
    };

    if (IsPricing2nd && firstRate)
    {
        args["FirstRate"] = firstRate.Rate;
        args["FirstTemplateId"] = firstRate.LpTemplateId;
    }
    
    if (typeof (PML) != "undefined" && PML != null) {
        PML.setSessionStorage("ClientPriceTiming_RequestId", requestId);
    }

    var pricingResult = gService.main.call('GetResults', args);
    displayResultsFromObject(pricingResult.value)
}

function displayResultsFromObject(pricingResult) {

    if (!pricingResult.error) {
        var renderResultStart = (new Date()).getTime();

        IsPricing2nd = ConvertBool(pricingResult.IsPricing2ndLoan);

        ML.Is8020PricingScenario = ConvertBool(pricingResult.Is8020PricingScenario);

        var pricingSettings = {
            showQm: ML.showQm,
            IsDebugColumns: ML.IsDebugColumns,
            IsBreakEvenMonths: ML.IsBreakEvenMonths,
            DenialReasons: JSON.parse(pricingResult.DenialReasons),
            RegisterFailedConditionType: pricingResult.RegisterFailedConditionType,
            RegisterDenialReasons: JSON.parse(pricingResult.RegisterDenialReasons),
            LockFailedConditionType: pricingResult.LockFailedConditionType,
            LockDenialReasons: JSON.parse(pricingResult.LockDenialReasons),
            isCheckEligibility: false,
            sProdFilterDisplayrateMerge: ML.IsPml || $j("#sProdFilterDisplayrateMerge").prop("checked"),
            IsPml: true,
            HasArmFilter: $j("#sProdFilterFinMeth7YrsArm:checked, #sProdFilterFinMeth3YrsArm:checked, #sProdFilterFinMeth10YrsArm:checked, #sProdFilterFinMeth5YrsArm:checked").length > 0,
            Has2ndLien: $j("#bYes2ndFinancing").prop("checked"),
            DisablePinLinks: ML.DisablePinLinks,
            IsAllowRequestingRateLock: ML.IsAllowRequestingRateLock,
            IsAllowRegisterRateLock: ML.IsAllowRegisterRateLock,

            show2ndLienLink: ConvertBool(pricingResult.show2ndLienLink),
            showSelectLink: ConvertBool(pricingResult.showSelectLink),

            showLockRateLink: ConvertBool(pricingResult.showLockRateLink),
            showLockUnavailable: ConvertBool(pricingResult.showLockUnavailable),
            showRegisterLoanLink: ConvertBool(pricingResult.showRegisterLoanLink),

            showLockUnavailableExpired: ConvertBool(pricingResult.showLockUnavailableExpired),
            showRegisterLoanLinkExpired: ConvertBool(pricingResult.showRegisterLoanLinkExpired),

            IsPricing2ndLoan: ConvertBool(pricingResult.IsPricing2ndLoan),
            Is8020PricingScenario : ConvertBool(pricingResult.Is8020PricingScenario),

            ShowPrice: ConvertBool(pricingResult.showPrice),
            IsTotalIncomeZero: ConvertBool(pricingResult.IsTotalIncomeZero),

            IsAutoLock: ML.IsAutoLock,
            CanSubmitIneligible: ML.CanSubmitIneligible,

            HasDuplicate: ML.HasDuplicate,
            IsManualSubmissionAllowed: ML.IsManualSubmissionAllowed,

            EnableRenovationLoanSupport: ConvertBool(ML.EnableRenovationLoanSupport),
            sIsRenovationLoan: $j("#sIsRenovationLoan").prop("checked"),
            IsUlad2019: ML.IsUlad2019
        };

        pricingMode = pricingResult.pricingMode;
        updateResults(pricingSettings, ML.sDisclosureRegulationT, pricingResult["PricingResults"]);
        isCurrentlyPricing = false;

        if (typeof (PML) != "undefined" && PML != null) {
            PML.setSessionStorage("ClientPriceTiming_RenderResultStart", renderResultStart);
            PML.setSessionStorage("ClientPriceTiming_RenderResultEnd", (new Date()).getTime());
            PML.setSessionStorage("ClientPriceTiming_NavTiming", JSON.stringify(performance.timing));
        }
    }
    else {
        displayError(pricingResult);
    }
}

function ConvertBool(val) {
    return val == "True" || val == true;
}


function f_allowClick() {
    //Check if a popup is already open.  If so, don't allow the click.
    if (typeof (PML) != "undefined") {
        return PML._popupWindow == null || PML._popupWindow.closed == true;
    }

    return true;
}

function addFirstArgs(args) {
    args["1stRate"] = firstRate.Rate;
    args["1stPoint"] = firstRate.Point;
    args["1stMPmt"] = firstRate.Payment;
    args["1stProductId"] = firstRate.LpTemplateId;
    args["1stRawId"] = firstRate.RateOptionId;
    args["1stBlockSubmit"] = firstRate.IsBlockedRateLockSubmission;
    args["1stBlockSubmitMsg"] = firstRate.RateLockSubmissionUserWarningMessage;
    args["1stUniqueChecksum"] = firstRate.UniqueChecksum;
    args["version"] = firstRate.Version;
}

function submitRate(rateOption, program, group, isLocking, isSkip, isManual, isPricing2ndLoan) {

    // Check to see if a popup is opened.  Only allow if all popups are closed.
    if (!f_allowClick()) return false;

    if (ML.HasDuplicate) {
        return false;
    }

    var args = new Object();
    args["loanid"] = ML.sLId;
    args["debugResultStartTicks"] = ML.DebugTimeStart;

    if (!isManual && !isSkip) {
        args["productid"] = rateOption.LpTemplateId;
        args["rate"] = rateOption.Rate;
        args["point"] = rateOption.Point;
        args["apr"] = rateOption.Apr;
        args["margin"] = rateOption.Margin;
        args["payment"] = rateOption.Payment;
        args["bottom"] = rateOption.Dti;
        args["productName"] = rateOption.LpTemplateNm;
        args["uniqueChecksum"] = rateOption.UniqueChecksum;
        args["teaserrate"] = rateOption.TeaserRate;
        args["qrate"] = rateOption.QualRate == '' ? rateOption.Rate : rateOption.QualRate;
        args["qualPmt"] = rateOption.QualPmt;
        args["RawId"] = rateOption.RateOptionId;
        args["blocksubmit"] = rateOption.IsBlockedRateLockSubmission;
        args["blocksubmitmsg"] = rateOption.RateLockSubmissionUserWarningMessage;
        args["requestratelock"] = isLocking;
        args["parId"] = group.ParRate;

        args["Version"] = rateOption.Version;
        args["Has2ndLoan"] = ML.Is8020PricingScenario;
    }

    if (isPricing2ndLoan) {
        if (isManual) {
            args["manual2nd"] = true;
            args["Has2ndLoan"] = ML.Is8020PricingScenario;
        }
        else if(isSkip) {

            args["Version"] = ML.Version;
            args["skip2nd"] = true;
        }

        addFirstArgs(args);
    }

    if (ML.Is8020PricingScenario && !isPricing2ndLoan) {
        f_qualify2ndLoan(rateOption.LpTemplateId, rateOption.Rate, rateOption.Point, rateOption.Payment, rateOption.RateOptionId, rateOption.Version, rateOption.IsBlockedRateLockSubmission, rateOption.RateLockSubmissionUserWarningMessage, rateOption.UniqueChecksum);
    } else if (typeof(ML.RegisterAndRateLockBlockingMessage) != "undefined" && ML.RegisterAndRateLockBlockingMessage != "") {
        alert(ML.RegisterAndRateLockBlockingMessage);
        return false;
    } else if (ML.UnableToSubmitWithoutCredit) {
        alert(ML.LPE_CannotSubmitWithoutCredit);
        return false;
    } else {
        var queryString = "";
        for (var o in args) {
            queryString += o + "=" + encodeURIComponent(args[o]) + "&";
        }

        queryString += "isBeta=true";

        if (ML.IsRunModeOriginalProductOnly) {
            var expiredRateDialog = gVirtualRoot + "/Main/LockUnavailable.aspx";
            var expiredDialogTitle = "Lock Unavailable";
        } else {
            var expiredRateDialog = gVirtualRoot + "/Main/ConfirmationPage.aspx";
            var expiredDialogTitle = ML.ConfirmationTitle;
        }

        var dialogUrl = isPricing2ndLoan ||  !rateOption.IsBlockedRateLockSubmission ? gVirtualRoot + "/Main/ConfirmationPage.aspx" : expiredRateDialog;
        var winTitle = isPricing2ndLoan || !rateOption.IsBlockedRateLockSubmission ? ML.ConfirmationTitle : expiredDialogTitle;
        winTitle = winTitle.replace(' ', '_');

        var newWindow = window.open(dialogUrl + "?" + queryString, winTitle, 'width=500px,height=300px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no');

        if(isPricing2ndLoan) {
            popupWindow = newWindow;
        }
        else {

            PML._popupWindow = newWindow;
        }

        return false;
    }
}

function displayError(result) {
    if (result.error) {
        alert(result.UserMessage);
    }
    else if (typeof (result.value.ErrorMessage) != "undefined") {
        alert(result.value.ErrorMessage);
    }
}

function f_qualify2ndLoan(productID, rate, point, payment, rawId, version, blockSubmit, blockSubmitMsg, uniqueChecksum) {
    IsPricing2nd = true;
    var args = {
        sLienQualifyModeT: 1,
    };

    addFirstArgs(args);

    requestPricing(args);
}

function f_onResultClose() {
    PML.goBack();
}