﻿function CheckPopup()
{
    //By: Long Nguyen
    //April-1-2015
    //Check that login page is not in a popup window.
    //https://lqbopm/default.asp?199056
    //http://forums.asp.net/t/1423807.aspx?how+to+check+whether+browser+window+is+a+popup+window+or+not
    //
    // 5/16/2015 - Revert this fix. dd. Because when the login is embedded in other frame it faile. We allow client to embedded the login page.
    // return; // NO-OP
    //       console.log("Opener: "+ (window.opener?window.opener.location.pathname:"null"));
    //       console.log("Top: "+ (window.top?window.top.location.pathname:"null"));
    //       console.log("Parent: "+ (window.parent?window.parent.location.pathname:"null"));

    if(window.opener != null)
    {
        window.opener.location.reload(true);
        window.close();
    }
    else
    {
        if(inIframe()) {
            try {
                window.parent.location.reload(true);
                window.close();
            } catch (e) {
            }
        }
    }
};

//Check that login page is not in an iframe
//http://stackoverflow.com/questions/326069/how-to-identify-if-a-webpage-is-being-loaded-inside-an-iframe-or-directly-into-t
function inIframe () {
    try {
        return (window.parent.location.pathname.toLowerCase().match(/main\/baseframe.aspx$/)||
                window.parent.location.pathname.toLowerCase().match(/main\/pipeline.aspx$/));
    } catch (e) {
        return true;
    }
};

/******************************************************/
function RedirectToResetPassword()
{
    var url = "PasswordReset/RequestPasswordReset.aspx";
    if (typeof ML.LenderPmlSiteID !== 'undefined') {
        url += "?LenderPmlSiteId=" + ML.LenderPmlSiteID;
    }
    window.location = url;
}
	
var browserValidators = 
[
    function(browser)
    {
        return typeof (browser.msie) !== "undefined" && browser.majorVersion >= 7;
    },
	    
    function(browser)
    {
        return browser.mozilla && browser.majorVersion >= 8;
    },
	    
    function(browser)
    {
        return browser.webkit &&  //Both Chrome and Safari are webkit (browser.safari is equivalent to browser.webkit)
               browser.majorVersion >= 4; //Safari OSX is at version 5, Safari iPad is version 500ish
        //and we generally want to let it in
        //jQuery doesn't support Chrome for browser detection,
        //so we'll just let it in (it reports a version number of 500ish too)
	              
	                                        
    },
    function(browser)
    {
        return browser.opera && browser.majorVersion >= 10;
    }
];

// 3/17/2016 BS - Case 873635. Check whether IE is 11 or newer. It can be removed after June release when we stop supporting old IE
var browserIe11Validators =
[
    function(browser)
    {
        return (typeof (browser.msie) !== "undefined" && browser.majorVersion >= 11) || browser.mozilla || browser.webkit || browser.opera;
    }
];
	
jQuery(function($){
    //If PML is unavailable, there's no point in doing any browser blocking.
    var unavailable = $('#UnavailableReason');
    if(unavailable.length)
    {
        disableLogin(unavailable.val());
        return;
    }

    CheckPopup();
    //Deprecated, but hey we're doing browser-based blocking so it's appropriate.
    var browser = $.browser;
    //We only care about major versions, and parseInt will strip off everything past the first dot.
    browser.majorVersion = parseInt(browser.version);
    var validBrowser = false;
    $.each(browserValidators, function(idx, fxn) { validBrowser = validBrowser || fxn(browser);});
	    
    if(!validBrowser)
    {
        disableLogin(ML.CompatNotification);
    }

    function disableLogin(msg)
    {
        $('#m_ValidBrowser').val(false);
        var label = $('#MessageLabel');
        label.html(msg);
        label.parent().css({
            border: 'black 1px solid',
            color: 'red',
            fontWeight: 'bold',
            backgroundColor: 'gainsboro',
            width: '400px',
            margin: '10px',
            padding: '5px'
        });

        var loginButton = $('#m_login');
        var loginFields = $('#LoginName, #Password');
        if(loginButton.length)
            loginButton.hide();
        if(loginFields.length) {
            loginFields.attr('disabled', 'disabled');
            loginFields.css({backgroundColor: "gainsboro"});
        }
    }

        	    
    // 3/17/2016 - Case 873635. If IE is older than 11, display warning message.
    var ie11OrNewerBrowser = false;
    $.each(browserIe11Validators, function(idx, fxn) { ie11OrNewerBrowser = ie11OrNewerBrowser || fxn(browser);});

    if (!ie11OrNewerBrowser)
    {
        displayWarning(ML.CompatNotification);
    }

    function displayWarning(msg)
    {
        var label = $('#MessageLabel');
        label.html(msg);
        label.parent().css({
            border: 'black 1px solid',
            color: 'red',
            fontWeight: 'bold',
            backgroundColor: 'gainsboro',
            width: '400px',
            margin: '10px',
            padding: '5px'
        });
    }
    $('#LoginName').focus();
});