﻿angular.module('LqbForms', [])

.component('lqbFormInputRow', {
    templateUrl: function (LqbUrl) {
        return LqbUrl.angularModules + '/lqbFormInputRow.html';
    },
    controller: function ()
    {
        var ctrl = this;
        ctrl.$onInit = function () {
            this.type = this.type || 'text';
            this.useInvalid = this.useInvalid || 'true';
        }
    },
    bindings: {
        inputId: '@',
        inputLabel: '@',
        type: '@',
        ngModel: '=',
        ngDisabled: '<',
        ngRequired: '=',
        useInvalid: '@',
        showInvalid: '<',
        optionsCollection: '<',
        groupBy: '@'
    }
})

.component('lqbAusCraOptions', {
    controller: function ($scope)
    {
        var ctrl = this;
        ctrl.$onInit = function () {
            $scope.creditInfo = ctrl.creditInfo;
            $scope.creditReportOptions = ctrl.creditReportOptions;
            $scope.creditProviders = ctrl.creditProviders;
            $scope.CredentialsRequired = function () {
                return ctrl.creditInfo.CreditReportOption === ctrl.creditReportOptions.OrderNew.value || ctrl.creditInfo.CreditReportOption === ctrl.creditReportOptions.Reissue.value
                    || (ctrl.aus === 'LPA' && ctrl.creditInfo.CreditReportOption === ctrl.creditReportOptions.UsePrevious.value);
            };
            if ($scope.creditInfo.HasCraAutoLogin) {
                $scope.creditInfo.CraUserId = '*****';
            }
            if ($scope.creditInfo.HasCraAutoLogin && $scope.creditInfo.HasCraAutoPassword) {
                $scope.creditInfo.CraPassword = '*****';
            }
            $scope.ApplicationName = function (application) {
                return application.BorrowerName + (application.CoborrowerName ? ' & ' + application.CoborrowerName : '');
            };
            $scope.CraReorderIdRequired = function(creditOption)
            {
                if (creditOption !== $scope.creditReportOptions.UsePrevious.value) {
                    return true;
                }
                for (var i = 0; i < $scope.creditInfo.Applications.length; i++) {
                    if ($scope.creditInfo.Applications[i].CraResubmitId != null && $scope.creditInfo.Applications[i].CraResubmitId != '') {
                        return false;
                    }
                }
                return true;
            }
        };
    },
    templateUrl: function (LqbUrl) {
        return LqbUrl.angularModules + '/LqbAusCraOptions.html';
    },
    bindings: {
        creditInfo: '=',
        creditReportOptions: '<',
        creditProviders: '<',
        ngDisabled: '<',
        aus: '@'
    }
})

/**
 * For accessing static URL parts like virtualroot.
 */
.constant('LqbUrl', {
        root: gVirtualRoot,
        seamlessFolder: typeof seamlessFolder === 'undefined' ? gVirtualRoot : seamlessFolder,
        angularModules: gVirtualRoot + '/inc/angularModules',
        getSearchObject: function(href) {
            var args = href.substring(href.lastIndexOf('?') + 1).split('&');
            var search = {};
            for (var i = 0; i < args.length; i++) {
                var matches = args[i].match(/([^=]*)=(.*)/);
                if (matches && matches[1])
                    search[matches[1]] = matches[2];
            }
            return search;
        }
    }
)

/**
 * Provide injectable access to the global 'ML' object.
 */
.factory('ML', function () { return window.ML; })

/**
 * A service for accessing the parent window from within an LQBPopup iframe.
 */
.service('LqbPopupService', function () {
    this.popup = window.parent.LQBPopup;

    /**
     * Navigates the parent window, so that a specific data field can be highlighted for editing.
     * @param {string} newAddress - The new address to navigate to (embedded PML navigation is a bit more complex, so it uses highUrl as well).
     * @param {int} highUrl - The page URL option (see DataAccess.E_UrlOption) for the loanapp.aspx page to navigate to. Required for Embedded PML.
     */
    this.navigateParent = function (newAddress, highUrl) {
        if (window.parent.pmlData && window.parent.pmlData.IsEmbeddedPML) {
            var urlParams = '';
            if (newAddress.indexOf('?') !== -1) {
                urlParams = '&' + newAddress.substring(newAddress.indexOf('?') + 1);
                // Multiple loanids causes system errors.
                urlParams = urlParams.replace(/&?loanid=[a-fA-F0-9-]+/i, '');
            }
            if (!!highUrl) {
                urlParams = urlParams + '&highUrl=' + highUrl;
            }
            window.parent.goBack(urlParams);
        }
        else {
            if (!redirectToUladIfUladPage(newAddress, true)) {
                window.parent.location = newAddress;
            }
        }
    };

    this.refreshParent = function () {
        window.parent.location.reload();
    };

    this.getArguments = function() {
        if (window.parent.LQBPopup) {
            return window.parent.LQBPopup.GetArguments();
        }
    };
})

/**
 * Provide injectable and configurable access to the global 'gService' object.
 */
.provider('gService', function gServiceProvider() {
    this.register = window.gService.register;

    this.$get = function() {
        return window.gService;
    };
})

.service('InternetExplorer', function () {
    /* IE doesn't support <a download="blah.txt">link</a>, so this is the way to download a js blob.
     * If IE ever does add support, this might cause double downloads. https://caniuse.com/#feat=download
     */
    this.msSaveOrOpenBlob = function (blob, defaultName) {
        // No-op if not IE
        if (navigator && navigator.msSaveOrOpenBlob) {
            navigator.msSaveOrOpenBlob(blob, defaultName);
        }
    };
})

/**
 * A parser for phone numbers in the general North American Numbering Plan format.
 * Ex: 
 *  +1 301 555 0100
 *  866.555.0186x0888
 *  (866) 555-0186 Ext. 0888
 *  8665550186#0888
 * Very permissive. Assumes that the number is generally in NA phone number 
 * format and strips out non-digit characters for normalization. Does not validate the phone number.
 *
 * @param {string} input The phone number string to parse.
 */
.factory('NAPhoneNumber', function () {
    return function(input)
    {
        if (typeof input !== 'string')
        {
            this.isPhoneNumber = false;
            return;
        }

        var regex = /^[^\d+]*(1|\+1)?[^\d]*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4})([^\d]+(\d+))?([^\d].*)?$/;
        var matches = input.match(regex);
        if (matches === null)
        {
            this.isPhoneNumber = false;
            return;
        }
        else
        {
            this.isPhoneNumber = true;
            this.countryCode = matches[1];
            this.areaCode = matches[2];
            this.exchangeCode = matches[3];
            this.subscriberNumber = matches[4];
            if (matches[5]) {
                this.extension = matches[6];
            }
        }

        this.toString = function(includeCountryCode)
        {
            var phoneNumber = '(' + this.areaCode + ') ' + this.exchangeCode + '-' + this.subscriberNumber;
            if ((typeof includeCountryCode !== 'undefined' && includeCountryCode) || (typeof includeCountryCode !== 'undefined' && this.countryCode)) {
                phoneNumber = '+1 ' + phoneNumber;
            }
            if (this.extension) {
                phoneNumber += ' x' + this.extension;
            }
            return phoneNumber;
        }
    };
})

.filter('phone', ['NAPhoneNumber', function (NAPhoneNumber) {
    return function (input) {
        var parsedPhone = new NAPhoneNumber(input);
        if (parsedPhone.isPhoneNumber) {
            return parsedPhone.toString();
        }
        else {
            return input;
        }
    }
}])

.run(function ($templateRequest, LqbUrl) {
    // Pre-load the lqbFormInputRow template into the cache for quicker page loads.
    $templateRequest(LqbUrl.angularModules + '/lqbFormInputRow.html');
});