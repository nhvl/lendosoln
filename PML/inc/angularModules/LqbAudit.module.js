﻿angular.module('LqbAudit', ['LqbForms'])

.component('lqbAudit', {
    controllerAs: 'audit',
    controller: function ($scope, LqbPopupService, LqbUrl) {
        $scope.navigateParent = LqbPopupService.navigateParent;
        $scope.virtualRoot = LqbUrl.root;
    },
    templateUrl: function (LqbUrl) {
        return LqbUrl.angularModules + '/LqbAudit.html';
    },
    bindings: {
        auditResult: '<',
        title: '@pageTitle'
    },
    transclude: {
        'success': '?success',
        'failure': '?failure'
    }
})

.run(function ($templateRequest, LqbUrl) {
    $templateRequest(LqbUrl.angularModules + '/LqbAudit.html');
});