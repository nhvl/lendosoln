﻿var AssetType=[
          {value: 0, text:"Auto"                                            }
        , {value: 1, text:"Bonds"                                            }
        , {value:18, text:"Bridge Loan"                                      }
        , {value:14, text:"Certificate Of Deposit"                           }
        , {value: 3, text:"Checking"                                         }
        , {value: 4, text:"Gift Funds"                                       }
        , {value:13, text:"Gift Of Equity"                                   }
        , {value:15, text:"Money Market Fund"                                }
        , {value:16, text:"Mutual Funds"                                     }
        , {value:-1, text:"Real Estate"                                      }
        , {value: 7, text:"Savings"                                          }
        , {value:17, text:"Secured Borrowed Funds"                           }
        , {value: 8, text:"Stocks"                                           }
        , {value:19, text:"Trust Funds"                                      }
        , {value:12, text:"Pending Net Sale Proceeds"                        }
        , {value: 9, text:"Other Non-liquid Asset (furniture, jewelry, etc.)"}
        , {value:11, text:"Other Liquid Asset (other bank accounts, etc.)"   }
    ];
var AssetPropertyType=[
        {value: 0, text:"Single Family Residence"     }
        , {value: 8, text:"2-4 unit property"           }
        , {value:90, text:"Commercial (Non-Residential)"}
        , {value:91, text:"Commercial (Residential)"    }
        , {value: 2, text:"Condo"                       }
        , {value:92, text:"Cooperative Housing"         }
        , {value:93, text:"Farm"                        }
        , {value:94, text:"Land"                        }
        , {value:95, text:"Mixed Use"                   }
        , {value:96, text:"Mobil"                       }
        , {value:97, text:"Townhouse"                   }
        , {value:98, text:"Other"                       }
    ];
var PropertyStatus=[
        {value:0, text:"Retained"    }
        ,{value:1, text:"Sold"        }
        ,{value:2, text:"Pending Sale"}
        ,{value:3, text:"Rental"      }
    ];