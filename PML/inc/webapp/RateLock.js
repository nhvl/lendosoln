﻿const ReLockPopupWidth = 900;
const FloatDownPopupWidth = 980;
const ExtendRateLockWithCalendarDaysPopupWidth = 1020;

var rateLock = angular.module('RateLock', ['LoanHeader']);

rateLock.controller('RateLockController', ['$scope', function ($scope) {
    $scope.vm = null;

    $scope.displayLenderCompPlan = function () {
        LQBPopup.ShowElement($('#lenderCompDiv'), { height: '100%', hideCloseButton: true, headerText:$scope.vm.lenderCompPlan.details, draggable:false });
    }

    $scope.downloadLockConfirmation = function () {
        var ticks = new Date().getTime();
        var url = VRoot + "/pdf/RateLockConfirmation.aspx?loanid=" + $scope.vm.sLId + "&crack=" + ticks;
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
    }

    $scope.renegotiateRate = function () {
        $scope.takeRateAction('floatdown', FloatDownPopupWidth);
    }

    $scope.extendRate = function () {
        if ($scope.vm.showCalendarDays) {
            $scope.takeRateAction('extend', ExtendRateLockWithCalendarDaysPopupWidth);
        }
        else {
            $scope.takeRateAction('extend');
        }
    }

    $scope.relockRate = function () {
        $scope.takeRateAction('relock', ReLockPopupWidth);
    }

    $scope.takeRateAction = function (action, width) {
        var url = VRoot + '/main/LockRate.aspx?hideCancel=t&loanid=' + $scope.vm.sLId + '&action=' + action;

        var settings = {
            hideCloseButton: false,
            modifyOverflow: false,
            draggable:false,
            closeText:'Cancel',
            onReturn: function () { window.location.reload();}
        };

        if (width) {
            settings.width = width;
        }

        LQBPopup.Show(url, settings);
    }

    const textAreaPadding = 2;
    $scope.autoExpand = function(e) {
        var element = typeof e === 'object' ? e.target : document.getElementById(e);
        var padding = element.offsetHeight - element.clientHeight;
        var scrollHeight = element.scrollHeight + padding;
        var loanStatusPanel = $("#loanStatus")[0];
        
        loanStatusPanel.style.height =  (loanStatusPanel.offsetHeight + scrollHeight - element.offsetHeight) + "px";

        element.style.height =  scrollHeight + "px";
        $("#" + e).addClass("textarea-fixed-size");
    };

    $scope.init = function () {
        $scope.vm = angular.fromJson(RateLockViewModel);
        window.setTimeout(function(){$scope.autoExpand('sSubmitN');});
    }

    $scope.init();
}]);

rateLock.directive('lenderCompTable', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: { plan: '=', },
        templateUrl: VRoot + '/webapp/Partials/RateLock/LenderCompTable.html'
    }
});

function ratePopupCloseCallback(isCancel) {
    if (isCancel) {
        window.LQBPopup.Hide();
    }
    else {
        // Return expects an argument array to be passed. Since there
        // are no arguments for this call, we'll pass in an empty object.
        window.LQBPopup.Return({});
    }
}

function ratePopupAlert(message) {
    simpleDialog.alert(message);
}

function setupRateLockApp() {
    // We have two separate "mini" angular applications: the loan navigation header
    // and the main page. For Angular to work correctly with both applications, we
    // need to bootstrap the module for the main page. Angular will take care of 
    // the loan navigation header since it is the first app defined on the page.
    angular.bootstrap(document.getElementById('RateLockDiv'), ['RateLock']);
    TPOStyleUnification.Components();
}

// Run the setup function once the page is ready so the 
// root div is defined on the page.
$(document).ready(setupRateLockApp);
