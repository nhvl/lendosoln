﻿function showHideCashOut() {
	var sRefPurposeClientID = 'Loan1003pg1_sRefPurpose';
	var sLT = document.getElementById('Loan1003pg1_sLT');
	var sRefPurpose = document.getElementById(sRefPurposeClientID);

	// Get No Cash-Out Item
	var item = null;
	var tbl = document.getElementById(sRefPurposeClientID + '_cbl');
	for (var i = 0; i < tbl.rows.length; i++) {
	    var text = tbl.rows[i].cells[0].innerText || tbl.rows[i].cells[0].textContent;
	    if (text.toUpperCase() === 'NO CASH-OUT RATE/TERM') {
			item = tbl.rows[i];
			break;
		}
	}

	// Show/Hide Item
	if (item && sLT.options[sLT.selectedIndex].text.toUpperCase() === 'CONVENTIONAL'
		&& sRefPurpose.value.toUpperCase() !== 'NO CASH-OUT RATE/TERM') {
		item.style.visibility = 'hidden';
	} else {
		item.style.visibility = 'visible';
	}

	return false;
}

function doAfterDateFormat(object) {
    if (object.target.id == ('Loan1003pg1' + "_aBDob")) {
        calculateAge(object.target, "aBAge");
    }
    else if (object.target.id == ('Loan1003pg1' + "_aCDob")) {
    calculateAge(object.target, "aCAge");
    }

}

function calculateAge(dob, age) {
    var args = new Object();
    args["dob"] = dob.value;
    args["age"] = document.getElementById('Loan1003pg1' + "_" + age).value;
    args["_ClientID"] = 'Loan1003pg1';
    var result = gService.Loan1003Service.call('Loan1003pg1' + "_CalculateAge", args);
    if (!result.error) {
        if (result.value["age"] != null) {
            document.getElementById('Loan1003pg1' + "_" + age).value = result.value["age"];
        }
    }
}


function on_sDownPmtPc_change() {
	var a = parseMoneyFloat('$3,000,000.00');
	var p = parseMoneyFloat(document.getElementById('Loan1003pg1_sPurchPrice').value);
	var sLPurposeTValue = document.getElementById('Loan1003pg1_sLPurposeT').value;
	if (sLPurposeTValue == '3'
		|| sLPurposeTValue == '4') {
		p = p + parseMoneyFloat(document.getElementById('Loan1003pg1_sLandCost').value);
	}

	var d = parseFloat(document.getElementById('Loan1003pg1_sDownPmtPc').value);

	var minPurchaseOrAppraisal = p;
	if (a > 0 && a < p) {
		minPurchaseOrAppraisal = a;
	}

	var e = ((d / 100.0) * minPurchaseOrAppraisal) + 0.005;
	var ctl = document.getElementById('Loan1003pg1_sEquity');
	ctl.value = e;
	format_money(ctl);
	updateDirtyBit();
	refreshCalculation();
}

function syncMailingAddress() {
    var ddl = document.getElementById('Loan1003pg1_aBAddrMailSourceT');
    var source = ddl.options[ddl.selectedIndex].value;
    switch(source)
    {
        case "0":   // PresentAddress
            document.getElementById('Loan1003pg1_aBAddrMail').value = document.getElementById('Loan1003pg1_aBAddr').value;
		    document.getElementById('Loan1003pg1_aBCityMail').value = document.getElementById('Loan1003pg1_aBCity').value;
		    document.getElementById('Loan1003pg1_aBStateMail').value = document.getElementById('Loan1003pg1_aBState').value;
		    document.getElementById('Loan1003pg1_aBZipMail').value = document.getElementById('Loan1003pg1_aBZip').value;
            break;
        case "1":   // SubjectPropertyAddress
            document.getElementById('Loan1003pg1_aBAddrMail').value = document.getElementById('Loan1003pg1_sSpAddr').value;
		    document.getElementById('Loan1003pg1_aBCityMail').value = document.getElementById('Loan1003pg1_sSpCity').value;
		    document.getElementById('Loan1003pg1_aBStateMail').value = document.getElementById('Loan1003pg1_sSpState').value;
		    document.getElementById('Loan1003pg1_aBZipMail').value = document.getElementById('Loan1003pg1_sSpZip').value;
            break;
    }

    ddl = document.getElementById('Loan1003pg1_aCAddrMailSourceT');
    source = ddl.options[ddl.selectedIndex].value;
    switch(source)
    {
        case "0":   // PresentAddress
            document.getElementById('Loan1003pg1_aCAddrMail').value = document.getElementById('Loan1003pg1_aCAddr').value;
	        document.getElementById('Loan1003pg1_aCCityMail').value = document.getElementById('Loan1003pg1_aCCity').value;
	        document.getElementById('Loan1003pg1_aCStateMail').value = document.getElementById('Loan1003pg1_aCState').value;
	        document.getElementById('Loan1003pg1_aCZipMail').value = document.getElementById('Loan1003pg1_aCZip').value;
            break;
        case "1":   // SubjectPropertyAddress
            document.getElementById('Loan1003pg1_aCAddrMail').value = document.getElementById('Loan1003pg1_sSpAddr').value;
            document.getElementById('Loan1003pg1_aCCityMail').value = document.getElementById('Loan1003pg1_sSpCity').value;
            document.getElementById('Loan1003pg1_aCStateMail').value = document.getElementById('Loan1003pg1_sSpState').value;
            document.getElementById('Loan1003pg1_aCZipMail').value = document.getElementById('Loan1003pg1_sSpZip').value;
            break;
    }

	updateMailingAddressFields();
}

function updateMailingAddressFields() {
    var ddl = document.getElementById('Loan1003pg1_aBAddrMailSourceT');
	var b = ddl.options[ddl.selectedIndex].value != "2";
	document.getElementById('Loan1003pg1_aBAddrMail').readOnly = b;
	document.getElementById('Loan1003pg1_aBCityMail').readOnly = b;
	disableDDL(document.getElementById('Loan1003pg1_aBStateMail'), b);
	document.getElementById('Loan1003pg1_aBZipMail').readOnly = b;

    ddl = document.getElementById('Loan1003pg1_aCAddrMailSourceT');
	b = ddl.options[ddl.selectedIndex].value != "2";
	document.getElementById('Loan1003pg1_aCAddrMail').readOnly = b;
	document.getElementById('Loan1003pg1_aCCityMail').readOnly = b;
	disableDDL(document.getElementById('Loan1003pg1_aCStateMail'), b);
	document.getElementById('Loan1003pg1_aCZipMail').readOnly = b;
}

function displayOther(control, value, otherField) {
	if (control.value == value) {
		otherField.readOnly = false;
	} else {
		otherField.value = "";
		otherField.readOnly = true;
	}
}

function displayConstruction() {
	var v = document.getElementById('Loan1003pg1_sLPurposeT').value;
	var bConstruction = v == '3' || v == '4';
	var bRefinance = v == '1' || v == '2' || v == '6' || v == '7' || v == '8';
	document.getElementById('Loan1003pg1_sLotAcqYr').readOnly = !bConstruction;
	document.getElementById('Loan1003pg1_sLotOrigC').readOnly = !bConstruction;
	document.getElementById('Loan1003pg1_sLotLien').readOnly = !bConstruction;
	document.getElementById('Loan1003pg1_sLotVal').readOnly = !bConstruction;
	document.getElementById('Loan1003pg1_sLotImprovC').readOnly = !bConstruction;
	document.getElementById('Loan1003pg1_sSpAcqYr').readOnly = !bRefinance;
	document.getElementById('Loan1003pg1_sSpOrigC').readOnly = !bRefinance;
	document.getElementById('Loan1003pg1_sSpLien').readOnly = !bRefinance;
	document.getElementById('Loan1003pg1_sRefPurpose').readOnly = !bRefinance;
	document.getElementById('Loan1003pg1_sSpImprovDesc').readOnly = !bRefinance;
	disableDDL(document.getElementById('Loan1003pg1_sSpImprovTimeFrameT'), !bRefinance);
	document.getElementById('Loan1003pg1_sSpImprovC').readOnly = !bRefinance;


    var colorConst = "";
    var colorRefi = "";

    if(bConstruction)
    {
        colorConst = "";
    }
    if (bRefinance) {
        colorRefi = "";
    }
    document.getElementById('Loan1003pg1_sLotAcqYr').style.backgroundColor = colorConst;
    document.getElementById('Loan1003pg1_sLotOrigC').style.backgroundColor = colorConst;
    document.getElementById('Loan1003pg1_sLotLien').style.backgroundColor = colorConst;
    document.getElementById('Loan1003pg1_sLotVal').style.backgroundColor = colorConst;
    document.getElementById('Loan1003pg1_sLotImprovC').style.backgroundColor = colorConst;
    document.getElementById('Loan1003pg1_sSpAcqYr').style.backgroundColor = colorRefi;
    document.getElementById('Loan1003pg1_sSpOrigC').style.backgroundColor = colorRefi;
    document.getElementById('Loan1003pg1_sSpLien').style.backgroundColor = colorRefi;
    document.getElementById('Loan1003pg1_sRefPurpose').style.backgroundColor = colorRefi;
    document.getElementById('Loan1003pg1_sSpImprovDesc').style.backgroundColor = colorRefi;
    document.getElementById('Loan1003pg1_sSpImprovC').style.backgroundColor = colorRefi;
}

function p1_init() {
	var purpose = document.getElementById('Loan1003pg1_sLPurposeT').value;
	var bPurchase = (purpose == '0'
		|| purpose == '3'
		|| purpose == '4'
	);

	updateMailingAddressFields();

	displayOther(document.getElementById('Loan1003pg1_sLT'), '4', document.getElementById('Loan1003pg1_sLTODesc'));
	displayOther(document.getElementById('Loan1003pg1_sLPurposeT'), '5', document.getElementById('Loan1003pg1_sOLPurposeDesc'));

	displayConstruction();

	document.getElementById('Loan1003pg1_sPurchPrice').readOnly = !bPurchase;
	document.getElementById('Loan1003pg1_sLAmtLckd').disabled = !bPurchase || ML.IsRenovationLoan;

	document.getElementById('Loan1003pg1_sDownPmtPc').readOnly = document.getElementById('Loan1003pg1_sLAmtLckd').checked;
	document.getElementById('Loan1003pg1_sEquity').readOnly = document.getElementById('Loan1003pg1_sLAmtLckd').checked;

	document.getElementById('Loan1003pg1_sLeaseHoldExpireD').readOnly = document.getElementById('Loan1003pg1_sEstateHeldT').selectedIndex == 0;
	document.getElementById('Loan1003pg1_sOccR').readOnly = document.getElementById('Loan1003pg1_sOccRLckd').checked == false;
	lockField(document.getElementById('Loan1003pg1_aTitleNm1Lckd'), 'aTitleNm1');
	lockField(document.getElementById('Loan1003pg1_aTitleNm2Lckd'), 'aTitleNm2');
	lockField(document.getElementById('Loan1003pg1_sLAmtLckd'), 'sLAmt');
	lockField(document.getElementById('Loan1003pg1_sQualIRLckd'), 'sQualIR');
	lockField(document.getElementById('Loan1003pg1_sFinMethodPrintAsOther'), 'sFinMethPrintAsOtherDesc');
	$j("#QualRateCalcLink").prop('disabled', document.getElementById('Loan1003pg1_sQualIRLckd').checked);

	showHideCashOut();

	TPOStyleUnification.Components();
}

function onCopyBorrowerClick() {

	if ('' == document.getElementById('Loan1003pg1_aCLastNm').value)
		document.getElementById('Loan1003pg1_aCLastNm').value = document.getElementById('Loan1003pg1_aBLastNm').value;

	document.getElementById('Loan1003pg1_aCHPhone').value = document.getElementById('Loan1003pg1_aBHPhone').value;
	document.getElementById('Loan1003pg1_aCMaritalStatT').value = document.getElementById('Loan1003pg1_aBMaritalStatT').value;
	document.getElementById('Loan1003pg1_aCAddr').value = document.getElementById('Loan1003pg1_aBAddr').value;
	document.getElementById('Loan1003pg1_aCCity').value = document.getElementById('Loan1003pg1_aBCity').value;
	document.getElementById('Loan1003pg1_aCState').value = document.getElementById('Loan1003pg1_aBState').value;
	document.getElementById('Loan1003pg1_aCZip').value = document.getElementById('Loan1003pg1_aBZip').value;
	document.getElementById('Loan1003pg1_aCAddrT').value = document.getElementById('Loan1003pg1_aBAddrT').value;
	document.getElementById('Loan1003pg1_aCAddrMailSourceT').value = document.getElementById('Loan1003pg1_aBAddrMailSourceT').value;
	document.getElementById('Loan1003pg1_aCAddrMail').value = document.getElementById('Loan1003pg1_aBAddrMail').value;
	document.getElementById('Loan1003pg1_aCCityMail').value = document.getElementById('Loan1003pg1_aBCityMail').value;
	document.getElementById('Loan1003pg1_aCStateMail').value = document.getElementById('Loan1003pg1_aBStateMail').value;
	document.getElementById('Loan1003pg1_aCZipMail').value = document.getElementById('Loan1003pg1_aBZipMail').value;
	document.getElementById('Loan1003pg1_aCAddrYrs').value = document.getElementById('Loan1003pg1_aBAddrYrs').value;
	document.getElementById('Loan1003pg1_aCPrev1Addr').value = document.getElementById('Loan1003pg1_aBPrev1Addr').value;
	document.getElementById('Loan1003pg1_aCPrev1City').value = document.getElementById('Loan1003pg1_aBPrev1City').value;
	document.getElementById('Loan1003pg1_aCPrev1State').value = document.getElementById('Loan1003pg1_aBPrev1State').value;
	document.getElementById('Loan1003pg1_aCPrev1Zip').value = document.getElementById('Loan1003pg1_aBPrev1Zip').value;
	document.getElementById('Loan1003pg1_aCPrev1AddrT').value = document.getElementById('Loan1003pg1_aBPrev1AddrT').value;
	document.getElementById('Loan1003pg1_aCPrev1AddrYrs').value = document.getElementById('Loan1003pg1_aBPrev1AddrYrs').value;
	document.getElementById('Loan1003pg1_aCPrev2Addr').value = document.getElementById('Loan1003pg1_aBPrev2Addr').value;
	document.getElementById('Loan1003pg1_aCPrev2City').value = document.getElementById('Loan1003pg1_aBPrev2City').value;
	document.getElementById('Loan1003pg1_aCPrev2State').value = document.getElementById('Loan1003pg1_aBPrev2State').value;
	document.getElementById('Loan1003pg1_aCPrev2Zip').value = document.getElementById('Loan1003pg1_aBPrev2Zip').value;
	document.getElementById('Loan1003pg1_aCPrev2AddrT').value = document.getElementById('Loan1003pg1_aBPrev2AddrT').value;
	document.getElementById('Loan1003pg1_aCPrev2AddrYrs').value = document.getElementById('Loan1003pg1_aBPrev2AddrYrs').value;
	updateMailingAddressFields();
	updateDirtyBit();
}

function fillPresentAddress() {
	document.getElementById('Loan1003pg1_aBAddr').value = document.getElementById('Loan1003pg1_sSpAddr').value;
	document.getElementById('Loan1003pg1_aBCity').value = document.getElementById('Loan1003pg1_sSpCity').value;
	document.getElementById('Loan1003pg1_aBZip').value = document.getElementById('Loan1003pg1_sSpZip').value;
	document.getElementById('Loan1003pg1_aBState').value = document.getElementById('Loan1003pg1_sSpState').value;
	updateDirtyBit();
}

function updateBorrowerName() {
//    var name = document.getElementById('Loan1003pg1_aBLastNm').value + ", " + document.getElementById('Loan1003pg1_aBFirstNm').value;
//    parent.info.f_updateApplicantDDL(name, 'e20b51ab-35a0-420e-b933-8d0bebc2ffba');
}

function calculateQualRate(link) {
    if (hasAttr(link, "disabled")) {
        return;
    }

    LQBPopup.Show("QualRateCalculationPopup.aspx?LoanID=" + document.getElementById("sLID").value, {
        height: 240,
        onReturn : function(dialogArgs) {
            if (dialogArgs.OK) {
                document.getElementById('Loan1003pg1_sQualIR').value = dialogArgs.sQualIR;
            }
        },
        hideCloseButton: true
    });
}
