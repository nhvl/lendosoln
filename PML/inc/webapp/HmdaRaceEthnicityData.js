﻿var enumFields = [
    'aBEthnicityCollectedByObservationOrSurname',
    'aCEthnicityCollectedByObservationOrSurname',
    'aBSexCollectedByObservationOrSurname',
    'aCSexCollectedByObservationOrSurname',
    'aBRaceCollectedByObservationOrSurname',
    'aCRaceCollectedByObservationOrSurname',
    'aBInterviewMethodT',
    'aCInterviewMethodT'
];

jQuery(function ($) {
    function SetEnumFields(resultValue)
    {
        for(var i = 0; i < enumFields.length; i++)
        {
            var result = resultValue[enumFields[i]];
            if(typeof(result) !== 'undefined')
            {
                if(result === '0')
                {
                    $('[name$="'+ enumFields[i] + '"]').prop('checked', false);
                }

                $('[name$="' + enumFields[i] + '"][value="' + result + '"]').prop('checked', true);
            }
        }
    }

    function SetEthnicityValues(resultValue)
    {
        var bValue = resultValue['aBHispanicT'];
        var bIsHispanic = $('[id$="aBHispanicT_is"]');
        var bNotHispanic = $('[id$="aBHispanicT_not"]');
        if(bValue === '3') // E_aHispanicT.BothHispanicAndNotHispanic
        {
            bIsHispanic.prop('checked', true);
            bNotHispanic.prop('checked', true);
        }
        else if(bValue === '1') //E_aHispanicT.Hispanic
        {
            bIsHispanic.prop('checked', true);
            bNotHispanic.prop('checked', false);
        }
        else if(bValue === '2') //E_aHispanicT.NotHispanic
        {
            bIsHispanic.prop('checked', false);
            bNotHispanic.prop('checked', true);
        }
        else
        {
            bIsHispanic.prop('checked', false);
            bNotHispanic.prop('checked', false);
        }

        var cValue = resultValue['aCHispanicT'];
        var cIsHispanic = $('[id$="aCHispanicT_is"]');
        var cNotHispanic = $('[id$="aCHispanicT_not"]');
        if(cValue === '3') //E_aHispanicT.BothHispanicAndNotHispanic
        {
            cIsHispanic.prop('checked', true);
            cNotHispanic.prop('checked', true);
        }
        else if(cValue === '1') //E_aHispanicT.Hispanic
        {
            cIsHispanic.prop('checked', true);
            cNotHispanic.prop('checked', false);
        }
        else if(cValue === '2') //E_aHispanicT.NotHispanic
        {
            cIsHispanic.prop('checked', false);
            cNotHispanic.prop('checked', true);
        }
        else
        {
            cIsHispanic.prop('checked', false);
            cNotHispanic.prop('checked', false);
        }
    }

    function SetGenderValues(resultValue)
    {
        var bResult = resultValue['aBGender'];
        if(typeof(bResult) !== 'undefined')
        {
            var bFemale = $('[id$="aBGender_f"]');
            var bMale = $('[id$="aBGender_m"]');
            var bNoProvide = $('[id$="aBGender_u"]');

            if (bResult === '8') // E_GenderT.MaleFemaleNotFurnished
            {
                bFemale.prop('checked', true);
                bMale.prop('checked', true);
                bNoProvide.prop('checked', true);
            }
            else if (bResult === '7') // E_GenderT.FemaleAndNotFurnished
            {
                bFemale.prop('checked', true);
                bMale.prop('checked', false);
                bNoProvide.prop('checked', true);
            }
            else if (bResult === '6') // E_GenderT.MaleAndNotFurnished
            {
                bFemale.prop('checked', false);
                bMale.prop('checked', true);
                bNoProvide.prop('checked', true);
            }
            else if (bResult === '4') //E_GenderT.Unfurnished
            {
                bFemale.prop('checked', false);
                bMale.prop('checked', false);
                bNoProvide.prop('checked', true);
            }
            else if(bResult == '5') //E_GenderT.MaleAndFemale
            {
                bFemale.prop('checked', true);
                bMale.prop('checked', true);
                bNoProvide.prop('checked', false);
            }
            else if(bResult == '1') //E_GenderT.Male
            {
                bFemale.prop('checked', false);
                bMale.prop('checked', true);
                bNoProvide.prop('checked', false);
            }
            else if(bResult == '2') //E_GenderT.Female
            {
                bFemale.prop('checked', true);
                bMale.prop('checked', false);
                bNoProvide.prop('checked', false);
            }
            else
            {
	            bFemale.prop('checked', false);
                bMale.prop('checked', false);
                bNoProvide.prop('checked', false);
            }
        }
    
        var cResult = resultValue['aCGender'];
        if(typeof(cResult) !== 'undefined')
        {
            var cFemale = $('[id$="aCGender_f"]');
            var cMale = $('[id$="aCGender_m"]');
            var cNoProvide = $('[id$="aCGender_u"]');

            if (cResult === '8') // E_GenderT.MaleFemaleNotFurnished
            {
                cFemale.prop('checked', true);
                cMale.prop('checked', true);
                cNoProvide.prop('checked', true);
            }
            else if (cResult === '7') // E_GenderT.FemaleAndNotFurnished
            {
                cFemale.prop('checked', true);
                cMale.prop('checked', false);
                cNoProvide.prop('checked', true);
            }
            else if (cResult === '6') // E_GenderT.MaleAndNotFurnished
            {
                cFemale.prop('checked', false);
                cMale.prop('checked', true);
                cNoProvide.prop('checked', true);
            }
            else if (cResult == '4') //E_GenderT.Unfurnished
            {
                cFemale.prop('checked', false);
                cMale.prop('checked', false);
                cNoProvide.prop('checked', true);
            }
            else if(cResult == '5') //E_GenderT.MaleAndFemale
            {
                cFemale.prop('checked', true);
                cMale.prop('checked', true);
                cNoProvide.prop('checked', false);
            }
            else if(cResult == '1') //E_GenderT.Male
            {
                cFemale.prop('checked', false);
                cMale.prop('checked', true);
                cNoProvide.prop('checked', false);
            }
            else if(cResult == '2') //E_GenderT.Female
            {
                cFemale.prop('checked', true);
                cMale.prop('checked', false);
                cNoProvide.prop('checked', false);
            }
            else
            {
                cFemale.prop('checked', false);
                cMale.prop('checked', false);
                cNoProvide.prop('checked', false);
            }
        }
    }

    function SetDataValues(resultValue, clientId)
    {
        SetEnumFields(resultValue);
        SetEthnicityValues(resultValue);
        SetGenderValues(resultValue);
    }

    function GetDataValues(obj) 
    {
        GetEnumFields(obj);
        GetEthnicityValues(obj);
        GetGenderValues(obj);
    }

    function GetEnumFields(obj)
    {
        for(var i = 0; i < enumFields.length; i++)
        {
            var value = $('[name$="' + enumFields[i] + '"]:checked').val();
            if(typeof(value) === 'undefined')
            {
                value = '0';
            }
                
            obj[enumFields[i]] = value;
        }
    }
        
    function GetGenderValues(obj)
    {
        var bFemale = $('[id$="aBGender_f"]').is(':checked');
        var bMale = $('[id$="aBGender_m"]').is(':checked');
        var bNoProvide = $('[id$="aBGender_u"]').is(':checked');

        var bValue;
        if (bNoProvide && bFemale && bMale)
        {
            bValue = '8'; //E_GenderT.MaleFemaleNotFurnished
        }
        else if (bNoProvide && bMale)
        {
            bValue = '6'; //E_GenderT.MaleAndNotFurnished
        }
        else if (bNoProvide && bFemale)
        {
            bValue = '7'; //E_GenderT.FemaleAndNotFurnished
        }
        else if (bNoProvide)
        {
            bValue = '4'; //E_GenderT.Unfurnished
        }
        else if(bMale && bFemale)
        {
            bValue = '5'; //E_GenderT.MaleAndFemale
        }
        else if(bMale)
        {
            bValue = '1'; //E_GenderT.Male
        }
        else if(bFemale)
        {
            bValue = '2'; //E_GenderT.Female
        }
        else
        {
            bValue = '0'; //E_GenderT.LeaveBlank
        }

        obj["aBGender"] = bValue;

        var cFemale = $('[id$="aCGender_f"]').is(':checked');
        var cMale = $('[id$="aCGender_m"]').is(':checked');
        var cNoProvide = $('[id$="aCGender_u"]').is(':checked');

        var cValue;
        if (cNoProvide && cFemale && cMale)
        {
            cValue = '8'; //E_GenderT.MaleFemaleNotFurnished
        }
        else if (cNoProvide && cMale)
        {
            cValue = '6'; //E_GenderT.MaleAndNotFurnished
        }
        else if (cNoProvide && cFemale)
        {
            cValue = '7'; //E_GenderT.FemaleAndNotFurnished
        }
        else if (cNoProvide)
        {
            cValue = '4'; //E_GenderT.Unfurnished
        }
        else if(cMale && cFemale)
        {
            cValue = '5'; //E_GenderT.MaleAndFemale
        }
        else if(cMale)
        {
            cValue = '1'; //E_GenderT.Male
        }
        else if(cFemale)
        {
            cValue = '2'; //E_GenderT.Female
        }
        else
        {
            cValue = '0'; //E_GenderT.LeaveBlank
        }

        obj["aCGender"] = cValue;
    }

    function GetEthnicityValues(obj)
    {
        var markedBIsHispanic = $('[id$="aBHispanicT_is"]').is(':checked');
        var markedBNotHispanic = $('[id$="aBHispanicT_not"]').is(':checked');

        var bValue;
        if (markedBIsHispanic && markedBNotHispanic)
        {
            bValue = '3'; //E_aHispanicT.BothHispanicAndNotHispanic
        }
        else if (markedBIsHispanic)
        {
            bValue = '1'; //E_aHispanicT.Hispanic
        }
        else if (markedBNotHispanic)
        {
            bValue = '2'; //E_aHispanicT.NotHispanic
        }
        else
        {
            bValue = '0'; //E_aHispanicT.LeaveBlank
        }
                
        obj['aBHispanicT'] = bValue;            

        var markedCIsHispanic = $('[id$="aCHispanicT_is"]').is(':checked');
        var markedCNotHispanic = $('[id$="aCHispanicT_not"]').is(':checked');

        var cValue;
        if (markedCIsHispanic && markedCNotHispanic)
        {
            cValue = '3'; //E_aHispanicT.BothHispanicAndNotHispanic
        }
        else if (markedCIsHispanic)
        {
            cValue = '1'; //E_aHispanicT.Hispanic
        }
        else if (markedCNotHispanic)
        {
            cValue = '2'; //E_aHispanicT.NotHispanic
        }
        else
        {
            cValue = '0'; //E_aHispanicT.LeaveBlank
        }

        obj['aCHispanicT'] = cValue;
    }

    function ToggleOptions(parentChecked, targetClass)
    {
        var elements = $('.' + targetClass);
        elements.prop('disabled', !parentChecked);
    }

    function HideElement(shouldHide, targetId)
    {
        var element = $('[id$="' + targetId + '"]');
        element.toggle(!shouldHide);
    }

    function ShouldHideGenderFallback(borrowerType) {
        var numChecked = 0;
        if (borrowerType === 'B') {
            numChecked += $('input[id$="aBGender_f"]').is(':checked') ? 1 : 0;
            numChecked += $('input[id$="aBGender_m"]').is(':checked') ? 1 : 0;
            numChecked += $('input[id$="aBGender_u"]').is(':checked') ? 1 : 0;
        }
        else {
            numChecked += $('input[id$="aCGender_f"]').is(':checked') ? 1 : 0;
            numChecked += $('input[id$="aCGender_m"]').is(':checked') ? 1 : 0;
            numChecked += $('input[id$="aCGender_u"]').is(':checked') ? 1 : 0;
        }

        return numChecked <= 1;
    }

    $('input[name$="aBInterviewMethodT"]').change(function () {
        ToggleOptions($('input[id$="aBInterviewMethodT_ftf"]').is(':checked'), 'BFtFCollected');
    });

    $('input[name$="aCInterviewMethodT"]').change(function () {
        ToggleOptions($('input[id$="aCInterviewMethodT_ftf"]').is(':checked'), 'CFtFCollected');
    });

    $('.RefreshCalculation').change(function() {
        refreshCalculation(true);
    });

    $('input[id$="aBHispanicT_is"], input[id$="aBHispanicT_not"]').change(function() {
        var shouldHide = !($('input[id$="aBHispanicT_is"]').is(':checked') && $('input[id$="aBHispanicT_not"]').is(':checked'));
        HideElement(shouldHide, 'BEthnicityFallbackRow');
    });

    $('input[id$="aCHispanicT_is"], input[id$="aCHispanicT_not"]').change(function() {
        var shouldHide = !($('input[id$="aCHispanicT_is"]').is(':checked') && $('input[id$="aCHispanicT_not"]').is(':checked'));
        HideElement(shouldHide, 'CEthnicityFallbackRow');
    });

    $('input[id$="aBGender_f"], input[id$="aBGender_m"], input[id$="aBGender_u"]').change(function () {
        var shouldHide = ShouldHideGenderFallback('B');
        HideElement(shouldHide, 'BGenderFallbackRow');
    });

    $('input[id$="aCGender_f"], input[id$="aCGender_m"], input[id$="aCGender_u"]').change(function () {
        var shouldHide = ShouldHideGenderFallback('C');
        HideElement(shouldHide, 'CGenderFallbackRow');
    });

    $('input[id$="aBNoFurnishLckd"]').change(function () {
        ToggleOptions(this.checked, 'aBNoFurnish');
    });

    $('input[id$="aCNoFurnishLckd"]').change(function () {
        ToggleOptions(this.checked, 'aCNoFurnish');
    });

    ToggleOptions($('input[id$="aBInterviewMethodT_ftf"]').is(':checked'), 'BFtFCollected');
    ToggleOptions($('input[id$="aCInterviewMethodT_ftf"]').is(':checked'), 'CFtFCollected');
    ToggleOptions($('input[id$="aBNoFurnishLckd"]').is(':checked'), 'aBNoFurnish');
    ToggleOptions($('input[id$="aCNoFurnishLckd"]').is(':checked'), 'aCNoFurnish');
    HideElement(!($('input[id$="aBHispanicT_is"]').is(':checked') && $('input[id$="aBHispanicT_not"]').is(':checked')), 'BEthnicityFallbackRow');
    HideElement(!($('input[id$="aCHispanicT_is"]').is(':checked') && $('input[id$="aCHispanicT_not"]').is(':checked')), 'CEthnicityFallbackRow');
    HideElement(ShouldHideGenderFallback('B'), 'BGenderFallbackRow');
    HideElement(ShouldHideGenderFallback('C'), 'CGenderFallbackRow');
    registerPostGetAllFormValuesCallback(GetDataValues);
    registerPostRefreshCalculationCallback(SetDataValues);
});
