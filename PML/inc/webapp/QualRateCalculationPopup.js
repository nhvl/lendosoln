﻿// core functionality of this javascript is based off of \lendersofficeapp\inc\qualratecalculationpopup.js
function onQualSave() {
    refreshCalculation(true);

    parent.LQBPopup.Return({
        OK: true,
        sQualIR: document.getElementById("sQualIR").value
    });
}

function onQualCancel() {
    parent.LQBPopup.Return({ OK: false });
}

function refreshCalculation(savePage) {
    var clientID = "QualRateCalculationPopup";
    // Since recalculate does not always required every field to be pass to server.
    // Add skipMe attribute to input that does not required in calculation.
    var args = getAllFormValues(clientID, true);
    if (typeof (_postGetAllFormValues) == "function") {
        _postGetAllFormValues(args);
    }

    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (typeof (_postGetAllFormValuesCallbacks[i]) == "function") {
            _postGetAllFormValuesCallbacks[i](args);
        }
    }

    var sFileVersion = null;
    if (document.getElementById("sFileVersion") != null)
        sFileVersion = document.getElementById("sFileVersion").value;
    var sLID = ML.sLID;

    args["_ClientID"] = clientID;
    args["sFileVersion"] = sFileVersion;
    args["loanid"] = sLID;
    args["applicationID"] = null;
    args["sQualIR"] = document.getElementById("sQualIR").value;
    var method = savePage ? "SavePage" : "RefreshData";

    var result = gService.QualRateCalculationPopupService.call(method, args);
    if (!result.error) {
        populateForm(result.value, clientID);
        if (typeof (_postRefreshCalculation) == "function") {
            _postRefreshCalculation(result.value, clientID);
        }
        for (var i = 0; i < _postRefreshCalculationCallbacks.length; i++) {
            if (typeof (_postRefreshCalculationCallbacks[i]) == "function") {
                _postRefreshCalculationCallbacks[i](result.value, clientID);
            }
        }
    }
    else {
        if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
            f_displayFieldWriteDenied(result.UserMessage);
            return false;
        }
        var errMsg = 'Unable to process data. Please try again.';
        if (null != result.UserMessage)
            errMsg = result.UserMessage;
        if (typeof (_onError) == "function")
            _onError(errMsg);
        else
            alert(errMsg);
        return false;
    }
    if (typeof (_init) == "function")
        _init();
    return false;
}

var lpeUploadValue = '0';

function updateQualRateCalculationModel() {
    if (typeof shouldQualRateModelUpdateReturnEarly === 'function' && shouldQualRateModelUpdateReturnEarly()) {
        return;
    }

    var useFlatValue = QualRateCalculationFlatValue.checked;

    $('#SecondCalculationRow').toggle(!useFlatValue);

    var useQualRate = ShouldUseQualRate == null || ShouldUseQualRate.checked;
    setQualRateCalculationOptionsDisabled(!useQualRate);
    if (!useQualRate) {
        return;
    }

    updateQualRateCalculationField(QualRateCalculationFieldT1);

    var firstCalculationAdjustment = $(QualRateCalculationAdjustment1);
    if (useFlatValue && $(QualRateCalculationFieldT1).val() === lpeUploadValue) {
        firstCalculationAdjustment.prop('readonly', true).val('');
    }
    else {
        firstCalculationAdjustment.prop('readonly', false);
    }

    if (useFlatValue) {
        return;
    }

    updateQualRateCalculationField(QualRateCalculationFieldT2);

    if (QualRateCalculationFieldT1.selectedIndex === QualRateCalculationFieldT2.selectedIndex) {
        QualRateCalculationFieldT2.selectedIndex = (QualRateCalculationFieldT2.selectedIndex + 1) % QualRateCalculationFieldT2.options.length;
    }

    setQualRateFieldsMutuallyExclusive(QualRateCalculationFieldT1);
    setQualRateFieldsMutuallyExclusive(QualRateCalculationFieldT2);
}

function setQualRateCalculationOptionsDisabled(disabled) {
    $(QualRateCalculationFlatValue).prop('disabled', disabled);
    $(QualRateCalculationMaxOf).prop('disabled', disabled);
    disableDDL(QualRateCalculationFieldT1, disabled);
    disableDDL(QualRateCalculationFieldT2, disabled);
    $(QualRateCalculationAdjustment1).prop('readonly', disabled);
    $(QualRateCalculationAdjustment2).prop('readonly', disabled);
}

function getCalculationFieldOptions() {
    var options = [];

    options.push(new Option('Note Rate', '1'));
    options.push(new Option('Fully Indexed Rate', '2'));
    options.push(new Option('Index', '3'));
    options.push(new Option('Margin', '4'));

    return options;
}

function updateQualRateCalculationField(dropdown) {
    var $dropdown = $(dropdown);
    var dropdownValue = $dropdown.val();
    var wasLegacyOption = dropdownValue === lpeUploadValue;

    $dropdown.empty();
    var calculationOptions = getCalculationFieldOptions();
    for (var i = 0; i < calculationOptions.length; i++) {
        dropdown.options.add(calculationOptions[i]);
    }

    if (dropdownValue === null || (wasLegacyOption && dropdownValue === lpeUploadValue)) {
        $dropdown.val('1'/*Note Rate*/);
    }
    else {
        $dropdown.val(dropdownValue);
    }
}

function setQualRateFieldsMutuallyExclusive(dropdown) {
    var selectedField = $(dropdown);

    $('select.QualRateCalculationField').not(selectedField)
        .find('option[value="' + selectedField.val() + '"]')
        .prop('disabled', true)
        .end()
        .find('option:not(option[value="' + selectedField.val() + '"])')
        .prop('disabled', false);
}

var wasDirty = false;
var originalQualRateValues = {};

var QualRateCalculationPopup = null;
var ShouldUseQualRate = null;
var LegacyValueName = 'Legacy Value';
var QualRateCalculationFlatValue = null;
var QualRateCalculationMaxOf = null;
var QualRateCalculationFieldT1 = null;
var QualRateCalculationFieldT2 = null;
var QualRateCalculationAdjustment1 = null;
var QualRateCalculationAdjustment2 = null;

function _postGetAllFormValues(obj) {
    setServiceArgumentValuesFromPopup(obj);
}

function _postRefreshCalculation(resultValue) {
    if (resultValue.HasQualRatePopup == "True") {
        updateQualRateFields(resultValue);
    }
}

function setServiceArgumentValuesFromPopup(obj) {
    obj.sUseQualRate = ShouldUseQualRate.checked;
    obj.sQualRateCalculationFieldT1 = $(QualRateCalculationFieldT1).val();
    obj.sQualRateCalculationAdjustment1 = $(QualRateCalculationAdjustment1).val();
    obj.sQualRateCalculationFieldT2 = $(QualRateCalculationFieldT2).val();
    obj.sQualRateCalculationAdjustment2 = $(QualRateCalculationAdjustment2).val();

    obj.sQualRateCalculationT = QualRateCalculationFlatValue.checked ? ML.FlatValue : ML.MaxOf;
}

function updateQualRateFields(resultValue) {
    ShouldUseQualRate.checked = resultValue.sUseQualRate.toLowerCase() === "true";

    $('#' + QualRateFieldIds.QualRate).val(resultValue.sQualIR);
    $(QualRateCalculationFieldT1).val(resultValue.sQualRateCalculationFieldT1);
    $(QualRateCalculationFieldT2).val(resultValue.sQualRateCalculationFieldT2);
    $(QualRateCalculationAdjustment1).val(resultValue.sQualRateCalculationAdjustment1);
    $(QualRateCalculationAdjustment2).val(resultValue.sQualRateCalculationAdjustment2);
    $('#' + QualRateFieldIds.QualRateCalculationResult1).val(resultValue.QualRateCalculationResult1);
    $('#' + QualRateFieldIds.QualRateCalculationResult2).val(resultValue.QualRateCalculationResult2);

    var isFlatValue = resultValue.sQualRateCalculationT === ML.FlatValue;
    QualRateCalculationFlatValue.checked = isFlatValue;
    QualRateCalculationMaxOf.checked = !isFlatValue;
}

jQuery(function ($) {
    QualRateCalculationPopup = document.getElementById(QualRateFieldIds.QualRateCalcPopup);
    ShouldUseQualRate = document.getElementById(QualRateFieldIds.ShouldUseQualRate);
    LegacyValueName = 'Legacy Value';
    QualRateCalculationFlatValue = document.getElementById(QualRateFieldIds.QualRateCalculationFlatValue);
    QualRateCalculationMaxOf = document.getElementById(QualRateFieldIds.QualRateCalculationMaxOf);
    QualRateCalculationFieldT1 = document.getElementById(QualRateFieldIds.QualRateCalculationFieldT1);
    QualRateCalculationFieldT2 = document.getElementById(QualRateFieldIds.QualRateCalculationFieldT2);
    QualRateCalculationAdjustment1 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment1);
    QualRateCalculationAdjustment2 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment2);

    updateQualRateCalculationModel();

    $('select.QualRateCalculationField, input.sQualRateCalculationT, #' + QualRateFieldIds.ShouldUseQualRate).change(function () {
        updateQualRateCalculationModel();
        refreshCalculation(false);
    });
    TPOStyleUnification.Components();
});
