(function ($) {
    "use strict";
    $(function () {
        function toItem(x) {
            x.content = x.FriendlyName + " (" + x.Id + ")";
            x.jets = x.content.toLowerCase();
            return x;
        }
        function items2Groups(items) {
            var groups = [];
            var group = null;
            items.forEach(function (item) {
                if (group == null || item.CategoryName != group.CategoryName) {
                    group = { items: [item], CategoryName: item.CategoryName, jets: "" };
                    groups.push(group);
                }
                else {
                    group.items.push(item);
                }
            });
            groups.forEach(function (group) {
                group.jets = group.items.map(function (x) { return (x.FriendlyName + " (" + x.Id + ")").toLowerCase(); }).join(" ");
            });
            return groups;
        }
        var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
        var iframeSelf = new IframeSelf();
        iframeSelf.popup = popup;
        var dateCalculatorHtml = "<style>{{vm.jetCss}}</style>\n        <div class=\"modal-header\">\n            <h4 class=\"modal-title\">Calculate the due date based on a loan date</h4>\n        </div>\n        <div class=\"modal-body\">\n            <div class=\"table\">\n                <div>\n                    <div class=\"text-grey\"><label>Search by Field Name or ID</label></div>\n                    <div>\n                        <input type=\"text\" ng-model=\"vm.searchFilter\" ng-change=\"vm.search()\" />\n                    </div>\n                    <div><button type=\"button\" ng-click=\"vm.search()\">Search <i class=\"material-icons\">&#xE8B6;</i></button></div>\n                </div>\n            </div>\n\n            <div class=\"calculate-section\">\n                <ul class=\"date-calculator-list\">\n                    <li ng-repeat=\"group in vm.groups\" data-jets=\"{{group.jets}}\">\n                        <div class=\"category\" ng-bind=\"group.CategoryName\"></div>\n                        <ul class=\"date-calculator-list\">\n                            <li ng-repeat=\"x in group.items\"\n                                data-jets=\"{{x.jets}}\"\n                                ng-bind=\"x.content\"\n                                class=\"item\" ng-class=\"{selected:x.Id == vm.selectedItem.Id}\"\n                                ng-click=\"vm.select(x)\"></li>\n                        </ul>\n                    </li>\n                </ul>\n            </div>\n            <div class=\"table table-modal-padding-bottom\">\n                <div>\n                    <div><input type=\"text\" ng-model=\"vm.duration\" class=\"form-control-number\"></div>\n                    <div>business days</div>\n                    <div>\n                        <input type=\"radio\" id=\"afterRadio\" ng-model=\"vm.positiveDuration\" ng-value=\"true\" label=\"after the\">\n                    </div>\n                    <div ng-bind=\"vm.selectedItem.FriendlyName\"></div>\n                </div>\n                <div>\n                    <div></div>\n                    <div></div>\n                    <div>\n                        <input type=\"radio\" id=\"priorRadio\" ng-model=\"vm.positiveDuration\" ng-value=\"false\" label=\"prior to the\">\n                    </div>\n                    <div></div>\n                </div>\n            </div>\n            <p ng-bind=\"vm.errorMessage\" class=\"text-danger\"></p>\n        </div>\n        <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-flat\" ng-click=\"vm.cancel()\">Cancel</button>\n            <button type=\"button\" class=\"btn btn-flat\" ng-click=\"vm.ok    ()\">OK</button>\n        </div>\n        ";
        var DateCalculatorController = (function () {
            function DateCalculatorController($element, $timeout) {
                this.$element = $element;
                this.$timeout = $timeout;
                var items = DateCalculatorViewModel.parameters.map(toItem);
                this.groups = items2Groups(items);
                this.selectedItem = items.filter(function (p) { return p.Id == DateCalculatorViewModel.fieldId; })[0];
                if (this.selectedItem == null)
                    this.selectedItem = items[0];
                this.searchFilter = "";
                this.duration = DateCalculatorViewModel.duration.toString();
                this.positiveDuration = DateCalculatorViewModel.positiveDuration;
                this.errorMessage = "";
                setTimeout(function () {
                    TPOStyleUnification.Components($element);
                    numberOnlyInput(".form-control-number");
                });
                window.dateCalculator = this;
            }
            DateCalculatorController.prototype.search = function () {
                var search = this.searchFilter.trim();
                this.jetCss = !search ? "" : ".date-calculator-list li:not([data-jets*=\"" + search.toLowerCase() + "\"]){ display:none }";
            };
            DateCalculatorController.prototype.select = function (item) {
                this.selectedItem = item;
                return false;
            };
            DateCalculatorController.prototype.cancel = function () {
                if (this.selectedItem == null && !this.duration) {
                    iframeSelf.resolve(null);
                    return false;
                }
                simpleDialog.confirm("", "Cancel?").then(function (isConfirm) {
                    if (isConfirm) {
                        iframeSelf.resolve(null);
                    }
                });
                iframeSelf.resolve(null);
                return false;
            };
            DateCalculatorController.prototype.ok = function () {
                if (this.selectedItem == null) {
                    simpleDialog.alert("Select a date");
                    return false;
                }
                var offset = this.duration * 1; // Multiplying by 1 to convert null strings to 0
                var result = gService.main.call("generateOutput", {
                    field: this.selectedItem.Id,
                    offset: offset,
                    addOffset: this.positiveDuration,
                    loanid: DateCalculatorViewModel.LoanID,
                });
                if (result.error) {
                    simpleDialog.alert(result.UserMessage);
                    return false;
                }
                if (result.value["pastDate"] == 'True') {
                    this.errorMessage = "The calculated date (" + result.value["date"] + ") has already passed. The due date must be either today or a future date.";
                    return false;
                }
                iframeSelf.resolve({
                    offset: result.value["offset"],
                    id: this.selectedItem.Id,
                    dateDescription: result.value["dateDescription"],
                    date: result.value["date"],
                    OK: true,
                });
                return false;
            };
            DateCalculatorController.$inject = ["$element", "$timeout"];
            return DateCalculatorController;
        }());
        function dateCalculatorDirective() {
            return {
                restrict: "EA",
                controller: DateCalculatorController,
                controllerAs: "vm",
                template: dateCalculatorHtml,
            };
        }
        angular.module("DateCalculator", []).directive("dateCalculator", dateCalculatorDirective);
        angular.bootstrap(document.querySelector("[date-calculator]"), ["DateCalculator"]);
        // https://stackoverflow.com/a/469362
        function numberOnlyInput(e) { $(e).keydown(function (e) { -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || 65 == e.keyCode && (e.ctrlKey === !0 || e.metaKey === !0) || 67 == e.keyCode && (e.ctrlKey === !0 || e.metaKey === !0) || 88 == e.keyCode && (e.ctrlKey === !0 || e.metaKey === !0) || e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.preventDefault(); }); }
    });
})(jQuery);
//# sourceMappingURL=DateCalculator.js.map