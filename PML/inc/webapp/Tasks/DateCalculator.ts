declare const DateCalculatorViewModel: {
    parameters: IItem[],
    duration: number,
    positiveDuration: boolean,
    fieldId: string,
    LoanID:string,
};
interface IItem {
    Id:string,
    FriendlyName:string,
    CategoryName:string,
    content:string,
    jets:string,
}

(function($){"use strict";$(() => {
    function toItem(x:IItem) {
        x.content = `${x.FriendlyName} (${x.Id})`;
        x.jets = x.content.toLowerCase();
        return x;
    }

    interface IGroup {
        items:IItem[],
        CategoryName:string,
        jets:string,
    }
    function items2Groups(items:IItem[]){
        const groups:IGroup[] = [];
        let group:IGroup|null = null;

        items.forEach(item => {
            if (group == null || item.CategoryName != group.CategoryName) {
                group = { items:[item], CategoryName:item.CategoryName, jets:"" };
                groups.push(group);
            } else {
                group.items.push(item);
            }
        });
        groups.forEach(group => {
            group.jets = group.items.map(x => `${x.FriendlyName} (${x.Id})`.toLowerCase()).join(" ");
        });

        return groups;
    }

    const popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    const iframeSelf = new IframeSelf();
    iframeSelf.popup = popup;

    const dateCalculatorHtml =
        `<style>{{vm.jetCss}}</style>
        <div class="modal-header">
            <h4 class="modal-title">Calculate the due date based on a loan date</h4>
        </div>
        <div class="modal-body">
            <div class="table">
                <div>
                    <div class="text-grey"><label>Search by Field Name or ID</label></div>
                    <div>
                        <input type="text" ng-model="vm.searchFilter" ng-change="vm.search()" />
                    </div>
                    <div><button type="button" ng-click="vm.search()">Search <i class="material-icons">&#xE8B6;</i></button></div>
                </div>
            </div>

            <div class="calculate-section">
                <ul class="date-calculator-list">
                    <li ng-repeat="group in vm.groups" data-jets="{{group.jets}}">
                        <div class="category" ng-bind="group.CategoryName"></div>
                        <ul class="date-calculator-list">
                            <li ng-repeat="x in group.items"
                                data-jets="{{x.jets}}"
                                ng-bind="x.content"
                                class="item" ng-class="{selected:x.Id == vm.selectedItem.Id}"
                                ng-click="vm.select(x)"></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="table table-modal-padding-bottom">
                <div>
                    <div><input type="text" ng-model="vm.duration" class="form-control-number"></div>
                    <div>business days</div>
                    <div>
                        <input type="radio" id="afterRadio" ng-model="vm.positiveDuration" ng-value="true" label="after the">
                    </div>
                    <div ng-bind="vm.selectedItem.FriendlyName"></div>
                </div>
                <div>
                    <div></div>
                    <div></div>
                    <div>
                        <input type="radio" id="priorRadio" ng-model="vm.positiveDuration" ng-value="false" label="prior to the">
                    </div>
                    <div></div>
                </div>
            </div>
            <p ng-bind="vm.errorMessage" class="text-danger"></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-flat" ng-click="vm.cancel()">Cancel</button>
            <button type="button" class="btn btn-flat" ng-click="vm.ok    ()">OK</button>
        </div>
        `;
    class DateCalculatorController {
        private groups: IGroup[];
        private selectedItem: IItem|undefined;
        private searchFilter:string;
        private duration:string;
        private positiveDuration:boolean;
        private errorMessage:string;

        private jetCss: string;

        static $inject = ["$element", "$timeout"];
        constructor(private $element:JQuery, private $timeout: angular.ITimeoutService) {
            const items = DateCalculatorViewModel.parameters.map(toItem);
            this.groups = items2Groups(items);

            this.selectedItem = items.filter(p => p.Id == DateCalculatorViewModel.fieldId)[0];
            if (this.selectedItem == null) this.selectedItem = items[0];

            this.searchFilter = "";
            this.duration = DateCalculatorViewModel.duration.toString();
            this.positiveDuration = DateCalculatorViewModel.positiveDuration;
            this.errorMessage = "";

            setTimeout(() => {
                TPOStyleUnification.Components($element);

                numberOnlyInput(".form-control-number");
            });

            window.dateCalculator = this;
        }

        private search() {
            const search = this.searchFilter.trim();
            this.jetCss = !search ? "" : `.date-calculator-list li:not([data-jets*="${search.toLowerCase()}"]){ display:none }`;
        }

        private select(item:IItem){
            this.selectedItem = item;
            return false;
        }
        private cancel(){
            if (this.selectedItem == null && !this.duration) {
                iframeSelf.resolve(null);
                return false;
            }

            simpleDialog.confirm("", "Cancel?").then((isConfirm:boolean) => {
                if (isConfirm) {
                    iframeSelf.resolve(null);
                }
            });

            iframeSelf.resolve(null);
            return false;
        }
        private ok(){
            if (this.selectedItem == null) {
                simpleDialog.alert("Select a date");
                return false;
            }

            const offset = this.duration * 1; // Multiplying by 1 to convert null strings to 0

            const result = gService.main.call("generateOutput", {
                field    : this.selectedItem.Id,
                offset   : offset,
                addOffset: this.positiveDuration,
                loanid   : DateCalculatorViewModel.LoanID,
            });

            if (result.error) {
                simpleDialog.alert(result.UserMessage);
                return false;
            }

            if (result.value["pastDate"] == 'True') {
                this.errorMessage = `The calculated date (${result.value["date"]}) has already passed. The due date must be either today or a future date.`;
                return false;
            }

            iframeSelf.resolve({
                offset          : result.value["offset"],
                id              : this.selectedItem.Id,
                dateDescription : result.value["dateDescription"],
                date            : result.value["date"],
                OK              : true,
            });

            return false;
        }
    }
    function dateCalculatorDirective(): angular.IDirective {
        return {
            restrict     : "EA",
            controller   : DateCalculatorController,
            controllerAs : "vm",
            template     : dateCalculatorHtml,
        };
    }

    angular.module("DateCalculator", []).directive("dateCalculator", dateCalculatorDirective);
    angular.bootstrap(document.querySelector("[date-calculator]")!, ["DateCalculator"]);

// https://stackoverflow.com/a/469362
function numberOnlyInput(e){$(e).keydown(function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||65==e.keyCode&&(e.ctrlKey===!0||e.metaKey===!0)||67==e.keyCode&&(e.ctrlKey===!0||e.metaKey===!0)||88==e.keyCode&&(e.ctrlKey===!0||e.metaKey===!0)||e.keyCode>=35&&e.keyCode<=39||(e.shiftKey||e.keyCode<48||e.keyCode>57)&&(e.keyCode<96||e.keyCode>105)&&e.preventDefault()})}

});})(jQuery);
