﻿//Script for OrderServices.aspx
var currentSec = "-1";
var $ = jQuery;
var lastPrimaryProduct = null;
var VendorIdToBeUpdated = "";
var g_eDocumentId = '';
var bypassTabClear = true;
var refreshOnPopupExit = false;
//$(document).ready(function() {
//    init();
//})

var VendorId;
var OrderNumber;

function _init() {
	if(!placeholderIsSupported())
	{
		//IE8 & IE9 does not support placeholder.
		//Use library to fix it
		//https://github.com/mathiasbynens/jquery-placeholder
		$('input, textarea').placeholder();
    }

    tabsHandler($("#tabSection").val());
	eventHandler();
	loadSignedRadio(null);
	loadLoginInfo();
	setOrder(false,false);
	setOrder(false, true);
	showAppraisalLogin();
	Reset4506T();

	if ($("#OrderNumber").val() != "") {
	    setOrder(true, true);
	    $("#password").val($("#password").attr("PlaceholderVal"));
	    checkAppraisalCredentials();
	    $("#SelectedAMC").attr("disabled", true);
	}

	if ($("#OrderNumber").val() != "") {
	    //set the value of the report types
	    $("#ReportType").val($("#ReportTypeEdit").val());
	    $("#ReportType2").val($("#ReportTypeEdit2").val());
	    $("#ReportType3").val($("#ReportTypeEdit3").val());
	    $("#ReportType4").val($("#ReportTypeEdit4").val());
	    $("#ReportType5").val($("#ReportTypeEdit5").val());
	}

	$("#aB4506TNmLckd").change();
	$("#aC4506TNmLckd").change();

	initDocPicker();

	mask();
	$("#btnAdd, #btnAdd_Appraisal, #VOA_OrderBtn, #VOE_OrderBtn").prop("disabled", true);

	if ($("#Allow4506T").val() == "True") {
	    $("#sec4506T .secInvalidPermission").hide();
	    $("#btnAdd").prop("disabled", false);
	}

	if ($("#AllowAppraisal").val() == "True") {
	    $("#secAppraisals .secInvalidPermission").hide();
	    $("#btnAdd_Appraisal").prop("disabled", false);
	}

	if (ML.CanOrderVoa) {
	    $("#secVoa .secInvalidPermission").hide();
	    $("#VOA_OrderBtn").prop("disabled", false);
	}

	if (ML.CanOrderVoe) {
	    $("#secVoe .secInvalidPermission").hide();
	    $("#VOE_OrderBtn").prop("disabled", false);
	}

	$('.additionalEmail').change();

    TPOStyleUnification.Components();
    var table = $("#DocTable")
    TPOStyleUnification.TableSorterStyle(table);

}

    function runPricing() {
        if ($("#IsNewPmlUIEnabled").val() == "True") {
            document.location = gVirtualRoot + '/webapp/pml.aspx?loanid=' + encodeURIComponent($("#sLId").val()) + '&source=PML';
        }
        else {
            document.location = gVirtualRoot + "/main/agents.aspx?loanid=" + encodeURIComponent($("#sLId").val());
        }

    }

    function editClosingCosts() {
        if ($("#IsUsing2015GFEUITPO").val() == "True")
        {
            document.location = gVirtualRoot + '/webapp/TPOPortalClosingCosts.aspx?loanid=' + encodeURIComponent($("#sLId").val()) + '&src=pipeline';
        }
        else{
            document.location = gVirtualRoot + '/webapp/TPOFeeEditor.aspx?loanid=' + encodeURIComponent($("#sLId").val()) + '&src=pipeline';
        }
    }

    function view1003()
    {
        document.location = gVirtualRoot + '/webapp/Loan1003.aspx?loanid='+ encodeURIComponent($("#sLId").val()) +'&src=pipeline';
    }

function eventHandler() {
	// On the click event of a tab.
    $("#tabs>li a").click(function(e) {
        e.preventDefault();
        //pushState("sec=" + $(this).attr("href").substring(5).toString());
        tabsHandler($(this).attr("href"));
    });

	$("input[type='text'],textarea").click(function() {
		$(this).select();
	});
	$("#Signed_Yes").change(function() {
		loadSignedRadio($(this).is(':checked'));
	});
	$("#Signed_No").change(function() {
	    loadSignedRadio(!$(this).is(':checked'));
	    load4506App();
	});
	$("#aB4506TNmLckd").change(function() {
	    $('#aB4506TNm').prop('disabled', !$(this).is(':checked'));
	    if (!$(this).is(':checked')) {
	        $('#aB4506TNm').val($("#OriginalaBNm").val());
	    }
	});
	$("#aC4506TNmLckd").change(function() {
	    $('#aC4506TNm').prop('disabled', !$(this).is(':checked'));
	    if (!$(this).is(':checked')) {
	        $('#aC4506TNm').val($("#OriginalaCNm").val());
	    }
	});
	$("#Vendor4506").change(function() {
		loadLoginInfo();
	});

	$("#SelectedAMC").change(function() {
	    showAppraisalLogin();
	    getAppraisalCredentials();
	    checkAppraisalCredentials();
	    $(".appraisal-need-date").toggle(!HideAppraisalNeededDate[$("#SelectedAMC").val()]);
        $(".AppraisalNeededDIndicator").toggle(AppraisalNeededDateRequired[$("#SelectedAMC").val()]);
	    $("#password").attr("DummyPass", true);
	});

	$("#btnPlaceOrder").click(function() {
	    SubmitOrder();
	});

	//http://jsfiddle.net/43nWM/3/
	$('#documents').on('click', 'a', function() {
	    var $parent = $(this).parent('li');

	    var ids = JSON.parse(document.getElementById('AttachedDocumentsIds').value);
	    ids = ids.splice([$parent.attr("DocID")]);

	    var idsString = JSON.stringify(ids);
	    document.getElementById('AttachedDocumentsIds').value = idsString;

	    $(this).parent('li').remove();
	    return false;
	});


	$("#btnAdd").click(function() {
	    setOrder(true, false);
	});
	$("#btnCancel").click(function() {
	    Reset4506T();
	});
	$("#btnAdd_Appraisal").click(function() {
	    setOrder(true, true);
	    $("#btnCancel_Appraisal").text("Cancel Order");
	    $("#btnPlaceOrder").text("Place Order");
	    showAppraisalLogin();
	});

	$("#BillingMethod").change(function() {
		loadBillingInfo();
    });

    $("#attachDocument").click(function() {
        selectAppraisalDoc();
        return false;
    });

    $("#login, #password, #accID").change(function() {
        checkAppraisalCredentials();
        $("#password").attr("DummyPass", false);
    });

    $("#AppraisalLoginBtn").click(function() {
        var data = new Object();
        data["Username"] = $("#login").val();
        data["Password"] = $("#password").val();
        data["AccountId"] = $("#accID").val();
        data["VendorId"] = $("#SelectedAMC").val();
        data["LoanId"] = $("#sLId").val();
        data["HasCredentials"] = $("#password").attr("DummyPass") != "true";
        refreshOnPopupExit = true;
        var result = gService.main.call("LoadOptions", data);

        loadVendorUI(result);

    });


    $(".RefreshLink").click(function() {
        VendorIdToBeUpdated = $(this).attr("vendorId");
        promptLogin();
    });

    $("#div-4506 input, #div-4506 select").change(function() {
	    validate4506T();
	});

	$("#div-Appraisal input, #div-Appraisal select").change(function() {
	    determineAppraisalBtnStatus();
	});

	$("#UpdateCredentialsBtn").click(function(){
	    UpdateCredentials();
	});

	$("#ApplicationName").change(function() {
	    load4506App();
	});

	$("#DownloadSign").click(function() {
	    save4506App();
	});

	$("#SelectEdocs").click(function() {
	    f_select4506TEdocs();
	});

	$("#PlaceOrder").click(function() {
	    Submit4506TOrder();
	});

	$("#btnRun").click(function() { runPricing(); });

	$("#btnEdit").click(function() { editClosingCosts(); });

	$("#btnView").click(function() { view1003(); });

	$("#btnCancel_Appraisal").click(function() {
	    resetAppraisals();
	});

	$('#PropertySelector').change(function () {
	    SetAppraisalPropertyAddress();
	}).change();

	$("#DocAttachmentType").change(function() {
	    checkAttachDocs();
	});

	$("#login_4506, #password_4506, #accID_4506").change(function() {
	    $("#password_4506").attr("DummyPass", false);
	});

	$("#PhoneNumber, #WorkNumber, #OtherNumber").blur(function() {
	    {
	        determineAppraisalBtnStatus();
	    };
	});
	$('.additionalEmail').change(function () {
	    var bLessThan8 = $('.additionalEmail').val().split(';').length <= 8;
	    $('.additionalEmailValidator').toggle(!bLessThan8);
	});
}

function showError(message) {
    simpleDialog.alert(message, null).then(function() {  if (refreshOnPopupExit) {window.location.reload();}});
}

////Begining of Appraisal Code

function getAppraisalCredentials() {
    var data = new Object();

    if ($("#SelectedAMC").val() != "")
    {
    data["VendorId"] = $("#SelectedAMC").val();
    var result = gService.main.call("GetAppraisalCredentials", data);

    if (result && !result.error && !result.value.ErrorMessage) {
        $("#login").val(result.value["login"]);
        $("#accID").val(result.value["AccountId"]);
        $("#password").val(result.value["Password"]);

    }
    else {
        $("#login").val("");
        $("#accID").val("");
        $("#password").val("");
    }
    }
}

function checkAppraisalCredentials() {
    var connectBtn = $("#AppraisalLoginBtn");
    var valid = true;

    valid &= $("#login").val() != "";
    valid &= $("#password").val() != "";
    if ($("#accID").is(":visible")) {
        valid &= $('#accID').val() != "";
    }

    if (valid) {
        connectBtn.removeProp("disabled");
    }
    else {
        connectBtn.prop("disabled", true);
    }
}

function resetAppraisals() {

    setOrder(false, true);

    $("#SelectedAMC").removeProp("disabled");

    $("#div-Appraisal input[type='text']").each(function() {
    //disbled fields should not be cleared since they're constant
    if ($(this).attr("disabled") == "" || $(this).attr("disabled") == undefined) {
            $(this).val("");
        }
    });

    $("#div-Appraisal select").each(function() {
        $(this).prop("selectedIndex", 0);
    });

    $("#div-Appraisal input[type='checkbox']").each(function() {
        $(this).removeProp("checked");
    });

    //clear the doc ids
    document.getElementById('AttachedDocumentsIds').value = "";

    $("#documents").empty();

    $('#PropertySelector').get(0).selectedIndex = 0;
    SetAppraisalPropertyAddress();
}

function SetAppraisalPropertyAddress() {
    var selectedReoId = $('#PropertySelector').val();
    var propertyDetails = PropertyInfoDetails[selectedReoId];
    if (propertyDetails) {
        $('#PropertyAddress').val(propertyDetails.Address);
        $('#PropertyZip').val(propertyDetails.Zip);
        $('#PropertyCity').val(propertyDetails.City);
        $('#PropertyState').val(propertyDetails.State);
        $('#BorrowerName').val(propertyDetails.BorrowerName);
        $('#BorrowerEmail').val(propertyDetails.BorrowerEmail);
    }
    else {
        $('.PropertyDetails').val('');
    }
}

function promptLogin() {
    LQBPopup.ShowDivPopup($("#LoginPromptDiv"),{
        backdrop:'static',
        minWidth:400
    });
}

function UpdateCredentials()
{
    var data = new Object();
		data["Username"] = $("#LoginPrompt").val();
		data["Password"] = $("#PasswordPrompt").val();
		data["AccountId"] = $("#AccountIDPrompt").val();
		data["VendorId"] = VendorIdToBeUpdated;
		data["LoanId"] = $("#sLId").val();
		data["HasCredentials"] = true;
		var result = gService.main.call("UpdateCredentials", data);

		if (result && result.value && !result.value.ErrorMessage) {
		    window.location = gVirtualRoot + '/webapp/OrderServices.aspx?loanid=' + encodeURIComponent($("#sLId").val()) + "&tabSection=secAppraisals";
		}
		else {
		    $("#ErrorDiv").text(result.value.ErrorMessage);
		}
}
function LoadBillingMethods(billingMethods)
{
    for (var i = 0; i < billingMethods.length; i++) {
        var name = billingMethods[i].Name;
        var identifier = billingMethods[i].Identifier;
        var ccIndicator = billingMethods[i].CreditCardIndicator;
        $('#BillingMethod').append($('<option/>', { text: name, value: identifier}).data('cc-indicator', ccIndicator));
    }
}
function IsCreditCard(selectedMethod)
{
    var regex = /credit.*card/i; 
    var selectedText = selectedMethod.text();
    var creditCardIndicator = selectedMethod.data('cc-indicator');
    var isCreditCard = creditCardIndicator == 1/*E_TriState.Yes*/ || (creditCardIndicator == 0/*E_TriState.Blank*/ && regex.test(selectedText));

    return isCreditCard;
}

function loadVendorUI(result) {
    if (result && result.value && !result.value.ErrorMessage && result.value.IsValid == "True") {
        refreshOnPopupExit = false;
        $("#AMCInner").show();
        $(".LoginRow").hide();
        $("#SelectedAMC").prop("disabled", true);
        if (result.value.IsGdms == "True") {
            var propT = JSON.parse(result.value.GDMSPropertyType);
            var reportT = JSON.parse(result.value.GDMSReportType);
            var useT = JSON.parse(result.value.GDMSIntendedUse);
            var loanT = JSON.parse(result.value.GDMSLoanType);
            var occT = JSON.parse(result.value.GDMSOccupancyType);
            var billT = JSON.parse(result.value.GDMSBillingMethod);
            var client2Opt = JSON.parse(result.value.GDMSClient2);
            var processorOpt = JSON.parse(result.value.GDMSProcessor);
            var processor2Opt = JSON.parse(result.value.GDMSProcessor2);
            var orderFileUploadT = JSON.parse(result.value.GDMSOrderFileUploadFileType);

            var LoadPropertyType = $('#LoadPropertyType').val();
            var LoadIntendedUse = $('#LoadIntendedUse').val();
            var LoadLoanType = $('#LoadLoanType').val();
            var LoadOccupancyType = $('#LoadOccupancyType').val();

            $('#PropertyType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < propT.length; i++) {
                $('#PropertyType').append('<option value="' + propT[i][0] + (propT[i][1] == LoadPropertyType ? '" selected="true' : '') + '">' + propT[i][1] + '</option>');
            }
            $('.ReportType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < reportT.length; i++) {
                $('.ReportType').append('<option value="' + reportT[i][0] + '">' + reportT[i][1] + '</option>');
            }
            $('#IntendedUse').empty().append('<option value="-1"></option>');
            for (var i = 0; i < useT.length; i++) {
                $('#IntendedUse').append('<option value="' + useT[i][0] + (useT[i][1] == LoadIntendedUse ? '" selected="true' : '') + '">' + useT[i][1] + '</option>');
            }
            $('#LoanType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < loanT.length; i++) {
                $('#LoanType').append('<option value="' + loanT[i][0] + (loanT[i][1] == LoadLoanType ? '" selected="true' : '') + '">' + loanT[i][1] + '</option>');
            }
            $('#OccupancyType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < occT.length; i++) {
                $('#OccupancyType').append('<option value="' + occT[i][0] + (occT[i][1] == LoadOccupancyType ? '" selected="true' : '') + '">' + occT[i][1] + '</option>');
            }
            $('#BillingMethod').empty().append('<option value="-1"></option>');
            LoadBillingMethods(billT);

            $('#Client2').empty().append('<option value="-1"></option>');
            for (var i = 0; i < client2Opt.length; i++) {
                $('#Client2').append('<option value="' + client2Opt[i][0] + '">' + client2Opt[i][1] + '</option>');
            }
            $('#Processor').empty().append('<option value="-1"></option>');
            for (var i = 0; i < processorOpt.length; i++) {
                $('#Processor').append('<option value="' + processorOpt[i][0] + '">' + processorOpt[i][1] + '</option>');
            }
            $('#Processor2').empty().append('<option value="-1"></option>');
            for (var i = 0; i < processor2Opt.length; i++) {
                $('#Processor2').append('<option value="' + processor2Opt[i][0] + '">' + processor2Opt[i][1] + '</option>');
            }
            $('#AttachmentType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < orderFileUploadT.length; i++) {
                $('#AttachmentType').append('<option value="' + orderFileUploadT[i][0] + '">' + orderFileUploadT[i][1] + '</option>');
            }

        }
        else {
            LQBAppraisalProductList = JSON.parse(result.value.ProductsList);
            var billingMethods = JSON.parse(result.value.BillingList);

            updateProductLists();

            $('#BillingMethod').empty();
            LoadBillingMethods(billingMethods);
            if (result.value.ErrorMessage)
                showError(result.value.ErrorMessage);
        }

        $('#AttachmentTypeReq').hide();

        $("#AMCInner").show();

        $('.ProductList').off('change').change(function() {
            determineAppraisalBtnStatus();
        });

        $("#BillingMethod").change();

        determineAppraisalBtnStatus();

        TPOStyleUnification.CheckScrollbar();

        return true;
    }
    if (result) {
        var errorMsg = "Connection has failed.  Please contact your Account Executive.";
        if (result.value && result.value.ErrorMessage)
            errorMsg = result.value.ErrorMessage;
        showError(errorMsg);
    }

}

function determineAppraisalBtnStatus(forceInvalid) {
    var enable = validatePage() && !forceInvalid;
    $('#btnPlaceOrder').prop('disabled', !enable);
}

function validatePage() {

    if ($('#SelectedAMC').val() == -1)
        return false;

    var valid = true;
    var isGDMS = $("#SelectedAMC option:selected").attr("UsesGlobalDMS") == "True";
    if (isGDMS) {
        valid &= $('#ReportType').val() != -1;
        valid &= $('#IntendedUse').val() != -1;
        valid &= $('#AppraisalNeededGDMS').val() != '';
        valid &= $('#BillingMethod').val() != -1;
        if ($('#documents li').length != 0) {
            valid &= $('#AttachmentType').val() != -1;
        }
    }
    else {
        valid &= $('#ReportType').val() != "";
    }

    if ($("#login").is(":visible")) {
        valid &= $('#login').val() != "";
        valid &= $('#password').val() != "";
        if ($("#accID").is(":visible")) {
            valid &= $('#accID').val() != "";
        }
    }

    if (AppraisalNeededDateRequired[$("#SelectedAMC").val()]) {
        if (isGDMS) {
            valid &= $('#AppraisalNeededDGDMS').val() != "";
        }
        else {
            valid &= $('#AppraisalNeededD').val() != "";
        }
    }


    var selectedMethod = $('#BillingMethod').children('option').filter(':selected')
    if (IsCreditCard(selectedMethod)) {
        valid &= $('#BillingName').val() != '';
        valid &= $('#BillingAdd').val() != '';
        valid &= $('#BillingCity').val() != '';
        valid &= $('#BillingState').val() != -1;
        valid &= $('#BillingZip').val() != '';
        valid &= $('#CardType').val() != '';
        valid &= $('#CCNumber').val() != '';
        valid &= $('#Expiration_Month').val() != '';
        valid &= $('#Expiration_Year').val() != '';
        valid &= $('#Expiration_CVV').val() != '';
    }

    valid &= $('#PropertyAddress').val() != '';
    valid &= $('#PropertyCity').val() != '';
    valid &= $('#PropertyState').val() != '';
    valid &= $('#PropertyZip').val() != '';


    valid &= ($("#PhoneNumber").val() + $("#WorkNumber").val() + $("#OtherNumber").val()) != "";
    valid &= $('#ContactName').val() != '';
    valid &= $('[preset="phone"][value!=""]').length > 0;

    // Validate additional emails
    valid &= $('.additionalEmail').val().split(';').length <= 8;

    return valid;
}

function launch(target, source) {
    var windowOptions = "Height=375px, Width=400px, center=yes, scrollbar=no, status=no";
    if (source === 'AttachDocs') {
    } else if (source === 'SendEmail') {
        LQBPopup.Show(target,{
            backdrop:'static',
            minWidth:700
        });
        $(".DocPickerLink").css("display", "");
    }
    //var result = window.open(target, null, windowOptions);
}

function updateProductLists() {
    var selectedValues = [];
    selectedValues.push($('#ReportType').val());
    selectedValues.push($('#ReportType2').val());
    selectedValues.push($('#ReportType3').val());
    selectedValues.push($('#ReportType4').val());
    selectedValues.push($('#ReportType5').val());
    $('.ProductList').empty().append('<option value=""></option>');
    //for some reason, a DLL is being called 2-3 times, so its unbound just to stop the uneeded iterations
    $('.ProductList').unbind();
    var $SelectedAMC = $('#SelectedAMC');
    var AMCId = $SelectedAMC.val();
    var onlyOnePrimaryAllowed = AllowOnlyOnePrimaryProduct[AMCId];

    var primaryProduct;
    var hasPrimaryProduct;
    var primaryIndex;

    var deselectedPrimary = true;
    for (var i = 0; i < selectedValues.length; i++) {
        deselectedPrimary &= !(lastPrimaryProduct == null || lastPrimaryProduct == selectedValues[i])
    }

    if (deselectedPrimary) {
        selectedValues = [];
        selectedValues.push($(""));
        selectedValues.push($(""));
        selectedValues.push($(""));
        selectedValues.push($(""));
        selectedValues.push($(""));
    }

    if (onlyOnePrimaryAllowed) {


        //iterate through all the products, and check if you have a primary selected
        for (var i = 0; i < LQBAppraisalProductList.length; i++) {
            var product = LQBAppraisalProductList[i];
            if (!product.requires) {
                switch (product.name) {
                    case selectedValues[0]:
                        {
                            primaryProduct = selectedValues[0];
                            hasPrimaryProduct = true;
                            primaryIndex = 0;
                            break;
                        }
                    case selectedValues[1]:
                        {
                            primaryProduct = selectedValues[1];
                            hasPrimaryProduct = true;
                            primaryIndex = 1;
                            break;
                        }
                    case selectedValues[2]:
                        {
                            primaryProduct = selectedValues[2];
                            hasPrimaryProduct = true;
                            primaryIndex = 2;
                            break;
                        }
                    case selectedValues[3]:
                        {
                            primaryProduct = selectedValues[3];
                            hasPrimaryProduct = true;
                            primaryIndex = 3;
                            break;
                        }
                    case selectedValues[4]:
                        {
                            primaryProduct = selectedValues[4];
                            hasPrimaryProduct = true;
                            primaryIndex = 4;
                            break;
                        }
                }
                if (hasPrimaryProduct)
                    break;
            }
        }
    }


    for (var i = 0; i < LQBAppraisalProductList.length; i++) {
        if (LQBAppraisalProductList[i].requires) {
            //If product[i] is secondary (requires another product), make sure the required product is one of the previously selected values before adding product[i]
            var required = LQBAppraisalProductList[i].requires;
            if (selectedValues[0] != required && selectedValues[1] != required && selectedValues[2] != required && selectedValues[3] != required && selectedValues[4] != required)
                continue;
        }
        else {
            if (onlyOnePrimaryAllowed && hasPrimaryProduct && primaryProduct != LQBAppraisalProductList[i].name)
                continue;
        }
        $('.ProductList').each(function(index) {
            if (primaryProduct == LQBAppraisalProductList[i].name) {
                if (primaryIndex == index)
                    this.add(new Option(LQBAppraisalProductList[i].name, LQBAppraisalProductList[i].name, false, selectedValues[index] == LQBAppraisalProductList[i].name));
            }
            else
            //add the option to every product list; if it was the already selected one, mark it as selected
                this.add(new Option(LQBAppraisalProductList[i].name, LQBAppraisalProductList[i].name, false, selectedValues[index] == LQBAppraisalProductList[i].name));
        });
    }
    //disable all the empty lists
    $('.ProductList').each(function() {
        var size = this.options.length;
        this.disabled = size <= 1 && onlyOnePrimaryAllowed && primaryProduct != null;
        $(this).blur();
    });

    lastPrimaryProduct = primaryProduct;
}

function SubmitOrder() {
    $('#ProcessingOrder').show();
    $('#OrderAppraisalBtn').hide();

    var AppraisalNeededId = "AppraisalNeededD";
    if ($("#SelectedAMC option:selected").attr("UsesGlobalDMS") == "True")
        AppraisalNeededId += "GDMS";

    args = {
        Username : $("#login").val(),
        Password : $("#password").val(),
        AccountId : $("#accID").val(),
        SelectedAMC: $('#SelectedAMC').val(),
        LoanID: $("#sLId").val(),
        sFileVersion: $("#sFileVersion").val(),
        sLNm: $('#sLNmInfo').val(),
        sAgencyCaseNum: $('#sAgencyCaseNum').val(),
        sLPurposeT: $('#sLPurposeT').val(),
        sLT: $('#sLT').val(),
        LoanOfficerName: $('#LoanOfficerName').val(),
        BorrowerName: $('#BorrowerName').val(),
        PropertyAddress: $('#PropertyAddress').val(),
        PropertyCity: $('#PropertyCity').val(),
        PropertyState: $('#PropertyState').val(),
        PropertyZip: $('#PropertyZip').val(),
        BorrowerEmail: $('#BorrowerEmail').val(),
        ReoId: $('#PropertySelector').val(),
        ContactName: $('#ContactName').val(),
        ContactPhone: $('#PhoneNumber').val(),
        ContactWork: $('#WorkNumber').val(),
        ContactOther: $('#OtherNumber').val(),
        ContactEmail: $('#ContactEmail').val(),
        PropertyTypeId: $('#PropertyType').val(),
        PropertyTypeName: $('#PropertyType > option:selected').text(),
        ReportTypeName: $('#ReportType > option:selected').text(),
        ReportTypeId: $('#ReportType').val(),
        ReportType2Id: $('#ReportType2').val(),
        ReportType3Id: $('#ReportType3').val(),
        ReportType4Id: $('#ReportType4').val(),
        ReportType5Id: $('#ReportType5').val(),
        IntendedUseName: $('#IntendedUse > option:selected').text(),
        IntendedUseId: $('#IntendedUse').val(),
        AppraisalNeeded: $('#' + AppraisalNeededId).val(),
        Client2Id: $('#Client2').val(),
        ProcessorId: $('#Processor').val(),
        Processor2Id: $('#Processor2').val(),
        LenderName: $('#Lender').val(),
        BillingMethodId: $('#BillingMethod').val(),
        BillingMethodName: $('#BillingMethod > option:selected').text(),
        CreditCardIndicator: $('#BillingMethod > option:selected').data('cc-indicator'),
        CCBillName: $('#BillingName').val(),
        CCAddress1: $('#BillingAdd').val(),
        CCCity: $('#BillingCity').val(),
        CCState: $('#BillingState').val(),
        CCZip: $('#BillingZip').val(),
        CCType: $('#CardType').val(),
        CCNumber: $('#CCNumber').val(),
        CCExpMonth: $('#Expiration_Month').val(),
        CCExpYear: $('#Expiration_Year').val(),
        CCCode: $('#Expiration_CVV').val(),
        Notes: $('#Notes').val(),
        LoanTypeName: $('#LoanType > option:selected').text(),
        LoanTypeId: $('#LoanType').val(),
        OccupancyTypeName: $('#OccupancyType > option:selected').text(),
        OccupancyTypeId: $('#OccupancyType').val(),
        OrderNumber: $("#OrderNumber").val(),
        LQBAppraisalNeeded: $("#AppraisalNeededD").text(),
        RushOrder: $('#RushOrder').prop('checked'),
        AttachedDocumentsIds: $('#AttachedDocumentsIds').val(),
        AttachmentType: $('#AttachmentType').val(),
        AdditionalEmail: $('#AdditionalEmail').val(),
    };

    var loading = $('<loading-icon id="LQBPopupUWaitTable">Submitting order....</loading-icon>');
    TPOStyleUnification.UnifyLoadingIcons(loading);

    LQBPopup.ShowElement(loading, {
        width: 350,
        height: 200,
        hideCloseButton: true,
        onReturn: function (result) {
            HandleAppraisalResult(result);
        }
    })

    window.setTimeout(function () {
        gService.main.callAsyncSimple("SubmitOrder", args, function (result) {
            LQBPopup.Return(result);
        });
    });
}

function HandleAppraisalResult(result) {
    var noMessage = true;
    if (result && result.error) {
        showError(result.UserMessage);
        noMessage = false;
    }
    else if (result && result.value && result.value.ErrorMessage) {
        showError(result.value.ErrorMessage);
        noMessage = false;
    }
    if (result && result.value && result.value.Success == "True") {
        if (result.value.FileErrorMessage) {
            showError(result.value.FileErrorMessage);
        }
        window.location = gVirtualRoot + '/webapp/OrderServices.aspx?loanid=' + encodeURIComponent($("#sLId").val()) + "&tabSection=secAppraisals";
    }
    else if (noMessage) {
        showError("Error ordering appraisal.");
        $('#ProcessingOrder').hide();
        $('#OrderAppraisalBtn').show();
    }
    else {
        $('#ProcessingOrder').hide();
        $('#OrderAppraisalBtn').show();
    }
}

function loadBillingInfo() {
    var selectedMethod = $("#BillingMethod option:selected");
    $('#creditInfo').toggle(IsCreditCard(selectedMethod));
	TPOStyleUnification.CheckScrollbar();
}
function setOrder(isAdd,isAppraisal) {
	if(isAppraisal) {
		if (isAdd) {
			$("#div-Appraisal").show();
			$("#btnAdd_Appraisal").hide();
		} else {
			$("#div-Appraisal").hide();
			$("#btnAdd_Appraisal").show();
		}
	}
	else {
		if (isAdd) {
			$("#div-4506").show();
			$("#btnAdd").hide();
		} else {
			$("#div-4506").hide();
			$("#btnAdd").show();
		}
	}
}

function loadSignedRadio(isYes) {
	if(isYes==null) {
		$("#Yes_Cont").hide();
		$("#No_Cont").hide();
		return;
	}
	if (isYes) {
		$("#Yes_Cont").show();
		$("#No_Cont").hide();
	} else {
		$("#Yes_Cont").hide();
		$("#No_Cont").show();
	}
	TPOStyleUnification.CheckScrollbar();
}
function loadLoginInfo() {

    $("#password_4506").attr("DummyPass", true);

    if ($("#Vendor4506 option:selected").text() == "") {
        $("#loginInfo").hide();
        $("#loginEmpty").show();
    }
    else {

        var data = new Object();
        data["VendorID"] = $("#Vendor4506").val();

        var result = gService.main.call("Get4506TCredentials", data);

        $("#loginInfo").show();

        $("#login_4506").val(result.value["login"]);
        $("#password_4506").val(result.value["Password"]);
        $("#accID_4506").val(result.value["AccountId"]);


        if ($("#Vendor4506 option:selected").attr("requiresAccId") == "True") {
            $("#accID_4506").show();
            $("#accID_4506Label").show();
            $("#accID_4506Asterisk").show();
        }
        else {
            $("#accID_4506").hide();
            $("#accID_4506Label").hide();
            $("#accID_4506Asterisk").hide();
        }
    }
    TPOStyleUnification.CheckScrollbar();
}

function showAppraisalLogin() {

    $("#AMC").show();
    $("#AMCLogin").show();
    $("#AMCInner").hide();

    var usesAccountIdScript = UsesAccountIdScript[$("#SelectedAMC").val()];
    $("#AppraisalLoginBtn").show();

    if ($("#SelectedAMC").val() != "") {
        $(".LoginRow").show();
    } else {
        $(".LoginRow").hide();
    }

    if (!usesAccountIdScript) {
        $("#accID").css("display", "none");
        $("#accIDLabel").css("display", "none");
        $("#accIdAsterisk").css("display", "none");

    }
    else {
        $("#accID").css("display", "");
        $("#accIDLabel").css("display", "");
        $("#accIdAsterisk").css("display", "");
    }

    // This controls specific controls' visibility specific to GDMS or Non-GDMS
    if ($("#SelectedAMC option:selected").attr("UsesGlobalDMS") == "False") {
        $("#sAgencyCaseNum").prop("disabled", "true");

        $("[amc=1i]").css('visibility', 'visible');
        $("[amc=1o]").removeClass("hidden-item");
        $("[amc=2o]").addClass("hidden-item");
    }
    else if ($("#SelectedAMC option:selected").attr("UsesGlobalDMS") == "True") {
        $("#sAgencyCaseNum").removeProp("disabled");

        $("[amc=1i]").css('visibility', 'hidden');
        $("[amc=1o]").addClass("hidden-item");
        $("[amc=2o]").removeClass("hidden-item");
    }


}

//Back button (only in HTML5)
window.onpopstate = function(event) {
	reload();
};

//call this function to reload page when tab changes or show dilalog. (QeuryString changes)
function reload() {
    var p = $("#tabs>li.active a").attr("href")

	if (currentSec != p) {
		currentSec = p;
		tabsHandler(p);
	}
	//update reformat fields
	mask();
	setRowColor();
}
function setRowColor() {
	//http://stackoverflow.com/questions/8189182/ie-nth-child-using-odd-even-isnt-working
	$(".tb-order-service tbody tr:nth-child(even)").addClass("even");
	$(".tb-order-service tbody tr:nth-child(odd)").addClass("odd");
}
function getParameterByName(name) {
	//http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//update the url without reload whole page. Only in HTML5
function pushState(queryString) {
	if (history && history.pushState) {
		window.history.pushState(null, null, window.location.pathname + "?" + queryString);
		reload();
	} else {
		//reload the page when not in HTML5
		window.location.href = "?" + queryString;
	}
}
//Some components of MeridianLinkCommonControls.dll run incorrect in popup dialog. Create other mask.
function mask() {
	var selector = $('input[preset=ssn]');
	selector.mask('999-99-9999');
	selector.prop("pattern", "^\d{3}-?\d{2}-?\d{4}$");
	selector.prop("title", "xxx-xx-xxxx");


	selector = $('input[preset=zipcode]');
	selector.mask('99999');
	selector.prop("pattern", "\\d{5}");
	selector.prop("title", "Enter the 5 digits zip code");

	selector = $('input[preset=phone]');
	selector.mask("(999) 999-9999");
	//http://stackoverflow.com/questions/18863420/html5-pattern-for-phone-number-with-parentheses
	selector.prop("pattern", "^(?:\\(\\d{3}\\)|\\d{3})[- ]?\\d{3}[- ]?\\d{4}$");
	selector.prop("title", "(xxx) xxx-xxxx");


	selector = $('input[preset=month]');
	selector.autoNumeric('init', { vMax: '12', vMin: '0' });

	selector = $('input[preset=year]');
	selector.mask("9999");

	selector = $('input[preset=numeric]');
	selector.autoNumeric('init', { vMax: '9999', vMin: '0' });

	//https://forum.jquery.com/topic/datepicker-input-mask
	selector = $('input[preset=date]');
	selector.mask('99/99/9999');
}
//placeholder check
//http://stackoverflow.com/questions/8263891/simple-way-to-check-if-placeholder-is-supported
function placeholderIsSupported() {
    var test = document.createElement('input');
    return ('placeholder' in test);
}


//documents (tab Appraisals)
function addDocument(name) {
	$("#documents").append("<li>"+name.toUpperCase()+"<a>remove</a></li>");
}

/*********************************************************TABS MANAGEMENT*********************************************************/

function tabsHandler(p) {
    if (bypassTabClear) {
        bypassTabClear = false;
    }
    else {
        resetAppraisals();
        Reset4506T();
        resetVoa();
        resetVoe();
    }
	// Hide all content.
	$("tbody[id^='sec']").hide();
	var tabName;
	if (p !== "") {
		showTab($($("#tabs>li a").filter('[href="' + p + '"]')[0]));
	} else {
		showFirstTab();
	}
}

function selectDocumentsClick() {
    var args = {};
    $("#documents").empty();
    try {
        var ids = JSON.parse(document.getElementById('AttachedDocumentsIds').value);
        args['SelectedDocsIds'] = ids;
    }
    catch (e) { }

    var result = showModal('/webapp/OrderAppraisalDocumentPicker.aspx?loanid=' + $("#sLId").val(), args);
    if (result.DocNames && result.DocIds) {
        var text = '';
        for (var i = 0; i < result.DocNames.length; i++) {
            addDocument(result.DocNames[i], result.DocIds[i]);
        }
        document.getElementById('AttachedDocumentsIds').value = JSON.stringify(result.DocIds);
    }
    //$('#AttachmentTypeReq').toggle($('#AttachedDocuments').val() != '');
}

function showTab(tab) {

	// Remove any "active" class.
	$("#tabs>li a").removeClass("selected");
    $("#tabs>li").removeClass("active");

	// Add "active" class to selected tab.
	tab.addClass("selected");
    tab.parent().addClass("active");

	// Hide all tab content
	$("div[id^='sec']").hide();

	// Find the href attribute value to identify the active tab + content
	var activeTab = tab.attr("href");

	if (navigator.appVersion.indexOf("MSIE 9.") != -1) {
		//IE 9 loses border cell when using fadeIn
		$(activeTab).show();
	} else {
		// Fade in the active ID content.
		$(activeTab).fadeIn();
	}


}

function showFirstTab() {
	// Activate first tab.
	$("#tabs>li a:first").addClass("selected").show();
    $("#tabs>li a:first").parent().addClass("active");

	// Show first tab content.
	$("tbody[id^='sec']:first").show();
}


///End of Mostly Appraisal Code

///Beginning of 4506-T Code
    function load4506App()
    {
        var data = new Object();
        data["sLId"] = $("#sLId").val();
	    data["appId"] = $("#ApplicationName").val();

	    var result = gService.main.call("Load4506T", data);
	    populateForm(result.value, "");
	}

	function save4506App() {
	    var data = new Object();

	    data["sLId"] = $("#sLId").val();
	    data["AppId"] = $("#ApplicationName").val();

	    data["sFileVersion"] = $("#sFileVersion").val();

	    data["aB4506TNm"] = $("#aB4506TNm").val();
	    data["aB4506TSsnTinEin"] = $("#aB4506TSsnTinEin").val();
	    data["aC4506TNm"] = $("#aC4506TNm").val();
	    data["aC4506TSsnTinEin"] = $("#aC4506TSsnTinEin").val();
	    data["aB4506TNmLckd"] = $("#aB4506TNmLckd").prop("checked");
	    data["aC4506TNmLckd"] = $("#aC4506TNmLckd").prop("checked");

	    data["a4506TPrevStreetAddr"] = $("#a4506TPrevStreetAddr").val();
	    data["a4506TPrevCity"] = $("#a4506TPrevCity").val();
	    data["a4506TPrevState"] = $("#a4506TPrevState").val();
	    data["a4506TPrevZip"] = $("#a4506TPrevZip").val();
	    data["a4506TThirdPartyName"] = $("#a4506TThirdPartyName").val();
	    data["a4506TThirdPartyStreetAddr"] = $("#a4506TThirdPartyStreetAddr").val();
	    data["a4506TThirdPartyCity"] = $("#a4506TThirdPartyCity").val();
	    data["a4506TThirdPartyState"] = $("#a4506TThirdPartyState").val();
	    data["a4506TThirdPartyZip"] = $("#a4506TThirdPartyZip").val();
	    data["a4506TThirdPartyPhone"] = $("#a4506TThirdPartyPhone").val();
	    data["a4506TTranscript"] = $("#a4506TTranscript").val();
	    data["a4056TIsReturnTranscript"] = $("#a4056TIsReturnTranscript").prop("checked");
	    data["a4506TIsAccountTranscript"] = $("#a4506TIsAccountTranscript").prop("checked");
	    data["a4056TIsRecordAccountTranscript"] = $("#a4056TIsRecordAccountTranscript").prop("checked");
	    data["a4506TVerificationNonfiling"] = $("#a4506TVerificationNonfiling").prop("checked");
	    data["a4506TSeriesTranscript"] = $("#a4506TSeriesTranscript").prop("checked");
	    data["a4506TYear1"] = $("#a4506TYear1").val();
	    data["a4506TYear2"] = $("#a4506TYear2").val();
	    data["a4506TYear3"] = $("#a4506TYear3").val();
	    data["a4506TYear4"] = $("#a4506TYear4").val();
	    data["a4506TRequestYrHadIdentityTheft"] = $("#a4506TRequestYrHadIdentityTheft").prop("checked");

	    var result = gService.main.call("Save4506T", data);

        if (result && result.value && !result.value.ErrorMessage) {
            var url = gVirtualRoot + "/pdf/IRS_4506_T.aspx?loanid=" + $("#sLId").val() + "&printoptions=&crack=" + new Date() + "&applicationid=" + $("#ApplicationName").val() + "&passwordid=";
            LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
	    }
	}

    function populateForm(obj, clientID) {

    for (property in obj) {
        var val = obj[property];
        $("#" + property).each(function() {
            var $this = jQuery(this);
            var type = $this.prop('type');
            if (type == 'radio') {
                if ($this.val() == val) {
                    $this.prop('checked', true);
                }
            } else if (type == 'checkbox') {
                $this.prop('checked', val == 'True');
            } else {
                $this.val(val);
            }
            var tagName = $this.prop('tagName');
            if (tagName == "SPAN" || tagName == "LABEL") {
                $this.text(val);
            }
        });
    }
}

function f_select4506TEdocs() {
    $(".DocPickerCheckbox").css("display", "none");
    $(".DocPickerLink").css("display", "");
    _showDocumentPicker();
}

function toggleStatusDetails(link) {
    var jqLink = $(link);

    if (jqLink.data('open')) {
        jqLink.siblings('#StatusDetailsContainer').html('');
        jqLink.html('details');
        jqLink.data('open', false);
    }
    else {
        jqLink.siblings('#StatusDetailsContainer').html(jqLink.prop('title'));
        jqLink.html('hide');
        jqLink.data('open', true);
    }
}

var directlyAttaching;
function selectAppraisalDoc() {
    directlyAttaching = false;
    $(".DocPickerCheckbox").css("display", "");
    $(".DocPickerLink").css("display", "none");
    _showDocumentPicker(attachDocSelected);
}

function selectAttachmentDoc() {
    directlyAttaching = true;
    $(".DocPickerCheckbox").css("display", "");
    $(".DocPickerLink").css("display", "none");
    _showDocumentPicker(attachDocSelected);
}

function _showDocumentPicker(onOk) {
    LQBPopup.ShowElementWithWrapper($("#DocumentPickerDiv"), {
        headerText: "Select Document",
        popupClasses: 'modal-order-services',
        backdrop: 'static',
        closeText: "Cancel",
        buttons: onOk == null ? undefined : [{ OK: onOk }],
    });

    TPOStyleUnification.fixedHeaderTable(document.getElementById("DocTable"));
}

function Submit4506TOrder() {
    clearDirty();
    var args = {
        LoanID : $("#sLId").val(),
        AppID: $("#ApplicationName").val(),
        AppName :$("#ApplicationName").val() + ":B:" + $("#ApplicationName option:selected").text(),
        VendorId : $("#Vendor4506").val(),
        DocumentID: g_eDocumentId,
        Username: $("#login_4506").val(),
        Password: $("#password_4506").val(),
        AccountId: $("#accID_4506").val(),
        HasCredentials: $("#password_4506").attr("DummyPass") != "true"
    };

    var loading = $('<loading-icon id="LQBPopupUWaitTable">Submitting order....</loading-icon>');
    TPOStyleUnification.UnifyLoadingIcons(loading);

    LQBPopup.ShowElement(loading, {
        width: 350,
        height: 200,
        hideCloseButton: true,
        onReturn: function (result) {
            Handle4506TResult(result);
        }
    });

    window.setTimeout(function () {
        gService.main.callAsyncSimple("Submit4506TOrder", args, function (result) {
            LQBPopup.Return(result);
        });
    });
}

function Handle4506TResult(result) {
    if (!result.error) {
        if (result.value.Status === "OK") {
            location.href = location.href;
        }
        else {
            showError('Error: ' + result.value.ErrorMessage);
        }
    }
    else {
        showError('Error: ' + (result.UserMessage ? result.UserMessage : 'System Error.'));

        //display the login controls since it will most likely be an incorrect login error
        $("#loginInfo").show();
        $("#loginEmpty").hide();

        if ($("#Vendor4506 option:selected").attr("requiresAccId") == "True") {
            $("#accID_4506").show();
            $("#accID_4506Label").show();
            $("#accID_4506Asterisk").show();
        }
        else {
            $("#accID_4506").hide();
            $("#accID_4506Label").hide();
            $("#accID_4506Asterisk").hide();
        }
    }
}

function Check4506TStatus(button, orderNumber, vendorId) {
    $(button).prop('disabled', true);

    var args = {
        LoanId: ML.sLId,
        OrderNumber: orderNumber,
        VendorId: vendorId,
        Username: $("#login_4506").val(),
        Password: $("#password_4506").val(),
        AccountId: $("#accID_4506").val(),
        HasCredentials: $("#password_4506").attr("DummyPass") != "true"
    };

    var result = gService.main.acall(
        /* methodName */ "Check4506TOrderStatus",
        args,
        /* successCallback */ function (result) { Check4506TStatusSuccess(result); $(button).prop('disabled', false); },
        /* errorCallback */ function (result) { Check4506TStatusFailure(result); $(button).prop('disabled', false); },
        /* keepQuiet */ false,
        /* bUseParentForError */ false,
        /* dontHandleError*/ true);
}

function Check4506TStatusSuccess(result)
{
    if (result.value.Status === "Error")
    {
        showError('Error: ' + result.value.ErrorMessage);
    }
    else if (result.value.Status === "Unknown")
    {
        showError(result.value.StatusMessage);
    }
    else
    {
        location.href = location.href;
    }
}

function Check4506TStatusFailure(result)
{
    if (result.UserMessage)
    {
        showError('Error: ' + result.UserMessage);
    }
    else {
        showError('Error: System error.');
    }
}

function Reset4506T() {
    setOrder(false, false);
    loadSignedRadio(null);

    g_eDocumentId = "";
    $('#SignReqForm').val("");

    $("#Vendor4506").val("");
    $("#ApplicationName").prop("selectedIndex", 0);

    $("#Signed_Yes").removeProp("checked");
    $("#Signed_No").removeProp("checked");

    $("#login_4506").val("");
    $("#password_4506").val("");
    $("#accID_4506").val("");


}
///End of 4506-T Code


/// Begin DocPicker Code

var g_rows, timeoutID;
var data = [];


    function initDocPicker() {

        var cbx = $('input.picked');

    $('#DocTable').tablesorter({ headers: { 0: { sorter: false }, 2: { sorter: 'text' },5: { sorter: false}} })
        .bind("sortEnd", function(){
                var alt = false;
                $('#DocTable tbody tr').each(function(i,o){
                    if ( alt ) { $(o).removeClass('GridItem').addClass('GridAlternatingItem');}
                    else { $(o).removeClass('GridAlternatingItem').addClass('GridItem'); }
                    alt = !alt;
                });
            });
    g_rows = $('#DocTable tbody tr');
    var isAlt = false;
    g_rows.each(function (i, o) {
                    var _obj = $(o);
                var $tds = _obj.children('td');

                str = $tds.eq(2).text().toLowerCase() + '~' + $tds.eq(3).text().toLowerCase() + '~' + $tds.eq(4).text().toLowerCase();
                data.push(str);
                _obj.addClass(isAlt ? 'odd' : 'even');
                isAlt = !isAlt;
    });
    $('#Query').keyup (function(){
        if (timeoutID) {
            clearTimeout(timeoutID);
        }

        timeoutID = setTimeout(searchTable, 500);
    });

    $('#selectAll').change(function() {
        var $o = $(this);

        if ($o.is(':checked')) {
            cbx.filter(':visible').prop('checked', true);
        }
        else {
            cbx.prop('checked', false);
        }

        cbx.change();
    });

      $('#DocTypeFilter').change(filterStatus);
  }

function attachDocSelected() {
    var cbx = $('input.picked');
    var docIds = [];
    var docNames = [];
    cbx.filter(':checked').each(function() {
        docIds.push($(this).data('docid'));
        docNames.push($(this).data('docname'));
    });

    if (!directlyAttaching) {
        if (docNames && docIds) {
            var text = '';
            for (var i = 0; i < docNames.length; i++) {
                addDocument(docNames[i], docIds[i]);
            }
            document.getElementById('AttachedDocumentsIds').value = JSON.stringify(docIds);
        }
    }

    else {
        if (docNames && docIds) {
            var text = '';
            for (var i = 0; i < docNames.length; i++) {
                text += docNames[i] + '\n';
            }
            document.getElementById('AttachedDocuments').value = text;
            document.getElementById('AttachedDocumentsIds').value = JSON.stringify(docIds);

            checkAttachDocs();
        }
    }
};

  function checkAttachDocs() {
      var valid = true;
      valid &= $('#AttachedDocuments').val() != "";
      valid &= $('#AttachedDocumentsIds').val() != "";
      valid &= (usingGlobalDMS != "True" || $('#DocAttachmentType').val() != "-1");

      if (valid) {
          $("#AttachDocsBtn").removeProp("disabled");
      }
      else {
          $("#AttachDocsBtn").prop("disabled", "disabled");
      }
  }

  function attachDocsClick() {
      $('#AttachDocsBtn').hide();
      $('#CloseWindowBtn').hide();
      $('#ProcessingOrder').show();
      window.setTimeout(function() {
          var args = {
              LoanId: $("#sLId").val(),
              VendorId: attachVendorId,
              AppraisalOrderNumber: attachDocsOrderNumber,
              AttachedDocumentsIds: $('#AttachedDocumentsIds').val(),
              AttachmentType: $('#DocAttachmentType').val()
          };
          var result;
          if (usingGlobalDMS == "True") {
              result = gService.main.call("AttachGDMSDocuments", args);
          } else {
              result = gService.main.call("AttachFrameworkDocuments", args);
          }

          var noMessage = true;
          if (result && result.value) {
              if (result.value.FileErrorMessage) {
                  showError(result.value.FileErrorMessage);
                  noMessage = false;
              }
              else if (result.value.ErrorMessage) {
                  showError(result.value.ErrorMessage);
                  noMessage = false;
              }
          }
          if (result && result.value && result.value.Success == "True") {
              showError("Document(s) successfully attached");
              $('#AttachedDocumentsIds').val("");
              $('#AttachedDocuments').val("");
              $('#AttachDocsBtn').attr('disabled', true);
          }
          else if (noMessage) {
              showError("Error adding documents to appraisal.");
          }
          $('#ProcessingOrder').hide();
          $('#AttachDocsBtn').show();
          $('#CloseWindowBtn').show();
      }, 0);
  }

function selectEdoc(docId, docName) {

    g_eDocumentId = docId;
    $('#SignReqForm').val(docName);
    LQBPopup.Hide();
    validate4506T();
    return false;
}
function _close() {
    window.dialogArguments.OK = false;
    window.close();
}

function viewEdoc(docid) {
window.open(EdocsUrl + '?docid=' + docid , '_self');
}

function searchTable() {
    var value = $('#Query').val(), i = 0, rowCount = data.length, row;
    if (value.length == 0) {
        g_rows.show();
        return;
    }

    value = value.toLowerCase();

    for (i; i < rowCount; i++) {
        row = data[i];
        if (row.indexOf(value) == -1) {
            g_rows.eq(i).hide();
        }
        else {
            g_rows.eq(i).show();
        }
    }
}

function filterStatus() {
    var value = $('#DocTypeFilter').val(), i = 0, rowCount = data.length, rowType;
    if (value == 'All') {
        g_rows.show();
    }
    else {
        for (i; i < rowCount; i++) {
            rowType = g_rows.eq(i).children('td').eq(1).text().replace(/^\s+|\s+$/g, '');
            if (rowType.toLowerCase() == value.toLowerCase()) {
                g_rows.eq(i).show();
            }
            else {
                g_rows.eq(i).hide();
            }
        }
    }
    TPOStyleUnification.fixedHeaderTable(document.getElementById("DocTable"));
}

var attachDocsOrderNumber;
var attachVendorId;
var usingGlobalDMS;
function attachDocsToAppraisal(orderNumber, vendorId, usesGlobalDMS) {
    attachDocsOrderNumber = orderNumber;
    attachVendorId = vendorId;
    usingGlobalDMS = usesGlobalDMS;

    if (usingGlobalDMS != "True") {
        $("#AttachmentTypeSection").hide();
    }

    LQBPopup.ShowElementWithWrapper($("#DocumentAttachDiv"),{
        headerText: "Attach Documents",
        backdrop:'static',
        minWidth:500,
        closeText: "close",
        onClose:function(){
            document.getElementById('AttachedDocuments').value = "";
            document.getElementById('AttachedDocumentsIds').value = "";
        },
        buttons:[{
                    'Attach' : function(){
                                        attachDocsClick();
                                     }
                  }],
        onShown: function(node){
                        var $doneButton = node.find('button[name="Attach"]');
                        $doneButton.attr( 'id', "AttachDocsBtn");
                        $doneButton.attr("disabled", "true");
                    }
    });

}

function validate4506T() {
    var valid = true;

    valid &= $("#Vendor4506").val() != "";

    if ($("#login_4506").is(":visible")) {
        valid &= $("#login_4506").val() != "";
        valid &= $("#password_4506").val() != "";

        if ($("#accID_4506").is(":visible")) {
            valid &= $("#accID_4506").val() != "";
        }
    }

    valid &= $("#SignReqForm").val() != "";

    if (valid) {
        $("#PlaceOrder").removeAttr("disabled")
    }
    else {
        $("#PlaceOrder").attr("disabled", "true")
    }
}
/// End DocPicker Code
