﻿var TabManager = (function () {
    var startUpScripts = [];
    function registerStartUpScript(functionToCall) {
        if (typeof functionToCall === 'function') {
            startUpScripts.push(functionToCall);
        }
    }

    function runStartUp() {
        for (var i = 0; i < startUpScripts.length; ++i) {
            startUpScripts[i]();
        }
    }

    var validationFunctions = [];
    function registerValidationFunction(validationFunction) {
        if (typeof validationFunction === 'function') {
            validationFunctions.push(validationFunction);
        }
    }

    function runValidationFunction() {
        var validated = true;
        for (var i = 0; i < validationFunctions.length; ++i) {
            validated = validationFunctions[i]() && validated;
        }
        return validated;
    }

    return {
        registerStartUpScript: registerStartUpScript,
        runStartUp: runStartUp,
        registerValidationFunction: registerValidationFunction,
        runValidationFunction: runValidationFunction
    }
})();

TabManager.registerStartUpScript(function () {
    document.getElementById('_ClientID').value = ML.LoanInformationTabId;
    var activeTabLink = document.getElementById(ML.LoanInformationTabId + 'TabLink');
    if (activeTabLink) {
        TPOStyleUnification.TabClick(activeTabLink);
        $(document.getElementById(ML.LoanInformationTabId + 'Tab')).removeClass('hide');
    }
});

function changeTab(element, link) {
    promptExplicitSave(ML.isSaveButtonEnabled, isDirty(), function (navigationFunction) { // save function
        var resultMessage = attemptSave();
        if (ML.isSaveButtonEnabled) {
            promptExplicitSaveCallback(resultMessage, navigationFunction, ML.isSaveButtonEnabled);
        }
        else {
            return normalSaveCallback(resultMessage);
        }
    }, function () { // function that switches tabs
        TPOStyleUnification.TabClick(element);
        document.location = link;
    });
}

// Run the setup function once the page is ready so the
// root div is defined on the page.
$(function () {
    TabManager.runStartUp();
    gService.register(VRoot + '/webapp/LoanInformationService.aspx', 'LoanInformationService');
    setTimeout(TPOStyleUnification.Components);
    checkValidation();
});

function checkValidation() {
    TabManager.runValidationFunction();
}

function updateDirtyBit_callback() {
    checkValidation();
}

function attemptSave() {
    if (isDirty()) {
        if (!TabManager.runValidationFunction()) {
            return 'Page is invalid. Could not save.';
        }

        var errorMessage = savePage();
        if (errorMessage) {
            return errorMessage;
        }
    }

    return null;
}

function savePage() {
    var clientID = null;
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value;

    var sFileVersion = null;
    if (document.getElementById("sFileVersion") != null)
        sFileVersion = document.getElementById("sFileVersion").value;

    var sLId = null;
    if (document.getElementById("sLId") != null)
        sLId = document.getElementById("sLId").value;

    var args = getAllFormValues(clientID);
    if (typeof (_postGetAllFormValues) == 'function') _postGetAllFormValues(args);
    args["_ClientID"] = clientID;
    args["sFileVersion"] = sFileVersion;
    args["loanid"] = sLId;
    var method = "SaveData";
    if (clientID != null && clientID != "") {
        method = clientID + "_" + method;
    }

    var result = gService.LoanInformationService.call(method, args);

    if (!result.error && result.value["ErrorMessage"] != undefined) {
        return result.value["ErrorMessage"];
    }

    clearDirty();
    return null;
}

function refreshCalculation() {
    // Copied here and adapted from LendersOfficeApp/inc/loanframework2.js:backgroundCalculation()
    var clientID = null;
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value;
    // Since recalculate does not always require every field to be passed to server:
    // Add skipMe attribute to inputs that are not required in calculations.
    var args = getAllFormValues(clientID, true);
    if (typeof (_postGetAllFormValues) == "function") {
        _postGetAllFormValues(args);
    }

    var sFileVersion = null;
    if (document.getElementById("sFileVersion") != null)
        sFileVersion = document.getElementById("sFileVersion").value;

    var sLId = null;
    if (document.getElementById("sLId") != null)
        sLId = document.getElementById("sLId").value;

    args["_ClientID"] = clientID;
    args["sFileVersion"] = sFileVersion;
    args["loanid"] = sLId;
    var method = "CalculateData";
    if (clientID != null && clientID != "")
        method = clientID + "_CalculateData";

    var result = gService.LoanInformationService.call(method, args);
    if (!result.error) {
        populateForm(result.value, clientID);
        if (typeof (_postRefreshCalculation) == "function") {
            _postRefreshCalculation(result.value, clientID);
        }
    }
    else {

        var errMsg = 'Unable to process data. Please try again.';

        if (null != result.UserMessage)
            errMsg = result.UserMessage;

        if (typeof (_onError) == "function")
            _onError(errMsg);
        else
            alert(errMsg);
        return false;
    }
    // End code from loanframework2.js
}

function lockFieldCustom(lockedCheckbox, lockedFieldInput) {
    lockedFieldInput.prop('readOnly', !lockedCheckbox.prop('checked')).toggleClass('readonly', !lockedCheckbox.prop('checked'));
    if (lockedFieldInput.is('select')) {
        lockedFieldInput.prop('disabled', !lockedCheckbox.prop('checked'));
    }
}
