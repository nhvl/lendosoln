﻿var FileInfo = (function($) {
    function init() {
        // This special binding is necessary since we don't use the actual field ID for the elements on this control
        // Second, we don't update <span>s when the model is changed.
        var updateOnLoanFieldChange = function(originalFieldId, fileInfoFieldId) {
            $(window).on('loan/changed/' + originalFieldId,
                function() { $('#' + fileInfoFieldId).text(PML.model.loan[originalFieldId]); }
            );
        }
        var updateOnPrimaryFieldChange = function(originalFieldId, fileInfoFieldId) {
            $(window).on('apps/changed/' + originalFieldId, function(e, appNum) {
                if (appNum != 0) { return; }
                $('#' + fileInfoFieldId).text(PML.model.apps[0][originalFieldId]);
            });
        }
        var updateOnNameFieldChange = function(originalFieldId, fileInfoFieldId) {
            $(window).on('apps/changed/' + originalFieldId, function(e, appNum) {
                if (appNum != 0) { return; }

                var app = PML.model.apps[0];
                var name = app['aBFirstNm'] + ' ' + app['aBMidNm'] + ' ' + app['aBLastNm'];
                if (app['aBSuffix'] && app['aBSuffix'].length > 0) {
                    name += ', ' + app['aBSuffix'];
                }
                $('#' + fileInfoFieldId).text(name);
            });
        }

        var isEmpty = function(str) {
            return (!str || 0 === str.length);
        }
        // This one's needed due to special logic
        var updateOnPrimaryPhoneFieldChange = function(originalFieldId, fileInfoFieldId) {
            $(window).on('apps/changed/' + originalFieldId, function(e, appNum) {
                if (appNum != 0) { return; }
                var app = PML.model.apps[0];
                var phone = '';

                if (!isEmpty(app['aBHPhone'])) { // home
                    phone = app['aBHPhone'];
                } else if (!isEmpty(app['aBBusPhone'])) { // work
                    phone = app['aBBusPhone'];
                } else if (!isEmpty(app['aBCellPhone'])) { // cell
                    phone = app['aBCellPhone'];
                }
                $('#' + fileInfoFieldId).text(phone);
            });
        }

        updateOnLoanFieldChange('sLNm', 'sLNm');
        updateOnLoanFieldChange('sCreditScoreType2', 'sCreditScoreType2');
        updateOnLoanFieldChange('sStatusT', 'sStatusT');
        
        updateOnNameFieldChange('aBFirstNm', 'primary_aBNm');
        updateOnNameFieldChange('aBMidNm', 'primary_aBNm');
        updateOnNameFieldChange('aBLastNm', 'primary_aBNm');
        updateOnNameFieldChange('aBSuffix', 'primary_aBNm');

        updateOnPrimaryPhoneFieldChange('aBHPhone', 'primary_aBHPhone');
        updateOnPrimaryPhoneFieldChange('aBBusPhone', 'primary_aBHPhone');
        updateOnPrimaryPhoneFieldChange('aBCellPhone', 'primary_aBHPhone');

        updateOnPrimaryFieldChange('aBEmail', 'primary_aBEmail');
    }

    return {
        init: init
    };

} (jQuery));