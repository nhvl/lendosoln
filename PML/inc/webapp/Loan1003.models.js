﻿var $ = jQuery;

function Asset() {
     
    this.OwnerT         =  0; // Owner :: int
    this.AssetT         =  0; // Asset Type :: int
    this.ComNm          = ""; //Company Name
    this.DepartmentName = "";
    this.Desc           = ""; // Description for Asset Type = Other
    this.StAddr         = "";
    this.City           = "";
    this.State          = "";
    this.Zip            = "";
    this.AccNum         = ""; // Account Number for Asset type =/= Auto | other non
    this.Val            =  0; // Asset Value
    this.recordID = ""; 
    // for RE asset
    this.Addr                = 0; // Address
    this.MAmt                = 0; // Mortgage Amount
    this.GrossRentI          = 0; // Gross Rent for StatT=Rental
    this.MPmt                = 0; // Mortgage Payment
    this.HExp                = 0; // Ins/Maint/Taxes
    this.OccR                = 0; // Occ. Rate for StatT=Rental
    this.Type                = 0; // Asset Property Type
    this.StatT               = 0; // Status: 0-Residence; 1-Sale; 2-Pending Sale; 3-Rental
    this.IsSubjectProp       = 0; // for loan.sLPurposeT =/= Purchase a home
    this.IsPrimaryResidence  = 0; //
	
}

function Employment() {
	this.IsSelfEmplmt = 0;
	this.EmplrNm = "Name";
	this.EmplrBusPhone = "";
	this.EmplrAddr = "";
	this.EmplrZip = "";
	this.EmplrCity = "";
	this.EmplrState = "";
	this.JobTitle = "";
	this.EmplmtStartD = "";
	this.EmplmtEndD = "";
	this.RecordID = "";
	this.ProfStartD = "";
}