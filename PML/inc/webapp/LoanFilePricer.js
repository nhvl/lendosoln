﻿var bIsPurchaseLoan;
var bNoApps;
var bIsEnableRenovationCheckboxInPML;
var bIsDisplayOriginatorCompensation;
var bIsEnabledLockPeriodDropDown;
var bIsProduction;

var bInitialized = false;

var validator = {
    'sSpAddr': false,
    'sSpZip': false,
    'sSpStatePe': false,
    'sSpCounty': false,
    'sSpCity': false,
    'sProdCondoStories': false,
    'sTotalRenovationCosts': false,
    'sProdCashoutAmt': false,
    'sProdCurrPIPmt': false,
    'sProdCurrMIPMo': false,
    'sSpLien': false,
    'sApprValPe': false,
    'sHouseValPe': false,
    'sAsIsAppraisedValuePeval': false,
    'sProdRLckdDays': false,
    'sCreditScoreEstimatePe': false,
    'sPrimAppTotNonspIPe': false,
    'sNumFinancedProperties': false,
    'sAppTotLiqAsset': false,
    'sPresOHExpPe': false,
    'sPriorSalesD': false,
    'sPriorSalesPrice': false,
    'sPriorSalesSellerT': false,
    'sConvSplitMIRT': false,
    'sFHASalesConcessions': false,
    'sOriginalAppraisedValue': false,
    'sLAmtCalcPe': false,
    'sProOFinBalPe': false,
    'sSubFinPe': false,
    'sOwnerTitleInsFPe': false,
    'sOriginatorCompensationBorrTotalAmount': false,
    // Assume these are valid until first validation because some
    // of them may not make it to the DOM.
    'sCustomPMLField1': true,
    'sCustomPMLField2': true,
    'sCustomPMLField3': true,
    'sCustomPMLField4': true,
    'sCustomPMLField5': true,
    'sCustomPMLField6': true,
    'sCustomPMLField7': true,
    'sCustomPMLField8': true,
    'sCustomPMLField9': true,
    'sCustomPMLField10': true,
    'sCustomPMLField11': true,
    'sCustomPMLField12': true,
    'sCustomPMLField13': true,
    'sCustomPMLField14': true,
    'sCustomPMLField15': true,
    'sCustomPMLField16': true,
    'sCustomPMLField17': true,
    'sCustomPMLField18': true,
    'sCustomPMLField19': true,
    'sCustomPMLField20': true,

    'sCorrespondentProcessT': false,
    'sProdConvMIOptionT': false
};

var oldSmartZipCode_postServiceCall = window.smartZipcode_postSeviceCall;
window.smartZipcode_postSeviceCall = function (result, zipcode, city, state, county) {
    oldSmartZipCode_postServiceCall(result, zipcode, city, state, county);
    PML.refreshFieldData();
}


function f_updateTexas50a6Checkbox() {
    var sLPurposeTPe = PML.model.loan['sLPurposeTPe'];
    var sSpStatePe = PML.model.loan['sSpStatePe'];
    var sOccTPe = PML.model.loan['sOccTPe'];
    var sProdCashoutAmt = f_getMoneyValue(PML.model.loan['sProdCashoutAmt']);
    var $cb = $j('#sProdIsTexas50a6Loan');
    var bIsDisableAutoCalcOfTexas50a6 = PML.options.IsDisableAutoCalcOfTexas50a6;

    var bVisible = false;
    var bCheck = false;
    if (!PML.options.IsStandAloneSecond) {
        bVisible = sSpStatePe === 'TX' && sLPurposeTPe != E_sLPurposeT_Purchase;
        bCheck = (sLPurposeTPe === E_sLPurposeT_RefinCashout || sLPurposeTPe === PML.constants.E_sLPurposeT.HomeEquity.toString())
            && sOccTPe === E_aOccT_PrimaryResidence;
    }
    else {
        bVisible = sSpStatePe === 'TX';
        bCheck = sProdCashoutAmt > 0 && sOccTPe === E_aOccT_PrimaryResidence;
    }

    $j('.Texas50a6Panel').toggle(bVisible);

    if (bVisible && bCheck && !bIsDisableAutoCalcOfTexas50a6) {
        $cb.prop('checked', true).change()
            .prop('disabled', true);
    }
    else {
        if (!bVisible) {
            $cb.prop('checked', false).change();
        }
        
        $cb.prop('disabled', false);
    }
}

function setAsUserInput(event) {
    if (event.originalEvent != undefined) {
        $('#sProdConvMIOptionT').removeAttr('requires-user-update');
    }
}

function f_onchange_sProdConvMIOptionT(event) {
    var $ddl = $('#sProdConvMIOptionT');
    if ($ddl.attr('requires-user-update')) {
        $ddl.val(E_sProdConvMIOptionT_Blank);
        PML.model.loan['sProdConvMIOptionT'] = E_sProdConvMIOptionT_Blank;
    }
    
    var selectedVal = PML.model.loan['sProdConvMIOptionT'];
    
    // If it is Borrower Paid Split Premium, then show the Split Mi Upfront Premium DDL
    $j('#SplitMIUpfrontPremiumPanel').toggle(selectedVal == E_sProdConvMIOptionT_BorrPaidSplitPrem);

    if (selectedVal != E_sProdConvMIOptionT_BorrPaidSplitPrem) {
        $j('#sConvSplitMIRT').val(E_sConvSplitMIRT_Blank);
        PML.model.loan['sConvSplitMIRT'] = E_sConvSplitMIRT_Blank;
    }

    f_validate_sConvSplitMIRT();
    validatesProdConvMIOptionT();
    f_toggleButtons();
}

function f_onchange_sProdSpT() {
    // Condo/pud/units 2-4 default -> attached, else detached
    var selectedVal = PML.model.loan['sProdSpT'];
    var list = E_sProdSpT_array;
    var arrayIndex = $j.inArray(selectedVal, list);
    var isCondo = selectedVal == E_sProdSpT_Condo;
    var isPUD = selectedVal == E_sProdSpT_PUD;
    var isManufactured = selectedVal == E_sProdSpT_Manufactured;

    if (bInitialized) { // OPM 170911.  Init setting should use DB.
        if ($j.inArray(selectedVal, list) >= 0) {
            $j('#sProdSpStructureT').val(E_sProdSpStructureT_Attached);
            PML.model.loan['sProdSpStructureT'] = E_sProdSpStructureT_Attached;
        }
        else {
            $j('#sProdSpStructureT').val(E_sProdSpStructureT_Detached);
            PML.model.loan['sProdSpStructureT'] = E_sProdSpStructureT_Detached;
        }
    }

    // If it is manufactured, set "Not Permanently Affixed?" and "Is home MH Advantage?" visible.
    $j('.manufactured-datapoint').toggle(isManufactured);

    // If it is condo or PUD, set Non-warrantable project visible
    $j('#NonWarrantableProj').toggle(isCondo || isPUD);
    
    // If it is condo, display condo panel and condotel
    $j('#CondoPanel').toggle(isCondo);
    $j('#CondotelPanel').toggle(isCondo);
    
    // Validate Condo stories.
    f_validate_sProdCondoStories();
    f_toggleButtons();
}

function f_toggleOriginalAppraisedValue() {
    // Show Original appraised value when FHA Streamline and Appraisal not checked
    var bIsRefinanceLoan = f_isRefinanceLoan();
    var bFHAStreamline = PML.model.loan['sLPurposeTPe'] == E_sLPurposeT_FhaStreamlinedRefinance;
    var bAppraisalChecked = $j('#sHasAppraisal').prop('checked');
    $j('#OriginalAppraisedValuePanel').toggle(bFHAStreamline && bAppraisalChecked == false && bIsRefinanceLoan);
}

function f_toggleDURefiPlusCB() {
    // Show DU Refi Plus CB when this is Refi R/T and DU AUS Response selected
    var index = $j.inArray(PML.model.loan['sProd3rdPartyUwResultT'], E_sProd3rdPartyUwResultT_DU);
    var bDUAUSResponseSelected = (index >= 0);
    var bRefiRT = PML.model.loan['sLPurposeTPe'] == E_sLPurposeT_Refin;
    var bIsRefinanceLoan = f_isRefinanceLoan();
    var bVisible = bDUAUSResponseSelected && bRefiRT && bIsRefinanceLoan;
    $j('#DURefiPlusPanel').toggle(bVisible);
    if (!bVisible) {
        $j('#sProdIsDuRefiPlus').prop('checked', false).change();
    }
}

function f_togglePriorSalesExtraInfoPanel() {
    var bPriorSalesDVisible = bIsPurchaseLoan; // only visible for purchase loans
    var sPriorSalesD = PML.model.loan['sPriorSalesD'];
    var bPriorSalesDValid = f_validatePriorSalesD(sPriorSalesD);
    var bBlankOrDefault = sPriorSalesD == '' || sPriorSalesD == 'mm/dd/yyyy';
    $j('#PriorSalesExtraInfoPanel').toggle(bPriorSalesDValid && !bBlankOrDefault && bPriorSalesDVisible);
}

function f_onchange_sLPurposeTPe() {
    var bIsRefinanceLoan = f_isRefinanceLoan();
    var selectedVal = PML.model.loan['sLPurposeTPe'];

    // Show cashout amount if standalone second or type is Refinance Cashout
    $j('#CashoutAmtPanel').toggle(PML.options.IsStandAloneSecond || ((selectedVal == E_sLPurposeT_RefinCashout) && bIsRefinanceLoan));
    
    // Show support for student loan cash-out if type is  Refinance Cashout
    $j('#sIsStudentLoanCashoutRefiPanel').toggle((selectedVal == E_sLPurposeT_RefinCashout) && bIsRefinanceLoan);

    // Show CreditQual,Appraisal,ssplien if FHA Streamline or VA IRRRL
    $j('#FHAStreamlineVAIRRRLPanel').toggle((selectedVal == E_sLPurposeT_FhaStreamlinedRefinance || 
                                             selectedVal == E_sLPurposeT_VaIrrrl) && bIsRefinanceLoan);
    
    // Show Upfront MIP Refund and endorsed if FHA Streamline
    $j('#UpfrontMIPRefundPanel').toggle((selectedVal == E_sLPurposeT_FhaStreamlinedRefinance) &&
                                        bIsRefinanceLoan);
    $j('#EndorsedPanel').toggle((selectedVal == E_sLPurposeT_FhaStreamlinedRefinance) &&
                                 bIsRefinanceLoan);
    // Don't allow this to affect FHA UFMIP value when not FHA Streamline Refi
    if (selectedVal != E_sLPurposeT_FhaStreamlinedRefinance) {
        $j('#sProdIsLoanEndorsedBeforeJune09').prop('checked', false);
        $j('#sProdIsLoanEndorsedBeforeJune09').change(); // update the model and refresh calculated value.
    }

    // If it is FHA Streamline or VA IRRRL, set Doc Type to Full Document and disable
    // Also hide Conv Loan PMI Type
    if (selectedVal == E_sLPurposeT_FhaStreamlinedRefinance || selectedVal == E_sLPurposeT_VaIrrrl) {
        $j('#sProdDocT').val(E_sProdDocT_Full);
        $j('#sProdDocT').readOnly(true);
        
        // Hide Conv Loan PMI Type and dependent field Split MI Upfront Premium
        $j('#ConvLoanPMIPanel,#SplitMIUpfrontPremiumPanel').hide();
    }
    else {
        if (!PML.options.IsReadOnly) {
            $j('#sProdDocT').readOnly(false);
        }

        // Show Conv Loan PMI Type and trigger change event to properly
        // toggle the Split MI Upfront Premium field.
        $j('#ConvLoanPMIPanel').show().change();
    }
    
    // Show Original appraised value when FHA Streamline and Appraisal not checked
    f_toggleOriginalAppraisedValue();
    
    // Show DU Refi Plus CB when this is Refi R/T and AUS Response selected
    f_toggleDURefiPlusCB();

    f_setLabel_sHouseValPe();
    
    // 10/10/2013 gf - opm 139257 only display custom pml fields for designated loan types.
    f_toggleCustomPmlFields();
    $j('.customfield').each(function() {
        f_validate_sCustomPMLField(this.id);
    });

    f_updateTexas50a6Checkbox();
    
    // Validate sSpLien and Cashout Amt and toggle buttons
    f_validate_sProdCashoutAmt();
    f_validate_sSpLien();
    f_validate_sOriginalAppraisedValue();
    f_toggleButtons();
    f_onchange_sProdConvMIOptionT();
}

function f_onchange_sOccT() {
    var selectedValue = PML.model.loan['sOccTPe'];
    // If it is investment, show the investment panel.
    $j("#investmentPanel").toggle(selectedValue == E_aOccT_Investment);
    // Non-occupant coborrower only visible if primary residence
    $j('#HasNonOccupantCoborrower').toggle(selectedValue == E_aOccT_PrimaryResidence);
    // Hidden if Primary Residence
    $j('#PresentHousingExpensePanel').toggle(selectedValue != E_aOccT_PrimaryResidence);

    f_updateTexas50a6Checkbox();

    // Validate Present Housing expense
    f_validate_sPresOHExpPe();
    f_toggleButtons();
}

function f_onclick_sIsRenovationLoan() {
    if (!bIsEnableRenovationCheckboxInPML) {
        return;
    }

    var isPurchase = !f_isRefinanceLoan();
	var isQuickPricerLoan = PML.model.loan.sLoanFileT == PML.constants.E_sLoanFileT.QuickPricer2Sandboxed;

    // Display the renovation cost textbox if this is checked
    var checked = $j('#sIsRenovationLoan').prop('checked');
    var EnableRenovationLoanSupport = $j('#EnableRenovationLoanSupport').val() === "True";
    $j('#TotalRenovationCosts').toggle(checked);
    $j('#sAsIsAppraisedValuePevalPanel').toggle(checked);
    $j('#determineTotalRenovationCosts').toggle(checked && EnableRenovationLoanSupport && !isQuickPricerLoan);
    $j('#InducementPurchPrice').toggle(checked && EnableRenovationLoanSupport && isPurchase);
    $j('#PurchPriceLessInducement').toggle(checked && EnableRenovationLoanSupport && isPurchase);
	$j('#sTotalRenovationCostsLckd').toggle(!(PML.model.loan['IsEnableRenovationCheckboxInPML'] == 'True' && !EnableRenovationLoanSupport) && !isQuickPricerLoan);
    var lckdValue = PML.model.loan['sTotalRenovationCostsLckd'];
    $j("#determineTotalRenovationCostsLink").prop('disabled', lckdValue === 'True');

    // If this is checked, then label for sApprValPe is As-IsAppraised Value
    // else it's Appraised Value
    if (checked) {
        $j('#sApprValPeLabel').text('As-Completed Value');
    }
    else {
        $j('#sApprValPeLabel').text('Appraised Value');
    }

    f_set_sApprValPeDisplay();
    f_validate_sApprValPe();
    f_setLabel_sHouseValPe();
    
    f_validate_sTotalRenovationCosts();
    f_validate_sAsIsAppraisedValuePeval();
    f_toggleButtons();
}

function f_onchange_b2ndFinancing() {
    var bIs2ndFinancing = PML.model.loan['sHasSecondFinancingT'] == 'True';
    var bIsHeloc = PML.model.loan['sSubFinT'] == E_sSubFinT_Heloc;

    if (!bIs2ndFinancing) {
        $j('#SecondFinancingTypePanel').hide();
        $j('#SecondFinancingFieldsPanel').hide()
        $j('#SecondFinancingHelocFieldsPanel').hide();
        // sProOFinBalPe, sLtvROtherFinPe, and sSubFinPe will all trigger data refresh
        // on change. Manually set model values and only trigger 1 change event.
        if (bIsHeloc) {
            $j('#sSubFinPe').val('$0.00');
			PML.model.loan['sSubFinPe'] = '$0.00';
        }
        $j('#sProOFinBalPe').val('$0.00');
        PML.model.loan['sProOFinBalPe'] = '$0.00';
        $j('#sLtvROtherFinPe').val('0.000%').change(); // Trigger data refresh for dependent fields.
    }
    else {
        $j('#SecondFinancingTypePanel').show();
        $j('#SecondFinancingFieldsPanel').show()
        $j('#SecondFinancingHelocFieldsPanel').toggle(bIsHeloc);
    }

	$j('#RequestCommunityOrAffordableSecondsPanel').toggle(!bIsHeloc && bIs2ndFinancing);

    // sSubFinPe validity depends on b2ndFinancing.
    f_validate_sProOFinBalPe();
    f_validate_sSubFinPe();
    f_toggleButtons();
}

function f_onchange_2ndFinancingType() {
    var bIsCloseEnd = PML.model.loan['sSubFinT'] == E_sSubFinT_CloseEnd;
    var bIsHeloc = !bIsCloseEnd;
    var bIs2ndFinancing = PML.model.loan['sHasSecondFinancingT'] == 'True';
    
    updateSecondFinancingAmountLabel();

    $j('#SecondFinancingHelocFieldsPanel').toggle(bIs2ndFinancing && bIsHeloc);
	$j('#RequestCommunityOrAffordableSecondsPanel').toggle(!bIsHeloc && bIs2ndFinancing);

    // sSubFinPe and sLAmtCalcPe validity depend on 2ndFinancing type.
    f_validate_sProOFinBalPe();
    f_validate_sSubFinPe();
    f_validate_sLAmtCalcPe();
    f_toggleButtons();
}

function updateSecondFinancingAmountLabel() {
    var bIsCloseEnd = PML.model.loan['sSubFinT'] == E_sSubFinT_CloseEnd;
    var bIsOFinNew = PML.model.loan['sIsOFinNewPe'] === 'True';

    if (bIsCloseEnd) {
        $j('#SecondFinancingAmountLabel').text('2nd Financing');
    }
    else {
        var helocLabel = bIsOFinNew ? 'Initial' : 'Current'
        $j('#SecondFinancingAmountLabel').text(helocLabel + ' Draw Amount');
    }
}

function f_onchange_sOriginatorCompensationPaymentSourceT() {
    if (!bIsDisplayOriginatorCompensation) {
        return;
    }

    var bIsBorrowerPaid = PML.model.loan['sOriginatorCompensationPaymentSourceT'] == E_sOriginatorCompensationPaymentSourceT_BorrowerPaid;
    $j('#LoanOriginatorSection').toggle(bIsBorrowerPaid);

    f_validate_sOriginatorCompensationBorrTotalAmount();
}

function f_validate_sOriginatorCompensationBorrTotalAmount() {
    var bIsValid = false;
    var bIsBorrowerPaid = PML.model.loan['sOriginatorCompensationPaymentSourceT'] == E_sOriginatorCompensationPaymentSourceT_BorrowerPaid;
    var borrowerPaidAmount = PML.model.loan['sOriginatorCompensationBorrTotalAmount'];
    var lenderPaidAmount = PML.model.loan['LenderPaidCompForBorrPaidCompCheck'];
    var errorMessage = 'The borrower-paid compensation ' + borrowerPaidAmount +
        ' exceeds the lender-paid compensation ' + lenderPaidAmount + '. Please ' +
        'either reduce the borrower-paid compensation or select lender-paid ' +
        'compensation in order to price.';
    
    if (bIsDisplayOriginatorCompensation && PML.options.BorrPdCompMayNotExceedLenderPdComp && bIsBorrowerPaid) {
        bIsValid = PML.model.loan['BorrowerPaidCompExceedsLenderPaidComp'] === 'False';
    }
    else {
        bIsValid = true;
    }
    
    validator['sOriginatorCompensationBorrTotalAmount'] = bIsValid;
    $j('#BorrowerPaidCompExceedsLenderPaidCompMessage').text(errorMessage);
    $j('.sOriginatorCompensationBorrTotalAmountValidator').toggle(!bIsValid);

    f_toggleButtons();
}

function f_onchange_sProdOverrideUFMIPFF() {
    // Want to disable FHA UFMIP, VA Funding Fee, and USDA Rural Guarantee Fee unless this is selected
    if ($j('#sProdOverrideUFMIPFF').prop('checked')) {
        $j('#RequireOverridenUFMIPFFToEdit :input').attr('readonly', false);
        $j('#RequireOverridenUFMIPFFToEdit :input').css('background-color', '');
    }
    else {
        $j('#RequireOverridenUFMIPFFToEdit :input').attr('readonly', true);
        $j('#RequireOverridenUFMIPFFToEdit :input').css('background-color', gReadonlyBackgroundColor);
    }
}

function f_onchange_sProdRLckdDaysDDL() {
    // Update the rate lock textbox from the DDL
    if (!bIsEnabledLockPeriodDropDown) {
        return;
    }

    var ddlVal = $j('#sProdRLckdDaysDDL').val();
    $j('#sProdRLckdDays').val(ddlVal);
    $j('#sProdRLckdDays').change();
}

function f_validatePriorSalesD(sPriorSalesD) {    
    if (!bIsPurchaseLoan) {
        return true;
    }
    if( sPriorSalesD === '' || sPriorSalesD === 'mm/dd/yyyy') {
        return true;
    }
    
    var parts = sPriorSalesD.match(/(\d+)/g);
    if( !parts || parts.length != 3 ) {
        return false;
    }
    var months  = parseInt( parts[0] );
    var days    = parseInt( parts[1] ); 
    var year    = parseInt( parts[2] ); 
    
    if( isNaN(months) || isNaN(days) || isNaN(year) )
    {
        return false;
    }
    var currentDate = new Date(); 
    if( months < 0 || months > 12 || days < 0 || days > 31 || year < 1900 || year > 2050 ) 
    {
        return false;
    }
    var d = new Date(year, months -1, days); // months are 0-based
    if( isNaN(d.getTime()) ) 
    {
        return false;
    }
    
    if( d > currentDate ) {
        return false;
    }
    
    return true;
}

function f_toggleButtons() {
    // If all of the required fields are valid, then enable the buttons
    var bEnableButtons = true;
    var sInvalidFieldIds = '';
    for (var reqField in validator) {
        if (validator.hasOwnProperty(reqField) && validator[reqField] == false) {
            bEnableButtons = false;
            sInvalidFieldIds += '\t' + reqField + '\n';                
        }
    }

    // alert('Invalid Fields:\n' + sInvalidFieldIds);
    
    //$j('#PriceBtn').attr('disabled', !bEnableButtons);
    $j('#ProcOrderCrBtn').attr('disabled', !bEnableButtons)

    // Also set the PML object's validation bool
    var prevIsValid = PML.model.loan["isValid"];
    if (prevIsValid != bEnableButtons) {
        PML.model.loan["isValid"] = bEnableButtons;
        $j(window).trigger("loan/changed/isValid", [bEnableButtons]);
    }
}

function f_validateRequiredFields() {
    f_validate_sSpAddr();
    f_validate_sSpZip();
    f_validate_sSpStatePe();
    f_validate_sSpCounty();
    f_validate_sSpCity();
    f_validate_sProdCondoStories();
    f_validate_sTotalRenovationCosts();
    f_validate_sProdCashoutAmt();        
    f_validate_sProdCurrPIPmt();
    f_validate_sProdCurrMIPMo();
    f_validate_sSpLien(); 
    f_validate_sApprValPe();
    f_validate_sHouseValPe();
    f_validate_sAsIsAppraisedValuePeval();
    f_validate_sProdRLckdDays();
    f_validate_sCreditScoreEstimatePe();
    f_validate_sPrimAppTotNonspIPe();
    f_validate_sNumFinancedProperties();
    f_validate_sAppTotLiqAsset();
    f_validate_sPresOHExpPe();
    f_validate_sPriorSalesD();
    f_validate_sPriorSalesPrice();
    f_validate_sPriorSalesSellerT();
    f_validate_sConvSplitMIRT();
    f_validate_sFHASalesConcessions();
    f_validate_sOriginalAppraisedValue();
    f_validate_sLAmtCalcPe();
    f_validate_sProOFinBalPe();
    f_validate_sSubFinPe();
    f_validate_sOwnerTitleInsFPe();
    f_validate_sOriginatorCompensationBorrTotalAmount();
    f_onchange_sCorrespondentProcessT();
    validatesProdConvMIOptionT();

    $j('.customfield').each(function() {
        f_validate_sCustomPMLField(this.id);
    });
}

function validatesProdConvMIOptionT() {
    var ltv80TestResult = PML.model.loan['sLtv80TestResultT'];
    var isEnabled = ML && ML.IsPmiEnabled && !(ltv80TestResult != E_sLtv80TestResultT_Over80 || PML.options.IsStandAloneSecond);
    var isApplicablePurposeT = !(PML.model.loan['sLPurposeTPe'] === E_sLPurposeT_FhaStreamlinedRefinance || PML.model.loan['sLPurposeTPe'] === E_sLPurposeT_VaIrrrl);

    var isValid = !isApplicablePurposeT || !isEnabled || $('#sProdConvMIOptionT').val() != E_sProdConvMIOptionT_Blank;
    validator['sProdConvMIOptionT'] = isValid;
    $('#sProdConvMIOptionTValidator').toggle(!isValid);
}

function f_validate_sSpAddr() {
    // As of OPM 118337, the loan file pricer is used for expanded lead editor.
    // This field is hidden for leads, so disable validation accordingly
    var bStreetAddressValid = $j.trim(PML.model.loan['sSpAddr']) != '';
    $j('#sSpAddrValidator').toggle(bStreetAddressValid == false && PML.options.IsLead == false);
    
    validator['sSpAddr'] = PML.options.IsLead || bStreetAddressValid;
}

function f_validate_sSpZip() {
    var bZipCodeValid = false;
    var zipCode = PML.model.loan['sSpZip'];
    var zipRegex = /\d{5}/;
    if (zipRegex.test(zipCode)) bZipCodeValid = true;
    // Set visibility so the checkbox will still take up space when
    // not visible.
    if (bZipCodeValid) {
        $j('#sSpZipValidator').css('visibility', 'hidden');
    }
    else {
        $j('#sSpZipValidator').css('visibility', 'visible');
    }
    
    validator['sSpZip'] = bZipCodeValid;
}

function f_validate_sSpStatePe() {
    var stateVal = PML.model.loan['sSpStatePe'];
    var bStateValid = stateVal != "";
    $j('#sSpStatePeValidator').toggle(bStateValid == false);

    $('.MiNeedSection').toggle(stateVal == 'NY');

    validator['sSpStatePe'] = bStateValid;
}

function f_validate_sSpCounty() {
    var county = PML.model.loan['sSpCounty'];
    var bCountyValid = county != '' && county != null;
    $j('#sSpCountyValidator').toggle(bCountyValid == false);
    
    validator['sSpCounty'] = bCountyValid;
}

function f_validate_sSpCity() {
    var bCityValid = $j.trim(PML.model.loan['sSpCity']) != '';
    $j('#sSpCityValidator').toggle(bCityValid == false);
    
    validator['sSpCity'] = bCityValid;
}

function f_validate_sProdCondoStories() {
    var bCondoStoriesValid = false;
    var bCondoStoriesVisible = $j('#sProdCondoStories').is(':visible');
    var condoStories = PML.model.loan['sProdCondoStories'];
    var condoStoriesRegex = /\d{1,3}/;
    if (condoStoriesRegex.test(condoStories) && condoStories > 0) bCondoStoriesValid = true;
    $j('#sProdCondoStoriesValidator').toggle(bCondoStoriesValid == false && bCondoStoriesVisible);
    
    validator['sProdCondoStories'] = !bCondoStoriesVisible || bCondoStoriesValid;
}

function f_validate_sTotalRenovationCosts() {
    var bTotalRenovationCostsVisible = $j('#sTotalRenovationCosts').is(':visible');
    var bTotalRenovationCostsValid = f_validateDollarAmount(PML.model.loan['sTotalRenovationCosts'], false);
    $j('#sTotalRenovationCostsValidator').toggle(bTotalRenovationCostsValid == false && bTotalRenovationCostsVisible);
    
    validator['sTotalRenovationCosts'] = !bTotalRenovationCostsVisible || bTotalRenovationCostsValid;
}

function f_validate_sProdCashoutAmt() {
    var bRefiCashout = PML.model.loan['sLPurposeTPe'] === E_sLPurposeT_RefinCashout;
    var isStandAloneSecond = PML.options.IsStandAloneSecond;
    var bCashoutAmountValid = f_validateDollarAmount(PML.model.loan['sProdCashoutAmt'], isStandAloneSecond);

    $j('#sProdCashoutAmtValidator').toggle(!bCashoutAmountValid && (bRefiCashout || isStandAloneSecond));
    validator['sProdCashoutAmt'] = !(bRefiCashout || isStandAloneSecond) || bCashoutAmountValid
}

function f_validate_sProdCurrPIPmt() {
    var bCurrPiPmtVisible = $j('#sProdCurrPIPmt').is(':visible');
    var bCurrPIPmtValid = f_validateDollarAmount(PML.model.loan['sProdCurrPIPmt'], true);
    $j('#sProdCurrPIPmtValidator').toggle(bCurrPIPmtValid == false && bCurrPiPmtVisible);
    
    validator['sProdCurrPIPmt'] = !bCurrPiPmtVisible || bCurrPIPmtValid;
}

function f_validate_sProdCurrMIPMo() {
    var bCurrMIPMoVisible = $j('#sProdCurrMIPMo').is(':visible');
    var bCurrMIPMoValid = f_validateDollarAmount(PML.model.loan['sProdCurrMIPMo'], true);
    if (bCurrMIPMoValid == false && bCurrMIPMoVisible) {
        $j('#sProdCurrMIPMoValidator').css('visibility', 'visible');
    }
    else {
        $j('#sProdCurrMIPMoValidator').css('visibility', 'hidden');
    }
    
    validator['sProdCurrMIPMo'] = !bCurrMIPMoVisible || bCurrMIPMoValid;
}

function f_validate_sSpLien() {
    var bOutstandingPrincipalBalanceVisible = $j('#sSpLien').is(':visible');
    var bOutstandingPrincipalBalanceValid = f_validateDollarAmount(PML.model.loan['sSpLien'], false);
    $j('#sSpLienValidator').toggle(bOutstandingPrincipalBalanceValid == false && bOutstandingPrincipalBalanceVisible);
    
    validator['sSpLien'] = !bOutstandingPrincipalBalanceVisible || bOutstandingPrincipalBalanceValid;
}

function f_validate_sApprValPe() {
    var bIsRenovationLoan = $j('#sIsRenovationLoan').prop('checked');
    var bAppraisedValueValid = f_validateDollarAmount($j('#sApprValPe').val(), false);
    $j('#sApprValPeValidator').toggle(bAppraisedValueValid == false && bIsRenovationLoan);
    validator['sApprValPe'] = !bIsRenovationLoan || bAppraisedValueValid;
}

function f_validate_sHouseValPe() {
    var bHouseValPePanelVisible = $j('#sHouseValPePanel').is(':visible');
    var bHouseValPeValid = f_validateDollarAmount(PML.model.loan['sHouseValPe'], false);
    var visibility = bHouseValPeValid ? 'hidden' : 'visible';
    $j('#sHouseValPeValidator').css('visibility', visibility);
    validator['sHouseValPe'] = bHouseValPeValid || !bHouseValPePanelVisible;
}

function f_validate_sAsIsAppraisedValuePeval() {
    var bIsRenovationLoan = $j('#sIsRenovationLoan').prop('checked');
    var bsAsIsAppraisedValuePeValid = f_validateDollarAmount(PML.model.loan['sAsIsAppraisedValuePeval'], true);

    var isQuickPricerLoan = PML.model.loan.sLoanFileT == PML.constants.E_sLoanFileT.QuickPricer2Sandboxed;
    var isRefinance = f_isRefinanceLoan();
    var isFhaProductType = $j('#sProdIncludeFHATotalProc').prop('checked');
    var isAsIsAppraisedValueNonZero = f_validateDollarAmount(PML.model.loan['sAsIsAppraisedValuePeval'], false);
    var requireAsIsAppraisedValueForQp = !isAsIsAppraisedValueNonZero && isQuickPricerLoan && isRefinance && isFhaProductType;

    $j('#sAsIsAppraisedValuePevalValidator').toggle(bIsRenovationLoan && (!bsAsIsAppraisedValuePeValid || requireAsIsAppraisedValueForQp));
    validator['sAsIsAppraisedValuePeval'] = !bIsRenovationLoan || (bsAsIsAppraisedValuePeValid && !requireAsIsAppraisedValueForQp);
}

function f_validate_sProdRLckdDays() {
    var bRateLockPeriodValid = false;
    var rateLockVal = PML.model.loan['sProdRLckdDays'];
    try {
        var intVal = parseInt(rateLockVal);
        if (intVal > 0) bRateLockPeriodValid = true;
    } catch (e) { }
    $j('#sProdRLckdDaysValidator').toggle(bRateLockPeriodValid == false);
    
    validator['sProdRLckdDays'] = bRateLockPeriodValid;
}

function f_validate_sCreditScoreEstimatePe() {
    if (!bNoApps) {
        $j('#sCreditScoreEstimatePeValidator').hide();
        validator['sCreditScoreEstimatePe'] = true;
        return;
    }
    
    var bEstimatedCreditScoreValid = false;
    var estimatedCreditScoreVal = PML.model.loan['sCreditScoreEstimatePe'];
    try {
        var intVal = parseInt(estimatedCreditScoreVal);
        if ((PML.options.IsLead ? intVal >= 0 : intVal > 0) && intVal <= 850) bEstimatedCreditScoreValid = true;
    } catch (e) { }
    var visibility = bEstimatedCreditScoreValid ? 'hidden' : 'visible';
    $j('#sCreditScoreEstimatePeValidator').css('visibility', visibility);
    
    validator['sCreditScoreEstimatePe'] = bEstimatedCreditScoreValid;
}

function f_validate_sPrimAppTotNonspIPe() {
    if (!bNoApps) {
        $j('#sPrimAppTotNonspIPeValidator').hide();
        validator['sPrimAppTotNonspIPe'] = true;
        return;
    }
    
    var bTotalMonthlyIncomeValid = f_validateDollarAmount(PML.model.loan['sPrimAppTotNonspIPe'], true, true);
    $j('#sPrimAppTotNonspIPeValidator').toggle(bTotalMonthlyIncomeValid == false);
    
    validator['sPrimAppTotNonspIPe'] = bTotalMonthlyIncomeValid;
}

function f_validate_sNumFinancedProperties() {
    var bNumFinancedPropertiesValid = false;
    var numFinancedVal = PML.model.loan['sNumFinancedProperties'];
    try {
        var intVal = parseInt(numFinancedVal);
        if (intVal >= 1) bNumFinancedPropertiesValid = true;
    } catch (e) { }
    if (numFinancedVal != '' && numFinancedVal < 1) {
        alert(JsMessages_NumFinancedPropertiesMustBePositive);
    }
    $j('#sNumFinancedPropertiesValidator').toggle(bNumFinancedPropertiesValid == false);
    
    validator['sNumFinancedProperties'] = bNumFinancedPropertiesValid;
}

function f_validate_sAppTotLiqAsset() {
    if (!bNoApps) {
        $j('#sAppTotLiqAssetValidator').hide();
        validator['sAppTotLiqAsset'] = true;
        return;
    }

    var bTotalLiqAssetsValid = $j.trim(PML.model.loan['sAppTotLiqAsset']) != '';
    $j('#sAppTotLiqAssetValidator').toggle(bTotalLiqAssetsValid == false);    

    validator['sAppTotLiqAsset'] = bTotalLiqAssetsValid;
}

function f_validate_sPresOHExpPe() {
    var bPresentHousingExpenseVisible = $j('#sPresOHExpPe').is(':visible');
    var bPresentHousingExpenseValid = f_validateDollarAmount(PML.model.loan['sPresOHExpPe'], true);
    $j('#sPresOHExpPeValidator').toggle(bPresentHousingExpenseValid == false && bPresentHousingExpenseVisible);
    
    validator['sPresOHExpPe'] = !bPresentHousingExpenseVisible || bPresentHousingExpenseValid;
}

function f_validate_sPriorSalesD() {
    var bPriorSalesDValid = f_validatePriorSalesD(PML.model.loan['sPriorSalesD']);
    $j('#sPriorSalesDValidator').toggle(bPriorSalesDValid == false);
    
    validator['sPriorSalesD'] = bPriorSalesDValid;
}

function f_validate_sPriorSalesPrice() {
    var bPriorSalesPriceValid = f_validateDollarAmount(PML.model.loan['sPriorSalesPrice'], false);
    var bPriorSalesPriceVisible = $j('#sPriorSalesPrice').is(':visible');
    $j('#sPriorSalesPriceValidator').toggle(bPriorSalesPriceValid == false && bPriorSalesPriceVisible);
    
    validator['sPriorSalesPrice'] = !bPriorSalesPriceVisible || bPriorSalesPriceValid;
}

function f_validate_sPriorSalesSellerT() {
    var val = PML.model.loan['sPriorSalesSellerT'];
    var bPriorSalesSellerValid = val != '' && val != null && val != E_sPriorSalesPropertySellerT_Blank;
    var bPriorSalesSellerVisible = $j('#sPriorSalesSellerT').is(':visible');
    $j('#sPriorSalesSellerTValidator').toggle(bPriorSalesSellerValid == false && bPriorSalesSellerVisible);
    
    validator['sPriorSalesSellerT'] = !bPriorSalesSellerVisible || bPriorSalesSellerValid;
}

function f_validate_sConvSplitMIRT() {
    var bSplitMIUpfrontPremVisible = $j('#sConvSplitMIRT').is(':visible');
    var bSplitMIUpfrontPremValid = PML.model.loan['sConvSplitMIRT'] != E_sConvSplitMIRT_Blank;
    $j('#sConvSplitMIRTValidator').toggle(bSplitMIUpfrontPremValid == false && bSplitMIUpfrontPremVisible);

    validator['sConvSplitMIRT'] = !bSplitMIUpfrontPremVisible || bSplitMIUpfrontPremValid;
}

function f_validate_sFHASalesConcessions() {
    // Must be >= 0 if visible
    var bVisible = $j('#sFHASalesConcessions').is(':visible');
    var bValid = f_validateDollarAmount(PML.model.loan['sFHASalesConcessions'], true);
    $j('#sFHASalesConcessionsValidator').toggle(bValid == false && bVisible);
    
    validator['sFHASalesConcessions'] = !bVisible || bValid;
}

function f_validate_sOriginalAppraisedValue() {
    var bVisible = $j('#sOriginalAppraisedValue').is(':visible');
    var dollarAmt = f_getMoneyValue(PML.model.loan['sOriginalAppraisedValue']);
    var bValid = (dollarAmt != null && dollarAmt >= 1);
    $j('#sOriginalAppraisedValueValidator').toggle(bValid == false && bVisible);
    
    validator['sOriginalAppraisedValue'] = !bVisible || bValid;
}

function f_validate_sLAmtCalcPe() {
    var bIs2ndFinancing = PML.model.loan['sHasSecondFinancingT'] == 'True';
    var bIsHeloc = PML.model.loan['sSubFinT'] == E_sSubFinT_Heloc;
    var bIsLineOfCredit = PML.options.IsLineOfCredit;
    var bAllowZero = bIsLineOfCredit || PML.options.IsStandAloneSecond && bIs2ndFinancing && bIsHeloc && PML.options.IsAllowHelocZeroInitialDraw;

    var bValid = f_validateDollarAmount(PML.model.loan['sLAmtCalcPe'], bAllowZero);
    $j('#sLAmtCalcPeValidator').toggle(bValid == false);

    validator['sLAmtCalcPe'] = bValid;
}

// This should only check validity when it is a standalone 2nd.
function f_validate_sProOFinBalPe() {
    var bIs2ndFinancing = PML.model.loan['sHasSecondFinancingT'] == 'True';
    var bIsCloseEnd = PML.model.loan['sSubFinT'] == E_sSubFinT_CloseEnd;

    var bValid = f_validateDollarAmount(PML.model.loan['sProOFinBalPe'], false);
    $j('#sProOFinBalPeValidator').toggle(bValid == false && bIs2ndFinancing && bIsCloseEnd);

    validator['sProOFinBalPe'] = !bIs2ndFinancing || !bIsCloseEnd || bValid;
}

function f_validate_sSubFinPe() {
    // OPM 107986 - This value should be > 0 and >= to the initial draw amount.
    // For standalone seconds the sLAmtCalcPe and sProOFinBalPe fields get swapped,
    // so need to check the appropriate one.
    var bIs2ndFinancing = PML.model.loan['sHasSecondFinancingT'] == 'True';
    var bIsHeloc = PML.model.loan['sSubFinT'] == E_sSubFinT_Heloc;
    
    var compareField = PML.options.IsStandAloneSecond ?
        PML.model.loan['sLAmtCalcPe'] : PML.model.loan['sProOFinBalPe'];
    var initialDrawAmt = f_getMoneyValue(compareField);
    
    var sSubFinPe = f_getMoneyValue(PML.model.loan['sSubFinPe']);

    var bValid = sSubFinPe != null && initialDrawAmt != null && sSubFinPe > 0 && 
        sSubFinPe >= initialDrawAmt;
    $j('#sSubFinPeValidator').toggle(bValid == false && bIs2ndFinancing && bIsHeloc);
    
    validator['sSubFinPe'] = !(bIs2ndFinancing && bIsHeloc) || bValid;
}

function f_setLabel_sHouseValPe() {
    var bHasAppraisal = $j('#sHasAppraisal').prop('checked');
    var bIsFHAStreamlineRefi = $j('#sLPurposeTPe').val() == E_sLPurposeT_FhaStreamlinedRefinance;
    var bIsRefinanceLoan = f_isRefinanceLoan();
    var bIsRenovationLoan = $j('#sIsRenovationLoan').prop('checked');

    $j('#sHouseValPePanel').show();
    if (PML.options.IsLineOfCredit) {
        $j('#sHouseValPeLabel').text('Home Value'); // OPM 107986
    }
    else if (bIsPurchaseLoan) {
        $j('#sHouseValPeLabel').text('Sales Price');
    }
    else if (bIsFHAStreamlineRefi && bHasAppraisal && !PML.options.IsStandAloneSecond) {
        $j('#sHouseValPeLabel').text('Appraised Value');
    }
    //OPM 114130
    else if (bIsFHAStreamlineRefi && !bHasAppraisal && !PML.options.IsStandAloneSecond) {
        $j('#sHouseValPePanel').hide();
    }
    else if (bIsRenovationLoan && bIsEnableRenovationCheckboxInPML && !PML.options.IsStandAloneSecond) {
        $j('#sHouseValPePanel').hide();
    }
    else {
        $j('#sHouseValPeLabel').text('Home Value');
    }

    f_validate_sHouseValPe();
    f_toggleButtons();
}

function f_onclick_sHasAppraisal() {
    f_toggleOriginalAppraisedValue();
    f_setLabel_sHouseValPe();
    f_validate_sOriginalAppraisedValue();
    f_toggleButtons();
}

function f_setLabel_sEquityPe() {
    if (bIsPurchaseLoan) {
        $j('#EquityDownPmtLabel').text('Down Payment');
    }
    else {
        $j('#EquityDownPmtLabel').text('Equity');
    }
}

function f_onchange_sTitleInsuranceCostT() {
    // Show/hide the manual entry field as appropriate.
    $j('#ManualTitlePanel').toggle(PML.model.loan['sTitleInsuranceCostT'] == E_sTitleInsuranceCostT_Manual);
    f_validate_sOwnerTitleInsFPe();
    f_toggleButtons();
}

function f_onchange_sCorrespondentProcessT() {
    var bVisible = $j("[id*='sCorrespondentProcessTContainer']").is(':visible');
    var isValid = false || !bVisible;
    $j("[name='sCorrespondentProcessT']").each(function() {
        isValid = isValid || $j(this).prop("checked");
    });

    $j("[id*='sCorrespondentProcessTValidator']").toggle(isValid == false && bVisible);
    validator['sCorrespondentProcessT'] = isValid;

    f_toggleButtons();
}

function f_validate_sOwnerTitleInsFPe() {
    var bValid = f_validateDollarAmount(PML.model.loan['sOwnerTitleInsFPe'], false);
    var bManualEntry = PML.model.loan['sTitleInsuranceCostT'] == E_sTitleInsuranceCostT_Manual;
    $j('#sOwnerTitleInsFPeValidator').toggle(bValid == false && bManualEntry && bIsPurchaseLoan);

    validator['sOwnerTitleInsFPe'] = !(bManualEntry && bIsPurchaseLoan) || bValid;
}

function f_enableFieldsForLoanType() {
    // If it is a purchase loan, refi type hidden, p&i hidden, MIPmon hidden, Cashout Amt hidden
    // OPM 107986 - Also handle standalone seconds: hide refi type panel, hide p&i and mip/mon,
    $j('#RefinanceTypePanel').toggle(bIsPurchaseLoan == false && PML.options.IsStandAloneSecond == false);
    $j('#CashoutAmtPanel').toggle(bIsPurchaseLoan == false || PML.options.IsStandAloneSecond);
    $j('#CurrPIPmtMIPMoPanel').toggle(bIsPurchaseLoan == false && PML.options.IsStandAloneSecond == false);

    // If it is not a purchase loan, seller credit is hidden
    $j('#SellerCreditPanel').toggle(bIsPurchaseLoan && (bIsProduction == false));

    f_set_sApprValPeDisplay();
    
    // labels change based on loan type
    f_setLabel_sHouseValPe();
    f_setLabel_sEquityPe();
    
    // hidden unless purchase loan
    $j('#MasterPriorSalesPanel').toggle(bIsPurchaseLoan);

    // OPM 120131 - Only show manual title info for purchase loans.
    // 5/9/2014 gf - opm 175976, do not show for purchase standalone seconds.
    $j('#TitleInsuranceCostPanel').toggle(bIsPurchaseLoan && !PML.options.IsStandAloneSecond);

    // Only display the other financing is new checkbox for first liens.
    if (PML.model.loan['sLienPosT'] !== '0') {
        $j('#SecondFinancingIsNew').hide();
    }
    else {
        $j('#sIsOFinNewPe').attr('disabled', bIsPurchaseLoan);
    }
}

function f_set_sApprValPeDisplay() {
    var isRenovationLoan = bIsEnableRenovationCheckboxInPML && $j('#sIsRenovationLoan').prop('checked');

    // If not purchase or renovation loan, then hide sApprValPe
    $j('#ApprValPePanel').toggle(bIsPurchaseLoan || isRenovationLoan);
}

function f_toggleCreditFields() {
    bNoApps = PML.model.apps.length == 0;
    bIsQP2File = PML.model.loan.sLoanFileT == PML.constants.E_sLoanFileT.QuickPricer2Sandboxed  
    if (bNoApps) {
        $j('#VisiblePriorToOrderingCredit1').show();
        $j('#VisiblePriorToOrderingCredit2').show();
        $j('#ProcOrderCrBtn').toggle(false === bIsQP2File);
    }
    else {
        $j('#VisiblePriorToOrderingCredit1').hide();
        $j('#VisiblePriorToOrderingCredit2').hide();
        $j('#ProcOrderCrBtn').hide();
    }
    
    f_validate_sCreditScoreEstimatePe();
    f_validate_sPrimAppTotNonspIPe();
    f_validate_sAppTotLiqAsset();
}

function f_set_sProdCalcEntryTData() {
    $j('#sDownPmtPcPe').data('sProdCalcEntryT', '0');
    $j('#sEquityPe').data('sProdCalcEntryT', '1');
    $j('#sLtvRPe').data('sProdCalcEntryT', '2');
    $j('#sLAmtCalcPe').data('sProdCalcEntryT', '3');
    $j('#sCltvRPe').data('sProdCalcEntryT', '4');
    $j('#sLtvROtherFinPe').data('sProdCalcEntryT', '5');
    $j('#sProOFinBalPe').data('sProdCalcEntryT', '6');
    $j('#sHCLTVRPe').data('sProdCalcEntryT', '9');
    $j('#sSubFinPe').data('sProdCalcEntryT', '5');
    $j('#sSubFinToPropertyValue').data('sProdCalcEntryT', '8');
    $j('#sApprValPe').data('sProdCalcEntryT', '7');
    $j('#sHouseValPe').data('sProdCalcEntryT', '7');
    $j('#sIsRenovationLoan').data('sProdCalcEntryT', '10');
    $j('#sTotalRenovationCosts').data('sProdCalcEntryT', '10');
    $j('#sAsIsAppraisedValuePeval').data('sAsIsAppraisedValuePeval', '10');
}

function f_set_RateLockSmartText() {
    $j('#RateLockSmartText').html(PML.model.loan['sProdRLckdExpiredDLabel']);
}

function f_uc_init() {
    $j('#sProdConvMIOptionT').on('change', setAsUserInput);
    $j('#PriceBtn').on('click', function() {
        window.scrollTo(0, 0);
        $j(window).trigger('loan/price');
    });
    $j('#ProcOrderCrBtn').on('click', function() {
        window.scrollTo(0, 0);
        $j(window).trigger('apps/create');
    });

    $j("[name='sCorrespondentProcessT']").change(f_onchange_sCorrespondentProcessT);
    f_set_sProdCalcEntryTData();
    $j(window).on('loan/changed/sLtv80TestResultT', f_set_sProdConvMIOptionT);
    $j(window).on('loan/changed/sOccTPe', f_onchange_sOccT);
    $j(window).on('loan/changed/sProdSpT', f_onchange_sProdSpT);
    $j(window).on('loan/changed/sLPurposeTPe', f_onchange_sLPurposeTPe);
    $j(window).on('loan/changed/sIsRenovationLoan', f_onclick_sIsRenovationLoan);
    $j(window).on('loan/changed/sProdConvMIOptionT', f_onchange_sProdConvMIOptionT);
    $j(window).on('loan/changed/sHasSecondFinancingT', f_onchange_b2ndFinancing);
    $j(window).on('loan/changed/sSubFinT', f_onchange_2ndFinancingType);
    $j(window).on('loan/changed/sIsOFinNewPe', updateSecondFinancingAmountLabel);
    $j(window).on('loan/changed/sOriginatorCompensationPaymentSourceT', f_onchange_sOriginatorCompensationPaymentSourceT);
    $j(window).on('loan/changed/BorrowerPaidCompExceedsLenderPaidComp', f_validate_sOriginatorCompensationBorrTotalAmount);
    $j(window).on('loan/changed/sOriginatorCompensationBorrTotalAmount', f_validate_sOriginatorCompensationBorrTotalAmount);
    $j(window).on('loan/changed/LenderPaidCompForBorrPaidCompCheck', f_validate_sOriginatorCompensationBorrTotalAmount);
    $j(window).on('loan/changed/sProdOverrideUFMIPFF', f_onchange_sProdOverrideUFMIPFF);
    $j(window).on('loan/changed/sHasAppraisal', f_onclick_sHasAppraisal);
    $j(window).on('loan/changed/sProdRLckdDays', function() {
        f_validate_sProdRLckdDays();
        f_toggleButtons();
    });
    $j('#sProdRLckdDaysDDL').on('change', f_onchange_sProdRLckdDaysDDL);
    $j(window).on('loan/changed/sProdRLckdExpiredDLabel', f_set_RateLockSmartText);
    $j(window).on('loan/changed/sProd3rdPartyUwResultT', f_toggleDURefiPlusCB);
    $j(window).on('loan/changed/sSpAddr', function() {
        f_validate_sSpAddr();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sSpZip', function() {
        // Update the values in the model for city, state, county
        // without triggering a changed event for all of the fields.
        PML.model.loan['sSpCity'] = $j('#sSpCity').val();
        PML.model.loan['sSpStatePe'] = $j('#sSpStatePe').val();
        PML.model.loan['sSpCounty'] = $j('#sSpCounty').val();
        
        f_updateTexas50a6Checkbox();

        f_validate_sSpZip();
        f_validate_sSpStatePe();
        f_validate_sSpCounty();
        f_validate_sSpCity();
        f_toggleButtons();
        $j(window).trigger('loan/changed/sSpZip_done'); // bad hack so that the action buttons will toggle correctly
    });
    // smartZipcode triggers the change event before exiting,
    // so only call it on blur
    $j('#sSpZip').blur(function() {
        smartZipcode(document.getElementById('sSpZip'),
                     document.getElementById('sSpCity'),
                     document.getElementById('sSpStatePe'),
                     document.getElementById('sSpCounty'));
    });
    $j('#sSpZip').focus(function() {
        this.oldValue = this.value;
    });
    $j(window).on('loan/changed/sSpStatePe', function() {
        f_updateTexas50a6Checkbox();
        UpdateCounties(document.getElementById('sSpStatePe'), document.getElementById('sSpCounty'), function() {
            PML.model.loan['sSpCounty'] = $j('#sSpCounty').val();
            f_validate_sSpStatePe();
            f_validate_sSpCounty();
            f_toggleButtons();
        });
    });
    $j(window).on('loan/changed/sSpCounty', function() {
        f_validate_sSpCounty();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sSpCity', function() {
        f_validate_sSpCity();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sProdCondoStories', function() {
        f_validate_sProdCondoStories();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sTotalRenovationCosts', function() {
        f_validate_sTotalRenovationCosts();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sProdCashoutAmt', function() {
        f_updateTexas50a6Checkbox();
        f_validate_sProdCashoutAmt();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sProdCurrPIPmt', function() {
        f_validate_sProdCurrPIPmt();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sProdCurrPIPmtLckd', function() {
        $j('#sProdCurrPIPmt').readOnly(PML.model.loan['sProdCurrPIPmtLckd'] === 'False');
    });
    $j(window).on('loan/changed/sTotalRenovationCostsLckd', function () {
        var lckdValue = PML.model.loan['sTotalRenovationCostsLckd'];
        $j('#sTotalRenovationCosts').readOnly(lckdValue === 'False');
        $j("#determineTotalRenovationCostsLink").prop('disabled', lckdValue === 'True');
    });
    $j(window).on('loan/changed/sProdCurrMIPMo', function() {
        f_validate_sProdCurrMIPMo();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sSpLien', function() {
        f_validate_sSpLien();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sApprValPe', function() {
        f_validate_sApprValPe();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sHouseValPe', function() {
        f_validate_sHouseValPe();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sAsIsAppraisedValuePeval', function() {
        f_validate_sAsIsAppraisedValuePeval();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sCreditScoreEstimatePe', function() {
        f_validate_sCreditScoreEstimatePe();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sPrimAppTotNonspIPe', function() {
        f_validate_sPrimAppTotNonspIPe();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sNumFinancedProperties', function() {
        f_validate_sNumFinancedProperties();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sAppTotLiqAsset', function() {
        f_validate_sAppTotLiqAsset();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sPresOHExpPe', function() {
        f_validate_sPresOHExpPe();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sPriorSalesD', function() {
        f_togglePriorSalesExtraInfoPanel();
        f_validate_sPriorSalesD();
        f_validate_sPriorSalesPrice();
        f_validate_sPriorSalesSellerT();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sPriorSalesPrice', function() {
        f_validate_sPriorSalesPrice();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sPriorSalesSellerT', function() {
        f_validate_sPriorSalesSellerT();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sConvSplitMIRT', function() {
        f_validate_sConvSplitMIRT();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sFHASalesConcessions', function() {
        f_validate_sFHASalesConcessions();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sOriginalAppraisedValue', function() {
        f_validate_sOriginalAppraisedValue();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sLAmtCalcPe', function() {
        f_validate_sLAmtCalcPe();
        f_validate_sSubFinPe();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sProOFinBalPe', function() {
        f_validate_sProOFinBalPe();
        f_validate_sSubFinPe();
        f_toggleButtons();
    });
    $j(window).on('loan/changed/sSubFinPe', function() {
        f_validate_sSubFinPe();
        f_toggleButtons();
    });
    $j(window).on("PageIsValid", function(e, pageValid) {
        $j('#PriceBtn').attr('disabled', !pageValid);
    });
    $j(window).on('loan/changed/sTitleInsuranceCostT', f_onchange_sTitleInsuranceCostT);
    $j(window).on('loan/changed/sOwnerTitleInsFPe', function() {
        f_validate_sOwnerTitleInsFPe();
        f_toggleButtons();
    });
    $j('#bYesIsPurchaseLoan').on('change', f_isPurchaseLoanToggleHandler);
    $j('#bNoIsPurchaseLoan').on('change', f_isPurchaseLoanToggleHandler);

    // Attach events for all of the custom fields in use
    $j('.customfield').each(function(index) {
        var fieldId = $j(this).attr('id');
        var changedEvent = 'loan/changed/' + fieldId;
        $j(window).on(changedEvent, function(eventObj, newVal) {
            // Extract which field this is from the event that was triggered.
            var eventNameComponents = eventObj.type.split('/');
            if (eventNameComponents.length == 0) throw ('Error validating Custom PML Fields');

            var fieldId = eventNameComponents[eventNameComponents.length - 1];
            f_validate_sCustomPMLField(fieldId);

            f_toggleButtons();
        });
    });

    f_links_init();
    
    $j('.LoanFilePricer').find('textarea,input[type=text],input[type=radio],input[type=checkbox],select')
        .readOnly(PML.options.IsReadOnly)
        .attr('disabled', PML.options.IsReadOnly); // disable all inputs if the loan is readonly

    // The Additional Monthly Housing Expenses field should always be readonly
    $j('#sMonthlyPmtPe').readOnly(true);
    
    //Price Group should always be readonly
    $j('#sProdLpePriceGroupNm').readOnly(true);

    // Sales Price Less Inducements should always be readonly
    $j('#sPurchPriceLessInducement').prop('readonly', true);
}

function f_links_init() {
    $j('#explainInRuralArea').click(function() {
        return f_openHelp('Q00014.html', 200, 100);
    });
    $j('#explainCreditQualifying').click(function() {
        return f_openHelp('Q00012.html', 500, 250);
    });
    $j('#explainOutstandingPrincipalBalance').click(function() {
        return f_openHelp('Q00011.html', 400, 150);
    });
    $j('#explainNumFinancedProperties').click(function() {
        return f_openHelp('NumberOfFinancedProperties.aspx', 450, 150);
    });
    $j('#explainPriorSalesDate').click(function() {
        return f_openHelp('Q00013.html', 150, 150); return false;
    });
    $j('#explainTotalLiquidAssets').click(function() {
        return f_openHelp('Q00016.html', 500, 200);
    });

    $j('#explainTexas50a6').click(function() {
        return f_openHelp('Q00010.html', 400, 200);
    });
    $j('#calculateAdditionalMonHousingExpenses').click(f_showOtherExpenses);

    $j('#explainLinkedSecondLien').click(function () {
        return f_openHelp('Q00019.html', 400, 150);
    });
}

function f_isPurchaseLoanToggleHandler() {
    var oldbIsPurchaseLoan = bIsPurchaseLoan;
    bIsPurchaseLoan = $j('#bYesIsPurchaseLoan').is(':checked') && !PML.options.IsLineOfCredit
    if (oldbIsPurchaseLoan === bIsPurchaseLoan) {
        return;
    }

    PML.ActionArea.UpdateNumPinned(0);
    if (bIsPurchaseLoan) {
        PML.model.loan['sLPurposeT'] = PML.constants.E_sLPurposeT.Purchase;
        $j(window).trigger('loan/changed/sLPurposeT');

        PML.model.loan['sLPurposeTPe'] = PML.constants.E_sLPurposeT.Purchase;
        $j(window).trigger('loan/changed/sLPurposeTPe');
    }
    else {
        PML.model.loan['sLPurposeT'] = PML.constants.E_sLPurposeT.Refin;
        $j(window).trigger('loan/changed/sLPurposeT');
        $j('#sLPurposeTPe').val(PML.constants.E_sLPurposeT.Refin).change()
    }

    f_enableFieldsForLoanType();
    f_togglePriorSalesExtraInfoPanel();
    f_validate_sOwnerTitleInsFPe();

    // Update the renovation loan UI, which depends on the loan's purpose.
    f_onclick_sIsRenovationLoan();
}

function f_showOtherExpenses(){

    LQBPopup.Show(gVirtualRoot + '/main/AdditionalMonthlyExpenses.aspx?loanid=' + ML.sLId
        + "&sApprValPe=" + $j("#sApprValPe").val()
        + "&sHouseValPe=" + $j("#sHouseValPe").val()
        + "&sDownPmtPcPe=" + $j("#sDownPmtPcPe").val()
        + "&sEquityPe=" + $j("#sEquityPe").val()
        + "&sLtvRPe=" + $j("#sLtvRPe").val()
        + "&sLAmtCalcPe=" + $j("#sLAmtCalcPe").val(), {
      onReturn:f_showOtherExpensesCallback,
      width:650,
      height:290,
      hideCloseButton:true
      
    });
    return false;
}

function f_showOtherExpensesCallback(args) {
    // Update the field value and then trigger the change event to update the model.
    // This will determine of the values have changed and need to be saved.
    $j('#sMonthlyPmtPe').val(args.sMonthlyPmtPe).change();
}

function f_populate_sSpStatePe() {
    var statesAndTerritories = new Array("", "AK", "AL", "AR", "AS", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", 
                                "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", 
                                "MD", "ME", "MI", "MN", "MO", "MP", "MS", "MT", "NC", "ND", "NE",  
                                "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", 
                                "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT", 
                                "WA", "WI", "WV", "WY");
    for (var i = 0; i < statesAndTerritories.length; i++) {
        var currItem = statesAndTerritories[i];
        $j('#sSpStatePe').append('<option value="' + currItem + '">' + currItem + '</option>');
    }
}

function f_populate_sProdRLckdDaysDDL() {
    var alreadyPopulated = $j('#sProdRLckdDaysDDL option').length != 0;
    if (!bIsEnabledLockPeriodDropDown || alreadyPopulated) return;

    var csv = PML.model.loan['AvailableLockPeriodOptionsCSV'];
    var rateLockArray = csv.split(',');
    rateLockArray.sort(function(a, b) {
        return a - b;
    });
    
    // Add all of the options to the DDL and set the value if one is available, then hide the textbox
    $j('#sProdRLckdDaysDDL').append('<option value="0">None</option');
    for (var i = 0; i < rateLockArray.length; i++) {
        var currItem = rateLockArray[i];
        if ($j.trim(currItem) == '') {
            continue;
        }
        $j('#sProdRLckdDaysDDL').append('<option value="' + currItem + '">' + currItem + '</option');
    }
    
    if (PML.model.loan['sProdRLckdDays'] != '') {
        $j('#sProdRLckdDaysDDL').val(PML.model.loan['sProdRLckdDays']);
    }

    $j('#sProdRLckdDays').hide();
}

// OPM 107986 - Handle case of standalone second.
function f_set_sProdConvMIOptionT() {
    var ltv80TestResult = PML.model.loan['sLtv80TestResultT'];
    var $ddl = $j('#sProdConvMIOptionT');
    if (ltv80TestResult != E_sLtv80TestResultT_Over80 || PML.options.IsStandAloneSecond) {
        $ddl.val(E_sProdConvMIOptionT_NoMI);
        $ddl.css('background-color', gReadonlyBackgroundColor);
        $ddl.prop('disabled', true);
        $ddl.removeAttr('requires-user-update');
    }
    else {
        $ddl.css('background-color', '');
        if ($ddl.prop('disabled')) {
            $ddl.prop('disabled', false);
            $ddl.attr('requires-user-update', true);
            $ddl.val(E_sProdConvMIOptionT_Blank);
        }
    }
    $ddl.change(); // this will validate the field and hide/show sConvSplitMIRT as needed

    validatesProdConvMIOptionT();
    f_toggleButtons();
}

function f_isRefinanceLoan() {
    return PML.model.loan["sLPurposeTPe"] !== E_sLPurposeT_HomeEquity &&
        PML.model.loan['sIsRefinancing'] === 'True';
}

// OPM 107986 - Handle standalone seconds.
//   sLPurposeTPe needs to be 'Refinance Cashout' and hidden
//     - handled in pml.aspx.cs -> PageLoad -> TransformData
//   hardcode 'Yes' for second financing and disable radio
//   hardcode No MI - handled in f_set_sProdConvMIOptionT
//   hide appraised value
//   hide P&I pmt and MIP/mon fields
//   show standalone second fields (StandAloneSecondPanel)
// Fields are hidden in f_enableFieldsForLoanType
// Mirror behavior of PML 1.0
//   First LTV and Balance/Amt becomes 'Other Financing'
function f_renderAsSecond() {
    $j('.SecondFinancingRadio').attr('disabled', true);

    var $firstFields = $j('#FirstLtvBal');
    var $secondFields = $j('#SecondLtvBal');
    f_swapElements($firstFields, $secondFields);
    $j('#IsLineOfCreditLineAmountPanel').hide();

    if (PML.options.IsLineOfCredit) {
        $j('#bCloseEnd2ndFinancing').attr('disabled', true);
    }
    if (PML.options.IsEnableHELOC) {
        $j('.SecondFinancingTypeRadio').attr('disabled', true);
    }
}

function f_determineTotalRenovationCosts() {
    // Renovation costs are dependent on fields like the
    // loan amount and equity. If the user has made changes,
    // update the loan file before opening the popup so the 
    // renovation costs reflect any changes the user made.
    if (PML.getIsDirty()) {
        var result = PML.save();
        if (result.error) {
            alert(result.UserMessage);
            return;
        }
    }

    LQBPopup.Show("TotalRenovationCostCalculator.aspx?loanid=" + ML.sLId, {
        height: 720,
        width: 1000,
        onReturn: function (dialogArgs) {
            if (dialogArgs.OK) {
                $j("#sTotalRenovationCosts").val(dialogArgs.sTotalRenovationCosts).change();
            }
        },
        hideCloseButton: true
    }, window);

}

jQuery(function($j) {
    f_populate_sSpStatePe();
    f_insertCustomFields('CustomPMLFieldsContainer', pmlData.CustomPMLFields);

    $j(window).on('loan/view_update_done', function () {
        if (!bInitialized) {
            f_uc_init();

            bIsProduction = (PML.model.loan['IsProduction'] == 'True');

            var bUseSingle = (PML.model.loan['UseBpmiSingle'] == 'True');
            var bUseSplit = (PML.model.loan['UseBpmiSplit'] == 'True');
            // Until the feature is ready, want to remove Borrower Paid Single Prem
            // and BorrowerPaid Split Prem from PMI type
            // Also want to hide the seller credit DDL, this is handled in f_enableFieldsForLoanType.

            if (bUseSingle == false)
                $j('#sProdConvMIOptionT option[value=' + E_sProdConvMIOptionT_BorrPaidSinglePrem + ']').remove();

            if (bUseSplit == false)
                $j('#sProdConvMIOptionT option[value=' + E_sProdConvMIOptionT_BorrPaidSplitPrem + ']').remove();

            $j('#StandAloneSecondPanel').toggle(PML.options.IsStandAloneSecond);
            if (PML.options.IsStandAloneSecond) {
                f_renderAsSecond();
            }
            if (PML.options.IsStandAloneSecond == false) {
                if (PML.options.IsLineOfCredit) {
                    $j('#IsLineOfCreditLineAmountPanel').show();
                    $j('.SecondFinancingRadio').attr('disabled', true);
                } else {
                    $j('#IsLineOfCreditLineAmountPanel').hide();
                }
            }

            bIsPurchaseLoan = !PML.options.IsLineOfCredit && PML.model.loan['sIsRefinancing'] === 'False';

            if (bIsPurchaseLoan) {
                $('#bYesIsPurchaseLoan').prop("checked", true)
            }
            else {
                $('#bNoIsPurchaseLoan').prop("checked", true)
            }

            bIsEnableRenovationCheckboxInPML = PML.model.loan['IsEnableRenovationCheckboxInPML'] == 'True' ||
                $j('#EnableRenovationLoanSupport').val() === "True";

            f_enableFieldsForLoanType();

            f_toggleCreditFields();

            f_onchange_sOccT();
            f_onchange_sProdSpT();
            f_onchange_sLPurposeTPe();

            if (!bIsEnableRenovationCheckboxInPML) {
                $j('#RenovationLoanPanel, #sAsIsAppraisedValuePevalPanel, #InducementPurchPrice, #PurchPriceLessInducement').hide();
                $j('#sApprValPeLabel').text('Appraised Value');
            }
            else {
                f_onclick_sIsRenovationLoan();
            }

            f_set_sProdConvMIOptionT();

            f_onchange_b2ndFinancing();
            f_onchange_2ndFinancingType();

            bIsDisplayOriginatorCompensation = PML.model.loan['sDisplayOriginatorCompensation'] == 'True';
            if (bIsDisplayOriginatorCompensation) {
                f_onchange_sOriginatorCompensationPaymentSourceT();
            }
            else {
                $j('.OriginatorCompensationPanel').hide();
            }

            f_onchange_sProdOverrideUFMIPFF();
            f_onclick_sHasAppraisal();

            bIsEnabledLockPeriodDropDown = PML.model.loan['IsEnabledLockPeriodDropDown'] == 'True';
            if (bIsEnabledLockPeriodDropDown) {
                f_populate_sProdRLckdDaysDDL();
            }
            else {
                $j('#sProdRLckdDaysDDL').remove();
            }

            f_togglePriorSalesExtraInfoPanel();
            f_onchange_sProdConvMIOptionT();

            f_set_RateLockSmartText();

            // If the prior sales seller is Blank, set the value of the DDL to ''
            // this is a workaround because the Bind method (which was already in place)
            // bound "" as the value instead of the Blank enum value
            if (PML.model.loan['sPriorSalesSellerT'] == '0') {
                $j('#sPriorSalesSellerT').val('');
            }

            if (PML.options['DisplayPriceGroup'] && PML.options.Source === 'LO') {
                $j('#PriceGroupPanel').show();
                $j('#sProdLpePriceGroupNm').val(PML.model.loan['sProdLpePriceGroupNm']);
            }
            else {
                $j('#PriceGroupPanel').hide();
            }

            f_onchange_sTitleInsuranceCostT();

            // As of OPM 118337, the expanded lead editor will use the loan file
            // pricer without the street address field and will not immediately 
            // require an application to be filled out.
            $j('#StreetAddressPanel').toggle(!PML.options.IsLead);

            // 1/3/2014 gf - opm 147935
            // 7/10/2014 gf - opm 182198
            $j('#sProdCurrPIPmt').readOnly(PML.model.loan['sProdCurrPIPmtLckd'] === 'False');

            $j('#sTotalRenovationCosts').readOnly(PML.model.loan['sTotalRenovationCostsLckd'] === 'False');

            if (PML.options.HasLinkedSecond) {
                $j('#sIsOFinNewPe').attr('disabled', true);
                $j('#explainLinkedSecondLien').show();
            }

            document.getElementById('sSpStatePe').oldValue = null; // so we can set the county
            UpdateCounties(document.getElementById('sSpStatePe'), document.getElementById('sSpCounty'), postUpdateCounties);

            f_validateRequiredFields();
            f_toggleButtons();
            bInitialized = true;
        }
    });

    function postUpdateCounties() {
        $j('#sSpCounty').val(PML.model.loan['sSpCounty']);
        f_validateRequiredFields();
        f_toggleButtons();
    }

    $j(window).on('apps/create_done', f_toggleCreditFields);
    $j(window).on('apps/delete_done', f_toggleCreditFields);
    $('.mi-need-explanation').on('click', function () {
        LQBPopup.ShowString('Due to state law, the LTV used for determining the need for mortgage insurance in New York ' +
            'is calculated differently than it is in other states. In New York, an appraised value based LTV is used for ' +
            'most property and transaction types, while the purchase price is used for coop purchase transactions.',
            {
                height: 100,
                width: 400,
            });
    });
});
