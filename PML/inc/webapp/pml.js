﻿// pml.js
// Page setup
// Define the PML module and initialize it


/**************
 * PAGE SETUP *
 **************
 * 
 * This section is allowed to know about everybody and everything
 */
var shouldCallUnload = true;


jQuery(window).load(function () {
    $ = $j;
    if (typeof (initializeAngular) != "undefined") {
        initializeAngular();
    }

        $j(".ui-type-switch").click(function () {
            var paramCheck = '&isBeta=true';
            var currentUrl = window.location.href;
            var index = currentUrl.indexOf(paramCheck);
            if (index >= 0) {
                window.location.href = currentUrl.slice(0, index) + currentUrl.slice(index + paramCheck.length);
            }
            else {
                window.location.href = currentUrl + paramCheck;
            }
        });

    jQuery.fx.off = true; // turn off animations

    // Debugging: define a console.log if none exists
    if (typeof window.console === 'undefined') {
        window.console = { log: function(m) { } }; // do nothing when we call console.log if it doesn't exist
    }

    // Debugging: console.log when a custom event triggers
    // Assumes that we're using `$(...).trigger` to fire events
    var oldTrigger = $.fn.trigger;
    $.fn.trigger = function(name, fn) {
        console.log(name + '(' + fn + ')');
        return oldTrigger.call(this, name, fn);
    };

    // The placeholder should go into hiding even if there was a js error.
    var pageIsLoading = true;
    setTimeout(function() {
        if (pageIsLoading) {
            $('#PMLLoadingScreen').hide();
            pageIsLoading = false;
            // logJSException(null, "PML 2.0 took longer than 15 seconds to load!");
        }
    }, 15000);

    FileInfo.init();
    DataInput.init();
    PML.init();

    PML.load();

    // OPM 143067 - remove hidden options from DDLs.
    PML.removeHiddenDDLOptions()

    PML.validate();

    PML.postinit();

    setTimeout(function () {
        $('.loan-info-bar a[data-toggle="tooltip"]').tooltip({
            content: function () {
                // Allow displaying the bolded portion of the tooltip.
                return $(this).prop('title');
            },
            position: {
                my: 'center bottom-5',
                at: 'center top'
            }
        });
    });

    // ** Page-level behavior
    // ** Init
    // Special init cases:
    // Quick pricer
    // No apps

    if (
        PML.options.Source === 'PML' &&
        PML.model.apps.length === 0 &&
        PML.model.loan["sLoanFileT"] != PML.constants.E_sLoanFileT.QuickPricer2Sandboxed) {
        // When we create a new loan from PML, do the following:
        // Using the sHasAnyAppsFilled field, the service page will return 0 apps

        // So create an application tab
        $(window).trigger('apps/create');

        // Then switch to that tab
        DataInput.switchToApplicationTab(0);

    } else if (PML.options.Source === 'LO' &&
            (PML.options.IsLead === false || ( // PML.options.IsLead === true &&
             PML.model.loan["sLoanFileT"] != PML.constants.E_sLoanFileT.QuickPricer2Sandboxed)
            )) {


        // For embedded PML, loans must have at least one application unless they are leads
        if (0 === PML.model.apps.length) {
            $(window).trigger('apps/create');
        }

        // Switch to the first app that is nonvalid
        var switchingToApp = false;
        for (var appNum in PML.model.apps) {
            var isValid = parseInt(model.apps[appNum]["isValid"]); // ok, this is a number for some reason
            if (isValid === 0) {
                switchingToApp = true;
                DataInput.switchToApplicationTab(appNum);
            }
        }

        // If all apps are valid, switch to the property and loan tab
        if (!switchingToApp) {
            DataInput.switchToPropertyAndLoanTab();
        }

    } else {
        DataInput.switchToPropertyAndLoanTab();
    }

    // ** Default element behavior
    // By default, run the loan pricer as soon as we load
    // Don't run if no loan officer is assigned - OPM 113737
    // 1/13/2014 gf - opm 149319 no longer run automatically.
    //    if (PML.options.IsNew === false && PML.getIsValid() && isLoanOfficerAssigned) {
    //        $(window).trigger('loan/price');
    //    }

    // Don't highlight buttons
    $('button,input[type=button]').attr('NoHighlight', 'NoHighlight');

    $('textarea:not(".NoPricing"),input[type=text]:not(".NoPricing"),input[type=radio]:not(".NoPricing"),input[type=checkbox]:not(".NoPricing"),select:not(".NoPricing")')
        .focus(function(e) {
            // When selecting an input, select ALL of the text.
            if (this.type && this.type.toLowerCase() === 'text') {
                $(this).select()
            }
            // For other inputs, this shouldn't affect anything
        }).keypress(function(e) {
            // When the user presses enter, trigger a price
            if (e.which && e.which === 13) {
                $(this).change(); // Trigger change event to ensure data used to price is current.
                $(window).trigger('loan/price');
                return false;
            }
            return true;
        });

    // Turn off the default label behavior. (OPM 106998)
    $('label').click(function(e) { e.preventDefault(); });

    // ** On unload, save if anything's dirty, then remove the window from the
    //   open windows list, unless we're just moving back to the loan

    // Workaround implemented to preserve window management
    //   when switching to and from the new PML UI

    //  OPM 367020 - PML can be framed within other, external pages.  Same Origin policy
    //  may present Permission Denied errors when accessing parent.window.opener.
    try{
        var winOpener = parent.window.opener;
        if (null != winOpener && !winOpener.closed && winOpener.getSkipWindowManagement()) {
            winOpener.gSkipWindowManagement = false;
        }
    }
    catch(e){}

    $(window).on('beforeunload', onPmlUnload);

    if (PML.options.EnableNewTpoLoanNavigation && typeof displayMessage === "function") {
        window.alert = displayMessage;
    }

    // ** Permissions checking
    // This loan purpose is not supported by PML
    if (PML.model.loan["sLPurposeT"] == PML.constants.E_sLPurposeT.Other) {

        var errorMessage = "The current loan purpose is not supported.";

        //If Encompass, redirect to an error page instead of alerting the error, and redirecting to the pipeline.
        if (ML.IsEncompass == "true")
        {
            self.location.href = ML.VirtualRoot + "/common/AppError.aspx?IsEncompass=true&errmsg=" + encodeURIComponent(errorMessage);
        }
        else
        {
            alert(errorMessage);
            PML.goBack();
        }
    }

    // ** Items specific to the header section (perhaps these should go in their own file?)
    //

    if (PML.model.loan["sLoanFileT"] == PML.constants.E_sLoanFileT.QuickPricer2Sandboxed) {
        var w = screen.availWidth;
        var h = screen.availHeight;
    }

    var employeePickerInfo = {
        LoanOfficer: {
            EmployeeIdFieldId: 'LoanOfficer_EmployeeId',
            EmployeeNameFieldId: 'LoanOfficer_FullName',
            EmployeeAssignLinkId: 'assignLoanOfficerLink',
            EmployeePickerUrl: pmlData.LoanOfficerPickerUrl
        },
        Processor: {
            EmployeeIdFieldId: 'Processor_EmployeeId',
            EmployeeNameFieldId: 'Processor_FullName',
            EmployeeAssignLinkId: 'assignProcessorLink',
            EmployeePickerUrl: pmlData.ProcessorPickerUrl
        },
        Secondary: {
            EmployeeIdFieldId: 'Secondary_EmployeeId',
            EmployeeNameFieldId: 'Secondary_FullName',
            EmployeeAssignLinkId: 'assignSecondaryLink',
            EmployeePickerUrl: pmlData.SecondaryPickerUrl
        },
        PostCloser: {
            EmployeeIdFieldId: 'PostCloser_EmployeeId',
            EmployeeNameFieldId: 'PostCloser_FullName',
            EmployeeAssignLinkId: 'assignPostCloserLink',
            EmployeePickerUrl: pmlData.PostCloserPickerUrl
        }
    };

    PML.setEmployeePickerInfo(employeePickerInfo);
    initEmployeePickers();

    $('#messageBtn').click(function() {
        LQBPopup.Show(gVirtualRoot + "/main/BrokerNotes.aspx?loanid=" + loanId, {
            closeText: 'Cancel',
            hideCloseButton: true
        });
    });

    // Label the back button and attach a link
    if (PML.options.Source === 'LO') {
        $('.backBtn').text('Go back to LendingQB');
    } else if (PML.options.Source === 'PML') {
        $('.backBtn').text('Go back to Loan Pipeline');
    } else {
        // What happened?!
        //throw "Unexpected page source, got " + source;
    }

    $('.backBtn').prop('href', '#')
                 .click(function () { PML.goBack(); });

    // We're done loading. Show the page.
    setTimeout(function() {
        if (pageIsLoading) {
            $('#PMLLoadingScreen').hide();
            pageIsLoading = false;
        }
    }, 500);

    function initEmployeePickers() {
        initializeEmployeePicker(employeePickerInfo.LoanOfficer);
        initializeEmployeePicker(employeePickerInfo.Processor);
        initializeEmployeePicker(employeePickerInfo.Secondary);
        initializeEmployeePicker(employeePickerInfo.PostCloser);

        if (PML.model.loan['sBranchChannelT'] === PML.constants.E_BranchChannelT.Correspondent.toString()) {
            $('.messageAndBackButtonContainer').detach().appendTo('.secondaryAndPostCloserAssignment');

            if (PML.options.Source === 'LO') {
                $('.loanOfficerAssignment, .processorAssignment').hide();
            }

            $('.secondaryAssignment, .postCloserAssignment').show();
        } else {
            $('.loanOfficerAssignment, .processorAssignment').show();
            $('.secondaryAssignment, .postCloserAssignment').hide();
        }

        // If it is from LO, then we want to hide from embedded.
        if (PML.options.Source === 'LO') {
            $('.hideIfEmbedded').hide();
        } else {
            $('.hideIfEmbedded').show();
        }
    }

    function initializeEmployeePicker(pickerInfo) {
        $('#' + pickerInfo.EmployeeNameFieldId).text(PML.model.loan[pickerInfo.EmployeeNameFieldId]);

        $('#' + pickerInfo.EmployeeAssignLinkId).click(function() {
            PML.promptEmployeeAssignment(
                pickerInfo.EmployeeIdFieldId,
                pickerInfo.EmployeeNameFieldId,
                pickerInfo.EmployeeAssignLinkId,
                pickerInfo.EmployeePickerUrl);
        }).text(function() {
            if (PML.model.loan[pickerInfo.EmployeeIdFieldId] !== '00000000-0000-0000-0000-000000000000') {
                return 're-assign';
            } else {
                return 'assign';
            }
        })
    }

    function toggleFthbContainer() {
        if (pmlData.IsQP2) {
            $j("div[id*='FTHBContainer']").toggle($j("#bYesIsPurchaseLoan").prop("checked"));
        }
    }

    function toggleHasHousingHist() {
        if (pmlData.IsQP2) {
            $j("#HasHousingHistContainer").toggle($j("#aBTotalScoreIsFthbQP").prop("checked"));
        }
    }

    $j(".IsPurchaseLoanRadio").click(toggleFthbContainer);
    $j("#aBTotalScoreIsFthbQP").click(toggleHasHousingHist);
    $j('#FthbExplain').click(function () {
        return f_openHelp('HousingHistory.aspx', 450, 360);
    });

    toggleFthbContainer();
    toggleHasHousingHist();

    
    /*
     * http://stackoverflow.com/questions/7731778/get-query-string-parameters-with-jquery#answer-7732379
     */
    function queryString(key) {
        key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
        var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
        return match && decodeURIComponent(match[1].replace(/\+/g, " "));
    }

    if (queryString('highlightId'))
    {
        $('#' + queryString('highlightId')).focus();
    }
});

function onPmlUnload() {
    if (!shouldCallUnload)
    {
        shouldCallUnload = true;
        return;
    }

    if (PML.getIsDirty()) {
        var result = PML.save();

        if (PML.options.EnableNewTpoLoanNavigation &&
            typeof result.value !== "undefined" &&
            typeof result.value.PermissionError === "string") {
            return result.value.PermissionError;
        }
    }

    if (PML.model.loan["sLoanFileT"] != PML.constants.E_sLoanFileT.QuickPricer2Sandboxed) {
        try {
            var winOpener = parent.window.opener;

            if (null != winOpener && !winOpener.closed) {
                if (typeof (winOpener.removeFromCurrentLoanList) != "undefined") {
                    winOpener.removeFromCurrentLoanList(PML.getLoanId());
                }
            }
        } catch (e) { }
    }

    if (typeof bSuccess != 'undefined' && bSuccess == false)
        return "Unable to save. If you leave the page, your changes will be lost.";
}

var Results = { AreDisplayed: false, AreStale: false };

/*******
 * PML *
 *******
 * The goal of this object is to
 * - populate the model
 * - populate the view
 * - keep track of changes to the view
 * - propagate those changes to the model
 * - --- Communicate with the server ---
 * - load from the codebehind
 * - save to the codebehind
 * - retrieve calculated values from the codebehind
 *
 * This shall NOT
 * - handle string formatting
 * - define any style or layout information
 * - know about any of the controls it'll be used with
 */
var PML = (function($) {
    isValid = true;
    isInitialLoad = false;
    var isRefreshing = false;
    loanId = '';
    options = {
        IsLead: false,
        IsEditLeadsInFullLoanEditor: false,
        IsNew: false,
        IsReadOnly: false,
        UserDoesntNeedCreditToPrice: false,
        Source: '', // PML or LO
        DisplayPriceGroup: false,
        RequireEmail: false,
        EnableMultipleApps: false,
        HideOrderNewCredit: false,
        IsStandAloneSecond: false,
        IsAllowHelocZeroInitialDraw: false,
        IsLineOfCredit: false,
        BorrPdCompMayNotExceedLenderPdComp: false,
        UserHasQP2Enabled: false,
        IsDisableAutoCalcOfTexas50a6: false,
        EnableNewTpoLoanNavigation : false

    };
    currentAppNum = 0;
    primaryAppNum = 0; // this is unlikely to change

    constants = {};

    dirty = false;
    model = {
        loan: {},
        apps: []
    };
    fieldLocations = {}; // Dictionary matching string fieldID to 'loan' or 'app'
    fieldDOM = {}; // Dictionary matching string fieldID to jquery object: $('[name=fieldID]')

    // Dictionary matching string dropdownID to a list of key-value pairs: [{ Key: #, Value: # }, ... ]
    ddls = {};
    hiddenOptions = {};
    var citizenshipDropdownConfigurations = {};

    // Array of fields that should not make the displayed results dirty.
    resultsDirtyBlacklist = [];

    // Array of functions and arguments to call after a successful data refresh.
    var postRefreshCallbacks = [];

    // Information about the employee pickers for role assignment.
    var employeePickerInfo;

    function init() {
        isInitialLoad = true;
        currentAppNum = -1;
        loanId = ML.sLId;

        // ** Extract from pmlData **
        ddls = pmlData.Dropdowns; // 'pmlData' is populated in pml.aspx.cs
        populateDropDownLists();
        citizenshipDropdownConfigurations = pmlData.CitizenshipDropdownConfigurations;

        hiddenOptions = pmlData.HidenOptions;  // 'pmlData' is populated in pml.aspx.cs

        options.Source = pmlData.Source;
        options.IsLead = pmlData.IsLead;
        options.IsNew = pmlData.IsNew;
        options.IsReadOnly = pmlData.IsReadOnly;
        options.IsEditLeadsInFullLoanEditor = pmlData.IsEditLeadsInFullLoanEditor;
        options.UserDoesntNeedCreditToPrice = pmlData.UserDoesntNeedCreditToPrice;
        options.DisplayPriceGroup = pmlData.DisplayPriceGroup;
        options.RequireEmail = pmlData.RequireEmail;
        options.EnableMultipleApps = pmlData.EnableMultipleApps;
        options.HideOrderNewCredit = pmlData.HideOrderNewCredit;
        options.IsStandAloneSecond = pmlData.IsStandAloneSecond;
        options.IsAllowHelocZeroInitialDraw = pmlData.IsAllowHelocZeroInitialDraw;
        options.IsLineOfCredit = pmlData.IsLineOfCredit;
        options.IsEnableHELOC = pmlData.IsEnableHELOC;
        options.BorrPdCompMayNotExceedLenderPdComp = pmlData.BorrPdCompMayNotExceedLenderPdComp;
        options.UserHasQP2Enabled = pmlData.UserHasQP2Enabled;
        options.IsDisableAutoCalcOfTexas50a6 = pmlData.IsDisableAutoCalcOfTexas50a6;
        options.EnableNewTpoLoanNavigation = pmlData.EnableNewTpoLoanNavigation;
        options.HasLinkedSecond = pmlData.HasLinkedSecond;
        $.extend(constants, pmlData.Constants);

        resultsDirtyBlacklist = pmlData.ResultsDirtyBlacklist;

        // ** Init view **

        // Name all inputs so that they'll work with getAllFormValues
        var inputs = $('form :input');
        for (var i in inputs) {
            var input = inputs[i];

            var name = input.name;
            if (!name || 0 === name.length) { // if doesn't have a name, give it the same name as the id
                input.name = input.id;
            }
        };

        // ** Attach listeners **

        $("input[name='IsPurchaseLoan'], input[name='sHasSecondFinancingT'], #sLPurposeTPe, #sProd3rdPartyUwResultT").on('change', delayChange);

        // Each data-calc input listens for the change event, which will trigger a changed_{id} event
        $('textarea[data-calc],input[data-calc],select[data-calc]').on('change', on_datacalc_changed);
        $('#sDownPmtPcPe,#sEquityPe,#sLtvRPe,#sLAmtCalcPe').on('blur', on_datacalc_changed);

        // Custom events
        $(window).on('apps/create', on_apps_create);
        $(window).on('apps/delete', on_apps_delete);
        $(window).on('apps/switch_current_app', on_apps_switch);

        $(window).on('loan/price', on_loan_price);

        $(PML.ActionArea).on('ComparePinned', on_compare_pinned);

        // Initial setup for display type section
        if (!ML.displayTypeEnabled) {
            $('td[displayTypeCell]').hide();
        } else {
            $('td[displayTypeCellHidden]').hide();
        }
        if (ML.bestPriceDisabled) {
            $('#AllProgramsRb').attr("checked", "checked");
        }
    }

    /**
    * load(data)
    *
    * Load values from data into the model and loan view.
    * If source is undefined, then load using a service call.
    */
    function load(data) {
        if (!data) {
            // Collect the values with a service call
            var result = loadLoan();
            data = result.value;
            // These values are set through "SetResult" in the service page
            // e.g. result.value.loan.sLId would be the loan ID
        }

        // Extend our current model with the results from the load
        updateModel(data);
        updateViewLoan();
        if (0 <= currentAppNum) {
            updateViewApp(currentAppNum);
        }
        isInitialLoad = false; // For the first load, this will be true. Every subsequent load will be false.
    }

    /**
    * validate()
    */
    function validate() {
        var pageValid = true;
        if (model.apps) {
            var appsLength = model.apps.length;
            for (var appNum = 0; appNum < appsLength; appNum++) {
                pageValid &= model.apps[appNum]["isValid"];
            }
        }
        pageValid &= model.loan["isValid"];
        pageValid &= model.loan["isFilterValid"];
        pageValid &= model.loan["isGeneralInfoValid"];
        pageValid &= (options.UserDoesntNeedCreditToPrice
                        || model.loan.sLoanFileT == constants.E_sLoanFileT.QuickPricer2Sandboxed
                        || model.loan["sAllAppsHasCreditReportOnFile"]);

        isValid = pageValid;
        $(window).trigger("PageIsValid", [pageValid]);
    }

    /**
    * postinit()
    */
    function postinit() {
        $(window).on("apps/changed/isValid", validate);
        $(window).on("loan/changed/isValid", validate);
        $(window).on("loan/changed/isFilterValid", validate);
        $(window).on("loan/changed/isGeneralInfoValid", validate);
    }

    /**
    * populateDropDownLists()
    *
    * Populate all the dropdownlists on the page.
    */
    function populateDropDownLists() {
        // For each dropdownlist, check if the id matches one in ddls
        // Then populate it based on the contents of ddls
        var dropdownlists = $('select');
        for (var i in dropdownlists) {
            var item = dropdownlists[i];
            var id = item.id;
            if (ddls[id]) {
                populateDropDownList($(item), ddls[id]);
            }
        }
    }

    /**
    * populateDropDownList()
    *
    * Populate a single dropdownlist.
    */
    function populateDropDownList($dropDown, dropDownOptions) {
        for (var i in dropDownOptions) {
            var item = dropDownOptions[i];
            var $option = $('<option>').val(item.Key).text(item.Value);
            $dropDown.append($option);
        }
    }

    /**
    * removeHiddenDDLOptions()
    *
    * Remove hidden options from all the dropdownlists on the page.
    */
    function removeHiddenDDLOptions() {
        // For each dropdownlist, check if the id matches one in ddls
        // Then remove options from it based on the contents of hiddenOptions
        var dropdownlists = $('select');
        for (var i in dropdownlists) {
            var item = dropdownlists[i];
            var id = item.id;
            if (hiddenOptions[id]) {
                removeHiddenDDLOptionsHelper($(item), hiddenOptions[id]);
            }
        }
    }

    /**
    * removeHiddenDDLOptionsHelper()
    *
    * Remove hidden options from a single dropdownlist.
    */
    function removeHiddenDDLOptionsHelper($dropDown, hiddenOptions) {
        var selectedValue = $dropDown.find(":selected").val();
        for (var i in hiddenOptions) {
            var val = hiddenOptions[i];
            var $option = $dropDown.find("option[value='" + val + "']");
            if ($option && $option.val() != selectedValue) {
                $option.remove();
            }
            else if ($option && $option.val() == selectedValue) {
                $option.text($option.text() + " (not available)");
            }
        }
    }

    /////////////////////
    // EVENT HANDLERS
    /////////////////////

    /**
    * on_datacalc_changed()
    *
    * When a field marked 'data-calc' is changed, update the model
    */
    function on_datacalc_changed(e) {
        var el = this;
        
        if (el && el.id == 'sProdConvMIOptionT' && setAsUserInput) {
            setAsUserInput(e);
        }

        updateField(el);

    }
    
    
    var timeLimit = 350;
    var lastTimeOut = {};
    var allWaitTime = {};
    var currentDelayedTrigger = "";

    var delayedCalcVersions = {};

    function delayChange(e)
    {
        var el = $(this);
        var id = el.attr("id");
        var lastWaitTime;

        // For Radio buttons, use the group name as the id.
        if (el.attr("type") == "radio")
        {
            id = el.attr("name");
        }

        // If this event was not triggered manually by a user, and if there isn't already a timeout for this element, then don't delay.
        if (!e.originalEvent && typeof lastTimeOut[id] == 'undefined') {
            return;
        }

        if (allWaitTime[id] == 0 || typeof allWaitTime[id] == 'undefined') {
            allWaitTime[id] = Date.now();
            lastWaitTime = Date.now();
        }
        else {
            lastWaitTime = allWaitTime[id];
        }

        var timeWaited = Date.now() - lastWaitTime;

        if (timeWaited < timeLimit) {
            allWaitTime[id] = Date.now();

            if (lastTimeOut[id] != undefined) {
                clearTimeout(lastTimeOut[id]);
            }

            lastTimeOut[id] = setTimeout(function () { triggerChange(el); }, timeLimit);
            e.stopImmediatePropagation();

            currentDelayedTrigger = el.attr("id");
            var lastVersion = delayedCalcVersions[el.attr("id")];
            if (typeof lastVersion == 'undefined') {
                lastVersion = 0;
            }
            delayedCalcVersions[el.attr("id")] = lastVersion + 1;

        }
        else {
            // Reset the stored items for this element.
            lastTimeOut[id] = undefined;
            allWaitTime[id] = 0;
        }
    }

    function triggerChange(el) {
        el.trigger("change");
        // Once the change event has ended, clear the currentDelayedTrigger to prevent other unrelated calculations from getting versioned.
        currentDelayedTrigger = "";
    }

    /**
    * on_apps_create(e)
    *
    * On 'apps/create', create an empty loan applicant.
    * On success, update the model and trigger the 'apps/create_done' event.
    */
    function on_apps_create(e) {
        var result = createApp();
        var appNum = result.value.appNum;

        if (result.value.app) {
            currentAppNum = -1;
            model.apps.push({});
            var app = JSON.parse(result.value.app);
            updateModelApp(app, appNum);
            $(window).trigger('apps/create_done', [appNum, !isInitialLoad]); // args are [int appNum, bool switchToTab]
        }
    }

    function on_apps_delete(e, appNum) {
        var result = deleteApp(appNum);

        if (result.value.success) {
            if (result.value.success == "True") {
                model.apps.splice(appNum, 1); // remove the app from the apps list in the model
                currentAppNum = -1; // right now, the current app doesn't exist
                $(window).trigger('apps/delete_done', [appNum]);
            }
            else {
                alert(result.value.Error);
            }
        }
    }

    /**
    * on_apps_switch(e, appNum)
    *
    * Switch the current app to the specified appNum.
    * Trigger 'apps/view_update_done', when the view has completed updating.
    */
    function on_apps_switch(e, appNum) {
        currentAppNum = appNum;
        updateViewApp(appNum);
    }

    /**
    * on_loan_price(e)
    *
    */
    function on_loan_price(e) {
        // see ResultsFilter.ascx
    }

    /**
    * on_compare_pinned(e)
    *
    */
    var _popupWindow;
    function on_compare_pinned(e) {
        save();
        _popupWindow = window.open('../webapp/RunLoanComparison.aspx?loanid=' + loanId, "Result",
    'width=750px, height=650px,  resizable=yes, scrollbars=yes, status=yes, help=no, centerscreen=yes');
        _popupWindow.focus();
    }

    /////////////////////
    // UPDATE
    /////////////////////

    function updateFieldInModel(el)
    {
        var val;
        var fieldId = el.name;
        var tagName = el.tagName;
        if (tagName) {
            tagName = tagName.toLowerCase();
        }
        if (tagName === 'input') {
            var inputType = (el.type || 'text').toLowerCase();
            if (inputType === 'checkbox') {
                val = el.checked ? "True" : "False";
            } else { // text, radio
                val = el.value;
            }
        } else if (tagName === 'select') {
            val = el.options[el.selectedIndex].value;
        } else { // textarea
            val = el.value;
        }

        var fieldIsDirty = false;
        if (fieldLocations[fieldId] === 'loan') {
            model.loan[fieldId] = val;
            $(window).trigger('loan/changed/' + fieldId, val);
            fieldIsDirty = true;
        } else if (fieldLocations[fieldId] === 'app' && 0 <= currentAppNum) {
            model.apps[currentAppNum][fieldId] = val;
            $(window).trigger('apps/changed/' + fieldId, [currentAppNum, val]);
            fieldIsDirty = true;
        } else if (pmlData.IsQP2) {
            fieldIsDirty = true;
        }

        if (fieldIsDirty)
        {
            dirty = true;
            Results.AreStale = $.inArray(fieldId, resultsDirtyBlacklist) < 0; // If this is in the blacklist then results aren't stale.
        }
        return fieldIsDirty;
    }

    function updateField(el) {
        // Update the model
        var fieldId = el.name;
        var fieldIsDirty = updateFieldInModel(el);

        // Refresh if we need to
        if (!isInitialLoad && fieldIsDirty) {
            dirty = true;
            Results.AreStale = $.inArray(fieldId, resultsDirtyBlacklist) < 0; // If this is in the blacklist then results aren't stale.
            var $this = $(el);
            // check if it has a 'data-refresh' attribute
            var refresh = $this.data('refresh');
            if (typeof refresh !== 'undefined') {
                refreshFieldData($this, fieldId)
            }
        }
    }

    function refreshFieldData($this, fieldId) {
        var args = getAllModelValues();
        args.CurrentApp = currentAppNum;
        if ($this) {
            var prodCalc = $this.data('sProdCalcEntryT');
            if (typeof prodCalc !== 'undefined') {
                args['sProdCalcEntryT'] = prodCalc;
            }

            console.log('Data refresh caused by ' + fieldId);
        }

        refreshData(args);

        validate(); // expecting this to be a low cost call
    }

    /**
    * updateModel(data)
    *
    * Take the data (usually from an ajax response), and apply it
    * to the corresponding fields.
    * We are assuming *data* has a *loan* object and an *apps* array.
    */
    function updateModel(data) {
        // App

        if (!pmlData.IsQP2) {
            var apps = JSON.parse(data.apps);
            updateModelApps(apps);
        }

        // Loan
        var loan = JSON.parse(data.loan);
        updateModelLoan(loan);

        if (loan['sLoanFileT'] == 2) {
            var apps = JSON.parse(data.apps);
            $j("#aBHasHousingHistQP").prop("checked", apps[0]["aBHasHousingHistQP"] == "True");
            $j("#aBTotalScoreIsFthbQP").prop("checked", apps[0]["aBTotalScoreIsFthbQP"] == "True");
        }
    }

    function updateModelLoan(loan) {
        for (var dataId in loan) {
            if (model.loan[dataId] !== loan[dataId]) {
                fieldLocations[dataId] = 'loan';

                // Update the model
                model.loan[dataId] = loan[dataId];

                // Trigger a custom event (publish) that says this field changed
                // To subscribe to this event use something like $(window).on('changed/{id}', on_trigger)
                $(window).trigger('loan/changed/' + dataId, model.loan[dataId]);
            }
        }
    }

    function updateModelApps(apps) {
        for (var appNum in apps) {
            // If the app does not exist within the model already, create it
            // Otherwise, use the existing app in the model
            var addingApp = false;
            if (model.apps.length <= appNum) {
                model.apps.push({});
                addingApp = true;
            } else {
                modelApp = model.apps[appNum];
            }

            updateModelApp(apps[appNum], appNum);

            // This will cause the view to be updated. Do this after the model is populated
            // so it populates the values.
            if (addingApp) {
                $(window).trigger('apps/create_done', [appNum, !isInitialLoad]); // args are [int appNum, bool switchToTab]
            }
        }
    }

    function updateModelApp(app, appNum) {
        var modelApp = model.apps[appNum]; ;

        for (var dataId in app) {
            if (modelApp[dataId] !== app[dataId]) {
                fieldLocations[dataId] = 'app';
                // Same as the loan

                modelApp[dataId] = app[dataId];
                $(window).trigger('apps/changed/' + dataId, [appNum, modelApp[dataId]]);
            }
        }
    }

    /**
    * updateViewLoan()
    *
    * Take the loan data from the model and apply it to the view.
    */
    function updateViewLoan() {
        var $activeElement = $(document.activeElement);
        populateDOM(model.loan);

        if (typeof updateLoanNavigationHeaderLoanData === 'function') {
            updateLoanNavigationHeader(model.loan);
        }

        $(window).trigger('loan/view_update_done');

        // If this is the result of a background calculation, this could cause
        // the newly updated/selected field to lose selection. Focus to restore.
        if ($activeElement.is('input[data-calc]:text')) {
            $activeElement.focus();
        }
    }

    /**
    * updateViewApp(appNum)
    *
    * Take the app data from apps[appNum] in the model and apply it to the view.
    */
    function updateViewApp(appNum) {
        var app = model.apps[appNum];
        var $activeElement = $(document.activeElement);
        populateDOM(app);

        $(window).trigger('apps/view_update_done', [appNum]);

        if ($activeElement.is('input[data-calc]:text')) {
            $activeElement.focus();
        }
    }

    function populateDOM(obj) {
        // Using this instead of common.js::populateForm because this is faster
        var $inputList = $('form :input');
        for (property in obj) {
            var val = obj[property];
            if (!fieldDOM[property]) {
                fieldDOM[property] = $inputList.filter('[name="' + property + '"]');
            }

            fieldDOM[property].each(function() {
                var $this = $(this);
                var type = $this.prop('type');
                if (type == 'radio') {
                    $this.prop('checked', $this.val() == val);
                }
                else if (type == 'checkbox') {
                    $this.prop('checked', val == 'True' || val == true);
                }
                else {
                    $this.val(val);
                }
            });
        }
    }

    function getAllModelValues() {
        var args = {};
        var loan = model.loan;
        for (var loanDataId in loan) {
            args[loanDataId] = loan[loanDataId];
        }

        if (loan['sLoanFileT'] == 2) {
            args["aBHasHousingHistQP"] = $j("#aBHasHousingHistQP").prop("checked");
            args["aBTotalScoreIsFthbQP"] = $j("#aBTotalScoreIsFthbQP").prop("checked");
        }

        if (0 <= currentAppNum) {
            var app = model.apps[currentAppNum];
            for (var appDataId in app) {
                args[appDataId] = app[appDataId];
            }
        }

        return args;
    }

    function updateAll(args)
    {
        if (args == null)
        {
            return;
        }

        if(typeof(loadError) != 'undefined')
        {
            var result = loadError(args);

            if (result)
            {
                return result;
            }
        }

        console.log('Data refresh');
        if (args.value.loan) {
            var loan = JSON.parse(args.value.loan);
            updateModelLoan(loan);
        }
        updateViewLoan();

        if (args.value.apps) {
            var apps = JSON.parse(args.value.apps);
            updateModelApp(apps[currentAppNum], currentAppNum);
        }

        // update the current app
        if (0 <= currentAppNum && currentAppNum < model.apps.length) {
            updateViewApp(currentAppNum);
        }

        isRefreshing = false;
        for (var i = 0; i < postRefreshCallbacks.length; i++) {
            var obj = postRefreshCallbacks[i];
            obj.fn.apply(window, obj.args);
        }
        postRefreshCallbacks.length = 0;

        return false;
    }

    ///////////////////////////////
    // SERVICE PAGE COMMUNICATION
    ///////////////////////////////

    /**
    * save()
    *
    * Save both the current app and the loan.
    */
    function save() {
        console.log('save');

        var args = getAllModelValues();
        args.sLId = loanId;
        args.CurrentApp = currentAppNum;
        var result = gService.PML.call("SaveData", args);

        dirty = false;
        return result;
    }

    /**
    * async_save()
    *
    * Asynchronous save.
    */
    function async_save(callback, errback, refresh) {
        console.log('async_save');

        if (typeof callback === 'undefined') {
            var callback = function(r) {
                console.log('Save complete.');
            }
        }

        // If it's readonly, don't save
        if (options.IsReadOnly) {
            callback();
            return;
        }

        if (refresh != null)
        {
            isRefreshing = refresh;
        }

        var args = getAllModelValues();
        args.sLId = loanId;
        args.CurrentApp = currentAppNum;
        var result = gService.PML.acall("SaveData", args, callback, errback);

        dirty = false;
        return result;
    }

    function updatePinCount() {
        console.log('updatePinCount');

        var args = { sLId: loanId };
        var result = gService.PML.call("GetPinCount", args);
        if (result.value && result.value.sPinCount) {
            $(window).trigger('loan/changed/sPinCount', result.value.sPinCount);
        }
    }

    function saveLoan() {
        console.log('saveLoan');
        // If it's readonly, don't save
        if (options.IsReadOnly) {
            return;
        }

        var args = getAllModelValues();
        args.sLId = loanId;
        args.CurrentApp = currentAppNum;
        var result = gService.PML.call("SaveLoan", args);

        dirty = false;
        return result;
    }

    function saveApp(appNum) {
        console.log('saveApp');
        // If it's readonly, don't save
        if (options.IsReadOnly) {
            return;
        }

        var args = getAllModelValues();
        args.sLId = loanId;
        args.CurrentApp = appNum;
        var result = gService.PML.call("SaveApp", args);

        dirty = false;
        return result
    }

    function saveAgents() {
        console.log('saveAgents');
        // If it's readonly, don't save
        if (options.IsReadOnly) {
            return false;
        }

        var args = {
            LoanOfficer_EmployeeId: model.loan[employeePickerInfo.LoanOfficer.EmployeeIdFieldId],
            Processor_EmployeeId: model.loan[employeePickerInfo.Processor.EmployeeIdFieldId],
            Secondary_EmployeeId: model.loan[employeePickerInfo.Secondary.EmployeeIdFieldId],
            PostCloser_EmployeeId: model.loan[employeePickerInfo.PostCloser.EmployeeIdFieldId]
        };
        args.sLId = loanId;
        var result = gService.PML.call("SaveAgents", args, false, false, true);

        // OPM 197251 - Handle service errors
        if (result.error) {
            alert("Error: " + result.UserMessage);
            return false;
        }

        if (result.value.sProdLpePriceGroupNm) {
            model.loan["sProdLpePriceGroupNm"] = result.value.sProdLpePriceGroupNm;
        }

        return true; // OPM 232633 - return true on success, false on failure.
    }

    function createApp() {
        console.log('createApp');

        // Want to save when creating the initial app so proper values
        // can be imported from property and loan tab.
        if (dirty || currentAppNum == -1) {
            save();
        }

        var args = {
            sLId: loanId,
            numApps: model.apps.length
        };

        var result = gService.PML.call("CreateApp", args);
        return result;
    }

    function deleteApp(appNum) {
        console.log('deleteApp');
        var args = {
            sLId: loanId,
            appNum: appNum
        };

        var result = gService.PML.call("DeleteApp", args);
        return result;
    }

    function loadLoan() {
        console.log('loadLoan');
        var args = {
            sLId: loanId
        };
        var result = gService.PML.call("LoadData", args);

        // error checking
        return result;
    }

    // Calc should be asynchronous
    function async_calc(args, callback, errback) {
        console.log('async_calc');

        args.sLId = loanId;

        args.CurrentApp = currentAppNum;

        gService.PML.acall("CalculateData", args, callback, errback);
    }

    function calc(args, callback, errback) {
        console.log('calc');

        args.sLId = loanId;

        args.CurrentApp = currentAppNum;

        var result = gService.PML.call("CalculateData", args);

        callback(result);
    }

    function sync_calc(callback, errback) {
        var args = getAllModelValues();

        calc(args, updateAll, function (result) {
            isRefreshing = false;
            postRefreshCallbacks.length = 0;
        });
    }

    /**
    * refreshData($el)
    *
    */
    function refreshData(args) {
        isRefreshing = true;
        var calcVersion = delayedCalcVersions[currentDelayedTrigger];
        var lastDelayedTrigger = currentDelayedTrigger;

        async_calc(args, function (result) {
            if (lastDelayedTrigger != null && lastDelayedTrigger != "" && delayedCalcVersions[lastDelayedTrigger] != calcVersion)
            {
                // If a calculation was triggered by a delayed field, and the calculation is already out of date, do not populate the 
                // loan model with it.  This will prevent looping errors.
                console.log("Refresh Callback stopped due to out of date.");
                return;
            }
            if (result.value.loan) {
                var loan = JSON.parse(result.value.loan);
                updateModelLoan(loan);
            }
            updateViewLoan();

            if (result.value.apps) {
                var apps = JSON.parse(result.value.apps);
                updateModelApp(apps[currentAppNum], currentAppNum);
            }

            // update the current app
            if (0 <= currentAppNum && currentAppNum < model.apps.length) {
                updateViewApp(currentAppNum);
            }

            isRefreshing = false;
            for (var i = 0; i < postRefreshCallbacks.length; i++) {
                var obj = postRefreshCallbacks[i];
                obj.fn.apply(window, obj.args);
            }
            postRefreshCallbacks.length = 0;
        }, function(result) {
            isRefreshing = false;
            postRefreshCallbacks.length = 0;
        });
    }

    /**
    * addPostRefreshCallback(fn, args)
    * 
    * Adds function and arguments to list to execute after a data refresh.
    * Assumes args is an array if it is provided.
    */
    function addPostRefreshCallback(fn, args) {
        if (typeof fn !== 'function') return;
        postRefreshCallbacks.push({ fn: fn, args: args || [] });
    }

    /////////////////////
    // MISC
    /////////////////////

    /**
     * Navigates back from the quick pricer (PML) to where the user came from.
     * For Embedded PML, this means the main loan editor, and for PML users it means the TPO portal.
     * @param {string} additionalUrlParams - Additional URL parameters to supply to the target page. 
     * Example: '&highurl=10&appid=8e87b974-eb7c-47ba-9969-a74f00cba1b5' for loanapp.aspx.
     */
    function goBack(additionalUrlParams) {
        if (ML.IsEncompass == "true")
        {
            // Don't perform any function if Encompass is being used.  Those users should not be able
            // to go back.
            return;
        }

        if (PML.model.loan.sLoanFileT == PML.constants.E_sLoanFileT.QuickPricer2Sandboxed
        && PML.options.Source === "LO") {
            top.close();
            return;
        }

        var trueVirtualRoot = ML.VirtualRoot.replace(/\/embeddedpml/i, '');
        if (typeof (additionalUrlParams) !== 'string')
        {
            additionalUrlParams = '';
        }

        if (options.Source === 'LO') {
            // Workaround implemented to preserve window management
            // when switching to and from the new PML UI
            try {
                var winOpener = parent.window.opener;
                if (null != winOpener && !winOpener.closed && winOpener.getSkipWindowManagement()) {
                    winOpener.gSkipWindowManagement = true;
                }
            } catch (e) { }

            if (options.IsLead && !options.IsEditLeadsInFullLoanEditor) {
                top.location.href = trueVirtualRoot + '/los/lead/leadmainframe.aspx?loanid=' + loanId + additionalUrlParams;
            } else {
                top.location.href = trueVirtualRoot + '/newlos/loanapp.aspx?loanid=' + loanId + additionalUrlParams;
            }
        } else if (PML.options.EnableNewTpoLoanNavigation) {
            self.location.href = ML.VirtualRoot + "/webapp/LoanInformation.aspx?loanid=" + loanId + additionalUrlParams;
        } else {
            self.location.href = ML.VirtualRoot + "/Main/Pipeline.aspx" + additionalUrlParams;
        }
    }
    window.goBack = goBack;

    // probably we should put the header stuff in it's own file.
    function promptEmployeeAssignmentForPricing(callbackOnAssignment) {
        var fieldInfo;

        if (model.loan['sBranchChannelT'] === constants.E_BranchChannelT.Correspondent.toString()) {
            fieldInfo = employeePickerInfo.Secondary;
        } else {
            fieldInfo = employeePickerInfo.LoanOfficer;
        }

        promptEmployeeAssignment(
            fieldInfo.EmployeeIdFieldId,
            fieldInfo.EmployeeNameFieldId,
            fieldInfo.EmployeeAssignLinkId,
            fieldInfo.EmployeePickerUrl,
            callbackOnAssignment);
    }

    function promptEmployeeAssignment(id, name, assign, url, callbackOnAssignment) {
        function assignAgent(agent) {
            // OPM 232663 - Store old values in case we need to revert.
            var oldId = model.loan[id];
            var oldName = model.loan[name];

            model.loan[id] = agent.employeeId;
            model.loan[name] = agent.fullName;

            // Warning: a loan officer can change this, causing them to LOSE ACCESS to the loan!
            if (!saveAgents())
            {
                // OPM 232633 - if save agents fails, then revert to old values and just return.
                // NOTE: saveAgents() alerts the user of error, no need to do so here.
                model.loan[id] = oldId;
                model.loan[name] = oldName;
                return;
            }

            $('#' + name).text(agent.fullName);

            dirty = true;
            updateViewLoan();

            $('#' + assign).text("re-assign");

            if (typeof (callbackOnAssignment) != 'undefined') {
                callbackOnAssignment();
            }
        }

        var bShowWarning = false;
        if (model.loan['sBranchChannelT'] === constants.E_BranchChannelT.Correspondent.toString()) {
            bShowWarning = id === employeePickerInfo.Secondary.EmployeeIdFieldId;
        } else {
            bShowWarning = id === employeePickerInfo.LoanOfficer.EmployeeIdFieldId;
        }

        var fullUrl = url;
        if (bShowWarning) {
            fullUrl += '&showlowarning=t';
        }

        if (model.loan[id] === '00000000-0000-0000-0000-000000000000') {
            LQBPopup.Show(fullUrl, {
                'height': 375,
                'width': 500,
                'onReturn': assignAgent,
                'closeText': "Cancel" // since no longer requiring LO assignment for PML, can just close the dialog.
            });
        }
        else {
            LQBPopup.Show(fullUrl, {
                'onReturn': assignAgent
            });
        }
    }

    function requiresEmployeeAssignmentForPricing() {
        if (model.loan["sLoanFileT"] === PML.constants.E_sLoanFileT.QuickPricer2Sandboxed.toString()) {
            return false;
        }

        var employeeId;
        if (model.loan['sBranchChannelT'] === constants.E_BranchChannelT.Correspondent.toString()) {
            employeeId = model.loan[employeePickerInfo.Secondary.EmployeeIdFieldId];
        } else {
            employeeId = model.loan[employeePickerInfo.LoanOfficer.EmployeeIdFieldId];
        }

        return employeeId === '00000000-0000-0000-0000-000000000000';
    }
    function setSessionStorage(key, value)
    {
        if (typeof(Storage) !== "undefined")
        {
            sessionStorage[key] = value;
        }
    }

    function triggerPricingDone() {
        $(window).trigger('loan/price_done');
    }

    /**
    * invalidateCachedField(fieldId)
    *
    * Deletes the jQuery object that was cached for the given field id.
    */
    function invalidateCachedField(fieldId) {
        fieldDOM[fieldId] = null;
    }
    /////////////////////
    // PUBLIC
    /////////////////////
    return {
        getLoanId: function() { return loanId; },
        getIsDirty: function() { return dirty; },
        setIsDirty: function(b) { dirty = b; },
        getIsValid: function () { return isValid; },
        setShouldCallUnload: function(value) { shouldCallUnload = value; },
        setIsValid: function(b) { isValid = b; },
        getIsRefreshing: function() { return isRefreshing; },
        getCurrentAppNum: function() { return currentAppNum; },
        getCitizenshipDropdownConfiguration: function(citizenship, residency) {
            return citizenshipDropdownConfigurations[citizenship][residency]; 
        },
        options: options,
        model: model,
        constants: constants,
        init: init,
        load: load,
        postinit: postinit,
        save: save,
        async_save: async_save,
        saveAgents: saveAgents,
        loadLoan: loadLoan,
        updateViewLoan: updateViewLoan,
        updateViewApp: updateViewApp,
        async_calc: async_calc,
        sync_calc: sync_calc,
        updateAll: function(args) { updateAll(args); },
        getAllModelValues: getAllModelValues,
        goBack: goBack,
        triggerPricingDone: triggerPricingDone,
        validate: validate,
        updatePinCount: updatePinCount,
        addPostRefreshCallback: addPostRefreshCallback,
        invalidateCachedField: invalidateCachedField,
        removeHiddenDDLOptions: removeHiddenDDLOptions,
        promptEmployeeAssignmentForPricing: promptEmployeeAssignmentForPricing,
        promptEmployeeAssignment: promptEmployeeAssignment,
        requiresEmployeeAssignmentForPricing: requiresEmployeeAssignmentForPricing,
        setEmployeePickerInfo: function (info) { employeePickerInfo = info; },
        updateFieldInModel: function (el) { updateFieldInModel(el); },
        setSessionStorage: setSessionStorage,
        refreshFieldData: refreshFieldData
    };
} (jQuery));
