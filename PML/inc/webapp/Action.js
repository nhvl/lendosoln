﻿if (!PML) PML = {
    save: function() { }
}

//From Integrations.js
if (!PML.Integrations) PML.Integrations = {};

jQuery(function($) {
    PML.ActionArea = {
        GetCompareButton: function() {
            return $CompareBtn;
        },
        GetConvertButton: function() {
            return $ConvertBtn;
        },

        EnableFHAButton: function() {
            $ToFHA.attr('disabled', false);
            updateActionButtons();
        },
        DisableFHAButton: function() {
            $ToFHA.attr('disabled', true);
            updateActionButtons();
        },

        EnableLPButton: function() {
            $ToLP.attr('disabled', false);
            updateActionButtons();
        },
        DisableLPButton: function() {
            $ToLP.attr('disabled', true);
            updateActionButtons();
        },

        SetLeadStatus: setLeadStatus,

        UpdateAvailableActions: updateAvailableActions,
        UpdateNumPinned: updateNumPinned
    };

    var $ActionArea = $('#AllActions');
    var $CompareArea = $('#ComparePinnedContainer');

    var $ConvertBtn = $('#ConvertLeadToLoan');
    var $CreateLeadBtn = $('#ConvertQP2FileToLead');
    var $CreateLoanBtn = $('#ConvertQP2FileToLoan');
    var $CompareBtn = $('#ComparePinned');

    $CreateLeadBtn.click(createLeadToLoanHandler({ isFromQP2File: true, isConvertToLead: true }));
    $CreateLoanBtn.click(createLeadToLoanHandler({ isFromQP2File: true, isConvertToLead: false }));

    if (ML.CanConvertLead) {
        $ConvertBtn
            .click(createLeadToLoanHandler({ isFromQP2File: false, isConvertToLead: false }))
            .find('.WarningIcon').hide();
    }
    else {
        $ConvertBtn
            .prop('disabled', true).prop('title', ML.CanConvertLeadDenialReason)
            .find('.WarningIcon').show();
    }

    function createLeadToLoanHandler(otherArgs, getFakePopupArgs) {
        return function (evt) {
            evt.preventDefault();
            if (otherArgs.isFromQP2File === true && (otherArgs.isConvertToLead == false || ML.PortalMode == "4" /* E_PortalMode.Retail */)) {
                // for QP => Loan conversion, the user may not have any options to select.  If not, skip the popup.
                if (false === pmlData.IsEmbeddedPML && true === pmlData.BrokerIsAllowExternalUserToCreateNewLoan) {
                    var popupArgs = {
                        TemplateOption: '0',
                        LoanNumbering: '1'
                    };
                    convertLeadToLoanHelper(popupArgs, otherArgs);
                    return;
                }
            }

            LQBPopup.Show(gVirtualRoot + '/webapp/ConvertLead.aspx?loanid=' + ML.sLId, {
                onReturn: function (popupArgs) { convertLeadToLoanHelper(popupArgs, otherArgs); },
                width: 500,
                height: 160,
                hideCloseButton: true
            }, otherArgs);
        };
    }

    $CompareBtn.click(function() {
        $(PML.ActionArea).trigger("ComparePinned");
    });

    var $ToDO = $('#DesktopOriginator');
    var $ToDU = $('#DesktopUnderwriter');
    var $ToDUSeamless = $('#DesktopUnderwriter_Seamless');
    var $ToLPASeamless = $('#LoanProductAdvisor_Seamless')
    var $ToLP = $('#LoanProspector');
    var $ToFHA = $('#FHATotal');
    var $ActionButtons = $('#DesktopOriginator, #DesktopUnderwriter, #DesktopUnderwriter_Seamless, #LoanProductAdvisor_Seamless, #LoanProspector, #FHATotal');
    $ActionButtons.disableSelection();
    updateNumPinned();

    $ToDO.on('click', function(e) {
        runIntegration(e, 'DO_Submitted', function(after) {
            PML.Integrations.DO.Submit(PML.getLoanId(), after);
        });
    });

    if (ML.CanRunDo) {
        $ToDO.find('.WarningIcon').hide();
    } else {
        $ToDO.prop('disabled', true);
        $ToDO.find('.WarningIcon').attr('title', ML.RunDoDenialReason);
    }

    $ToDU.on('click', function(e) {
        runIntegration(e, 'DU_Submitted', function(after) {
            PML.Integrations.DU.Submit(PML.getLoanId(), after);
        });
    });

    $ToDUSeamless.on('click', function (e) {
        runIntegration(e, 'DU_Submitted', function (after) {
            PML.Integrations.DU.SubmitSeamless(PML.getLoanId(), after);
        });
    });

    if (ML.CanRunDu) {
        $ToDU.find('.WarningIcon').hide();
        $ToDUSeamless.find('.WarningIcon').hide();
    } else {
        $ToDU.prop('disabled', true);
        $ToDU.find('.WarningIcon').prop('title', ML.RunDuDenialReason);
        $ToDUSeamless.prop('disabled', true);
        $ToDUSeamless.find('.WarningIcon').prop('title', ML.RunDuDenialReason);
    }

    //LP only works in MSIE, so disable it.
    if (!isInternetExplorer()) $ToLP.prop({
        'title': 'Send to LPA only available in Internet Explorer.',
        'disabled': true
    });
    $ToLP.on('click', function(e) {
        runIntegration(e, 'FHA_Submitted', function(after) {
            PML.Integrations.LP.Submit(PML.getLoanId(), after);
        });
    });

    $ToLPASeamless.on('click', function (e) {
        runIntegration(e, 'LPA_Submitted', function (after) {
            PML.Integrations.LP.SubmitSeamless(PML.getLoanId(), after);
        });
    });

    if (ML.CanRunLp) {
        $ToLP.find('.WarningIcon').hide();
        $ToLPASeamless.find('.WarningIcon').hide();
    } else {
        $ToLP.prop('disabled', true);
        $ToLP.find('.WarningIcon').prop('title', ML.RunLpDenialReason);
        $ToLPASeamless.prop('disabled', true);
        $ToLPASeamless.find('.WarningIcon').prop('title', ML.RunLpDenialReason);
    }

    $ToFHA.on('click', function(e) {
        runIntegration(e, 'FHA_Submitted', function(after) {
            PML.Integrations.FHA.Submit(PML.getLoanId(), after);
        });
    });

    var $waitMessage = $('#WaitMessage');
    var $waitDialog = $('#WaitDialog').dialog({
        autoOpen: false,
        title: "Please wait",
        modal: true,
        resizable: false,
        width: 'auto',
        height: 100
    });

    PML.Integrations.Init({
        SetMsg: function(msg) {
            $waitMessage.text(msg);
        },
        ShowMsg: function(msg, creditReferenceInfo) {
            if (msg) {
                if (creditReferenceInfo) {
                    $waitMessage.html(msg + creditReferenceInfo);
                    $waitDialog.dialog("option", "height", 300);
                }
                else {
                    $waitMessage.html(msg);
                    $waitDialog.dialog("option", "height", 100);
                }
            }
            $waitDialog.dialog('open');
        },
        HideMsg: function() {
            $waitDialog.dialog('close');
        }
    });

    function convertLeadToLoanHelper(popupArgs, otherArgs) {
        // OPM 231555 - Alert user if lead has archives.
        if (popupArgs.TemplateOption === '1' && ML.HasArchives) {
            var msg;
            if (ML.IsForceLeadToLoanTemplate) {
                msg = ML.LeadHasArchives_ForceTemplate;
            }
            else {
                msg = ML.LeadHasArchives;  
            }

            if(!confirm(msg)){
                return;
            }
        }

        if (otherArgs.isFromQP2File === true) {
            if (otherArgs.isConvertToLead === true) {
                $waitMessage.text("Please wait, creating lead...");
            }
            else {
                $waitMessage.text("Please wait, creating loan...");
            }
        }
        else {
            $waitMessage.text("Please wait, converting this lead to a loan...");
        }
        $waitDialog.dialog('open');
        window.setTimeout(function() {
            convertLeadToLoan(popupArgs, otherArgs);
        }, 100);
    }

    function convertLeadToLoan(popupArgs, otherArgs) {

        if (PML.getIsRefreshing()) {
            PML.addPostRefreshCallback(convertLeadToLoan, [popupArgs, otherArgs]);
            return;
        }

        if (PML.getIsDirty()) {
            var result = PML.save(); // if save failed, still convert.
        }

        var leadId = PML.getLoanId();
        var args = {
            LeadID: leadId,
            FileVersion: PML.model.loan.sFileVersion,
            ConvertWithTemplate: popupArgs.TemplateOption === '1',
            RemoveLeadPrefix: popupArgs.LoanNumbering === '0'
        };
        if (typeof (popupArgs.TemplateId) != 'undefined') {
            args.TemplateId = popupArgs.TemplateId;
        }
        var methodName = "ConvertLeadToLoan";
        if (otherArgs.isFromQP2File === true) {
            if (otherArgs.isConvertToLead === true) {
                methodName = "ConvertQP2FileToLead";
            }
            else {
                methodName = "ConvertQP2FileToLoan";
            }
        }

        var result = gService.PML.call(methodName, args).value;

        if (result.Message && result.Message.indexOf(ML.LoanInfo_DuplicateLoanNumber) == -1) {
            alert(result.Message);
        }

        if (!result.Error || result.Error == "False") {
            var LoanId = result.LoanId;
            if (PML.options.EnableNewTpoLoanNavigation) {
                if (window.parent) {
                    window.parent.location = ML.VirtualRoot + "/webapp/Loan1003.aspx?loanid=" + LoanId;
                }
                else {
                    window.location = ML.VirtualRoot + "/webapp/Loan1003.aspx?loanid=" + LoanId;
                }
            }
            else {
                window.location = window.location.toString().replace(leadId, LoanId);
            }
        } else {
            // OPM 243181 - If failed on CLoanNumberDuplicateException, offer user chance to convert with new loan number.
            if (ML.IsForceLeadToUseLoanCounter && result.Message && result.Message.indexOf(ML.LoanInfo_DuplicateLoanNumber) > -1) {
                $j('#dialog-convertNameError').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function() {
                            $j(this).dialog("close");
                            popupArgs.LoanNumbering = '1'
                            convertLeadToLoan(popupArgs, otherArgs)
                        },
                        "No": function() {
                            $j(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "300",
                    draggable: false,
                    dialogClass: "LQBDialogBox",
                    resizable: false
                });
            }
        }

        $waitDialog.dialog('close');

    }

    function runIntegration(e, eventName, toRun) {
        e.preventDefault();

        if (PML.getIsRefreshing()) {
            PML.addPostRefreshCallback(runIntegration, [e, eventName, toRun]);
            return;
        }

        PML.save();

        $(PML.ActionArea).trigger('BeforeIntegrationSubmission');
        toRun(function() {
            $(PML.ActionArea).trigger(eventName);
            $(PML.ActionArea).trigger('AfterIntegrationSubmission');
        });
    }

    function updateNumPinned(num) {
        var num = +num || +$('#NumPinnedResults').val() || 0;
        if (num) {
            $CompareBtn.val('Compare (' + num + ') Pinned Results');
            $CompareBtn.removeAttr('disabled');
        }
        else {
            $CompareBtn.val('No Pinned Results to Compare');
            $CompareBtn.attr('disabled', 'disabled');
        }
    }

    var enabledEngines = JSON.parse($('#AvailableEngines').val() || '[]');

    /**
    * updateAvailableActions(available)
    *
    * Given a list of available actions, show their corresponding buttons.
    */
    function updateAvailableActions(available) {
        available = available || enabledEngines;

        if (!$.isArray(available)) available = [];

        available = $.map(available, function(elem) {
            if (!/^#/.test(elem)) return '#' + elem;
            return elem;
        });

        var $buttons = $(available.join(',')).show();
        updateActionButtons($buttons);
    }

    /**
    * updateActionButtons($buttons)
    *
    * Update the style on the action buttons depending on how many there are.
    */
    function updateActionButtons($buttons) {
        $buttons = $buttons || $ActionButtons.filter(function() { return $(this).css("display") !== "none"; });
        // Might be better to have a field in the model that keeps track of which buttons should be visible

        $ActionArea.show();

        // When there are no visible buttons in the action div, hide it. In the
        // expanded lead editor, the convert button is in the compare div.
        //var bConvertBtnVisible = $ConvertBtn.is(function() { return $(this).css("display") !== "none"; });
        //var bConvertBtnInActionArea = $ConvertBtn.parent().attr("id") === "AllActions";
        //if ($buttons.length == 0 && (!bConvertBtnVisible || !bConvertBtnInActionArea)) {
        var skipButtonUpdate = false;
        if ($ActionArea.find('input[type="button"]:visible, button:visible').length == 0) {
            $ActionArea.hide();
            $CompareArea.addClass('FullHeight');
            skipButtonUpdate = true;
        }
        else {
            $CompareArea.removeClass('FullHeight');
            $ActionArea.addClass("FullHeight");
        }

        if ($CompareArea.find("input, button").length == 2) {
            $CompareArea.addClass("HalfWidth").removeClass("FullWidth");
        }
        else {
            $CompareArea.addClass("FullWidth").removeClass("HalfWidth");
        }

        if ($ActionArea.find("input, button").length > 2) {
            $ActionArea.removeClass("FullWidth").addClass("HalfWidth");
        }
        else if ($ActionArea.find("input, button").length == 2) {
            $ActionArea.removeClass("FullWidth").addClass("HalfWidth");
            $ActionArea.addClass("FullHeight");
        }
        else {
            $ActionArea.removeClass("HalfWidth").addClass("FullWidth");
        }

        if (skipButtonUpdate) {
            return;
        }
    }

    function setLeadStatus(isLead) {
        if (!isLead) {
            $ConvertBtn.hide();
            updateAvailableActions();
        }
        else {
            if (PML.options.IsLead && enabledEngines.length > 0) {
                $ConvertBtn.detach().addClass('Total_2').prependTo($CompareArea);
                $CompareBtn.addClass('Total_2');
                updateAvailableActions();
            }
            else {
                $ConvertBtn.show();
                updateAvailableActions([]);
            }
        }
    }

    function renderFHAButton() {
        var sLPurposeT = PML.model.loan['sLPurposeT'];
        var county = PML.model.loan['sSpCounty'];
        var bCountyValid = county != null && county != '';

        if (sLPurposeT != PML.constants.E_sLPurposeT.VaIrrrl && bCountyValid) {
            PML.ActionArea.EnableFHAButton();
        } else {
            PML.ActionArea.DisableFHAButton();
        }
    }

    function renderLPButton() {
        var county = PML.model.loan['sSpCounty'];
        var bCountyValid = county != null && county != '';

        if (bCountyValid && ML.CanRunLp) {
            PML.ActionArea.EnableLPButton();
        } else {
            PML.ActionArea.DisableLPButton();
        }
    }

    $(window).on('loan/view_update_done', function(e) {
        renderFHAButton();
        renderLPButton();
    });

    $(window).on('loan/changed/sLPurposeT', function(event, newVal) {
        renderFHAButton();
    });

    $(window).on('loan/changed/sSpCounty', function(event, newVal) {
        renderFHAButton();
        renderLPButton();
    });

    $(window).on('loan/changed/sSpZip_done', function(event, newVal) {
        renderFHAButton();
        renderLPButton();
    });

    $(window).bind('loan/changed/sStatusT', function(event, newVal) {
        setLeadStatus(newVal == "Lead New" || newVal == "Lead Canceled" ||
            newVal == "Lead Declined" || newVal == "Lead Other");
    });

    $(window).bind('loan/changed/sPinCount', function(event, newVal) {
        updateNumPinned(newVal);
    });
});
