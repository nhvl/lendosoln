﻿var DataInput = (function($) {

    var tabLinkTemplate = null;
    var tabContentTemplate = null;
    var $currentTab = null;

    function init() {
        tabLinkTemplate = $('#tabLinkTemplate').template();
        tabContentTemplate = $('#tabContentTemplate').template();
        $currentTab = $('#propertyAndLoanInfoTab');

        $(window).on("loan/changed/isValid", function(e, tabIsValid) {
            $('#propertyAndLoanInfoTabValidator').toggle(!tabIsValid);
        });
        $(window).on("apps/changed/isValid", function(e, appNum, tabIsValid) {
            $('#applicationLink' + appNum + 'TabValidator').toggle(!tabIsValid);
        });

        // Whenever the model creates an app, create a tab for it
        $(window).on('apps/create_done', function(e, appNum, doSwitchToTab) {
            createApplicantTab(appNum);
            f_validateApp(e, appNum);
            if (doSwitchToTab) {
                switchToApplicationTab(appNum);
            }
        });

        $(window).on('apps/delete_done', function(e, appNum) {
            removeApplicantTab(appNum);

            f_validateApp(e, 0);

            if (!PML.model.apps[0]["isValid"]) {
                switchToApplicationTab(0);
            }
            else {
                switchToPropertyAndLoanTab();
            }
        });

        $('.Tabs a').click(on_tab_clicked);
    }

    function switchToLead() {
        // NOT IMPLEMENTED
    }

    function switchToLoan() {
        // should these functions even be in here?
    }

    /**
    * on_tab_clicked(e)
    *
    * Determine which tab to switch to and switch to it.
    */
    function on_tab_clicked(e) {
        e.preventDefault();
        var $this = $(this); // the tab that was clicked

        // Only allow switching tabs if the current tab is valid
        var valid = false;
        var tabModel = $currentTab.data('model');
        if (typeof tabModel !== 'undefined') {
            if (tabModel === 'app') {
                valid = PML.model.apps[PML.getCurrentAppNum()]["isValid"];
            } else if (tabModel === 'loan') {
                valid = PML.model.loan["isValid"];
            }
        }

        if (!valid) {
            alert('Cannot switch as this tab is not valid.');
            return;
        }

        // Now determine if we are switching to property and loan info or the application tab.
        var tabNum = $this.attr('tab');

        // The Property & Loan tab does not have a 'tab' attribute
        if (typeof tabNum === 'undefined') {
            switchToPropertyAndLoanTab();
            return;
        }

        return switchToApplicationTab(tabNum);
    }

    function switchToApplicationTab(appNum) {
        var $applicationLink = $('.tabnav li a[tab=' + appNum + ']');
        var $applicationTab = $('#applicationTab');

        if (PML.getIsDirty()) { // Save, then run pricing if the tab is dirty
            PML.async_save(loadError); // 5/7/2013 PA - per David, don't run pricing
        }

        // If we're switching to the current app, do not refresh the view, as it should already be populated
        if (appNum !== PML.getCurrentAppNum()) {
            $applicationTab.find('input').val(''); // clear out all inputs, selections. do not trigger 'changed' event
            $(window).trigger('apps/switch_current_app', [appNum]); // this will trigger a view update
        }

        return switchToTab($applicationLink, $applicationTab);
    }

    function switchToPropertyAndLoanTab() {
        var $propertyAndLoanInfoLink = $('#propertyAndLoanInfoLink');
        var $propertyAndLoanInfoTab = $('#propertyAndLoanInfoTab');

        if (PML.getIsDirty()) { // Save, then run pricing if the tab is dirty
            // ejm opm 231433 - refresh the data fields when switching to property and loan tab.
            PML.async_save(PML.updateAll, null, true); // 5/7/2013 PA - per David, don't run pricing
        }

        return switchToTab($propertyAndLoanInfoLink, $propertyAndLoanInfoTab);
    }

    function switchToTab($link, $tab) {
        // Handle the tab link at the top
        $('.tabnav li').removeClass('selected');

        // Add the 'selected' class to the li that contains this link
        $link.parent().addClass('selected');

        // Hide any tabs currently showing
        $('div.Tab').hide();

        // Show the actual tab
        $tab.show();

        // Bookkeeping
        $currentTab = $tab;
        return;
    }

    /**
    * createApplicantTab(appNum)
    *
    * Create tab, add it to DOM
    */
    function createApplicantTab(appNum) {
        var tabContentId = 'applicationTab' + appNum;
        var appNum_rep = parseInt(appNum) + 1;

        // Create and append the link
        var tabLink = $.tmpl(tabLinkTemplate, {
            tabLinkId: 'applicationLink' + appNum,
            tabContentId: tabContentId,
            tabNum: appNum,
            tabLinkText: 'Application #' + appNum_rep
        });
        tabLink.insertBefore('#propertyAndLoanInfoLI');

        tabLink.find('a').on('click', on_tab_clicked);

        return tabLink;
    }

    /**
    * removeApplicantTab(appNum)
    *
    * Remove tab from DOM
    */
    function removeApplicantTab(appNum) {
        var tab = $('.tabnav li a[tab=' + appNum + ']').parent();
        tab.closest('ul').find('a[tab]').each(function() {
            var $this = $(this), tabNum = $this.attr('tab');
            if (tabNum > appNum) {
                $this.attr('tab', tabNum - 1).text('Application #' + (tabNum));
            }

        });
        tab.remove();
    }

    return {
        init: init,
        switchToApplicationTab: switchToApplicationTab,
        switchToPropertyAndLoanTab: switchToPropertyAndLoanTab
    };
} (jQuery));