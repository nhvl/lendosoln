﻿var m_Loan1003pg3_CFM = { "AddToContactClientId": "Loan1003pg3_CFM_m_linkAddToContact", "AgentRoleTDDLClientId": null, "ClientID": "Loan1003pg3_CFM", "DefaultAgentRoleT": "19", "FieldIdHolder": { "AgentAltPhone": null, "AgentCity": "Loan1003pg3_BrokerCity", "AgentCompanyName": "Loan1003pg3_BrokerName", "AgentDepartmentName": null, "AgentEmail": null, "AgentFax": null, "AgentLicenseNumber": "Loan1003pg3_LoanOfficerLicenseNumber", "AgentLicensesPanel": null, "AgentName": "Loan1003pg3_LoanOfficerName", "AgentNote": null, "AgentPager": null, "AgentPhone": "Loan1003pg3_LoanOfficerPhone", "AgentSourceT": null, "AgentState": "Loan1003pg3_BrokerState", "AgentStreetAddr": "Loan1003pg3_BrokerStreetAddr", "AgentTitle": null, "AgentZip": "Loan1003pg3_BrokerZip", "BranchName": null, "BrokerLevelAgentID": null, "CommissionMinBase": null, "CommissionPointOfGrossProfit": null, "CommissionPointOfLoanAmount": null, "CompanyCity": "Loan1003pg3_BrokerCity", "CompanyId": null, "CompanyLicenseNumber": "Loan1003pg3_BrokerLicenseNumber", "CompanyLicensesPanel": null, "CompanyLoanOriginatorIdentifier": "Loan1003pg3_LoanOfficerCompanyLoanOriginatorIdentifier", "CompanyState": "Loan1003pg3_BrokerState", "CompanyStreetAddr": "Loan1003pg3_BrokerStreetAddr", "CompanyZip": "Loan1003pg3_BrokerZip", "EmployeeIDInCompany": null, "EmployeeId": null, "FaxOfCompany": "Loan1003pg3_BrokerFax", "FurtherCreditToAccountName": null, "FurtherCreditToAccountNumber": null, "IsNotifyWhenLoanStatusChange": null, "LoanOriginatorIdentifier": "Loan1003pg3_LoanOfficerLoanOriginatorIdentifier", "PayToABANumber": null, "PayToAccountName": null, "PayToAccountNumber": null, "PayToBankCityState": null, "PayToBankName": null, "PhoneOfCompany": "Loan1003pg3_BrokerPhone", "ShouldMatchBrokerContact": null, "TaxID": null }, "LoanId": "e2ccfd7e-dcbe-4760-8bcb-b9fc90b9d570", "ManualOverrideClientId": "Loan1003pg3_CFM_m_rbManualOverride", "OfficialContactListClientId": "Loan1003pg3_CFM_m_officialContactList", "PickFromContactClientId": "Loan1003pg3_CFM_m_linkPickFromContact", "UseOfficialContactClientId": "Loan1003pg3_CFM_m_rbUseOfficialContact" };
var oRolodex = null;


function setForeignNational() {


	var aBDecForeignNational = document.getElementById('Loan1003pg3_aBDecForeignNational');
	var saBDecCitizen = document.getElementById('Loan1003pg3_aBDecCitizen').value;
	var saBDecResidency = document.getElementById('Loan1003pg3_aBDecResidency').value;

	if (saBDecCitizen == 'N' && saBDecResidency == 'N')
		aBDecForeignNational.readOnly = false;
	else {
		aBDecForeignNational.readOnly = true;
		aBDecForeignNational.value = 'N';
	}


}

function validateYesNo(control) {
	updateDirtyBit();

	if (event != null && !(31 < event.keyCode && event.keyCode < 127)) { // if it's not a letter, ignore it
		return;
	}

	var value = '';
	if (control.value.length > 0)
		value = control.value.charAt(0);
	if (value == 'y' || value == 'Y') {
		control.value = 'Y';
	} else if (value == 'n' || value == 'N') {
		control.value = 'N';
	} else {
		control.value = '';
	}

	if (event != null && (event.srcElement.id == 'Loan1003pg3_aBDecCitizen' || event.srcElement.id == 'Loan1003pg3_aBDecResidency'))
		setForeignNational();
}


function OfficialContactList_OnChange(oContactFieldMapperInfo) {
	if (isReadOnly()) return;

	var ddl = document.getElementById(oContactFieldMapperInfo.OfficialContactListClientId);

	if (null == ddl) return;
	_SetContactFieldMapperHiddenValue(oContactFieldMapperInfo);
	var agentRoleT = ddl.value;

//There is no service now
//	var oRolodex = new cRolodex();
//	var args = oRolodex.populateFromOfficialContactList(oContactFieldMapperInfo.LoanId, agentRoleT);
//	var oFieldIdHolder = oContactFieldMapperInfo.FieldIdHolder;
//	for (var p in oFieldIdHolder) {
//		_SafeSet(oFieldIdHolder[p], args[p]);
//	}

	return false;
}

function UseOfficialContact_OnClick(oContactFieldMapperInfo) {
	ContactFieldMapper_OnInit(oContactFieldMapperInfo);
	if (typeof(refreshCalculation) != 'undefined') {
		refreshCalculation();
	}

	return true;
}

function ManualOverride_OnClick(oContactFieldMapperInfo) {
	ContactFieldMapper_OnInit(oContactFieldMapperInfo);
	return true;

}

function _SetContactFieldMapperHiddenValue(oContactFieldMapperInfo) {
	var ddl = document.getElementById(oContactFieldMapperInfo.OfficialContactListClientId);
	var manualOverrideRadioButton = document.getElementById(oContactFieldMapperInfo.ManualOverrideClientId);
	var useOfficialContactRadioButton = document.getElementById(oContactFieldMapperInfo.UseOfficialContactClientId);

	if (null == manualOverrideRadioButton || null == useOfficialContactRadioButton) {
		return;
	}

	document.getElementById(oContactFieldMapperInfo.ClientID + "_AgentRoleT").value = ddl.value;
	document.getElementById(oContactFieldMapperInfo.ClientID + "_IsLocked").value = manualOverrideRadioButton.checked ? 'True' : 'False';
}

function _SafeSet(sFieldId, sValue) {
	if (null == sFieldId)
		return;

	if (sFieldId.indexOf("LicensesPanel") > 0) //Licenses Panel
	{
		//Get the control prefix
		var i = sFieldId.indexOf("LicensesPanel");
		var prefix = sFieldId.substring(0, i);
		LICENSES.fn.setLicenses(prefix, sValue);
		return;
	}

	var o = document.getElementById(sFieldId);
	if (null != o) {
		if (o.type == 'checkbox') {
			if (sValue == true || sValue == '1') {
				o.checked = true;
			} else {
				o.checked = false;
			}
		} else {
			if (null != sValue) {
				o.value = sValue;
			}
		}
	}
}