﻿var currentSec = "-1";
var tabChangeCheck = false;
var $ = jQuery;
var followLink = true;
var oldDirty;
var gfepObj = {}; // page object.
var SavedEscrow = false;
var shouldCalculateData = false;
var alreadyCalculatedData = false;
var shouldCalculateDate = false;
var alreadyCalculatedDate = false;
//flag the current tab. If not current tab, load the new tab

var keyChange = false;
function init() {
    reload();
    eventHandler();
    setComboBoxesReadOnly();
    updateGfeOriginatorComp();

    gfepObj.feeTypePropertySetter = new FeeTypePropertiesSetter(false);

    $j(".Combobox").each(function() {
        var $this = $(this);
        if ($this.attr("HasNoItems") == "true") {
            $this.parent().find(".combobox_img").css("display", "none");
        }
    });

    $j("[id*='PaidToDDL']").each(function() {
        checkPaidTo($j(this));
    });
    
    //the custom fee desc
    $j(".Combobox, .ComboBoxDescDDL").change(function() {
        gfepObj.feeTypePropertySetter.setFeeTypeProperties($j(this), $j(this).attr("Section"));
    });

    $j(".combobox_select, .combobox_unselect").click(function() {
        var id = $j(this).parents(".combobox").attr("id").replace("_cbl", "");
        var $elem = $j("#" + elem);
        gfepObj.feeTypePropertySetter.setFeeTypeProperties(elem, elem.attr("Section"));
    });

    if (sIsRequireFeesFromDropDown) {
        $j(".Combobox, .combobox_img").css("display", "none");
    }

    //Modify buttons
    $j("#sEstCloseDInitial").prop('disabled', !$j("#sEstCloseDLckdInitial").prop("checked"));

    $j("#sSchedDueD1Initial").prop('disabled', !$j("#sSchedDueD1LckdInitial").prop("checked"));
    
    $("#sAggregateAdjRsrv").prop('disabled', !$("#sAggregateAdjRsrvLckd").prop("checked"));

    $("#sEstCloseD").prop('disabled', !$("#sEstCloseDLckd").prop("checked"));

    $("#sSchedDueD1").prop('disabled', !$("#sSchedDueD1Lckd").prop("checked"));

    $("#sHazInsRsrvMon").prop('disabled', !$("#sHazInsRsrvMonLckd").prop("checked"));

    $("#sMInsRsrvMon").prop('disabled', !$("#sMInsRsrvMonLckd").prop("checked"));

    $("#sRealETxRsrvMon").prop('disabled', !$("#sRealETxRsrvMonLckd").prop("checked"));

    $("#sSchoolTxRsrvMon").prop('disabled', !$("#sSchoolTxRsrvMonLckd").prop("checked"));

    $("#sFloodInsRsrvMon").prop('disabled', !$("#sFloodInsRsrvMonLckd").prop("checked"));

    $("#s1006RsrvMon").prop('disabled', !$("#s1006RsrvMonLckd").prop("checked"));

    $("#s1007RsrvMon").prop('disabled', !$("#s1007RsrvMonLckd").prop("checked"));

    $("#sU3RsrvMon").prop('disabled', !$("#sU3RsrvMonLckd").prop("checked"));

    $("#sU4RsrvMon").prop('disabled', !$("#sU4RsrvMonLckd").prop("checked"));

    var pageSettings = {
        requiredFieldIndicator: '<span class="text-danger">*</span>'
    };
    WorkflowPageDisplay.SetDisplay(pageSettings);

    toggleOriginatorCompensation()
}

function runPricing() {
    Save(false);
    if (followLink) {
        if (IsNewPmlUIEnabled) {
            document.location = gVirtualRoot + '/webapp/pml.aspx?loanid=' + encodeURIComponent(loanId) + '&source=PML';
        }
        else {
            document.location = gVirtualRoot + "/main/agents.aspx?loanid=" + encodeURIComponent(loanId);
        }
    }
}


function setComboBoxesReadOnly() {
    var $feeTypeComboBoxes = $j(".Combobox");

    if (sIsRequireFeesFromDropDown) {
        // text box readonly
        $feeTypeComboBoxes.prop("readOnly", true); // set text portion of combobox read only
        $feeTypeComboBoxes.each(function() { this.NotEditable = true; }); // Needed so drop down will still open
        $feeTypeComboBoxes.css("background-color", ""); // removes grey (disabled) background
    } else {
        // text box not readonly
        $feeTypeComboBoxes.prop("readOnly", false);
        $feeTypeComboBoxes.each(function() { this.NotEditable = false; });
    }
}

function populateForm(obj, clientID) {

    for (property in obj) {
        var val = obj[property];
        $j("#" + property).each(function() {
            var $this = jQuery(this);
            var type = $this.prop('type');
            if (type == 'radio') {
                if ($this.val() == val) {
                    $this.prop('checked', true);
                }
            } else if (type == 'checkbox') {
                $this.prop('checked', val == 'True');
            } else {
                $this.val(val);
            }
            var tagName = $this.prop('tagName');
            if (tagName == "SPAN" || tagName == "LABEL") {
                $this.text(val);
            }
        });
    }
}

// Usage:
// getAllFormValues() will collect all input on current page and pack into an object.
// This could lead to performance problem if there are many input on the page. To skip
// unnessary input then add the attribute SkipMe to input tag (ie. <input type=text SkipMe>) and invoke
// getAllFormValues('', true); dd 7/25/2003
// // 1/30/2012 dd - Updated to cross browser compatible. Require JQuery.

function getAllFormValues(clientID, skipIgnore, selector) {
    var skipNameRegEx = /^(IsUpdate|__VIEWSTATE|__EVENTTARGET|__EVENTARGUMENT|__EVENTVALIDATION)$/;
    var skipInputTypeRegEx = /^(submit|button)$/;
    var obj = {};

    if (selector == null || selector == undefined) {
        selector = "form :input, #InitEscrow :input";
    }
    
    jQuery(selector).each(function() {
        var $el = jQuery(this);

        var name = $el.prop('name');
        var type = $el.prop('type');

        if (skipNameRegEx.test(name) || skipInputTypeRegEx.test(type)) {
            return true; // Continue
        }

        if (!!skipIgnore && $el.attr('SkipMe') != null) {
            return true;
        }

        if (clientID != null && clientID != '' && !name.match(clientID + '\\$') && !name.match(clientID + '_')) {
            return true; // Skip name if it does not belong to this control's clientID
        }
        if (clientID != null && clientID != '') {
            name = name.replace(clientID + '$', ''); // Remove clientID_ in front of input name.
        }
        if (type == 'radio') {
            if ($el.prop('checked')) {
                obj[name] = $el.val();
            }
        } else if (type == 'checkbox') {
            obj[name] = $el.prop('checked') ? 'True' : 'False';
        } else {
            obj[name] = $el.val();
        }
    }
	);

    return obj;
}

function LoadEscrow() {
    if (isDirty()) {
        var args = new Object();
        args["loanid"] = loanId;
        args["BrokerId"] = BrokerId;
        args["section"] = currentSec;
        var result = gService.main.call("LoadEscrowData", args);

        if (!result.error) {
            if (result.value["ErrorMessage"] != undefined) {
                showErrorPopup(result.value["ErrorMessage"], true);
                followLink = false;
            }
        }

        populateForm(result.value, "");
        clearDirty();
    }
}

function Save(isUnload) {
    if (isDirty()) {
        var args = getAllFormValues('');
        args["loanid"] = loanId;
        args["BrokerId"] = BrokerId;
        args["section"] = currentSec;
        var result = gService.main.call("SaveData", args);

        if (!result.error) {
            if (result.value["ErrorMessage"] != undefined) {
                if (isUnload) {
                    return result.value["ErrorMessage"];
                }

                if (currentSec !== "-1") {
                    ML.currentTab = currentSec;
                }
                showErrorPopup(result.value["ErrorMessage"], true);
                followLink = false;
            }
        }

        populateForm(result.value, "");
        clearDirty();
    }

    return null;
}

function LoadDates() {
    if (isDirty()) {
        var args = new Object();
        args["loanid"] = loanId;
        args["BrokerId"] = BrokerId;
        args["sEstCloseDInitial"] = $j("#sEstCloseDInitial").val();
        args["sEstCloseDLckdInitial"] = $j("#sEstCloseDLckdInitial")[0].checked;
        args["sSchedDueD1Initial"] = $j("#sSchedDueD1Initial").val();
        args["sSchedDueD1LckdInitial"] = $j("#sSchedDueD1LckdInitial")[0].checked;

        var result = gService.main.call("CalcDates", args);

        if (!result.error) {
            if (result.value["ErrorMessage"] != undefined) {
                showErrorPopup(result.value["ErrorMessage"], true);
                followLink = false;
            }
        }

        populateForm(result.value, "");
    }
}

function SaveDates()
{
    if (isDirty()) {
        var args = new Object();
        args["loanid"] = loanId;
        args["BrokerId"] = BrokerId;
        args["sEstCloseDInitial"] = $j("#sEstCloseDInitial").val();
        args["sEstCloseDLckdInitial"] = $j("#sEstCloseDLckdInitial")[0].checked;
        args["sSchedDueD1Initial"] = $j("#sSchedDueD1Initial").val();
        args["sSchedDueD1LckdInitial"] = $j("#sSchedDueD1LckdInitial")[0].checked;
        
        var result = gService.main.call("SaveDates", args);

        if (!result.error) {
            if (result.value["ErrorMessage"] != undefined) {
                showErrorPopup(result.value["ErrorMessage"], true);
                followLink = false;
            }
        }

        populateForm(result.value, "");
        clearDirty();

        if (result.value["sSchedDueD1"]) {
            HasSchedDueD1 = true;
        }
        
        calculateData();
    }
}

function showErrorPopup(message, reloadPage) {
    simpleDialog.alert(message).then(function () {
        if (reloadPage) {
            if (ML.currentTab !== "-1") {
                var index = window.location.href.indexOf("&currentTab=");
                if (index <= -1) {
                    window.location.href += "&currentTab=" + ML.currentTab;
                }
                else {
                    var url = window.location.href;
                    window.location.href = url.substring(0, index) + "&currentTab=" + ML.currentTab;
                }
            }
            else {
                window.location.reload();
            }
        }
    });
}

function updateGfeOriginatorComp() {
    $j("#sGfeOriginatorCompFBaseT").removeAttr("style");
    $j("#sGfeOriginatorCompFPc").prop("disabled", $j("#sGfeOriginatorCompBy").val() == "3");
    $j("#sGfeOriginatorCompFBaseT").prop("disabled", $j("#sGfeOriginatorCompBy").val() == "3");
    $j("#sGfeOriginatorCompFMb").prop("disabled", $j("#sGfeOriginatorCompBy").val() == "3");

}

function checkPaidTo(ddl)
{
    //ddl is a jquery object
    var id = ddl.attr("id");
    var paidToID = id.replace("DDL", "");

    var $paidTo = $j("#" + paidToID);
    $paidTo.prop("disabled", !(ddl.val() == "2"));

    if ((ddl.val() == "2")) {
        $paidTo.val("");
    }

}

function calculateData() {
    var args = getAllFormValues('');
    args["loanid"] = loanId;
    args["BrokerId"] = BrokerId;
    args["section"] = currentSec;
    var result = gService.main.call("CalculateData", args);

    if (!result.error) {
        if (result.value["ErrorMessage"] != undefined) {
            showErrorPopup(result.value["ErrorMessage"], true);
            followLink = false;
        }
    }

    populateForm(result.value, "");
}

function saveEscrow() {
    var args = getAllFormValues('', true, '#InitEscrow :input');
    args["loanid"] = loanId;
    args["BrokerId"] = BrokerId;
    args["section"] = currentSec;
    var result = gService.main.call("SaveEscrowData", args);

    if (!result.error) {
        if (result.value["ErrorMessage"] != undefined) {
            onCancel("InitEscrow");
            showErrorPopup(result.value["ErrorMessage"], true);
            followLink = false;
        }
        SavedEscrow = true;
    }


    clearDirty();
    if (oldDirty) {
        updateDirtyBit();
    }

    populateForm(result.value, "");
}

function eventHandler() {

    $j("[id*='PaidToDDL']").change(function() {
        checkPaidTo($j(this));
    });

    $j("#sGfeOriginatorCompBy").change(function() {
        updateGfeOriginatorComp();
    });

    $j("#tab-menu a").click(function(e) {

        Save(false);
        tabsHandler($j(this).attr("href").substring(5));
        e.preventDefault();

    });

    if (typeof ML.EnableNewTpoLoanNavigation !== "undefined" && !ML.EnableNewTpoLoanNavigation) {
        $(window).bind('beforeunload', function () { Save(false); });
    }

    $j("#runPricingBtn").click(function () { runPricing(); });


    $j("input").change(function(e) {
        shouldCalculateData = true && !alreadyCalculatedData;
        alreadyCalculatedData = false;
    });

    $j("input").keydown(function(e) {
        if (e.key != "Tab" && e.keyCode != 9) {
            keyChange = true;
        }
        if (e.key == "Tab" || e.keyCode == 9) {
            $j(this).blur();
            if (keyChange) {
                calculateData();
                alreadyCalculatedData = true;
            }
            keyChange = false;
        }
    });

    $j("input[type='checkbox']").off();

    $j("select, input[type='checkbox']").change(function(e) {
        calculateData();
    });

    $j("body").mouseup(function() {
        if (shouldCalculateData) {
            shouldCalculateData = false;
            calculateData();
        }
        if(shouldCalculateDate)
        {
            shouldCalculateDate = false;
            LoadDates();
        }
    });

    $j("#btn_save").click(function(e) {
        saveEscrow();
    });

    $j("#InitialDatesDiv input").off();

    $j("#InitialDatesDiv input").change(function() {
        shouldCalculateDate = true && !alreadyCalculatedDate;
        alreadyCalculatedDate = false;
    });

    $j("#InitialDatesDiv input").keydown(function(e) {
        if (e.key == "Tab" || e.keyCode == 9) {
            $j(this).blur();
            LoadDates();
            alreadyCalculatedDate = true;
        }
    });

    // On the click event of a tab.
    /*	$("#tabs>li a").click(function(e) {
    e.preventDefault();
    pushState("sec=" + $(this).attr("href").substring(5).toString());
    }); */

    $j("[onkeydown*='onComboboxKeypress']").attr("class", "Combobox");


    $("input[type='text']").click(function() {
        $(this).select();
    });
    $("#btnInitEscrow").click(function() {
        oldDirty = isDirty();
        calculateData();
        showDialog("InitEscrow");

    });
    $("#btn_prev").click(function() {
        var p = parseInt(currentSec) - 100;
        Save(false);
        tabsHandler("" + p);
    });
    $("#btn_next").click(function() {
        var p = parseInt(currentSec) + 100;
        Save(false);
        tabsHandler("" + p);
    });
   
    $("#btn_save").click(function() {
        onSave("InitEscrow");

    });

    $j("#btnSaveInitial").click(function() {
        SaveDates();
    });

    
    //Modify buttons
    $j("#sEstCloseDLckdInitial").change(function() {
        $j("#sEstCloseDInitial").prop('disabled', !this.checked);
    });

    $j("#sSchedDueD1LckdInitial").change(function() {
        $j("#sSchedDueD1Initial").prop('disabled', !this.checked);
    });
    
    $("#sAggregateAdjRsrvLckd").change(function() {
        $("#sAggregateAdjRsrv").prop('disabled', !this.checked);
    });
    $("#FirstPayMod").change(function() {
        $("#FirstPayD").prop('disabled', !this.checked);
    });

    $("#sHazInsRsrvMonLckd").change(function() {
        $("#sHazInsRsrvMon").prop('disabled', !this.checked);
    });

    $("#sMInsRsrvMonLckd").change(function() {
        $("#sMInsRsrvMon").prop('disabled', !this.checked);
    });

    $("#sRealETxRsrvMonLckd").change(function() {
        $("#sRealETxRsrvMon").prop('disabled', !this.checked);
    });


    $("#sSchoolTxRsrvMonLckd").change(function() {
        $("#sSchoolTxRsrvMon").prop('disabled', !this.checked);
    });

    $("#sFloodInsRsrvMonLckd").change(function() {
        $("#sFloodInsRsrvMon").prop('disabled', !this.checked);
    });

    $("#s1006RsrvMonLckd").change(function() {
        $("#s1006RsrvMon").prop('disabled', !this.checked);
    });
    $("#s1007RsrvMonLckd").change(function() {
        $("#s1007RsrvMon").prop('disabled', !this.checked);
    });

    $("#sU3RsrvMonLckd").change(function() {
        $("#sU3RsrvMon").prop('disabled', !this.checked);
    });

    $("#sU4RsrvMonLckd").change(function() {
        $("#sU4RsrvMon").prop('disabled', !this.checked);
    });

    $("#sEstCloseDLckd").change(function() {
        $("#sEstCloseD").prop('disabled', !this.checked);
    });

    $("#sSchedDueD1Lckd").change(function() {
        $("#sSchedDueD1").prop('disabled', !this.checked);
    });

    $j("#sGfeIsTPOTransaction").change(function() {
        toggleOriginatorCompensation();
    });
    
}

//Back button (only in HTML5)
window.onpopstate = function(event) {
    reload();
};

function toggleOriginatorCompensation() {
    if ($j("#IsReadOnly").val() == "false") {
        var BrokerOrCorrespondentChannel = $j("#BrokerOrCorrespondentChannel").val();
        var isTPOTransaction = $j("#sGfeIsTPOTransaction").prop("checked");

        if (BrokerOrCorrespondentChannel == "True" && isTPOTransaction) {
            $j("#sGfeOriginatorCompFPc").removeAttr("disabled");
            $j("#sGfeOriginatorCompFBaseT").removeAttr("disabled");
            $j("#sGfeOriginatorCompFMb").removeAttr("disabled");

            $j("#sGfeOriginatorCompFBaseT").css("background-color", "");
        }
        else {
            $j("#sGfeOriginatorCompFPc").attr("disabled", "true");
            $j("#sGfeOriginatorCompFBaseT").attr("disabled", "true");
            $j("#sGfeOriginatorCompFMb").attr("disabled", "true");

            $j("#sGfeOriginatorCompFBaseT").css("background-color", gReadonlyBackgroundColor);
        }
    }
}

//call this function to reload page when tab changes or show dilalog. (QeuryString changes)
function reload() {
    //tabs in main site
    var p = getParameterByName("sec");

    //tabs inside dialogs
    var d = getParameterByName("d");
    //Validate Query String
    if (p !== "800" && p !== "900" && p !== "1000" && p !== "1100" && p !== "1200" && p !== "1300")
        p = "800";
    if (p === "1000") {
        if (d !== "a0") {
            d = "";
        }
    }
    else {
        d = "";
    }
    if (currentSec != p) {
        currentSec = p;
        tabsHandler(p);
    }
    DialogHandler(p, d);
    //update reformat fields
    //this calls the method from autonumeric.js, use the method from mask.js instead.
    //mask();

    setRowColor();
}
function setRowColor() {
    //http://stackoverflow.com/questions/8189182/ie-nth-child-using-odd-even-isnt-working
    $(".tb-tabs tbody tr:nth-child(even)").addClass("even");
    $(".tb-tabs tbody tr:nth-child(odd)").addClass("odd");

    //table inside does not need row color
    $(".table-style tr").removeClass("odd");

    //$(".tb-tabs tbody tr td:nth-child(8)").addClass("table-inside-description");
}
function getParameterByName(name) {
    //http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//update the url without reload whole page. Only in HTML5

function pushState(queryString) {
    if (history && history.pushState) {
        window.history.pushState(null, null, window.location.pathname + "?" + queryString + "&loanid=" + encodeURIComponent(loanId));
        reload();
    } else {
        //reload the page when not in HTML5
        window.location.href = "?" + queryString + "&loanid=" + encodeURIComponent(loanId);
    }
}

//Some components of MeridianLinkCommonControls.dll run incorrect in popup dialog. Create other mask.
function mask() {
    var selector = $('input[decimalplace=4]');
    selector.autoNumeric('init', { aSign: '$ ', vMax: '999999.9999' });

    selector = $('input[preset=money]');
    selector.autoNumeric('init', { aSign: '$ ', vMax: '999999.99' });

    selector = $('input[preset=percent]');
    selector.autoNumeric('init', { vMax: '100.000', aSign: ' %', pSign: 's' });

    selector = $('input[preset=numeric]');
    selector.autoNumeric('init', { vMax: '100', vMin: '0' });

    //https://forum.jquery.com/topic/datepicker-input-mask
    selector = $('input[preset=date]');
    selector.removeClass('hasDatepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        /* fix buggy IE focus functionality */
        fixFocusIE: false,
        /* blur needed to correctly handle placeholder text */
        onSelect: function(dateText, inst) {
            this.fixFocusIE = true;
            $(this).blur().change().focus();
        },
        onClose: function(dateText, inst) {
            this.fixFocusIE = true;
            this.focus();
        },

        beforeShow: function(i) {
            if ($(i).attr('readonly')) {
                return false;
            }
            var result = navigator.appVersion.indexOf("MSIE") ? !this.fixFocusIE : true;
            this.fixFocusIE = false;
            return result;
        }
    }).mask('99/99/9999');
}

/*********************************************************TABS MANAGEMENT*********************************************************/

function tabsHandler(p) {
    // Hide all content.
    $("tbody[id^='sec-']").hide();
    // error occured during tab switches
    if (ML.currentTab !== "-1") {
        // tabsHandler is called twice, once to switch tabs and another when reloading the page
        // keep count of this and only reset ML.currentTab after the page reloads
        if (!tabChangeCheck) {
            tabChangeCheck = true;
            p = ML.currentTab;
        }
        else {
            ML.currentTab = "-1";
            tabChangeCheck = false;
        }
    }
    currentSec = p;
    var tabName;
    if (p !== "") {
        switch (p) {
            case "800":
                tabName = "#sec-800";
                break;
            case "900":
                tabName = "#sec-900";
                break;
            case "1000":
                tabName = "#sec-1000";
                if (!HasSchedDueD1) {
                    $j("#sEstCloseDInitial").val($j("#sEstCloseD").val());
                    $j("#sEstCloseDLckdInitial").val($j("#sEstCloseDLckd").val());
                    $j("#sSchedDueD1Initial").val($j("#sSchedDueD1").val());
                    $j("#sSchedDueD1LckdInitial")[0].checked = $j("#sSchedDueD1Lckd")[0].checked;
                    showDialog("InitialDatesDiv");
                }
                break;
            case "1100":
                tabName = "#sec-1100";
                break;
            case "1200":
                tabName = "#sec-1200";
                break;
            case "1300":
                tabName = "#sec-1300";
                break;
            default:
                tabName = "#sec-800";
                currentSec = "800";
                break;
        }
        showTab($($("#tabs>li a").filter('[href="' + tabName + '"]')[0]));
    } else {
        showFirstTab();
    }
    
}

function showTab(tab) {
    //textContent not working in IE8
    var text = tab[0].textContent || tab[0].innerText;
    (text === "Section 1000") ? $("#header_paidTo").hide() : $("#header_paidTo").show();
    (text === "Section 1000") ? $("#header_escrowed").show() : $("#header_escrowed").hide();
    (text === "Section 1000") ? $("#lockSection1000").show() : $("#lockSection1000").hide();
    if ((text === "Section 1100") || (text === "Section 1300")) {
        $("[loan-static-tab]").toggleClass("legacy-closing-costs",false);
        $("[loan-static-tab]").toggleClass("legacy-closing-costs-min",true);
        $("#header_bsp").show();
        $("#bsp_notif").show();
    } else {
        $("[loan-static-tab]").toggleClass("legacy-closing-costs",true);
        $("[loan-static-tab]").toggleClass("legacy-closing-costs-min",false);
        $("#header_bsp").hide();
        $("#bsp_notif").hide();
    }
    //show/hide button
    if (text === "Section 800") {
        $("#btn_prev").hide();
        $("#btn_next").show();
    }
    else {
        if (text === "Section 1300") {
            $("#btn_prev").show();
            $("#btn_next").hide();
        } else {
            $("#btn_prev").show();
            $("#btn_next").show();
        }
    }

    // Remove any "active" class.
    $("#tabs>li ").removeClass("active");

    // Add "active" class to selected tab.
    tab.parent().addClass("active");
    // Hide all tab content
    $("tbody[id^='sec-']").hide();
    // Find the href attribute value to identify the active tab + content
    var activeTab = tab.attr("href");

    if (navigator.appVersion.indexOf("MSIE 9.") != -1) {
        //IE 9 loses border cell when using fadeIn
        $(activeTab).show();
    } else {
        // Fade in the active ID content.
        $(activeTab).show();
    }


}

function showFirstTab() {
    // Activate first tab.
    $("#tabs>li:first").addClass("active").show();

    // Show first tab content.
    $("tbody[id^='sec-']:first").show();
}

/*********************************************************DIV POPUPS MANAGEMENT*********************************************************/
function DialogHandler(p, d) {
    if (d === "") return;
    // Hide all content.
    $("#InitEscrow").hide();
    switch (d) {
        case "a0":
            //show dialog
            showDialog("InitEscrow");
            break;
        default:
            break;
    }

}

function showDialog(name) {
    var width = 920;

    if (name=="InitEscrow") {
        LQBPopup.ShowElementWithWrapper($("#InitEscrow"),{
                 headerText: "Initial Escrow Account Setup " ,
                 width:  1000,
                 closeText:  'Cancel',
                 onClose:  function(){
                                if (SavedEscrow) {
                                    LoadEscrow();
                                    calculateData();
                                }
                                SavedEscrow = false;
                            },
                 onReturn:  null,
                 popupClasses: 'Init-Escrow-Model',
                 buttons:[{
                            'Save' : function(){
                                                    saveEscrow();
                                               }
                            }],
             });
    } else if (name == "InitialDatesDiv"){
        LQBPopup.ShowElementWithWrapper($("#InitialDatesDiv"),{
            headerText: "Set Estimated Closing and First Payment Dates" ,
            closeText:  'Cancel',
            buttons:[{
                            'Save' : function(){
                                                    SaveDates();
                                               }
                            }],
        });
    } else {
        $("#" + name).dialog({
            resizable: true,
            modal: true,
            width: width,
            height: 'auto',
            position: 'center',
            close: function() {
                if (name=="InitEscrow" && !SavedEscrow) {
                    LoadEscrow();
                    calculateData();
                }
                SavedEscrow = false;
            }
        }).css({
            maxHeight: $(window).height() - 50
        });
    }
    //$(".ui-dialog-titlebar").removeClass('ui-widget-header');
}

function onCancel(name) {
    //cancel dialog click event
    $("#" + name).dialog('close');
}

function onSave(name) {
    //save dialog click event
    $("#" + name).dialog('close');
}


///code copied and mofied from FeeTypePropertiesSetter.js
// Note the arrangement GFE2010RC must be in same row as the combobox, jquery must be enabled.
// it relies on the utils service.
function FeeTypePropertiesSetter(isArchivePage) {
    this._isArchivePage = isArchivePage;
}
FeeTypePropertiesSetter.prototype.setFeeTypeProperties = function(combobox, lineNumber) {
    var args = {
        'hfIsArchivePage': false,
        'Description': combobox.val() ? combobox.val() : "",
        'LineNumber': lineNumber,
        'BrokerId' : BrokerId
    };

    var result = gService.main.callAsyncSimple("GetFeeTypeProperties", args, function (result) {
        if (!result.error) {
            // if fee type not found (user entered description), just return
            if (result.value.notFound) {
                return;
            }

            var $row = $j(combobox).parents("tr.odd, tr.even").first();
            var $Apr = $row.find("input[id*='APR']");
            var $Fha = $row.find("input[id*='FHA']");
            var $Page2 = $row.find("select[id*='Page2']");

            // If blank option selected, uncheck boxes, reset radio, and blank textbox
            if (result.value.blank) {
                combobox.value = ""; // Blank textbox so special characters (&zwnj;) aren't saved to DB
                $Apr.prop("checked", false);
                $Fha.prop("checked", false);
                $Page2.prop("selectedIndex", 0);

                return;
            }

            $Apr.prop("checked", result.value.Apr == "True");
            $Fha.prop("checked", result.value.Fha == "True");
            $Page2.val(result.value.Page2);
        } else {
            var errMsg = 'Unexpected error. Could not auto-populate fee type properties.';

            if (null != result.UserMessage)
                errMsg += " error msg: " + result.UserMessage;

            if (console && console.log) {
                console.log(errMsg);
            }

            return false;
        }
    });    
}
