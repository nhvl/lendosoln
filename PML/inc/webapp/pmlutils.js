﻿function f_validateDollarAmount(sVal, bZeroAllowed, negativeAllowed) {
    var bValidDollarAmt = false;
    try {
        var moneyVal = f_getMoneyValue(sVal);

        var isNegativeAllowed = false;
        if (negativeAllowed != null)
        {
            isNegativeAllowed = negativeAllowed;
        }

        if (!bZeroAllowed) {
            if (isNegativeAllowed)
            {
                bValidDollarAmt = (moneyVal != null && moneyVal !== 0 );
            }
            else
            {
                bValidDollarAmt = (moneyVal != null && moneyVal > 0);
            }
        } else {
            if (isNegativeAllowed)
            {
                bValidDollarAmt = moneyVal != null;
            }
            else
            {
                bValidDollarAmt = (moneyVal != null && moneyVal >= 0);
            }
        }
    } catch (e) { } // Don't need to do anything, it's already false

    return bValidDollarAmt;
}

function f_getMoneyValue(str) {
    var val = null;
    try {
        if (!str) return null;
        if (str.length == 0) return null;
        var _v = str.replace(/\$/, '').replace(/,/g, '');

        if (_v.match(/^\((.+\))$/)) {
            _v = _v.substring(1, _v.length - 2);
            _v = "-" + _v;
        }
        if (!isNaN(_v)) {
            val = parseFloat(_v);
        }
    } catch (e) { }
    return val;
}

function f_openHelp(page, w, h) {
    var url = gVirtualRoot + "/help/" + page;
    LQBPopup.Show(url, {
        width: w,
        height: h
    });
    return false;
}

// Expects jQuery objects as parameters
function f_swapElements($el1, $el2) {
    var $el1Clone = $el1.clone(true);
    var $el2Clone = $el2.clone(true);
    $el1.replaceWith($el2Clone);
    $el2.replaceWith($el1Clone);

    // Need to invalidate the cached jQuery objects in PML
    // because the cached objects are no longer in the DOM.
    $el1.add($el2).find('[data-calc]').each(function() {
        PML.invalidateCachedField(this.id);
    });
}

// Custom PML Field Utilities - functionality is common to both lead/loan file pricers.
function f_insertCustomFields(idToAppendTo, customFieldsList) {

    for (var i in customFieldsList) {
        var field = customFieldsList[i];
        if (!field.IsValid) continue;

        var fieldId = 'sCustomPMLField' + field.KeywordNum;

        var $newDiv = $j('<div id="' + fieldId + 'Container" class="customfieldcontainer" />');
        $newDiv.append('<label>' + field.Description + '</label>');

        var $input;
        switch (field.Type) {
            case pmlData.Constants.E_CustomPmlFieldType.Blank:
                break;
            case pmlData.Constants.E_CustomPmlFieldType.Dropdown:
                $input = $j('<select id="' + fieldId + '" class="customfield" data-calc />');

                // Always add a blank option to custom field DDLs
                var $option = $j('<option>').val('').text('');
                $option.appendTo($input);

                for (var i in field.EnumMapKeyValuePairs) {
                    var keyValuePair = field.EnumMapKeyValuePairs[i];
                    var $option = $j('<option>').val(keyValuePair.Key).text(keyValuePair.Value);
                    $option.appendTo($input);
                }
                break;
            case pmlData.Constants.E_CustomPmlFieldType.NumericGeneric:
                $input = $j('<input type="text" id="' + fieldId + '" class="customfield numeric" preset="numeric" data-calc />');
                break;
            case pmlData.Constants.E_CustomPmlFieldType.NumericMoney:
                $input = $j('<input type="text" id="' + fieldId + '" class="customfield money" preset="money-allowblank" data-calc />');
                break;
            case pmlData.Constants.E_CustomPmlFieldType.NumericPercent:
                $input = $j('<input type="text" id="' + fieldId + '" class="customfield percent" preset="percent-allowblank" data-calc />');
                break;
        }

        $input.attr('visibility-type', field.VisibilityType);
        $newDiv.append($input);
        $newDiv.append('<img id="' + fieldId + 'Validator" ' + ' src="../images/error_pointer.gif" alt="This is a required field"/>');

        $j('#' + idToAppendTo).append($newDiv);

        // events are attached in f_uc_init
    }

    $j('input.customfield').css('text-align', 'right');
}

function f_validate_sCustomPMLField(fieldId) {
    var $field = $j('#' + fieldId);
    if ($field.length == 0) return; // It is possible that this field is not in the DOM.

    var bValid = PML.model.loan[fieldId] != '';
    if ($field.prop('tagName').toLowerCase() == 'select') {
        var selectedText = $field.children('option:selected').text();
        bValid = bValid && $j.trim(selectedText) != '';
    }

    // 10/10/2013 gf - custom pml fields now only visible for certain loan types.
    var visibilityType = parseInt($field.attr('visibility-type'), 10);
    var sLPurposeTPe = parseInt(PML.model.loan['sLPurposeTPe'], 10);
    var bIsFieldVisibleForLoanType = true;
    switch (visibilityType) {
        case PML.constants.E_CustomPmlFieldVisibilityT.AllLoans:
            bIsFieldVisibleForLoanType = true;
            break;
        case PML.constants.E_CustomPmlFieldVisibilityT.PurchaseLoans:
            bIsFieldVisibleForLoanType = sLPurposeTPe == PML.constants.E_sLPurposeT.Purchase;
            break;
        case PML.constants.E_CustomPmlFieldVisibilityT.RefinanceLoans:
            bIsFieldVisibleForLoanType = sLPurposeTPe == PML.constants.E_sLPurposeT.Refin
                || sLPurposeTPe == PML.constants.E_sLPurposeT.RefinCashout
                || sLPurposeTPe == PML.constants.E_sLPurposeT.FhaStreamlinedRefinance
                || sLPurposeTPe == PML.constants.E_sLPurposeT.VaIrrrl;
            break;
        case PML.constants.E_CustomPmlFieldVisibilityT.HomeEquityLoans:
            bIsFieldVisibleForLoanType = sLPurposeTPe == PML.constants.E_sLPurposeT.HomeEquity;
            break;
    }

    validator[fieldId] = bValid || !bIsFieldVisibleForLoanType;
    $j('#' + fieldId + 'Validator').toggle(bValid == false);
}

function f_toggleCustomPmlFields() {
    // 10/10/2013 gf - custom pml fields now only visible for certain loan types.
    var $conditionallyVisibleFields = $j('.customfield[visibility-type!="' + PML.constants.E_CustomPmlFieldVisibilityT.AllLoans + '"]');
    var typeToShow = '';

    var sLPurposeTPe = parseInt(PML.model.loan['sLPurposeTPe'], 10);
    switch (sLPurposeTPe) {
        case PML.constants.E_sLPurposeT.Purchase:
            typeToShow = PML.constants.E_CustomPmlFieldVisibilityT.PurchaseLoans;
            break;
        case PML.constants.E_sLPurposeT.Refin:
        case PML.constants.E_sLPurposeT.RefinCashout:
        case PML.constants.E_sLPurposeT.FhaStreamlinedRefinance:
        case PML.constants.E_sLPurposeT.VaIrrrl:
            typeToShow = PML.constants.E_CustomPmlFieldVisibilityT.RefinanceLoans;
            break;
        case PML.constants.E_sLPurposeT.HomeEquity:
            typeToShow = PML.constants.E_CustomPmlFieldVisibilityT.HomeEquityLoans;
            break;
    }

    var $fieldsToShow = $conditionallyVisibleFields.filter('[visibility-type="' + typeToShow + '"]');
    var $fieldsToHide = $conditionallyVisibleFields.filter('[visibility-type!="' + typeToShow + '"]');

    $fieldsToShow.closest('.customfieldcontainer').show();
    $fieldsToHide.closest('.customfieldcontainer').hide();
}