﻿if (!PML) PML = {
    save: function() { }
}

PML.Integrations = (function($) {
    var SetWaitMessageText = function(msg) { };
    var ShowWaitMessage = function() { };
    var HideWaitMessage = function() { };
    var pollingInterval = 1000;

    //----- DU/DO
    function submitToDU(loanId, callback) {
        var isDO = false;
        submitToDODU(loanId, callback, isDO);
    }

    function submitToDUSeamless(loanId, callback) {
        var args = {};
        LQBPopup.Show(gVirtualRoot + '/main/SeamlessDU/SeamlessDU.aspx#/audit?loanid=' + loanId,
            {
                onReturn: callback,
                width: '80%',
                height: 800,
                hideCloseButton: true
            }, args);
    }

    function submitToLPASeamless(loanId, callback) {
        var args = {};
        LQBPopup.Show(gVirtualRoot + '/main/SeamlessLPA/SeamlessLpa.aspx#/audit?loanid=' + loanId,
            {
                onReturn: callback,
                width: '80%',
                height: 800,
                hideCloseButton: true
            }, args);
    }

    function submitToDO(loanId, callback) {
        var isDO = true;
        submitToDODU(loanId, callback, isDO);
    }

    function submitToDODU(loanId, callback, isDO) {
        loginDODU(loanId, isDO, function(args) {
            if (!args.OK) return;

            //DownloadDODUData expects the object to be structured this way
            var loginResults = {};
            loginResults.bIsDo = args.IsDo;
            loginResults.sUserName = args.sUserName;
            loginResults.sPassword = args.sPassword;
            loginResults.sDuCaseId = args.sDuCaseId;
            loginResults.bHasAutoLogin = args.bHasAutoLogin

            var duWindow = launchDODU(args.CacheId);

            if (!duWindow) {
                alert('Unable to open new window. Check your popup-blocker.');
                return;
            }

            var args = { loanId: PML.getLoanId() };
            var creditReferenceData = "";
            var result = gService.action.call('RetrieveCreditReferenceData', args);
            if (!result.error) {
                if (result.value["CreditReferenceString"] != null) {
                    creditReferenceData = result.value["CreditReferenceString"];
                }
            }

            var waitMessage = "Please wait, retrieving results from " + (isDO ? "Desktop Originator" : "Desktop Underwriter");
            ShowWaitMessage(waitMessage, creditReferenceData);

            pollDODUWindow(duWindow, loginResults, function() {
                HideWaitMessage();
            });
        });
    }

    function loginDODU(loanId, isDO, afterLogin) {
        var args = { test: true };
        LQBPopup.Show(gVirtualRoot + '/main/FannieMaeExportLogin.aspx?loanid=' + loanId + '&isdo=' + (isDO ? 't' : 'f'),
        {
            onReturn: afterLogin,
            width: 580,
            height: 280,
            hideCloseButton: true
        }, args);
    }

    function launchDODU(sCacheId) {
        var bMaximize = true;
        var bCenter = false;

        var w = 0;
        var h = 0;
        var left = 0;
        var top = 0;

        if (!bMaximize) {
            var preferedWidth = 1024;
            var preferedHeight = 768;
            w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
            h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
        } else {
            w = screen.availWidth - 10;
            h = screen.availHeight - 50;
        }
        var duWindow = window.open(
            VRoot + '/main/LaunchDODU.aspx?id=' + sCacheId,
            'ExportLogin_' + PML.getLoanId().replace(/-/g, ''),
            'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes'
        );
        
        return duWindow;
    }

    function pollDODUWindow(duWindow, results, onComplete) {
        if (duWindow == null) {
            onComplete();
            return;
        }

        if (duWindow.closed) {
            duWindow = null;
            onComplete();
            downloadDODUData(results);

        } else {
            window.setTimeout(function() { pollDODUWindow(duWindow, results, onComplete) }, pollingInterval);
        }
    }

    function downloadDODUData(args) {
        LQBPopup.Show(gVirtualRoot + '/main/FannieDownloadOptions.aspx?loanid=' + PML.getLoanId(),
        {
            onReturn: downloadDataCB,
            width: 500,
            height: 450,
            hideCloseButton: true
        },
        args);
    }

    function downloadDataCB() { }

    //----- LP
    function submitToLP(LoanId, callback) {
        loginLP(LoanId, function(ok, entry) {
            if (!ok) return;

            var LPWindow = launchLP(entry, LoanId, true);
            if (!LPWindow) {
                alert('Unable to open new window. Check your popup-blocker.');
                return;
            }

            ShowWaitMessage("Waiting for LPA window to be closed to proceed.");

            pollLPWindow(LPWindow, function () {
                HideWaitMessage();
            });
        });
    }

    function loginLP(LoanId, callback) {
        var url = gVirtualRoot + '/main/LoanProspectorExportLogin.aspx?loanid=' + LoanId;
        LQBPopup.Show(url,
        {
            width: '600',
            height: '375',
            hideCloseButton: true,
            onReturn: callback
        });
    }

    function launchLP(entryPoint, LoanId, bIsBlockEditor) {
        var bMaximize = true;
        var bCenter = false;

        var w = 0;
        var h = 0;
        var left = 0;
        var top = 0;

        if (!bMaximize) {
            var preferedWidth = 1024;
            var preferedHeight = 768;
            w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
            h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
        }
        else {
            w = screen.availWidth - 10;
            h = screen.availHeight - 50;
        }

        var LPWindow = window.open(
            VRoot + '/main/LoanProspectorMain.aspx?loanid=' + LoanId + '&entrypoint=' + entryPoint,
            'ExportLP' + LoanId.replace(/-/g, ''),
            'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes'
        );
        
        return LPWindow;
    }

    function pollLPWindow(LPWindow, onComplete) {
        if (LPWindow == null) {
            onComplete();
            return;
        }

        if (LPWindow.closed) {
            LPWindow = null;
            onComplete();
        }
        else {
            window.setTimeout(function () { pollLPWindow(LPWindow, onComplete); }, pollingInterval);
        }
    }

    //----- FHA Total
    function submitToFHA(LoanId, callback) { 
        //TODO: Make sure it's not a VaIRRRL loan somehow
        var isVaIRRRL = false;
        if(isVaIRRRL) return;
  
        LQBPopup.Show(gVirtualRoot + '/main/FHATotalDataAudit.aspx?loanid=' + LoanId,
        {
            width:'860',
            height:'600',
            hideCloseButton: true,
            onReturn: function() { 
                setAUSOptionsVisibility();
            }
        });
    }

    function setAUSOptionsVisibility() {
    
    }

    return {
        DU: {
            Submit: submitToDU,
            SubmitSeamless: submitToDUSeamless
        },
        DO: {
            Submit: submitToDO
        },
        LP: {
            Submit: submitToLP,
            SubmitSeamless: submitToLPASeamless
        },
        FHA: {
            Submit: submitToFHA
        },
        Init: function(Settings) {
            if ($.isFunction(Settings.SetMsg)) SetWaitMessageText = Settings.SetMsg;
            if ($.isFunction(Settings.ShowMsg)) ShowWaitMessage = Settings.ShowMsg;
            if ($.isFunction(Settings.HideMsg)) HideWaitMessage = Settings.HideMsg;
        }
    }

})(jQuery);