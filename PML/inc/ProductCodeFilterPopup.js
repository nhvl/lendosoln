﻿var oldAvailableHtml = "";
var oldSelectedHtml = "";
var lastSearchTerm = "";

function OpenProductCodeFilterPopup() {
    PML.setShouldCallUnload(false);
    if (PML.getIsDirty()) {
        PML.sync_calc();
    }

    $('#AdvancedFilterPopup').dialog({
        title: "Product Code Filter",
        resizable: false,
        modal: true,
        width: 'auto',
        height: 'auto',
        dialogClass: "ProductCodeFilters"
    });

    oldAvailableHtml = $('#availableProductCodes').html();
    oldSelectedHtml = $('#selectedProductCodes').html();
}

function GenerateTooltipColumn(list, codesList) {
    list.empty();
    if (typeof (codesList) === 'undefined' || codesList.length == 0) {
        list.parent('td').hide();
    }
    $(codesList).each(function (i, codes) {
        list.append($('<li>', { html: codes }));
    });
}

function GenerateTooltip(toolTipCodes) {
    $('.TooltipList').parent('td').show();

    var convCodes = toolTipCodes['Conventional'];
    var convCodesList = $('#ConvCodes');

    var FHACodes = toolTipCodes['FHA'];
    var fhaCodesList = $('#FhaCodes');

    var VACodes = toolTipCodes['VA'];
    var vaCodesList = $('#VaCodes');

    var UsdaRuralCodes = toolTipCodes['UsdaRural'];
    var usdaRuralCodesList = $('#UsdaCodes');

    var OtherCodes = toolTipCodes['Other'];
    var otherCodesList = $('#OtherCodes');

    if ((typeof (convCodes) === 'undefined' || convCodes.length == 0) &&
        (typeof (FHACodes) === 'undefined' || FHACodes.length == 0) &&
        (typeof (VACodes) === 'undefined' || VACodes.length == 0) &&
        (typeof (UsdaRuralCodes) === 'undefined' || UsdaRuralCodes.length == 0) &&
        (typeof (OtherCodes) === 'undefined' || OtherCodes.length == 0)) {
        $('#NoCodesCol').show();
        $('.TooltipList').parent('td').hide();
    }
    else {
        $('#NoCodesCol').hide();
        GenerateTooltipColumn(convCodesList, convCodes);
        GenerateTooltipColumn(fhaCodesList, FHACodes);
        GenerateTooltipColumn(vaCodesList, VACodes);
        GenerateTooltipColumn(usdaRuralCodesList, UsdaRuralCodes);
        GenerateTooltipColumn(otherCodesList, OtherCodes);
    }
}

function DisplayTooltip(e, parent) {
    var offset = parent.offset();
    var left = e.pageX - offset.left + 10;
    var top = e.pageY - offset.top + 10;
    $('#SelectedCodesPopup').css({ left: left, top: top }).removeClass('Hidden');
}

function HideTooltip() {
    $('#SelectedCodesPopup').addClass('Hidden');
}

function RetrieveSelectedProductCodes() {
    return $('#sSelectedProductCodeFilter').val();
}

function LoadPickers() {
    var availablePickerOptions = JSON.parse($('#sAvailableProductCodeFilter').val());
    var availablePicker = $('#availableProductCodes');
    var availableOptions = [];
    for (var key in availablePickerOptions) {
        if (availablePickerOptions.hasOwnProperty(key)) {
            var visible = availablePickerOptions[key];
            if (visible) {
                availableOptions.push($('<option>', { value: key, text: key, title: key }));
            }
        }
    }

    var selectedPickerOptions = JSON.parse($('#sSelectedProductCodeFilter').val());
    var selectedPicker = $('#selectedProductCodes');
    var selectedOptions = [];
    selectedPicker.empty();
    for (var key in selectedPickerOptions) {
        if (selectedPickerOptions.hasOwnProperty(key)) {
            var visible = selectedPickerOptions[key];
            if (visible) {
                selectedOptions.push($('<option>', { value: key, text: key, title: key }));
            }
            else {
                selectedPicker.append($('<span>').append($('<option>', { value: key, text: key, title: key }).addClass('Hidden')));
            }
        }
    }

    var sortedAvailable = Sort($(availableOptions).map(function () {
        return this.toArray();
    }));
    availablePicker.empty();
    availablePicker.append(sortedAvailable);

    var sortedSelected = Sort($(selectedOptions).map(function () {
        return this.toArray();
    }));
    selectedPicker.append(sortedSelected);
}

function Sort(options) {
    options.sort(function (first, second) {
        return $(first).text() > $(second).text() ? 1 : -1;
    });

    return options;
}

jQuery(function ($) {
    $(window).on('loan/view_update_done', function (e) {
        var toolTipCodes = JSON.parse($('#sProductCodesByFileType').val());
        GenerateTooltip(toolTipCodes);
        LoadPickers();
    });

    $('#cancelBtn').click(function () {
        PML.setShouldCallUnload(true);
        var availablePicker = $('#availableProductCodes');
        availablePicker.empty();
        availablePicker.append(oldAvailableHtml);
        var selectedPicker = $('#selectedProductCodes');
        selectedPicker.empty();
        $('#availableCodeSearchBox').val("");
        $('#AdvancedFilterPopup').dialog('close');
    });

    $('#applyBtn').click(function () {
        PML.setShouldCallUnload(true);
        PML.setIsDirty(true);

        var selected = {};
        $('#selectedProductCodes').find('option').each(function () {
            selected[$(this).val()] = $(this).is(':visible');
        });

        // Update the model
        $('#sSelectedProductCodeFilter').val(JSON.stringify(selected));
        PML.updateFieldInModel($('#sSelectedProductCodeFilter')[0]);

        PML.sync_calc();

        $('#availableCodeSearchBox').val("");
        $('#AdvancedFilterPopup').dialog('close');
    });

    $('#addBtn').click(function () {
        $('#availableProductCodes').find('option:selected').remove().removeAttr('selected').appendTo('#selectedProductCodes');
        var selected = $('#selectedProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $('#addAllBtn').click(function () {
        $('#availableProductCodes').find("option:not('.Hidden')").remove().removeAttr('selected').appendTo('#selectedProductCodes');
        var selected = $('#selectedProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $('#removeBtn').click(function () {
        $('#selectedProductCodes').find('option:selected').remove().removeAttr('selected').appendTo('#availableProductCodes');
        lastSearchTerm = "";
        TriggerSearch();
        var selected = $('#availableProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $('#removeAllBtn').click(function () {
        $('#selectedProductCodes').find("option:not('.Hidden')").remove().removeAttr('selected').appendTo('#availableProductCodes');
        lastSearchTerm = "";
        TriggerSearch();
        var selected = $('#availableProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $('#selectNoneLink').click(function () {
        $('#selectedProductCodes').find('option:selected').removeAttr('selected');
        $('#availableProductCodes').find('option:selected').removeAttr('selected');
    });

    var timer;
    var delay = 500;
    $("#availableCodeSearchBox").bind('input propertychange', function () {
        clearTimeout(timer);
        var searchBox = $(this);
        timer = setTimeout(function () {
            TriggerSearch(searchBox);
        }, delay);
    });

    function TriggerSearch() {
        var searchBox = $('#availableCodeSearchBox');
        var currentTerm = searchBox.val().toLowerCase();
        if (currentTerm === lastSearchTerm) {
            return;
        }

        $('#availableProductCodes').find('span').each(function () {
            var option = $(this).find('option').removeClass('Hidden');
            $(this).replaceWith(option);
        });

        var terms = currentTerm.split(",");
        if (currentTerm !== '') {
            $('#availableProductCodes').find('option').each(function () {
                var option = $(this);
                var value = option.val().toLowerCase();

                var hasExclude = false;
                var hasInclude = false;
                var matchIncludeTerms = false;
                var matchExcludeTerms = false;
                $(terms).each(function (i, searchTerm) {
                    var currentTerm = $.trim(searchTerm);
                    var isNegation = currentTerm.indexOf('-') === 0;

                    if (isNegation) {
                        hasExclude = true;
                        currentTerm = currentTerm.substring(1);
                        matchExcludeTerms = matchExcludeTerms || value.indexOf(currentTerm) === -1;
                    }
                    else {
                        hasInclude = true;
                        matchIncludeTerms = matchIncludeTerms || value.indexOf(currentTerm) !== -1;
                    }
                });

                if ((hasInclude && !matchIncludeTerms) || (hasExclude && !matchExcludeTerms)) {
                    option.addClass('Hidden').wrap('<span>');
                }
            });
        }

        lastSearchTerm = currentTerm;
    }
});