﻿QuickPricer = {
    __oAdjustmentList: null,
    __oMainPanel: null,

    // Public Static Function
    RenderResult: function(oDivPanel, oData, sNoResultMessage, bAllowCreateLoan)
    {

        this.__oMainPanel = oDivPanel;
        var $oDivPanel = $j(oDivPanel);
        var _that = this;
        if (!!!$oDivPanel.data('initializedEvent'))
        {
            $oDivPanel.on('click', '.adjustmentsLink', { 'that': _that }, this.RenderAdjustment);
            $oDivPanel.on('click', '.btnCreateLoan', function() { f_createLoan(); });
        }
        $oDivPanel.empty(); //.innerHTML = "";

        var oTimeStamp = oData[0];
        var oTableHeaders = oData[1];
        var oLoanProgramList = oData[2];
        var oPriceGroup = oData[3];
        var oAdjustmentList = oData[4];
        $oDivPanel.data('AdjustmentList', oData[4]);
        //this.__oAdjustmentList = oData[4];

        var cssGroupName = "QuickPricerResultGroupName";
        var cssItemHeader = "QuickPricerResultItemHeader";
        var cssAltItem = "QuickPricerResultAltItem";
        var cssItem = "QuickPricerResultItem";
        var cssError = "QuickPricerError";
        var cssRateExpired = "QuickPricerRateExpired";
        var cssTimestamp = "Timestamp";

        $j('<div />').addClass(cssTimestamp).text("Executed: " + oTimeStamp).appendTo($oDivPanel);
        //var timeStampLabel = document.createElement("div");
        //timeStampLabel.className = cssTimestamp;
        //this.__SetTextContent(timeStampLabel, "Executed: " + oTimeStamp);

        //oDivPanel.appendChild(timeStampLabel);

        if (oPriceGroup != "")
        {
            $j('<div/>').addClass(cssTimestamp).text("Price Group: " + oPriceGroup).appendTo($oDivPanel);
            //var priceGroupLabel = document.createElement("div");
            //priceGroupLabel.className = cssTimestamp;
            //this.__SetTextContent(priceGroupLabel, "Price Group: " + oPriceGroup);

            //oDivPanel.appendChild(priceGroupLabel);

        }
        var $oTable = $j('<table/>').prop({ 'cellPadding': 0, 'cellSpacing': 0 });
        //var oTable = document.createElement("table");
        //oTable.cellPadding = "0";
        //oTable.cellSpacing = "0";

        var bHasNoRate = true;
        for (var i = 0; i < oLoanProgramList.length; i++)
        {

            var oLoanProgram = oLoanProgramList[i];
            var sLoanProgramName = oLoanProgram[0];
          
            var oRateOptions = oLoanProgram[2];
            if (oRateOptions.length == 0)
            {
                continue; // Skip render 0 rate.
            }
            bHasNoRate = false;
            var $oTBody = $j('<tbody/>');
            //var oTBody = document.createElement("tbody");

            this.__InsertFullRow($oTBody, oTableHeaders.length, sLoanProgramName, cssGroupName);
            this.__InsertRow($oTBody, oTableHeaders, cssItemHeader);
            for (var j = 0; j < oRateOptions.length; j++)
            {
                var bIsRateExpired = oRateOptions[j][6] === "1";
                var className = ((j % 2) == 0) ? cssAltItem : cssItem;
                if (bIsRateExpired)
                {
                    className += (" " + cssRateExpired);
                }
                
                this.__InsertRateRow($oTBody, oRateOptions[j], className, oAdjustmentList);
            }
            if (bAllowCreateLoan)
            {
                this.__InsertFullRow($oTBody, oTableHeaders.length, "<input id='btnCreate" + i + "' type='button' value='Create Loan' class='btnCreateLoan'>", "centerAlign");
            }
            this.__InsertFullRow($oTBody, oTableHeaders.length, "<hr>", "");
            $oTBody.appendTo($oTable);
            //oTable.appendChild($oTBody);
        }

        if (bHasNoRate)
        {
            var $oNoResultDiv = $j('<div/>').addClass(cssError).appendTo($oDivPanel);
            //var oNoResultDiv = document.createElement("div");
            //oNoResultDiv.className = cssError;
            if (null == sNoResultMessage || "" == sNoResultMessage)
            {
                $oNoResultDiv.text('No eligible loan program.');
                //this.__SetTextContent(oNoResultDiv, "No eligible loan program.");
            } else
            {
                $oNoResultDiv.text(sNoResultMessage);
                //this.__setTextContent(oNoResultDiv, sNoResultMessage);
            }
            //oDivPanel.appendChild(oNoResultDiv);
            return;
        }
        $oTable.appendTo($oDivPanel);
        //oDivPanel.appendChild(oTable);

        //for (var i = 0; i < oLoanProgramList.length; i++)
        //{
        //var o = document.getElementById('btnCreate' + i);
        //if (null != o) o.onclick = f_createLoan; // Bind Create Loan function.
        //}
    },

    RenderAdjustment: function(event)
    {
        var index = $j(event.target).data('index');
        var oAdjustmentList = $j(event.delegateTarget).data('AdjustmentList')[index][1][0];
        var opCode = $j(event.target).data('OpCode');
        var _that = event.data.that;
        var oId = "QuickPricer__AdjustmentPanel";
        var $oDiv = $j('#' + oId);
        //if (null == oDiv)
        if ($oDiv.length == 0)
        {
            $oDiv = $j('<div/>').prop('id', oId).addClass('QuickPricerAdjustmentPanel').appendTo(event.delegateTarget);

            $oDiv.on("click", ".adjustCloseLink", function(event)
            {
                // Handle close event
                $oDiv.hide();
                return false;
            });

            //oDiv = document.createElement("div");
            //oDiv.id = oId;
            //oDiv.className = "QuickPricerAdjustmentPanel";
            //this.__oMainPanel.appendChild(oDiv);
        }

        $oDiv.empty();
        $oDiv.css({ 'left': 0, 'top': 0, 'width': 'auto' }).show();
        //oDiv.innerHTML = "";
        //oDiv.style.left = "";
        //oDiv.style.top = "";
        //oDiv.style.width = "auto";
        //oDiv.style.display = '';

        var $oRow = null;
        var $oCell = null;

        var $oTable = $j("<table/>").prop('cellPadding', 3).appendTo($oDiv);
        var $oTBody = $j("<tbody/>").appendTo($oTable);
        //oTable.cellPadding = 3;
        //var oTBody = document.createElement("tbody");
        //oTable.appendChild(oTBody);
        //oDiv.appendChild(oTable);

        var $oRow = $j("<tr/>").appendTo($oTBody);
        var $oCell = $j("<td/>").prop('colSpan', 3).appendTo($oRow);
        //oRow = this.__CreateAndAppend("tr", oTBody);
        //oCell = this.__CreateAndAppend("td", oRow);
        //oCell.colSpan = 3;
        
        var adjustments = [], 
            adjCount = oAdjustmentList.length, 
            i = 0, j,
            adjustment;
        
        for (;i < adjCount; i++)
        {
            adjustment = oAdjustmentList[i];
            adjustmentOp = adjustment[3];
            
            if( adjustmentOp == '' )
            {
                adjustments.push(adjustment);
            }
            else if (opCode.length > 0){
                for ( j=0; j < opCode.length; j++)
                {
                    if (opCode.charAt(j) == adjustmentOp) 
                    {
                        adjustment.push(adjustment);
                        break;
                    }
                }
            }
        }
              
        
        if (adjustments.length == 0)
        {
            $oCell.text("No adjustments were made.");
            //this.__SetTextContent(oCell, "No adjustments were made.");
        }
        else
        {
            $oCell.text("The following adjustments were made:");
            //this.__SetTextContent(oCell, "The following adjustments were made:");
            _that.__InsertRow($oTBody, ["RATE", "POINT", "DESCRIPTION"], "bold");

            for (var i = 0; i < adjustments.length; i++)
            {
                var o = adjustments[i];
                var data = [o[0], o[1], o[2]];
                _that.__InsertRow($oTBody, data, "");
            }


            if ($oDiv.width() > 300)
            {
                $oDiv.width(300);
            }
            //if (oDiv.offsetWidth > 300)
            //    oDiv.style.width = "300px";
        }
        $oRow = $j("<tr/>").appendTo($oTBody);
        $oCell = $j("<td/>").prop({ 'align': 'center', 'colSpan': 3 }).appendTo($oRow);

        $j('<a/>').prop('href', '#').text('Close').addClass('adjustCloseLink').appendTo($oCell);
        // TODO Bind Event

        //oRow = this.__CreateAndAppend("tr", oTBody);
        //oCell = this.__CreateAndAppend("td", oRow);
        //oCell.align = "center";
        //oCell.colSpan = 3;
        //oCell.innerHTML = "<a href=\"#\" onclick=\"document.getElementById('" + oId + "').style.display = 'none'; return false;\">Close</a>";

        // TODO Set the right display cordinates.
        //var width = $j(document).width)
        var top = event.pageY - $oDiv.height(); // -this.offsetLeft;
        var left = event.pageX - ($oDiv.width() / 2); // -this.offsetTop;
        $oDiv.offset({ top: top, left: left });

        return false;
        //var e = window.event;
        //var B = document.body; //IE 'quirks'
        //var D = document.documentElement; //IE with doctype
        //D = (D.clientHeight) ? D : B;

        //var x_cord = D.scrollLeft + e.clientX - (oDiv.offsetWidth / 2);
        //x_cord = Math.min(x_cord, D.scrollLeft + D.offsetWidth - oDiv.offsetWidth - 15);
        //oDiv.style.left = x_cord + "px";
        //oDiv.style.top = (D.scrollTop + e.clientY - (oDiv.offsetHeight / 2)) + "px";
        //return false;

    },

    // Private Static Functions
    /*
    __CreateAndAppend: function(tagName, oParent, cssName)
    {
    console.log('DO NOT USE __CreateAndAppend');
    var oItem = document.createElement(tagName);
    if (null != cssName && "" != cssName)
    oItem.className = cssName;
    oParent.appendChild(oItem);
    return oItem;
    },
    */
    __InsertFullRow: function(oParent, colSpan, str, cssName)
    {
        var $oRow = $j('<tr/>').appendTo(oParent);
        $j('<td/>').addClass(cssName).prop('colSpan', colSpan).html(str).appendTo($oRow);
        /*
        var oRow = this.__CreateAndAppend("tr", oParent);
        var oCell = this.__CreateAndAppend("td", oRow);
        oCell.colSpan = colSpan;
        oCell.innerHTML = str;
        if (null != cssName && "" != cssName) {
        oCell.className = cssName;
        }
        */
    },

    __InsertRow: function(oParent, data, cssName)
    {
        var $oRow = $j('<tr/>').addClass(cssName).appendTo(oParent);
        for (var i = 0; i < data.length; i++)
        {
            $j('<td/>').prop('vAlign', 'top').text(data[i]).appendTo($oRow);
        }
        /*
        var oRow = this.__CreateAndAppend("tr", oParent);
        if (null != cssName && "" != cssName) {
        oRow.className = cssName;
        }
        for (var i = 0; i < data.length; i++) {
        var oCell = this.__CreateAndAppend("td", oRow);
        oCell.vAlign = "top";
        this.__SetTextContent(oCell, data[i]);
        }
        */
    },
    __InsertRateRow: function(oParent, data, cssName, adjustmentList)
    {
        var $oRow = $j('<tr/>').addClass(cssName).appendTo(oParent);
        $j('<td/>').text(data[0]).appendTo($oRow); // rate
        $j('<td/>').text(data[1]).appendTo($oRow); // point
        $j('<td/>').text(data[2]).appendTo($oRow); // payment
        // 8/17/2015 - IR - OPM 223223 - skip data[3] dti.
        var lpName = adjustmentList[data[4]][0];
        $j('<td/>').text(lpName).css({ 'text-align': 'left', 'border-left': '1px solid black', 'padding-left': '5px' }).prop('noWrap', true).appendTo($oRow);
        var $oCell = $j('<td/>').appendTo($oRow); // link
        $j('<a/>').addClass('padLeft adjustmentsLink').text('adjustments').prop('href', '#').data('index', data[4]).data('OpCode', data[5]).appendTo($oCell);

        /*
        var oRow = this.__CreateAndAppend("tr", oParent);
        if (null != cssName && "" != cssName) {
        oRow.className = cssName;
        }

        var oCell = null;
        oCell = this.__CreateAndAppend("td", oRow);
        this.__SetTextContent(oCell, data[0]); // rate

        oCell = this.__CreateAndAppend("td", oRow);
        this.__SetTextContent(oCell, data[1]); // point

        oCell = this.__CreateAndAppend("td", oRow);
        this.__SetTextContent(oCell, data[2]); // payment

        oCell = this.__CreateAndAppend("td", oRow);
        oCell.innerHTML = "<a class=\"padLeft\" href=\"#\" >adjustments</a>";

        oCell.onclick = function() { return QuickPricer.RenderAdjustment(data[3]); }
        */
    }
    /*
    __SetTextContent: function(element, text)
    {
    while (element.firstChild !== null)
    element.removeChild(element.firstChild); // remove all existing content
    element.appendChild(document.createTextNode(text));
    }
    */
};
