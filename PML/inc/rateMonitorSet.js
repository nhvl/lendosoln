﻿RateMonitorSet.prototype.classHasBeenConfigured = false;
RateMonitorSet.prototype.deleteSet = function() {
    args = { loanID: this.loanID };
    var thisMonitorSet = this;
    $j.ajax({
        type: "POST",
        url: this.serviceUrl + "/DeleteSet",
        async: false,
        data: JSON.stringify(args),
        contentType: "application/json",
        dataType: "json",
        cache: false,
        success: function(data) {
            var result = data.d;

            if (typeof thisMonitorSet.onDeleteSuccess === 'function') {
                thisMonitorSet.onDeleteSuccess(result, args);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("Errors happened: " + textStatus + ", thrown: " + errorThrown + ", xhr status: " + jqXHR.status);
            if (typeof onDeleteError === 'function') {
                onDeleteError();
            }
        }
    });
}

RateMonitorSet.prototype.config = function(serviceUrl, onDeleteSuccess, onDeleteError) {
    if (true === RateMonitorSet.prototype.classHasBeenConfigured) {
        throw new Error('Cannot reconfigure rateMonitorSet after it has been configured.');
    }

    RateMonitorSet.prototype.serviceUrl = serviceUrl;
    RateMonitorSet.prototype.onDeleteSuccess = onDeleteSuccess;
    RateMonitorSet.prototype.onDeleteError = onDeleteError;
    
    RateMonitorSet.prototype.classHasBeenConfigured = true;
}

function RateMonitorSet(loanID, scenarioName) {

    if (false === RateMonitorSet.prototype.classHasBeenConfigured) {
        throw new Error('RateMonitorSet class has not been configured so cannot be instantiated.');
    }
    this.loanID = loanID;
    this.scenarioName = scenarioName;

    // Public accessors:
    return this;
}