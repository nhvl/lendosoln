﻿$(function () {
    var $available = $('.available-items');
    var $assigned = $('.assigned-items');

    $('#Assign').click(assign);
    $('#Remove').click(remove);
    $('#MoveUp').click(moveUp);
    $('#MoveDown').click(moveDown);

    $available.change(function () { $assigned.val('') });
    $assigned.change(function () { $available.val('') });

    $available.dblclick(function () { transferSelected($available, $assigned) });
    $assigned.dblclick(function () { transferSelected($assigned, $available) });

    function assign() {
        transferSelected($available, $assigned);
    }

    function remove() {
        transferSelected($assigned, $available);
        sortAvailable();
    }

    function transferSelected(from, to) {
        transfer(from, to, 'option:selected');
    }

    function transfer(from, to, selector) {
        from.find(selector).each(function (index, elem) {
            to.append(elem.outerHTML);
        }).remove();
    }

    function moveUp() {
        $.each($assigned.find('option:selected'), function (index, option) {
            var $option = $(option);
            var $prev = $option.prev();

            if ($prev.length !== 0) {
                $option.insertBefore($prev);
            }
        });
    }

    function moveDown() {
        $.each($assigned.find('option:selected').get().reverse(), function (index, option) {
            var $option = $(option);
            var $next = $option.next();

            if ($next.length !== 0) {
                $option.insertAfter($next);
            }
        });
    }

    function sortAvailable() {
        var options = $available.children('option');
        options.sort(function (a, b) {
            return a.text.localeCompare(b.text);
        });

        $available.empty().append(options);
    }

    TPOStyleUnification.Components();
});
