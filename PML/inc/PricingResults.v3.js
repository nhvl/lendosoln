﻿var pricingResultsApp;
var resultsScope;
var firstElement;
var secondResultsScope;
var secondElement;
var mainScope;
var firstRate;
var isInitialized = false;
var isAuditRender = false;
var isNonQm = false;

function initializeAngular(IsNonQm) {
    isInitialized = true;
	pricingResultsApp = angular.module("pricingResults", ["ngSanitize"]);

	if (!!IsNonQm) {
		isNonQm = true;
	}

    $j("#ResultsContainer").on("mouseover", ".disqualified", showTooltip);
    $j("#ResultsContainer").on("mouseleave", ".disqualified", hideTooltip);
    $j("#ResultsContainer").on('mouseenter','.preview-col', function(){
        var $link = $(this).find('a');

        if(this.offsetWidth < this.scrollWidth){
            $link.attr('title', $link.text());
        }
        else{
            $link.attr('title', '');
        }
    });

    pricingResultsApp.controller("pricingResultsController", ['$scope', '$compile', '$element', '$timeout', function ($scope, $compile, $element, $timeout) {
        mainScope = $scope;
        $scope.selectOption = function (event, rateOption, program, group, isLock, isSkip, isManual) {
            if (isAuditRender) {
                return;
            }

            var el = $j(event.currentTarget);
            var scope;
            if (!el.attr("disabled")) {
                if (firstRate == null) {
                    scope = resultsScope;
                    firstRateLockDisabledReasons = GetReasonsActionDisabled(rateOption, group, "lock");
                    firstRateRegisterDisabledReasons = GetReasonsActionDisabled(rateOption, group, "register");
                    if (scope.pricingSettings.Is8020PricingScenario) {
                        firstRate = rateOption;
                        resultsScope.firstRate = rateOption;
                    }
                }
                else {
                    scope = secondResultsScope;
                }

                // If this popup is open when submitting, close it.
                $j(".disqual-reason-popup.ui-dialog-content").dialog("destroy");
                submitRate(rateOption, program, group, isLock, isSkip /*skip*/, isManual /*manual*/, scope.pricingSettings.IsPricing2ndLoan && scope.pricingSettings.Is8020PricingScenario);
            }
            else {
                var action;
                switch (el.text().trim()) {
                    case "register": action = "register";
                        break;  
                    case "price 2nd lien": action = "both";
                        break;
                    default: action = "lock";
                }
                displayPermissionError(action, mainScope, rateOption, group);
            }
        };

		$scope.nonQmSelect = function (rateOption, program, isDocTypeBankStatement, isScenario, showPrice) {
			RateOption  = rateOption;
			Program = program;
			
			$("#QuickPricerTable").hide();

			if (isDocTypeBankStatement && !isScenario) {
				if ($("#" + ML.BankStatementIncomeCalculatorLink, parent.document).length) {
					$("#" + ML.NonQmQuickPricerLink, parent.document).removeClass("selected");
					$("#" + ML.BankStatementIncomeCalculatorLink, parent.document).addClass("selected");
				}

				validateBankStatementInput();
				
				$("#BankStatementIncomeCalculatorTable").show();
			}
			else {
				if ($("#" + ML.ScenarioRequestFormLink, parent.document).length) {
					$("#" + ML.NonQmQuickPricerLink, parent.document).removeClass("selected");
					$("#" + ML.ScenarioRequestFormLink, parent.document).addClass("selected");
				}
				
				$("#ScenarioRequestFormTable").show();

				$("#ScenarioRequestForm_sLAmtCalcPe").val($("#QuickPricer_sLAmtCalcPe").val());
				$("#ScenarioRequestForm_sLtvRPe").val($("#QuickPricer_sLtvRPe").val());
				$("#ScenarioRequestForm_sLPurposeT").val($("#QuickPricer_sLPurposeT").val());
				$("#ScenarioRequestForm_sOccTPe").val($("#QuickPricer_sOccTPe").val());
				$("#ScenarioRequestForm_sProdSpT").val($("#QuickPricer_sProdSpT").val());
				$("#ScenarioRequestForm_sSpStatePe").val($("#QuickPricer_sSpStatePe").val());
				$("#ScenarioRequestForm_sCreditScoreEstimatePe").val($("#QuickPricer_sCreditScoreEstimatePe").val());
				$("#ScenarioRequestForm_sManuallyEnteredHousingHistory").val($("#QuickPricer_sManuallyEnteredHousingHistory").val());
				$("#ScenarioRequestForm_sManuallyEnteredHousingEvents").val($("#QuickPricer_sManuallyEnteredHousingEvents").val());
				$("#ScenarioRequestForm_sManuallyEnteredBankruptcy").val($("#QuickPricer_sManuallyEnteredBankruptcy").val());
				$("#ScenarioRequestForm_aProdBCitizenT").val($("#QuickPricer_aProdBCitizenT").val());
				$("#ScenarioRequestForm_sProdDocT").val($("#QuickPricer_sProdDocT").val());

				validateScenarioRequestFormInput();
			}

			containsPricingResults = true;

			$(".selectedRate").text("Selected rate: " + RateOption.Rate + "%\nSelected " + (showPrice ? "price: " + RateOption.Price : "point: " + RateOption.Point));
			$(".selectedRateRow").toggle(!isScenario); // isScenario is same as selecting a ineligible program
			isIneligible = !!isScenario;

			if (program.DisqualifiedRuleList == undefined) {
				return;
			}

			var disqualReason = "Requested Scenario: " + program.lLpTemplateNm + "\nRate: " + rateOption.Rate;


			if (showPrice) {
				disqualReason += " Price: " + rateOption.Price + "\nListed Reasons for Disqualification:";
			}
			else {
				disqualReason += " Point: " + rateOption.Point + "\nListed Reasons for Disqualification:";
			}

			var isEmpty = true;

			for (var i = 0; i < program.DisqualifiedRuleList.length; i++) {
				disqualReason += program.DisqualifiedRuleList[i] + "\n";
				if (program.DisqualifiedRuleList[i] !== "") {
					isEmpty = false;
				}
			}

			if (!isEmpty) {
				$("#ScenarioRequestForm_MsgToLender").val(disqualReason);
			}
		}

        $scope.viewRatePopup = function (rateOption, program, group, selector) {
            if (isAuditRender) {
                return;
            }

            var element;
            var height;
            if (selector == '.cash-to-close' && ML.IsUlad2019) {
                populateQualifyingBorrower(rateOption);
                selector = '.true-container';
                element = 'body';
                height = 700;
            }
            else {
                var scope = getCurrentScope();
                scope.selectedRate = rateOption;
                scope.selectedProgram = program;
                scope.selectedGroup = group;

                element = scope.Is2ndResults ? secondElement : firstElement;
                height = "auto";
            }

            $j(element).find(selector).dialog({
                buttons: [
                    {
                        text: "Ok",
                        click: function () {
                            $j(this).dialog("destroy");
                        }
                    }
                ],
                close: function() {
                    $j(this).dialog("destroy");
                },
                modal: true,
                width: "auto",
                height: height,
                maxHeight: 600
            });
        };

        $scope.IsActionDisabled = function (rateOption, group, action) {
            if (isAuditRender) {
                return;
            }
            var currentScope = getCurrentScope();
            var registerDenialReasons = currentScope.pricingSettings.RegisterDenialReasons;
            var lockDenialReasons = currentScope.pricingSettings.LockDenialReasons;

            var actionReasons;
            if (action == "both") {
                if (registerDenialReasons.length > 0 && lockDenialReasons.length > 0) {
                    actionReasons = $j.extend([], registerDenialReasons, lockDenialReasons);
                }
            }
            else {
                actionReasons = action == "register" ? registerDenialReasons : lockDenialReasons;
            }

            return ML.HasDuplicate || (actionReasons && actionReasons.length > 0) || GetReasonsActionDisabled(rateOption, group, action).length > 0;
        };

        $scope.toggleWorseRates = function (rateOption, rateOptionWinner) {
            if (rateOptionWinner) {
                rateOptionWinner.showWorseRates = !rateOptionWinner.showWorseRates;
            }
            else {
                rateOption.showWorseRates = !rateOption.showWorseRates;
            }
        };

        $scope.setSelectedRate = function (rateOption) {
            if (isAuditRender) {
                return;
            }
            $scope.selectedRate = rateOption;
        };

        $scope.displayPermissionError = displayPermissionError;

        $scope.previewCert = function (rateOption, program) {
            if (isAuditRender) { 
                return;
            }

            f_previewRateOption(rateOption, program);
        }

        $scope.backToFirst = function () {
            if (isAuditRender) {
                return;
            }

            if (!isCurrentlyPricing) {
                secondResultsScope.$destroy();
                resultsScope.firstRate = null;
                $j(secondElement).remove();
                IsPricing2nd = false;
                firstRate = null;
            }
        };

        $scope.compileResults = function (results, sDisclosureRegulationT, pricingSettings) {

            var scope = $scope.$new();

            scope.results = results;
            scope.sDisclosureRegulationT = sDisclosureRegulationT;
            scope.pricingSettings = pricingSettings;
            var compiledDirective = $compile("<pricing-results-directive></pricing-results-directive>");
            var element = compiledDirective(scope);
            $j("#ResultsContainer" + (isAuditRender ? (IsPricing2nd ? 2 : 1) : '')).append(element);

            if (pricingSettings.IsPricing2ndLoan) {
                secondResultsScope = scope;
                scope.Is2ndResults = true;
                secondElement = element;
            } else {
                resultsScope = scope;
                firstElement = element;
            }

            createScrollEvent();
        };

        $scope.hasFirstAmericanSource = function (rateOption) {
            if (rateOption) {
                var firstAmericanFees = rateOption.ClosingCostBreakdown.filter(function (fee) {
                    return fee.Source == "First American Quote";
                });

                return firstAmericanFees.length > 0;
            }

            return false;
        };

        $scope.pin = function (event, rateOption, program) {
            if (isAuditRender) {
                return;
            }
             var el = $j(event.currentTarget);
             if (el.attr("disabled")) {
                 return;
             }

             var newPinId = null, secondTemplateId = null, secondRate = null, firstTemplateId = null, firstRate = null, secondPayment = null;

            if (this.Is2ndResults) {
                secondTemplateId = rateOption.LpTemplateId;
                secondRate = rateOption.Rate;
                secondPayment = rateOption.Payment;
                firstRate = resultsScope.firstRate.Rate;
                firstTemplateId = resultsScope.firstRate.LpTemplateId;
            }

            else {
                firstRate = rateOption.Rate;
                firstTemplateId = rateOption.LpTemplateId;
            }

            newPinId = editPinState($j(event.currentTarget), firstTemplateId, firstRate, secondTemplateId, secondRate, secondPayment);
            if (newPinId != null) {
                rateOption.PinId = newPinId;
            }
        };

        $scope.determinePinText = function (rateOption) {
            var scope = getCurrentScope();

            var hasPin = rateOption.PinId != null && rateOption.PinId != '';

            if (scope.pricingSettings.Is8020PricingScenario && !scope.pricingSettings.IsPricing2ndLoan) {
                return hasPin ? "combo" : "";
            }
            else {
                return hasPin ? "unpin" : "pin";
            }
        };
    }]);

    pricingResultsApp.directive("pricingResultsDirective", function () {
		return {
			templateUrl: ML.VirtualRoot + (isNonQm ? "/LendersOffice/PricingResults.v3.html" : "/main/PmlAngular/PricingResults.v3.html"),
            restrict: "E",
            scope: true,
            link: function (scope, element, attrs) {
                scope.getPricingSetting = getPricingSetting;
            }
        };
    });

    pricingResultsApp.directive("programTable", function () {
        return {
            templateUrl: ML.VirtualRoot + "/main/PmlAngular/PricingResultProgramTable.html",
            replace: true,
            scope: true,
            link: function (scope, element, attrs) {
                scope.programSet = attrs.programset;
                scope.limitTo = attrs.limitto;
            }
        }
    })

    pricingResultsApp.directive("programRow", function () {
        return {
            templateUrl: ML.VirtualRoot + "/main/PmlAngular/PricingResultRow.html",
            replace: true,
            scope: true,
            link: function (scope, element, attrs) {
                scope.rateOption = scope.$parent.$eval(attrs.rateoption);
                scope.rateOption.WorseRates = sortWorstRates(scope.rateOption.WorseRates);
            }
        }
    });

        function sortWorstRates (rateOptions) {
            return rateOptions.sort(worseRatesComparator);
        }

    pricingResultsApp.filter("sortWorstPricing", function () {
        return function (rateOptions, isWorst) {
            if (!isWorst) {
                return rateOptions;
            }
            else {
                return rateOptions.sort(worstPricingComparator);
            }
        }
    });

    pricingResultsApp.config(['$compileProvider', function ($compileProvider) {
        $compileProvider.debugInfoEnabled(false);
    }]);

    angular.bootstrap(document.getElementById("PricingResultsContainer" + (isAuditRender ? (IsPricing2nd ? 2 : 1) : '')), ['pricingResults']);
}

var firstRateRegisterDisabledReasons = [];
var firstRateLockDisabledReasons = [];
function GetReasonsActionDisabled(rateOption, group, action) {
    var pricingSettings = getCurrentScope().pricingSettings;
    var reasons = rateOption == firstRate ? [] : [].concat(pricingSettings.DenialReasons);

    if (rateOption != null) {
        var warningMessage = action == "lock" ? rateOption.RateLockSubmissionUserWarningMessage : "";
        if (warningMessage != "") {
            if (warningMessage == "EXPIRED") {
                if (action == "lock") {
                    reasons.push("Rate lock request for possibly expired rates")
                }
            }
            else {
                reasons.push(rateOption.RateLockSubmissionUserWarningMessage)
            }
        }

        if (group && group.IsIneligible && !ML.CanSubmitIneligible) {
            reasons.push("Allow applying for ineligible loan programs")
        }
    }

    if (firstRate && rateOption != firstRate)
    {
        //Get the first rate lock errors.
        $.each(GetFirstRateActionDisabledReasons(action), function (index, reason) {
            if (reasons.indexOf(reason) <= -1) 
            {
                reasons.push(reason);
            }
        });
    }

    return reasons;
}

function GetFirstRateActionDisabledReasons(action) {
    return action == "lock" ? firstRateLockDisabledReasons : firstRateRegisterDisabledReasons;
}


function worseRatesComparator(a, b) {
    var priceA = parseFloat(a.Price);
    var priceB = parseFloat(b.Price);
    var cashToCloseA = parseFloat(a.CashToClose);
    var cashToCloseB = parseFloat(b.CashToClose);

    if (priceA < priceB) {
        return 1;
    }

    if (priceA > priceB) {
        return -1;
    }

    if (a.IsRateExpired && !b.IsRateExpired) {
        return 1;
    }

    if (!a.IsRateExpired && b.IsRateExpired) {
        return -1;
    }

    if (cashToCloseA < cashToCloseB) {
        return -1;
    }

    if (cashToCloseA > cashToCloseB) {
        return 1;
    }

    return a.LpTemplateNm.localeCompare(b.LpTemplateNm);
}

function worstPricingComparator(a, b) {
    var rateA = parseFloat(a.RawRate);
    var rateB = parseFloat(b.RawRate);
    if (rateA < rateB) {
        return 1;
    }

    if (rateA > rateB) {
        return -1;
    }

    if (a.IsPaired && !b.IsPaired) {
        return -1;
    }

    if (!a.IsPaired && b.IsPaired) {
        return 1;
    }

    if (a.IsHistorical && !b.IsHistorical) {
        return -1;
    }

    if (!a.IsHistorical && b.IsHistorical) {
        return 1;
    }

    var priceA = parseFloat(a.Price);
    var priceB = parseFloat(b.Price);
    if (priceA < priceB) {
        return 1;
    }

    if (priceA > priceB) {
        return -1;
    }

    return 0;
}

function getScope(selector) {
    var appElement = $j(selector);
    return angular.element(appElement).scope();
}

function getPricingSetting(fieldId) {
    console.log($j("#" + fieldId).prop("checked"));
    return $j("#" + fieldId).prop("checked");
}

function updateResults(pricingSettings, sDisclosureRegulationT, results) {
    $j(window).off("scroll", infiniteScrollHandler);

    if (resultsScope != null && !pricingSettings.IsPricing2ndLoan) {
        resultsScope.$destroy();
        $j("#ResultsContainer" + (isAuditRender ? (IsPricing2nd ? 2 : 1) : '')).empty();
        firstRate = null;
        IsPricing2nd = false;
    }

    for (i = 0; i < results.IneligibleGroups.length; i++) {
        results.IneligibleGroups[i].IsCollapsed = true;
        results.IneligibleGroups[i].IsIneligible = true;
	}

	for (i = 0; i < results.InsufficientGroups.length; i++) {
		results.InsufficientGroups[i].IsInsufficient = true;
	}

    mainScope.compileResults(results, sDisclosureRegulationT, pricingSettings);
    console.log(results);
}

function getCurrentScope() {
    return IsPricing2nd ? secondResultsScope : resultsScope;
}

function createScrollEvent() {
    $j(window).on("scroll", infiniteScrollHandler);
    determineRepeatLimits(getCurrentScope());
}

function infiniteScrollHandler(event) {
    var scope = getCurrentScope();

    if (scope != null) {
        var $window = $j(window);
        var $document = $j(document);
        var scrollHeight = $document.height();
        var scrollPosition = $window.height() + $window.scrollTop();

        if ((scrollHeight - scrollPosition) < 200) {
            //We've reached the bottom of the page, so let's render some more programs
            determineRepeatLimits(scope);
            scope.$apply();
        }
    }
}

var extraHeight = 200;
var programHeight = 63;
var rateHeight = 37;

var currentHeight = 0;

function determineRepeatLimits(scope) {
    currentHeight = $j("#ResultsContainer"+ (isAuditRender ? (IsPricing2nd ? 2 : 1) : '')).height() + $j(".results-filter").height() + $j(".outermost-container > .header").height();

    if (scope.limitEligible == undefined) {
        scope.limitEligible = 0;
    }

    if (scope.limitInsufficient == undefined) {
        scope.limitInsufficient = 0;
    }
    if (scope.limitIneligible == undefined) {
        scope.limitIneligible = 0;
    }

    var eligibleGroups = scope.results.EligibleGroups;
    calculateLimitGroup("limitEligible", eligibleGroups, scope);


    var insufficientGroups = scope.results.InsufficientGroups;
    if (scope.limitEligible == eligibleGroups.length) {
        calculateLimitGroup("limitInsufficient", insufficientGroups, scope);
    }

    var ineligibleGroups = scope.results.IneligibleGroups;
    if (scope.limitInsufficient == insufficientGroups.length) {
        calculateLimitGroup("limitIneligible", ineligibleGroups, scope);
    }
}

function calculateLimitGroup(limitFieldName, set, scope) {
    if (set.length > 0) {
        var documentHeight = $j(document).height();
        var currentLimit = scope[limitFieldName];
        currentLimit = currentLimit == 0 || set[0].ResultRateOptions ? currentLimit : currentLimit - 1;
        if (currentLimit <= set.length) {
            for (i = currentLimit; i < set.length; i++) {
                var group = set[i];

                if (group.IsCollapsed) {
                    currentHeight += 25;
                }
                else if (group.ResultLoanPrograms) {
                    calculateLimitProgram(group.ResultLoanPrograms, group, documentHeight);
                }
                else if (group.ResultRateOptions) {
                    currentHeight += programHeight + rateHeight * group.ResultRateOptions.length;
                }


                if (currentHeight > documentHeight + 200) {
                    scope[limitFieldName] = i + 1;
                    break;
                }
                else if (i == set.length - 1) {
                    scope[limitFieldName] = i + 1;
                }
            }
        }
    }
}

function calculateLimitProgram(programs, group, documentHeight) {
    var currentLimit = group.limitTo;
    if (currentLimit == undefined) {
        currentLimit = 0;
    }

    for (j = currentLimit; j < programs.length; j++) {
        var program = programs[j];
        var rateOptions = program.ResultRateOptions;

        if (rateOptions) {
            if (program.IsDisplayingAllRates) {
                currentHeight += programHeight + rateHeight * rateOptions.length;
            }
            else {
                currentHeight += programHeight + rateHeight;
            }

            if (currentHeight > documentHeight + 200) {
                group.limitTo = j + 1;
                break;
            }
        }

        if (j == programs.length - 1) {
            group.limitTo = j + 1;
        }
    }
}

function f_generateQueryString(obj) {
    var o = obj || '';
    var str = '';
    for (var p in o) {
        if (o.hasOwnProperty(p)) {
            str += p + '=' + o[p] + '&';
        }
    }
    return str;
}

function hideTooltip() {
    $j(".disqual-reason-popup").dialog("close");
}

function showTooltip(event) {
    $j(".disqual-reason-popup").dialog({
        modal: false,
        closeOnEscape: true,
        width: 200,
        minHeight: 0,
        minWidth: 0,
        width: "auto",
        height: "auto",
        position: {
            my: "left+3 bottom-3",
            of: event,
            collision: "flip fit"
        }
    });
}

function displayPermissionError(action, $scope, rateOption, group) {

    var permissionReasons = GetReasonsActionDisabled(rateOption, group, action);
    $scope.$root.permissionReasons = permissionReasons;

    var registerDenialReasons = getCurrentScope().pricingSettings.RegisterDenialReasons;
    var lockDenialReasons = getCurrentScope().pricingSettings.LockDenialReasons;
    var workflowReasons = [];

    var registerFailedConditionType = getCurrentScope().pricingSettings.RegisterFailedConditionType;
    var LockFailedConditionType = getCurrentScope().pricingSettings.LockFailedConditionType;
    var workflowFailedConditionType;

    $scope.$root.registerDenialReasons = [];
    $scope.$root.lockDenialReasons = [];
    $scope.$root.workflowReasons = [];

    $scope.$root.permissionMessage = "You do not have permission to submit this file.";

    if (action == "both") {
        $scope.$root.registerDenialReasons = registerDenialReasons;
        $scope.$root.lockDenialReasons = lockDenialReasons;
    }
    else {

        workflowReasons = action == "register" ? registerDenialReasons : lockDenialReasons;
        workflowFailedConditionType = action == "register" ? registerFailedConditionType : LockFailedConditionType;

        $scope.$root.permissionMessage = "You do not have permission to " + action + " this loan";
        $scope.$root.workflowReasons = workflowReasons;
    }

    if (permissionReasons.length == 0 && workflowReasons.length == 0 && $scope.$root.registerDenialReasons.length == 0 && $scope.$root.lockDenialReasons.length == 0) {
        $scope.$root.permissionMessage = "Registering is not permitted for this loan file.";
    }
    else if (permissionReasons.length > 0)
    {
        $scope.$root.permissionMessage += ".  The following user permissions are required:";
    }
    else if(action != "both"){
        if (workflowFailedConditionType == 2 /* Restraint */) {
            $scope.$root.permissionMessage += ", because of the following reasons:";
        } else {
            $scope.$root.permissionMessage += ".  The loan can be " + action + "ed under any of the following circumstances:";
        }
    }

    $scope.viewRatePopup(rateOption, null, group, ".no-permission");
}

function f_previewRateOption(rateOption, program) {
    var extraParms = '';
    var scope = getCurrentScope();

    if (scope.pricingSettings.IsPricing2ndLoan && rateOption != firstRate)
    {
        extraParms =
          '&lienqualifymodet=1' 
        + '&version=' + firstRate.Version
        + '&FirstLienNoteRate=' + firstRate.Rate
        + '&FirstLienPoint=' + firstRate.Point
        + '&FirstLienLpId=' + firstRate.LpTemplateId
        + '&FirstLienMPmt=' + firstRate.Payment;
        }
    else {
        extraParms = 
          '&lienqualifymodet=0'  
        + '&version=' + rateOption.Version
        + '&productRate=' + rateOption.Rate
        + '&productDTI=' + rateOption.Dti
        + '&productPoint=' + rateOption.Point
        + '&productRawId=' + rateOption.RateOptionId;
    }

    var version = 0;
    _popupWindow = window.open(gVirtualRoot + '/Main/QualifiedLoanProgramPrintFrame.aspx?loanid=' + ML.sLId
        + '&productid=' + rateOption.LpTemplateId
        + '&uniqueChecksum=' + rateOption.UniqueChecksum
        + extraParms,
        
        'Preview_Certificate',
        'width=880px,height=580px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no');
}