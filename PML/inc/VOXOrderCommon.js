﻿var VOXOrderCommon = (function () {
    var GatherRequestData;
    var serviceType;
    var VOXServiceName;
    var ToggleOrderBtn;
    var validStatusesAfterInitialRequest = null;
    var shouldPushOutErrors = false;
    var voxSection = null;

    jQuery(function ($) {
        $('body').on('click', '.VoxSection .VOXPlaceOrderBtn', function () {
            var creditCardData = null;
            var allowPayWithCcCb = voxSection.find('.AllowPayWithCreditCard');
            if (allowPayWithCcCb.is(':visible') && allowPayWithCcCb.is(':checked')) {
                simpleDialog.alert("Credit Card not supported.", VOXServiceName);
            }
            else {
                RunAudit(creditCardData);
            }
        });

        $('body').on('change', '.VoxSection .RequiredInput:not(.RequiredInputSpecial)', function () {
            if ($(this).is('select')) {
                // TPO wraps selects in a div, so we'll want to target the sibling RequiredSpan of the div.
                $(this).closest('div').siblings('.RequiredSpan').toggle($(this).val() === '');
            }
            else {
                $(this).siblings('.RequiredSpan').toggle($(this).val() === '');
            }

            ToggleOrderBtn();
        });

        function RunAudit(creditCardData) {
            var data = GatherRequestData();
            if (data == null) {
                return;
            }

            if (creditCardData != null) {
                $.extend(data, creditCardData);
            }

            var result = gService[serviceType].callAsyncSimple("RunAudit", data,
                function (result) {
                    if (result.value["Success"].toLowerCase() === 'true') {
                        var auditResults = JSON.parse(result.value["AuditResults"]);
                        var auditPassed = result.value["AuditPassed"].toLowerCase() === 'true';
                        var auditPageTitle = VOXServiceName + " Request Data Audit";

                        var auditData = {
                            AuditPageTitle: auditPageTitle,
                            AuditResults: auditResults,
                            AuditPassed: auditPassed
                        };

                        var page = AuditPage.CreateAuditPage(auditData);
                        //iOPM: 468981
                        LQBPopup.ShowElement($('<div>').append(page), {
                            width: 650,
                            height: 500,
                            hideCloseButton: true,
                            onReturn: function (returnArgs) {
                                if (returnArgs && returnArgs.OK == true) {
                                    RunRequest(data);
                                }
                            }
                        });
                    }
                    else {
                        simpleDialog.alert(result.value["Errors"], VOXServiceName);
                    }
                },
                function(result) {
                    simpleDialog.alert(result.UserMessage, VOXServiceName);
                },
                ML.VOXTimeoutInMilliseconds);
        }

        function RunRequest(data) {
            var loading = $('<loading-icon id="LQBPopupUWaitTable">Placing ' + VOXServiceName + ' Order...</loading-icon>');
            TPOStyleUnification.UnifyLoadingIcons(loading);

            LQBPopup.ShowElement(loading, {
                width: 350,
                height: 200,
                hideCloseButton: true
            });

            window.setTimeout(function () {
                var result = gService[serviceType].callAsyncSimple("RunRequest", data,
                    function (results) {
                        HandleRequestResults(results);
                    }, ML.VOXTimeoutInMilliseconds);
            });
        }

        function HandleRequestResults(result) {
            if (result.error) {
                LQBPopup.Return();
                simpleDialog.alert(result.UserMessage, VOXServiceName).then(function () { LQBPopup.Return(null) });
            }
            else if (result.value["Success"].toLowerCase() !== 'true') {
                LQBPopup.Return();
                var requestErrors = JSON.parse(result.value["Errors"]);
                simpleDialog.multilineAlert(requestErrors, VOXServiceName).then(function () { LQBPopup.Return(null) });
            }
            else {
                if (result.value.PublicJobId) {
                    var publicJobId = result.value.PublicJobId;
                    var pollingIntervalInMilliseconds = parseInt(result.value.PollingIntervalInSeconds) * 1000;
                    var attemptNumber = 1;
                    window.setTimeout(function () {
                        PollForInitialResult(publicJobId, pollingIntervalInMilliseconds, attemptNumber, result.value);
                    }, pollingIntervalInMilliseconds);
                }
                else {
                    LQBPopup.Return();
                    HandleSuccessResult(result);
                }
            }
        }

        function PollForInitialResult(publicJobId, pollingIntervalInMilliseconds, attemptNumber, previousResult) {
            var args = {};
            args.LoanId = ML.sLId;
            args.PublicJobId = publicJobId;
            args.AttemptNumber = attemptNumber;
            for (var key in previousResult) {
                if (previousResult.hasOwnProperty(key)) {
                    args[key] = previousResult[key];
                }
            }

            gService[serviceType].callAsyncBypassOverlay("PollForInitialResult", args,
                function (result) {
                    if (result.error) {
                        LQBPopup.Return();
                        simpleDialog.alert(result.UserMessage, VOXServiceName).then(function () { LQBPopup.Return(null) });
                    }
                    else if (result.value["Success"].toLowerCase() === 'true') {
                        if (result.value.PublicJobId) {
                            publicJobId = result.value.PublicJobId;
                            window.setTimeout(function () {
                                PollForInitialResult(publicJobId, pollingIntervalInMilliseconds, ++attemptNumber, previousResult, serviceType);
                            }, pollingIntervalInMilliseconds);
                        }
                        else {
                            LQBPopup.Return();
                            HandleSuccessResult(result);
                        }
                    }
                    else {
                        LQBPopup.Return();
                        var requestErrors = JSON.parse(result.value["Errors"]);
                        simpleDialog.multilineAlert(requestErrors, VOXServiceName).then(function () { LQBPopup.Return(null) });
                    }
                });
        }

        function HandleSuccessResult(result) {
            var orders = JSON.parse(result.value["Orders"]);
            var errors = [];
            var successfulOrders = [];
            var completedCount = 0;
            $.each(orders, function (index, order) {
                if (validStatusesAfterInitialRequest != null && typeof (validStatusesAfterInitialRequest[order.Status]) === 'boolean') {
                    successfulOrders.push(order);

                    // The true/false value determines whether this order is considered "Completed"(true) or "Pending"(false).
                    if (validStatusesAfterInitialRequest[order.Status]) {
                        completedCount++;
                    }
                }
                else {
                    errors.push(order);
                }
            });

            if (successfulOrders.length != 0) {
                var popup;
                var afterPopupFunction = function () {
                    var stitchedCacheKey = result.value["Key"]
                    var orders;
                    if (shouldPushOutErrors) {
                        orders = successfulOrders.concat(errors);
                    }
                    else {
                        orders = successfulOrders;
                    }

                    addOrdersToTable(orders, stitchedCacheKey);

                    LQBPopup.Return(null);
                };

                if (completedCount == successfulOrders.length) {
                    simpleDialog.alert("Order results have been received.", VOXServiceName).then(afterPopupFunction);
                }
                else {
                    simpleDialog.alert("Service order is pending. The vendor has received your order.", VOXServiceName).then(afterPopupFunction);
                }
            }
            else {
                var errorArray = $.map(errors, function (value, index) {
                    return value.Error;
                });

                simpleDialog.multilineAlert(errorArray, VOXServiceName).then(function () {
                    if (shouldPushOutErrors) {
                        addOrdersToTable(errors, null);
                    }
                    LQBPopup.Return(null);
                });
            }
        }
    });

    function _bindCredentialOptions(orderingDiv, usesAccountId, requiresAccountId, credentialId, serviceCredentialHasAccountId) {
        orderingDiv.find('.CredentialItem').show();
        orderingDiv.find('.UsernameRow, .PasswordRow').toggle(credentialId === null);
        orderingDiv.find('.Username, .Password').toggleClass('RequiredInput', credentialId === null);
        orderingDiv.find('.UsernameRequiredSpan, .PasswordRequiredSpan').toggle(credentialId === null);
        orderingDiv.find('.AccountIdRow').toggle(usesAccountId && !serviceCredentialHasAccountId);
        
        var accountIdRowHidden = !orderingDiv.find('.AccountIdRow').is(':visible');
        orderingDiv.find('.AccountIdRequiredSpan').toggle(!accountIdRowHidden && requiresAccountId && orderingDiv.find('.AccountId').val() === '');
        orderingDiv.find('.AccountId').toggleClass('RequiredInput', !accountIdRowHidden && requiresAccountId);

        var hideCredentialsRow = credentialId !== null && (serviceCredentialHasAccountId || !usesAccountId);
        orderingDiv.find('.CredentialsRow').toggle(!hideCredentialsRow);
    }

    function _initializeCommonFunctions(data) {
        if (typeof (data['GatherRequestData']) === 'function') {
            GatherRequestData = data['GatherRequestData'];
        }

        if (typeof (data['ServiceType']) === 'string') {
            serviceType = data['ServiceType'];
        }

        if (typeof (data['ToggleOrderBtn']) === 'function') {
            ToggleOrderBtn = data['ToggleOrderBtn'];
        }

        validStatusesAfterInitialRequest = null;
        if (typeof (data['ValidStatusesAfterInitialRequest']) === 'object') {
            validStatusesAfterInitialRequest = data['ValidStatusesAfterInitialRequest'];
        }

        shouldPushOutErrors = false;
        if (typeof (data['ShouldPushOutErrors']) === 'boolean') {
            shouldPushOutErrors = data['ShouldPushOutErrors'];
        }

        if(typeof (data['VOXServiceName'] === 'string')) {
            VOXServiceName = data['VOXServiceName'];
        }

        voxSection = null;
        if (typeof (data['VOXSection']=== 'object')) {
            voxSection = data['VOXSection'];
        }
    }

    return {
        InitializeCommonFunctions: _initializeCommonFunctions,
        BindCredentialOptions: _bindCredentialOptions
    };

})();

var AuthorizationDocPicker = (function () {
    var docPickerTemplate;
    var docChangeCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/webapp/Partials/VOX/AuthorizationDocPicker.html", function (data) {
            docPickerTemplate = $.template(data);
        });

        $('body').on('click', '.UploadDocLink', function () {
            var docPickerRow = $(this).closest('tr');
            var appId = docPickerRow.find('.AppId').val();
            var url = ML.VirtualRoot + '/main/EDocs/UploadVoxDocument.aspx?loanid=' + ML.sLId + '&appid=' + encodeURIComponent(appId);
            
            IframePopup.show(url, {}).then(function(pair) {
                var error = pair[0];
                var result = pair[1];
                if (error != null) {
                    simpleDialog.alert(typeof error == "string" ? error: JSON.stringify(error), "Error");
                }
                else {
                    if (result) {
                        var document = {};
                        document.DocumentId = result.docId;
                        document.Description = result.description;

                        UpdatePickerUI(docPickerRow, document);
                    }
                }
            });
        });

        $('body').on('click', '.SelectDocLink', function () {
            var docPickerRow = $(this).closest('tr');
            var url = ML.VirtualRoot + '/main/EDocs/EDocPicker.aspx?single=t&loanid=' + ML.sLId;
            var args = {
                docs: $(this).closest('.DocPickerRow').find('.DocId').val()
            };

            IframePopup.show(url, { args: args }).then(function(pair) {
                var error = pair[0];
                var result = pair[1];
                if (error != null) {
                    simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "Error");
                }
                else {
                    if(result) {
                        var document = null;

                        if (result.docIds.length > 1) {
                            simpleDialog.alert("Please select only one document.", "Error");
                        }
                        else if (result.docIds.length == 1) {
                            document = {
                                'Description': result.docDescriptions[result.docIds[0]],
                                'DocumentId': result.docIds[0],
                            };
                        }

                        UpdatePickerUI(docPickerRow, document);
                    }
                }
            });
        });

        $('body').on('click', '.ClearDocLink', function () {
            var docPickerRow = $(this).closest('tr');
            UpdatePickerUI(docPickerRow, null);
        });

        $('body').on('click', '.ViewDocLink', function () {
            var docId = $(this).closest('tr').find('.DocId').val();
            window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=" +docId, '_self');
        });

        function UpdatePickerUI(pickerRow, document) {
            if (typeof (document) == 'undefined' || document == null) {
                $(pickerRow).find('.DocId').val('');
                $(pickerRow).find('.DocDescription').text('');
                $(pickerRow).find('.DocUploaded').hide();
                $(pickerRow).find('.NoDoc').show();
            }
            else {
                var docId = document.DocumentId;
                var docDescription = document.Description;
                $(pickerRow).find('.DocId').val(docId);
                $(pickerRow).find('.DocDescription').text(docDescription);
                $(pickerRow).find('.DocUploaded').show();
                $(pickerRow).find('.NoDoc').hide();
            }

            docChangeCallback();
        }
    });

    function _createDocPicker(borrowerName, appId, borrowerType, key) {
        var data = {
            BorrowerName: borrowerName,
            AppId: appId,
            BorrowerType: borrowerType,
            Key: key
        };

        var newPicker = $.tmpl(docPickerTemplate, data);
        TPOStyleUnification.Components($(newPicker));
        return newPicker;
    }

    function _removeDocPicker(key, container) {
        if (key === null || key === '') {
            $(container).find('.DocPickerRow').remove();
        }
        else {
            $(container).find('.Key[value="' + key + '"]').closest('tr').remove();
        }
    }

    function _containsDocPicker(key, container) {
        if (key === null || key === '') {
            return $(container).find('.DocPickerRow').length > 0;
        }
        else {
            return $(container).find('.Key[value="' + key + '"]').length > 0;
        }
    }

    function _retrieveAllDocPickerInfo(container) {
        var info = [];
        $(container).find('.DocPickerRow').each(function () {
            var docId = $(this).find('.DocId').val();
            var appId = $(this).find('.AppId').val();
            var borrowerType = $(this).find('.BorrowerType').val();
            info.push(docId + '|' + appId + '|' + borrowerType);
        });

        return info;
    }

    function _allPickersHaveDocs(container) {
        return container.find('.DocPickerRow .DocId').filter(function () { return $(this).val() == ''; }).length === 0;
    }

    function _registerDocChangeCallback(callback) {
        docChangeCallback = callback;
    }

    return {
        CreateDocPicker: _createDocPicker,
        RemoveDocPicker: _removeDocPicker,
        ContainsDocPicker: _containsDocPicker,
        RetrieveAllDocPickerInfo: _retrieveAllDocPickerInfo,
        AllPickersHaveDocs: _allPickersHaveDocs,
        RegisterDocChangeCallback: _registerDocChangeCallback
    };
})();

var AuditPage = (function () {
    var auditPageTemplate;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/webapp/Partials/VOX/AuditPage.html", function (data) {
            auditPageTemplate = $.template(data);
        });

        $('body').on('click', '.VoxAuditPage #AuditCancelBtn', function () {
            LQBPopup.Return({ OK: false });
        });

        $('body').on('click', '.VoxAuditPage #AuditNextBtn', function () {
            LQBPopup.Return({ OK: true });
        });

        $('body').on('click', '.VoxAuditPage .FieldUrl', function () {
            document.location = gVirtualRoot + $(this).data('url');
        });
    });

    function _createAuditPage(data) {
        return $.tmpl(auditPageTemplate, data);
    }

    return {
        CreateAuditPage: _createAuditPage
    };
})();
