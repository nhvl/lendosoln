﻿var WorkflowPageDisplay = (function ($) {
    const inputTypesToDisable = ['input', 'select', 'textarea', 'button[data-path-name]'];

    const defaultSettings = {
        getSelectorForBasicElement: _getClassNameFromNodeName,
        getSelectorForSelectionElement: _getClassNameFromNodeName,
        getSelectorForSubsetElement: _getClassNameFromNodeName,
        getSelectorForCollectionElement: _getClassNameFromNodeName,
        getSelectorsForDropdownOptions: _getSelectorsForDropdownOptions,
        requiredFieldIndicator: null,
        getElementForRequiredFieldIndicator: _getElementForRequiredFieldIndicator,
        protectFieldCallback: null,
        writeFieldCallback: null,
        postSetDisplayCallback: null
    }

    var pageSettings = null;

    function _setDisplay(settings) {
        if (typeof WorkflowPageDisplayModel === 'undefined') {
            return;
        }

        pageSettings = $.extend(defaultSettings, settings || {});

        _setFieldProtectRules(WorkflowPageDisplayModel.FieldProtectRules);
        _setWriteFieldRules(WorkflowPageDisplayModel.HasFullWriteAccess, WorkflowPageDisplayModel.WriteFieldRules);

        if (typeof pageSettings.postSetDisplayCallback === 'function') {
            pageSettings.postSetDisplayCallback();
        }
    }

    function _setFieldProtectRules(fieldProtectRules) {
        for (var i = 0; i < fieldProtectRules.length; ++i) {
            var fieldProtectRule = fieldProtectRules[i];

            if (typeof fieldProtectRule.Id === 'string') {
                _setFieldProtectRuleById(fieldProtectRule);
            }
            else {
                _setFieldProtectRuleByPath(fieldProtectRule);
            }
        }

        if (typeof pageSettings.protectFieldCallback === 'function') {
            pageSettings.protectFieldCallback();
        }
    }

    function _setFieldProtectRuleById(fieldProtectRule) {
        var $protectedField = $('[id$="' + fieldProtectRule.Id + '"]');
        _setFieldProtectRuleDisplay(fieldProtectRule, $protectedField);
    }

    function _setFieldProtectRuleByPath(fieldProtectRule) {
        var selector = _getSelectorByPath(fieldProtectRule.Paths);
        var $protectedField = $(selector);
        _setFieldProtectRuleDisplay(fieldProtectRule, $protectedField);
    }

    function _setFieldProtectRuleDisplay(fieldProtectRule, $protectedField) {
        if ($protectedField.length === 0) {
            // This field isn't present or isn't displayed on this page.
            return;
        }

        var $parentLabel = $protectedField.parent('label');
        $parentLabel.toggleClass('disabled', !fieldProtectRule.CanUpdate);

        if (!fieldProtectRule.CanUpdate) {
            // Only set the disabled property when the field cannot be updated
            // so we don't enable a field that was disabled by separate logic
            // on a page. For hyperlinks, we need to set the attribute since
            // links don't have a "disabled" property. For tables (such as with
            // subsets), we need to disable the elements within the table.
            if ($protectedField.is('a')) {
                $protectedField.attr('disabled', 'disabled').prop('title', fieldProtectRule.Title);
            }
            else if ($protectedField.is('table, tr, td, div')) {
                $protectedField.find('input, select, textarea, button[data-path-name]').prop('disabled', true).prop('title', fieldProtectRule.Title);
                $protectedField.find('a[data-path-name]').attr('disabled', 'disabled').prop('title', fieldProtectRule.Title);
            }
            else {
                $protectedField.prop('disabled', true).prop('title', fieldProtectRule.Title);
            }

            $parentLabel.prop('title', fieldProtectRule.Title);
        }
        else if (fieldProtectRule.NotSettableValues.length > 0) {
            var optionsToDisable = pageSettings.getSelectorsForDropdownOptions(fieldProtectRule.Id, $protectedField, fieldProtectRule.NotSettableValues)
            if (optionsToDisable.length > 0) {
                $protectedField.find(optionsToDisable.join(',')).prop('disabled', true).prop('title', fieldProtectRule.Title);
            }
        }
        else {
            if (!pageSettings.requiredFieldIndicator) {
                throw 'Required field indicator must be supplied for protect field rules.';
            }

            // Protected fields that can be updated cannot be cleared.
            if ($protectedField.is('input[type="text"], textarea')) {
                var $element = pageSettings.getElementForRequiredFieldIndicator($protectedField);
                _appendRequiredFieldIndicator($element, pageSettings.requiredFieldIndicator);
            }
            else if ($protectedField.is('table, tr, td, div')) {
                $protectedField.find('input[type="text"], textarea').each(function (index, element) {
                    var $element = pageSettings.getElementForRequiredFieldIndicator($j(element));
                    _appendRequiredFieldIndicator($element, pageSettings.requiredFieldIndicator);
                });
            }
        }
    }

    function _setWriteFieldRules(hasFullWriteAccess, writeFieldRules) {
        $('.write-rule').removeClass('write-rule');

        if (hasFullWriteAccess) {
            return;
        }

        for (var i = 0; i < writeFieldRules.length; ++i) {
            var writeFieldRule = writeFieldRules[i];

            var $writeableField;
            if (typeof writeFieldRule.Id === 'string') {
                $writeableField = $('[id$="' + writeFieldRule.Id + '"]');
            }
            else {
                var selector = _getSelectorByPath(writeFieldRule.Paths);
                $writeableField = $(selector);
            }

            _setWriteFieldDisplay(writeFieldRule, $writeableField);

            if ($writeableField.is('table, tr, td, div')) {
                $writeableField.find('a[data-path-name],' + inputTypesToDisable.join(',')).addClass('write-rule');
            }
            else {
                $writeableField.addClass('write-rule');
            }
        }

        var toDisable = inputTypesToDisable.map(function (el) { return el + ':not(.write-rule)'; }).join(',');
        $(toDisable).prop('disabled', true).parent('label').toggleClass('disabled', true);

        $('a[data-path-name]:not(.write-rule)').attr('disabled', true);

        if (typeof pageSettings.writeFieldCallback === 'function') {
            pageSettings.writeFieldCallback();
        }
    }

    function _setWriteFieldDisplay(writeFieldRule, $writableField) {
        if ($writableField.length === 0 ) {
            // This field isn't present or isn't displayed on this page.
            return;
        }

        if (!writeFieldRule.CanClear) {
            if (!pageSettings.requiredFieldIndicator) {
                throw 'Required field indicator must be supplied when write field rules indicate a field cannot be cleared.';
            }

            if ($writableField.is('input[type="text"], textarea')) {
                var $element = pageSettings.getElementForRequiredFieldIndicator($writableField);
                _appendRequiredFieldIndicator($element, pageSettings.requiredFieldIndicator);
            }
            else if ($writableField.is('table, tr, td, div')) {
                $writableField.find('input[type="text"], textarea').each(function (index, element) {
                    var $element = pageSettings.getElementForRequiredFieldIndicator($writableField);
                    _appendRequiredFieldIndicator($element, pageSettings.requiredFieldIndicator);
                });
            }
        }

        if (writeFieldRule.AcceptedValues.length > 0) {
            var allowedOptions = pageSettings.getSelectorsForDropdownOptions(writeFieldRule.Id, $writableField, writeFieldRule.AcceptedValues)
            if (allowedOptions.length > 0) {
                $writableField.find('option:not(' + allowedOptions.join(',') + ')').prop('disabled', true).prop('title', writeFieldRule.Title);
            }
        }
    }

    function _getSelectorByPath(path) {
        var selector = '';
        for (var i = 0; i < path.length; ++i) {
            var currentPath = path[i];
            if (currentPath.Name === WorkflowPageDisplayConstants.WholeCollectionMarker) {
                // Selector will target the entire group.
                continue;
            }

            selector += _getSelectorFromPathNode(currentPath);
        }

        return selector;
    }

    function _getSelectorFromPathNode(currentPath) {
        if (currentPath.Name.lastIndexOf(WorkflowPageDisplayConstants.CollectionSubsetMarker, 0) === 0) {
            return pageSettings.getSelectorForSubsetElement(currentPath.Name.substring(WorkflowPageDisplayConstants.CollectionSubsetMarker.length));
        }

        switch (currentPath.Type) {
            case WorkflowPageDisplayConstants.PathTypes.DataPathBasicElement:
                return pageSettings.getSelectorForBasicElement(currentPath.Name);
            case WorkflowPageDisplayConstants.PathTypes.DataPathSelectionElement:
                return pageSettings.getSelectorForSelectionElement(currentPath.Name);
            case WorkflowPageDisplayConstants.PathTypes.DataPathCollectionElement:
                return pageSettings.getSelectorForCollectionElement(currentPath.Name);
            default:
                throw new Error('Unhandled type ' + currentPath.Type);
        }
    }

    function _getClassNameFromNodeName(name) {
        return ' .' + name;
    }

    function _getSelectorsForDropdownOptions(ruleId, $field, values) {
        return values.map(function (value) {
            return 'option[value="' + value + '"]';
        });
    }

    function _getElementForRequiredFieldIndicator($writeableField) {
        return $writeableField;
    }

    function _appendRequiredFieldIndicator($element, $indicator) {
        if ($element.find($indicator).length === 0) {
            $element.after($indicator);
        }
    }

    function _updateRulesFromModel(model) {
        if (typeof model === 'undefined' || typeof WorkflowPageDisplayModel === 'undefined') {
            return;
        }

        WorkflowPageDisplayModel = Object.assign(WorkflowPageDisplayModel, model);
    }

    return {
        SetDisplay: _setDisplay,
        UpdateRulesFromModel: _updateRulesFromModel
    }
})(jQuery);
