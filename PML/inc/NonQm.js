﻿var RateOption = "";
var Program = "";
var loanId = "";
var resultsScope;
var IsPricing2nd = false;
var containsPricingResults = false;
var isPricingResultsValid = true;
var isIneligible = false;
var oldValue;

$(document).ready(function () {	
    initializeAngular(true);

    var dragDropSettings = {
        isPmlParam: true,
        showBarcodes: false
    };

    registerDragDropUpload(document.getElementById('DragDropZone_BankStatement'), dragDropSettings);
	registerDragDropUpload(document.getElementById('DragDropZone_ScenarioRequest'), dragDropSettings);

	if (!ML.IsQuickPricer) {
		simpleDialog.confirm("Do you want to price out the scenario first?", "", "Yes", "No", null).then(function (isConfirmed) {
			if (isConfirmed === null) { // cancel
				return;
			}
			else if (isConfirmed) {
				$("#" + ML.NonQmQuickPricerLink, parent.document).addClass("selected"); 
				$("#" + ML.BankStatementIncomeCalculatorLink, parent.document).removeClass("selected"); 
				$("#" + ML.ScenarioRequestFormLink, parent.document).removeClass("selected"); 

				$("#QuickPricerTable").show();
				$("#BankStatementIncomeCalculatorTable").hide();
				$("#ScenarioRequestFormTable").hide();
				return;
			}
			else { // Discard & Proceed
				$(".submitButton").prop("disabled", true);
				return;
			}
		});
	}

	$("#NoResultsMsg").hide();
	$(".selectedRateRow").hide();

	if (window.location.href.indexOf('QuickPricer') > -1) {
		$("#" + ML.NonQmQuickPricerLink, parent.document).addClass("selected"); 
		$("#" + ML.BankStatementIncomeCalculatorLink, parent.document).removeClass("selected"); 
		$("#" + ML.ScenarioRequestFormLink, parent.document).removeClass("selected"); 
	}

	$("#QuickPricerTable").toggle(ML.IsQuickPricer);
	$("#BankStatementIncomeCalculatorTable").toggle(ML.IsBankStatementIncomeCalculator);
	$("#ScenarioRequestFormTable").toggle(ML.IsScenarioRequestForm);

	validateQpInput();
	validateScenarioRequestFormInput();
});

function onPriceClick() {
	$('.price-button').prop('disabled', true);
	$('.mask-when-pricing').toggleClass('mask', true);
	$("#ResultsContainer").toggleClass("mask-results", false);
	$("#ResultsContainer").hide();
	$("#NoResultsMsg").hide();

	var loading = $('<loading-icon id="LQBPopupUWaitTable" class="loading-popup">' + "Please wait ..." + '</loading-icon>');
	TPOStyleUnification.UnifyLoadingIcons(loading);

	LQBPopup.ShowElement(loading, {
		width: 350,
		height: 200,
		hideCloseButton: true
	});

	var args = {
		sLAmtCalcPe: $("#QuickPricer_sLAmtCalcPe").val(),
		sLtvRPe: $("#QuickPricer_sLtvRPe").val(),
		sLPurposeT: $("#QuickPricer_sLPurposeT").val(),
		sOccTPe: $("#QuickPricer_sOccTPe").val(),
		sProdSpT: $("#QuickPricer_sProdSpT").val(),
		sSpStatePe: $("#QuickPricer_sSpStatePe").val(),
		sCreditScoreEstimatePe: $("#QuickPricer_sCreditScoreEstimatePe").val(),
		sManuallyEnteredHousingHistory: $("#QuickPricer_sManuallyEnteredHousingHistory").val(),
		sManuallyEnteredHousingEvents: $("#QuickPricer_sManuallyEnteredHousingEvents").val(),
		sManuallyEnteredBankruptcy: $("#QuickPricer_sManuallyEnteredBankruptcy").val(),
		aProdBCitizenT: $("#QuickPricer_aProdBCitizenT").val(),
		sProdDocT: $("#QuickPricer_sProdDocT").val()
	}

	gService.NonQmService.callAsyncSimple("RunPricing", args, function (result) {
		if (!!result.error || typeof (result.value["ErrorMessage"]) != "undefined") {
			$("#ResultsContainer").show();
			displayErrorNonQm(result);
			$('.price-button').prop('disabled', false);
			$('.mask-when-pricing').toggleClass('mask', false);
			LQBPopup.Return();
			return;
		}

		var pricingResults = result.value["PricingResults"];

		var pricingSettings = {
			showQm: false,
			IsDebugColumns: false,
			IsBreakEvenMonths: false,
			isCheckEligibility: false,
			sProdFilterDisplayrateMerge: false,
			GetResultsUsing: "0", // Current pricing
			HasArmFilter: false,
			IsInvestorPricing: false,
			IsPml: false,
			ShowPrice: result.value["ShowPrice"] == "True",
			IsNonQm: true,
			IsDocTypeBankStatement: (13 <= $("#QuickPricer_sProdDocT").val() && $("#QuickPricer_sProdDocT").val() <= 17)
		};

		updateResults(pricingSettings, null, pricingResults);
		loanId = result.value["LoanId"];				

		if (result.value["HasNoResults"] == "True") {
			$("#NoResultsMsg").show();
		}
		else {
			$("#ResultsContainer").show();
		}				

		$('.price-button').prop('disabled', false);
		$('.mask-when-pricing').toggleClass('mask', false);
		LQBPopup.Return();
	});
}

function noResultSubmitScenarioClick() {
	$("#" + ML.NonQmQuickPricerLink, parent.document).removeClass("selected");
	$("#QuickPricerTable").hide();
	$("#" + ML.ScenarioRequestFormLink, parent.document).addClass("selected");
	$("#ScenarioRequestFormTable").show();

	$("#ScenarioRequestForm_sLAmtCalcPe").val($("#QuickPricer_sLAmtCalcPe").val());
	$("#ScenarioRequestForm_sLtvRPe").val($("#QuickPricer_sLtvRPe").val());
	$("#ScenarioRequestForm_sLPurposeT").val($("#QuickPricer_sLPurposeT").val());
	$("#ScenarioRequestForm_sOccTPe").val($("#QuickPricer_sOccTPe").val());
	$("#ScenarioRequestForm_sProdSpT").val($("#QuickPricer_sProdSpT").val());
	$("#ScenarioRequestForm_sSpStatePe").val($("#QuickPricer_sSpStatePe").val());
	$("#ScenarioRequestForm_sCreditScoreEstimatePe").val($("#QuickPricer_sCreditScoreEstimatePe").val());
	$("#ScenarioRequestForm_sManuallyEnteredHousingHistory").val($("#QuickPricer_sManuallyEnteredHousingHistory").val());
	$("#ScenarioRequestForm_sManuallyEnteredHousingEvents").val($("#QuickPricer_sManuallyEnteredHousingEvents").val());
	$("#ScenarioRequestForm_sManuallyEnteredBankruptcy").val($("#QuickPricer_sManuallyEnteredBankruptcy").val());
	$("#ScenarioRequestForm_aProdBCitizenT").val($("#QuickPricer_aProdBCitizenT").val());
	$("#ScenarioRequestForm_sProdDocT").val($("#QuickPricer_sProdDocT").val());

	containsPricingResults = false;
}

function storeOldValue(id) {
	oldValue = $("#" + id).val();
}

function invalidatePricingResult(id) {	
	if (containsPricingResults && isPricingResultsValid) {
		simpleDialog.confirm("The selected product and rate option will no longer be valid if data changes.", "", "OK", "Cancel", null).then(function (isConfirmed) {
			if (isConfirmed) {
				isPricingResultsValid = false;
				$('.mask-when-pricing').toggleClass('mask', true);
				$(".selectedRateRow").hide();
			}
			else {
				$("#" + id).val(oldValue);
			}
		});
	}
}

function invalidatePricing() {
	if ($("#ResultsContainer").is(":visible")) {
		$('.mask-when-pricing').toggleClass('mask', true);
	}
}

function displayErrorNonQm(result) {
	if (result.error) {
		simpleDialog.alert(result.UserMessage);
	}
	else if (typeof (result.value.ErrorMessage) != "undefined") {
		simpleDialog.alert(result.value.ErrorMessage);
	}
}

function validateQpInput() {
	$(".price-button").prop("disabled",
		$("#QuickPricer_sLAmtCalcPe").val().trim() == "" ||
		$("#QuickPricer_sLtvRPe").val().trim() == "" ||
		$("#QuickPricer_sSpStatePe").val().trim() == "" ||
		$("#QuickPricer_sCreditScoreEstimatePe").val().trim() == "" || 
		!$.isNumeric($("#QuickPricer_sCreditScoreEstimatePe").val().trim()));
}

function validateScenarioRequestFormInput() {
	$(".submitButton").prop("disabled",
		$("#ScenarioRequestForm_sLAmtCalcPe").val().trim() == "" ||
		$("#ScenarioRequestForm_sLtvRPe").val().trim() == "" ||
		$("#ScenarioRequestForm_sSpStatePe").val().trim() == "" ||
		$("#ScenarioRequestForm_sCreditScoreEstimatePe").val().trim() == "" ||
		$("#ScenarioRequestForm_aBFirstNm").val().trim() == "" ||
		$("#ScenarioRequestForm_aBLastNm").val().trim() == "" ||
		!$.isNumeric($("#ScenarioRequestForm_sCreditScoreEstimatePe").val().trim()));
}

function validateBankStatementInput() {
	$(".submitButton").prop("disabled",
		$("#BankStatementIncomeCalculator_aBFirstNm").val().trim() == "" ||
		$("#BankStatementIncomeCalculator_aBLastNm").val().trim() == "");
}

function onSubmit(isBankStatement) {
	var loading = $('<loading-icon id="LQBPopupUWaitTable" class="loading-popup">' + "Please wait ..." + '</loading-icon>');
	TPOStyleUnification.UnifyLoadingIcons(loading);

	LQBPopup.ShowElement(loading, {
		width: 350,
		height: 200,
		hideCloseButton: true
	});

	var args;

	if (isBankStatement) {
		args = {
			ContainsPricingResults: containsPricingResults,
			IsPricingResultsValid: isPricingResultsValid,
			IsBankStatement: true,
			IsIneligible: isIneligible,
			aBFirstNm: $("#BankStatementIncomeCalculator_aBFirstNm").val(),
			aBLastNm: $("#BankStatementIncomeCalculator_aBLastNm").val(),
			sIsSelfEmployed: $("#BankStatementIncomeCalculator_sIsSelfEmployed").val() === "1",
			sLAmtCalcPe: $("#QuickPricer_sLAmtCalcPe").val(),
			sLtvRPe: $("#QuickPricer_sLtvRPe").val(),
			sLPurposeT: $("#QuickPricer_sLPurposeT").val(),
			sOccTPe: $("#QuickPricer_sOccTPe").val(),
			sProdSpT: $("#QuickPricer_sProdSpT").val(),
			sSpStatePe: $("#QuickPricer_sSpStatePe").val(),
			sCreditScoreEstimatePe: $("#QuickPricer_sCreditScoreEstimatePe").val(),
			sManuallyEnteredHousingHistory: $("#QuickPricer_sManuallyEnteredHousingHistory").val(),
			sManuallyEnteredHousingEvents: $("#QuickPricer_sManuallyEnteredHousingEvents").val(),
			sManuallyEnteredBankruptcy: $("#QuickPricer_sManuallyEnteredBankruptcy").val(),
			aProdBCitizenT: $("#QuickPricer_aProdBCitizenT").val(),
			sProdDocT: $("#QuickPricer_sProdDocT").val(),
			MsgToLender: $("#BankStatementIncomeCalculator_MsgToLender").val(),
			LoanId: loanId,
			lLpTemplateId: Program.lLpTemplateId,
			Rate: RateOption.Rate,
			Point: RateOption.Point,
			RateOptionId: RateOption.RateOptionId
		}
	}
	else {
		args = {
			ContainsPricingResults: containsPricingResults,
			IsPricingResultsValid: isPricingResultsValid,
			IsBankStatement: false,
			IsIneligible: isIneligible,
			aBFirstNm: $("#ScenarioRequestForm_aBFirstNm").val(),
			aBLastNm: $("#ScenarioRequestForm_aBLastNm").val(),
			sLAmtCalcPe: $("#ScenarioRequestForm_sLAmtCalcPe").val(),
			sLtvRPe: $("#ScenarioRequestForm_sLtvRPe").val(),
			sLPurposeT: $("#ScenarioRequestForm_sLPurposeT").val(),
			sOccTPe: $("#ScenarioRequestForm_sOccTPe").val(),
			sProdSpT: $("#ScenarioRequestForm_sProdSpT").val(),
			sSpStatePe: $("#ScenarioRequestForm_sSpStatePe").val(),
			sCreditScoreEstimatePe: $("#ScenarioRequestForm_sCreditScoreEstimatePe").val(),
			sManuallyEnteredHousingHistory: $("#ScenarioRequestForm_sManuallyEnteredHousingHistory").val(),
			sManuallyEnteredHousingEvents: $("#ScenarioRequestForm_sManuallyEnteredHousingEvents").val(),
			sManuallyEnteredBankruptcy: $("#ScenarioRequestForm_sManuallyEnteredBankruptcy").val(),
			aProdBCitizenT: $("#ScenarioRequestForm_aProdBCitizenT").val(),
			sProdDocT: $("#ScenarioRequestForm_sProdDocT").val(),
			MsgToLender: $("#ScenarioRequestForm_MsgToLender").val(),
			LoanId: loanId,
			lLpTemplateId: Program.lLpTemplateId,
			Rate: RateOption.Rate,
			Point: RateOption.Point,
			RateOptionId: RateOption.RateOptionId
		}
	}

	gService.NonQmService.callAsyncSimple("Submit", args, function (result) {
		if (!!result.error || result.value["sLNm"] == "undefined") {
			setTimeout(function () {
				LQBPopup.Return();
				displayErrorNonQm(result);
			}, 300);

			return;
		}

		var leadName = result.value["sLNm"];
		var leadId = result.value["sLId"];

		var errors = "";

		if (!!result.value["ErrorMessage"]) {
			errors += result.value["ErrorMessage"] + "<br/>";
		}

		if (!!result.value["RegisterErrorMessage"]) {
			errors += result.value["RegisterErrorMessage"] + "<br/>";
		}

		if (!!result.value["ConversationLogErrorMessage"]) {
			errors += result.value["ConversationLogErrorMessage"] + "<br/>";
		}

		if (!!result.value["EmailErrorMessage"]) {
			errors += result.value["EmailErrorMessage"] + "<br/>";
		}

		var controlSuffix = (isBankStatement ? 'BankStatement' : 'ScenarioRequest');
		var fileCount = getFilesCount('DragDropZone_' + controlSuffix);

		if (fileCount == 0) {
			LQBPopup.Return();
			notifyUserThenRedirect(errors, leadName);
			return;
		}

		if (fileCount != 0) {
			var filesRead = 0;

			applyCallbackBase64Files('DragDropZone_' + controlSuffix, function (file, base64Content) {
				args = {
					Base64Content: base64Content,
					FileName: file.name,
					LeadId: leadId
				};

				gService.NonQmService.callAsyncSimple("UploadDoc", args, function (result) {
					if (!result.error) {
						if (!!result.value["ErrorMessage"]) {
							errors += result.value["ErrorMessage"] + "<br/>";
						}

						if (!!result.value["EDocErrorMessage"]) {
							errors += result.value["EDocErrorMessage"] + "<br/>";
						}
					}
					else {
						errors += "Error while uploading file " + file.name + "<br/>";
					}

					filesRead++;

					if (fileCount == filesRead) {
						LQBPopup.Return();
						notifyUserThenRedirect(errors, leadName);
					}
				});
			});
		}
	});
}

function notifyUserThenRedirect(errors, leadName) {
	removeOverlay();
	if (errors != "") {
		simpleDialog.alert("Lead '" + leadName + "' has been created, even with the error(s): <br/>" + errors).then(function () {
			parent.window.location.href = "../main/baseframe.aspx?isLoadingDashboard=true";
		});
	}
	else {
		simpleDialog.alert("Lead '" + leadName + "' has been created.").then(function () {
			parent.window.location.href = "../main/baseframe.aspx?isLoadingDashboard=true";
		});
	}
}

function prepareForPdfUpload(isBankStatement, callback) {
    var base64Files = {};
    var controlSuffix = (isBankStatement ? 'BankStatement' : 'ScenarioRequest');
    var fileCount = getFilesCount('DragDropZone_' + controlSuffix);
    var filesRead = 0;

    if (fileCount == 0) {
        callback(isBankStatement, base64Files);
    }
    else {
        applyCallbackBase64Files('DragDropZone_' + controlSuffix, function (file, base64Content) {
            base64Files[file.name] = base64Content;
            filesRead++;

            if (fileCount == filesRead) {
                callback(isBankStatement, base64Files);
            }
        });
    }
}

function returnToQp(isBankStatement) {
	$("#" + ML.NonQmQuickPricerLink, parent.document).addClass("selected");
	$("#QuickPricerTable").show();

	if (isBankStatement) {
		$("#" + ML.BankStatementIncomeCalculatorLink, parent.document).removeClass("selected");
		$("#BankStatementIncomeCalculatorTable").hide();
	}
	else {
		$("#" + ML.ScenarioRequestFormLink, parent.document).removeClass("selected");
		$("#ScenarioRequestFormTable").hide();
		$("#QuickPricer_sLAmtCalcPe").val($("#ScenarioRequestForm_sLAmtCalcPe").val());
		$("#QuickPricer_sLtvRPe").val($("#ScenarioRequestForm_sLtvRPe").val());
		$("#QuickPricer_sLPurposeT").val($("#ScenarioRequestForm_sLPurposeT").val());
		$("#QuickPricer_sOccTPe").val($("#ScenarioRequestForm_sOccTPe").val());
		$("#QuickPricer_sProdSpT").val($("#ScenarioRequestForm_sProdSpT").val());
		$("#QuickPricer_sSpStatePe").val($("#ScenarioRequestForm_sSpStatePe").val());
		$("#QuickPricer_sCreditScoreEstimatePe").val($("#ScenarioRequestForm_sCreditScoreEstimatePe").val());
		$("#QuickPricer_sManuallyEnteredHousingHistory").val($("#ScenarioRequestForm_sManuallyEnteredHousingHistory").val());
		$("#QuickPricer_sManuallyEnteredHousingEvents").val($("#ScenarioRequestForm_sManuallyEnteredHousingEvents").val());
		$("#QuickPricer_sManuallyEnteredBankruptcy").val($("#ScenarioRequestForm_sManuallyEnteredBankruptcy").val());
		$("#QuickPricer_aProdBCitizenT").val($("#ScenarioRequestForm_aProdBCitizenT").val());
		$("#QuickPricer_sProdDocT").val($("#ScenarioRequestForm_sProdDocT").val());
	}
}