﻿var VOAOrdering = (function () {
    var _initialize;

    jQuery(function ($) {
        var voaSection = $("#secVoa");
        var authDocsEnabled = false;
        var isAssetPickerBeingUsed;
        var oldIsAssetPickerBeingUsed;

        voaSection.on('click', '.CheckAllAssetsCB', function () {
            if (this.checked) {
                var assetCBs = voaSection.find('.AssetsCB:not(:checked)');
                assetCBs.each(function (index, assetCBElement) {
                    $(assetCBElement).prop('checked', true);
                    CheckBorrowerAuthDocsForAssets(assetCBElement, false);
                });
            }
            else {
                var assetsCBs = voaSection.find('.AssetsCB:checked');
                assetsCBs.each(function (index, assetCBElement) {
                    $(assetCBElement).prop('checked', false);
                    CheckBorrowerAuthDocsForAssets(assetCBElement, false);
                });
            }

            ToggleOrderBtn();
        });

        voaSection.on('click', '.AssetsCB', function () {
            CheckBorrowerAuthDocsForAssets(this, false);
            voaSection.find('.CheckAllAssetsCB').prop('checked', voaSection.find('.AssetsCB:checked').length === voaSection.find('.AssetsCB').length);
            ToggleOrderBtn();
        });

        voaSection.on('change', '.SectionToggle', function () {
            TogglePickerSection(!isAssetPickerBeingUsed);
        });

        voaSection.on('change', '.BorrowerList', function () {
            CheckBorrowerAuthDocsForBorrowers(false);
            ToggleOrderBtn();
        });

        voaSection.on('click', '.AssetsLink', function () {
            document.location = gVirtualRoot + "/webapp/Loan1003.aspx?src=pipeline&p=1&loanid=" + ML.sLId + "#tabs-1";
        });

        voaSection.on('click', '.NotificationEmailPicker', function () {
            var url = ML.VirtualRoot + '/webapp/RolodexListV2.aspx?loanid=' + ML.sLId;

            IframePopup.show(url, {}).then(function (pair) {
                var error = pair[0];
                var result = pair[1];
                if (error != null) {
                    simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "Error");
                }
                else if (result) {
                    voaSection.find('.NotificationEmail').val(result.AgentEmail).change();
                }
            });
        });

        voaSection.on('change', '.LenderServices', function () {
            var accountHistoryDDL = voaSection.find('.AccountHistoryOptions');
            accountHistoryDDL.find('option').remove();
            var refreshPeriodDDL = voaSection.find('.RefreshPeriodOptions');
            refreshPeriodDDL.find('option').remove();

            var lenderServiceId = $(this).val();
            if (!lenderServiceId) {
                ToggleOrderBtn();
                AuthorizationDocPicker.RemoveDocPicker(null, voaSection.find('.DocumentTable'));
                voaSection.find('.BorrowerAuthDocsSection').hide();
                authDocsEnabled = false;
                return;
            }

            var data = {
                LenderServiceId: lenderServiceId,
                LoanId: ML.sLId,
            };

            gService.vod.callAsyncSimple("LoadLenderService", data,
                function (result) {
                    if (result.value["Success"].toLowerCase() === 'true') {
                        var showNotification = result.value["AskForNotificationEmail"].toLowerCase() == 'true';
                        var showCreditCard = result.value["AllowPaymentByCreditCard"].toLowerCase() == 'true';
                        var requiresAccountId = result.value["RequiresAccountId"].toLowerCase() == 'true';
                        var usesAccountId = result.value["UsesAccountId"].toLowerCase() == 'true';
                        var accountHistory = JSON.parse(result.value["AccountHistoryOptions"]);
                        var accountHistoryDefault = result.value["AccountHistoryDefault"];
                        var refreshPeriod = JSON.parse(result.value["RefreshPeriodOptions"]);
                        var refreshPeriodDefault = result.value["RefreshPeriodDefault"];
                        var verificationType = result.value["VoaVerificationT"];
                        var voaAllowRequestWithoutAssets = result.value["VoaAllowRequestWithoutAssets"].toLowerCase() == 'true';
                        var voaEnforceAllowRequestWithoutAssets = result.value["VoaEnforceAllowRequestWithoutAssets"].toLowerCase() == 'true';

                        TogglePickerSection(!voaAllowRequestWithoutAssets, !voaEnforceAllowRequestWithoutAssets);

                        voaSection.find('.CreditCardRow').toggle(showCreditCard);
                        BindNotificationEmail(showNotification);

                        authDocsEnabled = verificationType == '1';
                        if (!authDocsEnabled) {
                            AuthorizationDocPicker.RemoveDocPicker(null, voaSection.find('.DocumentTable'));
                            voaSection.find('.BorrowerAuthDocsSection').hide();
                        }
                        else {
                            if (isAssetPickerBeingUsed) {
                                var assetsCBs = voaSection.find('.AssetsCB:checked');
                                assetsCBs.each(function (index) {
                                    CheckBorrowerAuthDocsForAssets(this, false);
                                });
                            }
                            else {
                                CheckBorrowerAuthDocsForBorrowers(false);
                            }
                        }

                        BindLenderServiceOptions(accountHistory, accountHistoryDefault, refreshPeriod, refreshPeriodDefault);
                        var credentialId = typeof (result.value["ServiceCredentialId"]) === 'undefined' ? null : result.value["ServiceCredentialId"];
                        var serviceCredentialHasAccountId = typeof (result.value["ServiceCredentialHasAccountId"]) === 'undefined' ? false : result.value["ServiceCredentialHasAccountId"].toLowerCase() === 'true';
                        VOXOrderCommon.BindCredentialOptions(voaSection, usesAccountId, requiresAccountId, credentialId, serviceCredentialHasAccountId);
                    }
                    else {
                        simpleDialog.alert(result.value["Errors"], "VOA");
                    }

                    ToggleOrderBtn();
                },
                function (result) {
                    simpleDialog.alert(result.UserMessage, "VOA");
                },
                ML.VOXTimeoutInMilliseconds);
        });

        function TogglePickerSection(useAssetPicker, canSwitch) {
            voaSection.find('.PickerSection').hide();
            if (typeof (canSwitch) === 'boolean') {
                voaSection.find('.SectionToggleSection').prop('disabled', !canSwitch).toggle(canSwitch);

                if (canSwitch && typeof (isAssetPickerBeingUsed) === 'boolean') {
                    // If switching is allowed with this new lender service, let it use the same selection picker.
                    useAssetPicker = isAssetPickerBeingUsed;
                }
            }

            voaSection.find('.AssetPickerSection').toggle(useAssetPicker);
            voaSection.find('.AssetToggle').prop('checked', useAssetPicker);
            voaSection.find('.BorrowerPickerSection').toggle(!useAssetPicker);
            voaSection.find('.BorrowerToggle').prop('checked', !useAssetPicker);

            oldIsAssetPickerBeingUsed = typeof (isAssetPickerBeingUsed) === 'boolean' ? isAssetPickerBeingUsed : useAssetPicker;
            isAssetPickerBeingUsed = useAssetPicker;

            if (oldIsAssetPickerBeingUsed !== isAssetPickerBeingUsed) {
                voaSection.find('.AssetsCB').prop('checked', false);
                voaSection.find('.BorrowerList').prop('selectedIndex', 0);
                AuthorizationDocPicker.RemoveDocPicker(null, voaSection.find('.DocumentTable'));

                if (!isAssetPickerBeingUsed) {
                    CheckBorrowerAuthDocsForBorrowers(false);
                }
            }

            voaSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, voaSection.find('.DocumentTable')));
            ToggleOrderBtn();
        }

        function CheckBorrowerAuthDocsForBorrowers(shouldCheckOrderBtn) {
            if (!authDocsEnabled) {
                return;
            }
            var table = voaSection.find('.DocumentTable');

            var $borrowerDdl = voaSection.find('.BorrowerList');
            var key = $borrowerDdl.val();

            if (!key) {
                AuthorizationDocPicker.RemoveDocPicker(null, table);
            }
            else {
                var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                if (!containsPicker) {
                    AuthorizationDocPicker.RemoveDocPicker(null, table);
                    var name = $borrowerDdl.find('option:selected').text();
                    var split = key.split('_');
                    var appId = split[0];
                    var ownerType = split[1];

                    var picker = AuthorizationDocPicker.CreateDocPicker(name, appId, ownerType, key);
                    table.append(picker);
                }
            }

            if (shouldCheckOrderBtn) {
                ToggleOrderBtn();
            }

            voaSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, table));
        }

        function CheckBorrowerAuthDocsForAssets(assetCb, shouldCheckOrderBtn) {
            if (!authDocsEnabled) {
                return;
            }

            var assetRow = $(assetCb).closest('tr');
            var appId = assetRow.find('.AppId').val();
            var ownerT = assetRow.find('.OwnerT').val();
            var ownerName = assetRow.find('.Owner').text();
            var key = assetRow.find('.Key').val();
            var table = voaSection.find('.DocumentTable');

            if (assetCb.checked) {
                // This got checked so if the picker doesn't already exist, add it
                var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                if (!containsPicker) {
                    var picker = AuthorizationDocPicker.CreateDocPicker(ownerName, appId, ownerT, key);
                    table.append(picker);
                }
            }
            else {
                // Unchecked, need to see if there are any more that are checked.
                var anyMoreChecked = voaSection.find('.AssetsTable').find('.Key[value="' + key + '"]').siblings('.AssetsCB:checked').length > 0;
                if (!anyMoreChecked && AuthorizationDocPicker.ContainsDocPicker(key, table)) {
                    AuthorizationDocPicker.RemoveDocPicker(key, table);
                }
            }

            if (shouldCheckOrderBtn) {
                ToggleOrderBtn();
            }

            voaSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, table));
        }

        function BindNotificationEmail(askForNotificationEmail) {
            var notificationEmailRow = voaSection.find('.NotificationEmailRow');
            var notificationEmail = voaSection.find('.NotificationEmail');
            notificationEmailRow.toggle(askForNotificationEmail);
            notificationEmailRow.find('.RequiredSpan').toggle(askForNotificationEmail && notificationEmail.val() === '');
            notificationEmail.toggleClass('RequiredInput', askForNotificationEmail);
        }

        function BindLenderServiceOptions(accountHistory, accountHistoryDefault, refreshPeriod, refreshPeriodDefault) {
            var accountHistoryDDL = voaSection.find('.AccountHistoryOptions');
            var refreshPeriodDDL = voaSection.find('.RefreshPeriodOptions');

            PopulateOptions(accountHistoryDDL, accountHistory, accountHistoryDefault);
            PopulateOptions(refreshPeriodDDL, refreshPeriod, refreshPeriodDefault);
        }

        function PopulateOptions(ddl, optionList, defaultValue) {
            ddl.prop('disabled', optionList.length == 1 || optionList.length == 0);
            if (optionList.length === 0) {
                ddl.append($('<option>', { value: '' }).text("None"));
            }
            else {
                for (var i = 0; i < optionList.length; i++) {
                    var option = optionList[i];
                    var optionString = option + " Days";
                    if (option == ML.MinOptionValue) {
                        optionString = "None";
                    }
                    else if (option == ML.MaxOptionValue) {
                        optionString = "Max Available";
                    }

                    ddl.append($('<option>', { value: option }).text(optionString));
                }
            }

            if (defaultValue !== undefined) {
                ddl.val(defaultValue);
            }
        }

        function ToggleOrderBtn() {
            var docsOk = authDocsEnabled ? AuthorizationDocPicker.AllPickersHaveDocs(voaSection.find('.DocumentTable')) : true;
            var inputsOk = voaSection.find('.RequiredInput:visible').filter(function () { return $(this).val() === '' }).length === 0;
            var lenderService = voaSection.find('.LenderServices').val();
            var lenderServiceChosen = lenderService != null && lenderService !== '';
            var assetChosen = !isAssetPickerBeingUsed || voaSection.find('.AssetsCB:checked').length > 0;
            var accountHistoryOptionSelected = voaSection.find('.AccountHistoryOptions').val() !== '';
            var refreshPeriodOptionSelected = voaSection.find('.RefreshPeriodOptions').val() !== '';
            var borrowerChosen = isAssetPickerBeingUsed || voaSection.find('.BorrowerList').val() !== '';

            var canOrder = ML.CanOrderVoa && docsOk && inputsOk && lenderServiceChosen && assetChosen && borrowerChosen && accountHistoryOptionSelected && refreshPeriodOptionSelected;
            voaSection.find('.VOXPlaceOrderBtn').prop('disabled', !canOrder);
            if (!ML.CanOrderVoa) {
                voaSection.find('.VOXPlaceOrderBtn').prop('title', ML.OrderVoaDenialReason);
            }
        }

        function GatherRequestData() {
            var assetsInfo = [];
            voaSection.find('.AssetsTable tbody').find('tr').each(function () {
                var assetCbChecked = $(this).find('.AssetsCB').is(':checked');
                if (assetCbChecked) {
                    var assetId = $(this).find('.AssetId').val();
                    var appId = $(this).find('.AppId').val();
                    var borrowerType = $(this).find('.OwnerT').val();

                    var data = {
                        BorrowerType: borrowerType,
                        AppId: appId,
                        AssetId: assetId
                    };

                    assetsInfo.push(data);
                }
            });

            var data = {
                LoanId: ML.sLId,
                LenderService: voaSection.find('.LenderServices').val(),
                NotificationEmail: voaSection.find('.NotificationEmail').is(':visible') ? voaSection.find('.NotificationEmail').val() : '',
                AccountHistory: voaSection.find('.AccountHistoryOptions').val(),
                RefreshPeriod: voaSection.find('.RefreshPeriodOptions').val(),
                PayWithCreditCard: voaSection.find('.AllowPayWithCreditCard').is(':visible') ? voaSection.find('.AllowPayWithCreditCard').is(':checked') : '',
                AccountId: voaSection.find('.AccountId').is(':visible') ? voaSection.find('.AccountId').val().trim() : '',
                UserName: voaSection.find('.Username').is(':visible') ? voaSection.find('.Username').val().trim() : '',
                Password: voaSection.find('.Password').is(':visible') ? voaSection.find('.Password').val().trim() : '',
                DocPickerInfo: authDocsEnabled ? AuthorizationDocPicker.RetrieveAllDocPickerInfo(voaSection.find('.DocumentTable')).join(',') : '',
                AssetInfo: JSON.stringify(assetsInfo),
                ServiceType: 'VOA',
                BorrowerInfo: voaSection.find('.BorrowerList').val(),
                VerifiesBorrower: !isAssetPickerBeingUsed
            };

            return data;
        }

        function Initialize() {
            AuthorizationDocPicker.RegisterDocChangeCallback(ToggleOrderBtn);
            var validStatus = {
                0: false, // Pending
                1: true, // Complete
                5: true, // Active
            };

            VOXOrderCommon.InitializeCommonFunctions({
                GatherRequestData: GatherRequestData,
                ServiceType: "vod",
                VOXServiceName: "VOA",
                ToggleOrderBtn: ToggleOrderBtn,
                ValidStatusesAfterInitialRequest: validStatus,
                VOXSection: voaSection
            });

            voaSection.find('.NotificationEmail').val(ML.NotificationEmailDefault).change();
        }

        _initialize = Initialize;
    });

    function _onTabOpen() {
        _initialize();
        $('#secVoa').find('.LenderServices').change();
    }

    function _clearSection() {
        var voaSection = $('#secVoa');
        voaSection.find('input:text, input:password').val('');
        voaSection.find('input:checkbox').prop('checked', false);
        voaSection.find('select').prop('selectedIndex', 0);
        AuthorizationDocPicker.RemoveDocPicker(null, voaSection.find('.DocumentTable'))
    }

    return {
        OnTabOpen: _onTabOpen,
        ClearSection: _clearSection
    };
})();