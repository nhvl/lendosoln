﻿// don't surround this to call to window.onerror with $j(document).ready as there's no guarantee
// another document.ready call won't be made before that one.

//window.onerror = logJSError; //do not use jquery for window error http://api.jquery.com/error/ //!

//! may want to add: frames contained in?, browser?
// adding history impossible (unless doing that server side.)
function logJSError(msg, url, num) {
    var data = {
        message: msg,
        url: url,
        lineNumber: num
    };

    logJSMessage(data, 'LogJSError');
    
    return false;       // returning true makes the error not appear in the browser.
}
function logJSException(e, msgFromDeveloper) {

    // if function given bad argument, email developers.
    if (null == e) {
        logJSDeveloperError('logJSException called with null exception as parameter; ' + ( msgFromDeveloper || ''));   
        return;
    }
    
    var msg = e.toString();
    var url = e['fileName'] || e['sourceURL'] || window.location.toString();  // FF, Safari, Chrome/IE
    var num = e['lineNumber'] || e['line'] || -1;                 // FF, Safari, Chrome/IE (Chrome has lineinfo in stack, so no worries there)

    var stacklines = '';
    if (e.stack) { // FF, Chrome
        stacklines = e.stack.toString().split('\n');
    }
    else { // IE, Safari
        stacklines = getJSCallStack();
    }

    var data = {
        message: msg,
        url: url,
        lineNumber: num,
        stack: stacklines,
        msgFromDeveloper: msgFromDeveloper || ''
    };

    logJSMessage(data, 'LogJSException');
}
function logJSDeveloperError(msg) {
    var stacklines = getJSCallStack();

    var data = {
        message: msg,
        url: window.location.toString(),
        stack: stacklines
    };

    logJSMessage(data, 'LogJSDeveloperError');
}
function logJSMessage(dataObj, methodName) {


    if (typeof(dataObj.lineNumber) != 'undefined') {
        dataObj.lines = JSAttemptGetLines(dataObj.lineNumber, dataObj.url);        
    }
    dataObj.windowLocation = window.location.toString();
    dataObj.referrer = window.document.referrer || 'none';
    if (window.opener) {
        dataObj.opener = window.opener.location.toString();
    }
    else {
        dataObj.opener = 'none';
    }
    
    // don't use $j.post() as it leads to extra errors (see FF web developer console, not Firebug)
    $j.ajax({
        url: gVirtualRoot + '/LoggingService.asmx/'+methodName,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(dataObj)
    });
}

function JSAttemptGetLines(lineNumber, url) {

    if (lineNumber < 0) {
        return 'line number was less than 0'.split(' ');
    }
    
    //strip the url of it's query string.
    var qInd = url.indexOf('?');
    if (qInd > 0) {
        url = url.substring(0, url.indexOf('?'));
    }
    
    // check if it's a js file, if so, can't get the line from the line # without doing it server side.
    if (url.substring(url.length - 3) == '.js') {
        return 'check the js file for this revision'.split(' ');
    }
    else {
        var linesFromStartToHeadOrBody = 3; //! guessed, should be made consistent across all pages!
        var offset = linesFromStartToHeadOrBody;
        var windowSize = 2; // number of lines around the line to include.
        var pageLines = document.getElementsByTagName('html')[0].innerHTML.split('\n');
        var linesAroundErr = pageLines.slice(Math.max(0, lineNumber - 1 - offset - windowSize), Math.min(pageLines.length - 1, lineNumber - offset + windowSize));
        return linesAroundErr;
    }   
}

// returns callstack as array of strings.
function getJSCallStack() {

    try {
        throw new Error('dummy');
    }
    catch (e) {
        if (e.stack) { // FF, Chrome.
            var stackLines = e.stack.toString().split('\n');  // most recent calls first.
            stackLines.shift();  // ignore the call to getCallStack();
            return stackLines;
        }

        var stackFuncs = []; // IE / Safari.
        var currFunc = arguments.callee.caller;
        var ignoreFuncString = 'function';
        while (currFunc) {
            var currFuncString = currFunc.toString();
            var funcName = currFuncString.substring(currFuncString.indexOf(ignoreFuncString) + 8,
                                                currFuncString.indexOf('('))
                    || 'anonymous';
            stackFuncs.push(funcName);
            currFunc = currFunc.caller;
        }
        return stackFuncs;
    }
}
