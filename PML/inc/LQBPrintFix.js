﻿function lqbPrintByFrame(frame) {
    if (document.queryCommandSupported('print')) {
        var doc = frame.document || frame.contentDocument || document;
        doc.execCommand('print', false, null);
    }
    else {
        frame.focus();
        window.print();
    }
}
