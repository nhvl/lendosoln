﻿var seamlessDu = angular.module('SeamlessDU', ['ngRoute', 'LqbForms', 'LqbAudit'])

.controller('SeamlessDUController', ['$scope', '$route', '$routeParams', '$location', 'LqbPopupService',
    function SeamlessDUController($scope, $route, $routeParams, $location, LqbPopupService) {
        this.$route = $route;
        this.$location = $location;
        this.$routeParams = $routeParams;
        $scope.$on('$routeChangeStart', function (event, next, current) {
            $scope.loaded = false;
            $scope.loadSuccess = false;
            $scope.errorMessage = 'Unknown Error.';
        });
        $scope.$on('$routeChangeSuccess', function (event, next, current) {
            $scope.loaded = true;
            $scope.loadSuccess = true;
            $scope.errorMessage = '';
            parent && parent.jQuery("#LQBPopupDiv").dialog('option', { title: next.locals.pageName });
        });
        $scope.$on('$routeChangeError', function (event, next, current, response) {
            $scope.loaded = true;
            $scope.loadSuccess = false;
            $scope.errorMessage = response.data.UserMessage + ' Reference Number: ' + response.data.ErrorReferenceNumber;
        });
        $scope.closeWindow = function()
        {
            return LqbPopupService.popup.Hide();
        }
    }])

.config(function ($compileProvider, $routeProvider, $locationProvider) {
    // Allow 'blob:' links for downloading text files directly from the JS. 
    // Otherwise, they're prefixed with 'unsafe:' and don't work.
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);

    $locationProvider.hashPrefix('');

    $routeProvider
    .when('/audit', {
        templateUrl: seamlessFolder + '/SeamlessDuAudit.html',
        controller: 'SeamlessDUAuditController',
        resolve: { 
            auditResult: function ($http, $route, LqbUrl) {
                return $http.post(LqbUrl.seamlessFolder + '/SeamlessDuService.aspx?method=RunAudit',
                    {
                        LoanId: $route.current.params.loanid,
                        IsEmbeddedPml: !!window.parent.pmlData && window.parent.pmlData.IsEmbeddedPML
                    })
                    .then(function successCallback(response) {
                        var responseData = angular.fromJson(response.data.d.AuditResult);
                        responseData.LoanErrors.auditSuccess = responseData.LoanErrors.length === 0;
                        for (var i = 0; i < responseData.BorrowerAudits.length; i++) {
                            responseData.BorrowerAudits[i].auditSuccess = responseData.BorrowerAudits[i].Errors.length === 0;
                        }
                        return {
                            success: true,
                            loanErrors: responseData.LoanErrors,
                            borrowers: responseData.BorrowerAudits,
                            auditSuccess:
                                responseData.LoanErrors.auditSuccess
                                && responseData.BorrowerAudits.every(function (borrower) { return borrower.auditSuccess; })
                        };
                    });
            },
            pageName: function ($rootScope) { $rootScope.pageTitle = 'DU Submission Data Audit'; return $rootScope.pageTitle; }
        }
    })

    .when('/login', {
        templateUrl: seamlessFolder + '/SeamlessDuLogin.html',
        controller: 'SeamlessDULoginContoller',
        resolve: {
            loginSettings: function ($http, $route, LqbUrl) {
                return $http.post(LqbUrl.seamlessFolder + '/SeamlessDuService.aspx?method=GetLoginModel', { LoanId: $route.current.params.loanid })
                    .then(function successCallback(response) {
                        var responseData = angular.fromJson(response.data.d.LoginModel);
                        return {
                            success: true,
                            creditProviders: responseData.creditProviderOptions,
                            creditReportOptions: responseData.creditReportOptions,
                            pagemodel: responseData.pagemodel,
                            auditSuccess: responseData.auditSuccess,
                            ignoreAudit: responseData.ignoreAudit
                        };
                    });
            },
            pageName: function ($rootScope) { $rootScope.pageTitle = 'DU Submission'; return $rootScope.pageTitle; }
        }
    })

    .when('/summary', {
        templateUrl: seamlessFolder + '/SeamlessDuSummary.html',
        controller: 'SeamlessDUSummaryController',
        resolve: {
            pageName: function ($rootScope) { $rootScope.pageTitle = 'Casefile Status Summary '; return $rootScope.pageTitle; }
        }
    })

    .otherwise({ redirectTo: '/audit' });
})

.controller('SeamlessDUAuditController', function ($scope, $location, $routeParams, LqbPopupService, LqbUrl, auditResult) {
    $scope.closeWindow = LqbPopupService.popup.Hide;
    $scope.loanErrors = [{ Message: "Loading DU Seamless audit results..." }];
    $scope.auditResult = auditResult;
    $scope.virtualRoot = LqbUrl.root;
    $scope.navigateParent = LqbPopupService.navigateParent;

    $scope.nextPage = function () {
        $location.url('/login?loanid=' + encodeURIComponent($routeParams.loanid));
    };
})

.controller('SeamlessDULoginContoller', function ($rootScope, $scope, $http, $location, $routeParams, $timeout, $window, LqbPopupService, LqbUrl, InternetExplorer, loginSettings) {
    $scope.closeWindow = LqbPopupService.popup.Hide;
    $scope.creditProviders = loginSettings.creditProviders;
    $scope.creditReportOptions = loginSettings.creditReportOptions;
    $scope.auditSuccess = loginSettings.auditSuccess;
    $scope.ignoreAudit = loginSettings.ignoreAudit;
    $scope.model = loginSettings.pagemodel;
    $scope.msSaveOrOpenBlob = InternetExplorer.msSaveOrOpenBlob;

    $scope.maxPolls = 20;
    $scope.pollCount = 0;

    $scope.previousPage = function () {
        $location.url('/audit?loanid=' + encodeURIComponent($routeParams.loanid));
    };

    $scope.setErrorStatus = function (message, logContent) {
        $scope.pollCount = 0;
        $scope.submitting = false;
        $scope.submitMessageClass = 'Error';
        $scope.submitMessageSymbol = 'fa-warning';
        $scope.submitMessage = message;
        if (logContent) {
            $scope.logContentBlob = new Blob([logContent], { type: 'application/octet-stream' });
            $scope.responseLogUrl = ($window.URL || $window.webkitURL).createObjectURL($scope.logContentBlob);
        }
    };

    $scope.submit = function () {
        if ($scope.responseLogUrl)
            $window.URL.revokeObjectURL($scope.responseLogUrl);
        $scope.responseLogUrl = null;
        $scope.logContentBlob = null;

        // If $scope.submitting is still true, it should mean we got a 
        // "Processing" response and are polling.
        // We don't want to keep changing the status message and submitting 
        // status.
        if (!$scope.submitting) {
            if (this.DuSubmissionForm.$invalid)
            {
                $scope.submitMessage = 'Could not submit. Form is invalid.';
                $scope.submitMessageClass = 'Error';
                $timeout(function () { $scope.submitMessage = ''; $scope.submitMessageClass = ''; }, 3000);
                return;
            }
            $scope.submitting = true;
            $scope.submitMessageClass = '';
            $scope.submitMessageSymbol = 'fa-spin fa-spinner';
            $scope.submitMessage = 'Submitting to Fannie Mae DU...';
            $scope.fannieId = '';
        }

        $http.post(LqbUrl.seamlessFolder + '/SeamlessDuService.aspx?method=Submit',
            {
                model: angular.toJson($scope.model),
                LoanId: $routeParams.loanid,
                FannieId: $scope.fannieId,
            }).then(function successCallback(response) {
                var responseData = angular.fromJson(response.data.d.DuResponseData);
                if (responseData.Status === 'Done') {
                    $scope.pollCount = 0;
                    $rootScope.resultsPageModel = responseData;
                    $rootScope.previousCreditReportUsed = $scope.model.CreditInfo.CreditReportOption === $scope.creditReportOptions.UsePrevious.value;
                    $rootScope.autoPopulateLiabilitiesSelected = $scope.model.ImportLiabilitiesFromCreditReport;
                    $location.url('/summary?loanid=' + encodeURIComponent($routeParams.loanid));

                    if (!responseData.HasCreditReport) {
                        $scope.creditReportMessage = "No Credit Report was returned from the AUS."
                    }
                }
                else if (responseData.Status === 'Processing' && $scope.pollCount < $scope.maxPolls) {
                    $scope.pollCount++;
                    $scope.fannieId = responseData.FannieId;
                    $scope.submitMessageSymbol = 'fa-spin fa-spinner';
                    $scope.submitMessage = 'Fannie Mae is processing your request.';
                    $timeout($scope.submit, 5000);
                    return;
                }
                else if ($scope.pollCount >= $scope.maxPolls) {
                    $scope.setErrorStatus('Did not receive a DU underwriting response after ' + $scope.maxPolls
                        + ' tries. Please try again later. If the problem persists, please contact your administrator.');
                    return;
                }
                else if (responseData.Status === 'LoginError'
                        || responseData.Status === 'Error') {
                    $scope.setErrorStatus(responseData.ErrorMessage, responseData.DuResponseLog);
                }
                else {
                    $scope.setErrorStatus('Unexpected Status from the server: "'
                        + responseData.Status + '" : ' + responseData.ErrorMessage,
                        responseData.DuResponseLog);
                }
            }, function failureCallback(response) {
                $scope.setErrorStatus(response.data.statusMessage + ': '
                    + response.data.UserMessage + '\n'
                    + '(Reference Number: ' + response.data.ErrorReferenceNumber + ')');
            });
    };
})

.controller('SeamlessDUSummaryController', function ($rootScope, $scope, $http, $location, $routeParams, $window, LqbPopupService, LqbUrl, InternetExplorer) {
    $scope.closeWindow = LqbPopupService.popup.Hide;
    $scope.updating = false;
    $scope.previousCreditReportUsed = $rootScope.previousCreditReportUsed;
    $scope.resultsPageModel = $rootScope.resultsPageModel;

    if ($scope.resultsPageModel) {
        $scope.duResults = $rootScope.resultsPageModel.DuResultsSummary;
    }
    else {
        $scope.duResults = {};
        $location.url('/login?loanid=' + encodeURIComponent($routeParams.loanid));
    }

    if ($scope.resultsPageModel.UnderwritingSuccess)
    {
        $scope.msSaveOrOpenBlob = InternetExplorer.msSaveOrOpenBlob;
        $scope.logContentBlob = new Blob([$scope.resultsPageModel.DuResponseLog], { type: 'application/octet-stream' });
        $scope.responseLogUrl = ($window.URL || $window.webkitURL).createObjectURL($scope.logContentBlob);
    }

    if ($scope.previousCreditReportUsed)
    {
        $scope.resultsPageModel.ImportCreditReport = false;
        $scope.resultsPageModel.ImportLiabilities = false;
    }
    else if ($scope.autoPopulateLiabilitiesSelected)
    {
        $scope.resultsPageModel.ImportCreditReport = true;
        $scope.resultsPageModel.ImportLiabilities = true;
    }

    $scope.importCreditReport_change = function(importCreditReport) {
        if (!importCreditReport) {
            $scope.resultsPageModel.ImportLiabilities = false;
        }
    };

    $scope.updateLoan = function () {
        $scope.updating = true;
        $http.post(LqbUrl.seamlessFolder + '/SeamlessDuService.aspx?method=UpdateLoan',
            {
                LoanId: $routeParams.loanid,
                ImportAusFindings: $scope.resultsPageModel.ImportAusFindings,
                ImportCreditReport: $scope.resultsPageModel.ImportCreditReport,
                ImportLiabilities: $scope.resultsPageModel.ImportLiabilities
            }).then(function successCallback(response) {
                $scope.updating = false;
                $scope.closeWindow();
                LqbPopupService.refreshParent();
            }, function errorCallback(response) {
                $scope.updating = false;
                $scope.errorMessage = response.data.UserMessage + '\n'
                    + '(Reference Number: ' + response.data.ErrorReferenceNumber + ')';
            });
    }
});
