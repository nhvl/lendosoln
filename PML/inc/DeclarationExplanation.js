﻿var serializedDecExplnData;
var decExplnData;
var declarationExplanationTemplate;

function getField(args) {
    args.aBDecJudgmentExplanation = decExplnData.aBDecJudgmentExplanation;
    args.aCDecJudgmentExplanation = decExplnData.aCDecJudgmentExplanation;
    args.aBDecBankruptExplanation = decExplnData.aBDecBankruptExplanation;
    args.aCDecBankruptExplanation = decExplnData.aCDecBankruptExplanation;
    args.aBDecForeclosureExplanation = decExplnData.aBDecForeclosureExplanation;
    args.aCDecForeclosureExplanation = decExplnData.aCDecForeclosureExplanation;
    args.aBDecLawsuitExplanation = decExplnData.aBDecLawsuitExplanation;
    args.aCDecLawsuitExplanation = decExplnData.aCDecLawsuitExplanation;
    args.aBDecObligatedExplanation = decExplnData.aBDecObligatedExplanation;
    args.aCDecObligatedExplanation = decExplnData.aCDecObligatedExplanation;
    args.aBDecDelinquentExplanation = decExplnData.aBDecDelinquentExplanation;
    args.aCDecDelinquentExplanation = decExplnData.aCDecDelinquentExplanation;
    args.aBDecAlimonyExplanation = decExplnData.aBDecAlimonyExplanation;
    args.aCDecAlimonyExplanation = decExplnData.aCDecAlimonyExplanation;
    args.aBDecBorrowingExplanation = decExplnData.aBDecBorrowingExplanation;
    args.aCDecBorrowingExplanation = decExplnData.aCDecBorrowingExplanation;
    args.aBDecEndorserExplanation = decExplnData.aBDecEndorserExplanation;
    args.aCDecEndorserExplanation = decExplnData.aCDecEndorserExplanation;
}

$j(function () {
    $j.get(ML.VirtualRoot + "/webapp/DeclarationExplanationTemplate.html", function (data) {
        declarationExplanationTemplate = $j.template(data);
    });

    serializedDecExplnData = $j('#serializedDecExplnData').val();
    if (serializedDecExplnData)
    {
        decExplnData = JSON.parse(serializedDecExplnData);
        registerPostGetAllFormValuesCallback(getField);
    }
});

function addExplanations() {
    // temporary set the names for the popup if they do not exist. These names will not be written to the loan.
    if (decExplnData.aBSsn && !decExplnData.aBNm) {
        decExplnData.aBNm = "(Borrower Name Blank)";
    }
    if (decExplnData.aCSsn && !decExplnData.aCNm) {
        decExplnData.aCNm = "(Coborrower Name Blank)";
    }

    if (!decExplnData.aBSsn && !decExplnData.aCSsn) {
        simpleDialog.alert('No borrowers on file');
    }
    else
    {
        var container = $j.tmpl(declarationExplanationTemplate, {
            'decExplnData': decExplnData
        });

		var oldIsDirty = isDirty();

        LQBPopup.ShowElement($j('<div></div>').append(container), {
            onReturn: function (dialogArgs) {
                if (dialogArgs.OK) {
                    handleLqbPopup(dialogArgs.aBDecExplanations, dialogArgs.aCDecExplanations);
                    updateDirtyBit();
					attemptSave();					
				}

				if (oldIsDirty) {
					updateDirtyBit();
				}
				else {
					clearDirty();
				}
            },
            headerText: "Explanations",
            popupClasses: "modal-explanations",
            hideXButton: true,
            hideCloseButton: true,
            buttons:[{
                Cancel: function(){
                    var returnObject = { OK: false };
                    LQBPopup.Return(returnObject);
                },
            }, {
                Save: function(){
                    var returnObj =
                        {
                            OK: true,
                            aBDecExplanations: document.getElementsByName("aBDecExplanations"),
                            aCDecExplanations: document.getElementsByName("aCDecExplanations")
                        }
                    LQBPopup.Return(returnObj);
                },
            }],
        });
    }
    _initInput();
}

function handleLqbPopup(aBDecExplanations, aCDecExplanations) {
    if (aBDecExplanations.length > 0) {
        decExplnData.aBDecJudgmentExplanation = aBDecExplanations[0].value;
        decExplnData.aBDecBankruptExplanation = aBDecExplanations[1].value;
        decExplnData.aBDecForeclosureExplanation = aBDecExplanations[2].value;
        decExplnData.aBDecLawsuitExplanation = aBDecExplanations[3].value;
        decExplnData.aBDecObligatedExplanation = aBDecExplanations[4].value;
        decExplnData.aBDecDelinquentExplanation = aBDecExplanations[5].value;
        decExplnData.aBDecAlimonyExplanation = aBDecExplanations[6].value;
        decExplnData.aBDecBorrowingExplanation = aBDecExplanations[7].value;
        decExplnData.aBDecEndorserExplanation = aBDecExplanations[8].value;
    }
    if (aCDecExplanations.length > 0) {
        decExplnData.aCDecJudgmentExplanation = aCDecExplanations[0].value;
        decExplnData.aCDecBankruptExplanation = aCDecExplanations[1].value;
        decExplnData.aCDecForeclosureExplanation = aCDecExplanations[2].value;
        decExplnData.aCDecLawsuitExplanation = aCDecExplanations[3].value;
        decExplnData.aCDecObligatedExplanation = aCDecExplanations[4].value;
        decExplnData.aCDecDelinquentExplanation = aCDecExplanations[5].value;
        decExplnData.aCDecAlimonyExplanation = aCDecExplanations[6].value;
        decExplnData.aCDecBorrowingExplanation = aCDecExplanations[7].value;
        decExplnData.aCDecEndorserExplanation = aCDecExplanations[8].value;
    }
}
