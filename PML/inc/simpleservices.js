var gService = new SimpleService();

function SimpleService() {
  this.register = f_register;
}

function f_register(url, name) {
  this[name] = new UrlService(url);
}

function UrlService(url) {
  this.url = url;
  this.call = f_call;
  this.acall = f_asyncCall;
  this.callAsyncSimple = function (methodName, args, successCallback, errorCallback, timeout) {
      this.acall(methodName, args, successCallback, errorCallback, true, null, true, timeout);
  };
  this.callAsyncBypassOverlay = function (methodName, args, successCallback, errorCallback, timeout) {
      this.acall(methodName, args, successCallback, errorCallback, true, null, true, timeout, true);
  };
}

function f_call(methodName, args, keepQuiet, bUseParentForError, dontHandleError, isAsync, successCallback, errorCallback, timeout, bypassOverlay)
{
    var result = {};

    var timeoutTrue = 0;
    if (typeof (timeout) === 'number') {
        timeoutTrue = timeout;
    }

    isAsync = isAsync || false;
    if (isAsync && !bypassOverlay) {
        if (typeof (triggerOverlay) == "function") {
            triggerOverlay();
        }
    }

    jQuery.ajax({
        type: "POST",
        url: this.url + "?method=" + methodName,
        async: isAsync,
        data: JSON.stringify(args),
        contentType: "application/json",
        dataType: "json",
        cache: false,
        timeout: timeoutTrue
    }).done(function(data, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('IsLogin') == 'true') {
            getHighestParentSameDomain().location = jqXHR.responseURL;
            return;
        }

        result.error = false;
        result.value = data.d;

        if (typeof successCallback === 'function') {
            successCallback(result);
        }

        attemptRemoveOverlay(isAsync);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.getResponseHeader('IsLogin') == 'true') {
            getHighestParentSameDomain().location = jqXHR.responseURL;
            return;
        }

        var sErrorUrl = gVirtualRoot + '/common/AppError.aspx';
        result.error = true;
        result.UserMessage = 'INTERNAL SERVER ERROR';
		if (!!jqXHR.responseText && jqXHR.responseText.indexOf("LP_f6d1d39dee3c435d9d14f1dc245e316d") != -1)
        {
            if (typeof clearDirty === 'function') clearDirty();

            result.UserMessage = 'Your session is expired. Please login again.';
            top.location = gVirtualRoot + '/SessionExpired.aspx';

        }
        else
        {
            try
			{
				if (!!jqXHR.responseText) {
					var errorObject = jQuery.parseJSON(jqXHR.responseText);

					result.UserMessage = errorObject.UserMessage;
					if (!!!dontHandleError) {
						if (typeof clearDirty === 'function') clearDirty();

						var errorUrl = sErrorUrl + '?id=' + errorObject.ErrorReferenceNumber;

						if (bUseParentForError != null && bUseParentForError)
							parent.location = errorUrl;
						else
							top.location = errorUrl;
					}
					else {
						result.type = errorObject.type;
						result.url = sErrorUrl + '?id=' + errorObject.ErrorReferenceNumber;

					}
				}                               
            }
            catch (e)
            {
                var jqXHRDebug = "textStatus = " + textStatus + ". errorThrown = " + errorThrown + ". "
                    + (jqXHR.readyState     ? " readyState = "      + jqXHR.readyState      + ".\n" : " no readyState.\n")
                    + (jqXHR.status         ? " status = "          + jqXHR.status          + ".\n" : " no status.\n")
                    + (jqXHR.statusText     ? " statusText = "      + jqXHR.statusText      + ".\n" : " no statusText.\n")
                    + (jqXHR.responseText   ? " responseText = "    + jqXHR.responseText    + ".\n" : " no responseText.\n")
                    + (jqXHR.responseXML    ? " responseXML = "     + jqXHR.responseXML     + ".\n" : " no responseXML.\n")
                logJSException(e, "problem with f_call's error handling. Got response: {" + jqXHRDebug + "}");
                result.errorDetail = 'INTERNAL SERVER ERROR';
            }
        }
        
        if (typeof errorCallback === 'function') {
            errorCallback(result);
        }
        else if (typeof successCallback === 'function') {
            successCallback(result);
        }

        attemptRemoveOverlay(isAsync);
    });

    return result;
}

function f_asyncCall(methodName, args, successCallback, errorCallback, keepQuiet, bUseParentForError, dontHandleError, timeout, bypassOverlay) {
    this.call(methodName, args, keepQuiet, bUseParentForError, dontHandleError, true, successCallback, errorCallback, timeout, bypassOverlay);
}

function attemptRemoveOverlay(isAsync) {
    if (isAsync && typeof (removeOverlay) == 'function') {
        removeOverlay();
    }
}

function buildXmlRequest(args) {
  if (args == null) {
    return '<request><data/></request>'
  } else {
    var str = '';
    for (p in args) {
      str += p + '="' + encodeValue(args[p]) + '" ';
    }
    if (typeof pml_action == 'function') pml_action('buildXmlRequest', str);
    return '<request><data ' + str + '/></request>';

  }
}

function encodeValue(str) {
  var  replaceItems = [
    {r : /&/g, s : '&amp;'},
    {r : /</g, s : '&lt;'},
    {r : />/g, s : '&gt;'},
    {r : /"/g, s : '&quot;'},
    {r : /'/g, s : '&apos;'}
  ];

  if (str == null)
  {
      str = '';
  }
  else
  {
      str = str.toString();
  }
    
    for( var i = 0; i < replaceItems.length; i++ ) {
        str = str.replace(replaceItems[i].r, replaceItems[i].s);
    }
    
    
   return str;


}
// Only do synchronous call.
function f_call_old(methodName, args, keepQuiet, bUseParentForError, dontHandleError) {
  var ret = new Object();
  ret.error = true;
  var sErrorUrl = gVirtualRoot + '/common/AppError.aspx';
  var sErrorMsg = '';
  var dtStarted = new Date();

  for (var i = 0; i < 3; i++) {
    try {
        var oHttp = null;
        var fullUrl = this.url + '?method=' + methodName;
        var isRedirect = false;

      if (window.XMLHttpRequest) {
          oHttp = new XMLHttpRequest();
          oHttp.onload = function () {
              if (oHttp.getResponseHeader('IsLogin') == 'true') {
                getHighestParentSameDomain().location = oHttp.responseURL;
                  isRedirect = true;
              }
          };
      } else {
        oHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      oHttp.open("POST", this.url + '?method=' + methodName, false);
      oHttp.setRequestHeader('Content-Type', 'text/xml');
      oHttp.send(buildXmlRequest(args));

      var doc = oHttp.responseXML;
      if (isRedirect) {
          return;
      }

      if (doc == null) throw ("doc is null." + oHttp.responseText);
      if (doc.documentElement == null) {
        if (oHttp.responseText.indexOf("LP_f6d1d39dee3c435d9d14f1dc245e316d") != -1) {
          if (typeof(clearDirty) == 'function') clearDirty();
          ret.error = true;
          ret.UserMessage = 'Your session is expired. Please login again.';
          top.location = gVirtualRoot + '/SessionExpired.aspx';
          return ret;
        }
        else if (oHttp.status >= 1200) {
            ret.error = true;
            ret.UserMessage  = 'You are experiencing connection issues. Please try again.';
            return ret;
        }
        else
          throw ("doc.documentElement is null. " + oHttp.responseText);        
      }     
      if (typeof pml_action == 'function') pml_action('f_call_response', oHttp.responseText);
      var rec = doc.documentElement.selectSingleNode("data");
      if (rec != null) {
        ret.value = new Object();
        ret.error = false;
        var coll = rec.attributes;
        for (var i = 0; i < coll.length; i++) {
          var a = coll.item(i);
          ret.value[a.name] = a.value;
        }
      } 
      rec = doc.documentElement.selectSingleNode("error");
      if (rec != null && !!!dontHandleError) {
        if (typeof (clearDirty) == "function") clearDirty();
        var errorUrl = sErrorUrl + '?id=' + rec.getAttribute("ErrorReferenceNumber");
        if (bUseParentForError != null && bUseParentForError)
          parent.location = errorUrl;
        else
          top.location = errorUrl;
      }
      if( rec && !!dontHandleError ) {
         ret.type = rec.getAttribute('type'); 
         ret.url = sErrorUrl + '?id=' + rec.getAttribute("ErrorReferenceNumber");
      }
      // TODO: Handle error.
      return ret;

    } catch (e) {
      if (null != e) {
        if (typeof (e) === 'string') {
          sErrorMsg = e;
        } else {
          sErrorMsg = e.message;
        }
       } logJSException(e, 'problem with synchronous call.');
    }
  }
  if (typeof (clearDirty) == "function") clearDirty();
  var duration = new Date() - dtStarted;
  var tmp = this.url + '::' + methodName + '. Duration=' + duration + '. Err=' + sErrorMsg;
  var maxLength = tmp.length < 500 ? tmp.length : 500;

  top.location = sErrorUrl + '?id=AJAX&msg=' + escape(tmp.substring(0, maxLength));
  ret.error = true;

  ret.errorDetail = 'Internal error'; // TODO: More descriptive error message.
  return ret;

}
