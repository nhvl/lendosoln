const $ = jQuery;
import { module, IDirective, IDirectiveLinkFn, IScope, ITimeoutService } from "angular";

import { simpleDialog, IframePopup } from "../components/IframePopup";
import { TPOStyleUnification } from "../components/TPOStyleUnification";
import { generateUrl } from "../utils/url";
import { pDOMContentLoaded } from "../utils/common-promise";
import { postJson, postJsonSafe } from "../utils/fetch";

import { pmlService } from "../api/gService";
import ILqbAsyncPdfDownloadHelper from '../api/ILqbAsyncPdfDownloadHelper';

import { StatusBar } from "./StatusBar";

declare const StatusAndAgentsViewModel: IViewModel;
declare const LqbAsyncPdfDownloadHelper: ILqbAsyncPdfDownloadHelper;

interface IViewModel {
    sLId                        : StrGuid;
    sStatusT                    : string;
    sStatusD                    : string;
    sBranchChannelT             : number;
    isInactiveStatus            : boolean;
    hasSuspenseNotice           : boolean;
    hasApprovalCertificate      : boolean;
    hasCreditDenial             : boolean;
    loanStatuses                : ILoanStatus[];
    isRetailMode                : boolean;
    assignedOcAgents            : IAssignedAgents;
    assignedLenderAgents        : IAssignedAgents;
    isLead                      : boolean;
    constants                   : {
        loanOfficerRoleId       : string;
        brokerProcessorRoleId   : string;
        externalSecondaryRoleId : string;
        externalPostCloserRoleId: string;
        correspondentChannelMode: number;
        documentCheckStatus     : number;
        conditionReviewStatus   : number;
    };
    HideStatusSubmissionButtonsInOriginatorPortal: boolean;
}
interface IAssignedAgents {
    sectionName: string;
    agents     : IAgent[];
}
interface IAgent {
    RoleId        : string; // number
    RoleName      : string;
    Name          : string;
    Email         : string;
    Phone         : string;
    AssignmentLink: string; // url
    Assigned      : boolean;
}
interface ILoanStatus {
    Name    ?: string;
    Date    ?: string;
    Tooltip  : string;
    Active   : boolean;
    Collapsed: boolean;
    Complete : boolean;
}

interface IAgent2 {
    employeeId: string,
    fullName:string,
    phone:string,
    email:string,
}

interface IStatusAndAgentsScope extends IScope {
    loanOfficerFieldName            : string,
    brokerProcessorFieldName        : string,
    externalSecondaryFieldName      : string,
    externalPostCloserFieldName     : string,
    roleExpectedArgumentNameMappings: { [key:string]: string },
    vm                              : IViewModel,
}

let $timeout: ITimeoutService;

class StatusAndAgentCtrl {
    private readonly loanOfficerFieldName        = "LoanOfficer_EmployeeId";
    private readonly brokerProcessorFieldName    = "Processor_EmployeeId";
    private readonly externalSecondaryFieldName  = "Secondary_EmployeeId";
    private readonly externalPostCloserFieldName = "PostCloser_EmployeeId";

    private roleExpectedArgumentNameMappings: { [key:string]: string };
    private vm: IViewModel;


    static $inject = ["$timeout"];
    constructor(_$timeout: ITimeoutService) {
        $timeout = _$timeout;

        this.vm = JSON.parse(JSON.stringify(StatusAndAgentsViewModel));

        this.roleExpectedArgumentNameMappings = {
            [this.vm.constants.loanOfficerRoleId]       : this.loanOfficerFieldName,
            [this.vm.constants.brokerProcessorRoleId]   : this.brokerProcessorFieldName,
            [this.vm.constants.externalSecondaryRoleId] : this.externalSecondaryFieldName,
            [this.vm.constants.externalPostCloserRoleId]: this.externalPostCloserFieldName,
        };

        setTimeout(function () {
            TPOStyleUnification.Components();
        });

        pDOMContentLoaded.then(() => {
            $("#WaitDialog").hide();
            $(window).resize(handleResize);
            setTimeout(handleResize);

            function handleResize() {
                const totalPageWidth = $("#statusSection").width()! - 32;
                const statusList = $("#statusList").children().toArray();
                StatusBar.resizeStatus(totalPageWidth, statusList, statusList.length);
            }
        });
    }

    viewStatusCertificate(){
        window.open(
            generateUrl({
                url: `${VRoot}/Common/PrintView_Frame.aspx`, qs: {
                    body_url: `${VRoot}/Main/LoanSummary.aspx?loanid=${this.vm.sLId}`
                }
            }), '_blank');
    }
    viewSuspenseNotice      () { this.openPdfPage('SuspenseNotice'); }
    viewApprovalCertificate () { this.openPdfPage('PmlApproval'   ); }
    viewCreditDenial        () { this.openPdfPage('CreditDenial'  ); }
    openPdfPage(pageName: string) {
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(`${VRoot}/pdf/${pageName}.aspx?loanid=${this.vm.sLId}&crack=${new Date().getTime()}`);
    }

    async displayAgentAssignment(originalAgent:IAgent) {
        const displayWarning = this.vm.sBranchChannelT === this.vm.constants.correspondentChannelMode
            ? originalAgent.RoleId === this.vm.constants.externalSecondaryRoleId
            : originalAgent.RoleId === this.vm.constants.loanOfficerRoleId;

        const url = originalAgent.AssignmentLink
            + (displayWarning         ? '&showlowarning=t' : "")
            + (originalAgent.Assigned ? "&isassigned=t"    : "")
            ;

        const [error, selectedAgent] = await IframePopup.show<IAgent2>(url, {});
        if (error != null) {
            console.error(error);
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            return;
        }

        if (selectedAgent == null) return;

        if (this.vm.isRetailMode) this.assignRetailAgent(selectedAgent, originalAgent);
        else                      this.assignAgent      (selectedAgent, originalAgent);
    }
    private assignRetailAgent(selectedAgent: IAgent2, originalAgent: IAgent) {
        const [err] = pmlService.SaveRetailAgent({
            loanId    : this.vm.sLId,
            employeeId: selectedAgent.employeeId,
            roleId    : originalAgent.RoleId
        });

        if (!!err) {
            displayMessage(err, false);
            return;
        }

        $timeout(() => {
            originalAgent.Name     = selectedAgent.fullName;
            originalAgent.Phone    = selectedAgent.phone;
            originalAgent.Email    = selectedAgent.email;
            originalAgent.Assigned = true;
        });
    }
    private assignAgent(selectedAgent: IAgent2, originalAgent: IAgent) {
        const fieldName = this.roleExpectedArgumentNameMappings[originalAgent.RoleId];

        const err = pmlService.SaveAgents({
            sLId: this.vm.sLId,
            [fieldName]: selectedAgent.employeeId,
        });

        if (!!err) {
            displayMessage(err, false);
            return;
        }

        $timeout(() => {
            originalAgent.Name     = selectedAgent.fullName;
            originalAgent.Phone    = selectedAgent.phone;
            originalAgent.Email    = selectedAgent.email;
            originalAgent.Assigned = true;
        });
    }

    async changeLoanStatus() {
        if (this.vm.HideStatusSubmissionButtonsInOriginatorPortal) {
            return;
        }

        const [error, result] = await IframePopup.show<IChangeStatusResult>(`${ML.VirtualRoot}/main/ChangeLoanStatus.aspx?hideHeader=1&loanid=${this.vm.sLId}`, {})
        if (error != null) {
            console.error(error);
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            return;
        }

        if (result == null) return;

        this.changeStatusCallback(result);
    }
    async changeStatusCallback(args: IChangeStatusResult) {
        if (!args.OK) return;

        // Open modal wait dialog.
        $("#WaitText").text('Please wait, changing loan status...');
        LQBPopup.ShowElementWithWrapper($("#WaitDialog"), {
            draggable: false,
            hideXButton: true,
            hideCloseButton: true,
            backdrop: "static",
        });
        const [err] = await postJsonSafe<void>(`${VRoot}/main/PipelineService.aspx/ChangeLoanStatus`, { sLId: this.vm.sLId, status: args.status });
        LQBPopup.Hide();
        if (err == null) displayMessage(`Submission to ${args.statusName} status successful.`, true);
        else displayMessage(`Submission to ${args.statusName} status failed.`, false);
    }


    async convertToLoanFile() {
        // Open modal wait dialog.
        $("#WaitText").text('Please wait, creating loan...');
        LQBPopup.ShowElementWithWrapper($("#WaitDialog"), {
            draggable: false,
            hideXButton: true,
            hideCloseButton: true,
            backdrop: "static",
        });

        var args = {
            LeadId: this.vm.sLId
        };
        const [err] = await postJsonSafe<void>(`${VRoot}/main/PipelineService.aspx/ConvertToLoanFile`, args);

        if (err == null) {
            window.location.reload();
        }
        else {
            LQBPopup.Hide();
            displayMessage(`Lead to loan conversion failed.`, false);
        }
    }
}

interface IChangeStatusResult {
    OK: boolean, status: number, statusName: string
}

const statusAndAgents = module('StatusAndAgents', ["LoanHeader"]);

import AssignedAgentsSectionHtml from "./AssignedAgentsSection.html";
statusAndAgents.component('assignedAgentSection', {
    bindings: {
        datasource: '<',
        allowlinks: '<',
        onAssign  : "&",
    },
    template: AssignedAgentsSectionHtml,
    controller: class AssignedAgentsSectionCtrl {
        private agent: IAgent;
        private onAssign: (params: { $agent: IAgent }) => void;

        private beginAssign($agent:IAgent) {
            this.onAssign({$agent});
        }
    },
});

import AssignedAgentHtml from "./AssignedAgent.html";
statusAndAgents.component('assignedAgent', {
    bindings: {
        agent     : '<',
        allowlinks: '<',
        onAssign  : "&",
    },
    template: AssignedAgentHtml,
    controller: class AssignedAgentCtrl {
        private agent   : IAgent;
        private onAssign: () => void;

        private assignClick(){
            this.onAssign();
        }
    },
});

import saHtml from "./index.html";

statusAndAgents.component('statusAndAgents', {
    template: saHtml,
    controller: StatusAndAgentCtrl,
});

function displayMessage(message:string, reloadPage:boolean) {
    simpleDialog.alert(message).then(() => {
        if (reloadPage) window.location.reload();
    });
}
