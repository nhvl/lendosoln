// opm 460944 - make the status bar more responsive
export class StatusBar {
    private static resetStatusBar(statusList:HTMLElement[], length:number) {
        var collapsedCount = 0;
        for (var i = 0; i < length; i++) {
            statusList[i].classList.add(String(i));
            if (!statusList[i].classList.contains("active")) {
                statusList[i].style.marginLeft = "1px";
                statusList[i].style.marginRight = "1px";
            }
            else {
                if (i > 0) {
                    statusList[i - 1].style.marginRight = "0px";
                }
                statusList[i].style.marginLeft = "0px";
                statusList[i].style.marginRight = "3px";
            }

            if (statusList[i].classList.contains("collapsed")) {
                statusList[i].style.paddingLeft = "6px";
                collapsedCount++;
            }
            else if (statusList[i].classList.contains("first")) {
                statusList[i].style.paddingLeft = "4px";
                statusList[i].style.paddingRight = "4px";
            }
            else if (statusList[i].classList.contains("last")) {
                statusList[i].style.paddingLeft = "20px";
                statusList[i].style.paddingRight = "4px";
            }
            else {
                statusList[i].style.paddingLeft = "20px";
                statusList[i].style.paddingRight = "6px";
            }
        }
        return collapsedCount;
    }

    private static calculateStatusBarWidth(collapsedCount:number, length:number) {
        let width = 0;

        for (var i = 0; i < length; i++) {
            var status = jQuery("." + i);
            if (status.width()! > 10) {
                width += status.width()!;
                width += parseInt(status[0].style.paddingLeft!);
                width += parseInt(status[0].style.paddingRight!);
                width += parseInt(status[0].style.marginLeft!);
                width += parseInt(status[0].style.marginRight!);
            }
        }

        return width + collapsedCount * 19;
    }

    public static resizeStatus(totalPageWidth: number, statusList:HTMLElement[], length:number) {
        // min sizes
        var collapsedWidth   = 1;
        var collapsedPadding = 6;
        var paddingLeft      = 4;
        var paddingRight     = 4;
        var pixelsAdded      = 0;

        var collapsedCount = StatusBar.resetStatusBar(statusList, length);
        var statusBarWidth = StatusBar.calculateStatusBarWidth(collapsedCount, length);

        // keep count of pixels that would be added by the resizing in a loop
        while (statusBarWidth + pixelsAdded < totalPageWidth) {
            paddingLeft += 1;
            if (paddingLeft > 19) {
                paddingRight += 1;
            }
            if (paddingLeft > 112) {
                collapsedPadding += 1;
            }
            if (paddingLeft > 118) {
                collapsedWidth += 1;
            }

            // if you're at a point where margin can be restored, restore the margins, and reset the paddings
            if ((paddingLeft - 20) * 8 + 16 > (length - 1) * 2 && (statusList[0].style.marginLeft === "1px" || statusList[0].style.marginLeft === "0px")) {
                paddingLeft = 4;
                paddingRight = 4;
                pixelsAdded = length * 2;
                for (var i = 0; i < length; i++) {
                    if (!statusList[i].classList.contains("active")) {
                        statusList[i].style.marginLeft = "2px";
                        statusList[i].style.marginRight = "2px";
                    }
                    else {
                        statusList[i].style.marginLeft = i === 0 ? "2px" : "1px"; // to prevent infinite loop
                        statusList[i].style.marginRight = "5px";
                    }

                    if (statusList[i].classList.contains("collapsed")) {
                        statusList[i].style.paddingLeft = "6px";
                    }
                    else if (statusList[i].classList.contains("first")) {
                        statusList[i].style.paddingLeft = "4px";
                        statusList[i].style.paddingRight = "4px";
                    }
                    else if (statusList[i].classList.contains("last")) {
                        statusList[i].style.paddingLeft = "20px";
                        statusList[i].style.paddingRight = "4px";
                    }
                    else {
                        statusList[i].style.paddingLeft = "20px";
                        statusList[i].style.paddingRight = "6px";
                    }
                }
                continue;
            }

            for (var i = 0; i < length; i++) {
                if (statusList[i].classList.contains("collapsed")) {
                    if(6 < collapsedPadding && collapsedPadding <= 10) {
                        pixelsAdded++;
                    }
                    if (1 < collapsedWidth && collapsedWidth <= 18) {
                        pixelsAdded++;
                    }
                }
                else if (statusList[i].classList.contains("first")) {
                    if (4 < paddingLeft) {
                        pixelsAdded++;
                    }
                    if (4 < paddingRight) {
                        pixelsAdded++;
                    }
                }
                else if (statusList[i].classList.contains("last")) {
                    if (20 < paddingLeft) {
                        pixelsAdded++;
                    }
                    if (4 < paddingRight) {
                        pixelsAdded++;
                    }
                }
                else {
                    if (20 < paddingLeft) {
                        pixelsAdded++;
                    }
                    if (4 < paddingRight) {
                        pixelsAdded++;
                    }
                }
            }
        }

        // after the resizing calculation is complete, update the width and padding of the status bar
        for (var i = 0; i < length; i++) {
            if (statusList[i].classList.contains("collapsed")) {
                if (6 <= collapsedPadding && collapsedPadding <= 10) {
                    statusList[i].style.paddingLeft = collapsedPadding + "px";
                }
                else if (collapsedPadding > 10) {
                    statusList[i].style.paddingLeft = "10px";
                }
                if (1 <= collapsedWidth && collapsedWidth <= 18) {
                    statusList[i].style.width = collapsedWidth + "px";
                }
                else if (collapsedWidth > 18) {
                    statusList[i].style.width = "18px";
                }
            }
            else if (statusList[i].classList.contains("first")) {
                if (4 <= paddingLeft) {
                    statusList[i].style.paddingLeft = paddingLeft + "px";
                }
                if (4 <= paddingRight) {
                    statusList[i].style.paddingRight = paddingRight + "px";
                }
            }
            else if (statusList[i].classList.contains("last")) {
                if (paddingLeft >= 20) {
                    statusList[i].style.paddingLeft = paddingLeft + "px";
                }
                if (4 <= paddingRight) {
                    statusList[i].style.paddingRight = paddingRight + "px";
                }
            }
            else {
                if (paddingLeft >= 20) {
                    statusList[i].style.paddingLeft = paddingLeft + "px";
                }
                if (4 <= paddingRight) {
                    statusList[i].style.paddingRight = paddingRight + "px";
                }
            }
        }
    }
}
