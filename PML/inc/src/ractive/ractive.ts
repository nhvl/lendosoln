import Ractive from "ractive";

Ractive.DEBUG = __DEV__;

export interface EventContext<TEvent = Event> extends RactiveContext {
    event   : TEvent,
    name    : string,
    node    : HTMLElement,
    original: TEvent,
}

interface RactiveContext extends Ractive.Ractive {
    element : any,
    fragment: any,
    model   : any,
    node    : HTMLElement,
    ractive : Ractive.Ractive,
    root    : EventContext,
}
