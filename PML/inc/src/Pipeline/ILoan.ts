import {E_sStatusT} from "../DataLayer/Enums/E_sStatusT";

export interface ILoan {
    sLId                              : string;
    sLNm                              : string;


    sNumOfActiveConditionOnly         : number;
    sNumOfActiveDueTodayConditionOnly : number;
    sNumOfActivePastDueConditionOnly  : number;

    sNumOfActiveTasksOnly             : number;
    sNumOfActiveDueTodayTasksOnly     : number;
    sNumOfActivePastDueTasksOnly      : number;

    sApprovalCertPdfLastSavedD        : Date|null;
    sSuspenseNoticePdfLastSavedD      : Date|null;
    sRateLockConfirmationPdfLastSavedD: Date|null;

    aBLastNm                          : string,
    aBFirstNm                         : string,
    sLAmtCalc                         : string,
    sSpAddr                           : string,
    LoanStatus                        : string,
    sStatusD                          : Date|null,
    sStatusT                          : E_sStatusT,
    InScope                           : string,
    IsStatusTApprovedNotCanceled      : string,
    sRateLockStatusT_rep              : string,
    sRLckdExpiredD                    : Date|null,
    sRateLockStatusT                  : string,
    sEmployeeLoanRepName              : string,
    sEmployeeBrokerProcessorName      : string,
    sEmployeeExternalSecondaryName    : string,
    sEmployeeExternalPostCloserName   : string,


    CanRead                           : boolean;
}
export function normalizeLoans(loans:ILoan[]) {
    return loans.map(normalizeLoan);
}
export function normalizeLoan(loan: ILoan): ILoan {
    [
        "sNumOfActiveConditionOnly",
        "sNumOfActiveDueTodayConditionOnly",
        "sNumOfActivePastDueConditionOnly",
        "sNumOfActiveDueTodayTasksOnly",
        "sNumOfActivePastDueTasksOnly",
        "sNumOfActiveTasksOnly",
    ].forEach(k => updateNumber(loan, k, 0));

    [
        "sApprovalCertPdfLastSavedD",
        "sSuspenseNoticePdfLastSavedD",
        "sRateLockConfirmationPdfLastSavedD",
        "sStatusD",
        "sRLckdExpiredD",
    ].forEach(k => updateDate(loan, k));

    loan.CanRead = loan.CanRead === true || (loan.CanRead as any === 1);

    return loan;
}

function updateNumber(x:any, key:string, defaultValue:any) {
    if (x[key] == null) x[key] = defaultValue;
}

export function updateDate(x:any, key:string) {
    if (x[key] == null) return;

    var results = /Date\(([^)]+)\)/.exec(x[key]);
    if (results != null) {
        return x[key] = new Date(parseFloat(results[1]));
    }

    var d = new Date(x[key]);
    if (Number.isNaN(d.getTime())) return;
    return x[key] = d;
}
