export enum E_TaskConditionOption
{
    Task = 0,
    Condition = 1,
    IncludeBoth = 2
}