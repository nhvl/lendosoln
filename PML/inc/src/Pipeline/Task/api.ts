import {E_sCorrespondentProcessT} from "../../DataLayer/Enums/E_sCorrespondentProcessT";
import {E_TaskStatus} from "../../DataLayer/Enums/Task/E_TaskStatus";
import {E_TaskConditionOption} from "./E_TaskConditionOption";

import { guidEmpty } from "../../utils/guid";

export function performOperation<T>(opName:string, data:{
    taskIds                              ?: string,
    resolutionBlockTriggerNamesRawString ?: string,
    resolutionDateFieldIdsRawString      ?: string,
    loanId                               ?: string,

    id                                   ?: string,
    type                                 ?: any,
    date                                 ?: string,
}) {
    if (data == null) return Promise.resolve(["data is null", null] as [string|null, T|null]);

    if (data.resolutionBlockTriggerNamesRawString == null) data.resolutionBlockTriggerNamesRawString = "";
    if (data.resolutionDateFieldIdsRawString      == null) data.resolutionDateFieldIdsRawString      = "";
    if (data.loanId                               == null) data.loanId                               = guidEmpty;

    return apiPipelineService<T>(opName, data);
}

export async function apiAssignTask(taskIds: string[], id:string, type:any): Promise<[string|null, boolean|null]>{
    const pair = await performOperation<string>('Assign', {
        taskIds: taskIds.join(','),
        id     ,
        type   ,
    });
    const [error, errorString] = pair;
    if (error) return [error, false];
    if (!errorString) return [null, true];
    else return [errorString, false];
}

export function apiSetFollowupDate(taskIds: string[], date:string){
    return performOperation<boolean>('SetFollowupDate', {
        taskIds: taskIds.join(','),
        date   ,
    });
}

export function apiSubscribeToTasks(taskIds: string[]){
    return performOperation<boolean>('SubscribeToTasks', { taskIds: taskIds.join(',') });
}

export function apiUnsubscribeTasks(taskIds: string[]){
    return performOperation('UnsubscribeTasks', { taskIds: taskIds.join(',') });
}

export function apiExportTaskAsPdf(taskIds: string[], loanIds:string[], conditionMode:boolean){
    return apiPipelineService<string>("ExportAllTasksToPDF", {
        taskIDs        : taskIds.join(","),
        loanIDs        : loanIds,
        conditionmode  : conditionMode,
    });
}

export function apiGetResolvedOrClosedTaskCount(mode:E_TaskConditionOption){
    return apiPipelineService<number>("GetResolvedOrClosedTaskCount", {mode});
}

export function apiTaskSearch(taskId:string, conditionMode:boolean){
    console.assert(!!taskId);

    return apiPipelineService<boolean>("CanAccessTask", {taskId, isCondition:conditionMode});
}

export async function apiGetTasks(assignedToMe:boolean, taskStatus:E_TaskStatus, underwritingType:E_sCorrespondentProcessT, sortExp:string, sortDesc:boolean, conditionMode:boolean){
    return apiPipelineService<any>("GetTasks", { assignedToMe, taskStatus, underwritingType, sortExp, sortDesc, conditionMode });
}

export async function apiExportAllTasksToCSV(taskIds:string[],conditionMode:boolean, all:boolean) {
    return apiPipelineService<string>("ExportAllTasksToCSV", {
        taskIds: taskIds.join(","),
        conditionmode: conditionMode,
        all
    });
}

async function apiPipelineService<T>(op:string, data:{}){
    try {
        const respone = await fetch(`PipelineService.aspx/${op}${location.search}`, {
            method             : 'POST',
            credentials        : 'same-origin',
            headers            : { 'Content-Type': 'application/json' },
            body               : JSON.stringify(data),
        });

        if (respone.status == 500) {
            const r = (await respone.json());
            debugger;
            if (r.Message != null) return [r.Message, null] as [null|string, T|null];
            return [r, null] as [null|string, T|null];
        }

        const {d} = (await respone.json()) as {d:T};
        return [null, d] as [null|string, T|null];
    } catch(e) {
        return [e, null] as [null|string, T|null];
    }
}