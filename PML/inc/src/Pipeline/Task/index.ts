
import * as angular from "angular";
import {IController, IDirective,  ITimeoutService, IAngularEvent, IChangesObject} from "angular";
import {IframePopup, simpleDialog} from "../../components/IframePopup";

import {download} from "../../utils/download";
import {saveFileFromUrl} from "../../utils/file";

import { E_PortalMode } from "../../DataLayer/Enums/E_PortalMode";
import { E_TaskRelatedEmailOptionT } from "../../DataLayer/Enums/E_TaskRelatedEmailOptionT";
import { E_TaskConditionOption } from "./E_TaskConditionOption";
import {E_TaskStatus} from "../../DataLayer/Enums/Task/E_TaskStatus";
import {E_sCorrespondentProcessT} from "../../DataLayer/Enums/E_sCorrespondentProcessT";
import {E_EDocsEnabledStatusT} from "../../DataLayer/Enums/E_EDocsEnabledStatusT";

import {apiAssignTask, apiSetFollowupDate, apiSubscribeToTasks, apiUnsubscribeTasks, apiExportTaskAsPdf, apiGetResolvedOrClosedTaskCount, apiTaskSearch, apiGetTasks,  apiExportAllTasksToCSV} from "./api";
import ILqbAsyncPdfDownloadHelper from '../../api/ILqbAsyncPdfDownloadHelper';

import * as startOfDay from "date-fns/start_of_day";

const ls_Task_key = "PML_Pipeline_Task_config";
const ls_Condition_key = "PML_Pipeline_Condition_config";

interface ITask {
    TaskId                 : string;
    LoanId                 : string;
    AssignedUserFullName   : string;
    BorrowerNmCached       : string;
    ConditionCategory      : string;
    LoanNumCached          : string;
    OwnerFullName          : string;
    TaskDueDate            : Date|null;
    TaskFollowUpDate       : Date|null;
    TaskLastModifiedDate   : Date;
    TaskStatus_rep         : string;
    TaskSubject            : string;

    isSelected             : boolean;

    TaskDueDate_isPast     : boolean;
    TaskFollowUpDate_isPast: boolean;
    loanEditorUrl          : string;
}

interface IMyTasksVm {
    tasks                      : ITask[];
    CurrentEmployee            : {
        TaskRelatedEmailOptionT: E_TaskRelatedEmailOptionT,
    },
    BrokerDB                   : {
        EDocsEnabledStatusT    : E_EDocsEnabledStatusT,
        HideStatusAndAgentsPageInOriginatorPortal: boolean
    },
    IsNewPmlUIEnabled          : boolean,
}

declare const MyTasksVm: IMyTasksVm;
declare const LqbAsyncPdfDownloadHelper: ILqbAsyncPdfDownloadHelper;

const $ = jQuery;

import html from "./index.html";
class MyTasksController implements IController {
    // private E_PortalMode              = E_PortalMode;
    private E_TaskRelatedEmailOptionT = E_TaskRelatedEmailOptionT;

    private taskStatusOptions         = [
                                            { value: E_TaskStatus.Active  , label: "Active"   },
                                            { value: E_TaskStatus.Resolved, label: "Resolved" },
                                        ];
    private taskAssignedToOptions     = [
                                            { value: 1, label: "me"     },
                                            { value: 0, label: "anyone" },
                                        ];

    private taskStatus                : E_TaskStatus;
    private taskAssignedTo            : number;
    private taskId                    = "";

    private tasks                     : ITask[];
    private enableConditionMode       : boolean;
    private percentResolvedTasks      : number|null = null;

    static $inject = ["$element", "$timeout"];
    constructor(private $element:JQuery, private $timeout: ITimeoutService) {}

    $onInit(){
        this.loadConfig();

        apiGetResolvedOrClosedTaskCount(this.enableConditionMode ? E_TaskConditionOption.Condition : E_TaskConditionOption.Task)
        .then(([error, nResolvedTask]) => {
            if (error != null) { console.error(error); return; }
            if (nResolvedTask == null) { console.error("nResolvedTask is null"); return; }
            this.$timeout(() => {
                const maxResolvedTasks = 40;
                this.percentResolvedTasks = clamp(nResolvedTask/maxResolvedTasks * 100, 1, 100);
            });
        });

        this.refresh();
    }

    $onChange({enableConditionMode}:{enableConditionMode:IChangesObject<boolean>}){
        if (enableConditionMode != null && this.enableConditionMode != enableConditionMode.currentValue) this.$onInit();
    }

    private extends(vm:IMyTasksVm){
        const today = startOfDay(new Date());
        vm.tasks.forEach(task => {
            [
                "TaskDueDate",
                "TaskFollowUpDate",
                "TaskLastModifiedDate",
            ].forEach(k => updateDate(task, k));

            task.TaskDueDate_isPast      = task.TaskDueDate      == null ? false : startOfDay(task.TaskDueDate     ) <= today;
            task.TaskFollowUpDate_isPast = task.TaskFollowUpDate == null ? false : startOfDay(task.TaskFollowUpDate) <= today;

            task.loanEditorUrl = vm.BrokerDB.HideStatusAndAgentsPageInOriginatorPortal
                ? `${VRoot}/webapp/Loan1003.aspx?src=pipeline&p=0&loanid=${task.LoanId}`
                : `${VRoot}/webapp/StatusAndAgents.aspx?loanid=${task.LoanId}`;
        });
        this.$timeout(() => {
            Object.assign(this, vm);
        });

        function updateNumber(x:any, key:string, defaultValue:any) {
            if (x[key] == null) x[key] = defaultValue;
        }
        function updateDate(x:any, key:string) {
            if (x[key] == null) return;

            var results = /Date\(([^)]+)\)/.exec(x[key]);
            if (results != null) {
                return x[key] = new Date(parseFloat(results[1]));
            }

            var d = new Date(x[key]);
            if (Number.isNaN(d.getTime())) return;
            return x[key] = d;
        }
    }

    private async refresh(){
        const [error, vm] = await apiGetTasks(this.taskAssignedTo == 1, this.taskStatus, E_sCorrespondentProcessT.Blank, this.sortKey, this.sortDesc, this.enableConditionMode);
        if (error != null) { simpleDialog.alert(error.replace(/\n/g, "<br/>"), "Error"); return; }
        this.extends(vm);
    }

    private taskSearch(){ this._taskSearch(); return false; }
    private async _taskSearch(){
        if (!this.taskId) return;
        const [error, foundAndCanAccess] = await apiTaskSearch(this.taskId, this.enableConditionMode);

        if (error != null) { simpleDialog.alert(error.replace(/\n/g, "<br/>"), "Error"); return; }

        if (!foundAndCanAccess) {
            simpleDialog.alert(`We cannot find the specified ${this.enableConditionMode ? "condition" : "task"}.`);
            return;
        }

        IframePopup.show(`Tasks/TaskViewer.aspx?taskId=${this.taskId}`, {});
    }

    private noTaskSelected            = true;
    private allTasksSelected          = false;
    private selectAllTask(){
        this.tasks.forEach(task => task.isSelected = this.allTasksSelected);
        this.noTaskSelected   = !this.allTasksSelected;
    }
    private selectTask(task:ITask, $event:IAngularEvent){
        this.noTaskSelected   =                         this.tasks.every(task =>  !task.isSelected);
        this.allTasksSelected = !this.noTaskSelected && this.tasks.every(task => !!task.isSelected);
    }

    private openTask(task:ITask) {
        IframePopup.show(`Tasks/TaskViewer.aspx?loanId=${task.LoanId}&taskId=${task.TaskId}&IsTemplate=false`, { backdrop:'static' });
        return false;
    }

    private assignTasks($event: IAngularEvent){
        if ($event != null) {}
        this._assignTasks();
        return false;
    }
    private async _assignTasks(){
        const selectedTasks = this.tasks.filter(t => t.isSelected);
        if (selectedTasks.length < 1) return;

        const [error, result] = await IframePopup.show<{OK:boolean, id:string, type:string}>(`${VRoot}/main/Tasks/RoleAndUserPicker.aspx?IsBatch=true&IsTemplate=false&IsPipeline=true`, {});
        if (error != null) {
            console.error(error);
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            return;
        }
        if (result == null) return;
        if (!result.OK) return;

        const [err, isSuccess] = await apiAssignTask(selectedTasks.map(t => t.TaskId), result.id, result.type);
        if (err) {
            simpleDialog.alert(err, "Error");
            return;
        }
        if (isSuccess) this.refresh();
    }

    private setFollowupDate($event: IAngularEvent){
        if ($event != null) {}
        this._setFollowupDate();
        return false;
    }
    private async _setFollowupDate(){
        const selectedTasks = this.tasks.filter(t => t.isSelected);
        if (selectedTasks.length < 1) return;

        const [error, result] = await IframePopup.show<{OK:boolean, date:string}>(`${VRoot}/main/Tasks/DateSelector.aspx`, {});
        if (error != null) {
            console.error(error);
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            return;
        }
        if (result == null) return;
        if (!result.OK || !result.date) return;

        const [err, isSuccess] = await apiSetFollowupDate(selectedTasks.map(t => t.TaskId), result.date);
        if (err == null && isSuccess) this.refresh();
    }

    private subscribeTasks($event: IAngularEvent & Event){
        if ($event != null) ($event.currentTarget as HTMLButtonElement).blur();
        this._subscribeTasks();
        return false;
    }
    private async _subscribeTasks() {
        const selectedTasks = this.tasks.filter(t => t.isSelected);
        if (selectedTasks.length < 1) return;

        const isConfirm = await simpleDialog.confirm("Are you sure you want to subscribe to the selected tasks?");
        if (!isConfirm) return;
        const [error, isSuccess] = await apiSubscribeToTasks(selectedTasks.map(t => t.TaskId));
        if (error == null && isSuccess) simpleDialog.alert("You have been subscribed to the selected tasks.");
    }

    private unsubscribeTasks($event: IAngularEvent & Event){
        if ($event != null) ($event.currentTarget as HTMLButtonElement).blur();
        this._unsubscribeTasks();
        return false;
    }
    private async _unsubscribeTasks() {
        const selectedTasks = this.tasks.filter(t => t.isSelected);
        if (selectedTasks.length < 1) return;

        const isConfirm = await simpleDialog.confirm("Are you sure you want to un-subscribe from the selected tasks?");
        if (!isConfirm) return;
        const [error, isSuccess] = await apiUnsubscribeTasks(selectedTasks.map(t => t.TaskId));
        if (error == null && isSuccess) simpleDialog.alert("You have been un-subscribed from the selected tasks.");
    }

    private isExportingCsv = false;
    private exportToCsv($event: IAngularEvent & Event){
        if ($event != null) ($event.currentTarget as HTMLButtonElement).blur();
        this._exportToCsv();
        return false;
    }
    private async _exportToCsv(){
        const selectedTasks = this.tasks.filter(t => t.isSelected);
        if (selectedTasks.length < 1) return;

        this.isExportingCsv = true;
        const [error, csv] = await apiExportAllTasksToCSV(selectedTasks.map(t => t.TaskId), this.enableConditionMode, true);
        this.$timeout(()=> this.isExportingCsv = false);

        if (error) { simpleDialog.alert(error, "Error"); return; }
        download(this.enableConditionMode ? "ConditionList.csv" : "TaskList.csv", csv!, "text/csv");
    }

    private isExportingPdf = false;
    private exportToPdf($event: IAngularEvent & Event){
        this._exportToPdf();
        if ($event != null) ($event.currentTarget as HTMLButtonElement).blur();
        return false;
    }
    private async _exportToPdf(){
        const selectedTasks = this.tasks.filter(t => t.isSelected);
        if (selectedTasks.length < 1) return;

        const loanIds = Array.from(new Set(selectedTasks.map(t => t.LoanId)));

        this.isExportingPdf = true;

        const [error, filePath] = await apiExportTaskAsPdf(selectedTasks.map(t => t.TaskId), loanIds, this.enableConditionMode);
        if (error) { simpleDialog.alert(error, "Error"); }
        else {
            LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(filePath!);
        }
        this.$timeout(()=> this.isExportingPdf = false);
    }

    private sortKey:string;
    private sortDesc:boolean;
    private onSort(sortKey: string) {
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
        this.saveConfig();
    }

    private saveConfig(){
        localStorage.setItem(
            this.enableConditionMode ? ls_Condition_key : ls_Task_key,
            JSON.stringify({
                sortKey       : this.sortKey,
                sortDesc      : this.sortDesc,
                taskAssignedTo: this.taskAssignedTo,
                taskStatus    : this.taskStatus,
            }));
    }
    private loadConfig(){
        return Object.assign(
            this,
            { sortKey:"TaskDueDate", sortDesc:false, taskAssignedTo:0, taskStatus:0},
            JSON.parse(localStorage.getItem(this.enableConditionMode ? ls_Condition_key : ls_Task_key) || "{}")
        );
    }

    private onTaskFilterChange(){
        this.saveConfig();
        this.refresh();
        return false;
    }
}
export function myTasksDirective(): IDirective {
    return {
        restrict               : "EA",
        controller             : MyTasksController,
        controllerAs           : "vm",
        scope                  : {
            enableConditionMode: "<",
            portalMode         : "<",
        },
        bindToController       : true,
        template               : html,
    };
}




function clamp(x:number, min:number, max:number) {
    return Math.max(min, Math.min(max, x));
}
