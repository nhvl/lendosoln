import {ITimeoutService, IDirective} from "angular";

import { E_PortalMode             } from "../../DataLayer/Enums/E_PortalMode";
import { E_sRateLockStatusT       } from "../../DataLayer/Enums/E_sRateLockStatusT";
import { E_sStatusT               } from "../../DataLayer/Enums/E_sStatusT";
import { E_sCorrespondentProcessT } from "../../DataLayer/Enums/E_sCorrespondentProcessT";

import {ILoan, normalizeLoans} from "../ILoan";

interface IPipelineLoansVm {
    loans                : ILoan[];
    IsUseTasksInTpoPortal: boolean;
    PortalMode           : E_PortalMode;
    isDuplicateResult: boolean;
    HideStatusAndAgentsPageInOriginatorPortal: boolean;
}
declare const PipelineLoansVm: IPipelineLoansVm;

const $ = jQuery;

import html from "./index.html";
import { postJsonSafeD } from "../../utils/fetch";
class PipelineLoanController {
    private E_PortalMode       = E_PortalMode;
    private E_sRateLockStatusT = E_sRateLockStatusT;
    private E_sStatusT         = E_sStatusT;

    private loans: ILoan[];
    private IsUseTasksInTpoPortal: boolean;
    private PortalMode: E_PortalMode;

    private showTopX = 25;
    private showTopXOptions = [25, 100];

    private underwritingType = E_sCorrespondentProcessT.Blank;

    private sLNm                   : string = "";
    private aBLastNm               : string = "";
    private aBSsn                  : string = "";
    private sStatusD               = -1;
    private sStatusT               = -1;
    private loanAssignedToT        = 0;
    private loanAssignedToTOptions = [{label: "me", value: 1}, {label: "anyone", value: 0}];

    private sortKey:string="sStatusD";
    private sortDesc:boolean;
    private onSort(sortKey: string) {
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
    }

    static $inject = ["$element", "$timeout"];
    constructor(private $element: JQuery, private $timeout: ITimeoutService, private IsLeadPipeline: boolean = false) {
        this.sortKey = "sStatusD";
        this.sortDesc = true;

        this.PortalMode = Number($("#TPOMode").val());

		this._fetchDefault();
    }

    private extends(vm:IPipelineLoansVm){
        vm.loans = normalizeLoans(vm.loans);
        Object.assign(this, vm);
    }

    private searchClick(event: Event) {
        this._fetchSearch();
        return false;
    }

    private onFilterChange(){
        this._fetchDefault();
        // if (
        //     (this.sLNm     == "") &&
        //     (this.aBLastNm == "") &&
        //     (this.aBSsn    == "") &&
        //     (this.sStatusD == -1) &&
        //     (this.sStatusT == -1) &&
        //     true
        // )
        // this._fetchDefault();
        // else this._fetchSearch();
    }

    private async _fetchDefault() {
        const [err, d] = await apiFetch("ListLoanDefault", {
            count          : this.showTopX,
            topRowsToPull  : null,
            process        : this.underwritingType,
            PortalMode     : this.PortalMode,
            LoanAssignedToT: this.loanAssignedToT,
            IsLeadPipeline : this.IsLeadPipeline
        });
        if (err != null) { console.error(err); return; }
        this.$timeout(() => { this.extends(d); });
    }
    private async _fetchSearch() {
        const [err, d] = await apiFetch("ListLoanSearch", {
            process        : this.underwritingType,
            PortalMode     : this.PortalMode,
            aBLastNm       : this.aBLastNm,
            aBSsn          : this.aBSsn,
            sLNm           : this.sLNm,
            sStatusT       : this.sStatusT,
            sStatusDateT   : this.sStatusD,
            LoanAssignedToT: this.loanAssignedToT,
            IsLeadPipeline : this.IsLeadPipeline
        });
        if (err != null) { console.error(err); return; }
        this.$timeout(() => { this.extends(d); });
    }

    private clearCriteriaClick(){
        this.sLNm     = "";
        this.aBLastNm = "";
        this.aBSsn    = "";
        this.sStatusD = -1;
        this.sStatusT = -1;

        this._fetchDefault();

        return false;
    }
}

export function pipelineLoanDirective(): IDirective {
    return {
        restrict     : "EA",
        controller   : PipelineLoanController,
        controllerAs : "vm",
        template: html,
    };
}

class PipelineLeadController extends PipelineLoanController {
    constructor($element: JQuery, $timeout: ITimeoutService) {
        super($element, $timeout, true);
    }
}

export function pipelineLeadDirective(): IDirective {
    return {
        restrict: "EA",
        controller: PipelineLeadController,
        controllerAs: "vm",
        template: html,
    };
}

async function apiFetch(op: "ListLoanSearch" | "ListLoanDefault", data:{}){
    return postJsonSafeD<IPipelineLoansVm>(`${location.pathname}/${op}${location.search}`, data);
}
