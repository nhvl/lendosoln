import {IDirective} from "angular";

import html from "./create-loan.html";
export function createLoanDirective(): IDirective {
    return {
        restrict     : "EA",
        scope        : false,
        template     : html,
    };
}