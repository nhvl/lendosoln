import * as angular from "angular";
import {IDirective, ITimeoutService, ISCEService, IAngularEvent} from "angular";

import {simpleDialog} from "../../components/IframePopup";
import {eventOnce} from "../../utils/eventOnce";
import {readFileAsText, readFileAsBase64} from "../../utils/file";

import { E_PortalMode } from "../../DataLayer/Enums/E_PortalMode";
import {E_sCorrespondentProcessT} from "../../DataLayer/Enums/E_sCorrespondentProcessT";
import {ILoan, normalizeLoans} from "../ILoan";
import {IServiceResult, pmlService} from "../../api/gService";
const $ = jQuery;

let $timeout: ITimeoutService;

declare const pipelineVm: {
    BrokerDB: {
        IsAllowImportDoDuInPml: boolean,
        IsPmlPointImportAllowed: boolean,
        IsUseTasksInTpoPortal: boolean,
        AllowImportFromLpaInPml: boolean,
        HideStatusAndAgentsPageInOriginatorPortal: boolean
    },
    PriceMyLoanUser: {
        IsPricingMultipleAppsSupported: boolean,
        DoDuLoginNm: string,
        CanCreateLoan: boolean,
    },
    LpaCredentials: {
        HasLpaSellerCredentials: boolean,
        HasLpaUserCredentials: boolean
    }
}

import html from "./index.html";
import { formatSsn } from "../../utils/formatter";
import { postJson, postJsonSafeD } from "../../utils/fetch";
class PipelineController {
    private E_PortalMode              = E_PortalMode;
    private E_sCorrespondentProcessT  = E_sCorrespondentProcessT;

    // loan pg=0 | task pg=1 | conditions pg=2 | create pg=3 | import pg=4 | lead pg=5 | report pg=6
    private pg                   : 0|1|2|3|4|5|6;
    private borrowerCacheId      : string|null;
    private PortalMode           : E_PortalMode;

    private dashboardUrl         : string|null;
    private showLoading          : boolean = false;

    static $inject = ["$element", "$timeout", "$sce"];
    constructor(private $element:JQuery, _$timeout: ITimeoutService, private $sce:ISCEService) {
        $timeout = _$timeout;

        const qs = new URLSearchParams(location.search);
        this.pg = !qs.has("pg") ? 0 : (Number(qs.get("pg"))||0 as any);
        this.borrowerCacheId = !qs.has("eventid") ? null : qs.get("eventid");
        this.PortalMode = Number($("#TPOMode").val());

        Object.assign(this, pipelineVm);

        (window as any).pipelineCtrl = this;
    }

    private navTo(pg:0|1|2|5|6) {
        if ((0 <= pg && pg < 3) || pg == 5 || pg == 6) {
            $timeout(() => {
                this.dashboardUrl = null;
                this.pg = pg;
                window.history.pushState(null, "", `${location.pathname}?pg=${pg}`);
            });
        }
    }

    private showDashboard: (url:String) => void;
    private setDashboardIframe(iframe:HTMLIFrameElement&HTMLEmbedElement) {
        if (this.showDashboard != null) {debugger; return;}

        window.iFrameScrollPosition = 0;

        this.showDashboard = async (url:String) => {
            // Double assign is for trigger reload from Iframe.
            // if `iframe.src == url` then setting `iframe.src = url` won't reload the page even the iframe has been redirect to other page.
            $timeout(() => this.dashboardUrl = this.$sce.trustAsResourceUrl(""));
            $timeout(() => this.dashboardUrl = this.$sce.trustAsResourceUrl(url), 100);

            iframe.hidden = true;
            this.showLoading = true;
            const loadEvent = await eventOnce(iframe, "load");
            const isFullIframe = $('html').hasClass('full-iframe-container');
            iframe.style.height = "";
            $timeout(() => { this.showLoading = false });
            iframe.hidden = false;
            if (window.iFrameScrollPosition == 0) iframe.scrollTop = 0;
            return iframe;
        };

        iframe.addEventListener("readystatechange", () => {
            iframe.style.height = "";
            if (iframe.readyState == "complete" || iframe.readyState == "interactive") {
                $timeout(() => { this.showLoading = false; });
                iframe.hidden = false;
                if (window.iFrameScrollPosition == 0) iframe.scrollTop = 0;
            }
        });

        window.addEventListener("message", function(e){
            const minHeight = 500;
            const threshold = 0;

            const data = e.data;
            if (typeof data == "string" && data.match(/^HEIGHT:/)) {
                const h = Math.max( Number(data.substring("HEIGHT:".length)), minHeight);
                if (Math.abs(iframe.offsetHeight - h) > threshold) {
                    iframe.style.height = `${h}px`;
                }
            }
            if (window.iFrameScrollPosition > 0 && iframe.offsetHeight > 0) {
                window.scrollTo(0, window.iFrameScrollPosition);
                window.iFrameScrollPosition = 0;
            }
        });

        const isLoadingDashboard = (!!(window as any).isLoadingDashboard) || new URLSearchParams(location.search).has("isLoadingDashboard");
        if (!isLoadingDashboard) return;
        const selectedLink = document.querySelector<HTMLAnchorElement>(`a.selected[target="MainPipelineFrame"]`);
        if (selectedLink == null || !selectedLink.href) return;
        this.showDashboard(selectedLink.href);
    }

    private createLoanHeader:string="Create Loan";
    private loanType:ICreateLoanType;
    private aBFirstNm:string;
    private aBLastNm:string;
    private aBSsn:string;
    private duplicatedLoans:ILoan[]|null;
    private beginCreateLoan(loanType:ICreateLoanType, header:string) {
        $timeout(() => {
            // clear error message
            this.createLoanHeader = header;
            this.loanType = loanType;
            this.aBFirstNm = this.aBLastNm = this.aBSsn = "";
            this.duplicatedLoans = null;
            this.pg = 3;
            this.dashboardUrl = null;
        });
    }
    private onCreateLoan(skipdupeCheck:boolean, event:IAngularEvent){
        if (!this.aBFirstNm || !this.aBLastNm || !this.aBSsn) return;
        this._onCreateLoan(skipdupeCheck);
        event.preventDefault(); return false;
    }
    private async _onCreateLoan(skipdupeCheck:boolean){
        this.showLoading = true;

        const [error, sLId] = await apiCreateNewLoan(
            this.aBFirstNm, this.aBLastNm, this.aBSsn, this.loanType,
            this.PortalMode, this.borrowerCacheId, skipdupeCheck);

        if (error != null) {
            $timeout(() => this.showLoading = false);

            if (typeof error == "string") {
                simpleDialog.alert(error, "Error");
                return;
            }

            $timeout(() => this.duplicatedLoans = normalizeLoans(error));

            return;
        }

        goToLoan(sLId!);
    }
    private cancelLoanCreation(){
        this.navTo(0);
    }
    private onChange_aBSsn(){
        this.aBSsn = formatSsn(this.aBSsn);
    }

    private importType: 0|1|2|3;
    private _fmFile:HTMLInputElement;
    private isFmFileInvalidExt:boolean = false;
    private fmAgree: boolean = false;
    private isLead: boolean = false;
    private showImport(isLead:boolean){
        $timeout(() => {
            this.importType              = 0;

            this.isFmFileInvalidExt      = false;
            this.fmAgree                 = false;

            this.isCpFileInvalidExt      = false;
            this.isImportCpCb            = false;
            this.cpCbFiles               = [];
            this.cpCbFileValid           = true;
            this.cpAgree                 = false;

            this.sDuCaseId               = "";
            this.FannieMaeMORNETPassword = "";
            this.FannieMaeMORNETUserID   = pipelineVm.PriceMyLoanUser.DoDuLoginNm || "";
            this.rememberLogin           = !!this.FannieMaeMORNETUserID;
            this.isImportCredit          = true;
            this.duAgree                 = false;

            this.pg                      = 4;
            this.dashboardUrl            = null;
            this.showLoading = false;

            this.LpaLoanId = "";
            this.LpaUserId = "";
            this.LpaUserPassword = "";
            this.LpaSellerNumber = "";
            this.LpaPassword = "";
            this.LpaAusKeyNumber = "";
            this.LpaNotpNumber = "";
            this.LpaImportCreditReport = false;
            this.LpaAgree = false;

            this.isLead = isLead;
        });
    }
    private onFmFileChange(file:HTMLInputElement){
        this._fmFile = file;
        const fileName = this._fmFile.value;
        this.isFmFileInvalidExt = !!fileName && (fileName.match(/\.(fnm|dat|exp|imp|\ddu)$/i) == null);
    }
    private importFm(skipDupCheck:boolean){
        if (this._fmFile == null || !this._fmFile.value || !this.fmAgree) return;

        this._importFm(skipDupCheck);
    }
    private async _importFm(skipDupCheck: boolean){
        // m_FNMImportBtn
        this.showLoading = true;

        const [error, fnmaContent] = await readFileAsText(this._fmFile);
        if (error) {
            $timeout(() => this.showLoading = false);
            simpleDialog.alert(error, "Error");
            return;
        }

        if (!fnmaContent) {
            $timeout(() => this.showLoading = false);
            simpleDialog.alert(`"${this._fmFile.value}" does not exist. Please try again.`);
            return;
        }

        const [err, sLId] = await apiImportFannieMae(fnmaContent!, this.PortalMode, skipDupCheck, this.isLead);
        if (err != null) {
            $timeout(() => this.showLoading = false);

            if (typeof err == "string") {
                const fileName = this._fmFile.value.split("\\").pop();
                const e =  (err.startsWith(" is")) ? fileName +  err : err;
                simpleDialog.alert(e, "Error");
                return;
            }

            this.duplicatedLoans = err;
            return;
        }

        goToLoan(sLId!);
    }

    private _cpFile           : HTMLInputElement;
    private isCpFileInvalidExt: boolean = false;
    private isImportCpCb      : boolean = false;
    private cpCbFiles         : {file   : HTMLInputElement|null, invalidExt: boolean, isDuplicated: boolean}[] = [];
    private cpCbFileValid     : boolean = true;
    private cpAgree           : boolean;
    private onCpFileChange = (file:HTMLInputElement) => {
        this._cpFile = file
        const fileName = this._cpFile.value;
        this.isCpFileInvalidExt = !!fileName && (fileName.match(/\.(brw|prs)$/i) == null);
    }
    private onCpCbFileChange(file:HTMLInputElement, cbFile:{file: HTMLInputElement|null, invalidExt:boolean, isDuplicated:boolean}){
        cbFile.file = file;
        cbFile.invalidExt = cbFile.file.value != null && (cbFile.file.value.match(/\.cb[1-5]$/i) == null);

        this.cpCbFiles.forEach((cf, i) => {
            if (i < 1) return;
            if (cf.file == null) return;
            cf.isDuplicated = this.cpCbFiles.slice(0, i).find(x => x.file != null && x.file.value == cf.file!.value) != null;
        });

        this.cpCbFileValid = this.cpCbFiles.every(f => !f.invalidExt && !f.isDuplicated);
    }
    private removeCpCb(cbFile:any){
        this.cpCbFiles = this.cpCbFiles.filter(x => x != cbFile);
    }
    private addCpCb(){
        if (this.cpCbFiles.length > 4) return;
        this.cpCbFiles.push({ file:null, invalidExt:false, isDuplicated:false });
    }
    private toggleCpCb(){
        if (this.isImportCpCb) this.addCpCb();
        else this.cpCbFiles = [];
    }
    private importCp(){
        if (!this._cpFile.value || !this.cpAgree) return;

        this._importCp();
    }
    private async _importCp(){
        this.showLoading = true;

        const [error, calyx] = await readFileAsBase64(this._cpFile);
        if (error) {
            $timeout(() => this.showLoading = false);
            simpleDialog.alert(error, "Error");
            return;
        }
        if (!calyx) {
            $timeout(() => this.showLoading = false);
            simpleDialog.alert(`"${this._cpFile.value}" does not exist. Please try again.`);
            return;
        }

        const cbfs:string[] =
            (await Promise.all(this.cpCbFiles.filter(f => f.file!= null && !!f.file.value).map(f => readFileAsBase64(f.file!))))
            .filter(([error, cb]) => error == null && !!cb).map(([error, cb]) => cb!);

        const [err, sLId] = await apiImportCalyxPoint(calyx!, cbfs, this.PortalMode, this.isLead);
        if (err != null) {
            $timeout(() => this.showLoading = false);

            const fileName = this._cpFile.value;
            const e =  (err.startsWith(" is")) ? fileName +  err : err;
            simpleDialog.alert(e, "Error");
            return;
        }

        goToLoan(sLId!);
    }

    private LpaLoanId: string;
    private LpaUserId: string;
    private LpaUserPassword: string;
    private LpaSellerNumber: string;
    private LpaPassword: string;
    private LpaAusKeyNumber: string;
    private LpaNotpNumber: string;
    private LpaImportCreditReport: boolean;
    private LpaAgree: boolean;
    private LpaImportBtnDisabled() {
    return !this.LpaLoanId || !this.LpaAgree ||
           (!pipelineVm.LpaCredentials.HasLpaUserCredentials && (!this.LpaUserId || !this.LpaUserPassword)) ||
           (!pipelineVm.LpaCredentials.HasLpaSellerCredentials && (!this.LpaSellerNumber || !this.LpaPassword));
    }

    private ImportLpaLoan() {
        if (this.LpaImportBtnDisabled()) {
            simpleDialog.alert("Please fill out all required fields.", "Error");
            return;
        }

        var data = {
            LpaLoanId: this.LpaLoanId,
            LpaUserId: this.LpaUserId,
            LpaUserPassword: this.LpaUserPassword,
            LpaSellerNumber: this.LpaSellerNumber,
            LpaPassword: this.LpaPassword,
            LpaAusKey: this.LpaAusKeyNumber,
            LpaNotpNum: this.LpaNotpNumber,
            ImportCredit: this.LpaImportCreditReport,
            PortalMode: this.PortalMode,
            IsLead: this.isLead,
        };

        this.showLoading = true;
        pmlService.ImportLpaLoanAsync(data,
            (value: any) => {
                $timeout(() => this.showLoading = false);
                if (value.Error) {
                    simpleDialog.alert(value.Error, "Error");
                }
                else {
                    goToLoan(value.LoanId);
                }
            },
            (error: string) => {
                $timeout(() => this.showLoading = false);
                simpleDialog.alert(error, "Error")
            });
    }

    private sDuCaseId              : string;
    private FannieMaeMORNETUserID  : string;
    private FannieMaeMORNETPassword: string;
    private rememberLogin          : boolean;
    private isImportCredit         : boolean = true;
    private duAgree                : boolean;
    private importDu(skipDupCheck:boolean){
        if (!this.sDuCaseId || !this.FannieMaeMORNETUserID || !this.FannieMaeMORNETPassword || !this.duAgree) return;

        this._importDu(skipDupCheck);
    }

    private async _importDu(skipDupCheck:boolean){
        this.showLoading = true;
        const [err, sLId] = await apiImportDoDu(this.sDuCaseId, this.FannieMaeMORNETUserID, this.FannieMaeMORNETPassword, this.rememberLogin, this.isImportCredit, this.PortalMode, skipDupCheck, this.isLead);

        if (err != null) {
            $timeout(() => this.showLoading = false);

            if (typeof err == "string") {
                simpleDialog.alert(err, "Error");
                return;
            }

            this.duplicatedLoans = err;
            return;
        }

        goToLoan(sLId!);
    }
}
export function pmlPipelineDirective(): IDirective {
    return {
        restrict     : "EA",
        controller   : PipelineController,
        controllerAs : "vm",
        template     : html,
    };
}

function goToLoan(sLId: StrGuid){
    if (!sLId) { return; debugger; }
    sLId = encodeURIComponent(sLId);
    window.location.href =
        `../webapp/Loan1003.aspx?src=pipeline&loanid=${sLId}&isnew=t`
        // `ImportLoanDataAudit.aspx?loanid=${sLId}` // showImportAudit
        ;

}
function downloadCoverSheets(sLId: StrGuid){
    if (!sLId) { return; debugger; }
    sLId = encodeURIComponent(sLId);
    window.location.href = `pipeline.aspx?loanid=${sLId}&fax=1`;
}

export type ICreateLoanType = "PURCHASE" | "REFINANCE" | "HELOC_1STLIEN" | "HELOC_2NDLIEN" | "SECONDLIEN";
export async function apiCreateNewLoan(
    aBFirstNm:string, aBLastNm:string, aBSsn:string,
    loanType:ICreateLoanType,
    portalMode: E_PortalMode,
    borrowerCacheId: StrGuid|null,
    skipDupCheck: boolean = false,
): Promise<[string|ILoan[]|null, StrGuid|null]> {
    const res = await apiPipeline<[string|ILoan[]|null, StrGuid|null]>("CreateNewLoan", {
        aBFirstNm, aBLastNm, aBSsn, loanType,
        portalMode,
        borrowerCacheId,
        skipDupCheck,
    });
    const [error, d] = res;
    return error != null ? ([error, d as any]) as any : d;
}

export async function apiImportFannieMae(
    fnmaContent:string,
    portalMode: E_PortalMode,
    skipDupCheck: boolean,
    isLead: boolean,
): Promise<[string|ILoan[]|null, StrGuid|null]> {
    const res = await apiPipeline<[string|ILoan[]|null, StrGuid|null]>("ImportFannieMae", {
        fnmaContent,
        portalMode,
        skipDupCheck,
        isLead,
    });
    const [error, d] = res;
    return error != null ? ([error, d as any]) as any : d!;
}

export async function apiImportCalyxPoint(
    calyxBase64:string,
    cbfs:string[]|null,
    portalMode: E_PortalMode,
    isLead: boolean,
): Promise<[string|null, StrGuid|null]> {
    const res = await apiPipeline<[string|null, StrGuid|null]>("ImportCalyxPoint", {
        calyxBase64,
        cbfs,
        portalMode,
        isLead
    });
    const [error, d] = res;
    return error != null ? ([error, d as any]) as any : d;
}

export async function apiImportDoDu(
    sDuCaseId           : string,
    userId              : string,
    password            : string,
    rememberLogin       : boolean,
    isImportCreditReport: boolean,
    portalMode          : E_PortalMode,
    skipDupCheck        : boolean,
    isLead              : boolean,
): Promise<[string|ILoan[]|null, StrGuid|null]> {
    const res = await apiPipeline<[string|ILoan[]|null, StrGuid|null]>("ImportDoDu", {
        sDuCaseId,
        userId,
        password,
        rememberLogin,
        isImportCreditReport,
        portalMode,
        skipDupCheck,
        isLead,
    });
    const [error, d] = res;
    return error != null ? ([error, d as any]) : d!;
}

async function apiPipeline<T>(op: string, data: {}): Promise<[any, T]>{
    return postJsonSafeD<T>(`${location.pathname}/${op}${location.search}`, data);
}

/*
Deprecate
hasDuplicate WHEN import
    setTimeout(alert("Warning:  Your import will expire in one minute.");, 13.5*60*1000);
    setTimeout(hideCreateNextBtn, 15*60*1000);

    don't need these since we don't use cache anymore
*/
