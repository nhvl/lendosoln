import { IDirective } from "angular";

import importFmHtml from "./import-fm.html";
export function importFmDirective(): IDirective {
    return {
        restrict: "EA",
        scope   : false,
        template: importFmHtml,
    };
}

import importCpHtml from "./import-cp.html";
export function importCpDirective(): IDirective {
    return {
        restrict: "EA",
        scope   : false,
        template: importCpHtml,
    };
}

import importDoHtml from "./import-do.html";
export function importDoDirective(): IDirective {
    return {
        restrict: "EA",
        scope   : false,
        template: importDoHtml,
    };
}

import importLoanHtml from "./import-loan.html";
export function importLoanDirective(): IDirective {
    return {
        restrict: "EA",
        scope   : false,
        template: importLoanHtml,
    };
}

import retrieveLpaHtml from "./retrieve-lpa.html";
export function retrieveLpaDirective(): IDirective {
    return {
        restrict: "EA",
        scope: false,
        template: retrieveLpaHtml,
    };
}
