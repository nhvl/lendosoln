import * as angular from "angular";

import {loanTableComponent   } from "./LoanTable";

import {pipelineLoanDirective} from "./Loan";
import {pipelineLeadDirective} from "./Loan";
import {myTasksDirective     } from "./Task";
import {pmlPipelineDirective } from "./pipeline";
import { createLoanDirective } from "./pipeline/create-loan";
import { importFmDirective, importCpDirective, importDoDirective, importLoanDirective, retrieveLpaDirective } from "./pipeline/import-loan";
import {pipelineReportDirective} from "./Report";

angular.module("PipelineLoan", ["TpoCommon"])

.component(loanTableComponent    .ngName, loanTableComponent    )

.directive("pipelineLoan", pipelineLoanDirective)
.directive("pipelineLead", pipelineLeadDirective)
.directive("myTasks", myTasksDirective)

.directive("importFm", importFmDirective)
.directive("importCp", importCpDirective)
.directive("importDo", importDoDirective)
.directive("importLoan", importLoanDirective)
.directive("retrieveLpa", retrieveLpaDirective)    

.directive("createLoan", createLoanDirective)

.directive("pmlPipeline", pmlPipelineDirective)

.directive("pipelineReport", pipelineReportDirective)
;

document.addEventListener("DOMContentLoaded", () => {
    const logoutBtn = (document.getElementById("LogoutButton") as HTMLAnchorElement);
    if (logoutBtn == null) { return; }

    logoutBtn.addEventListener("click", () => {
        let w = window.parent || window;
        // while (w.parent != w) w = w.parent;

		w.location.href = `${gVirtualRoot}/logout.aspx`;
        return false;
    });
});
