import { IDirective, ITimeoutService, IScope } from 'angular';
import { postJsonSafeD } from '../../utils/fetch';
import { IframePopup, simpleDialog } from '../../components/IframePopup';
import { E_ReportExtentScopeT } from '../../DataLayer/Enums/E_ReportExtentScopeT'; 
import { E_ClassType } from '../../DataLayer/Enums/E_ClassType'; 

interface IOriginatorPortalPipelineReportModel {
	Title: string;
	TotalRecordCount: number;
	MaxViewableRecords: number;
	Tables: IOriginatorPortalPipelineReportTableModel[];
	ColumnTypesByIndex: { [index: string] : E_ClassType }
}

interface IOriginatorPortalPipelineReportTableModel {
	Header: IOriginatorPortalPipelineReportRowModel;
	Rows: IOriginatorPortalPipelineReportRowModel[];
	CalculationTable: IOriginatorPortalPipelineReportTableModel;
}

interface IOriginatorPortalPipelineReportRowModel {
	LoanId: string;
	Cells: IOriginatorPortalPipelineReportCellModel[];
}

interface IOriginatorPortalPipelineReportCellModel {
	Text: string;
	AlignRight: boolean;
}

interface IAvailableReportScope {
	Name: string;
	Value: E_ReportExtentScopeT;
}

interface IRunPipelineReportResult {
	JobId: string;
	PollingInterval: number;
	MaxPollAttempts: number;
}

interface ICheckReportStatusResult {
	HasError: boolean;
	UserLacksReportRunPermission: boolean;
	IsComplete: boolean;
    RetailMode: boolean;
    HideStatusAndAgentsPageInOriginatorPortal: boolean;
	ReportModel: IOriginatorPortalPipelineReportModel
}

const $ = jQuery;

import html from './index.html';
class PipelineReportController {
	static $inject = ['$element', '$timeout', '$scope'];
	private availableReportScopes: IAvailableReportScope[] = [
		{ Name: 'me', Value: E_ReportExtentScopeT.Assign },
		{ Name: 'anyone', Value: E_ReportExtentScopeT.Default }
	];
	
	private readonly moneyRegex = /^\(?\$?-?\d+(,\d{3})*(\.\d*)?\)?$/;
	private readonly timeOnlyDateRegex = /^(0?[1-9]|1[012]):[0-5][0-9]\s*(a|p)m$/i;
	private readonly epochDateString = '1/1/1970';
	private readonly invalidDateString = '1/1/1901';
	
	private reportScope : IAvailableReportScope = this.availableReportScopes[0];
    private showLoading: boolean;
    private userErrorMessage: string;
	private jobId: string;
	private pollingHandle: number;
	private maxPollAttempts: number;
	private pollAttempts: number = 0;
	private reportId : string;
	private sortKey : number  = 0;
    private sortDesc : boolean = true;
	private columnTypesByIndex: { [index: string] : E_ClassType };
	private cellParseFunction: (input: string) => any;
    private cellCompareFunction: (first: any, second: any) => number;
    private HideStatusAndAgentsPageInOriginatorPortal: boolean;
	
	constructor(private $element:JQuery, private $timeout: ITimeoutService, private $scope: IScope) {
		const params = new URLSearchParams(location.search);
		if (!params.has('reportId')) {
			throw 'Missing report ID.';
		}
		
		this.reportId = params.get('reportId') as string;
		this.runPipelineReport();
	}
	
	private async editReportTitle() {
		const url = `${VRoot}/webapp/EditPipelineReportTitle.aspx?reportId=${this.reportId}`;
		const [error, result] = await IframePopup.show<{OK:boolean, Title:string}>(url, {});
		
		if (error != null) {
            simpleDialog.alert(typeof error === 'string' ? error : 'Unable to edit report title. Please try again.', 'Error');
            return;
        }
		
		if (result === null || !result.OK) {
			return;
		}
		
		const [saveError] = await apiFetch<string>('EditReportTitle', { reportId: this.reportId, newTitle: result.Title });
		if (saveError != null) {
			simpleDialog.alert(typeof error === 'string' ? error : 'Unable to edit report title. Please try again.', 'Error');
            return;
		}
		
		$('#PipelineReportTitle').text(result.Title);
		$(`.sidebar #nav a[data-node='${this.reportId}']`).text(result.Title);
	}
	
	private async runPipelineReport() {
		this.showLoading = true;
		this.pollAttempts = 0;
			
		const args = { 
			reportId: this.reportId,
			reportScope: this.reportScope.Value
		};
        const [err, result] = await apiFetch<IRunPipelineReportResult>('RunPipelineReport', args);
		
        if (err != null || typeof result === 'undefined') { 
			this.showLoading = false;
			simpleDialog.alert(typeof err === 'string' ? err : 'Unable to run report. Please refresh the page.', 'Error'); 
			return; 
		}
		
		this.jobId = result.JobId;
		this.maxPollAttempts = result.MaxPollAttempts;
		
		const pollingIntervalMillis = result.PollingInterval * 1000;
		this.pollingHandle = window.setInterval(this.checkReportStatus.bind(this), pollingIntervalMillis) as number;
    }
	
	private async checkReportStatus() {
		this.pollAttempts++;
		const args = {
			jobId: this.jobId
		}
		
		const [err, result] = await apiFetch<ICheckReportStatusResult>('CheckReportStatus', args);
		if (err != null || typeof result === 'undefined' || result.HasError || this.pollAttempts > this.maxPollAttempts) { 
			window.clearInterval(this.pollingHandle);
			this.showLoading = false;
			
			const message = result && result.UserLacksReportRunPermission 
				?  'You do not have the permissions required to run this report.' 
				: typeof err === 'string' ? err : 'Unable to run report. Please refresh the page.';
				
			await simpleDialog.alert(message, 'Error'); 
			
			if (result && result.UserLacksReportRunPermission) {
				window.location.href = `${VRoot}/main/pipeline.aspx?pg=0`;
			}
			return; 
		}
		
		if (!result.IsComplete) {
			return;
		}
		
		window.clearInterval(this.pollingHandle);
		
		this.columnTypesByIndex = result.ReportModel.ColumnTypesByIndex;
		this.setValueComparer();
		this.$scope.$apply(() =>
		{
			this.showLoading = false;
			this.extends(result);
			window.setTimeout(this.normalizeTableWidths, 0);
		});
	}
	
	private setValueComparer() {
        const columnType = this.columnTypesByIndex[this.sortKey];
        if (columnType == undefined) {
            this.userErrorMessage = "Nothing to show. Your pipeline is empty.";
            return;
        }

		this.cellCompareFunction = (first: number, second: number) => first > second ? -1 : second > first ? 1 : 0;
		
		switch (columnType) {
			case E_ClassType.Txt:
			case E_ClassType.LongText:
				this.cellParseFunction = (input: string) => input;
				this.cellCompareFunction = (first: string, second: string) => first.localeCompare(second);
				break;
			case E_ClassType.Pct:
			case E_ClassType.Cnt:
				this.cellParseFunction = this.parseCount;
				break;
			case E_ClassType.Csh:
				this.cellParseFunction = this.parseCash;
				break;
			case E_ClassType.Cal:
				this.cellParseFunction = this.parseCalendar;
				break;
			case E_ClassType.Invalid:
			default:
				throw 'Unhandled column type ' + columnType;
		}
	}
	
	private parseCount(input: string) : number {
		if (typeof input === 'undefined' || input.length === 0) {
			return -1;
		}
		
		return parseFloat(input);
	}
	
	private parseCash(input: string) : number {
		if (typeof input === 'undefined' || input.length === 0) {
			return -1;
		}
	
		if (this.moneyRegex.test(input)) {
			const inputAsNumber = Number(input.replace(/[^0-9\.-]+/g, ''));
			const isNegative = input.indexOf('(') !== -1;
			return isNegative ? (-1 * inputAsNumber) : inputAsNumber;
		}
		
		return 0;
	}
	
	private parseCalendar(input: string) : Date {
		if (typeof input === 'undefined' || input.length === 0) {
			return new Date(this.invalidDateString);
		}
		
		if (this.timeOnlyDateRegex.test(input)) {
			// This "date" is time-only. Use the same
			// date and compare the times.
			return new Date(this.epochDateString + ' ' + input);
		}
	
		const inputAsDate = new Date(input);
		if (!isNaN(inputAsDate.getTime())) {
			return inputAsDate;
		}
		
		return new Date(this.invalidDateString);
	}
	
	private orderTableRow(row: IOriginatorPortalPipelineReportRowModel) : string {
		return row.Cells[this.sortKey].Text;
	}
	
	private cellContentComparer(firstObj: { value: string; type : string }, secondObj: { value: string; type: string }) : number {
		const firstValue = firstObj.value;
		const secondValue = secondObj.value;
		
		// Tie-breaker
		if (firstObj.type === 'number' || secondObj.type === 'number') {
			const firstAsNumber = Number(firstValue);
			const secondAsNumber = Number(secondValue);
			
			if (firstAsNumber > secondAsNumber) {
				return -1;
			}
			
			return secondAsNumber > firstAsNumber ? 1 : 0;
		}
		
		const firstValueParsed = this.cellParseFunction(firstValue);
		const secondValueParsed = this.cellParseFunction(secondValue);
		return this.cellCompareFunction(firstValueParsed, secondValueParsed);
	}
	
    private onSort(sortKeyRep: string) {
		const sortKey = parseInt(sortKeyRep);
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
		
		this.setValueComparer();
    }
	
	private extends(vm:ICheckReportStatusResult) {
        Object.assign(this, vm);
    }
	
	private normalizeTableWidths() {
		let max = 0;    
		
		// Do not normalize the first column, which contains the edit icon
		// and is a fixed width.
		$('.PipelineReportGrid td:not(:first-child)').each(function(index, element) {
			max = Math.max($(element).width() as number, max);
		}).width(max);
	}
	
    private getLoanEditorUrl(row: IOriginatorPortalPipelineReportRowModel) { 
        if (this.HideStatusAndAgentsPageInOriginatorPortal) {
            return `${VRoot}/webapp/Loan1003.aspx?src=pipeline&p=0&loanid=${row.LoanId}`
        }

		return `${VRoot}/webapp/StatusAndAgents.aspx?loanid=${row.LoanId}`; 
	}
}

export function pipelineReportDirective(): IDirective {
    return {
        restrict     : 'EA',
        controller   : PipelineReportController,
        controllerAs : 'vm',
        template: html,
    };
}

async function apiFetch<T>(method:string, data:{}){
    return postJsonSafeD<T>(`${location.pathname}/${method}${location.search}`, data);
}