import * as angular from "angular";
import {IComponentOptions} from "angular";

import { E_PortalMode             } from "../../DataLayer/Enums/E_PortalMode";
import { E_sRateLockStatusT       } from "../../DataLayer/Enums/E_sRateLockStatusT";
import { E_sStatusT               } from "../../DataLayer/Enums/E_sStatusT";
import { E_sCorrespondentProcessT } from "../../DataLayer/Enums/E_sCorrespondentProcessT";

import {ILoan} from "../ILoan";
import ILqbAsyncPdfDownloadHelper from '../../api/ILqbAsyncPdfDownloadHelper';

interface IPipelineLoansVm {
    loans                : ILoan[];
    IsUseTasksInTpoPortal: boolean;
    HideStatusAndAgentsPageInOriginatorPortal: boolean;
    PortalMode           : E_PortalMode;
    isDuplicateResult    : boolean;
}
declare const PipelineLoansVm: IPipelineLoansVm;
declare const LqbAsyncPdfDownloadHelper: ILqbAsyncPdfDownloadHelper;

const $ = jQuery;

import html from "./index.html";
class LoanTableController {
    private E_PortalMode         = E_PortalMode;
    private E_sRateLockStatusT   = E_sRateLockStatusT;
    private E_sStatusT           = E_sStatusT;

    private loans                : ILoan[];
    private IsUseTasksInTpoPortal: boolean;
    private hideStatusAndAgentsPageInOriginatorPortal: boolean;
    private PortalMode           : E_PortalMode;
    private IsLeadPipeline       : boolean;

    private sortKey              : string  = "sStatusD";
    private sortDesc             : boolean = true;
    private onSort(sortKey: string) {
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
    }

    // static $inject = ["$element", "$timeout"];
    constructor() {
    }

    private getTasksListUrl     (loan:ILoan) { return         `${VRoot}/main/Tasks/TaskList.aspx?loanId=${loan.sLId}&src=pipeline&cv=0`; }
    private getConditionsListUrl(loan:ILoan) { return         `${VRoot}/main/Tasks/TaskList.aspx?loanId=${loan.sLId}&src=pipeline&cv=1`; }
    private viewLockConf        (loan:ILoan) { LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(`pdf/RateLockConfirmation.aspx?loanid=${loan.sLId}&crack=${Date.now()}`); }
    private viewApprovalCert    (loan:ILoan) { LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(`pdf/PmlApproval.aspx?loanid=${loan.sLId}&crack=${Date.now()}`); }
    private viewSuspenseNotice  (loan:ILoan) { LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(`pdf/SuspenseNotice.aspx?loanid=${loan.sLId}&crack=${Date.now()}`); }

    private getLoanEditorUrl(loan: ILoan) {
        if (this.hideStatusAndAgentsPageInOriginatorPortal) {
            return `${VRoot}/webapp/Loan1003.aspx?src=pipeline&p=0&loanid=${loan.sLId}`
        }

        return `${VRoot}/webapp/StatusAndAgents.aspx?loanid=${loan.sLId}`;
    }

    private toggleOptions(loan:ILoan){
        window.location.href=this.getLoanEditorUrl(loan);
    }
}


export const loanTableComponent : IComponentOptions & {ngName:string} = {
    ngName       : "loanTable",
    template     : html,
    controller   : LoanTableController,
    controllerAs : "vm",
    bindings: {
        loans                : "<",
        isUseTasksInTpoPortal: "<",
        portalMode           : "<",
        isLeadPipeline       : "<",
        hideStatusAndAgentsPageInOriginatorPortal: "<"
    },
}



function editLoan        (loan:ILoan){ window.location.href =            `../webapp/pml.aspx?loanid=${loan.sLId}&source=PML`; }
function editLoanOld     (loan:ILoan){ window.location.href =                   `agents.aspx?loanid=${loan.sLId}&source=PML`; }
function openCreditDenial(loan:ILoan){ LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(`pdf/CreditDenial.aspx?loanid=${loan.sLId}&crack=${Date.now()}`); }
function openEdocs       (loan:ILoan){ window.location.href =              `EDocs/EDocs.aspx?loanid=${loan.sLId}`; }
function viewLoanStatus  (loan:ILoan){ openPopup(     `${gVirtualRoot}/Main/LoanSummary.aspx?loanid=${loan.sLId}`); }
function viewPrequal     (loan:ILoan){ openPopup(`${gVirtualRoot}/Main/SavedCertificate.aspx?loanid=${loan.sLId}`); }

function openExtendLink   (loan:ILoan){ takeRateAction('extend'   , loan.sLId) }
function openFloatDownLink(loan:ILoan){ takeRateAction('floatdown', loan.sLId) }
function openReLockLink   (loan:ILoan){ takeRateAction('relock'   , loan.sLId, "modal-LockRate-Relock") }
function takeRateAction(action:string, sLId:StrGuid, popupClass?:string) {
    const url = `${gVirtualRoot}/main/LockRate.aspx?loanid=${sLId}&action=${action}`;
    LQBPopup.Show(url, {
        hideCloseButton: false,
        modifyOverflow: false,
        draggable: false,
        closeText: 'Cancel',
        onReturn: function () { window.location.reload(); },
        popupClasses: popupClass
    });
}


let _popupWindow: Window|null;
function openPopup(url:string){
    _popupWindow = window.open(`../Common/PrintView_Frame.aspx?body_url=${encodeURIComponent(url)}`, 'pipelinepopup');
    if (_popupWindow != null) _popupWindow.focus();
}
jQuery(window).on("unload", function () {
    if (_popupWindow != null && !_popupWindow.closed) {
        _popupWindow.close();
    }
});
