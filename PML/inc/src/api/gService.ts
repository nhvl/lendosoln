type JsonString = string;
type BoolString = "True" | "False";

export interface IServiceResult<T = any> {
    error                     : boolean,
    value                     : T,
    UserMessage               : string,
    IsWorkflowError           : boolean
}

declare const gService: {
    utils: {
        call(op: "AddToRolodex", args: { AgentName: string, AgentCompanyName: string, AddCommand: "AddIfUnique" | "Overwrite" | "Add" }): IServiceResult,
        call(op: "PopulateFromOfficialContactList", arg: { LoanID: string, AgentRoleT: number }): IServiceResult,
    },
    PML: {
        // StatusAndAgents
        call(op: "SaveAgents", args: { sLId: string }, keepQuiet: boolean, bUseParentForError: boolean, dontHandleError: boolean): IServiceResult,
        call(op: "SaveRetailAgent", args: { loanId: StrGuid, employeeId: StrGuid, roleId: StrGuid }, keepQuiet: boolean, bUseParentForError: boolean, dontHandleError: boolean): IServiceResult,
        callAsyncSimple(op: "ImportLpaLoan", 
                        args: { LpaLoanId: string, LpaUserId: string, LpaUserPassword: string, LpaSellerNumber: string, LpaPassword: string, LpaAusKey: string, LpaNotpNum: string, ImportCredit: boolean, PortalMode: number },
                        successCallback: (result: IServiceResult) => void,
                        errorCallback: (result: IServiceResult) => void,
                        timeout: number | null): IServiceResult,
    },
    main: {
        // C:\LendOSoln\PML\webapp\DisclosuresService.aspx.cs
        call(op: "GetDisclosureViewModel"      , args: { CanRequestRedisclosure: boolean, CanRequestInitialClosingDisclosure: boolean }): IServiceResult<{DisclosuresViewModel:string, IsWorkflowError: boolean}>,
        call(op: "GetDisclosureViewModel"      , args: { loanId: StrGuid, applicationID: StrGuid }): IServiceResult<{DisclosuresViewModel:string, IsWorkflowError: boolean}>,
		call(op: "RefreshCalculation"		   , args: { loanId: StrGuid }): IServiceResult<{ DisclosuresViewModel: string, UserMessage: string, IsWorkflowError: boolean }>,
		call(op: "SaveAntiSteeringDisclosure"  , args: { loanId: StrGuid }): IServiceResult<{ UserMessage: string, IsWorkflowError: boolean }>,
        call(op: "SaveLoanDates"               , args: { loanId: StrGuid }): IServiceResult<{UserMessage:string, IsWorkflowError: boolean}>,
        call(op: "RequestLenderToCompleteOrder", args: { Source: string, Notes: string, BlockReason?: string, SupplementalData?: string }): IServiceResult<{HasError:BoolString, AbortOrder:BoolString, UserMessage:string, IsWorkflowError: boolean}>,
		call(op: "CancelDisclosureRequest"     , args: { loanId: StrGuid, Type: string, Notes: string }): IServiceResult<{ HasError: BoolString, UserMessage: string, IsWorkflowError: boolean }>,
		call(op: "LoadAntiSteeringDisclosure"  , args: { loanId: StrGuid }): IServiceResult<{ HasError: BoolString, AbortOrder: BoolString, ErrorMessage: string, LoadAntiSteeringDisclosureViewModel: string, IsWorkflowError: boolean }>,
        call(op: "LoadRequestReview"           , args: { loanId: StrGuid }): IServiceResult<{ HasError: BoolString, AbortOrder: BoolString, ErrorMessage: string, RequestReviewViewModel:string, IsWorkflowError: boolean}>,
        call(op: "LoadDocumentAudit"           , args: { loanId: StrGuid }): IServiceResult<{ HasError: BoolString, AbortOrder: BoolString, ErrorMessage: string, BlockReason: string, HasFatal:boolean, UserMessage:string, DocumentAuditResults:any, IsWorkflowError: boolean}>,
        call(op: "PlaceDocumentPreviewOrder"   , args: { loanId: StrGuid }): IServiceResult<{ HasError: BoolString, AbortOrder: BoolString, ErrorMessage: string, BlockReason: string, PdfFileDbKey:string, IsWorkflowError: boolean}>,
        call(op: "PlaceActualDocumentOrder"    , args: { loanId: StrGuid }): IServiceResult<{ HasError: BoolString, AbortOrder: BoolString, ErrorMessage: string, BlockReason: string, PdfFileDbKey:string, IsWorkflowError: boolean}>,

        // C:\LendOSoln\PML\main\YourProfileService.aspx.cs
        call(op: "RegisterNewDevice", args: { DeviceName: string }): IServiceResult<{ Success: boolean, MobilePassword:string }>,
        call(op: "RemoveDevice", args: { DeviceId: number }): IServiceResult<{ Success: boolean }>,
        call(op: "RegisterForAuthenticator"): IServiceResult<{ Success: BoolString, ErrorMessage: string, Key: string, ManualCode: string, Url: string }>,
        call(op: "DisableAuthenticator"): IServiceResult<{ Success: BoolString }>, 
        call(op: "CompleteAuthenticatorRegistration", args: { token: string, key: StrGuid }): IServiceResult<{ ErrorMessage: string }>,
        call(name: string, args: {}, keepQuiet: boolean, bUseParentForError: boolean, dontHandleError: boolean, async: boolean,
            successCallback: (result:IServiceResult) => void, failureCallback: () => void): void;
    },
    cfpb_utils: { // C:\LendOSoln\PML\webapp\TPOPortalClosingCostsService.aspx.cs
        call(op: "GetAgentIdByAgentType", args: { loanid: StrGuid, AgentType: string }): IServiceResult<{ RecordId: string, CompanyName: string }>,
    }

};

class DisclosuresService {

    RefreshCalculation(args: { loanId: StrGuid }): [{ errorMessage: string, IsWorkflowError: boolean } | null, any] {
        const { error, value, UserMessage, IsWorkflowError } = gService.main.call('RefreshCalculation', args);
        if (!!error || (value != null && !!value.UserMessage)) return [{ errorMessage: UserMessage || value.UserMessage || 'System error. Please contact us if this happens again.', IsWorkflowError: IsWorkflowError || value && value.IsWorkflowError }, value];
        return [null, JSON.parse(value.DisclosuresViewModel)];
    }
    RequestLenderToCompleteOrder(args: { Source: string, Notes: string, BlockReason?: string, SupplementalData?: string }): [{ errorMessage: string, IsWorkflowError: boolean } | null, { AbortOrder: BoolString}] {
        const { error, value, UserMessage, IsWorkflowError } = gService.main.call('RequestLenderToCompleteOrder', args);
        if (!!error || value.HasError == "True") return [{ errorMessage: UserMessage || value.UserMessage || 'An error occurred while placing the request. Please try again.', IsWorkflowError: IsWorkflowError || value && value.IsWorkflowError }, value];
        return [null, value];
    }
	SaveAntiSteeringDisclosure(args: { loanId: StrGuid }): [{ errorMessage: string, IsWorkflowError: boolean } | null, any] {
		const { error, value, UserMessage, IsWorkflowError } = gService.main.call('SaveAntiSteeringDisclosure', args);
		if (!!error || (value != null && !!value.UserMessage)) {
			return [{ errorMessage: UserMessage || value.UserMessage || 'System error. Please contact us if this happens again.', IsWorkflowError: IsWorkflowError || value && value.IsWorkflowError }, value];
		}
		return [null, value];
	}
    SaveLoanDates(args: { loanId: StrGuid }): [{ errorMessage: string, IsWorkflowError: boolean }|null, any] {
        const { error, value, UserMessage, IsWorkflowError } = gService.main.call('SaveLoanDates', args);
        if (!!error || (value != null && !!value.UserMessage)) {
            return [{ errorMessage: UserMessage || value.UserMessage || 'System error. Please contact us if this happens again.', IsWorkflowError: IsWorkflowError || value && value.IsWorkflowError }, value];
        }
        return [null, value];
    }
    CancelDisclosureRequest(args: { loanId: StrGuid, Type: string, Notes: string }): [{ errorMessage: string, IsWorkflowError: boolean } | null, any] {
        const { error, value, UserMessage, IsWorkflowError } = gService.main.call('CancelDisclosureRequest', args);
        if (!!error || value.HasError === "True") {
            return [{ errorMessage: UserMessage || value.UserMessage || 'An error occurred while cancelling the request. Please try again.', IsWorkflowError: IsWorkflowError || value && value.IsWorkflowError }, value];
        }
        return [null, value];
    }
    GetDisclosureViewModel(args: { loanId: StrGuid, applicationID: StrGuid }): [{ errorMessage: string, IsWorkflowError: boolean } | null, any] {
        const { error, value, UserMessage, IsWorkflowError } = gService.main.call('GetDisclosureViewModel', args);
        if (!!error) return [{ errorMessage: 'Unable to load disclosure data. Please refresh the page.', IsWorkflowError: IsWorkflowError || value && value.IsWorkflowError }, null];
        return [null, JSON.parse(value.DisclosuresViewModel)];
	}
	async LoadAntiSteeringDisclosure(args: { loanId: StrGuid }): Promise<[string | null, any]> {
		const [err, result] = await this.serviceAsync<any>("LoadAntiSteeringDisclosure", args);
		if (!!err) return [err, result];

		if (result.error || result.value.HasError === "True") {
			return ([
				result.UserMessage || result.value.ErrorMessage || 'System error. Please contact us if this happens again.',
				result.value]);
		}

		try { return ([null, JSON.parse(result.value.LoadAntiSteeringDisclosureViewModel)]); }
		catch (e) { return ([e, result.value]) }
	}
    async LoadRequestReview(args: { loanId: StrGuid }): Promise<[string | null, any]> {
        const [err, result] = await this.serviceAsync<any>("LoadRequestReview", args);
        if (!!err) return [err, result];

        if (result.error || result.value.HasError === "True") {
            return ([
                result.UserMessage || result.value.ErrorMessage || 'System error. Please contact us if this happens again.',
                result.value]);
        }

        try { return ([null, JSON.parse(result.value.RequestReviewViewModel)]); }
        catch (e) { return ([e, result.value]) }
    }
    async LoadDocumentAudit(args: { loanId: StrGuid }): Promise<[string | null, any]> {
        const [err, result] = await this.serviceAsync<any>("LoadDocumentAudit", args);
        if (!!err) return [err, result];

        if (result.error || result.value.HasError === "True") {
            return ([
                result.UserMessage || result.value.ErrorMessage || 'System error. Please contact us if this happens again.',
                result.value]);
        }

        try { return ([null, result.value]); }
        catch (e) { return([e, result.value]) }
    }

    serviceAsync<T>(op:string, args: {}): Promise<[string|null, IServiceResult<T>]> {
        return new Promise(resolve => {
            gService.main.call(op, args, false, false, false, true,
                (result: IServiceResult<T>) => resolve([null, result]),
                () => {
                    resolve(['System error. Please contact us if this happens again.', null as any])
                });
        });
    }
}
export const disclosuresService = new DisclosuresService();

class YourProfileService {
    RemoveDevice(args:{ DeviceId: number }): [string|null, boolean] {
        const result = gService.main.call("RemoveDevice", args);
        if (result.error) return [result.UserMessage, false];
        return [null, result.value.Success];
    }
    RegisterForAuthenticator(): [string|null, string|null, string|null, string|null]  {
        const result = gService.main.call("RegisterForAuthenticator");
        if (result.error) return [result.UserMessage, null, null, null];
        if (result.value.Success == "False") return [result.value.ErrorMessage, null, null, null];
        return [null, result.value.Key, result.value.ManualCode, result.value.Url];
    }
    CompleteAuthenticatorRegistration(args: { key: StrGuid, token: string }) {
        const result = gService.main.call("CompleteAuthenticatorRegistration", args);
        let success = result.error || !result.value.ErrorMessage;
        let errorMessage = result.value.ErrorMessage;
        return {
            Success: success,
            ErrorMessage: errorMessage
        };
    }
    DisableAuthenticator(): [boolean, string | null] {
        const result = gService.main.call("DisableAuthenticator") 
        if (result.error) return [false, result.UserMessage];
        return [true, null];
    }
}
export const yourProfileService = new YourProfileService();

class ClosingCostService { // C:\LendOSoln\PML\webapp\TPOPortalClosingCostsService.aspx.cs
    GetAgentIdByAgentType(args: { loanid: StrGuid, AgentType: string }): [string | null, { RecordId: string, CompanyName: string }] {
        const result = gService.cfpb_utils.call("GetAgentIdByAgentType", args);
        if (result.error) return [result.UserMessage || "ERROR", result.value];
        return [null, result.value];
    }
}
export const closingCostService = new ClosingCostService();

class PMLService { // C:\LendOSoln\PML\webapp\PMLService.aspx.cs
    SaveRetailAgent(args: { loanId: StrGuid, employeeId: StrGuid, roleId: StrGuid }): [string | null, boolean] {
        const { error, UserMessage } = gService.PML.call("SaveRetailAgent", args, false, false, true);

        if (error) {
            return [UserMessage || 'System error. Please contact us if this happens again.', false];
        }

        return [null, true];
    }
    SaveAgents(args: { sLId: StrGuid }): string|null {
        const { error, UserMessage } = gService.PML.call("SaveAgents", args, false, false, true);

        if (error) {
            return (UserMessage || 'System error. Please contact us if this happens again.');
        }
        return null;
    }
    ImportLpaLoanAsync(
        args: { LpaLoanId: string, LpaUserId: string, LpaUserPassword: string, LpaSellerNumber: string, LpaPassword: string, LpaAusKey: string, LpaNotpNum: string, ImportCredit: boolean, PortalMode: number, IsLead: boolean },
        successCallback: ((value: { Error: string } | { LoanId: string }) => void),
        errorCallback: (error: string) => void)
    {
        gService.PML.callAsyncSimple("ImportLpaLoan", args,
            (result: IServiceResult) => {
                const { value } = result;
                successCallback(value);
            },
            (result: IServiceResult) => {
                const { UserMessage } = result;
                errorCallback(UserMessage);
            }, null);
    }

}
export const pmlService = new PMLService();
