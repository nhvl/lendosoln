export default interface ILqbAsyncPdfDownloadHelper {
    SubmitSinglePdfDownload: (url: string) => void;
}