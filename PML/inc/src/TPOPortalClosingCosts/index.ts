import Ractive from "ractive";
import { EventContext } from "../ractive/ractive";
import { ICcSection, IClosingCostFee, ICcFeePmt, IModel, IExpense, getPaidByDesc, getDefaultFeePmt, getDefaultDisbursement, ICcFeeF} from "./model";
import { E_ClosingCostFeePaymentPaidByT } from "../DataLayer/Enums/E_ClosingCostFeePaymentPaidByT";

import { Modal, FeePickerModal, SubFeeConfirmOverwriteModal } from "./Modal";
import { TimeControl } from "./TimeControl";
import { DatePicker } from "./DatePicker";
import { simpleDialog } from "../components/IframePopup";
import { TPOStyleUnification } from "../components/TPOStyleUnification";
import { E_GfeClosingCostFeePaymentTimingT } from "../DataLayer/Enums/E_GfeClosingCostFeePaymentTimingT";

declare const ClosingCostData:any;
declare const LQBPopup: any;

declare function updateDirtyBit():void;
declare function isDirty(): boolean;
declare function clearDirty(): boolean;
declare function event_keydown(): void;
declare function displayCalendar(id: string): void;
declare function smartZipcode(node: HTMLElement, city: HTMLElement, state: HTMLElement): void;
declare function toggleBlockMask(isShow: boolean, id: string): void;

declare function _initMask(node:JQuery, f?:boolean): void;
declare const WorkflowPageDisplay: {
    SetDisplay(settings?:{
            getSelectorForBasicElement         ?: (name  : string) => string,
            getSelectorForSelectionElement     ?: (name  : string) => string,
            getSelectorForSubsetElement        ?: (name  : string) => string,
            getSelectorForCollectionElement    ?: (name  : string) => string,
            getSelectorsForDropdownOptions     ?: (ruleId: string, $field: JQuery, values: string[]) => string[],
            requiredFieldIndicator             ?: any,
            getElementForRequiredFieldIndicator?: ($writeableField:JQuery) => JQuery,
            protectFieldCallback               ?: () => void,
            writeFieldCallback                 ?: () => void,
            postSetDisplayCallback             ?: () => void,
        }): void,
    UpdateRulesFromModel(param: any): void;
};

const $j = jQuery;

var currentSec = "-1";
var IsReadOnly = $j("#IsReadOnly").val() == "true";

const pageSettings = {
    getSelectorForBasicElement         : (name :string) => ` [data-path-name="${name}"]`,
    getSelectorForSelectionElement     : getSelectorByIdOrIndex,
    requiredFieldIndicator             : $j('<span class="text-danger">*</span>'),
    getElementForRequiredFieldIndicator: getInputForRequiredFieldIndicator,
    protectFieldCallback               : protectFieldAddButtonCallback,
    writeFieldCallback                 : writeFieldAddButtonCallback,
    postSetDisplayCallback             : postSetDisplayCallback,
};

//ractive.set(model);
//Set model for reactive. Reuse UI flags of old model.

function updateRactiveModel(_ractive: Ractive.Ractive, _model:IModel) {
    const SectionList = _ractive.get<ICcSection[]>("SectionList");
    let isSectionExisted = SectionList && _model && _model.SectionList;
    if (isSectionExisted) {
        _model.SectionList.forEach((newSection, i) => {
            const oldSection = SectionList[i];
            const isFeeListExisted = !!newSection.ClosingCostFeeList && !!oldSection && !!oldSection.ClosingCostFeeList;
            if (isFeeListExisted) {
                newSection.ClosingCostFeeList.forEach((fee, index) => {
                    fee.isShown = oldSection.ClosingCostFeeList[index] && oldSection.ClosingCostFeeList[index].isShown;
                });
            }
        });
    }

    _ractive.set(_model);
}

//flag the current tab. If not current tab, load the new tab

function checkHudLines(tbody:JQuery) {
    const h: {[key:string]: boolean} = {};

    tbody.find('input.hudline[readonly]').each(function (i, o) {
        const input = o as HTMLInputElement;
        var prop = 'n' + input.value;
        h[prop] = true;
    });

    tbody.find('input.hudline:not([readonly])').each(function (i, o) {
        const input = o as HTMLInputElement;
        const $o = $j(this);
        const value = Number(input.value);
        const hubStart = ($o.attr('data-hud-start'));
        const hubEnd = ($o.attr('data-hud-end'));
        const isSubFee = ($o.attr('data-is-sub-fee'));
        const isInRange = Number(hubStart) <= value && value <= Number(hubEnd);
        const td = $o.closest('td');

        td.find('img').remove();
        $o.toggleClass('invalid', !isInRange);

        if (value == Number(hubStart)) {
            $o.removeClass('dupe');
            return;
        }

        if (!isInRange) {
            td.append(`<img src="${VRoot}/images/fail.png" class="closing-cost-fail"
                title="You can only enter a HUD line number between ${hubStart} and ${hubEnd}.">`);
            return;
        }

        const prop = 'n' + input.value;
        if (h.hasOwnProperty(prop)) {
            $o.addClass('dupe');
            td.append(`<img src="${VRoot}/images/fail.png" class="closing-cost-fail"
                title="You cannot enter duplicate HUD line numbers.">`);
        } else {
            $o.removeClass('dupe');

            if (!isSubFee) {    // Don't mark subfees with same HUD line number as duplicates.
                h[prop] = true;
            }
        }
    });
}

function getSelectorByIdOrIndex(name:string|number) {
    // Not an integer-based indexer -> use the ID
    return isNaN(name as number) ? ` [data-id="${name}"]` : ` [data-number="${name}"]`;
}

function getInputForRequiredFieldIndicator($writeableField:JQuery) {
    var pathName = $writeableField.attr('data-path-name');
    if (pathName === 'Description') {
        // Textareas use container divs to automatically expand
        // with user input. We want to append the required field
        // indicator after the parent div so the flex display
        // works correctly.
        return $writeableField.parent();
    }

    return $writeableField;
}

function protectFieldAddButtonCallback() {
    $j('a.SectionAddDisabledByProtectRule').removeClass('SectionAddDisabledByProtectRule');
    $j('a.AddFeeToSection[disabled]'      ).addClass   ('SectionAddDisabledByProtectRule');
}

function writeFieldAddButtonCallback() {
    // When disabling fields on a page that a user doesn't have write
    // access to, we ideally want to disable the buttons to add fees
    // to a section unless the user can write to a specific fee in the
    // section. Because we don't know ahead of time which fees a user
    // can write to when they load a popup, we will always allow button
    // to be enabled. When the user opens the fee picker, we will then
    // use the workflow rules to determine which fees the user cannot
    // edit, so this approach will work as a stopgap until we can better
    // determine whether there will be enabled fees when the page loads.
    // Note that we only enable buttons that have not already been disabled
    // by a separate field protection rule.
    $j('a.AddFeeToSection:not(.SectionAddDisabledByProtectRule)').removeAttr('disabled');
}

function postSetDisplayCallback() {
    $j('input[data-path-name], select[data-path-name], textarea[data-path-name], a[data-path-name], button[data-path-name]').each(function (index, element) {
        var $this = $j(element);

        var title = $this.attr('title');
        if (title && title.length > 0 && $this.attr('disabled')) {
            setTooltipDisplayForDisabledInput($this, title);
        }
    });
}

function setTooltipDisplayForDisabledInput($this: JQuery, title: string) {
    if ($this.is('input[type="checkbox"], [data-path-name="PaymentDate"]')) {
        setWrappedTooltipDisplay($this, title);
    }
    else {
        setReadonlyTooltipDisplay($this);
    }
}

function setWrappedTooltipDisplay($this: JQuery, title: string) {
    // Checkbox inputs will be styled using MDC, which wraps
    // the native input in a div with MDC classes that prevents
    // tooltips from displaying correctly. For payment dates,
    // we need to wrap the input in a div as well to make sure
    // both the input and calendar icon have the tooltip.
    if ($this.parentsUntil('.even, .odd', '.tooltip-container').length !== 0) {
        // We've already wrapped this input, so don't wrap again.
        return;
    }

    $this.removeAttr('title');

    var $container = $j('<div class="tooltip-container" data-toggle="tooltip" data-placement="bottom"></div>');
    $container.prop('title', title);
    $container.css('display', 'inline');
    $this.wrap($container);

    $container.tooltip();
}

function setReadonlyTooltipDisplay($this: JQuery) {
    if ($this.is('select, input, textarea')) {
        // In Chrome, Bootstrap tooltips will not appear on
        // disabled dropdowns, inputs, or textareas.
        // Change the disabled to be readonly, which have
        // the same styling but allow tooltips.
        // https://github.com/twbs/bootstrap/issues/15540
        $this.removeAttr('disabled');
        $this.attr('readonly', 'readonly');

        if ($this.is('select')) {
            // Certain browsers will still allow readonly
            // dropdowns to display, but will prevent changes
            // to the selected value. Prevent this event from
            // propagating to fix this issue.
            $this.mousedown(function (e) {
                e.preventDefault();
                return false;
            });
        }
    }

    $this.tooltip({
        placement: 'bottom'
    });

    // Once Bootstrap has initialized the title data,
    // remove the original title attribute. This is to
    // prevent both the browser native tooltip and the
    // Bootstrap tooltip from appearing if Bootstrap
    // does not clear out the title attribute, which
    // can happen for some elements that have data-tooltip
    // already applied.
    $this.removeAttr('title');
}

import { pDOMContentLoaded } from "../utils/common-promise";
import { postJsonSafeD } from "../utils/fetch";
import { closingCostService } from "../api/gService";
pDOMContentLoaded.then(init);
function init() {
    if (document.getElementById("ClosingCostTemplate") == null) return;

    initialiseRactive();
    $j("input[type='text'], select").attr("NotEditable", "true");
    reload();
    eventHandler();

    $j(document).on('change', 'input,select', function () {
        updateDirtyBit();
    }).on('keydown', 'input', event_keydown);

    $j(document).delegate('input.hudline:not([readonly])', 'change', function () {
        checkHudLines($j(this).closest('tbody'));
    });

    $j('.table-closingcost tbody').each(function () {
        checkHudLines($j(this));
    });

	if (!ML.isSaveButtonEnabled) {
		$j(window).on("unload", () => ClosingCostsSave(false));
	}

    WorkflowPageDisplay.SetDisplay(pageSettings);

    if (ML.DisplayQmPage) {
        TPOStyleUnification.Components(document, true);
    } else {
        TPOStyleUnification.Components($j("div[id^='sec-']:first"), true);

        // Set the tooltip for the loan info header, which won't be triggered
        // when unifying a specific part of the page instead of the entire document.
        setTimeout(function () {
            $j('.loan-info-bar a[data-toggle="tooltip"]').tooltip();
        });
    }

    $j(".textarea-container").keydown(function () {
        this.style.height = 'auto';
        this.style.height = this.scrollHeight + 'px';
    }).keydown();

	if (!ML.isSaveButtonEnabled) {
		window.Save = ClosingCostsSave;
	}
}

function eventHandler() {
    // On the click event of a tab.
    $j("#tabs>li a").click(function (e) {
        e.preventDefault();
        tabsHandler($j(e.target));
    });

    $j("input[type='text']").click(function () {
        if (!$j(this).is('[readonly]')) { $j(this).select(); };
    });

    mask();

    $j("#sec-calculation [preset='date']").change(function () { $j(this).blur(); });
    $j("#sec-calculation [preset]").change(function () { ractive.updateModel(); });
    $j("#sec-calculation [preset]").each(function () { _initMask($j(this)); });

    $j("#sec-housing [preset='date']").change(function () { $j(this).blur(); });
    $j("#sec-housing [preset]").change(function () { ractive.updateModel(); });
    $j("#sec-housing [preset]").each(function () { _initMask($j(this)); });

    $j("#sec-calculation [recalc]").change(function () { ractive.fire('recalc'); });
    $j("#sec-housing [recalc]").change(function () { ractive.fire('recalc'); });

    $j("#runPricingBtn").click(runPricing);
    async function runPricing() {
        await ClosingCostsSave(false);

        const sLId = ractive.get<string>("sLId");

        location.href = $j("#IsNewPmlUIEnabled").val()
            ? `${gVirtualRoot}/webapp/pml.aspx?loanid=${sLId}&source=PML`
            : `${gVirtualRoot}/main/agents.aspx?loanid=${sLId}`
            ;
    }
}

//call this function to reload page when tab changes or show dilalog. (QeuryString changes)
function reload() {
    $j("div[id^='sec-']").hide();
    showFirstTab();

    //update reformat fields
    setRowColor();

    setCalculationCheckboxes('sQMLoanPassesPointAndFeesTest');
    setCalculationCheckboxes('sQMLoanDoesNotHaveNegativeAmort');
    setCalculationCheckboxes('sQMLoanDoesNotHaveBalloonFeature');
    setCalculationCheckboxes('sQMLoanDoesNotHaveAmortTermOver30Yr');
    setCalculationCheckboxes('sQMLoanHasDtiLessThan43Pc');
}

function setCalculationCheckboxes(id:string) {
    $j(`#${id}Img`).prop('src', $j('#' + id).prop('checked') ? 'images/yes.png' : 'images/no.png');
}

function setRowColor() {
    //http://stackoverflow.com/questions/8189182/ie-nth-child-using-odd-even-isnt-working
    $j(".tb-tabs tbody tr:nth-child(even)").addClass("even");
    $j(".tb-tabs tbody tr:nth-child(odd)").addClass("odd");

    //table inside does not need row color
    $j(".table-style tr").removeClass("odd");

    //$j(".tb-tabs tbody tr td:nth-child(8)").addClass("table-inside-description");
    $j(".table-calculation-2 tr:nth-child(even)").addClass("odd");
    $j(".table-calculation-2 tr:nth-child(odd)").addClass("even");
}
function getParameterByName(name:string) {
    //http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Some components of MeridianLinkCommonControls.dll run incorrect in popup dialog. Create other mask.
function mask() {
    //https://forum.jquery.com/topic/datepicker-input-mask
    $j('#sec-calculation input[preset=date]').removeClass('hasDatepicker').mask('99/99/9999');
}

/*********HOUSING TAB************/
function ToggleHousingContent(titleId:string, closingTitleIds:string[]) {
    const $header = $j("#" + titleId);
    // Get the next element. It should be the div containing the housing expense content.
    const $content = $header.next("[id$='_content']");
    $header.find('i').toggleClass("arrow-rotate", !$content.is(":visible"));
    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
    $content.slideToggle(500, function () {
        //execute this after slideToggle is done
        //scroll to the expanded div position
        let friendlyViewTopPadding = 5;
        const currentPos = $j(document).scrollTop()!;
        let pos = $header.position().top - $j('.content-closing-cost-qm').position().top - friendlyViewTopPadding;

        let animationConfig = { duration: 500, queue: false };
        closingTitleIds.forEach(function (id) {
            const $closingHeader = $j("#" + id);
            const $closingcontent = $closingHeader.next("[id$='_content']")!;
            $closingHeader.find('i').toggleClass("arrow-rotate", !$closingcontent.is(":visible"));
            const contentPos = $closingcontent.position().top;
            if (contentPos < pos) {
                pos = pos - $closingcontent.height()!;
            }

            $closingcontent.hide(animationConfig);
        });
        if ((currentPos != 0 || titleId != 'mi_title')
            && (Math.abs(currentPos - pos) > 40)) {
                $j('html, body').animate({ scrollTop: pos }, animationConfig);
            }
    });

    return false;
};
/*********************************************************TABS MANAGEMENT*********************************************************/

export async function ClosingCostsSave(isUnload:boolean = false):Promise<string> {
	$j("#save-button-with-tooltip").show();
	$j("#save-button-without-tooltip").hide();

    if ($j('input.dupe, input.invalid').length > 0) {
        const message = 'Cannot save until the HUD line issues are fixed.';
        if (isUnload || ML.isSaveButtonEnabled) return message;

        simpleDialog.alert(message).then(() => location.reload());
    }

    isUnloadingWindow = isUnload;

	const wasDirty = isDirty();
	if (typeof wasDirty !== 'undefined' && !wasDirty) return "";

    const modelData = await ractive.get<IModel>();
    const isExpensesTab = ($j("#tabs>li a.selected").prop('id') == "expensesTabLink");

    const [error, strRes] = await callWebMethod<string>(
        isExpensesTab ? 'SaveExpenses' : 'Save',
        { loanId: modelData.sLId, viewModelJson: JSON.stringify(modelData) });
    if (error != null) return error.message;

    let result: { m_Item1: any, m_Item2: any };
    try { result = JSON.parse(strRes); }
    catch(e) { return e }
    if (result == null) return "Error while saving.";

    clearDirty();
    const model = result.m_Item1;

    model.ByPassBgCalcForGfeAsDefault = ractive.get<boolean>("ByPassBgCalcForGfeAsDefault");
    model.viewT                       = ractive.get<number>("viewT");
    updateRactiveModel(ractive, model);
    if (!isUnload) {
        WorkflowPageDisplay.UpdateRulesFromModel(result.m_Item2);
    }

    return "";
}

async function tabsHandler(el:JQuery) {
    const badInputs = $j('input.dupe, input.invalid');

    if (badInputs.length > 0) {
        simpleDialog.alert('Please correct the HUD line entries before changing tabs.', 'Change tab');
        return;
    }

    if (el.attr("id") != 'expensesTabLink') {
        ractive.set("WillUseSellerFees", el.attr("value") == "True");
    }

    const wasDirty = isDirty();

    if (wasDirty && ML.isSaveButtonEnabled) {
        const isConfirmed = await simpleDialog.confirm<null>("Changes you made have not been saved.", "Leave This Page?", "SAVE & PROCEED", "DISCARD & PROCEED")
        if (isConfirmed == null) return; // cancel

        if (isConfirmed) {
            f_displayBlockMask(true);
            const resultMessage = await ClosingCostsSave(false);

            if (typeof resultMessage === "string" && !!resultMessage) {
                simpleDialog.alert(resultMessage);
                $j("#save-button-with-tooltip").show();
                $j("#save-button-without-tooltip").hide();
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-center",
                    "timeOut": 2000
                }
                toastr.success("Your changes have been saved.");
                ractive.fire('recalc');
                postSaveTabsHandler(el, wasDirty);
            }

            f_displayBlockMask(false);
        }
        else { // Discard & Proceed
            clearDirty();
            ractive.fire('recalc');
            postSaveTabsHandler(el, wasDirty);
        }
    }
	else {
		if (wasDirty) {
			const resultMessage = await ClosingCostsSave(false);
			if (!!resultMessage) {
				simpleDialog.alert(resultMessage);
				return;
			}
			else {
				ractive.fire('recalc');
				postSaveTabsHandler(el, wasDirty);
			}
		}
		else {
			ractive.fire('recalc');
			postSaveTabsHandler(el, wasDirty);
		}
	}
}

function postSaveTabsHandler(el:JQuery, wasDirty:boolean) {
    // Hide all content.
    const sectionSelector = el.attr("href");
    if (!!sectionSelector) {
        $j("tbody[id^='sec-']").hide();
        showTab(el);

        if (wasDirty) {
            TPOStyleUnification.Components(document.querySelector<HTMLElement>(sectionSelector)!, true);
            WorkflowPageDisplay.SetDisplay(pageSettings);
        }
    } else {
        showFirstTab();

        if (wasDirty) {
            TPOStyleUnification.Components($j("div[id^='sec-']:first"), true);
            WorkflowPageDisplay.SetDisplay(pageSettings);
        }
    }

    window.scrollTo(0, 0);
    return false;
}

var isUnloadingWindow = false;
var saveErrorMessage = null;

function showTab(tab:JQuery) {
    // Remove any "active" class.
    $j("#tabs>li a").removeClass("selected");
    $j("#tabs>li").removeClass("active");

    // Add "active" class to selected tab.
    tab.addClass("selected");
    tab.parent().addClass("active");
    if (tab.parent().attr("value")) {
        ractive.set("viewT", tab.parent().attr("value"));
    }
    // Hide all tab content
    $j("div[id^='sec-']").hide();

    // Find the href attribute value to identify the active tab + content
    const activeTab = tab.attr("href");
    $j(activeTab).show();
}

function showFirstTab() {
    if (ML.DisplayQmPage) {
        $j("#sec-calculation").show();
        $j("#tabs").hide();

        // When display QM in new layout, we disable [loan-static-tab] feature
        $j("[loan-static-tab]")
            .removeClass("warp-section-tabs")
            .removeClass("closing-costs-qm-pages")
            .addClass("qm-section")
            .removeAttr("loan-static-tab")
            ;
    } else {
        // Activate first tab.
        $j("#tabs>li a:first").addClass("selected").show();
        $j("#tabs>li a:first").parent().addClass("active");

        // Show first tab content.
        $j("div[id^='sec-']:first").show();
    }
}

var ractive: Ractive.Ractive;

function callWebMethod<T>(webMethodName:string, data:any): Promise<[Error | null, T]> {
	return postJsonSafeD(`${location.pathname}/${webMethodName}${location.search}`, data);
}

async function CalculateData(params: { loanId: string, viewModelJson: {} })
    : Promise<[Error | null, IModel]> {

    const isExpensesTab = ($j("#tabs>li a.selected").prop('id') == "expensesTabLink");

    // console.time("Time To Load Data");
    const [error, d] = await callWebMethod<string>(
        isExpensesTab ? 'CalculateDataExpenses' : 'CalculateData',
        {...params, viewModelJson: JSON.stringify(params.viewModelJson)});
    // console.timeEnd("Time To Load Data");

    if (error != null) return [error, d as any];
    if (!d) return [new Error("d is empty"), null as any];

    return [null, JSON.parse(d)];
}
async function GetAvailableClosingCostFeeTypes(params: { loanId: string, viewModelJson: {}, sectionName: string })
    : Promise<[Error | null, { Section: any, SubFeesBySourceFeeTypeId: any }]> {
    const [error, d] = await callWebMethod<string>(
        'GetAvailableClosingCostFeeTypes',
        {...params, viewModelJson: JSON.stringify(params.viewModelJson)});

    if (error != null) return [error, d as any];
    if (!d) return [new Error("d is empty"), null as any];

    return [null, JSON.parse(d)];
}

function initialiseRactive() {
    transitions();
    function transitions(){
        Ractive.transitions.initMask             = initMaskTransition;
        Ractive.transitions.datepickerTransition = datepickerTransition;
        Ractive.transitions.numericTransition    = numericTransition;
        Ractive.transitions.sectionIntro         = sectionTransition;

        function datepickerTransition(this:Ractive.Ractive, t: Ractive.Transition) {
            const ractive = this;

            if (!t.isIntro) return;

            const node = $j(t.node);
            node.removeClass('hasDatepicker').on("change", function (this: HTMLInputElement) {
                this.blur();
                ractive.updateModel();
                ractive.fire('recalc');
            });

            _initMask(node);
        }

        function numericTransition(this: Ractive.Ractive, t: Ractive.Transition, recalc:boolean) {
            const ractive = this;

            if (!t.isIntro) return;

            const node = $j(t.node);
            if (recalc) {
                node.on("change", function () {
                    ractive.updateModel();
                    ractive.fire('recalc');
                });
            }

            _initMask(node);
            node.css({ width: "", "text-align":""}); // remove mask.js style
        }

        function sectionTransition(t: Ractive.Transition, params: { hudline: string, isSystem: boolean, gfeGroup:number}) {
            if (!t.isIntro) return;

            const node = $j(t.node);
            const section = node.text();

            const selector = determineShownSections(Number(params.hudline), params.isSystem, params.gfeGroup);
            node.find("option").not(selector).remove();

            var numVisible = node.find("option").length;
            if (numVisible < 2) {
                //Pseudo disable it so it looks better
                node.mousedown(function (event) { event.preventDefault(); });
                //node.css("background-color", "white");
                //node.css("color", "grey");
            }
            if (numVisible < 1) {
                node.css('display', 'none');
            }
        }

        function determineShownSections(hudline: number, isSystem: boolean, gfeGroup: number) {
            return _determineShownSections(hudline, isSystem, gfeGroup).map(data => `:contains('${data}')`).join(",");
        }
        function _determineShownSections(hudline:number, isSystem:boolean, gfeGroup:number):string[] {
            // Create the selectors for the custom fees first.
            if (!isSystem) {
                if ( 800 <= hudline && hudline <  900) return ["A1", "B3", "N/A"];
                if ( 900 <= hudline && hudline < 1000) return ["B3", "B11", "N/A"];
                if (1000 <= hudline && hudline < 1100) return ["B9"];
                if (1100 <= hudline && hudline < 1200) return ["B4", "B6", "N/A"];
                if (1200 <= hudline && hudline < 1300) return ["B7", "B8"];
                if (1300 <= hudline && hudline < 1400) return ["B4", "B6", "N/A"];

                return [];
            }

            if      (gfeGroup ===  0) return ["N/A"];
            else if (gfeGroup ===  1) return ["A1"];
            else if (gfeGroup ===  2) return ["A2"];
            else if (gfeGroup ===  3) return ["B3"];
            else if (gfeGroup ===  4) return ["B4"];
            else if (gfeGroup ===  5) return ["B5"];
            else if (gfeGroup ===  6) return ["B6"];
            else if (gfeGroup ===  7) return ["B7"];
            else if (gfeGroup ===  8) return ["B8"];
            else if (gfeGroup ===  9) return ["B9"];
            else if (gfeGroup === 10) return ["B10"];
            else if (gfeGroup === 11) return ["B11"];
            else if (gfeGroup === 12) return ["B4", "B6"];
            return ["N/A"];
        }

        function initMaskTransition(t: Ractive.Transition) {
            _initMask($j(t.node), true);
            t.complete();
        }
    }

    function highlightLI(el:JQuery) {
        el.css("background-color", "yellow");
        el.attr("class", "selected");
    }

    function unhighlightLI(el: JQuery) {
        el.css("background-color", "");
        el.removeAttr("class");
    }

    interface ICalculationData {
        fee             : ICcFeeF,
        typeid          : string,
        loan            : {},
        section         : {
            SectionType : string,
            HudLineStart: number,
        }
    }

    const CalculationModal = Ractive.extend({
        template: document.getElementById('CalculationModalContent') == null ? "" : document.getElementById('CalculationModalContent')!.innerHTML,

        onrender(options:{}) {
            WorkflowPageDisplay.SetDisplay(pageSettings);
            this._super(options);
        },

        show(this:Ractive.Ractive, params: ICalculationData) {
            this.set(params);
            return new Promise<ICalculationData|null>(resolve => {
                LQBPopup.ShowElementWithWrapper($j((this as any).el), {
                    headerText: "Calculator",
                    closeText: 'cancel',
                    hideXButton: true,
                    onClose: () => {
                        resolve(null);
                    },
                    buttons: [{
                        Ok: () => {
                            resolve(this.get());
                        }
                    }],
                    popupClasses: "modal-calculator-closingcost",
                });
            });
        },

        on: {
            fee_t_change(){
                setTimeout(function () {
                    WorkflowPageDisplay.SetDisplay(pageSettings);
                });
            }
        }
    } as any);
    const calculationModal = new CalculationModal({ el: document.getElementById("calculationModal") }) as (
        Ractive.Ractive & {show(_:ICalculationData): Promise<ICalculationData|null>});

    console.time("Initial Render");

    ractive = new Ractive({
        el      : 'container',
        append  : false,
        template: '#ClosingCostTemplate',
        partials: {
            ClosingCostFeeTemplate        : document.getElementById('ClosingCostFeeTemplate'       )!.innerHTML,
            ClosingCostSectionFeeTemplate : document.getElementById('ClosingCostSectionFeeTemplate')!.innerHTML,
            ExpenseTemplate               : document.getElementById('ExpenseTemplate'              )!.innerHTML,
            ExpenseDisbursementTemplate   : document.getElementById('ExpenseDisbursementTemplate'  )!.innerHTML,
        },
        components: { datePicker: DatePicker, timeControl: TimeControl },
        data: ClosingCostData,
        on: {
            domblur(this:Ractive.Ractive, context: EventContext) {
                //sadly this blur event fires before the mask.js blur event
                //by using setTimeout we queue up the function until after all the other
                //js code.
                setTimeout(() => {
                    context.updateModel();
                    // this.updateModel(context.keypath, true);
                });
            },

            toggleDetail(context: EventContext) { context.toggle("isShown"); },

            async delete(this: Ractive.Ractive, context: EventContext, i: number, j: number, desc: string, amt: string, orgDesc: string) {
                if ((context.node as HTMLButtonElement).disabled || context.node.hasAttribute("disabled")) return;

                const d = !!desc ? desc : orgDesc;

                const isConfirm = await simpleDialog.confirm(
                    `<p>Are you sure you would like to remove the following fee?</p>
                    <p class="money-remove-fee">${d}: ${amt}</p>`,
                    "Remove fee", "Yes", "No");

                if (!isConfirm) return;

                updateDirtyBit();

                const list = this.get<boolean>("IsUsingSellerFees") ? (`SellerList.${i}.ClosingCostFeeList`) : (`SectionList.${i}.ClosingCostFeeList`);
                this.splice(list, j, 1);
                TPOStyleUnification.Components($j($j("#tabs>li a.selected").attr('href')), true);
            },

            updatePaidBy(this: Ractive.Ractive, context:EventContext, i:number, j:number, total:string) {
                const value = Number((context.node as HTMLSelectElement).value);
                if (value == -1) {
                    const model = this.get<IModel>();
                    const fee = (model.IsUsingSellerFees ? model.SellerList! : model.SectionList)[i].ClosingCostFeeList[j];
                    fee.pmts[0].paid_by = 1;

                    // Add new system payment
                    fee.pmts.push(getDefaultFeePmt({ is_system: true }));

                    // Display details when marking a fee's paid by as "split".
                    fee.isShown = true;

                    updateDirtyBit();
                }
                else if (value == 3) {
                    const pmt = context.get<ICcFeePmt>("pmts[0]");
                    pmt.paid_by = value;
                    pmt.pmt_at = 1; // Set to 'at closing'
                    context.update("pmts[0]");
                } else {
                    context.set("pmts[0].paid_by", value);
                }

                this.fire('recalc');
            },

            updatePaidBySplit(this: Ractive.Ractive, context:EventContext) {
                const value: E_ClosingCostFeePaymentPaidByT = Number((context.node as HTMLSelectElement).value);

                context.set(
                    (value == E_ClosingCostFeePaymentPaidByT.BorrowerFinance)
                    ? { paid_by: value, pmt_at: E_GfeClosingCostFeePaymentTimingT.AtClosing }
                    : { paid_by: value }
                );

                this.fire('recalc');
            },

            AddSplitPayment(this: Ractive.Ractive, context: EventContext, i: number, j: number) {
                updateDirtyBit();

                context.push("..", getDefaultFeePmt());
                // this.push(`SectionList.${i}.ClosingCostFeeList.${j}.pmts`, getDefaultFeePmt());

                this.fire('recalc');
            },

            async updateCalc(this: Ractive.Ractive, context: EventContext, i: number, j: number) {
                const input = context.node as HTMLInputElement;
                if (input.disabled || input.hasAttribute("disabled")) return;

                // const sectionPath = this.get("IsUsingSellerFees") ? `SellerList.${i}` : `SectionList.${i}`;
                // const feePath = `${sectionPath}.ClosingCostFeeList.${j}`;
                // context ~ feePath

                const selectedFee = context.get<IClosingCostFee>();
                const data = await calculationModal.show(JSON.parse(JSON.stringify({
                    fee             : selectedFee.f,
                    typeid          : selectedFee.typeid,
                    loan            : this.get(),
                    section         : {
                        SectionType : context.get(`../../SectionTypeRep`),
                        HudLineStart: context.get(`../../HudLineStart`),
                    },
                })));
                if (data == null) return;

                this.set(data.loan);
                context.set({ f: data.fee });
                this.fire("recalc");
            },

            recalc(this: Ractive.Ractive, context:EventContext) {
                if (!!this.get("ByPassBgCalcForGfeAsDefault")) return;

                //prevent user's interact when calculating
                const maskRecalcId = "temp_recalc_mask";
                toggleBlockMask(true, maskRecalcId);

                setTimeout(async () => {
                    this.set('GfeTilAgentRoleT', $j("#CFM_m_officialContactList").val());
                    this.set('GfeTilIsLocked', $j("#CFM_m_rbManualOverride").prop("checked"));

                    const [error, model] = await CalculateData({ loanId: this.get("sLId"), viewModelJson: this.get() });
                    if (error != null) {
                        toggleBlockMask(false, maskRecalcId);
                        alert('Error');
                        return;
                    }

                    console.time("Time to set data");
                    model.ByPassBgCalcForGfeAsDefault = this.get("ByPassBgCalcForGfeAsDefault");
                    model.viewT = this.get("viewT");
                    updateRactiveModel(this, model);
                    console.timeEnd("Time to set data");

                    WorkflowPageDisplay.SetDisplay(pageSettings);
                    TPOStyleUnification.Components($j($j("#tabs>li a.selected").attr('href')), true);

                    toggleBlockMask(false, maskRecalcId);
                });
            },

            async removePayment(this: Ractive.Ractive, context: EventContext,
                i: number, j: number, k: number,
                amt: string, paidBy: E_ClosingCostFeePaymentPaidByT, desc: string, orgDesc: string) {
                const isConfirm = await simpleDialog.confirm(`<div>
                        <p>Are you sure you would like to remove the following payment?</p>
                        <p class="money-remove-fee">${getPaidByDesc(paidBy)} split payment for the ${!!desc ? desc : orgDesc}: ${amt}</p>
                    </div>`, `Remove payment`, "Yes", "No");
                if (!isConfirm) return;

                // this.splice(`SectionList.${i}.ClosingCostFeeList.${j}.pmts`, k, 1);
                context.splice("..", k, 1);

                updateDirtyBit();
                this.fire('recalc');
            },

            async addFee(this: Ractive.Ractive, context: EventContext) {
                const addButton = context.node as HTMLButtonElement;
                if (addButton.disabled || addButton.hasAttribute('disabled')) return;
                
                //prevent user's interact when adding Fee
                const maskAddFeeId = "temp_addFee_mask";
                toggleBlockMask(true, maskAddFeeId);

                // const section = this.get("IsUsingSellerFees") ? `SellerList.${i}` : `SectionList.${i}`; // = context
                const sectionName = context.get<string>("SectionName"); // this.get(`${section}.SectionName`);

                const [error, result] = await GetAvailableClosingCostFeeTypes({loanId:this.get("sLId"), viewModelJson: this.get(), sectionName});
                if (error != null) {
                    toggleBlockMask(false, maskAddFeeId);
                    alert("Error");
                    return;
                }

                // Result will contain the specific section we're adding fees
                // to and will not contain this data point.
                result.Section.IsUsingSellerFees = this.get<boolean>("IsUsingSellerFees");

                const fee = await pickFee(sectionName, result);
                if (fee == null) {
                    toggleBlockMask(false, maskAddFeeId);
                    return;
                }

                context.push("ClosingCostFeeList", fee);
                // this.push(`${section}.ClosingCostFeeList`, fee);

                updateDirtyBit();
                checkHudLines($j(addButton).closest('table').find('tbody'));
                
                toggleBlockMask(false, maskAddFeeId);
                this.fire('recalc');
            },

            showCalendar(this: Ractive.Ractive, context: EventContext, id:string) {
                return displayCalendar(id);
            },

            CanShopChange(this: Ractive.Ractive, context: EventContext, i: number, j: number) {
                // We can simplify using conext keypath
                const keypath = this.get<boolean>("IsUsingSellerFees")
                    ?  `SellerList.${i}.ClosingCostFeeList.${j}.did_shop`
                    : `SectionList.${i}.ClosingCostFeeList.${j}.did_shop`
                    ;

                const can_shop = (context.node as HTMLInputElement).checked;
                if (!can_shop) this.set(keypath, false);
            },

            tpClick(this: Ractive.Ractive, context: EventContext, i: number, j: number) {
                const checkbox = context.node as HTMLInputElement;

                // const list = this.get("IsUsingSellerFees") ? (`SellerList.${i}`) : (`SectionList.${i}`);

                // Disable and uncheck the AFF checkbox when the TP checkbox is unchecked.
                if (checkbox && !checkbox.checked) {
                    // this.set(`${list}.ClosingCostFeeList.${j}.aff`, false);
                    context.set("aff", false);
                    updateRactiveModel(this, this.get<IModel>()); // TODO: why run updateRactiveModel
                }
            },

            HousingHeaderClick(this: Ractive.Ractive, context: EventContext, titleId:string) {
                const closingTitleIds = Array.from(document.querySelectorAll("[id$='_title']"))
                    .filter(title => title.id != titleId)
                    .filter(title => $j(title).next("[id$='_content']").is(':visible'))
                    .map(title => title.id)
                    ;

                return ToggleHousingContent(titleId, closingTitleIds);
            },

            highlightLI(this: Ractive.Ractive, context: EventContext) {
                const node = $j(context.node);
                const allLI = node.parent().find('li');
                unhighlightLI(allLI);
                highlightLI(node);
            },

            unhighlightLI(this: Ractive.Ractive, context: EventContext) {
                const node = $j(context.node);
                unhighlightLI(node);
            },

            addDisb(this: Ractive.Ractive, context: EventContext, expenseIndex: number) {
                context.push("Disbursements", getDefaultDisbursement());
                // this.push(`Expenses.${expenseIndex}.Disbursements`, getDefaultDisbursement());
                this.fire('recalc');
            },

            removeDisb(this: Ractive.Ractive, context: EventContext, expenseIndex: number, disbIndex: number) {
                // const expense = this.get(`Expenses[${expenseIndex}]`);
                // const disb = expense.Disbursements[disbIndex];
                // this.set(`Expenses[${expenseIndex}].removedDisbs`, expense.removedDisbs + disb.DisbId + ",");
                // this.splice(`Expenses[${expenseIndex}].Disbursements`, disbIndex, 1);

                context.set("../../removedDisbs", context.get<string>("../../removedDisbs") + context.get<string>("DisbId") + ",");
                context.splice("..", disbIndex, 1);

                this.fire('recalc');

                $j(context.node).closest('.div-housing-border-disbursements').find('.hasDatepicker').removeClass('hasDatepicker').mask('99/99/9999');
            },

            checkCustomSettings(this: Ractive.Ractive, context: EventContext) {
                const currExp = context.get<IExpense>();

                const customLineNum = Number((context.node as HTMLInputElement).value);
                context.set("customLineNum", customLineNum);

                // Don't bother checking if setting to Any or None
                if (customLineNum == 0 || customLineNum == 5) {
                    this.fire('recalc');
                    return;
                }

                // We can simplify using context.get("..")
                this.get<IExpense[]>('Expenses').forEach((exp, i) => {
                    if (exp !== currExp && exp.IsCustom && exp.customLineNum == currExp.customLineNum) {
                        this.set(`Expenses.${i}.customLineNum`, 5);
                    }
                });

                this.fire('recalc');
            },

            selectDDL(this: Ractive.Ractive, context: EventContext, i:number, j:number) {
                const value = (context.node as HTMLSelectElement).value;

                const [err, res] = closingCostService.GetAgentIdByAgentType({ loanid: this.get("sLId"), AgentType: value});

                if (!err) {
                    // const selectedAgent = this.get("IsUsingSellerFees") ? `SellerList.${i}.ClosingCostFeeList.${j}` : `SectionList.${i}.ClosingCostFeeList.${j}`;
                    // = context

                    //Update the fee's Beneficiary ID
                    context.set("bene_id", res.RecordId);
                    // Update the fee's Beneficiary Description
                    context.set("bene_desc", res.CompanyName);
                    //Update the fee's paidTo DDL
                    context.set("bene", value);
                    //Re-enable beneficiary automation.
                    context.set("disable_bene_auto", false);
                }

                updateDirtyBit();
                this.fire('recalc');
            },

            reselectInput(this: Ractive.Ractive, context: EventContext) {
                //this is called by the UL itself
                $j(context.node).parent().find('input').focus();
            },

            smartZipcodeChange(this: Ractive.Ractive, context: EventContext, cityId: string, stateId: string) {
                const city = document.getElementById(cityId)!;
                const state = document.getElementById(stateId)!;

                smartZipcode(context.node, city, state);
                this.updateModel(stateId);
                this.updateModel(cityId);
            },
        },
    });

    const usedList =
        ClosingCostData.SectionList ? ClosingCostData.SectionList : (
        ClosingCostData.SellerList  ? ClosingCostData.SellerList  : null );

    if (!!usedList) {
        const c = ClosingCostData.SectionList.reduce((c:number, list:any) => c + list.ClosingCostFeeList.length, 0);
        console.info('Number of fees : ' + c);
        console.timeEnd("Initial Render");
    }

    if (IsReadOnly) {
        $j("*").attr("readonly", "");
        $j("input[type!='text'], select").not("#ddlGFEArchives").not("#bPrintGFE").prop("disabled", "true");
    }
}

async function pickFee(sectionName: string, data: { Section: any, SubFeesBySourceFeeTypeId: any }){
    return new Promise<IClosingCostFee|null>(resolve => {
        $j("#closingCostModal").remove();
        const y = new FeePickerModal({
            el: document.body,
            data: data,
            on: {
                selectFee(this: Ractive.Ractive, context: EventContext < MouseEvent >, i: number) {
                    if ((context.node as HTMLAnchorElement).hasAttribute("disabled")) return false;

                var selectedFee: IClosingCostFee;
                    if (this.get("Section.IsUsingSellerFees")) {
                        selectedFee = this.get<IClosingCostFee>(`Section.FeeListAsSellerFees.${i}`);
                        selectedFee.pmts[0].paid_by = 2;
                        
                    } else {
                        selectedFee = this.get<IClosingCostFee>(`Section.FeeListAsBorrowerFees.${i}`);
                    }

                    var subFees;
                    for (var idx = 0; idx < data.SubFeesBySourceFeeTypeId.length; idx++) {
                        if (data.SubFeesBySourceFeeTypeId[idx].SourceFeeTypeId == selectedFee.typeid) {
                            subFees = data.SubFeesBySourceFeeTypeId[idx].SubFees;
                            break;
                        }
                    }

                    if (typeof (subFees) !== "undefined" && subFees.length > 0) {
                        $j("#subFeeModal").remove();
                        const confirmDlg = new SubFeeConfirmOverwriteModal({
                            el: document.body,
                            data: { SubFees: subFees, ParentDesc: selectedFee.desc }
                        });

                        LQBPopup.ShowElementWithWrapper($j("#subFeeModal"), {
                            buttons: [{
                                "OK": function () {
                                    resolve(selectedFee);
                                    confirmDlg.detach();

                                    setTimeout(function () {
                                        y.detach();
                                        LQBPopup.Hide();
                                    }, 500);
                                    
                                }
                            }],
                            closeText: 'CANCEL',
                            hideXButton: true,
                            onShown() { WorkflowPageDisplay.SetDisplay(pageSettings); },
                            onClose() { confirmDlg.detach(); }
                        });
                    }
                    else {
                        resolve(selectedFee);
                        this.detach();
                        LQBPopup.Hide();
                    }

                    return false;
                },
            },
        });

        LQBPopup.ShowElementWithWrapper($j("#closingCostModal"), {
            headerText: `Available Fees for ${sectionName}`,
            closeText: 'CANCEL',
            hideXButton: true,
            onShown() { WorkflowPageDisplay.SetDisplay(pageSettings); },
            onClose(){ resolve(null); },
        });
    });
}
