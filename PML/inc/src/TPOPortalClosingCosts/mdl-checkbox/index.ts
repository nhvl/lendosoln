/*

<MdlCheckbox
    value="{{}}"
    on-change="{{}}"
    disabled="{{}}"/>

*/
import Ractive from "ractive";

import template from "./index.ractive.html"

export const MdlCheckbox = Ractive.extend({
    template,
    data() {
        return {
            disabled: false,
            name    : "",
            id      : "",
            required: false,
        };
    },
    on: {
        change: (context:any) => {
            debugger;
        },
    },
    onrender(this:Ractive.Ractive) {
        const ele = this.find("div.mdc-checkbox");

        const checkbox = new mdc.checkbox.MDCCheckbox(ele);
    },
});
