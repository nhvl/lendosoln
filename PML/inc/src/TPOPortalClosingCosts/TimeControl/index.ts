import Ractive from "ractive";
import { EventContext } from "../../ractive/ractive";
import template from "./index.ractive.html";

export const TimeControl = Ractive.extend({
    template,
    data: {
        value   : '',
        id      : '',
        readonly: false
    },
    on: {
        minuteBlur(e: EventContext) {
            const input = e.node as HTMLInputElement;
            time_onminuteblur(input);
        },
        hourKeyUp(e: EventContext) {
            const input = e.node as HTMLInputElement;
            time_onhourkeyup(input, input.id + '_minute');
        },
        hourBlur(e: EventContext) {
            const input = e.node as HTMLInputElement;
            time_onhourblur(input);
        },
    },
});