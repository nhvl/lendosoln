import Ractive from "ractive";
import { EventContext } from "../../ractive/ractive";

import template from "./index.ractive.html";
export const DatePicker = Ractive.extend({
    template,
    data: {
        value   : '',
        id      : '',
        readonly: false
    },
    on: {
        showCalendar(e: EventContext, id:string) {
            return displayCalendar(id);
        },
        domblur(e: EventContext<KeyboardEvent>) {
            //sadly this blur event fires before the mask.js blur event
            //by using setTimeout we queue up the function until after all the other
            //js code.
            setTimeout(() => this.updateModel("value", true));
        },
        calendarClick(id:string){
            (document.getElementById(id) as HTMLInputElement).focus();
            return false;
        },
    },
});

