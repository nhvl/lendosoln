import Ractive from "ractive";
import template from "./modal.html";
import feePickerModalContent from "./FeePickerModalContent.ractive.html";
import subFeeConfirmOverwriteContent from "./SubFeeConfirmOverwriteContent.ractive.html";

export const Modal = Ractive.extend({
    append: true,
    template,
});

export const FeePickerModal = Modal.extend({
    partials: {
        modalContent: feePickerModalContent
    }
});

export const SubFeeConfirmOverwriteModal = Modal.extend({
    partials: {
        modalContent: subFeeConfirmOverwriteContent
    }
});