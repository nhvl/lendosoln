import {E_GfeClosingCostFeePaymentTimingT} from "../DataLayer/Enums/E_GfeClosingCostFeePaymentTimingT";
import { E_ClosingCostFeePaymentPaidByT } from "../DataLayer/Enums/E_ClosingCostFeePaymentPaidByT";
import { E_ClosingCostFeeFormulaT } from "../DataLayer/Enums/E_ClosingCostFeeFormulaT";
import { E_PercentBaseT } from "../DataLayer/Enums/E_PercentBaseT";
import { E_AgentRoleT } from "../DataLayer/Enums/E_AgentRoleT";

import { StrGuid } from "../utils/guid";

export function getPaidByDesc(paidby: E_ClosingCostFeePaymentPaidByT) {
    if (paidby == 1) return "borr pd";
    if (paidby == 2) return "seller";
    if (paidby == 3) return "borr fin";
    if (paidby == 4) return "lender";
    if (paidby == 5) return "broker";
    if (paidby == 6) return "other";
    return `Unknown paid by(${paidby})`;
}

export interface ICcFeeF {
    t          : E_ClosingCostFeeFormulaT;
    base      ?: string;
    p         ?: string;
    pb         : string;
    period     : string;
    pt         : E_PercentBaseT;
    base_fid  ?: string;
    period_fid?: string;
}

export interface ICcFeePmt {
    amt            : string;
    ent            : number;
    id            ?: string;
    is_fin        ?: boolean;

    is_system      : boolean;
    made           : boolean;
    paid_by        : E_ClosingCostFeePaymentPaidByT;
    pmt_at         : E_GfeClosingCostFeePaymentTimingT;
    pmt_dt         : string;
    responsible   ?: number;
    cashPdPmt     ?: boolean;
    mipFinancedPmt?: boolean;
}
export function getDefaultFeePmt(data:Partial<ICcFeePmt> = {}): ICcFeePmt {
    return {
        amt      : "",
        ent      : 0,
        is_fin   : false,
        is_system: false,
        made     : false,
        paid_by  : E_ClosingCostFeePaymentPaidByT.Borrower,
        pmt_at   : E_GfeClosingCostFeePaymentTimingT.AtClosing,
        pmt_dt   : "",

        ...data,
    };
}

export interface IClosingCostFee {
    aff                : boolean;
    apr                : boolean;
    bene               : E_AgentRoleT;
    can_shop           : boolean;
    changeSect         : boolean;
    desc               : string;
    dflp               : boolean;
    disc_sect          : number;
    editableSystem     : boolean;
    fha                : boolean;
    gfeGrps            : number;
    hudline            : string;
    id                 : string;
    is_optional        : boolean;
    is_system          : boolean;
    is_title           : boolean;
    legacy             : number;
    mismo              : number;
    modifiableMismoType: boolean;
    section            : number;
    sourceid?          : any;
    tp                 : boolean;
    typeid             : string;
    va                 : boolean;
    bene_desc          : string;
    bene_id            : string;
    did_shop           : boolean;
    disable_bene_auto  : boolean;
    is_bf              : boolean;
    org_desc           : string;
    responsible        : number;
    total              : string;
    f                  : ICcFeeF;
    is_qm              : boolean;
    pmts               : ICcFeePmt[];
    prov               : number;
    prov_choice        : number;
    qm_amount          : string;
    qm_warning         : boolean;

    isShown : boolean;
}

export interface ICcSection {
    HudLineEnd         : number;
    HudLineStart       : number;
    SectionName        : string;
    SectionType        : number;
    SectionTypeRep     : string;
    ClosingCostFeeList : IClosingCostFee[];
    FeeTemplate        : IClosingCostFee;
    TotalAmount        : string;
    TotalAmountBorrPaid: string;
}

export interface IDisbursement {
    DisbId          : StrGuid;
    DisbMon         : string;
    DisbPaidD       : string;
    DisbT           : number;
    DueAmt          : string;
    DueD            : string;
    IsPaidFromEscrow: boolean;
    PaidDT          : number;
    PaidFromD       : string;
    PaidToD         : string;
    PaySource       : number;
}
export function getDefaultDisbursement(): IDisbursement {
    return {
        DisbT           : 1,
        PaidDT          : 0,
        DueD            : "",
        DueAmt          : "",
        DisbMon         : "",
        DisbPaidD       : "",
        PaySource       : 0,
        PaidFromD       : "",
        PaidToD         : "",
        DisbId          : "00000000-0000-0000-0000-000000000000",
        IsPaidFromEscrow: false,
    };
}

export interface IExpense {
    Apr          : string;
    Aug          : string;
    Cush         : string;
    Dec          : string;
    Disbursements: IDisbursement[];
    Feb          : string;
    IsCustom     : boolean;
    IsInsurance  : boolean;
    IsOther      : boolean;
    IsTax        : boolean;
    Jan          : string;
    Jul          : string;
    Jun          : string;
    Mar          : string;
    May          : string;
    Nov          : string;
    Oct          : string;
    RepInterval  : number;
    Sep          : string;
    annAmtBase   : number;
    annAmtCalcT  : number;
    annAmtPerc   : string;
    annAmtTot    : string;
    calcMode     : number;
    customLineNum: number;
    expDesc      : string;
    expName      : string;
    expenseType  : number;
    flippedOnce  : boolean;
    isEscrowed   : boolean;
    isPrepaid    : boolean;
    isVisible    : boolean;
    monAmtFixed  : string;
    monTotPITI   : string;
    monTotServ   : string;
    prepAmt      : string;
    prepMnth     : string;
    removedDisbs : string;
    rsrvAmt      : string;
    rsrvMon      : string;
    rsrvMonLckd  : boolean;
    taxT         : number;
}

export interface IModel {
    Beneficiaries                         : { key: string; value: number; }[];
    DisableTPAffIfHasContact              : boolean;
    Expenses                              : IExpense[];
    IsUsingSellerFees                     : boolean;
    MIApr                                 : string;
    MIAug                                 : string;
    MICush                                : string;
    MIDec                                 : string;
    MIFeb                                 : string;
    MIJan                                 : string;
    MIJul                                 : string;
    MIJun                                 : string;
    MIMar                                 : string;
    MIMay                                 : string;
    MINov                                 : string;
    MIOct                                 : string;
    MISep                                 : string;
    SectionList                           : ICcSection[];
    SellerList                           ?: ICcSection[];
    VRoot                                 : string;
    WillUseSellerFees                     : boolean;
    sAggregateAdjRsrv                     : string;
    sAggregateAdjRsrvLckd                 : boolean;
    sClosingCostFeeVersionT               : number;
    sConsummationD                        : string;
    sDisclosureRegulationT                : number;
    sDocMagicClosingD                     : string;
    sDocMagicClosingDLck                  : boolean;
    sEstCloseD                            : string;
    sEstCloseDLckd                        : boolean;
    sFfUfMipIsBeingFinanced               : boolean;
    sFfUfmip1003                          : string;
    sFfUfmip1003Lckd                      : boolean;
    sFfUfmipFinanced                      : string;
    sFfUfmipR                             : string;
    sGfeInitialImpoundDeposit             : string;
    sGfeOriginatorCompF                   : string;
    sHighPricedMortgageT                  : number;
    sHighPricedMortgageTLckd              : boolean;
    sIPerDay                              : string;
    sIPerDayLckd                          : boolean;
    sIPiaDy                               : string;
    sIPiaDyLckd                           : boolean;
    sIsManuallySetThirdPartyAffiliateProps: boolean;
    sIsRequireFeesFromDropDown            : boolean;
    sIssMInsRsrvEscrowedTriReadOnly       : boolean;
    sLAmtCalc                             : string;
    sLAmtLckd                             : boolean;
    sLId                                  : string;
    sLenderUfmip                          : string;
    sLenderUfmipLckd                      : boolean;
    sLenderUfmipR                         : string;
    sLt                                   : number;
    sMInsRsrv                             : string;
    sMInsRsrvEscrowedTri                  : boolean;
    sMInsRsrvMon                          : string;
    sMInsRsrvMonLckd                      : boolean;
    sMiCertId                             : string;
    sMiCommitmentExpirationD              : string;
    sMiCommitmentReceivedD                : string;
    sMiCommitmentRequestedD               : string;
    sMiCompanyNmT                         : number;
    sMiInsuranceT                         : number;
    sMiLenderPaidCoverage                 : string;
    sMipFrequency                         : number;
    sMipPiaMon                            : string;
    sProMIns                              : string;
    sProMIns2                             : string;
    sProMIns2Mon                          : string;
    sProMInsBaseAmt                       : string;
    sProMInsBaseMonthlyPremium            : string;
    sProMInsCancelLtv                     : string;
    sProMInsCancelMinPmts                 : string;
    sProMInsLckd                          : boolean;
    sProMInsMb                            : string;
    sProMInsMidptCancel                   : boolean;
    sProMInsMon                           : string;
    sProMInsR                             : string;
    sProMInsR2                            : string;
    sProMInsT                             : number;
    sQMAprRSpread                         : string;
    sQMAprRSpreadCalcDesc                 : string;
    sQMAveragePrimeOfferR                 : string;
    sQMAveragePrimeOfferRLck              : boolean;
    sQMDiscntBuyDownR                     : string;
    sQMDiscntBuyDownRCalcDesc             : string;
    sQMExcessDiscntF                      : string;
    sQMExcessDiscntFCalcDesc              : string;
    sQMExcessDiscntFPc                    : string;
    sQMExcessUpfrontMIP                   : string;
    sQMExcessUpfrontMIPCalcDesc           : string;
    sQMExclDiscntPnt                      : string;
    sQMExclDiscntPntLck                   : boolean;
    sQMFhaUfmipAtClosing                  : string;
    sQMFhaUfmipAtClosingLck               : boolean;
    sQMIsEligibleByLoanPurchaseAgency     : boolean;
    sQMIsVerifiedIncomeAndAssets          : boolean;
    sQMLAmt                               : string;
    sQMLAmtCalcDesc                       : string;
    sQMLoanDoesNotHaveAmortTermOver30Yr   : boolean;
    sQMLoanDoesNotHaveBalloonFeature      : boolean;
    sQMLoanDoesNotHaveNegativeAmort       : boolean;
    sQMLoanHasDtiLessThan43Pc             : boolean;
    sQMLoanPassesPointAndFeesTest         : boolean;
    sQMLoanPurchaseAgency                 : number;
    sQMMaxPointAndFeesAllowedAmt          : string;
    sQMMaxPointAndFeesAllowedPc           : string;
    sQMMaxPrePmntPenalty                  : string;
    sQMNonExcludableDiscountPointsPc      : string;
    sQMParR                               : string;
    sQMParRSpread                         : string;
    sQMParRSpreadCalcDesc                 : string;
    sQMQualBottom                         : string;
    sQMQualR                              : string;
    sQMStatusT                            : number;
    sQMTotFeeAmount                       : string;
    sQMTotFeeAmt                          : string;
    sQMTotFeePc                           : string;
    sQMTotFinFeeAmount                    : string;
    sRLckdD                               : string;
    sRLckdDLck                            : boolean;
    sSchedDueD1                           : string;
    sSchedDueD1Lckd                       : boolean;
    sUfCashPd                             : string;
    sUfCashPdLckd                         : boolean;
    sUfmipIsRefundableOnProRataBasis      : boolean;
    viewT                                 : number;

    ByPassBgCalcForGfeAsDefault:boolean;
}
