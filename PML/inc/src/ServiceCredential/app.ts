﻿import { IframePopup } from '../components/IframePopup';

/**
* Represents the user-visible portion of a single service credential.
**/
interface CredentialEntry {
    /**
    * The id of the credential in the database.
    **/
    Id: number;
    /**
    * The list of service types for the credential to display to the user.
    **/
    EnabledServiceTypesToDisplay: string[];
    /**
    * The name of the service provider for the credential to display to the user.
    **/
    ServiceProviderDisplayName: string;
    /**
    * The account id of the credential.
    **/
    AccountId: string;
    /**
    * The user name of the credential.
    **/
    UserName: string;
}

/**
* Represents the data necessary to initialize a service credential table.
**/
interface InitializationData {
    /**
    * The window context to open the edit pop-up in.  Use this when inside a frame on the page to
    * get to the full window.
    **/
    WindowContext?: Window;
    /**
    * The id of the employee to edit for editing another employee.  Do not set this value if
    * viewing/editing the employee's own service credentials.
    **/
    EmployeeId?: string;
    /**
    * A value indicating whether the credentials are for the current user's OC.
    **/
    IsOriginatingCompany?: boolean;
    /**
    * The id of the hidden input containing the JSON of the service credentials to display to the user.
    **/
    ServiceCredentialJsonId: string;
    /**
    * The id of the table that will hold the service credentials.
    **/
    ServiceCredentialTableId: string;
}

/**
* The non-null result object returned by the add/edit/delete page.
**/
interface EditResults {
    /**
    * A value indicating whether the user accepted changes on the edit page.
    **/
    OK: boolean;
    /**
    * JSON representation of the CredentialEntry[] for this table after the edit.
    **/
    CredentialList: string;
}

const ServiceCredential = (function () {
    const $ = jQuery;
    let sc_windowContext: Window;
    let sc_employeeId: string | undefined;
    let sc_isOriginatingCompany: boolean | undefined;
    let sc_sortColumn: number = 0;
    let sc_isDescending: boolean = true;
    let sc_serviceCredentialJsonId: string | undefined;
    let sc_serviceCredentialTableId: string | undefined;

    function _sortServiceList(services: CredentialEntry[], sortColumn: number, isDesc: boolean): void {
        services.sort(function (a, b) {
            let valA: string, valB: string;
            if (sortColumn == 0) {
                valA = a.ServiceProviderDisplayName.toUpperCase();
                valB = b.ServiceProviderDisplayName.toUpperCase();
            }
            else if (sortColumn == 1) {
                valA = a.AccountId.toUpperCase();
                valB = b.AccountId.toUpperCase();
            }
            else {
                valA = a.UserName.toUpperCase();
                valB = b.UserName.toUpperCase();
            }

            if (valA < valB) {
                return -1;
            }
            if (valA > valB) {
                return 1;
            }

            return 0;
        });

        if (!isDesc) {
            services.reverse();
        }
    }

    function _initialize(data: InitializationData): void {
        sc_windowContext = data.WindowContext || window;
        sc_employeeId = data.EmployeeId || sc_employeeId;
        sc_isOriginatingCompany = data.IsOriginatingCompany || sc_isOriginatingCompany;
        sc_serviceCredentialJsonId = data.ServiceCredentialJsonId;
        sc_serviceCredentialTableId = data.ServiceCredentialTableId;
    }

    function _sortByHeader(this: HTMLElement): void {
        let sortLink = $(this);
        let column: number = sortLink.data('column');
        sc_isDescending = sc_sortColumn == column ? !sc_isDescending : true;
        sc_sortColumn = column;
        _renderCredentials();
    }

    function _renderCredentials() {
        let serviceCredentialsJson = $('#' + sc_serviceCredentialJsonId).val() as string || '[]';
        if (serviceCredentialsJson) {
            let credentials: CredentialEntry[] = $.parseJSON(serviceCredentialsJson);
            let credentialsTable = $('#' + sc_serviceCredentialTableId).find('tbody');

            credentialsTable.empty();
            _sortServiceList(credentials, sc_sortColumn, sc_isDescending);

            const createIcon = (text: string) => $('<i>').addClass('material-icons').text(text).css({ 'cursor': 'pointer' });

            const createSortColumnLink = (text: string, columnNumber: number) => {
                var link = $('<a>').data('column', columnNumber).text(text).click(_sortByHeader);
                return columnNumber !== sc_sortColumn ? link : link.append(createIcon(sc_isDescending ? 'expand_more' : 'expand_less').addClass('sort'));
            };

            // The header
            credentialsTable.append(
                $('<tr>').addClass('GridHeader')
                    .append($('<th>').text(''))
                    .append($('<th>').append(createSortColumnLink('Service Provider', 0)))
                    .append($('<th>').append($('<span>').text('Services')))
                    .append($('<th>').append(createSortColumnLink('Account ID', 1)))
                    .append($('<th>').append(createSortColumnLink('Login', 2)))
                    .append($('<th>').text(''))
            );

            var isAlt = false;
            $.each(credentials, function (count: number, item: CredentialEntry) {
                var serviceProviderName = item.ServiceProviderDisplayName;
                var serviceList = item.EnabledServiceTypesToDisplay.join(', ');

                credentialsTable.append(
                    $('<tr>').addClass(isAlt ? 'GridAlternatingItem' : 'GridItem')
                        .append($('<td>').append(createIcon('edit').data('id', item.Id).click(_editLinkClick)))
                        .append($('<td>').text(serviceProviderName))
                        .append($('<td>').text(serviceList))
                        .append($('<td>').text(item.AccountId))
                        .append($('<td>').text(item.UserName))
                        .append($('<td>').append(createIcon('delete').data('id', item.Id).click(_deleteLinkClick)))
                );

                isAlt = !isAlt
            });
        }
    }

    function _editLinkClick(this: HTMLElement) {
        _editServiceCredential($(this).data('id'), 'edit');
    }

    function _deleteLinkClick(this: HTMLElement) {
        _editServiceCredential($(this).data('id'), 'delete');
    }

    function _addServiceCredential() {
        _editServiceCredential(0, 'add');
    }

    function _editServiceCredential(id: number, cmd: string) {
        let url = ML.VirtualRoot + '/main/EditServiceCredential.aspx?cmd=' + cmd;
        if (id > 0) {
            url += '&id=' + id;
        }

        if (sc_employeeId) {
            url += '&employee=' + sc_employeeId;
        }

        if (sc_isOriginatingCompany) {
            url += '&oc=' + sc_isOriginatingCompany;
        }

        sc_windowContext.IframePopup.show<EditResults | null>(url, {}).then(([error, result]) => {
            _popupReturn(result);
        });
    }

    function _popupReturn(results: EditResults | null) {
        if (results != null && results.OK) {
            $('#' + sc_serviceCredentialJsonId).val(results.CredentialList);
            _renderCredentials();
        }
    }

    return {
        Initialize: _initialize,
        RenderCredentials: _renderCredentials,
        AddServiceCredential: _addServiceCredential
    };
})();

Object.assign(window, { ServiceCredential });
export default ServiceCredential;
