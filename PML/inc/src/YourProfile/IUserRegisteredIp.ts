import { StrGuid } from "../utils/guid";
import { updateDate } from "./IClientCertificate";

export interface IUserRegisteredIp {
    CreatedDate  : Date,
}

export function normalizeUserRegisteredIps(ips: IUserRegisteredIp[]) {
    return ips.map(normalizeUserRegisteredIp);
}
export function normalizeUserRegisteredIp(ip: IUserRegisteredIp): IUserRegisteredIp {
    [
        "CreatedDate",
    ].forEach(k => updateDate(ip, k));

    return ip;
}
