import {StrGuid} from "../utils/guid";

export interface IClientCertificate {
    CertificateId    : StrGuid,
    CreatedBy        : StrGuid,
    CreatedByUserName: string,
    CreatedDate      : Date,
    Description      : string,
}

export function normalizeClientCertificates(certificates: IClientCertificate[]) {
    return certificates.map(normalizeClientCertificate);
}
export function normalizeClientCertificate(c: IClientCertificate): IClientCertificate {
    [
        "CreatedDate",
    ].forEach(k => updateDate(c, k));

    return c;
}

export function updateDate(x: any, key: string) {
    if (x[key] == null) return;

    var results = /Date\(([^)]+)\)/.exec(x[key]);
    if (results != null) {
        return x[key] = new Date(parseFloat(results[1]));
    }

    var d = new Date(x[key]);
    if (Number.isNaN(d.getTime())) return;
    return x[key] = d;
}
