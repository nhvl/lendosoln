import {module, IDirective, ITimeoutService} from "angular";

import {reRefDirective        } from "../ng-common/reRef";
import {ngMdcCheckboxDirective} from "../ng-common/ngMdcCheckbox";
import {ngMdcRadioDirective   } from "../ng-common/ngMdcRadio";
import {ngFileInput           } from "../ng-common/ngFileInput";
import {sortHeaderDirective   } from "../ng-common/sortHeader";

const $ = jQuery;

const simpleDialog = (window.parent as any).simpleDialog;
import { yourProfileService } from "../api/gService";

import { IClientCertificate, normalizeClientCertificates } from "./IClientCertificate";
import { IUserRegisteredIp, normalizeUserRegisteredIps } from "./IUserRegisteredIp";
interface IMobileDevice {
    Id           : number,
    LastLoginDate: string,
    Name         : string,
    CreatedDate  : Date,
}

let $timeout: ITimeoutService;

import html from "./index.html";

class YourProfileController {

    BrokerDB               : {IsEnableMobileDeviceRegistrationTPO: boolean};
    EmployeeDB             : {EnabledMultiFactorAuthentication   : boolean, EnabledClientDigitalCertificateInstall: boolean};

    clientCertificates     : IClientCertificate[];
    registeredIps          : IUserRegisteredIp[];
    registeredMobileDevices: any[];

    private sortKey : string  = "UserName";
    private sortDesc: boolean = false;
    private onSort(sortKey: string) {
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
    }

    static $inject = ["$element", "$timeout"];
    constructor(private $element:JQuery, _$timeout: ITimeoutService) {
        $timeout = _$timeout;
        this.sortKey = "UserName";
        this.sortDesc = false;

        $element.find("[autofocus]").focus();

        this._refresh();
    }

    private _refresh = async () => {
        const [error, vm] = await apiGetData();
        if (error != null) {
            simpleDialog.alert(error, "Error");
            return;
        }

        $timeout(() => Object.assign(this, vm));
    }

    private deleteRegisteredIp(id:number) {
        this._deleteRegisteredIp(id);
        return false;
    }
    private async _deleteRegisteredIp(id: number) {
        const isConfirm = await simpleDialog.confirm("Do you want to delete IP?", "Remove IP");
        if (!isConfirm) return;
        const [error, registeredIps] = await apiDeleteRegisteredIp(id);
        if (error) {
            simpleDialog.alert(error, "Error");
            return;
        }
        $timeout(() => this.registeredIps = registeredIps!);
    }

    private revokeClientCertificate(certificateId:StrGuid){
        this._revokeClientCertificate(certificateId);
        return false;
    }
    private async _revokeClientCertificate(certificateId:StrGuid){
        const isConfirm = await simpleDialog.confirm("Do you want to revoke the client certificate?", "Remove Certificate");
        if (!isConfirm) return;
        const [error, clientCertificates] = await apiRevokeClientCertificate(certificateId);
        if (error) {
            simpleDialog.alert(error, "Error");
            return;
        }
        $timeout(() => this.clientCertificates = clientCertificates!);
    }

    private installClientCertificate(){
        window.parent.LQBPopup.Show(gVirtualRoot + '/InstallClientCertificate.aspx', {
            hideCloseButton: true,
            draggable      : false,
            onReturn       : this._refresh,
        });
    }

    private beginRegisterNewMobileDevice() {

        window.parent.LQBPopup.ShowElement($("#RegisterDevicePopup"),{
            popupClasses:'modal-register-mobile',
            backdrop:'static',
            draggable: false,
            
        });
    }

    private beginRegisterAuthenticator() {
        const result = yourProfileService.RegisterForAuthenticator();

        if (result[0]) {
            simpleDialog.alert(result[0]);
            return;
        }
        
        window.parent.LQBPopup.ShowDivPopup($("div.RegisterAuthenticatorPopup"), {
            popupClasses: 'modal-register-mobile',
            backdrop: 'static',
            draggable: false,
            onShown: function (popup: any) {
                window.setTimeout(function () {
                    popup.find('.AuthBarcode').prop("src", result["3"]);
                    popup.find('.ManualAuthCode').text(result["2"] || "");
                    popup.find('.AuthKey').val(result["1"] || "");
                    popup.find('.VerifyAuthenticatorConfig').click(function () {
                        var VerifyAuthenticatorConfig = (window as any).VerifyAuthenticatorConfig;
                        VerifyAuthenticatorConfig(popup);
                    });
                });
            }
        });
    }

    private disableAuthenticator() {
        const result = yourProfileService.DisableAuthenticator();
        if (!result[0]) {
            simpleDialog.alert(result[1]);
            return;
        }
        this._refresh();
    }

    private removeRegisterMobileDevice(md:IMobileDevice){
        this._removeRegisterMobileDevice(md);
        return false;
    }
    private async _removeRegisterMobileDevice(md:IMobileDevice){
        const isConfirm = await simpleDialog.confirm('Are you sure you want to delete the registration for this mobile device?','Remove Mobile Device');
        if (!isConfirm) return;

        const [err, success] = yourProfileService.RemoveDevice({ DeviceId: md.Id });
        if (!!err) {
            simpleDialog.alert(err);
            return;
        }

        if (!success) {
            simpleDialog.alert("Failed to delete device. Please try again.");
            return;
        }

        $timeout(() => this.registeredMobileDevices = this.registeredMobileDevices.filter(d => d != md));
    }

}
export function yourProfileDirective(): IDirective {
    return {
        restrict     : "EA",
        controller   : YourProfileController,
        controllerAs : "vm",
        template     : html,
    };
}

module("YourProfile", [])

.directive(reRefDirective        .ngName, reRefDirective        )
.directive(ngMdcCheckboxDirective.ngName, ngMdcCheckboxDirective)
.directive(ngMdcRadioDirective   .ngName, ngMdcRadioDirective   )
.directive(ngFileInput           .ngName, ngFileInput           )
.directive(sortHeaderDirective   .ngName, sortHeaderDirective   )

.directive("yourProfile", yourProfileDirective)
;




async function apiRevokeClientCertificate(certificateId: StrGuid): Promise<[string | null, IClientCertificate[] | null]> {
    const [error, certificates] = await apiFetch<IClientCertificate[]>("RevokeClientCertificate", {certificateId});
    if (error != null) return [error, certificates];
    if (certificates != null) return [error, normalizeClientCertificates(certificates)];
    return [error, certificates];
}

async function apiDeleteRegisteredIp(id: number): Promise<[string | null, IUserRegisteredIp[]|null]> {
    const [error, users] = await apiFetch<IUserRegisteredIp[]>("DeleteRegisteredIp", { id });
    if (error != null) return [error, users];
    if (users != null) return [error, normalizeUserRegisteredIps(users)];
    return [error, users];
}

async function apiGetData(){
    const [error, vm] = await apiFetch<any>("GetData", {});
    if (!error && vm != null) {
        vm.clientCertificates = vm.clientCertificates == null ? vm.clientCertificates: normalizeClientCertificates(vm.clientCertificates);
        vm.registeredIps      = vm.registeredIps      == null ? vm.registeredIps     : normalizeClientCertificates(vm.registeredIps);
    }
    return [error, vm];
}

async function apiFetch<T>(op:string, data:{}){
    const response = await fetch(`${location.pathname}/${op}`, {
        method     : 'POST',
        credentials: 'same-origin',
        headers    : { 'Content-Type': 'application/json' },
        body       : JSON.stringify(data),
    });
    if (response.status != 200) {
        debugger;
        return ["Error", null] as [null|string, T|null];
    }
    try {
        const {d} = (await response.json()) as {d:[string|null, T|null]};
        return d;
    } catch(e) {
        return [e, null] as [null|string, T|null];
    }
}
