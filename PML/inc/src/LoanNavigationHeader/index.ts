declare const LoanNavigationHeaderViewModel: any;
declare const getPreNavigationUnloadMessage: () => string | null;

import { module, IDirective } from "angular";
import { simpleDialog } from "../components/IframePopup";
const $ = jQuery;
import { StatusBar } from "../StatusAndAgents/StatusBar";

import { ClosingCostsSave } from "../TPOPortalClosingCosts";

// Constants
interface IImageClass { CssClass: string, IconType: "warning" | "clear" | "done" | "enhanced_encryption" | "no_encryption" | null };

const LegacyClosingCostUrl = `webapp/TPOFeeEditor.aspx`;
const ExpiredRateLockClass: IImageClass = { CssClass: "accent-high mini", IconType: "warning" }; // expired rate lock
const IneligibleQmClass   : IImageClass = { CssClass: "red"             , IconType: "clear"   };
const NonHighPricedQmClass: IImageClass = { CssClass: "green"           , IconType: "done"    };
const HighPricedQmClass   : IImageClass = { CssClass: "orange mini"     , IconType: "warning" };
const RateLockImageClasses: IImageClass[] = [
    /* 0 */ { CssClass: "locked"     , IconType: null                  }, // unlock "lock_open"
    /* 1 */ { CssClass: "unlocked"   , IconType: null                  }, // lock "lock"
    /* 2 */ { CssClass: "accent-high", IconType: "enhanced_encryption" }, // lock requested
    /* 3 */ { CssClass: "accent-high", IconType: "no_encryption"       }, // lock suspended
];

function displayMessage(message: string, url?: string) {
    if (location.href.includes("pml.aspx")) {
        //style for pricing page
        $("#LoanHeaderPopup").dialog({
            closeOnEscape: false,
            draggable: false,
            modal: true,
            resizable: false,
            height: 'auto',
            width: 425,
            open: function () {
                $('.ui-dialog-titlebar').hide();
                $("#LoanHeaderPopupText").html(message);
            },
            buttons: {
                'OK': function () {
                    $(this).dialog('close');

                    if (typeof url !== "undefined") {
                        window.location.href = url;
                    }
                }
            }
        });
    }
    else {
        simpleDialog.alert(message).then(() => {
            if (typeof url !== "undefined") {
                window.location.href = url;
            }
        });
    }
}

function updateLoanNavigationHeader(loanModel:any) {
    if (typeof loanModel['sCreditScoreType2'] !== 'undefined') {
        $('#sCreditScoreType2').val(loanModel['sCreditScoreType2']);
    }
}

async function navigateTo(isSaveButtonEnabled: boolean, disableExplicitSavingPage: boolean, isOnClosingCosts: boolean, url: string, closeOnReturn: boolean) {
    if (typeof getPreNavigationUnloadMessage === 'function') {
        const preNavigationUnloadMessage = getPreNavigationUnloadMessage();
        if (preNavigationUnloadMessage) {
            const isConfirmed = await simpleDialog.confirm(preNavigationUnloadMessage, "Leave This Page?", "YES", "NO");
            if (!!!isConfirmed) {
                return;
            }
        }
    }

	if (isSaveButtonEnabled && !disableExplicitSavingPage) {
		if (isDirty()) {
            const isConfirmed = await simpleDialog.confirm("Changes you made have not been saved.", "Leave This Page?", "SAVE & PROCEED", "DISCARD & PROCEED");
            if (isConfirmed == null) return; // cancel

            if (isConfirmed) { // save
                f_displayBlockMask(true);
                const resultMessage = await(
                    (typeof attemptSave === "function") ? attemptSave(true) : (
                    (typeof Save        === "function") ? Save(true)        : (
                    (isOnClosingCosts                 ) ? ClosingCostsSave(false) : (
                    "Error while trying to save"))));

                f_displayBlockMask(false);

                if (typeof resultMessage === "string" && !!resultMessage) {
                    displayMessage(resultMessage);
                    $("#save-button-with-tooltip").hide();
                    $("#save-button-without-tooltip").show();
                    return;
                }

                toastr.options = { positionClass: "toast-bottom-center", timeOut: 2000 }
                toastr.success("Your changes have been saved.");
            } else { // discard & proceed
                clearDirty();
            }
        }
	}
	else {
		const resultMessage = await(
			(typeof attemptSave === "function") ? attemptSave(true) : (
			(typeof Save        === "function") ? Save(true)        : (
			(typeof onPmlUnload === "function") ? onPmlUnload()     : (
            null))));
		if (typeof resultMessage === "string" && !!resultMessage) {
            displayMessage(resultMessage);
            return;
		}
    }

    if (closeOnReturn) window.close();
    else window.location.href = url;
}

import genericFrameworkVendorLinksHtml from "./genericFrameworkVendorLinks.html";

class GenericFrameworkVendorLinksController {
    private genericFrameworkVendorLinks: {
        DisplayAsButton                : boolean,
    }[];
    private alignmentModel             : {
        LeftAligned                    : boolean,
        CenterAligned                  : boolean,
        RightAligned                   : boolean,
    };
    private sLId                       : string;
    private currentLocation            : string;

    constructor() {
        Object.assign(this, JSON.parse(JSON.stringify(LoanNavigationHeaderViewModel)));
    }
    private launchGenericFramework(providerId:string){
        // This external function is included from genericframework.js
        f_genericFramework(this.sLId, providerId, this.currentLocation);
    }
}
function genericFrameworkVendorLinksDirective(): IDirective {
    return {
        restrict        : "EA",
        scope           : {},
        controller      : GenericFrameworkVendorLinksController,
        controllerAs    : "vm",
        template        : genericFrameworkVendorLinksHtml,
    };
}

import loanInfoBarHtml from "./loanInfoBar.html";
class LoanInfoBarController {
    private closeOnReturn            : boolean;
    private isSaveButtonEnabled      : boolean;
    private disableExplicitSavingPage: boolean;
    private isOnClosingCosts         : boolean;
    private isLead                   : boolean;

    constructor() {
        Object.assign(this, JSON.parse(JSON.stringify(LoanNavigationHeaderViewModel)));
    }

    private toPipeline() {
        var url = VRoot + '/main/pipeline.aspx';
        if (this.isLead) {
            url += '?pg=5'
        }
        navigateTo(this.isSaveButtonEnabled, this.disableExplicitSavingPage, this.isOnClosingCosts, url, this.closeOnReturn);
    }
    private getToPipelineText() {
        return this.closeOnReturn ? 'Close' : 'Pipeline';
    };

	private async onSave() {
		f_displayBlockMask(true);
		const resultMessage = await(
			(typeof attemptSave === "function") ? attemptSave(true) : (
			(typeof Save        === "function") ? Save(true)        : (
			(this.isOnClosingCosts            ) ? ClosingCostsSave(false) : (
			"Error while trying to save"))));

		if (typeof resultMessage === "string" && !!resultMessage) {
			displayMessage(resultMessage);
			$("#save-button-with-tooltip").hide();
			$("#save-button-without-tooltip").show();
		}
		else {
			toastr.options = { positionClass: "toast-bottom-center", timeOut: 2000 }
			toastr.success("Your changes have been saved.");
		}
		f_displayBlockMask(false);
	}

	private onInit() {
		$("#save-button-without-tooltip").hide();
	}
}

function loanInfoBarDirective(): angular.IDirective {
    return {
        restrict        : "EA",
        scope           : {},
        controller      : LoanInfoBarController,
        controllerAs    : "vm",
        template        : loanInfoBarHtml,
    };
}

import loanLeftNavigationHtml from "./loanLeftNavigation.html";
import { generateUrl } from "../utils/url";
import { E_sClosingCostFeeVersionT } from "../DataLayer/Enums/E_sClosingCostFeeVersionT";
class LoanLeftNavigationController {
    private closeOnReturn            : boolean;
    private isSaveButtonEnabled      : boolean;
    private disableExplicitSavingPage: boolean;
    private isOnClosingCosts         : boolean;

    static $inject = ["$element"];
    constructor($element: JQuery) {
        const vm = JSON.parse(JSON.stringify(LoanNavigationHeaderViewModel));

        vm.navigationItems.forEach(processNavigationItem);
        Object.assign(this, vm);

        setTimeout(function () {
            $element.find('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center top+10",
                    at: "center bottom"
                },
                content(this: HTMLElement) { return this.title },
            });
        });

        // TODO:
        // $("#menu-toggle").parent().toggleClass('toggled', true);
        // $("#menu-toggle").click();

        function processNavigationItem(navigationItem: { Name: string, Url: string, Selected: boolean, TabCount: number, ImageClass: IImageClass|null}) {
            const qs: {[query:string]:string} = { loanid: vm.sLId };
            switch (navigationItem.Name) {
                case 'Closing Costs':
                    if (vm.sClosingCostFeeVersionT === E_sClosingCostFeeVersionT.Legacy) {
                        navigationItem.Url = `${VRoot}/${LegacyClosingCostUrl}`;
                        navigationItem.Selected = vm.hasLegacyClosingCostUrl;
                    } else {
                        qs.DisplayQM = "0";
                        navigationItem.Selected = navigationItem.Selected && !vm.hasDisplayQmQueryString;
                    }
                    break;
                case 'Rate Lock':
                    navigationItem.ImageClass = vm.sIsRateLockExpired
                        ? ExpiredRateLockClass
                        : RateLockImageClasses[vm.sRateLockStatusT];
                    break;
                case 'QM':
                    qs.DisplayQM = "1";
                    navigationItem.ImageClass = getQmImageClass();
                    navigationItem.Selected = navigationItem.Selected && vm.hasDisplayQmQueryString;
                    break;
                case 'Tasks':
                    qs.cv = "0";
                    navigationItem.TabCount = vm.taskCount;
                    navigationItem.Selected = navigationItem.Selected && !vm.hasConditionQueryString;
                    break;
                case 'Conditions':
                    qs.cv = "1";
                    navigationItem.TabCount = vm.conditionCount;
                    navigationItem.Selected = navigationItem.Selected && vm.hasConditionQueryString;
                    break;
                default:
                    break;
            }

            navigationItem.Url = generateUrl({ url: navigationItem.Url, qs });
        }

        function getQmImageClass() {
            switch (vm.sQMStatusT) {
                // Blank
                case 0:
                    return null;
                // Ineligible
                case 1:
                    return IneligibleQmClass;
                // ProvisionallyEligible / Eligible
                case 2:
                case 3:
                    return GetQmImageClassFromHighPricedMortgageIndicator();
                default:
                    throw 'Unhandled sQMStatusT ' + vm.sQMStatusT;
            }
        }
        function GetQmImageClassFromHighPricedMortgageIndicator() {
            switch (vm.sHighPricedMortgageT) {
                // Unknown / None
                case 0:
                case 1:
                    return NonHighPricedQmClass;
                // HigherPricedQm / HPML / HigherPricedQmNotHpml
                case 2:
                case 3:
                case 4:
                    return HighPricedQmClass;
                default:
                    throw 'Unhandled sHighPricedMortgageT ' + vm.sHighPricedMortgageT;
            }
        }
    }

    private toggleLeftNav($event: MouseEvent) {
        const $this = $($event.currentTarget);
        const $parent = $this.parent();
        const isExpanded = $parent.hasClass('toggled');
        $this.find("i").toggleClass("arrow-rotate",!isExpanded);
        $parent.toggleClass("toggled", !isExpanded);

        $("div[generic-framework-vendor-links]").toggleClass("toggled", !isExpanded);
        $("[name='content-detail']").toggleClass("toggled", !isExpanded);

		if ($("#statusList").length != 0) {
            const statusList = $("#statusList").children().toArray();
            const windowWidth       : number = $(window).width()!;
            const contentDetailWidth: number = $(".content-detail").width()!;
            const sideBarWidth      : number = $(".sidebar").width()!;
            const collapseArrowWidth: number = $(".collapse-arrow").width()!;
            const width             : number = $("#statusSection").width()!;
            let totalPageWidth = width - 32;

            if (totalPageWidth + sideBarWidth > contentDetailWidth) {
                totalPageWidth = windowWidth - collapseArrowWidth - 96;
                if (isExpanded) {
                    totalPageWidth -= sideBarWidth;
                }
                if (totalPageWidth > 1294) { // max size
                    totalPageWidth = 1294;
                }
            }
            StatusBar.resizeStatus(totalPageWidth, statusList, statusList.length);
        }
    }

    private navigateTo(url:string) {
        navigateTo(this.isSaveButtonEnabled, this.disableExplicitSavingPage, this.isOnClosingCosts, url, this.closeOnReturn);
    }
}
function loanLeftNavigationDirective(): IDirective {
    return {
        restrict        : "EA",
        scope           : {},
        controller      : LoanLeftNavigationController,
        controllerAs    : "vm",
        template        : loanLeftNavigationHtml,
    };
}

module('LoanHeader', [])
    .directive("genericFrameworkVendorLinks", genericFrameworkVendorLinksDirective)
    .directive("loanInfoBar", loanInfoBarDirective)
    .directive("loanLeftNavigation", loanLeftNavigationDirective)
    ;


