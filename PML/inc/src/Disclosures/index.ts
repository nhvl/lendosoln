import { simpleDialog } from "../components/IframePopup";
import { TPOStyleUnification } from "../components/TPOStyleUnification";

const $ = jQuery;
import * as angular from "angular";
import { module, IScope, ILocationService, ITimeoutService, ISCEService } from "angular";
import * as ngRoute from "angular-route";
import * as ngSanitize from "angular-sanitize";

import { IServiceResult, disclosuresService } from "../api/gService";

declare const getAllFormValues: (clientID?:string, skipIgnore?:boolean) => {};
declare const lockField: (lockElement:HTMLInputElement, id:string) => void;

import { StrGuid } from "../utils/guid";
import { E_sDisclosureRegulationT } from "../DataLayer/Enums/E_sDisclosureRegulationT";

type IAuditType = "Fatal" | "Warning" | 'Critical';
interface IAAA {
    Severity:any,
    Message:string,
}

interface IDisclosureRequest {
    Id  : string,
    Type: string,
}

interface IActivityLog {
    Id                  : string;
    Time                : string;
    TimestampDescription: string;
    User                : string;
    Description         : string;
}

interface IDeadlineSection {
    sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD: string;
    sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD   : string;
    sInitialClosingDisclosureMailedDeadlineD                 : string;
    sInitialClosingDisclosureReceivedDeadlineD               : string;
}

interface IImportantDateSection {
    sAppSubmittedD                              : string;
    sAppSubmittedDLckd                          : boolean;
    sSubmitD                                    : string;
    sUseInitialLeSignedDateAsIntentToProceedDate: Date;
    sIntentToProceedD                           : string;
    sEstCloseD                                  : string;
    sEstCloseDLckd                              : boolean;
}

interface IAntiSteeringSection {
	sNoteIRSafeHarborWithRiskyFeatures: string;
	sNoteIRSafeHarborWithoutRiskyFeatures: string;
	sNoteIRSafeHarborLowestCost: string;
	sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures: string;
	sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures: string;
	sTotalDiscountPointOriginationFeeSafeHarborLowestCost: string;
	sNoteIR: string;
	sSumOfFees: string;
}

interface ILenderCompletionRequests {
    Type              : string;
    Status            : string;
    EnableActionButton: boolean;
    Timestamp         : string;
    Notes             : string;
}

interface IButtonModel {
    Hidden : boolean;
    Enabled: boolean;
    Tooltip: string;
}

interface ILoanEstimate {
    IssuedDate    : string;
    DeliveryMethod: string;
    ReceivedDate  : string;
    IsInitial     : boolean;
}

interface IClosingDisclosure {
    IssuedDate    : string;
    DeliveryMethod: string;
    ReceivedDate  : string;
    IsPreview     : boolean;
    IsInitial     : boolean;
    IsFinal       : boolean;
    IsPostClosing : boolean;
}

interface IMainVm {
    sLId                                      : StrGuid;
    sDisclosureRegulationT                    : E_sDisclosureRegulationT;
    orderInitialDisclosuresButtonModel        : IButtonModel;
    requestRedisclosureButtonModel            : IButtonModel;
    requestInitialClosingDisclosureButtonModel: IButtonModel;
    lenderCompletionRequests                  : ILenderCompletionRequests[];
    accountExecutiveData                      : any;
    importantDateSection                      : IImportantDateSection;
    deadlineSection                           : IDeadlineSection;
    loanEstimateSection                       : ILoanEstimate[];
    closingDisclosureSection                  : IClosingDisclosure[];
    activityLogSection                        : IActivityLog[];
    constants                                 : {
        TRID2015                              : number;
    };
}

const AccExcModalHeaderString = "Lender Account Excutive";
const DialogTimeout = 400;
const LenderRequestTextareaHeight = 70;

function displayErrorMessage(message: string, isWorkflowError: boolean) {
    if (isWorkflowError) {
        simpleDialog.alertWorkflow(message, "");
    }
    else {
        simpleDialog.alert(message, "");
    }
}

function displayLoadingPopup(displayText: string) {
    $('#LoadingText').text(displayText);
    LQBPopup.ShowElementWithWrapper($("#DisclosuresLoading"), {
        draggable      : false,
        hideXButton    : true,
        hideCloseButton: true,
        backdrop       : "static",
    });
}

function hideLoadingPopup() {
    LQBPopup.Hide();
}

function getPdfFrameHeight(sectionId:string) {
    var windowHeight = $(window).height()!;
    var disclosuresDivRect = document.getElementById('DisclosuresDiv')!.getBoundingClientRect();
    var sectionRect = document.getElementById(sectionId)!.getBoundingClientRect();

    // Set the height of the frame to fill the window with enough padding on the bottom.
    return Math.round(windowHeight - disclosuresDivRect.top - sectionRect.top);
}


function getServicePageArguments() {
    return {
        loanId       : ML.LoanId,
        applicationID: ML.ApplicationId,
    };
}

function displayRequestLenderPopup(args:{}, successCallback:()=>void, abortCallback:()=>void) {
    $('#DisclosureRequestNotes').val('').height(LenderRequestTextareaHeight);
    LQBPopup.ShowElementWithWrapper($('#DisclosureRequestPopup'),{
        headerText: "Request for Lender to Complete Initial Disclosure",
        draggable: false,
        hideXButton:  true,
        closeText: 'CANCEL',
        buttons:[{SUBMIT(){
            submitRequestForLenderToCompleteOrder(args, successCallback, abortCallback);
        }}],
        onShown(){
            const $text = $('#DisclosureRequestNotes');
            const $doneButton = $('button[name="SUBMIT"]');
            $doneButton.prop('disabled', true);
            $text.bind('input propertychange', function () { $doneButton.prop('disabled', !$text.val()); });
        }
    });
}

function submitRequestForLenderToCompleteOrder(args:any, successCallback:()=>void, abortCallback:()=>void) {
    const notes = ($('#DisclosureRequestNotes').val() as string).trim();
    if (!notes) return;

    const [err, value] = disclosuresService.RequestLenderToCompleteOrder({...args, Notes:notes});
    if (!!err) {
        displayErrorMessage(err.errorMessage, err.IsWorkflowError);
        if (value.AbortOrder === "True") abortCallback();
        return;
    }

    successCallback();
}



class DisclosuresController implements IMainVm {
    static ngName = "DisclosuresController";

    sLId                                      : StrGuid;
    sDisclosureRegulationT                    : E_sDisclosureRegulationT;
    orderInitialDisclosuresButtonModel        : IButtonModel;
    requestRedisclosureButtonModel            : IButtonModel;
    requestInitialClosingDisclosureButtonModel: IButtonModel;
    lenderCompletionRequests                  : ILenderCompletionRequests[];
    accountExecutiveData                      : { Name:string, Email:string, Phone:string } | null;
    importantDateSection                      : IImportantDateSection;
    deadlineSection                           : IDeadlineSection;
    loanEstimateSection                       : any[];
    closingDisclosureSection                  : any[];
    activityLogSection                        : IActivityLog[];
    constants                                 : {
        TRID2015                              : number;
    };

    private activityLogSectionReverse = false;
    private changeActivitySectionReverse() {
        this.activityLogSectionReverse = !this.activityLogSectionReverse;
    }

    private displayAccountExecutivePopup() {
        if (this.accountExecutiveData == null) { debugger; return; }

        const {Name, Email, Phone} = this.accountExecutiveData;
        $('#AeName').text(Name);
        $('#AeEmail').text(Email);
        $('#AePhone').text(Phone);
        $('.ae-data').show();

        LQBPopup.ShowElement($("#DisclosuresPopup"), {
            draggable  : false,
            backdrop   : true,
            hideXButton: true,
            width      : 350,
            closeText  : "OK",
            headerText : AccExcModalHeaderString,
        });
    }

    private refreshCalculation() {
        if (this.sDisclosureRegulationT != this.constants.TRID2015) return;

		const refreshArgs = Object.assign(getAllFormValues(), getServicePageArguments());

        const [err, DisclosuresViewModel] = disclosuresService.RefreshCalculation(refreshArgs);
        if (!!err) {
            displayErrorMessage(err.errorMessage, err.IsWorkflowError);
            return;
        }

        const { importantDateSection, deadlineSection } = DisclosuresViewModel
        this.importantDateSection = importantDateSection;
        this.deadlineSection      = deadlineSection;
        lockFields();
        updateDirtyBit();
    }

    private _save() {
        if (this.sDisclosureRegulationT != this.constants.TRID2015) return null;

		if (window.location.href.indexOf("AntiSteeringDisclosure") > 1) {
			const [err] = disclosuresService.SaveAntiSteeringDisclosure(Object.assign(getAllFormValues(), { loanId: this.sLId }));
			return err;
		}
		else {
			const [err] = disclosuresService.SaveLoanDates(Object.assign(getAllFormValues(), { loanId: this.sLId }));
			return err;
		}        
    }

    private getLeTypeDescription(dateInfo: IClosingDisclosure) {
        return (dateInfo.IsInitial) ? 'Initial LE' : 'Re-disclosed LE';
    }
    private getCdTypeDescription(dateInfo: IClosingDisclosure) {
        const descriptions = [
            dateInfo.IsPreview     ? 'Preview CD'      : null,
            dateInfo.IsInitial     ? 'Initial CD'      : null,
            dateInfo.IsFinal       ? 'Final CD'        : null,
            dateInfo.IsPostClosing ? 'Post-Closing CD' : null,
        ].filter(s => s != null);

        return (descriptions.length < 1) ? 'Re-disclosed CD' : descriptions.join(', ');
    }

    private displayCancelRequestDialog(disclosureRequest: IDisclosureRequest) {
        const popup = $('#DisclosureRequestPopup');

        $('#DisclosureRequestNotes').val('').height(LenderRequestTextareaHeight);

        LQBPopup.ShowElementWithWrapper(popup,{
            headerText: 'Cancel ' + disclosureRequest.Type,
            draggable: false,
            hideXButton: true,
            closeText: 'CANCEL',
            buttons:[{
                'SUBMIT' : () => {
                    this.cancelDisclosureRequest(disclosureRequest);
                }
            }],
            onShown(){
                const $text = $('#DisclosureRequestNotes');
                const $doneButton = $('button[name="SUBMIT"]');
                $doneButton.prop('disabled', true);
                $text.on('input propertychange', function () { $doneButton.prop('disabled', ($text.val() as string).trim().length < 1); });
            }
        });
    }
    private cancelDisclosureRequest(disclosureRequest: IDisclosureRequest) {
        const notes = ($('#DisclosureRequestNotes').val() as string).trim();
        if (!notes) return;

        const args = Object.assign(getServicePageArguments(), {
            Type : disclosureRequest.Type,
            Notes: notes,
        }, (typeof disclosureRequest.Id === "undefined") ? {} : {Id: disclosureRequest.Id});

        const [err] = disclosuresService.CancelDisclosureRequest(args);
        if (err != null) {
            displayErrorMessage(err.errorMessage, err.IsWorkflowError);
        } else {
            location.reload(true);
        }
    }

    private orderInitialLoanEstimate() {
        const errorMessage = Save();
        const errorMessageObject: any = errorMessage;
        if (errorMessage != null ) {
            var isString = typeof (errorMessageObject) == 'string';
            if (isString) {
                displayErrorMessage(errorMessage[0], false);
            }
            else if (errorMessageObject != null) {
                displayErrorMessage(errorMessageObject.errorMessage as string, errorMessageObject.IsWorkflowError as boolean);
            }
			return;
		}

		if (ML.IsAntiSteeringEnabled) {
			displayLoadingPopup('Generating request for anti-steering disclosure...');
			this.$location.path('/AntiSteeringDisclosure');
		}
		else {
			displayLoadingPopup('Generating request for review...');
			this.$location.path('/RequestReview');
		}
    }

    private requestRedisclosure            () { this._displayDisclosureRequestFormDialog(1 /* CoC / Redisclosure         */); }
    private requestInitialClosingDisclosure() { this._displayDisclosureRequestFormDialog(2 /* Initial Closing Disclosure */); }
    private _displayDisclosureRequestFormDialog(type:1|2) {
        LQBPopup.Show(`${VRoot}/webapp/DisclosureFormUpload.aspx?LoanID=${ML.LoanId}&ApplicationId=${ML.ApplicationId}&type=${type}`, {
            hideCloseButton: true,
            onReturn() { location.reload(true); }
        });
    }

    static $inject = ['$scope', '$location', '$timeout'];
    constructor($scope: IScope & any, private $location: ILocationService, $timeout: ITimeoutService) {
        const args = getServicePageArguments();
        const [err, DisclosuresViewModel] = disclosuresService.GetDisclosureViewModel(args);
        if (err) { displayErrorMessage(err.errorMessage, err.IsWorkflowError); return; }

        $scope.vm = Object.assign(this, DisclosuresViewModel);

        window.Save = () => {
            if (!isDirty()) return null;

            const errorMessage = this._save();
            if (!!errorMessage) return errorMessage;

            clearDirty();
            return null;
        }

        // Allow the view loading and digest cycle to complete before
        // setting up individual fields.
        $timeout(() => {
            lockFields();

            $('#sAppSubmittedD, #sIntentToProceedD, #sEstCloseD').keyup(date_keyup);

            TPOStyleUnification.Components();
            $('.warp-section').toggleClass("disclosures-page", this.sDisclosureRegulationT == this.constants.TRID2015);
        });
    }
}

AntiSteeringDisclosureController.$inject = '$scope $location $timeout'.split(" ");
function AntiSteeringDisclosureController($scope: IScope & any, $location: ILocationService, $timeout: ITimeoutService) {
	$scope.passImageUrl = VRoot + '/images/success.png';
	$scope.failImageUrl = VRoot + '/images/fail.png';
	$scope.warningImageUrl = VRoot + '/images/warning.svg';

	$scope.errorMessage = null;
	$scope.hasError = false;
	$scope.vm = null;

	$scope.isAntiSteeringEnabled = ML.IsAntiSteeringEnabled;	

	$scope.returnToMainPage = function () {
		$location.path('/');
	}

	$scope.goToRequestReview = function () {
		const errorMessage = Save();
		const errorMessageObject: any = errorMessage;
		if (errorMessage != null) {
			var isString = typeof (errorMessageObject) == 'string';
			if (isString) {
				displayErrorMessage(errorMessage[0], false);
			}
			else if (errorMessageObject != null) {
				displayErrorMessage(errorMessageObject.errorMessage as string, errorMessageObject.IsWorkflowError as boolean);
			}
			return;
		}		

		displayLoadingPopup('Generating request for review...');
		$location.path('/RequestReview');
	}

	$scope.getImageSource = function (passed: boolean) {
		return passed ? $scope.passImageUrl : $scope.failImageUrl;
	}

	$scope.copyFromCurrentLoan = function (index: number) {
		switch (index) {
			case 0:
				$('#sNoteIRSafeHarborWithRiskyFeatures').val($scope.vm.antiSteeringData.sNoteIR);
				$('#sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures').val($scope.vm.antiSteeringData.sSumOfFees);
				break;
			case 1:
				$('#sNoteIRSafeHarborWithoutRiskyFeatures').val($scope.vm.antiSteeringData.sNoteIR);
				$('#sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures').val($scope.vm.antiSteeringData.sSumOfFees);
				break;
			case 2:
				$('#sNoteIRSafeHarborLowestCost').val($scope.vm.antiSteeringData.sNoteIR);
				$('#sTotalDiscountPointOriginationFeeSafeHarborLowestCost').val($scope.vm.antiSteeringData.sSumOfFees);
				break;
			default:
				throw 'Invalid Operation.';
		}

		updateDirtyBit();
	}

	$scope.init = function () {
		disclosuresService.LoadAntiSteeringDisclosure(getServicePageArguments()).then(([error, value]) => {
			hideLoadingPopup();

			$('#WorkflowWarningImage').tooltip({
				position: {
					my: "center top+10",
					at: "center bottom"
				},
				content() {
					return $(this).attr('title');
				}
			});

			$timeout(() => {
				if (!!error) {
					if (value != null && value.AbortOrder === "True") {
						$scope.returnToMainPage();
					} else {
						$scope.hasError = true;
						displayErrorMessage(error, false);
					}
					return;
				}

				$scope.hasError = false;
				$scope.vm = value;

				setTimeout(() => $('#checkListTable a[data-toggle="tooltip"]').tooltip());
				TPOStyleUnification.CheckScrollbar();

			}, DialogTimeout);
		});

		TPOStyleUnification.Components();
		const loadIcon = $('#LoadingIcon');
		if (!loadIcon.children().length) {
			TPOStyleUnification.wrapByLoadingContent(loadIcon);
		}

		_initInput();
	}

	$scope.init();
}


RequestReviewController.$inject = '$scope $location $timeout'.split(" ");
function RequestReviewController($scope: IScope & any, $location: ILocationService, $timeout: ITimeoutService) {
    $scope.passImageUrl    = VRoot + '/images/success.png';
    $scope.failImageUrl    = VRoot + '/images/fail.png';
    $scope.warningImageUrl = VRoot + '/images/warning.svg';

    $scope.errorMessage = null;
    $scope.hasError     = false;
    $scope.vm           = null;

	$scope.isAntiSteeringEnabled = ML.IsAntiSteeringEnabled;

    $scope.returnToMainPage = function () {
        $location.path('/');
	}

	$scope.onAntiSteeringStatusClick = function () {
		displayLoadingPopup('Generating request for anti-steering disclosure...');
		$location.path('/AntiSteeringDisclosure');
	}

    $scope.requestLenderToCompleteOrder = function () {
        var callback = function () { $timeout($scope.returnToMainPage); }

		var args: any = getServicePageArguments();
		args.Source = ML.IsAntiSteeringEnabled ? ML.StepTwoEventSource : ML.StepOneEventSource;

        if ($scope.vm !== null && $scope.vm.checklist !== null) {
            args.SupplementalData = JSON.stringify($scope.vm.checklist.LineItems);
            args.BlockReason = 'Requirements failure';
        }
        else {
            args.BlockReason = 'Requirements failure: ' + $scope.errorMessage;
        }

        displayRequestLenderPopup(args, callback, callback);
    }

    $scope.goToDocumentVendorAudit = function () {
        displayLoadingPopup('Retrieving document vendor audit...');
        $location.path('/DocumentVendorAudit');
    }

    $scope.retryPreviewGeneration = function () {
        displayLoadingPopup('Generating request for review...');
        $scope.init();
    }

    $scope.getImageSource = function (passed:boolean) {
        return passed ? $scope.passImageUrl : $scope.failImageUrl;
    }

    $scope.init = function () {
        disclosuresService.LoadRequestReview(getServicePageArguments()).then(([error, value]) => {
            hideLoadingPopup();

            $('#WorkflowWarningImage').tooltip({
                position: {
                    my: "center top+10",
                    at: "center bottom"
                },
                content() {
                    return $(this).attr('title');
                }
            });

            $timeout(() => {
                if (!!error) {
                    if (value != null && value.AbortOrder === "True") {
                        $scope.returnToMainPage();
                    } else {
                        $scope.hasError = true;
                        displayErrorMessage(error, false);
                    }
                    return;
                }

                $scope.hasError = false;
                $scope.vm = value;

                setTimeout(() => $('#checkListTable a[data-toggle="tooltip"]').tooltip());
                TPOStyleUnification.CheckScrollbar();

            }, DialogTimeout);
        });

        TPOStyleUnification.Components();
        const loadIcon = $('#LoadingIcon');
        if (!loadIcon.children().length){
            TPOStyleUnification.wrapByLoadingContent(loadIcon);
        }
    }

    $scope.init();
}

DocumentVendorAuditController.$inject = '$scope $location $timeout'.split(" ");
function DocumentVendorAuditController($scope: IScope & any, $location: ILocationService, $timeout: ITimeoutService) {
    $scope.warningAuditImageUrl = VRoot + '/images/warn.png';
    $scope.fatalAuditImageUrl   = VRoot + '/images/fail.png';
    $scope.warningImageUrl      = VRoot + '/images/warning.svg';

    $scope.hasError             = false;
    $scope.errorMessage         = null;
    $scope.hasFatal             = false;
    $scope.auditResults         = null;

	$scope.isAntiSteeringEnabled = ML.IsAntiSteeringEnabled;

    $scope.returnToMainPage = function () {
        $location.path('/');
	}

	$scope.onAntiSteeringStatusClick = function () {
		displayLoadingPopup('Generating request for anti-steering disclosure...');
		$location.path('/AntiSteeringDisclosure');
	}

	$scope.onRequestReviewStatusClick = function () {
		displayLoadingPopup('Generating request for review...');
		$location.path('/RequestReview');
	}

    $scope.getAuditData = () => JSON.stringify($scope.auditResults.map((item: IAAA) => ({ Key: item.Severity, Value: item.Message })));

    $scope.requestLenderToCompleteOrder = function () {
        var callback = function () { $timeout($scope.returnToMainPage); }

		var args: any = getServicePageArguments();
		args.Source = ML.IsAntiSteeringEnabled ? ML.StepThreeEventSource : ML.StepTwoEventSource;

        if ($scope.auditResults === null) {
            args.BlockReason = 'Document vendor request error: ' + $scope.errorMessage;
        }
        else {
            args.SupplementalData = $scope.getAuditData();
            args.BlockReason = 'Document vendor audit failure';
        }

        displayRequestLenderPopup(args, callback, callback);
    }

    $scope.goToReviewDocumentPreview = function () {
        if ($scope.hasFatal || $scope.hasError) {
            return;
        }

        displayLoadingPopup('Placing document preview order...');
        $location.path('/ReviewDocumentPreview');
    }

    $scope.getAuditIcon = function (auditType:IAuditType) {
        switch (auditType) {
            case 'Fatal':
                return $scope.fatalAuditImageUrl;
            case 'Warning':
            case 'Critical':
                return $scope.warningAuditImageUrl;
            default:
                return '';
        }
    }

    $scope.retryAudit = function () {
        displayLoadingPopup('Retrieving document vendor audit...');
        $scope.init();
    }


    $scope.init = function () {
        disclosuresService.LoadDocumentAudit(getServicePageArguments()).then(([err, value]) => {
            hideLoadingPopup();

            $('#WorkflowWarningImage').tooltip({
                position: {
                    my: "center top+10",
                    at: "center bottom"
                },
                content: function () {
                    return $(this).attr('title');
                }
            });

            $timeout(function () {
                if (!!err) {
                    if (value == null) {
                        $scope.hasError = true;
                        displayErrorMessage(err, false);
                    } else if (value.AbortOrder === "True") {
                        $scope.returnToMainPage();
                    } else {
                        $scope.errorMessage = value && value.BlockReason;
                        $scope.hasError = true;
                    }

                    return;
                }

                $scope.hasError = false;
                $scope.hasFatal = value.HasFatal === "True";
                $scope.auditResults = JSON.parse(value.DocumentAuditResults);
                TPOStyleUnification.UnifyDiclosureIcons();
                TPOStyleUnification.CheckScrollbar();
            }, DialogTimeout);
        });

        TPOStyleUnification.Components();
    }

    $scope.init();
}

DocumentPreviewController.$inject = '$scope $location $timeout $sce'.split(" ");
function DocumentPreviewController($scope: IScope & any, $location: ILocationService, $timeout: ITimeoutService, $sce: ISCEService) {
    $scope.errorMessage = null;
    $scope.hasError = false;
    $scope.vm = {
		hasAcceptedPreview: false,
	};
    $scope.pdfLocation = null;
	$scope.frameHeight = 0;

	$scope.isAntiSteeringEnabled = ML.IsAntiSteeringEnabled;

    $scope.returnToMainPage = function () {
        // In IE versions below Edge, the PDF will be rendered by third-party software like
        // Adobe. This causes issues when trying to switch which page is being displayed
        // by the view, as the next page will often display blank due to rendering jank.
        // Hide the PDF frame to prevent this issue.
        $('#DocumentPreviewFrame').hide();
        $(window).off('resize');
        $timeout(function () { $location.path('/'); });
	}

	$scope.onAntiSteeringStatusClick = function () {
		// In IE versions below Edge, the PDF will be rendered by third-party software like
		// Adobe. This causes issues when trying to switch which page is being displayed
		// by the view, as the next page will often display blank due to rendering jank.
		// Hide the PDF frame to prevent this issue.
		$('#DocumentPreviewFrame').hide();
		$(window).off('resize');
		$timeout(function () {
			$location.path('/');
			displayLoadingPopup('Generating request for anti-steering disclosure...');
			$location.path('/AntiSteeringDisclosure');
		});		
	}

	$scope.onRequestReviewStatusClick = function () {
		// In IE versions below Edge, the PDF will be rendered by third-party software like
		// Adobe. This causes issues when trying to switch which page is being displayed
		// by the view, as the next page will often display blank due to rendering jank.
		// Hide the PDF frame to prevent this issue.
		$('#DocumentPreviewFrame').hide();
		$(window).off('resize');
		$timeout(function () {
			$location.path('/');
			displayLoadingPopup('Generating request for review...');
			$location.path('/RequestReview');
		});		
	}

	$scope.onVendorAuditStatusClick = function () {
		// In IE versions below Edge, the PDF will be rendered by third-party software like
		// Adobe. This causes issues when trying to switch which page is being displayed
		// by the view, as the next page will often display blank due to rendering jank.
		// Hide the PDF frame to prevent this issue.
		$('#DocumentPreviewFrame').hide();
		$(window).off('resize');
		$timeout(function () {
			$location.path('/');
			displayLoadingPopup('Retrieving document vendor audit...');
			$location.path('/DocumentVendorAudit');
		});
	}

    $scope.requestLenderToCompleteOrder = function () {
        var callback = function () { $timeout($scope.returnToMainPage); }

        var args: any = getServicePageArguments();
		args.Source = ML.IsAntiSteeringEnabled ? ML.StepFourEventSource : ML.StepThreeEventSource;
        args.BlockReason = 'Document vendor request error: ' + $scope.errorMessage;

        displayRequestLenderPopup(args, callback, callback);
    }

    $scope.goToRequestComplete = function () {
        if ($scope.hasError || !$scope.vm.hasAcceptedPreview || $scope.pdfLocation === null) {
            return;
        }

        // Same situation as described in returnToMainPage.
        $('#DocumentPreviewFrame').hide();
        $(window).off('resize');
        displayLoadingPopup('Placing document order...');
        $timeout(function () { $location.path('/RequestComplete'); });
    }

    $scope.successCallback = function (result:IServiceResult) {
        $timeout(function () {
            if (result.error || result.value.HasError === "True") {
                displayErrorMessage(result.UserMessage || result.value.ErrorMessage || 'System error. Please contact us if this happens again.', result.IsWorkflowError);

                if (result.value.AbortOrder === "True") {
                    $timeout($scope.returnToMainPage);
                }
                else {
                    $scope.errorMessage = result.value && result.value.BlockReason;
                    $scope.hasError = true;
                }

                return;
            }

            $scope.hasError = false;
            $scope.pdfLocation = $sce.trustAsResourceUrl(`${VRoot}/webapp/DocumentViewer.aspx?loanid=${ML.LoanId}&key=${result.value.PdfFileDbKey}`);

            $scope.frameHeight = getPdfFrameHeight('DocumentPreviewSection');
            $(window).resize(function () {
                $('#DocumentPreviewFrame').css('height', getPdfFrameHeight('DocumentPreviewSection'));
            });
			TPOStyleUnification.Components();
			TPOStyleUnification.CheckScrollbar();
        }, DialogTimeout);
    }

    $scope.failureCallback = function(errorMessage:string) {
        setTimeout(function () {
            $scope.hasError = true;
            displayErrorMessage(errorMessage, false);
        }, DialogTimeout);
    }

    $scope.retryPreviewOrder = function () {
        $('#DocumentPreviewFrame').hide();
        $(window).off('resize');
        displayLoadingPopup('Placing document preview order...');
        $scope.init();
    }

    $scope.init = function () {
        $('#ReviewRequiredImage').prop('src', VRoot + '/images/error_icon.gif');
        disclosuresService.serviceAsync('PlaceDocumentPreviewOrder', getServicePageArguments()).then(([err, res]) => {
            hideLoadingPopup();
            if (!!err) {
                $scope.failureCallback(err);
                return;
            }
            $scope.successCallback(res);
        });

        TPOStyleUnification.Components();
    }

    $scope.init();
}

RequestCompleteController.$inject = '$scope $location $timeout $sce'.split(" ");
function RequestCompleteController($scope: IScope & any, $location: ILocationService, $timeout: ITimeoutService, $sce: ISCEService) {
    $scope.hasError = false;
    $scope.errorMessage = null;
    $scope.pdfLocation = null;
    $scope.frameHeight = 0;

	$scope.isAntiSteeringEnabled = ML.IsAntiSteeringEnabled;

    $scope.returnToMainPage = function () {
        $('#RequestCompleteFrame').hide();
        $(window).off('resize');
        $timeout(function () { $location.path('/'); });
    }

    $scope.requestLenderToCompleteOrder = function () {
        var callback = function () { $timeout($scope.returnToMainPage); }

        var args: any = getServicePageArguments();
		args.Source = ML.IsAntiSteeringEnabled ? ML.StepFiveEventSource : ML.StepFourEventSource;
        args.BlockReason = 'Document vendor request error: ' + $scope.errorMessage;

        displayRequestLenderPopup(args, callback, callback);
    }

    $scope.successCallback = function (result:IServiceResult) {
        $timeout(function () {
            if (result.error || result.value.HasError === "True") {
                displayErrorMessage(result.UserMessage || result.value.ErrorMessage || 'An error has occurred while processing your request. If this error happens again, please request the lender to complete the order on your behalf.', result.IsWorkflowError);

                if (result.value.AbortOrder === "True") {
                    $timeout($scope.returnToMainPage);
                }
                else {
                    $scope.errorMessage = result.value && result.value.BlockReason;
                    $scope.hasError = true;
                }

                return;
            }

            $scope.hasError = false;
            $scope.pdfLocation = $sce.trustAsResourceUrl(`${VRoot}/webapp/DocumentViewer.aspx?loanid=${ML.LoanId}&key=${result.value.PdfFileDbKey}`);

            $scope.frameHeight = getPdfFrameHeight('RequestCompleteSection');
            $(window).resize(function () {
                $('#RequestCompleteFrame').css('height', getPdfFrameHeight('RequestCompleteSection'));
            });
            TPOStyleUnification.CheckScrollbar();
        }, DialogTimeout);
    }

    $scope.failureCallback = function (result:IServiceResult) {
        setTimeout(function () {
            displayErrorMessage('An error has occurred while processing your request. If this error happens again, please request the lender to complete the order on your behalf.', result.IsWorkflowError);
            $scope.hasError = true;
        }, DialogTimeout);
    }

    $scope.retryDocumentOrder = function () {
        $('#RequestCompleteFrame').hide();
        $(window).off('resize');
        displayLoadingPopup('Placing document order...');
        $scope.init();
    }

    $scope.init = function () {
        disclosuresService.serviceAsync('PlaceActualDocumentOrder', getServicePageArguments()).then(([err, res]) => {
            hideLoadingPopup();
            if (!!err) $scope.failureCallback(err);
            else $scope.successCallback(res);
        });

        TPOStyleUnification.Components();
    }

    $scope.init();
}


const disclosures = module('Disclosures', [ngRoute, ngSanitize, 'LoanHeader']);

import { sortIcon } from "../ng-common/sortIcon";
disclosures.directive('sortIcon', sortIcon);

import htmlLoanEstimateClosingDisclosureDatesSection from "./LoanEstimateClosingDisclosureDatesSection.html";
disclosures.directive('lecdDatesSection', function () {
    return {
        restrict              : 'EA',
        replace               : true,
        scope                 : {
            id                : '@',
            section           : '@',
            source            : '=',
            getTypeDescription: "=",
        },
        template              : htmlLoanEstimateClosingDisclosureDatesSection,
    }
});

import htmlOrderInitialDisclosuresHeader from "./OrderInitialDisclosures/OrderInitialDisclosuresHeader.html";
disclosures.directive('orderInitialDisclosuresHeader', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
			step: '='
        },
        link: (scope:IScope & any, element, attrs) => {
            // To keep control logic out of this directive,
            // we'll have methods in the directive's scope
            // point to the parent's controller.
            scope.returnToMainPage             = scope.$parent.returnToMainPage;
			scope.requestLenderToCompleteOrder = scope.$parent.requestLenderToCompleteOrder;
			scope.isAntiSteeringEnabled = ML.IsAntiSteeringEnabled;
			scope.onAntiSteeringStatusClick = scope.$parent.onAntiSteeringStatusClick;
			scope.onRequestReviewStatusClick = scope.$parent.onRequestReviewStatusClick;
			scope.onVendorAuditStatusClick = scope.$parent.onVendorAuditStatusClick;
        },
        template: htmlOrderInitialDisclosuresHeader,
    }
});

import htmlMain from "./Main.html";
import htmlAntiSteeringDisclosure from "./AntiSteeringDisclosure.html";
import htmlRequestReview         from "./RequestReview.html";
import htmlDocumentVendorAudit   from "./DocumentVendorAudit.html";
import htmlReviewDocumentPreview from "./ReviewDocumentPreview.html";
import htmlRequestComplete       from "./RequestComplete.html";
disclosures.config(['$routeProvider', ($routeProvider:any) => {
    $routeProvider.
		when('/', { template: htmlMain, controller: DisclosuresController }).
		when('/AntiSteeringDisclosure/', { template: htmlAntiSteeringDisclosure, controller: AntiSteeringDisclosureController }).
        when('/RequestReview/'        , {template: htmlRequestReview        , controller: RequestReviewController       }).
        when('/DocumentVendorAudit/'  , {template: htmlDocumentVendorAudit  , controller: DocumentVendorAuditController }).
        when('/ReviewDocumentPreview/', {template: htmlReviewDocumentPreview, controller: DocumentPreviewController     }).
        when('/RequestComplete/'      , {template: htmlRequestComplete      , controller: RequestCompleteController     });
}]);

function lockFields() {
    const sAppSubmittedDLckd = document.getElementById('sAppSubmittedDLckd') as HTMLInputElement | null;
    if (sAppSubmittedDLckd != null) lockField(sAppSubmittedDLckd, 'sAppSubmittedD');

    const sEstCloseDLckd = document.getElementById('sEstCloseDLckd') as HTMLInputElement | null;
    if (sEstCloseDLckd != null) lockField(sEstCloseDLckd, 'sEstCloseD');

    $('a[class*="calendar-svg"]',$('#sAppSubmittedD').parent()).toggleClass("disabled", $('#sAppSubmittedD').prop("readonly")||$('#sAppSubmittedD').prop("disabled"));
    $('a[class*="calendar-svg"]',$('#sEstCloseD'    ).parent()).toggleClass("disabled", $('#sEstCloseD')    .prop("readonly")||$('#sEstCloseD')    .prop("disabled"));
}
