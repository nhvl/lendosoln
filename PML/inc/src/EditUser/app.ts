/// <reference types="jquery" />

import { IframePopup as _IframePopup, IframeSelf as _IframeSelf, LqbPopup as _LqbPopup, SimpleDialog } from "../components/IframePopup";
import { TPOStyleUnification as _TPOStyleUnification } from "../components/TPOStyleUnification";
import ServiceCredential from '../ServiceCredential/app';
import { setTimeout } from "timers";
declare const simpleDialog: SimpleDialog;
declare const LqbPopup: typeof _LqbPopup;
declare const IframeSelf: typeof _IframeSelf;
declare const IframePopup: typeof _IframePopup;
declare const TPOStyleUnification: typeof _TPOStyleUnification;
declare const autoClose: boolean | undefined;
declare const passwordGuidelines: string;
declare function Page_ClientValidate(): void;
declare function saveAllLicenses(): void;
declare function __doPostBack(command: string, args: string): void;
declare function logJSException(e: Error, m: string): void;
declare const gService: any;

declare global {
    interface Window {
        dialogArguments: any;
        resize(w: number, h: number): void;
        validateUserInfoPage(): void;
        dirtySearch(): void;
        autoClose: boolean;
    }
}

declare const m_AssignedBrokerProcessorName             : HTMLInputElement;
declare const m_AssignedCorrExternalPostCloserId        : HTMLInputElement;
declare const m_AssignedCorrExternalPostCloserName      : HTMLInputElement;
declare const m_AssignedMiniCorrExternalPostCloserId    : HTMLInputElement;
declare const m_AssignedMiniCorrExternalPostCloserName  : HTMLInputElement;
declare const m_CycleExpiration                         : HTMLInputElement;
declare const m_Dirty                                   : HTMLInputElement;
declare const m_ExpirationDate                          : HTMLInputElement;
declare const m_ExpirationPeriod                        : HTMLInputElement;
declare const m_ExternalPostCloser                      : HTMLInputElement;
declare const m_IsAccountLocked                         : HTMLInputElement;
declare const m_MustChangePasswordAtNextLogin           : HTMLInputElement;
declare const m_PasswordExpiresOn                       : HTMLInputElement;
declare const m_PCPickerUrl                             : HTMLInputElement;
declare const m_UnlockAccountPanel                      : HTMLInputElement;
declare const m_BrokerProcessor                         : HTMLInputElement;
declare const m_BPPickerUrl                             : HTMLInputElement;
declare const m_AssignedBrokerProcessorId               : HTMLInputElement;
declare const m_Login                                   : HTMLInputElement;
declare const RequireCellPhone                          : HTMLInputElement;
declare const RequiredFieldMessage                      : HTMLInputElement;

declare const expirationPeriodDisabled                  : boolean;
declare const cycleExpirationDisabled                   : boolean;
declare const expirationDateDisabled                    : boolean;
declare const employeeId                                : string;
declare const IsPostBack                                : boolean;

interface IPickResult {
    employeeId:string,
    fullName:string,
}

const $ = jQuery;

const guidEmpty = "00000000-0000-0000-0000-000000000000";
const m_CurrentTab = document.getElementById('m_CurrentTab') as HTMLInputElement;
const m_Active      = document.getElementById("m_Active"     ) as HTMLInputElement;
const m_Inactive    = document.getElementById("m_Inactive"   ) as HTMLInputElement;
const m_ChangeLogin = document.getElementById("m_ChangeLogin") as HTMLInputElement;
const m_Password    = document.getElementById("m_Password"   ) as HTMLInputElement;
const m_Confirm     = document.getElementById("m_Confirm"    ) as HTMLInputElement;
const m_LoginNotes  = document.getElementById("m_LoginNotes" ) as HTMLDivElement  ;

$(function() {
    const iframeSelf = new IframeSelf();
    if (!!window.autoClose) {
        setTimeout(function () { iframeSelf.resolve(true); }, 0);
        return;
    }

    const popup = new LqbPopup(document.querySelector("div[lqb-popup]") as HTMLDivElement);
    iframeSelf.popup = popup;

    TPOStyleUnification.Components();
    onTabClick(document.getElementById(m_CurrentTab!.value)!);
    $('#tabs a').on('click.Tabstrip', function(this:HTMLElement, event) { onTabClick(this) });

    //if the account is locked, display the account locked message and unlock button
    m_UnlockAccountPanel.hidden = m_IsAccountLocked.value != "true";

    const errorMessage = document.getElementById('m_errorMessage') as HTMLInputElement;
    if (errorMessage == null)
    {
        const m_isUpdated = document.getElementById('m_isUpdated') as HTMLInputElement;
        if( m_isUpdated != null && m_isUpdated.value.toLowerCase() == 'true' ) dirtyParent();
    }
    else
    {
        if (errorMessage.value) {
            setTimeout(showErrorPopup, 100, errorMessage.value);
        }
    }

    if (!IsPostBack) {
        if (window.dialogArguments != null) {
            window.resize(700, 520);
        }

        m_Dirty.value = "0";
    }

    if (document.getElementById('ServiceCredentialsJson'))
    {
        ServiceCredential.Initialize({
            EmployeeId: employeeId,
            ServiceCredentialJsonId: 'ServiceCredentialsJson',
            ServiceCredentialTableId: 'ServiceCredentialsTable'
        });
        ServiceCredential.RenderCredentials();
        $('#AddServiceCredentialBtn').on('click', ServiceCredential.AddServiceCredential);
    }

    validatePage();
    f_setBrokerProcessorPickerStatus();
    $('#m_AssignedBrokerProcessor').text(m_AssignedBrokerProcessorName.value + "   ");

    f_setExternalPostCloserPickerStatus();
    $('#m_AssignedMiniCorrExternalPostCloser').text(m_AssignedMiniCorrExternalPostCloserName.value + "   ");
    $('#m_AssignedCorrExternalPostCloser').text(m_AssignedCorrExternalPostCloserName.value + "   ");

    m_ExpirationPeriod.disabled = expirationPeriodDisabled;
    m_CycleExpiration.disabled = cycleExpirationDisabled;
    TPOStyleUnification.ToggleDisabledCheckbox($(m_CycleExpiration), cycleExpirationDisabled);

    m_ExpirationDate.disabled = expirationDateDisabled;

    // 04/12/06 mf HACK: For some reason when you give this window focus by closing it,
    // it attemps to highlight the last selected field (which doesn't exist anymore) and
    // causes an error. This hack removes the input onfocus handlers when the window closes
    $(window).on('unload', function() {
        $('input').off('.highlightField');
    });

    $('.viewLoan').on('click', function(this:HTMLInputElement) {
        const hasViewPermission = this.checked;
        const $createCb = $(this).closest('.margin-bottom-edit-user').next().find('.createLoan').first();
        $createCb.prop('disabled', !hasViewPermission);
        if (!hasViewPermission) {
            $createCb.prop('checked', false);
        }
        TPOStyleUnification.ToggleDisabledCheckbox($createCb, $createCb.is(':disabled'));
    });

    $('.createLoan').not(':checked').each((index:number, element:HTMLElement) => {
        const checkbox = element as HTMLInputElement;
        const $viewCb = $(element).closest('.margin-bottom-edit-user').prev().find('.viewLoan').first();
        if (!$viewCb.prop('checked')) {
            checkbox.disabled = true;
        }
        TPOStyleUnification.ToggleDisabledCheckbox($(checkbox), checkbox.disabled);
    });

    $("#EnableAuthCodeViaSms").click(function(this:HTMLInputElement) {
        $(RequireCellPhone).val(this.checked ? "True" :  "False").trigger("change");
    }).triggerHandler('click');

    $('#DisableAuthenticator').click(function(this: HTMLInputElement){
        $('#EnableAuthCodeViaAuthenticator').prop('checked', false);
        $(this).addClass('Hidden');
        $('#IsDisablingAuthenticator').val("True");
        m_Dirty.value = "1";
    });

    function setMaterialCheckboxDisabled(checkbox:HTMLInputElement, disable: boolean){
        (checkbox.parentElement as any).MDCCheckbox.disabled = disable;
    }

    function onActiveChange(){
        m_Dirty.value = "1";

        setMaterialCheckboxDisabled(m_ChangeLogin, !m_Active.checked);
        if (!m_Active.checked) m_ChangeLogin.checked = false;

        onChangeLoginChange();
    }

    function onChangeLoginChange(){
        $(".changeLoginNote").prop("hidden", !m_ChangeLogin.checked);
        [m_Login, m_Password, m_Confirm].forEach((input: HTMLInputElement) => input.readOnly = !m_Active.checked || !m_ChangeLogin.checked);
        m_LoginNotes.hidden = !m_Active.checked || !m_ChangeLogin.checked;
    }

    $(m_ChangeLogin).on("change", onChangeLoginChange);
    $(m_Active).on("change", onActiveChange);
    $(m_Inactive).on("change", onActiveChange);

    (function wrapValidateUserInfoPage(){
        if (window.validateUserInfoPage == null) {
            setTimeout(wrapValidateUserInfoPage);
        } else {
            const {validateUserInfoPage} = window;
            window.validateUserInfoPage = () => {
                validateUserInfoPage();
                validatePage();
            }
            validatePage();
        }
    })();

    function onCancelClick() {
        if ( isUserEditorDirty() ) {
            confirmExitPopup("Close without saving?");
        }
        else {
            const shouldReload = $("#HasSaved").val() == "true";
            iframeSelf.resolve(shouldReload);
        }
    }

    function onClientOkClick() {
        if ( isUserEditorDirty() ) {
            // We'll call this method to make sure the required
            // field validators have performed their validation.
            // If we don't, the validation may not have been trigged
            // just by changing the field's value, resulting in the
            // validatePage() call not picking up missing fields.
            if (typeof Page_ClientValidate === "function")
            {
                Page_ClientValidate();
            }

            if (!validatePage()) {
                return;
            }

            if (typeof saveAllLicenses === 'function') {
                saveAllLicenses();
            }
            $('#m_Ok').trigger('click');
        } else {
            const willReload = $("#HasSaved").val() == "true";
            selfClose(willReload);
            // window.parent.LQBPopup.Hide();
            // if(willReload)
            // {
            //     window.parent.reloadUsersManager();
            // }
        }
    }

    function selfClose(shouldReload:boolean) {
        iframeSelf.resolve(shouldReload);
    }

    function onClientApplyClick() {
        if ( isUserEditorDirty() ) {
            // Same as above, we want to ensure that the
            // required field validators have performed
            // their validation before calling validatePage().
            if (typeof Page_ClientValidate === "function")
            {
                Page_ClientValidate();
            }

            if (!validatePage()) {
                return;
            }

            if (typeof saveAllLicenses === 'function') {
                saveAllLicenses();
            }
            $('#m_Apply').trigger('click');
        } else {
            iframeSelf.resolve(null);
            // window.parent.LQBPopup.Hide();
        }
    }

    function revokeClientCertificate(certificateId:string) {
        simpleDialog.confirm('Do you want to revoke the client certificate?').then((isConfirm:boolean) => {
            if (!isConfirm) return;
            __doPostBack('ClientCertificateRevoke', certificateId);
        });
        return false;
    }

    function deleteIp(id:string) {
        simpleDialog.confirm('Do you want to delete IP?').then((isConfirm:boolean) => {
            if (!isConfirm) return;
            __doPostBack('RegisteredIpDelete', id);
        });
        return false;
    }

    function validatePage() {
        const errorMessageLabel = document.getElementById('m_RequiredFieldMessage')!;
        var changeLoginCB = document.getElementById('m_ChangeLogin') as HTMLInputElement;

        const roleRequired = ['m_LoanOfficer', 'm_BrokerProcessor', 'm_ExternalSecondary', 'm_ExternalPostCloser']
            .every(id => !(document.getElementById(id) as HTMLInputElement).checked);

        const requiredLogin = (changeLoginCB == null || changeLoginCB.checked);

        const missingFields = (
            [
                                 !(m_Login   .value.trim()) ? "Login Name"      : null,
                requiredLogin && !(m_Password.value       ) ? "Password"        : null,
                requiredLogin && !(m_Confirm .value       ) ? "Retype Password" : null,
                !!roleRequired                              ? "Roles"           : null,
            ]
            .filter(x => !!x)
        ) as string[];

        var isValid = true;
        if (requiredLogin)
            isValid = isValid && isNotEmptyTextbox('m_Login');
        if (isNotEmptyTextbox('m_Password') || isNotEmptyTextbox('m_Confirm'))
            isValid = isValid && isNotEmptyTextbox('m_Password') && isNotEmptyTextbox('m_Confirm');
        if (roleRequired)
            isValid = false;

        var innerError = $(RequiredFieldMessage).text();
        $(RequiredFieldMessage).hide();

        if (!!innerError)
        {
            isValid = false;
            const fs = [innerError.replace("Please fill out all required fields:", "")].concat(missingFields);
            $(errorMessageLabel).text(fs.length < 1 ? "" : `Please fill out all required fields: ${fs.join(", ")}`);
        }
        else
        {
            $(errorMessageLabel).text(missingFields.length < 1 ? "" : `Please fill out all required fields: ${missingFields.join(", ")}`);
        }
        console.log("validatePage");

        validateLoanViewingPermissions();
        validateLoanCreationPermissions();

        return isValid;
    }

    function isUserEditorDirty() {
        return m_Dirty.value == "1";
    }

    function confirmExitPopup(message:string) {
        simpleDialog.confirm(message, "", "Yes", "Cancel").then((isConfirm:boolean) => {
            if (!isConfirm) return;
            iframeSelf.resolve(false);
        });
    }

    function validateLoanCreationPermissions()
    {
        //Check Loan Creation Permissions
        if(
            ($("#AllowWholesaleCreateLoanRow").css("display") != "none" && $("#m_AllowCreateWholesaleChannelLoans").prop("checked")) ||
            ($("#AllowMiniCorrCreateLoanRow" ).css("display") != "none" && $("#m_AllowCreateMiniCorrChannelLoans" ).prop("checked")) ||
            ($("#AllowCorrCreateLoanRow"     ).css("display") != "none" && $("#m_AllowCreateCorrChannelLoans"     ).prop("checked"))
        ) {
            $("#m_LoanCreationMessage").hide();
        }
        else
        {
            $("#m_LoanCreationMessage").show();
        }
    }

    function validateLoanViewingPermissions()
    {
        if(
            ($("#AllowWholesaleViewingLoanRow").css("display") != "none" && $("#m_AllowViewingWholesaleChannelLoans").prop("checked")) ||
            ($("#AllowMiniCorrViewingLoanRow" ).css("display") != "none" && $("#m_AllowViewingMiniCorrChannelLoans" ).prop("checked")) ||
            ($("#AllowCorrViewingLoanRow"     ).css("display") != "none" && $("#m_AllowViewingCorrChannelLoans"     ).prop("checked")) )
        {
            $("#m_LoanViewingMessage").hide();
        }
        else
        {
            $("#m_LoanViewingMessage").show();
        }
    }

    function isNotEmptyTextbox(elementId:string) {
        return (document.getElementById(elementId) as HTMLInputElement).value.replace(/^\s+|\s+$/g, '') != '';
    }

function showErrorPopup(message:string) {
    simpleDialog.alert(message);
}

function onTabClick( oTab:HTMLElement ) {
    try
    {
        if( oTab == null || oTab.id.indexOf( "Tab" ) == -1 ) return;

        $("#tabs>li").removeClass("active");
        $(".tabDiv").prop("hidden", true);

        $(oTab).parent().addClass("active");

        switch( oTab.id )
        {
            case "TabA":
                document.getElementById('BaseA')!.hidden = false;
                break;
            case "TabB":
                document.getElementById('BaseB')!.hidden = false;
                break;
            case "TabC":
                document.getElementById('BaseC')!.hidden = false;
                break;
            case "TabD":
                document.getElementById('BaseD')!.hidden = false;
                break;
            case "TabE":
                document.getElementById('BaseE')!.hidden = false;
                break;
            case "TabF":
                document.getElementById('BaseF')!.hidden = false;
                break;
            case "TabG":
                document.getElementById('BaseG')!.hidden = false;
                break;
            case "TabH":
                document.getElementById('BaseH')!.hidden = false;
                break;
            case 'TabI':
                document.getElementById('BaseI')!.hidden = false;
                break;
        }
        m_CurrentTab.value = oTab.id;
    }
    catch( e )
    {
        logJSException(e, 'problem changing tabs.');
        showErrorPopup(e);
        showErrorPopup( e.message );
    }
}

function onUnlockAccountClick() {
    simpleDialog.confirm("Unlock this user's account?", "", "Yes", "No").then((isConfirm:boolean) => {
        if (!isConfirm) return;

        const result = gService.main.call("UnlockAccount", {
            login: m_Login.value,
            id   : employeeId,
        });
        if (result.value && result.value.ErrorMessage)
            showErrorPopup(result.value.ErrorMessage);
        else
            m_UnlockAccountPanel.hidden = true;
    });
}

function updateDirtyBit_callback() {
    // This functionality is based on the keyup event, which makes
    // it too sensitive.  In the future, we could filter out some
    // of the situations where the data would not change, such as
    // Control-C if someone wants to copy data with out marking as
    // dirty.  For now, we stay in sync with our system framework.
    setTimeout(validatePage);
    m_Dirty.value = "1";
}

function dirtyParent() {
    if ( window.parent && window.parent.dirtySearch )
        window.parent.dirtySearch();
}

// 2-18-09 jk OPM 25370 - new password div
function showPasswordRules() {
    const {header, content} = TPOStyleUnification.GetPasswordGuidelines(passwordGuidelines);
    // LQBPopup.ShowElement($(`<div><div class="modal-show-rules">${content}</div><div>`),{
    //     headerText:header,
    //     hideXButton:true,
    //     draggable:false
    // });
    simpleDialog.alert(`<div class="modal-show-rules" style="max-width:600px">${content}</div>`, header, "Close");
}

function expirationPolicyClick()
{
    m_Dirty.value = "1";

    if( m_PasswordExpiresOn.checked || m_MustChangePasswordAtNextLogin.checked )
    {
        expirationCycleClick();
        m_CycleExpiration.disabled = false;
    }
    else
    {
        m_ExpirationPeriod.disabled = true;
        m_CycleExpiration.disabled  = true;
        m_CycleExpiration.checked = false;
    }

    m_ExpirationDate.disabled = !m_PasswordExpiresOn.checked;

    TPOStyleUnification.ToggleDisabledCheckbox($(m_CycleExpiration),m_CycleExpiration.disabled);
}

function expirationCycleClick()
{
    m_Dirty.value = "1";
    m_ExpirationPeriod.disabled = m_CycleExpiration.checked?false:true;
}

function f_assignMiniCorrExternalPostCloser() {
    if (m_ExternalPostCloser.checked) return;

    IframePopup.show<IPickResult>(m_PCPickerUrl.value, {}).then(([error, results]) => {
        if (error != null) {
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            return;
        }

        if (results == null) return;
        m_AssignedMiniCorrExternalPostCloserId.value = results.employeeId;
        m_AssignedMiniCorrExternalPostCloserName.value = results.fullName;
        $('#m_AssignedMiniCorrExternalPostCloser').text(results.fullName + "   ");
        updateDirtyBit_callback();
    });
}

function f_clearMiniCorrExternalPostCloser(alertUser:boolean) {
    if (!m_ExternalPostCloser.checked) return;

    if (m_AssignedMiniCorrExternalPostCloserId.value != guidEmpty) {
        if (alertUser) showErrorPopup('The previous Post-Closer relationship was cleared.');

        updateDirtyBit_callback();
    }

    m_AssignedMiniCorrExternalPostCloserId.value = guidEmpty;
    m_AssignedMiniCorrExternalPostCloserName.value = "<-- None -->";
    $('#m_AssignedMiniCorrExternalPostCloser').text("<-- None -->   ");
}

function f_assignCorrExternalPostCloser() {
    if (m_ExternalPostCloser.checked) return;

    IframePopup.show<IPickResult>(m_PCPickerUrl.value, {}).then(([error, results]) => {
        if (error != null) {
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            return;
        }

        if (results == null) return;

        m_AssignedCorrExternalPostCloserId.value = results.employeeId;
        m_AssignedCorrExternalPostCloserName.value = results.fullName;
        $('#m_AssignedCorrExternalPostCloser').text(results.fullName + "   ");
        updateDirtyBit_callback();
    });
}

function f_clearCorrExternalPostCloser(alertUser:boolean) {
    if (!m_ExternalPostCloser.checked) return;

    if (m_AssignedCorrExternalPostCloserId.value != guidEmpty) {
        if (alertUser) showErrorPopup('The previous Post-Closer relationship was cleared.');

        updateDirtyBit_callback();
    }

    m_AssignedCorrExternalPostCloserId.value = guidEmpty;
    m_AssignedCorrExternalPostCloserName.value = "<-- None -->";
    $('#m_AssignedCorrExternalPostCloser').text("<-- None -->   ");
}

function f_setExternalPostCloserPickerStatus() {
    const isChecked = m_ExternalPostCloser.checked;

    [
        document.getElementById("m_miniCorrExternalPostCloserPickerNoneLink"),
        document.getElementById("m_miniCorrExternalPostCloserPickerPickLink"),
        document.getElementById("m_corrExternalPostCloserPickerNoneLink"),
        document.getElementById("m_corrExternalPostCloserPickerPickLink"),
    ].forEach((e) => {
        if (e != null) $(e).attr("disabled", isChecked as any);
    });

    if (isChecked) {
        f_clearMiniCorrExternalPostCloser(true);
        f_clearCorrExternalPostCloser(true);
    }
}

function f_assignBrokerProcessor() {
    if (m_BrokerProcessor.checked) return;

    IframePopup.show<IPickResult>(m_BPPickerUrl.value, {}).then(([error, data]) => {
        if (error != null) {
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            return;
        }
        if (data == null) return;

        m_AssignedBrokerProcessorId.value = data.employeeId;
        m_AssignedBrokerProcessorName.value = data.fullName;
        $('#m_AssignedBrokerProcessor').text(data.fullName + "   ");
        updateDirtyBit_callback();
    });
}

function f_clearBrokerProcessor(alertUser:boolean) {
    if (!m_BrokerProcessor.checked) return;

    if (m_AssignedBrokerProcessorId.value != guidEmpty) {
        if (alertUser) showErrorPopup('The previous Processor relationship was cleared.');
        updateDirtyBit_callback();
    }

    m_AssignedBrokerProcessorId.value = guidEmpty;
    m_AssignedBrokerProcessorName.value = "<-- None -->";
    $('#m_AssignedBrokerProcessor').text("<-- None -->   ");
}

function f_setBrokerProcessorPickerStatus() {
    const disabled = m_BrokerProcessor.checked;

    // <a> use .attr() instead of .prop()
    $("#m_pickerPickLink").attr("disabled", disabled as any);
    $("#m_pickerNoneLink").attr("disabled", disabled as any);

    if (disabled) f_clearBrokerProcessor(true);
}

    Object.assign(window, {
        onCancelClick,
        onClientOkClick,
        onClientApplyClick,
        revokeClientCertificate,
        deleteIp,
        validatePage,
        selfClose,

        showErrorPopup,
        onTabClick,
        onUnlockAccountClick,
        updateDirtyBit_callback,
        dirtyParent,
        showPasswordRules,
        expirationPolicyClick,
        expirationCycleClick,
        f_assignMiniCorrExternalPostCloser,
        f_clearMiniCorrExternalPostCloser,
        f_assignCorrExternalPostCloser,
        f_clearCorrExternalPostCloser,
        f_setExternalPostCloserPickerStatus,
        f_assignBrokerProcessor,
        f_clearBrokerProcessor,
        f_setBrokerProcessorPickerStatus,
    });
});
