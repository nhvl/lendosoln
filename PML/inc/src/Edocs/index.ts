import { module, ITimeoutService } from "angular";

declare const docsReceivedVm: IViewModel;
interface IDocument {
    __type              : "Doc";
    DocumentId          : StrGuid;
    DocStatus           : string;
    IsObsoleteOrRejected: boolean;
    FolderNm            : string;
    DocTypeName         : string;
    AppName             : string;
    Description         : string;
    LastModifiedDate    : Date;
    PageCount           : number;
    LinkedGenericDoc    : boolean;
}
interface IGenericDoc {
    __type              : "GenericDoc";
    DocumentId          : StrGuid;
    FolderNm            : string;
    DocTypeName         : string;
    AppName             : string;
    Description         : string;
    LastModifiedDate    : Date;

    DocStatus           : string;
    IsObsoleteOrRejected: boolean;
    PageCount           : string;
}
interface IViewModel {
    docs                      : IDocument[];
    genericDocs               : IGenericDoc[];
    isBarcodeScannerEnabled   : boolean;
    HideStatusColumnInTpoEdocs: boolean;
}

let $timeout: ITimeoutService;

class DocsReceivedCtrl {
    private docs                      : IDocument[];
    private isBarcodeScannerEnabled   : boolean;
    private HideStatusColumnInTpoEdocs: boolean;

    private sortKey                   : string = "LastModifiedDate";
    private sortDesc                  : boolean = true;

    static $inject = ["$timeout"];
    constructor(_$timeout: ITimeoutService) {
        $timeout = _$timeout;

        docsReceivedVm.docs.forEach(d => {
            d.__type = "Doc";
            d.LastModifiedDate = new Date(d.LastModifiedDate);
        });
        docsReceivedVm.genericDocs.forEach(d => {
            d.__type = "GenericDoc";
            d.LastModifiedDate = new Date(d.LastModifiedDate);

            d.DocStatus = d.DocStatus || "";
            d.PageCount = d.PageCount || "N/A";
            d.IsObsoleteOrRejected = d.IsObsoleteOrRejected || false;
        });
        docsReceivedVm.docs = docsReceivedVm.docs.concat(docsReceivedVm.genericDocs as any[])
        delete docsReceivedVm.genericDocs;

        Object.assign(this, docsReceivedVm);
    }
}

const edocsModule = module('Edocs', []);

import { sortHeaderDirective } from "../ng-common/sortHeader";
edocsModule.directive(sortHeaderDirective.ngName, sortHeaderDirective);

import DocsReceivedHtml from "./docsReceived.html";
edocsModule.component('docsReceived', {
    template: DocsReceivedHtml,
    controller: DocsReceivedCtrl,
});
