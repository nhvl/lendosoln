import * as angular from "angular";

import {
    IframePopup,
    LqbPopup,
    IframeSelf,
    simpleDialog, SimpleDialog,
}  from "../components/IframePopup";
import { TPOStyleUnification } from "../components/TPOStyleUnification";

declare global {
    interface Window {
        IframePopup : typeof IframePopup,
        IframeSelf  : typeof IframeSelf,
        LqbPopup    : typeof LqbPopup,
        simpleDialog: SimpleDialog,
    }
}
Object.assign(window, {
    IframePopup,
    IframeSelf,
    LqbPopup,
    simpleDialog,
});



import {reRefDirective        } from "../ng-common/reRef";
import {ngMdcCheckboxDirective} from "../ng-common/ngMdcCheckbox";
import {ngMdcRadioDirective   } from "../ng-common/ngMdcRadio";
import {tpoSelectDirective    } from "../ng-common/tpoSelect";
import {ngFileInput           } from "../ng-common/ngFileInput";
import {sortHeaderDirective   } from "../ng-common/sortHeader";

angular.module("TpoCommon", [])
.directive(reRefDirective        .ngName, reRefDirective        )
.directive(ngMdcCheckboxDirective.ngName, ngMdcCheckboxDirective)
.directive(ngMdcRadioDirective   .ngName, ngMdcRadioDirective   )
.directive(tpoSelectDirective    .ngName, tpoSelectDirective    )
.directive(ngFileInput           .ngName, ngFileInput           )
.directive(sortHeaderDirective   .ngName, sortHeaderDirective   )
;



import "../LoanNavigationHeader";

import "../Pipeline";
import "../StatusAndAgents";
import "../Edocs";
import "../Disclosures";
import "../UserAdmin";
import "../YourProfile";

import "../RolodexList";
import "../RoleSearch";
import "../RoleAndUserPicker";
import "../TPOPortalClosingCosts";
import "./ng-bootstrap";

(function($){
    $(document).on("focus", "input, textarea", function(this: HTMLInputElement){
        if (this.hasAttribute("nohighlight")) return;
        this.select();
    });
})(jQuery);
