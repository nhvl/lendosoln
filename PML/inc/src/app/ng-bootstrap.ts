/*
Usage:

This is like ng-app directive.

<div ng-bootstrap="app"></div>

*/

import { bootstrap } from "angular";

document.addEventListener("DOMContentLoaded", function(event) {
    const elements = Array.from(document.querySelectorAll('[ng-bootstrap]')).sort((x, y) => {
        const vx = Number(x.getAttribute("ng-bootstrap-priority")) || 0;
        const vy = Number(y.getAttribute("ng-bootstrap-priority")) || 0;
        return -(vx - vy);
    });
    elements.forEach(ele => {
        const ngModuleName = ele.getAttribute("ng-bootstrap");
        if (!ngModuleName) return;
        console.info("ng-bootstrap", ele, ngModuleName);
        bootstrap(ele, [ngModuleName]);
    });
});
