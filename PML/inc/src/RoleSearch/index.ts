import * as angular from "angular";
import {LqbPopup, IframeSelf} from "../components/IframePopup";

declare const RoleSearchViewModel: any;

class RoleSearchController {
    static $inject = ["$element", "$timeout"];

    private LoDesc               : string;
    private m_noLOAssignedWarning: boolean;

    private key                  : string;
    private searchFilter         : string;
    private employeeStatus       : number;
    private searchResults        : {}[];

    private iframeSelf: IframeSelf;

    constructor($element:JQuery, private $timeout: angular.ITimeoutService) {
        $element.on("keydown", "input:text", null, (e: JQueryKeyEventObject) => e.which != 13);

        const popup = new LqbPopup(document.querySelector<HTMLDivElement>("div[lqb-popup]")!);
        const iframeSelf = this.iframeSelf = new IframeSelf();
        iframeSelf.popup = popup;

        this.LoDesc                = "Loan Officer";
        this.m_noLOAssignedWarning = true;
        Object.assign(this, RoleSearchViewModel);

        $element.find('input[autofocus]').focus();

        jQuery("body").on("click", "button.modal-close-btn", () => {
            this.closeDialog();
        });
    }

    private search(event: Event) {
        postJsonSafeD<{}[]>(`${location.pathname}/Search`,{
            key           : this.key,
            searchFilter  : this.searchFilter,
            employeeStatus: this.employeeStatus,
        }).then(([err, d]) => {
            this.$timeout(() => {
                this.searchResults = d;
            });
        });
        event.preventDefault();
        return false;
    }

    private selectRole(role:any){
        this.iframeSelf.resolve(role);
        return false;
    }

    closeDialog(){
        this.iframeSelf.resolve(null);
        return false;
    }
}

import roleSearchHtml from "./index.html";
import { postJsonSafeD } from "../utils/fetch";

function roleSearchDirective(): angular.IDirective {
    return {
        restrict     : "EA",
        controller   : RoleSearchController,
        controllerAs : "vm",
        template     : roleSearchHtml,
    };
}
angular.module("RoleSearch", ["TpoCommon"])
.directive("roleSearch", roleSearchDirective);
