import { SimpleDialog } from "../components/IframePopup";
declare const simpleDialog: SimpleDialog;

interface IArgs {
    _ClientID:string,
    sFileVersion:string, loanid:string, applicationID:string,
    [key:string]:string,
}

export function getCommonLoadArgs():IArgs {
    const _sFileVersion = document.getElementById("sFileVersion") as HTMLInputElement;
    const sFileVersion = _sFileVersion == null ? null : _sFileVersion.value;

    const sLID = document.getElementById("sLID") as HTMLInputElement;
    const loanid = sLID == null ? null : sLID.value;

    const _applicationID = document.getElementById("applicationID") as HTMLInputElement;
    const applicationID = _applicationID == null ? null : _applicationID.value;

    return ({ sFileVersion, loanid, applicationID }) as any;
}

export function getCommonArgs(clientID:string):IArgs {
    return {
        ...getAllFormValues(clientID),
        _ClientID: clientID,
        ...getCommonLoadArgs(),
    };
}

export function formatMoney(n:number) {
    return n.toLocaleString("en-US", { style:"currency", currency:"USD" });
}


export function showErrorPopup(message:string, reload:boolean) {
    simpleDialog.alert(message, "Error", "OK").then(() => {
        if (reload) window.location.reload();
    });
}


export function getParameterByName(name:string) {
    //http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    const regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

export function resizeDialogs() {
    var name = "";
    if ($("#EmploymentDialog").css("display") != "none") {
        name = "Employment";
    }
    else if ($("#AssetDialog").css("display") != "none") {
        name = "Asset";
    }

    if (name != "") {
        $("#" + name + "Dialog").css("height", ($(window).height()! - 50) + "px");
        $("#" + name + "Dialog").css("maxHeight", ($(window).height()! - 50) + "px");
        $("[aria-describedby=" + name + "Dialog]").css("maxWidth", "920px");
        $("[aria-describedby=" + name + "Dialog]").css("width", ($(window).width()! - 40) + "px");
        $("[aria-describedby=" + name + "Dialog]").css("left", (($(window).width()! - $("[aria-describedby=" + name + "Dialog]").width()!) / 2) + "px");
    }
}

