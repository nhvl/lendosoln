﻿/// <reference types="jquery" />

import { IframePopup as _IframePopup, SimpleDialog } from "../components/IframePopup";
import { onPropertyChange } from "../utils/onPropertyChange";
declare const IframePopup: typeof _IframePopup;
declare const simpleDialog: SimpleDialog;

declare function _postGetAllFormValues(args:{[key:string]: string}): void;
declare function p1_init(): void;
declare function p2_init(): void;
declare function p3_init(): void;
declare function _onError(m:string): void;
declare function _postRefreshCalculation(value:{}, clientId:string|null): void;
declare function f_displayFieldWriteDenied(m:string): void;

declare function addEmployment(x:boolean): void;
declare function onSave(x:string): void;

declare const gService: any;
declare const Asset: any;

declare var followLink: boolean; (window as any).followLink = true;

import {getCommonLoadArgs, getCommonArgs, showErrorPopup, getParameterByName, resizeDialogs} from "./utils";

const $ = jQuery;

let keyChange = false;

declare let vbConfirmNo: 7;
declare let vbConfirmCancel: 2;
declare let vbConfirmYes: 6;
declare let ConfirmSave: () => number;
declare let ConfirmSaveNoCancel: () => number;
if (typeof (vbConfirmNo) != 'number') {
    //vbs is not working fall back to confirm
    (window as any).vbConfirmNo     = 7;
    (window as any).vbConfirmCancel = 2; //there is not a no option in confirm
    (window as any).vbConfirmYes    = 6;

    (window as any).ConfirmSave =
    (window as any).ConfirmSaveNoCancel =
        () => confirm("Do you want to save the changes?") ? vbConfirmYes : vbConfirmNo;
}

$(window).resize(resizeDialogs);

function correctCalendarPos(el:HTMLElement) {
    if ($("#EmploymentDialog").css("display") == "none") return;
    const $div = $("#EmploymentDialog");

    var $calendar = $(".calendar");
    $div.append($calendar);
    var $target = $(el);
    var pos = $target.position();
    pos.top = pos.top + $target.height()!;

    $calendar.css({
        top : (pos.top  + $div.scrollTop ()!),
        left: (pos.left + $div.scrollLeft()!),
    });
}

jQuery(init);
function init() {
    if (typeof (_initInput) == 'function') _initInput();

    eventHandler();

    var p = getParameterByName("p");
    //Validate Query String
    if (p !== "0" && p !== "1" && p !== "2" && p !== "3")
        p = "0";
    tabsHandler(p);

    const IsUsing2015GFEUITPO = $("#IsUsing2015GFEUITPO").val() == "True";

    $("[id*='editClosingCosts']").on("click", function() {
        attemptSave(false);
        const _sLID = document.getElementById("sLID") as HTMLInputElement;
        if (_sLID != null) {
            const sLID = _sLID.value;

            location.assign(IsUsing2015GFEUITPO
                ? `${gVirtualRoot}/webapp/TPOPortalClosingCosts.aspx?loanid=${sLID}`
                : `${gVirtualRoot}/webapp/TPOFeeEditor.aspx?loanid=${sLID}`);
        }
    });

    if (typeof ML.EnableNewLoanNavigation !== "undefined" && !ML.EnableNewLoanNavigation) {
        $(window).on('beforeunload', function () {
            attemptSave(false);
        });
    }

    //set client id
    (document.getElementById("_ClientID") as HTMLInputElement).value = "Loan1003pg" + (Number(p) + 1);

    var coll = document.getElementsByTagName("INPUT");
    var length = coll.length;
    for (let i = 0; i < length; i++) {
        var o = coll[i];
        onPropertyChange(o, (propertyName, target) => {
            if (propertyName == "readOnly") {
                const input = target as HTMLInputElement;
                if (isReadOnly() && $(input).attr("AlwaysEnable") == null) input.readOnly = true;

                if (input.readOnly) formatReadonlyField(input);
                else {
                    input.style.backgroundColor = "";
                    // @ts-ignore
                    input.tabIndex = target.oldTabIndex != null ? target.oldTabIndex : 0;
                }
            }
        });
    }
    initializeApplicationList();
}

function promptSave() {
    if (!isDirty()) return;
    simpleDialog.confirm("Do you want to save the changes?").then((answer) => {
        if (!answer) return;
        savePage(false);
    });
}

function attemptSave(hideDialog:boolean) {
    followLink = true;
    var result = null;
    if (isDirty()) {
        result = savePage(hideDialog);
    }
    return hideDialog ? result : followLink;
}

function savePage(hideDialog:boolean) {
    const _ClientID = document.getElementById("_ClientID") as HTMLInputElement;
    const clientID = _ClientID == null ? null : _ClientID.value;

    const args = getAllFormValues(clientID);

    if (typeof (_postGetAllFormValues) == "function") {
        _postGetAllFormValues(args);
    }

    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (typeof (_postGetAllFormValuesCallbacks[i]) == "function") {
            _postGetAllFormValuesCallbacks[i](args);
        }
    }

    const saveArgs = { ...args, _ClientID: clientID, ...getCommonLoadArgs() };
    const result = gService.Loan1003Service.call("SavePage", saveArgs);

    if (!result.error) {
        if (result.value.ErrorMessage != undefined) {
            if (hideDialog) {
                return result.value.ErrorMessage;
            } else {
                showErrorPopup(result.value.ErrorMessage, true);
                followLink = false;
            }
        }
    }

    clearDirty();
    return null;
}

function on1003TabClick(tabIndex:number|string) {
    promptExplicitSave(ML.isSaveButtonEnabled, isDirty(),
        (navigationFunction:any) => {
            const resultMessage = attemptSave(true);

            if (ML.isSaveButtonEnabled) {
                promptExplicitSaveCallback(resultMessage, navigationFunction);
            } else {
                normalSaveCallback(resultMessage);
            }
        },
        () => followLink && (location.href = ML.main1003Url + "&p=" + tabIndex));
}

function loadEmploymentPage() {
    const clientID = "Employment";
    const args = getCommonArgs(clientID);

    const aBPrimaryEmplrNm                            = document.getElementById("aBPrimaryEmplrNm"                           ) as HTMLInputElement;
    const aBPrimaryEmplrAddr                          = document.getElementById("aBPrimaryEmplrAddr"                         ) as HTMLInputElement;
    const Loan1003pg1_Employment_aBPrimaryEmplrCity   = document.getElementById("Loan1003pg1_Employment_aBPrimaryEmplrCity"  ) as HTMLInputElement;
    const aBPrimaryEmplrBusPhone                      = document.getElementById("aBPrimaryEmplrBusPhone"                     ) as HTMLInputElement;
    const Loan1003pg1_Employment_aBPrimaryEmplrZip    = document.getElementById("Loan1003pg1_Employment_aBPrimaryEmplrZip"   ) as HTMLInputElement;
    const Loan1003pg1_Employment_aBPrimaryEmplrState  = document.getElementById("Loan1003pg1_Employment_aBPrimaryEmplrState" ) as HTMLInputElement;
    const Loan1003pg1_Employment_aBPrimaryEmpltStartD = document.getElementById("Loan1003pg1_Employment_aBPrimaryEmpltStartD") as HTMLInputElement;
    const Loan1003pg1_Employment_aBPrimaryProfStartD  = document.getElementById("Loan1003pg1_Employment_aBPrimaryProfStartD" ) as HTMLInputElement;
    const aBPrimaryJobTitle                           = document.getElementById("aBPrimaryJobTitle"                          ) as HTMLInputElement;
    const aCPrimaryEmplrNm                            = document.getElementById("aCPrimaryEmplrNm"                           ) as HTMLInputElement;
    const aCPrimaryEmplrAddr                          = document.getElementById("aCPrimaryEmplrAddr"                         ) as HTMLInputElement;
    const Loan1003pg1_Employment_aCPrimaryEmplrCity   = document.getElementById("Loan1003pg1_Employment_aCPrimaryEmplrCity"  ) as HTMLInputElement;
    const aCPrimaryEmplrBusPhone                      = document.getElementById("aCPrimaryEmplrBusPhone"                     ) as HTMLInputElement;
    const Loan1003pg1_Employment_aCPrimaryEmplrZip    = document.getElementById("Loan1003pg1_Employment_aCPrimaryEmplrZip"   ) as HTMLInputElement;
    const Loan1003pg1_Employment_aCPrimaryEmplrState  = document.getElementById("Loan1003pg1_Employment_aCPrimaryEmplrState" ) as HTMLInputElement;
    const Loan1003pg1_Employment_aCPrimaryEmpltStartD = document.getElementById("Loan1003pg1_Employment_aCPrimaryEmpltStartD") as HTMLInputElement;
    const Loan1003pg1_Employment_aCPrimaryProfStartD  = document.getElementById("Loan1003pg1_Employment_aCPrimaryProfStartD" ) as HTMLInputElement;
    const aCPrimaryJobTitle                           = document.getElementById("aCPrimaryJobTitle"                          ) as HTMLInputElement;

    const result = gService.Loan1003PopupService.call("LoadEmploymentRecords", args);
    if (!result.error && result.value.aBPrimaryEmplrNm != undefined) {
        $("[name=aBPrimaryIsSelfEmplmt][value="+ (result.value.aBPrimaryIsSelfEmplmt == "True") +"]").prop('checked', true);
        aBPrimaryEmplrNm                           .value = result.value.aBPrimaryEmplrNm;
        aBPrimaryEmplrAddr                         .value = result.value.aBPrimaryEmplrAddr;
        Loan1003pg1_Employment_aBPrimaryEmplrCity  .value = result.value.aBPrimaryEmplrCity;
        aBPrimaryEmplrBusPhone                     .value = result.value.aBPrimaryEmplrBusPhone;
        Loan1003pg1_Employment_aBPrimaryEmplrZip   .value = result.value.aBPrimaryEmplrZip;
        Loan1003pg1_Employment_aBPrimaryEmplrState .value = result.value.Loan1003pg1_Employment_aBPrimaryEmplrState;
        Loan1003pg1_Employment_aBPrimaryEmpltStartD.value = result.value.Loan1003pg1_Employment_aBPrimaryEmpltStartD;
        Loan1003pg1_Employment_aBPrimaryProfStartD .value = result.value.Loan1003pg1_Employment_aBPrimaryProfStartD;
        aBPrimaryJobTitle                          .value = result.value.aBPrimaryJobTitle;

        $("[name=aCPrimaryIsSelfEmplmt][value=" + (result.value.aCPrimaryIsSelfEmplmt == "True") + "]").prop('checked', true);
        aCPrimaryEmplrNm                           .value = result.value.aCPrimaryEmplrNm;
        aCPrimaryEmplrAddr                         .value = result.value.aCPrimaryEmplrAddr;
        Loan1003pg1_Employment_aCPrimaryEmplrCity  .value = result.value.aCPrimaryEmplrCity;
        aCPrimaryEmplrBusPhone                     .value = result.value.aCPrimaryEmplrBusPhone;
        Loan1003pg1_Employment_aCPrimaryEmplrZip   .value = result.value.aCPrimaryEmplrZip;
        Loan1003pg1_Employment_aCPrimaryEmplrState .value = result.value.Loan1003pg1_Employment_aCPrimaryEmplrState;
        Loan1003pg1_Employment_aCPrimaryEmpltStartD.value = result.value.Loan1003pg1_Employment_aCPrimaryEmpltStartD;
        Loan1003pg1_Employment_aCPrimaryProfStartD .value = result.value.Loan1003pg1_Employment_aCPrimaryProfStartD;
        aCPrimaryJobTitle                          .value = result.value.aCPrimaryJobTitle;

        for (var i = 0; i < result.value["numBEmployments"]; i++) {
            var employmentDiv = $("#aB_prev" + (i + 1));
            if (employmentDiv.length == 0) {
                addEmployment(true);
                employmentDiv = $("#aB_prev" + (i + 1));
            }
            var bIsSelfEmplmt = (result.value["bIsSelfEmplmt" + (i + 1)] == "True");
            employmentDiv.find("input[id^=IsSelfEmplmt1]").prop('checked', bIsSelfEmplmt);
            employmentDiv.find("input[id^=IsSelfEmplmt2]").prop('checked', !bIsSelfEmplmt);
            employmentDiv.find("#EmplrNm").val(result.value["bEmplrNm" + (i + 1)]);
            employmentDiv.find("#EmplrAddr").val(result.value["bEmplrAddr" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplrCity").val(result.value["bEmplrCity" + (i + 1)]);
            employmentDiv.find("#EmplrBusPhone").val(result.value["bEmplrBusPhone" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplrZip").val(result.value["bEmplrZip" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplrState").val(result.value["bEmplrState" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplmtStartD" + (i + 1)).val(result.value["Loan1003pg1_Employment_bEmplmtStartD" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplmtEndD" + (i + 1)).val(result.value["Loan1003pg1_Employment_bEmplmtEndD" + (i + 1)]);
            employmentDiv.find("#ProfStartD").val(result.value["Loan1003pg1_Employment_bEmplmtEndD" + (i + 1)]);
            employmentDiv.find("#JobTitle").val(result.value["bJobTitle" + (i + 1)]);
            employmentDiv.find("#RecordID").val(result.value["bRecordID" + (i + 1)]);
        }

        for (var i = 0; i < result.value["numCEmployments"]; i++) {

            var employmentDiv = $("#aC_prev" + (i + 1));
            if (employmentDiv.length == 0) {
                addEmployment(false);
                employmentDiv = $("#aC_prev" + (i + 1));
            }
            var cIsSelfEmplmt = (result.value["cIsSelfEmplmt" + (i + 1)] == "True");
            employmentDiv.find("input[id^=IsSelfEmplmt1]").prop('checked', cIsSelfEmplmt);
            employmentDiv.find("input[id^=IsSelfEmplmt2]").prop('checked', !cIsSelfEmplmt);
            employmentDiv.find("#EmplrNm").val(result.value["cEmplrNm" + (i + 1)]);
            employmentDiv.find("#EmplrAddr").val(result.value["cEmplrAddr" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplrCity").val(result.value["cEmplrCity" + (i + 1)]);
            employmentDiv.find("#EmplrBusPhone").val(result.value["cEmplrBusPhone" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplrZip").val(result.value["cEmplrZip" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplrState").val(result.value["cEmplrState" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplmtStartD" + (i + 1)).val(result.value["Loan1003pg1_Employment_cEmplmtStartD_rep" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_EmplmtEndD" + (i + 1)).val(result.value["Loan1003pg1_Employment_cEmplmtEndD_rep" + (i + 1)]);
            employmentDiv.find("#Loan1003pg1_Employment_ProfStartD").val(result.value["Loan1003pg1_Employment_cEmplmtEndD" + (i + 1)]);
            employmentDiv.find("#JobTitle").val(result.value["cJobTitle" + (i + 1)]);
            employmentDiv.find("#RecordID").val(result.value["cRecordID" + (i + 1)]);
        }
    }
}

function loadNonRealEstateAssets() {
    const clientID = "Employment";

    const args = getCommonArgs(clientID);

	var oldIsDirty = isDirty();

    const result = gService.Loan1003PopupService.call("LoadNonRealEstateAssets", args);
    if (!result.error) {
        setReoVisibility(false);
        loadAssetsFromResult(result);
        $('#btnSave').off('click').on("click", function () { onSave('AssetDialog'); });
        loadAssets();
	}

	if (oldIsDirty) {
		updateDirtyBit();
	}
	else {
		clearDirty();
	}
}

function loadRealEstateRecords() {
	var oldIsDirty = isDirty();

    const result = gService.Loan1003PopupService.call("LoadRealEstateRecords", getCommonArgs("Employment"));
    if (!result.error) {
        setReoVisibility(true);
        loadAssetsFromResult(result);
        $('#btnSave').off('click').on("click", function () { onSave('ReoDialog'); });
        loadAssets();
	}

	if (oldIsDirty) {
		updateDirtyBit();
	}
	else {
		clearDirty();
	}
}


function loadAssetsFromResult(result:any) {
    // clear content of arrays
    RealEstateAssets.splice(0);
    NonRealEstateAssets.splice(0);
    //load the records into the fields, then click the add asset button or call the method
    for (var i = 0; i < result.value["numRecords"]; i++) {
        var asset = new Asset();
        asset.AssetT             = result.value["AssetT" + i];
        asset.IsSubjectProp      = result.value["IsSubjectProp" + i] != "False";
        asset.IsPrimaryResidence = result.value["IsPrimaryResidence" + i] != "False";
        asset.recordID           = result.value["recordID" + i];
        asset.Addr               = result.value["Addr" + i];
        asset.Zip                = result.value["Zip" + i];
        asset.City               = result.value["City" + i];
        asset.State              = result.value["AssetST" + i];
        asset.Type               = result.value["Type" + i];
        asset.StatT              = result.value["StatT" + i];
        asset.HExp               = result.value["HExp" + i];
        asset.Val                = result.value["Val" + i];
        asset.OwnerT             = result.value["OwnerT" + i];
        asset.GrossRentI         = result.value["GrossRentI" + i];
		asset.OccR				 = result.value["OccR" + i];
        //the rest of the non-real estate info
        asset.Desc               = result.value["Desc" + i];
        asset.ComNm              = result.value["ComNm" + i];
        asset.AccNum             = result.value["AccNum" + i];
        if (result.value["AssetT" + i] == -1)
            RealEstateAssets.push(asset);
        else
            NonRealEstateAssets.push(asset);
    }
}

function setReoVisibility(show:boolean) {
    $('.reo').toggle(show);
    $('.non-reo').toggle(!show);

    var assetTypeDropdown = $('#Loan1003pg2_Assets_AssetT');
    if (show) {
        if (assetTypeDropdown.find('option[value="-1"]').length == 0) {
            assetTypeDropdown.prepend('<option value="-1"></option>');
        }

        assetTypeDropdown.val('-1'/*REO*/).change();
        $('.asset-type').hide();
    }
    else {
        assetTypeDropdown
            .find('option[value="-1"]').remove().end()
            .val('0').change();

        $('.asset-type').show();
    }
}

function tabsHandler(p:string) {
    // Hide all content.
    $("div[id^='tabs-']").hide();
    var tabName;
    if (p !== "") {
        switch (p) {
            case "0":
                tabName = "#tabs-1";
                break;
            case "1":
                tabName = "#tabs-2";
                break;
            case "2":
                tabName = "#tabs-3";
                break;
            case "3":
                tabName = "#tabs-4";
                break;
            default:
                tabName = "#tabs-1";
        }
        showTab($($("ul>li a").filter('[href="' + tabName + '"]')[0]));
    } else {
        showFirstTab();
    }
    /*
    // On the click event of a tab.
    $("ul>li a").on("click", function(e) {
    e.preventDefault();
    showTab($(this));
    //window.location.href = "?p=" + (parseInt($(this).attr("href").slice(-1)) - 1).toString();
    });
    */
}

function showTab(tab:JQuery) {
    // Remove any "active" class.
    // Add "active" class to selected tab.
    tab.parent().addClass("active");
    // Hide all tab content

    if (tab.attr("id") == "Loan1003pg1_Employment_employTab1") {
        $("#Loan1003pg1_Employment_employTab2").parent().removeAttr("class");
        $("div[id^='employ-']").hide();
    }
    else if (tab.attr("id") == "Loan1003pg1_Employment_employTab2") {
        $("#Loan1003pg1_Employment_employTab1").parent().removeAttr("class");
        $("div[id^='employ-']").hide();
    }
    else
        $("div[id^='tabs-']").hide();

    // Find the href attribute value to identify the active tab + content
    var activeTab = tab.attr("href");

    // Fade in the active ID content.
    //window.location.href = "?p="+activeTab.slice(-1);
    $(activeTab).fadeIn();

}

function showFirstTab() {
    // Activate first tab.
    $("ul>li a:first").addClass("active").show();

    // Show first tab content.
    $("div[id^='tabs-']:first").show();
}

function _init() {
    const _ClientID = document.getElementById("_ClientID") as HTMLInputElement;
    const clientID = _ClientID == null ? null : _ClientID.value;

    switch (clientID) {
        case "Loan1003pg1":
            p1_init();
            break;
        case "Loan1003pg2":
            p2_init();
            break;
        case "Loan1003pg3":
            p3_init();
            break;
        case "Loan1003pg4":
            TPOStyleUnification.Components();
            break;
        default:
            p1_init();
            break;
    }
}
declare const Loan1003pg1_sRefPurpose_items      : string[];
declare const Loan1003pg1_aManner_items          : string[];
declare const Loan1003pg1_sDwnPmtSrc_items       : string[];
declare const Loan1003pg1_aBSuffix_items         : string[];
declare const Loan1003pg1_aCSuffix_items         : string[];
declare const Loan1003pg2_OtherIncomeCombo_items : string[];
declare const Loan1003pg3_sOCredit1Desc_items    : string[];
declare const Loan1003pg3_sOCredit2Desc_items    : string[];
declare const Loan1003pg3_sOCredit3Desc_items    : string[];
declare const Loan1003pg3_sOCredit4Desc_items    : string[];
function __initCombobox() {
    if (typeof (Loan1003pg1_sRefPurpose_items     ) != 'undefined') createCombobox('Loan1003pg1_sRefPurpose', Loan1003pg1_sRefPurpose_items);
    if (typeof (Loan1003pg1_aManner_items         ) != 'undefined') createCombobox('Loan1003pg1_aManner', Loan1003pg1_aManner_items);
    if (typeof (Loan1003pg1_sDwnPmtSrc_items      ) != 'undefined') createCombobox('Loan1003pg1_sDwnPmtSrc', Loan1003pg1_sDwnPmtSrc_items);
    if (typeof (Loan1003pg1_aBSuffix_items        ) != 'undefined') createCombobox('Loan1003pg1_aBSuffix', Loan1003pg1_aBSuffix_items);
    if (typeof (Loan1003pg1_aCSuffix_items        ) != 'undefined') createCombobox('Loan1003pg1_aCSuffix', Loan1003pg1_aCSuffix_items);
    if (typeof (Loan1003pg2_OtherIncomeCombo_items) != 'undefined') createCombobox('Loan1003pg2_OtherIncomeCombo', Loan1003pg2_OtherIncomeCombo_items);
    if (typeof (Loan1003pg3_sOCredit1Desc_items   ) != 'undefined') createCombobox('Loan1003pg3_sOCredit1Desc', Loan1003pg3_sOCredit1Desc_items);
    if (typeof (Loan1003pg3_sOCredit2Desc_items   ) != 'undefined') createCombobox('Loan1003pg3_sOCredit2Desc', Loan1003pg3_sOCredit2Desc_items);
    if (typeof (Loan1003pg3_sOCredit3Desc_items   ) != 'undefined') createCombobox('Loan1003pg3_sOCredit3Desc', Loan1003pg3_sOCredit3Desc_items);
    if (typeof (Loan1003pg3_sOCredit4Desc_items   ) != 'undefined') createCombobox('Loan1003pg3_sOCredit4Desc', Loan1003pg3_sOCredit4Desc_items);
}


// If this value set to true then value are recalculate on every field's value change.
// There is a delay when tab to next field, therefore for data entry purpose, turn this bit off.

var bIsAutoCalculate = true;
function backgroundCalculation(isAsync:boolean) {
    const _ClientID = document.getElementById("_ClientID") as HTMLInputElement;
    const clientID = _ClientID == null ? null : _ClientID.value;

    // Since recalculate does not always required every field to be pass to server.
    // Add skipMe attribute to input that does not required in calculation.
    const args = getAllFormValues(clientID, true);
    if (typeof (_postGetAllFormValues) == "function") {
        _postGetAllFormValues(args);
    }

    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (typeof (_postGetAllFormValuesCallbacks[i]) == "function") {
            _postGetAllFormValuesCallbacks[i](args);
        }
    }

    const calcArgs = { ...args, _ClientID: clientID, ...getCommonLoadArgs() };

    const method = !clientID ? "CalculateData" : `${clientID}_CalculateData`;

    const result = gService.Loan1003Service.call(method, calcArgs, null, null, null, isAsync);
    if (!result.error) {
        populateForm(result.value, clientID);
        if (typeof (_postRefreshCalculation) == "function") {
            _postRefreshCalculation(result.value, clientID);
        }

        for (var i = 0; i < _postRefreshCalculationCallbacks.length; i++) {
            if (typeof (_postRefreshCalculationCallbacks[i]) == "function") {
                _postRefreshCalculationCallbacks[i](result.value, clientID);
            }
        }
    } else {
        if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
            f_displayFieldWriteDenied(result.UserMessage);
            return false;
        }

        const errMsg = !!result.UserMessage ? result.UserMessage :
            'Unable to process data. Please try again.';

        if (typeof (_onError) == "function")
            _onError(errMsg);
        else
            alert(errMsg);
        return false;
    }

    if (typeof (_init) == "function") _init();
    return false;
}

var shouldUpdateCalculation = false;
var alreadyCalculatedData = false;
function updateCalculation() {
    shouldUpdateCalculation = true;
    alreadyCalculatedData = false;
}

function refreshCalculation(isAsync:boolean = false) {
    if (bIsAutoCalculate == true)
        backgroundCalculation(isAsync);
    else {
        if (typeof (_init) == "function") _init();
    }
    shouldUpdateCalculation = false;
}

function eventHandler() {
    $("body").mouseup(function() {
        if (shouldUpdateCalculation) {
            refreshCalculation();
        }
    });

    $("[Update='true']").change(updateCalculation);

    $("[Update='true']").keydown(function(e) {
        if (e.key != "Tab" && e.keyCode != 9) {
            keyChange = true;
        }
        if (e.key == "Tab" || e.keyCode == 9) {
            if (keyChange) {
                refreshCalculation();
                alreadyCalculatedData = true;
            }
            keyChange = false;
        }
    });

    $("input[type='checkbox'][Update='true']").off();
    $("input[type='checkbox'][Update='true']").change(function() { refreshCalculation(); });

    // On the click event of a tab.
    $("#employ>li a").on("click", function(e) {
        e.preventDefault();
        showTab($(this));
    });

    $("#btnAsset").on("click", function(event) {
        event.preventDefault();
        ($("#btnAsset").text() == "Add Asset") ? addAsset() : updateAsset();
		updateDirtyBit();
        $("#btnAsset").blur();
    });

    $("#aBForm").submit(function(event) {
        event.preventDefault();
        onSave('EmploymentDialog');
    });

    $("#aCForm").submit(function(event) {
        event.preventDefault();
        onSave('EmploymentDialog');
    });
}

function importFannie() {
    const sLID = document.getElementById("sLID") as HTMLInputElement;

    IframePopup.show(`${ML.VirtualRoot}/main/ImportIntoExistingFile.aspx?loanid=${sLID.value}`, { backdrop:'static' })
    .then(([error, result]) => {
        if (error != null) {
            console.error(error);
            simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
        } else {
            if (result == null) return;
            if (!!result) location.reload(true);
        }
    });
}

function addApp() {
    const sLID = document.getElementById("sLID") as HTMLInputElement;

    const args = {
        loanid   : sLID.value,
        _ClientID: "Loan1003Header"
    }

    var result = gService.Loan1003Service.call("AddApp", args);

    if (result.error || result.value.ErrorMessage) {
        displayErrorPopup('Could not add new application: ' + result.value.ErrorMessage);
        return;
    }

    var applicationRegex = /(applicationid=)[^&]*/i;
    var pageRegex = /p=\d/i;

    if (!applicationRegex.test(location.href)) {
        location.href = location.href.replace(pageRegex, "p=0") + "&applicationid=" + result.value["NewAppId"];
    }
    else {
        location.href = location.href.replace(applicationRegex, "$1" + result.value["NewAppId"]).replace(pageRegex, "p=0");
    }
}

function deleteApp() {
    simpleDialog.confirm('Do you want to delete all data of borrower and spouse?').then((confirm) => {
        if (!confirm) return;
        performApplicationDeletion();
    });
    return false;
}

function performApplicationDeletion() {
    const sLID          = document.getElementById("sLID") as HTMLInputElement;
    const applicationID = document.getElementById("applicationID") as HTMLInputElement;

    const args = {
        loanid       : sLID.value,
        applicationID: applicationID.value,
        _ClientID    : "Loan1003Header"
    }

    const result = gService.Loan1003Service.call("DeleteApp", args);

    if (result.error || result.value.ErrorMessage) {
        displayErrorPopup('Could not delete application: ' + result.value.ErrorMessage);
        return;
    }

    const applicationRegex = /&applicationid=[^&]*/i;
    const pageRegex = /p=\d/i;

    if (!applicationRegex.test(location.href)) {
        location.href = location.href.replace(pageRegex, "p=0");
    }
    else {
        location.href = location.href.replace(applicationRegex, "").replace(pageRegex, "p=0");
    }
}

function displayErrorPopup(message:string) {
    simpleDialog.alert(message);
}

function reload() {
    if (typeof (_init) == 'function') _init();
    //update reformat fields
}


function changeStatus(item:JQuery) {
    item.on('focusout', function(this:HTMLElement) { // When losing focus
        $(this).css('background', '#FFF');
    });
    item.on('focus', function(this:HTMLElement) { // When focus
        $(this).css('background', 'yellow');
    });
}

function onNextClick(){
    $('ul.tabmenu li.active').next().find('a').click();
}
function onBackClick(){
    $('ul.tabmenu li.active').prev().find('a').click();
}
$(document).on("click", function(e) {
    if(!$(e.target).closest("application-list").length)
    {
        if($("application-list application-content").is(':visible')){
            $("application-list application-header").click();
        }
    }
});
function initializeApplicationList() {
    $("application-list").each((i, accordion) => {
        let collapse = true;

        const $accordion = $(accordion);
        const header = $accordion.find("application-header");
        const content = $accordion.find("application-content");

        const render = () => {
            $accordion.toggleClass("application-collapse", !collapse);
            console.log("collapse", collapse);
            content.collapse(!collapse ? "show" : "hide");

            //iOPM: 460779 Set position of App Content
            var ulContent = content.find("ul");
            ulContent.outerHeight("auto");
            var dropdownHeight = ulContent[0].getBoundingClientRect().height;
            var hasScroll = ulContent[0].scrollHeight > dropdownHeight;
            var width = header.width()!;
            if(hasScroll){
                //add scroll width
                width += 17;
                ulContent.scrollTop(0);
            }
            content.css({width: width, height: ""});
            content.find('li a').css('width', header.find("label").width()!);

            //update height after set width for dropdown.
            dropdownHeight = ulContent[0].getBoundingClientRect().height;

            var basePosition = -12; //padding of ul
            var loanInfoBarTop = 0;
            if($('.loan-info-bar').offset() != null){
                loanInfoBarTop = $('.loan-info-bar').offset()!.top;
            }
            var topPosition = header.position().top - (header.offset()!.top - $('.loan-info-bar').outerHeight()! - loanInfoBarTop);
            var activeLi = content.find('li.active');

            var top = basePosition;
            for(var i = 0; i < activeLi.index(); i++){
                top = top - content.find('li:eq('+i+')')[0].getBoundingClientRect().height;

                if(top < topPosition){
                    if(!hasScroll) {
                        //touch the top. Add scrollbar.
                        hasScroll = true;
                        width += 17;
                        content.css({width: width});
                        dropdownHeight = ulContent[0].getBoundingClientRect().height;
                    }
                }
            }

            if(hasScroll && top < topPosition){
                var reduceHeight = topPosition - top;
                dropdownHeight = dropdownHeight - reduceHeight;
                //base now is 0.
                var minDropDownHeight = 0 - topPosition + header[0].getBoundingClientRect().height;
                if(minDropDownHeight > dropdownHeight){
                    dropdownHeight = minDropDownHeight;
                }
                ulContent.outerHeight(dropdownHeight);
                ulContent.scrollTop(reduceHeight);
                top = topPosition;
            }

            content.css({top: top});
        };
        let tabMDC = new window.mdc.tabs.MDCTabBar(document.querySelector('application-list'));
        tabMDC.layout();
        header.on("click.ApplicationList", () => {
            collapse = !collapse;
            render();
        });
        render();
    });
}
Object.assign(window, {
    onNextClick, onBackClick,
    refreshCalculation,
    attemptSave,
    correctCalendarPos,
    on1003TabClick,
    importFannie,
    addApp, deleteApp,
    // page 1
    loadEmploymentPage,
    // page 2
    loadRealEstateRecords,
    loadNonRealEstateAssets,
    //
    reload,
    __initCombobox,
    _init,
    promptSave,
});

import {addAsset, loadAssets, updateAsset, RealEstateAssets, NonRealEstateAssets} from "./Loan1003.popup";
