declare function _initInput(): void;
declare function mask(): void;
declare const Employment: any;
declare const Asset: any;
declare const gService: any;
declare const AssetType: {value:number, text:string}[]; // file:///C:/LendOSoln/PML/inc/webapp/Loan1003.constants.js

import { IframePopup as _IframePopup, SimpleDialog } from "../components/IframePopup";
declare const IframePopup: typeof _IframePopup;
declare const simpleDialog: SimpleDialog;

declare var followLink: boolean;

import { formatMoney, showErrorPopup, getParameterByName, getCommonLoadArgs, getCommonArgs, resizeDialogs } from "./utils";

var oldIsDirty = false;
var BorrowerEmployPrevious = [];
var CoBorrowerEmployPrevious = [];
export var NonRealEstateAssets: any[]= [];
export var RealEstateAssets: any[] = [];

//manage to show div dialogs
export function DialogHandler(d:string) {
    if (!d) return;
    // Hide all content.
    $("div[id^='employ-']").hide();
    switch (d) {
        case "a0":
            //show assets dialg
            showDialog("AssetDialog");
            break;
        case "e0":
            showEmploymentDialog($($("#employ>li a").filter('[href="#employ-1"]')[0]));
            break;
        case "e1":
            showEmploymentDialog($($("#employ>li a").filter('[href="#employ-2"]')[0]));
            break;
        default:
            break;
    }
    resizeDialogs();
}

function showEmploymentDialog(tab:JQuery) {
    showDialog("EmploymentDialog");
    $("#employ>li").removeClass("active");
    // Add "active" class to selected tab.
    tab.parent().addClass("active");
    // Hide all tab content
    $("div[id^='employ-']").hide();

    // Find the href attribute value to identify the active tab + content
    var activeTab = tab.attr("href");

    // Fade in the active ID content.
    //window.location.href = "?p="+activeTab.slice(-1);
    $(activeTab).fadeIn();
}

function showDialog(name:string) {
    oldIsDirty = isDirty();
    clearDirty();

    // jQuery datepickers use absolute positioning from the top
    // of the screen, so move the popup to the top to avoid any
    // position issues. The modal will do this anyways when
    // closed.
    var oldOverflow = $('body').css('overflow');
    if(name == 'EmploymentDialog')
        window.scrollTo(0, 0);

    $("#" + name)
        .modal({ backdrop:'static', keyboard: false, })
        .on('show.bs.modal', function () { $('body').css('overflow', 'hidden'); })
        .on('hide.bs.modal', function () { $('body').css('overflow', oldOverflow); })
}

function promptDialogSave() {
    if (!isDirty()) return;
    simpleDialog.confirm("Do you want to save the changes?").then((answer) => {
        if (!answer) return;
        if ($("#EmploymentDialog").css("display") != "none")
            onSave("EmploymentDialog");
        else {
            onSave("AssetDialog");
        }
	});
}

export function onCancel(name:string) {
	const closeHandler = () => {
		editingAsset = "";
		cleanUp();
		$("#" + name).modal("hide");

		if (oldIsDirty) {
			updateDirtyBit();
		}
	};

    if (!isDirty()) {
		closeHandler();
		return;
	}
	
	simpleDialog.confirm("You have unsaved work. Changes to records that have been added or updated will be lost. Continue?", "Continue without saving?", "Yes", "No").then((response) => {
		if (response) {
			clearDirty();
			closeHandler();
		}
	});
}

export function onSave(name:string) {
    //save dialog click event
    ///
    const clientID = "Employment";
    const args = getCommonArgs(clientID);

    if (name == "EmploymentDialog") {
        var bPrimary = new Employment();
        bPrimary.IsSelfEmplmt  = $("[name=aBPrimaryIsSelfEmplmt][value=true]").prop("checked");
        bPrimary.EmplrNm       = $("#aBPrimaryEmplrNm").val();
        bPrimary.EmplrBusPhone = $("#aBPrimaryEmplrBusPhone").val();
        bPrimary.EmplrAddr     = $("#aBPrimaryEmplrAddr").val();
        bPrimary.EmplrZip      = $("#Loan1003pg1_Employment_aBPrimaryEmplrZip").val();
        bPrimary.EmplrCity     = $("#Loan1003pg1_Employment_aBPrimaryEmplrCity").val();
        bPrimary.EmplrState    = $("#Loan1003pg1_Employment_aBPrimaryEmplrState").val();
        bPrimary.JobTitle      = $("#aBPrimaryJobTitle").val();
        bPrimary.EmplmtStartD  = $("#Loan1003pg1_Employment_aBPrimaryEmpltStartD").val();
        bPrimary.RecordID      = $("#aBPrimaryRecordID").val();
        bPrimary.ProfStartD    = $("#Loan1003pg1_Employment_aBPrimaryProfStartD").val();

        args["bPrimary"] = JSON.stringify(bPrimary);

        var cPrimary = new Employment();
        cPrimary.IsSelfEmplmt  = $("[name=aCPrimaryIsSelfEmplmt][value=true]").prop("checked");
        cPrimary.EmplrNm       = $("#aCPrimaryEmplrNm").val();
        cPrimary.EmplrBusPhone = $("#aCPrimaryEmplrBusPhone").val();
        cPrimary.EmplrAddr     = $("#aCPrimaryEmplrAddr").val();
        cPrimary.EmplrZip      = $("#Loan1003pg1_Employment_aCPrimaryEmplrZip").val();
        cPrimary.EmplrCity     = $("#Loan1003pg1_Employment_aCPrimaryEmplrCity").val();
        cPrimary.EmplrState    = $("#Loan1003pg1_Employment_aCPrimaryEmplrState").val();
        cPrimary.JobTitle      = $("#aCPrimaryJobTitle").val();
        cPrimary.EmplmtStartD  = $("#Loan1003pg1_Employment_aCPrimaryEmpltStartD").val();
        cPrimary.RecordID      = $("#aCPrimaryRecordID").val();
        cPrimary.ProfStartD    = $("#Loan1003pg1_Employment_aCPrimaryProfStartD").val();

        args["cPrimary"] = JSON.stringify(cPrimary);

        //turn the data into a list
        var bprevEmployments = [];
        var i = 1;
        //get the borrower employments
        while (true) {
            var div = $("#aB_prev" + i);
            if (div.length == 0)
                break;
            else {
                var employment = new Employment();
                employment.IsSelfEmplmt = div.find("input[id^=IsSelfEmplmt1]").prop("checked");
                employment.EmplrNm = div.find("#EmplrNm").val();
                employment.EmplrBusPhone = div.find("#EmplrBusPhone").val();
                employment.EmplrAddr = div.find("#EmplrAddr").val();
                employment.EmplrZip = div.find("#Loan1003pg1_Employment_EmplrZip").val();
                employment.EmplrCity = div.find("#Loan1003pg1_Employment_EmplrCity").val();
                employment.EmplrState = div.find("#Loan1003pg1_Employment_EmplrState").val();
                employment.JobTitle = div.find("#JobTitle").val();
                employment.EmplmtStartD = div.find("#Loan1003pg1_Employment_EmplmtStartD" + i).val();
                employment.EmplmtEndD = div.find("#Loan1003pg1_Employment_EmplmtEndD" + i).val();
                employment.RecordID = div.find("#RecordID").val();
                bprevEmployments.push(employment);
                i++;
            }

        }

        args["bPrevEmployments"] = JSON.stringify(bprevEmployments);

        var cprevEmployments = [];
        i = 1;
        //get the coborrower employments
        while (true) {
            var div = $("#aC_prev" + i);
            if (div.length == 0)
                break;
            else {
                var employment = new Employment();
                employment.IsSelfEmplmt = div.find("input[id^=IsSelfEmplmt1]").prop("checked");
                employment.EmplrNm = div.find("#EmplrNm").val();
                employment.EmplrBusPhone = div.find("#EmplrBusPhone").val();
                employment.EmplrAddr = div.find("#EmplrAddr").val();
                employment.EmplrZip = div.find("#Loan1003pg1_Employment_EmplrZip").val();
                employment.EmplrCity = div.find("#Loan1003pg1_Employment_EmplrCity").val();
                employment.EmplrState = div.find("#Loan1003pg1_Employment_EmplrState").val();
                employment.JobTitle = div.find("#JobTitle").val();
                employment.EmplmtStartD = div.find("#Loan1003pg1_Employment_EmplmtStartD" + i).val();
                employment.EmplmtEndD = div.find("#Loan1003pg1_Employment_EmplmtEndD" + i).val();
                employment.RecordID = div.find("#RecordID").val();
                cprevEmployments.push(employment);
                i++;
            }

        }
        args["cPrevEmployments"] = JSON.stringify(cprevEmployments);
        var result = gService.Loan1003PopupService.call("SaveEmploymentRecords", args);
        if (!result.error) {
            if (result.value["ErrorMessage"] != undefined) {
                showErrorPopup(result.value["ErrorMessage"], false);
                followLink = false;
            }
        }
    }
    else if (name == 'AssetDialog') {
        var nreAssets = JSON.stringify(NonRealEstateAssets);
        args["nreAssets"] = nreAssets;
        var result = gService.Loan1003PopupService.call("SaveNonRealEstateAssetRecords", args);
        if (!result.error) {
            if (result.value["ErrorMessage"] != undefined) {
                showErrorPopup(result.value["ErrorMessage"], false);
                followLink = false;
            }
            else {
				editingAsset = "";
				cleanUp();
                refreshCalculation();
            }
        }
    }
    else if (name == "ReoDialog") {
        var reAssets = JSON.stringify(RealEstateAssets);
        args["reAssets"] = reAssets;
        var result = gService.Loan1003PopupService.call("SaveRealEstateRecords", args);
        if (!result.error) {
            if (result.value["ErrorMessage"] != undefined) {
                showErrorPopup(result.value["ErrorMessage"], false);
                followLink = false;
            }
            else {
				editingAsset = "";
				cleanUp();
                refreshCalculation();
            }
        }

        // Assets and REOs share the 'AssetDialog' modal, so
        // set the name here to ensure the dialog is hidden.
        name = 'AssetDialog';
    }

    ///	
	if (oldIsDirty) {
		updateDirtyBit();
	}
	else {
		clearDirty();
	}

    $("#" + name).modal('hide');
}

const AssetRequired = ("Loan1003pg2_Assets_OwnerT Loan1003pg2_Assets_AssetT Desc Addr Zip City assets_AssetST Loan1003pg2_Assets_Type Loan1003pg2_Assets_StatT GrossRentI OccR HExp ComNm Val").split(" ");

function checkValidation(formName:JQuery) {
    var inputItems = formName.find('input,select');
    inputItems.removeAttr('required');

    inputItems.each((i, ele) => {
        const input = ele as HTMLInputElement;
        input.required = (AssetRequired.includes(input.id) && $(input).is(":visible"));
    });
}

/********************** EMPLOYMENT POPUP ********************************/
var borrowerLastIndex = 0;
var coborrowerLastIndex = 0;

//update radio button id for input and label
function setRadioButton(id:string, index:number){
    $(`div#${id}${index} input[id^=IsSelfEmplmt1]`).attr({
        name: `IsSelfEmplmt${id}${index}`,
        id  : `IsSelfEmplmt1${id}${index}`,
    });
    $('div#' + id + index + " input[id^=IsSelfEmplmt1]").parent().next().attr('for', `IsSelfEmplmt1${id}${index}`);

    $(`div#${id}${index} input[id^=IsSelfEmplmt2]`).attr({
        name: `IsSelfEmplmt${id}${index}`,
        id  : `IsSelfEmplmt2${id}${index}`,
    });
    $(`div#${id}${index} input[id^=IsSelfEmplmt2]`).parent().next().attr('for', `IsSelfEmplmt2${id}${index}`);
}

function setEmployment(employ:any, isForBorrower:boolean) {
    const id = isForBorrower ? "aB_prev" : "aC_prev";
    const index = isForBorrower ? borrowerLastIndex : coborrowerLastIndex;
    $(`div#${id}${index} input#EmplrNm`).val(employ.EmplrNm);

    setRadioButton(id, index);

    $(`div#${id}${index} input[name=IsSelfEmplmt${id}${index}][value=${employ.IsSelfEmplmt}]`).prop('checked', true);
}

function addEmployment(isForBorrower:boolean) {
    var id = isForBorrower ? "aB_prev" : "aC_prev";
    var index = isForBorrower ? ++borrowerLastIndex : ++coborrowerLastIndex;
    $("#" + id).append(`<div id="${id}${index}"></div>`);
    $("#" + id + index).html($("#prev_template").html());

    //modify the date startDe's id and their links' onclick method so that the calendar fills in the correct box
    var $div = $("#" + id + index);
    var $startD = $div.find("#Loan1003pg1_Employment_EmplmtStartD");
    $startD.attr("id", $startD.attr("id")! + index);
    $startD.removeClass('mask-initialized');
    var $startDEnd = $div.find("#Loan1003pg1_Employment_EmplmtEndD");
    $startDEnd.attr("id", $startDEnd.attr("id")! + index);
    $startDEnd.removeClass('mask-initialized');
    TPOStyleUnification.UnifyTemplateCalendars($startD);
    TPOStyleUnification.UnifyTemplateCalendars($startDEnd);

    var $phone = $div.find("#EmplrBusPhone");

    if (typeof (_initMask) == 'function') {
        _initMask($startD);
        _initMask($startDEnd);
        _initMask($phone);
    }

    attachCommonEvents($startD[0]);
    attachCommonEvents($startDEnd[0]);
    attachCommonEvents($phone[0]);

    $phone.blur();

    var $zip = $div.find("#Loan1003pg1_Employment_EmplrZip");
    var $cityText = $div.find("#Loan1003pg1_Employment_EmplrCity");
    var $stateDDL = $div.find("#Loan1003pg1_Employment_EmplrState");
    $zip.blur(function(this:HTMLInputElement) {
        smartZipcode(this,
            $cityText[0] as HTMLInputElement,
            $stateDDL[0] as HTMLInputElement);
    });

    setRadioButton(id, index);

    $(`div#${id}${index} input[name=IsSelfEmplmt${id}${index}][value=false]`).prop('checked', true);

    TPOStyleUnification.Components($div);
}

function removeEmployment(item:HTMLElement) {
    var id = $(item).parent().parent().parent().parent().attr('id');
    $("#" + id).remove();
}


/********************** ASSETS POUPUP ********************************/
var editingAsset = "";
//the id of current editting row

var isReset = false;
//check if the form is reset or not

function getAssetId(row:HTMLElement) {
    return $(row).closest('tr').attr('id')!.split(".");
}

function f_editLiability() {
    const _sLID = document.getElementById("sLID") as HTMLInputElement;
    const _applicationID = document.getElementById("applicationID") as HTMLInputElement;
    const sLID = (_sLID == null) ? null : _sLID.value;
    const applicationID = (_applicationID == null) ? null : _applicationID.value;

    IframePopup.show(`${ML.VirtualRoot}/webapp/Loan1003LiabilityEditor.aspx?loanid=${sLID}&appid=${applicationID}`, {})
    .then(() => refreshCalculation());
    return false;
}


function updateTotalAssetValues() {
    const nonLiquid = NonRealEstateAssets.filter(a => a.AssetT == 0).map(a => Number(a.Val.toString().replace(/[^0-9\.]+/g, ""))).reduce((s, v) => s+v, 0);
    const liquid = NonRealEstateAssets.filter(a => a.AssetT != 0).map(a => Number(a.Val.toString().replace(/[^0-9\.]+/g, ""))).reduce((s, v) => s+v, 0);
    const realAsset = RealEstateAssets.map(a => Number(a.Val.toString().replace(/[^0-9\.]+/g, ""))).reduce((s, v) => s+v, 0);
    const total = liquid + nonLiquid + realAsset;

    $("#TotalLiquidAssetValue").html(formatMoney(liquid));
    $("#TotalNonLiquidAssetValue").html(formatMoney(nonLiquid));
    $("#TotalRealEstateAssetValue").html(formatMoney(realAsset));
    $("#TotalAssetValue").html(formatMoney(total));
}

export function loadAssets() {
    var r = false; //is real estate
    var n = false; //is no real estate
    var index = -1; //row number
    if (editingAsset != "") {
        $("#btnAsset").text("Update Asset");
        var [type, sIndex] = editingAsset.split(".");
        n = type == "n";
        r = type == "r";
        index = Number(sIndex);
    } else {
        $("#btnAsset").text("Add Asset");
    }
    var nonRealEstateContent = "";
    for (var i = 0; i < NonRealEstateAssets.length; i++) {
        nonRealEstateContent += (n && index == i) ? editNonRealEstateSchema(i) : normalNonRealEstateSchema(i);
    }
    $("#tNonRealEstateAssets").html(nonRealEstateContent);

    var realEstateContent = "";
    for (var i = 0; i < RealEstateAssets.length; i++) {
        realEstateContent += (r && index == i) ? editRealEstateSchema(i) : normalRealEstateSchema(i);
    }
    $("#tRealEstateAssets").html(realEstateContent);
    updateTotalAssetValues();
    //update the visibility of items every loading time
    AssetsChanged();
    PropStatusChanged();
}

function normalNonRealEstateSchema(index:number) {
    const ownerT = NonRealEstateAssets[index].OwnerT;

    return `<tr id='n.${index}'>
        <td><a onclick='editAsset(this)'>edit</a></td>
        ${(
            ownerT == 0 ? `<td>${$("#owner1").text()}</td>` : (
            ownerT == 1 ? `<td>${$("#owner2").text()}</td>` : (
            ownerT == 2 ? `<td>Joint</td>` : (
            ""))))}
        <td>${getAssetType(NonRealEstateAssets[index].AssetT)}</td>
        <td>${NonRealEstateAssets[index].ComNm}</td>
        <td>${NonRealEstateAssets[index].AccNum}</td>
        <td class='currency input-w135'>${NonRealEstateAssets[index].Val}</td>
        <td class='input-w70'><a onclick='removeAsset(this)'>delete</a></td>
    </tr>`;
}

const statusLabel: {[key:string]: string} = {
    S: "Sold",
    PS: "Pending Sale",
    R: "Rental",
};
function normalRealEstateSchema(index:number) {

    var owner = RealEstateAssets[index].OwnerT;
    var status = RealEstateAssets[index].StatT;

    return `<tr id='r.${index}'>
        <td><a onclick='editAsset(this)'>edit</a></td>
        ${(
            owner == 0 ? `<td>${$("#owner1").text()}</td>` : (
            owner == 1 ? `<td>${$("#owner2").text()}</td>` : (
            owner == 2 ? `<td>Joint</td>` : (
            ""))))}
        <td>${RealEstateAssets[index].Addr}</td>
        <td>${!status ? "Retained" : (statusLabel[status] || "")}</td>
        <td class='currency'>${RealEstateAssets[index].Val}</td>
        <td class='currency'>${RealEstateAssets[index].GrossRentI}</td>
        <td class='currency input-w135'>${RealEstateAssets[index].HExp}</td>
        <td class='input-w70'><a onclick='removeAsset(this)'>delete</a></td>
    </tr>`;
}

function editNonRealEstateSchema(index:number) {
    const ownerT = NonRealEstateAssets[index].OwnerT;

    return `<tr id='n.${index}'>
        <td><a onclick='resetAsset()'>reset</a></td>
        ${(
            ownerT == 0 ? "<td>Borrower</td>" : (
            ownerT == 1 ? "<td>CoBorrower</td>" : (
            ownerT == 2 ? "<td>Joint</td>" : (
            ""))))}
        <td>${getAssetType(NonRealEstateAssets[index].AssetT)}</td>
        <td>${NonRealEstateAssets[index].ComNm}</td>
        <td>${NonRealEstateAssets[index].AccNum}</td>
        <td class='currency'>${NonRealEstateAssets[index].Val}</td>
        <td><a onclick='cancelEditAsset()'>cancel</a></td>
    </tr>`;
}

function editRealEstateSchema(index:number) {
    var ownerT = RealEstateAssets[index].OwnerT;
    var status = RealEstateAssets[index].StatT;

    return `<tr id='r.${index}'>
        <td><a onclick='resetAsset()'>reset</a></td>
        ${(
            ownerT == 0 ? "<td>Borrower</td>" : (
            ownerT == 1 ? "<td>CoBorrower</td>" : (
            ownerT == 2 ? "<td>Joint</td>" : (
            ""))))}
        <td>${RealEstateAssets[index].Addr}</td>
        <td>${!status ? "Retained" : (statusLabel[status] || "")}</td>
        <td class='currency'>${RealEstateAssets[index].Val}</td>
		<td class='currency'>${RealEstateAssets[index].GrossRentI}</td>
		<td class='currency'>${RealEstateAssets[index].HExp}</td>
		<td><a onclick='cancelEditAsset()'>cancel</a></td>
    </tr>`;
}

function getAssetType(id:string) {
    const i = Number(id);
    const x = AssetType.find(({value}) => value == i);
    return x == null ? "" : x.text;
}

function cleanUp() {
    //clear the data of form. Just remain the Asset type.
    isReset = true;
    //var val = $("#Loan1003pg2_Assets_AssetT").val();
    //$("#Loan1003pg2_Assets_AssetT").val(val);

    $("#Loan1003pg2_Assets_OwnerT").val("0");
    $("#IsPrimaryResidence").prop('checked', false);
    $("#IsSubjectProp").prop('checked', false);
    $("#Addr").val("");
    $("#Loan1003pg2_Assets_Zip").val("");
    $("#Loan1003pg2_Assets_City").val("");
    $("#Loan1003pg2_Assets_AssetST").val("");
    $("#Loan1003pg2_Assets_Type").val("0");
    $("#Loan1003pg2_Assets_StatT").val("S");
    $("#GrossRentI").val("");
	$("#OccR").val("");
    $("#HExp").val("");
    $("#Val").val("");


    isReset = false;
    if (typeof (_initInput) == 'function') _initInput();
}

export function AssetsChanged() {
    //onchange function of Asset Type dropdownlist
    if (isReset) return;
    const s = $("#Loan1003pg2_Assets_AssetT").val();
    $("#Other_group").toggle(s == 9 || s == 11);
    $("[name='RE_group']").toggle(s == -1);
    $("[name='Financial_group']").toggle(s != 0 && s != 9 && s != -1)
    checkValidation($("#assetForm"));
}

export function PropStatusChanged() {
    if (isReset) {
		return;
	}

    var s = $("#Loan1003pg2_Assets_StatT").val();
	$(".Rental_group").toggle(s == "R");
	if (s == "R" && $("#OccR").val() == "") {
		$("#OccR").val("75");
	}
    checkValidation($("#assetForm"));
}

export function addAsset() {
    //alert($("#AssetT").val());
    if ($("#Loan1003pg2_Assets_AssetT").val() != -1)//non real estate
    {
        NonRealEstateAssets.push(retrieveAsset());
    } else { //real estate
        RealEstateAssets.push(retrieveAsset());
    }
    cleanUp();
    loadAssets();
}

export function updateAsset() {
    //alert($("#AssetT").val());
    //Update current row
    const [aType, aIndex] = editingAsset.split(".");
    const index = Number(aIndex);
    if (aType == "n" && $("#Loan1003pg2_Assets_AssetT").val() != -1) { //non real estate no change
        NonRealEstateAssets[index] = retrieveAsset();
    }
    if (aType == "r" && $("#Loan1003pg2_Assets_AssetT").val() == -1) { //real estate no change
        RealEstateAssets[index] = retrieveAsset();
    }
    if (aType == "n" && $("#Loan1003pg2_Assets_AssetT").val() == -1) { //non real estate -> real estate
        //remove the asset in Non real estate
        NonRealEstateAssets.splice(index, 1);
        //add new asset to Real Estate
        RealEstateAssets.push(retrieveAsset());

    }
    if (aType == "r" && $("#Loan1003pg2_Assets_AssetT").val() != -1) { //real estate -> non real estate
        //remove the asset in Real estate
        RealEstateAssets.splice(index, 1);
        //add new asset to Non real estate
        NonRealEstateAssets.push(retrieveAsset());
    }
    editingAsset = "";
    cleanUp();
    loadAssets();
}

function editAsset(row:HTMLElement) {
    isReset = false;
    editingAsset = $(row).closest('tr').attr('id')!;
    loadAssets();
    //fill up
    resetAsset();

    //update the visibility: fix bug in Edit assets
    AssetsChanged();
    PropStatusChanged();
	updateDirtyBit();
}

function removeAsset(row:HTMLElement) {
    simpleDialog.confirm('Do you want to delete this asset?').then((isConfirmed:boolean)=>{
        if(isConfirmed){
            var [aType, aIndex] = getAssetId(row);
            const index = Number(aIndex);
            if (aType == "n")//Non real estate
            {
                NonRealEstateAssets.splice(index, 1);
            }
            if (aType == "r")//Real estate
            {
                RealEstateAssets.splice(index, 1);
            }
            if (editingAsset != "") {
                //update id
                var itemEditting = editingAsset.split(".");
                if (index < Number(itemEditting[1])) {
                    itemEditting[1] = (Number(itemEditting[1]) - 1).toString();
                }
                editingAsset = itemEditting[0] + "." + itemEditting[1];
            }
            loadAssets();
			updateDirtyBit();
        }
    });
}

export function cancelEditAsset() {
    editingAsset = "";
    cleanUp();
    loadAssets();
}

function retrieveAsset() {
    const IsSubjectProp = document.getElementById("IsSubjectProp") as HTMLInputElement;
    const IsPrimaryResidence = document.getElementById("IsPrimaryResidence") as HTMLInputElement;

    //get data from form to variables
    const asset = new Asset();
    asset.OwnerT             = $("#Loan1003pg2_Assets_OwnerT").val();
    asset.AssetT             = $("#Loan1003pg2_Assets_AssetT").val();
    asset.ComNm              = $("#ComNm").val();
    asset.Desc               = $("#Desc").val();
    asset.City               = $("#Loan1003pg2_Assets_City").val();
    asset.State              = $("[id$='AssetST']").first().val();
    asset.Zip                = $("#Loan1003pg2_Assets_Zip").val();
    asset.AccNum             = $("#AccNum").val();
    asset.Val                = $("#Val").val();

    asset.Addr               = $("#Addr").val();
    asset.GrossRentI		 = $("#GrossRentI").val();
	asset.OccR				 = $("#OccR").val();
    asset.HExp               = $("#HExp").val();
    asset.Type               = $("#Loan1003pg2_Assets_Type").val();
    asset.StatT              = $("#Loan1003pg2_Assets_StatT").val();
    asset.IsSubjectProp      = IsSubjectProp.checked;
    asset.IsPrimaryResidence = IsPrimaryResidence.checked;
    asset.recordID           = $("#recordID").val();
    return asset;
}

export function resetAsset() {
    const IsSubjectProp = document.getElementById("IsSubjectProp") as HTMLInputElement;
    const IsPrimaryResidence = document.getElementById("IsPrimaryResidence") as HTMLInputElement;

    const [aType, aIndex] = editingAsset.split(".");
    const index = Number(aIndex);
    const asset = aType == "n" ? NonRealEstateAssets[index] : RealEstateAssets[index];
    $("#Loan1003pg2_Assets_OwnerT").val(asset.OwnerT);
    $("#Loan1003pg2_Assets_AssetT").val(asset.AssetT);
    AssetsChanged();
    $("#ComNm").val(asset.ComNm);
    $("#Desc").val(asset.Desc);
    $("#Loan1003pg2_Assets_City").val(asset.City);
    $("[id$='AssetST']").val(asset.State);
    $("#Loan1003pg2_Assets_Zip").val(asset.Zip);
    $("#AccNum").val(asset.AccNum);
    $("#Val").val(asset.Val);

    $("#Addr").val(asset.Addr);
    $("#HExp").val(asset.HExp);
    $("#Loan1003pg2_Assets_Type").val(asset.Type);

	// set the Assets_StatT, then PropStatusChanged to trigger visibility, then reset the values to original
    $("#Loan1003pg2_Assets_StatT").val(asset.StatT);	
	PropStatusChanged();
	$("#GrossRentI").val(asset.GrossRentI);
	$("#OccR").val(asset.OccR);

    IsSubjectProp.checked = asset.IsSubjectProp;
    IsPrimaryResidence.checked = asset.IsPrimaryResidence;
    $("#recordID").val(asset.recordID);
}

Object.assign(window, {
    addEmployment,
    AssetsChanged,
    cancelEditAsset,
    DialogHandler,
    editAsset,
    f_editLiability,
    getAssetType,
    onCancel,
    onSave,
    removeAsset,
    removeEmployment,
    PropStatusChanged,
    resetAsset,
});

