declare const attachCommonEvents: (e: HTMLElement, b?: boolean) => void;
declare const disableDDL: (e: HTMLElement, b: boolean) => void;
declare const format_money: (e: HTMLElement) => void;
declare const formatReadonlyField: (e: HTMLElement) => void;
declare const _initMask: (e: JQuery) => void;
declare const onSelect: (id: string) => void;
declare const TPOStyleUnification: {
	UnifyTemplateCalendars(el?:JQuery<HTMLInputElement>|JQLite): void;
	Components:(el?:JQuery<HTMLElement>|JQLite) => void
};
declare const isDisableOtherIncomeFreeEntry_135564 : boolean;
declare const refreshCalculation: () => void;

let sProOHExp       :HTMLInputElement;
let sProOHExpLckd   :HTMLInputElement;
let OtherIncomeJson :HTMLInputElement;
$(() => {
	sProOHExp       = document.getElementById('Loan1003pg2_sProOHExp'      ) as HTMLInputElement;
	sProOHExpLckd   = document.getElementById('Loan1003pg2_sProOHExpLckd'  ) as HTMLInputElement;
	OtherIncomeJson = document.getElementById('Loan1003pg2_OtherIncomeJson') as HTMLInputElement;
});

interface IOtherIncome {
	Desc:string,
	Amount:string,
	IsForCoBorrower:boolean;
}

function p2_init() {
	sProOHExp.readOnly = !sProOHExpLckd.checked;
	bindOtherIncomeTable();
	TPOStyleUnification.Components();
}

function checkForSubsequentBlanks() {
	// 8/20/2013 EM - If there are only 3 rows, and a row has all subsequent rows as blanks, disable the delete
	const data = JSON.parse(OtherIncomeJson.value) as IOtherIncome[];

	if (data.length == 3) {
		//work backwards from the last index
		for (var i = 2; i >= 0; i--) {
			const income = data[i];
			if (income.Desc == "" && (income.Amount == "" || income.Amount == "0.00")) {
				const deleteLink = document.getElementById("delete_" + i) as HTMLButtonElement;
				deleteLink.removeAttribute("href");
				deleteLink.disabled = true;
			} else
				break;
		}
	}
}
function calculateOtherIncomeJson() {
	const OtherIncomeContent = document.getElementById("OtherIncomeContent") as HTMLElement;
	const data = Array.from(OtherIncomeContent.querySelectorAll("tr")).map(function(tr:HTMLElement, i:number) {

		const isForCoborrower = (document.querySelector(`#s_${i}`) as HTMLInputElement).value == "C";
		const desc = (document.querySelector(`#d_${i}`) as HTMLInputElement).value;
		const valueEle = document.querySelector(`#a_${i}`) as HTMLInputElement;

		format_money(valueEle);

		let amt = valueEle.value.replace('$', '').replace(/,/g, '');
		if (amt == '') amt = '0';
		if (amt.indexOf("(", 0) == 0) {
			amt = amt.replace("(", "-");
			amt = amt.replace(")", "");
		}

		return ({ Amount: amt, Desc: desc, IsForCoBorrower: isForCoborrower });
	});

	OtherIncomeJson.value = JSON.stringify(data);
}

function onChangeOtherIncome() {
	calculateOtherIncomeJson();
	refreshCalculation();
}

function onDeleteClick(index:number) {
	const data: IOtherIncome[] = JSON.parse(OtherIncomeJson.value);
	const dataNew = data.filter((x, i) => i != index);

	OtherIncomeJson.value = JSON.stringify(dataNew);
	bindOtherIncomeTable();
	updateDirtyBit();
	//subtract by to ensure you have at least three since you're deleting an entry as well
	if (data.length - 4 < 0) onAddMoreIncomeClick();
}

function onAddMoreIncomeClick() {
	calculateOtherIncomeJson();

	const jsonStr = document.getElementById('Loan1003pg2_OtherIncomeJson') as HTMLInputElement;
	const data = JSON.parse(jsonStr.value);
	data.push({ "Amount": 0.00, "Desc": "", "IsForCoBorrower": false });
	jsonStr.value = JSON.stringify(data);
	bindOtherIncomeTable();
	updateDirtyBit();
}

const bindOtherIncomeTable = (function() {
	return function bindOtherIncomeTable() {
        const isReadOnly = ML.sIsIncomeCollectionEnabled;
        const disabledStr = isReadOnly ? "disabled" : "";
        const readOnlyStr = isReadOnly ? "readonly" : "";

		const data = JSON.parse($('#Loan1003pg2_OtherIncomeJson').val() as string) as IOtherIncome[];
		const isDisabled = data.length < 4;

		const body = data.map((x, i:number) => (
			`<tr>
				<td style='padding-top: 4px;padding-bottom: 4px;'>
					<div class='select-box select-box-noimage'>
						<select id='s_${i}' onChange='onChangeOtherIncome()' ${disabledStr} class='select-w40'>
							<option value='B' ${!x.IsForCoBorrower ? "selected" : ""}>B</option>
							<option value='C' ${ x.IsForCoBorrower ? "selected" : ""}>C</option>
						</select>
					</div>
				</td>
				<td style='padding-right: 2em;'>
					<div class='wrap-combo'>
						<input type='text' id='d_${i}' value='${x.Desc}' style='width:430px' maxlength='100' class='form-control-combo' onkeydown='onComboboxKeypress(this);' onChange='onChangeOtherIncome()' ${readOnlyStr}>
						<i id='i_${i}' class='combobox_img'>&#xE5C5;</i>
					</div>
				</td>
				<td>
					<input type='text' id='a_${i}' value='${x.Amount}' preset='money' class='form-control-money' onChange='onChangeOtherIncome()' ${readOnlyStr}>
				</td>
				<td>${
					(!isReadOnly && x.Desc == "" && (x.Amount == "" || x.Amount == "0.00") && !isDisabled)
						? (`<a id='delete_${i}' onclick="onDeleteClick('${i}'); return false;"><i class='material-icons large-material-icons'>&#xE909;</i></a>`)
						: (`<a id='delete_${i}' disabled='true' class='disabled'><i class='material-icons large-material-icons disabled'>&#xE909;</i></a>`)
					}</td>
			</tr>`
			)).join("");
		const OtherIncomeContent = document.getElementById("OtherIncomeContent") as HTMLElement;
		OtherIncomeContent.innerHTML = body;

		$("#OtherIncomeContent").find("select").each(function() {
			attachCommonEvents(this, false);

			if (isReadOnly) disableDDL(this, true);
		});

		$("#OtherIncomeContent").find("input").each(function() {
			attachCommonEvents(this, false);

			if ($(this).attr("preset") == "money") {
				format_money(this);
				_initMask($(this));
				$(this).removeAttr("style");
			}
			if (isReadOnly) formatReadonlyField(this);
		});

		$('#Loan1003pg2_OtherIncomeCombo_cbl').attr('shared_combo', 'shared_combo');
		$("#OtherIncomeContent").find(".combobox_img").each(function() {

			attachCommonEvents(this, false);

			if (isReadOnly)
				formatReadonlyField(this);
			else {
				$(this).on("click", function(this:HTMLElement) {
					let thisId = $(this).attr('id')!.replace('i', 'd');
					$('table[shared_combo]').attr('id', thisId + '_cbl');
					onSelect(thisId);
				});
			}
		});
		checkForSubsequentBlanks();
	}
})();

Object.assign(window,  {
	p2_init,
	onChangeOtherIncome,
	onDeleteClick,
	onAddMoreIncomeClick,
});
