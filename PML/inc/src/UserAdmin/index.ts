import { module, IDirective, ITimeoutService } from "angular";

import {simpleDialog} from "../components/IframePopup";

import {reRefDirective        } from "../ng-common/reRef";
import {ngMdcCheckboxDirective} from "../ng-common/ngMdcCheckbox";
import {ngMdcRadioDirective   } from "../ng-common/ngMdcRadio";
import {ngFileInput           } from "../ng-common/ngFileInput";
import {sortHeaderDirective   } from "../ng-common/sortHeader";

import {IUser} from "./IUser";
const $ = jQuery;

import html from "./index.html";
import { postJsonSafeD } from "../utils/fetch";
class UserAdminController {
    private users   : IUser[];

    private userName: string  = "";

    private sortKey : string  = "UserName";
    private sortDesc: boolean = false;
    private onSort(sortKey: string) {
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
    }

    static $inject = ["$element", "$timeout"];
    constructor(private $element:JQuery, private $timeout: ITimeoutService) {
        this.sortKey = "UserName";
        this.sortDesc = false;

        this.searchUser();
        $element.find("[autofocus]").focus();

        (window as any).postback = () => {
            this._searchUser();
        };
    }


    private searchUser(){
        this._searchUser();
    }
    private resetSearch(){
        this.userName = "";
        this.searchUser();
    }
    private async _searchUser() {
        const [firstName = "", lastName = ""] = this.userName.split(" ");

        const [error, res] = await postJsonSafeD<[string|null, IUser[]]>(`${location.pathname}/GetUsers`, { firstName, lastName });
        if (error) {
            simpleDialog.alert(error.toString(), "Error");
            return;
        }
        const [err, users] = res;
        if (err) {
            simpleDialog.alert(err.toString(), "Error");
            return;
        }

        this.$timeout(() => {
            this.users = users!;
        });
    }

    private editUser(user:IUser){
        const url = `./EditUser.aspx?cmd=edit&employeeId=${user.EmployeeId}`;
        window.parent.showEditUser(url, {
            backdrop:'static',
            resizable:false, draggable: false,
        });
    }
    private addUser(){
        window.parent.showEditUser(`./EditUser.aspx?cmd=add`, {
            resizable:false, draggable: false,
        });
    }
}
export function userAdminDirective(): IDirective {
    return {
        restrict     : "EA",
        controller   : UserAdminController,
        controllerAs : "vm",
        template     : html,
    };
}

async function apiFetch<T>(op:string, data:{}){
    return postJsonSafeD<T>(`${location.pathname}/${op}`, data);
}


export const moduleUserAdmin = module("UserAdmin", [])

.directive(reRefDirective        .ngName, reRefDirective        )
.directive(ngMdcCheckboxDirective.ngName, ngMdcCheckboxDirective)
.directive(ngMdcRadioDirective   .ngName, ngMdcRadioDirective   )
.directive(ngFileInput           .ngName, ngFileInput           )
.directive(sortHeaderDirective   .ngName, sortHeaderDirective   )

.directive("userAdmin", userAdminDirective)
;
