import * as angular from "angular";
import {LqbPopup, IframeSelf} from "../components/IframePopup";
import { TPOStyleUnification } from "../components/TPOStyleUnification";
import { getQueryStringFromSearch } from "../utils/url";

declare const rolodexListViewModel: {
    ShowAssignedEmployees  : boolean;
    AllowReadingFromRolodex: boolean;
    AllowWritingToRolodex  : boolean;
    HasFeaturesPriceMyLoan : boolean,
    typeFilterOptions0     : {label: string, value: number}[],
    typeFilterOptions1     : {label: string, value: string}[],
};
const $ = jQuery;

interface IItem {
    AgentID                          : string,
    AgentType                        : number,
    AgentNm                          : string,
    AgentTitle                       : string,
    AgentPhone                       : string,
    AgentAltPhone                    : string,
    AgentFax                         : string,
    AgentComNm                       : string,
    AgentEmail                       : string,
    AgentAddr                        : string,
    AgentCity                        : string,
    AgentState                       : string,
    AgentZip                         : string,
    AgentCounty                      : string,
    AgentDepartmentName              : string,
    AgentPager                       : string,
    AgentLicenseNumber               : string,
    CompanyLicenseNumber             : string,
    PhoneOfCompany                   : string,
    FaxOfCompany                     : string,
    IsNotifyWhenLoanStatusChange     : boolean,
    AgentWebsiteUrl                  : string,
    AgentN                           : string,
    CommissionPointOfLoanAmount      : number,
    CommissionPointOfGrossProfit     : number,
    CommissionMinBase                : number,
    LicenseXmlContent                : string,
    CompanyId                        : string,
    AgentBranchName                  : string,
    AgentPayToBankName               : string,
    AgentPayToBankCityState          : string,
    AgentPayToABANumber              : string,
    AgentPayToAccountName            : string,
    AgentPayToAccountNumber          : string,
    AgentFurtherCreditToAccountName  : string,
    AgentFurtherCreditToAccountNumber: string,
    AgentIsApproved                  : true,
    AgentIsAffiliated                : boolean,
    OverrideLicenses                 : boolean,
    AgentComNmBranchNm               : string,
    SystemId                         : number,
    IsSettlementServiceProvider      : boolean,
    AgentTypeDesc                    : string,

    IsAgentAltPhoneForMultiFactorOnly: boolean,
    CompanyDisplayName               : string,
    companyAddress                   : string,
    companyCity                      : string,
    companyState                     : string,
    companyZip                       : string,
    LosIdentifier                    : string,
    CompanyLosIdentifier             : string,
    companyLicensesXml               : string,
    EmployeeIDInCompany              : string,

    returnData                       : { AgentLicensesPanel: string, CompanyLicensesPanel:string},
}


import rolodexListTemplate from "./index.html";
import { postJsonSafeD } from "../utils/fetch";
class RolodexListController {
    private readonly statusFilterOptions     = [{ label: "Active", value: 1 }, {label: "Inactive", value: 0}];
    private readonly typeFilterOptions0      = rolodexListViewModel.typeFilterOptions0;
    private readonly typeFilterOptions1      = rolodexListViewModel.typeFilterOptions1;
    private readonly AllowReadingFromRolodex = rolodexListViewModel.AllowReadingFromRolodex;
    private readonly AllowWritingToRolodex   = rolodexListViewModel.AllowWritingToRolodex;
    private readonly HasFeaturesPriceMyLoan  = rolodexListViewModel.HasFeaturesPriceMyLoan;
    private readonly ShowAssignedEmployees   = rolodexListViewModel.ShowAssignedEmployees;


    private nameFilter     :string;
    private typeFilter0    :number|null;
    private typeFilter1    :string|null;
    private statusFilter   :0|1;
    private sLId           :(string|null);
    private tabIndex       :number;

    private items          : IItem[]|null;
    private disabledSearch = false;
    private element        : JQuery;
    private iframeSelf     : IframeSelf;

    static $inject = ["$element", "$timeout"];
    constructor(private $element:JQuery, private $timeout: angular.ITimeoutService) {
        const popup = new LqbPopup($("div[lqb-popup]")[0]);
        const iframeSelf = this.iframeSelf = new IframeSelf();
        iframeSelf.popup = popup;
        if (window.self === window.top) iframeSelf._show({} as any);

        this.onSort("AgentNm");

        const qs = getQueryStringFromSearch(location.search);

        this.nameFilter      = "";
        this.typeFilter0     = Number(qs.type) || null;
        this.typeFilter1     = null;
        this.statusFilter    = 1;
        this.sLId            = null;
        this.tabIndex        = 0;
        this.element         = $element;

        this.search();
    }

    private search(event?: Event) {
        this.disabledSearch = true;

        postJsonSafeD<IItem[]>(`${location.pathname}/GetDataSet`, {
            tab               : this.tabIndex,
            nameFilter        : this.nameFilter,
            typeFilter0       : this.typeFilter0 || -1,
            typeFilter1       : this.typeFilter1,
            statusFilter      : this.statusFilter,
            sLId              : !this.sLId ? null : this.sLId,
        }).then(([err, d]) => {
            this.$timeout(() => {
                this.disabledSearch = false;
                if (err != null) { console.error(err); return; }
                this.items = d;
                if (d.length) {
                    setTimeout(() => {
                        TPOStyleUnification.fixedHeaderTable(this.element.find("table.modal-table-scrollbar")[0] as any);
                    });
                }
            });
        });

        if (event != null) event.preventDefault();
        return false;
    }

    private changeTab(tab:number) {
        this.tabIndex = tab;
        //this.typeFilter0 = -1;
        this.statusFilter = 1;

        if (tab == 2 && !this.nameFilter) {
            this.items = null;
            return;
        }

        this.search();
    }

    private onTypeFilter0Change (){ this.search(); }
    private onTypeFilter1Change (){ this.search(); }
    private onStatusFilterChange(){ this.search(); }

    private select(agent:IItem) {
        if (this.tabIndex == 3) {
            this.onEmailClick(agent);
            return;
        }

        const args = Object.assign(
            { OK: true, },
            agent.returnData,
            {
                AgentLicensesPanel   : `LICENSES.List['User'] = ${agent.returnData.AgentLicensesPanel}`,
                CompanyLicensesPanel : `LICENSES.List['Company'] = ${agent.returnData.CompanyLicensesPanel}`,
            },
            (this.tabIndex !== 0) ? {} : {
                AgentSourceT      : 2, // from contact list
            },
        );
        this.iframeSelf.resolve(args);
    }

    private onEmailClick(item: IItem, event ?: MouseEvent) {
        if (!rolodexListViewModel.ShowAssignedEmployees) return;

        this.iframeSelf.resolve({OK:true, AgentEmail:item.AgentEmail});
        if (event != null) event.preventDefault();
        return false;
    }

    private onAddClick(){
        const rolodexController = this;
        showModal('/los/rolodex/rolodex.aspx', null, null, null, function(args:any){
            if (args.OK) rolodexController.changeTab(0);
        });
    }
    private onEditClick(item:IItem) {
        const rolodexController = this;
        var args = showModal('/los/rolodex/rolodex.aspx?cmd=edit&agentid=' + item.AgentID, null, null, null, function(args: any){
            if (args.OK) rolodexController.changeTab(0);
        });
    }

    private closeDialog(event ?: MouseEvent){
        this.iframeSelf.resolve({OK:false});
        if (event != null) event.preventDefault();
        return false;
    }

    private sortKey:string;
    private sortDesc:boolean;
    private onSort(sortKey: string) {
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
    }
}

function rolodexListDirective(): angular.IDirective {
    return {
        restrict    : "EA",
        controller  : RolodexListController,
        controllerAs: "vm",
        template    : rolodexListTemplate,
    }
}
angular.module("RolodexList", ["TpoCommon"])
.directive("rolodexList", rolodexListDirective);
