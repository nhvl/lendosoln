/// <reference types="bootstrap"/>
/// <reference path="../dt-local/tpo.d.ts"/>

import { passiveSupported } from "../utils/eventOnce";

declare function updateDirtyBit(input:HTMLInputElement): void;

const $ = jQuery;

const calendarSvgData:string =
    `<svg viewBox="0 0 18 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <title>Calendar</title>
        <desc>Created using Figma</desc>
        <g id="Canvas" transform="translate(-255 -3935)">
            <g id="DatePicker">
                <g id="Vector">
                    <use xlink:href="#calendar_path0_fill" transform="translate(257.25 3936.5)" />
                    <use xlink:href="#calendar_path1_fill" transform="translate(257.25 3936.5)" />
                    <use xlink:href="#calendar_path2_fill" transform="translate(257.25 3936.5)" />
                    <use xlink:href="#calendar_path3_fill" transform="translate(257.25 3936.5)" />
                </g>
            </g>
        </g>
        <defs>
            <path id="calendar_path0_fill" d="M 5 7.65L 3.33333 7.65L 3.33333 9.35L 5 9.35L 5 7.65ZM 8.33333 7.65L 6.66667 7.65L 6.66667 9.35L 8.33333 9.35L 8.33333 7.65ZM 11.6667 7.65L 10 7.65L 10 9.35L 11.6667 9.35L 11.6667 7.65ZM 13.3333 1.7L 12.5 1.7L 12.5 0L 10.8333 0L 10.8333 1.7L 4.16667 1.7L 4.16667 0L 2.5 0L 2.5 1.7L 1.66667 1.7C 0.741667 1.7 0.00833333 2.465 0.00833333 3.4L 0 15.3C 0 16.235 0.741667 17 1.66667 17L 13.3333 17C 14.25 17 15 16.235 15 15.3L 15 3.4C 15 2.465 14.25 1.7 13.3333 1.7ZM 13.3333 15.3L 1.66667 15.3L 1.66667 5.95L 13.3333 5.95L 13.3333 15.3Z"/>
            <path id="calendar_path1_fill" d="M 3.33333 10.7667L 5 10.7667L 5 12.4667L 3.33333 12.4667L 3.33333 10.7667Z"/>
            <path id="calendar_path2_fill" d="M 6.66667 10.7667L 8.33333 10.7667L 8.33333 12.4667L 6.66667 12.4667L 6.66667 10.7667Z"/>
            <path id="calendar_path3_fill" d="M 10 10.7667L 11.6667 10.7667L 11.6667 12.4667L 10 12.4667L 10 10.7667Z"/>
        </defs>
    </svg>`;

const getTextWidth = (() => {
    let _canvas: HTMLCanvasElement | null = null;
    return function getTextWidth(text: string, font: string) {
        // re-use canvas object for better performance
        const canvas = _canvas == null ? _canvas = document.createElement("canvas") : _canvas;
        const context = canvas.getContext("2d");
        if (context == null) throw new Error("context is null");

        context.font = font;
        const metrics = context.measureText(text);
        return metrics.width;
    }
})();

function unifySortIconsStyle(grid: JQuery){
    grid.find('img[src*="Tri_ASC"]' ).hide().after('<i class="sort material-icons">&#xE5CE;</i>').parent().css("font-weight","Bold");
    grid.find('img[src*="Tri_DESC"]').hide().after('<i class="sort material-icons">&#xE5CF;</i>').parent().css("font-weight","Bold");
}

//unify jquery.tablesorter.js
function unifyTableSorterStyle(table:JQuery){
    table.on("sortEnd",function(this:HTMLElement, e, t){
        const iTags = $(this).find('th i');
        iTags.parent().css("font-weight","");
        iTags.remove();
        $(this).find('th[class*=headerSortDown]').append('<i class="sort material-icons">&#xE5CE;</i>').css("font-weight","Bold");
        $(this).find('th[class*=headerSortUp]'  ).append('<i class="sort material-icons">&#xE5CF;</i>').css("font-weight","Bold");
    });
}

const TPOStyleUnification = (function($) {
    var components:any;

    function unifyComboboxes(){
        /* Format of a styled combobox
        <div class="wrap-combo">
            <input name="FannieMaeAddendumSections$sFannieARMPlanNum" type="text" id="sFannieARMPlanNum" class="form-control-w380 form-control-combo" onkeydown="onComboboxKeypress(this);">
            <i class="combobox_img">&#xE5C5;</i>
            <img src="/PMLDev/images/dropdown_arrow.gif" onclick="onSelect('sFannieARMPlanNum');" class="combobox_img" align="absbottom" style="display: none;">
        </div>
        */
        var comboboxes = $(components).find('img[src*="dropdown_arrow.gif"]');
        comboboxes.each(function(){
            // Need go to 2 previous elements to check form-control-combo class exists or not.
            if(!$(this).prev().prev().hasClass("form-control-combo")){
                var isHidden = ($(this).css('display') == 'none');
                if(!isHidden) {
                    $(this).prev().toggleClass("form-control-combo",true);
                    $(this).hide().before('<i class="combobox_img">&#xE5C5;</i>');
                    var combo=$(this);
                    combo=combo.add($(this).prev());
                    combo=combo.add($(this).prev().prev());
                    combo.wrapAll('<div class="wrap-combo"></div>');
                    $(this).prev().click(function(){
                        $(this).next().click();
                    });
                }
            }
        });
    }

    function unifyInputs(){
        var items=$(components).find('input[preset="percent"]');
        items.each(function(){
            $(this).prop("class","form-control-percent");
            $(this).removeAttr("style");
        });

        items=$(components).find('input[preset="money"]');
        items.each(function(){
            $(this).prop("class","form-control-money");
            $(this).removeAttr("style");
        });

        $(components).find('input[preset="phone"]')
            .addClass("form-control-phone")
            .removeAttr("style");

        $(components).find('input[preset="ssn"]')
            .prop("class", "form-control-ssn")
            .removeAttr("style");

        $(components).find('input[preset="zipcode"]')
            .addClass("form-control-zipcode")
            .removeAttr("style");

        $(components).find('input[preset="date"]')
            .addClass("form-control-date")
            .removeAttr("style");

        items = $(components).find('input[type="file"]');
        items.each(function(i, input){
            const $input = $(input);
            if (!$input.is('[skip-unification]')) {
                const id = input.id;
                if (!$input.hasClass("inputfile")) {
                    $input.addClass("inputfile");
                    $input.after(`<label for="${id}"><strong>Choose file</strong><span>No file chosen</span></label>`);
                    $input.on("change", onFileChange);
                }
            }
        });

        function onFileChange(e:Event){
            onInputFileChange($(e.currentTarget) as JQuery<HTMLInputElement>);
        }

    }
    function onInputFileChange(inputFile: JQuery<HTMLInputElement>){
        const value = inputFile.prop("value");

        if (!value) {
            inputFile.next().html("<strong>Choose file</strong><span>No file chosen</span>");
            inputFile.next().toggleClass("label-file-full-w", false);
        }
        else {
            inputFile.next().html(`<strong>Choose file</strong><span>${value.split('\\').pop()}</span>`);
            inputFile.next().toggleClass("label-file-full-w", true);
        }
    }

    function unifyCalendars(el?:JQuery<HTMLInputElement>) {
        const calendars = el || $(components).find('input[preset="date"]:not(.picker-template)') as JQuery<HTMLInputElement>;

        function updateCalendarFunc(_calendarSvgData:string, _button:JQuery){
            const link = $(`<a title="Open calendar" class="calendar-svg" data-toggle="tooltip" data-placement="bottom">${_calendarSvgData}</a>`);
            link.on('click', function () {
                _button.click();
            });
            link.on('mouseenter',function(){
                if($(this).hasClass('calendar-svg')){
                    var textInput = $(this).prev().prev();
                    if (textInput.prop('readonly') || textInput.prop('disabled')) {
                        $(this).tooltip('disable');
                    }
                    else{
                        $(this).tooltip('enable');
                    }
                }
            });

            _button.hide().wrap(link);
        };

        if (calendars.length > 0) {
            calendars.each(function(i, input) {
                const $calendar = $(input) as JQuery<HTMLInputElement>;
                if (!$calendar.hasClass('picker-init')) {
                    initDatepicker($calendar);
                }
            });

            calendars.parent().find('.ui-datepicker-trigger').each(function (i, button) {
                const $button = $(this);
                if ($button.css('display') != 'none') {
                    $button.hide();
                    updateCalendarFunc(calendarSvgData, $button);
                }
            });
        }
    }

    function initDatepicker($calendar: JQuery<HTMLInputElement>) {
        var picker = $('<input type="hidden" class="datepicker" />');

        $calendar
            .addClass('form-control-date picker-init')
            .add(picker)
            .wrapAll('<div class="calendar-svg"></div>');

        picker.datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: 'both',
            onSelect: function (dateText) {
                var textInput = $(this).prev('input');

                if (textInput.prop('readonly') || textInput.prop('disabled')) {
                    return false;
                }

                textInput.val(dateText).blur().change().focus();

                // The dirty bit may not update based on the change() call above.
                // To make sure the page is marked as dirty, call the update method.
                if (typeof updateDirtyBit === 'function') {
                    updateDirtyBit(textInput[0] as HTMLInputElement);
                }
                return true;
            },
            beforeShow(input:Element, picker:any): JQueryUI.DatepickerOptions {
                const thisInput = $(input);
                const textInput = thisInput.prev('input');
                // @ts-ignore
                if (textInput.prop('readonly') || textInput.prop('disabled')) return false;


                const dateInputVal = textInput.val();
                thisInput.datepicker('setDate', !dateInputVal ? dateInputVal : new Date());

                setTimeout(function () {
                    // Position the picker below the icon.
                    picker.dpDiv.css('margin-top', '18px');
                });

                // @ts-ignore
                return true;
            },
        });
    }

    function unifyErrors(){
        const errImg = $(components).find('img[src*="error_pointer.gif"]');
        errImg.each(function (i, img) {
            if (!$(img).next().is('span')) {
                $(img).hide().after('<span class="text-danger">*</span>');
            }
        });
    }
    function unifyLoadingIcons(el?:JQuery){
        /*
        Origin style:
        <loading-icon>Loading text...</loading-icon>
        Format of a styled loading icon:
        <loading-icon id="WaitTable" style="display: none">
            <div class="wrap-loading">
                <div class="loading-content">
                    <img class="img-loading" src="../images/loading.gif">
                    <h3 class="text-loading">Loading text...</h3>
                </div>
            </div>
        </loading-icon>
        */
        const loadIcons = el || $(components).find('loading-icon');
        loadIcons.each(function(i, icon){
            var $loadIcon = $(icon);
            if ($loadIcon.find("img[src*='loading.gif']").length == 0) {
                wrapByLoadingContent($loadIcon);
                $loadIcon.children().wrapAll('<div class="wrap-loading"></div>');
            }
        });
    }

    function wrapByLoadingContent(_$element: JQuery){
        _$element.contents().wrapAll('<h3 class="text-loading"></h3>');
        _$element.prepend('<img class="img-loading" src="' + VRoot + '/images/loading.gif">');
        _$element.children().wrapAll('<div class="loading-content"></div>');
    }

    function unifyLockCheckboxStyle(){
        if (components == null) components = document;
        const lockCheckboxes = $(components).find('.lock-checkbox input[type=checkbox]') as JQuery<HTMLInputElement>;
        lockCheckboxes.each(function (i, lockCb) {
            const lb = $(lockCb).parent();
            lb.toggleClass("checked", lockCb.checked);
            const spanTag = lb.children('.material-icons');
            if (spanTag == null || spanTag.length == 0) {
                $(lockCb).parent().prepend('<span class="material-icons"></span>');
            }
        });
        lockCheckboxes.change(function () {
            $(this).parent().toggleClass("checked", this.checked);
        });
    }

    //add class to parents and row to show style.
    function styleSelectedCheckbox(checkbox:HTMLInputElement){
        $(checkbox).parent().toggleClass("selected-checkbox", checkbox.checked);
        var tRow = $(checkbox).closest('tr');
        if (tRow.length > 0 && location.href.indexOf("TPOFeeEditor.aspx") <= -1 && location.href.indexOf("Loan1003LiabilityEditor.aspx") <= -1) {
            //inside a tr and not in TPOFeeEditor, Loan1003LiabilityEditor pages.
            tRow.toggleClass("selected", (tRow.has("td").length > 0) && checkbox.checked);
        }
    }

    function toggleDisabledCheckbox(checkbox: JQuery<HTMLInputElement>|JQLite, isDisabled:boolean){
        var parent = checkbox.parent();
        if (parent.hasClass('mdc-checkbox')) {
            parent.toggleClass("mdc-checkbox--disabled", isDisabled);
        }
    }
    function unifyChexboxes(){
        /* Format of a styled checkbox
        <div class="mdc-form-field">
            <div class="mdc-checkbox" data-mdc-auto-init="MDCCheckbox">
                <input id="sIsCommunityLending" type="checkbox" name="FannieMaeAddendumSections$sIsCommunityLending" onclick="onClick_sIsCommunityLending();" class="mdc-checkbox__native-control">
                <div class="mdc-checkbox__background"><svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24"><path class="mdc-checkbox__checkmark__path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"></path></svg><div class="mdc-checkbox__mixedmark"></div></div>
            </div>
            <label for="sIsCommunityLending">Community Lending</label>
        </div>
        */
        unifyLockCheckboxStyle();
        const checkboxes = $(components).find(':not(.lock-checkbox,.mdc-checkbox) input[type=checkbox]') as JQuery<HTMLInputElement>;
        checkboxes.each(function(i, checkbox){
            var isHidden = ($(checkbox).css('display') == 'none');
            var isDisabled = $(checkbox).is(':disabled');
            var parent = $(checkbox).parent();
            if(!parent.hasClass('mdc-checkbox')&&!parent.hasClass('lock-checkbox') && $(checkbox).attr('NoUnify')==null){

                $(checkbox).wrap(
                    $(checkbox).attr('NoHover') != null
                        ? '<div class="mdc-form-field"><div class="mdc-checkbox none-hover-checkbox"></div></div>'
                        : '<div class="mdc-form-field"><div class="mdc-checkbox" data-mdc-auto-init="MDCCheckbox"></div></div>');
                $(checkbox).toggleClass('mdc-checkbox__native-control',true);

                var formField = $(checkbox).parent().parent();
                if (isHidden) formField.hide();

                parent = $(checkbox).parent();
                parent.append('<div class="mdc-checkbox__background"><svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24"><path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/></svg><div class="mdc-checkbox__mixedmark"></div></div>');

                toggleDisabledCheckbox($(checkbox) as JQuery<HTMLInputElement>, isDisabled);

                styleSelectedCheckbox(checkbox);

                $(checkbox).change(function (this: HTMLInputElement){
                    if (!this.checked) this.blur();
                    styleSelectedCheckbox(this);
                });
                
                //iOPM 469775. MDC error: temporarily remove activation class when mouse leave the item
                $(checkbox).mouseout(function(){
                    setTimeout(function(){
                        $(checkbox).blur();
                        $(checkbox).parent().removeClass('mdc-ripple-upgraded--foreground-activation');
                    },500);
                });

                var id = $(checkbox).attr('id');
                var noLabel=$(checkbox).attr('NoLabel');
                if(noLabel==null)
                {
                    var value = $(checkbox).attr('value');
                    var labelText = $(checkbox).attr('label');
                    if (labelText != null) value = labelText;
                    if (id != null) {
                        var labels = $('label[for="' + id + '"]');
                        if (labels != null) {
                            for (var i = 0; i < labels.length; i++) {
                                if (!labels.eq(i).hasClass('mdc-checkbox')) {
                                    value = labels.eq(i).html();
                                    labels.eq(i).remove();
                                    break;
                                }
                            }
                        }
                    }

                    if (!!value) {
                        formField.append(`<label ${((id != null) ? ` for="${id}"` : "")}>${value}</label>`);
                    }
                }
            }
            else{
                if(parent.hasClass('mdc-checkbox')){
                    if(isHidden) $(checkbox).parent().parent().hide();
                    else $(checkbox).parent().parent().show();

                    $(checkbox).parent().toggleClass("mdc-checkbox--disabled",isDisabled);
                }
            }
        });
    }

    function unifyRadioButtons(){
        /* Format of a styled Radio button
        <div class="mdc-form-field">
            <div class="mdc-radio" data-mdc-auto-init="MDCRadio">
                <input id="DUDownPaymentSource1003" type="radio" name="FannieMaeAddendumSections$DwnSrc" value="DUDownPaymentSource1003" checked="checked" class="mdc-radio__native-control">
                <div class="mdc-radio__background"><div class="mdc-radio__outer-circle"></div><div class="mdc-radio__inner-circle"></div></div>
                </div>
            <label for="DUDownPaymentSource1003">Use 1003 downpayment source</label>
        </div>
        */
        var radioBtns=$(components).find('input[type=radio]');
        radioBtns.each(function(i, radioBtn){
            var isHidden = ($(radioBtn).css('display') == 'none');
            var isDisabled = $(radioBtn).is(':disabled');
            var parent = $(radioBtn).parent();
            if(!parent.hasClass('mdc-radio')){
                var id=$(radioBtn).attr('id');
                $(radioBtn).wrap('<div class="mdc-form-field"><div class="mdc-radio" data-mdc-auto-init="MDCRadio"></div></div>');
                $(radioBtn).toggleClass('mdc-radio__native-control',true);

                var formField=$(radioBtn).parent().parent();
                if(isHidden) formField.hide();

                parent = $(radioBtn).parent();
                parent.append(`<div class="mdc-radio__background">
                    <div class="mdc-radio__outer-circle"></div>
                    <div class="mdc-radio__inner-circle"></div>
                </div>`);

                parent.toggleClass("mdc-radio--disabled",isDisabled);

                //iOPM 469775. MDC error: temporarily remove activation class when mouse leave the item
                $(radioBtn).mouseout(function(){

                    setTimeout(function(){
                        $(radioBtn).blur();
                        $(radioBtn).parent().removeClass('mdc-ripple-upgraded--foreground-activation');
                    },500);
                });

                var id = $(radioBtn).attr('id');
                var noLabel=$(radioBtn).attr('NoLabel');
                if(noLabel==null){
                    var value = $(radioBtn).attr('value');
                    var labelText = $(radioBtn).attr('label');
                    if (labelText != null) value = labelText;
                    if (id != null) {
                        var labels = $(`label[for="${id}"]`);
                        if (labels != null) {
                            for (var i = 0; i < labels.length; i++) {
                                if (!labels.eq(i).hasClass('mdc-radio')) {
                                    value = labels.eq(i).html();
                                    labels.eq(i).remove();
                                    break;
                                }
                            }
                        }
                    }

                    if (!!value ) {
                        formField.append(`<label ${((id != null) ? `for="${id}"` : '')}>${value}</label>`);
                    }
                }
            }
            else{
                $(radioBtn).parent().toggleClass("mdc-radio--disabled",isDisabled);
            }
        });

        //https://material-components-web.appspot.com/radio.html
        var MDCFormField = window.mdc.formField.MDCFormField;
        var MDCRadio     = window.mdc.radio.MDCRadio;

        var formFields = document.querySelectorAll('.mdc-form-field');
        for (const formField of formFields) {
            if (formField == null) continue;
            var radio = formField.querySelector('.mdc-radio:not([data-demo-no-js])');
            if (radio) {
                var radioInstance = new MDCRadio(radio);
                var formFieldInstance = new MDCFormField(formField);
                formFieldInstance.input = radioInstance;
            }
        }
    }

    function unifySelectboxes(){
        $(components).find('select').each(function(i, select) {
            const $select = $(select);
            if (!$select.parent().hasClass('select-box')) {
                const isHidden = ($select.css('display') == 'none');
                if (!isHidden) $select.wrap('<div class="select-box select-box-noimage"></div>');
            }
        });
    }

    function unifyDiclosureIcons(){
        setTimeout(function(){
            $(document).find('img[src*="warn.png"').each(function(i, ele){
                const ico = $(ele);
                const isHidden = (ico.css('display') == 'none');
                if (!isHidden) {
                    ico.hide().after('<i class="material-icons">&#xE000;</i>');
                }
            });

            $(document).find('img[src*="fail.png"').each(function(i, ele) {
                const ico = $(ele);
                var isHidden = (ico.css('display') == 'none');
                if (!isHidden) {
                    ico.hide().after('<i class="material-icons red">&#xE147;</i>');
                }
            });
        });
    }

    function displayComponent(component:HTMLElement, isDisplay:boolean){
        var isHidden = ($(component).css('display') == 'none');
        var formField = $(component).parent().parent();
        if (formField.hasClass("mdc-form-field")) {
            if (isHidden) formField.hide();
            else formField.show();
        }
    }

    function setButtonEvents(){
        $(components).find('button').on("mouseup",function(){
            if ($(this).is(':focus')) {
                $(this).blur();
            }
        });
    }
    function unifyComponents($components?: JQuery|Element|Document, forceScroll?: boolean){
        components = $components == null ? document : $components;

        setTimeout(function(){
            unifyChexboxes();
            unifyRadioButtons();
            unifyComboboxes();
            unifyErrors();
            unifyCalendars();
            unifySelectboxes();
            unifyInputs();
            unifyLoadingIcons();
            allTabStyles();
            setButtonEvents();
            window.mdc.autoInit();
            $('[data-toggle="tooltip"]', $components).tooltip();
			bootstrapLoanStaticTabs();
            if (components == document || forceScroll != null) {
                checkScrollbar(forceScroll);
            }
        });
        return components;
    }

    // 458992 - add padding for .content-tabs  so it not cover by .static-tabs
    function bootstrapLoanStaticTabs(){
        const tab = $("[loan-static-tab]"); if (tab.length < 1) return;
        const staticTab = tab.find(".static-tabs")[0];
        const contentDiv = tab.find(".content-tabs")[0];

        if (staticTab == null) { debugger; return; }
        console.count("bootstrapLoanStaticTabs (This should only be call 1 time per frame)");
        let ticking = false;
        function updatePadding() {
            console.count("update loan-static-tab .content-tabs padding");
            contentDiv.style.paddingTop = staticTab.offsetHeight+"px";
            ticking = false;
        }
        function onResize(){
            if (!ticking) requestAnimationFrame(updatePadding);
            ticking = true;
        }
        function onScroll(this: HTMLElement){
            $(staticTab).toggleClass("static-tabs-scroll-down", $(this).scrollTop() != 0);
        }

        window.addEventListener("resize", onResize, passiveSupported ? { passive: true } : false as any);
        document.addEventListener("scroll", onScroll, passiveSupported ? { passive: true } : false as any);
        updatePadding();
    }

    function allTabStyles(){
        var tabs = $(components).find('.nav-tabs-panels a');
        tabs.each(function () {
            var padding = 16;
            var maxWidth = 320;
            var text = $(this).text();
            var w16 = getTextWidth(text, '16px "Helvetica Neue",Helvetica,Arial,sans-serif') + 2 * padding;
            $(this).toggleClass("tab-break-word", w16 >= maxWidth);
            var w14 = getTextWidth(text, '14px "Helvetica Neue",Helvetica,Arial,sans-serif') + 2 * padding;

            var shouldBreakManually = (w16 >= maxWidth && w14 <= maxWidth);
            if (window.location.href.indexOf("EditUser.aspx") > -1 && text.length >= 'Broker Relationships'.length) {
                //Handle special tabs in Edit User dialog. Dialog has fixed width 1115px.
                //Broker Relationships; Correstpondent Relationships; Mini-correstpondent Relationships
                $(this).toggleClass("tab-break-word",true);
                shouldBreakManually = true;
            } else if (window.location.href.indexOf("Loan1003.aspx")){
                if ($(this).hasClass("tab-break-word")){
                    var tabElement = $(this)[0];
                    var width = tabElement.offsetWidth;
                    var height = tabElement.offsetHeight;
                    for (var w = width; w; w--) {
                        tabElement.style.width = w + 'px';
                        if (tabElement.offsetHeight !== height) break;
                    }
                    tabElement.style.width = (w + 1) + 'px';
                }
            }

            if (shouldBreakManually) {
            // Rules of line break:
            // 1. last line must be the longest line that still shorter than the previous line.
                var prevLine = text;
                var nextLine = "";
                var n = prevLine.lastIndexOf(" ");

                while (n >= 0 && (nextLine == "" || prevLine.slice(0, n).length > (prevLine.slice(n).length + nextLine.length))) {
                    nextLine = prevLine.slice(n) + nextLine;
                    prevLine = prevLine.slice(0, n);
                    n = prevLine.lastIndexOf(" ");
                }

                $(this).html(prevLine + "<br/>" + nextLine);
            }
        });
    }

    function unifyTabs(){
        $(document).find('ul li a').each(function(i, ele){
            $(ele).parent().toggleClass('active', $(ele).hasClass('selected'));
        });
    }

    function tabClick(tab:HTMLElement){
        $(document).find('ul li').removeClass('active');
        $(tab).parent().addClass('active');
    }

    function getPasswordGuidelines(passwordGuidelines:string){
        var [header, content] = passwordGuidelines.split(":&lt;&#x2F;b&gt;");
        return {
            content : $("<div/>").html(content).text(),
            header  : header.replace("&lt;b&gt;",""),
        };
    }
    function checkScrollbar(forceScroll?:boolean ){
        setTimeout(function(){
            const hasScroll = (forceScroll != null) ? forceScroll : hasScrollbar();

            $('.static-tabs-inner').toggleClass('hasScroll',hasScroll);
            $('.content-tabs').toggleClass('hasScroll',hasScroll);
            $('.warp-section').toggleClass('hasScroll',hasScroll);
        });
    }
    function hasScrollbar() {
        var hContent = $(document).height()!; // get the height of your content
        var hWindow = $(window).height()!;  // get the height of the visitor's browser window
        // if the height of your content is bigger than the height of the
        // browser window, we have a scroll bar
        return (hContent > hWindow);
    }

    $(function() {
        $("accordion").each((i, accordion) => {
            let collapse = false;

            const $accordion = $(accordion);
            const header = $accordion.find("accordion-header");
            const content = $accordion.find("accordion-content");
            const indicator = $(`<i class="material-icons">&#xE5CF;</i>`);

            header.wrapInner("<span></span>");
            header.prepend(indicator);

            const render = () => {
                // indicator.html(collapse ? "&#xE5CC;" : "&#xE5CF;");
                $accordion.toggleClass("accordion-collapse", collapse);
                console.log("collapse", collapse);
                content.collapse(!collapse ? "show" : "hide");
            };
            header.on("click.Accordion", () => {
                collapse = !collapse;
                render();
            });
            render();
        });
    });

    function fixedHeaderTable(table:HTMLTableElement){
        if (table == null) return;

        const thead_tr = table.querySelector<HTMLTableRowElement>("thead>tr")!;

        const thead_tds = Array.from(thead_tr.querySelectorAll<HTMLTableCellElement>("td,th"));
        const tbody_tr = Array.from(table.querySelectorAll<HTMLTableRowElement>("tbody>tr")).find((tr) => tr.offsetParent !== null);

        let n = 10;
        if (tbody_tr != null) setTimeout(function setWidth(){
            const tds = Array.from(tbody_tr.querySelectorAll<HTMLTableCellElement>("td,th"));
            const bs = tds.slice(0,-1).map((td, i) => {
                const th = thead_tds[i];
                if (td.clientWidth > 0) {
                    th.style.width = td.clientWidth + "px";
                    return true;
                } else {
                    return false;
                }
            });
            if (bs.some(b => !b) && (n--) > 0) {
                setTimeout(setWidth, 300);
            } else {
                table.classList.add("fixed-header-table");
            }
        }); else setTimeout(() => fixedHeaderTable(table));
    }

    function refactorTaskHistory(historyEle: HTMLElement){
        if (historyEle == null) return;

        [
            historyEle.querySelector("meta"),
            historyEle.querySelector("style"),
            historyEle.querySelector("head"),
        ].forEach((e) => !!e && e.remove());

        const histSecs:HTMLElement[][] = [[]];
        let histRows: HTMLElement[] = histSecs[0];
        Array.from(historyEle.childNodes).forEach(function(node){
            const ele = node as HTMLElement;
            if (ele.tagName == "BR") {
                histRows = [];
                histSecs.push(histRows);
            } else {
                histRows.push(ele);
            }
        });

        const histSecStrings = histSecs.map(nodes2string)
            .filter(s => !!s)
            ;
        const histData = histSecStrings.reduce<string[][]>((histArray, x) => {
            if (histArray.length < 1 || x.startsWith("<strong>")) {
                return histArray.concat([[x]]);
            }

            const last = histArray[histArray.length - 1];
            return histArray.slice(0, -1).concat([last.concat([x])]);
        }, []);
        historyEle.innerHTML = histData.map((histSecItems) =>
            `<div class="hist-sec">${histSecItems.map(histItem =>
                `<div class="hist-row">${histItem}</div>`).join("")
            }</div>`).join("");

        function nodes2string(xs: HTMLElement[]) {
            const d = document.createElement("div");
            xs.forEach(x => d.appendChild(x));
            return d.innerHTML.trim();
        }

        // const xs = historyEle.innerHTML.split("<br>")
        //     .map(s => s.trim())
        //     .filter(s => !!s)
        //     ;
        // const ys = xs.reduce((gs, x) => {
        //     if (gs.length < 1 || x.startsWith("<strong>")) {
        //         return gs.concat([[x]]);
        //     }
        //     else {
        //         var last = gs[gs.length - 1];
        //         return gs.slice(0, -1).concat([last.concat([x])]);
        //     }
        // }, []);
        // historyEle.innerHTML = ys.map((xs) => `<div class="hist-sec">${xs.map(x => `<div class="hist-row">${x}</div>`).join("")}</div>`).join("");
    }

    return {
        SortIcons             : unifySortIconsStyle,
        TableSorterStyle      : unifyTableSorterStyle,
        Components            : unifyComponents,
        DisplayComponent      : displayComponent,
        CheckScrollbar        : checkScrollbar,
        InputFileChange       : onInputFileChange,
        TabPages              : unifyTabs,
        TabClick              : tabClick,
        UnifyDiclosureIcons   : unifyDiclosureIcons,
        UnifyLoadingIcons     : unifyLoadingIcons,
        GetPasswordGuidelines : getPasswordGuidelines,
        ToggleDisabledCheckbox: toggleDisabledCheckbox,
        UnifyTemplateCalendars: unifyCalendars,
        wrapByLoadingContent  : wrapByLoadingContent,
        fixedHeaderTable      : fixedHeaderTable,
        refactorTaskHistory   : refactorTaskHistory,
    };
})(jQuery);

declare global {
    interface Window {
        TPOStyleUnification: typeof TPOStyleUnification;
    }
}
window.TPOStyleUnification = TPOStyleUnification;
export {TPOStyleUnification};
