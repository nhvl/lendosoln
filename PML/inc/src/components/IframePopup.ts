/* Usage:
check function __usage() below
When minification, the __usage function will be removed on dead code elimination.
*/
function __usage() {
    // in parent
    // const iframePopup = window.iframePopup = new IframePopup(); iframePopup.show(...)
    IframePopup.show<{}>("path/to/your/popup.aspx", {}).then(([error, result]) => {
        if (error != null) {
            console.error(error);
            simpleDialog.alert("ERROR: " + error);
        } else {
            if (result == null) {
                // may be user cancel
            } else {
                console.log("process ", result);
            }
        }
    });
    // or

    // in iframe popup
    const iframeSelf = new IframeSelf();
    const onCancel = () => {
        iframeSelf.resolve<number>(-1);
    };
    const onOk = () => {
        iframeSelf.resolve<number>(42);
    };
}

const $ = jQuery;
import { TPOStyleUnification } from "./TPOStyleUnification";
declare const copyStringToClipboard: any;

declare global {
    interface Window {
        iframePopup: IframePopup;
    }
    // interface JQuery<TElement extends Node = HTMLElement> {
    //     draggable(): void;
    //     draggable(s:"enable"|"disable"): void;
    //     resizable(o:{ minHeight:number, minWidth:number }):void;
    //     resizable(s: "enable" | "disable"): void;
    // }
}

interface ISettings {
    width          : number|null,//450,
    height         : number|null,//350,
    closeText      : string,
    modifyOverflow : boolean,//true,
    className      : string|null,
    headerText     : string|null,
    buttons        : null,
    backdrop       : boolean|"static",
    draggable      : boolean,
    resizable      : boolean,
    minHeight      : number,// use for resizable = true
    minWidth       : number,// use for resizable  = true
    [key:string]   : any;
}

export class LqbPopup {
    private element: HTMLElement;
    private $popup : JQuery<HTMLElement>;
    private props  : ISettings;

    constructor(element: HTMLElement) {
        this.element = element;

        this.$popup = $(element);
        this.$popup.addClass("modal").attr("role", "dialog");
        const children = $(element.childNodes); children.detach();
        this.$popup.html(
            `<div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>`
        ).find(".modal-content").append(children);

        this.props = Object.assign(LqbPopup.defaultSettings);

        this.$popup.on('hide.bs.modal', (event: Event) => {
            if (this.onRequestClose != null) {
                if (!!this.onRequestClose()) {
                    event.preventDefault();
                    return false;
                }
            }
            return true;
        });
        this.$popup.on('hidden.bs.modal', function () {
            const p = $(this);
            p.find('.modal-dialog').width('');
            p.find('.modal-body').height('');
        });
    }

    show(settings:ISettings|null) {
        this.props = Object.assign({}, LqbPopup.defaultSettings);
        if (settings != null) this.updateSettings(settings, true);
        this.$popup.modal("show");
    }
    hide() {
        // if (isShown) return;

        this.$popup.modal("hide");
        // isShown = false;
        // $popup.removeClass(popupClasses);
    }

    updateSettings(settings:Partial<ISettings>, reset = false){
        const props     = this.props;
        const nextProps = this.props = Object.assign({}, this.props, settings);

        Object.keys(props).filter(p => reset || props[p] != nextProps[p]).forEach(p => {
            switch(p) {
                case "draggable":
                    // https://api.jqueryui.com/draggable/
                    const modalDialog = this.$popup.find('.modal-dialog');
                    if (!modalDialog.data('ui-draggable')) {
                        if (!!nextProps.draggable) modalDialog.draggable();
                    } else {
                        modalDialog.draggable(nextProps.draggable ? "enable" : "disable");
                    }
                    break;
                case "resizable":
                    const modalContent = this.$popup.find('.modal-content');
                    if (!modalContent.data('ui-resizable')) {
                        if (!!nextProps.resizable) {
                            modalContent.resizable({
                                minHeight: nextProps.minHeight,
                                minWidth : nextProps.minWidth,
                            })
                        }
                    } else {
                        modalContent.resizable(nextProps.resizable ? "enable" : "disable");
                    }
                    this.$popup.on('show.bs.modal', function () {
                        $(this).find('.modal-body').css({ 'max-height': '100%' });
                    });
                    break;
                case "hideCloseButton":
                    console.warn("deprecate");
                    break;
                case "className":
                    this.$popup.removeClass(props.className!).addClass(nextProps.className!);
                    break;
                case "height":
                    const height = (nextProps.height != null)
                        ? clamp(nextProps.height, nextProps.minHeight, window.innerHeight - 140)
                        : "";
                    this.$popup.find('.modal-body').css({height});
                    break;
                case "width":
                    const width = (nextProps.width != null)
                        ? clamp(nextProps.width, nextProps.minwidth, window.innerWidth - 140)
                        : "";
                    this.$popup.find('.modal-dialog').css({width});
                    break;
                case "backdrop":
                    this.$popup.modal({
                        backdrop: this.props.backdrop,
                        keyboard: false,
                        show: false,
                    });
            }
        });
    }

    onRequestClose: () => boolean;

    static defaultSettings :ISettings = {
        width          : null,//450,
        height         : null,//350,
        closeText      : 'Close',
        hideCloseButton: false,
        modifyOverflow : false,//true,
        className      : null,
        headerText     : null,
        buttons        : null,
        backdrop       : 'static',
        draggable      : false,
        resizable      : false,
        minHeight      : 300,//use for resizable = true
        minWidth       : 300//use for resizable  = true
    };
}

export class IframePopup {
    private resolve: ((<T>(v:([string|null, T])) => void)|null);
    private wrapIframe: HTMLDivElement|null;
    private overflowY:string;

    constructor() {
    }

    show<T>(url:string, settings: Partial<ISettings>):Promise<[string|null, T]> {
        this.closePopup();

        this.wrapIframe = string2Dom<HTMLDivElement>(
            `<div class="wrap-iframe" hidden>
                <div class="modal-backdrop in modal-stack"></div>
                <iframe src="${url}"></iframe>
            </div>`);

        const frame = this.wrapIframe.querySelector("iframe");
        if (frame == null) return Promise.resolve<[string | null, T]>(["iframe not found", null as any]);

        frame.addEventListener("load", (e) => {
            frame.contentWindow.postMessage(({lqbPopup:{settings}}), "*");
            this.wrapIframe!.hidden = false;
        });

        document.body.appendChild(this.wrapIframe);
        this.overflowY = document.body.style.overflowY!;
        document.body.style.overflowY = "hidden";

        return new Promise<[string|null, T]>((resolve, reject) => { this.resolve = resolve as any; });
    }

    _return<T>(v:T|null) {
        if (this.resolve != null) {
            this.resolve([null, v]);
            this.resolve = null;
        }
        this.closePopup();
    }

    private closePopup() {
        if (this.wrapIframe != null) this.wrapIframe.remove();
        this.wrapIframe = null;
        document.body.style.overflowY = this.overflowY;
    }

    static singelton: IframePopup;
    static show<T>(url:string, settings: Partial<ISettings>) {
        const iframePopup: IframePopup = (window.iframePopup == null) ? (window.iframePopup = new IframePopup()) : window.iframePopup;
        return iframePopup.show<T>(url, settings);
    }
}

export class IframeSelf {
    constructor() {
        window.addEventListener("message", this.receiveMessage, false);
    }

    onShow: (settings:ISettings) => boolean; // return true to prevent show
    _show(settings: ISettings){
        if (this.onShow != null && !!this.onShow(settings)) return;

        document.body.style.backgroundColor="rgba(0,0,0,0)";

        if (this.popup == null) return;
        this.popup.show(settings);
        this.popup.onRequestClose = () => {
            this.resolve(null);
            return true;
        };
    }

    popup: LqbPopup;
    private receiveMessage = (event: MessageEvent) => {
        const {lqbPopup} = event.data;
        if (lqbPopup == null) return;

        const {settings} = lqbPopup;
        this._show(settings);
    };

    resolve<T>(result:T) {
        const iframePopup:IframePopup = window.parent.iframePopup;
        if (iframePopup == null) { debugger; return; }
        iframePopup._return<T>(result);
    }
}

export class SimpleDialog {
    private root: HTMLDivElement;
    private popup: LqbPopup;
    private modalHeader: HTMLDivElement;
    private modalBody: HTMLDivElement;
    private modalFooter: HTMLDivElement;
    private modalTitle: HTMLHeadingElement;
    private options: {label:string, value:any}[];
    private resolve: (((v:any) => void) | null);
    private defaultValue:any;
    constructor() {
        this.resolve = null;
    }

    private init(){
        this.root = string2Dom<HTMLDivElement>(`<div lqb-popup class="modal-auto-width">
            <div class="modal-header d-flex">
                <h4 class="modal-title flex-modal-title"></h4>
                <button type="button" class="close" aria-label="Close"><span aria-hidden="true"><i class="material-icons">&#xE5CD;</i></span></button>
            </div>
            <div class="modal-body min-width-300"></div>
            <div class="modal-footer"></div>
        </div>`);
        document.body.appendChild(this.root);
        this.popup = new LqbPopup(this.root);

        this.modalHeader = this.root.querySelector<HTMLDivElement>(".modal-header")!;
        this.modalBody   = this.root.querySelector<HTMLDivElement>(".modal-body")!;
        this.modalFooter = this.root.querySelector<HTMLDivElement>(".modal-footer")!;
        this.modalTitle  = this.modalHeader.querySelector<HTMLHeadingElement>(".modal-title")!;

        this.modalFooter.addEventListener("click", (event:MouseEvent) => {
            const button= event.target as HTMLButtonElement;
            try {
                const {index} = button.dataset;
                if (this.resolve == null || this.options == null) { debugger; return; }
                this.resolve(this.options[Number(index)].value);
                this.popup.hide();
            }
            catch(e) {
                debugger;
                console.error(e);
            }
        });
        $("button", this.modalHeader).on("click", () => {
            if (this.resolve != null) this.resolve(this.defaultValue);
            this.popup.hide();
        });
    }

    alert(message:string, title:string = "", btnLabel="OK") {
        return this.show(message, title, [{label:btnLabel, value:true}], null);
    }

    alertWorkflow(message: string, title: string = "", btnLabel = "OK") {
        var messageHtml = '<div class="prewrap">' + message + '</div>';
        let alertWorkflowPromise = this.show(messageHtml, title, [{ label: btnLabel, value: true }, { label: 'Copy and Close', value: null }], null);

        alertWorkflowPromise.then(function (value) {
            if (value == null) {
                var messageDiv = $("<div></div>").html(message);
                var messageText = messageDiv.text();
                copyStringToClipboard(messageText);
            }
        });

        return alertWorkflowPromise;
    }
    
    multilineAlert(messages: string[], title: string = "", btnLabel = "OK") {
        let messageListHtml: string = "";
        messageListHtml += messages.map((message, i) => `<div>${message}</div>` ).join("");

        return this.show(messageListHtml, title, [{ label: btnLabel, value: true }], null);
    }
    confirm<T=boolean>(message:string, title:string = "", labelYes="OK", labelNo="Cancel", closeValue:T|boolean = false) {
        return this.show<boolean|T>(message, title, [
            {label:labelNo , value:false},
            {label:labelYes, value:true },
        ], closeValue);
    }
    ync(message:string, title:string = "", labelYes="Yes", labelNo="No", labelCancel="Cancel") {
        return this.show<boolean|null>(message, title, [
            {label:labelYes   , value:true },
            {label:labelNo    , value:false},
            {label:labelCancel, value:null }
        ], null);
    }
    show<T = any>(htmlContent:string, htmlTitle:string, buttons:{label:string, value:any}[], defaultValue:any = null) {
        if (this.popup == null) this.init();
        this.options = buttons;
        this.defaultValue = defaultValue;

        this.modalHeader.hidden = !htmlTitle;
        this.modalTitle.innerHTML = htmlTitle || "";

        this.modalBody.innerHTML = htmlContent;
        this.modalFooter.innerHTML =
            buttons.map(({label}, i) =>
                `<button class="btn btn-flat" type="button" data-index="${i}">${label}</button>`
            ).join("");

		this.popup.show({
			backdrop: "static",
		} as ISettings);

        return new Promise<T>((resolve) => this.resolve = resolve);
    }
}
export const simpleDialog = new SimpleDialog();
// const d = new SimpleDialog();
// await d.alert(message, title="", btnLabel="OK")
// const isConfirm: boolean        = await d.confirm(message, title, labelYes="OK", labelNo="Cancel")
// const isConfirm: (boolean|null) = await d.ync    (message, title, labelYes="Yes", labelNo="No", labelCancel="Cancel")
// const value = await d.show(html, htmlTitle, {label, value}[])

// https://stackoverflow.com/a/24914782
{
    $(document).on('show.bs.modal', '.modal', function(this:HTMLElement) {
        if (this.style.display == "block"
            || (!this.style.display && this.classList.contains("in"))
        ) return;

        const zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    $(document).on('hidden.bs.modal', '.modal', function () {
        if ($('.modal:visible').length > 0)
            $(document.body).addClass('modal-open');
    });
}

function string2Dom<T=HTMLElement>(html:string) {
    const div = document.createElement("div");
    div.innerHTML = html;
    return div.firstChild as any as T;
}
function clamp(v: number, min:number, max:number) {
    return Math.min(max||Number.POSITIVE_INFINITY, Math.max(min||Number.NEGATIVE_INFINITY, v));
}
