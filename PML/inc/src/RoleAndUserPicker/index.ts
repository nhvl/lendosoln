import * as angular from "angular";
import {LqbPopup, IframeSelf} from "../components/IframePopup";
import { TPOStyleUnification } from "../components/TPOStyleUnification";

declare const RoleAndUserPickerViewModel: {
    LoanID           : string;
    IsPipeline       : boolean,
    PermissionLevelID: number,
    SkipSecurityCheck: boolean,
    roleListOptions  : {label: string, value: string}[];
};

const $ = jQuery;

interface IUser {
    UserId  : string;
    FullName: string;
    Roles   : string;
}
import roleAndUserPickerHtml from "./index.html";
import { postJsonSafeD } from "../utils/fetch";
class RoleAndUserPickerController {
    static $inject = ["$element", "$timeout"];

    private users                : IUser[];
    private searchText:string = "";
    private disabledSearch:boolean = false;
    private IsPipeline = RoleAndUserPickerViewModel.IsPipeline;
    private roleListOptions = RoleAndUserPickerViewModel.roleListOptions;

    private sortKey:string;
    private sortDesc:boolean;
    private onSort(sortKey: string) {
        if (sortKey == this.sortKey) {
            this.sortDesc = !this.sortDesc;
        } else {
            this.sortKey = sortKey;
            this.sortDesc = false;
        }
    }

    private iframeSelf: IframeSelf;

    constructor(private $element:JQuery, private $timeout: angular.ITimeoutService) {
        {
            const popup = new LqbPopup(document.querySelector<HTMLDivElement>("div[lqb-popup]")!);
            const iframeSelf = this.iframeSelf = new IframeSelf();
            iframeSelf.popup = popup;
        }

        setTimeout(() => {
            $element.find('input[autofocus]').focus();
        }, 300);
        this.onSort("FullName");

        if (!RoleAndUserPickerViewModel.IsPipeline) this.search();
    }

    private search(event?: Event) {
        this.searchText = this.searchText.trim();
        if (!RoleAndUserPickerViewModel.IsPipeline || !!this.searchText) {
            this._search();
        }

        if (event != null) event.preventDefault();
        return false;
    }
    private async _search(){
        this.disabledSearch = true;

        const [err, d] = await postJsonSafeD<any>(`${location.pathname}/Search`, {
            text             : this.searchText,
            IsPipeline       : RoleAndUserPickerViewModel.IsPipeline,
            LoanID           : RoleAndUserPickerViewModel.LoanID,
            SkipSecurityCheck: RoleAndUserPickerViewModel.SkipSecurityCheck,
            PermissionLevelID: RoleAndUserPickerViewModel.PermissionLevelID,
        });
        this.$timeout(() => {
            this.disabledSearch = false;
            if (err != null) return;

            this.users = d;

            if (d.length) {
                setTimeout(() => {
                    TPOStyleUnification.fixedHeaderTable(this.$element.find("table.modal-table-scrollbar")[0] as HTMLTableElement);
                });
            }
        });
    }

    private selectUser(user:IUser){
        this.iframeSelf.resolve({ id: user.UserId, type: "user", name: user.FullName, OK: true });
        return false;
    }
    private closeDialog(){
        this.iframeSelf.resolve(null);
        return false;
    }
}
function roleAndUserPickerDirective(): angular.IDirective {
    return {
        restrict     : "EA",
        controller   : RoleAndUserPickerController,
        controllerAs : "vm",
        template     : roleAndUserPickerHtml,
    };
}

angular.module("RoleAndUserPicker", ["TpoCommon"])
.directive("roleAndUserPicker", roleAndUserPickerDirective)
;
