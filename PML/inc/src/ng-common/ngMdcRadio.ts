import {IDirective, IAugmentedJQuery, IAttributes} from "angular";

let idCounter = 0;


ngMdcRadioDirective.ngName = "ngMdcRadio";
export function ngMdcRadioDirective(): IDirective {
    return {
        restrict: "EA",
        replace: true,
        template(element:IAugmentedJQuery, attributes:IAttributes){
            const name = attributes.name; // element.attr("name");

            const label = attributes.label; //element.attr("label");
            let forId = attributes.forId; // element.attr("for-id");
            if (!forId && !!label) forId = `ngMdcRadio_${idCounter++}`;

            const template =
            `<div class="mdc-radio" ng-class="{'mdc-checkbox--disabled': ${attributes.ngDisabled}}">
                <input class="mdc-radio__native-control" type="radio"
                    ${!name                         ? "" :        `name="${name}"`                 }
                    ${!forId                        ? "" :          `id="${forId}"`                }
                    ${attributes.ngModel    == null ? "" :    `ng-model="${attributes.ngModel}"`   }
                    ${attributes.ngValue    == null ? "" :    `ng-value="${attributes.ngValue}"`   }
                    ${attributes.ngDisabled == null ? "" : `ng-disabled="${attributes.ngDisabled}"`}
                    ${attributes.required   == null ? "" : "required"                              }
                    ng-change="${attributes.ngChange == null ? "" : attributes.ngChange};$event.target.blur()"
                >
                <div class="mdc-radio__background">
                    <div class="mdc-radio__outer-circle"></div>
                    <div class="mdc-radio__inner-circle"></div>
                </div>
            </div>`;

            return !label ? template :
            `<div class="mdc-form-field">
                ${template}
                <label for="${forId}">${label}</label>
            </div>`;
        },
        link(scope, element, attrs){
            const ele = element.hasClass("mdc-radio") ? element[0] : element.find(".mdc-radio")[0];

            const radioBtn = new mdc.radio.MDCRadio(ele);

            //iOPM 469775. MDC error: temporarily remove activation class when mouse leave the item
            element.on("mouseout", (event) => {
                setTimeout(function(){
                    radioBtn.nativeControl_.blur();
                    radioBtn.foundation_.adapter_.removeClass('mdc-ripple-upgraded--foreground-activation');
                },500);
            });
        },
      };
}

