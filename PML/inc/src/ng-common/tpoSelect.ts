/*
Usage:

```html
<tpo-select
    ng-model="vm.x"
    tpo-options="o.lable as o.value for o in vm.options"
    ng-required="vm.isRequired"
    ng-change="vm.onXChanged()"
    ng-attr-size="{{5}}"
    name="inputName"
    required
    disabled
    readonly
    multiple
></tpo-select>
```

*/

import {IDirective, IAugmentedJQuery, IAttributes} from "angular";

tpoSelectDirective.ngName = "tpoSelect";
export function tpoSelectDirective(): IDirective {
    return {
        restrict: "EA",
        replace: true,
        template(element:IAugmentedJQuery, attributes:IAttributes){

            const {
                ngModel, ngRequired, ngChange, tpoOptions, ngAttrSize,
                name, multiple, required, readonly, disabled
            } = attributes; // element.attr("name");

            "ng-model ng-required ng-change tpo-options ng-attr-size name required multiple".split(" ").forEach(attr => element.removeAttr(attr));

            return (
            `<div class="select-box select-box-noimage">
                <select
                    ${ ngModel    == null ? "" : `ng-model="${ngModel}"` }
                    ${ ngRequired == null ? "" : `ng-required="${ngRequired}"` }
                    ${ ngChange   == null ? "" : `ng-change="${ngChange}"` }
                    ${ tpoOptions == null ? "" : `ng-options="${tpoOptions}"` }
                    ${ ngAttrSize == null ? "" : `ng-attr-size="${ngAttrSize}"`}

                    ${ name       == null ? "" : `name="${name}"`}
                    ${ required   == null ? "" : "required"}
                    ${ readonly   == null ? "" : "readonly"}
                    ${ disabled   == null ? "" : "disabled"}
                    ${ multiple   == null ? "" : "multiple"}
                >${element.html()}</select>
            </div>`
            );
        },
    };
}

