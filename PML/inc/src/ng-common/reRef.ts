/*
Usage:

```html
<div re-ref="vm.ele = $element; vm.ctrl = $controller"></div>
```
*/

import { IDirective, IParseService, IRootScopeService, IDirectiveCompileFn, IDirectiveLinkFn } from "angular";

reRefDirective.ngName = "reRef";
reRefDirective.$inject = ['$parse', '$rootScope'];
export function reRefDirective($parse: IParseService, $rootScope: IRootScopeService): IDirective {
    return {
        restrict: 'A',
        scope: false,
        compile: (($element, attr): IDirectiveLinkFn => {
            // copy from ngEventDirectives
            const fn = $parse(attr.reRef);
            return (scope, element, attrs, $controller) => {
                const callback = function() {
                    fn(scope, { $element: element[0], $controller });
                };
                if ($rootScope.$$phase) scope.$evalAsync(callback);
                else                    scope.$apply    (callback);
            };
        }) as IDirectiveCompileFn,
    };
}
