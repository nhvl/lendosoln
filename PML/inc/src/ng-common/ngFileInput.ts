import {IDirective, IAugmentedJQuery, IAttributes, ITimeoutService, IController} from "angular";

let idCounter = 0;

class FileInputController implements IController {
    private fileId: string;
    private fileName:string;
    private file:HTMLInputElement;

    static $inject=["$timeout", "$element"];
    constructor(private $timeout:ITimeoutService, private $element:JQuery) {
        // let {fileId} = attributes;
        // if (fileId == null) fileId = `ngFileInput_${idCounter++}`
        this.fileId = `ngFileInput_${idCounter++}`;
    }
    $onInit(){
        this.fileName = "";
        this.setFile(this.$element.find("input")[0] as HTMLInputElement);
    }

    private setFile(file:HTMLInputElement) {
        this.file = file;
        this.file.addEventListener("change", this.onFileChange);
        this.setRef({$file: file, $ctrl:this});
    }
    $onDestroy(){
        this.file.removeEventListener("change", this.onFileChange);
    }

    private onFileChange = (event: Event) => {
        this.$timeout(() => {
            this.fileName = this.file.value.split("\\").pop()!;
            if (this.onChange)
            this.onChange({ $file:this.file, $ctrl:this, $event: event });
        });
    }

    private setRef   : (_:{$file:HTMLInputElement, $ctrl:FileInputController              }) => void;
    private onChange : (_:{$file:HTMLInputElement, $ctrl:FileInputController, $event:Event}) => void;
}

ngFileInput.ngName = "ngFileInput";
export function ngFileInput(): IDirective {
    return {
        restrict: "EA",
        replace: true,
        template(element:IAugmentedJQuery, attributes:IAttributes){
            const {
                accept, required,
            } = attributes;

            return `<div>
                <input type="file" class="inputfile" id="{{vm.fileId}}"
                    ${accept   == null ? "" : `accept="${accept}"`}
                    ${required == null ? "" : `required`}>
                <label for="{{vm.fileId}}"><strong>Choose file</strong><span>{{vm.fileName || "No file chosen"}}</span></label>
            </div>`;
        },
        controller: FileInputController,
        controllerAs:"vm",
        scope:{
            setRef  : "&",
            onChange: "&",
        },
        bindToController:true,
      };
}
