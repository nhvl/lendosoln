/* Usage:

```html
<i sort-icon="vm.sortDesc"></i>
```

*/

import { IDirective } from "angular";

export function sortIcon(): IDirective {
    return {
        restrict    : 'EA',
        replace     : true,
        scope       : {
            sortIcon: "=",
        },
        template    : `<i class="sort material-icons" ng-bind="sortIcon ? '&#xE5CF;': '&#xE5CE;'"></i>`,
    }
}