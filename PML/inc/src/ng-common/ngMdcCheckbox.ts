import {IDirective, IAugmentedJQuery, IAttributes} from "angular";

let idCounter = 0;

ngMdcCheckboxDirective.ngName = "ngMdcCheckbox";
export function ngMdcCheckboxDirective(): IDirective {
    return {
        restrict: "EA",
        replace: true,
        template(element:IAugmentedJQuery, attributes:IAttributes){
            const {
                name, label, required,
                ngDisabled, ngModel, ngChange,
            } = attributes;

            let {forId} = attributes;
            if (!forId && !!label) forId = `ngMdcCheckbox_${idCounter++}`;

            element.addClass("mdc-checkbox");
            if (ngDisabled != null) element.attr("ng-class", `{'mdc-checkbox--disabled': ${ngDisabled}}`);
            const template = `<div class="mdc-checkbox" ${ngDisabled == null ? "" : `ng-class="{'mdc-checkbox--disabled':${ngDisabled}}"`}>
                <input type="checkbox" class="mdc-checkbox__native-control"
                    ${!name ? "" : `name="${name}"`}
                    ${!forId ? "" : `id="${forId}"`}

                    ${ngModel    == null ? "" :    `ng-model="${ngModel}"`   }
                    ${ngChange   == null ? "" :   `ng-change="${ngChange};$event.target.blur()"`  }
                    ${ngDisabled == null ? "" : `ng-disabled="${ngDisabled}"`}
                    ${required   == null ? "" : "required"}
                    />
                <div class="mdc-checkbox__background">
                    <svg class="mdc-checkbox__checkmark"
                        viewBox="0 0 24 24">
                        <path class="mdc-checkbox__checkmark-path"
                            fill="none"
                            stroke="white"
                            d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                    </svg>
                    <div class="mdc-checkbox__mixedmark"></div>
                </div>
            </div>`;

            return !label ? template :
                `<div class="mdc-form-field">
                    ${template}
                    <label for="${forId}">${label}</label>
                </div>`;
        },
        link(scope, element, attrs){
            const ele = element.hasClass("mdc-checkbox") ? element[0] : element.find(".mdc-checkbox")[0];

            const checkbox = new mdc.checkbox.MDCCheckbox(ele);
            element.on("click", (event) => {
                if (!checkbox.checked) {
                    checkbox.nativeCb_.blur();
                }
            });

            //iOPM 469775. MDC error: temporarily remove activation class when mouse leave the item
            element.on("mouseout", (event) => {
                setTimeout(function(){
                    checkbox.nativeCb_.blur();
                    checkbox.foundation_.adapter_.removeClass('mdc-ripple-upgraded--foreground-activation');
                },500);
            });

            // scope.$watch(attrs.ngDisabled)
        },
      };
}
