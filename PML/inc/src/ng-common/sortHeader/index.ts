/*

Usage:

```js
class Ctrl {
    sortKey :string;
    sortDesc:boolean;

    onSort(key:string); // optional - will be called when onSort is click
}
```

```html
<a sort-header sort-key="ConditionCategory" pvm="vm" active-class="text-bold">Category</a>
```
with `vm` is instance of `Ctrl`

*/

import {IDirective} from "angular";
import template from "./index.html";

class SortHeaderController {
    private sortKey:string;
    private pvm    :any;

    private onClick() {
        if (this.pvm.onSort != null) {
            this.pvm.onSort(this.sortKey);
            return;
        }

        if (this.sortKey == this.pvm.sortKey) {
            this.pvm.sortDesc = !this.pvm.sortDesc;
        } else {
            this.pvm.sortKey = this.sortKey;
            this.pvm.sortDesc = false;
        }
    }
}
sortHeaderDirective.ngName = "sortHeader";
export function sortHeaderDirective(): IDirective {
    return {
        restrict        : 'EA',
        // replace      : true,
        transclude      : true,

        scope           : {
            sortKey     : "@",
            pvm         : "=",
            className   : "@",
            activeClass : "@",
        },
        bindToController: true,

        controller      : SortHeaderController,
        controllerAs    : "vm",
        template,
    }
}
