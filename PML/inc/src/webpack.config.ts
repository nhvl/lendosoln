import * as path from "path";
import {fromPairs} from "lodash";

import * as webpack from "webpack";
const {ModuleConcatenationPlugin, UglifyJsPlugin} = webpack.optimize;
const {DefinePlugin, IgnorePlugin, ProvidePlugin, LoaderOptionsPlugin, ContextReplacementPlugin} = webpack;
// import * as CompressionPlugin from "compression-webpack-plugin";
import {CheckerPlugin} from "awesome-typescript-loader";

// Minify
// babel-minify-webpack-plugin babili-webpack-plugin webpack-closure-compiler
// clean-css cssnano
// posthtml

// const ExtractTextPlugin = require("extract-text-webpack-plugin");
// import {compress as brotliCompress} from 'iltorb';
// import {BundleAnalyzerPlugin} from 'webpack-bundle-analyzer';

const useBabel = false;
const useAwesomeTypescriptLoader = false;

const entryFolders = [
  "app",
  "EditUser",
  "Loan1003",
  "Loan1003.page2",
  "ServiceCredential",
];

module.exports = (env:{production?:boolean} = {}) :webpack.Configuration  => {
  const __DEV__ = env.production !== true || process.argv.slice(2).includes("--watch");
  // process.env.npm_lifecycle_event
  console.log("__DEV__ = ", __DEV__);

  const plugins: webpack.Plugin[] = (
    [
      new DefinePlugin({
        __DEV__: JSON.stringify(__DEV__),
        "process.env.NODE_ENV": JSON.stringify(__DEV__ ? "development" : "production"),
      }),

      new IgnorePlugin(/^\.\/locale$/, /moment$/),  // http://stackoverflow.com/a/25426019/1101570
      // new ContextReplacementPlugin(/moment[\/\\]locale/, /(en-gb|en-us)\.js/),

      !useAwesomeTypescriptLoader ? null as any : new CheckerPlugin(), // check at https://github.com/s-panferov/awesome-typescript-loader - `CheckerPlugin` is optional. Use it if you want async error reporting. We need this plugin to detect a `--watch` mode. It may be removed later after https://github.com/webpack/webpack/issues/3460 will be resolved.
    ]
    .concat(__DEV__ ? [] : [
      new ModuleConcatenationPlugin(),

      new UglifyJsPlugin({ sourceMap:false, parallel:true, comments:false }),

      // new CompressionPlugin({ // https://github.com/webpack-contrib/compression-webpack-plugin
      //   asset: "[path].gz[query]",
      //   algorithm: "gzip",
      //   test: /\.js$|\.html$/,
      //   threshold: 10240,
      //   minRatio: 0.8
      // }),
      // new CompressionPlugin({
      //   asset: "[path].br[query]",
      //   algorithm(buf, options, callback) {
      //     brotliCompress(buf, {
      //       mode    : 1, // 0 = generic, 1 = text, 2 = font (WOFF2)
      //       quality : 11, // 0 - 11
      //       lgwin   : 22, // window size
      //       lgblock : 0 // block size
      //     }, callback);
      //   },
      // }),
    ])
    .filter(p => p != null)
  );

  const awesomeTypescriptLoader = ({
    loader: 'awesome-typescript-loader',
    options: {
      transpileOnly: !__DEV__,
      forceIsolatedModules: true,
      useCache: true,
    }
  });
  const tsLoader = ({
    loader: 'ts-loader',
    options: {
      transpileOnly: !__DEV__,
      onlyCompileBundledFiles: true,
    }
  });
  const babelLoader = ({
    loader: 'babel-loader',
    options: {
      presets: [
        "@babel/preset-env",
        "@babel/preset-stage-3",
        "@babel/preset-typescript",
        // { modules: false, },
      ],
      // plugins: [],
    }
  });

  const module: webpack.NewModule = {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules|bower_components)/,
        use:(
          useBabel                   ? babelLoader : (
          useAwesomeTypescriptLoader ? awesomeTypescriptLoader : (
                                       tsLoader
        ))),
        // include: [
        //   `${__dirname}/src`,
        // ],
      },

      // {issuer: /\.js$/, use: 'style-loader'}
      { test: /\.css$/, use: ["style-loader", {loader:"css-loader", options:{minimize:true}}] },
      // { test: /\.scss$/, use: ["style-loader", "css-loader", "sass-loader"] },

      { test: /\.html$/, use: [
        { loader: 'html-loader', options: {
          exportAsEs6Default: true,
          removeAttributeQuotes: false, attrs: false,
          ignoreCustomFragments: [/\{\{.*?}}/],
        } },
      ] },
      { test: /\.svg$/, use: "html-loader" },
    ],
  };

  const resolve: webpack.Resolve = {
    modules: [ // https://webpack.js.org/configuration/resolve/#resolve-modules
      "node_modules",
      path.resolve(__dirname)
      // path.resolve(__dirname, './'),
    ],
    extensions: ['.ts', '.tsx', '.js', '.jsx'], // https://webpack.js.org/configuration/resolve/#resolve-extensions
  };

  return {
    entry: fromPairs(entryFolders.map(e => [
      e.split("/").join("."),
      `${__dirname}/${e}/app.ts`
    ])),
    output: {
      path           : path.resolve(`${__dirname}/../`),
      filename       : `[name].js`,
      // publicPath     : `/js/`
      pathinfo: __DEV__,
    },
    context: __dirname,
    devtool: __DEV__ ? "source-map" : false, // https://webpack.js.org/configuration/devtool/
    // bail: !__DEV__, // https://webpack.js.org/configuration/other-options/#bail
    module,
  resolve,
    target: "web", // https://webpack.js.org/configuration/target/
    plugins,
    // @ts-ignore

  };
};

//
// https://medium.com/webpack/unambiguous-webpack-config-with-typescript-8519def2cac7
