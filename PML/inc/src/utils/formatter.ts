export function formatSsn(ssn: string): string {
    const val = ssn.replace(/\D/g, '');
    const newVal =
        (val.length < 4)                       ? val : (
        ((3 < val.length) && (val.length < 6)) ? val.substr(0, 3) + '-' + val.substr(3) : (
        (5 < val.length)                       ? val.substr(0, 3) + '-' + val.substr(3, 2) + '-' + val.substr(5) : (
                                                 val
        )));
    return newVal.substring(0, 11);
}