import 'whatwg-fetch';
import { getUrlWithQueryString } from "./getUrlWithQueryString";

export interface IOptions {
    // method?: "GET" | "POST" | "PUT" | "DELETE";
    // url?: string;
    // data?: any;

    headers?: {
        [header: string]: string;
    };
    basicAuth?: { username: string, password: string };
}

export function getJson<T>(url: string, data?: any, options?: IOptions): Promise<T> {
    return fetchJson("GET", url, data, options);
}

export function postJson<T>(url: string, data: any, options?: IOptions): Promise<T> {
    return fetchJson("POST", url, data, options);
}
export function postJsonSafe<T>(url: string, data: any, options?: IOptions): Promise<[Error | null, T]> {
    return postJson<T>(url, data, options).then(res => [null, res] as [Error | null, T], err => [err, null as any] as [Error | null, T]);
}
export function postJsonSafeD<T>(url: string, data: any, options?: IOptions): Promise<[Error | null, T]> {
    return postJson<{d:T}>(url, data, options).then(res => [null, res.d] as [Error | null, T], err => [err, null as any] as [Error | null, T]);
}

export function fetchJson<T>(
    method: "GET" | "POST" | "PUT" | "DELETE",
    url: string,
    data?: any, options?: IOptions
): Promise<T> {
    const headers = getHeaders(options);

    if (method == "GET" && data != null) {
        url = getUrlWithQueryString(url, data);
    }

    return (
        fetch(url, {
            method,
            credentials: 'include',
            headers,
            body: method == "GET" ? undefined : JSON.stringify(data),
        })
        .then(res => res.json() as any)
    );
}

export function uploadFile<T>(
    url: string,
    data: { [name:string]:File|Blob|string },
    options?: IOptions
): Promise<T> {
    const headers = Object.assign(getHeaders(options), {'Accept': undefined, 'Content-Type': undefined});
    delete headers["Accept"];
    delete headers["Content-Type"];

    const fd =
        Object.entries(data).reduce((fd, [name,value]) => {
            fd.append(name, value);
            return fd;
        }, new FormData());

    return (
        fetch(url, {
            method: "POST",
            credentials: 'include',
            headers,
            body: fd,
        })
        .then(res => res.json() as any)
    );
}

function getHeaders(options?: IOptions) {
    options = Object.assign({ basicAuth: null, token: null, header: {} }, options) as IOptions;
    const { basicAuth } = options;

    return Object.assign(
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        (basicAuth == null ? {} : {
            "Authorization": `Basic ${btoa(`${basicAuth.username}:${basicAuth.password}`)}`,
        }),
        options.headers
    );
}
