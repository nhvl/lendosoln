export function generateUrl({url, base, qs}:{url:string, base?:string, qs:{[key:string]:string|number}}) {
    if (base == null) base = location.href;
    const u = new URL(url, base);
    Object.entries(qs).forEach(([key, value]) => {
        u.searchParams.set(key, value.toString())
    });
    return u.toString();
}

export function getQueryStringFromSearch(query: string = location.search): { [key: string]: string } {
    // https://stackoverflow.com/a/3855394
    if (!query) return {};

    return (/^[?#]/.test(query) ? query.slice(1) : query)
        .split('&')
        .reduce<{ [key: string]: string }>((params, param) => {
            const [key, value] = param.split('=');
            params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
            return params;
        }, {});
}
