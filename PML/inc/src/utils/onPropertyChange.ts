
export function onPropertyChange(o:Node, handler:(propertyName:string, target:Node) => void) {
    if (typeof MutationObserver != "undefined") {
        new MutationObserver(
            ([record]: MutationRecord[], observer: MutationObserver) => handler(record.attributeName!, record.target)
        ).observe(o, {
            attributes: true,
            // @ts-ignore
            attributeName: true,
            subtree: true
        });
        return;
    }
    if ("attachEvent" in o) {
        // @ts-ignore
        o.attachEvent("onpropertychange", () => handler(event.propertyName, event.srcElement));
        return;
    }
    throw new Error("Not support");
}
