export type StrGuid = string;
export const GUID_REGEXP =
    /^[{(]?[0-9A-F]{8}[-]?([0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$/i
    // /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i
    ;
export const guidEmpty = "00000000-0000-0000-0000-000000000000";
export function isGuid(u:string) {
    return (u === guidEmpty) || GUID_REGEXP.test(u);
}
export function isGuidEmpty(u:string|null) {
    return (!u || (u === guidEmpty));
}
