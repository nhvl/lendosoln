﻿export function download(filename: string, text: string, mime:string="text/plain") {
    if (navigator.msSaveBlob != null) {
        navigator.msSaveBlob(new Blob([text]), filename);
        return;
    }

    const uriContent = `data:${mime};charset=utf-8,${encodeURIComponent(text)}`;

    const element = document.createElement("a");
    element.setAttribute("href", uriContent);
    element.setAttribute("download", filename);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}
