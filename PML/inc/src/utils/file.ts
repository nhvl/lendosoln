const textType = /text.*/;

export function readFileAsText(fileInput: HTMLInputElement) {
    return new Promise<[string|null, string|null]>(resolve => {
        if (fileInput.files == null) {
            resolve(["Empty file.", null]);
            return;
        }

        const file = fileInput.files[0];

        // if (!file.type.match(textType)) {
        //     resolve(["File not supported!", null]);
        //     return;
        // }

        const reader = new FileReader();
        reader.onload = function(e) {
            resolve([null, reader.result]);
        };
        reader.onerror = function(error) {
            resolve([error.toString(), null]);
        }
        reader.readAsText(file);
    });
}

export function readFileAsArrayBuffer(fileInput: HTMLInputElement) {
    return new Promise<[string|null, Uint8Array|null]>(resolve => {
        if (fileInput.files == null) {
            resolve(["Empty file.", null]);
            return;
        }

        const file = fileInput.files[0];
        const fileData = new Blob([file]);

        const reader = new FileReader();
        reader.onload = function() {
          resolve([null, new Uint8Array(reader.result)]);
        };
        reader.onerror = function (error) {
            resolve([error.toString(), null]);
        }
        reader.readAsArrayBuffer(fileInput.files[0]);
    });
}

export function readFileAsBase64(fileInput: HTMLInputElement) {
    return new Promise<[string|null, string|null]>(resolve => {
        if (fileInput.files == null || fileInput.files.length < 1) {
            resolve(["Empty file.", null]);
            return;
        }
        const reader = new FileReader();
        reader.readAsDataURL(fileInput.files[0]);
        reader.onload = function() {
          resolve([null, reader.result.split(",")[1]]);
        };
        reader.onerror = function (error) {
            resolve([error as any, null]);
        };
    });
}

const iframe = document.createElement("iframe");
iframe.hidden = true;
export function saveFileFromUrl(filePath:string){
    return new Promise<void>((resolve, reject) => {
        if (document.body.contains(iframe)) document.body.removeChild(iframe);
        iframe.src = filePath;
        document.body.appendChild(iframe);

        const isIE = /(msie)/i.exec(navigator.userAgent);
        if (isIE) {
            iframe.addEventListener("readystatechange", function handler(e){
                if (iframe.contentDocument.readyState == "interactive") {
                    resolve();
                    iframe.removeEventListener("readystatechange", handler);
                    document.body.removeChild(iframe);
                }
            });
        } else {
            setTimeout(()=> {
                resolve();
                // document.body.removeChild(iframe);
            }, 1000); // just wait a second if it's not IE
        }
    });
}
