export let passiveSupported = false;
let onceSupported = false;

try {
    const testOption = {};
    Object.defineProperty(testOption, "passive", {
        get: function () {
            passiveSupported = true;
        },
    });
    Object.defineProperty(testOption, "once", {
        get: function () {
            onceSupported = true;
        },
    });
    window.addEventListener("test", null as any, testOption);
} catch (err) { }

export const eventOnce = onceSupported ? eventOnceModern : eventOnceLegacy;

function eventOnceModern<TEvent = Event>(element:HTMLElement, event:string, passive=false) {
    return new Promise<TEvent>((resolve) => {
        element.addEventListener(event, function(e){ resolve(e as any) }, { once:true, passive } as any);
    });
}

function eventOnceLegacy<TEvent = Event>(element:HTMLElement, event:string, passive=false) {
    return new Promise<TEvent>((resolve) => {
        element.addEventListener(event, handler as any);
        function handler(e:TEvent) {
            resolve(e);
            element.removeEventListener(event, handler as any);
        }
    });
}
