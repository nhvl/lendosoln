export enum E_ReportExtentScopeT {
	Default = 0,
	Assign = 1,
	Branch = 2,
	Broker = 3,
	SpecificLoans = 4,
	Team = 5
}
