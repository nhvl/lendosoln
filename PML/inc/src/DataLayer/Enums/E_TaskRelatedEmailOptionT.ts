export enum E_TaskRelatedEmailOptionT {
    ReceiveEmail = 0,
    DontReceiveEmail = 1
}
