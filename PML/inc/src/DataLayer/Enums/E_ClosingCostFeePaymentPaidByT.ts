export enum E_ClosingCostFeePaymentPaidByT {
    LeaveBlank      = 0,
    Borrower        = 1,
    Seller          = 2,
    BorrowerFinance = 3,
    Lender          = 4,
    Broker          = 5,
    Other           = 6,
}
