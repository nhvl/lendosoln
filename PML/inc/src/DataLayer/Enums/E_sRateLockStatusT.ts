export enum E_sRateLockStatusT {
    NotLocked = 0,
    Locked = 1,
    LockRequested = 2,
    LockSuspended = 3
}
