export enum E_ClassType
{
	Invalid = 0, // invalid
	Txt = 1, // text
	Pct = 2, // percent
	Cnt = 3, // count
	Csh = 4, // cash
	Cal = 5, // calendar
	LongText = 6 // TEXT in DB.
}