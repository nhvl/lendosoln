export enum E_PortalMode
{
    Blank             = 0,
    Broker            = 1,
    MiniCorrespondent = 2,
    Correspondent     = 3,
    Retail            = 4,
}

