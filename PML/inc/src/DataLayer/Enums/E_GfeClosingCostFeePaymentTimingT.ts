export enum E_GfeClosingCostFeePaymentTimingT {
    LeaveBlank       = 0,
    AtClosing        = 1,
    OutsideOfClosing = 2,
}
