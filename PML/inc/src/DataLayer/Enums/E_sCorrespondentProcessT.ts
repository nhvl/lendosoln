export enum E_sCorrespondentProcessT {
    Blank             = 0,
    Delegated         = 1,
    PriorApproved     = 2,
    MiniCorrespondent = 3,
    Bulk              = 4,
    MiniBulk          = 5,
}
