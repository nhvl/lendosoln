/// <reference types="angular" />
declare global {
    interface Function {
        ngName:string;
    }
}

import * as angular from 'angular';
declare module 'angular' {
    interface IAngularEvent {
        currentTarget : EventTarget;
        delegateTarget: EventTarget;
        originalEvent : Event;
    }

    interface IComponentOptions {
        isolate?: boolean;
        ngName ?: string,
    }
}

