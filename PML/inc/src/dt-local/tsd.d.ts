/// <reference path="./promise.d.ts" />
/// <reference path="./angularjs/index.d.ts" />
/// <reference path="./console.d.ts" />
/// <reference path="./HTMLInputElement.d.ts" />
/// <reference path="./tpo.d.ts" />
/// <reference path="./material-components-web/index.d.ts" />

declare module "*.html" {
    const content: string;
    export default content;
}

declare module "*.json" {
    const data: any;
    export = data;
}

declare const __DEV__: boolean;
