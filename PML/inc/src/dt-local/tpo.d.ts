type StrGuid = string;

declare const ML: {
    LoanId                            : StrGuid,
    ApplicationId                     : StrGuid,
    VirtualRoot                       : string,
    CanRequestRedisclosure            : boolean,
	CanRequestInitialClosingDisclosure: boolean,
	IsAntiSteeringEnabled			  : boolean,
    StepOneEventSource                : any,
    StepTwoEventSource                : any;
    StepThreeEventSource              : any;
	StepFourEventSource				  : any;
	StepFiveEventSource				  : any;

    DisplayQmPage                     : boolean;

    isSaveButtonEnabled               : boolean;

    EnableNewLoanNavigation           : boolean;

    sIsIncomeCollectionEnabled        : boolean;

    main1003Url:string;
};

declare const VRoot: string;
declare const gVirtualRoot: string;

// file:///C:/LendOSoln/PML/inc/mask.js
declare function time_onminuteblur(o: HTMLInputElement): void;
declare function time_onhourkeyup(o: HTMLInputElement, m: string): void;
declare function time_onhourblur(o: HTMLInputElement): void;

// file:///C:/LendOSoln/LendersOfficeApp/inc/common.js
declare function updateDirtyBit(input?: HTMLInputElement): void;
declare function isDirty(): boolean;
declare function clearDirty(): void;

declare function date_keyup(): void;
declare function f_displayBlockMask(b: boolean): void;
declare function displayCalendar(id:string): void;
declare function getAllFormValues(clientId:string|null, skipIgnore?:boolean):{[key:string]:string};
declare function populateForm(obj:{[key:string]:string}, clientID:string|null) : void;
declare function smartZipcode(zipcode: HTMLInputElement, city: HTMLInputElement, state: HTMLInputElement, county?: HTMLInputElement, event?:Event):void;
declare function _initInput():void;
declare function isReadOnly():boolean;
declare function normalSaveCallback(resultMessage:string): string;
declare function promptExplicitSave(isSaveButtonEnabled:boolean, wasDirty:boolean, saveFunction:any, navigationFunction:any): void;
declare function promptExplicitSaveCallback(resultMessage:string, navigationFunction:any): void;
declare const _postGetAllFormValuesCallbacks: Function[];
declare const _postRefreshCalculationCallbacks: Function[];
//

declare function showModal(href:string, a?:string|null, feature?:string|null, forceEdge?:boolean|null, callback?:Function|null, lqbPopupSettings?:{}|null):any;

declare function attemptSave        (b: boolean): string|null; // file:///C:/LendOSoln/PML/inc/src/Loan1003/app.ts
declare function Save               (b?: boolean): string|null;
declare function onPmlUnload        (): string|null; // file:///C:/LendOSoln/PML/inc/webapp/pml.js
declare function f_genericFramework (sLId: string, providerId: string, location: string): void; // file:///C:/LendOSoln/PML/inc/genericframework.js

declare function createCombobox(id:string, items:string[]):void; // file:///C:/LendOSoln/PML/inc/webapp/combobox.js



interface Window {
    LQBPopup: any;
    Save(): any;

    showEditUser(url:string, options:{}): void;
    iFrameScrollPosition: number;
}
declare const LQBPopup: any;

interface JQuery {
    mask(m:string): JQuery;
}

declare const toastr: any;
