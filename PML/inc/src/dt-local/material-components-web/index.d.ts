import * as mdc from "material-components-web";

declare global {
    interface Window {
        mdc: typeof mdc,
    }
    declare const mdc: typeof mdc;
}