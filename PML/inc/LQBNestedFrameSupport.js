/*global console,setInterval*/
(function (window) {
    "use strict";
    var f = 500, l = null, lw = null;
    function postDocumentHeight() {
        try {
            var document = window.document, body = document.body, html = document.documentElement, height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight),
                width = Math.max(body.scrollWidth, body.offsetWidth, html.clientWidth, html.scrollWidth, html.offsetWidth);
            if (l !== height || lw !== width) {
                l = height;
                lw = width;
                window.parent.postMessage('HEIGHT:' + height, '*');
                window.parent.postMessage('WIDTH:' + width, '*');
            }
        } catch (ignore) { }
    }
    setInterval(postDocumentHeight, f);
    (window.opener || window.parent).postMessage('loaded', '*');
}(this));