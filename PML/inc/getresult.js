var wConfirmDialog = null;
var g_iStartTime = null;
var g_iStopTime = null;
var g_iDownloadTime = null;
var g_bDisplayTime= false;
var g_iTimeoutRenewalCount = 0;
var g_setupRequestMs = null;
var g_iResultPollingCount = 0; 
var oEventArg = '';
var g_sRequestID = '';
var g_iPollingInterval = 2000; // 2 seconds
var g_sNumberOfProducts = "";
var g_isResultAvailable = false;
var g_iStartCapture = 0;
var g_iEndCapture = 0;
var g_iResultAvailableTiming = 0;

if (!window.console) { window.console = { log : function() {} }; };

function f_copyToClipboard(str)
{
// This method is only use internally.
  window.clipboardData.setData( 'Text', str );
}
function f_quickDebug(event)
{
    // ctrl+alt+.
  if (event.ctrlKey && event.altKey && event.which == 190) {
  
    var o = f_calcTiming();

    var msg = '<table style="color:black">';
    msg += '<tr><td colspan=2><b># of programs:</b> ' + o.numOfProducts + '</td></tr>';
    msg += '<tr><td colspan=2><b># of calc servers:</b> ' + o.calcServerCount + '</td></tr>';
    msg += '<tr><td colspan=2><b>Created Time:</b> ' + o.batchCreated + '</td></tr>';
    msg += '<tr><td>&nbsp;</td></tr>';
    msg += '<tr><td colspan=2><b>Server Timing:</b></td></tr>';
    msg += '<tr><td style="padding-left:15px"><b>Server Retrieve LPs:</b></td><td>' + f_round(o.s4) + ' seconds.</td></tr>';
    msg += '<tr><td style="padding-left:15px"><b>Server Pricing (0-6):</b></td><td>' + f_round(o.s2) + ' seconds.</td></tr>';
    msg += '<tr><td style="padding-left:15px"><b>Server IsResultAvailable duration:</b></td><td>' + f_round(o.s5) + ' seconds. PollingCount=' + o.pollingCount + '.</td></tr>';
    
    msg += '<tr><td style="padding-left:15px"><b>Server Misc:</b></td><td>' + f_round(o.s3) + ' seconds.</td></tr>';
    msg += '<tr><td style="padding-left:15px"><b>Render Result:</b></td><td>' + f_round(o.s1) + ' seconds.</td></tr>';
    msg += '<tr><td>&nbsp;</td></tr>';
    msg += '<tr><td colspan=2><b>Client Timing:</b></td></tr>';
    msg += '<tr><td style="padding-left:15px"><b>Download:</b></td><td>' + f_round(o.c1) + ' seconds.</td></tr>';
    msg += '<tr><td style="padding-left:15px"><b>Render:</b></td><td>' + f_round(o.c0) + ' seconds.</td></tr>';
    msg += '<tr><td>&nbsp;</td></tr>';
    msg += '<tr><td><b>TOTAL:</b></td><td>' + f_round(o.total) + ' seconds.</td></tr>';
    msg += '<tr><td><b>% in server:</b></td><td>' + f_round(o.percent_server) + '%</td></tr>';
    msg += '<tr><td><b>% in client:</b></td><td>' + f_round(o.percent_client) + '%</td></tr>';
    msg += '<tr><td colspan=2><b>Comment (Require for email to support)</b></td></tr>';
    msg += '<tr><td colspan=2><textarea id=TimingDebugComment style="width:300px;height:50px"></textarea></td></tr>';
    msg += '<tr><td>&nbsp;</td></tr><tr><td align=center colspan=2>[<a href=# onclick=f_closeTiming();>Close</a>]&nbsp;&nbsp;[<a href=# onclick=f_emailTiming();>Email to support</a>]</td></tr></table>';

    $j('#DebugTimingPanel').html(msg).css({ 'width': '400px', 'height': '550px', 'top': '100px', 'left': '100px','overflow':'scroll' }).show();

  }
}
function f_emailTiming()
{
    var comment = $j('#TimingDebugComment').val();
    if (comment == '')
    {
    alert('Comment is required for email to support.');
    return;
    }
  var result = f_emailTimingImpl(comment);
  if (!result.error) {
    f_closeTiming();
  } else {
    alert('Unable to send email. Please try again.');
  }
}
function f_closeTiming() {
    $j('#DebugTimingPanel').hide();
}
function f_round(o) {
  return Math.round(o * 1000) / 1000;
}
function f_displayErrorMessage(errorMessage) {
  console.log('Got an error: ' + errorMessage);
  if (typeof(f_displayError) != 'undefined') {
    f_displayError( errorMessage );
  }
  else 
  {
    document.getElementById("ErrorMessagePanel").style.display = "";
    f_hideWaiting();
    $j("#ErrorMessage").text(errorMessage);
  }
}
function f_rerun1stLoan() {
  if( !f_allowClick() ) return false;
  window.close();
}
function f_L(id, cb) {
    if (!f_allowClick()) return false;
    $j('#M_' + id).hide();
    $j('#L_' + id).show();
  //document.getElementById("M_" + id).style.display = 'none';
  //document.getElementById("L_" + id).style.display = '';
  cb.checked = true;
}
function f_M(id, cb, index) {
  if (!f_allowClick()) return false;
  
  var $oTBodyLess = $j("#L_" + id);
  var $oTBody = $j("#M_" + id);
  $oTBody.addClass($oTBodyLess.children().eq(0).prop('class'));
  //oTBody.className = oTBodyLess.childNodes(0).className;
  if (id.match(/_e0|_i0/))
    f_buildHiddenRates0($oTBody, gData0[index], id, index);
  else
    f_buildHiddenRates1($oTBody, gData1[index], id, index);

$oTBody.show();
$oTBodyLess.hide();
  //oTBody.style.display = '';
  //oTBodyLess.style.display = 'none';
  cb.checked = false;
  
} 
function f_Underwriting2Cb(result){
  if (!result.error) {    
    // OPM 44950 m.p.
    if (typeof (result.value["ErrorMessage"]) != "undefined") {
      f_hideWaiting();
      document.getElementById("MainPanel").style.display = "none";
      if (document.getElementById("NavigationPanel") != null) {
          document.getElementById("NavigationPanel").style.display = "";
      }
      if (typeof (result.value["ShowErrorReasons"]) != "undefined") {
          var errorReasons = document.getElementById("ErrorReasons");
          if (errorReasons != null) {
            errorReasons.style.display = "";
          }
      }
      f_displayErrorMessage(result.value["ErrorMessage"]);
      return;
  }
    
    g_sNumberOfProducts = result.value["ProductCount"];
    g_sRequestID = result.value["RequestID"];

    if (parent.PML != null)
    {
        parent.PML.setSessionStorage("ClientPriceTiming_RequestId", g_sRequestID);
        parent.PML.setSessionStorage("ClientPriceTiming_ServerPolling", result.value["ServerPolling"]);
    }

    if (result.value["IsAvailable"] == "1") {
        setTimeout("f_PostbackRenderResult();", 100);
        return;
    } else {
        g_isResultAvailable = false;
        f_reportTimeout();
        alert(g_sUpdateMessage);
        window.location = "about:Blank"; 
        return;
    }
    
  }
}

function f_Underwriting2(sLienQualifyModeT, extraArgs) {
 
  g_iStartTime = (new Date()).getTime();    
  if (typeof(f_disableStepLinks) == 'function') f_disableStepLinks(); // OPM 14208
  var args = {};
  args["loanid"] = g_loanID;
  args["sLienQualifyModeT"] = sLienQualifyModeT;
  args["renderResultModeT"] = g_renderResultModeT;

  if (parent.PML != null) {
      parent.PML.setSessionStorage("ClientPriceTiming_BeforeSubmit", (new Date()).getTime());
  }

  if (extraArgs != null)
  {
      for (var p in extraArgs)
      {
          if (extraArgs.hasOwnProperty(p))
          {
              args[p] = extraArgs[p];
          }
      }
  }
  g_iResultAvailableTiming = 0;

  var result = gService.main.acall("GetResult_SubmitUnderwritingByGrouping", args, f_Underwriting2Cb);
  
}
function f_Underwriting(sLienQualifyModeT, extraArgs) {
    if (window.parent && window.parent.PML) {
        f_Underwriting2(sLienQualifyModeT, extraArgs);
        return;
    }
  g_iStartTime = (new Date()).getTime();    
  if (typeof(f_disableStepLinks) == 'function') f_disableStepLinks(); // OPM 14208
  var args = {};
  args["loanid"] = g_loanID;
  args["sLienQualifyModeT"] = sLienQualifyModeT;
  args["renderResultModeT"] = g_renderResultModeT;

  if (extraArgs != null)
  {
      for (var p in extraArgs)
      {
          if (extraArgs.hasOwnProperty(p))
          {
              args[p] = extraArgs[p];
          }
      }
  }
  g_iResultAvailableTiming = 0;

  console.log('Submit to underwriting');
  var result = gService.main.call("GetResult_SubmitUnderwriting", args);
  console.log('Finished with submit to underwriting');
  if (!result.error) {
    // OPM 44950 m.p.
    if (typeof (result.value["ErrorMessage"]) != "undefined") {
      console.log('Got an error!');
      f_hideWaiting();
      document.getElementById("MainPanel").style.display = "none";
      if (document.getElementById("NavigationPanel") != null) {
          document.getElementById("NavigationPanel").style.display = "";
      }
      if (typeof (result.value["ShowErrorReasons"]) != "undefined") {
          var errorReasons = document.getElementById("ErrorReasons");
          if (errorReasons != null) {
            errorReasons.style.display = "";
          }
      }
      f_displayErrorMessage(result.value["ErrorMessage"]);
      return;
  }
    
    g_sNumberOfProducts = result.value["ProductCount"];
    g_sRequestID = result.value["RequestID"];

    if (typeof (Storage) !== "undefined") {
        // Only support client side timing if DOM Storage is enable.
        sessionStorage["ClientPriceTiming_RequestId"] = g_sRequestID;
    }

    if (result.value["IsAvailable"] == "1") {
      setTimeout("f_PostbackRenderResult();", 100);
      return;
    } else {
    g_isResultAvailable = false;
    f_reportTimeout();
    alert(g_sUpdateMessage);

    if (typeof (f_goToStep3) != 'undefined') f_goToStep3();
    else f_goToPipeline(false);
    return;
    }
    
  }
}
function f_IsAvailable() {
  console.log('Polling...');
  g_iResultPollingCount++;
  var args = new Object();
  args["requestid"] = g_sRequestID;
  
  var result = gService.main.call("GetResult_IsAvailable", args);
  if (!result.error) {
    g_iResultAvailableTiming += parseInt(result.value["Duration"]);
    if (result.value["IsAvailable"] == "0") {
      setTimeout("f_IsAvailable();", g_iPollingInterval);
      return;
    }
  }
  f_PostbackRenderResult();
}
function f_PostbackRenderResult()
{
  $j('#ActionCmd').val('Result');
  $j('#RequestID').val(g_sRequestID);
  g_isResultAvailable = true;

  g_iStopTime = (new Date()).getTime();
  g_setupRequestMs = 0;
  document.forms[0]['JsScriptStats'].value = '' + (g_iStopTime - g_iStartTime) + ':' + g_sNumberOfProducts + ':' + g_setupRequestMs + ':' + g_iResultPollingCount + ':' + g_iResultAvailableTiming;

  __doPostBack('', oEventArg);

}
function f_resultTimeout() {
  if (g_isResultAvailable)
    return;

  if (g_iTimeoutRenewalCount < 2) {
    var args = new Object();
    args["requestid"] = g_sRequestID;
  
    var result = gService.main.call("GetResult_ExtendTimeout", args);
    if (!result.error) {
      if (result.value["IsExtendTime"] == "1") {
        g_iTimeoutRenewalCount++;
        setTimeout("f_resultTimeout();", g_iResultTimeout);          
        return;
      }
    }
  }
  f_reportTimeout();
  alert(g_sUpdateMessage);
  
  if ( typeof(f_goToStep3) != 'undefined' ) f_goToStep3();
  else f_goToPipeline( false );
}
function f_minuteWarning() {
  if (g_isResultAvailable)
    return;
        
  var args = new Object();
  args["LoanID"] = g_loanID;
  args["ProductCount"] = g_sNumberOfProducts;      
  gService.main.call("GetResult_MinuteWarning", args);
}    
function f_displayWaiting() {
  f_hideNavigation();
  document.getElementById("WaitTable").style.display = "";
  document.getElementById("MainPanel").style.display = "none";

  if( typeof parent.f_displayEditMask == 'function' )
    parent.f_displayEditMask(true);
}

function f_hideWaiting() {
    document.getElementById("WaitTable").style.display = "none";

    if (typeof parent.f_displayEditMask == 'function')
        parent.f_displayEditMask(false);
}

function f_openHelp(page, w, h)
{
  var url = "/help/" + page;
  if( !f_allowClick() ) return false;
  _popupWindow = showSoloModeless( url , "dialogwidth: " + w + "px; dialogheight: " + h + "px; menu: no; status: yes; location: no; resizable: yes;", "Help");
}
function f_emailTimingImpl(comment) {
  console.log('Email timing info');
  var o = f_calcTiming();
  
  var msg = '# of programs: ' + o.numOfProducts + '\n';
  msg += '# of calc servers: ' + o.calcServerCount + '\n\n';
  msg += 'Created Time: ' + o.batchCreated + '\n\n';
  msg += 'Server Timing\n';
  msg += 'Server Pricing (0-6): ' + f_round(o.s2) + ' seconds.\n';
  msg += 'Server IsResultAvailable duration: ' + f_round(o.s5) + ' seconds. PollingCount=' + o.pollingCount + '.\n';
  msg += 'Server Misc: ' + f_round(o.s3) + ' seconds.\n';
  msg += 'Render Result: ' + f_round(o.s1) + ' seconds.\n';
  msg += '      ' + o.renderResultDebug + '\n\n';
  msg += 'Client Timing\n';
  msg += 'Download: ' + f_round(o.c1) + ' seconds.\n';
  msg += '    end_render - start_capture: ' + f_round((g_iStartCapture - g_endRenderPage) / 1000) + ', start_capture - end_capture:' + f_round((g_iEndCapture - g_iStartCapture) / 1000) +'\n';
  msg += 'Render: ' + f_round(o.c0) + ' seconds.\n';
  msg += '    0-1:' + o.debugClient0 + ', 1-2:' + o.debugClient1 + ', 2-3:' + o.debugClient2 + ', 3-4:' + o.debugClient3 + '.\n';
  msg += 'Total: ' + f_round(o.total) + ' seconds.\n';
  msg += '% in server: ' + f_round(o.percent_server) + '%\n';
  msg += '% in client: ' + f_round(o.percent_client) + '%\n';
  
  var args = new Object();
  args["msg"] = msg;
  args["loanid"] = g_loanID;
  args["comment"] = comment;
  args["detailTransaction"] = g_sensitiveData;
  args["issue"] = o.percent_server > o.percent_client ? 'Server' : 'Client';
  args["total"] = f_round(o.total) + '';
  args["useragent"] = navigator.userAgent;
  args["debugdetail"] = g_debugDetail;
  var result = gService.main.call("EmailTiming", args);
  
  return result;
}
