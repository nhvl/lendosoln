﻿function f_genericFramework(loanId, providerId, location)
{
    var loanData =
    {
        sLId: loanId,
        ProviderID: providerId,
        currentLocation: location
    };
    var response = GetGenericFrameworkWindowData(loanData);
    if (response)
    {
        if (response.Window && response.Window.Url)
        {
            if (response.Window.IsModal) {
                showModal(response.Window.Url, 'generic', 'dialogHeight:' + response.Window.Height + 'px; dialogWidth:' + response.Window.Width + 'px; center: yes; resizable: yes;');
            } else {
                window.open(response.Window.Url, '_blank', 'height=' + response.Window.Height + ', width=' + response.Window.Width + ', resizable=yes, scrollbars=yes');
            }
        }
        else
        {
            alert(response.ErrorMessage || "System error. Please contact us if this happens again.");
        }
    }
}

function GetGenericFrameworkWindowData(loanData) {
    var result;
    $j.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VRoot + '/main/PipelineService.aspx/GenericFrameworkVendorLaunch',
        data: JSON.stringify(loanData),
        dataType: 'json',
        async: false,
        success: function (response) { result = response.d; },
        failure: function (response) { alert("System error. Please contact us if this happens again."); },
        error: function (xhr, textStatus, errorThrown) { alert("System error. Please contact us if this happens again."); }
    });
    return result;
}