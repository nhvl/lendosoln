﻿var VOEOrdering = (function () {
    var _initialize;
    
    jQuery(function ($) {
        var voeSection = $("#secVoe");
        var verifyIncomeEnabled = false;
        var borrowerAuthDocsEnabled = false;
        var isEmploymentPickerBeingUsed;
        var salaryKeyEnabled = false;

        voeSection.find('.EmploymentLink').click(function () {
            document.location = gVirtualRoot + "/webapp/Loan1003.aspx?src=pipeline&p=0&loanid=" + ML.sLId + "#tabs-1";
        });

        voeSection.find('.CheckAllCB').click(function () {
            if (this.checked) {
                var employmentCBs = voeSection.find('.EmploymentCB:not(:checked)');
                employmentCBs.each(function (index) {
                    $(this).prop('checked', true);
                    CheckBorrowerAuthDocsForEmployments(this, false);
                    CheckSalaryKey(this);
                    CheckSelfEmployment(this, false);
                });
            }
            else {
                var employmentCBs = voeSection.find('.EmploymentCB:checked');
                employmentCBs.each(function (index) {
                    $(this).prop('checked', false);
                    CheckBorrowerAuthDocsForEmployments(this, false);
                    CheckSalaryKey(this);
                    CheckSelfEmployment(this, false);
                });
            }

            TPOStyleUnification.Components(voeSection.find('.SelfEmploymentTarget'));
            ToggleOrderBtn();
        });

        voeSection.find('.EmploymentCB').click(function () {
            CheckBorrowerAuthDocsForEmployments(this, false);
            CheckSalaryKey(this);
            CheckSelfEmployment(this, false);
            TPOStyleUnification.Components(voeSection.find('.SelfEmploymentTarget'));
            voeSection.find('.CheckAllCB').prop('checked', voeSection.find('.EmploymentCB:checked').length === voeSection.find('.EmploymentCB').length);
            ToggleOrderBtn();
        });

        voeSection.on('change', '.SectionToggle', function () {
            TogglePickerSection(!isEmploymentPickerBeingUsed);
        });

        voeSection.on('change', '.BorrowerList', function () {
            CheckBorrowerAuthDocsForBorrowers(false);
            ToggleOrderBtn();
        });

        voeSection.on('change', '.LenderServices', function () {
            var previousValue = $(this).data('previous');
            var lenderServiceId = $(this).val();
            $(this).data('previous', lenderServiceId);

            if (!lenderServiceId) {
                OnClearLenderServicesDDL();
                return;
            }

            var data = {
                LenderServiceId: lenderServiceId,
                LoanId: ML.sLId,
            };

            gService.voe.callAsyncSimple("LoadLenderService", data,
                function (result) {
                    if (result.value["Success"].toLowerCase() !== 'true') {
                        simpleDialog.alert(result.value["Errors"], "VOE");
                    } else {
                        var showCreditCard = result.value["AllowPaymentByCreditCard"].toLowerCase() == 'true';
                        var requiresAccountId = result.value["RequiresAccountId"].toLowerCase() == 'true';
                        var usesAccountId = result.value["UsesAccountId"].toLowerCase() === 'true';
                        var verificationType = result.value["VoeVerificationT"];
                        var askForSalaryKey = result.value["AskForSalaryKey"];
                        var useSpecificEmployerRecordSearch = result.value['UseSpecificEmployerRecordSearch'] === 'True';
                        var enforceUseSpecificEmployerRecordSearch = result.value['EnforceUseSpecificEmployerRecordSearch'] === 'True';
                        var voeAllowRequestWithoutEmployment = result.value["VoeAllowRequestWithoutEmployment"].toLowerCase() == 'true';
                        var voeEnforceRequestsWithoutEmployments = result.value["VoeEnforceRequestsWithoutEmployment"].toLowerCase() == 'true';

                        TogglePickerSection(!voeAllowRequestWithoutEmployment, !voeEnforceRequestsWithoutEmployments);

                        voeSection.find('.CreditCardRow').toggle(showCreditCard);
                        ToggleSpecificEmployerRecordSearch({ UseSpecificEmployerRecordSearch: useSpecificEmployerRecordSearch, EnforceUseSpecificEmployerRecordSearch: enforceUseSpecificEmployerRecordSearch });

                        var credentialId = typeof (result.value["ServiceCredentialId"]) === 'undefined' ? null : result.value["ServiceCredentialId"];
                        var serviceCredentialHasAccountId = typeof (result.value["ServiceCredentialHasAccountId"]) === 'undefined' ? false : result.value["ServiceCredentialHasAccountId"].toLowerCase() === 'true';
                        VOXOrderCommon.BindCredentialOptions(voeSection, usesAccountId, requiresAccountId, credentialId, serviceCredentialHasAccountId);

                        voeSection.find('.BorrowerAuthDocsSection').show();
                        borrowerAuthDocsEnabled = true;

                        var salaryKeyPreviouslyEnabled = salaryKeyEnabled;
                        salaryKeyEnabled = askForSalaryKey.toLowerCase() === 'true';
                        if (!salaryKeyEnabled) {
                            SalaryKey.RemoveSalaryKey(null, voeSection.find('.SalaryKeyTable'));
                            voeSection.find('.SalaryKeySection').hide();
                        }
                        else if (!salaryKeyPreviouslyEnabled && salaryKeyEnabled) {
                            voeSection.find('.EmploymentCB:checked').each(function (index, element) {
                                CheckSalaryKey(this);
                            });
                        }

                        var verifyIncomeCb = voeSection.find('.VerifyIncome');
                        var verifyIncomeRow = voeSection.find('.VerifyIncomeRow');
                        var verifyIncomePreviouslyEnabled = verifyIncomeEnabled;
                        verifyIncomeEnabled = verificationType === '1';
                        var verifyIncomePrevious = verifyIncomeCb.is(':checked');
                        if (!verifyIncomeEnabled) {
                            verifyIncomeCb.prop('checked', false);
                            verifyIncomeRow.hide();
                        }
                        else if (!verifyIncomePreviouslyEnabled && verifyIncomeEnabled) {
                            verifyIncomeCb.prop('checked', true);
                            verifyIncomeRow.show();
                        }

                        if (isEmploymentPickerBeingUsed) {
                            var employmentCBs = voeSection.find('.EmploymentCB:checked');
                            employmentCBs.each(function () {
                                CheckBorrowerAuthDocsForEmployments(this, false);
                            });
                        }
                        else {
                            CheckBorrowerAuthDocsForBorrowers(false);
                        }

                        if (verifyIncomePrevious != verifyIncomeCb.is(':checked')) {
                            verifyIncomeCb.change();
                        }
                    }

                    ToggleOrderBtn();
                    voeSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, voeSection.find('.DocumentTable')));
                },
                function (result) {
                    simpleDialog.alert(result.UserMessage, "VOE");
                },
                ML.VOXTimeoutInMilliseconds);
        });

        voeSection.find('.VerifyIncome').change(function () {
            var isVerifyIncome = $(this).is(':checked');
            var table = voeSection.find('.DocumentTable');
            voeSection.find('.EmploymentCB:checked').each(function () {
                if (isVerifyIncome && isEmploymentPickerBeingUsed) {
                    CheckBorrowerAuthDocsForEmployments(this, false);
                }
                else if (!isVerifyIncome && isEmploymentPickerBeingUsed) {
                    var employmentRow = $(this).closest('tr');
                    var isSelfEmployment = employmentRow.find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                    var key = employmentRow.find('.Key').val();

                    var anyMoreChecked = voeSection.find('.EmploymentTable').find('.Key[value="' + key + '"]').filter(function () {
                        var isChecked = $(this).siblings('.EmploymentCB').is(':checked');
                        var isSelfEmployment = $(this).closest('tr').find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                        return isChecked && isSelfEmployment;
                    }).length > 0;

                    if (!anyMoreChecked && AuthorizationDocPicker.ContainsDocPicker(key, table)) {
                        AuthorizationDocPicker.RemoveDocPicker(key, table);
                    }
                }
            });

            if (!isEmploymentPickerBeingUsed) {
                CheckBorrowerAuthDocsForBorrowers(true);
            }

            ToggleOrderBtn();
            voeSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, voeSection.find('.DocumentTable')));
        });

        voeSection.on('click', '.NotificationEmailPicker', function () {
            var url = ML.VirtualRoot + '/webapp/RolodexListV2.aspx?loanid=' + ML.sLId;

            IframePopup.show(url, {}).then(function (pair) {
                var error = pair[0];
                var result = pair[1];
                if (error != null) {
                    simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "Error");
                }
                else if (result) {
                    ToggleNotificationEmail(result.AgentEmail);
                }
            });
        });

        function ToggleNotificationEmail(email) {
            if (typeof (email) === 'string') {
                voeSection.find('.NotificationEmail').val(email);
            }

            voeSection.find('.NotificationEmail').siblings('.RequiredSpan').toggle(email === '');
            ToggleOrderBtn();
        }

        function OnClearLenderServicesDDL() {
            ToggleSpecificEmployerRecordSearch({ UseSpecificEmployerRecordSearch: false, EnforceUseSpecificEmployerRecordSearch: true });
            SalaryKey.RemoveSalaryKey(null, voeSection.find('.SalaryKeyTable'));
            salaryKeyEnabled = false;
            voeSection.find('.SalaryKeySection').hide();

            AuthorizationDocPicker.RemoveDocPicker(null, voeSection.find('.DocumentTable'));
            borrowerAuthDocsEnabled = false;
            voeSection.find('.BorrowerAuthDocsSection').hide();

            voeSection.find('.VerifyIncome').prop('checked', false);
            verifyIncomeEnabled = false;
            ToggleOrderBtn();
        }

        function CheckBorrowerAuthDocsForBorrowers(shouldCheckOrderBtn) {
            if (!borrowerAuthDocsEnabled) {
                return;
            }
            var table = voeSection.find('.DocumentTable');

            var $borrowerDdl = voeSection.find('.BorrowerList');
            var key = $borrowerDdl.val();
            var verifyIncome = voeSection.find('.VerifyIncome').is(':visible :checked');

            if (!key || !verifyIncome) {
                AuthorizationDocPicker.RemoveDocPicker(null, table);
            }
            else {
                var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                if (!containsPicker) {
                    AuthorizationDocPicker.RemoveDocPicker(null, table);
                    var name = $borrowerDdl.find('option:selected').text();
                    var split = key.split('_');
                    var appId = split[0];
                    var ownerType = split[1];

                    var picker = AuthorizationDocPicker.CreateDocPicker(name, appId, ownerType, key);
                    table.append(picker);
                }
            }

            if (shouldCheckOrderBtn) {
                ToggleOrderBtn();
            }

            voeSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, table));
        }

        function CheckBorrowerAuthDocsForEmployments(employmentCB, shouldCheckOrderBtn) {
            if (!borrowerAuthDocsEnabled) {
                return;
            }

            var employmentRow = $(employmentCB).closest('tr');
            var appId = employmentRow.find('.AppId').val();
            var borrowerType = employmentRow.find('.BorrowerType').val();
            var borrowerName = employmentRow.find('.Borrower').text();
            var isSelfEmployment = employmentRow.find('.IsSelfEmployment').text().toLowerCase() === 'yes';
            var verifyIncome = voeSection.find('.VerifyIncome').is(':visible :checked');
            var key = employmentRow.find('.Key').val();
            var table = voeSection.find('.DocumentTable');

            if (employmentCB.checked) {
                // This got checked so if the picker doesn't already exist, add it
                var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                if (!containsPicker && (verifyIncome || isSelfEmployment)) {
                    var picker = AuthorizationDocPicker.CreateDocPicker(borrowerName, appId, borrowerType, key);
                    table.append(picker);
                }
            }
            else {
                // Unchecked, need to see if there are any more that are checked.
                var anyMoreChecked;
                if (verifyIncome) {
                    // If verify income is enabled, all checkboxes are up for grabs.
                    anyMoreChecked = voeSection.find('.EmploymentTable').find('.Key[value="' + key + '"]').siblings('.EmploymentCB:checked').length > 0;
                }
                else {
                    // If not enabled, we only consider the rows that are self employment
                    anyMoreChecked = voeSection.find('.EmploymentTable').find('.Key[value="' + key + '"]').filter(function () {
                        var isChecked = $(this).siblings('.EmploymentCB').is(':checked');
                        var isSelfEmployment = $(this).closest('tr').find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                        return isChecked && isSelfEmployment;
                    }).length > 0;
                }

                if (!anyMoreChecked && AuthorizationDocPicker.ContainsDocPicker(key, table)) {
                    AuthorizationDocPicker.RemoveDocPicker(key, table);
                }
            }

            if (shouldCheckOrderBtn) {
                ToggleOrderBtn();
            }

            voeSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, table));
        }

        function CheckSalaryKey(employmentCb) {
            if (!salaryKeyEnabled) {
                return;
            }

            var employmentRow = $(employmentCb).closest('tr');
            var appId = employmentRow.find('.AppId').val();
            var borrowerType = employmentRow.find('.BorrowerType').val();
            var borrowerName = employmentRow.find('.Borrower').text();
            var key = employmentRow.find('.Key').val();
            var container = voeSection.find('.SalaryKeyTable');

            if (employmentCb.checked) {
                // This was checked. If no salary key, then add it.
                var containsSalaryKey = SalaryKey.ContainsSalaryKey(key, container);
                if (!containsSalaryKey) {
                    var picker = SalaryKey.CreateSalaryKeyRow(borrowerName, appId, borrowerType, key);
                    container.append(picker);
                }
            }
            else {
                var anyMoreChecked = voeSection.find('.EmploymentTable').find('.Key[value="' + key + '"]').siblings('.EmploymentCB:checked').length > 0;
                if (!anyMoreChecked && SalaryKey.ContainsSalaryKey(key, container)) {
                    SalaryKey.RemoveSalaryKey(key, container);
                }
            }

            voeSection.find('.SalaryKeySection').toggle(SalaryKey.ContainsSalaryKey(null, container));
        }

        function CheckSelfEmployment(employmentCb, shouldCheckOrderBtn) {
            var employmentRow = $(employmentCb).closest('tr');
            var isSelfEmployment = employmentRow.find('.IsSelfEmployment').text().toLowerCase() === 'yes';
            if (!isSelfEmployment) {
                return;
            }

            var appId = employmentRow.find('.AppId').val();
            var borrowerType = employmentRow.find('.BorrowerType').val();
            var borrowerName = employmentRow.find('.Borrower').text();
            var key = employmentRow.find('.Key').val();
            var container = voeSection.find('.SelfEmploymentTarget');

            if (employmentCb.checked) {
                var containsPicker = SelfEmployment.ContainsSelfEmploymentRow(key, container);
                if (!containsPicker) {
                    var picker = SelfEmployment.CreateSelfEmploymentRow(borrowerName, appId, borrowerType, key);
                    voeSection.find('.SelfEmploymentTarget').append(picker);
                }
            }
            else {
                var anyMoreChecked = voeSection.find('.EmploymentTable').find('.Key[value="' + key + '"]').filter(function () {
                    var isChecked = $(this).siblings('.EmploymentCB').is(':checked');
                    var isSelfEmployment = $(this).closest('tr').find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                    return isChecked && isSelfEmployment;
                }).length > 0;

                if (!anyMoreChecked && SelfEmployment.ContainsSelfEmploymentRow(key, container)) {
                    SelfEmployment.RemoveSelfEmploymentRow(key, container);
                }
            }

            if (typeof (shouldCheckOrderBtn) !== 'undefined' && shouldCheckOrderBtn !== null && shouldCheckOrderBtn) {
                ToggleOrderBtn();
            }
        }

        function ToggleSpecificEmployerRecordSearch(lenderService) {
            voeSection.find('.UseSpecificEmployerRecordSearch').prop('checked', lenderService.UseSpecificEmployerRecordSearch);
            voeSection.find('.UseSpecificEmployerRecordSearchRow').toggle(!lenderService.EnforceUseSpecificEmployerRecordSearch);
        }

        function ToggleOrderBtn() {
            var docsVisible = voeSection.find(".BorrowerAuthDocsSection").is(':visible');
            var docsOk = docsVisible ? AuthorizationDocPicker.AllPickersHaveDocs(voeSection.find('.DocumentTable')) : true;
            var inputsOk = voeSection.find('.RequiredInput:visible').filter(function () { return $(this).val() === '' }).length === 0;
            var selfPrepareOk = SelfEmployment.CheckAllSelfEmploymentFilledOut(voeSection.find('.SelfEmploymentTarget'));
            var lenderService = voeSection.find('.LenderServices').val();
            var lenderServiceChosen = lenderService != null && lenderService !== '';
            var employmentChosen = !isEmploymentPickerBeingUsed || voeSection.find('.EmploymentCB:checked').length > 0;
            var borrowerChosen = isEmploymentPickerBeingUsed || voeSection.find('.BorrowerList').val() !== '';

            var canOrder = ML.CanOrderVoe && docsOk && inputsOk && lenderServiceChosen && selfPrepareOk && employmentChosen && borrowerChosen;
            voeSection.find('.VOXPlaceOrderBtn').prop('disabled', !canOrder);
            if (!ML.CanOrderVoe) {
                voeSection.find('.VOXPlaceOrderBtn').prop('title', ML.OrderVoeDenialReason);
            }
        }

        function TogglePickerSection(useEmploymentPicker, canSwitch) {
            voeSection.find('.PickerSection').hide();
            if (typeof (canSwitch) === 'boolean') {
                voeSection.find('.SectionToggleSection').prop('disabled', !canSwitch).toggle(canSwitch);
                if (canSwitch && typeof (isEmploymentPickerBeingUsed) === 'boolean') {
                    useEmploymentPicker = isEmploymentPickerBeingUsed;
                }
            }

            voeSection.find('.EmploymentPickerSection').toggle(useEmploymentPicker);
            voeSection.find('.EmploymentToggle').prop('checked', useEmploymentPicker);
            voeSection.find('.BorrowerPickerSection').toggle(!useEmploymentPicker);
            voeSection.find('.BorrowerToggle').prop('checked', !useEmploymentPicker);

            var oldIsEmploymentPickerBeingUsed = typeof (isEmploymentPickerBeingUsed) === 'boolean' ? isEmploymentPickerBeingUsed : useEmploymentPicker;
            isEmploymentPickerBeingUsed = useEmploymentPicker;

            if (oldIsEmploymentPickerBeingUsed !== isEmploymentPickerBeingUsed) {
                voeSection.find('.EmploymentCB').prop('checked', false);
                voeSection.find('.BorrowerList').prop('selectedIndex', 0);
                AuthorizationDocPicker.RemoveDocPicker(null, voeSection.find('.DocumentTable'));

                if (!isEmploymentPickerBeingUsed) {
                    CheckBorrowerAuthDocsForBorrowers(true);
                }
            }

            voeSection.find('.BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, voeSection.find('.DocumentTable')));
            ToggleOrderBtn();
        }

        OnClearLenderServicesDDL();

        function GatherRequestData() {
            var employmentInfo = [];
            voeSection.find('.EmploymentTable tbody').find('tr').each(function () {
                var employmentCbChecked = $(this).find('.EmploymentCB').is(':checked');
                if (employmentCbChecked) {
                    var recordId = $(this).find('.EmploymentRecordId').val();
                    var appId = $(this).find('.AppId').val();
                    var borrowerType = $(this).find('.BorrowerType').val();

                    var recordData = {
                        AppId: appId,
                        EmploymentRecordId: recordId,
                        BorrowerType: borrowerType
                    };

                    employmentInfo.push(recordData);
                }
            });

            var notificationEmail = voeSection.find('.NotificationEmail');
            var allowPayWithCreditCard = voeSection.find('.AllowPayWithCreditCard');
            var accountId = voeSection.find('.AccountId');
            var username = voeSection.find('.Username');
            var password = voeSection.find('.Password');
            var data = {
                LoanId: ML.sLId,
                LenderService: voeSection.find('.LenderServices').val(),
                NotificationEmail: notificationEmail.is(':visible') ? notificationEmail.val() : '',
                PayWithCreditCard: allowPayWithCreditCard.is(':visible') && allowPayWithCreditCard.is(':checked'),
                UseSpecificEmployerRecordSearch: voeSection.find('.UseSpecificEmployerRecordSearch').is(':checked'),
                AccountId: accountId.is(':visible') ? accountId.val() : '',
                UserName: username.is(':visible') ? username.val() : '',
                Password: password.is(':visible') ? password.val() : '',
                DocPickerInfo: voeSection.find('.BorrowerAuthDocsSection').is(':visible') ? AuthorizationDocPicker.RetrieveAllDocPickerInfo(voeSection.find('.DocumentTable')).join(',') : '',
                EmploymentInfo: JSON.stringify(employmentInfo),
                VerifyIncome: verifyIncomeEnabled && voeSection.find('.VerifyIncome').is(':checked'),
                SalaryKeys: JSON.stringify(salaryKeyEnabled ? SalaryKey.RetrieveAllSalaryKeys(voeSection.find('.SalaryKeySection')) : []),
                SelfEmploymentInfo: JSON.stringify(SelfEmployment.RetrieveAllSelfEmploymentSections(voeSection.find('.SelfEmploymentTarget'))),
                BorrowerInfo: voeSection.find('.BorrowerList').val(),
                VerifiesBorrower: !isEmploymentPickerBeingUsed
            };

            return data;
        }

        function Initialize() {
            AuthorizationDocPicker.RegisterDocChangeCallback(ToggleOrderBtn);
            SelfEmployment.RegisterStates(States);
            SelfEmployment.RegisterPostChangeCallback(ToggleOrderBtn);
            var validStatus = {
                0: false, // Pending
                1: true, // Complete
                3: true, // NoHit
            };

            VOXOrderCommon.InitializeCommonFunctions({
                GatherRequestData: GatherRequestData,
                ServiceType: "voe",
                VOXServiceName: "VOE",
                ToggleOrderBtn: ToggleOrderBtn,
                ValidStatusesAfterInitialRequest: validStatus,
                VOXSection: voeSection,
                ShouldPushOutErrors: true
            });

            ToggleNotificationEmail(ML.NotificationEmailDefault);
        }

        _initialize = Initialize;
    });

    function _onTabOpen() {
        _initialize();
        $('#secVoe').find('.LenderServices').change();
    }

    function _clearSection() {
        var voeSection = $('#secVoe');
        voeSection.find('input:text, input:password').val('');
        voeSection.find('input:checkbox').prop('checked', false);
        voeSection.find('select').prop('selectedIndex', 0);
        AuthorizationDocPicker.RemoveDocPicker(null, voeSection.find('.DocumentTable'))
        SalaryKey.RemoveSalaryKey(null, voeSection.find('.SalaryKeyTable'));
        SelfEmployment.RemoveSelfEmploymentRow(null, voeSection.find('.SelfEmploymentTarget'));
    }

    return {
        OnTabOpen: _onTabOpen,
        ClearSection: _clearSection
    };
})();

var SalaryKey = (function () {
    var salaryKeyTemplate;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/webapp/Partials/VOX/SalaryKey.html", function (data) {
            salaryKeyTemplate = $.template(data);
        });
    });

    function _createSalaryKeyRow(borrowerName, appId, borrowerType, key) {
        var data = {
            BorrowerName: borrowerName,
            AppId: appId,
            BorrowerType: borrowerType,
            Key: key
        };

        return $.tmpl(salaryKeyTemplate, data);
    }

    function _removeSalaryKey(key, container) {
        if (key === null || key === '') {
            $(container).find('.SalaryKeyRow').remove();
        }
        else {
            $(container).find('.Key[value="' + key + '"]').closest('tr').remove();
        }
    }

    function _containsSalaryKey(key, container) {
        if (key === null || key === '') {
            return $(container).find('.SalaryKeyRow').length > 0;
        }
        else {
            return $(container).find('.Key[value="' + key + '"]').closest('tr').length > 0;
        }
    }

    function _retrieveAllSalaryKeys(container) {
        var allData = [];
        $(container).find('.SalaryKeyRow').each(function (index, element) {
            var data = {
                SalaryKey: $(this).find('.SalaryKey').val(),
                AppId: $(this).find('.AppId').val(),
                BorrowerType: $(this).find('.BorrowerType').val()
            };

            allData.push(data);
        });

        return allData;
    }

    return {
        CreateSalaryKeyRow: _createSalaryKeyRow,
        RemoveSalaryKey: _removeSalaryKey,
        ContainsSalaryKey: _containsSalaryKey,
        RetrieveAllSalaryKeys: _retrieveAllSalaryKeys
    };
})();

var SelfEmployment = (function () {
    var selfEmploymentTemplate;
    var states = [];
    var selfEmploymentPostChangeCallback;

    jQuery(function ($) {
        var selfEmploymentTarget = $('#secVoe').find('.SelfEmploymentTarget');

        $.get(ML.VirtualRoot + "/webapp/Partials/VOX/SelfEmployment.html", function (data) {
            selfEmploymentTemplate = $.template(data);
        });

        selfEmploymentTarget.on('change', '.TaxesSelfPrepared', function () {
            var row = $(this).closest('.SelfEmploymentRow');
            if (this.checked) {
                row.find('input').not(this).not('[type="hidden"]').val('');
                row.find('input').not(this).not('[type="hidden"]').prop('disabled', true);
                row.find('input.RequiredInput').removeClass('RequiredInput');
                row.find('.State').removeClass('RequiredInput');
                row.find('select').val('');
                row.find('select').prop('disabled', true);
                row.find('.RequiredSpan').hide();
                row.find('.TaxPreparerPickerTooltip').attr('disabled', true);
            }
            else {
                row.find('input').not(this).prop('disabled', false);
                row.find('select').prop('disabled', false);
                row.find('.CompanyName').addClass('RequiredInput');
                row.find('.StreetAddress').addClass('RequiredInput');
                row.find('.PhoneNumber').addClass('RequiredInput');
                row.find('.City').addClass('RequiredInput')
                row.find('.State').addClass('RequiredInput')
                row.find('.Zip').addClass('RequiredInput')
                row.find('.RequiredSpan').show();
                row.find('.TaxPreparerPickerTooltip').attr('disabled', false);
            }

            if (typeof (selfEmploymentPostChangeCallback) === 'function') {
                selfEmploymentPostChangeCallback();
            }
        });

        selfEmploymentTarget.on('blur', '.Zipcode', function () {
            var zipcode = $(this);
            if ($(this).val() === '') {
                zipcode.siblings('.RequiredSpan').show();
                return;
            }

            var row = zipcode.closest('.SelfEmploymentRow');
            var city = row.find('.City');
            var state = row.find('.State');
            smartZipcode(this, city[0], state[0], null, null);
        });

        selfEmploymentTarget.on('change', '.Zipcode', function () {
            $(this).siblings('.RequiredSpan').toggle($(this).val() === '');
            var row = $(this).closest('.SelfEmploymentRow');
            var city = row.find('.City');
            var state = row.find('.State');
            city.change();
            state.change();
        });

        selfEmploymentTarget.on('click', '.TaxPreparerPicker', function () {
            var row = $(this).closest('.SelfEmploymentRow');
            if (row.find('.TaxesSelfPrepared').is(':checked')) {
                return;
            }

            var url = ML.VirtualRoot + '/webapp/RolodexListV2.aspx?loanid=' + ML.sLId;
            IframePopup.show(url, {}).then(function (pair) {
                var error = pair[0];
                var result = pair[1];
                if (error != null) {
                    simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "Error");
                }
                else if (result) {
                    row.find('.CompanyName').val(result.AgentCompanyName);
                    row.find('.StreetAddress').val(result.AgentStreetAddr);
                    row.find('.City').val(result.AgentCity);
                    row.find('.State').val(result.AgentState);
                    row.find('.Zipcode').val(result.AgentZip);
                    row.find('.PhoneNumber').val(result.PhoneOfCompany);
                    row.find('.FaxNumber').val(result.FaxOfCompany);

                    row.find('.CompanyName').change();
                    row.find('.StreetAddress').change();
                    row.find('.City').change();
                    row.find('.State').change();
                    row.find('.Zipcode').change();
                    row.find('.PhoneNumber').change();
                }
            });
        });
    });

    function _createSelfEmploymentRow(borrowerName, appId, borrowerType, key) {
        var data = {
            BorrowerName: borrowerName,
            AppId: appId,
            BorrowerType: borrowerType,
            Key: key,
            States: states
        };

        var row = $.tmpl(selfEmploymentTemplate, data);
        var needToInit = $(row).find('[preset]');
        $.each(needToInit, function (index, value) {
            _initMask($(value));
        });

        return row;
    }

    function _removeSelfEmploymentRow(key, container) {
        if (key === null || key === '') {
            $(container).find('.SelfEmploymentRow').remove();
        }
        else {
            $(container).find('.Key[value="' + key + '"]').closest('.SelfEmploymentRow').remove();
        }
    }

    function _containsSelfEmploymentRow(key, container) {
        if (key === null || key === '') {
            return $(container).find('.SelfEmploymentRow').length > 0;
        }
        else {
            return $(container).find('.Key[value="' + key + '"]').closest('.SelfEmploymentRow').length > 0;
        }
    }

    function _checkAllSelfEmploymentFilledOut(container) {
        return container.find('.SelfEmploymentRow:visible').filter(function () {
            if ($(this).find('.TaxesSelfPrepared').is(':checked')) {
                return false;
            }

            var city = $(this).find('.City').val();
            var zipcode = $(this).find('.Zipcode').val();
            var state = $(this).find('.State').val();

            return city === '' || zipcode === '' || state === '';
        }).length === 0;
    }

    function _retrieveAllSelfEmploymentSections(container) {
        var allData = [];
        container.find('.SelfEmploymentRow').each(function (index, element) {
            var data = {
                AppId: $(this).find('.AppId').val(),
                BorrowerType: $(this).find('.BorrowerType').val(),
                Notes: $(this).find('.Notes').val(),
                TaxesSelfPrepared: $(this).find('.TaxesSelfPrepared').is(':checked'),
                CompanyName: $(this).find('.CompanyName').val(),
                StreetAddress: $(this).find('.StreetAddress').val(),
                City: $(this).find('.City').val(),
                State: $(this).find('.State').val(),
                Zipcode: $(this).find('.Zipcode').val(),
                PhoneNumber: $(this).find('.PhoneNumber').val(),
                FaxNumber: $(this).find('.FaxNumber').val()
            };

            allData.push(data);
        });

        return allData;
    }

    return {
        CreateSelfEmploymentRow: _createSelfEmploymentRow,
        RemoveSelfEmploymentRow: _removeSelfEmploymentRow,
        ContainsSelfEmploymentRow: _containsSelfEmploymentRow,
        RegisterStates: function (stateList) { states = stateList; },
        RegisterPostChangeCallback: function (callback) { selfEmploymentPostChangeCallback = callback; },
        CheckAllSelfEmploymentFilledOut: _checkAllSelfEmploymentFilledOut,
        RetrieveAllSelfEmploymentSections: _retrieveAllSelfEmploymentSections
    };
})();

