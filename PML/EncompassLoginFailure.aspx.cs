﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace PriceMyLoan
{
    public partial class LoginFailure : PriceMyLoan.UI.BasePage
    {
        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (!IsPostBack && RequestHelper.GetSafeQueryString("errorcode") != null)
            {
                DisplayErrorMessage(RequestHelper.GetSafeQueryString("errorcode"));
            }
        }

        private void DisplayErrorMessage(string sErrorCode)
        {
            switch (sErrorCode)
            {
                case "IsDisabled":
                    m_ErrMsg.Text = ErrorMessages.AccountDisabled;
                    m_ErrMsg2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    break;
                case "IsLocked":
                    m_ErrMsg.Text = ErrorMessages.AccountLocked;
                    m_ErrMsg2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    break;
                case "NeedsToWait":
                    m_ErrMsg.Text = ErrorMessages.InvalidLoginPassword;
                    m_ErrMsg2.Text = "";
                    break;
                case "InvalidAndWait":
                    m_ErrMsg.Text = ErrorMessages.InvalidLoginPassword;
                    m_ErrMsg2.Text = "";
                    break;
                case "InvalidLoginPassword":
                    m_ErrMsg.Text = ErrorMessages.InvalidLoginPassword;
                    m_ErrMsg2.Text = "";
                    break;
                case "SessionExpired":
                    m_ErrMsg.Text = "Your session has expired.";
                    m_ErrMsg2.Text = "";
                    break;
                case "LPEIsLocked":
                    m_ErrMsg.Text = "The pricing engine cannot be run.";
                    m_ErrMsg2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    break;
                case "NotEnabled":
                    m_ErrMsg.Text = ErrorMessages.Encompass_NotSignedUp;
                    m_ErrMsg2.Text = "";
                    break;
                case "MissingSSN":
                    m_ErrMsg.Text = ErrorMessages.Encompass_MissingSSN;
                    m_ErrMsg2.Text = "";
                    break;
                case "None":
                default:
                    m_ErrMsg.Text = ErrorMessages.FailedToImportData;
                    m_ErrMsg2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    Tools.LogBug("Unknown login error code in login page: " + sErrorCode + ".");
                    break;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
