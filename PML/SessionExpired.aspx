<%@ Page Language="C#" Codebehind="SessionExpired.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.SessionExpired" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="PriceMyLoan.Common" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>SessionExpired</title>
    <style type="text/css">
    .container { position: absolute; top:50%; left:50%; margin-top: -5em; }
    .innercontainer { position: relative; left: -10em; width: 30em;}
    </style>
</head>
<body MS_POSITIONING="FlowLayout" id="LP_f6d1d39dee3c435d9d14f1dc245e316d">

<script type="text/javascript">
  function f_exit() {
      self.close();
      
      // Can't close? Then just display a message telling the user to manually close.
      $j('#CloseMessage').text(<%= AspxTools.JsString(ErrorMessages.PML.AppErrorFailedToClose) %>);
      $j('#ExitButton').prop("disabled", true);
      
      // Center it, since the width changed.
      $window = $j(window);
      $container = $j('.container');
      $container.css("position", "absolute");
      $container.css("top", (($window.height() - $container.outerHeight()) / 2 ) + $window.scrollTop() + "px");
      $container.css("left", (($window.width() - $container.outerWidth()) / 2 ) + $window.scrollLeft() + "px");
  }
  function f_logout() {
      self.location = <%= AspxTools.JsString(VirtualRoot) %> + "/logout.aspx";
  }
</script>
<form id="SessionExpired" method="post" runat="server">
<div class="container">
    <div class="innercontainer">
    <div style="font-weight: bold; font-size: 18px">Your session is expired.</div>
    <asp:Panel id="ExitPanel" runat="server">
        <br />
        <div>
            <input id="ExitButton" type="button" value="Exit" onclick="f_exit();" />
            <asp:Panel id="LogoutPanel" runat="server">
                <input type="button" value="LOG OFF" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: Black; margin-top: 5; margin-right: 15;" onclick="f_logout();" />
            </asp:Panel>
        </div>
    </asp:panel>

    <br />
    <div id="CloseMessage"></div>
    </div>
</div>

</form>
	
</body>
</html>
