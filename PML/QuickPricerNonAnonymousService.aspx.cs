﻿namespace PriceMyLoan
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using global::DataAccess;
    using global::DataAccess.LoanComparison;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.QuickPricer;
    using LendersOffice.Security;
    using PriceMyLoan.Common;

    public partial class QuickPricerNonAnonymousService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private AbstractUserPrincipal GetPrincipal()
        {
            AbstractUserPrincipal principal = null;
            try
            {
                // Quick Pricer access by authenticate users. Pull information from cookie.
                HttpCookie cookie = Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                if (null != cookie)
                {
                    string cookieValue = Request.Cookies.Get(FormsAuthentication.FormsCookieName).Value;
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookieValue);
                    if (ticket.Expired == false)
                    {
                        principal = PriceMyLoan.Security.PrincipalFactory.Create(new FormsIdentity(ticket)) as AbstractUserPrincipal;
                    }
                }
            }
            catch (AccessDenied)
            {
                throw;
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw new CSessionExpiredException();
            }

            if (null == principal)
            {
                throw new CSessionExpiredException();
            }

            return principal;
        }

        private string GetLeadSrcCookieId(AbstractUserPrincipal principal)
        {
            return string.Format("QuickPricerLeadSrc-{0}", (principal == null) ? "" : principal.UserId.ToString());
        }

        protected override void Process(string methodName)
        {
            try
            {
                switch (methodName)
                {
                    case "SubmitQuickPrice":
                        SubmitQuickPrice();
                        break;
                    case "IsResultReady":
                        IsResultReady();
                        break;
                    case "ConvertToLoan":
                        ConvertToLoan(false /*SetBorrowerData */);
                        break;
                    case "ConvertToLoanFromBorrower":
                        ConvertToLoanFromBorrower();
                        break;
                    case "CalculateData":
                        CalculateData();
                        break;
                }
            }
            catch (CSessionExpiredException)
            {
                SetResult("SessionExpired", "True");
            }
            catch (CBaseException exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
                Tools.LogErrorWithCriticalTracking(exc);
            }
        }

        private void SubmitQuickPrice()
        {
            AbstractUserPrincipal principal = GetPrincipal();
            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
            var quickPricerLoanItem = GetQuickPricerLoanItem(broker);
            Guid requestId = QuickPricerEngine.Submit(principal, quickPricerLoanItem);

            SetResult("RequestID", requestId.ToString());
            SetResult("Timing", DateTime.Now.Ticks); // 3/26/2009 dd - To track how long pricing is executed.

            // 3/28/14 gf - opm 173059, store the lead src to a cookie.
            if (PriceMyLoanConstants.IsEmbeddedPML && broker.AddLeadSourceDropdownToInternalQuickPricer)
            {
                RequestHelper.StoreToCookie(GetLeadSrcCookieId(principal), quickPricerLoanItem.sLeadSrcDesc, SmallDateTime.MaxValue);
            }
        }
        private bool? x_isDebugMode = null;
        private bool IsDebugMode
        {
            get
            {
                if (x_isDebugMode == null)
                {
                    x_isDebugMode = false;
                    x_isDebugMode = GetBool("IsDebugMode", x_isDebugMode.Value);
                }
                return x_isDebugMode.Value;
            }

        }

        private QuickPricerLoanItem GetQuickPricerLoanItem(BrokerDB brokerDB)
        {
            QuickPricerLoanItem item = new QuickPricerLoanItem();
            if (PriceMyLoanConstants.IsEmbeddedPML && brokerDB.AddLeadSourceDropdownToInternalQuickPricer)
            {
                item.sLeadSrcDesc = GetString("sLeadSrcDesc");
            }
            item.sLPurposeTPe = (E_sLPurposeT)GetInt("sLPurposeTPe");
            item.sLAmtCalcPe_rep = GetString("sLAmtCalcPe");
            item.sHouseValPe_rep = GetString("sHouseValPe");
            item.sSpStatePe = GetString("sSpStatePe");
            item.sSpCounty = GetString("sSpCounty");
            item.sSpZip = GetString("sSpZip");
            item.sCreditScoreType1_rep = GetString("sCreditScoreType1");
            item.sProdSpT = (E_sProdSpT)GetInt("sProdSpT");
            item.sOccTPe = (E_sOccT)GetInt("sOccTPe");
            item.sHas2ndFinPe = (GetString("secondFinancing") == "bYes2ndFinancing");

            if (item.sLPurposeTPe == E_sLPurposeT.Purchase)
            {
                item.sApprValPe_rep = GetString("sApprValPe");
            }

            item.sProdImpound = GetBool("sProdImpound");
            item.sProdRLckdDays_rep = GetString("sProdRLckdDays");

            item.sCltvRPe_rep = GetString("sCltvRPe");
            item.sDownPmtPcPe_rep = GetString("sDownPmtPcPe");
            item.sEquityPe_rep = GetString("sEquityPe");
            item.sLtvROtherFinPe_rep = GetString("sLtvROtherFinPe");
            item.sProOFinBalPe_rep = GetString("sProOFinBalPe");

            //opm 42128 fs 11/13/09
            item.sLtv80TestResultT = (E_sLtv80TestResultT)GetInt("sLtv80TestResultT");

            // OPM 109393.
            if (brokerDB.HasEnabledPMI || brokerDB.IsNewPmlUIEnabled || IsNewPMLUser())
            {
                item.sProdConvMIOptionT = (item.sLtv80TestResultT == E_sLtv80TestResultT.Over80) ? (E_sProdConvMIOptionT)GetInt("sProdConvMIOptionT") : E_sProdConvMIOptionT.NoMI;
                item.sConvSplitMIRT = (E_sConvSplitMIRT)GetInt("sConvSplitMIRT");
            }
            else
            {
                item.sProdMIOptionT = (item.sLtv80TestResultT == E_sLtv80TestResultT.Over80) ? (E_sProdMIOptionT)GetInt("sProdMIOptionT") : E_sProdMIOptionT.LeaveBlank;
            }

            bool bProdFilterDueOther = item.sProdFilterDueOther = GetBool("sProdFilterDueOther", false);
            item.sProdFilterDue10Yrs = GetBool("sProdFilterDue10Yrs", bProdFilterDueOther);
            item.sProdFilterDue15Yrs = GetBool("sProdFilterDue15Yrs", bProdFilterDueOther);
            item.sProdFilterDue20Yrs = GetBool("sProdFilterDue20Yrs", bProdFilterDueOther);
            item.sProdFilterDue25Yrs = GetBool("sProdFilterDue25Yrs", bProdFilterDueOther);
            item.sProdFilterDue30Yrs = GetBool("sProdFilterDue30Yrs", bProdFilterDueOther);
            bool bProdFilterFinMethOther = item.sProdFilterFinMethOther = GetBool("sProdFilterFinMethOther", false);
            item.sProdFilterFinMethFixed = GetBool("sProdFilterFinMethFixed", bProdFilterFinMethOther);
            item.sProdFilterFinMeth3YrsArm = GetBool("sProdFilterFinMeth3YrsArm", bProdFilterFinMethOther);
            item.sProdFilterFinMeth5YrsArm = GetBool("sProdFilterFinMeth5YrsArm", bProdFilterFinMethOther);
            item.sProdFilterFinMeth7YrsArm = GetBool("sProdFilterFinMeth7YrsArm", bProdFilterFinMethOther);
            item.sProdFilterFinMeth10YrsArm = GetBool("sProdFilterFinMeth10YrsArm", bProdFilterFinMethOther);
            item.sProdFilterPmtTPI = GetBool("sProdFilterPmtTPI");
            item.sProdFilterPmtTIOnly = GetBool("sProdFilterPmtTIOnly");
            item.sProdIncludeNormalProc = GetBool("sProdIncludeNormalProc");
            item.sProdIsDuRefiPlus = GetBool("sProdIsDuRefiPlus");
            item.sProdIncludeMyCommunityProc = GetBool("sProdIncludeMyCommunityProc");
            item.sProdIncludeHomePossibleProc = GetBool("sProdIncludeHomePossibleProc");
            item.sProdIncludeFHATotalProc = GetBool("sProdIncludeFHATotalProc");
            item.sProdIncludeVAProc = GetBool("sProdIncludeVAProc");
            item.sProdIncludeUSDARuralProc = GetBool("sProdIncludeUSDARuralProc");
            item.sProdCalcEntryT = (E_sProdCalcEntryT)GetInt("sProdCalcEntryT");
            item.sLtvRPe_rep = GetString("sLtvRPe");
            if (brokerDB.IsAsk3rdPartyUwResultInPml)
            {
                item.sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT)GetInt("sProd3rdPartyUwResultT", 0);
            }
            item.IsDebugMode = IsDebugMode;

            return item;
        }

        private bool IsNewPMLUser()
        {
            var principal = GetPrincipal();
            var empDB = EmployeeDB.RetrieveById(principal.BrokerId, principal.EmployeeId);
            if (empDB != null)
            {
                return empDB.IsNewPmlUIEnabled;
            }

            return false;
        }

        private void IsResultReady()
        {
            Guid requestId = GetGuid("RequestID");
            bool isReady = QuickPricerEngine.IsResultReady(requestId);
            SetResult("IsReady", isReady);

            if (isReady)
            {
                AbstractUserPrincipal principal = GetPrincipal();
                QuickPricerResult result = QuickPricerEngine.GetResult(principal, requestId, IsDebugMode);

                if (IsDebugMode)
                {
                    Tools.LogInfo("QuickPricerDebugResults: "
                        + Environment.NewLine + Environment.NewLine + "requestid: " + result.RequestId
                        + Environment.NewLine + Environment.NewLine + "hasError: " + result.HasError
                        + Environment.NewLine + Environment.NewLine + "errorMsg: " + result.ErrorMessage
                        + Environment.NewLine + Environment.NewLine + "debuginfo: " + result.DebugInfo
                        + Environment.NewLine + Environment.NewLine + "js obj: " + result.ToJavascriptObject(false));

                }
                if (result.HasError)
                {
                    SetResult("ErrorMessage", result.ErrorMessage);
                }
                else
                {
                    if (!PriceMyLoanConstants.IsEmbeddedPML)
                    {
                        result.PriceGroupName = null; // Never display price group in stand alone PML. 2/26/09.
                    }
                    bool displayPmlFeeIn100Format = false;
                    if (result.PriceGroup != null)
                    {
                        displayPmlFeeIn100Format = result.PriceGroup.DisplayPmlFeeIn100Format;
                    }
                    SetResult("Result", result.ToJavascriptObject(displayPmlFeeIn100Format));
                    SetResult("IneligibleProductList", result.ConvertIneligibleListToJavascriptObject());
                }
                long startTicks = GetLong("Timing", 0);
                if (startTicks != 0)
                {
                    TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - startTicks);
                    if (ts.TotalSeconds > (double)ConstStage.LpeTimingEmailThresholdInSeconds)
                    {
                        string emailBody = "UserPrincipal: " + principal + Environment.NewLine + result.DetailTimingInfo;
                        EmailUtilities.SendPmlTimingEmail("PML Timing Result - Server. (QuickPricer). Total " + ts.TotalSeconds + " seconds", emailBody);
                    }
                    DistributeUnderwritingEngine.RecordPricingTotalTime(requestId, "ManualQuickPricer", (int)ts.TotalMilliseconds);
                }
            }
        }
        private void ConvertToLoanFromBorrower()
        {
            // OPM 32225
            AbstractUserPrincipal principal = GetPrincipal();

            if (principal != null && principal.IsPmlQuickPricerAnonymousLogin)
            {
                return; // Anonymous cannot create loan.
            }
            BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
            if (!CheckForDuplicates(principal))
            {
                SetResult("DuplicatesExist", "0");
                ConvertToLoan(true);
            }
            else
            {
                // There are duplicates.  Add our result to cache

                LoanBorrowerData data = new LoanBorrowerData()
                {
                    BorrowerFirstName = GetString("CreateLoan_BorrFirstName"),
                    BorrowerLastName = GetString("CreateLoan_BorrLastName"),
                    BorrowerSsn = GetString("CreateLoan_BorrSsn"),
                    LoanData = GetQuickPricerLoanItem(brokerDB)
                };

                string resultData = SerializationHelper.XmlSerialize(data);
                Guid newKey = Guid.NewGuid();
                AutoExpiredTextCache.AddToCache(resultData, TimeSpan.FromHours(1), newKey);
                SetResult("DuplicatesExist", "1");
                SetResult("DupeKey", newKey);
            }
        }

        private void ConvertToLoan(bool setBorrower)
        {

            AbstractUserPrincipal principal = GetPrincipal();

            if (principal != null && principal.IsPmlQuickPricerAnonymousLogin)
            {
                return; // Anonymous cannot create loan.
            }

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);

            E_LoanCreationSource source = E_LoanCreationSource.UserCreateFromBlank;

            if (Guid.Empty != broker.PmlLoanTemplateID)
            {
                source = E_LoanCreationSource.LeadConvertToLoanUsingTemplate;
            }
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, source);

            Guid sLId = Guid.Empty;

            if (Guid.Empty == broker.PmlLoanTemplateID)
            {
                sLId = creator.CreateBlankLoanFile();
            }
            else
            {
                sLId = creator.CreateLoanFromTemplate(templateId: broker.PmlLoanTemplateID);

            }

            CPageData dataLoan = new CQuickPricerLoanData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            dataLoan.sQuickPricerLoanItem = GetQuickPricerLoanItem(broker);

            if (setBorrower)
            {
                CAppData dataApp = dataLoan.GetAppData(0);
                dataApp.aBFirstNm = GetString("CreateLoan_BorrFirstName");
                dataApp.aBLastNm = GetString("CreateLoan_BorrLastName");
                dataApp.aBSsn = GetString("CreateLoan_BorrSsn");
            }

            dataLoan.Save();

            SetResult("sLId", sLId);

        }

        private bool CheckForDuplicates(AbstractUserPrincipal principal)
        {
            string firstNm = GetString("CreateLoan_BorrFirstName");
            string lastNm = GetString("CreateLoan_BorrLastName");
            string ssn = GetString("CreateLoan_BorrSsn");

            IEnumerable<Dictionary<string, object>> duplicates = DuplicateFinder.FindDuplicateLoansForPmlPipeline(principal, firstNm, lastNm, ssn);

            return duplicates.Any();
        }

        private void CalculateData()
        {
            AbstractUserPrincipal principal = GetPrincipal();
            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
            // 3/30/2009 dd - In order to speed up the calculation time, I will load up the 
            // quick pricer template instead of grabing new loan from the QuickPricerLoanPool.
            // DO NOT invoke InitSave() or Save() method.
            Guid sLId = GetGuid("QuickPricerTemplateId");

            CPageData dataLoan = new CQuickPricerLoanData(sLId);
            dataLoan.InitLoad();

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            dataLoan.sQuickPricerLoanItem = GetQuickPricerLoanItem(broker);

            SetResult("sProdCalcEntryT", dataLoan.sProdCalcEntryT);
            SetResult("sHouseValPe", dataLoan.sHouseValPe_rep);
            SetResult("sLtvRPe", dataLoan.sLtvRPe_rep);
            SetResult("sLAmtCalcPe", dataLoan.sLAmtCalcPe_rep);

            //opm 32479 fs 07/21/09
            SetResult("sDownPmtPcPe", dataLoan.sDownPmtPcPe_rep);
            SetResult("sEquityPe", dataLoan.sEquityPe_rep);
            SetResult("sLtvROtherFinPe", dataLoan.sLtvROtherFinPe_rep);
            SetResult("sProOFinBalPe", dataLoan.sProOFinBalPe_rep);
            SetResult("sCltvRPe", dataLoan.sCltvRPe_rep);
            SetResult("sLtv80TestResultT", dataLoan.sLtv80TestResultT);

            //opm 28030 mp Set result for the date
            SetResult("sProdRLckdExpiredDLabel", dataLoan.sProdRLckdExpiredDLabel);
        }
    }
}
