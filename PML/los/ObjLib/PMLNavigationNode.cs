﻿using System;
using System.Linq;
using DataAccess;
using LendersOffice.ObjLib.PMLNavigation;
using System.Collections.Generic;
using PriceMyLoan.Security;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.ObjLib.ServiceCredential;
using LqbGrammar.DataTypes;

namespace PriceMyLoan.ObjLib
{
    public sealed class PMLNavigationManager
    {
        private EmployeeDB m_employeeDB;
        private BrokerDB m_brokerDb;
        private NavigationConfiguration m_brokerNavConfig;
        private PriceMyLoanPrincipal m_principal;
        private Dictionary<Guid, bool> m_PageIdPermission;
        private Func<string, string> m_urlResolver;
        private LendersOffice.ObjLib.TPO.TPOPortalConfig m_tpoConfig; // may be null.

        public LendersOffice.ObjLib.TPO.TPOPortalConfig TpoConfig { get { return m_tpoConfig; } }

        public PMLNavigationManager(PriceMyLoanPrincipal principal, Func<string, string> urlResolver)
        {
            m_employeeDB = EmployeeDB.RetrieveById(principal.BrokerId, principal.EmployeeId);
            m_brokerDb = BrokerDB.RetrieveById(principal.BrokerId);
            m_brokerNavConfig = m_brokerDb.GetPmlNavigationConfiguration(principal);
            m_principal = principal;
            m_urlResolver = urlResolver;

            IEnumerable<Guid> companyLinks;
            if (string.Equals("P", principal.Type, StringComparison.OrdinalIgnoreCase))
            {
                m_tpoConfig = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveByUserID(m_principal.UserId, m_principal.BrokerId);

                var pmlBroker = PmlBroker.RetrievePmlBrokerById(m_employeeDB.PmlBrokerId, principal.BrokerId);
                companyLinks = pmlBroker.AccessiblePmlNavigationLinksId;
            }
            else
            {
                m_tpoConfig = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveByBranchId(m_principal.BranchId, m_principal.BrokerId);

                var accessibleBranchLinks = new BranchEnabledRetailPortalCustomPage(principal.BrokerId, principal.BranchId);
                companyLinks = accessibleBranchLinks.RetrieveEnabledPages();
            }

            this.DetermineInitialPagePermissions();

            foreach (Guid g in m_employeeDB.AccessiblePmlNavigationLinksId.Union(companyLinks))
            {
                m_PageIdPermission.Add(g, true);
            }
        }

        private void DetermineInitialPagePermissions()
        {
            bool isRetailMode = m_principal.PortalMode == E_PortalMode.Retail;
            bool isNonQmOpEnabled = m_brokerDb.IsNonQmOpEnabled(m_principal);
            m_PageIdPermission = new Dictionary<Guid, bool>() {
                { NavigationConfiguration.SystemNode.LaunchQuickPricerLink , m_brokerDb.IsQuickPricerEnable && false == m_principal.IsActuallyUsePml2AsQuickPricer },
                { NavigationConfiguration.SystemNode.LaunchQuickPricer2Link , m_principal.IsActuallyUsePml2AsQuickPricer },
                { NavigationConfiguration.SystemNode.QuickPricerRateMonitorsLink, m_principal.IsActuallyUsePml2AsQuickPricer && m_brokerDb.AllowRateMonitorForQP2FilesInTpoPml},
                { NavigationConfiguration.SystemNode.Loans, true},
                { NavigationConfiguration.SystemNode.Leads, isRetailMode},
                { NavigationConfiguration.SystemNode.Tasks, m_brokerDb.IsUseNewTaskSystem},
                { NavigationConfiguration.SystemNode.Dashboard, m_tpoConfig != null },
                { NavigationConfiguration.SystemNode.ConditionLink, m_brokerDb.IsUseNewTaskSystem },
                { NavigationConfiguration.SystemNode.PortalMode, true},
                { NavigationConfiguration.SystemNode.ManageUsers, m_principal.IsPmlManager},
                { NavigationConfiguration.SystemNode.OriginatingCompanyServiceCredentials, m_principal.IsPmlManager && ServiceCredential.HasAvailableServiceCredentialConfiguration(ServiceCredentialContext.CreateEditOriginatingCompanyContextForUser(this.m_principal)) },
                { NavigationConfiguration.SystemNode.MyProfile, true},
                { NavigationConfiguration.SystemNode.NonQmToolkitFolder, isNonQmOpEnabled && m_principal.IsActuallyUsePml2AsQuickPricer},
                { NavigationConfiguration.SystemNode.NonQmQp, isNonQmOpEnabled && m_principal.IsActuallyUsePml2AsQuickPricer},
                { NavigationConfiguration.SystemNode.BankStatementIncomeCalculator, isNonQmOpEnabled && m_principal.IsActuallyUsePml2AsQuickPricer},
                { NavigationConfiguration.SystemNode.ScenarioRequestForm, isNonQmOpEnabled && m_principal.IsActuallyUsePml2AsQuickPricer}
            };

            bool userCanCreateLoans;
            if (string.Equals("B", this.m_principal.Type, StringComparison.OrdinalIgnoreCase))
            {
                userCanCreateLoans = m_principal.HasAtLeastOneRole(new E_RoleT[]
                {
                    E_RoleT.LoanOfficer,
                    E_RoleT.Processor,
                    E_RoleT.Secondary,
                    E_RoleT.PostCloser,
                    E_RoleT.LenderAccountExecutive
                });

                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreatePurchaseLoan, userCanCreateLoans);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateRefiLoan, userCanCreateLoans);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateHeloc1stLien, userCanCreateLoans && m_brokerDb.IsEnableHELOC && m_brokerDb.IsHelocsAllowedInPml);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateHeloc2ndLien, userCanCreateLoans && m_brokerDb.IsEnableHELOC && m_brokerDb.IsHelocsAllowedInPml && m_brokerDb.IsStandAloneSecondAllowedInPml);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.Create2ndLien, userCanCreateLoans && m_brokerDb.IsStandAloneSecondAllowedInPml);
            }
            else
            {
                userCanCreateLoans = m_principal.HasAtLeastOneRole(new E_RoleT[]
                {
                    E_RoleT.Pml_LoanOfficer,
                    E_RoleT.Pml_BrokerProcessor,
                    E_RoleT.Pml_Secondary,
                    E_RoleT.Pml_PostCloser
                });

                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreatePurchaseLoan, m_brokerDb.IsAllowExternalUserToCreateNewLoan && userCanCreateLoans);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateRefiLoan, m_brokerDb.IsAllowExternalUserToCreateNewLoan && userCanCreateLoans);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateHeloc1stLien, m_brokerDb.IsAllowExternalUserToCreateNewLoan && userCanCreateLoans && m_brokerDb.IsEnableHELOC && m_brokerDb.IsHelocsAllowedInPml);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateHeloc2ndLien, m_brokerDb.IsAllowExternalUserToCreateNewLoan && userCanCreateLoans && m_brokerDb.IsEnableHELOC && m_brokerDb.IsHelocsAllowedInPml && m_brokerDb.IsStandAloneSecondAllowedInPml);
                this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.Create2ndLien, m_brokerDb.IsAllowExternalUserToCreateNewLoan && userCanCreateLoans && m_brokerDb.IsStandAloneSecondAllowedInPml);
            }

            this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.ImportLoan, userCanCreateLoans);

            bool userCanCreateLeads = isRetailMode && m_principal.HasPermission(Permission.AllowCreatingNewLeadFiles);
            this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.ImportLead, userCanCreateLeads);
            this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreatePurchaseLead, userCanCreateLeads);
            this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateRefiLead, userCanCreateLeads);
            this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateHeloc1stLienLead, userCanCreateLeads && m_brokerDb.IsEnableHELOC && m_brokerDb.IsHelocsAllowedInPml);
            this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.CreateHeloc2ndLienLead, userCanCreateLeads && m_brokerDb.IsEnableHELOC && m_brokerDb.IsHelocsAllowedInPml && m_brokerDb.IsStandAloneSecondAllowedInPml);
            this.m_PageIdPermission.Add(NavigationConfiguration.SystemNode.Create2ndLienLead, userCanCreateLeads && m_brokerDb.IsStandAloneSecondAllowedInPml);
        }

        private bool EmployeeHasAccessTo(Guid id)
        {
            bool hasPermission;
            return m_PageIdPermission.TryGetValue(id, out hasPermission) && hasPermission;
        }

        public IEnumerable<DynaTreeNode> Nodes()
        {
            return m_brokerNavConfig.GetNavigation(Configurer,
                p => (p.isFolder && p.children.Count > 0) ||
                     (!p.isFolder && EmployeeHasAccessTo(p.Id)));
        }

        public IEnumerable<DynaTreeNode> PagesWithAccessRecursive()
        {
            return m_brokerNavConfig.RootNode.NodesRecursive((node) => 
                !node.isFolder && EmployeeHasAccessTo(node.Id));
        }        

        public DynaTreeNode GetLoansNode()
        {
            var node = m_brokerNavConfig.GetNavigation(Configurer, p => p.Id == NavigationConfiguration.SystemNode.Loans || p.Id == NavigationConfiguration.SystemNode.PipelineFolder).FirstOrDefault();

            if (node == null)
            {
                return null;
            }

            return node.children.FirstOrDefault();            
        }

        private void Configurer(DynaTreeNode p)
        {
            if (p.Id == NavigationConfiguration.SystemNode.Loans)
            {
                p.ConfigTarget = E_NavTarget.Self;
                p.addClass = "system";
                p.ConfigHref = m_urlResolver("~/main/pipeline.aspx?pg=0");
            }
            else if (p.Id == NavigationConfiguration.SystemNode.Leads)
            {
                p.ConfigTarget = E_NavTarget.Self;
                p.addClass = "system";
                p.ConfigHref = m_urlResolver("~/main/pipeline.aspx?pg=5");
            }
            else if (p.Id == NavigationConfiguration.SystemNode.Tasks)
            {
                p.ConfigTarget = E_NavTarget.Self;
                p.addClass = "system";
                p.ConfigHref = m_urlResolver("~/main/pipeline.aspx?pg=1");
            }
            else if (p.Id == NavigationConfiguration.SystemNode.ConditionLink)
            {
                p.ConfigTarget = E_NavTarget.Self;
                p.addClass = "system";
                p.ConfigHref = m_urlResolver("~/main/pipeline.aspx?pg=2");
            }
            else if (p.Id == NavigationConfiguration.SystemNode.LaunchQuickPricerLink)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.ConfigHref = m_urlResolver("~/QuickPricerNonAnonymous.aspx?if=1");
            }
            else if (p.Id == NavigationConfiguration.SystemNode.LaunchQuickPricer2Link)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.ConfigHref = m_urlResolver("~/webapp/pml.aspx?source=PML&withinPipeline=1");
                p.addClass = "full-expand";
            }
            else if (p.Id == NavigationConfiguration.SystemNode.QuickPricerRateMonitorsLink)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.ConfigHref = m_urlResolver("~/QuickPricer2RateMonitors.aspx");
            }
            else if (p.Id == NavigationConfiguration.SystemNode.ImportLead)
            {
                p.addClass = "ImportLeadLink";
                p.ConfigHref = "#";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreatePurchaseLead)
            {
                p.ConfigHref = "#";
                p.addClass = "createPLeadLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreateRefiLead)
            {
                p.ConfigHref = "#";
                p.addClass = "createRLeadLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreateHeloc1stLienLead)
            {
                p.ConfigHref = "#";
                p.addClass = "createH1LeadLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreateHeloc2ndLienLead)
            {
                p.ConfigHref = "#";
                p.addClass = "createH2LeadLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.Create2ndLienLead)
            {
                p.ConfigHref = "#";
                p.addClass = "createSLeadLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.ImportLoan)
            {
                p.addClass = "ImportLoanLink";
                p.ConfigHref = "#";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreatePurchaseLoan)
            {
                p.ConfigHref = "#";
                p.addClass = "createPLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreateRefiLoan)
            {
                p.ConfigHref = "#";
                p.addClass = "createRLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreateHeloc1stLien)
            {
                p.ConfigHref = "#";
                p.addClass = "createH1Link";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.CreateHeloc2ndLien)
            {
                p.ConfigHref = "#";
                p.addClass = "createH2Link";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.Create2ndLien)
            {
                p.ConfigHref = "#";
                p.addClass = "createSLink";
                p.ConfigTarget = E_NavTarget.Self;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.Dashboard)
            {
                if (m_tpoConfig != null)
                {
                    p.ConfigTarget = E_NavTarget.WithinPage;
                    p.ConfigHref = m_urlResolver(m_tpoConfig.Url);
                }
            }
            else if (p.Id == NavigationConfiguration.SystemNode.PortalMode)
            {
                p.ConfigTarget = E_NavTarget.Self;
                p.ConfigHref = "#";
            }
            else if (p.Id == NavigationConfiguration.SystemNode.ManageUsers)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.ConfigHref = m_urlResolver("~/main/UserAdmin.aspx");
                p.addClass = "system";
            }
            else if (p.Id == NavigationConfiguration.SystemNode.OriginatingCompanyServiceCredentials)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.ConfigHref = m_urlResolver("~/main/OriginatingCompanyServiceCredentials.aspx");
                p.addClass = "system";
            }
            else if (p.Id == NavigationConfiguration.SystemNode.MyProfile)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.addClass = "system";
                p.ConfigHref = m_urlResolver("~/main/YourProfile.aspx?if=1");
            }
            else if (p.Id == NavigationConfiguration.SystemNode.NonQmQp)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.addClass = "full-expand non-qm";
                p.ConfigHref = m_urlResolver("~/webapp/NonQm.aspx?QuickPricer=true");
                p.IsEditableSystemPage = true;
                p.CanBeRenamed = true;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.BankStatementIncomeCalculator)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.addClass = "full-expand non-qm";
                p.ConfigHref = m_urlResolver("~/webapp/NonQm.aspx?BankStatementIncomeCalculator=true");
                p.IsEditableSystemPage = true;
                p.CanBeRenamed = true;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.ScenarioRequestForm)
            {
                p.ConfigTarget = E_NavTarget.WithinPage;
                p.addClass = "full-expand non-qm";
                p.ConfigHref = m_urlResolver("~/webapp/NonQm.aspx?ScenarioRequestForm=true");
                p.IsEditableSystemPage = true;
                p.CanBeRenamed = true;
            }
            else if (p.Id == NavigationConfiguration.SystemNode.NonQmToolkitFolder)
            {
                p.CanBeRenamed = true;
            }
        }
    }
}
