using System;
using System.Collections;
using System.Data;
using DataAccess;
namespace PriceMyLoan.DataAccess
{
	public class CPreparerOnlyData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPreparerOnlyData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfGetAgentOfRole");
            list.Add("sfGetPreparerOfForm");

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

		public CPreparerOnlyData(Guid fileId) : base(fileId, "CPreparerOnlyData", s_selectProvider)
		{
		}
	}
}
