using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CValidateForRunningPricingData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CValidateForRunningPricingData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfValidateForRunningPricing");
            list.Add("sLNm");
            list.Add("aBNm");
            list.Add("sLAmtCalc");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sHcltvR");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CValidateForRunningPricingData(Guid fileId) : base(fileId, "CValidateForRunningPricingData", s_selectProvider)
		{
		}
	}
}
