using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CLoanProspectorLoginData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLoanProspectorLoginData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sFreddieLoanId");
            list.Add("sFreddieTransactionId");
            list.Add("sLpAusKey");
            list.Add("sFreddieSellerNum");
            list.Add("sFreddieTpoNum");
            list.Add("sFreddieLpPassword");
            list.Add("sFreddieNotpNum");
            list.Add("sFredProcPointT");
            list.Add("sHasFreddieInfileCredit");
            list.Add("aBNm");
            list.Add("aCNm");
            list.Add("aBSsn");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLoanProspectorLoginData(Guid fileId) : base(fileId, "CLoanProspectorLoginData", s_selectProvider)
		{
		}
	}
}
