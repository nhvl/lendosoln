using System;
using System.Collections;
using System.Data;
using DataAccess;

using LODataAccess = DataAccess;
namespace PriceMyLoan.DataAccess
{
	public class CPmlCertificateData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPmlCertificateData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            LODataAccess.DataAccessUtils.AddCertificateFields(list);
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPmlCertificateData(Guid fileId) : base(fileId, "CPmlCertificateData", s_selectProvider)
		{
		}
	}
}
