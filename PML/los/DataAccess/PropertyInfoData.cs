using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace PriceMyLoan.DataAccess
{
	public class CPropertyInfoData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPropertyInfoData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddLoanSummaryFields(list);
            list.Add("sOccRPe");
            list.Add("sSpGrossRentPe");
            list.Add("sOccTPe");
            list.Add("aPresOHExpPe");
            list.Add("sApprTPe");
            list.Add("sProdCondoStories");
            list.Add("sProdSpT");
            list.Add("sSpStatePe");
            list.Add("sIsIOnlyPe");
            list.Add("sLAmtCalcPe");
            list.Add("sLPurposeTPe");
            list.Add("sLienPosTPe");
            list.Add("sProOFinBalPe");
            list.Add("sProOFinPmtPe");
            list.Add("sProdCashoutAmt");
            list.Add("sProdImpound");
            list.Add("sProdRLckdDays");
            list.Add("sHouseValPe");
            list.Add("sProdCalcEntryT");
            list.Add("sDownPmtPcPe");
            list.Add("sEquityPe");
            list.Add("sLtvRPe");
            list.Add("sCltvRPe");
            list.Add("sLtvROtherFinPe");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sProdDocT");
            list.Add("sFannieDocT");
            list.Add("sIsOFinNewPe");
            list.Add("sProdHasExistingOFin");

            list.Add("sStatusT");
            list.Add("sProdIsSpInRuralArea");
            list.Add("sProdFinMethFilterT");
            list.Add("sProdPpmtPenaltyMon2ndLien");
            list.Add("sProdSpStructureT");
            list.Add("sProdIsCondotel");
            list.Add("sProdIsNonwarrantableProj");
            list.Add("sPrimAppTotNonspIPe");
            list.Add("sHas1stTimeBuyerPe");
            list.Add("sProdLpePriceGroupId");
            list.Add("sProdAvailReserveMonths");
            list.Add("sPml1stVisitPropLoanStepD");
            list.Add("sProd3rdPartyUwResultT");
            list.Add("sSpZip");
            list.Add("sSpAddr");
            list.Add("sSpCity");
            list.Add("sSpCounty");
			list.Add("sProdMIOptionT");
			list.Add("sLtv80TestResultT");
			list.Add("sProdHasHousingHistory");
            list.Add("sProdFilterDue10Yrs");
			list.Add("sProdFilterDue15Yrs");
            list.Add("sProdFilterDue20Yrs");
            list.Add("sProdFilterDue25Yrs");
            list.Add("sProdFilterDue30Yrs");			
			list.Add("sProdFilterDueOther");          
			list.Add("sProdFilterFinMethFixed");      			
			list.Add("sProdFilterFinMeth3YrsArm");    
			list.Add("sProdFilterFinMeth5YrsArm");
            list.Add("sProdFilterFinMeth7YrsArm");
            list.Add("sProdFilterFinMeth10YrsArm");
			list.Add("sProdFilterFinMethOther");
            list.Add("sProdFilterPmtTPI");
            list.Add("sProdFilterPmtTIOnly");
            list.Add("sProdEstimatedResidualI");
			list.Add("sProd3rdPartyUwProcessingT"); // 04/13/07 mf - OPM 12103 - AU Processing
			list.Add("sLpIsNegAmortOtherLien");
			list.Add("sOtherLFinMethT");
            list.Add("sProdIncludeMyCommunityProc");
            list.Add("sProdIncludeHomePossibleProc");
            list.Add("sProdIncludeNormalProc");
            list.Add("sProdIncludeFHATotalProc");
            list.Add("sProdIncludeVAProc");
            list.Add("sProdIncludeUSDARuralProc");
			//list.Add("sAgentCollection"); //opm 2703 fs 08/26/08
			list.Add("sHas2ndFinPe"); //opm 25227 fs 11/18/08
            list.Add("sProdLpePriceGroupNm");
            list.Add("sProdIsFhaMipFinanced"); //opm 28086 fs 03/03/09
            list.Add("sProdIsDuRefiPlus"); // 4/10/2009 dd - OPM 29134
            
            //opm 25872 fs 04/23/09
            list.Add("nApps");
            list.Add("aAppNm");
            list.Add("aAppId");
            list.Add("aBHasSpouse");
            list.Add("aCLastNm");
            list.Add("aCFirstNm");

            //opm 25872 fs 04/23/09
            list.Add("aTransmOMonPmtPe");
            list.Add("aProdCrManualNonRolling30MortLateCount");
            list.Add("aProdCrManualRolling60MortLateCount");
            list.Add("aProdCrManualRolling90MortLateCount");
            list.Add("aProdCrManual120MortLateCount");
            list.Add("aProdCrManual150MortLateCount");
            list.Add("aProdCrManual30MortLateCount");
            list.Add("aProdCrManual60MortLateCount");
            list.Add("aProdCrManual90MortLateCount");
            list.Add("aProdCrManualBk13Has");
            list.Add("aProdCrManualBk13RecentFileMon");
            list.Add("aProdCrManualBk13RecentFileYr");
            list.Add("aProdCrManualBk13RecentSatisfiedMon");
            list.Add("aProdCrManualBk13RecentSatisfiedYr");
            list.Add("aProdCrManualBk13RecentStatusT");
            list.Add("aProdCrManualBk7Has");
            list.Add("aProdCrManualBk7RecentFileMon");
            list.Add("aProdCrManualBk7RecentFileYr");
            list.Add("aProdCrManualBk7RecentSatisfiedMon");
            list.Add("aProdCrManualBk7RecentSatisfiedYr");
            list.Add("aProdCrManualBk7RecentStatusT");
            list.Add("aProdCrManualForeclosureHas");
            list.Add("aProdCrManualForeclosureRecentFileMon");
            list.Add("aProdCrManualForeclosureRecentFileYr");
            list.Add("aProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("aProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("aProdCrManualForeclosureRecentStatusT");

            list.Add("aBIsPrimaryWageEarner");
            list.Add("aCIsPrimaryWageEarner");
            list.Add("sPresOHExpPe");
            list.Add("sProdIsTexas50a6Loan");
            list.Add("sPreviousLoanIsTexas50a6Loan");
            list.Add("sIsCreditQualifying");

            list.Add("sMipFrequency");
            list.Add("sProdFhaUfmip");
            list.Add("sProdIsVaFundingFinanced");
            list.Add("sProdVaFundingFee");
            list.Add("sSpLien");
            list.Add("sFhaCondoApprovalStatusT");
            list.Add("sProdIsUsdaRuralHousingFeeFinanced");
            list.Add("sHasAppraisal");
            list.Add("sOriginalAppraisedValue");
            list.Add("sFHASalesConcessions");
            list.Add("sfUpdate_sProMInsR");
            list.Add("sLT");
            list.Add("sFHAPurposeIsStreamlineRefiWithAppr");
            list.Add("sFHAPurposeIsStreamlineRefiWithoutAppr");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sOriginatorCompensationBorrPaidPc");
            list.Add("sOriginatorCompensationBorrPaidBaseT");
            list.Add("sOriginatorCompensationBorrPaidMb");
            list.Add("sIsQualifiedForOriginatorCompensation");

            list.Add("sPriorSalesD");
            list.Add("sPriorSalesPrice");
            list.Add("sPriorSalesPropertySellerT");

            // Findings
            list.Add("sDuFindingsHtml");
            list.Add("sFreddieFeedbackResponseXml");
            list.Add("sTotalScoreCertificateXmlContent");
            list.Add("sSubFinPe");
            list.Add("sSubFinT");
            list.Add("sSubFin");
            list.Add("sConcurSubFin");
            list.Add("sSubFinToPropertyValue");
            list.Add("sHCLTVRPe");

            //OPM 80002: Occupancy data
            list.Add("sHasNonOccupantCoborrower");
            list.Add("sHasNonOccupantCoborrowerPe");
            
            // OPM 74766
            list.Add("sNumFinancedProperties");
            list.Add("sProdIsLoanEndorsedBeforeJune09");


            list.Add("sProRealETxPe");
            list.Add("sProOHExpPe");
            list.Add("ProRealETxPe");     
            list.Add("ProHazInsPe");       
            list.Add("sProHoAssocDuesPe"); 
            list.Add("sProMInsPe");        
            list.Add("sProOHExpPe");       
            list.Add("sMonthlyPmtPe");
            list.Add("sProOHExpDesc");

            list.Add("sIsRenovationLoan");
            list.Add("sTotalRenovationCosts");
            list.Add("sAsCompletedValuePeval");
            list.Add("sProdIncludeVAProc");
            list.Add("sProdRLckdExpiredDLabel");
            list.Add("sIsLineOfCredit");
            list.Add("sProdConvMIOptionT");
            list.Add("sConvSplitMIRT");

            // OPM 244633
            list.Add("sRequestCommunityOrAffordableSeconds");

            list.Add("sIsStandAlone2ndLien");
	        #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPropertyInfoData(Guid fileId) : base(fileId, "CPropertyInfoData", s_selectProvider)
		{
		}
	}
}
