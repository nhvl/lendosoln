using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace PriceMyLoan.DataAccess
{
    public class CDocTypeOnlyData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
        static CDocTypeOnlyData() 
        {
            StringList list = new StringList();
            list.Add("sProdDocT");
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }
        public CDocTypeOnlyData(Guid fileId) : base(fileId, "CDocTypeOnlyData", s_selectProvider) 
        {
        }
    }
    public class CStatusOnlyData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;

        static CStatusOnlyData() 
        {
            StringList list = new StringList();
            list.Add("sStatusT");
            list.Add("lpeRunModeT");
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }
        protected override bool m_enforceAccessControl
        {
            get
            {
                // 10/13/2010 dd - OPM 58031 - Allow Encompass Integration to change the sStatusT after register loan.
                return false;
            }
        }
        public CStatusOnlyData(Guid fileId) : base(fileId, "CStatusOnlyData", s_selectProvider) 
        {
        }
    }
    public class CLoanNameData : CPageData 
    {
        private static CSelectStatementProvider s_selectProvider;

        static CLoanNameData() 
        {
            StringList list = new StringList();
            list.Add("sLNm");
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }
        public CLoanNameData(Guid fileId) : base(fileId, "CLoanNameData", s_selectProvider) 
        {
        }
    }

	// OPM 24638
	public class CDataTracLoanProgramData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;

		static CDataTracLoanProgramData() 
		{
			StringList list = new StringList();
			list.Add("sDataTracLpId");
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}
		public CDataTracLoanProgramData(Guid fileId) : base(fileId, "CDataTracLoanProgramData", s_selectProvider) 
		{
		}
	}

	public class CPriceMyLoanData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPriceMyLoanData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("aBLastNm");
            list.Add("aCNm");
            list.Add("aBNm");
            list.Add("sLNm");
            list.Add("sEstCloseD");
            list.Add("sNoteIR");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPriceMyLoanData(Guid fileId) : base(fileId, "CPriceMyLoanData", s_selectProvider)
		{
		}
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }
	}
}
