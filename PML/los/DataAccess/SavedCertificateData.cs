using System;
using System.Collections;
using System.Data;

using DataAccess;
namespace PriceMyLoan.DataAccess
{
    public class CPmlSubmitStatusOnlyData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CPmlSubmitStatusOnlyData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sPmlSubmitStatusT");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CPmlSubmitStatusOnlyData(Guid fileId) : base(fileId, "CPmlSubmitStatusOnlyData", s_selectProvider)
        {
        }
    }
}
