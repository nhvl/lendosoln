using System;
using System.Collections;
using System.Data;

namespace DataAccess
{

	public class CPmlUsagePatternData : CPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPmlUsagePatternData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sPml1stVisitCreditStepD");
            list.Add("sPml1stVisitApplicantStepD");
            list.Add("sPml1stVisitPropLoanStepD");
            list.Add("sPml1stVisitResultStepD");
            list.Add("sPml1stSubmitD");
            list.Add("sPmlLastGenerateResultD");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }
	        
		public CPmlUsagePatternData(Guid fileId) : base(fileId, "CPmlUsagePatternData", s_selectProvider)
		{
		}
	}
}
