using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace PriceMyLoan.DataAccess
{

	//opm 11948 fs 08/12/08
	public class CPmlLoan_UpdateLockDays : CPmlPageData
	{
		private static CSelectStatementProvider s_selectProvider;

		private Hashtable m_modifiedFieldsHash = new Hashtable(20);
	    
		static CPmlLoan_UpdateLockDays()
		{
			StringList list = new StringList();
	        
	        #region Target Fields
			list.Add("sProdRLckdDays");
            list.Add("sProdRLckdExpiredDLabel");
	        #endregion
	        
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}        
	        
		public CPmlLoan_UpdateLockDays(Guid fileId) : base(fileId, "CLoan_UpdateLockDays", s_selectProvider)
		{
		}
        // 10/6/2010 dd - We do not need this hack anymore. The CPageData will automatically check for
        // bypass fields base on new Executing Engine Allowable Operation.
		//Override the Access control system.
        //protected override bool m_enforceAccessControl
        //{
        //    get { return false; }
        //}
        //override public void Save()
        //{
        //    if (dirtyFields != null)
        //    {
        //        if (dirtyFields.Count == 1 && dirtyFields["sProdRLckdDays"] != null)
        //            base.Save();
        //        else throw new PageDataSaveDenied(new Exception("Page data save denied: loan is past 'open' status and a field different than 'sProdRLckdDays' was modified => DenyWrite"));
        //    }
        //}
	}


}
