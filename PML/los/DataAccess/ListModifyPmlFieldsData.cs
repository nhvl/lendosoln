using System;
using System.Collections;
using System.Data;

using DataAccess;
namespace PriceMyLoan.DataAccess
{
	public class CListModifyPmlFieldsData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CListModifyPmlFieldsData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sfListModifyPmlData");
            list.Add("sfRetrievePmlSnapshotAuditInformation");
			list.Add("sIsStandAlone2ndLien");
            list.Add("sFfUfmipFinanced");
            list.Add("sMipFrequency");
            list.Add("sProdFhaUfmip");
            list.Add("sFfUfmipIsBeingFinanced");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CListModifyPmlFieldsData(Guid fileId) : base(fileId, "CListModifyPmlFieldsData", s_selectProvider)
		{
		}
	}
}
