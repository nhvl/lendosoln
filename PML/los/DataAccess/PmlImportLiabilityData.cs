using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CPmlImportLiabilityData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPmlImportLiabilityData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
	        list.Add("sfImportLiabilitiesFromCreditReport");
			list.Add("sfImportPublicRecordsFromCreditReport");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CPmlImportLiabilityData(Guid fileId) : base(fileId, "CPmlImportLiabilityData", s_selectProvider)
		{
		}
	}
}
