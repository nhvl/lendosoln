using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace PriceMyLoan.DataAccess
{
    public class CFill8020Loan : CPmlPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CFill8020Loan()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            //list.Add("sFinMethTPe");
            list.Add("sIsIOnlyPe");
            list.Add("sLAmtCalcPe");
            list.Add("sLPurposeTPe");
            list.Add("sProOFinBalPe");
            list.Add("sProdCashoutAmt");
            list.Add("sProdImpound");
            list.Add("sProdRLckdDays");
            list.Add("sHouseValPe");
            list.Add("sProdCalcEntryT");
            list.Add("sDownPmtPcPe");
            list.Add("sEquityPe");
            list.Add("sLtvRPe");
            list.Add("sCltvRPe");
            list.Add("sLtvROtherFinPe");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sIsOFinNewPe");
            list.Add("sProdHasExistingOFin");
            list.Add("sfFillOut8020PmlLoan");

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CFill8020Loan(Guid fileId) : base(fileId, "CFill8020Loan", s_selectProvider)
        {
        }
    }
	public class CLoanInformationData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CLoanInformationData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            //list.Add("sFinMethTPe");
            list.Add("sIsIOnlyPe");
            list.Add("sLAmtCalcPe");
            list.Add("sLPurposeTPe");
            list.Add("sLienPosTPe");
            list.Add("sProOFinBalPe");
            list.Add("sProOFinPmtPe");
            list.Add("sProdCashoutAmt");
            list.Add("sProdImpound");
            list.Add("sProdRLckdDays");
            list.Add("sHouseValPe");
            list.Add("sProdCalcEntryT");
            list.Add("sDownPmtPcPe");
            list.Add("sEquityPe");
            list.Add("sLtvRPe");
            list.Add("sCltvRPe");
            list.Add("sLtvROtherFinPe");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sIsOFinNewPe");
            list.Add("sProdHasExistingOFin");
            list.Add("sProOHExpPe");
            list.Add("sProRealETxPe");
            list.Add("sLienPosT");
			list.Add("sHas2ndFinPe"); //opm 25227 fs 11/20/08
            list.Add("sIsCreditQualifying"); //opm 32180 fs 11/23/09
            list.Add("sPrimAppTotNonspIPe"); //opm 32180 fs 11/23/09
            list.Add("sProdEstimatedResidualI"); //opm 32180 fs 11/23/09
            list.Add("sProdAvailReserveMonths"); //opm 32180 fs 11/23/09
            list.Add("sSpLien");
            list.Add("sProdVaFundingFee");
            list.Add("sProdIsVaFundingFinanced");
            list.Add("sHasAppraisal");
            list.Add("sOriginalAppraisedValue");
            list.Add("sSubFinPe");
            list.Add("sSubFinToPropertyValue");
            list.Add("sHCLTVRPe");
            list.Add("sProdIsLoanEndorsedBeforeJune09");
            list.Add("sSubFinT");

            // OPM 244633
            list.Add("sRequestCommunityOrAffordableSeconds");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CLoanInformationData(Guid fileId) : base(fileId, "CLoanInformationData", s_selectProvider)
		{
		}
	}
}
