using System;
using DataAccess;
using PriceMyLoan.Security;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace PriceMyLoan.los.DataAccess
{
	/// <summary>
	/// Summary description for PmlAddLiabilityData.
	/// </summary>
	public sealed class PmlAddLiabilityData
	{

		private Guid m_loanId;
        private Guid m_appId;
		private AbstractUserPrincipal m_pmlUser;
		
		private CPageData m_dataLoan;
		private ILiabilityRegular m_liability;
		

		public PmlAddLiabilityData(Guid loanId, Guid appId, AbstractUserPrincipal PmlUser)
		{
            if (loanId == Guid.Empty)
            {
                CBaseException exc = new CBaseException(ErrorMessages.Generic, "PmlAddLiabilityData() called with an invalid parameter. loanId");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
            if (appId == Guid.Empty)
            {
                CBaseException exc = new CBaseException(ErrorMessages.Generic, "PmlAddLiabilityData() called with an invalid parameter. appId");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
			if (PmlUser == null)
            {
                CBaseException exc = new CBaseException(ErrorMessages.Generic, "PmlAddLiabilityData() called with an invalid parameter. pmluser");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
			 
			try
			{
				m_loanId = loanId;
                m_appId = appId;
				m_pmlUser = PmlUser;
			}
			catch(CBaseException exc)
			{
                Tools.LogError("Error initializing PmlAddLiabilityData object.", exc);
				throw;
			}
			
		}

		public Guid Add(int version, E_DebtRegularT debtType, E_LiaOwnerT ownerType, string balance, string payment, string Nm, string Addr, string City, string State, string Zip, string AccountNm, string AccountNum, bool ratio, bool paid, Guid matchedReRecordId = new Guid())
        {
			if (balance == null || payment == null || Nm == null || Addr == null || City == null || State == null || Zip == null || AccountNm == null || AccountNum == null)
				throw new ArgumentException("Add() method called with an invalid parameter.");

            m_dataLoan = new CLiaData(m_loanId, true);
            m_dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            m_dataLoan.InitSave(version);

            CAppData dataApp = m_dataLoan.GetAppData(m_appId); // Always get primary application
            ILiaCollection recordList = dataApp.aLiaCollection;
            var record = recordList.AddRegularRecord();
            m_liability = record;

            m_liability.DebtT = debtType;
            m_liability.OwnerT = ownerType;
            m_liability.Bal_rep = balance;
            m_liability.Pmt_rep = payment;
            m_liability.ComNm = Nm;
            m_liability.NotUsedInRatio = !ratio;
            m_liability.WillBePdOff = paid;
            m_liability.ComAddr = Addr;
            m_liability.ComCity = City;
            m_liability.ComState = State;
            m_liability.ComZip = Zip;
            m_liability.AccNm = AccountNm;
            m_liability.AccNum = AccountNum;

            if (matchedReRecordId != Guid.Empty)
            {
                m_liability.MatchedReRecordId = matchedReRecordId;
            }

            m_liability.LiabilityCreationAudit(m_pmlUser.DisplayName, m_pmlUser.LoginNm);
            m_liability.Update();
            m_dataLoan.Save();

            return m_liability.RecordId;

		}

        ////For testing purposed only.
        public void AddEmpty(int version)
        {
            this.Add(version, E_DebtRegularT.Installment, E_LiaOwnerT.Borrower, "", "", "", "", "", "", "", "", "", true, false);
        }
	}
}
