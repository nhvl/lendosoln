using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace PriceMyLoan.DataAccess
{
	public class CManualCreditData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CManualCreditData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("aBExperianScorePe");
            list.Add("aBTransUnionScorePe");
            list.Add("aBEquifaxScorePe");
            list.Add("aCExperianScorePe");
            list.Add("aCTransUnionScorePe");
            list.Add("aCEquifaxScorePe");
            list.Add("sTransmOMonPmtPe");
            list.Add("sStatusT");
            list.Add("sProdCrManual120MortLateCount");
            list.Add("sProdCrManual150MortLateCount");
            list.Add("sProdCrManual30MortLateCount");
            list.Add("sProdCrManual60MortLateCount");
            list.Add("sProdCrManual90MortLateCount");
            list.Add("sProdCrManualBk13Has");
            list.Add("sProdCrManualBk13RecentFileMon");
            list.Add("sProdCrManualBk13RecentFileYr");
            list.Add("sProdCrManualBk13RecentSatisfiedMon");
            list.Add("sProdCrManualBk13RecentSatisfiedYr");
            list.Add("sProdCrManualBk13RecentStatusT");
            list.Add("sProdCrManualBk7Has");
            list.Add("sProdCrManualBk7RecentFileMon");
            list.Add("sProdCrManualBk7RecentFileYr");
            list.Add("sProdCrManualBk7RecentSatisfiedMon");
            list.Add("sProdCrManualBk7RecentSatisfiedYr");
            list.Add("sProdCrManualBk7RecentStatusT");
            list.Add("sProdCrManualForeclosureHas");
            list.Add("sProdCrManualForeclosureRecentFileMon");
            list.Add("sProdCrManualForeclosureRecentFileYr");
            list.Add("sProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("sProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("sProdCrManualForeclosureRecentStatusT");
            list.Add("sProdCrManualNonRolling30MortLateCount");
            list.Add("sProdCrManualRolling60MortLateCount");
			list.Add("sProdCrManualRolling90MortLateCount");

            list.Add("aBHasSpouse");	//opm 4382 fs 07/23/08
            list.Add("aBFirstNm");      //opm 25872 fs 04/18/09
            list.Add("aBLastNm");
            list.Add("aBSsn");
            list.Add("aCFirstNm");
            list.Add("aCLastNm");
            list.Add("aCSsn");
            list.Add("sIsSelfEmployed");
            list.Add("sHas1stTimeBuyerPeval");

            list.Add("aTransmOMonPmtPe");
            list.Add("aProdCrManual120MortLateCount");
            list.Add("aProdCrManual150MortLateCount");
            list.Add("aProdCrManual30MortLateCount");
            list.Add("aProdCrManual60MortLateCount");
            list.Add("aProdCrManual90MortLateCount");
            list.Add("aProdCrManualBk13Has");
            list.Add("aProdCrManualBk13RecentFileMon");
            list.Add("aProdCrManualBk13RecentFileYr");
            list.Add("aProdCrManualBk13RecentSatisfiedMon");
            list.Add("aProdCrManualBk13RecentSatisfiedYr");
            list.Add("aProdCrManualBk13RecentStatusT");
            list.Add("aProdCrManualBk7Has");
            list.Add("aProdCrManualBk7RecentFileMon");
            list.Add("aProdCrManualBk7RecentFileYr");
            list.Add("aProdCrManualBk7RecentSatisfiedMon");
            list.Add("aProdCrManualBk7RecentSatisfiedYr");
            list.Add("aProdCrManualBk7RecentStatusT");
            list.Add("aProdCrManualForeclosureHas");
            list.Add("aProdCrManualForeclosureRecentFileMon");
            list.Add("aProdCrManualForeclosureRecentFileYr");
            list.Add("aProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("aProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("aProdCrManualForeclosureRecentStatusT");
            list.Add("aProdCrManualNonRolling30MortLateCount");
            list.Add("aProdCrManualRolling60MortLateCount");
            list.Add("aProdCrManualRolling90MortLateCount");

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CManualCreditData(Guid fileId) : base(fileId, "CManualCreditData", s_selectProvider)
		{
		}
	}
}
