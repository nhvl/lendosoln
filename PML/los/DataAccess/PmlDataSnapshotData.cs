using System;
using System.Collections;
using System.Data;

namespace DataAccess
{
	public class CPmlDataSnapshotData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CPmlDataSnapshotData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfTakeSnapshotOfPmlDataBeforeRunningPricing");
            list.Add("sMipFrequency");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }	        
		public CPmlDataSnapshotData(Guid fileId) : base(fileId, "CPmlDataSnapshotData", s_selectProvider)
		{
		}
	}
}
