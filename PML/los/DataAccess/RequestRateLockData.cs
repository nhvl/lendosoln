using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace DataAccess
{
    public class CHackGetFirstLoanSubmitInformation : CPmlPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CHackGetFirstLoanSubmitInformation()
        {
            StringList list = new StringList();
	        
	        #region Target Fields

            list.Add("sLpTemplateNm");
            list.Add("sLoanProductIdentifier");
            list.Add("sNoteIRSubmitted");
            list.Add("sProThisMPmt");
            list.Add("sQualTopR");
            list.Add("sQualBottomR");
            list.Add("sProOFinPmtPe");
            list.Add("sLOrigFPcSubmitted");
            list.Add("sProThisMPmt");
            list.Add("sRAdjMarginR_rep");
            list.Add("sfRunLpe");
            list.Add("sLpIsArmMarginDisplayed");
            list.Add("sLpeRateOptionIdOf1stLienIn8020");
            list.Add("sTempLpeTaskMessageXml");
            list.Add("sLpTemplateId");
            list.Add("sQualIR");
            list.Add("sOptionArmTeaserR");
            list.Add("sPriceGroup");
            #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CHackGetFirstLoanSubmitInformation(Guid fileId) : base(fileId, "CHackGetFirstLoanSubmitInformation", s_selectProvider)
        {
        }
    }
    public class CGetFirstLoanSubmitInformation : CPmlPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
        static CGetFirstLoanSubmitInformation() 
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sLpTemplateId");
            list.Add("sLpTemplateNm");
            list.Add("sNoteIRSubmitted");
            list.Add("sLOrigFPcSubmitted");
            list.Add("sRAdjMarginRSubmitted");
            list.Add("sTermSubmitted");
            list.Add("sDueSubmitted");
            list.Add("sFinMethTSubmitted");
            list.Add("sIsOFinNewPe");
            list.Add("sProOFinPmtPe");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }
        public CGetFirstLoanSubmitInformation(Guid fileId) : base(fileId, "CGetFirstLoanSubmitInformation", s_selectProvider)  
        {
        }
    }

    public class CCheckFor2ndLoan : CPmlPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
        static CCheckFor2ndLoan() 
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            PriceMyLoan.DataAccess.DataAccessUtils.AddLoanSummaryFields(list);
            list.Add("sIsOFinNewPe");
            list.Add("sProOFinBalPe");
            list.Add("sLienPosTPe");
			list.Add("sProdHasExistingOFin");
			list.Add("sLPurposeT");
            list.Add("sIs8020PricingScenario");
            list.Add("sLinkedLoanInfo");
            list.Add("sProdFinMethFilterT");
            list.Add("sProdRLckdDays");
            list.Add("sProdPpmtPenaltyMon");
            list.Add("sProdPpmtPenaltyMon2ndLien");
            list.Add("sProdLpePriceGroupId");
            list.Add("sLpTemplateId");
            list.Add("sPml1stVisitResultStepD");
            list.Add("aIsCreditReportOnFile");
            list.Add("sLinkedLoanInfo");
            list.Add("sTempLpeTaskMessageXml");
            list.Add("lpeRunModeT");
			list.Add("sProdDocT");
            list.Add("sProdFilterDue15Yrs");
            list.Add("sProdFilterDue20Yrs");
            list.Add("sProdFilterDue30Yrs");            
            list.Add("sProdFilterDueOther");
            list.Add("sProdFilterFinMethFixed");
            list.Add("sProdFilterFinMethOptionArm");            
            list.Add("sProdFilterFinMeth3YrsArm");
            list.Add("sProdFilterFinMeth5YrsArm");
            list.Add("sProdFilterFinMeth7YrsArm");
            list.Add("sProdFilterFinMeth10YrsArm");
            list.Add("sProdFilterFinMethOther");
			list.Add("sEmployeeLenderAccExecId"); // added for loan pricing ran event.
            list.Add("sIOnlyMon");
            list.Add("sLienPosT");
            list.Add("sBranchId");
            list.Add("sfGetLoanProgramsToRunLpe");
            list.Add("sIsCreditQualifying"); //opm 32180 fs 11/19/09
            list.Add("sfGetAgentOfRole");
            list.Add("sEmployeeLoanRepId");
            list.Add("sOriginatorCompPoint");
            list.Add("sIsQualifiedForOriginatorCompensation");
            list.Add("sOriginatorCompensationPaymentSourceT");
            list.Add("sOriginatorCompensationLenderFeeOptionT");
            list.Add("sSubFinT");
            list.Add("sSubFinPe");
            list.Add("sfGetPricingStates");
            list.Add("sfGetLoanMatchingPricingStates");
            list.Add("sPriceGroup");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sStatusT");
            list.Add("sPriceGroup");
            list.Add("sSelectedProductCodeFilter");
            list.Add("sIsRenovationLoan");
            list.Add("sConstructionPurposeT");
            list.Add("sLoanVersionT");
            list.Add("sGseTargetApplicationT");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }
        public CCheckFor2ndLoan(Guid fileId) : base(fileId, "CCheckFor2ndLoan", s_selectProvider)  
        {
        }
    }
    public class CSubmitMainLoanFromPMLData : CPmlPageData 
    {
        private static CSelectStatementProvider s_selectProvider;
        static CSubmitMainLoanFromPMLData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            list.Add("sfSubmitMainLoanFromPML");
	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CSubmitMainLoanFromPMLData(Guid fileId) : base(fileId, "CSubmitMainLoanFromPMLData", s_selectProvider)
        {
        }
    }

    public class CRequestRateLockData : CPmlPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CRequestRateLockData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeUnderwriter");
            list.Add("sPmlCertXmlContent");
            list.Add("sLNm");
            list.Add("aBNm_acNm");
            list.Add("sfApplyLoanProductTemplate");
            list.Add("sNoteIR");
            list.Add("sLTotI");
            list.Add("sBrokComp1Lckd");
            list.Add("sBrokComp1MldsLckd");
            list.Add("sBrokComp1Desc");
            list.Add("sBrokComp1Pc");
            list.Add("sRAdjMarginR");
            list.Add("sBranchId");
            list.Add("sBranchChannelT");
            list.Add("sCorrespondentProcessT");
            list.Add("sTransNetCash");
            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

        public CRequestRateLockData(Guid fileId) : base(fileId, "CRequestRateLockData", s_selectProvider)
        {
        }
    }
}
