using System;
using System.Collections;
using System.Data;
using DataAccess;

namespace PriceMyLoan.DataAccess
{
    public class DataAccessUtils 
    {
        /// <summary>
        /// Add dependency fields for Loan Summary section. Loan #, Borrower Name, Loan Amount, LTV, CLTV, Score1, Score2
        /// </summary>
        /// <param name="list"></param>
        public static void AddLoanSummaryFields(StringList list) 
        {
            list.Add("sLNm");
            list.Add("aBNm");
            list.Add("sLAmtCalc");
            list.Add("sLtvR");
            list.Add("sCltvR");
            list.Add("sCreditScoreType1");
            list.Add("sCreditScoreType2");
        }
    }
    public class CEnterCreditData : CPmlPageData
    {
        private static CSelectStatementProvider s_selectProvider;
	    
        static CEnterCreditData()
        {
            StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddLoanSummaryFields(list);
            list.Add("sfInitNewLoanForPml");
            list.Add("sLNm");
            list.Add("aBNm");
            list.Add("aCNm");
            list.Add("aBSsn");
            list.Add("aCSsn");
            list.Add("sIsSelfEmployed");
            list.Add("aIsBorrSpousePrimaryWageEarner");
            list.Add("sOpNegCfPe");
            list.Add("sPrimAppTotNonspI");
            list.Add("aBExperianScorePe");
            list.Add("aBTransUnionScorePe");
            list.Add("aBEquifaxScorePe");
            list.Add("aCExperianScorePe");
            list.Add("aCTransUnionScorePe");
            list.Add("aCEquifaxScorePe");
            list.Add("aProdBCitizenT");
            list.Add("sStatusT");
            list.Add("sTransmOMonPmtPe");
            list.Add("sLiaMonLTot");
            list.Add("sHas1stTimeBuyerPe");
            list.Add("aIsCreditReportOnFile");
            list.Add("aBAddr");
            list.Add("aBCity");
            list.Add("aBState");
            list.Add("aBZip");
            list.Add("aBDob");
            list.Add("aCDob");
            list.Add("sfTransformDataToPml");
            list.Add("sProdCrManual120MortLateCount");
            list.Add("sProdCrManual150MortLateCount");
            list.Add("sProdCrManual30MortLateCount");
            list.Add("sProdCrManual60MortLateCount");
            list.Add("sProdCrManual90MortLateCount");
            list.Add("sProdCrManualBk13Has");
            list.Add("sProdCrManualBk13RecentFileMon");
            list.Add("sProdCrManualBk13RecentFileYr");
            list.Add("sProdCrManualBk13RecentSatisfiedMon");
            list.Add("sProdCrManualBk13RecentSatisfiedYr");
            list.Add("sProdCrManualBk13RecentStatusT");
            list.Add("sProdCrManualBk7Has");
            list.Add("sProdCrManualBk7RecentFileMon");
            list.Add("sProdCrManualBk7RecentFileYr");
            list.Add("sProdCrManualBk7RecentSatisfiedMon");
            list.Add("sProdCrManualBk7RecentSatisfiedYr");
            list.Add("sProdCrManualBk7RecentStatusT");
            list.Add("sProdCrManualForeclosureHas");
            list.Add("sProdCrManualForeclosureRecentFileMon");
            list.Add("sProdCrManualForeclosureRecentFileYr");
            list.Add("sProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("sProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("sProdCrManualForeclosureRecentStatusT");
            list.Add("aLiaCollection");
            list.Add("sPml1stVisitCreditStepD");
            list.Add("aIsCreditReportManual");

            //opm 25872 fs 04/23/09
            list.Add("aTransmOMonPmtPe");
            list.Add("aProdCrManualNonRolling30MortLateCount");
            list.Add("aProdCrManualRolling60MortLateCount");
			list.Add("aProdCrManualRolling90MortLateCount");
            list.Add("aProdCrManual120MortLateCount");
            list.Add("aProdCrManual150MortLateCount");
            list.Add("aProdCrManual30MortLateCount");
            list.Add("aProdCrManual60MortLateCount");
            list.Add("aProdCrManual90MortLateCount");
            list.Add("aProdCrManualBk13Has");
            list.Add("aProdCrManualBk13RecentFileMon");
            list.Add("aProdCrManualBk13RecentFileYr");
            list.Add("aProdCrManualBk13RecentSatisfiedMon");
            list.Add("aProdCrManualBk13RecentSatisfiedYr");
            list.Add("aProdCrManualBk13RecentStatusT");
            list.Add("aProdCrManualBk7Has");
            list.Add("aProdCrManualBk7RecentFileMon");
            list.Add("aProdCrManualBk7RecentFileYr");
            list.Add("aProdCrManualBk7RecentSatisfiedMon");
            list.Add("aProdCrManualBk7RecentSatisfiedYr");
            list.Add("aProdCrManualBk7RecentStatusT");
            list.Add("aProdCrManualForeclosureHas");
            list.Add("aProdCrManualForeclosureRecentFileMon");
            list.Add("aProdCrManualForeclosureRecentFileYr");
            list.Add("aProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("aProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("aProdCrManualForeclosureRecentStatusT");
            list.Add("aAddrMail");
            list.Add("aCityMail");
            list.Add("aStateMail");
            list.Add("aZipMail");
            list.Add("aCAddrMail");
            list.Add("aCCityMail");
            list.Add("aCStateMail");
            list.Add("aCZipMail");

            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
        public CEnterCreditData(Guid fileId) : base(fileId, "CEnterCreditData", s_selectProvider)
        {
        }
    }

	public class CApplicantInfoData : CPmlPageData
	{
	    private static CSelectStatementProvider s_selectProvider;
	    
	    static CApplicantInfoData()
	    {
	        StringList list = new StringList();
	        
	        #region Target Fields
            DataAccessUtils.AddLoanSummaryFields(list);
            list.Add("aLiaCollection");
            list.Add("sfInitNewLoanForPml");
            list.Add("sLNm");
            list.Add("aBNm");
            list.Add("aCNm");
            list.Add("aBSsn");
            list.Add("aCSsn");
            list.Add("sIsSelfEmployed");
            list.Add("aIsBorrSpousePrimaryWageEarner");
            list.Add("sOpNegCfPe");
            list.Add("sPrimAppTotNonspI");
            list.Add("aBExperianScorePe");
            list.Add("aBTransUnionScorePe");
            list.Add("aBEquifaxScorePe");
            list.Add("aCExperianScorePe");
            list.Add("aCTransUnionScorePe");
            list.Add("aCEquifaxScorePe");
            list.Add("aProdBCitizenT");
            list.Add("sStatusT");
            list.Add("sLiaMonLTot");
            list.Add("sHas1stTimeBuyerPe");
            list.Add("aIsCreditReportOnFile");
            list.Add("aBAddr");
            list.Add("aBCity");
            list.Add("aBState");
            list.Add("aBZip");
			list.Add("aPublicRecordCollection");
            list.Add("sPml1stVisitApplicantStepD");

            list.Add("sProdCrManualNonRolling30MortLateCount");
            list.Add("sProdCrManualRolling60MortLateCount");
            list.Add("sProdCrManualRolling90MortLateCount");
            list.Add("sProdCrManual120MortLateCount");
            list.Add("sProdCrManual150MortLateCount");
            list.Add("sProdCrManual30MortLateCount");
            list.Add("sProdCrManual60MortLateCount");
            list.Add("sProdCrManual90MortLateCount");
            list.Add("sProdCrManualBk13Has");
            list.Add("sProdCrManualBk13RecentFileMon");
            list.Add("sProdCrManualBk13RecentFileYr");
            list.Add("sProdCrManualBk13RecentSatisfiedMon");
            list.Add("sProdCrManualBk13RecentSatisfiedYr");
            list.Add("sProdCrManualBk13RecentStatusT");
            list.Add("sProdCrManualBk7Has");
            list.Add("sProdCrManualBk7RecentFileMon");
            list.Add("sProdCrManualBk7RecentFileYr");
            list.Add("sProdCrManualBk7RecentSatisfiedMon");
            list.Add("sProdCrManualBk7RecentSatisfiedYr");
            list.Add("sProdCrManualBk7RecentStatusT");
            list.Add("sProdCrManualForeclosureHas");
            list.Add("sProdCrManualForeclosureRecentFileMon");
            list.Add("sProdCrManualForeclosureRecentFileYr");
            list.Add("sProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("sProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("sProdCrManualForeclosureRecentStatusT");

            //opm 25872 fs 04/23/09
            list.Add("aTransmOMonPmtPe");
            list.Add("aProdCrManualNonRolling30MortLateCount");
            list.Add("aProdCrManualRolling60MortLateCount");
            list.Add("aProdCrManualRolling90MortLateCount");
            list.Add("aProdCrManual120MortLateCount");
            list.Add("aProdCrManual150MortLateCount");
            list.Add("aProdCrManual30MortLateCount");
            list.Add("aProdCrManual60MortLateCount");
            list.Add("aProdCrManual90MortLateCount");
            list.Add("aProdCrManualBk13Has");
            list.Add("aProdCrManualBk13RecentFileMon");
            list.Add("aProdCrManualBk13RecentFileYr");
            list.Add("aProdCrManualBk13RecentSatisfiedMon");
            list.Add("aProdCrManualBk13RecentSatisfiedYr");
            
            list.Add("aProdCrManualBk13RecentStatusT");
            list.Add("aProdCrManualBk7Has");
            list.Add("aProdCrManualBk7RecentFileMon");
            list.Add("aProdCrManualBk7RecentFileYr");
            list.Add("aProdCrManualBk7RecentSatisfiedMon");
            list.Add("aProdCrManualBk7RecentSatisfiedYr");
            list.Add("aProdCrManualBk7RecentStatusT");
            list.Add("aProdCrManualForeclosureHas");
            list.Add("aProdCrManualForeclosureRecentFileMon");
            list.Add("aProdCrManualForeclosureRecentFileYr");
            
            list.Add("aProdCrManualForeclosureRecentSatisfiedMon");
            list.Add("aProdCrManualForeclosureRecentSatisfiedYr");
            list.Add("aProdCrManualForeclosureRecentStatusT");

            list.Add("aBIsSelfEmplmt");
            list.Add("aCIsSelfEmplmt");
            list.Add("aProdCCitizenT");
            list.Add("aBEmail");
            list.Add("aCEmail");
            list.Add("aCDob");
            list.Add("aBDob");

            list.Add("aBTotalScoreIsFthb");
            list.Add("aCTotalScoreIsFthb");
            list.Add("aBHasHousingHist");
            list.Add("aCHasHousingHist");
            list.Add("afGetApplicableCitizenshipValues");

	        #endregion
	        
            s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

        }        
	        
		public CApplicantInfoData(Guid fileId) : base(fileId, "CApplicantInfoData", s_selectProvider)
		{
		}
	}

	public class CPublicRecordData : CPmlPageData
	{
		private static CSelectStatementProvider s_selectProvider;
	    
		static CPublicRecordData()
		{
			StringList list = new StringList();
	        
	        #region Target Fields
			list.Add("aPublicRecordCollection");

	        #endregion
	        
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );

		}
		public CPublicRecordData(Guid fileId) : base(fileId, "CPublicRecordData", s_selectProvider)
		{
		}

	}

    public class CPmlNewLoanData : CPmlPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static CPmlNewLoanData()
        {
            StringList list = new StringList();

            #region Target Fields
            list.Add("aBNm");
            list.Add("aBFirstNm");
            list.Add("aBLastNm");
            list.Add("aBSsn");
            list.Add("sLienPosT");
            list.Add("sLPurposeT");

            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }
        public CPmlNewLoanData(Guid fileId)
            : base(fileId, "CPmlNewLoanData", s_selectProvider)
        {
        }

    }

}
