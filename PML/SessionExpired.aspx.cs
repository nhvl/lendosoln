using System;
using PriceMyLoan.Common;
using LendersOffice.Constants;
using System.Web;

namespace PriceMyLoan
{
	public partial class SessionExpired : PriceMyLoan.UI.BasePage
	{
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected bool IsCitiBroker
        {
            get
            {
                return false; // 5/23/2013 dd - Citi Lender is no longer our client.
            }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            Response.Headers.Add("IsLogin", "true");
			// Put user code to initialize the page here
            if (!IsEncompassIntegration)
            {
                ExitPanel.Enabled = true;
                ExitPanel.Visible = true;
            } 
            else
            {
                ExitPanel.Enabled = false;
                ExitPanel.Visible = false;
            }


            LogoutPanel.Enabled = false;
            LogoutPanel.Visible = false;
            
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
