﻿namespace PriceMyLoan
{
    using System;
    using System.Web;
    using System.Web.UI;
    using Common;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.UI.Themes;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Security;

    /// <summary>
    /// Provides a means for sending customized CSS content to a client.
    /// </summary>
    public partial class StyleProvider : Page
    {
        /// <summary>
        /// Represents the URL to send the request when no principal data is available.
        /// </summary>
        private const string AnonymousProviderUrl = "AnonymousStyleProvider.aspx?lenderpmlsiteid=";

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var content = this.GetStyleContent();
            this.WriteContent(content);
        }

        /// <summary>
        /// Gets the style content.
        /// </summary>
        /// <returns>
        /// The style content.
        /// </returns>
        protected virtual string GetStyleContent()
        {
            if (PriceMyLoanConstants.IsEmbeddedPML)
            {
                return string.Empty;
            }

            var user = this.Context.User as PriceMyLoanPrincipal;

            // If we have no user, we're likely at a page where we 
            // can't pull specific theme data. Redirect to the anonymous
            // handler.
            if (user == null)
            {
                var lenderPmlSiteId = RequestHelper.GetGuid("lenderpmlsiteid", Guid.Empty);
                this.Response.Redirect(AnonymousProviderUrl + lenderPmlSiteId);
                return string.Empty;
            }

            // OPM 365311, 6/21/2017, ML
            // Navigation and pricing content needs to be split out of the master
            // SASS file to maintain the existing pricing page layout. This provider 
            // can be simplified to just return the master color theme CSS once the 
            // pricing page is updated to use material design.
            switch (RequestHelper.GetSafeQueryString("f"))
            {
                case "Master":
                    return ThemeManager.GetCssForColorTheme(user.BrokerId, user.TpoColorThemeId, ThemeCollectionType.TpoPortal);
                case "Navigation":
                    return ThemeManager.GetCssForNavigation(user.BrokerId, user.TpoColorThemeId);
                case "Pricing":
                    return SassDefaults.CompiledDefaultPricingCss;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Writes content to the client.
        /// </summary>
        /// <param name="content">
        /// The content to write.
        /// </param>
        private void WriteContent(string content)
        {
            this.Response.Clear();
            this.Response.ContentType = "text/css";

            this.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            this.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            this.Response.Cache.SetExpires(DateTime.Now.AddMinutes(ConstStage.SassCacheDuration.Minutes));
            this.Response.Cache.SetOmitVaryStar(true);

            this.Response.Write(content);

            this.Context.ApplicationInstance.CompleteRequest();
        }
    }
}