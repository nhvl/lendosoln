<%@ Page Language="c#" CodeBehind="logout.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.logout" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>logout</title>
    <script type='text/javascript'>
        function redirect(url)
        {
            try
            {
                if(window.parent.location.href && window.parent.location.href.search('/pml.aspx') >= 0) 
                {
                    window.parent.location.href = url;
                }
                else 
                {
                    window.location.href = url;
                }
            }
            catch(e)
            {
                window.location.href = url;
            }
        }
    </script>
</head>
<body ms_positioning="FlowLayout">

    <form id="logout" method="post" runat="server">
        <div>You have been logged out. If you are not redirected, please <a id="returnLink" runat="server" href="./">click here</a> to return to the home page.</div>
    </form>

</body>
</html>
