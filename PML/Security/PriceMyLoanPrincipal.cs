namespace PriceMyLoan.Security
{
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;

    using CommonProjectLib.Caching;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar.DataTypes;

    public class PriceMyLoanPrincipal : AbstractUserPrincipal
	{

        private Guid m_lastUsedCreditProtocolID;
		private string m_lastUsedCreditLoginName;

        private string m_email;
        private bool m_isPmlManager;
        private Guid m_pmlExternalManagerEmployeeID;

		private bool m_isPasswordExpired;

        public Guid LastUsedCreditProtocolID 
        {
            get { return m_lastUsedCreditProtocolID; }
        }

		public string LastUsedCreditLoginNm
		{
			get { return m_lastUsedCreditLoginName; }
		}

        public bool IsPmlManager 
        {
            get { return m_isPmlManager; }
        }

		public bool IsPasswordExpired 
		{
			get { return m_isPasswordExpired; }
		}

        public Guid PmlExternalManagerEmployeeID 
        {
            get { return m_pmlExternalManagerEmployeeID; }
        }
        

        public string Email 
        {
            get { return m_email; }
        }

        public bool IsAllowGoToPipeline
        {
            get
            {
                // 1/20/2005 dd - Return whether user able to click on "Start" button to go back to pipeline.
                // Currently only restrict generic Mortgage Leage user.
                // 6/29/2005 dd - Also hide ability to go back to pipeline for embedded PML from LO.
                // 2/6/2009 dd - Also add QuickPricer Anonymous log in.
                return !IsPmlMortgageLeagueLogin && !IsPmlQuickPricerAnonymousLogin && !PriceMyLoanConstants.IsEmbeddedPML;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the new TPO portal loan navigation 
        /// is enabled for the user.
        /// </summary>
        /// <value>
        /// True if the new navigation is enabled for the user, false otherwise.
        /// </value>
        public bool EnableNewTpoLoanNavigation => !PriceMyLoanConstants.IsEmbeddedPML;

        /// <summary>
        /// Gets a value indicating whether the "Disclosures" page will appear
        /// in the TPO portal loan navigation header.
        /// </summary>
        /// <value>
        /// True if the "Disclosures" page will appear in the header for the user,
        /// false otherwise.
        /// </value>
        public bool EnableDisclosurePageInTpoLoanNavigation
        {
            get
            {
                return this.BrokerDB.IsEnableDisclosurePageInNewTpoLoanNavigation(this.Type, this.PmlBrokerId, this.BranchId);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the "Order Initial Disclosures" button on the TPO 
        /// "Disclosures" page will be visible.
        /// </summary>
        /// <value>
        /// True if the "Order Initial Disclosures" button will be visible for the user,
        /// false otherwise.
        /// </value>
        public bool EnableOrderInitialDisclosuresButtonOnTpoDisclosuresPage
        {
            get
            {
                return this.BrokerDB.IsEnableOrderInitialDisclosuresButtonOnTpoDisclosuresPage(this.Type, this.PmlBrokerId, this.BranchId);
            }
        }

        /// <summary>
        /// Gets the color theme ID for the user.
        /// </summary>
        /// <value>
        /// The color theme ID for the user.
        /// </value>
        public Guid TpoColorThemeId => MemoryCacheUtilities.GetOrAddExisting(this.GetTpoThemeKey(), this.GetTpoColorThemeId, ConstStage.SassCacheDuration);

        public PriceMyLoanPrincipal(IIdentity identity, Guid brokerID, Guid branchID, Guid employeeID, string permissions, 
            Guid lastUsedCreditProtocolID, string firstName, string lastName, string email, Guid userID, 
            string loginNm, Guid loginSessionID, E_ApplicationT applicationT,
            bool isPmlManager, DateTime passwordExpirationD, Guid pmlExternalManagerEmployeeID, string[] roles, bool isOthersAllowedToEditUnderwriterAssignedFile, 
            bool isLOAllowedToEditProcessorAssignedFile, string type, Guid lpePriceGroupId, bool isRateLockedAtSubmission, 
            bool hasLenderDefaultFeatures, string lastUsedCreditLoginNm, bool isQuickPricerEnable,
            bool isPricingMultipleAppsSupported, string cookieSessionId, Guid pmlBrokerId, E_PmlLoanLevelAccess pmlAccess,
            bool isNewPmlUIEnabled, bool isUsePml2AsQuickPricer, E_PortalMode portalMode, Guid miniCorrespondentBranchId,
            Guid correspondentBranchId, List<string> ipWhiteList, HashSet<Guid> teams,
            InitialPrincipalTypeT initialPrincipalType, Guid initialUserId
            ) : 
            base (identity, roles, userID, brokerID, branchID, employeeID, loginSessionID, firstName, lastName, permissions, isOthersAllowedToEditUnderwriterAssignedFile, isLOAllowedToEditProcessorAssignedFile, lpePriceGroupId, isRateLockedAtSubmission
            , hasLenderDefaultFeatures, Guid.Empty /* becomeUserId */,
            isQuickPricerEnable, isPricingMultipleAppsSupported, cookieSessionId, pmlBrokerId, pmlAccess, type, applicationT, ipWhiteList, teams,
            isNewPmlUIEnabled, isUsePml2AsQuickPricer, portalMode, miniCorrespondentBranchId, correspondentBranchId, initialPrincipalType, initialUserId, PostLoginTask.Redirect)
        {
            m_lastUsedCreditProtocolID = lastUsedCreditProtocolID;
            m_lastUsedCreditLoginName = lastUsedCreditLoginNm;
            m_email = email;
            m_isPmlManager = isPmlManager;
            m_pmlExternalManagerEmployeeID = pmlExternalManagerEmployeeID;
            m_isPasswordExpired = DateTime.Now.CompareTo(passwordExpirationD) >= 0;
        }

        /// <summary>
        /// Gets a value indicating whether the current user has the given permission enabled.
        /// </summary>
        /// <param name="p">The permission to check.</param>
        /// <returns>True if the user has the given permission</returns>
        public override bool HasPermission(Permission p)
        {
            // Per case 245766. This permission is checked by the rest of the code but it is not reachable in the PML user editor. 
            if (p == Permission.CanViewEDocs)
            {
                return true;
            }

            return base.HasPermission(p);
        }

        /// <summary>
        /// Gets the key for the cached theme.
        /// </summary>
        /// <returns></returns>
        private string GetTpoThemeKey() => this.BrokerId.ToString("N") + "_" + this.UserId.ToString("N") + "_tpotheme";

        /// <summary>
        /// Gets the color theme ID for the user.
        /// </summary>
        /// <returns>
        /// The color theme ID for the user.
        /// </returns>
        /// <remarks>
        /// Small optimization: only load the broker's theme collection when
        /// necessary instead of up front.
        /// </remarks>
        private Guid GetTpoColorThemeId()
        {
            if (string.Equals("B", this.Type, StringComparison.OrdinalIgnoreCase))
            {
                // The retail portal always uses the broker's default theme.
                var themeCollection = ThemeManager.GetThemeCollection(this.BrokerId, ThemeCollectionType.TpoPortal);
                return themeCollection.SelectedThemeId;
            }

            var selectedOcThemeId = PmlBroker.RetrieveTpoColorThemeId(this.PmlBrokerId, this.BrokerId);
            return selectedOcThemeId ?? ThemeManager.GetThemeCollection(this.BrokerId, ThemeCollectionType.TpoPortal).SelectedThemeId;
        }
    }
}
