using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LqbGrammar.DataTypes;
using LqbGrammar.Drivers.SecurityEventLogging;
using PriceMyLoan.Common;
///
/// Author: David Dao
/// 
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web.Security;

namespace PriceMyLoan.Security
{

    public class PrincipalFactory
	{
		public enum E_LoginProblem
		{
			None = 0,					// successful login
			InvalidLoginPassword = 1,	// invalid login or password
			IsDisabled = 2,				// account is disabled
			IsLocked = 3,				// account is locked
			NeedsToWait = 4,			// user must wait before attempting to log in
			InvalidAndWait = 5			// invalid password and due to the current # of failures, now must wait before trying to log in again
		}

        public static IPrincipal CreateByUserId(Guid brokerId, Guid userId, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid())
        {
            // 6/29/2005 dd - Only embedded PML from inside LO invoke this method
            SqlParameter[] parameters = { new SqlParameter("@UserID", userId) };

            // 7/1/2005 dd - Embedded PML user principal will treat as LendersOffice application.
            return CreatePrincipal(brokerId, "Pml_RetrieveLoginInfoByUserID", parameters, Guid.Empty, false, E_ApplicationT.LendersOffice, true /*isStoreToCookies */, "" /* CookieSessionId */, initialPrincipalType: initialPrincipalType, initialUserId: initialUserId);
        }

        public static IPrincipal CreateByPmlUserId(Guid brokerId, Guid userId)
        {
            // Use by LOAdmin user to become P user.
            SqlParameter[] parameters = { new SqlParameter("@UserID", userId) };

            return CreatePrincipal(brokerId, "Pml_RetrieveLoginInfoByUserID", parameters, Guid.Empty, true, E_ApplicationT.PriceMyLoan, true /*isStoreToCookies */, "" /* CookieSessionId */);
        }

		public static IPrincipal CreateByUserID_SLOW(Guid userID, InitialPrincipalTypeT initialPrincipalType, Guid initialUserId) 
        {
            Guid brokerId = Guid.Empty;

            DbConnectionInfo.GetConnectionInfoByUserId(userID, out brokerId);

            // 6/29/2005 dd - Only embedded PML from inside LO invoke this method
            SqlParameter[] parameters = { new SqlParameter("@UserID", userID) };

            // 7/1/2005 dd - Embedded PML user principal will treat as LendersOffice application.
            return CreatePrincipal(brokerId, "Pml_RetrieveLoginInfoByUserID", parameters, Guid.Empty, false, E_ApplicationT.LendersOffice, true /*isStoreToCookies */, "" /* CookieSessionId */, initialPrincipalType: initialPrincipalType, initialUserId: initialUserId);
        }
        public static IPrincipal CreateByPmlUserID_SLOW(Guid userID, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid()) 
        {
            Guid brokerId = Guid.Empty;

            DbConnectionInfo.GetConnectionInfoByUserId(userID, out brokerId);
            // Use by LOAdmin user to become P user.
            SqlParameter[] parameters = { new SqlParameter("@UserID", userID) };

            return CreatePrincipal(brokerId, "Pml_RetrieveLoginInfoByUserID", parameters, Guid.Empty, true, E_ApplicationT.PriceMyLoan, true /*isStoreToCookies */, "" /* CookieSessionId */, initialPrincipalType: initialPrincipalType, initialUserId: initialUserId);
        }

		private static int IncrementLoginFailureCount(Guid brokerId, string userName, Guid BrokerPmlSiteId)
		{
			int loginFailureCount = 0;

            SqlParameter[] parameters = { 
                                                new SqlParameter("@LoginNm", userName),
                                                new SqlParameter("@Type", "P"), 
                                                new SqlParameter("@BrokerPmlSiteId", BrokerPmlSiteId)
                                            };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "UpdateLoginFailureCount", parameters))
            {
                if (reader.Read())
                {
                    loginFailureCount = Int32.Parse(reader["LoginFailureCount"].ToString());
                }
            }

			return loginFailureCount;
		}

        public static IPrincipal CreateWithFailureType(string userName, string password, Guid brokerPmlSiteID, out PrincipalFactory.E_LoginProblem loginProblem, bool isStoreToCookies, bool isAnonymousQP = false) 
		{
			return CreateWithFailureType(userName, password, brokerPmlSiteID, out loginProblem, false, isStoreToCookies, isAnonymousQP);
		}

		//3/26/07 db - OPM 10294 Notify users if their account has been disabled
		public static IPrincipal CreateWithFailureType(string userName, string password, Guid brokerPmlSiteID, out PrincipalFactory.E_LoginProblem loginProblem, bool allowDuplicateLogin, bool isStoreToCookies, bool isAnonymousQP = false) 
		{
            Guid brokerId = BrokerDB.GetBrokerIdByBrokerPmlSiteId(brokerPmlSiteID);

			//Check first if they're allowed to log in

			DateTime nextAvailableLoginTime = GetNextAvailableLoginTime(brokerId, userName, brokerPmlSiteID);
			if(nextAvailableLoginTime != LendersOffice.Common.SmallDateTime.MinValue)
			{
				if (nextAvailableLoginTime.Equals(LendersOffice.Common.SmallDateTime.MaxValue))				
				{
					loginProblem = E_LoginProblem.IsLocked;
					return null;
				}
				if(nextAvailableLoginTime > DateTime.Now)
				{
					loginProblem = E_LoginProblem.NeedsToWait;
					return null;
				}
			}
            IPrincipal principal = Create(brokerId, userName, password, brokerPmlSiteID, isStoreToCookies, isAnonymousQP);
			loginProblem = E_LoginProblem.None;

			if (principal == null)
			{
                var employeeDB = EmployeeDB.CreateEmployeeDBWithOnlyUserNamesByBrokerIdAndLoginName(brokerId, userName);
                AbstractUserPrincipal logPrincipal = null;

                if (employeeDB != null)
                {
                    logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, userName, employeeDB.FirstName, employeeDB.LastName, InitialPrincipalTypeT.NonInternal, "P", employeeDB.UserID);
                }

                if (IsDisabled(brokerId, userName, brokerPmlSiteID))
                {
                    loginProblem = E_LoginProblem.IsDisabled;
                    SecurityEventLogHelper.CreateAccountLockLog(logPrincipal);
                }
                else
                {
                    int loginFailureCount = IncrementLoginFailureCount(brokerId, userName, brokerPmlSiteID);
                    BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                    int numBadPw = broker.NumberBadPasswordsAllowed; // OPM 19920

                    if (loginFailureCount >= numBadPw)
                    {
                        LockAccount(brokerId, userName, brokerPmlSiteID, logPrincipal);
                        loginProblem = E_LoginProblem.IsLocked;
                    }
                    else if (loginFailureCount < 10)
                    {
                        SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                        loginProblem = E_LoginProblem.InvalidLoginPassword;
                    }
                    else
                    {
                        SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                        AddTimeToLoginBlock(brokerId, userName, Math.Floor((double)(loginFailureCount / 10)), brokerPmlSiteID);
                        loginProblem = E_LoginProblem.InvalidAndWait;
                    }
                }
			}
			return principal;
		}

		private static DateTime GetNextAvailableLoginTime(Guid brokerId, string userName, Guid brokerPmlSiteId)
		{
			DateTime nextAvailableLoginTime = LendersOffice.Common.SmallDateTime.MinValue;
			SqlParameter[] parameters = {
											new SqlParameter("@LoginNm", userName),
											new SqlParameter("@Type", "P"),
											new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId)
										};
			try
			{
				using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoginBlockExpirationDate", parameters)) 
				{
                    if (reader.Read())
                    {
                        nextAvailableLoginTime = DateTime.Parse(reader["LoginBlockExpirationD"].ToString());
                    }
				}
			}
			catch{}
			return nextAvailableLoginTime;
		}

		private static void LockAccount(Guid brokerId, string userName, Guid brokerPmlSiteId, AbstractUserPrincipal logPrincipal)
		{
			SqlParameter[] parameters = {
											new SqlParameter("@LoginNm", userName),
											new SqlParameter("@Type", "P"),
											new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId),
											new SqlParameter("@NextAvailableLoginTime", LendersOffice.Common.SmallDateTime.MaxValue)
										};
			StoredProcedureHelper.ExecuteNonQuery(brokerId, "SetLoginBlockExpirationDate",1, parameters);

            SecurityEventLogHelper.CreateAccountLockLog(logPrincipal);
        }

		private static bool IsDisabled(Guid brokerId, string userName, Guid brokerPmlSiteId)
		{
			SqlParameter[] parameters = { 
                                            new SqlParameter("@LoginName",userName), 
                                            new SqlParameter("@BrokerPmlSiteID", brokerPmlSiteId) 
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveDisabledPmlUserByLoginNm", parameters))
            {
                return reader.Read();
            }
		}

		private static void AddTimeToLoginBlock(Guid brokerId, string userName, double minutesToAdd, Guid brokerPmlSiteId)
		{
			if(minutesToAdd < 0)
				return;
			DateTime nextAvailableTime = DateTime.Now.AddMinutes(minutesToAdd);
			SqlParameter[] parameters = {
											new SqlParameter("@LoginNm", userName),
											new SqlParameter("@Type", "P"),
											new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId),
											new SqlParameter("@NextAvailableLoginTime", nextAvailableTime)
										};
			StoredProcedureHelper.ExecuteNonQuery(brokerId, "SetLoginBlockExpirationDate", 1,parameters);
		}

        public static IPrincipal Create(FormsIdentity identity, bool isAnonymousQP = false) 
        {
            // NOTE: UserData CANNOT exceed 1000 characters or it will not stored in cookies.
            // UserData = UserType|UserID|LoginSessionId|ApplicationType|BecomeUserId|CookieSessionId|BrokerId|InitialPrincipalType|InitialUserId | PostLoginTask
            string[] data = identity.Ticket.UserData.Split('|');
            // OPM 19222 -jM. now because of IP field  |IP Address <- populated in RequestHelper and length is now 7	
            // OPM 471099 - jk. IP address was removed in case 24152, & now we're at length 9 with InitialPrincipalType & InitialUserId
            // OPM 472306 - jk. length 10 with PostLoginTask
            if (isAnonymousQP)
            {
                // opm 451761 ejm - The anonymous QP has an extra part to it.
                if (data.Length != 11 || data[10] != "aqp")
                {
                    LendersOffice.Common.ErrorUtilities.DisplayErrorPage(new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Not using AQP ticket for AQP page."), false, Guid.Empty, Guid.Empty);
                }
            }
            else if (data.Length != 10) 
            {
                RequestHelper.ClearAuthenticationCookies();
                LendersOffice.Common.ErrorUtilities.DisplayErrorPage(new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Cookie has wrong data."), false, Guid.Empty, Guid.Empty);
            }
            
            string userType = data[0];
            Guid userID = new Guid(data[1]);
            Guid loginSessionID = new Guid(data[2]);
            string cookieSessionId = data[5];
            Guid brokerId = new Guid(data[6]);

            InitialPrincipalTypeT initialPrincipalType;
            Enum.TryParse(data[7], out initialPrincipalType);
            Guid initialUserId = Guid.Empty;
            Guid.TryParse(data[8], out initialUserId);

            if (LOCache.Get("COOKIE_SESSION_ID_" + cookieSessionId) != null)
            {
                // 10/28/2009 dd - This cookie session id mark as log out, how come it still being use. Return null.

                return null;
            }
            E_ApplicationT applicationT = data[3] == "0" ? E_ApplicationT.LendersOffice : E_ApplicationT.PriceMyLoan;
            
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserID", userID)
                                        };


            return CreatePrincipal(brokerId, "Pml_RetrieveLoginInfoByUserID", parameters, loginSessionID, false, applicationT, true /* isStoreToCookie */, cookieSessionId, isAnonymousQP, initialPrincipalType, initialUserId);
        }
        private static IPrincipal Create(Guid brokerId, string userName, string password, Guid brokerPmlSiteID, bool isStoreToCookies, bool isAnonymousQP) 
        {
            // OPM 194431 - Check Password before creating principal.
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            if (!Tools.IsPasswordValid(connInfo, userName, brokerPmlSiteID, password))
            {
                return null;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", userName),
                                            new SqlParameter("@BrokerPmlSiteID", brokerPmlSiteID)
                                        };
            return CreatePrincipal(brokerId, "Pml_ValidatePriceMyLoanLogin", parameters, Guid.Empty, false, E_ApplicationT.PriceMyLoan, isStoreToCookies, "" /* CookieSessionId */, isAnonymousQP);
        }
        public static IPrincipal CreateUsingMLAuthentication(string userName, Guid brokerPmlSiteID) 
        {
            Guid brokerId = BrokerDB.GetBrokerIdByBrokerPmlSiteId(brokerPmlSiteID);
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", userName),
                                            new SqlParameter("@BrokerPmlSiteID", brokerPmlSiteID)
                                        };
            return CreatePrincipal(brokerId, "Pml_ValidateMortgageLeagueAuthentication", parameters, Guid.Empty, true, E_ApplicationT.PriceMyLoan, true /* isStoreToCookie */, "" /* CookieSessionId */);

        }
        public static IPrincipal CreateUsingAnonymousQuickPricerUserId(Guid brokerPmlSiteId, Guid anonymousQuickPricerUserId)
        {
            Guid brokerId = BrokerDB.GetBrokerIdByBrokerPmlSiteId(brokerPmlSiteId);
            SqlParameter[] parameters = {
                                            new SqlParameter("@AnonymousQuickPricerUserId", anonymousQuickPricerUserId)
                                            , new SqlParameter("@BrokerPmlSiteID", brokerPmlSiteId)
                                        };
            return CreatePrincipal(brokerId, "Pml_RetrieveLoginInfoByAnonymousQuickPricerUserId", parameters, Guid.Empty, true, E_ApplicationT.PriceMyLoan, true /* isStoreToCookie */, "" /* CookieSessionId */, isAnonymousQP: true);

        }

        // OPM 28343 db - auth ID will now expire after 2 minutes, so we will check here if it's expired, and if so, return a null principal
        // The entry in the cache takes the auth ID as the cache ID and a DateTime representing the expiration time.
        // We do not clear the auth ID from the all_users table explicitly if the ID is expired because that would
        // require an extra hit to the large all_users table, and it gets cleared automatically anyways when the user
        // successfully logs in.  Therefore, it will still hang around (permanently unused) until the user successfully 
        // logs in, but that is worth the trade off in not having another db hit.
        private static bool IsAuthenticationIdExpired(Guid authenticationID)
        {
            string authIdExpiresAt = AutoExpiredTextCache.GetFromCache(authenticationID);

            if (!string.IsNullOrEmpty(authIdExpiresAt))
            {
                try
                {
                    DateTime expiresAtDT = DateTime.Parse(authIdExpiresAt);
                    if (DateTime.Now >= expiresAtDT)
                    {
                        return true;
                    }
                }
                catch { }
            }

            return false;
        }

        public static IPrincipal CreateUsingTemporaryId(Guid brokerId, Guid tempId, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid()) 
        {
            SqlParameter[] parameters = { 
                                            new SqlParameter("@ByPassTicket", tempId)
                                        };

            if (IsAuthenticationIdExpired(tempId))
            {
                return null;
            }

            return CreatePrincipal(brokerId, "Pml_ValidateUsingByPassTicket", parameters, Guid.Empty, true, E_ApplicationT.PriceMyLoan, true /* isStoreToCookie */, "" /* CookieSessionId */, initialPrincipalType: initialPrincipalType, initialUserId: initialUserId);
        }

        public static IPrincipal CreateRetailWithFailureType(string username, string password, bool isStoreToCookie, out E_LoginProblem loginProblem)
        {
            Guid brokerId;
            DbConnectionInfo connInfo = null;
            try
            {
                connInfo = DbConnectionInfo.GetConnectionInfoByLoginName(username, out brokerId);
            }
            catch (NotFoundException)
            {
                loginProblem = E_LoginProblem.InvalidLoginPassword;
                return null;
            }

            var nextAvailableLoginTime = GetNextAvailableLoginTime(brokerId, username, brokerPmlSiteId: Guid.Empty);
            if (nextAvailableLoginTime != SmallDateTime.MinValue)
            {
                if (nextAvailableLoginTime.Equals(SmallDateTime.MaxValue))
                {
                    loginProblem = E_LoginProblem.IsLocked;
                    return null;
                }

                if (nextAvailableLoginTime > DateTime.Now)
                {
                    loginProblem = E_LoginProblem.NeedsToWait;
                    return null;
                }
            }

            if (!Tools.IsPasswordValid(connInfo, username, brokerPMLSiteId: Guid.Empty, password: password))
            {
                loginProblem = E_LoginProblem.InvalidLoginPassword;
                return null;
            }

            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoginName", username)
            };

            var principal = CreatePrincipal(
                brokerId,
                "Pml_RetrieveRetailLoginInfoByUserName", 
                parameters, 
                expectedLoginSessionID: Guid.Empty,
                allowDuplicateLogin: false, 
                applicationT: E_ApplicationT.LendersOffice, 
                isStoreToCookies: isStoreToCookie,
                cookieSessionId: string.Empty, 
                isAnonymousQP: false);

            if (principal != null)
            {
                loginProblem = E_LoginProblem.None;
                return principal;
            }


            var employeeDB = EmployeeDB.CreateEmployeeDBWithOnlyUserNamesByBrokerIdAndLoginName(brokerId, username);
            AbstractUserPrincipal logPrincipal = null;

            if (employeeDB != null)
            {
                logPrincipal = SystemUserPrincipal.SecurityLogPrincipal(brokerId, username, employeeDB.FirstName, employeeDB.LastName, InitialPrincipalTypeT.NonInternal, "P", employeeDB.UserID);
            }

            if (IsDisabled(brokerId, username, brokerPmlSiteId: Guid.Empty))
            {
                loginProblem = E_LoginProblem.IsDisabled;
            }
            else
            {
                int loginFailureCount = IncrementLoginFailureCount(brokerId, username, BrokerPmlSiteId: Guid.Empty);
                BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                int numBadPw = broker.NumberBadPasswordsAllowed; // OPM 19920

                if (loginFailureCount >= numBadPw)
                {
                    LockAccount(brokerId, username, brokerPmlSiteId: Guid.Empty, logPrincipal: logPrincipal);
                    loginProblem = E_LoginProblem.IsLocked;
                }
                else if (loginFailureCount < 10)
                {
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                    loginProblem = E_LoginProblem.InvalidLoginPassword;
                }
                else
                {
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(logPrincipal, LoginSource.Website);
                    AddTimeToLoginBlock(brokerId, username, Math.Floor((double)(loginFailureCount / 10)), brokerPmlSiteId: Guid.Empty);
                    loginProblem = E_LoginProblem.InvalidAndWait;
                }
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="parameters"></param>
        /// <param name="expectedLoginSessionID">Guid.Empty - Create new login session ID. Else match LoginSessionID from DB with this arguments</param>
        /// <param name="allowDuplicateLogin"></param>
        /// <returns></returns>
        private static IPrincipal CreatePrincipal(Guid brokerId, string storedProcedureName, IEnumerable<SqlParameter> parameters, Guid expectedLoginSessionID, bool allowDuplicateLogin, E_ApplicationT applicationT, bool isStoreToCookies, string cookieSessionId, bool isAnonymousQP = false, InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal, Guid initialUserId = new Guid()) 
        {
            // 9/20/2004 dd - If Adding new info return by stored procedure, need to make sure ALL following procedures have the 
            // same result set.
            // Pml_RetrieveLoginInfoByPmlSiteId
            // Pml_ValidatePriceMyLoanLogin
            // Pml_ValidateMortgageLeagueAuthentication
            // Pml_RetrieveLoginInfoByUserID
            // Pml_ValidateUsingByPassTicket
            // Pml_RetrieveLoginInfoByAnonymousQuickPricerUserId // Added 2/26/2009. dd
            // Pml_RetrieveRetailLoginInfoByUserName, OPM 460932
            PriceMyLoanPrincipal principal = null;

            Guid newLoginSessionID = Guid.NewGuid();

            Guid userID = Guid.Empty;
            Guid employeeID = Guid.Empty;
            Guid branchID = Guid.Empty;
            string permissions = "";
            string firstName = "";
            string lastName = "";
            string email = "";
            string loginNm = "";
            bool isSharable;
            Guid lastUsedCreditProtocolID = Guid.Empty;
			string lastUsedCreditLoginNm = "";
            Guid currentLoginSessionID = Guid.Empty;
            bool isPmlManager = false;
            Guid pmlExternalManagerEmployeeId = Guid.Empty;
            bool isOthersAllowedToEditUnderwriterAssignedFile = false;
            bool isLOAllowedToEditProcessorAssignedFile = false;
            Guid lpePriceGroupId = Guid.Empty;
            string type = "P";
            bool isRateLockedAtSubmission = false;
            bool hasLenderDefaultFeatures = false;
			DateTime passwordExpirationD = DateTime.MaxValue;
            bool isQuickPricerEnable = false;
            bool isUserQuickPricerEnable = false;
            bool isPricingMultipleAppsSupported = false;
            bool isValid = false;
            bool isNewPmlUIEnabled = false;
            bool isUsePml2AsQuickPricer = false;
            E_PortalMode portalMode = E_PortalMode.Blank;

            E_PmlLoanLevelAccess pmlLevelAccess = E_PmlLoanLevelAccess.Individual;
            Guid pmlBrokerId = Guid.Empty;
            Guid miniCorrespondentBranchId = Guid.Empty;
            Guid correspondentBranchId = Guid.Empty;

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, storedProcedureName, parameters)) 
            {
                if (reader.Read()) 
                {
                    isValid = true;
                    if (brokerId == Guid.Empty)
                    {
                        // 10/23/2014 dd - Still need to work on to make sure all call pass appropriate brokerid.
                        Tools.LogBug("BrokerId is empty.");
                        brokerId = (Guid)reader["BrokerId"];
                    }
                    employeeID                   = (Guid)   reader["EmployeeID"];
                    branchID                     = (Guid)   reader["BranchID"];
                    permissions                  = (string) reader["Permissions"];
                    firstName                    = (string) reader["UserFirstNm"];
                    lastName                     = (string) reader["UserLastNm"];
                    email                        = (string) reader["Email"];
                    userID                       = (Guid)   reader["UserID"];
                    loginNm                      = (string) reader["LoginNm"];
                    isSharable                   = (bool)   reader["IsSharable"];
                    isPmlManager                 = (bool) reader["IsPmlManager"];
                    type                         = (string) reader["Type"];
                    isRateLockedAtSubmission     = (bool) reader["IsRateLockedAtSubmission"];
                    hasLenderDefaultFeatures = (bool) reader["HasLenderDefaultFeatures"];
                    isQuickPricerEnable = Convert.IsDBNull(reader["IsQuickPricerEnable"]) ? false : (bool)reader["IsQuickPricerEnable"];
                    isUserQuickPricerEnable = (bool)reader["IsUserQuickPricerEnabled"];
                    isPricingMultipleAppsSupported = (bool)reader["IsPricingMultipleAppsSupported"];
                    isNewPmlUIEnabled            = (bool)reader["IsNewPmlUIEnabled"];
                    isUsePml2AsQuickPricer       = (bool)reader["IsUsePml2AsQuickPricer"];
                    portalMode = (E_PortalMode)((int)reader["PortalMode"]);

                    if (reader["PmlBrokerId"] != DBNull.Value)
                    {
                        pmlBrokerId = (Guid)reader["PmlBrokerId"];
                    }

                     pmlLevelAccess = (E_PmlLoanLevelAccess)reader["PmlLevelAccess"]; 

                    isOthersAllowedToEditUnderwriterAssignedFile = (bool) reader["IsOthersAllowedToEditUnderwriterAssignedFile"]; 
                    isLOAllowedToEditProcessorAssignedFile = (bool) reader["IsLOAllowedToEditProcessorAssignedFile"];
                    if (reader["PmlExternalManagerEmployeeId"] != DBNull.Value) 
                    {
                        pmlExternalManagerEmployeeId = (Guid) reader["PmlExternalManagerEmployeeId"];
                    }
					
					try
					{
						if (reader["PasswordExpirationD"] != DBNull.Value) 
						{
							passwordExpirationD = (DateTime) reader["PasswordExpirationD"];
						}
					}
					catch{}
					
                    if (reader["PmlLoginSessionId"] != DBNull.Value) 
                    {
                        currentLoginSessionID = (Guid) reader["PmlLoginSessionId"];
                    }
                    if (reader["LastUsedCreditProtocolID"] != DBNull.Value)
                    {
                        lastUsedCreditProtocolID = (Guid) reader["LastUsedCreditProtocolID"];
                    }
					// 12/27/2006 nw - OPM 5083 - Remember login name
					if (reader["LastUsedCreditLoginNm"] != DBNull.Value)
					{
						lastUsedCreditLoginNm = (string) reader["LastUsedCreditLoginNm"];
					}
                    if (reader["LpePriceGroupId"] != DBNull.Value) 
                    {
                        lpePriceGroupId = (Guid) reader["LpePriceGroupId"];
                    }

                    if (reader["MiniCorrespondentBranchId"] != DBNull.Value)
                    {
                        miniCorrespondentBranchId = (Guid)reader["MiniCorrespondentBranchId"];
                    }

                    if (reader["CorrespondentBranchId"] != DBNull.Value)
                    {
                        correspondentBranchId = (Guid)reader["CorrespondentBranchId"];
                    }

                    if (!allowDuplicateLogin && expectedLoginSessionID == Guid.Empty) 
                    {
                        if (PriceMyLoanConstants.IsEmbeddedPML)   //dont use a new session id for embedded PML
                        {
                            currentLoginSessionID = Guid.Empty;
                        }
                        else
                        {
                            currentLoginSessionID = newLoginSessionID;
                        }
                        // Will save newLoginSessionID to DB.
                    } 
                    else if (!allowDuplicateLogin && !isSharable) 
                    {
                        if (expectedLoginSessionID != currentLoginSessionID)
                            throw new LendersOffice.Security.DuplicationLoginException(loginNm, false);
                    }


                }
            }

            if (isValid) 
            {
                List<string> roles;
                List<string> ipWhiteList = null;
                HashSet<Guid> teamList = null;
                bool isLoggedOut = false;

                if (type == "B")
                {
                    Tools.GetAdditionalCredentialForBrokerUserPrincipal(connInfo, 
                        userID, employeeID, brokerId, cookieSessionId,
                        out roles, out ipWhiteList, out teamList, out isLoggedOut);
                }
                else
                {
                    roles = new List<string>();
                    EmployeeRoles employeeRoles = new EmployeeRoles(brokerId, employeeID);
                    foreach (var role in employeeRoles.Items)
                    {
                        roles.Add(role.Desc);
                    }
                }

                principal = new PriceMyLoanPrincipal(new GenericIdentity(loginNm), brokerId, branchID, employeeID, permissions, lastUsedCreditProtocolID, firstName, lastName, email, userID, loginNm, currentLoginSessionID, applicationT, 
                    isPmlManager, passwordExpirationD,
                    pmlExternalManagerEmployeeId,
                    roles.ToArray(), isOthersAllowedToEditUnderwriterAssignedFile, isLOAllowedToEditProcessorAssignedFile, type, lpePriceGroupId, isRateLockedAtSubmission, 
                    hasLenderDefaultFeatures, lastUsedCreditLoginNm, isQuickPricerEnable || isUserQuickPricerEnable, isPricingMultipleAppsSupported, cookieSessionId, pmlBrokerId, pmlLevelAccess,
                    isNewPmlUIEnabled, isUsePml2AsQuickPricer, portalMode, miniCorrespondentBranchId, correspondentBranchId, ipWhiteList, teamList, initialPrincipalType, initialUserId);
                if (isStoreToCookies)
                {
                    int CookieExpireMinutes = PriceMyLoanConstants.CookieExpireMinutes;

                    if (PriceMyLoanConstants.IsEmbeddedPML)
                    {
                        // 7/14/2010 dd - OPM 11870 - Make session time out for embedded pml as long as LendersOffice.
                        CookieExpireMinutes = ConstApp.LOCookieExpiresInMinutes;
                    }

                    // 6/15/2010 dd - OPM 52772 - Change PML User cookie to non-persistent cookie.
                    // 5/22/2012 dd - We are no longer support Citi. However we need the cookie to persist for Encompass 360 integration to work/
                    //                See OPM 75176
                    LendersOffice.Common.RequestHelper.StoreToCookie(principal, CookieExpireMinutes, E_CookieModeT.Persistent, isAnonymousQP);
                }

                System.Threading.Thread.CurrentPrincipal = principal;
            }

            if (userID != Guid.Empty && !allowDuplicateLogin && expectedLoginSessionID == Guid.Empty) 
            {
                SqlParameter[] parms = { 
                                           new SqlParameter("@UserID", userID),
                                           new SqlParameter("@PmlLoginSessionID", newLoginSessionID)
                                       };
                StoredProcedureHelper.ExecuteNonQuery(brokerId, "Pml_UpdateLoginSessionID", 2, parms);
            }

            return principal;
        }
	}
}
