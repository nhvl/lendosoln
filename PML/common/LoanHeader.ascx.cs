﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.Common;

namespace PriceMyLoan.common
{
    public partial class LoanHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LogoutButtonPanel.Visible = PriceMyLoanRequestHelper.IsEncompassIntegration == false && !PriceMyLoanConstants.IsEmbeddedPML;
        }

        public void SetInfo(string sLNm, string aBNm, string sLAmtCalc_Rep, string sLtvR_Rep, string sCltvR_Rep, string sCreditScoreType1_rep, string sCreditScoreType2_rep)
        {
            this.sLNm.Text = sLNm;
            this.aBNm.Text = aBNm;
            sLAmtCalc.Text = sLAmtCalc_Rep.Replace(".00", "");
            // 10/8/2004 dd - Temporary Hack to remove .000;
            sLtvR.Text = sLtvR_Rep.Replace(".000", "");
            sCltvR.Text = sCltvR_Rep.Replace(".000", "");
            sCreditScoreType1.Text = sCreditScoreType1_rep;
            sCreditScoreType2.Text = sCreditScoreType2_rep;
        }

        public bool ShowLogoutButton
        {
            get
            {
                return LogoutButtonPanel.Visible;
            }
            set
            {
                LogoutButtonPanel.Visible = value;
            }
        }

        public bool ShowPipelineButton
        {
            get
            {
                return PipelineButtonPanel.Visible;
            }
            set
            {
                PipelineButtonPanel.Visible = value;
            }
        }
    }
}