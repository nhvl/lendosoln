﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestError.aspx.cs" Inherits="PriceMyLoan.common.RequestError" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>HTML Characters Not Allowed</title>
    <style type="text/css">
        body {
          background-color:white;
          padding-left:5em;
          padding-top:5em;
          font-family:Verdana, Arial, Helvetica, sans-serif
        }
        h1{ font-size:11px; font-weight:bold; color:Black;}
        p{ font-size:11px; color:#565656;}
    </style>
</head>
<body onload="f_load();">
    <form id="form1" runat="server">
    <div>
        <h1>Unable to save changes because of invalid characters</h1>
        <p>Remove any invalid characters (e.g. &lt;, &gt;, &amp;), then try saving again. </p>
        <p><a id="link" href="#" onclick="f_go();">Go back</a></p>
        <input type="hidden" id="hField" runat="server" />
    </div>
    </form>
    <script type="text/javascript">
        function f_go() {
            try {
                window.location = document.getElementById('hField').value;
            }catch(e){logJSException(e, 'unable to set window location'); }
        }
        function f_load() {
            try {
                var hf = document.getElementById('hField').value;
                var l = document.getElementById('link');
                if (hf.length == 0) {
                    l.style.display = "none";
                }
            } catch (e) { logJSException(e, 'unable to do load'); }
        }
  </script>
</body>
</html>
