using DataAccess;
using LendersOffice.Common;
using PriceMyLoan.Common;
using System;
using System.Linq;
using System.Web.UI;

namespace PriceMyLoan.UI.Common
{
    public class PmlErrorMessages
    {
        public const string Generic = "System error.  Please contact us if this happens again.";
        public const string AccessDenied = "Access Denied.";
        public const string NeedsAllowLoginFromMCL = "This account needs to have the \'Allow login from MCL\' permission enabled."; // reviewed by Dominic 10/11/05
        public const string CertificateNullResult = "Internal error (returned null result)."; // reviewed by Dominic 10/11/05
        //OPM 5068 Cord -- Error message for access denied to external pml. Referenced in $pml\main\AccessDenied.aspx
        public const string ExternalAccessDenied = "You do not have access to this loan. Please contact your account executive for assistance.";
    }

    public partial class AppError : PriceMyLoan.UI.BasePage
	{
        private static string[] AccessDeniedExceptionTypes = { "DataAccess.PageDataLoadDenied", "LendersOffice.Security.AccessDenied" };

        public const string Key_SentEmail = "SentEmail";
        public const string Key_LOException = "LOException";
        public const string Key_ErrorMessage = "ErrorMessage";
        public const string Key_ErrorCode = "errorcode";

        protected bool m_isDuplicateLoginException = false;

        private bool m_ExceptionMissing = false;

        protected string m_errorReferenceNumber = string.Empty;

        protected string m_errorMessage = string.Empty;

        protected override bool IncludeMaterialDesignFiles => false;

        protected void PageLoad(object sender, System.EventArgs e)
		{
            m_isDuplicateLoginException = RequestHelper.GetSafeQueryString("id") == "DUP_LOGIN";
            
            if (!Page.IsPostBack && Request.UrlReferrer != null)
            {
                ViewState["UrlReferrer"] = Request.UrlReferrer.ToString();
            }

            DisplayErrorMessage();
		}

        /// <summary>
        /// 01-30-08 av opm 19618 Retrieve Error From Cache  All the time not just on first load.
        /// </summary>
        /// <returns></returns>
        private ExceptionInfo GetErrorFromCache()
        {
            ExceptionInfo eI;

            string errorId = RequestHelper.GetSafeQueryString("id");

            if (string.IsNullOrEmpty(errorId))
            {
                var errmsg = RequestHelper.GetSafeQueryString("errmsg");

                var errorCode = RequestHelper.GetSafeQueryString(Key_ErrorCode);

                if (!string.IsNullOrEmpty(errmsg))
                {
                    eI = new ExceptionInfo(new CBaseException(errmsg, "Exception's origin is from the querystring of AppError."));
                }
                else if(!string.IsNullOrEmpty(errorCode))
                {
                    eI = new ExceptionInfo(new CBaseException(errorCode, errorCode));
                }
                else
                {
                    eI = new ExceptionInfo(new CBaseException(PmlErrorMessages.Generic, "No errmsg, error code or exception id existed in the querystring of AppError."));
                }
                m_ExceptionMissing = true;
            }
            else if (errorId == "AJAX")
            {
                // 8/12/2009 dd - Error from our background service.
                string msg = RequestHelper.GetSafeQueryString("msg");
                eI = new ExceptionInfo(new CBaseException(PmlErrorMessages.Generic, msg));

                Tools.LogErrorWithCriticalTracking(eI.ErrorReferenceNumber + "::" + msg);

                m_ExceptionMissing = true;
            }
            else if (errorId == "DUP_LOGIN")
            {
                eI = new ExceptionInfo(new LendersOffice.Security.DuplicationLoginException(""));
                m_ExceptionMissing = true;
            }
            else
            {
                // Retrieve exception from cache.
                // 05/30/06 mf - As per OPM 2582, we no longer use server cache.
                // Instead of caching the entire Exception object, we only cache
                // the data this page needs in a ExceptionInfo object.


                eI = ErrorUtilities.RetrieveExceptionFromCache(errorId);

                if (eI == null)
                {
                    // We were unable to get the exception object from cache.
                    Tools.LogError("Unable to load Exception from cache in AppError.aspx");
                    eI = new ExceptionInfo(new CBaseException(PmlErrorMessages.Generic, "Failed to retrieve Exception from cache.  System clock may help determine thrower."));
                    m_ExceptionMissing = true;
                }
            }
            return eI;

        }

        private void DisplayErrorMessage() 
        {
            ExceptionInfo eI = GetErrorFromCache();

            m_isDuplicateLoginException = (eI.ExceptionType == "LendersOffice.Security.DuplicationLoginException");

            m_errorReferenceNumber = eI.ErrorReferenceNumber;

            if (AccessDeniedExceptionTypes.Contains(eI.ExceptionType) && !PriceMyLoanConstants.IsEmbeddedPML)
            {
                string queryString = string.Empty;
                if (eI.UserMessage == ErrorMessages.SmsAuthenticationNotEnabled)
                {
                    queryString = "?generic=t";
                }

                Server.Transfer($"~/main/FriendlyAccessDenial.aspx{queryString}", false);
            }

            m_errorMessage = eI.UserMessage;

            //OPM 56322 - DuplicationLoginException uses html in UserMessage; render as special case
            if (m_isDuplicateLoginException)
            {
                if (!IsPostBack)
                {
                    RequestHelper.ClearAuthenticationCookies();
                }
                m_errorMessage = "You have been logged out because you logged in on a different computer.";
                m_errorMessagePanel.Visible = true;
                if (!"You have been logged out because you logged in on a different computer.<br><br>".Equals(eI.UserMessage))
                {
                    string loginUrl = LendersOffice.Constants.ConstSite.LoginUrl.Replace("~/", Tools.VRoot + "/");
                    m_loginUrl.HRef = loginUrl;
                    m_loginUrl.Visible = true;
                }
            }

            // Hide all elements that may contain scripts for navigation.
            if (RequestHelper.GetBool("IsEncompass"))
            {
                CloseBtn.Visible = false;
                m_loginUrl.Visible = false;
                m_errorMessagePanel.Visible = false;
            }

        }
		
		// 01-31-08 av 
		private void DeleteFromCache() 
		{	
			if ( ! this.m_ExceptionMissing ) 
			{
				ErrorUtilities.ExpireException( RequestHelper.GetSafeQueryString("id") );
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

        // 01-31-08 av 
		protected void m_btnClose_Click(object sender, System.EventArgs e)
		{
			if ( !this.m_ExceptionMissing )
				DeleteFromCache();

			ClientScript.RegisterStartupScript(this.GetType(), "CloseScript",
@"<script>
f_onCloseClick();
</script>"
            );

		}
	}
}
