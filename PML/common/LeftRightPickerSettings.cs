﻿#region Generated Code
namespace PriceMyLoan.common
#endregion
{
    /// <summary>
    /// Provides a collection of settings for the left-right picker.
    /// </summary>
    public class LeftRightPickerSettings
    {
        /// <summary>
        /// Gets or sets the data source for the available items.
        /// </summary>
        public object AvailableSource { get; set; }

        /// <summary>
        /// Gets or sets the label for the available items.
        /// </summary>
        public string AvailableLabel { get; set; }

        /// <summary>
        /// Gets or sets the data source for the assigned items.
        /// </summary>
        public object AssignedSource { get; set; }

        /// <summary>
        /// Gets or sets the label for the assigned items.
        /// </summary>
        public string AssignedLabel { get; set; }

        /// <summary>
        /// Gets or sets the name of the text field.
        /// </summary>
        public string DataTextField { get; set; }

        /// <summary>
        /// Gets or sets the name of the value field.
        /// </summary>
        public string DataValueField { get; set; }
    }
}
