﻿using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System;
using System.Data.Common;
using System.Data.SqlClient;

namespace PriceMyLoan.Common
{
    public class PasswordUtil
    {
        public static int GetNumberBadPwAllowed(Guid brokerId)
        {
            BrokerDB broker = BrokerDB.RetrieveById(brokerId);
            int numBadPw = broker.NumberBadPasswordsAllowed;
            return numBadPw;
        }

        public static int IncrementLoginFailureCount(Guid brokerId, Guid userId)
        {
            int loginFailureCount = 0;
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "UpdateLoginFailureCountByUserId", parameters))
            {
                if (reader.Read())
                {
                    loginFailureCount = Int32.Parse(reader["LoginFailureCount"].ToString());
                }
            }
            return loginFailureCount;
        }

        public static StrongPasswordStatus VerifyPassword(EmployeeDB employee, string sPassword, out string sStatus)
        { 
				StrongPasswordStatus ret = employee.IsStrongPassword(sPassword);
                sStatus = string.Empty;
				switch (ret) 
				{
					case StrongPasswordStatus.PasswordContainsLogin:
						sStatus += "Password cannot contain login name, first name or last name.<br>";
						break;
					case StrongPasswordStatus.PasswordMustBeAlphaNumeric:
						sStatus += "Password must contain at least 1 number and 1 letter.<br>";
						break;
					case StrongPasswordStatus.PasswordTooShort:
						sStatus += "Password must be at least 6 characters in length.<br>";
						break;
					case StrongPasswordStatus.PasswordRecycle:
						sStatus += "You cannot recycle old passwords.  Please specify a new password.<br>";
						break;
					// OPM 24614
					case StrongPasswordStatus.PasswordContainsConsecutiveIdenticalChars:
						sStatus += (ErrorMessages.PasswordContainsConsecIdenticalChars + "<br>");
						break;
					// OPM 24614
					case StrongPasswordStatus.PasswordContainsConsecutiveAlphanumericChars:
						sStatus += (ErrorMessages.PasswordContainsConsecCharsInAlphaOrNumericOrder + "<br>");
						break;
                    case StrongPasswordStatus.PasswordHasUnverifiedCharacters:
                        sStatus += Utilities.SafeHtmlString(ErrorMessages.PasswordHasUnverifiedCharacters) + "<br>" ;
                        break;
				}
				return ret;
        }
    }
}
