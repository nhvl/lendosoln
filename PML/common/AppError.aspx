<%@ Page Language="c#" CodeBehind="AppError.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Common.AppError" ValidateRequest="false" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
    <head runat="server">
        <title>Error Page</title>
        <style type="text/css">
            #ErrorTitle 
            {
                color:Red;
                font-weight:bold;
                font-size:medium;
                text-align:center;
            }
            .EditBackground 
            {
                background-color: gainsboro;
            }
      </style>
    </head>
    <body class="EditBackground" onload="init();">
        <script type="text/javascript">
            <!--
            function init() {
                <%-- Only applicable to duplicate login exception --%>
                <% if (m_isDuplicateLoginException) { %>
                    if (window.dialogArguments != null) {
                        window.dialogArguments.DuplicateLogin = true;
                        self.close();
                    }
                    if (window.opener != null) {
                        window.opener.location = window.opener.location;
                        self.close();
                    }
                  <% } %>
            }

            function f_onCloseClick() {
                self.close();
            }
            //-->
        </script>

        <form id="AppError" method="post" runat="server">
            <div id="content">
                <center>  
                    <br />
                    <div style="clear:both"></div>
                    <div id="ErrorTitle">
                        System Error    
                    </div>
                    <br />
                    <b>Reference Number </b>: <%= AspxTools.HtmlString(m_errorReferenceNumber) %>
                    <br />
                    <b>Error </b>: <%= AspxTools.HtmlString(m_errorMessage) %>
                    <asp:Panel ID="m_errorMessagePanel" runat="server" Visible="false">
                        <br />
                        <a href="" id="m_loginUrl" runat="server" visible="false">Click here to login again</a>  
                    </asp:Panel>
                    <br />
                    <br />
                    <br />
                    <br />
                    <asp:Button runat="server" ID="CloseBtn" OnClick="m_btnClose_Click" Font-Bold="true" Text=" Close " />
                    <br />
                </center>  
            </div>
        </form>
    </body>
</html>
