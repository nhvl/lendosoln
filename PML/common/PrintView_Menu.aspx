<%@ Page Language="C#" CodeBehind="PrintView_Menu.aspx.cs" AutoEventWireup="false"
    Inherits="PriceMyLoan.common.PrintView_Menu" %>

<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>PrintView_Menu</title>
    <style type="text/css">
        body { padding: 0; margin: 0; }
    </style>
</head>
<body ms_positioning="FlowLayout">

    <script language="javascript">
    <!--
    function printMe() {
      var f = parent.frmBody;
      f.focus();
      f.print();
    }
    <% if (m_displayTwoCertificates) { %>
    
    function f_switchCertificate(id) {
      var url = <%= AspxTools.JsString(VirtualRoot + "/main/SavedCertificate.aspx?loanid=") %> + id;    
      parent.frmBody.location = url;
    }
    function f_switchFirst() {
      var id = <%= AspxTools.JsString(RequestHelper.GetGuid("1st", Guid.Empty)) %>;
      f_switchCertificate(id);
    }
    function f_switchSecond() {
      var id = <%= AspxTools.JsString(RequestHelper.GetGuid("2nd", Guid.Empty)) %>;
      f_switchCertificate(id);
    }
    
    <% } %>
    //-->
    </script>

    <form>
    <table style="height: 100%">
        <tr>
            <td nowrap>
                <button type="button" class="ButtonStyle" onclick="printMe();">Print ...</button>&nbsp;&nbsp;
            </td>
            <td nowrap>
                <button type="button" class="ButtonStyle" onclick="parent.close();">Close</button>
            </td>
            <%-- 10/16/04 dd - This is a hack for display 2 Loans certificate. --%>
            <td nowrap>
                <table id="TwoCertificatesTable" runat="server">
                    <tr>
                        <td nowrap>
                            <input type="radio" name="a" checked id="First" onclick='f_switchFirst();' />
                            <label for="First" class="FieldLabel" onclick='f_switchFirst();'>
                                1st Loan (#<%=AspxTools.HtmlString(m_1st_sLNm) %>)
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <input type="radio" name="a" id="Second" onclick='f_switchSecond();'>
                            <label for="Second" class="FieldLabel" onclick='f_switchSecond();'>
                                2nd Loan (#<%=AspxTools.HtmlString(m_2nd_sLNm) %>)
                            </label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
