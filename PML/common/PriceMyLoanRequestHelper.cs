namespace PriceMyLoan.Common
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Security;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using MeridianLink.CommonControls;
    using PriceMyLoan.Security;
    using PriceMyLoan.UI.Common;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public class PriceMyLoanRequestHelper
	{
        private const string EncompassCookieIndicator = "_ENCOMPASS_EPASS";
        private const string EncompassCookieRequest = "_ENCOMPASS_REQUEST_TYPE";
        /// <summary>
        /// This will indicate whether PML was invoke through Encompass EPASS integration. When PML is enter through Encompass, need to set
        /// this bit to true.
        /// </summary>
        public static bool IsEncompassIntegration
        {
            get
            {
                if (null != HttpContext.Current)
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(EncompassCookieIndicator);
                    if (null != cookie)
                    {
                        if (cookie.Value == "Y")
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            set
            {
                if (value == true)
                {
                    RequestHelper.StoreToCookie(EncompassCookieIndicator, "Y", true);
                }
                else
                {
                    // Expire cookie
                    RequestHelper.StoreToCookie(EncompassCookieIndicator, "N", DateTime.MinValue);
                }
            }
        }
        public static E_Encompass360RequestT EncompassRequestType
        {
            get 
            {
                if (null != HttpContext.Current)
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(EncompassCookieRequest);
                    if (null != cookie)
                    {
                        return ConvertEncompassRequestType(cookie.Value);
                    }
                }
                return E_Encompass360RequestT.QuoteRequest;
            }
            set
            {
                RequestHelper.StoreToCookie(EncompassCookieRequest, value.ToString(), false);
            }
        }
        public static E_Encompass360RequestT ConvertEncompassRequestType(string sRequestType)
        {
            if (string.IsNullOrEmpty(sRequestType))
                return E_Encompass360RequestT.QuoteRequest;

            switch (sRequestType.TrimWhitespaceAndBOM().ToUpper())
            {
                case "QUOTEREQUEST":
                    return E_Encompass360RequestT.QuoteRequest;
                case "BUYSIDELOCK":
                    return E_Encompass360RequestT.BuysideLock;
                case "SELLSIDELOCK":
                    return E_Encompass360RequestT.SellsideLock;
                case "UNDERWRITEONLY":
                    return E_Encompass360RequestT.UnderwriteOnly;
                case "REGISTERORLOCK":
                default:
                    return E_Encompass360RequestT.RegisterOrLock;
            }
        }
        public static void AuthenticateRequest() 
        {
			bool isValid = false;

            Guid userID = RequestHelper.GetGuid("UserID", Guid.Empty);
            Guid gmlUniqueID = RequestHelper.GetGuid("gmlid", Guid.Empty); // 1/21/2005 dd - MortgageLeague unique session id
            Guid byPassKey = RequestHelper.GetGuid("authid", Guid.Empty);
            Guid lenderPmlSiteId = RequestHelper.GetGuid("lenderpmlsiteid", Guid.Empty);

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string[] ignorePages = {
                                       "index.aspx"
                                       , "lpe.aspx"
                                       , "apperror.aspx"
										, "_login.aspx"
										, "loader.aspx"
                                        , "logout.aspx"
                                        , "sessionexpired.aspx"
                                        , "inetapi/mortgageleague.aspx"
                                        , "trace.axd"
                                        , "quickpricerservice.aspx"
                                        , "/encompass.aspx"
                                        , "PasswordReset/RequestPasswordReset.aspx"
                                        , "PasswordReset/ResetPassword.aspx"                                        
                                        , "EncompassLoginFailure.aspx"
                                        , "logopl.aspx"
                                        , "userprofileimage.aspx"
                                        ,"authenticationcodepage.aspx"
                                        , "sitecheck.aspx"
                                        ,"selfcheck.aspx"
                                        , "quickpricernonanonymousservice.aspx"
                                        , "RetailLogin.aspx"
                                        , "anonymousstyleprovider.aspx"
                                        , "sitecheckpml.aspx"
                                   };

            string currentPath = context.Request.Url.AbsolutePath.ToLower();
            foreach (string page in ignorePages) 
            {
                // 8/26/2004 dd - These pages don't need authorization.

                if (currentPath.EndsWith(page.ToLower())) 
                {
                    context.User = new GenericPrincipal(new GenericIdentity("FOO"), new string[0]);
                    return;

                }
            }

            // 9/8/2015 - dd - For PML any request to custom private label (~/custom/xxxx or ~/custom_frame) will not require authenticate.
            if (currentPath.StartsWith(VirtualPathUtility.ToAbsolute("~/custom"), StringComparison.OrdinalIgnoreCase) ||
                context.Request.Url.PathAndQuery.Equals("/") /* So private site, i.e: https://goto.jmaclending.com will work */)
            {
                context.User = new GenericPrincipal(new GenericIdentity("FOO"), new string[0]);
                return;
            }

            var isLoadAnonymousQp = false;
            var requestQueryString = context.Request.QueryString;
            if (context.Request.HttpMethod == "GET" && lenderPmlSiteId != Guid.Empty && currentPath.EndsWith("quickpricer.aspx"))
            {
                // 1/30/2009 dd - QuickPricer page is a special page. Since we allow anonymous user to access the page, we are 
                // implement following logic.
                // If user already login when access the page then it will use the current credential.
                // If user is not login and access the page then I try to load a default account. If Default account is not setup
                // then redirect user to expiration page.
                // 4/6/2017 ejm OPM 451761 - When loading up a principal in the anonymous quick pricer, we're now going to load it in a separate cookie. This is to prevent
                // overriding any sessions that use the normal forms authentication cookie. This cookie will also prevent the anonymous QP from being able to access any other page in the system due to a unique bit it has.
                isLoadAnonymousQp = true;
                Guid anonymousId = RequestHelper.GetGuid("anonymousid", Guid.Empty); // 2/26/2009 dd - Add supported for multiple anonymous accounts.
                if (anonymousId == Guid.Empty)
                {
                    PrincipalFactory.E_LoginProblem loginProblem = PrincipalFactory.E_LoginProblem.None;
                    IPrincipal principal = PrincipalFactory.CreateWithFailureType(ConstAppDavid.PmlQuickPricerAnonymousLogin, ConstAppDavid.PmlQuickPricerAnonymousPassword,
                        lenderPmlSiteId, out loginProblem, true, isAnonymousQP: true);

                    if (loginProblem == PrincipalFactory.E_LoginProblem.None)
                    {
                        context.User = principal;
                        return;
                    }
                }
                else
                {
                    IPrincipal principal = PrincipalFactory.CreateUsingAnonymousQuickPricerUserId(lenderPmlSiteId, anonymousId);
                    context.User = principal;
                }
            }
            else if (
                context.Request.HttpMethod == "GET"
                && PriceMyLoanConstants.IsEmbeddedPML
                && null == context.User
                && null != context.Request.Cookies[ConstSite.LqbAuthCookieName]
            ) // If we're coming from LO, check the cookies to see if we're both logged into LO and not logged into PML
            {

                // Previously, we would pass in a userid and authenticate using that id.
                // That approach is obsolete, though there may still be code that uses it

                // Check if we're still authenticated in LO
                FormsAuthenticationTicket lqbTicket = FormsAuthentication.Decrypt(context.Request.Cookies[ConstSite.LqbAuthCookieName].Value);
                if (lqbTicket.Expired == false)
                {
                    // See PrincipalFactory::Create(FormsIdentity identity) for more info about the FormsAuthenticationTicket UserData
                    string[] userData = lqbTicket.UserData.Split('|');
                    Guid lqbUserId = new Guid(userData[1]);
                    Guid brokerId = new Guid(userData[6]);
                    InitialPrincipalTypeT initialPrincipalType;
                    Enum.TryParse(userData[7], out initialPrincipalType);

                    Guid initialUserId = Guid.Empty;
                    Guid.TryParse(userData[8], out initialUserId);

                    // Sign into PML
                    context.User = PrincipalFactory.CreateByUserId(brokerId, lqbUserId, initialPrincipalType, initialUserId);
                }
                else
                {
                    // Cookie expired
                    Signout();
                }
                return;
            }
            else if (context.Request.HttpMethod == "GET" || context.User == null) 
            {
                if (Guid.Empty != gmlUniqueID) 
                {
                    // 1/21/2005 dd - Check to see if GmlUniqueID existed in server cache.s
                    //                object o = context.Cache.Remove("MortgageLeague_" + gmlUniqueID.ToString());
                    string o = AutoExpiredTextCache.GetFromCache("Mcl2Pml_" + gmlUniqueID.ToString());
                    if (null != o) 
                    {
                        Guid userId = new Guid(o);
                        context.User = PrincipalFactory.CreateByPmlUserID_SLOW(userId);
                        AutoExpiredTextCache.UpdateCache("", "Mcl2Pml_" + gmlUniqueID.ToString());
                        return;
                    }
                    ErrorUtilities.DisplayErrorPage(PmlErrorMessages.AccessDenied, false, Guid.Empty, Guid.Empty);
                } 
                else if (Guid.Empty != userID) 
                {
                    // 11/2/2005 dd - Allow internal user become PML user.
                    string secretKey = RequestHelper.GetSafeQueryString("secret");
					
                    if (secretKey == Tools.SecretHashForPml(userID)) 
                    {
                        string userDataString = AutoExpiredTextCache.GetFromCache(secretKey);
                        if (userDataString == null)
                        {
                            Tools.LogWarning("Got matching secret key, but user data was not found in the cache!");
                            return;
                        }
                        string[] userData = userDataString.Split('|');

                        string clientIp = userData[1];

                        if (RequestHelper.ClientIP != clientIp)
                        {
                            Tools.LogWarning("Got userID, and matching secret key, but client IP did not match!");
                            return;
                        }

                        Guid initialUserId = Guid.Empty;
                        Guid.TryParse(userData[0], out initialUserId);

                        if (PriceMyLoanConstants.IsEmbeddedPML || 
                            context.Request.Path.Contains("embeddedpml") ||
                            RequestHelper.GetBool("isRetail"))
                        {
                            context.User = PrincipalFactory.CreateByUserID_SLOW(userID, InitialPrincipalTypeT.Internal, initialUserId);
                            return;
                        }
                        context.User = PrincipalFactory.CreateByPmlUserID_SLOW(userID, InitialPrincipalTypeT.Internal, initialUserId);
                        return;
                    }
                    Tools.LogWarning("Got userID, but secret key did not match!");
                } 
                else if (Guid.Empty != byPassKey) 
                {
					// 11/13/07 db - OPM 17752
					bool isLoUser = false;
                    bool found = false;
                    Guid brokerId = Guid.Empty;

                    foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                    {
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@ByPassTicket", byPassKey)
                                                    };
                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetUserTypeByByPassTicket", parameters))
                        {
                            if (reader.Read())
                            {
                                isLoUser = ((string)reader["Type"] == "B");
                                userID = (Guid)reader["UserId"];
                                brokerId = (Guid)reader["BrokerId"];
                                found = true;
                                break;

                            }
                        }
                    }
                    if (found)
                    {
                        if (isLoUser)
                        {
                            context.User = PrincipalFactory.CreateByUserId(brokerId, userID);
                        }
                        else
                        {
                            context.User = PrincipalFactory.CreateUsingTemporaryId(brokerId, byPassKey);
                        }
                    }
                    return;
                }
            }
            
            if (context.SkipAuthorization || context.User == null || context.User.Identity.AuthenticationType != "Forms") 
            {
                if (null == context.User && !context.SkipAuthorization) 
                {
					// 10/13/2004 dd - Cookie expired.
                    Signout();
                }
                return;
            }

			// OPM 19920 - added check if account is locked, and if so, log out
            if (!context.User.Identity.IsAuthenticated || IsAccountLocked(userID) )
            {
				Signout();
                return;
            }

            FormsIdentity identity = (FormsIdentity) context.User.Identity;
            FormsAuthenticationTicket ticket = identity.Ticket;
            if (!isLoadAnonymousQp)
            {
                string[] userData = ticket.UserData.Split('|');
                if (userData.Length > 10 && userData[10] == "aqp")
                {
                    ErrorUtilities.DisplayErrorPage(PmlErrorMessages.AccessDenied, false, Guid.Empty, Guid.Empty);
                }
            }

            if (ticket.Version != ConstAppDavid.CookieVersion) 
            {
				Signout();
            }

            context.User = PrincipalFactory.Create(identity);
            isValid = context.User != null;

            if (!isValid)
            {
                Signout();
            }

			string thisUrl = context.Request.Url.AbsolutePath ;

            // OPM 7611
			// If request from YourProfile.aspx then skip the redirect on password expiration.
			// If below check is missing then an infinite loop will occur.
			if (!thisUrl.EndsWith("YourProfile.aspx") && !thisUrl.EndsWith("EmbeddedPmlPasswordExpired.aspx")) 
            {
				if (null != context.User) 
				{
					PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) context.User;
					
					if (principal.IsPasswordExpired) //OPM 19602
					{
                        if (PriceMyLoanConstants.IsEmbeddedPML)
                        {
                            var url = "~/webapp/EmbeddedPmlPasswordExpired.aspx";

                            var loanId = RequestHelper.GetGuid("loanid", Guid.Empty);
                            if (loanId != Guid.Empty)
                            {
                                url += "?loanid=" + loanId;
                            }

                            context.Response.Redirect(url);
                        }
                        else
                        {
                            /// redirect to the change password page.
                            context.Response.Redirect("~/main/YourProfile.aspx?cmd=forcedchange");
                        }
					}
				}
			}

			//av opm 24860 9 - 23 -08
			if ( null != context.User && !PriceMyLoanConstants.IsEmbeddedPML) 
			{
                LendersOffice.Security.AbstractUserPrincipal p = context.User as LendersOffice.Security.AbstractUserPrincipal;
                SqlParameter[] parameters = {
                                                new SqlParameter("@UserId", p.UserId ), 
                                                new SqlParameter("@IpAddress", RequestHelper.ClientIP )
                                            };
				using( DbDataReader reader =  StoredProcedureHelper.ExecuteReader(p.BrokerId, "CheckIpForUser", parameters))
				{
					//sp returns 0 for not found -1 for empty ip list and pos number for found
					if ( reader.Read() && (int)reader["result"] == 0 ) //reader.read should never be false. If So then there is a bigger problem.
					{
						
						Tools.LogError("Access failed due to IP restriction. Current IP :" + RequestHelper.ClientIP 
							+ " UserId:" + p.UserId.ToString() + " Pml Username :" + p.LoginNm  ); //doing this to make sure its working
						ErrorUtilities.DisplayErrorPage(new CBaseException( LendersOffice.Common.ErrorMessages.Generic, "Access failed due to ip restriction."), false, Guid.Empty, Guid.Empty);
					}
				}
			}

        }
		private static bool IsAccountLocked(Guid userId)	
		{
            if (Guid.Empty == userId)
            {
                return false;
            }

			DateTime nextAvailableLoginTime = GetNextAvailableLoginTime(userId);
			if (nextAvailableLoginTime.Equals(LendersOffice.Common.SmallDateTime.MaxValue))				
				return true;
			else
				return false;
		}

		private static DateTime GetNextAvailableLoginTime(Guid userId)
		{
			DateTime nextAvailableLoginTime = LendersOffice.Common.SmallDateTime.MinValue;

            Guid brokerId;
            DbConnectionInfo.GetConnectionInfoByUserId(userId, out brokerId);

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoginBlockExpirationDateByUserId", parameters))
            {
                if (reader.Read())
                {
                    nextAvailableLoginTime = (DateTime)reader["LoginBlockExpirationD"];
                }
            }

			return nextAvailableLoginTime;
		}

        public static void Signout(LendersOffice.Security.AbstractUserPrincipal principal = null) 
        {
            SecurityEventLogHelper.CreateLogoutLog(principal);

            System.Web.HttpContext.Current.Response.Redirect(PriceMyLoanConstants.LogoutUrl);
        }

        public static void AccessDenial(Guid sLId)
        {
            System.Web.HttpContext.Current.Response.Redirect(PriceMyLoanConstants.AccessDenialdUrl + "?loanid=" + sLId);
        }

		public static void Signout(string errorCode)
		{
			System.Web.HttpContext.Current.Response.Redirect(PriceMyLoanConstants.LogoutUrl + "?errorcode=" + errorCode);
		}

        public static void DownloadCoverSheetList(Guid loanId)
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                return;
            }

            var coverDataList = EDocs.EdocFaxCoversheetBuilder.BuildCoverSheets(loanId);
            var tempFilePath = TempFileUtils.NewTempFilePath();
            BinaryFileHelper.WriteAllBytes(tempFilePath, EDocs.EdocFaxCover.GenerateBarcodePackagePdf(coverDataList.ToArray()));

            RequestHelper.SendFileToClient(context, tempFilePath, "application/pdf", "FaxCover.pdf");
            context.Response.SuppressContent = true;
            context.ApplicationInstance.CompleteRequest();
        }

        public static string GetErrorCodeFromLoginProblem(PrincipalFactory.E_LoginProblem loginProblem)
        {
            switch (loginProblem)
            {
                case PrincipalFactory.E_LoginProblem.IsDisabled:
                case PrincipalFactory.E_LoginProblem.IsLocked:
                case PrincipalFactory.E_LoginProblem.NeedsToWait:
                case PrincipalFactory.E_LoginProblem.InvalidAndWait:
                case PrincipalFactory.E_LoginProblem.InvalidLoginPassword:
                    return loginProblem.ToString();
                case PrincipalFactory.E_LoginProblem.None:
                default:
                    Tools.LogBug("The PriceMyLoanPrincipal is null, but no login problem error code was returned");
                    return "InvalidLoginPassword";
            }
        }

        public static void SetRequestErrorMessage(string errorCode, EncodedLabel message1, EncodedLabel message2)
        {
            switch (errorCode)
            {
                case "IsDisabled":
                    message1.Text = ErrorMessages.AccountDisabled;
                    message2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    break;
                case "IsLocked":
                    message1.Text = ErrorMessages.AccountLocked;
                    message2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    break;
                case "NeedsToWait":
                    message1.Text = ErrorMessages.InvalidLoginPassword;
                    message2.Text = string.Empty;
                    break;
                case "InvalidAndWait":
                    message1.Text = ErrorMessages.InvalidLoginPassword;
                    message2.Text = string.Empty;
                    break;
                case "InvalidLoginPassword":
                    message1.Text = ErrorMessages.InvalidLoginPassword;
                    message2.Text = string.Empty;
                    break;
                case "None":
                default:
                    message1.Text = ErrorMessages.InvalidLoginPassword;
                    message2.Text = string.Empty;
                    Tools.LogBug("Unknown login error code in login page: " + errorCode + ".");
                    break;
            }
        }

        //opm 34111 fs 11/04/09
        public static bool isCitiBroker
        {
            get
            {
                return false;               
            }
        }
	}
}
