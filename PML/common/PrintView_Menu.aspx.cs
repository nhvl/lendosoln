using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using PriceMyLoan.DataAccess;
using LendersOffice.Common;

namespace PriceMyLoan.common
{
	public partial class PrintView_Menu : PriceMyLoan.UI.BasePage
	{
        protected bool m_displayTwoCertificates 
        {
            get 
            {
                // 10/16/2004 dd - Return true if display 1st and 2nd loan certificate.
                string _1st = RequestHelper.GetSafeQueryString("1st");
                string _2nd = RequestHelper.GetSafeQueryString("2nd");
                return _1st != null && _1st != "" && _2nd != null && _2nd != "";
                

            }
        }
        protected string m_1st_sLNm 
        {
            get 
            {
                // 1/26/2005 dd - Pull loan name from database instead of passing over by query string.
                // Reason query string will not work is if loan name contains space.
                Guid sLId = LendersOffice.Common.RequestHelper.GetGuid("1st", Guid.Empty);
                if (Guid.Empty != sLId) 
                {
                    CPageData dataLoan = new CLoanNameData(sLId);
                    dataLoan.InitLoad();
                    return dataLoan.sLNm;
                }
                return "";
            }
        }

        protected string m_2nd_sLNm 
        {
            get 
            {
                // 1/26/2005 dd - Pull loan name from database instead of passing over by query string.
                // Reason query string will not work is if loan name contains space.

                Guid sLId = LendersOffice.Common.RequestHelper.GetGuid("2nd", Guid.Empty);
                if (Guid.Empty != sLId) 
                {
                    CPageData dataLoan = new CLoanNameData(sLId);
                    dataLoan.InitLoad();
                    return dataLoan.sLNm;
                }
                return "";
            }

        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

        private void PageInit(object sender, System.EventArgs e)
        {
            TwoCertificatesTable.Visible = m_displayTwoCertificates;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
