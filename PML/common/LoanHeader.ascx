﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoanHeader.ascx.cs"
    Inherits="PriceMyLoan.common.LoanHeader" EnableViewState="false" %>
<style type="text/css">
    a:link.Feedback
    {
        color: White;
    }
    a:hover.Feedback
    {
        color: Orange;
    }
</style>
<div class="LoanHeader">
    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="TopHeaderBackgroundColor">
        <tbody>
            <tr>
                <td width="1%" height="1%">
                    <asp:PlaceHolder runat="server" ID="PipelineButtonPanel">
                        <input type="button" value="Pipeline" class="ButtonStyle PipelineBtn" nohighlight="nohighlight" />
                    </asp:PlaceHolder>
                </td>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr class="LoanSummaryBackgroundColor">
                                <td class="LoanSummaryItem">
                                    Loan Number:&nbsp;<ml:EncodedLiteral ID="sLNm" runat="server" />
                                </td>
                                <td class="LoanSummaryItem">
                                    Borrower Name:&nbsp;<ml:EncodedLiteral ID="aBNm" runat="server" />
                                </td>
                                <td class="LoanSummaryItem">
                                    Loan Amount:&nbsp;<ml:EncodedLiteral ID="sLAmtCalc" runat="server" />
                                </td>
                            </tr>
                            <tr class="LoanSummaryBackgroundColor">
                                <td class="LoanSummaryItem">
                                    Primary Credit Score: &nbsp;<ml:EncodedLiteral ID="sCreditScoreType1" runat="server" />
                                    &nbsp;&nbsp;<a href='#' class="HelpLink" title='Help'>(explain)</a>
                                </td>
                                <td class="LoanSummaryItem">
                                    Lowest Credit Score: &nbsp;<ml:EncodedLiteral ID="sCreditScoreType2" runat="server" />
                                    &nbsp;&nbsp;<a href='#' class="HelpLink" title='Help'>(explain)</a>
                                </td>
                                <td class="LoanSummaryItem">
                                    LTV:
                                    <ml:EncodedLiteral ID="sLtvR" runat="server" />
                                </td>
                                <td class="LoanSummaryItem">
                                    CLTV:
                                    <ml:EncodedLiteral ID="sCltvR" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <asp:PlaceHolder runat="server" ID="LogoutButtonPanel">
                        <div>
                            <input type="button" value="LOG OFF" class="ButtonStyle LogOffBtn" nohighlight="nohighlight"
                                runat="server">
                        </div>
                    </asp:PlaceHolder>                    
                </td>
            </tr>
        </tbody>
    </table>
</div>



<script type="text/javascript">
    $j(function() {
        $j('.LoanHeader .HelpLink').click(function() {
            LQBPopup.Show(ML.VirtualRoot + '/help/Q00006.html', { 'width': 400, 'height': 150 });
            return false;
        });
        $j('.LoanHeader .PipelineBtn').click(function() {
            self.location = ML.VirtualRoot + "/Main/Pipeline.aspx";
        });
        $j('.LoanHeader .LogOffBtn').click(function() {
            self.location = ML.VirtualRoot + "/logout.aspx";
        });
    });
</script>

