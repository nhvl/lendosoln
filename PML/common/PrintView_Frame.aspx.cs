using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace PriceMyLoan.common
{
	public partial class PrintView_Frame : PriceMyLoan.UI.BasePage
	{
		protected bool IsShowingResult
		{
			// Access calculated member.  This will be true when the
			// spawning page puts "isResult=1" in our query string.

			get
			{
                string isResult = RequestHelper.GetSafeQueryString("isResult");
                if (!string.IsNullOrEmpty(isResult))
                {
                    switch (isResult)
                    {
                        case "True":
                        case "true":
                        case "Yes":
                        case "yes":
                        case "1":
                            return true;
                    }

                }

				return false;
			}
		}

        protected string FrameMenuUrl
        {
            get
            {
                string _1st = Server.UrlEncode(RequestHelper.GetSafeQueryString("1st"));
                string _2nd = Server.UrlEncode(RequestHelper.GetSafeQueryString("2nd"));
                return "PrintView_Menu.aspx?1st=" + _1st + "&2nd=" + _2nd;
            }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
		}

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

}
