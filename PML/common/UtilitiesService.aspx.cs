using LendersOffice.Security;
using PriceMyLoan.Security;
using System;
using System.Web.UI;

namespace PriceMyLoan.Common
{
    public partial class UtilitiesService : LendersOffice.Common.AbstractUtilitiesService
	{
		private PriceMyLoanPrincipal BrokerUser
		{
			get { return Page.User as PriceMyLoanPrincipal; }
		}

        protected override Guid BrokerID 
        {
            get { return BrokerUser.BrokerId; }
        }
        protected override bool HasPermission(Permission p) 
        {
            return BrokerUser.HasPermission(p);
        }
	}
}
