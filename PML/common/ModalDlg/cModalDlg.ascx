<%@ Control Language="c#" AutoEventWireup="false" Codebehind="cModalDlg.ascx.cs" Inherits="PriceMyLoan.common.ModalDlg.cModalDlg" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%
//To use this modal dialog box class, just put the UserControl in your form. You only
//need one instance of this control. On the client side, use javascript to create the 
//cModalDlg object and call the Open function. The VRoot (virtual root) property is
//provided as a helper function for your benefit.
//
//example code:
//
//	function test()
//	{
//		var dlg = new cModalDlg() ;
//		dlg.Open(dlg.VRoot + '/MyPath/MySubPath/MyForm.aspx', MyParam, MyFeatureList) ;
//	}
//	
//	function test2()
//	{
//		var dlg = new cModalDlg() ;
//		var arg = new cGenericArgumentObject() ;
//		dlg.Open(dlg.VRoot + '/MyPath/MySubPath/MyForm.aspx', arg, "dialogHeight:400px; dialogWidth:600px;center:yes; resizable:no; scroll:yes; status=no; help=no;") ;
//	}
//
%>