<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="PriceMyLoan.Common" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <head>
    <title><%= AspxTools.HtmlString( DialogFrameTitleValidator.CheckTitle(RequestHelper.GetSafeQueryString("title")))%></title>
  </head>
  <script language=javascript>
    <!--
      <%--
      // 01/16/08 mf. OPM 19669. It was decided that we need a logout button on
      // every screen of PML. To logout here we call the logout function of our
      // parent window, or if our parent is framed by another DialogFrame, we call
      // its logout. The window openers will handle the cleanup in their unload
      // events.
      --%>
      function f_logoutParent()
      {
        var opener = window.dialogArguments.opener;
        if ( opener )
        {
          if (opener.f_logout ) opener.f_logout();
          else if (opener.top.f_logoutParent) opener.top.f_logoutParent();
        }
        else
        {
          <%-- // We must be modal. Signal that a logout was requested. --%>
          window.dialogArguments.Logout = '1';
          self.close();
        }
      }
    //-->
</script>
  <BODY scroll=no>
  <table cellpadding=0 cellpadding=0 border=0 height='100%' width='100%'>

    <tr><td>
    <IFRAME FrameBorder=0 width='100%' height='100%' NAME="ModalFrame" SRC=<%= AspxTools.SafeUrlFromQueryString("url") %>></IFRAME>
    </td></tr>
  </table>
  </BODY>
</HTML>
