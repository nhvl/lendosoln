using System;
using System.Data;
using System.Drawing;
using System.Web;
using DataAccess;
using LendersOffice.Common;

namespace PriceMyLoan.Common
{
	/// <summary>
	///	Summary description for ModelessDlg.
	/// </summary>

	public partial  class ModelessDlg : PriceMyLoan.common.ModalDlg.cModalDlg
	{
		/// <summary>
		/// Initialize this user control.
		/// </summary>

		protected void ControlLoad( object sender , System.EventArgs a )
		{
			// 11/16/2004 kb - Initialize page by including modeless
			// script helper functions.  We only want this to go into
			// the containing page once.

			Page.ClientScript.RegisterClientScriptBlock(Page.GetType(),  "ModelessDlg" , String.Format( @"<script type=""text/javascript"" src=""{0}""></script>"
				, Tools.VRoot + "/inc/ModelessDlg.js"
				));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Load += new System.EventHandler( ControlLoad );
		}
		#endregion

		public static string FrameSafeUrl( string Url)
		{
			// 01/08/08 mf. OPM 19655. 
			// In DialogFrame.aspx, we display the provided url in an iframe.
			// We will cheaply check that is a relative, not absolute, url.
			//
			// 03/14/08 mf. There is a possiblity there will be an absolute url in the
			// querystring of a relative url. We should only throw if this string starts
			// with an absolute url (http|https).

			if ( Url.TrimWhitespaceAndBOM().StartsWith("http") )
			{
				throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Client attempted to access url: " + Url );
			}
			return Url;
		}

	}

}
