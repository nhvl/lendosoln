﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PriceMyLoan.Common;
using LendersOffice.Email;

using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace PriceMyLoan.Common
{
    public static class DialogFrameTitleValidator
    {
        public static string CheckTitle(string title)
        {
            if (string.IsNullOrEmpty(title)) return "";              //we dont care about empty titles.
            foreach (string acceptableTitle in PriceMyLoanConstants.AcceptableDialogFrameTitles)
            {
                if (acceptableTitle.TrimWhitespaceAndBOM() == title.TrimWhitespaceAndBOM())
                    return title;
            }

            return "";

        }

        /*OPM 73594. Extension method for Menu to select a menu item given an index. Move this to separate static class later*/
        public static void SelectedIndex(this Menu menu, int idx)
        {
            if (idx < menu.Items.Count)
            {
                menu.Items[idx].Selected = true;
            }
        }
    }
}
