using System;
using System.IO;
using System.Web;
using System.Xml.Xsl;
using System.Configuration;

using DataAccess;

namespace PriceMyLoan.Common
{
	public class PriceMyLoanConstants
	{
        public const int CookieExpireMinutes = 240; // Ext. 284403. 4 hours for PML Cookie.
        public const string LogoutUrl = "~/logout.aspx";
        public const string PipelineUrl = "~/main/baseframe.aspx";
        public const string AccessDenialdUrl = "~/main/AccessDenial.aspx";
        public const string SetSecurityQuestionsUrl = "~/PasswordReset/SetSecurityQuestions.aspx";
        public const string TpoPortalNavigationFilename = "~/main/TpoLoanNavigation.xml.config";

        /// <summary>
        /// Return true this url embedded in LO frame.
        /// </summary>
        public static bool IsEmbeddedPML 
        {
            get 
            { 
                string s = Tools.VRoot.ToLower();
                return s.IndexOf("/embeddedpml") >= 0; 
            }
        }

		// 03/28/08 mf OPM 21030. Citi has a message to show between 7:30 and 8:30am weekdays
		public static bool IsCitiUpdatingRates
		{
			get 
			{ 
				// Assumption: server time is correct and Pacific.
				TimeSpan centralTime = DateTime.Now.AddHours(2).TimeOfDay;
				DayOfWeek today = DateTime.Now.DayOfWeek;

				return ( today != DayOfWeek.Saturday  // Currently not concerned with 10-12pm day disagreement.
					&& today != DayOfWeek.Sunday
					&& centralTime > TimeSpan.FromHours(7.5)
					&& centralTime < TimeSpan.FromHours(8.5) );
			}
		}


        public const string SortType_ByNoteRate = "NoteR";
        public const string SortType_ByName = "Name";
        public const string SortType_BestPrice = "BestX";
        public static string[] AcceptableDialogFrameTitles = new string[] { "FHA TOTAL Scorecard", "Edit Record", "Result", "Send to Loan Prospector", "Send to Loan Product Advisor", "Message to Lender", "Help", "Warning", "Public Records", "Preview Certificate", "Confirmation", "Summary", "Notes", "Manage Users", "Edit User", "Change Password", "Edit Liabilities", "Add Liability", "Messege to Lender", "Submit Directly To Fannie Mae", "Merit Matrix", "Confirmation", "Preview", "Public Records", "Email Preview Certificate", "Lock Unavailable" };
	}
}


