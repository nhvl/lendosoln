﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace PriceMyLoan.common
{
    public partial class LQBPopup : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var page = Page as BasePage;

            if (page != null)
            {
                page.RegisterJsScript("LQBPopup.js");
            }
        }
    }
}