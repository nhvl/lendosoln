<%@ Import Namespace="DataAccess"%>
<%@ Page language="c#" Codebehind="PrintView_Frame.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.common.PrintView_Frame" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<TITLE>PrintView_Frame</TITLE>
		<script src="ModalDlg/CModalDlg.js">
		</script>
	
	</HEAD>
			<script type="text/javascript">

	<!--

	function _init() {
		resize( 780 , 580 );
		self.focus();
        f_registerMe( 'printview' );
	}

	function f_registerMe( sName ) { try
	{
		if( window.parent != null && window.parent.opener != null )
		{
			if( window.parent.opener.closed == false )
			{
				if( window.parent.opener.f_updateChildName != null )
				{
					window.parent.opener.f_updateChildName( sName );
				}
			}
		}
	}
	finally
	{
	}}

	function f_resultClose() { try
	{
		if( window.parent != null && window.parent.opener != null )
		{
			if( window.parent.opener.closed == false )
			{
				if( window.parent.opener.f_onResultClose != null )
				{
					window.parent.opener.f_onResultClose();
				}
			}
		}
	}
	finally
	{
	} 
}

function f_onFramesetUnload() {
  if (<%= AspxTools.JsBool(IsShowingResult) %>)
  {
    f_resultClose();
  }
}

	//-->

		</script>
	<frameset rows="55,*" frameborder="yes"  onbeforeunload="f_registerMe( '' );" onunload="f_onFramesetUnload();">
		<frame name=frmMenu src=<%= AspxTools.SafeUrl(FrameMenuUrl) %> scrolling=no>
		<frame name=frmBody src=<%= AspxTools.SafeUrlFromQueryString("body_url") %>>
	</frameset>

</HTML>
