﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResultsFilter.ascx.cs" Inherits="PriceMyLoan.webapp.ResultsFilter" %>
<%@ Register TagPrefix="PCF" TagName="ProductCodeFilterPopup" Src="~/webapp/ProductCodeFilterPopup.ascx" %>
<script type="text/javascript">
    function getRenderResultModeT() {
        return $j('#AllProgramsRb').attr('checked') == 'checked' ? 0 : 1; // TODO: this should be in the model
    }

    function loadResult(args) {
        if (typeof args !== 'undefined' && args !== null) {
            PML.updateAll(args);
        }

        if (ML.EnablePmlUIResults3) {
            $j("#PricingResultsContainer").toggleClass("mask", false);
            $j(".content-container .left-panel, #ResultsFilterContainer").toggleClass("mask", true);
            $j("#ResultsContainer").hide();
            var args = {
                sLienQualifyModeT: 0,
            };

            IsPricing2nd = false;
            requestPricing(args);
        } else {
            document.getElementById('resultsIframe').src = '../main/mainNewUI.aspx?loanId=' + PML.getLoanId() + "&isBestPrice=" + getRenderResultModeT();
        }
    }

    function loadError() {
        if (arguments[0] && arguments[0].value.PermissionError) {
            alert(arguments[0].value.PermissionError);
            window.location = window.location;

            return true;
        }

        return false;
    }

    function loadPricingResult() {
        // Force loan officer assignment in order to run pricing.  per 185732, moving this to pricing time.
        if (PML.requiresEmployeeAssignmentForPricing()) {
            PML.promptEmployeeAssignmentForPricing(loadPricingResult);
            return;
        }
        if (PML.getIsRefreshing()) {
            PML.addPostRefreshCallback(loadPricingResult);
            return;
        }
        else if (PML.getIsValid()) { // at least the property and loan info must be valid
            Results.AreDisplayed = false;
            Results.AreStale = false;
            $j('#ResultsPriceBtn').attr('disabled', true);
            $j('#PriceBtn').attr('disabled', true);
            $j(window).trigger('loan/price_started');
            if (PML.getIsDirty()) {
                PML.async_save(loadResult);
            } else {
                loadResult();
            }
        }
    }

    function f_validateFilter() {
        var termValid = $j('#sProdFilterDue10Yrs').is(':checked')
                     || $j('#sProdFilterDue15Yrs').is(':checked')
                     || $j('#sProdFilterDue20Yrs').is(':checked')
                     || $j('#sProdFilterDue25Yrs').is(':checked')
                     || $j('#sProdFilterDue30Yrs').is(':checked')
                     || $j('#sProdFilterDueOther').is(':checked');
        $j('#TermValidator').toggle(termValid == false);
        var amortizationTypeValid = $j('#sProdFilterFinMethFixed').is(':checked')
                                 || $j('#sProdFilterFinMeth3YrsArm').is(':checked')
                                 || $j('#sProdFilterFinMeth5YrsArm').is(':checked')
                                 || $j('#sProdFilterFinMeth7YrsArm').is(':checked')
                                 || $j('#sProdFilterFinMeth10YrsArm').is(':checked')
                                 || $j('#sProdFilterFinMethOther').is(':checked');
        $j('#AmortizationTypeValidator').toggle(amortizationTypeValid == false);
        var productTypeValid = $j('#sProdIncludeNormalProc').is(':checked')
                            || $j('#sProdIncludeMyCommunityProc').is(':checked')
                            || $j('#sProdIncludeHomePossibleProc').is(':checked')
                            || $j('#sProdIncludeFHATotalProc').is(':checked')
                            || $j('#sProdIncludeVAProc').is(':checked')
                            || $j('#sProdIncludeUSDARuralProc').is(':checked');
        $j('#ProductTypeValidator').toggle(productTypeValid == false);

        var paymentTypeValid = $('#sProdFilterPmtTPI').is(':checked')
                     || $('#sProdFilterPmtTIOnly').is(':checked');

        $('#PaymentTypeValidator').toggle(paymentTypeValid == false);

        var filterValid = termValid && amortizationTypeValid && productTypeValid && paymentTypeValid;

        if (filterValid) {
            var args = {};
            args["loanid"] = PML.getLoanId();
            args["sProdFilterDue10Yrs"] = $j('#sProdFilterDue10Yrs').is(':checked');
            args["sProdFilterDue15Yrs"] = $j('#sProdFilterDue15Yrs').is(':checked');
            args["sProdFilterDue20Yrs"] = $j('#sProdFilterDue20Yrs').is(':checked');
            args["sProdFilterDue25Yrs"] = $j('#sProdFilterDue25Yrs').is(':checked');
            args["sProdFilterDue30Yrs"] = $j('#sProdFilterDue30Yrs').is(':checked');
            args["sProdFilterDueOther"] = $j('#sProdFilterDueOther').is(':checked');
            args["sProdFilterFinMethFixed"] = $j('#sProdFilterFinMethFixed').is(':checked');
            args["sProdFilterFinMeth3YrsArm"] = $j('#sProdFilterFinMeth3YrsArm').is(':checked');
            args["sProdFilterFinMeth5YrsArm"] = $j('#sProdFilterFinMeth5YrsArm').is(':checked');
            args["sProdFilterFinMeth7YrsArm"] = $j('#sProdFilterFinMeth7YrsArm').is(':checked');
            args["sProdFilterFinMeth10YrsArm"] = $j('#sProdFilterFinMeth10YrsArm').is(':checked');
            args["sProdFilterFinMethOther"] = $j('#sProdFilterFinMethOther').is(':checked');
            args["sProdIncludeNormalProc"] = $j('#sProdIncludeNormalProc').is(':checked')
            args["sProdIncludeMyCommunityProc"] = $j('#sProdIncludeMyCommunityProc').is(':checked')
            args["sProdIncludeHomePossibleProc"] = $j('#sProdIncludeHomePossibleProc').is(':checked')
            args["sProdIncludeFHATotalProc"] = $j('#sProdIncludeFHATotalProc').is(':checked')
            args["sProdIncludeVAProc"] = $j('#sProdIncludeVAProc').is(':checked')
            args["sProdIncludeUSDARuralProc"] = $j('#sProdIncludeUSDARuralProc').is(':checked');
            args["sIsRenovationLoan"] = $j('#sIsRenovationLoan').is(':checked');
            args["sProdFilterPmtTPI"] = $j('#sProdFilterPmtTPI').is(':checked');
            args["sProdFilterPmtTIOnly"] = $j('#sProdFilterPmtTIOnly').is(':checked');

            if (typeof (RetrieveSelectedProductCodes) === 'function') {
                args["sSelectedProductCodeFilter"] = RetrieveSelectedProductCodes();
            }

            gService.PML.acall("GetNumberOfLoanProgramsFor1st", args, loadNumProgramsResult, null);
        }

        PML.model.loan["isFilterValid"] = filterValid;
        $j(window).trigger("loan/changed/isFilterValid", [filterValid]);
    }

    function loadNumProgramsResult(result) {
        if (result == null) {
            return;
        }

        if (!result.error) {
            if (typeof (result.value["ErrorMessage"]) != "undefined") {
                console.log('Got an error!');
                console.log(result.value["ErrorMessage"]);
                $j('#NumLPs').html("Number of Programs: N/A");
            }
            else {
                $j('#NumLPs').html("Number of Programs: " + result.value["NumLPs"]);
            }
        }
    }

    function f_smartFilters() { //OPM 115546 - 8/26/2013 pa        
        if (PML.model.loan["sProdIsDuRefiPlus"] == "True") {
            $j('#sProdIncludeNormalProc').attr("checked", "checked");
            PML.model.loan["sProdIncludeNormalProc"] = true;
            $j('#sProdIncludeNormalProc').attr("disabled", "disabled");
        }
        else {
            $j('#sProdIncludeNormalProc').removeAttr("disabled");
        }

        if (PML.model.loan["sLPurposeTPe"] == PML.constants.E_sLPurposeT.FhaStreamlinedRefinance) {
            $j('#sProdIncludeFHATotalProc').attr("checked", "checked");
            PML.model.loan["sProdIncludeFHATotalProc"] = true;
            $j('#sProdIncludeFHATotalProc').attr("disabled", "disabled");
        }
        else {
            $j('#sProdIncludeFHATotalProc').removeAttr("disabled");
        }

        if (PML.model.loan["sLPurposeTPe"] == PML.constants.E_sLPurposeT.VaIrrrl) {
            $j('#sProdIncludeVAProc').attr("checked", "checked");
            PML.model.loan["sProdIncludeVAProc"] = true;
            $j('#sProdIncludeVAProc').attr("disabled", "disabled");
        }
        else {
            $j('#sProdIncludeVAProc').removeAttr("disabled");
        }
        f_validateFilter();
    }

    function f_clearClientPriceTiming() {
        if (typeof (Storage) !== "undefined") {
            // Only support client side timing if DOM Storage is enable.
            sessionStorage.removeItem("ClientPriceTiming_Start");
            sessionStorage.removeItem("ClientPriceTiming_RenderResultStart");
            sessionStorage.removeItem("ClientPriceTiming_RenderResultEnd");
            sessionStorage.removeItem("ClientPriceTiming_End");
            sessionStorage.removeItem("ClientPriceTiming_RequestId");
            sessionStorage.removeItem("ClientPriceTiming_BeforeSubmit");
            sessionStorage.removeItem("ClientPriceTiming_ServerPolling");
            sessionStorage.removeItem("ClientPriceTiming_NavTiming");
        }
    }
    jQuery(function ($j) {
        $j(window).on('loan/view_update_done', f_smartFilters);
        $j(window).on('loan/changed/sProdFilterDue10Yrs', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterDue15Yrs', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterDue20Yrs', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterDue25Yrs', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterDue30Yrs', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterDueOther', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterPmtTPI', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterPmtTIOnly', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterFinMethFixed', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterFinMeth3YrsArm', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterFinMeth5YrsArm', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterFinMeth7YrsArm', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterFinMeth10YrsArm', f_validateFilter);
        $j(window).on('loan/changed/sProdFilterFinMethOther', f_validateFilter);
        $j(window).on('loan/changed/sProdIncludeNormalProc', f_validateFilter);
        $j(window).on('loan/changed/sProdIncludeMyCommunityProc', f_validateFilter);
        $j(window).on('loan/changed/sProdIncludeHomePossibleProc', f_validateFilter);
        $j(window).on('loan/changed/sProdIncludeFHATotalProc', function() {
            f_validateFilter();
            
            if (typeof f_validate_sAsIsAppraisedValuePeval === 'function') {
                f_validate_sAsIsAppraisedValuePeval();
                f_toggleButtons();
            }
        });
        $j(window).on('loan/changed/sProdIncludeVAProc', f_validateFilter);
        $j(window).on('loan/changed/sProdIncludeUSDARuralProc', f_validateFilter);

        $j(window).on("PageIsValid", function (e, pageValid) {
            $j('#ResultsPriceBtn').attr('disabled', !pageValid);
        });

        $j('#ResultsPriceBtn').on('click', function () {
            $j(window).trigger('loan/price');
        });

        $j(window).on('loan/price', function () {
            if (typeof (Storage) !== "undefined") {
                // Only support client side timing if DOM Storage is enable.
                f_clearClientPriceTiming();
                sessionStorage["ClientPriceTiming_Start"] = (new Date()).getTime();
            }
            loadPricingResult();
        });
        $j(window).on('loan/price_done', function () {
            $j(".mask").toggleClass("mask", false)
            // re-enable the pricing buttons once we're done with pricing
            if (PML.getIsValid()) {
                $j('#ResultsPriceBtn').attr('disabled', false);
                $j('#PriceBtn').attr('disabled', false);

                if (typeof (Storage) !== "undefined") {
                    // Only support client side timing if DOM Storage is enable.
                    sessionStorage["ClientPriceTiming_End"] = (new Date()).getTime();

                    var args = {};
                    args.LoanID = ML.sLId;
                    var propertyList = ["ClientPriceTiming_Start", "ClientPriceTiming_RenderResultStart", "ClientPriceTiming_RenderResultEnd", "ClientPriceTiming_End", "ClientPriceTiming_RequestId", "ClientPriceTiming_ServerPolling", "ClientPriceTiming_BeforeSubmit", "ClientPriceTiming_NavTiming"];

                    for (var i = 0; i < propertyList.length; i++) {
                        args[propertyList[i]] = sessionStorage[propertyList[i]];
                    }

                    gService.utils.call("RecordClientTiming", args);
                }
            }
        });

        $j("#advancedFilterBtn").click(function () {
            if (typeof OpenProductCodeFilterPopup === 'function') {
                OpenProductCodeFilterPopup();
            }
        });

        var timer;
        var delay = 500;
        $j("#advancedFilterBtn").hover(
            function (e) {
                var parent = $(this).closest('.container');
                timer = setTimeout(function () {
                    if (typeof DisplayTooltip === 'function') {
                        DisplayTooltip(e, parent);
                    }
                }, delay);
            },
            function () {
                clearTimeout(timer);
                if (typeof HideTooltip === 'function') {
                    HideTooltip();
                }
            }
        );
    });
</script>
<style type="text/css">
    #ResultsFilterContainer {
        border: 1px solid gainsboro;
        background-color: #FFF;
    }

        #ResultsFilterContainer label {
            display: inline;
            float: none;
            width: auto;
            padding-right: 0px;
        }

        #ResultsFilterContainer .SectionHeader {
            font-weight: bold;
            text-decoration: underline;
            font-size: 1em;
            color: Black;
            line-height: 17px;
            width: 100%;
        }

    #ResultsFilterTable {
        width: calc(100% - 60px);
        margin-left: auto;
        margin-right: auto;
        table-layout: fixed;
        min-width: 760px;
    }

    #ResultsFilterTable label, #ResultsFilterTable input {
        vertical-align: middle;
    }

    .flex-container {
        display: flex;
        align-items: center;
    }

    .flex-container > div:nth-child(odd) {
        flex: .25 1 154px;
    }

    .flex-container > div:nth-child(even) {
        flex: 1 1 125px;
    }

    #ResultsFilterTitle {
        font-size: 1.2em;
        font-weight: bold;
        color: #ff9933;
        margin-bottom: 5px;
        padding: 3px;
    }

    #ResultsPriceBtn {
        width: 22em;
    }

    .results-btn-container {
        width: 100px;
    }
</style>

<PCF:ProductCodeFilterPopup ID="ProductCodeFilterPopup" runat="server" />
<div id="ResultsFilterContainer" class="container">
    <div id="ResultsFilterTitle">Results Filter</div>
    <table id="ResultsFilterTable">
        <tr>
            <td class="SectionHeader">Term
                <img id="TermValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field" />
            </td>
            <td class="SectionHeader">Amortization Type
                <img id="AmortizationTypeValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field" />
            </td>
            <td class="SectionHeader">Product Type
                <img id="ProductTypeValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field" />
            </td>
            <td rowspan="3" class="results-btn-container">
                <input type="button" id="ResultsPriceBtn" value="Price" style="width: 100px; height: 50px;" />
            </td>
        </tr>
        <tr>
            <td>
                <div class="flex-container">
                    <div>
                        <input id="sProdFilterDue10Yrs" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterDue10Yrs">10 Year</label>
                    </div>
                    <div>
                        <input id="sProdFilterDue25Yrs" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterDue25Yrs">25 Year</label>
                    </div>
                </div>
            </td>

            <td>
                <div class="flex-container">
                    <div>
                        <input id="sProdFilterFinMethFixed" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterFinMethFixed">Fixed</label>
                    </div>
                    <div>
                        <input id="sProdFilterFinMeth7YrsArm" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterFinMeth7YrsArm">7 Year ARM</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="flex-container">
                    <div>
                        <input id="sProdIncludeNormalProc" type="checkbox" data-calc data-refresh />
                        <label for="sProdIncludeNormalProc">Conventional</label>
                    </div>
                    <div>
                        <input id="sProdIncludeFHATotalProc" type="checkbox" data-calc data-refresh />
                        <label for="sProdIncludeFHATotalProc">FHA</label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="flex-container">
                    <div>
                        <input id="sProdFilterDue15Yrs" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterDue15Yrs">15 Year</label>
                    </div>
                    <div>
                        <input id="sProdFilterDue30Yrs" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterDue30Yrs">30 Year</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="flex-container">
                    <div>
                        <input id="sProdFilterFinMeth3YrsArm" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterFinMeth3YrsArm">3 Year ARM</label>
                    </div>
                    <div>
                        <input id="sProdFilterFinMeth10YrsArm" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterFinMeth10YrsArm">10 Year ARM</label>
                    </div>
                </div>
            </td>
            <td>

                <div class="flex-container">
                    <div>
                        <input id="sProdIncludeMyCommunityProc" type="checkbox" data-calc data-refresh />
                        <label for="sProdIncludeMyCommunityProc">HomeReady</label>
                    </div>
                    <div>
                        <input id="sProdIncludeVAProc" type="checkbox" data-calc data-refresh />
                        <label for="sProdIncludeVAProc">VA</label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="flex-container">
                    <div>
                        <input id="sProdFilterDue20Yrs" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterDue20Yrs">20 Year</label>
                    </div>
                    <div>
                        <input id="sProdFilterDueOther" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterDueOther">Other</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="flex-container">
                    <div width="13%">
                        <input id="sProdFilterFinMeth5YrsArm" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterFinMeth5YrsArm">5 Year ARM</label>
                    </div>
                    <div width="16%">
                        <input id="sProdFilterFinMethOther" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterFinMethOther">Other</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="flex-container">
                    <div width="18%">
                        <input id="sProdIncludeHomePossibleProc" type="checkbox" data-calc data-refresh />
                        <label for="sProdIncludeHomePossibleProc">Home Possible</label>
                    </div>
                    <div width="13%">
                        <input id="sProdIncludeUSDARuralProc" type="checkbox" data-calc data-refresh />
                        <label for="sProdIncludeUSDARuralProc">USDA</label>
                    </div>
                </div>
            </td>
            <td rowspan="3" style="text-align: center;">
                <span id="NumLPs"></span>
            </td>
        </tr>
        <tr>
            <td class="SectionHeader">Payment Type
                <img id="PaymentTypeValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field" />
            </td>
        </tr>
        <tr>
            <td>
                <div class="flex-container">
                    <div width="18%">
                        <input id="sProdFilterPmtTPI" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterPmtTPI">P&I</label>
                    </div>
                    <div width="13%">
                        <input id="sProdFilterPmtTIOnly" type="checkbox" data-calc data-refresh />
                        <label for="sProdFilterPmtTIOnly">I/O</label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px" class="SectionHeader" colspan="5" displaytypecell>Results Display Type
            </td>
        </tr>
        <tr>
            <td displaytypecell>
                <input type="radio" id="BestPricingRb" name="ResultsDisplayType" checked="checked" data-calc />
                <label for="BestPricingRb">Display Best Prices Per Product</label>
            </td>
            <td displaytypecell>
                <input type="radio" id="AllProgramsRb" name="ResultsDisplayType" data-calc />
                <label for="AllProgramsRb">Display All Programs Per Product</label>
            </td>
            <td colspan="2" style="padding-top: 5px;" displaytypecellhidden></td>
        </tr>


        <tr id="advancedFilterRow" runat="server">
            <td colspan="4">
                <a id="advancedFilterBtn">Advanced Filter Options</a>
            </td>
        </tr>


    </table>
</div>
