﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Loan1003LinkLiabilityWIthReo.aspx.cs" Inherits="PriceMyLoan.webapp.Loan1003LinkLiabilityWIthReo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        #form1 .btn {
            margin: 5px;
        }
    </style>
    <title>Select REO</title>
</head>
<body>
    <script type="text/javascript">
        $j(document).ready(function () {
            matchedReoRecordId_onchange();
            TPOStyleUnification.Components();
        });

        function matchedReoRecordId_onchange() {
            $j("#btnAddToReo").prop("disabled", $j("#matchedReoRecordId option:selected").index() == 0);
        }

        function addToReo() {
            parent.LQBPopup.Return({
                OK: true,
                ReoId: $j("#matchedReoRecordId").val()
            });
        }
    </script>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" onclick="parent.LQBPopup.Return({OK: false});"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title">Select REO</h4>
        </div>
        <div class="modal-body">
            <asp:dropdownlist id="matchedReoRecordId" runat="server" onchange="matchedReoRecordId_onchange();"></asp:dropdownlist>
        </div>
        <div class="modal-footer">
            <button class="btn btn-flat" id="btnAddToReo" type="button" onclick="addToReo();">Link with REO</button>
            <button class="btn btn-flat" type="button" onclick="parent.LQBPopup.Return({OK: false});">Cancel</button>
        </div>
    </form>
</body>
</html>
