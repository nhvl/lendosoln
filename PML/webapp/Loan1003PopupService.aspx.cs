﻿//-----------------------------------------------------------------------
// <author>Eduardo Michel</author>
// <summary>Loads and Saves the Dialogs from PML's 1003 Editor.</summary>
//-----------------------------------------------------------------------

namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using Security;
    using LendersOffice.Constants;

    /// <summary>
    /// This service handles the loading and saving for the Employment and Assets Dialogs found in '10031' and 2 (PML).
    /// </summary>
    public partial class Loan1003PopupService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Calls any custom method, depending on the method parameter.
        /// </summary>
        /// <param name="methodName">The name of the method to be called.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadEmploymentRecords":
                    this.LoadEmploymentRecords();
                    break;
                case "SaveEmploymentRecords":
                    this.SaveEmploymentRecords();
                    break;
                case "LoadRealEstateRecords":
                    this.LoadRealEstateRecords();
                    break;
                case "SaveRealEstateRecords":
                    this.SaveRealEstateRecords();
                    break;
                case "LoadNonRealEstateAssets":
                    this.LoadNonRealEstateAssets();
                    break;
                case "SaveNonRealEstateAssetRecords":
                    this.SaveNonRealEstateAssetRecords();
                    break;
                case "SaveLiabilityRecords":
                    this.SaveLiabilityRecords();
                    break;
                case "LinkLiabilityWithReo":
                    this.LinkLiabilityWithReo();
                    break;
            }
        }

        public void LinkLiabilityWithReo()
        {
            Guid reoId = GetGuid("ReoId");
            Guid loanId = GetGuid("LoanID");
            Guid appId = GetGuid("ApplicationID");
            Guid liabilityId = GetGuid("LiabilityId");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(Loan1003PopupService));
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(appId);

            ILiaCollection liabilities = dataApp.aLiaCollection;
            ILiabilityRegular liabilityRecord = liabilities.GetRegRecordOf(liabilityId);

            liabilityRecord.MatchedReRecordId = reoId;
            liabilityRecord.Update();

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        /// <summary>
        /// Loads Employment Records.
        /// </summary>
        public void LoadEmploymentRecords()
        {
            CPageData dataLoan = new CBorrowerInfoData(GetGuid("loanid"));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(GetGuid("applicationID"));

            IEmpCollection borrowerRecordList = dataApp.aBEmpCollection;
            IEmpCollection coborrowerRecordList = dataApp.aCEmpCollection;

            IPrimaryEmploymentRecord borrowerPrimary = borrowerRecordList.GetPrimaryEmp(true);
            if (borrowerPrimary != null)
            {
                this.SetResult("aBPrimaryIsSelfEmplmt", borrowerPrimary.IsSelfEmplmt);
                this.SetResult("aBPrimaryEmplrNm", borrowerPrimary.EmplrNm);
                this.SetResult("aBPrimaryEmplrAddr", borrowerPrimary.EmplrAddr);
                this.SetResult("aBPrimaryEmplrCity", borrowerPrimary.EmplrCity);
                this.SetResult("aBPrimaryEmplrBusPhone", borrowerPrimary.EmplrBusPhone);
                this.SetResult("aBPrimaryEmplrZip", borrowerPrimary.EmplrZip);
                this.SetResult("Loan1003pg1_Employment_aBPrimaryEmplrState", borrowerPrimary.EmplrState);
                this.SetResult("Loan1003pg1_Employment_aBPrimaryEmpltStartD", borrowerPrimary.EmpltStartD_rep);
                this.SetResult("Loan1003pg1_Employment_aBPrimaryProfStartD", borrowerPrimary.ProfStartD_rep);
                this.SetResult("aBPrimaryJobTitle", borrowerPrimary.JobTitle);
            }

            IPrimaryEmploymentRecord coborrowerPrimary = coborrowerRecordList.GetPrimaryEmp(true);
            if (borrowerPrimary != null)
            {
                this.SetResult("aCPrimaryIsSelfEmplmt", coborrowerPrimary.IsSelfEmplmt);
                this.SetResult("aCPrimaryEmplrNm", coborrowerPrimary.EmplrNm);
                this.SetResult("aCPrimaryEmplrAddr", coborrowerPrimary.EmplrAddr);
                this.SetResult("aCPrimaryEmplrCity", coborrowerPrimary.EmplrCity);
                this.SetResult("aCPrimaryEmplrBusPhone", coborrowerPrimary.EmplrBusPhone);
                this.SetResult("aCPrimaryEmplrZip", coborrowerPrimary.EmplrZip);
                this.SetResult("Loan1003pg1_Employment_aCPrimaryEmplrState", coborrowerPrimary.EmplrState);
                this.SetResult("Loan1003pg1_Employment_aCPrimaryEmpltStartD", coborrowerPrimary.EmpltStartD_rep);
                this.SetResult("Loan1003pg1_Employment_aCPrimaryProfStartD", coborrowerPrimary.ProfStartD_rep);
                this.SetResult("aCPrimaryJobTitle", coborrowerPrimary.JobTitle);
            }

            this.SetResult("numBEmployments", borrowerRecordList.CountRegular);

            for (int i = 0; i < borrowerRecordList.CountRegular; i++)
            {
                IRegularEmploymentRecord record = borrowerRecordList.GetRegularRecordAt(i);

                this.SetResult("bIsSelfEmplmt" + (i + 1), record.IsSelfEmplmt);
                this.SetResult("bEmplrNm" + (i + 1), record.EmplrNm);
                this.SetResult("bEmplrAddr" + (i + 1), record.EmplrAddr);
                this.SetResult("bEmplrCity" + (i + 1), record.EmplrCity);
                this.SetResult("bJobTitle" + (i + 1), record.JobTitle);
                this.SetResult("bEmplrBusPhone" + (i + 1), record.EmplrBusPhone);
                this.SetResult("bEmplrZip" + (i + 1), record.EmplrZip);
                this.SetResult("bEmplrState" + (i + 1), record.EmplrState);
                this.SetResult("Loan1003pg1_Employment_bEmplmtStartD" + (i + 1), record.EmplmtStartD_rep);
                this.SetResult("Loan1003pg1_Employment_bEmplmtEndD" + (i + 1), record.EmplmtEndD_rep);
                this.SetResult("bRecordID" + (i + 1), record.RecordId);
            }


            this.SetResult("numCEmployments", coborrowerRecordList.CountRegular);
            for (int i = 0; i < coborrowerRecordList.CountRegular; i++)
            {
                IRegularEmploymentRecord record = coborrowerRecordList.GetRegularRecordAt(i);

                this.SetResult("cIsSelfEmplmt" + (i + 1), record.IsSelfEmplmt);
                this.SetResult("cEmplrNm" + (i + 1), record.EmplrNm);
                this.SetResult("cEmplrAddr" + (i + 1), record.EmplrAddr);
                this.SetResult("cEmplrCity" + (i + 1), record.EmplrCity);
                this.SetResult("cJobTitle" + (i + 1), record.JobTitle);
                this.SetResult("cEmplrBusPhone" + (i + 1), record.EmplrBusPhone);
                this.SetResult("cEmplrZip" + (i + 1), record.EmplrZip);
                this.SetResult("cEmplrState" + (i + 1), record.EmplrState);
                this.SetResult("Loan1003pg1_Employment_cEmplmtStartD_rep" + (i + 1), record.EmplmtStartD_rep);
                this.SetResult("Loan1003pg1_Employment_cEmplmtEndD_rep" + (i + 1), record.EmplmtEndD_rep);
                this.SetResult("cRecordID" + (i + 1), record.RecordId);
            }
        }

        /// <summary>
        /// Saves Employment Records.
        /// </summary>
        public void SaveEmploymentRecords()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanid"), typeof(Loan1003PopupService));
            dataLoan.InitSave(this.GetInt("sFileVersion"));

            CAppData dataApp = dataLoan.GetAppData(GetGuid("applicationID"));

            string borrowerPrimaryData = GetString("bPrimary");
            string coborrowerPrimaryData = GetString("cPrimary");

            string borrowerPrev = GetString("bPrevEmployments");
            string coborrowerPrev = GetString("cPrevEmployments");

            EmploymentField borrowerPrimary = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonDeserializer<EmploymentField>(borrowerPrimaryData);
            EmploymentField coborrowerPrimary = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonDeserializer<EmploymentField>(coborrowerPrimaryData);

            var borrowerPreviousEmployments = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<EmploymentField>>(borrowerPrev);
            var coborrowerPreviousEmployments = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<EmploymentField>>(coborrowerPrev);

            List<IRegularEmploymentRecord> foundBorrowerRecords = new List<IRegularEmploymentRecord>();
            List<IRegularEmploymentRecord> foundCoBorrowerRecords = new List<IRegularEmploymentRecord>();

            IEmpCollection borrowerRecordList = dataApp.aBEmpCollection;
            IEmpCollection coborrowerRecordList = dataApp.aCEmpCollection;

            IPrimaryEmploymentRecord borrowerPrimaryRec = borrowerRecordList.GetPrimaryEmp(EmploymentHasValue(borrowerPrimary));
            if (borrowerPrimaryRec != null)
            {
                borrowerPrimaryRec.EmplrNm = borrowerPrimary.EmplrNm;
                borrowerPrimaryRec.EmplrBusPhone = borrowerPrimary.EmplrBusPhone;
                borrowerPrimaryRec.EmplrAddr = borrowerPrimary.EmplrAddr;
                borrowerPrimaryRec.EmplrZip = borrowerPrimary.EmplrZip;
                borrowerPrimaryRec.EmplrCity = borrowerPrimary.EmplrCity;
                borrowerPrimaryRec.EmplrState = borrowerPrimary.EmplrState;
                borrowerPrimaryRec.JobTitle = borrowerPrimary.JobTitle;
                borrowerPrimaryRec.SetEmploymentStartDate(borrowerPrimary.EmplmtStartD);
                borrowerPrimaryRec.SetProfessionStartDate(borrowerPrimary.ProfStartD);
                borrowerPrimaryRec.IsSelfEmplmt = borrowerPrimary.IsSelfEmplmt;
            }

            IPrimaryEmploymentRecord coborrowerPrimaryRec = coborrowerRecordList.GetPrimaryEmp(EmploymentHasValue(coborrowerPrimary));
            if (coborrowerPrimaryRec != null)
            {
                coborrowerPrimaryRec.EmplrNm = coborrowerPrimary.EmplrNm;
                coborrowerPrimaryRec.EmplrBusPhone = coborrowerPrimary.EmplrBusPhone;
                coborrowerPrimaryRec.EmplrAddr = coborrowerPrimary.EmplrAddr;
                coborrowerPrimaryRec.EmplrZip = coborrowerPrimary.EmplrZip;
                coborrowerPrimaryRec.EmplrCity = coborrowerPrimary.EmplrCity;
                coborrowerPrimaryRec.EmplrState = coborrowerPrimary.EmplrState;
                coborrowerPrimaryRec.JobTitle = coborrowerPrimary.JobTitle;
                coborrowerPrimaryRec.SetEmploymentStartDate(coborrowerPrimary.EmplmtStartD);
                coborrowerPrimaryRec.SetProfessionStartDate(coborrowerPrimary.ProfStartD);
                coborrowerPrimaryRec.IsSelfEmplmt = coborrowerPrimary.IsSelfEmplmt;
            }

            foreach (EmploymentField employment in borrowerPreviousEmployments)
            {
                IRegularEmploymentRecord record = null;
                if (employment.RecordID != string.Empty)
                {
                    try
                    {
                        record = borrowerRecordList.GetRegRecordOf(new Guid(employment.RecordID));
                    }
                    catch (CBaseException) 
                    {
                    }
                }

                if (record == null)
                {
                    record = borrowerRecordList.AddRegularRecord();
                }

                foundBorrowerRecords.Add(record);
                record.IsSelfEmplmt = employment.IsSelfEmplmt;
                record.EmplrNm = employment.EmplrNm;
                record.EmplrBusPhone = employment.EmplrBusPhone;
                record.EmplrAddr = employment.EmplrAddr;
                record.EmplrZip = employment.EmplrZip;
                record.EmplrCity = employment.EmplrCity;
                record.EmplrState = employment.EmplrState;
                record.JobTitle = employment.JobTitle;
                record.EmplmtStartD_rep = employment.EmplmtStartD;
                record.EmplmtEndD_rep = employment.EmplmtEndD;
                record.IsSelfEmplmt = employment.IsSelfEmplmt;
            }

            foreach (EmploymentField employment in coborrowerPreviousEmployments)
            {
                IRegularEmploymentRecord record = null;
                if (employment.RecordID != string.Empty)
                {
                    try
                    {
                        record = coborrowerRecordList.GetRegRecordOf(new Guid(employment.RecordID));
                    }
                    catch (CBaseException) 
                    {
                    }
                }

                if (record == null)
                {
                    record = coborrowerRecordList.AddRegularRecord();
                }

                foundCoBorrowerRecords.Add(record);
                record.IsSelfEmplmt = employment.IsSelfEmplmt;
                record.EmplrNm = employment.EmplrNm;
                record.EmplrBusPhone = employment.EmplrBusPhone;
                record.EmplrAddr = employment.EmplrAddr;
                record.EmplrZip = employment.EmplrZip;
                record.EmplrCity = employment.EmplrCity;
                record.EmplrState = employment.EmplrState;
                record.JobTitle = employment.JobTitle;
                record.EmplmtStartD_rep = employment.EmplmtStartD;
                record.EmplmtEndD_rep = employment.EmplmtEndD;
                record.IsSelfEmplmt = employment.IsSelfEmplmt;
            }

            // delete the removed records
            for (int i = 0; i < borrowerRecordList.CountRegular; i++)
            {
                IRegularEmploymentRecord rec = borrowerRecordList.GetRegularRecordAt(i);
                if (!foundBorrowerRecords.Contains(rec))
                {
                    rec.IsOnDeathRow = true;
                }
            }

            for (int i = 0; i < coborrowerRecordList.CountRegular; i++)
            {
                IRegularEmploymentRecord rec = coborrowerRecordList.GetRegularRecordAt(i);
                if (!foundCoBorrowerRecords.Contains(rec))
                {
                    rec.IsOnDeathRow = true;
                }
            }

            try
            {
                dataLoan.Save();
            }
            catch (PageDataAccessDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
        }

        /// <summary>
        /// Loads REO assets.
        /// </summary>
        public void LoadRealEstateRecords()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanid"), typeof(Loan1003PopupService));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(GetGuid("applicationID"));
            var recordList = dataApp.aReCollection;

            this.SetResult("numRecords", recordList.CountRegular);
            for (var i = 0; i < recordList.CountRegular; i++)
            {
                var record = recordList.GetRegularRecordAt(i);
                this.SetResult("recordID" + i, record.RecordId);
                this.SetResult("AssetT" + i, -1);
                this.SetResult("IsPrimaryResidence" + i, record.IsPrimaryResidence);
                this.SetResult("IsSubjectProp" + i, record.IsSubjectProp);
                this.SetResult("Addr" + i, record.Addr);
                this.SetResult("Zip" + i, record.Zip);
                this.SetResult("City" + i, record.City);
                this.SetResult("AssetST" + i, record.State);
                this.SetResult("Type" + i, record.TypeT);
                this.SetResult("StatT" + i, record.Stat);
                this.SetResult("HExp" + i, record.HExp_rep);
                this.SetResult("Val" + i, record.Val_rep);
                this.SetResult("OwnerT" + i, record.ReOwnerT);
                this.SetResult("GrossRentI" + i, record.GrossRentI_rep);
                this.SetResult("OccR" + i, record.OccR_rep);
            }
        }

        /// <summary>
        /// Saves REO assets.
        /// </summary>
        public void SaveRealEstateRecords()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanid"), typeof(Loan1003PopupService));
            dataLoan.InitSave(this.GetInt("sFileVersion"));

            CAppData dataApp = dataLoan.GetAppData(GetGuid("applicationID"));

            string re = GetString("reAssets");
            var realEstateAssets = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<RealEstateField>>(re);

            var recordList = dataApp.aReCollection;

            for (int i = 0; i < recordList.CountRegular; i++)
            {
                recordList.GetRegularRecordAt(i).IsOnDeathRow = true;
            }

            foreach (RealEstateField recordData in realEstateAssets)
            {
                IRealEstateOwned record = null;

                if (recordData.RecordID != string.Empty)
                {
                    try
                    {
                        record = recordList.GetRegRecordOf(new Guid(recordData.RecordID));
                    }
                    catch (CBaseException)
                    {
                    }
                }

                if (record == null)
                {
                    record = recordList.AddRegularRecord();
                }

                record.IsOnDeathRow = false;
                record.IsPrimaryResidence = recordData.IsPrimaryResidence;
                record.IsSubjectProp = recordData.IsSubjectProp;
                record.Addr = recordData.Addr;
                record.Zip = recordData.Zip;
                record.City = recordData.City;
                record.State = recordData.State;
                record.TypeT = (E_ReoTypeT)int.Parse(recordData.Type);
                record.Stat = recordData.StatT;

                record.HExp_rep = recordData.HExp;
                record.Val_rep = recordData.Val;
                record.ReOwnerT = (E_ReOwnerT)int.Parse(recordData.OwnerT);
                if (recordData.GrossRentI != string.Empty)
                {
                    record.GrossRentI_rep = recordData.GrossRentI;
                }
                else
                {
                    record.GrossRentI = 0;
                }
                if (recordData.OccR != string.Empty)
                {
                    record.OccR_rep = recordData.OccR;
                }
                else
                {
                    record.OccR = 0;
                }
            }

            try
            {
                dataLoan.Save();
            }
            catch (PageDataAccessDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
        }

        /// <summary>
        /// Loads non-REO assets.
        /// </summary>
        public void LoadNonRealEstateAssets()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanid"), typeof(Loan1003PopupService));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(GetGuid("applicationID"));
            IAssetCollection assetList = dataApp.aAssetCollection;

            this.SetResult("numRecords", assetList.CountRegular);
            for (var i = 0; i < assetList.CountRegular; i++)
            {
                IAssetRegular asset = assetList.GetRegularRecordAt(i);
                this.SetResult("recordID" + i, asset.RecordId);
                this.SetResult("OwnerT" + i, asset.OwnerT);
                this.SetResult("AssetT" + i, asset.AssetT);
                this.SetResult("Desc" + i, asset.Desc);
                this.SetResult("ComNm" + i, asset.ComNm);
                this.SetResult("AccNum" + i, asset.AccNum.Value);
                this.SetResult("Val" + i, asset.Val_rep);
            }
        }

        /// <summary>
        /// Saves non-REO assets.
        /// </summary>
        public void SaveNonRealEstateAssetRecords()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanid"), typeof(Loan1003PopupService));
            dataLoan.InitSave(this.GetInt("sFileVersion"));

            CAppData dataApp = dataLoan.GetAppData(GetGuid("applicationID"));

            string nre = GetString("nreAssets");
            var nonRealEstateAssets = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<NonRealEstateField>>(nre);

            IAssetCollection assetList = dataApp.aAssetCollection;

            for (int i = 0; i < assetList.CountRegular; i++)
            {
                assetList.GetRegularRecordAt(i).IsOnDeathRow = true;
            }

            foreach (NonRealEstateField assetData in nonRealEstateAssets)
            {
                IAssetRegular asset = null;
                if (assetData.RecordID != string.Empty)
                {
                    try
                    {
                        asset = assetList.GetRegRecordOf(new Guid(assetData.RecordID));
                    }
                    catch (CBaseException) 
                    {
                    }
                }

                if (asset == null)
                {
                    asset = assetList.AddRegularRecord();
                }

                asset.IsOnDeathRow = false;

                asset.OwnerT = (E_AssetOwnerT)int.Parse(assetData.OwnerT);
                asset.AssetT = (E_AssetRegularT)assetData.AssetT;
                asset.Desc = assetData.Desc;
                asset.ComNm = assetData.ComNm;
                asset.AccNum = assetData.AccNum;
                asset.Val_rep = assetData.Val;
            }

            try
            {
                dataLoan.Save();
            }
            catch (PageDataAccessDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
        }

        /// <summary>
        /// Gets a value indicating whether <paramref name="employment"/> has any non-default values.
        /// </summary>
        /// <param name="employment">The employment record to check.</param>
        /// <returns><see langword="true"/> if the employment record has a value; <see langword="false"/> otherwise.</returns>
        private static bool EmploymentHasValue(EmploymentField employment)
        {
            return employment.IsSelfEmplmt
                || !string.IsNullOrEmpty(employment.EmplrNm)
                || !string.IsNullOrEmpty(employment.EmplrBusPhone)
                || !string.IsNullOrEmpty(employment.EmplrAddr)
                || !string.IsNullOrEmpty(employment.EmplrZip)
                || !string.IsNullOrEmpty(employment.EmplrCity)
                || !string.IsNullOrEmpty(employment.EmplrState)
                || !string.IsNullOrEmpty(employment.JobTitle)
                || !string.IsNullOrEmpty(employment.EmplmtStartD)
                || !string.IsNullOrEmpty(employment.EmplmtEndD)
                || !string.IsNullOrEmpty(employment.RecordID)
                || !string.IsNullOrEmpty(employment.ProfStartD);
        }

        /// <summary>
        /// Saves the liability records for the file.
        /// </summary>
        private void SaveLiabilityRecords()
        {
            CPageData dataLoan = new CLiaData(this.GetGuid("LoanId"), true);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitSave(this.GetInt("sFileVersion"));

            CAppData dataApp = dataLoan.GetAppData(this.GetGuid("AppId"));
            ILiaCollection recordList = dataApp.aLiaCollection;

            var user = this.User as PriceMyLoanPrincipal;
            int count = recordList.CountRegular;
            for (int i = 0; i < count; i++)
            {
                ILiabilityRegular record = recordList.GetRegularRecordAt(i);
                bool willBePaidOff = this.GetBool("pdoff_" + record.RecordId, false);
                bool usedInRatio = this.GetBool("ratio_" + record.RecordId, false);

                if (record.WillBePdOff != willBePaidOff)
                {
                    // Use friendly description and value
                    // -jM OPM 17403 added login name
                    record.FieldUpdateAudit(user.DisplayName, user.LoginNm, "Pd Off", willBePaidOff ? "Yes" : "No");
                }

                if (record.NotUsedInRatio == usedInRatio)
                {
                    record.FieldUpdateAudit(user.DisplayName, user.LoginNm, "Used In Ratio", usedInRatio ? "Yes" : "No");
                }

                record.WillBePdOff = willBePaidOff;
                record.NotUsedInRatio = !usedInRatio;
                record.Update();
            }

            var alimony = recordList.GetAlimony(true);
            alimony.OwedTo = this.GetString("Alimony_OwedTo");
            alimony.Pmt_rep = this.GetString("Alimony_MonthlyPayment");
            alimony.RemainMons_rep = this.GetString("Alimony_RemainingMonths");
            alimony.NotUsedInRatio = this.GetBool("Alimony_NotUsedInRatio");
            alimony.Update();

            var childSupport = recordList.GetChildSupport(true);
            childSupport.OwedTo = this.GetString("ChildSupport_OwedTo");
            childSupport.Pmt_rep = this.GetString("ChildSupport_MonthlyPayment");
            childSupport.RemainMons_rep = this.GetString("ChildSupport_RemainingMonths");
            childSupport.NotUsedInRatio = this.GetBool("ChildSupport_NotUsedInRatio");
            childSupport.Update();

            var jobRelated = recordList.GetJobRelated1(true);
            jobRelated.ExpenseDesc = this.GetString("JobRelated1_ExpenseDescription");
            jobRelated.Pmt_rep = this.GetString("JobRelated1_MonthlyPayment");
            jobRelated.NotUsedInRatio = this.GetBool("JobRelated1_NotUsedInRatio");
            jobRelated.Update();

            jobRelated = recordList.GetJobRelated2(true);
            jobRelated.ExpenseDesc = this.GetString("JobRelated2_ExpenseDescription");
            jobRelated.Pmt_rep = this.GetString("JobRelated2_MonthlyPayment");
            jobRelated.NotUsedInRatio = this.GetBool("JobRelated2_NotUsedInRatio");
            jobRelated.Update();

            dataLoan.Save();

            this.SetResult("fileVersion", dataLoan.sFileVersion);
            this.SetResult("aTransmOMonPmtPe", dataApp.aTransmOMonPmtPe);
        }        

        /// <summary>
        /// Holds Real Estate Fields.
        /// </summary>
        public class RealEstateField
        {
            /// <summary>
            /// Initializes a new instance of the RealEstateField class.
            /// </summary>
            public RealEstateField()
            {
            }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named.</value>
            public int AssetT { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this asset is a primary residence.
            /// </summary>
            /// <value>What it's named.</value>
            public bool IsPrimaryResidence { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this is a subject property.
            /// </summary>
            /// <value>What it's named.</value>
            public bool IsSubjectProp { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named.</value>
            public string Addr { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named.</value>
            public string Zip { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string City { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string State { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string Type { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string StatT { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string HExp { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string Val { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string OwnerT { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string GrossRentI { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string RecordID { get; set; }

            public string OccR { get; set; }
        }

        /// <summary>
        /// Holds Non Real Estate Fields.
        /// </summary>
        public class NonRealEstateField
        {
            /// <summary>
            /// Initializes a new instance of the NonRealEstateField class.
            /// </summary>
            public NonRealEstateField()
            {
            }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string OwnerT { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// /// <value>What it's named. </value>
            public int AssetT { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string Desc { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string ComNm { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string AccNum { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string Val { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string RecordID { get; set; }
        }

        /// <summary>
        /// Holds Employment Fields.
        /// </summary>
        public class EmploymentField
        {
            /// <summary>
            /// Initializes a new instance of the EmploymentField class.
            /// </summary>
            /// <value>What it's named. </value>
            public EmploymentField()
            {
            }

            /// <summary>
            /// Gets or sets a value indicating whether this was a self-employment job.
            /// </summary>
            /// <value>What it's named. </value>
            public bool IsSelfEmplmt { get; set; }

            /// <summary>
            /// Gets or sets whatever this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplrNm { get; set; }

            /// <summary>
            /// Gets or sets whatever this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplrBusPhone { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplrAddr { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplrZip { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplrCity { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplrState { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string JobTitle { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplmtStartD { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string EmplmtEndD { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string RecordID { get; set; }

            /// <summary>
            /// Gets or sets whatever  this is named.
            /// </summary>
            /// <value>What it's named. </value>
            public string ProfStartD { get; set; }
        }
    }
}
