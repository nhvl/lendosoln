﻿namespace PriceMyLoan.webapp
{
    using System;
    using global::DataAccess;
    using LendersOffice.Constants;
    using PriceMyLoan.UI;
    using static LendersOffice.ObjLib.PMLNavigation.NavigationConfiguration;

    /// <summary>
    /// Non-QM page to hold all three user controls.
    /// </summary>
    public partial class NonQm : BasePage
    {
        /// <summary>
        /// On pre init.
        /// </summary>
        /// <param name="e">Event args.</param>
        protected override void OnPreInit(EventArgs e)
        {
            this.RegisterService("NonQmService", "/webapp/QuickPricerNonQmService.aspx");
            
            this.RegisterJsScript("PricingResults.v3.js");
            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("NonQm.js");
            this.RegisterCSS("PricingResults.css");
            this.RegisterJsGlobalVariables("MaxJsonLength", ConstApp.MaxJsonLength);
            this.RegisterCSS("DragDropUpload.css");
            this.RegisterCSS("bootstrap.min.css");
            this.RegisterJsScript("mask.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");            
            this.RegisterJsScript("NoConflict.js");
            this.RegisterCSS("NonQm.css");
            this.RegisterJsScript("JSErrorHandling.js");
            base.OnPreInit(e);
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event args.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var isQuickPricer = LendersOffice.Common.RequestHelper.GetBool("QuickPricer");
            var isBankStatementIncomeCalculator = LendersOffice.Common.RequestHelper.GetBool("BankStatementIncomeCalculator");
            var isScenarioRequestForm = LendersOffice.Common.RequestHelper.GetBool("ScenarioRequestForm");

            RegisterJsGlobalVariables("IsQuickPricer", isQuickPricer);
            RegisterJsGlobalVariables("IsBankStatementIncomeCalculator", isBankStatementIncomeCalculator);
            RegisterJsGlobalVariables("IsScenarioRequestForm", isScenarioRequestForm);

            RegisterJsGlobalVariables("NonQmQuickPricerLink", SystemNode.NonQmQp);
            RegisterJsGlobalVariables("BankStatementIncomeCalculatorLink", SystemNode.BankStatementIncomeCalculator);
            RegisterJsGlobalVariables("ScenarioRequestFormLink", SystemNode.ScenarioRequestForm);
        }
    }
}