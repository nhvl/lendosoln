﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.IO;
    using System.Web;
    using System.Xml;
    using EDocs;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.ObjLib.Events;
    using LendersOffice.ObjLib.TPO;
    using LendersOffice.PdfForm;

    /// <summary>
    /// Provides a means for uploading disclosure request forms.
    /// </summary>
    public partial class DisclosureFormUpload : UI.BasePage
    {
        /// <summary>
        /// Gets the application ID for the form.
        /// </summary>
        /// <value>
        /// The application for the form.
        /// </value>
        public Guid ApplicationId => RequestHelper.GetGuid("ApplicationId");

        /// <summary>
        /// Gets the type of disclosure form to upload.
        /// </summary>
        /// <value>
        /// The type of disclosure form to upload.
        /// </value>
        private TpoDisclosureType FormType => (TpoDisclosureType)RequestHelper.GetInt("type");

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterCSS("/webapp/jquery-ui.css");
            this.RegisterCSS("bootstrap.min.css");

            this.RegisterJsScript("Jquery-ui.js");
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (this.FormType)
            {
                case TpoDisclosureType.Redisclosure:
                    this.DisclosureFormType.InnerText = "CoC / Redisclosure";
                    break;
                case TpoDisclosureType.InitialClosingDisclosure:
                    this.DisclosureFormType.InnerText = "Initial Closing Disclosure";
                    break;
                default:
                    throw new UnhandledEnumException(this.FormType);
            }

            if (!this.HasConfiguredDownloadForm())
            {
                this.FormPlaceholder.Visible = false;

                this.ClientScript.RegisterClientScriptBlock(
                    typeof(DisclosureFormUpload),
                    "Resize Popup",
                    "resizePopup();",
                    addScriptTags: true);
            }
        }

        /// <summary>
        /// Handles the "Download Request Form" button click event.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void DisclosureFormDownloadButton_Click(object sender, EventArgs e)
        {
            var formSource = this.FormType == TpoDisclosureType.Redisclosure ?
                this.PriceMyLoanUser.BrokerDB.TpoRedisclosureFormSource :
                this.PriceMyLoanUser.BrokerDB.TpoInitialClosingDisclosureFormSource;

            switch (formSource)
            {
                case TpoRequestFormSource.CustomPdf:
                    this.SendCustomPdf();
                    break;
                case TpoRequestFormSource.Hosted:
                    this.SendHostedPdf();
                    break;
                default:
                    throw new UnhandledEnumException(formSource);
            }
        }

        /// <summary>
        /// Handles the "Submit" button click event.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected void SubmitButton_Click(object sender, EventArgs args)
        {
            var result = this.UploadDocument();

            if (result.Success)
            {
                this.AddDisclosureEvent(result.DocumentId);
            }
            else
            {
                this.SetErrorMessage(result.ErrorMessage);
            }
        }

        /// <summary>
        /// Determines whether the lender has configured a downloadable form.
        /// </summary>
        /// <returns>
        /// True if a downloadable form has been configured, false otherwise.
        /// </returns>
        private bool HasConfiguredDownloadForm()
        {
            var broker = this.PriceMyLoanUser.BrokerDB;
            bool hasConfiguredDocType = false, hasConfiguredPdf = false, hasConfiguredHostedUrl = false;

            switch (this.FormType)
            {
                case TpoDisclosureType.Redisclosure:
                    hasConfiguredDocType = broker.TpoRedisclosureFormRequiredDocType.HasValue;

                    hasConfiguredPdf = broker.TpoRedisclosureFormSource == TpoRequestFormSource.CustomPdf &&
                        broker.TpoRedisclosureFormCustomPdfId.HasValue;

                    hasConfiguredHostedUrl = broker.TpoRedisclosureFormSource == TpoRequestFormSource.Hosted &&
                        !string.IsNullOrWhiteSpace(broker.TpoRedisclosureFormHostedUrl);
                    break;
                case TpoDisclosureType.InitialClosingDisclosure:
                    hasConfiguredDocType = broker.TpoInitialClosingDisclosureFormRequiredDocType.HasValue;

                    hasConfiguredPdf = broker.TpoInitialClosingDisclosureFormSource == TpoRequestFormSource.CustomPdf &&
                        broker.TpoInitialClosingDisclosureFormCustomPdfId.HasValue;

                    hasConfiguredHostedUrl = broker.TpoInitialClosingDisclosureFormSource == TpoRequestFormSource.Hosted &&
                        !string.IsNullOrWhiteSpace(broker.TpoInitialClosingDisclosureFormHostedUrl);
                    break;
                default:
                    throw new UnhandledEnumException(this.FormType);
            }

            return hasConfiguredDocType && (hasConfiguredPdf || hasConfiguredHostedUrl);
        }

        /// <summary>
        /// Sends an LQB custom PDF to the user.
        /// </summary>
        private void SendCustomPdf()
        {
            try
            {
                var formId = this.FormType == TpoDisclosureType.Redisclosure ?
                    this.PriceMyLoanUser.BrokerDB.TpoRedisclosureFormCustomPdfId :
                    this.PriceMyLoanUser.BrokerDB.TpoInitialClosingDisclosureFormCustomPdfId;

                var pdfForm = PdfForm.LoadById(formId.Value);

                LendersOffice.PdfLayout.PdfFormLayout unused = null;
                var pdfData = pdfForm.GeneratePrintPdfAndPdfLayout(this.LoanID, this.ApplicationId, recordId: Guid.Empty, pdfLayout: out unused);

                var tempFile = TempFileUtils.NewTempFile();
                BinaryFileHelper.WriteAllBytes(tempFile, pdfData);

                RequestHelper.SendFileToClient(this.Context, tempFile.Value, "application/pdf", pdfForm.FileName);
                this.Context.ApplicationInstance.CompleteRequest();
            }
            catch (SystemException exc)
            {
                Tools.LogError(exc);
                this.SetErrorMessage("Unable to download PDF. Please try again.");
            }
        }

        /// <summary>
        /// Sends the hosted PDF to the user.
        /// </summary>
        private void SendHostedPdf()
        {
            var pdfUrl = this.FormType == TpoDisclosureType.Redisclosure ?
                this.PriceMyLoanUser.BrokerDB.TpoRedisclosureFormHostedUrl :
                this.PriceMyLoanUser.BrokerDB.TpoInitialClosingDisclosureFormHostedUrl;

            this.ClientScript.RegisterClientScriptBlock(
                typeof(DisclosureFormUpload),
                "Send Hosted PDF",
                "openPdf('" + pdfUrl + "');",
                addScriptTags: true);
        }

        /// <summary>
        /// Uploads the disclosure form.
        /// </summary>
        /// <returns>
        /// A <see cref="DisclosureFormUploadResult"/> for the upload.
        /// </returns>
        private DisclosureFormUploadResult UploadDocument()
        {
            var result = new DisclosureFormUploadResult();

            if (string.IsNullOrEmpty(this.DisclosureFormNotes.Text))
            {
                result.Success = false;
                result.ErrorMessage = "Please enter a message to the lender.";
                return result;
            }

            if (!this.HasConfiguredDownloadForm())
            {
                result.Success = true;
                return result;
            }

            if (!this.CompletedRequestForm.HasFile)
            {
                result.Success = false;
                result.ErrorMessage = "You must specify a completed request form for submission.";
                return result;
            }
            
            try
            {
                var docType = this.FormType == TpoDisclosureType.Redisclosure ?
                    this.PriceMyLoanUser.BrokerDB.TpoRedisclosureFormRequiredDocType :
                    this.PriceMyLoanUser.BrokerDB.TpoInitialClosingDisclosureFormRequiredDocType;

                result.DocumentId = EDocsUpload.UploadDoc(
                    this.PriceMyLoanUser,
                    this.CompletedRequestForm,
                    this.LoanID,
                    this.ApplicationId,
                    docType.Value,
                    internalComments: string.Empty,
                    description: string.Empty);

                result.Success = true;
            }
            catch (XmlException ex)
            {
                Tools.LogWarning("Failure parsing xml " + ex.ToString());
                result.ErrorMessage = Path.GetFileName(this.CompletedRequestForm.FileName) + " is not a valid xml file.";
                result.Success = false;
            }
            catch (InvalidPDFFileException)
            {
                result.ErrorMessage = Path.GetFileName(this.CompletedRequestForm.FileName) + " is not a valid pdf.";
                result.Success = false;
            }
            catch (InsufficientPdfPermissionException)
            {
                result.ErrorMessage = Path.GetFileName(this.CompletedRequestForm.FileName) + " is password protected.";
                result.Success = false;
            }
            catch (CBaseException cbe) when (cbe.UserMessage == ErrorMessages.EDocs.InvalidXML)
            {
                result.ErrorMessage = "Unexpected XML Format: " + Path.GetFileName(this.CompletedRequestForm.FileName);
                result.Success = false;
            }
            catch (CBaseException e)
            {
                Tools.LogError("Unexpected exception from EDocs Uploader", e);
                result.ErrorMessage = "Could not process " + Path.GetFileName(this.CompletedRequestForm.FileName) + " Error: " + e.UserMessage;
                result.Success = false;
            }

            return result;
        }

        /// <summary>
        /// Adds a disclosure request event to the loan file.
        /// </summary>
        /// <param name="documentId">
        /// The ID of the uploaded document if the lender has
        /// a request form configured, null otherwise.
        /// </param>
        private void AddDisclosureEvent(Guid? documentId)
        {
            try
            {
                var dataloan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(DisclosureFormUpload));
                dataloan.InitSave(ConstAppDavid.SkipVersionCheck);

                switch (this.FormType)
                {
                    case TpoDisclosureType.Redisclosure:
                        var redisclosureCollection = dataloan.AddTpoRequestForRedisclosureCollection();

                        var redisclosureEventSettings = new LoanEventAdditionSettings<TpoRequestForRedisclosureGenerationEventType>
                        {
                            EventType = TpoRequestForRedisclosureGenerationEventType.Active,
                            Notes = this.DisclosureFormNotes.Text,
                            AssociatedDocumentId = documentId == Guid.Empty ? default(Guid?) : documentId
                        };

                        redisclosureCollection.AddEvent(this.PriceMyLoanUser, redisclosureEventSettings);
                        break;
                    case TpoDisclosureType.InitialClosingDisclosure:
                        var initialClosingDisclosureSettings = new LoanEventAdditionSettings<TpoRequestForInitialClosingDisclosureGenerationEventType>
                        {
                            EventType = TpoRequestForInitialClosingDisclosureGenerationEventType.Active,
                            Notes = this.DisclosureFormNotes.Text,
                            AssociatedDocumentId = documentId == Guid.Empty ? default(Guid?) : documentId
                        };

                        var initialClosingDisclosureCollection = dataloan.sTpoRequestForInitialClosingDisclosureGenerationEventCollection;
                        initialClosingDisclosureCollection.AddEvent(this.PriceMyLoanUser, initialClosingDisclosureSettings);
                        break;
                    default:
                        throw new UnhandledEnumException(this.FormType);
                }

                dataloan.Save();

                this.ClientScript.RegisterClientScriptBlock(
                    typeof(DisclosureFormUpload),
                    "CompleteUpload",
                    "completeUpload();",
                    addScriptTags: true);
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                this.SetErrorMessage(exc.UserMessage);
            }
        }

        /// <summary>
        /// Sets the error message for the page.
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        private void SetErrorMessage(string message)
        {
            this.ClientScript.RegisterClientScriptBlock(
                typeof(DisclosureFormUpload),
                "Display Error",
                "displayError('" + message + "');",
                addScriptTags: true);
        }

        /// <summary>
        /// Provides a simple container for disclosure form upload results.
        /// </summary>
        private class DisclosureFormUploadResult
        {
            /// <summary>
            /// Gets or sets a value indicating whether the upload was successful.
            /// </summary>
            /// <value>
            /// True if the upload was successful, false otherwise.
            /// </value>
            public bool Success { get; set; }

            /// <summary>
            /// Gets or sets the error message for the upload, if any.
            /// </summary>
            /// <value>
            /// The error message for the upload.
            /// </value>
            public string ErrorMessage { get; set; }

            /// <summary>
            /// Gets or sets the ID of the uploaded document.
            /// </summary>
            /// <value>
            /// The ID of the uploaded document or null if the lender has
            /// not configured a form for download.
            /// </value>
            public Guid? DocumentId { get; set; }
        }
    }
}