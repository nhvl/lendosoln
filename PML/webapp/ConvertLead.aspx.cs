﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using System.Data.SqlClient;
using System.Data;
using DataAccess;
using LendersOffice.Security;
using PriceMyLoan.Common;

namespace PriceMyLoan.webapp
{
    public partial class ConvertLead : PriceMyLoan.UI.BasePage
    {
        private BrokerDB m_brokerDB;
        protected BrokerDB CurrentBroker
        {
            get
            {
                if (null == m_brokerDB)
                {
                    m_brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
                }
                return m_brokerDB;
            }
        }
        private CPageData m_dataLoan;

        override protected void OnInit(EventArgs e)
        {
            this.RegisterCSS("/webapp/pml.css");

            // Bind Template DDL
            bool filterByBranch = PriceMyLoanUser.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch) ? false : true;

            SqlParameter[] parameters = {
                                            new SqlParameter( "@BrokerId"         , PriceMyLoanUser.BrokerId   )
                                            , new SqlParameter( "@BranchId"       , PriceMyLoanUser.BranchId   )
                                            , new SqlParameter( "@EmployeeId"     , PriceMyLoanUser.EmployeeId )
                                            , new SqlParameter( "@FilterByBranch" , filterByBranch             )
                                        };

            DataSet dS = new DataSet();
            DataSetHelper.Fill(dS, PriceMyLoanUser.BrokerId, "RetrieveLoanTemplateByBrokerID", parameters);

            // 10/16/06 OPM 7843 mf.  We now sort the list here instead of in query
            if (dS.Tables.Count > 0)
            {
                dS.Tables[0].DefaultView.Sort = "sLNm ASC";
                var view = dS.Tables[0].DefaultView;

                m_dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(LoanID, typeof(ConvertLead));
                m_dataLoan.AllowLoadWhileQP2Sandboxed = true;
                m_dataLoan.InitLoad();

                if (!m_dataLoan.sIsRefinancing)
                {
                    view.RowFilter = "sLPurposeT = 0";
                }
                else
                {
                    view.RowFilter = "sLPurposeT = 1" // Refin
                                   + " OR sLPurposeT = 2" // RefinCashout
                                   + " OR sLPurposeT = 6" // FHA Streamline Refi
                                   + " OR sLPurposeT = 7" // VaIrrrl
                                   + " OR sLPurposeT = 8"; // Home Equity
                }

                templateDDL.DataSource = view;
                templateDDL.DataTextField = "sLNm";
                templateDDL.DataValueField = "sLId";
                templateDDL.DataBind();
            }

            templateDDL.Items.Insert(0, new ListItem("<-- Choose a template -->", Guid.Empty.ToString()));
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (m_dataLoan == null)
            {
                m_dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(LoanID, typeof(ConvertLead));
                m_dataLoan.AllowLoadWhileQP2Sandboxed = true;
                m_dataLoan.InitLoad();
            }
            var isQP2File = E_sLoanFileT.QuickPricer2Sandboxed == m_dataLoan.sLoanFileT;

            var convertOptions =  new { 
                IsDisallowTemplateSelection = isQP2File && false == PriceMyLoanConstants.IsEmbeddedPML,
                IsForceLeadToLoanTemplate = CurrentBroker.IsForceLeadToLoanTemplate && false == (isQP2File && false == PriceMyLoanConstants.IsEmbeddedPML),
                IsForceLeadToLoanNewLoanNumber = isQP2File ? true : CurrentBroker.IsForceLeadToLoanNewLoanNumber,
                IsForceLeadToLoanRemoveLeadPrefix = isQP2File ? false : CurrentBroker.IsForceLeadToLoanRemoveLeadPrefix,
                IsForceLeadToUseLoanCounter = CurrentBroker.IsForceLeadToUseLoanCounter,
                IsForceLeadToLoanWithoutTempate = CurrentBroker.IsEditLeadsInFullLoanEditor,
                IsLeadMadeFromBlankOrQPTemplate = m_dataLoan.sSrcsLId == Guid.Empty || m_dataLoan.sSrcsLId == CurrentBroker.QuickPricerTemplateId
            };

            var serializedData = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonSerialize(convertOptions);

            ClientScript.RegisterStartupScript(typeof(ConvertLead), "convertOptions", "var convertOptions = " + serializedData, true);
        }
    }
}
