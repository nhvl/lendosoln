﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using MeridianLink.CommonControls;

    public partial class Loan1003 : PriceMyLoan.UI.BasePage
    {
        protected string main1003Url =>
            $"{VirtualRoot}/webapp/Loan1003.aspx?loanID={sLID}&applicationId={appID}&src={RequestHelper.GetSafeQueryString("src")}";

        protected string sLID => LoanID.ToString().Replace("-", "");

        protected string appID
        {
            get
            {
                Guid applicationID = RequestHelper.GetGuid("applicationid", Guid.Empty);
                if (applicationID == Guid.Empty)
                {
                    CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003));
                    dataLoan.InitLoad();
                    CAppData dataApp = dataLoan.GetAppData(0);
                    applicationID = dataApp.aAppId;
                }
                return applicationID.ToString().Replace("-", "");
            }
        }

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return this.PriceMyLoanUser.EnableNewTpoLoanNavigation
                ? new[]
                {
                    WorkflowOperations.ImportIntoExistingFile,
                    WorkflowOperations.DeleteApp
                }
                : Enumerable.Empty<WorkflowOperation>();
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return this.PriceMyLoanUser.EnableNewTpoLoanNavigation ? E_XUAComaptibleValue.Edge : base.GetForcedCompatibilityMode();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            this.RegisterJsScript("DeclarationExplanation.js");
            this.RegisterJsScript("jquery.tmpl.js");

            BrokerDB CurrentBroker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);

            if (!CurrentBroker.DisableFeeEditorInTPOPortalCase183419 &&
                !this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                editClosingCostsBtn1.Visible = true;
            }

            var enableNewUi = this.PriceMyLoanUser.EnableNewTpoLoanNavigation;

            this.RegisterJsGlobalVariables("EnableNewTpoLoanNavigation", enableNewUi);
            if (CurrentBroker.Enable1003EditorInTPOPortal || enableNewUi)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003));
                dataLoan.InitLoad();

                ClientScript.RegisterHiddenField("IsUsing2015GFEUITPO", (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy).ToString());

                RegisterService("Loan1003Service", "/webapp/Loan1003Service.aspx");
                RegisterService("Loan1003PopupService", "/webapp/Loan1003PopupService.aspx");
                ClientScript.RegisterHiddenField("sLID", LoanID.ToString());
                ClientScript.RegisterHiddenField("applicationID", RequestHelper.GetGuid("applicationID", Guid.Empty).ToString());
                RegisterJsScript("/webapp/combobox.js");
                RegisterJsScript("/Loan1003.js");
                RegisterJsScript("/webapp/Loan1003.constants.js");
                RegisterJsScript("/webapp/Loan1003.models.js");
                RegisterJsScript("jquery-ui-1.11.4.min.js");
                RegisterCSS("/webapp/jquery-ui.css");
                RegisterCSS("/webapp/style.css");
                RegisterCSS("/webapp/main.css");

                /*       <link href="../css/webapp/jquery-ui.css" type="text/css" rel="Stylesheet" />
        <link href="../css/webapp/style.css" type="text/css" rel="Stylesheet" />
        <!--main.css from Vu-->
        <link href="../css/webapp/main.css" rel="stylesheet" type="text/css" />*/

                string p = RequestHelper.GetSafeQueryString("p");

                tab1Top.Attributes.Remove("class");
                tab2Top.Attributes.Remove("class");
                tab3Top.Attributes.Remove("class");
                tab4Top.Attributes.Remove("class");

                this.RegisterJsGlobalVariables("main1003Url", main1003Url);
                this.RegisterJsGlobalVariables("IsRenovationLoan", dataLoan.sIsRenovationLoan);
                this.RegisterJsGlobalVariables("sIsIncomeCollectionEnabled", dataLoan.sIsIncomeCollectionEnabled);

                int page;
                if (!int.TryParse(p, out page))
                {
                    page = 0;
                }

                btnBack.InnerHtml = "<i class=\"material-icons\">&#xE5CB;</i> Go to Page " + AspxTools.HtmlString(page);
                btnNext.InnerHtml = "Go to Page " + (AspxTools.HtmlString(page + 2)) + " <i class=\"material-icons\">&#xE5CC;</i>";

                switch (page)
                {
                    case 0:
                        tab1Top.Attributes.Add("class", "active");
                        RegisterJsScript("/webapp/Loan1003.page1.js");
                        Loan1003pg1.LoadData();
                        btnBack.Visible = false;
                        break;
                    case 1:
                        tab2Top.Attributes.Add("class", "active");
                        RegisterJsScript("/Loan1003.page2.js");
                        Loan1003pg2.LoadData();
                        break;
                    case 2:
                        tab3Top.Attributes.Add("class", "active");
                        RegisterJsScript("/webapp/Loan1003.page3.js");
                        Loan1003pg3.LoadData();
                        break;
                    case 3:
                        tab4Top.Attributes.Add("class", "active");
                        Loan1003pg4.LoadData();
                        btnNext.Visible = false;
                        break;
                    default:
                        break;
                }

                this.LoanNavHeader.SetDataFromLoan(dataLoan, LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationApplicationInformation);

                if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
                {
                    runPricingBtn1.Visible = false;

                    this.SetButtonDisplayFromWorkflow(this.ImportFannieButton, this.importFannieDiv, WorkflowOperations.ImportIntoExistingFile);

                    if (dataLoan.nApps == 1)
                    {
                        this.DeleteApplicationButton.Visible = false;
                    }
                    else
                    {
                        this.SetButtonDisplayFromWorkflow(this.DeleteApplicationButton, this.deleteApplicationDiv, WorkflowOperations.DeleteApp);
                    }
                }
                else
                {
                    this.NewLoanNavigationButtonArea.Visible = false;
                    this.DeleteApplicationButton.Visible = false;

                    bool IsNewPmlUiEnabled = CurrentBroker.IsNewPmlUIEnabled
                        || (EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId).IsNewPmlUIEnabled);

                    runPricingBtn1.Attributes.Add("onclick", IsNewPmlUiEnabled
                        ? ("attemptSave(); if(followLink)  document.location = '" + VirtualRoot + "/webapp/pml.aspx?loanid=" + LoanID.ToString().Replace("-", "") + "&source=PML'")
                        : ("attemptSave(); if(followLink)  document.location = '" + VirtualRoot + "/main/agents.aspx?loanid=" + LoanID + "'"));
                }

                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);

                    string tabDescription = "";

                    if (dataApp.aBLastFirstNm != "")
                    {
                        var borrowerName = dataApp.aBFirstNm + " " + dataApp.aBLastNm;
                        tabDescription = borrowerName;
                    }
                    if (dataApp.aCLastFirstNm != "")
                    {
                        var coborrowerName = dataApp.aCFirstNm + " " + dataApp.aCLastNm;
                        if (dataApp.aBLastFirstNm != "")
                            tabDescription += " & " + coborrowerName;
                        else
                            tabDescription = coborrowerName;
                    }

                    if (dataApp.aBLastFirstNm == "" && dataApp.aCLastFirstNm == "")
                        tabDescription = "Application #" + (i + 1);

                    string className =
                        (RequestHelper.GetGuid("applicationid", Guid.Empty) == Guid.Empty && i == 0) ||
                        (RequestHelper.GetGuid("applicationid", Guid.Empty) == dataApp.aAppId)
                        ? "class='active'"
                        : "";

                    string srcArg = RequestHelper.GetQueryString("src");
                    if (srcArg != null && srcArg.Equals("pml", StringComparison.OrdinalIgnoreCase))
                    {
                        srcArg = "pml";
                    }
                    else if (srcArg != null && srcArg.Equals("pipeline", StringComparison.OrdinalIgnoreCase))
                    {
                        srcArg = "pipeline";
                    }
                    else
                    {
                        srcArg = string.Empty;
                    }

                    var tabOnClick = AspxTools.HtmlAttribute($"attemptSave(); if(followLink) document.location =\'{VirtualRoot}/webapp/Loan1003.aspx?loanID={sLID}&applicationId={dataApp.aAppId.ToString().Replace("-", "")}&src={srcArg}\';");
                    appTabs.Controls.Add(new PassthroughLiteral
                    {
                        Text =
                            $"<li {className}><a href=\"#\" onclick={tabOnClick}>{AspxTools.HtmlString(tabDescription)}</a></li>"
                    });
                    if (className != "")
                    {
                        appName.InnerText = tabDescription;
                    }
                }
            }
        }
    }
}
