﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Results.ascx.cs" Inherits="PriceMyLoan.webapp.Results" %>
<style type="text/css">
#ResultsContainer
{
    border: 1px solid gainsboro;
    width: 100%;
    background-color: #FFF;
    padding: 0;
}
#resultsIframe
{
    margin: 0 auto;
    padding: 0;
}
</style>

<div id="ResultsContainer" class="container">
    <iframe id="resultsIframe" scrolling="no" frameBorder="0" seamless="seamless" width="100%" height="400"></iframe>
</div>