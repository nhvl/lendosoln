﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Security;
    using PriceMyLoan.Common;

    /// <summary>
    /// This is a helper file.  It's purpose is to centralize the loading of fields needed for
    /// rendering the third version of PML results.
    /// </summary>
    public class PmlResults3FieldRegisterer
    {
        /// <summary>
        /// Gets the user's principal.
        /// </summary>
        /// <value>The user's principal.</value>
        public static AbstractUserPrincipal Principal
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal;
            }
        }

        /// <summary>
        /// Gets the user's broker object.
        /// </summary>
        /// <value>The user's broker.</value>
        public static BrokerDB Broker
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB;
            }
        }

        /// <summary>
        /// Gets a value determining whether the user is within Encompass.
        /// </summary>
        /// <value>Whether the user is using encompass.</value>
        public static bool IsEncompass
        {
            get
            {
                return Principal.IsEncompassIntegrationEnabled || Principal.IsSpecialRemnEncompassTotalAccount;
            }
        }

        /// <summary>
        /// This method takes the given parameters, and adds all the necessary fields needed for generating the
        /// angular PML Results to the ML object.
        /// </summary>
        /// <param name="dataLoan">The loan that's being priced.</param>
        /// <param name="page">The Page that is displaying the results.</param>
        /// <param name="isPricing2ndLoan">Whether the loan being priced is the 2nd loan for combo pricing.</param>
        /// <param name="hasDuplicate">Whether the loan has a duplicate that has been priced.</param>
        /// <param name="reasonsCannotLock">A list of reaons the user cannot lock a rate.</param>
        /// <param name="reasonsCannotRegister">A list of reasons why the user cannot register a rate.</param>
        public static void RegisterFields(CPageData dataLoan, PriceMyLoan.UI.BasePage page, bool isPricing2ndLoan, bool? hasDuplicate = false, IEnumerable<string> reasonsCannotLock = null, IEnumerable<string> reasonsCannotRegister = null)
        {
            WorkflowOperation[] operations = { WorkflowOperations.RequestRateLockCausesLock, WorkflowOperations.UseOldSecurityModelForPml, WorkflowOperations.RegisterLoan, WorkflowOperations.RequestRateLockRequiresManualLock};
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(Principal.ConnectionInfo, Principal.BrokerId, dataLoan.sLId, operations);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(Principal));

            bool canAutoLock = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockCausesLock, valueEvaluator);
            bool canRegister = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockRequiresManualLock, valueEvaluator);


            bool isUsingOldSecurity = LendingQBExecutingEngine.CanPerform(WorkflowOperations.UseOldSecurityModelForPml, valueEvaluator);
            bool IsAllowRegisterRateLock = isUsingOldSecurity || LendingQBExecutingEngine.CanPerform(WorkflowOperations.RegisterLoan, valueEvaluator);
            bool IsAllowRequestingRateLock = canAutoLock || canRegister;

            bool isBreakEvenMonths = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && (!Broker.EnableCashToCloseAndReservesDebugInPML || Broker.DisplayBreakEvenMthsInsteadOfReserveMthsInPML2ForNonPurLoans);
            var disablePinLinks = dataLoan.sLtvR != dataLoan.sCltvR || dataLoan.sLienPosT == E_sLienPosT.Second;

            bool unableToSubmitWithoutCredit = !Principal.HasPermission(Permission.CanSubmitWithoutCreditReport)
                            && !dataLoan.sAllAppsHasCreditReportOnFile && !IsEncompass;

            E_sLienQualifyModeT lienQualifyModeT = dataLoan.sIs8020PricingScenario ? E_sLienQualifyModeT.OtherLoan : E_sLienQualifyModeT.ThisLoan;

            page.RegisterJsGlobalVariables("IsManualSubmissionAllowed", Broker.IsLpeManualSubmissionAllowed);

            page.RegisterJsGlobalVariables("IsPml", true);
            page.RegisterJsGlobalVariables("EnablePmlUIResults3", Broker.IsEnablePmlResults3Ui(Principal));

            page.RegisterJsGlobalVariables("IsAutoLock", canAutoLock);

            page.RegisterJsGlobalVariables("sLienQualifyModeT", (int)lienQualifyModeT);
            page.RegisterJsGlobalVariables("HasDuplicate", hasDuplicate.HasValue ? hasDuplicate.Value : false);
            page.RegisterJsGlobalVariables("RenderResultModeT", (int)E_RenderResultModeT.BestPricesPerProgram);           
            page.RegisterJsGlobalVariables("IsPricing2ndLoan", isPricing2ndLoan);

            page.RegisterJsGlobalVariables("showQm", Broker.ShowQMStatusInPml2 && dataLoan.BranchChannelT != E_BranchChannelT.Correspondent);
            page.RegisterJsGlobalVariables("IsDebugColumns", Broker.EnableCashToCloseAndReservesDebugInPML);
            page.RegisterJsGlobalVariables("IsBreakEvenMonths", isBreakEvenMonths);
            page.RegisterJsGlobalVariables("sDisclosureRegulationT", (int)dataLoan.sDisclosureRegulationT);
            page.RegisterJsGlobalVariables("DisablePinLinks", disablePinLinks);

            page.RegisterJsGlobalVariables("IsAllowRegisterRateLock", IsAllowRegisterRateLock);
            page.RegisterJsGlobalVariables("IsAllowRequestingRateLock", IsAllowRequestingRateLock);
            page.RegisterJsGlobalVariables("ReasonsCannotLockJson", SerializationHelper.JsonNetSerialize(reasonsCannotLock.ToList()));
            page.RegisterJsGlobalVariables("ReasonsCannotRegisterJson", SerializationHelper.JsonNetSerialize(reasonsCannotRegister.ToList()));

            page.RegisterJsGlobalVariables("IsRunModeOriginalProductOnly", dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct);
            page.RegisterJsGlobalVariables("HasLinkedLoan", dataLoan.sLinkedLoanInfo.IsLoanLinked && dataLoan.lpeRunModeT != E_LpeRunModeT.OriginalProductOnly_Direct);
            page.RegisterJsGlobalVariables("UnableToSubmitWithoutCredit", unableToSubmitWithoutCredit);
            page.RegisterJsGlobalVariables("LPE_CannotSubmitWithoutCredit", JsMessages.LPE_CannotSubmitWithoutCredit);
            page.RegisterJsGlobalVariables("LpeUnableToSubmitLoanProgram", JsMessages.LpeUnableToSubmitLoanProgram);

            page.RegisterJsGlobalVariables("ConfirmationTitle", Broker.IsPmlSubmissionAllowed && !IsEncompass ? "Confirmation" : "Summary");
            page.RegisterJsGlobalVariables("CanSubmitIneligible", Principal.HasPermission(Permission.CanApplyForIneligibleLoanPrograms));

            page.RegisterJsGlobalVariables("EnableRenovationLoanSupport", dataLoan.BrokerDB.EnableRenovationLoanSupport.ToString());

            if (isPricing2ndLoan)
            {
                page.RegisterJsGlobalVariables("Version", RequestHelper.GetSafeQueryString("version"));
                page.RegisterJsGlobalVariables("FirstRate", RequestHelper.GetSafeQueryString("1stRate"));
                page.RegisterJsGlobalVariables("FirstPoint", RequestHelper.GetSafeQueryString("1stPoint"));
                page.RegisterJsGlobalVariables("FirstMPmt", RequestHelper.GetSafeQueryString("1stMPmt"));
                page.RegisterJsGlobalVariables("FirstProductId", RequestHelper.GetGuid("1stProductId", Guid.Empty));
                page.RegisterJsGlobalVariables("FirstRawId", RequestHelper.GetGuid("1stRawId", Guid.Empty));
                page.RegisterJsGlobalVariables("FirstBlockSubmit", RequestHelper.GetBool("1stBlockSubmit"));
                page.RegisterJsGlobalVariables("FirstBlockSubmitMsg", RequestHelper.GetSafeQueryString("1stBlockSubmitMsg"));
                page.RegisterJsGlobalVariables("FirstUniqueChecksum", RequestHelper.GetSafeQueryString("1stUniqueChecksum"));
            }

            if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
            {
                if (PriceMyLoanConstants.IsEmbeddedPML)
                {
                    page.RegisterJsGlobalVariables("RegisterAndRateLockBlockingMessage", JsMessages.LPE_CannotSubmitInQP2Mode);
                }
                else
                {
                    if (Broker.IsAllowExternalUserToCreateNewLoan)
                    {
                        page.RegisterJsGlobalVariables("RegisterAndRateLockBlockingMessage", JsMessages.LPE_CannotSubmitInQP2ModeInPML);
                    }
                    else
                    {
                        page.RegisterJsGlobalVariables("RegisterAndRateLockBlockingMessage", JsMessages.LPE_CannotSubmitInQP2ModeInPMLAndCantCreateLoan);
                    }
                }
            }
        }
    }
}