﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Web.UI;
    using global::DataAccess;
    using LendersOffice.Security;
    using LendersOffice.Admin;
    using PriceMyLoan.Common;
    using PriceMyLoan.UI;
    using Security;
    using LendersOffice.ObjLib.DU.Seamless;
    using LqbGrammar.DataTypes;

    public partial class Action : BaseUserControl
    {
        public bool IsLead { get; set; }
        public bool IsQP2File { get; set; }
        private void RegisterIncludes(BasePage page)
        {
            page.RegisterJsScript("LQBPopup.js");
            page.RegisterCSS("webapp/Action.css");
            page.RegisterCSS("jquery-ui-1.11.css");
            page.RegisterJsScript("webapp/Action.js");
            page.RegisterJsScript("webapp/Integration.js");
            page.RegisterService("action", "/webapp/ActionService.aspx");
            page.RegisterJsScript("json.js");
            page.RegisterJsScript("jquery-ui-1.11.4.min.js");
            page.EnableJqueryMigrate = false;
            page.EnableJquery = false;
        }

        private bool allowSubmission(bool regular, bool embedded, bool expandedLeadEditor)
        {
            if (IsQP2File) return false;
            else if (PriceMyLoanConstants.IsEmbeddedPML && IsLead) return expandedLeadEditor;
            else if (PriceMyLoanConstants.IsEmbeddedPML) return embedded;
            else return regular;
        }

        public bool AllowDoSubmission(BrokerDB broker)
        {
            return allowSubmission(broker.IsPmlDoEnabled, broker.IsEmbeddedPmlDoEnabled, broker.IsExpandedLeadEditorDoEnabled);
        }
        public bool AllowDuSubmission(BrokerDB broker)
        {
            return allowSubmission(broker.IsPmlDuEnabled, broker.IsEmbeddedPmlDuEnabled, broker.IsExpandedLeadEditorDuEnabled);
        }
        public bool AllowLPSubmission(BrokerDB broker)
        {
            return allowSubmission(broker.IsPmlLpEnabled, broker.IsEmbeddedPmlLpEnabled, broker.IsExpandedLeadEditorLpEnabled);
        }


        private bool AllowFHASubmission(BrokerDB broker, AbstractUserPrincipal user)
        {
            var isTotalEnabled = IsLead ? broker.IsExpandedLeadEditorTotalEnabled :
                broker.IsTotalScorecardEnabled;

            return (false == IsQP2File) && isTotalEnabled && user.HasPermission(Permission.CanAccessTotalScorecard);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var page = Page as BasePage;
            if (page == null) throw new Exception("ActionRegion can only be used on a BasePage");
            RegisterIncludes(page);

            var user = (AbstractUserPrincipal)Page.User;
            var broker = BrokerDB.RetrieveById(user.BrokerId);

            List<string> AvailableEngines = new List<string>(4);

            if (AllowFHASubmission(broker, user)) AvailableEngines.Add("FHATotal");
            if (AllowDoSubmission(broker)) AvailableEngines.Add("DesktopOriginator");

            BrokerSeamlessDuSettings seamlessDuSettings = broker.GetSeamlessDuSettings();
            bool showDuSeamlessButton = seamlessDuSettings.SeamlessDuEnabled 
                && (user.Type == "B" || ((PriceMyLoanPrincipal)user).EnableNewTpoLoanNavigation); // Should only show DU Seamless in the new PML UI and embedded PML.
            if (AllowDuSubmission(broker) && (!showDuSeamlessButton || seamlessDuSettings.IsDuFallbackEnabled(user)))
            {
                AvailableEngines.Add("DesktopUnderwriter");
            }
            if (AllowDuSubmission(broker) && showDuSeamlessButton)
            {
                AvailableEngines.Add("DesktopUnderwriter_Seamless");
            }

            bool showLpaSeamlessButton = broker.IsSeamlessLpaEnabled
                && (user.Type == "B" || ((PriceMyLoanPrincipal)user).EnableNewTpoLoanNavigation);
            if (AllowLPSubmission(broker) && broker.IsNonSeamlessLpaEnabled(user))
            {
                AvailableEngines.Add("LoanProspector");
            }

            if (AllowLPSubmission(broker) && showLpaSeamlessButton)
            {
                AvailableEngines.Add("LoanProductAdvisor_Seamless");
            }

            page.ClientScript.RegisterHiddenField("AvailableEngines", LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonSerialize(AvailableEngines));

            PriceMyLoan.Security.PriceMyLoanPrincipal PriceMyLoanUser = ((BasePage)Page).PriceMyLoanUser;

            page.RegisterJsGlobalVariables("PortalMode", PriceMyLoanUser.PortalMode.ToString("d"));

            string source = LendersOffice.Common.RequestHelper.GetSafeQueryString("source");
            if (!PriceMyLoanUser.EnableNewTpoLoanNavigation && 
                !broker.DisableFeeEditorInTPOPortalCase183419 && 
                "PML".Equals(source) && 
                !IsQP2File)
            {
                EditClosingCostsBtn.Visible = true;
            }

            BrokerUserPermissions buP = new BrokerUserPermissions(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);

            bool PortalModeAllowCreate = false;
            switch (PriceMyLoanUser.PortalMode)
            {
                case E_PortalMode.Blank:
                    PortalModeAllowCreate = true;
                    break;
                case E_PortalMode.Retail:
                    PortalModeAllowCreate = buP.HasPermission(Permission.AllowCreatingNewLoanFiles);
                    break;
                case E_PortalMode.Broker:
                    PortalModeAllowCreate = buP.HasPermission(Permission.AllowCreatingWholesaleChannelLoans);
                    break;
                case E_PortalMode.MiniCorrespondent:
                    PortalModeAllowCreate = buP.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans);
                    break;
                case E_PortalMode.Correspondent:
                    PortalModeAllowCreate = buP.HasPermission(Permission.AllowCreatingCorrChannelLoans);
                    break;
                default:
                    throw new UnhandledEnumException(((BasePage)Page).PriceMyLoanUser.PortalMode);

            }

            if (IsQP2File)
            {
                ConvertLeadToLoanContainer.Visible = false;
                ActionButtonsContainer.Visible = false;

                ConvertQP2ToLeadContainer.Visible = (PriceMyLoanConstants.IsEmbeddedPML || PriceMyLoanUser.PortalMode == E_PortalMode.Retail)
                    && buP.HasPermission(Permission.AllowCreatingNewLeadFiles);

                if (PriceMyLoanConstants.IsEmbeddedPML)
                {
                    ConvertQP2ToLoanContainer.Visible = buP.HasPermission(Permission.AllowCreatingNewLoanFiles);
                }
                else
                {
                    ConvertQP2ToLoanContainer.Visible = broker.IsAllowExternalUserToCreateNewLoan
                        && PortalModeAllowCreate;
                }
            }
            else
            {
                ConvertQP2ToLeadContainer.Visible = false;
                ConvertQP2ToLoanContainer.Visible = false;
            }
        }
    }
}
