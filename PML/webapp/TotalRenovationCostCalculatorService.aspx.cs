﻿namespace PriceMyLoan.webapp
{
    using global::DataAccess;
    using LendersOffice.Constants;

    public partial class TotalRenovationCostCalculatorService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Calls any custom method, depending on the method parameter.
        /// </summary>
        /// <param name="methodName">The name of the method to be called.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "RefreshCalc":
                    RefreshCalc();
                    break;
                case "SaveData":
                    SaveData();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        private void BindData(CPageData dataLoan)
        {
            dataLoan.sFHA203kType = this.GetEnum<E_sFHA203kType>("sFHA203kType");
            dataLoan.sCostConstrRepairsRehab_rep = GetString("sCostConstrRepairsRehab"); // 1.a
            dataLoan.sContingencyRsrvPcRenovationHardCost_rep = GetString("sContingencyRsrvPcRenovationHardCost"); // 1.b percentage
            dataLoan.sArchitectEngineerFee_rep = GetString("sArchitectEngineerFee"); // 1.c
            dataLoan.sConsultantFee_rep = GetString("sConsultantFee"); // 1.d
            dataLoan.sRenovationConstrInspectFee_rep = GetString("sRenovationConstrInspectFee"); // 1.e
            dataLoan.sTitleUpdateFee_rep = GetString("sTitleUpdateFee"); // 1.f
            dataLoan.sPermitFee_rep = GetString("sPermitFee"); // 1.g
            dataLoan.sRenoMtgPmntRsrvMnth_rep = GetString("sRenoMtgPmntRsrvMnth"); // 1.h months
            dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep = GetString("sFinanceMortgagePmntRsrvTgtVal"); // 1.h Target
            dataLoan.sFinanceMortgagePmntRsrvTgtValLckd = GetBool("sFinanceMortgagePmntRsrvTgtValLckd"); // 1.h Target Locked box
            dataLoan.sOtherRenovationCost_rep = GetString("sOtherRenovationCost"); // 1.i
            dataLoan.sOtherRenovationCostDescription = GetString("sOtherRenovationCostDescription"); // 1.i Description
            dataLoan.sFinanceOriginationFeeTgtVal_rep = GetString("sFinanceOriginationFeeTgtVal"); // 1.i financeable origination fee Target
            dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep = GetString("sDiscntPntOnRepairCostFeeTgtVal"); // 1.i discount points Target
            dataLoan.sFeasibilityStudyFee_rep = GetString("sFeasibilityStudyFee"); // 1.i feasibility study
        }

        private void RefreshCalc()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanId"), typeof(TotalRenovationCostCalculatorService));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            BindData(dataLoan);

            // "Load Data"
            this.SetResult("sFHA203kType", dataLoan.sFHA203kType);
            this.SetResult("sCostConstrRepairsRehab", dataLoan.sCostConstrRepairsRehab_rep); // 1.a
            this.SetResult("sContingencyRsrvPcRenovationHardCost", dataLoan.sContingencyRsrvPcRenovationHardCost_rep); // 1.b percentage
            this.SetResult("sFinanceContingencyRsrv", dataLoan.sFinanceContingencyRsrv_rep); // 1.b
            this.SetResult("sArchitectEngineerFee", dataLoan.sArchitectEngineerFee_rep); // 1.c
            this.SetResult("sConsultantFee", dataLoan.sConsultantFee_rep); // 1.d
            this.SetResult("sRenovationConstrInspectFee", dataLoan.sRenovationConstrInspectFee_rep); // 1.e
            this.SetResult("sTitleUpdateFee", dataLoan.sTitleUpdateFee_rep); // 1.f
            this.SetResult("sPermitFee", dataLoan.sPermitFee_rep); // 1.g
            this.SetResult("sFinanceMortgagePmntRsrv", dataLoan.sFinanceMortgagePmntRsrv_rep); // 1.h
            this.SetResult("sRenoMtgPmntRsrvMnth", dataLoan.sRenoMtgPmntRsrvMnth_rep); // 1.h months
            this.SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep); // 1.h months multiplier
            this.SetResult("sFinanceMortgagePmntRsrvTgtVal", dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep); // 1.h Target
            this.SetResult("sFinanceMortgagePmntRsrvTgtValLckd", dataLoan.sFinanceMortgagePmntRsrvTgtValLckd); // 1.h Target Locked box
            this.SetResult("sMaxFinanceMortgagePmntRsrv", dataLoan.sMaxFinanceMortgagePmntRsrv_rep); // 1.h Max value
            this.SetResult("sOtherRenovationCost", dataLoan.sOtherRenovationCost_rep); // 1.i
            this.SetResult("sOtherRenovationCostDescription", dataLoan.sOtherRenovationCostDescription); // 1.i Description
            this.SetResult("sFinanceOriginationFee", dataLoan.sFinanceOriginationFee_rep); // 1.i financeable origination fee
            this.SetResult("sFinanceOriginationFeeTgtVal", dataLoan.sFinanceOriginationFeeTgtVal_rep); // 1.i financeable origination fee Target
            this.SetResult("sMaxFinanceOriginationFee", dataLoan.sMaxFinanceOriginationFee_rep); // 1.i financeable origination fee Max value
            this.SetResult("sDiscntPntOnRepairCostFee", dataLoan.sDiscntPntOnRepairCostFee_rep); // 1.i discount points
            this.SetResult("sDiscntPntOnRepairCostFeeTgtVal", dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep); // 1.i discount points Target
            this.SetResult("sMaxDiscntPntOnRepairCostFee", dataLoan.sMaxDiscntPntOnRepairCostFee_rep); // 1.i. discount points Max value
            this.SetResult("sFeasibilityStudyFee", dataLoan.sFeasibilityStudyFee_rep); // 1.i feasibility study
            this.SetResult("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep); // 2
        }

        private void SaveData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanId"), typeof(TotalRenovationCostCalculatorService));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            BindData(dataLoan);

            try
            {
                dataLoan.SyncTotalRenovationCostSnapshot();
                dataLoan.Save();

                this.SetResult("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep);
            }
            catch (CBaseException)
            {
                throw new CBaseException("Error while saving total renovation cost data.", "Error while saving total renovation cost data.");
            }
        }
    }
}