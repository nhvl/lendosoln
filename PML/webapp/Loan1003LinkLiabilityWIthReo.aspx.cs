﻿namespace PriceMyLoan.webapp
{
    using System;
    using global::DataAccess;
    using System.Web.UI.WebControls; 
    using LendersOffice.Common;

    public partial class Loan1003LinkLiabilityWIthReo : PriceMyLoan.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterCSS("/webapp/main.css");
            this.RegisterService("Loan1003", "/webapp/Loan1003PopupService.aspx");

            Guid appId = RequestHelper.GetGuid("appid");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003AddLiability));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(appId);

            IReCollection recoll = dataApp.aReCollection;
            ISubcollection subcoll = recoll.GetSubcollection(true, E_ReoGroupT.All);

            matchedReoRecordId.Items.Add(new ListItem("<-- Select a matched REO -->", Guid.Empty.ToString()));

            foreach (IRealEstateOwned reField in subcoll)
            {
                string addr = string.Format(@"{0}, {1}, {2} {3}", reField.Addr, reField.City, reField.State, reField.Zip);
                matchedReoRecordId.Items.Add(new ListItem(addr, reField.RecordId.ToString()));
            }
        }
    }
}