﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using global::DataAccess.FannieMae;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Migration;
    using MeridianLink.CommonControls;
    using PriceMyLoan.UI;

    /// <summary>
    /// FannieAddendumSections control. Holds the data for the FannieAddendum form.
    /// </summary>
    /// <remarks>
    /// This control replicates the \LendOSoln\LendersOfficeApp\newlos\FannieAddendum\FannieAddendumSections.ascx page in the LendersOfficeApp project.
    /// Make sure to keep both pages synchronized.
    /// </remarks>
    public partial class FannieAddendumSections : BaseUserControl
    {
        /// <summary>
        /// Radio checkbox name for when <see cref="CPageData.sIsUseDUDwnPmtSrc"/> is true.
        /// </summary>
        public const string nameof_DUDownPaymentSourceJSON = nameof(DUDownPaymentSourceJSON);

        /// <summary>
        /// Radio checkbox name for when <see cref="CPageData.sIsUseDUDwnPmtSrc"/> is false.
        /// </summary>
        public const string nameof_DUDownPaymentSource1003 = nameof(DUDownPaymentSource1003);

        /// <summary>
        /// Runs on the Page Init event.
        /// </summary>
        /// <param name="e">Event arguments for the Page Init event.</param>
        protected override void OnInit(EventArgs e)
        {
            FannieMaeArmPlanTable.BindToComboBox(this.sFannieARMPlanNum);
            Tools.Bind_sArmIndexT(this.sArmIndexT);
            Tools.Bind_sFannieSpT(this.sFannieSpT);
            Tools.Bind_TriState(this.sHomeIsMhAdvantageTri);
            Tools.Bind_sFannieDocT(this.sFannieDocT);
            Tools.Bind_sFannieCommunityLendingT(this.sFannieCommunityLendingT);
            Tools.Bind_sFannieCommunitySecondsRepaymentStructureT(this.sCommunitySecondsRepaymentStructureT);
            Tools.BindGenericEnum<E_FannieHomebuyerEducation>(this.sFannieHomebuyerEducationT);
            Tools.BindGenericEnum<E_sSpProjectClassFannieT>(this.sSpProjectClassFannieT);
            base.OnInit(e);
        }

        /// <summary>
        /// Gets the workflow operations that this control will expose to the user.
        /// </summary>
        public IEnumerable<WorkflowOperation> AdditionalWorkflowOperations => new[] { WorkflowOperations.RunDu, };

        /// <summary>
        /// Fills out all the data for this control from the given initialized <see cref="CPageData"/>.
        /// </summary>
        /// <param name="dataLoan">
        /// A <see cref="CPageData"/> object. 
        /// This method expects that the parent page will use <see cref="CPageData.CreateUsingSmartDependency(Guid, Type)"/> to create and <see cref="CPageData.InitLoad()"/> to load the object before passing it in here. 
        /// </param>
        public void LoadData(CPageData dataLoan)
        {
            // Load Data
            // There is no specific app associated with this page in the TPO portal, so use the first as default.
            CAppData dataApp = dataLoan.GetAppData(0);

            // Hidden Fields
            this.sSpAddr.Value = dataLoan.sSpAddr;
            this.sSpCity.Value = dataLoan.sSpCity;
            this.sSpCounty.Value = dataLoan.sSpCounty;
            this.sSpState.Value = dataLoan.sSpState;
            this.sSpZip.Value = dataLoan.sSpZip;
            this.sHmdaActionD.Value = dataLoan.sHmdaActionD_rep;
            this.sHmdaMsaNum.Value = dataLoan.sHmdaMsaNum;
            this.sHmdaCountyCode.Value = dataLoan.sHmdaCountyCode;
            this.sHmdaStateCode.Value = dataLoan.sHmdaStateCode;
            this.sHmdaCensusTract.Value = dataLoan.sHmdaCensusTract;

            // "Loan File" section.
            this.sDuCaseId.Text = dataLoan.sDuCaseId;
            // sDuLenderInstitutionId is not shown in the TPO portal.
            Tools.SetDropDownListValue(this.sFannieDocT, dataLoan.sFannieDocT);
            // OPM 21129 - disables community lending if loan is va or fha
            Tools.Bind_sLT(this.sLT);
            Tools.SetDropDownListValue(this.sLT, dataLoan.sLT);
            if (Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(this.sLT);
            }

            // "Loan Application"
            this.a1003SignD.Text = dataApp.a1003SignD_rep;
            this.a1003InterviewD.Text = dataApp.a1003InterviewD_rep;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                a1003InterviewDLckd.Checked = dataApp.a1003InterviewDLckd;
            }
            else
            {
                a1003InterviewDLckd.Visible = false;
                a1003InterviewDLckdLabel.Visible = false;
            }

            this.sFannieProdDesc.Text = dataLoan.sFannieProdDesc;
            this.sFannieProductCode.Text = dataLoan.sFannieProductCode;
            this.sFannieARMPlanNum.Text = dataLoan.sFannieARMPlanNum_rep;
            this.sFannieARMPlanNumLckd.Checked = dataLoan.sFannieARMPlanNumLckd;
            Tools.SetDropDownListValue(this.sArmIndexT, dataLoan.sArmIndexT);
            this.sArmIndexTLckd.Checked = dataLoan.sArmIndexTLckd;
            this.sFannieFips.Text = dataLoan.sFannieFips;
            this.sFannieFipsLckd.Checked = dataLoan.sFannieFipsLckd;

            // "Transmittal Summary"
            this.sIsSellerProvidedBelowMktFin.Checked = dataLoan.sIsSellerProvidedBelowMktFin == "Y";
            Tools.SetDropDownListValue(this.sFannieSpT, dataLoan.sFannieSpT);
            Tools.SetDropDownListValue(this.sSpProjectClassFannieT, dataLoan.sSpProjectClassFannieT);
            Tools.SetDropDownListValue(this.sHomeIsMhAdvantageTri, dataLoan.sHomeIsMhAdvantageTri);

            // "Community Lending"
            // 10/05/07 mf. OPM 18181. Community Lending Section
            this.sIsCommunityLending.Checked = dataLoan.sIsCommunityLending;
            this.sFannieMsa.Text = dataLoan.sFannieMsa;
            Tools.SetDropDownListValue(this.sFannieCommunityLendingT, dataLoan.sFannieCommunityLendingT);
            Tools.SetDropDownListValue(this.sCommunitySecondsRepaymentStructureT, dataLoan.sFannieCommunitySecondsRepaymentStructureT);
            Tools.SetDropDownListValue(this.sFannieHomebuyerEducationT, dataLoan.sFannieHomebuyerEducationT);
            this.sIsCommunitySecond.Checked = dataLoan.sIsCommunitySecond;
            this.sFannieIncomeLimitAdjPc.Text = dataLoan.sFannieIncomeLimitAdjPc_rep;

            // "Loan Application" (2nd container - Present Housing Expenses)
            this.sPresLTotPersistentHExpMinusRent.Text = dataLoan.sPresLTotPersistentHExpMinusRent_rep;
            this.sReTotMPmtPrimaryResidence.Text = dataLoan.sReTotMPmtPrimaryResidence_rep;
            this.sExportAdditionalLiabitiesFannieMae.Checked = dataLoan.sExportAdditionalLiabitiesFannieMae;
            this.PresentHousingRepeater.DataSource = AppsPresentHousingData(dataLoan);
            this.PresentHousingRepeater.DataBind();

            // "Down Payment"
            this.DUDownPaymentSource1003.Checked = !dataLoan.sIsUseDUDwnPmtSrc;
            this.DUDownPaymentSourceJSON.Checked = dataLoan.sIsUseDUDwnPmtSrc;

            var page = (UI.BasePage)this.Page;
            page.RegisterJsObjectWithJsonNetAnonymousSerializer("downpaymentSources", Tools.Options_sDwnPmtSrc);
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DownPaymentData", string.Format("var downPaymentData = {0};", dataLoan.sDUDwnPmtSrc.Serialize()), true);

            // "DU Validation"
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DuApprovedProviders", $"var duApprovedProviders = {SerializationHelper.JsonNetSerialize(DuServiceProviders.Instance.Providers.ToList())};", true);
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ThirdPartyProviders", $"var thirdPartyProviders = {dataLoan.sDuThirdPartyProviders.SerializeJson()};", true);

            // "Additional FHA Loan Information"
            // OPM 222854, 9/22/2015, ML
            Tools.Bind_sFHAHousingActSection(this.sFHAHousingActSection);
            this.sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;
            // The Originator Identifier fields from the main loan editor (sFhaLenderIdT, sFHALenderIdCode, sSponsoredOriginatorEIN, sFHASponsorAgentIdCode)
            //    should not be accessible in the TPO portal (OPM 449974).
            this.sFHASellerContribution.Text = dataLoan.sFHASellerContribution_rep;
            this.AppCounselTypes.DataSource = dataLoan.Apps;
            this.AppCounselTypes.DataBind();

            // "Additional VA Loan Information"
            this.aVaEntitleAmt.Text = dataLoan.Apps.Where(app => app.aIsPrimary).Single().aVaEntitleAmt_rep;
            this.sVaProMaintenancePmt.Text = dataLoan.sVaProMaintenancePmt_rep;
            this.sVaProUtilityPmt.Text = dataLoan.sVaProUtilityPmt_rep;
            this.sFHASellerContributionVa.Text = dataLoan.sFHASellerContribution_rep;
            this.VaAppTaxes.DataSource = dataLoan.Apps;
            this.VaAppTaxes.DataBind();

            page.RegisterJsGlobalVariables("AvailableEngines", string.Join("|", this.GetAvailableEngines(dataLoan)));
            page.RegisterJsGlobalVariables("CanRunDu", page.CurrentUserCanPerform(WorkflowOperations.RunDu));
            page.RegisterJsGlobalVariables("RunDuDenialReason", page.GetReasonsUserCannotPerformOperation(WorkflowOperations.RunDu).FirstOrDefault());
        }

        /// <summary>
        /// Event handler that initializes "Present Housing" values for each repeated template in the Repeater,
        /// based on the bound <see cref="AppPresentHousingData"/> objects.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="args">Event arguments for the "OnItemDataBound" event.</param>
        protected void PresentHousingRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Separator)
            {
                return;
            }

            AppPresentHousingData data = args.Item.DataItem as AppPresentHousingData;
            EncodedLiteral aBNm = args.Item.FindControl("aBNm") as EncodedLiteral;
            EncodedLiteral aCNm = args.Item.FindControl("aCNm") as EncodedLiteral;
            TextBox aPresTotHExpDesc = args.Item.FindControl("aPresTotHExpDesc") as TextBox;
            TextBox aPresTotHExpCalc = args.Item.FindControl("aPresTotHExpCalc") as TextBox;
            CheckBox aPresTotHExpLckd = args.Item.FindControl("aPresTotHExpCalcLckd") as CheckBox;

            aBNm.Text = data.aBNm;
            aCNm.Text = data.aCNm;
            aPresTotHExpDesc.Text = data.aPresTotHExpDesc;
            aPresTotHExpCalc.Text = data.aPresTotHExpCalc;
            aPresTotHExpLckd.Checked = data.aPresTotHExpLckd;
        }

        /// <summary>
        /// Event handler that initializes "App Counseling Types" values for each repeated template in the Repeater,
        /// based on the bound <see cref="CAppData"/> objects.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="args">Event arguments for the "OnItemDataBound" event.</param>
        protected void AppCounselTypes_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            CAppData data = args.Item.DataItem as CAppData;

            EncodedLiteral aBNm = args.Item.FindControl("aBNm") as EncodedLiteral;
            EncodedLiteral aCNm = args.Item.FindControl("aCNm") as EncodedLiteral;
            Control coborrower = args.Item.FindControl("coborrower") as Control;
            DropDownList aBTotalScoreFhtbCounselingT = args.Item.FindControl("aBTotalScoreFhtbCounselingT") as DropDownList;
            DropDownList aCTotalScoreFhtbCounselingT = args.Item.FindControl("aCTotalScoreFhtbCounselingT") as DropDownList;

            Tools.Bind_aBTotalScoreFhtbCounselingT(aBTotalScoreFhtbCounselingT);
            Tools.SetDropDownListValue(aBTotalScoreFhtbCounselingT, data.aBTotalScoreFhtbCounselingT);
            Tools.Bind_aBTotalScoreFhtbCounselingT(aCTotalScoreFhtbCounselingT);
            Tools.SetDropDownListValue(aCTotalScoreFhtbCounselingT, data.aCTotalScoreFhtbCounselingT);
            aBNm.Text = data.aBNm;
            aCNm.Text = data.aCNm;
            coborrower.Visible = !string.IsNullOrEmpty(data.aCSsn);
        }

        /// <summary>
        /// Event handler that initializes VA "Total Taxes" values for each repeated template in the Repeater,
        /// based on the bound <see cref="CAppData"/> objects.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="args">Event arguments for the "OnItemDataBound" event.</param>
        protected void VaAppTaxes_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            CAppData data = args.Item.DataItem as CAppData;

            // Borrower taxes
            EncodedLiteral aBNm0 = args.Item.FindControl("aBNm0") as EncodedLiteral;
            EncodedLiteral aBNm1 = args.Item.FindControl("aBNm1") as EncodedLiteral;
            EncodedLiteral aBNm2 = args.Item.FindControl("aBNm2") as EncodedLiteral;
            EncodedLiteral aBNm3 = args.Item.FindControl("aBNm3") as EncodedLiteral;
            MoneyTextBox aVaBFedITax = args.Item.FindControl("aVaBFedITax") as MoneyTextBox;
            MoneyTextBox aVaBStateITax = args.Item.FindControl("aVaBStateITax") as MoneyTextBox;
            MoneyTextBox aVaBSsnTax = args.Item.FindControl("aVaBSsnTax") as MoneyTextBox;
            MoneyTextBox aVaBOITax = args.Item.FindControl("aVaBOITax") as MoneyTextBox;

            aBNm0.Text = data.aBNm;
            aBNm1.Text = data.aBNm;
            aBNm2.Text = data.aBNm;
            aBNm3.Text = data.aBNm;
            aVaBFedITax.Text = data.aVaBFedITax_rep;
            aVaBStateITax.Text = data.aVaBStateITax_rep;
            aVaBSsnTax.Text = data.aVaBSsnTax_rep;
            aVaBOITax.Text = data.aVaBOITax_rep;

            // Co-borrower taxes
            Control coborrower = args.Item.FindControl("coborrower") as Control;
            EncodedLiteral aCNm0 = args.Item.FindControl("aCNm0") as EncodedLiteral;
            EncodedLiteral aCNm1 = args.Item.FindControl("aCNm1") as EncodedLiteral;
            EncodedLiteral aCNm2 = args.Item.FindControl("aCNm2") as EncodedLiteral;
            EncodedLiteral aCNm3 = args.Item.FindControl("aCNm3") as EncodedLiteral;
            MoneyTextBox aVaCFedITax = args.Item.FindControl("aVaCFedITax") as MoneyTextBox;
            MoneyTextBox aVaCStateITax = args.Item.FindControl("aVaCStateITax") as MoneyTextBox;
            MoneyTextBox aVaCSsnTax = args.Item.FindControl("aVaCSsnTax") as MoneyTextBox;
            MoneyTextBox aVaCOITax = args.Item.FindControl("aVaCOITax") as MoneyTextBox;

            aCNm0.Text = data.aCNm;
            aCNm1.Text = data.aCNm;
            aCNm2.Text = data.aCNm;
            aCNm3.Text = data.aCNm;
            aVaCFedITax.Text = data.aVaCFedITax_rep;
            aVaCStateITax.Text = data.aVaCStateITax_rep;
            aVaCSsnTax.Text = data.aVaCSsnTax_rep;
            aVaCOITax.Text = data.aVaCOITax_rep;
            coborrower.Visible = !string.IsNullOrEmpty(data.aCSsn);
        }

        /// <summary>
        /// Gets a list of <see cref="AppPresentHousingData"/> objects from the loan, for displaying in the UI.
        /// </summary>
        /// <param name="dataLoan">The loan data to load housing data from.</param>
        /// <returns>A list of present housing data entries for the applications on the loan.</returns>
        private static List<AppPresentHousingData> AppsPresentHousingData(CPageData dataLoan)
        {
            List<AppPresentHousingData> housingApps = new List<AppPresentHousingData>(dataLoan.nApps);
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData currentApp = dataLoan.GetAppData(i);
                AppPresentHousingData appForBind = new AppPresentHousingData();
                appForBind.aBNm = currentApp.aBNm;
                appForBind.aCNm = currentApp.aCNm;
                appForBind.aPresTotHExpDesc = currentApp.aPresTotHExpDesc;
                appForBind.aPresTotHExpCalc = currentApp.aPresTotHExpCalc_rep;
                appForBind.aPresTotHExpLckd = currentApp.aPresTotHExpLckd;
                housingApps.Add(appForBind);
            }

            return housingApps;
        }

        /// <summary>
        /// Gets the list of available underwriting/origination engines for the current page.
        /// </summary>
        /// <param name="dataLoan">The loan.</param>
        /// <returns>A list of the buttons for the available engines.</returns>
        private IReadOnlyCollection<string> GetAvailableEngines(CPageData dataLoan)
        {
            List<string> availableEngines = new List<string>(3);
            var actionCtrl = new Action();
            actionCtrl.IsLead = Tools.IsStatusLead(dataLoan.sStatusT);
            actionCtrl.IsQP2File = dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed;

            var user = (LendersOffice.Security.AbstractUserPrincipal)Page.User;
            var seamlessDuSettings = this.CurrentBroker.GetSeamlessDuSettings();
            bool showDuSeamlessButton = seamlessDuSettings.SeamlessDuEnabled
                && (user.Type == "B" || ((Security.PriceMyLoanPrincipal)user).EnableNewTpoLoanNavigation); // Should only show DU Seamless in the new PML UI and embedded PML.
            if (actionCtrl.AllowDuSubmission(this.CurrentBroker) && showDuSeamlessButton)
            {
                availableEngines.Add(this.DesktopUnderwriter_Seamless.ClientID);
            }

            return availableEngines;
        }

        /// <summary>
        /// Defines a container for a subset of Application data to use for "Present Housing" information.
        /// </summary>
        protected class AppPresentHousingData
        {
            public string aBNm { get; set; }
            public string aCNm { get; set; }
            public string aPresTotHExpDesc { get; set; }
            public string aPresTotHExpCalc { get; set; }
            public bool aPresTotHExpLckd { get; set; }

            public AppPresentHousingData() { }
        }
    }
}
