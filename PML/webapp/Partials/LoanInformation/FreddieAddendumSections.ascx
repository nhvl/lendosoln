﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreddieAddendumSections.ascx.cs" Inherits="PriceMyLoan.webapp.Partials.LoanInformation.FreddieAddendumSections" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<%-- Developer note: The code on this page is intended to mirror most of \LendOSoln\LendersOfficeApp\newlos\FreddieExport.aspx --%>

<script>
    TabManager.registerStartUpScript(function() {
        (function($){<%-- This ready function is reserved specifically for LPA Submission button binding. Behavior copied from Action.js --%>
            var $ToLPASeamless = $('<%= AspxTools.JSelector(LoanProductAdvisor_Seamless) %>');

            $ToLPASeamless.click(function() {
                LQBPopup.Show(gVirtualRoot + '/main/SeamlessLPA/SeamlessLpa.aspx#/audit?loanid=' + $('#sLId').val(), {
                    width: $(window).width() * .80,
                    height: 800,
                    hideCloseButton: true
                });
            });
                    
            function toggleEnableSubmission($Button, enable, userMessage) {
                $Button.prop('disabled', !enable);
                $Button.closest('.WarningIconMessage').prop('title', enable ? '' : userMessage);
                $Button.find('.WarningIcon').toggle(!enable).prop('title', enable ? '' : userMessage);
            }

            toggleEnableSubmission($ToLPASeamless, ML.CanRunLp, ML.RunLpDenialReason);

            var availableEngines = (ML.AvailableEngines || '').split('|');
            function toggleShowSubmission($Button) {
                var id = $Button.prop('id');
                $Button.toggleClass('hidden', availableEngines.indexOf(id) < 0);
            }

            toggleShowSubmission($ToLPASeamless);

            $('.WarningIconMessage[data-toggle="tooltip"]').tooltip();
        })(jQuery);
    });
    TabManager.registerStartUpScript(function () {
        var isDUDownPmtSrcsReadOnly = isReadOnly() || <%=AspxTools.JsGetElementById(this.DUDownPaymentSource1003)%>.checked;
        $j('#sDUDownPmtSrcs').downpayments({
            'downpaymentSources': downpaymentSources, 
            'data' : downPaymentData,
            readonly : isDUDownPmtSrcsReadOnly
        });
    
        <%=AspxTools.JQuery(this.DUDownPaymentSource1003)%>.change(function(){
            $j('#sDUDownPmtSrcs').downpayments('setReadOnly', true);
        });

        <%=AspxTools.JQuery(this.DUDownPaymentSourceJSON)%>.change(function(){
            $j('#sDUDownPmtSrcs').downpayments('setReadOnly', false);
        });
        <%=AspxTools.JQuery(this.sFredieReservesAmtLckd)%>.click(refreshCalculation);
        <%=AspxTools.JQuery(this.sFreddieArmIndexTLckd)%>.click(refreshCalculation);
        <%=AspxTools.JQuery(this.sBuydown)%>.click(onclick_sBuydown);

        updateFieldDisplay();
    });

    function onclick_sBuydown() {
        var b = <%=AspxTools.JsGetElementById(sBuydown)%>.checked;
        <%=AspxTools.JsGetElementById(sBuydownContributorT)%>.disabled = !b;
    }

    function updateFieldDisplay() {
        lockFieldCustom(<%=AspxTools.JQuery(sFredieReservesAmtLckd)%>, <%=AspxTools.JQuery(sFredieReservesAmt)%>);
        lockFieldCustom(<%=AspxTools.JQuery(sFreddieArmIndexTLckd)%>, <%=AspxTools.JQuery(sFreddieArmIndexT)%>);
        onclick_sBuydown();
    }

    function _postGetAllFormValues(args) {
        args.sDUDwnPmtSrc = JSON.stringify($j('#sDUDownPmtSrcs').downpayments('serialize'));
    }

    function _postRefreshCalculation(value, clientId) {
        $j('#sDUDownPmtSrcs').downpayments('initialize', JSON.parse(value.sDUDwnPmtSrc));
        <%=AspxTools.JQuery(this.sProdCashoutAmt)%>.prop('readOnly', value.sProdCashoutAmtReadOnly === 'True');
        updateFieldDisplay();
    }
</script>

<div class="content-container container-fluid">
    <div class="section">
        <header class="header">Property Info</header>
        <div class="loan-info-padding-left">
            <div class="table">
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sSpIsInPud) %>">Planned Unit Development Indicator</label>
                        <asp:CheckBox ID="sSpIsInPud" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sBuildingStatusT) %>">Building Status Type</label>
                        <asp:DropDownList ID="sBuildingStatusT" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sFreddieConstructionT) %>">New Construction</label>
                        <asp:DropDownList ID="sFreddieConstructionT" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sSpMarketVal) %>">Property Estimated Value Amount</label>
                        <ml:MoneyTextBox ID="sSpMarketVal" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <header class="header">Loan Info</header>
        <div class="loan-info-padding-left">
            <div class="table">
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sPayingOffSubordinate) %>">Secondary Financing Refinance Indicator</label>
                        <asp:CheckBox ID="sPayingOffSubordinate" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sProdCashoutAmt) %>">Cash Out Amount</label>
                        <ml:MoneyTextBox ID="sProdCashoutAmt" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sHelocCreditLimit) %>">HELOC Maximum Balance Amount</label>
                        <ml:MoneyTextBox ID="sHelocCreditLimit" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sHelocBal) %>">Subordinate Lien - HELOC Amount</label>
                        <ml:MoneyTextBox ID="sHelocBal" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sFHASalesConcessions) %>">Sales Concession Amount</label>
                        <ml:MoneyTextBox ID="sFHASalesConcessions" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <header class="header">Buydown</header>
        <div class="loan-info-padding-left">
            <div class="table">
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sBuydown) %>">Rate Buydown</label>
                        <asp:CheckBox ID="sBuydown" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sBuydownContributorT) %>">Buydown Contributor Type</label>
                        <asp:DropDownList ID="sBuydownContributorT" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <header class="header">Additional Info</header>
        <div class="loan-info-padding-left">
            <div class="table">
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sFredAffordProgId) %>">Offering Identifier</label>
                        <ml:ComboBox ID="sFredAffordProgId" runat="server" CssClass="input-w260" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sFredieReservesAmt) %>">Reserves</label>
                        <ml:MoneyTextBox ID="sFredieReservesAmt" runat="server" /> <label class="lock-checkbox no-float"><asp:CheckBox ID="sFredieReservesAmtLckd" runat="server" /></label>
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sNegAmortT) %>">Negative Amortization Type</label>
                        <asp:DropDownList ID="sNegAmortT" runat="server" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sFreddieArmIndexT) %>">ARM Index Type</label>
                        <asp:DropDownList ID="sFreddieArmIndexT" runat="server" /> <label class="lock-checkbox"><asp:CheckBox ID="sFreddieArmIndexTLckd" runat="server" /></label>
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sFreddieFHABorrPdCc) %>">Borrower Paid FHA/VA Closing Cost Amount</label>
                        <ml:MoneyTextBox ID="sFreddieFHABorrPdCc" runat="server" ReadOnly="true" />
                    </div>
                </div>
                <div>
                    <div>
                        <label for="<%= AspxTools.ClientId(sFHAFinancedDiscPtAmt) %>">Borrower Financed FHA Discount Points Amount</label>
                        <ml:MoneyTextBox ID="sFHAFinancedDiscPtAmt" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <header class="header">Down Payment</header>
        <div class="loan-info-padding-left">
            <div class="table wide">
                <div>
                    <div>
                        <asp:RadioButton runat="server" ID="DUDownPaymentSource1003" Text="Use 1003 downpayment source" GroupName="DwnSrc" />
                        <asp:RadioButton runat="server" ID="DUDownPaymentSourceJSON" Text="Enter new downpayment sources" GroupName="DwnSrc" />
                    </div>
                </div>
                <div>
                    <table id="sDUDownPmtSrcs" class="primary-table fnma-addendum-table down-payment-table none-hover-table"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <span class="WarningIconMessage" data-toggle="tooltip">
            <button type="button" class="btn btn-default hidden" id="LoanProductAdvisor_Seamless" runat="server">
                Submit to LPA (Seamless)
                <img runat="server" src="~/images/warning20x20.png" class="WarningIcon" />
            </button>
        </span>
    </div>
</div>
