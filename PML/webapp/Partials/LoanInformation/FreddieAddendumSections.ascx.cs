﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;

namespace PriceMyLoan.webapp.Partials.LoanInformation
{
    public partial class FreddieAddendumSections : PriceMyLoan.UI.BaseUserControl
    {
        /// <summary>
        /// Radio checkbox name for when <see cref="CPageData.sIsUseDUDwnPmtSrc"/> is true.
        /// </summary>
        public const string nameof_DUDownPaymentSourceJSON = nameof(DUDownPaymentSourceJSON);

        /// <summary>
        /// Radio checkbox name for when <see cref="CPageData.sIsUseDUDwnPmtSrc"/> is false.
        /// </summary>
        public const string nameof_DUDownPaymentSource1003 = nameof(DUDownPaymentSource1003);

        protected void Page_Init(object sender, EventArgs e)
        {
            Tools.Bind_sBuildingStatusT(this.sBuildingStatusT);
            Tools.Bind_sFreddieConstructionT(this.sFreddieConstructionT);
            Tools.Bind_sBuydownContributorT(this.sBuydownContributorT);
            Tools.Bind_sFredAffordProgId(this.sFredAffordProgId);
            Tools.Bind_sNegAmortT(this.sNegAmortT);
            Tools.Bind_sFreddieArmIndexT(this.sFreddieArmIndexT);
            ((BasePage)this.Page).RegisterJsObjectWithJsonNetAnonymousSerializer("downPaymentSources", Tools.Options_sDwnPmtSrc);
        }

        /// <summary>
        /// Gets the workflow operations that this control will expose to the user.
        /// </summary>
        public IEnumerable<WorkflowOperation> AdditionalWorkflowOperations => new[] { WorkflowOperations.RunLp, };

        public void LoadData(CPageData loanData)
        {
            this.sSpIsInPud.Checked = loanData.sSpIsInPud;
            Tools.SetDropDownListValue(this.sBuildingStatusT, loanData.sBuildingStatusT);
            Tools.SetDropDownListValue(this.sFreddieConstructionT, loanData.sFreddieConstructionT);
            this.sSpMarketVal.Text = loanData.sSpMarketVal_rep;

            this.sPayingOffSubordinate.Checked = loanData.sPayingOffSubordinate;
            this.sProdCashoutAmt.Text = loanData.sProdCashoutAmt_rep;
            this.sProdCashoutAmt.ReadOnly = loanData.sProdCashoutAmtReadOnly;
            this.sHelocCreditLimit.Text = loanData.sHelocCreditLimit_rep;
            this.sHelocBal.Text = loanData.sHelocBal_rep;
            this.sFHASalesConcessions.Text = loanData.sFHASalesConcessions_rep;

            this.sBuydown.Checked = loanData.sBuydown;
            Tools.SetDropDownListValue(this.sBuydownContributorT, loanData.sBuydownContributorT);

            this.sFredAffordProgId.Text = loanData.sFredAffordProgId;
            this.sFredieReservesAmt.Text = loanData.sFredieReservesAmt_rep;
            this.sFredieReservesAmtLckd.Checked = loanData.sFredieReservesAmtLckd;
            Tools.SetDropDownListValue(this.sNegAmortT, loanData.sNegAmortT);
            Tools.SetDropDownListValue(this.sFreddieArmIndexT, loanData.sFreddieArmIndexT);
            this.sFreddieArmIndexTLckd.Checked = loanData.sFreddieArmIndexTLckd;
            this.sFreddieFHABorrPdCc.Text = loanData.sFreddieFHABorrPdCc_rep;
            this.sFHAFinancedDiscPtAmt.Text = loanData.sFHAFinancedDiscPtAmt_rep;

            bool useDuDownPaymentSource = loanData.sIsUseDUDwnPmtSrc;
            this.DUDownPaymentSource1003.Checked = !useDuDownPaymentSource;
            this.DUDownPaymentSourceJSON.Checked = useDuDownPaymentSource;
            var page = (UI.BasePage)this.Page;
            page.RegisterJsObjectWithJsonNetAnonymousSerializer("downpaymentSources", Tools.Options_sDwnPmtSrc);
            this.Page.ClientScript.RegisterClientScriptBlock(
                typeof(FreddieAddendumSections),
                "DownPaymentData",
                "var downPaymentData = " + loanData.sDUDwnPmtSrc.Serialize() + ";",
                addScriptTags: true);

            page.RegisterJsGlobalVariables("AvailableEngines", string.Join("|", this.GetAvailableEngines(loanData)));
            page.RegisterJsGlobalVariables("CanRunLp", page.CurrentUserCanPerform(WorkflowOperations.RunLp));
            page.RegisterJsGlobalVariables("RunLpDenialReason", page.GetReasonsUserCannotPerformOperation(WorkflowOperations.RunLp).FirstOrDefault());
        }

        /// <summary>
        /// Gets the list of available underwriting/origination engines for the current page.
        /// </summary>
        /// <param name="dataLoan">The loan.</param>
        /// <returns>A list of the buttons for the available engines.</returns>
        private IReadOnlyCollection<string> GetAvailableEngines(CPageData dataLoan)
        {
            List<string> availableEngines = new List<string>(3);
            var actionCtrl = new Action();
            actionCtrl.IsLead = Tools.IsStatusLead(dataLoan.sStatusT);
            actionCtrl.IsQP2File = dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed;

            var user = (LendersOffice.Security.AbstractUserPrincipal)Page.User;
            bool showLpaSeamlessButton = this.CurrentBroker.IsSeamlessLpaEnabled
                && (user.Type == "B" || ((Security.PriceMyLoanPrincipal)user).EnableNewTpoLoanNavigation);
            if (actionCtrl.AllowLPSubmission(this.CurrentBroker) && showLpaSeamlessButton)
            {
                availableEngines.Add(this.LoanProductAdvisor_Seamless.ClientID);
            }

            return availableEngines;
        }
    }
}