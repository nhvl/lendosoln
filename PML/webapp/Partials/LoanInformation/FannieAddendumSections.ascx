﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FannieAddendumSections.ascx.cs" Inherits="PriceMyLoan.webapp.FannieAddendumSections" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<%-- Developer Note: This page replicates the page at \LendOSoln\LendersOfficeApp\newlos\FannieAddendum\FannieAddendumSections.ascx in the main loan editor. Make sure to update both when changing content.--%>

<script>
    TabManager.registerStartUpScript(function() {
        (function($){<%-- This ready function is reserved specifically for DU Submission button binding. Behavior copied from Action.js --%>
            var $ToDUSeamless = $('<%= AspxTools.JSelector(DesktopUnderwriter_Seamless) %>');

            $ToDUSeamless.click(function() {
                LQBPopup.Show(gVirtualRoot + '/main/SeamlessDU/SeamlessDU.aspx#/audit?loanid=' + $('#sLId').val(), {
                    width: $(window).width() * .80,
                    height: 800,
                    hideCloseButton: true
                });
            });
                    
            function toggleEnableSubmission($Button, enable, userMessage) {
                $Button.prop('disabled', !enable);
                $Button.closest('.WarningIconMessage').prop('title', enable ? '' : userMessage);
                $Button.find('.WarningIcon').toggle(!enable).prop('title', enable ? '' : userMessage);
            }

            toggleEnableSubmission($ToDUSeamless, ML.CanRunDu, ML.RunDuDenialReason);

            var availableEngines = (ML.AvailableEngines || '').split('|');
            function toggleShowSubmission($Button) {
                var id = $Button.prop('id');
                $Button.toggleClass('hidden', availableEngines.indexOf(id) < 0);
            }

            toggleShowSubmission($ToDUSeamless);

            $('.WarningIconMessage[data-toggle="tooltip"]').tooltip();
        })(jQuery);
    });
    function DocumentReady() {
        var isDUDownPmtSrcsReadOnly = isReadOnly() || <%=AspxTools.JsGetElementById(this.DUDownPaymentSource1003)%>.checked;
        $j('#sDUDownPmtSrcs').downpayments({
            'downpaymentSources': downpaymentSources, 
            'data' : downPaymentData,
            readonly : isDUDownPmtSrcsReadOnly
        });
    
        <%=AspxTools.JQuery(this.DUDownPaymentSource1003)%>.change(function(){
            $j('#sDUDownPmtSrcs').downpayments('setReadOnly', true);
        });

        <%=AspxTools.JQuery(this.DUDownPaymentSourceJSON)%>.change(function(){
            $j('#sDUDownPmtSrcs').downpayments('setReadOnly', false);
        });

        <%=AspxTools.JQuery(this.sLT)%>.change(function() {
            var sLT = $j(this).val();
            $j('#FHAAddendumRow').toggle(sLT === <%=AspxTools.JsString(E_sLT.FHA.ToString("D"))%>);
            $j('#AdditionalVaInformation').toggle(sLT === <%=AspxTools.JsString(E_sLT.VA.ToString("D"))%>);
            FannieAddendumSectionsValidate();
        }).change();

        <%=AspxTools.JQuery(this.sFannieSpT)%>.change(function () {
            onPropertyTypeChange();
            refreshCalculation();
        });

        onPropertyTypeChange();

        <%=AspxTools.JQuery(this.sFannieCommunityLendingT)%>.change(FannieAddendumSectionsValidate).change;
        <%=AspxTools.JQuery(this.sFannieFips)%>.change(validateFips).change();
        <%=AspxTools.JQuery(this.sFannieFipsLckd)%>.change(onFipsLocked).change();
        <% if (!IsReadOnly) { %>
        <%= AspxTools.JQuery(this.sIsCommunityLending) %>.change(onClick_sIsCommunityLending).change();
        <% } %>
        <%=AspxTools.JQuery(sFannieARMPlanNumLckd)%>.change(function() {lockFieldCustom(<%=AspxTools.JQuery(sFannieARMPlanNumLckd)%>, <%=AspxTools.JQuery(sFannieARMPlanNum)%>);}).change();
        <%= AspxTools.JQuery(sArmIndexTLckd) %>.change(function() {lockFieldCustom(<%= AspxTools.JQuery(sArmIndexTLckd) %>, <%= AspxTools.JQuery(sArmIndexT) %>)}).change();
        
        $j("[id$='_aPresTotHExpCalcLckd']")
            .change(function() {
                /*
                Structure of Housing expense:
                <div class="presentHousingItem">
                    <div>..</div>
                    <div>
                        <label class="lock-checkbox checked">
                            <span class="material-icons"></span>
                            <input id="FannieMaeAddendumSections_PresentHousingRepeater_ctl00_aPresTotHExpCalcLckd" type="checkbox" name="FannieMaeAddendumSections$PresentHousingRepeater$ctl00$aPresTotHExpCalcLckd" checked="checked" onclick="refreshCalculation();">
                        </label>
                    </div>
                </div>
                */
                lockFieldCustom($j(this), $j(this).parent().parent().parent().find("[id$='_aPresTotHExpCalc']"))
            }).change();

        <%= AspxTools.JQuery(a1003InterviewDLckd)%>.change(function() {lockFieldCustom(<%= AspxTools.JQuery(a1003InterviewDLckd)%>, <%= AspxTools.JQuery(a1003InterviewD)%>); }).change();
        <%=AspxTools.JQuery(this.sLT, this.sFannieFips, this.sFannieFipsLckd, this.sIsCommunityLending, this.sFannieCommunityLendingT)%>.add($j('#IsUpdate')).on("change blur", checkValidation).change();
    }

    TabManager.registerStartUpScript(DocumentReady);
    TabManager.registerStartUpScript(function() {
        angular.bootstrap(angular.element(document.getElementById('duValidation')), ['duValidation']);
    });
    TabManager.registerValidationFunction(FannieAddendumSectionsValidate);

    var fannieCommunityLendingT_originalIndex = <%=AspxTools.JsString(sFannieCommunityLendingT.SelectedValue)%>;

    var duValidationApp = angular.module('duValidation', []);
    duValidationApp.factory('thirdPartyProvider', function() {
        thirdPartyProvider = function(Name, ReferenceNumber)
        {
            if (typeof Name === 'undefined') this.Name = '';
            else this.Name = Name;
            if (typeof ReferenceNumber === 'undefined') this.ReferenceNumber = '';
            else this.ReferenceNumber = ReferenceNumber;
        }

        return thirdPartyProvider;
    });

    duValidationApp.controller('duValidationController', function($scope, thirdPartyProvider)
    {
        if (typeof thirdPartyProviders === 'undefined' || !thirdPartyProviders || thirdPartyProviders.length === 0)
        {
            $scope.thirdPartyProviders = [new thirdPartyProvider(), new thirdPartyProvider(), new thirdPartyProvider()];
        }
        else
        {
            $scope.thirdPartyProviders = thirdPartyProviders;
            while ($scope.thirdPartyProviders.length < 3)
            {
                $scope.thirdPartyProviders.push(new thirdPartyProvider());
            }
        }

        if (typeof duApprovedProviders !== 'undefined' && !!duApprovedProviders && duApprovedProviders.length !== 0)
        {
            duApprovedProviders.sort(function(a, b){<%-- By DisplayName, except Placeholder is last --%>
                if (a.ProviderName.toLowerCase() === 'placeholder') {
                    return 1;
                } else if (b.ProviderName.toLowerCase() === 'placeholder') {
                    return -1;
                } else {
                    return a.DisplayName.localeCompare(b.DisplayName);
                }
            });
            $scope.thirdPartyProviderNames = duApprovedProviders;
        }

        $scope.addItem = function() {
            var newProvider = new thirdPartyProvider();
            $scope.thirdPartyProviders.push(newProvider);
        };

        $scope.removeItem = function(index) {
            $scope.thirdPartyProviders.splice(index, 1);
            $scope.onInputChange(true);
        }

        $scope.onInputChange = function(updateDirty) {
            checkValidation();
            // Set dirty bit.
            if (updateDirty)
                updateDirtyBit();
        }
    });

    function _postGetAllFormValues(args)
    {
        args.sDUDwnPmtSrc = JSON.stringify($j('#sDUDownPmtSrcs').downpayments('serialize'));
        args.presentHousing = JSON.stringify($j('.presentHousingItem').map(function() {
            return {
                "aPresTotHExpDesc": $j(this).find("[id*='aPresTotHExpDesc']").val(),
                "aPresTotHExpCalc": $j(this).find("[id*='aPresTotHExpCalc']").val(),
                "aPresTotHExpCalcLckd": $j(this).find("[id*='aPresTotHExpCalcLckd']").prop('checked')
            };
        }).get());
        args.appCounseling = JSON.stringify(jQuery('.appCounseling').map(function() 
        { return {
            "aBTotalScoreFhtbCounselingT": $j(this).find("[id*='aBTotalScoreFhtbCounselingT']").val(), 
            "aCTotalScoreFhtbCounselingT": $j(this).find("[id*='aCTotalScoreFhtbCounselingT']").val() }; 
        }).get());
        args.vaTaxes = JSON.stringify(jQuery('.AppTaxes').map(function() 
        { return {
            "aVaBFedITax": $j(this).find("[id*='aVaBFedITax']").val(), 
            "aVaBStateITax": $j(this).find("[id*='aVaBStateITax']").val(), 
            "aVaBSsnTax": $j(this).find("[id*='aVaBSsnTax']").val(), 
            "aVaBOITax": $j(this).find("[id*='aVaBOITax']").val(),
            "aVaCFedITax": $j(this).find("[id*='aVaCFedITax']").val(), 
            "aVaCStateITax": $j(this).find("[id*='aVaCStateITax']").val(), 
            "aVaCSsnTax": $j(this).find("[id*='aVaCSsnTax']").val(),
            "aVaCOITax": $j(this).find("[id*='aVaCOITax']").val() };
        }).get());
        if(angular.element($j('#duValidation')).scope())
            args.duThirdPartyProviders = JSON.stringify(angular.element($j('#duValidation')).scope().thirdPartyProviders.filter(function(provider) { return provider.Name && provider.ReferenceNumber }));
        else
            args.duThirdPartyProviders = '[]';
    }

    function _postSaveCallback(values) {<%-- 2017-10 tj - I couldn't find evidence that this is ever called --%>
        $j('#sDUDownPmtSrcs').downpayments('initialize', JSON.parse(values.sDUDwnPmtSrc) );
        _postRefreshCalculation(values);
    }

    function _postRefreshCalculation(values) {
        $j("[id$='_aPresTotHExpCalcLckd']").each(function(index) {
            if (!$j(this).prop('checked'))
                $j(this).parent().parent().find("[id$='_aPresTotHExpCalc']").val(values['aPresTotHExpCalc' + index]);
        });
    }
    
    function FannieAddendumSectionsValidate()
    {
        var invalidFields = $j('.ng-invalid');
        var communityLendingValid = ValidateCommunityLending()
        var fipsValid = validateFips();

        return communityLendingValid && invalidFields.length === 0 && validateFips();
    }

    var FannieAddendumSectionsValid = false;

    function ValidateCommunityLending()
    {
        var v = <%=AspxTools.JQuery(sLT)%>.val();
        var communityLendingEnabled = $j(<%=AspxTools.JsGetElementById(sIsCommunityLending)%>).prop('checked');
        if (!communityLendingEnabled)
        {
            $j('#sDiscontinuedProductImage, #sDiscontinuedProductMessage, #sDisableCL, #enableCommunityLending').hide();
            return true;
        }

        var communityLendingForbiddenForLoanType = (v === <%=AspxTools.JsString(E_sLT.FHA.ToString("D"))%> || v === <%=AspxTools.JsString(E_sLT.VA.ToString("D"))%>);
        var communityLendingProduct = $j('#<%=AspxTools.JsStringUnquoted(sFannieCommunityLendingT.ClientID)%> option:selected').text();
        var communityLendingProductDiscontinued = communityLendingProduct.match('discontinued') != null && <%=AspxTools.JQuery(sFannieCommunityLendingT)%>.val() !== fannieCommunityLendingT_originalIndex;

        $j('#sDiscontinuedProductImage, #sDiscontinuedProductMessage').toggle(communityLendingProductDiscontinued);
        $j('#sDisableCL, #enableCommunityLending').toggle(communityLendingForbiddenForLoanType);

        return !communityLendingForbiddenForLoanType && !communityLendingProductDiscontinued;
    }

    function onClick_sIsCommunityLending()
    {
        <% if (!IsReadOnly) { %>
        bEnable = <%=AspxTools.JQuery(sIsCommunityLending)%>.prop('checked');
        <%=AspxTools.JQuery(sFannieMsa, sFannieIncomeLimitAdjPc)%>.prop('readOnly', !bEnable);
        <%=AspxTools.JQuery(sFannieCommunityLendingT, sCommunitySecondsRepaymentStructureT, sFannieHomebuyerEducationT)%>.prop('disabled', !bEnable).toggleClass('readonly', !bEnable);
        <%=AspxTools.JQuery(sIsCommunitySecond)%>.prop('disabled', !bEnable);
        TPOStyleUnification.ToggleDisabledCheckbox(<%=AspxTools.JQuery(sIsCommunitySecond)%>, !bEnable);
        ValidateCommunityLending();
        var v = <%=AspxTools.JQuery(sLT)%>.val();
        if ((v == <%=AspxTools.JsString(E_sLT.FHA.ToString("D"))%> || v == <%=AspxTools.JsString(E_sLT.VA.ToString("D"))%>) && !bEnable)
        {
            resetCommunityLending();
        }
        FannieAddendumSectionsValidate();
        <% } %>
    }

    function resetCommunityLending()
    {
        <%=AspxTools.JQuery(sFannieMsa)%>.val("");
        <%=AspxTools.JsGetElementById(sFannieCommunityLendingT)%>.value = "0";
        <%=AspxTools.JsGetElementById(sIsCommunitySecond)%>.checked = false;
        <%=AspxTools.JsGetElementById(sFannieIncomeLimitAdjPc)%>.value = "0.000%";
    }

    function validateFips()
    {
        lockFieldCustom(<%=AspxTools.JQuery(sFannieFipsLckd)%>, <%=AspxTools.JQuery(sFannieFips)%>);
        if (<%=AspxTools.JQuery(sFannieFipsLckd)%>.prop('checked'))
        {
            var fipsCode = <%=AspxTools.JQuery(sFannieFips)%>;
            if (fipsCode.val().length === 0 || fipsCode.val().length === 11)
            {
                $j("#FipsCodeWarning").hide();
                $j("#FipsCodeInvalid").hide();
                return true;
            }
            else
            {
                $j("#FipsCodeWarning").show();
                $j("#FipsCodeInvalid").show();
                return false;
            }
        }

        return true;
    }

    function onFipsLocked()
    {
        if (<%=AspxTools.JQuery(this.sFannieFipsLckd)%>.prop('checked'))
        {
            <%=AspxTools.JQuery(this.sFannieFips)%>.prop('disabled', false);
        }
        else
        {
            <%=AspxTools.JQuery(this.sFannieFips)%>.prop('disabled', true);
            $j("#FipsCodeWarning").hide();
            $j("#FipsCodeInvalid").hide();
            
            // Recalculate the FIPS code from the existing geocode information.
            var stateCode = <%=AspxTools.JQuery(this.sHmdaStateCode)%>.val();
            var countyCode = <%=AspxTools.JQuery(this.sHmdaCountyCode)%>.val();
            var censusTract = <%=AspxTools.JQuery(this.sHmdaCensusTract)%>.val();
            var fipsCode = getFipsCode(stateCode, countyCode, censusTract);
            
            if (fipsCode.length != 11)
            {
                fipsCode = '';
            }
                
            <%=AspxTools.JQuery(this.sFannieFips)%>.val(fipsCode);
        }

        validateFips();
    }

    var dontHandleError = false;

    function fannieAddendum_importGeoCodes()
    {
        return importGeoCodes({
            "sSpAddr": <%=AspxTools.JsGetElementById(this.sSpAddr)%>.value,
            "sSpCity": <%=AspxTools.JsGetElementById(this.sSpCity)%>.value,
            "sSpCounty": <%=AspxTools.JsGetElementById(this.sSpCounty)%>.value,
            "sSpState": <%=AspxTools.JsGetElementById(this.sSpState)%>.value,
            "sSpZip": <%=AspxTools.JsGetElementById(this.sSpZip)%>.value,
            "sHmdaActionD": <%=AspxTools.JsGetElementById(this.sHmdaActionD)%>.value
        });
    }

    function onPropertyTypeChange() {
        var manufacturedTypes = [
            <%=AspxTools.JsString(E_sFannieSpT.Manufactured)%>,
            <%=AspxTools.JsString(E_sFannieSpT.ManufacturedCondoPudCoop)%>
        ];

        var propertyType = <%=AspxTools.JQuery(this.sFannieSpT)%>.val();
        $('#HomeIsMhAdvantageRow').toggle(manufacturedTypes.indexOf(propertyType) !== -1);
    }
</script>

<input type="Hidden" id="sSpAddr" runat="server" value="" />
<input type="Hidden" id="sSpCity" runat="server" value="" />
<input type="Hidden" id="sSpCounty" runat="server" value="" />
<input type="Hidden" id="sSpState" runat="server" value="" />
<input type="Hidden" id="sSpZip" runat="server" value="" />
<input type="Hidden" id="sHmdaActionD" runat="server" value="" />
<input type="Hidden" id="sHmdaMsaNum" runat="server" ClientIDMode="Static" value="" />
<input type="Hidden" id="sHmdaCountyCode" runat="server" ClientIDMode="Static" value="" />
<input type="Hidden" id="sHmdaStateCode" runat="server" ClientIDMode="Static" value="" />
<input type="Hidden" id="sHmdaCensusTract" runat="server" ClientIDMode="Static" value="" />

<div class="content-container">
    <div class="container-fluid">
        <div class="section">
            <header class="header">Loan File</header>
            <div class="loan-info-padding-left">
                <div class="table table-bottom">
                    <div>
                        <div>
                            <label>DO / DU Case ID</label>
                            <asp:TextBox ID="sDuCaseId" runat="server" MaxLength="30" ClientIDMode="Static"></asp:TextBox>
                        </div>
                        <div>
                            <label>Doc Type</label>
                            <asp:DropDownList ID="sFannieDocT" runat="server"></asp:DropDownList>
                        </div>
                        <div>
                            <label>Loan Type</label>
                            <asp:DropDownList ID="sLT" runat="server" Enabled="false" CssClass="readonly" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <header class="header">Loan Application</header>
            <div class="loan-info-padding-left">
                <div class="table table-loan-app-prod-desc">
                    <div>
                        <div>
                            <label>Signature Date</label>
                            <ml:DateTextBox ID="a1003SignD" runat="server" preset="date" IsDisplayCalendarHelper="false"></ml:DateTextBox>
                        </div>
                        <div>
                            <label>Interview Date</label>
                            <ml:DateTextBox ID="a1003InterviewD" runat="server" Width="75" preset="date" IsDisplayCalendarHelper="false"></ml:DateTextBox>
                            <label class="lock-checkbox" runat="server" id="a1003InterviewDLckdLabel"><asp:CheckBox ID="a1003InterviewDLckd" runat="server" onclick="refreshCalculation();"/></label>
                        </div>
                        <div>
                            <label>Product Description</label>
                            <asp:TextBox ID="sFannieProdDesc" runat="server" />
                        </div>
                        <div>
                            <label>Fannie Product Code</label>
                            <asp:TextBox ID="sFannieProductCode" MaxLength="15" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="table">
                    <div>
                        <div class="col-fannie-arm">
                            <label>Fannie ARM Plan #</label>
                            <div><ml:ComboBox ID="sFannieARMPlanNum" runat="server" class="form-control-w380"></ml:ComboBox><label class="lock-checkbox"><asp:CheckBox ID="sFannieARMPlanNumLckd" runat="server" onclick="refreshCalculation();" /></label></div>
                        </div>
                        <div>
                            <label>ARM Index Type</label>
                            <asp:DropDownList ID="sArmIndexT" onchange="refreshCalculation();" runat="server"></asp:DropDownList><label class="lock-checkbox"><asp:CheckBox ID="sArmIndexTLckd" runat="server" onclick="refreshCalculation();" /></label>
                        </div>
                    </div>
                </div>
                <div class="table table-bottom">
                    <div>
                        <div>
                            <label>FIPS Code</label>
                            <asp:TextBox ID="sFannieFips" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                            <label class="lock-checkbox"><asp:CheckBox ID="sFannieFipsLckd" runat="server" ClientIDMode="Static" /></label>
                            <img id="FipsCodeInvalid" class="Hidden" src="<%=AspxTools.HtmlString(ResolveUrl("~/images/error_icon.gif"))%>" alt="Required" />
                            <div class="field-container Hidden WarningText" id="FipsCodeWarning">The FIPS Code must be 11 digits. State Code + County Code + Census Tract</div>
                        </div>
                        <div class="vertical-align-bottom">
                            <button type="button" id="LookupGeoCode" onclick="fannieAddendum_importGeoCodes();" class="btn btn-default">Import Geocodes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <header class="header">Transmittal Summary</header>
            <div class="loan-info-padding-left">
                <div class="table">
                    <div>
                        <div>
                            <asp:CheckBox ID="sIsSellerProvidedBelowMktFin" runat="server" Text="Seller Provided Below Market Financing" CssClass="FieldLabel"></asp:CheckBox>
                        </div>
                    </div>
                </div>
                <div class="table">
                    <div>
                        <div>
                            <label>FNMA Property Type</label>
                            <asp:DropDownList ID="sFannieSpT" runat="server"></asp:DropDownList>
                        </div>
                        <div>
                            <label>FNMA Project Classification</label>
                            <asp:DropDownList ID="sSpProjectClassFannieT" CssClass="ddl-fnma-classify" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="table table-bottom" id="HomeIsMhAdvantageRow">
                    <div>
                        <div>
                            <label>Is home MH Advantage?</label>
                            <asp:DropDownList ID="sHomeIsMhAdvantageTri" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <header class="header">Community Lending</header>
            <div class="loan-info-padding-left">
                <div class="table">
                    <div>
                        <div>
                            <asp:CheckBox ID="sIsCommunityLending" onclick="onClick_sIsCommunityLending();" runat="server" Text="Community Lending" CssClass="FieldLabel"></asp:CheckBox>
                            <img id="sDisableCL" class="Hidden" src="<%=AspxTools.HtmlString(ResolveUrl("~/images/error_icon.gif")) %>" alt="Error" />
                            <span class="Hidden WarningText" id="enableCommunityLending">Community Lending is not allowed for FHA and VA loans. </span>
                        </div>
                    </div>
                </div>
                <div class="table table-loan-info-community">
                    <div>
                        <div>
                            <label>Metropolitan Statistical Area or County</label>
                            <asp:TextBox ID="sFannieMsa" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                        </div>
                        <div>
                            <label>Community Lending Product</label>
                            <asp:DropDownList ID="sFannieCommunityLendingT" runat="server" CssClass="xl"></asp:DropDownList><img id="sDiscontinuedProductImage" src="<%=AspxTools.HtmlString(ResolveUrl("~/images/error_icon.gif"))%>" class="Hidden" alt="Required" />
                            <span id="sDiscontinuedProductMessage" class="Hidden WarningText display-inline-flex">Product no longer supported by Fannie Mae.<br/>Please select a different product to submit</span>
                        </div>
                    </div>
                </div>
                <div class="table">
                    <div>
                        <div>
                            <label>Community Seconds Repayment Structure</label>
                            <asp:DropDownList runat="server" ID="sCommunitySecondsRepaymentStructureT" CssClass="xl"></asp:DropDownList>
                        </div>
                        <div>
                            <label>Homebuyer Education Completion</label>
                            <asp:DropDownList runat="server" ID="sFannieHomebuyerEducationT"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="table">
                    <div>
                        <div><asp:CheckBox ID="sIsCommunitySecond" runat="server" Text="Community Seconds" CssClass="FieldLabel"></asp:CheckBox></div>
                    </div>
                </div>
                <div class="table table-bottom">
                    <div>
                        <div>
                            <label>Income Limit Adj. Factor</label>
                            <ml:PercentTextBox ID="sFannieIncomeLimitAdjPc" Width="60px" runat="server" preset="percent"></ml:PercentTextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <header class="header">Loan Application</header>
            <div class="loan-info-padding-left">
                <div class="table">
                    <div>
                        <div>
                            <label>Present housing expense included in ratios (excluding rent)</label>
                            <ml:MoneyTextBox ID="sPresLTotPersistentHExpMinusRent" runat="server" ReadOnly="true" />
                        </div>
                        <div>
                            <label>Mortgage liability payments linked to primary residences (all applications)</label>
                            <ml:MoneyTextBox ID="sReTotMPmtPrimaryResidence" runat="server" ReadOnly="true" />
                        </div>
                    </div>
                    <div>
                        <div>
                            <asp:CheckBox ID="sExportAdditionalLiabitiesFannieMae" runat="server" Text="Add liabilities for additional housing expenses"/>
                        </div>
                        <div></div>
                    </div>
                </div>
                <div class="table table-bottom">
                    <asp:Repeater ID="PresentHousingRepeater" runat="server" OnItemDataBound="PresentHousingRepeater_OnItemDataBound" ClientIDMode="AutoID">
                        <ItemTemplate>
                            <div class="presentHousingItem">
                                <div>
                                    <div class="table table-font-default table-fnma-addendum-loan-app">
                                        <div>
                                            <div>
                                                <label>
                                                    <ml:EncodedLiteral ID="aBNm" runat="server"></ml:EncodedLiteral>
                                                </label>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <label>
                                                    <ml:EncodedLiteral ID="aCNm" runat="server"></ml:EncodedLiteral>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <asp:TextBox ID="aPresTotHExpDesc" runat="server" Width="200px"></asp:TextBox>
                                </div>
                                <div>
                                    <ml:MoneyTextBox ID="aPresTotHExpCalc" runat="server" />
                                    <label class="lock-checkbox"><asp:CheckBox ID="aPresTotHExpCalcLckd" runat="server" onclick="refreshCalculation();" /></label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div class="section">
            <header class="header">Down Payment</header>
            <div class="loan-info-padding-left">
                <div>
                    <asp:RadioButton runat="server" ID="DUDownPaymentSource1003" CssClass="FieldLabel" Text="Use 1003 downpayment source" GroupName="DwnSrc" />
                    <asp:RadioButton runat="server" ID="DUDownPaymentSourceJSON" CssClass="FieldLabel" Text="Enter new downpayment sources" GroupName="DwnSrc" />
                    <br/><br/>
                    <div class="padding-bottom-1-5em">
                        <table class="primary-table fnma-addendum-table down-payment-table none-hover-table" id="sDUDownPmtSrcs"></table>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="duValidation" ng-controller="duValidationController">
            <header class="header">DU Validation</header>
            <div class="loan-info-padding-left">
                <div class="row padding-bottom-1-5em">
                    <div class="col-xs-12">
                        <table class="primary-table fnma-addendum-table du-validation-table none-hover-table" id="sDuThirdPartyProviders" >
                            <thead class="header">
                                <tr>
                                    <td class="FieldLabel">3rd Party Data Provider Name</td>
                                    <td class="FieldLabel">Reference Number</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="provider in thirdPartyProviders" ng-form="thirdPartyProvider">
                                    <td><div class="select-box select-box-noimage">
                                        <select class="select-du-validation" name="providerNameSelect" ng-model="provider.Name" ng-options="providerName.ProviderName as providerName.DisplayName for providerName in thirdPartyProviderNames" ng-required="!provider.Name && !!provider.ReferenceNumber" ng-blur="onInputChange(thirdPartyProvider.providerNameSelect.$dirty)" ng-model-options="{updateOn: 'click blur'}"></select></div>
                                        <div ng-show="thirdPartyProvider.providerNameSelect.$error.required" class="WarningText">Please choose a provider name.</div>
                                    </td>
                                    <td>
                                        <input name="providerReferenceNumber" type="text" maxlength="50" ng-model="provider.ReferenceNumber" ng-required="!provider.ReferenceNumber && !!provider.Name" ng-blur="onInputChange(thirdPartyProvider.providerReferenceNumber.$dirty)" ng-model-options="{updateOn: 'blur'}"/>
                                        <div ng-show="thirdPartyProvider.providerReferenceNumber.$error.required" class="WarningText">Please enter a reference number.</div>
                                    </td>
                                    <td>
                                <a ng-click="removeItem($index)"><i class="material-icons large-material-icons">&#xE909;</i></a>
                                    </td>
                                </tr>
                                <tr class="none-hover-tr none-border">
                                    <td><a ng-click="addItem()"><i class="material-icons large-material-icons">&#xE146;</i></a></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="FHAAddendumRow" class="section Hidden">
            <header class="header">Additional FHA Loan Information</header>
            <div class="loan-info-padding-left">
                <div class="table">
                    <div>
                        <div class="col-section-act">
                            <label>Section of the Act</label>
                            <ml:ComboBox runat="server" ID="sFHAHousingActSection"></ml:ComboBox>
                        </div>
                        <div>
                            <label>Seller Contribution</label>
                            <ml:MoneyTextBox ID="sFHASellerContribution" runat="server"></ml:MoneyTextBox>
                        </div>
                    </div>
                </div>
                <hr/>
                <asp:Repeater runat="server" ID="AppCounselTypes" OnItemDataBound="AppCounselTypes_OnItemDataBound">
                    <ItemTemplate>
                        <div class="border-bottom-loan-info-fnma padding-bottom-p8">
                            <div class="table table-font-default">
                                <div class="appCounseling">
                                    <div>
                                        <label>
                                            <ml:EncodedLiteral runat="server" ID="aBNm" />
                                            Counsel Type
                                        </label>
                                        <asp:DropDownList runat="server" ID="aBTotalScoreFhtbCounselingT"></asp:DropDownList>
                                    </div>
                                    <div>
                                        <asp:PlaceHolder runat="server" ID="coborrower">
                                                <label>
                                                    <ml:EncodedLiteral runat="server" ID="aCNm" />
                                                    Counsel Type
                                                </label>
                                                <asp:DropDownList runat="server" ID="aCTotalScoreFhtbCounselingT"></asp:DropDownList>
                                        </asp:PlaceHolder>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div id="AdditionalVaInformation" class="section Hidden">
            <header class="header">Additional VA Information</header>
            <div class="loan-info-padding-left">
                <div class="table table-va-info">
                    <div>
                        <div>
                            <label>Entitlement Amount</label>
                            <ml:MoneyTextBox runat="server" preset="money" ID="aVaEntitleAmt"></ml:MoneyTextBox>
                        </div>
                        <div>
                            <label>Maintenance</label>
                            <ml:MoneyTextBox runat="server" preset="money" ID="sVaProMaintenancePmt"></ml:MoneyTextBox>
                        </div>
                        <div>
                            <label>Utilities (including heat)</label>
                            <ml:MoneyTextBox runat="server" preset="money" ID="sVaProUtilityPmt"></ml:MoneyTextBox>
                        </div>
                        <div>
                            <label>Seller Contribution</label>
                            <ml:MoneyTextBox runat="server" preset="money" ID="sFHASellerContributionVa"></ml:MoneyTextBox>
                        </div>
                    </div>
                </div>
                <asp:Repeater runat="server" ID="VaAppTaxes" OnItemDataBound="VaAppTaxes_OnItemDataBound">
                    <ItemTemplate>
                        <hr />
                        <div class="table AppTaxes table-va-info">
                            <div>
                                <div>
                                    <label>
                                        <ml:EncodedLiteral runat="server" ID="aBNm0" />
                                        Federal Tax (Monthly)
                                    </label>
                                    <ml:MoneyTextBox runat="server" preset="money" ID="aVaBFedITax"></ml:MoneyTextBox>
                                </div>
                                <div>
                                    <label>
                                        <ml:EncodedLiteral runat="server" ID="aBNm1" />
                                        State Tax (Monthly)
                                    </label>
                                    <ml:MoneyTextBox runat="server" preset="money" ID="aVaBStateITax"></ml:MoneyTextBox>
                                </div>
                                <div>
                                    <label>
                                        <ml:EncodedLiteral runat="server" ID="aBNm2" />
                                        Social Security Tax (Monthly)
                                    </label>
                                    <ml:MoneyTextBox runat="server" preset="money" ID="aVaBSsnTax"></ml:MoneyTextBox>
                                </div>
                                <div>
                                    <label>
                                        <ml:EncodedLiteral runat="server" ID="aBNm3" />
                                        Other Tax (Monthly)
                                    </label>
                                    <ml:MoneyTextBox runat="server" preset="money" ID="aVaBOITax"></ml:MoneyTextBox>
                                </div>
                            </div>
                            <asp:PlaceHolder runat="server" ID="coborrower">
                                <div>
                                    <div>
                                        <label>
                                            <ml:EncodedLiteral runat="server" ID="aCNm0" />
                                            Federal Tax (Monthly)
                                        </label>
                                        <ml:MoneyTextBox runat="server" preset="money" ID="aVaCFedITax"></ml:MoneyTextBox>
                                    </div>
                                    <div>
                                        <label>
                                            <ml:EncodedLiteral runat="server" ID="aCNm1" />
                                            State Tax (Monthly)
                                        </label>
                                        <ml:MoneyTextBox runat="server" preset="money" ID="aVaCStateITax"></ml:MoneyTextBox>
                                    </div>
                                    <div>
                                        <label>
                                            <ml:EncodedLiteral runat="server" ID="aCNm2" />
                                            Social Security Tax (Monthly)
                                        </label>
                                        <ml:MoneyTextBox runat="server" preset="money" ID="aVaCSsnTax"></ml:MoneyTextBox>
                                    </div>
                                    <div>
                                        <label>
                                            <ml:EncodedLiteral runat="server" ID="aCNm3" />
                                            Other Tax (Monthly)
                                        </label>
                                        <ml:MoneyTextBox runat="server" preset="money" ID="aVaCOITax"></ml:MoneyTextBox>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="section">
            <span class="WarningIconMessage" data-toggle="tooltip">
                <button type="button" class="btn btn-default hidden" id="DesktopUnderwriter_Seamless" runat="server">
                    Submit to DU (Seamless)
                    <img runat="server" src="~/images/warning20x20.png" class="WarningIcon" />
                </button>
            </span>
        </div>
    </div>
</div>
