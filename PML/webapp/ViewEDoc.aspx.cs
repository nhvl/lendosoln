﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using EDocs;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.AntiXss;
using LendersOffice.HttpModule;
using System.IO;
namespace PriceMyLoan.UI.Main
{
    public partial class ViewEDoc : PriceMyLoan.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // 9/25/2011 dd - Disable PageSize Warning on here.
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (item != null)
            {
                item.IsEnabledPageSizeWarning = false;
            }

            //This is to view consumer portal stuff 
            long reqId; 
            if (long.TryParse(RequestHelper.GetSafeQueryString("reqid"), out reqId))
            {
                ViewRequestPdf(reqId);
                return;
            }
            //end view consumer portal doc

            int page = RequestHelper.GetInt("pg", -1);
            string mode = RequestHelper.GetSafeQueryString("mode");

            byte[] buffer = null;

            if (page > 0)
            {

                try
                {
                    Guid docId = RequestHelper.GetGuid("docid", Guid.Empty);
                    int version = RequestHelper.GetInt("version", 1);
                    int rotation = RequestHelper.GetInt("rotation", 0);
                    string key = RequestHelper.GetSafeQueryString("pngkey");
                    bool isThumb = RequestHelper.GetBool("thumb"); 
                    if (string.IsNullOrEmpty(key))
                    {
                        throw CBaseException.GenericException("Invalid PNG Key");
                    }
                    if (mode == "png")
                    {
                        string pngPath;
                        try
                        {
                            pngPath = EDocumentViewer.GetPngPath(key, isThumb);
                            RenderPng(pngPath);
                        }
                        catch (FileNotFoundException exc)
                        {
                            Tools.LogWarning("Could not find png file for key: " + key + ", referrer is: " + Request.UrlReferrer +", is thumb: " + isThumb, exc);
                            throw;
                        }
                        return;
                    }
                    else
                    {
                        buffer = EDocumentViewer.LoadPage(docId, version, page, rotation);
                    }
                }
                catch (AccessDenied exc)
                {
                    DisplayErrorMessage(exc.UserMessage);
                    return;
                }
                catch (InvalidEDocumentVersion exc)
                {
                    DisplayErrorMessage(exc.UserMessage);
                    return;
                }
                catch (InsufficientPdfPermissionException exc)
                {
                    DisplayErrorMessage(exc.UserMessage);
                }
            }
            else
            {
                DisplayEmptyPage();
                return;
            }

            if (buffer != null)
            {
                if (mode == "png" || mode == "pngthumb")
                {
                    RenderPng(buffer);
                }
                else
                {
                    RenderPdf(buffer);
                }
            }
            else
            {
                DisplayErrorMessage("The document has no pages.");
            }
        }

        private void ViewRequestPdf(long key)
        {
            ConsumerActionItem item = new ConsumerActionItem(key);

            byte[] buffer = FileDBTools.ReadData(E_FileDB.EDMS, item.PdfFileDbKey.ToString());
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=request.pdf");
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.OutputStream.Write(buffer, 0, buffer.Length);

            if (Response.IsClientConnected)
            {
                Response.Flush();
                Response.End();
            }
        }
        private void DisplayErrorMessage(string msg)
        {
            m_errorMsg.Text = msg;
            this.ClientScript.RegisterStartupScript(this.GetType(), "DisplayError", "parent.EditEDoc.ForceQuit(" + AspxTools.JsString(msg) + ");", true);
        }
        private void DisplayEmptyPage()
        {
            m_errorMsg.Text = "The document has no pages.";
        }
        private void RenderPdf(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            if (Response.IsClientConnected)
            {
                Response.Flush();
                Response.End();
            }
        }
        private void RenderPng(string filePath)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.TransmitFile(filePath);

            if (Response.IsClientConnected)
            {
                Response.Flush();
                Response.End();
            }
        }
        private void RenderPng(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);

            Response.OutputStream.Write(buffer, 0, buffer.Length);

            if (Response.IsClientConnected)
            {
                Response.Flush();
                Response.End();
            }

        }
    }
}
