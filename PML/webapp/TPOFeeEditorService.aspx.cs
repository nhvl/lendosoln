﻿using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;
using System.Linq;
using System.Collections.Generic;

namespace PriceMyLoan.webapp
{
    public partial class TPOFeeEditorService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }
        protected BrokerDB Broker
        {
            get { return BrokerDB.RetrieveById(GetGuid("BrokerId")); }
        }
        protected Guid loanId
        {
            get { return RequestHelper.GetGuid("loanid", Guid.Empty); }
        }
        
        public List<string> excludePoc;
        public List<string> includeBorr;
        public List<string> excludePaidTo;

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveData":
                    SaveData();
                    break;
                case "CalculateData":
                    CalculateData();
                    break;
                case "LoadEscrowData":
                    LoadEscrow();
                    break;
                case "SaveEscrowData":
                    saveEscrowData();
                    break;
                case "GetFeeTypeProperties":
                    GetFeeTypeProperties();
                    break;
                case "SaveDates":
                    SaveDates();
                    break;
                case "CalcDates":
                    CalcDates();
                    break;
            }
        }

        private void SaveDates()
        {
            CPageData dataLoan = CalcDates();

            try
            {
                dataLoan.Save();
            }
            catch (PageDataAccessDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (PageDataValidationException exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
        }

        private CPageData CalcDates()
        {
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";

            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(sFileVersion);

            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckdInitial");
            dataLoan.sEstCloseD_rep = GetString("sEstCloseDInitial");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1LckdInitial");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1Initial");

            SetResult("sEstCloseDInitial", dataLoan.sEstCloseD_rep);
            SetResult("sEstCloseDLckdInitial", dataLoan.sEstCloseDLckd);
            SetResult("sSchedDueD1Initial", dataLoan.sSchedDueD1_rep);
            SetResult("sSchedDueD1LckdInitial", dataLoan.sSchedDueD1Lckd);

            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
            SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);

            return dataLoan;
        }

        private void SaveData()
        {
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";

            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(sFileVersion);
            BindData(dataLoan, true);

            if (dataLoan.sIsShowsIsPreserveGFEFees && dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.PreserveFeesOnLoan)
            {
                // OPM 141471.
                dataLoan.RecalculateClosingFeeConditions(true); //Force the calc regardless if revision changed.
                dataLoan.sClosingCostAutomationUpdateT = E_sClosingCostAutomationUpdateT.UpdateOnConditionChange;
            }

            try
            {
                dataLoan.Save();
            }
            catch (PageDataAccessDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (PageDataValidationException exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }

            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan);

            /*BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            SqlParameter[] parameters = {
                                            new SqlParameter("@ByPassBgCalcForGfeAsDefault", byPassBgCalcForGfeAsDefault)
                                            , new SqlParameter("@UserId", principal.UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery("SetPassBgCalcForGfeAsDefaultByUserId", 3, parameters);
            */
        }
        private DateTime GetDateTime(string id)
        {
            DateTime dt = DateTime.MinValue;

            string date = GetString(id);
            CDateTime cdateTime = CDateTime.Create(date, null);
            if (cdateTime.IsValid)
            {
                string time = GetString(id + "_Time") + ":" + GetString(id + "_Time_minute") + " " + GetString(id + "_Time_am");
                DateTime.TryParse(cdateTime.ToStringWithDefaultFormatting() + " " + time, out dt);
            }
            else
            {
                // 6/17/2010 dd - Allow user to enter time without date enter.
                string time = GetString(id + "_Time") + ":" + GetString(id + "_Time_minute") + " " + GetString(id + "_Time_am");

                DateTime.TryParse("01/01/1900 " + time, out dt);
            }
            return dt;
        }

        private DateTime? GetDate(string id)
        {
            string date = GetString(id);
            CDateTime cdateTime = CDateTime.Create(date, null);
            if (cdateTime.IsValid)
            {
                return cdateTime.DateTimeForComputation;
            }

            return null;
        }
        private bool BindData(CPageData dataLoan, bool isBindAll)
        {
            bool alertUser = false;


            dataLoan.s800U5F_rep = GetString("s800U5F");
            dataLoan.s800U4F_rep = GetString("s800U4F");
            dataLoan.s800U3F_rep = GetString("s800U3F");
            dataLoan.s800U2F_rep = GetString("s800U2F");
            dataLoan.s800U1F_rep = GetString("s800U1F");
            dataLoan.s1006ProHExp_rep = GetString("s1006ProHExp");
            dataLoan.sProU3Rsrv_rep = GetString("sProU3Rsrv");
            dataLoan.sProU4Rsrv_rep = GetString("sProU4Rsrv");
            dataLoan.sProU4Rsrv_rep = GetString("sProU4Rsrv");
            dataLoan.sU3Tc_rep = GetString("sU3Tc");
            dataLoan.sU2Tc_rep = GetString("sU2Tc");
            dataLoan.sU1Tc_rep = GetString("sU1Tc");
            dataLoan.sU4Sc_rep = GetString("sU4Sc");
            dataLoan.sU3Sc_rep = GetString("sU3Sc");
            dataLoan.sU2Sc_rep = GetString("sU2Sc");
            dataLoan.sU1Sc_rep = GetString("sU1Sc");

            if (isBindAll)
            {
                if (dataLoan.sIsRequireFeesFromDropDown)
                {
                    dataLoan.s800U5FDesc = GetString("s800U5FDescDDL");
                    dataLoan.s800U4FDesc = GetString("s800U4FDescDDL");
                    dataLoan.s800U3FDesc = GetString("s800U3FDescDDL");
                    dataLoan.s800U2FDesc = GetString("s800U2FDescDDL");
                    dataLoan.s800U1FDesc = GetString("s800U1FDescDDL");
                    dataLoan.s900U1PiaDesc = GetString("s900U1PiaDescDDL");
                    dataLoan.s904PiaDesc = GetString("s904PiaDescDDL");
                    dataLoan.s1007ProHExpDesc = GetString("s1007ProHExpDescDDL");
                    dataLoan.s1006ProHExpDesc = GetString("s1006ProHExpDescDDL");
                    dataLoan.sU3RsrvDesc = GetString("sU3RsrvDescDDL");
                    dataLoan.sU4RsrvDesc = GetString("sU4RsrvDescDDL");
                    dataLoan.sU4TcDesc = GetString("sU4TcDescDDL");
                    dataLoan.sU3TcDesc = GetString("sU3TcDescDDL");
                    dataLoan.sU2TcDesc = GetString("sU2TcDescDDL");
                    dataLoan.sU1TcDesc = GetString("sU1TcDescDDL");
                    dataLoan.sU3GovRtcDesc = GetString("sU3GovRtcDescDDL");
                    dataLoan.sU2GovRtcDesc = GetString("sU2GovRtcDescDDL");
                    dataLoan.sU1GovRtcDesc = GetString("sU1GovRtcDescDDL");
                    dataLoan.sU5ScDesc = GetString("sU5ScDescDDL");
                    dataLoan.sU4ScDesc = GetString("sU4ScDescDDL");
                    dataLoan.sU3ScDesc = GetString("sU3ScDescDDL");
                    dataLoan.sU2ScDesc = GetString("sU2ScDescDDL");
                    dataLoan.sU1ScDesc = GetString("sU1ScDescDDL");
                }
                else
                {
                    dataLoan.s800U5FDesc = GetString("s800U5FDesc");
                    dataLoan.s800U4FDesc = GetString("s800U4FDesc");
                    dataLoan.s800U3FDesc = GetString("s800U3FDesc");
                    dataLoan.s800U2FDesc = GetString("s800U2FDesc");
                    dataLoan.s800U1FDesc = GetString("s800U1FDesc");
                    dataLoan.s900U1PiaDesc = GetString("s900U1PiaDesc");
                    dataLoan.s904PiaDesc = GetString("s904PiaDesc");
                    dataLoan.s1007ProHExpDesc = GetString("s1007ProHExpDesc");
                    dataLoan.s1006ProHExpDesc = GetString("s1006ProHExpDesc");
                    dataLoan.sU3RsrvDesc = GetString("sU3RsrvDesc");
                    dataLoan.sU4RsrvDesc = GetString("sU4RsrvDesc");
                    dataLoan.sU4TcDesc = GetString("sU4TcDesc");
                    dataLoan.sU3TcDesc = GetString("sU3TcDesc");
                    dataLoan.sU2TcDesc = GetString("sU2TcDesc");
                    dataLoan.sU1TcDesc = GetString("sU1TcDesc");
                    dataLoan.sU3GovRtcDesc = GetString("sU3GovRtcDesc");
                    dataLoan.sU2GovRtcDesc = GetString("sU2GovRtcDesc");
                    dataLoan.sU1GovRtcDesc = GetString("sU1GovRtcDesc");
                    dataLoan.sU5ScDesc = GetString("sU5ScDesc");
                    dataLoan.sU4ScDesc = GetString("sU4ScDesc");
                    dataLoan.sU3ScDesc = GetString("sU3ScDesc");
                    dataLoan.sU2ScDesc = GetString("sU2ScDesc");
                    dataLoan.sU1ScDesc = GetString("sU1ScDesc");
                }
            }

            dataLoan.sWireF_rep = GetString("sWireF");
            dataLoan.sUwF_rep = GetString("sUwF");
            dataLoan.sProcF_rep = GetString("sProcF");
            dataLoan.sTxServF_rep = GetString("sTxServF");
            dataLoan.sFloodCertificationDeterminationT = (E_FloodCertificationDeterminationT)GetInt("sFloodCertificationDeterminationT");
            dataLoan.sMBrokFMb_rep = GetString("sMBrokFMb");
            dataLoan.sMBrokFPc_rep = GetString("sMBrokFPc");
            dataLoan.sMBrokFBaseT = (E_PercentBaseT)GetInt("sMBrokFBaseT");
            dataLoan.sInspectF_rep = GetString("sInspectF");
            dataLoan.sCrF_rep = GetString("sCrF");
            dataLoan.sApprF_rep = GetString("sApprF");
            dataLoan.sFloodCertificationF_rep = GetString("sFloodCertificationF");
            dataLoan.sLDiscntFMb_rep = GetString("sLDiscntFMb");

            dataLoan.sLDiscntPc_rep = GetString("sLDiscntPc");
            dataLoan.sLDiscntBaseT = (E_PercentBaseT)GetInt("sLDiscntBaseT");

            dataLoan.sLOrigFMb_rep = GetString("sLOrigFMb");
            dataLoan.sLOrigFPc_rep = GetString("sLOrigFPc");
            dataLoan.s900U1Pia_rep = GetString("s900U1Pia");

            dataLoan.s904Pia_rep = GetString("s904Pia");
            dataLoan.sHazInsPiaMon_rep = GetString("sHazInsPiaMon");
            dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
            dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
            dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
            dataLoan.sIPiaDy_rep = GetString("sIPiaDy");
            dataLoan.sIPerDay_rep = GetString("sIPerDay");
            dataLoan.sAggregateAdjRsrv_rep = GetString("sAggregateAdjRsrv");
            dataLoan.sAggregateAdjRsrvLckd = GetBool("sAggregateAdjRsrvLckd");
            dataLoan.s1007ProHExp_rep = GetString("s1007ProHExp");

            /*    if (Broker.EnableAdditionalSection1000CustomFees)
                {
                    dataLoan.sProU3Rsrv_rep = GetString("sProU3Rsrv");
                    if (dataLoan.sU3RsrvMonLckd) { dataLoan.sU3RsrvMon_rep = GetString("sU3RsrvMon"); }
                    if (isBindAll)
                    {
                        dataLoan.sU3RsrvDesc = GetString("sU3RsrvDesc");
                    }
                    dataLoan.sProU4Rsrv_rep = GetString("sProU4Rsrv");
                    if (dataLoan.sU4RsrvMonLckd) { dataLoan.sU4RsrvMon_rep = GetString("sU4RsrvMon"); }
                    if (isBindAll)
                    {
                        dataLoan.sU4RsrvDesc = GetString("sU4RsrvDesc");
                    }
                }
             * */

            dataLoan.sProFloodIns_rep = GetString("sProFloodIns");
            dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
            dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
            dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
            dataLoan.sProSchoolTx_rep = GetString("sProSchoolTx");
            dataLoan.sU4Tc_rep = GetString("sU4Tc");

            dataLoan.sHazInsRsrvMonLckd = GetBool("sHazInsRsrvMonLckd");
            dataLoan.sMInsRsrvMonLckd = GetBool("sMInsRsrvMonLckd");
            dataLoan.sRealETxRsrvMonLckd = GetBool("sRealETxRsrvMonLckd");
            dataLoan.sSchoolTxRsrvMonLckd = GetBool("sSchoolTxRsrvMonLckd");
            dataLoan.sFloodInsRsrvMonLckd = GetBool("sFloodInsRsrvMonLckd");
            dataLoan.s1006RsrvMonLckd = GetBool("s1006RsrvMonLckd");
            dataLoan.s1007RsrvMonLckd = GetBool("s1007RsrvMonLckd");
            dataLoan.sU3RsrvMonLckd = GetBool("sU3RsrvMonLckd");
            dataLoan.sU4RsrvMonLckd = GetBool("sU4RsrvMonLckd");

            dataLoan.sHazInsRsrvMon = GetInt("sHazInsRsrvMon");
            dataLoan.sMInsRsrvMon = GetInt("sMInsRsrvMon");
            dataLoan.sRealETxRsrvMon = GetInt("sRealETxRsrvMon");
            dataLoan.sSchoolTxRsrvMon = GetInt("sSchoolTxRsrvMon");
            dataLoan.sFloodInsRsrvMon = GetInt("sFloodInsRsrvMon");
            dataLoan.s1006RsrvMon = GetInt("s1006RsrvMon");
            dataLoan.s1007RsrvMon = GetInt("s1007RsrvMon");
            dataLoan.sU3RsrvMon = GetInt("sU3RsrvMon");
            dataLoan.sU4RsrvMon = GetInt("sU4RsrvMon");

            dataLoan.sOwnerTitleInsF_rep = GetString("sOwnerTitleInsF");
            dataLoan.sTitleInsF_rep = GetString("sTitleInsF");
            dataLoan.sAttorneyF_rep = GetString("sAttorneyF");
            dataLoan.sNotaryF_rep = GetString("sNotaryF");
            dataLoan.sDocPrepF_rep = GetString("sDocPrepF");
            dataLoan.sEscrowF_rep = GetString("sEscrowF");
            dataLoan.sU3GovRtcMb_rep = GetString("sU3GovRtcMb");
            dataLoan.sU3GovRtcBaseT = (E_PercentBaseT)GetInt("sU3GovRtcBaseT");
            dataLoan.sU3GovRtcPc_rep = GetString("sU3GovRtcPc");

            dataLoan.sU2GovRtcMb_rep = GetString("sU2GovRtcMb");
            dataLoan.sU2GovRtcBaseT = (E_PercentBaseT)GetInt("sU2GovRtcBaseT");
            dataLoan.sU2GovRtcPc_rep = GetString("sU2GovRtcPc");

            dataLoan.sU1GovRtcMb_rep = GetString("sU1GovRtcMb");
            dataLoan.sU1GovRtcBaseT = (E_PercentBaseT)GetInt("sU1GovRtcBaseT");
            dataLoan.sU1GovRtcPc_rep = GetString("sU1GovRtcPc");

            dataLoan.sStateRtcMb_rep = GetString("sStateRtcMb");
            dataLoan.sStateRtcBaseT = (E_PercentBaseT)GetInt("sStateRtcBaseT");
            dataLoan.sStateRtcPc_rep = GetString("sStateRtcPc");
            dataLoan.sCountyRtcMb_rep = GetString("sCountyRtcMb");
            dataLoan.sCountyRtcBaseT = (E_PercentBaseT)GetInt("sCountyRtcBaseT");
            dataLoan.sCountyRtcPc_rep = GetString("sCountyRtcPc");
            dataLoan.sRecFMb_rep = GetString("sRecFMb");
            dataLoan.sRecBaseT = (E_PercentBaseT)GetInt("sRecBaseT");
            dataLoan.sRecFPc_rep = GetString("sRecFPc");

            dataLoan.sRecFLckd = GetBool("sRecFLckd");
            dataLoan.sRecDeed_rep = GetString("sRecDeed");
            dataLoan.sRecMortgage_rep = GetString("sRecMortgage");
            dataLoan.sRecRelease_rep = GetString("sRecRelease");

            dataLoan.sU5Sc_rep = GetString("sU5Sc");

            dataLoan.sPestInspectF_rep = GetString("sPestInspectF");

            dataLoan.sGfeCreditLenderPaidItemT = (E_CreditLenderPaidItemT)GetInt("sGfeCreditLenderPaidItemT");

            dataLoan.sLOrigFProps = RetrieveItemProps("sLOrigF", false, true, true, true, dataLoan.sLOrigFProps);

            int gfeOriginatorPaidBy = GetInt("sGfeOriginatorCompBy", 0);
            if (gfeOriginatorPaidBy == 0)
            {
                dataLoan.sOriginatorCompensationPaymentSourceT = E_sOriginatorCompensationPaymentSourceT.BorrowerPaid;
            }
            else if (gfeOriginatorPaidBy == 3)
            {
                dataLoan.sOriginatorCompensationPaymentSourceT = E_sOriginatorCompensationPaymentSourceT.LenderPaid;
            }
            if (ContainsField("sGfeIsTPOTransaction"))
            {
                dataLoan.sGfeIsTPOTransaction = GetBool("sGfeIsTPOTransaction");
            }

            string sGfeOriginatorCompFPc_rep = GetString("sGfeOriginatorCompFPc");
            string sGfeOriginatorCompFMb_rep = GetString("sGfeOriginatorCompFMb");
            E_PercentBaseT sGfeOriginatorCompFBaseT = (E_PercentBaseT)GetInt("sGfeOriginatorCompFBaseT");

            dataLoan.SetOriginatorCompensation(dataLoan.sOriginatorCompensationPaymentSourceT, sGfeOriginatorCompFPc_rep, sGfeOriginatorCompFBaseT, sGfeOriginatorCompFMb_rep);


            //dataLoan.sLDiscntProps = RetrieveItemProps("sLDiscnt", false, false);
            dataLoan.sGfeOriginatorCompFProps = RetrieveItemProps("sGfeOriginatorComp", dataLoan.sGfeOriginatorCompFProps);

            dataLoan.sGfeLenderCreditFProps = RetrieveItemProps("sGfeLenderCredit", false, false, false, false, dataLoan.sGfeLenderCreditFProps);
            dataLoan.sGfeDiscountPointFProps = RetrieveItemProps("sGfeDiscountPoint", false, false, true, true, dataLoan.sGfeDiscountPointFProps);
            dataLoan.sApprFProps = RetrieveItemProps("sAppr", true, true, false, false, dataLoan.sApprFProps);
            dataLoan.sCrFProps = RetrieveItemProps("sCr", true, true, false, false, dataLoan.sCrFProps);
            dataLoan.sInspectFProps = RetrieveItemProps("sInspect", true, true, false, false, dataLoan.sInspectFProps);
            dataLoan.sMBrokFProps = RetrieveItemProps("sMBrok", true, true, false, false, dataLoan.sMBrokFProps);
            dataLoan.sTxServFProps = RetrieveItemProps("sTxServ", true, true, false, false, dataLoan.sTxServFProps);
            dataLoan.sFloodCertificationFProps = RetrieveItemProps("sFloodCertification", true, true, false, false, dataLoan.sFloodCertificationFProps);
            dataLoan.sProcFProps = RetrieveItemProps("sProc", true, true, false, false, dataLoan.sProcFProps);
            dataLoan.sUwFProps = RetrieveItemProps("sUw", true, true, false, false, dataLoan.sUwFProps);
            dataLoan.sWireFProps = RetrieveItemProps("sWire", true, true, false, false, dataLoan.sWireFProps);
            dataLoan.s800U1FProps = RetrieveItemProps("s800U1", true, true, false, false, dataLoan.s800U1FProps);
            dataLoan.s800U2FProps = RetrieveItemProps("s800U2", true, true, false, false, dataLoan.s800U2FProps);
            dataLoan.s800U3FProps = RetrieveItemProps("s800U3", true, true, false, false, dataLoan.s800U3FProps);
            dataLoan.s800U4FProps = RetrieveItemProps("s800U4", true, true, false, false, dataLoan.s800U4FProps);
            dataLoan.s800U5FProps = RetrieveItemProps("s800U5", true, true, false, false, dataLoan.s800U5FProps);
            dataLoan.sIPiaProps = RetrieveItemProps("sI", true, false, false, false, dataLoan.sIPiaProps);
            //dataLoan.sMipPiaProps = RetrieveItemProps902905("sMip", dataLoan.sMipPiaProps);
            dataLoan.sMipPiaProps = RetrieveItemProps("sMip", true, false, false, false, dataLoan.sMipPiaProps);
            dataLoan.sHazInsPiaProps = RetrieveItemProps("sHazIns", true, false, false, false, dataLoan.sHazInsPiaProps);
            dataLoan.s904PiaProps = RetrieveItemProps("s904", true, false, false, false, dataLoan.s904PiaProps);
            //dataLoan.sVaFfProps = RetrieveItemProps902905("sVaF", dataLoan.sVaFfProps);
            dataLoan.sVaFfProps = RetrieveItemProps("sVa", true, false, false, false, dataLoan.sVaFfProps);
            dataLoan.s900U1PiaProps = RetrieveItemProps("s900U1", true, false, false, false, dataLoan.s900U1PiaProps);
            dataLoan.sHazInsRsrvProps = RetrieveItemProps("sHazInsRsrv", true, false, false, false, dataLoan.sHazInsRsrvProps);
            dataLoan.sMInsRsrvProps = RetrieveItemProps("sMInsRsrv", true, false, false, false, dataLoan.sMInsRsrvProps);
            dataLoan.sSchoolTxRsrvProps = RetrieveItemProps("sSchoolTxRsrv", true, false, false, false, dataLoan.sSchoolTxRsrvProps);
            dataLoan.sRealETxRsrvProps = RetrieveItemProps("sRealETxRsrv", true, false, false, false, dataLoan.sRealETxRsrvProps);
            dataLoan.sFloodInsRsrvProps = RetrieveItemProps("sFloodInsRsrv", true, false, false, false, dataLoan.sFloodInsRsrvProps);
            dataLoan.s1006RsrvProps = RetrieveItemProps("s1006Rsrv", true, false, false, false, dataLoan.s1006RsrvProps);
            dataLoan.s1007RsrvProps = RetrieveItemProps("s1007Rsrv", true, false, false, false, dataLoan.s1007RsrvProps);
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sU3RsrvProps = RetrieveItemProps("sU3Rsrv", true, false, false, false, dataLoan.sU3RsrvProps);
                dataLoan.sU4RsrvProps = RetrieveItemProps("sU4Rsrv", true, false, false, false, dataLoan.sU4RsrvProps);
            }
            dataLoan.sAggregateAdjRsrvProps = RetrieveItemProps("sAggregateAdjRsrv", true, false, false, false, dataLoan.sAggregateAdjRsrvProps);

            dataLoan.sHazInsRsrvEscrowedTri = getEscrowedTri("sHazInsRsrvEscrowedTri");
            dataLoan.sMInsRsrvEscrowedTri = getEscrowedTri("sMInsRsrvEscrowedTri");
            dataLoan.sRealETxRsrvEscrowedTri = getEscrowedTri("sRealETxRsrvEscrowedTri");
            dataLoan.sSchoolTxRsrvEscrowedTri = getEscrowedTri("sSchoolTxRsrvEscrowedTri");
            dataLoan.sFloodInsRsrvEscrowedTri = getEscrowedTri("sFloodInsRsrvEscrowedTri");
            dataLoan.s1006RsrvEscrowedTri = getEscrowedTri("s1006RsrvEscrowedTri");
            dataLoan.s1007RsrvEscrowedTri = getEscrowedTri("s1007RsrvEscrowedTri");
            dataLoan.sU3RsrvEscrowedTri = getEscrowedTri("sU3RsrvEscrowedTri");
            dataLoan.sU4RsrvEscrowedTri = getEscrowedTri("sU4RsrvEscrowedTri");


            #region ( 1100 Props )
            dataLoan.sEscrowFProps = RetrieveItemProps("sEscrow", true, true, false, false, true, dataLoan.sEscrowFProps);
            dataLoan.sOwnerTitleInsProps = RetrieveItemProps("sOwnerTitleIns", true, true, false, false, true, dataLoan.sOwnerTitleInsProps);
            dataLoan.sTitleInsFProps = RetrieveItemProps("sTitleIns", true, true, false, false, true, dataLoan.sTitleInsFProps);
            dataLoan.sDocPrepFProps = RetrieveItemProps("sDocPrep", true, true, false, false, true, dataLoan.sDocPrepFProps);
            dataLoan.sNotaryFProps = RetrieveItemProps("sNotary", true, true, false, false, true, dataLoan.sNotaryFProps);
            dataLoan.sAttorneyFProps = RetrieveItemProps("sAttorney", true, true, false, false, true, dataLoan.sAttorneyFProps);
            dataLoan.sU1TcProps = RetrieveItemProps("sU1Tc", true, true, false, false, true, dataLoan.sU1TcProps);
            dataLoan.sU2TcProps = RetrieveItemProps("sU2Tc", true, true, false, false, true, dataLoan.sU2TcProps);
            dataLoan.sU3TcProps = RetrieveItemProps("sU3Tc", true, true, false, false, true, dataLoan.sU3TcProps);
            dataLoan.sU4TcProps = RetrieveItemProps("sU4Tc", true, true, false, false, true, dataLoan.sU4TcProps);
            #endregion
            #region ( 1200 props )
            dataLoan.sRecFProps = RetrieveItemProps("sRecF", true, false, false, false, dataLoan.sRecFProps);
            dataLoan.sCountyRtcProps = RetrieveItemProps("sCountyRtc", true, false, false, false, dataLoan.sCountyRtcProps);
            dataLoan.sStateRtcProps = RetrieveItemProps("sStateRtc", true, false, false, false, dataLoan.sStateRtcProps);
            dataLoan.sU1GovRtcProps = RetrieveItemProps("sU1GovRtc", true, false, false, false, dataLoan.sU1GovRtcProps);
            dataLoan.sU2GovRtcProps = RetrieveItemProps("sU2GovRtc", true, false, false, false, dataLoan.sU2GovRtcProps);
            dataLoan.sU3GovRtcProps = RetrieveItemProps("sU3GovRtc", true, false, false, false, dataLoan.sU3GovRtcProps);
            #endregion
            #region ( 1300 props )
            dataLoan.sPestInspectFProps = RetrieveItemProps("sPestInspect", true, true, false, false, true, dataLoan.sPestInspectFProps);
            dataLoan.sU1ScProps = RetrieveItemProps("sU1Sc", true, true, false, false, true, dataLoan.sU1ScProps);
            dataLoan.sU2ScProps = RetrieveItemProps("sU2Sc", true, true, false, false, true, dataLoan.sU2ScProps);
            dataLoan.sU3ScProps = RetrieveItemProps("sU3Sc", true, true, false, false, true, dataLoan.sU3ScProps);
            dataLoan.sU4ScProps = RetrieveItemProps("sU4Sc", true, true, false, false, true, dataLoan.sU4ScProps);
            dataLoan.sU5ScProps = RetrieveItemProps("sU5Sc", true, true, false, false, true, dataLoan.sU5ScProps);
            #endregion



            dataLoan.sTitleInsFTable = GetString("sTitleInsPaidTo");
            dataLoan.sEscrowFTable = GetString("sEscrowPaidTo");
            dataLoan.sStateRtcDesc = GetString("sStateRtcPaidTo");
            dataLoan.sCountyRtcDesc = GetString("sCountyRtcPaidTo");
            dataLoan.sRecFDesc = GetString("sRecFPaidTo");

            dataLoan.sApprFPaidTo = GetString("sApprPaidTo");
            dataLoan.sCrFPaidTo = GetString("sCrPaidTo");
            dataLoan.sTxServFPaidTo = GetString("sTxServPaidTo");
            dataLoan.sFloodCertificationFPaidTo = GetString("sFloodCertificationPaidTo");
            dataLoan.sInspectFPaidTo = GetString("sInspectPaidTo");
            dataLoan.sProcFPaidTo = GetString("sProcPaidTo");
            dataLoan.sUwFPaidTo = GetString("sUwPaidTo");
            dataLoan.sWireFPaidTo = GetString("sWirePaidTo");
            dataLoan.s800U1FPaidTo = GetString("s800U1PaidTo");
            dataLoan.s800U2FPaidTo = GetString("s800U2PaidTo");
            dataLoan.s800U3FPaidTo = GetString("s800U3PaidTo");
            dataLoan.s800U4FPaidTo = GetString("s800U4PaidTo");
            dataLoan.s800U5FPaidTo = GetString("s800U5PaidTo");
            dataLoan.sOwnerTitleInsPaidTo = GetString("sOwnerTitleInsPaidTo");
            dataLoan.sDocPrepFPaidTo = GetString("sDocPrepPaidTo");
            dataLoan.sNotaryFPaidTo = GetString("sNotaryPaidTo");
            dataLoan.sAttorneyFPaidTo = GetString("sAttorneyPaidTo");
            dataLoan.sU1TcPaidTo = GetString("sU1TcPaidTo");
            dataLoan.sU2TcPaidTo = GetString("sU2TcPaidTo");
            dataLoan.sU3TcPaidTo = GetString("sU3TcPaidTo");
            dataLoan.sU4TcPaidTo = GetString("sU4TcPaidTo");
            dataLoan.sU1GovRtcPaidTo = GetString("sU1GovRtcPaidTo");
            dataLoan.sU2GovRtcPaidTo = GetString("sU2GovRtcPaidTo");
            dataLoan.sU3GovRtcPaidTo = GetString("sU3GovRtcPaidTo");
            dataLoan.sPestInspectPaidTo = GetString("sPestInspectPaidTo");
            dataLoan.sU1ScPaidTo = GetString("sU1ScPaidTo");
            dataLoan.sU2ScPaidTo = GetString("sU2ScPaidTo");
            dataLoan.sU3ScPaidTo = GetString("sU3ScPaidTo");
            dataLoan.sU4ScPaidTo = GetString("sU4ScPaidTo");
            dataLoan.sU5ScPaidTo = GetString("sU5ScPaidTo");
            dataLoan.sHazInsPiaPaidTo = GetString("sHazInsPaidTo");
            dataLoan.sMipPiaPaidTo = GetString("sMipPaidTo");
            dataLoan.sVaFfPaidTo = GetString("sVaPaidTo");

            dataLoan.s800U1FGfeSection = (E_GfeSectionT)GetInt("s800U1Page2");
            dataLoan.s800U2FGfeSection = (E_GfeSectionT)GetInt("s800U2Page2");
            dataLoan.s800U3FGfeSection = (E_GfeSectionT)GetInt("s800U3Page2");
            dataLoan.s800U4FGfeSection = (E_GfeSectionT)GetInt("s800U4Page2");
            dataLoan.s800U5FGfeSection = (E_GfeSectionT)GetInt("s800U5Page2");
            dataLoan.sEscrowFGfeSection = (E_GfeSectionT)GetInt("sEscrowPage2");
            //dataLoan.sTitleInsFGfeSection = (E_GfeSectionT)GetInt("sTitleInsPage2");
            dataLoan.sDocPrepFGfeSection = (E_GfeSectionT)GetInt("sDocPrepPage2");
            dataLoan.sNotaryFGfeSection = (E_GfeSectionT)GetInt("sNotaryPage2");
            dataLoan.sAttorneyFGfeSection = (E_GfeSectionT)GetInt("sAttorneyPage2");
            dataLoan.sU1TcGfeSection = (E_GfeSectionT)GetInt("sU1TcPage2");
            dataLoan.sU2TcGfeSection = (E_GfeSectionT)GetInt("sU2TcPage2");
            dataLoan.sU3TcGfeSection = (E_GfeSectionT)GetInt("sU3TcPage2");
            dataLoan.sU4TcGfeSection = (E_GfeSectionT)GetInt("sU4TcPage2");
            dataLoan.sU1GovRtcGfeSection = (E_GfeSectionT)GetInt("sU1GovRtcPage2");
            dataLoan.sU2GovRtcGfeSection = (E_GfeSectionT)GetInt("sU2GovRtcPage2");
            dataLoan.sU3GovRtcGfeSection = (E_GfeSectionT)GetInt("sU3GovRtcPage2");
            dataLoan.sU1ScGfeSection = (E_GfeSectionT)GetInt("sU1ScPage2");
            dataLoan.sU2ScGfeSection = (E_GfeSectionT)GetInt("sU2ScPage2");
            dataLoan.sU3ScGfeSection = (E_GfeSectionT)GetInt("sU3ScPage2");
            dataLoan.sU4ScGfeSection = (E_GfeSectionT)GetInt("sU4ScPage2");
            dataLoan.sU5ScGfeSection = (E_GfeSectionT)GetInt("sU5ScPage2");
            dataLoan.s904PiaGfeSection = (E_GfeSectionT)GetInt("s904Page2");
            dataLoan.s900U1PiaGfeSection = (E_GfeSectionT)GetInt("s900U1Page2");

            return alertUser;
        }

        private E_TriState getEscrowedTri(string key)
        {
            if (GetBool(key))
            {
                return E_TriState.Yes;
            }
            else
            {
                return E_TriState.No;
            }
        }

        /// <summary>
        /// For performance reason, I only return calculated data. Therefore
        /// DO NOT use this method to return complete GFE data. dd 7/25/2003
        /// </summary>
        /// <param name="dataLoan"></param>
        /// <returns></returns>
        private void LoadData(CPageData dataLoan)
        {
            excludePoc = new List<string>();
            excludePoc.Add("sLOrigF");
            excludePoc.Add("sGfeOriginatorComp");
            excludePoc.Add("sGfeLenderCredit");
            excludePoc.Add("sGfeDiscountPoint");

            includeBorr = new List<string>();
            includeBorr.Add("sEscrow");
            includeBorr.Add("sOwnerTitleIns");
            includeBorr.Add("sTitleIns");
            includeBorr.Add("sDocPrep");
            includeBorr.Add("sNotary");
            includeBorr.Add("sAttorney");
            includeBorr.Add("sU1Tc");
            includeBorr.Add("sU2Tc");
            includeBorr.Add("sU3Tc");
            includeBorr.Add("sU4Tc");
            includeBorr.Add("sPestInspect");
            includeBorr.Add("sU1Sc");
            includeBorr.Add("sU2Sc");
            includeBorr.Add("sU3Sc");
            includeBorr.Add("sU4Sc");
            includeBorr.Add("sU5Sc");

            excludePaidTo = new List<string>();
            excludePaidTo.Add("sHazInsRsrv");
            excludePaidTo.Add("sMInsRsrv");
            excludePaidTo.Add("sRealETxRsrv");
            excludePaidTo.Add("sSchoolTxRsrv");
            excludePaidTo.Add("sFloodInsRsrv");
            excludePaidTo.Add("sAggregateAdjRsrv");
            excludePaidTo.Add("s1006Rsrv");
            excludePaidTo.Add("s1007Rsrv");
            excludePaidTo.Add("sU3Rsrv");
            excludePaidTo.Add("sU4Rsrv");



            SetResult("s800U5F", dataLoan.s800U5F_rep);

            SetResult("s800U4F", dataLoan.s800U4F_rep);

            SetResult("s800U3F", dataLoan.s800U3F_rep);

            SetResult("s800U2F", dataLoan.s800U2F_rep);

            SetResult("s800U1F", dataLoan.s800U1F_rep);


            SetResult("sWireF", dataLoan.sWireF_rep);

            SetResult("sUwF", dataLoan.sUwF_rep);

            SetResult("sProcF", dataLoan.sProcF_rep);

            SetResult("sTxServF", dataLoan.sTxServF_rep);

            SetResult("sFloodCertificationF", dataLoan.sFloodCertificationF_rep);
            SetResult("sFloodCertificationDeterminationT", dataLoan.sFloodCertificationDeterminationT);

            SetResult("sMBrokF", dataLoan.sMBrokF_rep);

            SetResult("sMBrokFMb", dataLoan.sMBrokFMb_rep);

            SetResult("sMBrokFPc", dataLoan.sMBrokFPc_rep);
            SetResult("this.sMBrokFBaseT", dataLoan.sMBrokFBaseT);

            SetResult("sInspectF", dataLoan.sInspectF_rep);

            SetResult("sCrF", dataLoan.sCrF_rep);

            SetResult("sApprF", dataLoan.sApprF_rep);

            SetResult("sLOrigF", dataLoan.sLOrigF_rep);

            SetResult("sLOrigFMb", dataLoan.sLOrigFMb_rep);

            SetResult("sLOrigFPc", dataLoan.sLOrigFPc_rep);

            SetResult("s900U1Pia", dataLoan.s900U1Pia_rep);

            SetResult("sVaFf", dataLoan.sVaFf_rep);
            SetResult("s904Pia", dataLoan.s904Pia_rep);

            SetResult("sHazInsPia", dataLoan.sHazInsPia_rep);

            SetResult("sHazInsPiaMon", dataLoan.sHazInsPiaMon_rep);

            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);

            SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);

            SetResult("sMipPia", dataLoan.sMipPia_rep);

            SetResult("sIPia", dataLoan.sIPia_rep);

            SetResult("sIPiaDy", dataLoan.sIPiaDy_rep);

            SetResult("sIPerDay", dataLoan.sIPerDay_rep);

            SetResult("sAggregateAdjRsrv", dataLoan.sAggregateAdjRsrv_rep);
            SetResult("sAggregateAdjRsrvLckd", dataLoan.sAggregateAdjRsrvLckd);
            SetResult("s1007Rsrv", dataLoan.s1007Rsrv_rep);

            SetResult("s1007ProHExp", dataLoan.s1007ProHExp_rep);

            SetResult("s1007RsrvMon", dataLoan.s1007RsrvMon_rep);

            SetResult("s1006Rsrv", dataLoan.s1006Rsrv_rep);

            SetResult("s1006ProHExp", dataLoan.s1006ProHExp_rep);

            SetResult("s1006RsrvMon", dataLoan.s1006RsrvMon_rep);



            SetResult("sU3Rsrv", dataLoan.sU3Rsrv_rep);

            SetResult("sU3ProHExp", dataLoan.sProU3Rsrv_rep);

            SetResult("sU3RsrvMon", dataLoan.sU3RsrvMon_rep);


            SetResult("sU4Rsrv", dataLoan.sU4Rsrv_rep);

            SetResult("sU4ProHExp", dataLoan.sProU4Rsrv_rep);

            SetResult("sU4RsrvMon", dataLoan.sU4RsrvMon_rep);



            SetResult("sFloodInsRsrv", dataLoan.sFloodInsRsrv_rep);

            SetResult("sProFloodIns", dataLoan.sProFloodIns_rep);

            SetResult("sFloodInsRsrvMon", dataLoan.sFloodInsRsrvMon_rep);

            SetResult("sRealETxRsrv", dataLoan.sRealETxRsrv_rep);

            SetResult("sProRealETxT", dataLoan.sProRealETxT);

            SetResult("sProRealETxMb", dataLoan.sProRealETxMb_rep);

            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);

            SetResult("sRealETxRsrvMon", dataLoan.sRealETxRsrvMon_rep);

            SetResult("sSchoolTxRsrv", dataLoan.sSchoolTxRsrv_rep);

            SetResult("sProSchoolTx", dataLoan.sProSchoolTx_rep);
            SetResult("sSchoolTxRsrvMon", dataLoan.sSchoolTxRsrvMon_rep);

            SetResult("sMInsRsrv", dataLoan.sMInsRsrv_rep);

            SetResult("sProMIns", dataLoan.sProMIns_rep);

            SetResult("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep);

            SetResult("sHazInsRsrv", dataLoan.sHazInsRsrv_rep);

            SetResult("sProHazIns", dataLoan.sProHazIns_rep);

            SetResult("sHazInsRsrvMon", dataLoan.sHazInsRsrvMon_rep);


            SetResult("sU4Tc", dataLoan.sU4Tc_rep);

            SetResult("sU3Tc", dataLoan.sU3Tc_rep);

            SetResult("sU2Tc", dataLoan.sU2Tc_rep);

            SetResult("sU1Tc", dataLoan.sU1Tc_rep);

            SetResult("sTitleInsF", dataLoan.sTitleInsF_rep);

            SetResult("sAttorneyF", dataLoan.sAttorneyF_rep);

            SetResult("sNotaryF", dataLoan.sNotaryF_rep);

            SetResult("sDocPrepF", dataLoan.sDocPrepF_rep);

            SetResult("sEscrowF", dataLoan.sEscrowF_rep);

            SetResult("sOwnerTitleInsF", dataLoan.sOwnerTitleInsF_rep);

            SetResult("sU3GovRtc", dataLoan.sU3GovRtc_rep);

            SetResult("sU3GovRtcMb", dataLoan.sU3GovRtcMb_rep);

            SetResult("sU3GovRtcBaseT", dataLoan.sU3GovRtcBaseT);
            SetResult("sU3GovRtcPc", dataLoan.sU3GovRtcPc_rep);

            SetResult("sU2GovRtc", dataLoan.sU2GovRtc_rep);

            SetResult("sU2GovRtcMb", dataLoan.sU2GovRtcMb_rep);

            SetResult("sU2GovRtcBaseT", dataLoan.sU2GovRtcBaseT);
            SetResult("sU2GovRtcPc", dataLoan.sU2GovRtcPc_rep);

            SetResult("sU1GovRtc", dataLoan.sU1GovRtc_rep);

            SetResult("sU1GovRtcMb", dataLoan.sU1GovRtcMb_rep);

            SetResult("sU1GovRtcBaseT", dataLoan.sU1GovRtcBaseT);
            SetResult("sU1GovRtcPc", dataLoan.sU1GovRtcPc_rep);

            SetResult("sStateRtc", dataLoan.sStateRtc_rep);

            SetResult("sStateRtcMb", dataLoan.sStateRtcMb_rep);

            SetResult("sStateRtcBaseT", dataLoan.sStateRtcBaseT);
            SetResult("sStateRtcPc", dataLoan.sStateRtcPc_rep);

            SetResult("sCountyRtc", dataLoan.sCountyRtc_rep);

            SetResult("sCountyRtcMb", dataLoan.sCountyRtcMb_rep);

            SetResult("sCountyRtcBaseT", dataLoan.sCountyRtcBaseT);
            SetResult("sCountyRtcPc", dataLoan.sCountyRtcPc_rep);

            SetResult("sRecF", dataLoan.sRecF_rep);

            SetResult("sRecFMb", dataLoan.sRecFMb_rep);

            SetResult("sRecBaseT", dataLoan.sRecBaseT);
            SetResult("sRecFPc", dataLoan.sRecFPc_rep);

            SetResult("sRecFLckd", dataLoan.sRecFLckd);
            SetResult("sRecDeed", dataLoan.sRecDeed_rep);
            SetResult("sRecMortgage", dataLoan.sRecMortgage_rep);
            SetResult("sRecRelease", dataLoan.sRecRelease_rep);

            SetResult("sU5Sc", dataLoan.sU5Sc_rep);

            SetResult("sU4Sc", dataLoan.sU4Sc_rep);

            SetResult("sU3Sc", dataLoan.sU3Sc_rep);

            SetResult("sU2Sc", dataLoan.sU2Sc_rep);

            SetResult("sU1Sc", dataLoan.sU1Sc_rep);

            SetResult("sPestInspectF", dataLoan.sPestInspectF_rep);

            SetResult("sLDiscntBaseT", dataLoan.sLDiscntBaseT);

            SetResult("sGfeLenderCreditAPR", LosConvert.GfeItemProps_Apr(dataLoan.sGfeLenderCreditFProps));



            if (dataLoan.sIsRequireFeesFromDropDown)
            {
                SetResult("s800U5FDescDDL", dataLoan.s800U5FDesc);
                SetResult("s800U4FDescDDL", dataLoan.s800U4FDesc);
                SetResult("s800U3FDescDDL", dataLoan.s800U3FDesc);
                SetResult("s800U2FDescDDL", dataLoan.s800U2FDesc);
                SetResult("s800U1FDescDDL", dataLoan.s800U1FDesc);
                SetResult("s900U1PiaDescDDL", dataLoan.s900U1PiaDesc);
                SetResult("s904PiaDescDDL", dataLoan.s904PiaDesc);
                SetResult("s1007ProHExpDescDDL", dataLoan.s1007ProHExpDesc);
                SetResult("s1006ProHExpDescDDL", dataLoan.s1006ProHExpDesc);
                SetResult("sU3RsrvDescDDL", dataLoan.sU3RsrvDesc);
                SetResult("sU3RsrvDescDDL", dataLoan.sU4RsrvDesc);
                SetResult("sU4TcDescDDL", dataLoan.sU4TcDesc);
                SetResult("sU3TcDescDDL", dataLoan.sU3TcDesc);
                SetResult("sU2TcDescDDL", dataLoan.sU2TcDesc);
                SetResult("sU1TcDescDDL", dataLoan.sU1TcDesc);
                SetResult("sU3GovRtcDescDDL", dataLoan.sU3GovRtcDesc);
                SetResult("sU2GovRtcDescDDL", dataLoan.sU2GovRtcDesc);
                SetResult("sU1GovRtcDescDDL", dataLoan.sU1GovRtcDesc);
                SetResult("sU5ScDescDDL", dataLoan.sU5ScDesc);
                SetResult("sU4ScDescDDL", dataLoan.sU4ScDesc);
                SetResult("sU3ScDescDDL", dataLoan.sU3ScDesc);
                SetResult("sU2ScDescDDL", dataLoan.sU2ScDesc);
                SetResult("sU1ScDescDDL", dataLoan.sU1ScDesc);

            }
            else
            {

                SetResult("s800U5FDesc", dataLoan.s800U5FDesc);
                SetResult("s800U4FDesc", dataLoan.s800U4FDesc);
                SetResult("s800U3FDesc", dataLoan.s800U3FDesc);
                SetResult("s800U2FDesc", dataLoan.s800U2FDesc);
                SetResult("s800U1FDesc", dataLoan.s800U1FDesc);
                SetResult("s900U1PiaDesc", dataLoan.s900U1PiaDesc);
                SetResult("s904PiaDesc", dataLoan.s904PiaDesc);
                SetResult("s1007ProHExpDesc", dataLoan.s1007ProHExpDesc);
                SetResult("s1006ProHExpDesc", dataLoan.s1006ProHExpDesc);
                SetResult("sU3RsrvDesc", dataLoan.sU3RsrvDesc);
                SetResult("sU4RsrvDesc", dataLoan.sU4RsrvDesc);
                SetResult("sU4TcDesc", dataLoan.sU4TcDesc);
                SetResult("sU3TcDesc", dataLoan.sU3TcDesc);
                SetResult("sU2TcDesc", dataLoan.sU2TcDesc);
                SetResult("sU1TcDesc", dataLoan.sU1TcDesc);
                SetResult("sU3GovRtcDesc", dataLoan.sU3GovRtcDesc);
                SetResult("sU2GovRtcDesc", dataLoan.sU2GovRtcDesc);
                SetResult("sU1GovRtcDesc", dataLoan.sU1GovRtcDesc);
                SetResult("sU5ScDesc", dataLoan.sU5ScDesc);
                SetResult("sU4ScDesc", dataLoan.sU4ScDesc);
                SetResult("sU3ScDesc", dataLoan.sU3ScDesc);
                SetResult("sU2ScDesc", dataLoan.sU2ScDesc);
                SetResult("sU1ScDesc", dataLoan.sU1ScDesc);

            }

            if (dataLoan.s1007ProHExpDesc != string.Empty)
            {
                SetResult("s1007ProHExpDescSpan", dataLoan.s1007ProHExpDesc);
            }
            else
            {
                SetResult("s1007ProHExpDescSpan", "User Defined 2");
            }

            if (dataLoan.s1006ProHExpDesc != string.Empty)
            {
                SetResult("s1006ProHExpDescSpan", dataLoan.s1006ProHExpDesc);
            }
            else
            {
                SetResult("s1006ProHExpDescSpan", "User Defined 1");
            }

            if (dataLoan.sU3RsrvDesc != string.Empty)
            {
                SetResult("sU3RsrvDescSpan", dataLoan.sU3RsrvDesc);
            }
            else
            {
                SetResult("sU3RsrvDescSpan", "User Defined 3");
            }

            if (dataLoan.sU4RsrvDesc != string.Empty)
            {
                SetResult("sU4RsrvDescSpan", dataLoan.sU4RsrvDesc);
            }
            else
            {
                SetResult("sU4RsrvDescSpan", "User Defined 4");
            }

            BindProperties("sGfeDiscountPoint", dataLoan.sGfeDiscountPointFProps, "", E_GfeSectionT.LeaveBlank, dataLoan);

            BindProperties("sLOrigF", dataLoan.sLOrigFProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sGfeOriginatorComp", dataLoan.sGfeOriginatorCompFProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sAppr", dataLoan.sApprFProps, dataLoan.sApprFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sCr", dataLoan.sCrFProps, dataLoan.sCrFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sInspect", dataLoan.sInspectFProps, dataLoan.sInspectFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sMBrok", dataLoan.sMBrokFProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sTxServ", dataLoan.sTxServFProps, dataLoan.sTxServFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sFloodCertification", dataLoan.sFloodCertificationFProps, dataLoan.sFloodCertificationFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sProc", dataLoan.sProcFProps, dataLoan.sProcFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sUw", dataLoan.sUwFProps, dataLoan.sUwFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sWire", dataLoan.sWireFProps, dataLoan.sWireFPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("s800U1", dataLoan.s800U1FProps, dataLoan.s800U1FPaidTo, dataLoan.s800U1FGfeSection, dataLoan);
            BindProperties("s800U2", dataLoan.s800U2FProps, dataLoan.s800U2FPaidTo, dataLoan.s800U2FGfeSection, dataLoan);
            BindProperties("s800U3", dataLoan.s800U3FProps, dataLoan.s800U3FPaidTo, dataLoan.s800U3FGfeSection, dataLoan);
            BindProperties("s800U4", dataLoan.s800U4FProps, dataLoan.s800U4FPaidTo, dataLoan.s800U4FGfeSection, dataLoan);
            BindProperties("s800U5", dataLoan.s800U5FProps, dataLoan.s800U5FPaidTo, dataLoan.s800U5FGfeSection, dataLoan);

            BindProperties("sI", dataLoan.sIPiaProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sMip", dataLoan.sMipPiaProps, dataLoan.sMipPiaPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sHazIns", dataLoan.sHazInsPiaProps, dataLoan.sHazInsPiaPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("s904", dataLoan.s904PiaProps, "", dataLoan.s904PiaGfeSection, dataLoan);
            BindProperties("sVa", dataLoan.sVaFfProps, dataLoan.sVaFfPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("s900U1", dataLoan.s900U1PiaProps, "", dataLoan.s900U1PiaGfeSection, dataLoan);

            BindProperties("sHazInsRsrv", dataLoan.sHazInsRsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sMInsRsrv", dataLoan.sMInsRsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sSchoolTxRsrv", dataLoan.sSchoolTxRsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sRealETxRsrv", dataLoan.sRealETxRsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sFloodInsRsrv", dataLoan.sFloodInsRsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("s1006Rsrv", dataLoan.s1006RsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("s1007Rsrv", dataLoan.s1007RsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sU3Rsrv", dataLoan.sU3RsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sU4Rsrv", dataLoan.sU4RsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sAggregateAdjRsrv", dataLoan.sAggregateAdjRsrvProps, "", E_GfeSectionT.LeaveBlank, dataLoan);

            BindProperties("sEscrow", dataLoan.sEscrowFProps, dataLoan.sEscrowFTable, dataLoan.sEscrowFGfeSection, dataLoan);
            BindProperties("sOwnerTitleIns", dataLoan.sOwnerTitleInsProps, dataLoan.sOwnerTitleInsPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sDocPrep", dataLoan.sDocPrepFProps, dataLoan.sDocPrepFPaidTo, dataLoan.sDocPrepFGfeSection, dataLoan);
            BindProperties("sNotary", dataLoan.sNotaryFProps, dataLoan.sNotaryFPaidTo, dataLoan.sNotaryFGfeSection, dataLoan);
            BindProperties("sAttorney", dataLoan.sAttorneyFProps, dataLoan.sAttorneyFPaidTo, dataLoan.sAttorneyFGfeSection, dataLoan);
            BindProperties("sTitleIns", dataLoan.sTitleInsFProps, dataLoan.sTitleInsFTable, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sU1Tc", dataLoan.sU1TcProps, dataLoan.sU1TcPaidTo, dataLoan.sU1TcGfeSection, dataLoan);
            BindProperties("sU2Tc", dataLoan.sU2TcProps, dataLoan.sU2TcPaidTo, dataLoan.sU2TcGfeSection, dataLoan);
            BindProperties("sU3Tc", dataLoan.sU3TcProps, dataLoan.sU3TcPaidTo, dataLoan.sU3TcGfeSection, dataLoan);
            BindProperties("sU4Tc", dataLoan.sU4TcProps, dataLoan.sU4TcPaidTo, dataLoan.sU4TcGfeSection, dataLoan);

            BindProperties("sRecF", dataLoan.sRecFProps, dataLoan.sRecFDesc, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sCountyRtc", dataLoan.sCountyRtcProps, dataLoan.sCountyRtcDesc, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sStateRtc", dataLoan.sStateRtcProps, dataLoan.sStateRtcDesc, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sU1GovRtc", dataLoan.sU1GovRtcProps, dataLoan.sU1GovRtcPaidTo, dataLoan.sU1GovRtcGfeSection, dataLoan);
            BindProperties("sU2GovRtc", dataLoan.sU2GovRtcProps, dataLoan.sU2GovRtcPaidTo, dataLoan.sU2GovRtcGfeSection, dataLoan);
            BindProperties("sU3GovRtc", dataLoan.sU3GovRtcProps, dataLoan.sU3GovRtcPaidTo, dataLoan.sU3GovRtcGfeSection, dataLoan);

            BindProperties("sPestInspect", dataLoan.sPestInspectFProps, dataLoan.sPestInspectPaidTo, E_GfeSectionT.LeaveBlank, dataLoan);
            BindProperties("sU1Sc", dataLoan.sU1ScProps, dataLoan.sU1ScPaidTo, dataLoan.sU1ScGfeSection, dataLoan);
            BindProperties("sU2Sc", dataLoan.sU2ScProps, dataLoan.sU2ScPaidTo, dataLoan.sU2ScGfeSection, dataLoan);
            BindProperties("sU3Sc", dataLoan.sU3ScProps, dataLoan.sU3ScPaidTo, dataLoan.sU3ScGfeSection, dataLoan);
            BindProperties("sU4Sc", dataLoan.sU4ScProps, dataLoan.sU4ScPaidTo, dataLoan.sU4ScGfeSection, dataLoan);
            BindProperties("sU5Sc", dataLoan.sU5ScProps, dataLoan.sU5ScPaidTo, dataLoan.sU5ScGfeSection, dataLoan);

            // Line 802
            // Credit or charge
            SetResult("sLDiscnt", dataLoan.sLDiscnt_rep);
            SetResult("sLDiscntFMb", dataLoan.sLDiscntFMb_rep);
            SetResult("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            SetResult("sLDiscntBaseT", dataLoan.sLDiscntBaseT);

            // Credit for lender paid fees
            SetResult("sGfeCreditLenderPaidItemT", dataLoan.sGfeCreditLenderPaidItemT);
            SetResult("sGfeCreditLenderPaidItemF", dataLoan.sGfeCreditLenderPaidItemF_rep);

            // General Lender credit
            SetResult("sGfeLenderCreditFPc", dataLoan.sGfeLenderCreditFPc_rep);
            SetResult("sGfeLenderCreditF", dataLoan.sGfeLenderCreditF_rep);

            // Discount points
            SetResult("sGfeDiscountPointFPc", dataLoan.sGfeDiscountPointFPc_rep);
            SetResult("sGfeDiscountPointF", dataLoan.sGfeDiscountPointF_rep);


            SetResult("sMBrokFBaseT", dataLoan.sMBrokFBaseT);
            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sU3GovRtcBaseT", dataLoan.sU3GovRtcBaseT);
            SetResult("sU2GovRtcBaseT", dataLoan.sU2GovRtcBaseT);
            SetResult("sU1GovRtcBaseT", dataLoan.sU1GovRtcBaseT);
            SetResult("sStateRtcBaseT", dataLoan.sStateRtcBaseT);
            SetResult("sCountyRtcBaseT", dataLoan.sCountyRtcBaseT);
            SetResult("sRecBaseT", dataLoan.sRecBaseT);
            SetResult("sGfeOriginatorCompFPc", dataLoan.sGfeOriginatorCompFPc_rep);
            SetResult("sGfeOriginatorCompFMb", dataLoan.sGfeOriginatorCompFMb_rep);
            SetResult("sGfeOriginatorCompF", dataLoan.sGfeOriginatorCompF_rep);
            SetResult("sGfeOriginatorCompFBaseT", dataLoan.sGfeOriginatorCompFBaseT);
            SetResult("sLDiscntBaseT", dataLoan.sLDiscntBaseT);
            SetResult("sGfeCreditLenderPaidItemT", dataLoan.sGfeCreditLenderPaidItemT);
            SetResult("sFloodCertificationDeterminationT", dataLoan.sFloodCertificationDeterminationT);

            SetResult("sGfeIsTPOTransaction", dataLoan.sGfeIsTPOTransaction);

        }

        private void BindProperties(String name, int Props, string PaidTo, E_GfeSectionT page2Selection, CPageData dataLoan)
        {
            string apr = name + "APR";
            string fha = name + "FHA";
            string poc = name + "POC";
            string borr = name + "BSP";
            string paidToTextbox = name + "PaidTo";
            string paidByList = name + "By";
            string GfeSectionList = name + "Page2";

            SetResult(paidByList, "" + LosConvert.GfeItemProps_Payer(Props));

            SetResult(apr, LosConvert.GfeItemProps_Apr(Props));
            SetResult(fha, LosConvert.GfeItemProps_FhaAllow(Props));


            if (!excludePoc.Contains(name))
            {
                SetResult(poc, LosConvert.GfeItemProps_Poc(Props));
            }

            if (includeBorr.Contains(name))
            {
                SetResult(borr, LosConvert.GfeItemProps_Borr(Props));
            }

            if (!excludePaidTo.Contains(name))
            {


                if (LosConvert.GfeItemProps_ToBr(Props))
                {
                    CAgentFields loanOfficer = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    SetResult(name + "PaidToDDL", 1);
                    if (loanOfficer != null)
                    {
                        SetResult(paidToTextbox, loanOfficer.CompanyName);
                    }
                }
                else if (LosConvert.GfeItemProps_PaidToThirdParty(Props))
                {
                    SetResult(name + "PaidToDDL", 2);
                    SetResult(paidToTextbox, PaidTo);
                }
                else
                {
                    SetResult(name + "PaidToDDL", 0);
                    SetResult(paidToTextbox, dataLoan.sLenNm);
                }
            }

            if (page2Selection != E_GfeSectionT.LeaveBlank)
            {
                SetResult(GfeSectionList, page2Selection);
            }
        }

        private int RetrieveItemProps902905(string name, int dflpValueSrc)
        {
            bool apr = GetBool(name + "APR", false);
            bool fhaAllow = GetBool(name + "_ctrl_Fha_chk", false);
            int payer = GetInt(name + "_ctrl_PdByT_dd", 0);
            bool poc = GetBool(name + "POC");

            bool trdPty = GetString(name + "_ctrl_TrdPty_hdn", "N") == "Y";
            bool aff = GetString(name + "_ctrl_Aff_hdn", "N") == "Y";
            bool qmWarn = GetString(name + "_ctrl_QmWarn_hdn", "N") == "Y";

            bool dflp = LosConvert.GfeItemProps_Dflp(dflpValueSrc);
            return LosConvert.GfeItemProps_Pack(apr, false, payer, fhaAllow, poc, dflp, trdPty, aff, qmWarn);
        }

        // 2/26/14 gf - opm 150695, Since it is now possible to combine the GFE/SC pages, we
        // maintain some of the settings where they may possibly be set on the SC page.
        // POC only needs to be preserved when it isn't shown. DFLP will need to be preserved as
        // long as POC is false.
        private int RetrieveItemProps(string name, bool hasPOC, bool hasB, bool hasGBF, bool hasBF, bool hasBorr, int originalProps)
        {
            bool apr = GetBool(name + "APR", false);
            bool fhaAllow = GetBool(name + "FHA", false);
            bool toBr = LosConvert.GfeItemProps_ToBr(originalProps);
            int payer = GetInt(name + "By", 0);
            bool poc = hasPOC ? GetBool(name + "POC") : LosConvert.GfeItemProps_Poc(originalProps);

            bool gbf = false;
            // bool gbf = hasGBF ? GetBool(name + "_ctrl_GBF_chk") : false;

            bool bf = LosConvert.GfeItemProps_BF(originalProps);

            bool borr = hasBorr ? GetBool(name + "BSP") : false;

            int selectedPaidTo = GetInt(name + "PaidToDDL", 2);

            bool dflp = false;
            if (!poc)
            {
                dflp = LosConvert.GfeItemProps_Dflp(originalProps);
            }
            bool trdPty = LosConvert.GfeItemProps_PaidToThirdParty(originalProps);
            bool aff = LosConvert.GfeItemProps_ThisPartyIsAffiliate(originalProps);
            bool qmWarn = LosConvert.GfeItemProps_ShowQmWarning(originalProps);
            if (selectedPaidTo == 1)
            {
                //lender is selected
                toBr = true;
                trdPty = false;
                aff = false;
            }
            else if (selectedPaidTo == 0)
            {
                //broker is selected
                toBr = false;
                trdPty = false;
                aff = false;
            }
            else
            {
                //other is selected
                toBr = false;
                trdPty = true;
            }

            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, borr, trdPty, aff, qmWarn);
        }

        private int RetrieveItemProps(string name, bool hasPOC, bool hasB, bool hasGBF, bool hasBF, int originalProps)
        {
            return RetrieveItemProps(name, hasPOC, hasB, hasGBF, hasBF, false /*hasBorr*/, originalProps);
        }


        // OPM 126764.  sGfeOriginatorCompFProps is also featured on the Settlement charges page.
        // Use this to make sure we respect existing values for that one.
        // Expect this to be temporary once we have GFE versioning.
        //RetrieveItemProps("sGfeOriginatorCompFProps", false, false, true, true);
        private int RetrieveItemProps(string name, int originalProps)
        {
            bool apr = GetBool(name + "APR", false);
            bool fhaAllow = GetBool(name + "FHA", false);
            int payer = GetInt(name + "By", 0);
            bool bf = LosConvert.GfeItemProps_BF(originalProps);

            bool trdPty = LosConvert.GfeItemProps_PaidToThirdParty(originalProps);
            bool aff = LosConvert.GfeItemProps_ThisPartyIsAffiliate(originalProps);
            bool toBr = LosConvert.GfeItemProps_ToBr(originalProps);

            int selectedPaidTo = GetInt(name + "PaidToDDL", 1);

            if (selectedPaidTo == 1)
            {
                //broker is selected
                toBr = true;
                trdPty = false;
                aff = false;
            }
            else if (selectedPaidTo == 0)
            {
                //lender is selected
                toBr = false;
                trdPty = false;
                aff = false;
            }
            else
            {
                //other is selected
                toBr = false;
                trdPty = true;
            }

            bool poc = LosConvert.GfeItemProps_Poc(originalProps);
            bool gbf = LosConvert.GfeItemProps_GBF(originalProps);
            bool borr = LosConvert.GfeItemProps_Borr(originalProps);
            bool dflp = LosConvert.GfeItemProps_Dflp(originalProps);
            bool qmWarn = LosConvert.GfeItemProps_ShowQmWarning(originalProps);
            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, borr, trdPty, aff, qmWarn);
        }

        private void saveEscrowData()
        {

            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";

            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(sFileVersion);
            BindEscrowData(dataLoan);

            try
            {
                dataLoan.Save();
            }
            catch (PageDataAccessDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (PageDataValidationException exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }

        }

        private void BindEscrowData(CPageData dataLoan)
        {



            string[,] mappingTable = new string[,] {
                                                                      {"RETax0", "HazIns0", "MortIns0", "FloIns0", "SchoTax0", "UsrDef10", "UsrDef20", "UsrDef30", "UsrDef40"},
                                                                      {"RETax1", "HazIns1", "MortIns1", "FloIns1", "SchoTax1", "UsrDef11", "UsrDef21", "UsrDef31", "UsrDef41"},
                                                                      {"RETax2", "HazIns2", "MortIns2", "FloIns2", "SchoTax2", "UsrDef12", "UsrDef22", "UsrDef32", "UsrDef42"},
                                                                      {"RETax3", "HazIns3", "MortIns3", "FloIns3", "SchoTax3", "UsrDef13", "UsrDef23", "UsrDef33", "UsrDef43"},
                                                                      {"RETax4", "HazIns4", "MortIns4", "FloIns4", "SchoTax4", "UsrDef14", "UsrDef24", "UsrDef34", "UsrDef44"},
                                                                      {"RETax5", "HazIns5", "MortIns5", "FloIns5", "SchoTax5", "UsrDef15", "UsrDef25", "UsrDef35", "UsrDef45"},
                                                                      {"RETax6", "HazIns6", "MortIns6", "FloIns6", "SchoTax6", "UsrDef16", "UsrDef26", "UsrDef36", "UsrDef46"},
                                                                      {"RETax7", "HazIns7", "MortIns7", "FloIns7", "SchoTax7", "UsrDef17", "UsrDef27", "UsrDef37", "UsrDef47"},
                                                                      {"RETax8", "HazIns8", "MortIns8", "FloIns8", "SchoTax8", "UsrDef18", "UsrDef28", "UsrDef38", "UsrDef48"},
                                                                      {"RETax9", "HazIns9", "MortIns9", "FloIns9", "SchoTax9", "UsrDef19", "UsrDef29", "UsrDef39", "UsrDef49"},
                                                                      {"RETax10", "HazIns10", "MortIns10", "FloIns10", "SchoTax10", "UsrDef110", "UsrDef210", "UsrDef310", "UsrDef410"},
                                                                      {"RETax11", "HazIns11", "MortIns11", "FloIns11", "SchoTax11", "UsrDef111", "UsrDef211", "UsrDef311", "UsrDef411"},
                                                                      {"RETax12", "HazIns12", "MortIns12", "FloIns12", "SchoTax12", "UsrDef112", "UsrDef212", "UsrDef312", "UsrDef412"}

                                                                  };


            int[,] table = new int[13, 9];
            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    table[i, j] = GetInt(mappingTable[i, j], 0);
                }
            }

            dataLoan.sInitialEscrowAcc = table;
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");

        }

        private void LoadEscrow()
        {

            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";

            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            LoadEscrowData(dataLoan);
        }

        private void LoadEscrowData(CPageData dataLoan)
        {
            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);

            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);

            SetResult("sHazInsRsrvMonRecommended", dataLoan.sHazInsRsrvMonRecommended_rep);

            SetResult("sMInsRsrvMonRecommended", dataLoan.sMInsRsrvMonRecommended_rep);

            SetResult("sRealETxRsrvMonRecommended", dataLoan.sRealETxRsrvMonRecommended_rep);

            SetResult("sSchoolTxRsrvMonRecommended", dataLoan.sSchoolTxRsrvMonRecommended_rep);

            SetResult("sFloodInsRsrvMonRecommended", dataLoan.sFloodInsRsrvMonRecommended_rep);

            SetResult("s1006RsrvMonRecommended", dataLoan.s1006RsrvMonRecommended_rep);

            SetResult("s1007RsrvMonRecommended", dataLoan.s1007RsrvMonRecommended_rep);

            SetResult("sU3RsrvMonRecommended", dataLoan.sU3RsrvMonRecommended_rep);

            SetResult("sU4RsrvMonRecommended", dataLoan.sU4RsrvMonRecommended_rep);

            string[,] mappingTable = new string[,] {
                                                                      {"RETax0", "HazIns0", "MortIns0", "FloIns0", "SchoTax0", "UsrDef10", "UsrDef20", "UsrDef30", "UsrDef40"},
                                                                      {"RETax1", "HazIns1", "MortIns1", "FloIns1", "SchoTax1", "UsrDef11", "UsrDef21", "UsrDef31", "UsrDef41"},
                                                                      {"RETax2", "HazIns2", "MortIns2", "FloIns2", "SchoTax2", "UsrDef12", "UsrDef22", "UsrDef32", "UsrDef42"},
                                                                      {"RETax3", "HazIns3", "MortIns3", "FloIns3", "SchoTax3", "UsrDef13", "UsrDef23", "UsrDef33", "UsrDef43"},
                                                                      {"RETax4", "HazIns4", "MortIns4", "FloIns4", "SchoTax4", "UsrDef14", "UsrDef24", "UsrDef34", "UsrDef44"},
                                                                      {"RETax5", "HazIns5", "MortIns5", "FloIns5", "SchoTax5", "UsrDef15", "UsrDef25", "UsrDef35", "UsrDef45"},
                                                                      {"RETax6", "HazIns6", "MortIns6", "FloIns6", "SchoTax6", "UsrDef16", "UsrDef26", "UsrDef36", "UsrDef46"},
                                                                      {"RETax7", "HazIns7", "MortIns7", "FloIns7", "SchoTax7", "UsrDef17", "UsrDef27", "UsrDef37", "UsrDef47"},
                                                                      {"RETax8", "HazIns8", "MortIns8", "FloIns8", "SchoTax8", "UsrDef18", "UsrDef28", "UsrDef38", "UsrDef48"},
                                                                      {"RETax9", "HazIns9", "MortIns9", "FloIns9", "SchoTax9", "UsrDef19", "UsrDef29", "UsrDef39", "UsrDef49"},
                                                                      {"RETax10", "HazIns10", "MortIns10", "FloIns10", "SchoTax10", "UsrDef110", "UsrDef210", "UsrDef310", "UsrDef410"},
                                                                      {"RETax11", "HazIns11", "MortIns11", "FloIns11", "SchoTax11", "UsrDef111", "UsrDef211", "UsrDef311", "UsrDef411"},
                                                                      {"RETax12", "HazIns12", "MortIns12", "FloIns12", "SchoTax12", "UsrDef112", "UsrDef212", "UsrDef312", "UsrDef412"}

                                                                  };


            int[,] table = dataLoan.sInitialEscrowAcc;
            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    string val = "" + table[i, j];
                    if (val.Equals("0"))
                        val = "";
                    SetResult(mappingTable[i, j], val);
                }
            }


        }


        private void LoadData()
        {
            CPageData dataLoan = CreatePageData();

            dataLoan.InitLoad();
            LoadData(dataLoan);
        }

        private void GetFeeTypeProperties()
        {
            string description = GetString("Description");
            string lineNumber = GetString("LineNumber");

            // Check for blank option
            if (description == ((char)8204).ToString()) // description == &zwnj;
            {
                SetResult("blank", true);
                return;
            }

            FeeType feeType = FeeType.RetrieveByDescription(Broker.BrokerID, lineNumber, description);

            if (feeType == null)
            {
                SetResult("notFound", true);
                return;
            }

            SetResult("Apr", feeType.Apr);
            SetResult("Fha", feeType.FhaAllow);
            SetResult("Page2", feeType.GfeSection);
        }

        private void CalculateData()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            BindData(dataLoan, true);
            BindEscrowData(dataLoan);
            LoadData(dataLoan);
            LoadEscrowData(dataLoan);
        }

        private CPageData CreatePageData()
        {
            Guid loanID = GetGuid("loanid");
            if (Broker.IsGFEandCoCVersioningEnabled)
            {
                return new CPageData(loanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(TPOFeeEditorService)).Union(
                    CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
            }
            else
            {
                return CPageData.CreateUsingSmartDependency(loanID, typeof(TPOFeeEditorService));
            }

        }
    }
}
