﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides rate details and lock information for a loan.
    /// </summary>
    public partial class RateLock : UI.BasePage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the initialization.
        /// </param>
        /// <param name="e">
        /// The arguments for the initialization.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("/json.js");
            this.RegisterJsScript("/webapp/RateLock.js");

            this.RegisterCSS("/webapp/RateLock.css");
            this.RegisterCSS("jquery-ui-1.11.css");
            this.RegisterCSS("/webapp/style.css");
            this.RegisterCSS("/webapp/main.css");

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this, includeSimplePopupDependencies: false);
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the load.
        /// </param>
        /// <param name="e">
        /// The arguments for the load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var dataloan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(RateLock));
            dataloan.InitLoad();

            this.LoanNavHeader.SetDataFromLoan(dataloan, LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationRateLock);
            this.RegisterViewModel(dataloan);
        }

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Registers the view model for the specified <paramref name="dataloan"/>.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing rate lock data for the page.
        /// </param>
        private void RegisterViewModel(CPageData dataloan)
        {
            dataloan.ByPassFieldSecurityCheck = true;
            dataloan.LoadBrokerLockData();

            var hideRateLockHistory = this.PriceMyLoanUser.BrokerDB.HideRateLockHistoryGridInTpoPortal;

            var viewModel = new
            {
                sLId = dataloan.sLId,
                sFinMethT = dataloan.sFinMethT,
                sIsOptionArm = dataloan.sIsOptionArm,

                sLpTemplateNm = dataloan.sLpTemplateNm,
                sLpTemplateNmSubmitted = dataloan.sLpTemplateNmSubmitted,
                sSubmitD = dataloan.sSubmitD_rep,
                sSubmitN = dataloan.sSubmitN,
                sOriginatorCompensationAmount = dataloan.sOriginatorCompensationAmount_rep,
                sOriginatorCompNetPoints = dataloan.sOriginatorCompNetPoints_rep,
                sOriginatorCompensationPaymentSourceT = dataloan.sOriginatorCompensationPaymentSourceT,

                sStatusT = dataloan.sStatusTForTpoPortal_rep,
                sRateLockStatusT = dataloan.sRateLockStatusT_rep,
                sRLckdD = dataloan.sRLckdD_rep,
                sRLckdDays = dataloan.sRLckdDays_rep,
                sRLckdN = dataloan.sRLckdN,
                sRLckdExpiredD = dataloan.sRLckdExpiredD_rep,
                sRLckdExpiredN = dataloan.sRLckdExpiredN,
                sIsRateLockFloatDownAllowed = dataloan.sIsRateLockFloatDownAllowed,
                sIsRateLockExtentionAllowed = dataloan.sIsRateLockExtentionAllowed,
                sIsRateReLockAllowed = dataloan.sIsRateReLockAllowed,
                displayRateLockDownloadButton = this.GetRateLockDownloadButtonVisibility(dataloan),

                sBrokerLockBrokerBaseNoteIR = dataloan.sBrokerLockBrokerBaseNoteIR_rep,
                sBrokerLockBrokerBaseBrokComp1PcPrice = dataloan.sBrokerLockBrokerBaseBrokComp1PcPrice,
                sBrokerLockBrokerBaseBrokComp1PcFee = dataloan.sBrokerLockBrokerBaseBrokComp1PcFee_rep,
                sBrokerLockBrokerBaseRAdjMarginR = dataloan.sBrokerLockBrokerBaseRAdjMarginR_rep,
                sBrokerLockBrokerBaseOptionArmTeaserR = dataloan.sBrokerLockBrokerBaseOptionArmTeaserR_rep,

                sBrokerLockTotVisibleAdjNoteIR = dataloan.sBrokerLockTotVisibleAdjNoteIR_rep,
                sBrokerLockTotVisibleAdjBrokComp1PcPrice = dataloan.sBrokerLockTotVisibleAdjBrokComp1PcPrice,
                sBrokerLockTotVisibleAdjBrokComp1PcFee = dataloan.sBrokerLockTotVisibleAdjBrokComp1PcFee_rep,
                sBrokerLockTotVisibleAdjRAdjMarginR = dataloan.sBrokerLockTotVisibleAdjRAdjMarginR_rep,
                sBrokerLockTotVisibleAdjOptionArmTeaserR = dataloan.sBrokerLockTotVisibleAdjOptionArmTeaserR_rep,

                sNoteIR = dataloan.sNoteIR_rep,
                sBrokerLockFinalBrokComp1PcPrice = dataloan.sBrokerLockFinalBrokComp1PcPrice_rep,
                sBrokComp1Pc = dataloan.sBrokComp1Pc_rep,
                sRAdjMarginR = dataloan.sRAdjMarginR_rep,
                sOptionArmTeaserR = dataloan.sOptionArmTeaserR_rep,

                sBrokerLockOriginatorCompAdjNoteIR = dataloan.sBrokerLockOriginatorCompAdjNoteIR_rep,
                sBrokerLockOriginatorCompAdjBrokComp1PcPrice = dataloan.sBrokerLockOriginatorCompAdjBrokComp1PcPrice,
                sBrokerLockOriginatorCompAdjBrokComp1PcFee = dataloan.sBrokerLockOriginatorCompAdjBrokComp1PcFee_rep,
                sBrokerLockOriginatorCompAdjRAdjMarginR = dataloan.sBrokerLockOriginatorCompAdjRAdjMarginR_rep,
                sBrokerLockOriginatorCompAdjOptionArmTeaserR = dataloan.sBrokerLockOriginatorCompAdjOptionArmTeaserR_rep,

                sBrokerLockOriginatorPriceNoteIR = dataloan.sBrokerLockOriginatorPriceNoteIR_rep,
                sBrokerLockOriginatorPriceBrokComp1PcPrice = dataloan.sBrokerLockOriginatorPriceBrokComp1PcPrice,
                sBrokerLockOriginatorPriceBrokComp1PcFee = dataloan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep,
                sBrokerLockOriginatorPriceRAdjMarginR = dataloan.sBrokerLockOriginatorPriceRAdjMarginR_rep,
                sBrokerLockOriginatorPriceOptionArmTeaserR = dataloan.sBrokerLockOriginatorPriceOptionArmTeaserR_rep,

                adjustments = dataloan.sBrokerLockAdjustments.Where(pricingAdjustment => !pricingAdjustment.IsHidden),

                hideRateLockHistory = hideRateLockHistory,
                rateLockHistory = this.GetRateLockHistoryTable(hideRateLockHistory, dataloan),

                lenderCompPlan = this.GetLenderCompPlan(dataloan),

                showCalendarDays = dataloan.BrokerDB.RateLockExpirationWeekendHolidayBehavior != E_RateLockExpirationWeekendHolidayBehavior.AllowWeekendHoliday,
                dataloan.BrokerDB.HideRateLockPageLoanStatusInOriginatorPortal
        };

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("RateLockViewModel", viewModel);
        }

        /// <summary>
        /// Obtains the visibility setting for the "Download Lock Confirmation" button.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull data from.
        /// </param>
        /// <returns>
        /// True if the button is visible, false otherwise.
        /// </returns>
        private bool GetRateLockDownloadButtonVisibility(CPageData dataloan)
        {
            return dataloan.sRateLockStatusT == E_sRateLockStatusT.Locked &&
                dataloan.sHasRateLockConfirmationPdf;
        }

        /// <summary>
        /// Gets the rate lock history data to display on the page.
        /// </summary>
        /// <param name="hideRateLockHistory">
        /// True if the history should be hidden, false otherwise.
        /// </param>
        /// <param name="dataloan">
        /// The loan to pull data from.
        /// </param>
        /// <returns>
        /// The list of rate lock history items.
        /// </returns>
        private IEnumerable<RateLockHistoryItem> GetRateLockHistoryTable(bool hideRateLockHistory, CPageData dataloan)
        {
            if (hideRateLockHistory)
            {
                return Enumerable.Empty<RateLockHistoryItem>();
            }

            return RateLockHistoryItem.GetSortedRateHistory(dataloan.sRateLockHistoryXmlContent);
        }

        /// <summary>
        /// Obtains the lender compensation data for the loan.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull lender compensation data from.
        /// </param>
        /// <returns>
        /// A complex type containing the lender compensation data or 
        /// null if the specified <paramref name="dataloan"/> does not have
        /// a lender compensation plan.
        /// </returns>
        private object GetLenderCompPlan(CPageData dataloan)
        {
            if (!dataloan.sHasOriginatorCompensationPlan)
            {
                return null;
            }

            return new
            {
                details = Tools.GetOrigCompPlanDesc(dataloan.sOriginatorCompensationPlanT, dataloan.sOriginatorCompensationAppliedBy, dataloan.sOriginatorCompensationPlanSourceEmployeeName),
                sOriginatorCompensationPlanAppliedD = dataloan.sOriginatorCompensationPlanAppliedD_rep,
                sOriginatorCompensationEffectiveD = dataloan.sOriginatorCompensationEffectiveD_rep,
                sOriginatorCompensationPercent = dataloan.sOriginatorCompensationPercent_rep,
                sOriginatorCompensationAdjBaseAmount = dataloan.sOriginatorCompensationAdjBaseAmount_rep,
                sOriginatorCompensationMinAmount = dataloan.sOriginatorCompensationMinAmount_rep,
                sOriginatorCompensationMaxAmount = dataloan.sOriginatorCompensationMaxAmount_rep,
                sOriginatorCompensationFixedAmount = dataloan.sOriginatorCompensationFixedAmount_rep,
                sOriginatorCompensationTotalAmount = dataloan.sOriginatorCompensationTotalAmount_rep,
                sOriginatorCompensationHasMinAmount = dataloan.sOriginatorCompensationHasMinAmount,
                sOriginatorCompensationHasMaxAmount = dataloan.sOriginatorCompensationHasMaxAmount,
                sOriginatorCompensationBaseT = dataloan.sOriginatorCompensationBaseT
            };
        }
    }
}
