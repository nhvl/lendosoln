﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisclosureFormUpload.aspx.cs" Inherits="PriceMyLoan.webapp.DisclosureFormUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Request Form Upload</title>
    <style type="text/css">
        div.row {
            margin-bottom: 15px;
        }

        #DisclosureFormNotes {
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        function _init() {
            setupSubmitButton();
            TPOStyleUnification.Components();
        }

        function setupSubmitButton() {
            setSubmitButtonDisplay();
            $j('#CompletedRequestForm').change(setSubmitButtonDisplay);
            $j('#DisclosureFormNotes').keyup(setSubmitButtonDisplay);
        }

        function setSubmitButtonDisplay() {
            var $notes = $j('#DisclosureFormNotes');
            var $fileInput = $j('#CompletedRequestForm');

            var isDisabled = $j.trim($notes.val()) === '' || $j.trim($fileInput.val()) === '';
            $j('#SubmitButton').prop('disabled', isDisabled);
        }

        function resizePopup() {
            if (typeof(parent.LQBPopup.Resize) === "function") {
            parent.LQBPopup.Resize(450, 300);

            }
        }

        function closePopup() {
            parent.LQBPopup.Hide();
        }

        function openPdf(pdfUrl) {
            window.open(pdfUrl);
        }

        function completeUpload() {
            parent.LQBPopup.Return();
        }

        function displayError(text) {
            parent.simpleDialog.alert(text);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h4>Request for <span id="DisclosureFormType" runat="server"></span></h4>
                </div>
            </div>
            <asp:PlaceHolder runat="server" ID="FormPlaceholder">
            <div class="row">
                <div class="col-xs-12">
                    <asp:Button runat="server" ID="DisclosureFormDownloadButton" CssClass="btn btn-default" Text="Download Request Form" OnClick="DisclosureFormDownloadButton_Click" nohighlight="true" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    Completed Request Form:
                </div>
                <div class="col-xs-7">
                    <asp:FileUpload runat="server" ID="CompletedRequestForm" />
                </div>
            </div>
            </asp:PlaceHolder>
            <div class="row">
                <div class="col-xs-12">
                    Message to Lender:
                    <br />
                    <asp:TextBox runat="server" ID="DisclosureFormNotes" Columns="10" Rows="8" TextMode="MultiLine" nohighlight="true"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-right">
                    <input type="button" class="btn btn-flat" value="Cancel" onclick="closePopup();" />
                    <asp:Button runat="server" ID="SubmitButton" CssClass="btn btn-flat" Text="Submit" OnClick="SubmitButton_Click" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
