﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FriendlyErrorPopup.aspx.cs" Inherits="PriceMyLoan.webapp.FriendlyErrorPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <span id="errMsg">
    </form>
    <script type="text/javascript">
        $(function() {
            var dialogArgs = parent.LQBPopup.GetArguments();
            if (dialogArgs && dialogArgs.errMsg) {
                $("#errMsg").text(dialogArgs.errMsg);
            }
        });
    </script>
</body>
</html>
