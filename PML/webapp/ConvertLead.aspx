﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConvertLead.aspx.cs" Inherits="PriceMyLoan.webapp.ConvertLead" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Convert Lead</title>
    <style type="text/css">
        body { padding-top: 5px; }
        label  
        {
            padding-right: 5px;
            color:#565656;
        }
        #ButtonPanel  
        {
            padding-top: 10px;
            text-align: center;
        }
        #LoanNumberingPanel { padding-top: 10px; }
        #NumberingRadiosPanel { padding-top: 5px; }
        #ConvertWithTemplatePanel { padding-top: 5px; }
    </style>
</head>
<body>
    <h4 class="page-header">Convert Lead</h4>
    <form id="form1" runat="server">
    <div>
        <div id="ConvertNoTemplatePanel">
            <input type="radio" id="convertNoTemplate" name="convert" value="0"/>
            <label for="convertNoTemplate">Convert to loan</label>
        </div>
        <div id="ConvertWithTemplatePanel">
            <input type="radio" id="convertWithTemplate" name="convert" value="1"/>
            <label for="convertWithTemplate">Convert to loan using template</label>
            <asp:DropDownList ID="templateDDL" runat="server"></asp:DropDownList>
        </div>
        
        <div id="LoanNumberingPanel">
            <label>Loan numbering options:</label>
            <div id="NumberingRadiosPanel">
                <input type="radio" id="removeLeadPrefix" name="numbering" value="0" />
                <label>Remove "lead" prefix from loan number</label>
                <input type="radio" id="autoCreateNew" name="numbering" value="1" />
                <label>Auto-create new loan number</label>
            </div>
        </div>
        
        <div id="ButtonPanel">
            <input type="button" id="convert" style="width: 60px;" value="Convert" />
            <input type="button" id="cancel"  style="width: 60px;" value="Cancel" />
        </div>
        
    </div>
    </form>
    <script type="text/javascript">
        jQuery(function($) {
            $('#cancel').click(function() {
                parent.LQBPopup.Hide();
            });

            $('#convert').click(function() {
                // Return the options to the opener to call convert to lead.
                var convertArgs = {
                    LoanNumbering: $('input:radio[name="numbering"]:checked').val(),
                    TemplateOption: $('input:radio[name="convert"]:checked').val(),
                    TemplateId: $('#templateDDL').val()
                };
                parent.LQBPopup.Return(convertArgs);
            }).attr('disabled', true);

            $('#templateDDL').change(function() {
                $('#convert').attr('disabled', $(this).val() === <%= AspxTools.JsString(Guid.Empty) %>);
            });
            
            $('input:radio[name="convert"]').click(function() {
                if ($('#convertNoTemplate').prop('checked')) {
                    $('#templateDDL').attr('disabled', true);
                    $('#convert').attr('disabled', false);
                }
                else {
                    $('#templateDDL').attr('disabled', false)
                        .change(); // toggle the convert button.
                }
            });

            // Configure UI based on object
            if(!convertOptions.IsLeadMadeFromBlankOrQPTemplate)
            {
                $('#convertNoTemplate').prop('checked',true);
                $('#templateDDL').attr('disabled', true);
                $('#ConvertWithTemplatePanel').hide();
                $('#convertNoTemplate').hide();
            }
            else if(convertOptions.IsForceLeadToLoanTemplate)
            {
                $('#ConvertNoTemplatePanel').hide();
                $('#convertWithTemplate').prop('checked', true).hide();
            }
            else 
            {
                $('#convertNoTemplate').prop('checked', true);
            }

            if(convertOptions.IsDisallowTemplateSelection)
            {
                $('#ConvertNoTemplatePanel').show();
                $('#ConvertWithTemplatePanel').hide();
                $('#convertWithTemplate').prop('checked', false)
                    .hide();
                $('#convertNoTemplate').prop('checked', true);
            }

            if (convertOptions.IsForceLeadToLoanNewLoanNumber) {
                $('#autoCreateNew').prop('checked', true);
                $('#LoanNumberingPanel').hide();
            }
            else if (convertOptions.IsForceLeadToLoanRemoveLeadPrefix || convertOptions.IsForceLeadToUseLoanCounter) {
                $('#removeLeadPrefix').prop('checked', true);
                $('#LoanNumberingPanel').hide();
            }
            else {
                // Default to remove lead prefix
                $('#removeLeadPrefix').prop('checked', true);
            }
            
            $('#templateDDL').attr('disabled', $('#convertNoTemplate').prop('checked'));
            $('#convert').attr('disabled', $('#convertWithTemplate').prop('checked'));
            if (parent.LQBPopup.GetArguments().isConvertToLead) {
                function replaceNodeTextLoanToLead() {<%--http://stackoverflow.com/a/19318945/2946652--%>
                    if (this.nodeType === 3){
                        this.nodeValue = this.nodeValue.replace('loan', 'lead');
                    } else {
                        $(this).contents().each(replaceNodeTextLoanToLead);
                    }
                }
                $('#ConvertNoTemplatePanel label, #ConvertWithTemplatePanel label').contents().each(replaceNodeTextLoanToLead);
            }
        });
    </script>
</body>
</html>
