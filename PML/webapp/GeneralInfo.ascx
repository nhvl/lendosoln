﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfo.ascx.cs" Inherits="PriceMyLoan.webapp.GeneralInfo" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
    jQuery(function($) {
        $(window).on('loan/changed/sHorizonOfBorrowerInterestInMonths', f_fixHorizonOfBorrowerInterest);
        $(window).on('loan/view_update_done', function() {
            f_validateGeneralInfo();
            
            $j('.GeneralInfo').find('textarea,input[type=text],input[type=radio],input[type=checkbox],select')
                    .readOnly(PML.options.IsReadOnly)
                    .attr('disabled', PML.options.IsReadOnly); // disable all inputs if the loan is readonly
        });

        $j('#explainHorizonOfBorrowerInterest').click(function() {
            return f_openHelp('HorizonOfBorrowerInterest.html', 300, 105);
        });
    });
    function f_fixHorizonOfBorrowerInterest() {
        // Horizon of borrower interest must be 1-999.
        var horizonRegex = /^[1-9]\d?\d?$/;
        var horizonVal = $.trim(PML.model.loan['sHorizonOfBorrowerInterestInMonths']);
        var bHorizonValid = horizonRegex.test(horizonVal);
        if (false == bHorizonValid) {
            var defaultVal = 1;
            $('#sHorizonOfBorrowerInterestInMonths').val(defaultVal).change();
        }        
    }
    
    function f_validateGeneralInfo() {
        var bGeneralInfoValid = true;
        // Add validation as needed by updating bGeneralInfoValid.

        // Update Pml valid
        var prevIsValid = PML.model.loan['isGeneralInfoValid'];
        if (prevIsValid != bGeneralInfoValid) {
            PML.model.loan['isGeneralInfoValid'] = bGeneralInfoValid;
            $j(window).trigger('loan/changed/isGeneralInfoValid', [bGeneralInfoValid]);
        }
    }
</script>
<style type="text/css">
    .GeneralInfo .validator { vertical-align: middle; }
    .GeneralInfo .narrow { width: 30px; }
    .GeneralInfo { font-weight: bold; }
</style>

<div class="GeneralInfo">
    <div>
        <label>Horizon of borrower interest:</label>
        <input type="text" id="sHorizonOfBorrowerInterestInMonths" class="narrow" preset="numeric-nocss" maxlength="3" data-calc />
        <label>months</label>
        <%--<img id="sHorizonOfBorrowerInterestInMonthsValidator" class="validator" src="../images/error_pointer.gif" alt="Must be a value 1-999"/>--%>
        (<a href="#" id="explainHorizonOfBorrowerInterest">explain</a>)
    </div>
</div>
