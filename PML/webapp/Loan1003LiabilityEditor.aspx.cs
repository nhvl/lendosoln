﻿//-----------------------------------------------------------------------
// <author> Eduardo Michel</author>
// <summary> Allows the user to edit the Liabilities </summary>
//-----------------------------------------------------------------------

namespace PriceMyLoan.webapp
{
    using System;
    using System.Data;
    using System.Web.UI.HtmlControls;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.AntiXss;

    /// <summary>
    /// This is the same Liability Editor as in Price My Loan 2.0, the only difference is that the page is styled differently.
    /// </summary>
    public partial class Loan1003LiabilityEditor : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// The current application ID.
        /// </summary>
        private Guid currentAppId = Guid.Empty;

        /// <summary>
        /// The borrower's full name (first last)
        /// </summary>
        private string borrowerFullName = "";

        /// <summary>
        /// The coborrower's full name (first last)
        /// </summary>
        private string coborrowerFullName = "";

        /// <summary>
        /// Data app object.
        /// </summary>
        private CAppData dataApp;

        /// <summary>
        /// Gets the current application ID.
        /// </summary>
        /// <value>The current application ID.</value>
        protected Guid CurrentAppId
        {
            get { return this.currentAppId; }
        }

        /// <summary>
        /// Loads the Page.
        /// </summary>
        /// <param name="sender">The object that caused the event.</param>
        /// <param name="e">Event arguments.</param>
        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.RegisterCSS("/webapp/jquery-ui.css");
            this.RegisterCSS("/webapp/main.css");
            this.RegisterService("Loan1003", "/webapp/Loan1003PopupService.aspx");
            this.currentAppId = RequestHelper.GetGuid("appid");
            this.RegisterJsGlobalVariables("LoanId", this.LoanID);
            this.RegisterJsGlobalVariables("AppId", this.currentAppId);

            if (!Page.IsPostBack)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003LiabilityEditor));
                dataLoan.InitLoad();

                this.sFileVersion = dataLoan.sFileVersion;
                dataApp = dataLoan.GetAppData(this.currentAppId);

                borrowerFullName = dataApp.aBFirstNm + " " + dataApp.aBLastNm;
                coborrowerFullName = dataApp.aCFirstNm + " " + dataApp.aCLastNm;

                ILiaCollection recordList = dataApp.aLiaCollection;

                this.SetSpecialLiabilities(recordList);

                m_dg.DataSource = recordList.SortedView;
                m_dg.DataBind();
            }
        }

        /// <summary>
        /// Returns the owner type as a string.
        /// </summary>
        /// <param name="type">The integer value of OwnerT as a string.</param>
        /// <returns>The owner type as a string.</returns>
        protected string DisplayOwnerType(string type)
        {
            try
            {
                return CLiaFields.DisplayStringOfOwnerT((E_LiaOwnerT)int.Parse(type));
            }
            catch (System.FormatException)
            {
                // Make unknown type and default become borrower.
                return "B";
            }
            catch (System.ArgumentNullException)
            {
                // Make unknown type and default become borrower.
                return "B";
            }
        }

        /// <summary>
        /// Returns the owner name as a string.
        /// </summary>
        /// <param name="type">The integer value of OwnerT as a string.</param>
        /// <returns>The owner type as a string.</returns>
        protected string DisplayOwnerName(string type)
        {
            try
            {
                E_LiaOwnerT typeT = (E_LiaOwnerT)int.Parse(type);
                switch (typeT)
                {
                    case E_LiaOwnerT.Borrower:
                        return (borrowerFullName == " ") ? "Borrower" : borrowerFullName;
                    case E_LiaOwnerT.CoBorrower:
                        return (coborrowerFullName == " ") ? "Co-Borrower" : coborrowerFullName;
                    case E_LiaOwnerT.Joint:
                        return "Joint";
                    default:
                        return borrowerFullName;
                }
            }
            catch (System.FormatException)
            {
                // Make unknown type and default become borrower.
                return "B";
            }
            catch (System.ArgumentNullException)
            {
                // Make unknown type and default become borrower.
                return "B";
            }
        }

        /// <summary>
        /// Returns the Liability type as a string.
        /// </summary>
        /// <param name="type">The integer value of Liability as a string.</param>
        /// <returns>The Liability type as a string.</returns>
        protected string DisplayLiabilityType(string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                return string.Empty;
            }

            int v;
            if (int.TryParse(type, out v))
            {
                E_DebtRegularT debtType = (E_DebtRegularT)v;
                return CLiaRegular.DisplayStringOfDebtT(debtType);
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns a string in Money Format.
        /// </summary>
        /// <param name="value">The value of the money as a string.</param>
        /// <returns>The string converted into Money format.</returns>
        protected string DisplayMoneyString(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value.Replace("*", string.Empty);
        }

        /// <summary>
        /// Generates a Payoff Checkbox.
        /// </summary>
        /// <param name="dataItem">The Item that triggered this call.</param>
        /// <returns>Returns an HTML Input Check Box.</returns>
        protected HtmlInputCheckBox CreatePayoffCheckbox(object dataItem)
        {
            DataRowView row = dataItem as DataRowView;

            HtmlInputCheckBox checkbox = null;

            if (row != null)
            {
                string recordId = row["RecordId"].ToString();
                bool willBePaidOff = row["WillBePdOff"].ToString() == "True";

                checkbox = new HtmlInputCheckBox
                {
                    ID = "pdoff_" + recordId,
                    Checked = willBePaidOff,
                };
                checkbox.Name = checkbox.ID;
                checkbox.Attributes.Add("onclick", "f_onPaidOffCheck(this);");
            }

            return checkbox;
        }

        /// <summary>
        /// Generates a CreateUsedInRatioCheckbox Checkbox.
        /// </summary>
        /// <param name="dataItem">The Item that triggered this call.</param>
        /// <returns>Returns an HTML Input Check Box.</returns>
        protected HtmlControl CreateUsedInRatioCheckbox(object dataItem)
        {
            DataRowView row = dataItem as DataRowView;

            HtmlControl control = null;
            if (row != null)
            {
                E_DebtT debtT = E_DebtT.Other;

                try
                {
                    debtT = (E_DebtT)int.Parse(row["DebtT"].ToString());
                }
                catch (System.FormatException)
                {
                }
                catch (System.ArgumentNullException)
                {
                }

                if (debtT == E_DebtT.Mortgage)
                {
                    control = new HtmlGenericControl("span");
                    ((HtmlGenericControl)control).InnerText = "***";
                }
                else
                {
                    bool notUsedInRatio = row["NotUsedInRatio"].ToString() == "True";
                    string recordId = row["RecordId"].ToString();
                    HtmlInputCheckBox cb = new HtmlInputCheckBox();
                    cb.Checked = !notUsedInRatio;
                    cb.ID = "ratio_" + recordId;
                    cb.Name = cb.ID;
                    control = cb;
                }
            }

            return control;
        }

        protected HtmlControl CreateReoLink(object dataItem)
        {
            IReCollection recordList = dataApp.aReCollection;

            DataRowView row = dataItem as DataRowView;

            HtmlControl control = null;
            if (row != null)
            {
                E_DebtT debtT = E_DebtT.Other;

                try
                {
                    debtT = (E_DebtT)int.Parse(row["DebtT"].ToString());
                }
                catch (System.FormatException)
                {
                }
                catch (System.ArgumentNullException)
                {
                }

                if (debtT == E_DebtT.Mortgage)
                {
                    var reoLinkButton = new HtmlInputButton();

                    var matchedReo = this.GetMatchedReo(row["MatchedReRecordId"], recordList);
                    if (matchedReo != null)
                    {
                        reoLinkButton.Value = "Update REO";

                        var helpIcon = new HtmlGenericControl("i")
                        {
                            InnerHtml = "&#xE887;"
                        };

                        helpIcon.Attributes["class"] = "material-icons";
                        helpIcon.Attributes["data-toggle"] = "tooltip";
                        helpIcon.Attributes["data-placement"] = "bottom";
                        helpIcon.Attributes["title"] = $"{matchedReo.Addr}, {matchedReo.City}, {matchedReo.State} {matchedReo.Zip}";

                        var span = new HtmlGenericControl("span");
                        span.InnerText = "REO Linked";

                        span.Controls.Add(helpIcon);
                        span.Controls.Add(reoLinkButton);

                        control = span;
                    }
                    else
                    {
                        reoLinkButton.Value = "Select REO";
                        control = reoLinkButton;
                    }

                    var recordId = row["RecordId"];
                    reoLinkButton.Attributes.Add("onclick", $"linkReo({AspxTools.JsString(recordId.ToString())})");
                    reoLinkButton.Attributes.Add("class", "btn btn-resize");
                }
            }

            return control;
        }

        /// <summary>
        /// Retrieves the matched REO for a mortgage liability.
        /// </summary>
        /// <param name="dataRowId">
        /// The ID of the matched REO from the data row.
        /// </param>
        /// <param name="recordList">
        /// The list of REO records.
        /// </param>
        /// <returns>
        /// The matched REO or null if the REO could not be retrieved.
        /// </returns>
        private IRealEstateOwned GetMatchedReo(object dataRowId, IReCollection recordList)
        {
            if (dataRowId == DBNull.Value)
            {
                return null;
            }
            
            Guid recordId;
            if (!Guid.TryParse((string)dataRowId, out recordId))
            {
                return null;
            }

            return recordList.GetRecordOf(recordId) as IRealEstateOwned;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        #endregion

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += this.PageLoad;
        }

        /// <summary>
        /// Sets the special liabilities for the page.
        /// </summary>
        /// <param name="recordList">
        /// The list of records.
        /// </param>
        private void SetSpecialLiabilities(ILiaCollection recordList)
        {
            var alimony = recordList.GetAlimony(false);
            if (alimony != null)
            {
                this.Alimony_OwedTo.Value = alimony.OwedTo;
                this.Alimony_MonthlyPayment.Text = alimony.Pmt_rep;
                this.Alimony_RemainingMonths.Value = alimony.RemainMons_rep;
                this.Alimony_NotUsedInRatio.Checked = alimony.NotUsedInRatio;
            }

            var childSupport = recordList.GetChildSupport(false);
            if (childSupport != null)
            {
                this.ChildSupport_OwedTo.Value = childSupport.OwedTo;
                this.ChildSupport_MonthlyPayment.Text = childSupport.Pmt_rep;
                this.ChildSupport_RemainingMonths.Value = childSupport.RemainMons_rep;
                this.ChildSupport_NotUsedInRatio.Checked = childSupport.NotUsedInRatio;
            }

            var jobRelatedExpense = recordList.GetJobRelated1(false);
            if (jobRelatedExpense != null)
            {
                this.JobRelated1_ExpenseDescription.Value = jobRelatedExpense.ExpenseDesc;
                this.JobRelated1_MonthlyPayment.Text = jobRelatedExpense.Pmt_rep;
                this.JobRelated1_NotUsedInRatio.Checked = jobRelatedExpense.NotUsedInRatio;
            }

            jobRelatedExpense = recordList.GetJobRelated2(false);
            if (jobRelatedExpense != null)
            {
                this.JobRelated2_ExpenseDescription.Value = jobRelatedExpense.ExpenseDesc;
                this.JobRelated2_MonthlyPayment.Text = jobRelatedExpense.Pmt_rep;
                this.JobRelated2_NotUsedInRatio.Checked = jobRelatedExpense.NotUsedInRatio;
            }
        }
    }
}
