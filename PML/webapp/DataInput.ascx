﻿<%@ Register TagPrefix="pml" TagName="LoanFilePricer" Src="~/webapp/LoanFilePricer.ascx" %>
<%@ Register TagPrefix="pml" TagName="LoanApplication" Src="~/webapp/LoanApplication.ascx" %>
<%@ Register TagPrefix="pml" TagName="GeneralInfo" Src="~/webapp/GeneralInfo.ascx" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataInput.ascx.cs" Inherits="PriceMyLoan.webapp.DataInput" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<style type="text/css">
#DataInputContainer { }
#generalInfo { padding-bottom: 3px; }
</style>

<script id="tabLinkTemplate" type="text/x-jquery-tmpl"><li><a id="${tabLinkId}" href="#${tabContentId}" tab="${tabNum}">${tabLinkText}&nbsp;<img id="${tabLinkId}TabValidator" src="../images/error_pointer.gif" alt="This tab contains required field(s)."/></a></li></script>

<div id="DataInputContainer" class="container">
    <div id="generalInfo" data-model="loan">
        <pml:GeneralInfo ID="GeneralInfo" runat="server"></pml:GeneralInfo>
    </div>
    <div id="DataInputTabs" class="Tabs">
        <ul class="tabnav">
            <%-- Also some kind of template --%>
            
            <li id="propertyAndLoanInfoLI" class="selected"><a id="propertyAndLoanInfoLink" href="#propertyAndLoanInfoTab">Property & Loan Info&nbsp;<img id="propertyAndLoanInfoTabValidator" src="../images/error_pointer.gif" alt="This tab contains required field(s)."/></a></li>
        </ul>
    </div>
    
    <div id="applicationTab" class="Tab" data-model="app">
        <pml:LoanApplication ID="LoanApplication" runat="server" />
    </div>
    
    <div id="propertyAndLoanInfoTab" class="Tab visible" data-model="loan">
        <pml:LoanFilePricer ID="LoanFilePricer" runat="server" />
    </div>
    
    <div style="clear: both"></div>
</div>

