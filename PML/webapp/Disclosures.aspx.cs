﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.ObjLib.Events;

    /// <summary>
    /// Provides a means for users to view disclosures.
    /// </summary>
    public partial class Disclosures : UI.BasePage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the initialization.
        /// </param>
        /// <param name="e">
        /// The arguments for the initialization.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            this.RegisterJsScript("mask.js");

            this.RegisterCSS("/webapp/Disclosures.css");

            this.RegisterService("main", "/webapp/DisclosuresService.aspx");

            this.RegisterJsGlobalVariables("CanRequestRedisclosure", this.CurrentUserCanPerform(WorkflowOperations.RequestRedisclosureInTpoPortal));
            this.RegisterJsGlobalVariables("CanRequestInitialClosingDisclosure", this.CurrentUserCanPerform(WorkflowOperations.RequestInitialClosingDisclosureInTpoPortal));                       
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the load.
        /// </param>
        /// <param name="e">
        /// The arguments for the load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var dataloan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(Disclosures));
            dataloan.InitLoad();

            bool IsAntiSteeringEnabled = dataloan.sGfeIsTPOTransaction && (PriceMyLoanUser.BrokerDB.OrigPortalAntiSteeringDisclosureAccess == OrigPortalAntiSteeringDisclosureAccessType.AllTpoTransactions ||
                (PriceMyLoanUser.BrokerDB.OrigPortalAntiSteeringDisclosureAccess == OrigPortalAntiSteeringDisclosureAccessType.LenderPaidOriginationOnly &&
                dataloan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid));

            if (IsAntiSteeringEnabled)
            {
                this.RegisterJsGlobalVariables("StepOneEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosuresWithAntiSteering.StepOne);
                this.RegisterJsGlobalVariables("StepTwoEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosuresWithAntiSteering.StepTwo);
                this.RegisterJsGlobalVariables("StepThreeEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosuresWithAntiSteering.StepThree);
                this.RegisterJsGlobalVariables("StepFourEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosuresWithAntiSteering.StepFour);
                this.RegisterJsGlobalVariables("StepFiveEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosuresWithAntiSteering.StepFive);
            }
            else
            {
                this.RegisterJsGlobalVariables("StepOneEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosures.StepOne);
                this.RegisterJsGlobalVariables("StepTwoEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosures.StepTwo);
                this.RegisterJsGlobalVariables("StepThreeEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosures.StepThree);
                this.RegisterJsGlobalVariables("StepFourEventSource", LoanEventSource.Disclosure.OriginatorPortalInitialDisclosures.StepFour);
            }

            this.RegisterJsGlobalVariables("IsAntiSteeringEnabled", IsAntiSteeringEnabled);
            this.RegisterJsGlobalVariables("LoanId", this.LoanID);
            this.RegisterJsGlobalVariables("ApplicationId", dataloan.GetAppData(0).aAppId);

            this.LoanNavHeader.SetDataFromLoan(dataloan, LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationDisclosures);
        }

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Gets additional workflow operations that need checks on the page.
        /// </summary>
        /// <returns>
        /// The additional workflow operations that need checks.
        /// </returns>
        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new[]
            {
                WorkflowOperations.RequestRedisclosureInTpoPortal,
                WorkflowOperations.RequestInitialClosingDisclosureInTpoPortal
            };
        }
    }
}
