﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanInformation.aspx.cs" Inherits="PriceMyLoan.webapp.LoanInformation" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<%@ Register TagPrefix="lqb" TagName="FannieAddendumSections" Src="~/webapp/Partials/LoanInformation/FannieAddendumSections.ascx"%>
<%@ Register TagPrefix="lqb" TagName="FreddieAddendumSections" Src="~/webapp/Partials/LoanInformation/FreddieAddendumSections.ascx"%>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Loan Information</title>
</head>
<body id="MainBody">
<form id="aspNetForm" runat="server">
    <input type="hidden" id="sLId" runat="server" />
    <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />
    <div class="content-detail" name="content-detail">
        <div class="warp-section warp-section-tabs" loan-static-tab>
            <div class="static-tabs"><div class="static-tabs-inner">
                <header class="page-header">Loan Information</header>
                <div ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>

                <ul id="LinkRow" class="nav nav-tabs nav-tabs-panels">
                    <li><a class="tabLink" id="LoanTermsTabLink" runat="server">Loan Terms</a></li>
                    <li><a class="tabLink" id="FannieMaeAddendumSectionsTabLink" runat="server">FNMA Addendum</a></li>
                    <li><a class="tabLink" id="FreddieMacAddendumSectionsTabLink" runat="server">FHLMC Addendum</a></li>
                </ul>
            </div></div>
            <div class="content-tabs">
                <input type="Hidden" id="_ClientID" name="_ClientID" value="" />
                <div>
                    <div class="content tab-pane hide" id="LoanTermsTab">
                        <asp:PlaceHolder ID="LoanTerms" runat="server" Visible="false">
                            <script type="text/javascript">
                                var loanInformation = angular.module('LoanInformation', []);

                                loanInformation.controller('LoanInformationController', function ($scope, $location) {
                                    $scope.vm = null;

                                    $scope.viewRegistrationCertificate = function () {
                                        var url = VRoot + '/Main/SavedCertificate.aspx?loanid=' + $scope.vm.sLId;
                                        window.open(VRoot + '/Common/PrintView_Frame.aspx?body_url=' + url, '_blank');
                                    }

                                    $scope.shouldShowRegistrationCertificateLink = function () {
                                        return $scope.vm.sStatusT !== $scope.vm.constants.LoanOpen &&
                                            $scope.vm.sPmlSubmitStatusT !== $scope.vm.constants.NeverSubmittedInPml;
                                    }

                                    $scope.init = function () {
                                        $scope.vm = angular.fromJson(LoanInformationViewModel);
                                    }

                                    $scope.init();
                                });

                                loanInformation.directive('mainDataTable', function () {
                                    return {
                                        restrict: 'E',
                                        replace: true,
                                        scope: { source: '=', },
                                        link: function (scope, element, attrs) {
                                            // To keep control logic out of this directive,
                                            // we'll have methods in the directive's scope
                                            // point to the parent's controller.
                                            scope.viewRegistrationCertificate = scope.$parent.viewRegistrationCertificate;
                                            scope.shouldShowRegistrationCertificateLink = scope.$parent.shouldShowRegistrationCertificateLink;
                                        },
                                        templateUrl: VRoot + '/webapp/Partials/LoanInformation/MainData.html'
                                    }
                                });

                                loanInformation.directive('armTable', function () {
                                    return {
                                        restrict: 'E',
                                        replace: true,
                                        scope: { source: '=', },
                                        templateUrl: VRoot + '/webapp/Partials/LoanInformation/ArmTable.html'
                                    }
                                });

                                loanInformation.directive('helocTable', function () {
                                    return {
                                        restrict: 'E',
                                        replace: true,
                                        scope: { source: '=', },
                                        link: function (scope, element, attrs) {
                                            // To keep control logic out of this directive,
                                            // we'll have methods in the directive's scope
                                            // point to the parent's controller.
                                            scope.viewRegistrationCertificate = scope.$parent.viewRegistrationCertificate;
                                            scope.shouldShowRegistrationCertificateLink = scope.$parent.shouldShowRegistrationCertificateLink;
                                        },
                                        templateUrl: VRoot + '/webapp/Partials/LoanInformation/HelocTable.html'
                                    }
                                });

                                function setupLoanInformationApp() {
                                    // We have two separate "mini" angular applications: the loan navigation header
                                    // and the main page. For Angular to work correctly with both applications, we
                                    // need to bootstrap the module for the main page. Angular will take care of
                                    // the loan navigation header since it is the first app defined on the page.
                                    angular.bootstrap(angular.element("[ng-controller='LoanInformationController']"), ['LoanInformation']);
                                }
                                TabManager.registerStartUpScript(setupLoanInformationApp);
                            </script>
                            <div ng-controller="LoanInformationController" id="LoanInformationControllerId">
                                <main-data-table ng-if="vm.mainData != null" source="vm.mainData"></main-data-table>

                                <heloc-table ng-if="vm.helocData != null" source="vm.helocData"></heloc-table>

                                <arm-table ng-if="vm.armData != null" source="vm.armData"></arm-table>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="content tab-pane hide" id="FannieMaeAddendumSectionsTab">
                        <lqb:FannieAddendumSections id="FannieMaeAddendumSections" runat="server" Visible="false" />
                    </div>
                    <div class="content tab-pane hide" id="FreddieMacAddendumSectionsTab">
                        <lqb:FreddieAddendumSections id="FreddieMacAddendumSections" runat="server" Visible="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</body>
</html>
