﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoanApplication.ascx.cs" Inherits="PriceMyLoan.webapp.LoanApplication" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<script type="text/javascript">
    var shouldAlwaysShowCoAppSection = false;

    jQuery(function ($j) {
        $j(window).on('apps/view_update_done', f_validateApp);
        // If it is now false, want to reset the SSN value and trigger
        // a data-refresh. By triggering the change of aCSsn it will
        // both refresh and validate the entire app. The refresh will
        // update the UI to reflect the auto-calc VA Funding Fee
        $j(window).on('apps/changed/aBHasSpouse', function(e, appNum, value) {
            PML.model.apps[appNum]['aBHasSpouse'] = value;
            if (value === 'False') {
                $j('#aCSsn').val('').change();
                if (shouldAlwaysShowCoAppSection == true)
                {
                    PML.model.apps[appNum]['aBHasSpouse'] = 'True';
                }
                else
                {
                    PML.model.apps[appNum]['aBHasSpouse'] = 'False';
                }
            }
            f_validateApp(e, appNum);
        });
        $j(window).on('apps/changed/aBFirstNm', f_validateApp);
        $j(window).on('apps/changed/aBLastNm', f_validateApp);
        $j(window).on('apps/changed/aBEmail', f_validateApp);
        $j(window).on('apps/changed/aBSsn', f_validateApp);
        $j(window).on('apps/changed/aBMonInc', f_validateApp);
        $j(window).on('apps/changed/aCFirstNm', f_validateApp);
        $j(window).on('apps/changed/aCLastNm', f_validateApp);
        $j(window).on('apps/changed/aCEmail', f_validateApp);
        $j(window).on('apps/changed/aCSsn', f_validateApp);
        $j(window).on('apps/changed/aCMonInc', f_validateApp);
        $j(window).on('apps/changed/aLiquidAssetsPe', f_validateApp);
        $j(window).on('apps/changed/aOpNegCfPe', f_validateApp);
        $j(window).on('apps/changed/aIsCVAElig', f_validateApp);
        $j(window).on('apps/changed/aIsCVAFFEx', f_validateApp);
        $j(window).on('apps/changed/aCVServiceT', f_validateApp);
        $j(window).on('apps/changed/aCVEnt', f_validateApp);

        $j('#lnkHelpHousingHistoryB,#lnkHelpHousingHistoryC').click(function() {
            return f_openHelp('HousingHistory.aspx', 450, 360);
        });
        $j('#lnkHelpVALoanEligibilityB,#lnkHelpVALoanEligibilityC').click(function () {
            if ($j.trim(PML.model.loan["IsBeyondV1_CalcVaLoanElig"]) == "False")
            {
            return f_openHelp('VALoanEligibility.aspx', 450, 150);
            }

            var isVeteran;
            var isSurvivingSpouseOfVeteran;
            var bType;

            if (this.id === "lnkHelpVALoanEligibilityB")
            {
                bType = "b";
                isVeteran = $j('#aBIsVeteran').is(":checked");
                isSurvivingSpouseOfVeteran = $j('#aBIsSurvivingSpouseOfVeteran').is(":checked");
            }
            else
            {
                bType = "c";
                isVeteran = $j('#aCIsVeteran').is(":checked");
                isSurvivingSpouseOfVeteran = $j('#aCIsSurvivingSpouseOfVeteran').is(":checked");
            }

            LQBPopup.Show(gVirtualRoot + '/help/VALoanEligibility.aspx?isMigrated=1&bType=' + bType +
                                         '&isVeteran=' + isVeteran +
                                         '&isSSpouseOfVet=' + isSurvivingSpouseOfVeteran,
            {
                width: 450,
                height: 175,
                hideCloseButton: true,
                onReturn: vaLoanEligibilityCallBack
        });

            return false;
        });

        function vaLoanEligibilityCallBack(args)
        {
            var newIsVeteran = args.isVeteran;
            var newIsSurvivingSpouseOfVeteran = args.IsSurvivingSpouseOfVeteran;
            var bType = args.bType;

            if(bType === "b")
            {
                $j('#aBIsVeteran').prop('checked', newIsVeteran);
                PML.updateFieldInModel($j('#aBIsVeteran')[0]);
                $j('#aBIsSurvivingSpouseOfVeteran').prop('checked', newIsSurvivingSpouseOfVeteran).change();
            }
            else
            {
                $j('#aCIsVeteran').prop('checked', newIsVeteran);
                PML.updateFieldInModel($j('#aCIsVeteran')[0]);
                $j('#aCIsSurvivingSpouseOfVeteran').prop('checked', newIsSurvivingSpouseOfVeteran).change();

                shouldAlwaysShowCoAppSection = true;
            }
        }

        $j('#lnkHelpTotalIncomeB,#lnkHelpTotalIncomeC').click(function() {
            return f_openHelp('Q00004.html', 730, 600);
        });
        $j('#lnkHelpNegCf').click(function() {
            return f_openHelp('Q00004.html', 730, 600);
        });
        $j('#lnkHelpLiquidAssets').click(function() {
            return f_openHelp('liquidassets.aspx', 450, 200);
        });
        $j('#lnkHelpCreditProvider').click(function() {
            return f_openHelp('Q00001.aspx', 450, 200);
        });

        $j('#addAppBtn').click(function() {
            if (PML.model.apps[PML.getCurrentAppNum()]["isValid"]) {
                $(window).trigger('apps/create');
            }
            return false;
        });

        $j('#removeAppBtn').click(function() {
            $j("#RemoveApplicationPopup").dialog({
                resizable: false,
                modal: true,
                width: 'auto',
                height: 'auto',
            }).css({
        });
        return true;
    });

    // Workaround for mask silliness
    // Any preset="ssn", "phone", or "date" would need this workaround
    // Since the masking function changes the values of these fields on keyup, the browser
    //   thinks that no change has occurred, so it does not fire the change event
    $j('#aBSsn').blur(function(e) { $(this).change(); });
    $j('#aCSsn').blur(function(e) { $(this).change(); });


    // Element visibility
    // The "First Time Home Buyer" checkbox determines the visiblity of
    // "Has Housing History"
    $j(window).on('apps/changed/aBTotalScoreIsFthb', function(e, appNum) {
        $j('#aBHasHousingHistContainer').toggle(PML.model.apps[appNum]["aBTotalScoreIsFthb"] == 'True');
    });
    $j(window).on('apps/changed/aBTotalScoreIsFthbReadOnly', function (e, appNum) {
        toggleFthbReadOnly(appNum, 'aBTotalScoreIsFthb', 'aBTotalScoreIsFthbReadOnly');
    });

    $j(window).on('apps/changed/aCTotalScoreIsFthb', function(e, appNum) {
        $j('#aCHasHousingHistContainer').toggle(PML.model.apps[appNum]["aCTotalScoreIsFthb"] == 'True');
    });
    $j(window).on('apps/changed/aCTotalScoreIsFthbReadOnly', function (e, appNum) {
        toggleFthbReadOnly(appNum, 'aCTotalScoreIsFthb', 'aCTotalScoreIsFthbReadOnly');
    });

    function toggleFthbReadOnly(appNum, cbId, cbReadOnlyId) {
        $j('#' + cbId).readOnly(PML.model.apps[appNum][cbReadOnlyId] === 'True' || PML.options.IsReadOnly);
    }

    // The "Eligible for VA Loan" checkbox determines the visibility of
    // "VA Funding Fee Exempt", "Service Type", and "Entitlement"
    $j(window).on('apps/changed/aIsBVAElig', function(e, appNum) {
        $j('#aIsBVAFFExContainer').toggle(PML.model.apps[appNum]["aIsBVAElig"] == 'True');
        $j('#aBVServiceTContainer').toggle(PML.model.apps[appNum]["aIsBVAElig"] == 'True');
        $j('#aBVEntContainer').toggle(PML.model.apps[appNum]["aIsBVAElig"] == 'True');
    });
    $j(window).on('apps/changed/aIsCVAElig', function(e, appNum) {
        $j('#aIsCVAFFExContainer').toggle(PML.model.apps[appNum]["aIsCVAElig"] == 'True');
        $j('#aCVServiceTContainer').toggle(PML.model.apps[appNum]["aIsCVAElig"] == 'True');
        $j('#aCVEntContainer').toggle(PML.model.apps[appNum]["aIsCVAElig"] == 'True');
    });

    // Ideally there would be a model value for this
    $j("input[name='CreditAction']").change(function(e) {
        renderOrderCreditBtn();
    });
    $j('#DataInput1_LoanApplication_CreditProtocol').parent().change(function (e) {
        SetOrderCreditState();
    });


    $j("#removeAppOK").click(function() {
        $(window).trigger('apps/delete', [PML.getCurrentAppNum()]);

        $j("#RemoveApplicationPopup").dialog('close');
    });

    $j("#removeAppCancel").click(function() {
        $j("#RemoveApplicationPopup").dialog('close');
    });


    $j(window).on('apps/view_update_done', function(e, appNum) {
        if (PML.options.HideOrderNewCredit) {
            $j('#OrderNewLine').hide();
        }
        else {
            $j('#OrderNewLine').show();
        }

        if (PML.model.apps[appNum]["HasEdocs"] == 'True') {
            $j("#removeAppPopupMessage").text("Application has associated EDocs, and cannot be deleted from this screen. Contact your Account Executive if you need to delete this application.");

            $j("#removeAppOK").css("display", "none");

            $j("#removeAppCancel").text("OK");
        }
        else {
            $j("#removeAppPopupMessage").text("Do you want to delete all data of borrower and spouse?");

            $j("#removeAppCancel").text("No");

            $j("#removeAppOK").css("display", "");
        }

        $j('#aBHasHousingHistContainer').toggle(PML.model.apps[appNum]["aBTotalScoreIsFthb"] == 'True');

        $j('#aCHasHousingHistContainer').toggle(PML.model.apps[appNum]["aCTotalScoreIsFthb"] == 'True');

        $j('#aIsBVAFFExContainer').toggle(PML.model.apps[appNum]["aIsBVAElig"] == 'True');
        $j('#aBVServiceTContainer').toggle(PML.model.apps[appNum]["aIsBVAElig"] == 'True');
        $j('#aBVEntContainer').toggle(PML.model.apps[appNum]["aIsBVAElig"] == 'True');

        $j('#aIsCVAFFExContainer').toggle(PML.model.apps[appNum]["aIsCVAElig"] == 'True');
        $j('#aCVServiceTContainer').toggle(PML.model.apps[appNum]["aIsCVAElig"] == 'True');
        $j('#aCVEntContainer').toggle(PML.model.apps[appNum]["aIsCVAElig"] == 'True');

        $j('#aBTotalScoreIsFthbContainer').toggle(PML.model.loan["sLPurposeTPe"] == '0');
        $j('#aCTotalScoreIsFthbContainer').toggle(PML.model.loan["sLPurposeTPe"] == '0');

        var $creditProtocol = $j('#DataInput1_LoanApplication_CreditProtocol').parent();
        // "Does the application have a credit report on file?"
        if (PML.model.apps[appNum]["aIsCreditReportOnFile"] == 'False') {
            $j('#UseCreditReportOnFilePanel').hide();
            $j('#ReportID').hide();
            $j('#ReportIDLabel').hide();
            $j('#viewCreditLink').hide();
            $j('#missingPublicRecordsLabel').hide();
            $j('#editPublicRecords').hide();
        } else {
            $j('input:checkbox[name="CreditAction"]').removeAttr('checked');
            $j('#UseCreditReportOnFilePanel').show()
            $j('#UseCreditReportOnFile').attr('checked', 'checked');

            var creditProtocolSelect = $creditProtocol.find('select');
            creditProtocolSelect.val(PML.model.apps[appNum]["LastCreditReportingAgency"]); // select the dropdown
            if(creditProtocolSelect.val() == null) {
                creditProtocolSelect.find("option:first-child").prop('selected', true);
            }

            $j('#ReportID').show();
            $j('#ReportIDLabel').show();
            $j('#viewCreditLink').show();
            if (PML.model.apps[appNum]["aDisplayPublicRecords"] == 'False') {
                $j('#missingPublicRecordsLabel').show();
                $j('#editPublicRecords').hide();
            } else {
                $j('#missingPublicRecordsLabel').hide();
                $j('#editPublicRecords').show();
            }
        }
        if (PML.model.apps[appNum]["aIsCreditReportOnFile"] == 'False'
                && PML.model.apps[appNum]["aBExperianScorePe"] == 0
                && PML.model.apps[appNum]["aBTransUnionScorePe"] == 0
                && PML.model.apps[appNum]["aBEquifaxScorePe"] == 0) {
            $j('#sBCreditScoresContainer').hide();
        } else {
            $j('#sBCreditScoresContainer').show();
        }
        if (PML.model.apps[appNum]["aIsCreditReportOnFile"] == 'False'
                && PML.model.apps[appNum]["aCExperianScorePe"] == 0
                && PML.model.apps[appNum]["aCTransUnionScorePe"] == 0
                && PML.model.apps[appNum]["aCEquifaxScorePe"] == 0) {
            $j('#sCCreditScoresContainer').hide();
        } else {
            $j('#sCCreditScoresContainer').show();
        }

        if (PML.options.EnableMultipleApps) {
            $('#appManagement').show();
        }
        else {
            $('#appManagement').hide();
        }

        // Ideally there would be a model value for this; for now, we use the DOM
        var creditAction = $j("input[name='CreditAction']:selected").val();
        renderOrderCreditBtn();

        // The statement below should really go in some sort of init function instead
        $j('.OrderCredit').find('textarea,input[type=text],input[type=radio],input[type=checkbox],select')
                .not('[alwaysDisabled]')
                .attr('disabled', PML.options.IsReadOnly)
                .not('[readonly]') // Always want inputs with 'readonly' attribute to be readonly.
                .readOnly(PML.options.IsReadOnly); // disable all inputs if the loan is readonly

        // The following calls may disable fields and must come after the preceding statement.
        updateCitizenshipDropdowns(appNum);
        toggleFthbReadOnly(appNum, 'aBTotalScoreIsFthb', 'aBTotalScoreIsFthbReadOnly');
        toggleFthbReadOnly(appNum, 'aCTotalScoreIsFthb', 'aCTotalScoreIsFthbReadOnly');

        if ($j.trim(PML.model.loan["IsBeyondV1_CalcVaLoanElig"]) == "False")
        {
            $j('#lnkHelpVALoanEligibilityB').text("Explain");
            $j('#lnkHelpVALoanEligibilityC').text("Explain");
            $j('#aIsBVAElig').prop("disabled", false);
            $j('#aIsCVAElig').prop("disabled", false);
        }
        else
        {
            $j('#lnkHelpVALoanEligibilityB').text("Determine");
            $j('#lnkHelpVALoanEligibilityC').text("Determine");
            $j('#aIsBVAElig').prop("disabled", true);
            $j('#aIsCVAElig').prop("disabled", true);

            var aIsBVAElig = $j.trim(PML.model.apps[appNum]["aIsBVAElig"]) == "True";
            var aIsCVAElig = $j.trim(PML.model.apps[appNum]["aIsCVAElig"]) == "True";
            if (aIsBVAElig) {
                $j('#aBVServiceT').prop('disabled', false);
                $j('#aBVEnt').prop('disabled', false);
                $j('#aIsBVAFFEx').prop('disabled', false);
            }
            else {
                $j('#aBVServiceT').prop('disabled', true);
                $j('#aBVEnt').prop('disabled', true);
                $j('#aIsBVAFFEx').prop('disabled', true);
            }
            if (aIsCVAElig) {
                $j('#aCVServiceT').prop('disabled', false);
                $j('#aCVEnt').prop('disabled', false);
                $j('#aIsCVAFFEx').prop('disabled', false);
            }
            else {
                $j('#aCVServiceT').prop('disabled', true);
                $j('#aCVEnt').prop('disabled', true);
                $j('#aIsCVAFFEx').prop('disabled', true);
            }
        }

        shouldAlwaysShowCoAppSection = false;
    });

    function updateCitizenshipDropdowns(appNum) {
        var app = PML.model.apps[appNum];

        var borrowerConfig = PML.getCitizenshipDropdownConfiguration(app['aBDecCitizen'], app['aBDecResidency']);
        bindCitizenshipDropdown($j('#aProdBCitizenT'), borrowerConfig, app.aProdBCitizenT);

        var coborrowerConfig = PML.getCitizenshipDropdownConfiguration(app['aCDecCitizen'], app['aCDecResidency']);
        bindCitizenshipDropdown($j('#aProdCCitizenT'), coborrowerConfig, app.aProdCCitizenT);
    }

    function bindCitizenshipDropdown($dropdown, config, val) {
        $dropdown.empty();

        for (var i = 0; i < config.length; i++) {
            var kvp = config[i];
            var $option = $j('<option></option>').val(kvp.Key).html(kvp.Value);
            $dropdown.append($option);
        }

        $dropdown.val(val);

        var disableDropdown = config.length === 1 || PML.options.IsReadOnly;
        $dropdown.css('background-color', disableDropdown ? gReadonlyBackgroundColor : '');
        $dropdown.readOnly(disableDropdown);
    }

    $j(window).on('apps/view_update_done', function(e, appNum) {
        // OPM 140637 - set liquid asset field to readonly if assets are imported from app
        if (Number(PML.model.apps[appNum]["aAsstLiqTot"].replace(/[^0-9\.]+/g, "")) > 0
            && Number(PML.model.apps[appNum]["aAssetCollectionCount"]) > 0) {
            $j('#aLiquidAssetsPe').attr('readonly', 'readonly');
            //$j('#aLiquidAssetsPe').css({ "background-color": "lightgray" });
        } else {
            $j('#aLiquidAssetsPe').removeAttr('readonly');
            $j('#aLiquidAssetsPe').css({ "background-color": "transparent" });
        }
    });

});

    // OPM 107075: m.p.
    function renderOrderCreditBtn() {
        var $orderCreditBtn = $('#oCreditBtn');
        var $orderCreditBtnText = $('.oCreditBtnText');
        var manualCreditWarning = $('#ManualCreditWarning');
        var $creditProtocol = $j('#DataInput1_LoanApplication_CreditProtocol').parent();
        manualCreditWarning.hide();
        $('#CreditProviderData').show();
        if (document.getElementById('UseCreditReportOnFile').checked) {
            $creditProtocol.prop('disabled', true);
            $orderCreditBtn.hide();
        } else if (document.getElementById('OrderNew').checked) {
            $creditProtocol.prop('disabled', false);
            $orderCreditBtn.show();
            $orderCreditBtnText.text('Order Credit');
        } else if (document.getElementById('Reissue').checked) {
            $creditProtocol.prop('disabled', false);
            $orderCreditBtn.show();
            $orderCreditBtnText.text('Re-Issue Credit');
        } else if (document.getElementById('Upgrade').checked) {
            $creditProtocol.prop('disabled', false);
            $orderCreditBtn.show();
            $orderCreditBtnText.text('Upgrade Credit');
        } else if (document.getElementById('ManualCredit') != null && document.getElementById('ManualCredit').checked) {
            $creditProtocol.prop('disabled', false);
            $orderCreditBtn.show();
            $orderCreditBtnText.text('Enter Credit');
            $('#CreditProviderData').hide();
            if (PML.model.apps[PML.getCurrentAppNum()]["aIsCreditReportOnFile"] == 'True') {
                manualCreditWarning.show();
            }
        }

        SetOrderCreditState();
    }

    function SetOrderCreditState() {
        var $orderCreditBtn = $('#oCreditBtn');
        var $warningIcon = $orderCreditBtn.find('.WarningIcon');

        var canOrder = false;
        var reason = '';
        if(document.getElementById('ManualCredit') != null && document.getElementById('ManualCredit').checked) {
            canOrder = true;
        }
        else {
            var craId = $j('#<%= AspxTools.ClientId(CreditProtocol) %>').val();
            if (craId == <%= AspxTools.JsString(Guid.Empty.ToString()) %> ) {
                canOrder = true;
            }
            else {
                var args = {
                    loanid: PML.getLoanId(),
                    CraId: craId
                };
                var result = gService.PML.call("GetOrderCreditPermission", args);
                if (result.value.Error == null) {
                    canOrder = result.value.CanOrderCredit == '1';
                    reason = result.value.CanOrderCreditReason;
                } else {
                    alert(result.value.Error);
                }
            }
        }

        if (canOrder) {
            $orderCreditBtn.prop('disabled', false);
            $warningIcon.hide();
        } else {
            $orderCreditBtn.prop('disabled', true);
            $warningIcon.show();
            $warningIcon.attr('title', reason);
        }
    }

    function f_editPublicRecords() {
        LQBPopup.Show(ML.VirtualRoot + '/main/PublicRecordList.aspx?loanid=' + PML.getLoanId() + '&appid=' + PML.model.apps[PML.getCurrentAppNum()]["aAppId"],
            {
                width: 1000,
                height: 700
            }
        );
	    return false;
    }

    function f_editLiability() {
        LQBPopup.Show(ML.VirtualRoot + '/main/LiabilityList.aspx?loanid=' + PML.getLoanId() + '&appid=' + PML.model.apps[PML.getCurrentAppNum()]["aAppId"],
            {
                hideCloseButton: true,
                width: 700,
                height: 500,
                onReturn: function(args) {
                    PML.load();

                    // The value for the monthly payment will be updated for the loan file
                    // after saving in the popup editor but won't be updated in the UI until
                    // the next time the PML data transformation function is called, which
                    // happens outside of this load. As with PML 1.0, we can update this
                    // calculated field in the meantime since the liability popup will
                    // give us the updated value after the user modifies the liability list.
                    $j('#aTransmOMonPmtPe').val(args.aTransmOMonPmtPe);
                }
            }
        );
        }

        function f_editLoan1003() {
            document.location = ML.VirtualRoot + '/webapp/Loan1003.aspx?loanid=' + PML.getLoanId() + '&applicationid=' + PML.model.apps[PML.getCurrentAppNum()]["aAppId"] +"&src=pml";
        }

    function f_viewCreditReport() {
        LQBPopup.Show(ML.VirtualRoot + '/main/ViewCreditFrame.aspx?loanid=' + PML.getLoanId() + '&applicationid=' + PML.model.apps[PML.getCurrentAppNum()]["aAppId"],
            {
                hideCloseButton: true,
                width: 700,
                height: 500
            }
        );
    }

    function f_validateApp(e, appNum) {
        var validApp = true;

        var aBFirstNmValid = $j.trim(PML.model.apps[appNum]["aBFirstNm"]) != '';
        $j('#aBFirstNmValidator').toggle(aBFirstNmValid == false);
        validApp &= aBFirstNmValid;
        var aBLastNmValid = $j.trim(PML.model.apps[appNum]["aBLastNm"]) != '';
        $j('#aBLastNmValidator').toggle(aBLastNmValid == false);
        validApp &= aBLastNmValid;

        var emailRegex = new RegExp(<%=AspxTools.JsString(ConstApp.EmailValidationExpression) %>);

        var aBEmail = $j.trim(PML.model.apps[appNum]["aBEmail"]);
        var HasABEmail = aBEmail != '';
        var aBEmailValid = emailRegex.test(aBEmail);
        //Force valid if email is not required and no email is provided.  Emails should still be validated even if not required.
        aBEmailValid |= (!PML.options.RequireEmail && !HasABEmail);
        $j('#aBEmailValidator').toggle(aBEmailValid == false);
        validApp &= aBEmailValid;

        var ssnRegex = /^\d{3}-?\d{2}-?\d{4}$/;
        var aBSsnValid = PML.options.UserDoesntNeedCreditToPrice || ssnRegex.test( $j.trim(PML.model.apps[appNum]["aBSsn"] ) );
        $j('#aBSsnValidator').toggle(aBSsnValid == false);
        validApp &= aBSsnValid;

        var aBMonIncValid = f_validateDollarAmount($j.trim(PML.model.apps[appNum]["aBMonInc"]), true, true);
        $j('#aBMonIncValidator').toggle(aBMonIncValid == false);
        validApp &= aBMonIncValid;
        var aLiquidAssetsPeValid = $j.trim(PML.model.apps[appNum]["aLiquidAssetsPe"]) != '';
        $j('#aLiquidAssetsPeValidator').toggle(aLiquidAssetsPeValid == false);
        validApp &= aLiquidAssetsPeValid;
        var aOpNegCfPeValid = $j.trim(PML.model.apps[appNum]["aOpNegCfPe"]) != '';
        $j('#aOpNegCfPeValidator').toggle(aOpNegCfPeValid == false);
        validApp &= aOpNegCfPeValid;

        if ($j.trim(PML.model.apps[appNum]["aIsBVAElig"]) == 'True') {
            var aBVServiceTValid = $j.trim(PML.model.apps[appNum]["aBVServiceT"]) != '0';
            $j('#aBVServiceTValidator').toggle(aBVServiceTValid == false);
            validApp &= aBVServiceTValid;
            var aBVEntValid = $j.trim(PML.model.apps[appNum]["aBVEnt"]) != '0';
            $j('#aBVEntValidator').toggle(aBVEntValid == false);
            validApp &= aBVEntValid;
        }

        if (shouldAlwaysShowCoAppSection == false && PML.model.apps[appNum]['aBHasSpouse'] === 'False')
        {
            $j('#CoApplicantInfo').hide();
        }
        else
        {
            $j('#CoApplicantInfo').show();
            var aCFirstNmValid = $j.trim(PML.model.apps[appNum]["aCFirstNm"]) != '';
            $j('#aCFirstNmValidator').toggle(aCFirstNmValid == false);
            validApp &= aCFirstNmValid;
            var aCLastNmValid = $j.trim(PML.model.apps[appNum]["aCLastNm"]) != '';
            $j('#aCLastNmValidator').toggle(aCLastNmValid == false);
            validApp &= aCLastNmValid;

            var aCEmail = $j.trim(PML.model.apps[appNum]["aCEmail"]);
            var HasACEmail = aCEmail != '';
            var aCEmailValid = emailRegex.test(aCEmail);
            aCEmailValid |= (!PML.options.RequireEmail && !HasACEmail); //Force valid if email not required
            $j('#aCEmailValidator').toggle(aCEmailValid == false);
            validApp &= aCEmailValid;

            var ssnRegex = /^\d{3}-?\d{2}-?\d{4}$/;
            var aCSsnValid = PML.options.UserDoesntNeedCreditToPrice || ssnRegex.test( $j.trim( PML.model.apps[appNum]["aCSsn"] ) );
            $j('#aCSsnValidator').toggle(aCSsnValid == false);
            validApp &= aCSsnValid;

            var aCMonIncValid = $j.trim(PML.model.apps[appNum]["aCMonInc"]) != '';
            $j('#aCMonIncValidator').toggle(aCMonIncValid == false);
            validApp &= aCMonIncValid;

            if ($j.trim(PML.model.apps[appNum]["aIsCVAElig"]) == 'True') {
                var aCVServiceTValid = $j.trim(PML.model.apps[appNum]["aCVServiceT"]) != '0';
                $j('#aCVServiceTValidator').toggle(aCVServiceTValid == false);
                validApp &= aCVServiceTValid;
                var aCVEntValid = $j.trim(PML.model.apps[appNum]["aCVEnt"]) != '0';
                $j('#aCVEntValidator').toggle(aCVEntValid == false);
                validApp &= aCVEntValid;
            }
        }

        if (validApp) {
            $j('#addAppBtn').removeAttr('disabled');
            $j('#oCreditBtn').removeAttr('disabled');
            $j('#btnToPropAndLoanPg').removeAttr('disabled');

            SetOrderCreditState();
        } else {
            $j('#addAppBtn').attr('disabled', 'disabled');
            $j('#oCreditBtn').attr('disabled', 'disabled');
            $j('#btnToPropAndLoanPg').attr('disabled', 'disabled');
        }

        if (PML.model.apps.length == 1) {
            $j('#removeAppBtn').attr('disabled', 'disabled');
        } else {
            $j('#removeAppBtn').removeAttr('disabled');
        }

        PML.model.apps[appNum]["isValid"] = validApp;
        $j(window).trigger("apps/changed/isValid", [appNum, validApp]);
    }

    function openCreditOrder() {
        LQBPopup.Show('OrderCredit.aspx?loanid=' + PML.getLoanId()
            + '&AppNum=' + PML.getCurrentAppNum()
            + '&CreditProtocol=' + $j('#<%= AspxTools.ClientId(CreditProtocol) %>').val()
            + '&CreditAction=' + getCreditAction(),
            {
                hideCloseButton: true,
                width: 700,
                height: 550,
                onReturn: function() { PML.load(); }
            });
    }

    function getCreditAction() {
        if (document.getElementById('OrderNew').checked)
            return 2;
        if (document.getElementById('Reissue').checked)
            return 1;
        if (document.getElementById('Upgrade').checked)
            return 4;
        if (document.getElementById('ManualCredit') != null && document.getElementById('ManualCredit').checked)
            return 3;

        return -1;
    }

    function editPinState($anchor, productId, rate, secondProductId, secondRate, secondMonthlyPayment) {
        if ($anchor.attr('pinId') == null || $anchor.attr('pinId') == '') { // No pin id means currently not pinned
            var args = {};
            args["SelectedRate"] = rate;
            args["sLId"] = PML.getLoanId();
            args["IsRateMerge"] = getRenderResultModeT();
            args["lLpTemplateId"] = productId;
            args['secondlLpTemplateId'] = secondProductId;
            args['secondSelectedRate'] = secondRate;
            args['secondLoanMonthlyPayment'] = secondMonthlyPayment;

            var result = gService.PML.call("AddPinState", args);
            if (result.value.Error == null) {
                PML.ActionArea.UpdateNumPinned(result.value.sPinCount);
                $anchor.attr('pinId', result.value.NewPinId);
                $anchor.text('unpin');
                return result.value.NewPinId;
            } else {
                alert(result.value.Error);
            }
        } else {
        var args = {};
            args["sLId"] = PML.getLoanId();
            args["PinId"] = $anchor.attr('pinId');
            var result = gService.PML.call("DeletePinState", args);
            if (!result.error) {
                PML.ActionArea.UpdateNumPinned(result.value.sPinCount);
                $anchor.attr('pinId', '');
                $anchor.text('pin');
                return '';
            }
        }

        return null;
    }
</script>
<style type="text/css">
.OrderCredit
{
	width: 100%;
	/*background: peachpuff;*/
}

.OrderCredit legend
{
    font-size: 1.2em;
    font-weight: bold;
    color:#ff9933;
    margin-bottom: 5px;
    padding: 3px;
}
.OrderCredit div
{
    /*border: solid 1px black;*/
    overflow: hidden;
}

.OrderCredit .normal
{
	display:inline;
	float:none;
	width:auto;
	padding-right:0px;
}

.OrderCredit .smallFont
{
    font-size: smaller;
}

.OrderCredit label
{
    font-family:verdana, arial, sans-serif;
    text-align: right;
    width: 100px;
    display: inline-block;
    vertical-align: top;
    padding-right: 5px;
    color:#565656;
    margin-top: 0 !important;
    margin-bottom: 1em;
}

.OrderCredit input
{
    vertical-align: middle;
    line-height: 1em;
}

.OrderCredit .explain
{
    line-height: 1em;
}

.OrderCredit .wide
{
    width: 210px !important;
}

.OrderCredit .floatLeftField
{
    width: 195px;
}

.OrderCredit .validator
{
    vertical-align: middle;
}

.WarningIcon {
        vertical-align: bottom;
    }

#removeAppBtn
{
    width: 170px;
    float: left;
}
#addAppBtn
{
    width: 180px;
    float: right;
}

#ManualCreditWarning
{
    color: red;
}

#loanManagement
{
    text-align: center;
}
#oCreditBtn
{
    width: 160px;
}

#btnToPropAndLoanPg
{
    width: 190px;
}


#RemoveApplicationPopup
{
    display:none;
}

.padding-left-credit {
    padding-left: 3em;
}
</style>

<div class="OrderCredit">
<div id="appManagement">
    <button id="removeAppBtn" type="button">Remove this application</button>
    <button id="addAppBtn" type="button">Add New 1003 Application</button>
    <div style="clear: both"></div>

</div>
<button type="button" onclick="f_editLoan1003();" id="Loan1003Link" style="width:100%; margin-top:5px;" runat="server">View Full 1003 Application</button>
<fieldset id="ApplicantInfo">
<legend>Applicant Info</legend>
    <div>
        <label for="aBFirstNm" class="singleLine">First Name</label>
        <input type="text" id="aBFirstNm" class="wide" data-calc />
        <img id="aBFirstNmValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div>
        <label for="aBMidNm" class="singleLine">Middle Name</label>
        <input type="text" id="aBMidNm" class="wide" data-calc />
    </div>
    <div>
        <label for="aBLastNm" class="singleLine">Last Name</label>
        <input type="text" id="aBLastNm" class="wide" data-calc/>
        <img id="aBLastNmValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div>
        <label for="aBSuffix" class="singleLine">Suffix</label>
        <input type="text" id="aBSuffix" class="wide" data-calc />
    </div>
    <div>
        <label for="aBSsn" class="singleLine">SSN</label>
        <input type="text" id="aBSsn" preset="ssn" class="wide" data-calc />
        <img id="aBSsnValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div>
        <label for="aBEmail" class="singleLine">E-mail</label>
        <input type="text" id="aBEmail" class="wide" data-calc />
        <img id="aBEmailValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field" />
    </div>
    <div>
        <label for="aProdBCitizenT" class="singleLine">Citizenship</label>
        <select id="aProdBCitizenT" class="wide" data-calc>
        </select>
    </div>

    <div>
        <div class="floatLeftField">
            <label for="aBMonInc" class="singleLine">Monthly Income<br /><a class="explain" href="#" id="lnkHelpTotalIncomeB">Explain</a></label>
            <input type="text" id="aBMonInc" preset="money-allowblank-nocss" style="width:60px !important; display: inline-block; vertical-align: top" data-calc/>
            <img id="aBMonIncValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
        </div>
        <div class="floatRightField">
            <label for="aBIsSelfEmplmt" class="singleLine" style="display: inline-block; vertical-align: middle">Self Employed?</label>
            <input type="checkbox" id="aBIsSelfEmplmt" style="display: inline-block; vertical-align: middle" data-calc />
        </div>
        <div style="clear:both"></div>
    </div>

    <div>
        <div class="floatLeftField" id="aBTotalScoreIsFthbContainer">
            <label for="aBTotalScoreIsFthb" class="singleLine">First Time Home Buyer?</label>
            <input type="checkbox" id="aBTotalScoreIsFthb" data-calc />
            <a class="explain" href="#" id="lnkHelpHousingHistoryB">Explain</a>
        </div>
        <div class="floatRightField" id="aBHasHousingHistContainer">
            <label for="aBHasHousingHist" class="singleLine">Has Housing History?</label>
            <input type="checkbox" id="aBHasHousingHist" data-calc />
        </div>
    </div>
    <div>
        <div class="floatLeftField">
            <label for="aIsBVAElig" class="singleLine">Is Eligible for VA Loan?</label>
            <input type="checkbox" id="aIsBVAElig" data-calc data-refresh alwaysDisabled/>
            <a class="explain" href="#" id="lnkHelpVALoanEligibilityB">Explain</a>
            <input class="Hidden" disabled="disabled" type="checkbox" id="aBIsVeteran" data-calc data-refresh alwaysDisabled />
            <input class="Hidden" disabled="disabled" type="checkbox" id="aBIsSurvivingSpouseOfVeteran" data-calc data-refresh alwaysDisabled />
        </div>
        <div class="floatRightField" id="aIsBVAFFExContainer">
            <label for="aIsBVAFFEx" class="singleLine">Is VA Funding Fee Exempt?</label>
            <input type="checkbox" id="aIsBVAFFEx" data-calc data-refresh />
        </div>
    </div>
    <div id="aBVServiceTContainer">
        <label for="aBVServiceT" class="singleLine">Service Type</label>
        <select id="aBVServiceT" data-calc data-refresh>
        </select>
        <img id="aBVServiceTValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div id="aBVEntContainer">
        <label for="aBVEnt" class="singleLine">Entitlement</label>
        <select id="aBVEnt" data-calc data-refresh>
        </select>
        <img id="aBVEntValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div id="sBCreditScoresContainer">
        <label class="singleLine">Credit Scores</label>
        XP: <input type="text" id="aBExperianScorePe" readonly="readonly" style="width:30px" data-calc />
        TU: <input type="text" id="aBTransUnionScorePe" readonly="readonly" style="width:30px" data-calc />
        EF: <input type="text" id="aBEquifaxScorePe" readonly="readonly" style="width:30px" data-calc />
    </div>
    <hr />
    <div>
        <label for="aBHasSpouse" class="singleLine">Has Co-Applicant</label>
        <input type="checkbox" id="aBHasSpouse" data-calc />
    </div>
    <hr />
</fieldset>
<fieldset id="CoApplicantInfo">
    <legend>Co-Applicant Info</legend>
        <div>
        <label for="aCFirstNm" class="singleLine">First Name</label>
        <input type="text" id="aCFirstNm" class="wide" data-calc />
        <img id="aCFirstNmValidator" class="validator" class="Validator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div>
        <label for="aCMidNm" class="singleLine">Middle Name</label>
        <input type="text" id="aCMidNm" class="wide" data-calc />
    </div>
    <div>
        <label for="aCLastNm" class="singleLine">Last Name</label>
        <input type="text" id="aCLastNm" class="wide" data-calc />
        <img id="aCLastNmValidator" class="validator" class="Validator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div>
        <label for="aCSuffix" class="singleLine">Suffix</label>
        <input type="text" id="aCSuffix" class="wide" data-calc />
    </div>
    <div>
        <label for="aCSsn" class="singleLine">SSN</label>
        <input type="text" id="aCSsn" preset="ssn" class="wide" data-calc />
        <img id="aCSsnValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div>
        <label for="aCEmail" class="singleLine">E-mail</label>
        <input type="text" id="aCEmail" class="wide" data-calc />
        <img id="aCEmailValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field" />
    </div>
    <div>
        <label for="aProdCCitizenT" class="singleLine">Citizenship</label>
        <select id="aProdCCitizenT" class="wide" data-calc>
        </select>
    </div>
    <div>
        <div class="floatLeftField">
            <label for="aCMonInc" class="singleLine">Monthly Income<br /><a class="explain" href="#" id="lnkHelpTotalIncomeC">Explain</a></label>
            <input type="text" id="aCMonInc" preset="money-allowblank-nocss" style="width:60px !important; vertical-align: top" data-calc />
            <img id="aCMonIncValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
        </div>
        <div class="floatRightField">
            <label for="aCIsSelfEmplmt" class="singleLine">Self Employed?</label>
            <input type="checkbox" id="aCIsSelfEmplmt" data-calc />
        </div>
    </div>
    <div>
        <div class="floatLeftField" id="aCTotalScoreIsFthbContainer">
            <label for="aCTotalScoreIsFthb" class="singleLine">First Time Home Buyer?</label>
            <input type="checkbox" id="aCTotalScoreIsFthb" data-calc />
            <a class="explain" href="#" id="lnkHelpHousingHistoryC">Explain</a>
        </div>
        <div class="floatRightField" id="aCHasHousingHistContainer">
            <label for="aCHasHousingHist" class="singleLine">Has Housing History?</label>
            <input type="checkbox" id="aCHasHousingHist" data-calc />
        </div>
    </div>
    <div>
        <div class="floatLeftField">
            <label for="aIsCVAElig" class="singleLine">Is Eligible for VA Loan?</label>
            <input type="checkbox" id="aIsCVAElig" data-calc alwaysDisabled/>
            <a class="explain" href="#" id="lnkHelpVALoanEligibilityC">Explain</a>
            <input class="Hidden" disabled="disabled" type="checkbox" id="aCIsVeteran" data-calc data-refresh alwaysDisabled />
            <input class="Hidden" disabled="disabled" type="checkbox" id="aCIsSurvivingSpouseOfVeteran" data-calc data-refresh alwaysDisabled />
        </div>
        <div class="floatRightField" id="aIsCVAFFExContainer">
            <label for="aIsCVAFFEx" class="singleLine">Is VA Funding Fee Exempt?</label>
            <input type="checkbox" id="aIsCVAFFEx" data-calc />
        </div>
    </div>
    <div id="aCVServiceTContainer">
        <label for="aCVServiceT" class="singleLine">Service Type</label>
        <select id="aCVServiceT" data-calc >
        </select>
        <img id="aCVServiceTValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div id="aCVEntContainer">
        <label for="aCVEnt" class="singleLine">Entitlement</label>
        <select id="aCVEnt" data-calc >
        </select>
        <img id="aCVEntValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    <div id="sCCreditScoresContainer">
        <label class="singleLine">Credit Scores</label>
        XP: <input type="text" id="aCExperianScorePe" readonly="readonly" style="width:30px" data-calc />
        TU: <input type="text" id="aCTransUnionScorePe" readonly="readonly" style="width:30px" data-calc />
        EF: <input type="text" id="aCEquifaxScorePe" readonly="readonly" style="width:30px" data-calc />
    </div>
    <hr />
</fieldset>
<fieldset id="FinancialData">
    <div>
        <label for="aTransmOMonPmtPe" class="singleLine">Total Payment</label>
        <input type="text" id="aTransmOMonPmtPe" readonly="readonly" preset="money-nocss" data-calc /> / month
    </div>
    <div style="margin-bottom: 1em; text-align:center">
        <a id="editPublicRecords" style="padding:4px" href="#" onclick="f_editPublicRecords();">Edit Public Records</a>
        <label id="missingPublicRecordsLabel" style="width:110px; margin-bottom: 0em">No Public Records</label>
        &nbsp;
        <a style="padding:4px" href="#" onclick="f_editLiability();">Edit Liabilities</a>
        &nbsp;&nbsp;
        <a id="viewCreditLink" href="#" onclick="f_viewCreditReport();">View Credit Report</a>
    </div>
    <div>
        <label for="aLiquidAssetsPe" class="singleLine">Liquid Assets</label>
        <input type="text" id="aLiquidAssetsPe" preset="money-allowblank-nocss" data-calc />
        <img id="aLiquidAssetsPeValidator" class="validator" src="../images/error_pointer.gif" alt="This is a required field"/>
        <a class="explain" href="#" id="lnkHelpLiquidAssets">Explain</a>
    </div>
    <div>
        <label for="aOpNegCfPe" class="singleLine" style="display: inline-block; vertical-align: middle">Negative Cash <br/>Flow from Other Properties</label>
        <input type="text" id="aOpNegCfPe" style="display: inline-block; vertical-align: middle" preset="money-allowblank-nocss" data-calc />
        <img id="aOpNegCfPeValidator" class="validator" style="display: inline-block; vertical-align: middle" src="../images/error_pointer.gif" alt="This is a required field"/>
        <a class="explain" id="lnkHelpNegCf" href="#">Explain</a>
    </div>
    <legend>Please select an option</legend>
    <div>
        <span id="UseCreditReportOnFilePanel">
            <input type="radio" id="UseCreditReportOnFile" name="CreditAction" value="UseCreditReportOnFile" />Use Credit Report on File
            <br />
        </span>
        <span id="OrderNewLine"><input type="radio" id="OrderNew" name="CreditAction" value="OrderNew" />Order New Credit Report<br /></span>
        <input type="radio" id="Reissue" name="CreditAction" value="Reissue" checked="checked" />Re-Issue Credit Report<br />
        <input type="radio" id="Upgrade" name="CreditAction" value="Upgrade" />Upgrade Existing Credit Report to Tri-Merge Report<br />
		<% if (m_canRunLpeWithoutCreditReport) { %>
            <input type="radio" id="ManualCredit" name="CreditAction" value="ManualCredit" />Manually Enter Credit Report<br />
		<% } %>
    </div>
    <div id="ManualCreditWarning">
        CAUTION: This will remove the existing credit report from the file.
    </div>
</fieldset>
<fieldset id="CreditProviderData">
    <legend>Credit Provider Information</legend>
    <div style="text-align:right">
    </div>
    <div style="padding-left: 1em; height: 2em;">
        <label for="CreditProtocol" class="singleLine" style="text-align: left">Credit Provider:</label>
        <a id="lnkHelpCreditProvider" href="#">Is my credit provider supported?</a>
    </div>
    <div class="padding-left-credit">
        <asp:DropDownList ID="CreditProtocol" runat="server"></asp:DropDownList>
    </div>
    <div style="padding-left: 1em; padding-top: 5px; height: 2em;">
        <label id="ReportIDLabel" for="ReportID" class="singleLine" style="padding-top:3px; text-align: left">Report ID:</label>
    </div>
    <div class="padding-left-credit">
        <input type="text" id="ReportID" readonly="readonly" />
    </div>
    <div>
        &nbsp;
    </div>
</fieldset>

<div id="loanManagement">
    <!-- 111817 - When the "Use Credit Report On File" option is selected, then the bottom left button in the application tab is hidden
        and the "Revise Property and Loan Info" button is centered. -->
    <button id="oCreditBtn" onclick="PML.save(); openCreditOrder(); return false;" type="button">
        <span class="oCreditBtnText">Order Credit</span>
        <img src="../images/warning15x15.png" class="WarningIcon" />
    </button>
    <button id="btnToPropAndLoanPg" onclick="window.scrollTo(0,0); DataInput.switchToPropertyAndLoanTab()" type="button">Revise Property & Loan Info</button>
    <div style="clear: both"></div>
</div>

<div class="DialogPopup" id="RemoveApplicationPopup">
    <span id="removeAppPopupMessage">Do you want to delete all data of borrower and spouse?</span> <br /> <br />
    <button id="removeAppOK">Yes</button>  <button id="removeAppCancel">No</button>
</div>

</div>
