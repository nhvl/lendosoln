﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportUpdateAssets.aspx.cs" Inherits="PriceMyLoan.webapp.ImportUpdateAssets" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asset Verification Review</title>
    <style type="text/css">
        span.material-icons {
            font-size: 36px;
        }
        .AccountInfo {
            display: flex;
            justify-content: center;
        }
        .AccountBox-ToBeRemoved {
            background-color: gainsboro;
        }
        .AccountBox-ToBeRemoved > div{
            text-decoration: line-through;
            text-decoration: line-through wavy darkred;
        }
        .AccountBox-ToBeRemoved > div:hover{
            background-color: gainsboro;
            text-decoration: none;
        }
        .AccountBox {
            min-width: 450px;
            border-top: 1px solid rgb(197,197,197);
            border-bottom: 1px solid rgb(197,197,197);
            padding: 5px;
            display: flex;
            line-height: 22px;
            margin-top: -1px; 
        }
        .AccountDescription {
            font-weight: bold;
            color: black;
            flex: 2 0 200px;
        }
        .AccountLabel-AccountType {
            font-weight: normal;
            font-style: italic;
        }
        .AccountValueContainer {
            font-weight: normal;
            color: black;
            flex-grow: 1;
            text-align: center;
            flex: 1 0 auto;
        }
        .AccountValue-Positive {
            color: green;
        }
        .AccountValue-Neutral {
            color: black;
        }
        .AccountValue-Negative {
            color: red;
        }
        .AccountValue-Used {
            font-weight: bold;
        }
        .AccountStatusMark {
            display: flex;
            align-items: center;
        }
        div.AccountStatusMark {
            text-decoration: none;
        }
        .UpdatedAccountIcon {
            color: green;
            flex: 0 0 auto;
            user-select: none;
        }
        .RemoveAccountIcon {
            color: red;
            flex: 0 0 auto;
            user-select: none;
            cursor: pointer;
        }
        .RemoveAccountIcon:hover {
            color: darkred;
        }
        .KeepAccountIcon {
            color: #505050;
            flex: 0 0 auto;
            user-select: none;
            cursor: pointer;
        }
        .KeepAccountIcon:hover {
            color: darkred;
        }
        .Summary {
            display: flex;
        }
        .indented {
            margin-left: 20px;
        }
        .SummaryContainer {
            flex: 1 0 49%;
            display: flex;
            justify-content: center;
        }
        .BorrowerContainer {
            border-right: 1px solid rgb(197,197,197);
        }
        .SummaryContainer > div {
            width: 50%;
            min-width: 200px;
        }
        .BorrowerValueSummary {
            display: flex;
        }
        .Confirmation {
            text-align: center;
            border-top: 1px solid rgb(197,197,197);
            padding-top: 5px;
        }
        .ConfirmationText {
            font-weight: bold;
            padding-bottom: 5px;
        }
    </style>
</head>
<body ng-controller="VoaImportAssets" ng-app="AssetImportUpdate">
<form id="form1" runat="server">
    <div lqb-popup>
        <div class="modal-header">
            <div class="ng-cloak">
                <header class="header modal-title">Asset Verification Review - {{borrowerName}}</header>
            </div>
        </div> 
        <div class="modal-body">
            <div class="Accounts scrollable">
                <div class="AccountInfo" ng-repeat="account in accounts">
                    <div class="AccountBox" ng-class="accountRowClass(account)">
                        <div class="AccountDescription">
                            <div ng-class="accountLabelClass(account)">{{account.accountLabel}}</div>
                            <div style="font-family: monospace">{{account.accountNumber}}</div>
                        </div>
                        <div class="AccountValueContainer">
                            Value on file:<br />
                            <div ng-show="account.valueOnFile" ng-class="getValueOnFileClass(account)">${{account.valueOnFile | number : 2}}</div>
                            <div ng-show="!account.valueOnFile" title="No value on file is available.">---</div>
                        </div>
                        <div class="AccountValueContainer">
                            Verified value:
			                <div ng-show="account.verifiedValue" ng-class="getUpdatedValueClass(account.valueOnFile, account.verifiedValue)">${{account.verifiedValue | number : 2}}</div>
                            <div ng-show="!account.verifiedValue" title="No verified value is available.">---</div>
                        </div>
                        <div class="AccountStatusMark">
                            <span ng-show="account.verifiedValue" class="material-icons UpdatedAccountIcon" title="This account will be updated.">check_circle</span>
                            <span ng-show="account.removeAccount" class="material-icons RemoveAccountIcon" title="This account will be removed. Click here to keep it." ng-click="toggleRemoveAccount(account)">remove_circle</span>
                            <span ng-show="!account.verifiedValue && !account.removeAccount" class="material-icons KeepAccountIcon" title="This account will be kept as it is. Click here to mark it for removal." ng-click="toggleRemoveAccount(account)">remove_circle_outline</span>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="StaticBottomSection">
                <div class="Summary">
                    <div class="SummaryContainer BorrowerContainer">
                        <div>
                            <div style="font-weight: bold;">{{borrowerName}}</div>
                            <div class="indented BorrowerValueSummary">
                                <div>Assets on File: </div>
                                <div>${{previousBorrowerTotal | number : 2}}</div>
                            </div>
                            <div class="indented BorrowerValueSummary">
                                <div>Updated value: </div>
                                <div ng-class="getUpdatedValueClass(previousBorrowerTotal, borrowerTotal)">${{borrowerTotal | number : 2}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="SummaryContainer">
                        <div>
                            <div style="font-weight: bold;">Loan File</div>
                            <div class="indented BorrowerValueSummary">
                                <div>Assets on File: </div>
                                <div>${{previousLoanTotal | number : 2}}</div>
                            </div>
                            <div class="indented BorrowerValueSummary">
                                <div>Updated value: </div>
                                <div ng-class="getUpdatedValueClass(previousLoanTotal, loanTotal)">${{loanTotal | number : 2}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="Confirmation">
                <div class="ConfirmationText">
                    Update asset records on file with the above changes?
                </div>
                <button type="button" class="btn btn-default" ng-click="exit()">Do Not Update</button>
                <button type="button" class="btn btn-default" ng-click="updateAssets()">Update Assets</button>
            </div>
        </div>    
    </div>
</form>
<script>
    var iframeSelf;
    jQuery(function ($) {
        var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
        iframeSelf = new IframeSelf();
        iframeSelf.popup = popup;

        if (!ML.VoaAllowImportingAssets) {
            iframeSelf.resolve({ NoAccess: true });
            return;
        }

        resizeForIE6And7(700, 500);

        TPOStyleUnification.Components();
    });

    var assetImportModule = angular.module('AssetImportUpdate', ['LqbForms']);
    assetImportModule.controller('VoaImportAssets', function ($scope, $window, LqbPopupService, gService, $location) {
        $scope.previousLoanTotal = $window.ML.previousLoanTotal;
        $scope.previousBorrowerTotal = $window.ML.previousBorrowerTotal;
        $scope.loanTotal = $window.ML.loanTotal;
        $scope.borrowerTotal = $window.ML.borrowerTotal;
        $scope.borrowerName = $window.ML.borrowerName;
        $scope.accounts = $window.accounts;

        $scope.accountRowClass = function(account) {
            if (account.removeAccount) {
                return 'AccountBox-ToBeRemoved';
            }
            else {
                return 'AccountBox-Normal';
            }
        };

        $scope.accountLabelClass = function (account) {
            if (account.accountLabelIsAccountType) {
                return 'AccountLabel-AccountType';
            }
            else return '';
        }

        // Bolds the value on file if it's going to be used
        $scope.getValueOnFileClass = function (account) {
            if (((typeof account.verifiedValue) !== 'number' || !account.verifiedValue) && !account.removeAccount) {
                return 'AccountValue-Used';
            }
            else return '';
        };

        $scope.getUpdatedValueClass = function (valueOnFile, verifiedValue) {
            var classes = [];
            if ((typeof verifiedValue) === 'number') {
                classes.push('AccountValue-Used');
            }

            if (!valueOnFile || valueOnFile < verifiedValue) {
                classes.push('AccountValue-Positive');
            }
            else if (valueOnFile > verifiedValue) {
                classes.push('AccountValue-Negative');
            }
            else {
                classes.push('AccountValue-Neutral');
            }

            return classes;
        };

        $scope.toggleRemoveAccount = function (account) {
            account.removeAccount = !account.removeAccount;
            $scope.getLoanTotals();
        };

        $scope.getLoanTotals = function () {
            var result = gService.voa.call("GetLoanTotals", { accountUpdates: angular.toJson($scope.accounts), loanId: $window.ML.sLId, appId: $window.ML.appId, orderId: $window.ML.orderId });
            if(result.error) {
                simpleDialog.alert(result.UserMessage, "Error");
            }
            else {
                if (result.value.totals && typeof (result.value.totals) === 'string') {
                    var totals = angular.fromJson(result.value.totals);
                    $scope.loanTotal = totals.loanTotal || $scope.loanTotal;
                    $scope.borrowerTotal = totals.borrowerTotal || $scope.borrowerTotal;
                }
            }
        };

        $scope.exit = function () {
            iframeSelf.resolve({});
        };

        $scope.updateAssets = function () {
            var result = gService.voa.call('UpdateAssets', { accountUpdates: angular.toJson($scope.accounts), loanId: $window.ML.sLId, appId: $window.ML.appId, orderId: $window.ML.orderId });
            if (result.error) {
                simpleDialog.alert(result.UserMessage, "Error");
            }
            else {
                if (result.value["Success"] !== 'True') {
                    simpleDialog.alert(result.value["Error"], "Error");
                    return;
                }

                iframeSelf.resolve({ Success: true, Added: result.value["Added"], Modified: result.value["Modified"], Removed: result.value["Removed"] });
            }
        };
    });
</script>
</body>
</html>
