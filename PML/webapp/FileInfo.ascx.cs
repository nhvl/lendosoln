﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PriceMyLoan.webapp
{
    public partial class FileInfo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put in the files that this control needs to include here
            var parent = this.Parent.Page as PriceMyLoan.UI.BasePage;
            parent.RegisterJsScript("webapp/fileinfo.js");
        }
    }
}