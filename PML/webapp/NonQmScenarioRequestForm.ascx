﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonQmScenarioRequestForm.ascx.cs" Inherits="PriceMyLoan.webapp.NonQmScenarioRequestForm" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<table id="ScenarioRequestFormTable">
    <tr>
        <td>
            <header class="page-header">Scenario Submission</header>
        </td>
    </tr>
    <tr class="selectedRateRow">
        <td>
            <div class="selectedRate">
            </div>
        </td>
    </tr>
    <tr>
        <td class="align-top">                    
            <div class="section">
                <header class="header">Borrower</header>
                <div class="loan-info-padding-left">
                    <div class="table table-bottom">                        
                        <div>                                        
                            <div>
                                <label>Borrower First Name<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <asp:TextBox ID="aBFirstNm" runat="server" onchange="validateScenarioRequestFormInput()"></asp:TextBox>
                        </div>
                        </div>
                        <div>
                            <div>
                                <label>Borrower Last Name<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <asp:TextBox ID="aBLastNm" runat="server" onchange="validateScenarioRequestFormInput()"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Credit Score<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <asp:TextBox ID="sCreditScoreEstimatePe" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id); validateScenarioRequestFormInput()" CssClass="credit-score-field"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Housing History</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sManuallyEnteredHousingHistory" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Housing Events</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sManuallyEnteredHousingEvents" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Bankruptcy</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sManuallyEnteredBankruptcy" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Citizenship</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="aProdBCitizenT" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Income Doc Type</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sProdDocT" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
        <td class="align-top">
            <div class="section">
                <header class="header">Loan</header>
                <div class="loan-info-padding-left">
                    <div class="table table-bottom">
                        <div>
                            <div>
                                <label>Loan Amount<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <ml:MoneyTextBox ID="sLAmtCalcPe" class="loan-amount" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id); validateScenarioRequestFormInput()"></ml:MoneyTextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>LTV %<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <ml:PercentTextBox ID="sLtvRPe" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id); validateScenarioRequestFormInput()"></ml:PercentTextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Loan Purpose</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sLPurposeT" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Occupancy</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sOccTPe" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Property Type</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sProdSpT" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id)"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Property State<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <ml:StateDropDownList ID="sSpStatePe" IncludeTerritories="false" runat="server" onfocus="storeOldValue(this.id)" onchange="invalidatePricingResult(this.id); validateScenarioRequestFormInput()"></ml:StateDropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <header class="header">Attach Documents</header>
            <div id="DragDropZoneContainer" class="InsetBorder full-width">
                <div id="DragDropZone_ScenarioRequest" class="drag-drop-container" extensions=".pdf" limit="10" ></div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <header class="header">Message to Lender:</header>
            <div class="box-section-border-red" id="box3-section10">
                <textarea cols="1" rows="1" id="MsgToLender" runat="server" placeholder="Add your comments or questions here..."></textarea>
                </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <button type="button" class="btn btn-flat" onclick="returnToQp(false)">Back</button>
            <button type="button" class="btn btn-default submitButton" onclick="onSubmit(false)">Submit</button>
        </td>
    </tr>
</table>