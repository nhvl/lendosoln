﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI.WebControls;
    using System.Xml;
    using global::DataAccess;
    using global::DataAccess.LoanValidation;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ObjLib.QuickPricer;
    using LendersOffice.RatePrice;
    using LendersOffice.RatePrice.Model;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Non-QM QuickPricer service page.
    /// </summary>
    public partial class QuickPricerNonQmService : BaseServiceJsonNetOptimized
    {
        /// <summary>
        /// Processes that you can call the service wtih.
        /// </summary>
        /// <param name="methodName">Method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "RunPricing":
                    RunPricing();
                    break;
                case "Submit":
                    Submit();
                    break;
                case "UploadDoc":
                    UploadDoc();
                    break;
                default:
                    Tools.LogBug(string.Format("Service method \"{0}\" not found!", methodName));
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Runs pricing and returns results to the page.
        /// </summary>
        private void RunPricing()
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            var brokerDB = principal.BrokerDB;

            if (!principal.IsNewPmlUIEnabled && !brokerDB.IsNewPmlUIEnabled)
            {
                throw new GenericUserErrorMessageException("PML 2.0 is not enabled.");
            }

            var nonQmQpData = LoadPricingData();

            Guid loanId = QuickPricer2LoanPoolManager.GetSandboxedUnusedLoanAndMarkAsInUse();            

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(loanId, typeof(QuickPricerNonQmService));

            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            dataLoan.RecalculateClosingFeeConditions(false); // OPM 140122.  

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            LoadLoanWithData(dataLoan, nonQmQpData);
            dataLoan.Save();

            LoanProgramByInvestorSet lpSet = dataLoan.GetLoanProgramsToRunLpe(principal, E_sLienQualifyModeT.ThisLoan, Guid.Empty, true, true);
            E_RenderResultModeT renderResultModeT = brokerDB.IsBestPriceEnabled ? E_RenderResultModeT.BestPricesPerProgram : E_RenderResultModeT.Regular;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(brokerDB);

            var requestId = DistributeUnderwritingEngine.SubmitToEngine(principal, loanId, lpSet, E_sLienQualifyModeT.ThisLoan, dataLoan.sProdLpePriceGroupId,
                    string.Empty /* tempLpeTaskMessageXml */, renderResultModeT, E_sPricingModeT.RegularPricing,
                    Guid.Empty, 0, 0, 0, 0, options);

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);

            if (resultItem == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
            }
            if (resultItem.IsErrorMessage)
            {
                throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
            }

            var isTotalIncomeZero = dataLoan.sLTotI == 0;
            var pricingStates = dataLoan.GetMatchingPricingStatesForScenario(false, false, Guid.Empty, string.Empty);
            var resultFormatter = ResultPricingFormatter.GetFormatter(brokerDB, principal, E_sPricingModeT.InternalBrokerPricing, dataLoan.sOriginatorCompensationPaymentSourceT, dataLoan.sOriginatorCompensationLenderFeeOptionT, pricingStates.ToList(), isTotalIncomeZero);
            ResultPricing result = resultFormatter.GetSingleResult(resultItem);

            this.UseJsonOptimization = true;
            SetResult("PricingResults", result);
            SetResult("ShowPrice", dataLoan.sPriceGroup.DisplayPmlFeeIn100Format);
            SetResult("HasNoResults", result.EligibleGroups.Count == 0 && result.IneligibleGroups.Count == 0);
            SetResult("LoanId", loanId);
        }

        /// <summary>
        /// Creates the lead from the QuickPricer loan.
        /// </summary>
        /// <param name="leadId">Lead id.</param>
        /// <returns>Lead conversion results.</returns>
        private LeadConversionResults CreateLead(Guid leadId)
        {
            var parameters = new LeadConversionParameters();
            parameters.LeadId = leadId;
            parameters.TemplateId = PrincipalFactory.CurrentPrincipal.BrokerDB.QuickPricerTemplateId;
            parameters.CreateFromTemplate = PrincipalFactory.CurrentPrincipal.BrokerDB.QuickPricerTemplateId != null && PrincipalFactory.CurrentPrincipal.BrokerDB.QuickPricerTemplateId != Guid.Empty;
            parameters.RemoveLeadPrefix = false;
            parameters.ConvertFromQp2File = true;
            parameters.ConvertToLead = true;
            parameters.IsEmbeddedPML = false;
            parameters.IsNonQm = true;

            return LeadConversion.ConvertLeadToLoan_Pml(PrincipalFactory.CurrentPrincipal, parameters);
        }

        /// <summary>
        /// Loads the pricing data from the UI.
        /// </summary>
        /// <returns>Non-QM QuickPricer data.</returns>
        private NonQmQpData LoadPricingData()
        {
            var nonQmQpData = new NonQmQpData();

            nonQmQpData.LAmtCalcPe = GetString("sLAmtCalcPe");
            nonQmQpData.LtvRPe = GetString("sLtvRPe");
            nonQmQpData.LPurposeT = GetEnum<E_sLPurposeT>("sLPurposeT");
            nonQmQpData.OccTPe = GetEnum<E_sOccT>("sOccTPe");
            nonQmQpData.ProdSpT = GetEnum<E_sProdSpT>("sProdSpT");
            nonQmQpData.SpStatePe = GetString("sSpStatePe");
            nonQmQpData.ManuallyEnteredHousingHistory = GetEnum<ManuallyEnteredHousingHistory>("sManuallyEnteredHousingHistory");
            nonQmQpData.ManuallyEnteredHousingEvents = GetEnum<ManuallyEnteredHousingEvents>("sManuallyEnteredHousingEvents");
            nonQmQpData.ManuallyEnteredBankruptcy = GetEnum<ManuallyEnteredBankruptcy>("sManuallyEnteredBankruptcy");
            nonQmQpData.ProdBCitizenT = GetEnum<E_aProdCitizenT>("aProdBCitizenT");
            nonQmQpData.ProdDocT = GetEnum<E_sProdDocT>("sProdDocT");

            int creditScoreEstimatePe;

            if (int.TryParse(GetString("sCreditScoreEstimatePe"), out creditScoreEstimatePe))
            {
                nonQmQpData.CreditScoreEstimatePe = creditScoreEstimatePe;
            }
            else
            {
                throw new CBaseException("Invalid credit score input.", "Invalid credit score input.");
            }

            if (string.IsNullOrWhiteSpace(nonQmQpData.LAmtCalcPe) || string.IsNullOrWhiteSpace(nonQmQpData.LtvRPe) || string.IsNullOrEmpty(nonQmQpData.SpStatePe))
            {
                throw new CBaseException("Required fields cannot be blank.", "Required fields cannot be blank.");
            }

            return nonQmQpData;
        }

        /// <summary>
        /// Loads the loan object with data from the UI.
        /// </summary>
        /// <param name="dataLoan">Data loan object.</param>
        /// <param name="nonQmQpData">Non QM QuickPricer data.</param>
        private void LoadLoanWithData(CPageData dataLoan, NonQmQpData nonQmQpData)
        {
            dataLoan.sLAmtCalcPe_rep = nonQmQpData.LAmtCalcPe;
            dataLoan.sLtvRPe_rep = nonQmQpData.LtvRPe;

            // calculate house val with ltv & loan amount
            var sLtvRPe = dataLoan.m_convertLos.ToRate(nonQmQpData.LtvRPe);
            var sLAmtCalcPe = dataLoan.m_convertLos.ToMoney(nonQmQpData.LAmtCalcPe);

            if (sLtvRPe != 0)
            {
                dataLoan.sHouseValPe_rep = dataLoan.m_convertLos.ToMoneyString(100M * sLAmtCalcPe / sLtvRPe, FormatDirection.ToRep);
            }

            dataLoan.sLPurposeT = nonQmQpData.LPurposeT;
            dataLoan.sOccTPe = nonQmQpData.OccTPe;
            dataLoan.sProdSpT = nonQmQpData.ProdSpT;
            dataLoan.sSpStatePe = nonQmQpData.SpStatePe;
            dataLoan.sCreditScoreEstimatePe_rep = nonQmQpData.CreditScoreEstimatePe.ToString();

            if (dataLoan.sHasAnyAppsFilled)
            {
                dataLoan.GetAppData(0).aBExperianScorePe = nonQmQpData.CreditScoreEstimatePe;
                dataLoan.GetAppData(0).aBEquifaxScorePe = nonQmQpData.CreditScoreEstimatePe;
                dataLoan.GetAppData(0).aBTransUnionScorePe = nonQmQpData.CreditScoreEstimatePe;
            }

            if (dataLoan.nApps != 0)
            {
                dataLoan.GetAppData(0).aProdBCitizenT = nonQmQpData.ProdBCitizenT;
            }

            dataLoan.sProdDocT = nonQmQpData.ProdDocT;

            switch (nonQmQpData.ManuallyEnteredHousingHistory)
            {
                case ManuallyEnteredHousingHistory._0x30:
                    dataLoan.sProdCrManualNonRolling30MortLateCount = 0;
                    dataLoan.sProdCrManual30MortLateCount = 0;
                    dataLoan.sProdCrManual60MortLateCount = 0;
                    dataLoan.sProdCrManualRolling60MortLateCount = 0;
                    dataLoan.sProdCrManual90MortLateCount = 0;
                    dataLoan.sProdCrManualRolling90MortLateCount = 0;
                    break;
                case ManuallyEnteredHousingHistory._1x30:
                    dataLoan.sProdCrManualNonRolling30MortLateCount = 1;
                    dataLoan.sProdCrManual30MortLateCount = 1;
                    dataLoan.sProdCrManual60MortLateCount = 0;
                    dataLoan.sProdCrManualRolling60MortLateCount = 0;
                    dataLoan.sProdCrManual90MortLateCount = 0;
                    dataLoan.sProdCrManualRolling90MortLateCount = 0;
                    break;
                case ManuallyEnteredHousingHistory._0x60:
                    dataLoan.sProdCrManualNonRolling30MortLateCount = 2;
                    dataLoan.sProdCrManual30MortLateCount = 2;
                    dataLoan.sProdCrManual60MortLateCount = 0;
                    dataLoan.sProdCrManualRolling60MortLateCount = 0;
                    dataLoan.sProdCrManual90MortLateCount = 0;
                    dataLoan.sProdCrManualRolling90MortLateCount = 0;
                    break;
                case ManuallyEnteredHousingHistory._0x90:
                    dataLoan.sProdCrManualNonRolling30MortLateCount = 0;
                    dataLoan.sProdCrManual30MortLateCount = 0;
                    dataLoan.sProdCrManual60MortLateCount = 2;
                    dataLoan.sProdCrManualRolling60MortLateCount = 2;
                    dataLoan.sProdCrManual90MortLateCount = 0;
                    dataLoan.sProdCrManualRolling90MortLateCount = 0;
                    break;
                case ManuallyEnteredHousingHistory._1x90Plus:
                    dataLoan.sProdCrManualNonRolling30MortLateCount = 0;
                    dataLoan.sProdCrManual30MortLateCount = 0;
                    dataLoan.sProdCrManual60MortLateCount = 0;
                    dataLoan.sProdCrManualRolling60MortLateCount = 0;
                    dataLoan.sProdCrManual90MortLateCount = 1;
                    dataLoan.sProdCrManualRolling90MortLateCount = 1;
                    break;
                default:
                    throw new UnhandledEnumException(nonQmQpData.ManuallyEnteredHousingHistory);
            }

            switch (nonQmQpData.ManuallyEnteredHousingEvents)
            {
                case ManuallyEnteredHousingEvents.None:
                    dataLoan.sProdCrManualForeclosureHas = false;
                    break;
                case ManuallyEnteredHousingEvents.Within12Months:
                    dataLoan.sProdCrManualForeclosureHas = true;
                    dataLoan.sProdCrManualForeclosureRecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualForeclosureRecentFileYr = DateTime.Today.AddMonths(-1).Year;
                    dataLoan.sProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
                    break;
                case ManuallyEnteredHousingEvents.Within24Months:
                    dataLoan.sProdCrManualForeclosureHas = true;
                    dataLoan.sProdCrManualForeclosureRecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualForeclosureRecentFileYr = DateTime.Today.AddMonths(-1).AddYears(-1).Year;
                    dataLoan.sProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
                    break;
                case ManuallyEnteredHousingEvents.Within36Months:
                    dataLoan.sProdCrManualForeclosureHas = true;
                    dataLoan.sProdCrManualForeclosureRecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualForeclosureRecentFileYr = DateTime.Today.AddMonths(-1).AddYears(-2).Year;
                    dataLoan.sProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
                    break;
                case ManuallyEnteredHousingEvents.Within48Months:
                    dataLoan.sProdCrManualForeclosureHas = true;
                    dataLoan.sProdCrManualForeclosureRecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualForeclosureRecentFileYr = DateTime.Today.AddMonths(-1).AddYears(-3).Year;
                    dataLoan.sProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
                    break;
                default:
                    throw new UnhandledEnumException(nonQmQpData.ManuallyEnteredHousingEvents);
            }

            switch (nonQmQpData.ManuallyEnteredBankruptcy)
            {
                case ManuallyEnteredBankruptcy.None:
                    dataLoan.sProdCrManualBk7Has = false;
                    break;
                case ManuallyEnteredBankruptcy.Within12Months:
                    dataLoan.sProdCrManualBk7Has = true;
                    dataLoan.sProdCrManualBk7RecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentFileYr = DateTime.Today.AddMonths(-1).Year;
                    dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
                    dataLoan.sProdCrManualBk7RecentSatisfiedMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentSatisfiedYr = DateTime.Today.AddMonths(-1).Year;
                    break;
                case ManuallyEnteredBankruptcy.Within24Months:
                    dataLoan.sProdCrManualBk7Has = true;
                    dataLoan.sProdCrManualBk7RecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentFileYr = DateTime.Today.AddMonths(-1).AddYears(-1).Year;
                    dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
                    dataLoan.sProdCrManualBk7RecentSatisfiedMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentSatisfiedYr = DateTime.Today.AddMonths(-1).AddYears(-1).Year;
                    break;
                case ManuallyEnteredBankruptcy.Within36Months:
                    dataLoan.sProdCrManualBk7Has = true;
                    dataLoan.sProdCrManualBk7RecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentFileYr = DateTime.Today.AddMonths(-1).AddYears(-2).Year;
                    dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
                    dataLoan.sProdCrManualBk7RecentSatisfiedMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentSatisfiedYr = DateTime.Today.AddMonths(-1).AddYears(-2).Year;
                    break;
                case ManuallyEnteredBankruptcy.Within48Months:
                    dataLoan.sProdCrManualBk7Has = true;
                    dataLoan.sProdCrManualBk7RecentFileMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentFileYr = DateTime.Today.AddMonths(-1).AddYears(-3).Year;
                    dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
                    dataLoan.sProdCrManualBk7RecentSatisfiedMon = DateTime.Today.AddMonths(-1).Month;
                    dataLoan.sProdCrManualBk7RecentSatisfiedYr = DateTime.Today.AddMonths(-1).AddYears(-3).Year;
                    break;
                default:
                    throw new UnhandledEnumException(nonQmQpData.ManuallyEnteredBankruptcy);
            }
        }

        /// <summary>
        /// Submits the data and creates the lead, registers the program, attaches EDOCs, sends message to the conversation log, and email, if appropriate.
        /// </summary>
        private void Submit()
        {
            var containsPricingResults = GetBool("ContainsPricingResults");
            var IsPricingResultsValid = GetBool("IsPricingResultsValid");
            var isBankStatement = GetBool("IsBankStatement");
            var isIneligible = GetBool("IsIneligible");

            NonQmQpData nonQmQpData = null;

            if (containsPricingResults || !isBankStatement)
            {
                nonQmQpData = LoadPricingData();
            }
            else
            {
                nonQmQpData = new NonQmQpData();
            }

            nonQmQpData.BFirstNm = GetString("aBFirstNm");
            nonQmQpData.BLastNm = GetString("aBLastNm");

            if (string.IsNullOrWhiteSpace(nonQmQpData.BFirstNm) && string.IsNullOrWhiteSpace(nonQmQpData.BLastNm))
            {
                SetResult("ErrorMessage", "Borrower names cannot be blank.");
                return;
            }

            nonQmQpData.MsgToLender = GetString("MsgToLender");

            if (isBankStatement)
            {
                nonQmQpData.IsSelfEmployed = GetBool("sIsSelfEmployed");
            }

            if (containsPricingResults)
            {
                nonQmQpData.LoanId = GetGuid("LoanId");
                nonQmQpData.LpTemplateId = GetGuid("lLpTemplateId");
                nonQmQpData.Rate = GetRate("Rate");
                nonQmQpData.Point = GetRate("Point");
                nonQmQpData.RateOptionId = GetString("RateOptionId");
            }

            var principal = PrincipalFactory.CurrentPrincipal;
            Guid leadId = new Guid();

            if (containsPricingResults)
            {
                var leadResult = CreateLead(nonQmQpData.LoanId);
                leadId = leadResult.ConvertedLoanId;
            }
            else
            {
                // lead creation from blank template
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.LeadCreate);
                leadId = creator.CreateLead(templateId: Guid.Empty);
            }
            CPageData dataLead = CPageData.CreatePmlPageDataUsingSmartDependency(leadId, typeof(QuickPricerNonQmService));

            dataLead.InitSave();

            dataLead.GetAppData(0).aBFirstNm = nonQmQpData.BFirstNm;
            dataLead.GetAppData(0).aBLastNm = nonQmQpData.BLastNm;

            LoadLoanWithData(dataLead, nonQmQpData);

            dataLead.sIsUsingNonQMProgram = true;
            dataLead.sWasCreatedThroughNonQmPortal = true;

            dataLead.Save();

            if (containsPricingResults && IsPricingResultsValid && !isIneligible)
            {
                try
                {
                    DistributeUnderwritingEngine.RegisterLoanProgram(principal, leadId, nonQmQpData.LpTemplateId, nonQmQpData.Rate, nonQmQpData.Point, nonQmQpData.RateOptionId, false);
                }
                catch (CBaseException e)
                {
                    SetResult("RegisterErrorMessage", e.UserMessage.Equals(ErrorMessages.Generic) ? "Error while registering the loan." : e.UserMessage);
                    Tools.LogError(e);
                }
            }            

            if (principal.BrokerDB.EnableConversationLogForLoans)
            {
                try
                {
                    NonQmQpHelper.PostConLogMsgForNonQmQP(principal, nonQmQpData, dataLead.sLRefNm);
                }
                catch (CBaseException e)
                {
                    SetResult("ConversationLogErrorMessage", e.UserMessage.Equals(ErrorMessages.Generic) ? "Error while sending a message to the conversation log." : e.UserMessage);
                    Tools.LogError(e);
                }
                catch (LqbException e)
                {
                    SetResult("ConversationLogErrorMessage", "Error while sending a message to the conversation log.");
                    Tools.LogError(e);
                }
            }            

            try
            {
                NonQmQpHelper.SendEmailToConfiguredAddrForNonQmQp(principal, nonQmQpData, dataLead.sLNm);
            }
            catch (CBaseException e)
            {
                SetResult("EmailErrorMessage", e.UserMessage.Equals(ErrorMessages.Generic) ? "Error while sending an email to the designated address." : e.UserMessage);
                Tools.LogError(e);
            }

            SetResult("sLNm", dataLead.sLNm);
            SetResult("sLId", dataLead.sLId);
        }

        private void UploadDoc()
        {
            var leadId = GetGuid("LeadId");
            CPageData dataLead = CPageData.CreatePmlPageDataUsingSmartDependency(leadId, typeof(QuickPricerNonQmService));
            dataLead.InitLoad();

            var base64File = GetString("Base64Content");
            var fileName = GetString("FileName");

            Guid appId = Guid.Empty;

            if (dataLead.nApps > 0)
            {
                appId = dataLead.GetAppData(0).aAppId;
            }

            try
            {                
                EDocs.EDocsUpload.UploadDocFromNonQm(PrincipalFactory.CurrentPrincipal, fileName, base64File, dataLead.sLId, appId);
            }
            catch (CBaseException e)
            {
                if (e.UserMessage.Equals(ErrorMessages.Generic))
                {
                    SetResult("EDocErrorMessage", $"Error while uploading file: {fileName}.");
                }
                else if (e.UserMessage.Equals(ErrorMessages.EDocs.InvalidPDF)) {
                    SetResult("EDocErrorMessage", $"File {fileName} failed to upload because it is an invalid PDF.");
                }
                else
                {
                    SetResult("EDocErrorMessage", e.UserMessage);
                }

                Tools.LogError(e);
            }
        }
    }
}