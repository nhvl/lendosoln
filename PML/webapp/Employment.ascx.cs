﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using DataAccess;
using LendersOffice.Common;

namespace PriceMyLoan.webapp
{
    public partial class Employment : PriceMyLoan.UI.BaseUserControl
    {
        protected void PageInit()
        { 
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //get the employment records for both the borrower and co-borrower
            //just pass it in through the parent?
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(RequestHelper.GetGuid("applicationid", Guid.Empty));

            IEmpCollection borrowerRecordList = dataApp.aBEmpCollection;
            IEmpCollection coBorrowerRecordList = dataApp.aCEmpCollection;

            var borrowerName = dataApp.aBFirstNm + " " + dataApp.aBLastNm;
            var coborrowerName = dataApp.aCFirstNm + " " + dataApp.aCLastNm;
            if (borrowerName.Equals(" "))
                borrowerName = "Borrower";
            if (coborrowerName.Equals(" "))
                coborrowerName = "Co-Borrower";

            employTab1.InnerText = borrowerName;
            employTab2.InnerText = coborrowerName;

            aBPrimaryEmplrZip.SmartZipcode(aBPrimaryEmplrCity, aBPrimaryEmplrState);
            aCPrimaryEmplrZip.SmartZipcode(aCPrimaryEmplrCity, aCPrimaryEmplrState);
        }
    }
}