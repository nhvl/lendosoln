﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pml.aspx.cs" Inherits="PriceMyLoan.webapp.pml" %>
<%@ Register TagPrefix="pml" TagName="FileInfo" Src="~/webapp/FileInfo.ascx" %>
<%@ Register TagPrefix="pml" TagName="Action" Src="~/webapp/Action.ascx" %>
<%@ Register TagPrefix="pml" TagName="ResultsFilter" Src="~/webapp/ResultsFilter.ascx" %>
<%@ Register TagPrefix="pml" TagName="DataInput" Src="~/webapp/DataInput.ascx" %>
<%@ Register TagPrefix="pml" TagName="Results" Src="~/webapp/Results.ascx" %>
<%@ Register TagPrefix="pml" TagName="Disclaimer" Src="~/webapp/Disclaimer.ascx" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<%@ Register TagPrefix="pml" TagName="QualifyingBorrower" Src="~/main/QualifyingBorrower.ascx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script type="text/javascript"> 
        if (typeof console == "undefined") {
            this.console = {log: function() {}}
        }

        $(function () {
            if (ML.IsUlad2019) {
                initializeQualifyingBorrower(true, null, true);
                var $trueContainer = $('.true-container');
                $trueContainer.attr('class', $trueContainer.attr('class') + ' wrap');
            }
            else {
                $('.qualifying-borrower-container').remove();
            }
        });
    </script>
    <title>Price My Loan</title>
    
</head>
<body id="Body" style="background-color: #EEE;" runat="server" class="pml-body">
    <style>
        .Mask
        {
            width: 100%;
            height: 100%;
            z-index: 200; 
            opacity: .3;
            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
            filter: alpha(opacity=30); 
            background-color: #333333; 
            position:absolute;
            top: 0;
            left: 0;
        }

        .center-block {
            margin-left: auto;
            margin-right: auto;
        }

        input[readonly]
        {
            background-color: rgb(238,238,238);
        }
    </style>

    <script language="javascript" type="text/javascript">
        function f_displayEditMask(bDisplay) {
            var oRightBlockMask = document.getElementById('rightBlockMask');
            var oTopBlockMask = document.getElementById('topBlockMask');
            var isDisplayed = oRightBlockMask.style.display == '' && oTopBlockMask.style.display == '';
            
            if (!isDisplayed && bDisplay) {
                oRightBlockMask.style.display = '';
                oTopBlockMask.style.display = '';
                f_disabledInputForBlockMask()
            } else if (isDisplayed && !bDisplay) {
                oRightBlockMask.style.display = 'none';
                oTopBlockMask.style.display = 'none';
                f_enabledInputForBlockMask()
            }
        }
    </script>
    
    <form id="form1" runat="server">
    <asp:HiddenField ID="data" runat="server" />
    <div id="PMLLoadingScreen">
        <div class="loading-content">
            <img class="img-loading" src="../images/loading.gif" alt="Loading..."/>
            <h3 class="text-loading">PriceMyLoan is loading...</h3>
        </div>
    </div>
    <%-- Layout should go in this file --%>
    <%-- GRID LAYOUT --%>
    <button runat="server" type="button" id="UiTypeSwitchEmbeddedPml" class="ui-type-switch">Switch UI</button>
    <asp:PlaceHolder runat="server" ID="HeaderInfo">
    <div id="header" class="gridcontainer" style="padding-bottom: 5px;">
        <div class="dynamicrow" runat="server" id="HeaderSpacerRow">
            <div class="twelvecol last">
                &nbsp;
            </div>
        </div>
        <div class="dynamicrow">
            <div class="fourcol loanOfficerAssignment">
                <div class="agentsContainer">
                    <div>
                        Loan Officer:
                        <span id="LoanOfficer_FullName"></span>
                        (<a href="#" id="assignLoanOfficerLink">assign</a>)
                    </div>
                </div>
            </div>
            <div class="fourcol processorAssignment">
                <div class="agentsContainer">
                    <div class="hideIfEmbedded">
                        Processor:
                        <span id="Processor_FullName"></span>
                        (<a href="#" id="assignProcessorLink">assign</a>)
                    </div>
                </div>
            </div>
            <div class="fourcol last messageAndBackButtonContainer" style="text-align:right;">
                <div id="messageBtnContainer" runat="server" class="messageBtnContainer" style="display:inline;">
                    <img id="MessageImg" alt="Message to Lender" src="../images/MessageIcon.png" />
                    <a id="messageBtn" href="#">Message To Lender</a>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="backBtnContainer">
                    <img class="backBtnImg" alt="Go back" src="../images/GoBackIcon_transparent.png" />
                    <a class="backBtn" href="#">Go back to LendingQB / Go back to Loan Pipeline</a>
                </div>
            </div>
        </div>
        <div class="dynamicrow secondaryAndPostCloserAssignment">
            <div class="fourcol secondaryAssignment">
                <div class="agentsContainer">
                    <div>
                        <ml:EncodedLiteral runat="server" ID="SecondaryEmployeeLabel">Secondary</ml:EncodedLiteral>:
                        <span id="Secondary_FullName"></span>
                        (<a href="#" id="assignSecondaryLink">assign</a>)
                    </div>
                </div>
            </div>
            <div class="fourcol postCloserAssignment">
                <div class="agentsContainer">
                    <div class="hideIfEmbedded">
                        Post-Closer:
                        <span id="PostCloser_FullName"></span>
                        (<a href="#" id="assignPostCloserLink">assign</a>)
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gridcontainer">
        <div class="dynamicrow">
            <div class="twelvecol last">
                <pml:FileInfo ID="FileInfo1" runat="server"></pml:FileInfo>
            </div>
        </div>
    </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="HeaderForTPOQP2" Visible="false">
        <div class="dynamicrow hide-within-pipeline" runat="server">
            <div class="fourcol">
                <div>
                    <div>
                        
                    </div>
                </div>
            </div>
            <div class="fourcol">
                <div>
                    <div>
                        
                    </div>
                </div>
            </div>
            <div class="fourcol last" style="text-align:right;">
                <div>
                    
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="backBtnContainer">
                    <img class="backBtnImg" alt="Go back" src="../images/GoBackIcon_transparent.png" />
                    <a class="backBtn" href="#">Go back to LendingQB / Go back to Loan Pipeline</a>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />
    <div class="gridcontainer content-detail" name="content-detail" style="position:relative">
        <div class="row content-container center-block">
            <div class="warp-section">
                <header class="page-header" id="Header2017UI" runat="server">
                    <span runat="server" id="Header2017Title">Pricing</span>
                    <button runat="server" type="button" id="UiTypeSwitchRetailPortal" class="ui-type-switch">Switch UI</button>
                </header>
                <div id="ContentHeader" runat="server">
                    <div ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>
                </div>
            </div>
            <div class="warp-section table">
                <div class=" table-row">
                    <asp:PlaceHolder runat="server" ID="Pml3RetailModeSpacer">
                        <br />
                    </asp:PlaceHolder>
                    <div class="fourcol left-panel" style="position: relative;">
                        <div id="rightBlockMask" class="Mask" style="display: none;"></div>
                        <pml:Action ID="Action1" runat="server" />
                        <pml:DataInput ID="DataInput1" runat="server" />
                    </div>
                    <div class="eightcol right-panel">
                        <div style="position: relative;">
                            <div id="topBlockMask" class="Mask" style="display: none;"></div>
                            <pml:ResultsFilter ID="ResultsFilter1" runat="server" />
                            <pml:Disclaimer ID="Disclaimer1" runat="server" />
                        </div>

                        <pml:Results ID="Results1" runat="server" />

                        <div runat="server" id="PricingResultsContainer" ng-controller="pricingResultsController">
                            <div id="ResultsContainer"></div>
                            <div id="LoadingMessage">
                                <div class="message-container">
                                    <img id="resultsLoadingGif" src="../images/loading.gif" />
                                    <p>Loading...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="Hidden">
            <pml:QualifyingBorrower runat="server" ID="QualifyingBorrower" />
        </div>
    </form>
</body>
</html>

