﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;

namespace PriceMyLoan.webapp
{
    public partial class DataInput : PriceMyLoan.UI.BaseUserControl
    {
        private bool m_IsQP2File;
        public bool IsQP2File
        {
            get
            {
                return m_IsQP2File;
            }
            set
            {
                m_IsQP2File = value;
                LoanFilePricer.IsQP2File = value;
            }
        }
        public bool IsLineOfCredit
        {
            set
            {
                LoanFilePricer.IsLineOfCredit = value;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DataInput));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Put in the files that this control needs to include here
            var page = this.Page as PriceMyLoan.UI.BasePage;
            page.RegisterJsScript("webapp/datainput.js");

        }
    }
}