﻿namespace PriceMyLoan.webapp
{
    using System;
    using global::DataAccess;
    using LendersOffice.ObjLib.QuickPricer;
    using PriceMyLoan.UI;

    /// <summary>
    /// Non-QM Scenario Request Form user control.
    /// </summary>
    public partial class NonQmScenarioRequestForm : BaseUserControl
    {
        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event args.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Tools.Bind_ManuallyEnteredBankruptcy(sManuallyEnteredBankruptcy);
            Tools.Bind_ManuallyEnteredHousingEvents(sManuallyEnteredHousingEvents);
            Tools.Bind_ManuallyEnteredHousingHistory(sManuallyEnteredHousingHistory);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sOccT(sOccTPe);
            Tools.Bind_sProdSpT(sProdSpT);
            Tools.Bind_sProdDocT(sProdDocT, PriceMyLoanUser.BrokerDB, E_sProdDocT.Full);
            Tools.Bind_aProdBCitizenT(aProdBCitizenT);

            Tools.SetDropDownListValue(sLPurposeT, E_sLPurposeT.Purchase);
            Tools.SetDropDownListValue(sOccTPe, E_sOccT.PrimaryResidence);
            Tools.SetDropDownListValue(sProdSpT, E_sProdSpT.SFR);
            Tools.SetDropDownListValue(sManuallyEnteredHousingHistory, ManuallyEnteredHousingHistory._0x30);
            Tools.SetDropDownListValue(sManuallyEnteredHousingEvents, ManuallyEnteredHousingEvents.None);
            Tools.SetDropDownListValue(sManuallyEnteredBankruptcy, ManuallyEnteredBankruptcy.None);
            Tools.SetDropDownListValue(sProdDocT, E_sProdDocT.Full);
        }
    }
}