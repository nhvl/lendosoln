<%@ Page language="c#" Codebehind="RolodexListV2.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.webapp.RolodexListV2" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Contacts</title>
    <style>[ng-click]{ cursor: pointer;}</style>
</head>
<body class="EditBackground modal-iframe-body">
<div lqb-popup class="modal-rolodex" hidden>
    <div ng-bootstrap="RolodexList" rolodex-list></div>
</div>
<form method="post" runat="server">
    <uc1:cmodaldlg id="CModalDlg1" runat="server"/>
</form>
</body>
</html>
