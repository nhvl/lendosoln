﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EDocs;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.Templates;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    public partial class OrderVOAService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadLenderService":
                    this.LoadLenderService();
                    break;
                case "PollOrder":
                    this.SendSubsequentRequest(VOXRequestT.Get);
                    break;
                case "RefreshOrder":
                    this.SendSubsequentRequest(VOXRequestT.Refresh);
                    break;
                case "PollForSubsequentResults":
                    this.PollForSubsequentResults();
                    break;
                case "RunAudit":
                    this.RunAudit();
                    break;
                case "RunRequest":
                    this.RunRequest();
                    break;
                case "PollForInitialResult":
                    this.PollForInitialResult();
                    break;
                case "UpdateAssets":
                    this.UpdateAssets();
                    break;
                case nameof(this.GetLoanTotals):
                    this.GetLoanTotals();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Update the assets on the loan file.
        /// </summary>
        private void UpdateAssets()
        {
            var loan = CPageData.CreateUsingSmartDependency(GetGuid("loanId"), typeof(OrderVOAService));
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            var assets = loan.GetAppData(GetGuid("appId")).aAssetCollection;

            var updateJson = GetString("accountUpdates");
            var accountUpdates = SerializationHelper.JsonNetDeserialize<List<AssetImport.AccountUpdateFromClient>>(updateJson);
            var voaOrderId = GetInt("orderId");
            var order = VOAOrder.GetOrderForLoan(loan.sBrokerId, loan.sLId, voaOrderId);

            var result = AssetImport.UpdateAssets(PrincipalFactory.CurrentPrincipal, loan, assets, accountUpdates, order);

            if (result.HasError)
            {
                Tools.LogError(result.Error);
                this.SetResult("Error", ErrorMessages.Generic);
            }
            else
            {
                loan.Save();
                this.SetResult("Success", true);
                this.SetResult("Added", result.Value.Count(update => update.UpdateT == LendersOffice.Audit.AssetImportAuditItem.UpdateType.New));
                this.SetResult("Modified", result.Value.Count(update => update.UpdateT == LendersOffice.Audit.AssetImportAuditItem.UpdateType.Modify));
                this.SetResult("Removed", result.Value.Count(update => update.UpdateT == LendersOffice.Audit.AssetImportAuditItem.UpdateType.Remove));
            }
        }

        /// <summary>
        /// Gets the totals of borrower and loan assets for displaying.
        /// </summary>
        private void GetLoanTotals()
        {
            var loan = CPageData.CreateUsingSmartDependencyForLoad(GetGuid("loanId"), typeof(OrderVOAService));
            var app = loan.GetAppData(GetGuid("appId"));

            var updateJson = GetString("accountUpdates");
            var accountUpdates = SerializationHelper.JsonNetDeserialize<List<AssetImport.AccountUpdateFromClient>>(updateJson);
            var voaOrderId = GetInt("orderId");
            var order = VOAOrder.GetOrderForLoan(loan.sBrokerId, loan.sLId, voaOrderId);

            var assetIdsBeingRemoved = new HashSet<Guid>(accountUpdates.Where(update => update.AssetId != null && update.RemoveAccount).Select(update => update.AssetId.Value));
            var result = AssetImport.GetAssetTotals(assetIdsBeingRemoved, loan, app, order);

            this.SetResult("totals", SerializationHelper.JsonNetSerialize(result));
        }

        private void RunRequest()
        {
            ServiceCredential usedServiceCredential;
            VOARequestData requestData = GetVOARequestData(out usedServiceCredential);
            if (requestData == null)
            {
                return;
            }

            VOARequestHandler requestHandler = new VOARequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            if (auditResults.HasErrors)
            {
                List<string> errorsFromVendor = new List<string>()
                {
                    "Invalid data. Please run the audit again."
                };

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsFromVendor));
                return;
            }

            if (ConstStage.DisableVoaViaBJP)
            {
                var result = requestHandler.SubmitRequest(doAudit: false);
                IEnumerable<string> errors = result?.Errors;
                var initialOrders = result?.Orders;
                if (initialOrders == null || !initialOrders.Any() ||
                    (errors != null && errors.Any()))
                {
                    List<string> requestErrors = new List<string>() { "Unable to fulfill order request." };
                    if (errors != null && errors.Any())
                    {
                        requestErrors = errors.ToList();
                    }

                    SetResult("Success", false);
                    SetResult("Errors", SerializationHelper.JsonNetSerialize(requestErrors));
                    return;
                }

                List<VOAOrderViewModel> orderViewModels = new List<VOAOrderViewModel>();
                List<Guid> edocIdsToStitch = new List<Guid>();

                ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
                {
                    RequiresAccountId = requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId,
                    UsesAccountId = requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId,
                    AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(requestData.LoanId)
                };

                if (usedServiceCredential != null)
                {
                    extraInfo.ServiceCredentialId = usedServiceCredential.Id;
                    extraInfo.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(usedServiceCredential.AccountId);
                }

                foreach (VOAOrder order in initialOrders)
                {
                    VOAOrderViewModel model = order.CreateViewModel(extraInfo);
                    orderViewModels.Add(model);

                    if (model.AssociatedEdocs.Any())
                    {
                        edocIdsToStitch.AddRange(model.AssociatedEdocs.Where((edoc) => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select((edoc) => edoc.EDocId));
                    }
                }

                this.SetResult("Orders", SerializationHelper.JsonNetSerialize(orderViewModels));
                this.SetResult("Success", true);

                if (edocIdsToStitch.Any())
                {
                    var joinedIds = string.Join("|", edocIdsToStitch);
                    var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                    SetResult("Key", key);
                }
            }
            else
            {
                Guid publicJobId = requestHandler.SubmitToProcessor();
                this.SetResult("Success", true);
                this.SetResult("PublicJobId", publicJobId.ToString());
                this.SetResult("PollingIntervalInSeconds", ConstStage.VoaBJPPollingIntervalInSeconds);
                this.SetResult("RequiresAccountId", requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId);
                this.SetResult("UsesAccountId", requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId);
                this.SetResult("ServiceCredentialId", usedServiceCredential?.Id);
                this.SetResult("ServiceCredentialHasAccountId", !string.IsNullOrEmpty(usedServiceCredential?.AccountId));
            }
        }

        /// <summary>
        /// Polls the processor for job results.
        /// </summary>
        private void PollForInitialResult()
        {
            Guid publicJobId = this.GetGuid("PublicJobId");
            int attemptNumber = this.GetInt("AttemptNumber");
            Guid loanId = this.GetGuid("LoanId");
            int maxPollingAttempts = ConstStage.VoaBJPMaxPollingAttempts;

            var results = VOARequestHandler.CheckRequest(publicJobId, loanId, PrincipalFactory.CurrentPrincipal.BrokerId);

            IEnumerable<string> errors = null;
            IEnumerable<VOAOrder> orders = null;
            if (results.Status == LendersOffice.Integration.IntegrationJobStatus.Processing)
            {
                if (++attemptNumber > maxPollingAttempts)
                {
                    errors = new List<string>() { "VOA request has timed out." };
                }
                else
                {
                    this.SetResult("Success", true);
                    this.SetResult("PublicJobId", publicJobId);
                    return;
                }
            }
            else if (results.Status == LendersOffice.Integration.IntegrationJobStatus.Error)
            {
                errors = results.Errors;
            }
            else
            {
                orders = results.Orders;
            }

            if (orders == null || !orders.Any() || (errors != null && errors.Any()))
            {
                IEnumerable<string> requestErrors = new List<string>() { "Unable to fulfill order request." };
                if (errors != null && errors.Any())
                {
                    requestErrors = errors;
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetAnonymousSerialize(requestErrors));
                return;
            }

            List<VOAOrderViewModel> orderViewModels = new List<VOAOrderViewModel>();
            List<Guid> edocIdsToStitch = new List<Guid>();

            ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
            {
                RequiresAccountId = this.GetBool("RequiresAccountId"),
                UsesAccountId = this.GetBool("UsesAccountId"),
                AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(loanId),
                ServiceCredentialId = this.GetInt("ServiceCredentialId", -1),
                ServiceCredentialHasAccountId = this.GetBool("ServiceCredentialHasAccountId", false),
            };

            foreach (VOAOrder order in orders)
            {
                VOAOrderViewModel model = order.CreateViewModel(extraInfo);
                orderViewModels.Add(model);

                if (model.AssociatedEdocs.Any())
                {
                    edocIdsToStitch.AddRange(model.AssociatedEdocs.Where((edoc) => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select((edoc) => edoc.EDocId));
                }
            }

            this.SetResult("Orders", SerializationHelper.JsonNetSerialize(orderViewModels));
            this.SetResult("Success", true);
            if (edocIdsToStitch.Any())
            {
                var joinedIds = string.Join("|", edocIdsToStitch);
                var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                SetResult("Key", key);
            }
        }

        private void SendSubsequentRequest(VOXRequestT type)
        {
            string accountId = this.GetString("AccountId", string.Empty);
            string username = this.GetString("Username", string.Empty);
            string password = this.GetString("Password", string.Empty);
            Guid loanId = this.GetGuid("LoanId");
            int orderId = this.GetInt("OrderId");

            VOAOrder previousOrder = VOAOrder.GetOrderForLoan(PrincipalFactory.CurrentPrincipal.BrokerId, loanId, orderId);
            if (previousOrder == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { "Order not found." }));
                return;
            }

            if (previousOrder.UpdateForLenderService() && previousOrder.CurrentStatus == VOXOrderStatusT.Error)
            {
                VOAOrderViewModel errorModel = previousOrder.CreateViewModel(null);
                SetResult("UpdatedOrder", SerializationHelper.JsonNetSerialize(errorModel));
                SetResult("Success", true);
                return;
            }

            VOARequestData requestData = new VOARequestData(PrincipalFactory.CurrentPrincipal, loanId, type, previousOrder)
            {
                AccountId = accountId,
                Username = username,
                Password = password
            };

            var principal = PrincipalFactory.CurrentPrincipal;
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderVOAService));
            dataLoan.InitLoad();

            var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, enabledServices: ServiceCredentialService.Verifications)
                                            .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == requestData.LenderService.VendorForProtocolAndTransmissionData.VendorId);
            if (serviceCredential != null)
            {
                requestData.Username = serviceCredential.UserName;
                requestData.Password = serviceCredential.UserPassword.Value;
                requestData.AccountId = string.IsNullOrEmpty(serviceCredential.AccountId) ? accountId : serviceCredential.AccountId;
            }

            string validationError;
            if (!requestData.ValidateRequestData(out validationError))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { validationError }));
                return;
            }

            VOARequestHandler requestHandler = new VOARequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            if (auditResults.HasErrors)
            {
                List<string> errorsFromVendor = new List<string>();
                foreach (var auditErrors in auditResults.GetAllErrors())
                {
                    errorsFromVendor.Add(auditErrors.ErrorMessage);
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsFromVendor));
                return;
            }

            if (ConstStage.DisableVoaViaBJP)
            {
                var result = requestHandler.SubmitRequest(doAudit: false);
                int attempts = 0;
                while ((result?.Errors == null || !result.Errors.Any()) &&
                       result?.SubsequentRequestData != null &&
                       attempts < ConstStage.VoaBJPMaxPollingAttempts)
                {
                    System.Threading.Thread.Sleep(ConstStage.VoaBJPPollingIntervalInSeconds * 1000);

                    var newRequestHandler = new VOARequestHandler(result.SubsequentRequestData as VOARequestData);
                    result = newRequestHandler.SubmitRequest(doAudit: false);
                }

                IEnumerable<string> errors = attempts >= ConstStage.VoaBJPMaxPollingAttempts ? new List<string>() { "VOA request has timed out" } : result?.Errors;
                VOAOrder updatedOrder = result?.Orders?.FirstOrDefault() as VOAOrder;
                if (updatedOrder == null ||
                    (errors != null && errors.Any()))
                {
                    List<string> errorsForUi = new List<string>() { "Unable to fulfill order request." };
                    if (errors != null && errors.Any())
                    {
                        errorsForUi = errors.ToList();
                    }

                    SetResult("Success", false);
                    SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsForUi));
                    return;
                }

                var lenderService = requestData.LenderService;
                if (updatedOrder.LenderServiceId != requestData.LenderServiceId)
                {
                    lenderService = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, updatedOrder.LenderServiceId);
                }

                ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
                {
                    RequiresAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId,
                    UsesAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId,
                    AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(requestData.LoanId)
                };

                if (serviceCredential != null)
                {
                    extraInfo.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(serviceCredential.AccountId);
                    extraInfo.ServiceCredentialId = serviceCredential.Id;
                }

                VOAOrderViewModel model = updatedOrder.CreateViewModel(extraInfo);

                if (updatedOrder.VerifiedAssetRecords != null && updatedOrder.VerifiedAssetRecords.Any() && lenderService.VoaAllowImportingAssets)
                {
                    SetResult("HasReturnedAssets", true);
                    SetResult("OrderId", updatedOrder.OrderId);
                }

                SetResult("UpdatedOrder", SerializationHelper.JsonNetSerialize(model));
                SetResult("Success", true);

                if (model.AssociatedEdocs.Count > 0)
                {
                    var joinedIds = string.Join("|", model.AssociatedEdocs.Where(edoc => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select(edoc => edoc.EDocId));
                    var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                    SetResult("Key", key);
                }
            }
            else
            {
                var publicJobId = requestHandler.SubmitToProcessor();
                this.SetResult("Success", true);
                this.SetResult("PublicJobId", publicJobId.ToString());
                this.SetResult("PollingIntervalInSeconds", ConstStage.VoaBJPPollingIntervalInSeconds);
                this.SetResult("ServiceCredentialId", serviceCredential?.Id);
                this.SetResult("ServiceCredentialHasAccountId", !string.IsNullOrEmpty(serviceCredential?.AccountId));
            }
        }

        private void PollForSubsequentResults()
        {
            Guid publicJobId = this.GetGuid("PublicJobId");
            int attemptNumber = this.GetInt("AttemptNumber");
            Guid loanId = this.GetGuid("LoanId");
            int maxPollingAttempts = ConstStage.VoaBJPMaxPollingAttempts;

            var results = VOARequestHandler.CheckRequest(publicJobId, loanId, PrincipalFactory.CurrentPrincipal.BrokerId);

            IEnumerable<string> errors = null;
            VOAOrder updatedOrder = null;
            if (results.Status == LendersOffice.Integration.IntegrationJobStatus.Processing)
            {
                if (++attemptNumber > maxPollingAttempts)
                {
                    errors = new List<string>() { "VOA request has timed out." };
                }
                else
                {
                    this.SetResult("Success", true);
                    this.SetResult("PublicJobId", publicJobId);
                    return;
                }
            }
            else if (results.Status == LendersOffice.Integration.IntegrationJobStatus.Error)
            {
                errors = results.Errors;
            }
            else
            {
                updatedOrder = results.Orders.FirstOrDefault();
            }

            if (updatedOrder == null || (errors != null && errors.Any()))
            {
                IEnumerable<string> requestErrors = new List<string>() { "Unable to fulfill order request." };
                if (errors != null && errors.Any())
                {
                    requestErrors = errors;
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetAnonymousSerialize(requestErrors));
                return;
            }

            var lenderService = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, updatedOrder.LenderServiceId);
            ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
            {
                RequiresAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId,
                UsesAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId,
                AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(loanId),
                ServiceCredentialId = this.GetInt("ServiceCredentialId", -1),
                ServiceCredentialHasAccountId = this.GetBool("ServiceCredentialHasAccountId", false)
            };

            VOAOrderViewModel model = updatedOrder.CreateViewModel(extraInfo);
            if (updatedOrder.VerifiedAssetRecords != null && updatedOrder.VerifiedAssetRecords.Any() && lenderService.VoaAllowImportingAssets)
            {
                SetResult("HasReturnedAssets", true);
                SetResult("OrderId", updatedOrder.OrderId);
            }

            SetResult("UpdatedOrder", SerializationHelper.JsonNetSerialize(model));
            SetResult("Success", true);

            if (model.AssociatedEdocs.Count > 0)
            {
                var joinedIds = string.Join("|", model.AssociatedEdocs.Where(edoc => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select(edoc => edoc.EDocId));
                var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                SetResult("Key", key);
            }
        }

        protected void LoadLenderService()
        {
            int? lenderServiceId = this.GetNullableInt("LenderServiceId");
            if (!lenderServiceId.HasValue)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to load Service Provider");
                return;
            }

            Guid loanId = GetGuid("LoanId");
            var principal = PrincipalFactory.CurrentPrincipal;
            VOXLenderService lenderService = VOXLenderService.GetLenderServiceById(principal.BrokerId, lenderServiceId.Value);
            VOXVendor vendor = lenderService.VendorForVendorAndServiceData;
            AbstractVOXVendorService service;
            if (vendor.Services.TryGetValue(VOXServiceT.VOA_VOD, out service))
            {
                VOAVendorService voaService = (VOAVendorService)service;
                this.SetResult("AskForNotificationEmail", voaService.AskForNotificationEmail);
                this.SetResult("AllowPaymentByCreditCard", lenderService.AllowPaymentByCreditCard);
                this.SetResult("RequiresAccountId", lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId);
                this.SetResult("UsesAccountId", lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId);
                this.SetResult("VoaVerificationT", voaService.VoaVerificationT.ToString("D"));
                this.SetResult("VoaAllowRequestWithoutAssets", lenderService.VoaAllowRequestWithoutAssets);
                this.SetResult("VoaEnforceAllowRequestWithoutAssets", lenderService.VoaEnforceAllowRequestWithoutAssets);

                var optionConfiguration = VOAOptionConfiguration.Load(principal.BrokerId, lenderServiceId.Value, vendor.VendorId);
                if (principal.Type.Equals("B", StringComparison.Ordinal))
                {
                    this.SetResult("AccountHistoryOptions", SerializationHelper.JsonNetSerialize(optionConfiguration.AccountHistoryOptions_LqbUsers.Select(o => o.OptionValue).ToList()));
                    this.SetResult("AccountHistoryDefault", optionConfiguration.AccountHistoryOptionsDefaultValue_LqbUsers);
                    this.SetResult("RefreshPeriodOptions", SerializationHelper.JsonNetSerialize(optionConfiguration.RefreshPeriodOptions_LqbUsers.Select(o => o.OptionValue).ToList()));
                    this.SetResult("RefreshPeriodDefault", optionConfiguration.RefreshPeriodOptionsDefaultValue_LqbUsers);
                }
                else
                {
                    this.SetResult("AccountHistoryOptions", SerializationHelper.JsonNetSerialize(optionConfiguration.AccountHistoryOptions_PmlUsers.Select(o => o.OptionValue).ToList()));
                    this.SetResult("AccountHistoryDefault", optionConfiguration.AccountHistoryOptionsDefaultValue_PmlUsers);
                    this.SetResult("RefreshPeriodOptions", SerializationHelper.JsonNetSerialize(optionConfiguration.RefreshPeriodOptions_PmlUsers.Select(o => o.OptionValue).ToList()));
                    this.SetResult("RefreshPeriodDefault", optionConfiguration.RefreshPeriodOptionsDefaultValue_PmlUsers);
                }

                var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderVOAService));
                dataLoan.InitLoad();

                var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, enabledServices: ServiceCredentialService.Verifications)
                    .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == lenderService.VendorForProtocolAndTransmissionData.VendorId);
                if (serviceCredential != null)
                {
                    this.SetResult("ServiceCredentialId", serviceCredential.Id);
                    this.SetResult("ServiceCredentialHasAccountId", !string.IsNullOrEmpty(serviceCredential.AccountId));
                }

                this.SetResult("Success", true);
            }
            else
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to load Service Provider");
            }
        }

        /// <summary>
        /// Runs a data audit for the VOA/VOD order request.
        /// </summary>
        private void RunAudit()
        {
            ServiceCredential usedServiceCredential;
            VOARequestData requestData = this.GetVOARequestData(out usedServiceCredential);
            if (requestData == null)
            {
                return;
            }

            VOARequestHandler requestHandler = new VOARequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>> auditResultsForUi = new List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>>();
            foreach (var section in auditResults.SectionNames)
            {
                KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>> errorsForSection = new KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>(section, auditResults.GetErrorsForSection(section));
                auditResultsForUi.Add(errorsForSection);
            }

            SetResult("Success", true);
            SetResult("AuditResults", SerializationHelper.JsonNetSerialize(auditResultsForUi));
            SetResult("AuditPassed", !auditResults.HasErrors);
        }

        /// <summary>
        /// Gathers the request data from the page.
        /// </summary>
        /// <param name="usedServiceCredential">The service credential used to populate credentials.</param>
        /// <returns>The VOARequestData if successful. Null otherwise.</returns>
        private VOARequestData GetVOARequestData(out ServiceCredential usedServiceCredential)
        {
            usedServiceCredential = null;

            Guid loanId = this.GetGuid("LoanId");
            int lenderService = this.GetInt("LenderService");
            string notificationEmail = this.GetString("NotificationEmail");
            int accountHistory = this.GetInt("AccountHistory");
            int refreshPeriod = this.GetInt("RefreshPeriod");
            string accountId = this.GetString("AccountId");
            string userName = this.GetString("UserName");
            string password = this.GetString("Password");
            bool payWithCreditCard = this.GetBool("PayWithCreditCard", false);
            string billingFirstName = this.GetString("BillingFirstName", string.Empty);
            string billingMiddleName = this.GetString("BillingMiddleName", string.Empty);
            string billingLastName = this.GetString("BillingLastName", string.Empty);
            string billingCardNumber = this.GetString("BillingCardNumber", string.Empty);
            string billingCVV = this.GetString("BillingCVV", string.Empty);
            string billingStreetAddress = this.GetString("BillingStreetAddress", string.Empty);
            string billingCity = this.GetString("BillingCity", string.Empty);
            string billingState = this.GetString("BillingState", string.Empty);
            string billingZipcode = this.GetString("BillingZipcode", string.Empty);
            int billingExpirationMonth = this.GetInt("BillingExpirationMonth", 0);
            int billingExpirationYear = this.GetInt("BillingExpirationYear", 0);

            List<VOXBorrowerAuthDoc> docPickerInfo = new List<VOXBorrowerAuthDoc>();
            string docPickerString = this.GetString("DocPickerInfo");
            if (!string.IsNullOrEmpty(docPickerString))
            {
                foreach (var info in docPickerString.Split(','))
                {
                    var infoSplit = info.Split('|');
                    E_BorrowerModeT assetOwner;
                    if (!Enum.TryParse(infoSplit[2], out assetOwner) && Enum.IsDefined(typeof(E_BorrowerModeT), assetOwner))
                    {
                        SetResult("Success", false);
                        SetResult("Errors", "Invalid asset owner type.");
                        return null;
                    }

                    docPickerInfo.Add(new VOXBorrowerAuthDoc(Guid.Parse(infoSplit[0]), Guid.Parse(infoSplit[1]), assetOwner == E_BorrowerModeT.Coborrower));
                }
            }

            bool verifiesBorrower = this.GetBool("VerifiesBorrower");
            Guid? borrowerAppId = null;
            E_BorrowerModeT? borrowerType = null;
            string borrowerInfo = this.GetString("BorrowerInfo");
            if (verifiesBorrower && !string.IsNullOrEmpty(borrowerInfo))
            {
                var split = borrowerInfo.Split('_');
                borrowerAppId = split[0].ToNullable<Guid>(Guid.TryParse);
                borrowerType = split[1].TryParseEnum<E_BorrowerModeT>();
            }

            List<VOAServiceAssetData> assetInfo = new List<VOAServiceAssetData>();
            List<AssetRecordModel> recordModels = SerializationHelper.JsonNetDeserialize<List<AssetRecordModel>>(GetString("AssetInfo", null));
            if (!verifiesBorrower && recordModels != null)
            {
                foreach (var model in recordModels)
                {
                    assetInfo.Add(new VOAServiceAssetData(model.AppId, model.AssetId, model.BorrowerType == E_AssetOwnerT.CoBorrower));
                }
            }

            VOARequestData requestData = new VOARequestData(PrincipalFactory.CurrentPrincipal, lenderService, loanId, VOXRequestT.Initial)
            {
                AccountHistoryOption = accountHistory,
                AccountId = accountId,
                AssetsToInclude = assetInfo,
                BillingExpirationYear = billingExpirationYear,
                BillingCardNumber = billingCardNumber,
                BillingCity = billingCity,
                BillingCVV = billingCVV,
                BillingExpirationMonth = billingExpirationMonth,
                BillingFirstName = billingFirstName,
                BillingMiddleName = billingMiddleName,
                BillingLastName = billingLastName,
                BillingState = billingState,
                BillingStreetAddress = billingStreetAddress,
                BorrowerAuthDocs = docPickerInfo,
                NotificationEmail = notificationEmail,
                Password = password,
                PayWithCreditCard = payWithCreditCard,
                RefreshPeriodOption = refreshPeriod,
                Username = userName,
                BillingZipcode = billingZipcode,
                VerifiesBorrower = verifiesBorrower,
                BorrowerAppId = borrowerAppId,
                BorrowerType = borrowerType
            };

            var principal = PrincipalFactory.CurrentPrincipal;
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderVOAService));
            dataLoan.InitLoad();

            var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, enabledServices: ServiceCredentialService.Verifications)
                                            .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == requestData.LenderService.VendorForProtocolAndTransmissionData.VendorId);
            if (serviceCredential == null && (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password)))
            {
                SetResult("Success", false);
                SetResult("Errors", "Invalid credentials.");
                return null;
            }

            if (serviceCredential != null)
            {
                usedServiceCredential = serviceCredential;
                requestData.Username = serviceCredential.UserName;
                requestData.Password = serviceCredential.UserPassword.Value;
                requestData.AccountId = string.IsNullOrEmpty(serviceCredential.AccountId) ? accountId : serviceCredential.AccountId;
            }

            string errors;
            if (!requestData.ValidateRequestData(out errors))
            {
                SetResult("Success", false);
                SetResult("Errors", errors);
                return null;
            }

            requestData.IsForTpo = true;
            return requestData;
        }

        /// <summary>
        /// View model for asset record info.
        /// </summary>
        private class AssetRecordModel
        {
            /// <summary>
            /// Gets or sets the app id.
            /// </summary>
            /// <value>The app id.</value>
            public Guid AppId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the asset record id.
            /// </summary>
            /// <value>The asset record id.</value>
            public Guid AssetId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the borrower type.
            /// </summary>
            /// <value>The borrower type.</value>
            public E_AssetOwnerT BorrowerType
            {
                get; set;
            }
        }
    }
}