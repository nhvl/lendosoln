﻿// <copyright file="TPOFeeEditor.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   7/6/2014
// </summary>

namespace TPOFeeEditor
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.UI.Workflow;
    using LendersOffice.Security;

    /// <summary>
    /// This page loads "price my loan's" Fee Editor.
    /// </summary>
    public partial class TPOFeeEditor : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// Gets the url to get back to the pipeline.
        /// </summary>
        /// <value>A string containing the path to the pipeline.</value>
        protected string PipelineUrl
        {
            get { return this.VirtualRoot + "/Main/Pipeline.aspx"; }
        }

        protected bool IsNewPmlUIEnabled
        {
            get { return BrokerDB.RetrieveById(this.BrokerID).IsNewPmlUIEnabled || PriceMyLoanUser.IsNewPmlUIEnabled; }
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        /// <value>A loan id.</value>
        protected Guid LoanId
        {
            get { return RequestHelper.GetGuid("loanid", Guid.Empty); }
        }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>A broker id.</value>
        protected Guid BrokerID
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the combo boxes require values only from the defined values.
        /// </summary>
        /// <value>A boolean.</value>
        protected bool IsRequireFeesFromDropDown
        {
            get;
            set;
        }

        protected bool HasSchedDueD1
        {
            get { return dataLoan.sSchedDueD1.IsValid; }
        }

        protected string CurrentTab
        {
            get { return RequestHelper.GetSafeQueryString("currentTab"); }
        }

        protected CPageData dataLoan;

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                return E_XUAComaptibleValue.Edge;
            }

            return base.GetForcedCompatibilityMode();
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            this.m_loadDefaultStylesheet = false;
        }

        /// <summary>
        /// Binds the options for the Paid By drop down lists.
        /// </summary>
        /// <param name="ddl">The drop down list who will gain the list items.</param>
        protected void BindPaidByDDL(DropDownList ddl)
        {

            ddl.Items.Add(new ListItem("Borrower", "0"));
            if (ddl.ID != "sGfeOriginatorCompBy")
            {
                ddl.Items.Add(new ListItem("Financing", "1"));
                ddl.Items.Add(new ListItem("Seller", "2"));
            }
            ddl.Items.Add(new ListItem("Lender", "3"));
            if (ddl.ID != "sGfeOriginatorCompBy")
            {
                ddl.Items.Add(new ListItem("Broker", "4"));
            }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Event Arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            this.RegisterCSS("/webapp/tabStyle_TPO.css");
            this.RegisterCSS("jquery-ui-1.11.css");
            this.RegisterCSS("combobox.css");

            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("/webapp/tpo.js");
            this.RegisterJsScript("mask.js");
            this.RegisterJsScript("/webapp/combobox.js");
            this.RegisterJsScript("WorkflowPageDisplay.js");

            this.RegisterJsGlobalVariables("currentTab", CurrentTab ?? "-1");

            this.RegisterService("main", "/webapp/TPOFeeEditorService.aspx");
            ClientScript.RegisterHiddenField("IsReadOnly", IsReadOnlyJs);

            BrokerDB brokerDB = BrokerDB.RetrieveById(this.BrokerID);
            if (!brokerDB.EnableAdditionalSection1000CustomFees)
            {
                row1010.Style.Add("display", "none");
                row1011.Style.Add("display", "none");

                row1010Popup.Style.Add("display", "none");
                row1011Popup.Style.Add("display", "none");
            }

            this.ViewState["loanId"] = this.LoanId;
            dataLoan = CPageData.CreateUsingSmartDependency(this.LoanId, typeof(TPOFeeEditor));
            dataLoan.InitLoad();


            ClientScript.RegisterHiddenField("BrokerOrCorrespondentChannel", (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent || dataLoan.sBranchChannelT == E_BranchChannelT.Broker).ToString());
            sGfeIsTPOTransaction.Checked = dataLoan.sGfeIsTPOTransaction;

            sGfeOriginatorCompFPc.Text = dataLoan.sGfeOriginatorCompFPc_rep;
            sGfeOriginatorCompFMb.Text = dataLoan.sGfeOriginatorCompFMb_rep;
            sGfeOriginatorCompFBaseT.SelectedValue = dataLoan.sGfeOriginatorCompFBaseT.ToString();
            sGfeOriginatorCompF.Text = dataLoan.sGfeOriginatorCompF_rep;

            sGfeIsTPOTransactionRow.Visible = dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent;
            
            this.DataBind(dataLoan.ExtractStaticGFEArchive());

            this.LoanNavHeader.SetDataFromLoan(dataLoan, LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationFeeEditor);

            var enableNewUi = this.PriceMyLoanUser.EnableNewTpoLoanNavigation;

            this.RegisterJsGlobalVariables("EnableNewTpoLoanNavigation", enableNewUi);
            this.LegendTable.Visible = !enableNewUi;

            // Setting up the escrow.
            System.Web.UI.WebControls.TextBox[,] m_controls = new System.Web.UI.WebControls.TextBox[,] 
            {
                                                                      { RETax0, HazIns0, MortIns0, FloIns0, SchoTax0, UsrDef10, UsrDef20, UsrDef30, UsrDef40 },
                                                                      { RETax1, HazIns1, MortIns1, FloIns1, SchoTax1, UsrDef11, UsrDef21, UsrDef31, UsrDef41 },
                                                                      { RETax2, HazIns2, MortIns2, FloIns2, SchoTax2, UsrDef12, UsrDef22, UsrDef32, UsrDef42 },
                                                                      { RETax3, HazIns3, MortIns3, FloIns3, SchoTax3, UsrDef13, UsrDef23, UsrDef33, UsrDef43 },
                                                                      { RETax4, HazIns4, MortIns4, FloIns4, SchoTax4, UsrDef14, UsrDef24, UsrDef34, UsrDef44 },
                                                                      { RETax5, HazIns5, MortIns5, FloIns5, SchoTax5, UsrDef15, UsrDef25, UsrDef35, UsrDef45 },
                                                                      { RETax6, HazIns6, MortIns6, FloIns6, SchoTax6, UsrDef16, UsrDef26, UsrDef36, UsrDef46 },
                                                                      { RETax7, HazIns7, MortIns7, FloIns7, SchoTax7, UsrDef17, UsrDef27, UsrDef37, UsrDef47 },
                                                                      { RETax8, HazIns8, MortIns8, FloIns8, SchoTax8, UsrDef18, UsrDef28, UsrDef38, UsrDef48 },
                                                                      { RETax9, HazIns9, MortIns9, FloIns9, SchoTax9, UsrDef19, UsrDef29, UsrDef39, UsrDef49 },
                                                                      { RETax10, HazIns10, MortIns10, FloIns10, SchoTax10, UsrDef110, UsrDef210, UsrDef310, UsrDef410 },
                                                                      { RETax11, HazIns11, MortIns11, FloIns11, SchoTax11, UsrDef111, UsrDef211, UsrDef311, UsrDef411 },
                                                                      { RETax12, HazIns12, MortIns12, FloIns12, SchoTax12, UsrDef112, UsrDef212, UsrDef312, UsrDef412 }
                                                                  };

            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;

            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;

            sSchedDueD1LckdInitial.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1Initial.Text = dataLoan.sSchedDueD1_rep;

            sEstCloseDLckdInitial.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseDInitial.Text = dataLoan.sEstCloseD_rep;

            int[,] table = dataLoan.sInitialEscrowAcc;

            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (table[i, j] != 0)
                    {
                        var val = table[i, j].ToString();
                        if(val.Equals("0"))
                        {
                            val = "";
                        }
                        m_controls[i, j].Text = val;
                    }
                }
            }

            sHazInsRsrvEscrowedTri.Checked = dataLoan.sHazInsRsrvEscrowedTri == E_TriState.Yes;
            sMInsRsrvEscrowedTri.Checked = dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes;
            if (dataLoan.sIssMInsRsrvEscrowedTriReadOnly)
            {
                sMInsRsrvEscrowedTri.Disabled = true;
            }
            sRealETxRsrvEscrowedTri.Checked = dataLoan.sRealETxRsrvEscrowedTri == E_TriState.Yes;
            sSchoolTxRsrvEscrowedTri.Checked = dataLoan.sSchoolTxRsrvEscrowedTri == E_TriState.Yes;
            sFloodInsRsrvEscrowedTri.Checked = dataLoan.sFloodInsRsrvEscrowedTri == E_TriState.Yes;
            s1006RsrvEscrowedTri.Checked = dataLoan.s1006RsrvEscrowedTri == E_TriState.Yes;
            s1007RsrvEscrowedTri.Checked = dataLoan.s1007RsrvEscrowedTri == E_TriState.Yes;
            sU3RsrvEscrowedTri.Checked = dataLoan.sU3RsrvEscrowedTri == E_TriState.Yes;
            sU4RsrvEscrowedTri.Checked = dataLoan.sU4RsrvEscrowedTri == E_TriState.Yes;

            sHazInsRsrvMonLckd.Checked = dataLoan.sHazInsRsrvMonLckd;
            sHazInsRsrvMonRecommended.Text = dataLoan.sHazInsRsrvMonRecommended_rep;

            sMInsRsrvMonLckd.Checked = dataLoan.sMInsRsrvMonLckd;
            sMInsRsrvMonRecommended.Text = dataLoan.sMInsRsrvMonRecommended_rep;

            sRealETxRsrvMonLckd.Checked = dataLoan.sRealETxRsrvMonLckd;
            sRealETxRsrvMonRecommended.Text = dataLoan.sRealETxRsrvMonRecommended_rep;

            sSchoolTxRsrvMonLckd.Checked = dataLoan.sSchoolTxRsrvMonLckd;
            sSchoolTxRsrvMonRecommended.Text = dataLoan.sSchoolTxRsrvMonRecommended_rep;

            sFloodInsRsrvMonLckd.Checked = dataLoan.sFloodInsRsrvMonLckd;
            sFloodInsRsrvMonRecommended.Text = dataLoan.sFloodInsRsrvMonRecommended_rep;

            s1006RsrvMonLckd.Checked = dataLoan.s1006RsrvMonLckd;
            s1006RsrvMonRecommended.Text = dataLoan.s1006RsrvMonRecommended_rep;

            s1007RsrvMonLckd.Checked = dataLoan.s1007RsrvMonLckd;
            s1007RsrvMonRecommended.Text = dataLoan.s1007RsrvMonRecommended_rep;

            sU3RsrvMonRecommended.Text = dataLoan.sU3RsrvMonRecommended_rep;
            sU3RsrvMonLckd.Checked = dataLoan.sU3RsrvMonLckd;

            sU4RsrvMonRecommended.Text = dataLoan.sU4RsrvMonRecommended_rep;
            sU4RsrvMonLckd.Checked = dataLoan.sU4RsrvMonLckd;

            s1007ProHExpDescSpan.InnerText = dataLoan.s1007ProHExpDesc != string.Empty ? dataLoan.s1007ProHExpDesc : "User Defined 2";
            s1006ProHExpDescSpan.InnerText = dataLoan.s1006ProHExpDesc != string.Empty ? dataLoan.s1006ProHExpDesc : "User Defined 1";
            sU3RsrvDescSpan.InnerText = dataLoan.sU3RsrvDesc != string.Empty ? dataLoan.sU3RsrvDesc : "User Defined 3";
            sU4RsrvDescSpan.InnerText = dataLoan.sU4RsrvDesc != string.Empty ? dataLoan.sU4RsrvDesc : "User Defined 4";

            if (brokerDB.EnablePageDisplayChangesViaWorkflow)
            {
                var generator = new WorkflowPageDisplayModelGenerator(dataLoan.LoanFileWorkflowRules);
                var model = generator.Generate();
                this.RegisterJsObjectWithJsonNetSerializer("WorkflowPageDisplayModel", model);
            }
        }

        /// <summary>
        /// This method loads the GFE values.
        /// </summary>
        /// <param name="gfea">The GFE archive.</param>
        protected void DataBind(LendersOffice.Common.SerializationTypes.IGFEArchive gfea)
        {
            BrokerDB broker = BrokerDB.RetrieveById(this.BrokerID);

            this.IsRequireFeesFromDropDown = gfea.sIsRequireFeesFromDropDown;

            this.s800U5F.Text = gfea.s800U5F_rep;

            this.s800U5FDesc.Text = gfea.s800U5FDesc;
            this.s800U4F.Text = gfea.s800U4F_rep;

            this.s800U4FDesc.Text = gfea.s800U4FDesc;
            this.s800U3F.Text = gfea.s800U3F_rep;

            this.s800U3FDesc.Text = gfea.s800U3FDesc;
            this.s800U2F.Text = gfea.s800U2F_rep;

            this.s800U2FDesc.Text = gfea.s800U2FDesc;
            this.s800U1F.Text = gfea.s800U1F_rep;

            this.s800U1FDesc.Text = gfea.s800U1FDesc;

            this.sWireF.Text = gfea.sWireF_rep;

            this.sUwF.Text = gfea.sUwF_rep;

            this.sProcF.Text = gfea.sProcF_rep;

            this.sTxServF.Text = gfea.sTxServF_rep;

            this.sFloodCertificationF.Text = gfea.sFloodCertificationF_rep;
            Tools.SetDropDownListValue(this.sFloodCertificationDeterminationT, gfea.sFloodCertificationDeterminationT);

            this.sMBrokF.Text = gfea.sMBrokF_rep;

            this.sMBrokFMb.Text = gfea.sMBrokFMb_rep;

            this.sMBrokFPc.Text = gfea.sMBrokFPc_rep;
            Tools.SetDropDownListValue(this.sMBrokFBaseT, gfea.sMBrokFBaseT);

            this.sInspectF.Text = gfea.sInspectF_rep;

            this.sCrF.Text = gfea.sCrF_rep;

            this.sApprF.Text = gfea.sApprF_rep;

            this.sLOrigF.InnerText = gfea.sLOrigF_rep;

            this.sLOrigFMb.Text = gfea.sLOrigFMb_rep;

            this.sLOrigFPc.Text = gfea.sLOrigFPc_rep;

            this.s900U1Pia.Text = gfea.s900U1Pia_rep;

            this.s900U1PiaDesc.Text = gfea.s900U1PiaDesc;
            this.sVaFf.Text = gfea.sVaFf_rep;
            this.s904Pia.Text = gfea.s904Pia_rep;

            this.s904PiaDesc.Text = gfea.s904PiaDesc;
            this.sHazInsPia.Text = gfea.sHazInsPia_rep;

            this.sHazInsPiaMon.Text = gfea.sHazInsPiaMon_rep;

            Tools.SetDropDownListValue(this.sProHazInsT, gfea.sProHazInsT);
            this.sProHazInsR.Text = gfea.sProHazInsR_rep;

            this.sProHazInsMb.Text = gfea.sProHazInsMb_rep;

            this.sMipPia.Text = gfea.sMipPia_rep;

            this.sIPia.Text = gfea.sIPia_rep;

            this.sIPiaDy.Text = gfea.sIPiaDy_rep;

            this.sIPerDay.Text = gfea.sIPerDay_rep;

            this.sAggregateAdjRsrv.Text = gfea.sAggregateAdjRsrv_rep;
            this.sAggregateAdjRsrvLckd.Checked = gfea.sAggregateAdjRsrvLckd;
            this.s1007Rsrv.Text = gfea.s1007Rsrv_rep;

            this.s1007ProHExp.Text = gfea.s1007ProHExp_rep;

            this.s1007RsrvMon.Text = gfea.s1007RsrvMon_rep;

            this.s1007ProHExpDesc.Text = gfea.s1007ProHExpDesc;
            this.s1006Rsrv.Text = gfea.s1006Rsrv_rep;

            this.s1006ProHExp.Text = gfea.s1006ProHExp_rep;

            this.s1006RsrvMon.Text = gfea.s1006RsrvMon_rep;

            this.s1006ProHExpDesc.Text = gfea.s1006ProHExpDesc;

            this.sU3Rsrv.Text = gfea.sU3Rsrv_rep;

            this.sProU3Rsrv.Text = gfea.sProU3Rsrv_rep;

            this.sU3RsrvMon.Text = gfea.sU3RsrvMon_rep;

            this.sU3RsrvDesc.Text = gfea.sU3RsrvDesc;

            this.sU4Rsrv.Text = gfea.sU4Rsrv_rep;

            this.sProU4Rsrv.Text = gfea.sProU4Rsrv_rep;

            this.sU4RsrvMon.Text = gfea.sU4RsrvMon_rep;

            this.sU4RsrvDesc.Text = gfea.sU4RsrvDesc;

            this.sFloodInsRsrv.Text = gfea.sFloodInsRsrv_rep;

            this.sProFloodIns.Text = gfea.sProFloodIns_rep;

            this.sFloodInsRsrvMon.Text = gfea.sFloodInsRsrvMon_rep;

            this.sRealETxRsrv.Text = gfea.sRealETxRsrv_rep;

            Tools.SetDropDownListValue(this.sProRealETxT, gfea.sProRealETxT);
            this.sProRealETxMb.Text = gfea.sProRealETxMb_rep;

            this.sProRealETxR.Text = gfea.sProRealETxR_rep;

            this.sRealETxRsrvMon.Text = gfea.sRealETxRsrvMon_rep;

            this.sSchoolTxRsrv.Text = gfea.sSchoolTxRsrv_rep;

            this.sProSchoolTx.Text = gfea.sProSchoolTx_rep;
            this.sSchoolTxRsrvMon.Text = gfea.sSchoolTxRsrvMon_rep;

            this.sMInsRsrv.Text = gfea.sMInsRsrv_rep;

            this.sProMIns.Text = gfea.sProMIns_rep;

            this.sMInsRsrvMon.Text = gfea.sMInsRsrvMon_rep;

            this.sHazInsRsrv.Text = gfea.sHazInsRsrv_rep;

            this.sProHazIns.Text = gfea.sProHazIns_rep;

            this.sHazInsRsrvMon.Text = gfea.sHazInsRsrvMon_rep;

            this.sU4Tc.Text = gfea.sU4Tc_rep;

            this.sU4TcDesc.Text = gfea.sU4TcDesc;
            this.sU3Tc.Text = gfea.sU3Tc_rep;

            this.sU3TcDesc.Text = gfea.sU3TcDesc;
            this.sU2Tc.Text = gfea.sU2Tc_rep;

            this.sU2TcDesc.Text = gfea.sU2TcDesc;
            this.sU1Tc.Text = gfea.sU1Tc_rep;

            this.sU1TcDesc.Text = gfea.sU1TcDesc;
            this.sTitleInsF.Text = gfea.sTitleInsF_rep;

            this.sAttorneyF.Text = gfea.sAttorneyF_rep;

            this.sNotaryF.Text = gfea.sNotaryF_rep;

            this.sDocPrepF.Text = gfea.sDocPrepF_rep;

            this.sEscrowF.Text = gfea.sEscrowF_rep;

            this.sOwnerTitleInsF.Text = gfea.sOwnerTitleInsF_rep;

            this.sU3GovRtc.Text = gfea.sU3GovRtc_rep;

            this.sU3GovRtcMb.Text = gfea.sU3GovRtcMb_rep;

            Tools.SetDropDownListValue(this.sU3GovRtcBaseT, gfea.sU3GovRtcBaseT);
            this.sU3GovRtcPc.Text = gfea.sU3GovRtcPc_rep;

            this.sU3GovRtcDesc.Text = gfea.sU3GovRtcDesc;
            this.sU2GovRtc.Text = gfea.sU2GovRtc_rep;

            this.sU2GovRtcMb.Text = gfea.sU2GovRtcMb_rep;

            Tools.SetDropDownListValue(this.sU2GovRtcBaseT, gfea.sU2GovRtcBaseT);
            this.sU2GovRtcPc.Text = gfea.sU2GovRtcPc_rep;

            this.sU2GovRtcDesc.Text = gfea.sU2GovRtcDesc;
            this.sU1GovRtc.Text = gfea.sU1GovRtc_rep;

            this.sU1GovRtcMb.Text = gfea.sU1GovRtcMb_rep;

            Tools.SetDropDownListValue(this.sU1GovRtcBaseT, gfea.sU1GovRtcBaseT);
            this.sU1GovRtcPc.Text = gfea.sU1GovRtcPc_rep;

            this.sU1GovRtcDesc.Text = gfea.sU1GovRtcDesc;
            this.sStateRtc.Text = gfea.sStateRtc_rep;

            this.sStateRtcMb.Text = gfea.sStateRtcMb_rep;

            Tools.SetDropDownListValue(this.sStateRtcBaseT, gfea.sStateRtcBaseT);
            this.sStateRtcPc.Text = gfea.sStateRtcPc_rep;

            this.sCountyRtc.Text = gfea.sCountyRtc_rep;

            this.sCountyRtcMb.Text = gfea.sCountyRtcMb_rep;

            Tools.SetDropDownListValue(this.sCountyRtcBaseT, gfea.sCountyRtcBaseT);
            this.sCountyRtcPc.Text = gfea.sCountyRtcPc_rep;

            this.sRecF.Text = gfea.sRecF_rep;

            this.sRecFMb.Text = gfea.sRecFMb_rep;

            Tools.SetDropDownListValue(this.sRecBaseT, gfea.sRecBaseT);
            this.sRecFPc.Text = gfea.sRecFPc_rep;

            this.sRecFLckd.Checked = gfea.sRecFLckd;
            this.sRecDeed.Text = gfea.sRecDeed_rep;
            this.sRecMortgage.Text = gfea.sRecMortgage_rep;
            this.sRecRelease.Text = gfea.sRecRelease_rep;

            this.sU5Sc.Text = gfea.sU5Sc_rep;

            this.sU5ScDesc.Text = gfea.sU5ScDesc;
            this.sU4Sc.Text = gfea.sU4Sc_rep;

            this.sU4ScDesc.Text = gfea.sU4ScDesc;
            this.sU3Sc.Text = gfea.sU3Sc_rep;

            this.sU3ScDesc.Text = gfea.sU3ScDesc;
            this.sU2Sc.Text = gfea.sU2Sc_rep;

            this.sU2ScDesc.Text = gfea.sU2ScDesc;
            this.sU1Sc.Text = gfea.sU1Sc_rep;

            this.sU1ScDesc.Text = gfea.sU1ScDesc;
            this.sPestInspectF.Text = gfea.sPestInspectF_rep;

            this.sLDiscnt.Text = gfea.sLDiscnt_rep;
            this.sLDiscntFMb.Text = gfea.sLDiscntFMb_rep;
            this.sLDiscntPc.Text = gfea.sLDiscntPc_rep;
            Tools.SetDropDownListValue(this.sLDiscntBaseT, gfea.sLDiscntBaseT);

            sGfeLenderCreditAPR.Checked = LosConvert.GfeItemProps_Apr(gfea.sGfeLenderCreditFProps);

             this.BindProperties("sGfeDiscountPoint", gfea.sGfeDiscountPointFProps, string.Empty, E_GfeSectionT.LeaveBlank);

             this.BindProperties("sLOrigF", gfea.sLOrigFProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sGfeOriginatorComp", gfea.sGfeOriginatorCompFProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sAppr", gfea.sApprFProps, gfea.sApprFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sCr", gfea.sCrFProps, gfea.sCrFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sInspect", gfea.sInspectFProps, gfea.sInspectFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sMBrok", gfea.sMBrokFProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sTxServ", gfea.sTxServFProps, gfea.sTxServFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sFloodCertification", gfea.sFloodCertificationFProps, gfea.sFloodCertificationFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sProc", gfea.sProcFProps, gfea.sProcFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sUw", gfea.sUwFProps, gfea.sUwFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sWire", gfea.sWireFProps, gfea.sWireFPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("s800U1", gfea.s800U1FProps, gfea.s800U1FPaidTo, gfea.s800U1FGfeSection);
             this.BindProperties("s800U2", gfea.s800U2FProps, gfea.s800U2FPaidTo, gfea.s800U2FGfeSection);
             this.BindProperties("s800U3", gfea.s800U3FProps, gfea.s800U3FPaidTo, gfea.s800U3FGfeSection);
             this.BindProperties("s800U4", gfea.s800U4FProps, gfea.s800U4FPaidTo, gfea.s800U4FGfeSection);
             this.BindProperties("s800U5", gfea.s800U5FProps, gfea.s800U5FPaidTo, gfea.s800U5FGfeSection);

             this.BindProperties("sI", gfea.sIPiaProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sMip", gfea.sMipPiaProps, gfea.sMipPiaPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sHazIns", gfea.sHazInsPiaProps, gfea.sHazInsPiaPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("s904", gfea.s904PiaProps, string.Empty, gfea.s904PiaGfeSection);
             this.BindProperties("sVa", gfea.sVaFfProps, gfea.sVaFfPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("s900U1", gfea.s900U1PiaProps, string.Empty, gfea.s900U1PiaGfeSection);

             this.BindProperties("sHazInsRsrv", gfea.sHazInsRsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sMInsRsrv", gfea.sMInsRsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sSchoolTxRsrv", gfea.sSchoolTxRsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sRealETxRsrv", gfea.sRealETxRsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sFloodInsRsrv", gfea.sFloodInsRsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("s1006Rsrv", gfea.s1006RsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("s1007Rsrv", gfea.s1007RsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sU3Rsrv", gfea.sU3RsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sU4Rsrv", gfea.sU4RsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sAggregateAdjRsrv", gfea.sAggregateAdjRsrvProps, string.Empty, E_GfeSectionT.LeaveBlank);

             this.BindProperties("sEscrow", gfea.sEscrowFProps, gfea.sEscrowFTable, gfea.sEscrowFGfeSection);
             this.BindProperties("sOwnerTitleIns", gfea.sOwnerTitleInsProps, gfea.sOwnerTitleInsPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sDocPrep", gfea.sDocPrepFProps, gfea.sDocPrepFPaidTo, gfea.sDocPrepFGfeSection);
             this.BindProperties("sNotary", gfea.sNotaryFProps, gfea.sNotaryFPaidTo, gfea.sNotaryFGfeSection);
             this.BindProperties("sAttorney", gfea.sAttorneyFProps, gfea.sAttorneyFPaidTo, gfea.sAttorneyFGfeSection);
             this.BindProperties("sTitleIns", gfea.sTitleInsFProps, gfea.sTitleInsFTable, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sU1Tc", gfea.sU1TcProps, gfea.sU1TcPaidTo, gfea.sU1TcGfeSection);
             this.BindProperties("sU2Tc", gfea.sU2TcProps, gfea.sU2TcPaidTo, gfea.sU2TcGfeSection);
             this.BindProperties("sU3Tc", gfea.sU3TcProps, gfea.sU3TcPaidTo, gfea.sU3TcGfeSection);
             this.BindProperties("sU4Tc", gfea.sU4TcProps, gfea.sU4TcPaidTo, gfea.sU4TcGfeSection);

             this.BindProperties("sRecF", gfea.sRecFProps, gfea.sRecFDesc, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sCountyRtc", gfea.sCountyRtcProps, gfea.sCountyRtcDesc, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sStateRtc", gfea.sStateRtcProps, gfea.sStateRtcDesc, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sU1GovRtc", gfea.sU1GovRtcProps, gfea.sU1GovRtcPaidTo, gfea.sU1GovRtcGfeSection);
             this.BindProperties("sU2GovRtc", gfea.sU2GovRtcProps, gfea.sU2GovRtcPaidTo, gfea.sU2GovRtcGfeSection);
             this.BindProperties("sU3GovRtc", gfea.sU3GovRtcProps, gfea.sU3GovRtcPaidTo, gfea.sU3GovRtcGfeSection);

             this.BindProperties("sPestInspect", gfea.sPestInspectFProps, gfea.sPestInspectPaidTo, E_GfeSectionT.LeaveBlank);
             this.BindProperties("sU1Sc", gfea.sU1ScProps, gfea.sU1ScPaidTo, gfea.sU1ScGfeSection);
             this.BindProperties("sU2Sc", gfea.sU2ScProps, gfea.sU2ScPaidTo, gfea.sU2ScGfeSection);
             this.BindProperties("sU3Sc", gfea.sU3ScProps, gfea.sU3ScPaidTo, gfea.sU3ScGfeSection);
             this.BindProperties("sU4Sc", gfea.sU4ScProps, gfea.sU4ScPaidTo, gfea.sU4ScGfeSection);
             this.BindProperties("sU5Sc", gfea.sU5ScProps, gfea.sU5ScPaidTo, gfea.sU5ScGfeSection);

            // Line 802
            // Credit or charge
            sLDiscnt.Text = gfea.sLDiscnt_rep;
            sLDiscntFMb.Text = gfea.sLDiscntFMb_rep;
            sLDiscntPc.Text = gfea.sLDiscntPc_rep;
            Tools.SetDropDownListValue(this.sLDiscntBaseT, gfea.sLDiscntBaseT);

            // Credit for lender paid fees
            Tools.SetDropDownListValue(this.sGfeCreditLenderPaidItemT, gfea.sGfeCreditLenderPaidItemT);
            sGfeCreditLenderPaidItemF.Text = gfea.sGfeCreditLenderPaidItemF_rep;

            // General Lender credit
            sGfeLenderCreditFPc.Text = gfea.sGfeLenderCreditFPc_rep;
            sGfeLenderCreditF.Text = gfea.sGfeLenderCreditF_rep;

            // Discount points
            sGfeDiscountPointFPc.Text = gfea.sGfeDiscountPointFPc_rep;
            sGfeDiscountPointF.Text = gfea.sGfeDiscountPointF_rep;

            Tools.Bind_sProRealETxT(this.sProRealETxT);
            Tools.Bind_PercentBaseT(this.sProHazInsT);
            Tools.Bind_PercentBaseLoanAmountsT(this.sLDiscntBaseT);
            Tools.Bind_FloodCertificationDeterminationT(this.sFloodCertificationDeterminationT);
            Tools.Bind_PercentBaseLoanAmountsT(this.sMBrokFBaseT);
            Tools.Bind_PercentBaseT(this.sRecBaseT);
            Tools.Bind_PercentBaseT(this.sCountyRtcBaseT);
            Tools.Bind_PercentBaseT(this.sStateRtcBaseT);
            Tools.Bind_PercentBaseT(this.sU1GovRtcBaseT);
            Tools.Bind_PercentBaseT(this.sU2GovRtcBaseT);
            Tools.Bind_PercentBaseT(this.sU3GovRtcBaseT);
            Tools.Bind_sGfeOriginatorCompFBaseT(this.sGfeOriginatorCompFBaseT);
            this.sGfeOriginatorCompFBaseT.Items.RemoveAt(2);//remove the All YSP option
            Tools.Bind_sGfeCreditLenderPaidItemT(this.sGfeCreditLenderPaidItemT);

            Tools.Bind_CustomFeeDesc(this.s800U1FDesc,  this.BrokerID, "800");
            Tools.Bind_CustomFeeDesc(this.s800U2FDesc,  this.BrokerID, "800");
            Tools.Bind_CustomFeeDesc(this.s800U3FDesc,  this.BrokerID, "800");
            Tools.Bind_CustomFeeDesc(this.s800U4FDesc,  this.BrokerID, "800");
            Tools.Bind_CustomFeeDesc(this.s800U5FDesc,  this.BrokerID, "800");

            Tools.Bind_CustomFeeDesc(this.s904PiaDesc,  this.BrokerID, "900");
            Tools.Bind_CustomFeeDesc(this.s900U1PiaDesc,  this.BrokerID, "900");

            Tools.Bind_CustomFeeDesc(this.s1006ProHExpDesc,  this.BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(this.s1007ProHExpDesc,  this.BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(this.sU3RsrvDesc,  this.BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(this.sU4RsrvDesc,  this.BrokerID, "1000");

            Tools.Bind_CustomFeeDesc(this.sU1TcDesc,  this.BrokerID, "1100");
            Tools.Bind_CustomFeeDesc(this.sU2TcDesc,  this.BrokerID, "1100");
            Tools.Bind_CustomFeeDesc(this.sU3TcDesc,  this.BrokerID, "1100");
            Tools.Bind_CustomFeeDesc(this.sU4TcDesc,  this.BrokerID, "1100");

            Tools.Bind_CustomFeeDesc(this.sU1GovRtcDesc,  this.BrokerID, "1200");
            Tools.Bind_CustomFeeDesc(this.sU2GovRtcDesc,  this.BrokerID, "1200");
            Tools.Bind_CustomFeeDesc(this.sU3GovRtcDesc,  this.BrokerID, "1200");

            Tools.Bind_CustomFeeDesc(this.sU1ScDesc,  this.BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(this.sU2ScDesc,  this.BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(this.sU3ScDesc,  this.BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(this.sU4ScDesc,  this.BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(this.sU5ScDesc,  this.BrokerID, "1300");


            //bind the DDLs
            Tools.Bind_CustomFeeDescDDL(this.s800U1FDescDDL, this.BrokerID, "800");
            Tools.Bind_CustomFeeDescDDL(this.s800U2FDescDDL, this.BrokerID, "800");
            Tools.Bind_CustomFeeDescDDL(this.s800U3FDescDDL, this.BrokerID, "800");
            Tools.Bind_CustomFeeDescDDL(this.s800U4FDescDDL, this.BrokerID, "800");
            Tools.Bind_CustomFeeDescDDL(this.s800U5FDescDDL, this.BrokerID, "800");

            Tools.Bind_CustomFeeDescDDL(this.s904PiaDescDDL, this.BrokerID, "900");
            Tools.Bind_CustomFeeDescDDL(this.s900U1PiaDescDDL, this.BrokerID, "900");

            Tools.Bind_CustomFeeDescDDL(this.s1006ProHExpDescDDL, this.BrokerID, "1000");
            Tools.Bind_CustomFeeDescDDL(this.s1007ProHExpDescDDL, this.BrokerID, "1000");
            Tools.Bind_CustomFeeDescDDL(this.sU3RsrvDescDDL, this.BrokerID, "1000");
            Tools.Bind_CustomFeeDescDDL(this.sU4RsrvDescDDL, this.BrokerID, "1000");

            Tools.Bind_CustomFeeDescDDL(this.sU1TcDescDDL, this.BrokerID, "1100");
            Tools.Bind_CustomFeeDescDDL(this.sU2TcDescDDL, this.BrokerID, "1100");
            Tools.Bind_CustomFeeDescDDL(this.sU3TcDescDDL, this.BrokerID, "1100");
            Tools.Bind_CustomFeeDescDDL(this.sU4TcDescDDL, this.BrokerID, "1100");

            Tools.Bind_CustomFeeDescDDL(this.sU1GovRtcDescDDL, this.BrokerID, "1200");
            Tools.Bind_CustomFeeDescDDL(this.sU2GovRtcDescDDL, this.BrokerID, "1200");
            Tools.Bind_CustomFeeDescDDL(this.sU3GovRtcDescDDL, this.BrokerID, "1200");

            Tools.Bind_CustomFeeDescDDL(this.sU1ScDescDDL, this.BrokerID, "1300");
            Tools.Bind_CustomFeeDescDDL(this.sU2ScDescDDL, this.BrokerID, "1300");
            Tools.Bind_CustomFeeDescDDL(this.sU3ScDescDDL, this.BrokerID, "1300");
            Tools.Bind_CustomFeeDescDDL(this.sU4ScDescDDL, this.BrokerID, "1300");
            Tools.Bind_CustomFeeDescDDL(this.sU5ScDescDDL, this.BrokerID, "1300");

            if (broker.IsB4AndB6DisabledIn1100And1300OfGfe)
            {
                this.HideB4andB6();
            }

            Tools.SetDropDownListValue(this.sMBrokFBaseT, gfea.sMBrokFBaseT);
            Tools.SetDropDownListValue(this.sProHazInsT, gfea.sProHazInsT);
            Tools.SetDropDownListValue(this.sProRealETxT, gfea.sProRealETxT);
            Tools.SetDropDownListValue(this.sU3GovRtcBaseT, gfea.sU3GovRtcBaseT);
            Tools.SetDropDownListValue(this.sU2GovRtcBaseT, gfea.sU2GovRtcBaseT);
            Tools.SetDropDownListValue(this.sU1GovRtcBaseT, gfea.sU1GovRtcBaseT);
            Tools.SetDropDownListValue(this.sStateRtcBaseT, gfea.sStateRtcBaseT);
            Tools.SetDropDownListValue(this.sCountyRtcBaseT, gfea.sCountyRtcBaseT);
            Tools.SetDropDownListValue(this.sRecBaseT, gfea.sRecBaseT);
            Tools.SetDropDownListValue(this.sGfeOriginatorCompFBaseT, gfea.sGfeOriginatorCompFBaseT);
            Tools.SetDropDownListValue(this.sLDiscntBaseT, gfea.sLDiscntBaseT);
            Tools.SetDropDownListValue(this.sGfeCreditLenderPaidItemT, gfea.sGfeCreditLenderPaidItemT);
            Tools.SetDropDownListValue(this.sFloodCertificationDeterminationT, gfea.sFloodCertificationDeterminationT);

            sGfeOriginatorCompFPc.Text = gfea.sGfeOriginatorCompFPc_rep;
            sGfeOriginatorCompFMb.Text = gfea.sGfeOriginatorCompFMb_rep;
            sGfeOriginatorCompF.Text = gfea.sGfeOriginatorCompF_rep;


            if (!gfea.sIsRequireFeesFromDropDown)
            {
                this.s800U1FDescDDL.Style.Add("display", "none");
                this.s800U2FDescDDL.Style.Add("display", "none");
                this.s800U3FDescDDL.Style.Add("display", "none");
                this.s800U4FDescDDL.Style.Add("display", "none");
                this.s800U5FDescDDL.Style.Add("display", "none");

                this.s904PiaDescDDL.Style.Add("display", "none");
                this.s900U1PiaDescDDL.Style.Add("display", "none");

                this.s1006ProHExpDescDDL.Style.Add("display", "none");
                this.s1007ProHExpDescDDL.Style.Add("display", "none");
                this.sU3RsrvDescDDL.Style.Add("display", "none");
                this.sU4RsrvDescDDL.Style.Add("display", "none");

                this.sU1TcDescDDL.Style.Add("display", "none");
                this.sU2TcDescDDL.Style.Add("display", "none");
                this.sU3TcDescDDL.Style.Add("display", "none");
                this.sU4TcDescDDL.Style.Add("display", "none");

                this.sU1GovRtcDescDDL.Style.Add("display", "none");
                this.sU2GovRtcDescDDL.Style.Add("display", "none");
                this.sU3GovRtcDescDDL.Style.Add("display", "none");

                this.sU1ScDescDDL.Style.Add("display", "none");
                this.sU2ScDescDDL.Style.Add("display", "none");
                this.sU3ScDescDDL.Style.Add("display", "none");
                this.sU4ScDescDDL.Style.Add("display", "none");
                this.sU5ScDescDDL.Style.Add("display", "none");
            }
            else
            {
                this.s800U1FDescDDL.SelectedValue = gfea.s800U1FDesc;
                this.s800U2FDescDDL.SelectedValue = gfea.s800U2FDesc;
                this.s800U3FDescDDL.SelectedValue = gfea.s800U3FDesc;
                this.s800U4FDescDDL.SelectedValue = gfea.s800U4FDesc;
                this.s800U5FDescDDL.SelectedValue = gfea.s800U5FDesc;

                this.s904PiaDescDDL.SelectedValue = gfea.s904PiaDesc;
                this.s900U1PiaDescDDL.SelectedValue = gfea.s900U1PiaDesc;

                this.s1006ProHExpDescDDL.SelectedValue = gfea.s1006ProHExpDesc;
                this.s1007ProHExpDescDDL.SelectedValue = gfea.s1007ProHExpDesc;
                this.sU3RsrvDescDDL.SelectedValue = gfea.sU3RsrvDesc;
                this.sU4RsrvDescDDL.SelectedValue = gfea.sU4RsrvDesc;

                this.sU1TcDescDDL.SelectedValue = gfea.sU1TcDesc;
                this.sU2TcDescDDL.SelectedValue = gfea.sU2TcDesc;
                this.sU3TcDescDDL.SelectedValue = gfea.sU3TcDesc;
                this.sU4TcDescDDL.SelectedValue = gfea.sU4TcDesc;

                this.sU1GovRtcDescDDL.SelectedValue = gfea.sU1GovRtcDesc;
                this.sU2GovRtcDescDDL.SelectedValue = gfea.sU2GovRtcDesc;
                this.sU3GovRtcDescDDL.SelectedValue = gfea.sU3GovRtcDesc;

                this.sU1ScDescDDL.SelectedValue = gfea.sU1ScDesc;
                this.sU2ScDescDDL.SelectedValue = gfea.sU2ScDesc;
                this.sU3ScDescDDL.SelectedValue = gfea.sU3ScDesc;
                this.sU4ScDescDDL.SelectedValue = gfea.sU4ScDesc;
                this.sU5ScDescDDL.SelectedValue = gfea.sU5ScDesc;
            }
        }

        /// <summary>
        /// Binds the Fee's properties.
        /// </summary>
        /// <param name="name">The prefix of the fee.</param>
        /// <param name="props">The number containing the property values.</param>
        /// <param name="paidTo">The name of whom the fee is paid to.</param>
        /// <param name="page2Selection">The good faith estimate section.</param>
        private void BindProperties(string name, int props, string paidTo, E_GfeSectionT page2Selection)
        {
            HtmlControl apr = (HtmlControl)FindControl(name + "APR");
            HtmlControl fha = (HtmlControl)FindControl(name + "FHA");
            HtmlControl poc = (HtmlControl)FindControl(name + "POC");
            HtmlControl borr = (HtmlControl)FindControl(name + "BSP");
            TextBox paidToTextbox = (TextBox)FindControl(name + "PaidTo");
            DropDownList paidToDDL = (DropDownList)FindControl(name + "PaidToDDL");
            DropDownList paidByList = (DropDownList)FindControl(name + "By");
            DropDownList gfeSectionList = (DropDownList)FindControl(name + "Page2");

            if (paidByList != null)
            {
                this.BindPaidByDDL(paidByList);
                paidByList.SelectedValue = string.Empty + LosConvert.GfeItemProps_Payer(props);
            }

            if (apr != null && LosConvert.GfeItemProps_Apr(props))
            {
                apr.Attributes.Add("Checked", "true");
            }

            if (apr != null && LosConvert.GfeItemProps_FhaAllow(props))
            {
                fha.Attributes.Add("Checked", "true");
            }

            if (poc != null && LosConvert.GfeItemProps_Poc(props))
            {
                poc.Attributes.Add("Checked", "true");
            }

            var hasBorr = LosConvert.GfeItemProps_Borr(props);
            if (borr != null && LosConvert.GfeItemProps_Borr(props))
            {
                borr.Attributes.Add("Checked", "true");
            }

            if (paidToTextbox != null)
            {
                if (paidToDDL != null)
                {
                    CAgentFields loanOfficer = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (LosConvert.GfeItemProps_ToBr(props))
                    {
                        paidToDDL.SelectedValue = "1";

                        paidToTextbox.Text = loanOfficer.CompanyName;
                    }
                    else if (LosConvert.GfeItemProps_PaidToThirdParty(props))
                    {
                        paidToDDL.SelectedValue = "2";
                        paidToTextbox.Text = paidTo;
                    }
                    else
                    {
                        paidToDDL.SelectedValue = "0";

                        paidToTextbox.Text = dataLoan.sLenNm;
                    }
                }
            }

            if (gfeSectionList != null)
            {
                foreach (ListItem item in gfeSectionList.Items)
                {
                    item.Selected = ((E_GfeSectionT)Enum.Parse(typeof(E_GfeSectionT), item.Value)).Equals(page2Selection);
                }
            }
        }

        /// <summary>
        /// Hides the values from the good faith estimate section lists.
        /// </summary>
        private void HideB4andB6()
        {
            List<DropDownList> charges1100 =
                new List<DropDownList>
                {
                    this.sEscrowPage2,
                    this.sOwnerTitleInsPage2,
                    this.sTitleInsPage2,
                    this.sDocPrepPage2,
                    this.sNotaryPage2,
                    this.sAttorneyPage2,
                    this.sU1TcPage2,
                    this.sU2TcPage2,
                    this.sU3TcPage2,
                    this.sU4TcPage2
                };

            foreach (DropDownList list in charges1100)
            {
                foreach (ListItem item in list.Items)
                {
                    if (item.Value.Equals("6"))
                    {
                        item.Enabled = false;
                    }
                }
            }

            List<DropDownList> charges1300 =
                new List<DropDownList>
                {
                    this.sU1ScPage2,
                    this.sU2ScPage2,
                    this.sU3ScPage2,
                    this.sU4ScPage2,
                    this.sU5ScPage2
                };

            foreach (DropDownList list in charges1300)
            {
                foreach (ListItem item in list.Items)
                {
                    if (item.Value.Equals("4"))
                    {
                        item.Enabled = false;
                    }
                }
            }
        }
    }
}
