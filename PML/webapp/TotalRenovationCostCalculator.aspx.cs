﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Linq;
    using global::DataAccess;

    /// <summary>
    /// Total renovation cost calculator for loan file field sTotalRenovationCosts.
    /// </summary>
    public partial class TotalRenovationCostCalculator : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            Tools.Bind_sFHA203kType(this.sFHA203kType);
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">EventArgs.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterService("TotalRenovationCostCalculatorService", "/webapp/TotalRenovationCostCalculatorService.aspx");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TotalRenovationCostCalculator));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            Tools.SetDropDownListValue(this.sFHA203kType, dataLoan.sFHA203kType);

            this.sCostConstrRepairsRehab.Text = dataLoan.sCostConstrRepairsRehab_rep; // 1.a
            this.sContingencyRsrvPcRenovationHardCost.Text = dataLoan.sContingencyRsrvPcRenovationHardCost_rep; // 1.b percentage
            this.sFinanceContingencyRsrv.Text = dataLoan.sFinanceContingencyRsrv_rep; // 1.b
            this.sArchitectEngineerFee.Text = dataLoan.sArchitectEngineerFee_rep; // 1.c
            this.sArchitectEngineerFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee).Any();
            this.sConsultantFee.Text = dataLoan.sConsultantFee_rep; // 1.d
            this.sConsultantFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KConsultantFee).Any();
            this.sRenovationConstrInspectFee.Text = dataLoan.sRenovationConstrInspectFee_rep; // 1.e
            this.sRenovationConstrInspectFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KInspectionFee).Any();
            this.sTitleUpdateFee.Text = dataLoan.sTitleUpdateFee_rep; // 1.f
            this.sTitleUpdateFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KTitleUpdate).Any();
            this.sPermitFee.Text = dataLoan.sPermitFee_rep; // 1.g
            this.sPermitFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KPermits).Any();
            this.sFinanceMortgagePmntRsrv.Text = dataLoan.sFinanceMortgagePmntRsrv_rep; // 1.h
            this.sRenoMtgPmntRsrvMnth.Text = dataLoan.sRenoMtgPmntRsrvMnth_rep; // 1.h months
            this.sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep; // 1.h months multiplier
            this.sFinanceMortgagePmntRsrvTgtVal.Text = dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep; // 1.h Target
            this.sFinanceMortgagePmntRsrvTgtValLckd.Checked = dataLoan.sFinanceMortgagePmntRsrvTgtValLckd; // 1.h Target Locked box
            this.sMaxFinanceMortgagePmntRsrv.Text = dataLoan.sMaxFinanceMortgagePmntRsrv_rep; // 1.h Max value
            this.sOtherRenovationCost.Text = dataLoan.sOtherRenovationCost_rep; // 1.i
            this.sOtherRenovationCostDescription.Text = dataLoan.sOtherRenovationCostDescription; // 1.i Description
            this.sFinanceOriginationFee.Text = dataLoan.sFinanceOriginationFee_rep; // 1.i financeable origination fee
            this.sFinanceOriginationFeeTgtVal.Text = dataLoan.sFinanceOriginationFeeTgtVal_rep; // 1.i financeable origination fee Target
            this.sMaxFinanceOriginationFee.Text = dataLoan.sMaxFinanceOriginationFee_rep; // 1.i financeable origination fee Max value
            this.sDiscntPntOnRepairCostFee.Text = dataLoan.sDiscntPntOnRepairCostFee_rep; // 1.i discount points
            this.sDiscntPntOnRepairCostFeeTgtVal.Text = dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep; // 1.i discount points Target
            this.sMaxDiscntPntOnRepairCostFee.Text = dataLoan.sMaxDiscntPntOnRepairCostFee_rep; // 1.i. discount points Max value
            this.sFeasibilityStudyFee.Text = dataLoan.sFeasibilityStudyFee_rep; // 1.i feasibility study
            this.sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep; // 2
        }
    }
}