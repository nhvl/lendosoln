﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RunLoanComparison.aspx.cs" Inherits="PriceMyLoan.webapp.RunLoanComparison" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Loan Comparison</title>
    <style type="text/css">
        div.loading {
            width: 400px;
            height: 100px;
            margin: 150px auto 0 auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:PlaceHolder runat="server" ID="RunningLoanComparison">
    <div class="loading WaitMessageLabel"  >
        <asp:Image runat="server" ID="LoadingGiff" ImageUrl="~/images/status.gif" />
        <br />
        <ml:EncodedLiteral runat="server" ID="WaitMsg" >  Composing Comparison Report... </ml:EncodedLiteral>
    </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder runat="server" ID="NoPinnnedStates">
        Error - No Pinned States 
    </asp:PlaceHolder>
    
    </form>
    <script type="text/javascript">
        $(function(){
            var requestId = $('#RequestId').val(),
                loanId = $('#LoanId');
            
            
            if (!requestId){
                return; //nothing to do
            }
            
            function checkStatus()
            {
                $.ajax({
                    url: 'RunLoanComparison.aspx/GetResult',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({  loanId : loanId.val(), requestId : requestId } )
                }).done(function(msg) {
                        var status = msg.d.Status;
                        
                        if (status == "ExpiredRunAgain") {
                            window.location.reload();
                        }
                        else if( status == "NotReady") {
                            setTimeout(checkStatus, 2000);
                        }
                        else if (status == 'Ready') {
                            window.location = window.location + "&ResultId=" + msg.d.ResultId;
                        }
                        else {
                            alert("Unhandled System Error");
                        }
                        
                }).fail(function(msg) {
                    alert('System error.');
                });
            }
            
            checkStatus();
            
        });
    </script>
</body>
</html>
