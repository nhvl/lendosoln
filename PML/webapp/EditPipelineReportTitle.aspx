﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditPipelineReportTitle.aspx.cs" Inherits="PriceMyLoan.webapp.EditPipelineReportTitle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Pipeline Report Title</title>
    <script>
        $(function () {
            var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
            var iframeSelf = new IframeSelf();
            iframeSelf.popup = popup;

            $("#SaveButton").click(function () {
                iframeSelf.resolve({ OK: true, Title: $("#NewReportTitle").val() });
            });

            $("#CancelButton").click(function () {
                iframeSelf.resolve({ OK: false });
            });

            TPOStyleUnification.Components();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div lqb-popup>
        <div class="modal-header">
            <h4 class="modal-title">Edit Pipeline Report Title</h4>
        </div>
        <div class="modal-body">
            <div class="table">
                <div>
                    <div>
                        <label>Report Title:</label>
                    </div>
                    <div>
                        <span runat="server" id="ReportTitle"></span>
                    </div>
                </div>
                <div>
                    <div>
                        <label>New Title:</label>
                    </div>
                    <div>
                        <input type="text" id="NewReportTitle" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-flat" id="CancelButton">Cancel</button>
            <button type="button" class="btn btn-flat" id="SaveButton">Save</button>
        </div>
    </div>
    </form>
</body>
</html>
