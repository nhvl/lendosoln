﻿// <copyright file="OrderServicesService.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: eduardom
//    Date:   9/9/2014 11:45:49 AM 
// </summary>
namespace PriceMyLoan.webapp
{
    using EDocs;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.Security;
    using LQBAppraisal.LQBAppraisalRequest;
    using LQBAppraisal.LQBAppraisalResponse;
    using LqbGrammar.Commands;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Web.UI.WebControls;

    public partial class OrderServicesService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private const int DDL_SENTINEL_VALUE = -1;

        private string passwordPlaceholder = "************";

        private AbstractUserPrincipal CurrentPrincipal
        {
            get
            {
                return ((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "AttachGDMSDocuments":
                    AttachGDMSDocuments();
                    break;
                case "LoadOptions":
                    this.LoadOptions();
                    break;
                case "SubmitOrder":
                    this.SubmitOrder();
                    break;
                case "LoadDocumentTypeOptions":
                    LoadDocumentTypeOptions();
                    break;
                case "SendEmail":
                    SendEmail();
                    break;
                case "AttachFrameworkDocuments":
                    AttachFrameworkDocuments();
                    break;
                case "UpdateCredentials":
                    UpdateCredentials();
                    break;
                case "Load4506T":
                    Load4506T();
                    break;
                case "Save4506T":
                    Save4506T();
                    break;
                case "Submit4506TOrder":
                    Submit4506TOrder();
                    break;
                case "Get4506TCredentials":
                    Get4506TCredentials();
                    break;
                case "Check4506TOrderStatus":
                    Check4506TOrderStatus();
                    break;
                case "GetAppraisalCredentials":
                    GetAppraisalCredentials();
                    break;
                    
            }
        }

        private void Check4506TOrderStatus()
        {
            AbstractUserPrincipal user = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;

            Guid loanId = GetGuid("LoanId");
            string orderNumber = GetString("OrderNumber");
            Guid vendorId = GetGuid("VendorId");

            Irs4506TOrderInfo order = Irs4506TOrderInfo.Retrieve(vendorId, loanId, orderNumber, user.BrokerId);
            if (order == null)
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", "The order was not found. Please refresh the page and try again.");
                return;
            }

            Irs4506TVendorConfiguration vendor = Irs4506TVendorConfiguration.RetrieveById(order.VendorId);
            if (vendor.CommunicationModel == Irs4506TCommunicationModel.LendingQB)
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", "The vendor for this order does not support checking status. The order will automatically update when it is done.");
                return;
            }

            if (vendor.CommunicationModel == Irs4506TCommunicationModel.EquifaxVerificationServices)
            {
                Irs4506TVendorCredential credential;
                bool hasCredential = GetBool("HasCredentials");
                if (hasCredential)
                {
                    credential = new Irs4506TVendorCredential(CurrentPrincipal.BrokerId, GetString("Username"), this.GetString("Password"), GetString("AccountId"), vendorId, user.UserId);
                }
                else
                {
                    credential = Irs4506TVendorCredential.Retrieve(CurrentPrincipal.BrokerId, CurrentPrincipal.UserId, vendorId);
                }

                if (string.IsNullOrEmpty(credential.DecryptedPassword))
                {
                    SetResult("Status", "Error");
                    SetResult("ErrorMessage", "There are no credentials on file. Please enter a Login and Password in the Add Order form.");
                    return;
                }

                IStatusDetails<Irs4506TOrderStatus, string> statusDetails = vendor.Server.CheckOrderStatus(loanId, order, user, credential);

                SetResult("Status", statusDetails.Status.ToString());
                SetResult("StatusMessage", statusDetails.Messages.Any() ? statusDetails.Messages[0] : string.Empty);

                if (hasCredential)
                {
                    credential.Save();
                }

                return;
            }
        }

        private void GetAppraisalCredentials()
        {
            Guid vendorId = GetGuid("VendorId");

            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);

            if (vendor.UsesGlobalDMS)
            {
                GDMSCredentials credentials;
                if (GetGDMSCredentials(vendor, out credentials))
                {
                    SetResult("login", credentials.UserName);
                    SetResult("AccountId", credentials.CompanyId);

                    if (string.IsNullOrEmpty(credentials.Password))
                    {
                        SetResult("Password", "");
                    }
                    else
                    {
                        SetResult("Password", passwordPlaceholder);
                    }
                }
            }
            else
            {
                AppraisalVendorCredentials credentials = new AppraisalVendorCredentials(CurrentPrincipal.BrokerId, CurrentPrincipal.EmployeeId, vendorId);

                SetResult("login", credentials.UserName);
                SetResult("AccountId", credentials.AccountId);

                if (string.IsNullOrEmpty(credentials.Password))
                {
                    SetResult("Password", "");
                }
                else
                {
                    SetResult("Password", passwordPlaceholder);
                }
            }
            
        }

        private void Get4506TCredentials()
        {
            Guid vendorId = GetGuid("VendorId");
            Irs4506TVendorCredential credential = Irs4506TVendorCredential.Retrieve(CurrentPrincipal.BrokerId, CurrentPrincipal.UserId, vendorId);

            Irs4506TVendorConfiguration vendor = Irs4506TVendorConfiguration.RetrieveById(vendorId);

            SetResult("login", credential.UserName);
            SetResult("AccountId", credential.AccountId);

            string decrypted = credential.DecryptedPassword;
            if (string.IsNullOrEmpty(decrypted))
            {
                SetResult("Password", "");
            }
            else
            {
                SetResult("Password", passwordPlaceholder);
            }
        }

        private void Submit4506TOrder()
        {
            Guid sLId = GetGuid("LoanID");

            // AppName has the following format: {aAppId}:{B | C}:{Name}
            string appName = GetString("AppName");

            string[] parts = appName.Split(':');


            Guid aAppId = new Guid(parts[0]);
            string borrowerName = parts[2];
            string borrType = parts[1];
            Guid vendorId = GetGuid("VendorId");
            Guid documentId = GetGuid("DocumentID", Guid.Empty);
            bool hasCredentials = GetBool("HasCredentials");

            Guid transactionId = Guid.NewGuid();

            Guid userId = CurrentPrincipal.UserId;

            Irs4506TVendorCredential credential;
            if (hasCredentials)
            {
                credential = new Irs4506TVendorCredential(CurrentPrincipal.BrokerId, GetString("Username"), this.GetString("Password"), GetString("AccountId"), vendorId, userId);
            }
            else
            {
                credential = Irs4506TVendorCredential.Retrieve(CurrentPrincipal.BrokerId, CurrentPrincipal.UserId, vendorId);
            }

            if (string.IsNullOrEmpty(credential.DecryptedPassword))
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", "There are no credentials on file. Please enter a Login and Password in the Add Order form.");
                return;
            }

            EDocument edoc = null;
            if (documentId != Guid.Empty)
            {
                EDocumentRepository repository = EDocumentRepository.GetUserRepository();

                edoc = repository.GetDocumentById(documentId);
            }

            Irs4506TVendorConfiguration vendor = Irs4506TVendorConfiguration.RetrieveById(vendorId);

            IStatusDetails<bool, string> result = 
                vendor.Server.SubmitNewOrder(
                    sLId,
                    aAppId,
                    borrType == "C" ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower,
                    edoc, 
                    LendersOffice.Security.PrincipalFactory.CurrentPrincipal, 
                    credential,
                    borrowerName);

            if (result.Status)
            {
                SetResult("Status", "OK");
            }
            else
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", string.Join("; ", result.Messages));
            }

            if (hasCredentials)
            {
                credential.Save();
            }
        }

        private void Save4506T()
        {
            Guid sLId = GetGuid("sLId");
            Guid appId = GetGuid("appId");
            int fileVersion = GetInt("sFileVersion");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(OrderServicesService));
            dataLoan.InitSave(1);
            CAppData dataApp = dataLoan.GetAppData(appId);

            dataApp.aB4506TNm =  GetString("aB4506TNm");
            dataApp.aB4506TSsnTinEin =  GetString("aB4506TSsnTinEin");
            dataApp.aC4506TNm =  GetString("aC4506TNm");
            dataApp.aC4506TSsnTinEin =  GetString("aC4506TSsnTinEin");
            dataApp.aB4506TNmLckd =  GetBool("aB4506TNmLckd");
            dataApp.aC4506TNmLckd =  GetBool("aC4506TNmLckd");

            // TPO users are only given the option of doing a joint 4506-T request.
            dataApp.aB4506TPrevStreetAddr = GetString("a4506TPrevStreetAddr");
            dataApp.aB4506TPrevCity = GetString("a4506TPrevCity");
            dataApp.aB4506TPrevState = GetString("a4506TPrevState");
            dataApp.aB4506TPrevZip = GetString("a4506TPrevZip");
            dataApp.aB4506TThirdPartyName = GetString("a4506TThirdPartyName");
            dataApp.aB4506TThirdPartyStreetAddr = GetString("a4506TThirdPartyStreetAddr");
            dataApp.aB4506TThirdPartyCity = GetString("a4506TThirdPartyCity");
            dataApp.aB4506TThirdPartyState = GetString("a4506TThirdPartyState");
            dataApp.aB4506TThirdPartyZip = GetString("a4506TThirdPartyZip");
            dataApp.aB4506TThirdPartyPhone = GetString("a4506TThirdPartyPhone");
            dataApp.aB4506TTranscript = GetString("a4506TTranscript");
            dataApp.aB4056TIsReturnTranscript = GetBool("a4056TIsReturnTranscript");
            dataApp.aB4506TIsAccountTranscript = GetBool("a4506TIsAccountTranscript");
            dataApp.aB4056TIsRecordAccountTranscript = GetBool("a4056TIsRecordAccountTranscript");
            dataApp.aB4506TVerificationNonfiling = GetBool("a4506TVerificationNonfiling");
            dataApp.aB4506TSeriesTranscript = GetBool("a4506TSeriesTranscript");
            dataApp.aB4506TYear1_rep = GetString("a4506TYear1");
            dataApp.aB4506TYear2_rep = GetString("a4506TYear2");
            dataApp.aB4506TYear3_rep = GetString("a4506TYear3");
            dataApp.aB4506TYear4_rep = GetString("a4506TYear4");
            dataApp.aB4506TRequestYrHadIdentityTheft = GetBool("a4506TRequestYrHadIdentityTheft");

            dataLoan.Save();
        }
        private void Load4506T()
        {
            Guid sLId = GetGuid("sLId");
            Guid appId = GetGuid("appId");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(OrderServicesService));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(appId);

            SetResult("aB4506TNm", dataApp.aB4506TNm);
            SetResult("aB4506TSsnTinEin", dataApp.aB4506TSsnTinEin);
            SetResult("aC4506TNm", dataApp.aC4506TNm);
            SetResult("aC4506TSsnTinEin", dataApp.aC4506TSsnTinEin);
            SetResult("aB4506TNmLckd", dataApp.aB4506TNmLckd);
            SetResult("aC4506TNmLckd", dataApp.aC4506TNmLckd);
            SetResult("a4506TPrevStreetAddr", dataApp.a4506TPrevStreetAddr);
            SetResult("a4506TPrevCity", dataApp.a4506TPrevCity);
            SetResult("a4506TPrevState", dataApp.a4506TPrevState);
            SetResult("a4506TPrevZip", dataApp.a4506TPrevZip);
            SetResult("a4506TThirdPartyName", dataApp.a4506TThirdPartyName);
            SetResult("a4506TThirdPartyStreetAddr", dataApp.a4506TThirdPartyStreetAddr);
            SetResult("a4506TThirdPartyCity", dataApp.a4506TThirdPartyCity);
            SetResult("a4506TThirdPartyState", dataApp.a4506TThirdPartyState);
            SetResult("a4506TThirdPartyZip", dataApp.a4506TThirdPartyZip);
            SetResult("a4506TThirdPartyPhone", dataApp.a4506TThirdPartyPhone);
            SetResult("a4506TTranscript", dataApp.a4506TTranscript);
            SetResult("a4056TIsReturnTranscript", dataApp.a4056TIsReturnTranscript);
            SetResult("a4506TIsAccountTranscript", dataApp.a4506TIsAccountTranscript);
            SetResult("a4056TIsRecordAccountTranscript", dataApp.a4056TIsRecordAccountTranscript);
            SetResult("a4506TVerificationNonfiling", dataApp.a4506TVerificationNonfiling);
            SetResult("a4506TSeriesTranscript", dataApp.a4506TSeriesTranscript);
            SetResult("a4506TYear1", dataApp.a4506TYear1_rep);
            SetResult("a4506TYear2", dataApp.a4506TYear2_rep);
            SetResult("a4506TYear3", dataApp.a4506TYear3_rep);
            SetResult("a4506TYear4", dataApp.a4506TYear4_rep);
            SetResult("a4506TRequestYrHadIdentityTheft", dataApp.a4506TRequestYrHadIdentityTheft);
        }

        private void UpdateCredentials()
        {
            if (GetBool("HasCredentials"))
            {
                Guid vendorId = GetGuid("VendorId");
                AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);

                string username = GetString("Username");
                string password = GetString("Password");
                string accountID = GetString("AccountId");
                try
                {
                    if (vendor.UsesGlobalDMS)
                    {

                        GDMSCredentials credentials = new GDMSCredentials();
                        int.TryParse(accountID, out credentials.CompanyId);
                        credentials.ExportToStage = vendor.ExportToStagePath;
                        credentials.Password = password;
                        credentials.UserName = username;
                        credentials.UserType = 2;

                        using (var lookupMethodsClient = new LookupMethodsClient(credentials))
                        {
                            try
                            {
                                var propertyTypes = lookupMethodsClient.GetPropertyTypes();
                            }

                            catch (System.Net.Sockets.SocketException exc)
                            {
                                SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                                Tools.LogError(exc);
                                return;
                            }
                            catch (System.Xml.XmlException exc)
                            {
                                SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                                Tools.LogError(exc);
                                return;
                            }
                            catch (System.Net.WebException exc)
                            {
                                SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                                Tools.LogError(exc);
                                return;
                            }
                            catch (System.TimeoutException exc)
                            {
                                SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                                Tools.LogError(exc);
                                return;
                            }
                        }
                    }
                    else
                    {
                        AppraisalVendorCredentials cred = new AppraisalVendorCredentials(CurrentPrincipal.BrokerId, vendorId, accountID, username, password);

                        // Attempt to retrieve the options from the db
                        LQBAppraisalRequest request = LQBAppraisalExporter.CreateAccountInfoRequest(cred, GetGuid("LoanId"), vendor.VendorName);
                        LQBAppraisalResponse response = AppraisalVendorServer.Submit(request, vendor.PrimaryExportPath);
                        if (response.ServerResponse.RequestStatus != E_ServerResponseRequestStatus.Success)
                        {
                            SetResult("ErrorMessage", CreateErrorMessage(response.ServerResponse));
                            return;
                        }
                    }

                    AppraisalVendorCredentials login = new AppraisalVendorCredentials(CurrentPrincipal.BrokerId, CurrentPrincipal.EmployeeId, vendorId);
                    login.AccountId = accountID;
                    login.UserName = username;
                    if (!string.IsNullOrEmpty(password))
                    {
                        login.Password = password;
                    }

                    login.SaveCredentials();
                }
                catch (GDMSErrorResponseException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                catch (System.Net.Sockets.SocketException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                catch (System.Xml.XmlException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                catch (System.Net.WebException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                catch (System.TimeoutException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
            }

        }

        private void SendEmail()
        {
            Guid vendorId = GetGuid("VendorId");
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);
            GDMSCredentials credentials;
            if (!GetGDMSCredentials(vendor, out credentials))
            {
                SetResult("ErrorMessage", "Unable to send email: Invalid Credentials");
                return;
            }

            var emailToSend = new LendersOffice.GDMS.Orders.SendEmailMessageObject();
            emailToSend.Subject = GetString("SubjectField");
            emailToSend.EmailMessage = GetString("MessageField");
            emailToSend.SendEmailToUserType = OrdersClient.sendEmailToWho(GetString("ToField"));

            var orderIdentification = new LendersOffice.GDMS.Orders.OrderIdentifier();
            orderIdentification.FileNumber = GetInt("AppraisalOrderNumber");

            string reply = "";
            using (var ordersClient = new OrdersClient(credentials))
            {
                try
                {
                    reply = ordersClient.SendEmailCommunication(emailToSend, orderIdentification);
                    SetResult("Success", "True");
                }
                catch (GDMSErrorResponseException exc)
                {
                    string errorMessage = CreateErrorMessage(exc);
                    foreach (string error in exc.Errors)
                    {
                        if (error == "Unable to send email: The message did not have a 'To' email address")
                        {
                            errorMessage = "Unable to send email: Appraiser not yet assigned to this order";
                            break;
                        }
                    }
                    SetResult("ErrorMessage", errorMessage);
                }
            }
        }

        private void LoadDocumentTypeOptions()
        {
            Guid vendorId = GetGuid("VendorId");

            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);
            SetResult("IsGdms", vendor.UsesGlobalDMS);
            if (vendor.UsesGlobalDMS)
            {
                GDMSCredentials credentials;
                GetGDMSCredentials(vendor, out credentials);
                using (var lookupMethodsClient = new LookupMethodsClient(credentials))
                {
                    try
                    {
                        var orderFileUploadFileTypes = lookupMethodsClient.GetOrderFileUploadFileTypes();
                        orderFileUploadFileTypes = Array.FindAll(orderFileUploadFileTypes, o => o.ViewableByClient);

                        IEnumerable<string[]> options; // string[] = DDL option: ["Value", "Text"]
                        options = orderFileUploadFileTypes.Select<LendersOffice.GDMS.LookupMethods.OrderFileUploadFileType, string[]>(o => new string[] { o.FileTypesId.ToString(), o.FileTypeDescription });
                        SetResult("GDMSOrderFileUploadFileType", ObsoleteSerializationHelper.JsonSerialize(options));
                    }
                    catch (GDMSErrorResponseException exc)
                    {
                        SetResult("ErrorMessage", CreateErrorMessage(exc));
                        Tools.LogError(exc);
                        return;
                    }
                }
            }
            SetResult("IsValid", "True");
        }

        private bool GetGDMSCredentials(AppraisalVendorConfig vendor, out GDMSCredentials credentials)
        {
            string errorMessage;
            credentials = vendor.GetGDMSCredentials(this.CurrentPrincipal.BrokerId, this.CurrentPrincipal.EmployeeId, out errorMessage);
            if (credentials == null)
            {
                SetResult("ErrorMessage", errorMessage);
                return false;
            }

            return true;
        }

        [DataContract]
        private class AppraisalOrderProduct
        {
            //used for Javascript serialization for product dependencies - OPM 125002
            public AppraisalOrderProduct(string n, string r)
            {
                name = n;
                requires = r;
            }
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public string requires { get; set; }
        }

        private void LoadOptions()
        {
            Guid vendorId = GetGuid("VendorId");
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);

            string username = "";
            string password = "";
            string accountID = "";

            SetResult("IsGdms", vendor.UsesGlobalDMS);
            
            if (!vendor.UsesGlobalDMS)
            {
                //If credentials are supplied, use them.  If not, get the credentials from the DB.
                AppraisalVendorCredentials cred;
                if (GetBool("HasCredentials"))
                {
                    username = GetString("Username");
                    password = GetString("Password");
                    accountID = GetString("AccountId");
                    cred = new AppraisalVendorCredentials(CurrentPrincipal.BrokerId, vendorId, accountID, username, password);
                    UpdateCredentials();
                }
                else
                {
                    cred = new AppraisalVendorCredentials(CurrentPrincipal.BrokerId, CurrentPrincipal.EmployeeId, vendorId);
                }

                //attempt to retrieve the options from the db
                try
                {
                    LQBAppraisalRequest request = LQBAppraisalExporter.CreateAccountInfoRequest(cred, GetGuid("LoanId"), vendor.VendorName);
                    LQBAppraisalResponse response;

                    response = AppraisalVendorServer.Submit(request, vendor.PrimaryExportPath);
                    if (response.ServerResponse.RequestStatus == E_ServerResponseRequestStatus.Success)
                    {
                        List<AppraisalOrderProduct> products = new List<AppraisalOrderProduct>();
                        foreach (AvailableProduct prod in response.AccountInfoResponse.AvailableProductList)
                        {
                            products.Add(new AppraisalOrderProduct(prod.ProductName, ""));
                            foreach (AvailableSecondaryProduct prod2 in prod.AvailableSecondaryProductList)
                            {
                                products.Add(new AppraisalOrderProduct(prod2.ProductName, prod.ProductName));
                            }
                        }

                        var billing = response.AccountInfoResponse.BillingMethodList.Select(method => new BillingMethodDetails(method));
                        SetResult("ProductsList", ObsoleteSerializationHelper.JsonSerialize(products.ToArray()));
                        SetResult("BillingList", SerializationHelper.JsonNetAnonymousSerialize(billing));
                        SetResult("IsValid", "True");

                    }
                    else
                    {
                        string errorMessage = CreateErrorMessage(response.ServerResponse);
                        Tools.LogError(errorMessage);
                        errorMessage = "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.";
                        SetResult("ErrorMessage", errorMessage);
                    }
                }

                catch (System.Net.Sockets.SocketException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                catch (System.Xml.XmlException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                catch (System.Net.WebException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                catch (System.TimeoutException exc)
                {
                    SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                    Tools.LogError(exc);
                    return;
                }
                
                
            }
            else
            {
                GDMSCredentials m_credentials;
                string errorMessage;

                if (GetBool("HasCredentials"))
                {
                    m_credentials = new GDMSCredentials();
                    int.TryParse(GetString("AccountId"), out m_credentials.CompanyId);
                    m_credentials.Password = GetString("Password");
                    m_credentials.UserName = GetString("Username");
                    m_credentials.ExportToStage = vendor.ExportToStagePath;
                    m_credentials.UserType = 2;

                    //update their lqb credentials with this data
                    UpdateCredentials();
                }
                else
                {
                    m_credentials = vendor.GetGDMSCredentials(this.CurrentPrincipal.BrokerId, this.CurrentPrincipal.EmployeeId, out errorMessage);
                }

                using (var lookupMethodsClient = new LookupMethodsClient(m_credentials))
                {
                    try
                    {
                        var propertyTypes = lookupMethodsClient.GetPropertyTypes();
                        var products = lookupMethodsClient.GetProducts();
                        var intendedUses = lookupMethodsClient.GetIntendedUses();
                        var loanTypes = lookupMethodsClient.GetLoanTypes();
                        var occupancyTypes = lookupMethodsClient.GetOccupancyTypes();

                        var client2Users = lookupMethodsClient.GetClient2Users();
                        var clientUsers = lookupMethodsClient.GetClientUsers(0);

                        var billingMethods = lookupMethodsClient.GetBillingMethods();

                        var orderFileUploadFileTypes = lookupMethodsClient.GetOrderFileUploadFileTypes();
                        orderFileUploadFileTypes = Array.FindAll(orderFileUploadFileTypes, o => o.ViewableByClient);

                        IEnumerable<string[]> options; // string[] = DDL option: ["Value", "Text"]
                        options = propertyTypes.Select<LendersOffice.GDMS.LookupMethods.PropertyType, string[]>(o => new string[] { o.PropertyTypeId.ToString(), o.PropertyTypeName });
                        SetResult("GDMSPropertyType", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = products.Select<LendersOffice.GDMS.LookupMethods.Product, string[]>(o => new string[] { o.ProductID.ToString(), o.ProductFriendlyName });
                        SetResult("GDMSReportType", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = intendedUses.Select<LendersOffice.GDMS.LookupMethods.LoanType, string[]>(o => new string[] { o.LoanTypeId.ToString(), o.LoanTypeName });
                        SetResult("GDMSIntendedUse", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = loanTypes.Select<LendersOffice.GDMS.LookupMethods.LoanType, string[]>(o => new string[] { o.LoanTypeId.ToString(), o.LoanTypeName });
                        SetResult("GDMSLoanType", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = occupancyTypes.Select<LendersOffice.GDMS.LookupMethods.OccupancyType, string[]>(o => new string[] { o.OccupancyID.ToString(), o.OccupancyName });
                        SetResult("GDMSOccupancyType", ObsoleteSerializationHelper.JsonSerialize(options));

                        var billingMethodViews = billingMethods.Select(method => new BillingMethodDetails(method));
                        SetResult("GDMSBillingMethod", SerializationHelper.JsonNetAnonymousSerialize(billingMethodViews));

                        options = client2Users.Select<LendersOffice.GDMS.LookupMethods.clsClientForDropDown, string[]>(o => new string[] { o.ClientID.ToString(), o.ClientName });
                        SetResult("GDMSClient2", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = clientUsers.Select<LendersOffice.GDMS.LookupMethods.clsClientUser, string[]>(o => new string[] { o.ClientUserID.ToString(), o.UserFullName });
                        SetResult("GDMSProcessor", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = clientUsers.Select<LendersOffice.GDMS.LookupMethods.clsClientUser, string[]>(o => new string[] { o.ClientUserID.ToString(), o.UserFullName });
                        SetResult("GDMSProcessor2", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = orderFileUploadFileTypes.Select<LendersOffice.GDMS.LookupMethods.OrderFileUploadFileType, string[]>(o => new string[] { o.FileTypesId.ToString(), o.FileTypeDescription });
                        SetResult("GDMSOrderFileUploadFileType", ObsoleteSerializationHelper.JsonSerialize(options));

                    }
                    catch (GDMSErrorResponseException exc)
                    {
                        SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                        Tools.LogError(exc);
                        return;
                    }
                    catch (System.Net.Sockets.SocketException exc)
                    {
                        SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                        Tools.LogError(exc);
                        return;
                    }
                    catch (System.Xml.XmlException exc)
                    {
                        SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                        Tools.LogError(exc);
                        return;
                    }
                    catch (System.Net.WebException exc)
                    {
                        SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                        Tools.LogError(exc);
                        return;
                    }
                    catch (System.TimeoutException exc)
                    {
                        SetResult("ErrorMessage", "Connection with " + vendor.VendorName + " has failed.  Please contact your Account Executive.");
                        Tools.LogError(exc);
                        return;
                    }
                    finally
                    {
                        SetResult("IsValid", "True");
                    }
                }
            }
        }

        private bool UserHasOrderAppraisalPrivilege(Guid LoanID)
        {
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(CurrentPrincipal.ConnectionInfo, CurrentPrincipal.BrokerId, LoanID, WorkflowOperations.OrderAppraisal);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(CurrentPrincipal));

            bool bHasPrivilege = LendingQBExecutingEngine.CanPerform(WorkflowOperations.OrderAppraisal, valueEvaluator);

            if (!bHasPrivilege)
            {
                SetResult("ErrorMessage", LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.OrderAppraisal, valueEvaluator));
            }

            return bHasPrivilege;
        }

        private void AttachGDMSDocuments()
        {
            Guid vendorId = GetGuid("VendorId");
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);
            GDMSCredentials credentials;
            bool credentialsBuilt = GetGDMSCredentials(vendor, out credentials);
            int fileNumber = GetInt("AppraisalOrderNumber");
            if (credentialsBuilt && AttachGDMSDocumentsToOrder(credentials, fileNumber))
                SetResult("Success", "True");
            else
                SetResult("Success", "False");
        }

        private bool AttachGDMSDocumentsToOrder(GDMSCredentials credentials, int fileNumber)
        {
            List<Guid> docIds = null;
            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            if (ids != null)
            {
                docIds = ids.Select(id => new Guid(id)).ToList();
                EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                StringBuilder errorList = new StringBuilder();
                foreach (Guid docId in docIds)
                {
                    EDocument edoc = repo.GetDocumentById(docId);
                    string fileName = edoc.FolderAndDocTypeName + ".pdf";
                    //need to make sure fileName has no invalid characters for a file name; otherwise GDMS rejects the file
                    fileName = fileName.Replace('/', '-').Replace('\\', '-').Replace('?', '-').Replace('%', '-').Replace('*', '-')
                            .Replace(':', '-').Replace('|', '-').Replace('"', '-').Replace('<', '-').Replace('>', '-');
                    string filePath = edoc.GetPDFTempFile_Current();
                    using (System.IO.MemoryStream fileData = new System.IO.MemoryStream(BinaryFileHelper.ReadAllBytes(filePath)))
                    {
                        using (var fileUploadClient = new FileUploadClient(credentials, fileName, fileNumber, GetInt("AttachmentType"), fileData))
                        {
                            try
                            {
                                var uploadResults = fileUploadClient.SubmitReport();
                                if (!uploadResults.success)
                                {
                                    if (errorList.Length != 0) errorList.AppendLine();
                                    errorList.AppendFormat("{0}:\n\t{1} did not upload properly.", uploadResults.message, fileName);
                                }
                            }
                            catch (GDMSErrorResponseException exc)
                            {
                                Tools.LogError(string.Format("Unable to upload file: {0} to ETrac.\n", fileName), exc);
                                if (errorList.Length != 0) errorList.AppendLine();
                                errorList.AppendFormat("Unable to upload file {0}. Please contact support if this happens again.", fileName);
                            }
                            catch (OutOfMemoryException exc)
                            {
                                Tools.LogError(string.Format("Unable to upload file: {0} to ETrac.\n", fileName), exc);
                                if (errorList.Length != 0) errorList.AppendLine();
                                errorList.AppendFormat("Unable to upload file {0}. Please contact support if this happens again.", fileName);
                            }
                        }
                    }
                }
                if (errorList.Length != 0)
                {
                    SetResult("FileErrorMessage", errorList.ToString());
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }

        public static string GetFriendlyCCTypeNameFromEnumValue(string cctypeEnumValue) // opm 126671
        {
            CommonLib.CreditCardType val;
            try
            {
                val = (CommonLib.CreditCardType)Enum.Parse(typeof(CommonLib.CreditCardType), cctypeEnumValue);
            }
            catch (System.ArgumentException e)
            {
                Tools.LogError("unhandle cctype " + cctypeEnumValue, e);
                return cctypeEnumValue;
            }
            switch (val)
            {
                case CommonLib.CreditCardType.AmericanExpress:
                    return "Amex";
                case CommonLib.CreditCardType.Discover:
                    return "Discover";
                case CommonLib.CreditCardType.MasterCard:
                    return "Mastercard";
                case CommonLib.CreditCardType.Visa:
                    return "Visa";
                default:
                    Tools.LogError("unhandle cctype " + cctypeEnumValue);
                    return cctypeEnumValue;
            }

        }

        private void AttachFrameworkDocuments()
        {
            string orderNumber = GetString("AppraisalOrderNumber");
            Guid LoanID = GetGuid("LoanId");
            LQBAppraisalOrder order = LQBAppraisalOrder.Load(LoanID.ToString(), orderNumber);
            if (order == null)
                throw new CBaseException("Order Number not found", "Failed to load order number " + orderNumber);

            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(GetGuid("VendorId"));
            var cred = new AppraisalVendorCredentials(CurrentPrincipal.BrokerId, CurrentPrincipal.EmployeeId, vendor.VendorId);

            var docs = new List<EmbeddedDoc>();
            List<Guid> docIds = null;
            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            order.AppraisalOrder.EmbeddedDocList.Clear();
            if (ids != null)
            {
                docIds = ids.Select(id => new Guid(id)).ToList();
                EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                foreach (Guid docId in docIds)
                {
                    EDocument edoc = repo.GetDocumentById(docId);
                    EmbeddedDoc embeddedDoc = new EmbeddedDoc();
                    embeddedDoc.DocumentFormat = E_EmbeddedDocDocumentFormat.PDF;
                    embeddedDoc.DocumentName = edoc.Folder.FolderNm + " : " + edoc.DocType.DocTypeName;

                    byte[] docBytes = BinaryFileHelper.ReadAllBytes(edoc.GetPDFTempFile_Current());
                    embeddedDoc.Content = Convert.ToBase64String(docBytes);
                    docs.Add(embeddedDoc);
                }
                order.AppraisalOrder.EmbeddedDocList.AddRange(docs);
            }

            order.AppraisalOrder.OrderType = E_AppraisalOrderOrderType.UpdateOrder;
            order.AppraisalOrder.OrderNumber = orderNumber;
            order.AppraisalOrder.OrderInfo.TransactionID = Guid.NewGuid().ToString();

            var request = LQBAppraisalExporter.CreateAppraisalOrderRequest(cred, order.AppraisalOrder);
            var response = AppraisalVendorServer.Submit(request, vendor.PrimaryExportPath);

            SetResult("ErrorMessage", CreateErrorMessage(response.ServerResponse));
            if (response.ServerResponse.RequestStatus == E_ServerResponseRequestStatus.Success)
            {
                if (docIds != null)
                {
                    order.EmbeddedDocumentIds.AddRange(docIds);
                }
                order.OrderedDate = CDateTime.Create(DateTime.Now);
                order.Save();
                SetResult("Success", "True");
            }
            else
                SetResult("Success", "False");
        }

        private LqbOrderRequestDetails CreateLqbOrderRequestDetails()
        {
            LqbOrderRequestDetails details = new LqbOrderRequestDetails();
            details.ContactName = this.GetString("ContactName");
            details.ContactPhone = this.GetString("ContactPhone");
            details.ContactPhoneWork = this.GetString("ContactWork");
            details.ContactPhoneOther = this.GetString("ContactOther");
            details.ContactEmail = this.GetString("ContactEmail");
            details.ContactAditionalEmail = this.GetString("AdditionalEmail");

            details.Property = new Property();
            details.Property.Address = GetString("PropertyAddress");
            details.Property.City = GetString("PropertyCity");
            details.Property.State = GetString("PropertyState");
            details.Property.Zip = GetString("PropertyZip");
            Guid reoId = this.GetGuid("ReoId");
            details.LinkedReoId = reoId == Guid.Empty ? (Guid?)null : reoId;

            details.AppraisalNeededBy = GetString("AppraisalNeeded", string.Empty).ToNullable<DateTime>(DateTime.TryParse);

            details.IsRushOrder = GetBool("RushOrder");
            details.Notes = GetString("Notes");
            details.Products = new List<string>();
            for (int i = 1; i <= 5; i++)
            {
                string name = "ReportTypeID";
                if (i != 1)
                {
                    name = "ReportType" + i + "ID";
                }

                var product = GetString(name);
                if (string.IsNullOrEmpty(product))
                {
                    continue;
                }

                details.Products.Add(product);
            }

            details.BillingMethod = GetString("BillingMethodName");
            details.CreditCardIndicator = GetEnum<E_TriState>("CreditCardIndicator");

            details.Billing = new BillingDetails();
            details.Billing.Name = GetString("CCBillName", string.Empty);
            details.Billing.Street = GetString("CCAddress1", string.Empty);
            details.Billing.City = GetString("CCCity", string.Empty);
            details.Billing.State = GetString("CCState", string.Empty);
            details.Billing.Zip = GetString("CCZip", string.Empty);

            details.Billing.CreditCardType = GetString("CCType", string.Empty);
            details.Billing.CreditCardNumber = GetString("CCNumber", string.Empty);
            details.Billing.CreditCardExpirationMonth = GetInt("CCExpMonth", -1);
            details.Billing.CreditCardExpirationYear = GetInt("CCExpYear", -1);
            details.Billing.CreditCardSecurityCode = GetString("CCCode", string.Empty);

            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            if (ids != null)
            {
                details.DocumentIds = ids.Select(id => Guid.Parse(id)).ToList();
            }

            details.OrderNumber = GetString("OrderNumber");

            return details;
        }

        private GdmsOrderRequestDetails CreateGdmsOrderRequestDetails()
        {
            GdmsOrderRequestDetails details = new GdmsOrderRequestDetails();

            details.Property = new Property();
            details.Property.Address = GetString("PropertyAddress");
            details.Property.City = GetString("PropertyCity");
            details.Property.State = GetString("PropertyState");
            details.Property.Zip = GetString("PropertyZip");
            Guid reoId = this.GetGuid("ReoId");
            details.LinkedReoId = reoId == Guid.Empty ? (Guid?)null : reoId;

            details.CaseNumber = GetString("sAgencyCaseNum");
            details.ContactName = GetString("ContactName");
            details.ContactPhone = GetString("ContactPhone");
            details.ContactPhoneWork = GetString("ContactWork");
            details.ContactPhoneOther = GetString("ContactOther");

            details.PropertyTypeId = this.GetInt("PropertyTypeId");
            details.PropertyTypeName = this.GetString("PropertyTypeName");

            details.ReportTypeName = GetString("ReportTypeName");
            details.ReportType1Id = GetInt("ReportTypeId");
            details.ReportType2Id = GetInt("ReportType2Id");
            details.ReportType3Id = GetInt("ReportType3Id");
            details.ReportType4Id = GetInt("ReportType4Id");
            details.ReportType5Id = GetInt("ReportType5Id");

            details.AppraisalNeededBy = GetString("AppraisalNeeded", string.Empty).ToNullable<DateTime>(DateTime.TryParse);

            details.Client2Id = GetInt("Client2Id");
            details.ProcessorId = GetInt("ProcessorId");
            details.Processor2Id = GetInt("Processor2Id");
            details.BillingMethodId = GetInt("BillingMethodId");

            details.IntendedUseName = GetString("IntendedUseName");
            details.IntendedUseId = GetInt("IntendedUseId");

            details.LoanTypeName = GetString("LoanTypeName");
            details.LoanTypeId = GetInt("LoanTypeId");

            details.OccupancyTypeName = GetString("OccupancyTypeName");
            details.OccupancyTypeId = GetInt("OccupancyTypeId");

            details.LenderName = GetString("LenderName");
            details.Notes = GetString("Notes");

            details.BillingMethodDescription = GetString("BillingMethodName");

            details.Billing = new BillingDetails();
            details.Billing.Name = GetString("CCBillName", string.Empty);
            details.Billing.Street = GetString("CCAddress1", string.Empty);
            details.Billing.City = GetString("CCCity", string.Empty);
            details.Billing.State = GetString("CCState", string.Empty);
            details.Billing.Zip = GetString("CCZip", string.Empty);
            details.Billing.CreditCardType = GetString("CCType", string.Empty);
            details.Billing.CreditCardNumber = GetString("CCNumber", string.Empty);
            details.Billing.CreditCardExpirationMonth = GetInt("CCExpMonth", -1);
            details.Billing.CreditCardExpirationYear = GetInt("CCExpYear", -1);
            details.Billing.CreditCardSecurityCode = GetString("CCCode", string.Empty);

            details.OrderFileUploadTypeId = this.GetInt("AttachmentType");
            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            if (ids != null)
            {
                details.DocumentIds = ids.Select(id => Guid.Parse(id)).ToList();
            }

            return details;
        }

        private void SubmitOrder()
        {
            Guid LoanID = GetGuid("LoanID");
            
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(GetGuid("SelectedAMC"));
            AbstractOrderRequestDetails details;
            if (vendor.UsesGlobalDMS)
            {
                details = this.CreateGdmsOrderRequestDetails();
            }
            else 
            {
                //not GDMS
                details = this.CreateLqbOrderRequestDetails();
            }

            var result = AppraisalVendorFactory.SubmitOrder(CurrentPrincipal, LoanID, details, vendor);
            if (result.Status == LendersOffice.ObjLib.Webservices.ServiceResultStatus.Error)
            {
                var errorList = new List<string>();
                errorList.Add("There was an error processing your request.");
                errorList.AddRange(result.GetErrorList());
                this.SetResult("ErrorMessage", string.Join(Environment.NewLine, errorList));
                return;
            }

            SetResult("Success", "True");
            return;
        }

        private string CreateErrorMessage(GDMSErrorResponseException exc)
        {
            StringBuilder errorMsg = new StringBuilder();
            errorMsg.AppendLine("There was an error processing your request.");
            errorMsg.AppendFormat(" {0}.", exc.ErrorMessage);
            foreach (string error in exc.Errors)
            {
                errorMsg.AppendFormat(" {0}.", error);
            }

            return errorMsg.ToString();
        }

        private string CreateErrorMessage(ServerResponse exc)
        {
            if (exc.ServerMessageList.Count == 0)
                return "";
            StringBuilder errorMsg = new StringBuilder();

            if (exc.RequestStatus != E_ServerResponseRequestStatus.Success)
                errorMsg.AppendLine("There was an error processing your request.\n");

            var errors = exc.ServerMessageList;
            errors.Sort(ErrorMessageSortComparer.Instance);
            foreach (ServerMessage error in errors)
            {
                string errorType;
                switch (error.Type)
                {
                    case E_ServerMessageType.DataError:
                        errorType = "Data Error";
                        break;
                    case E_ServerMessageType.LoginError:
                        errorType = "Login Error";
                        break;
                    case E_ServerMessageType.ServerError:
                        errorType = "Server Error";
                        break;
                    case E_ServerMessageType.Other:
                    case E_ServerMessageType.Undefined:
                        errorType = "Warning";
                        break;
                    default:
                        //throw new UnhandledEnumException(error.Type);
                        errorType = "UnhandledException";
                        break;
                }
                errorMsg.AppendFormat("({0}) {1}\n", errorType, error.Description);
            }

            return errorMsg.ToString();
        }

        private class ErrorMessageSortComparer : IComparer<ServerMessage>
        {
            public static readonly ErrorMessageSortComparer Instance = new ErrorMessageSortComparer();
            private ErrorMessageSortComparer() { }
            public int Compare(ServerMessage x, ServerMessage y)
            {
                var sortOrder = new List<E_ServerMessageType> { E_ServerMessageType.ServerError, E_ServerMessageType.LoginError, E_ServerMessageType.DataError, E_ServerMessageType.Other, E_ServerMessageType.Undefined };
                int xIndex = sortOrder.IndexOf(x.Type);
                int yIndex = sortOrder.IndexOf(y.Type);
                if (xIndex == -1) xIndex = sortOrder.Count;
                if (yIndex == -1) yIndex = sortOrder.Count;
                return xIndex.CompareTo(yIndex);
            }
        }
    }

}

