﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileInfo.ascx.cs" Inherits="PriceMyLoan.webapp.FileInfo" %>

<script type="text/javascript">
    jQuery(function($) {
        $j('#explainLowestMiddleCreditScore').click(function() {
            return f_openHelp('Q00018.html', 300, 75);
        });
    });
</script>

<style type="text/css">
#FileInfoContainer
{
    border-spacing: 0px;
    border-collapse: collapse;
}

#FileInfoContainer td
{
    border: 1px solid black;
    padding: 1px;
}

#FileInfoContainer .Header
{
    text-align: left;
    font-weight: bold;
    background-color: #DDD;
    height: 1.25em;
}

#FileInfoContainer .Content
{
    background-color: White;
}
</style>

<%-- WARNING: the fields in here are a special case, since we only want to show the primary app. --%>
<%-- The borrower field IDs need to be prefixed with primary_ and some code needs to be added to fileinfo.js. --%>
<table id="FileInfoContainer" class="container" width="100%">
    <tr>
        <td class="Header">
            <div>
                Loan Number
            </div>
        </td>
        <td class="Header">
            <div>
                Loan Status
            </div>
        </td>
        
        <td class="Header">
            <div>
                Name
            </div>
        </td>
        
        <td class="Header" style="width:12em">
            <div>
                Phone Number
            </div>
        </td>
        
        <td class="Header">
            <div>
                Email Address
            </div>
        </td>
        
        <td class="last Header" style="width:17em">
            <div style="text-align:center">
                Lowest Middle Credit Score
            </div>
        </td>
    </tr>
    <tr>
        <td class="Content">
            <div>
                <span id="sLNm"></span>
            </div>
        </td>
        <td class="Content">
            <div>
                <span id="sStatusT"></span>
            </div>
        </td>
    
        <td class="Content">
            <div>
                <span id="primary_aBNm"></span>
            </div>
        </td>
    
        <td class="Content">
            <div style="text-align:center">
                <span id="primary_aBHPhone"></span>
            </div>
        </td>
        
        <td class="Content">
            <div>
                <span id="primary_aBEmail"></span>
            </div>
        </td>
        
        <td class="Content">
            <div style="text-align:center">
                <span id="sCreditScoreType2"></span> (<a href="#" id="explainLowestMiddleCreditScore">explain</a>)
            </div>
        </td>
    </tr>
</table>
