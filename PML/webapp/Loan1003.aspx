<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Loan1003.aspx.cs" Inherits="PriceMyLoan.webapp.Loan1003" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<%@ Register TagPrefix="tabs" TagName="Loan1003pg1" Src="~/webapp/Loan1003pg1.ascx" %>
<%@ Register TagPrefix="tabs" TagName="Loan1003pg2" Src="~/webapp/Loan1003pg2.ascx" %>
<%@ Register TagPrefix="tabs" TagName="Loan1003pg3" Src="~/webapp/Loan1003pg3.ascx" %>
<%@ Register TagPrefix="tabs" TagName="Loan1003pg4" Src="~/webapp/Loan1003pg4.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Application Information</title>
    <style>
        div#Loan1003AlertPopup {
            display: none;
        }
        .ui-dialog .ui-dialog-buttonpane div.center-align-popup-buttons {
            float: none;
            text-align: center;
        }
        .disabled-div {
            cursor: not-allowed;
            width:185px;
            height:30px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .disabled-tooltip-container #DeleteApplicationButton, .disabled-tooltip-container #ImportFannieButton {
            pointer-events: none;
        }
        #NewLoanNavigationButtonArea {
            text-align: right;
        }
        #ImportFannieButton {
            margin-right: 15px;
        }
        #AddApplicationButton {
            margin-left: 15px;
        }
        #importFannieDiv {
            display: inline-block;
        }
    </style>
</head>
<body id="MainBody" runat="server">
    <div id="Loan1003AlertPopup"><p id="Loan1003AlertPopupText">&nbsp;</p></div>
    <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />

    <div class="content-detail" name="content-detail">
        <div class="warp-section warp-section-tabs" loan-static-tab>
            <div class="static-tabs no-border">
                <div class="static-tabs-inner">
                    <header class="page-header table-flex table-flex-app-info">
                        <div>Application Information</div>
                        <div id="topMenu">
                            <div id="NewLoanNavigationButtonArea" runat="server" class="btnDiv">
                                <div id="importFannieDiv" runat="server">
                                    <button type="button" id="ImportFannieButton" runat="server" onclick="importFannie();" class="btn btn-default">Import FNM 3.2 File</button>
                                </div>
                                <button type="button" id="AddApplicationButton" onclick="addApp();" class="btn btn-default">Add New Application</button>
                            </div>
                        </div>
                    </header>
                    <div ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>
                </div>
                <div>
                    <div class="static-tabs-inner">
                        <div class="flex-tab-app-info">
                            <div class="tab-app-info-only">
                                <div class="padding-right-p8 d-flex-center text-grey">Applicant(s):</div>
                                <div class="padding-right-34 tab-dropdown-app-info">
                                    <application-list class="mdc-tab-bar">
                                        <application-header class="mdc-tab">
                                            <label id="appName" runat="server"></label>
                                            <i class="material-icons">&#xE5CF;</i>
                                        </application-header>
                                        <application-content class="collapse">
                                            <ul runat="server" id="appTabs"></ul>
                                        </application-content>
                                        <span class="mdc-tab-bar__indicator hidden"></span>
                                    </application-list>
                                </div>
                                <div class="d-flex-center text-grey">1003 Page:</div>
                                <div class="d-flex-center">
                                    <ul class="tabmenu">
                                        <li><a id="tab1Top" runat="server" href="#tabs-1" onclick="on1003TabClick(0);">Page 1</a></li>
                                        <li><a id="tab2Top" runat="server" href="#tabs-2" onclick="on1003TabClick(1);">Page 2</a></li>
                                        <li><a id="tab3Top" runat="server" href="#tabs-3" onclick="on1003TabClick(2);">Page 3</a></li>
                                        <li><a id="tab4Top" runat="server" href="#tabs-4" onclick="on1003TabClick(3);">Page 4</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                <div id="deleteApplicationDiv" runat="server">
                                    <button type="button" id="DeleteApplicationButton" onclick="deleteApp()" runat="server" class="pull-right btn btn-default">Delete Application</button>
                                </div>
                                <div>
                                    <button type="button" id="runPricingBtn1" runat="server">Run Pricing</button>
                                    <button type="button" id="editClosingCostsBtn1" runat="server" visible="false">Edit Closing Costs</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-tabs">
                <form name="" action="#" method="post" runat="server">
                    <div id="tabs-1" class="content-app-info"><tabs:Loan1003pg1 ID="Loan1003pg1" runat="server"/></div>
                    <div id="tabs-2" class="content-app-info"><tabs:Loan1003pg2 ID="Loan1003pg2" runat="server"/></div>
                    <div id="tabs-3" class="content-app-info hidden"><tabs:Loan1003pg3 ID="Loan1003pg3" runat="server"/></div>
                    <div id="tabs-4" class="content-app-info"><tabs:Loan1003pg4 ID="Loan1003pg4" runat="server"/></div>
                    <input type="hidden" name="_ClientID" id="_ClientID" value="" />
                    <div class="flex-space-between btn-bottom-app-info">
                        <div>
                            <button id="btnBack" type="button" runat="server" onclick="onBackClick()">Back</button>
                        </div>
                        <div>
                            <button id="btnNext" type="button" runat="server" onclick="onNextClick()">Next</button>
                        </div>
                    </div>
                </form>

                <div id="ErrorMessageDiv">
                    <span id="ErrorMessageSpan"></span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
