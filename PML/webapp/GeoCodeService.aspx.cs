﻿#region Generated code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using global::DataAccess;
    using LendersOffice.Common;

    public partial class GeoCodeService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ImportGeoCodes":
                    this.ImportGeoCodes();
                    break;
                default:
                    throw new ArgumentException($"Unknown method: {methodName}.");
            }
        }

        protected void ImportGeoCodes()
        {
            string sSpAddr = GetString("sSpAddr", "");
            string sSpCity = GetString("sSpCity", "");
            string sSpCounty = GetString("sSpCounty", "");
            string sSpState = GetString("sSpState", "");
            string sSpZip = GetString("sSpZip", "");
            string sHmdaActionD = GetString("sHmdaActionD");
            if (!string.IsNullOrEmpty(sSpAddr) && !string.IsNullOrEmpty(sSpCity) && !string.IsNullOrEmpty(sSpCounty) && !string.IsNullOrEmpty(sSpState) && !string.IsNullOrEmpty(sSpZip))
            {
                FFIECGeocode.Geocode geoCodeContainer = new FFIECGeocode.Geocode(sHmdaActionD, sSpAddr, sSpCity, sSpState, sSpZip);
                if (geoCodeContainer.IsError)
                {
                    this.ThrowError(geoCodeContainer.ErrorMessage, ErrorMessages.GeoCode.LibraryReturnedError_DEV);
                }
                else
                {
                    string msaNum = geoCodeContainer.MsaCode.Equals("99999") ? "NA" : geoCodeContainer.MsaCode;
                    this.SetResult("sHmdaMsaNum", msaNum);
                    this.SetResult("sHmdaCountyCode", geoCodeContainer.CountyCode);
                    this.SetResult("sHmdaStateCode", geoCodeContainer.StateCode);
                    this.SetResult("sHmdaCensusTract", geoCodeContainer.TractCode);
                }
            }
            else
            {
                this.ThrowError(ErrorMessages.GeoCode.EmptyAddress, ErrorMessages.GeoCode.EmptyAddress_DEV);
            }
        }

        private void ThrowError(string userMessage, string devMessage)
        {
            CBaseException exception = new CBaseException(userMessage, devMessage);
            exception.IsEmailDeveloper = false;
            throw exception;
        }
    }
}