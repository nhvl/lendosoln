﻿<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="PriceMyLoan.Common"%>
<%@ Import Namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoanFilePricer.ascx.cs" Inherits="PriceMyLoan.webapp.LoanFilePricer" %>

<script type="text/javascript">
var E_sProdConvMIOptionT_BorrPaidSplitPrem = <%= AspxTools.JsString(E_sProdConvMIOptionT.BorrPaidSplitPrem) %>;
var E_sProdConvMIOptionT_BorrPaidSinglePrem = <%= AspxTools.JsString(E_sProdConvMIOptionT.BorrPaidSinglePrem) %>;
var E_sProdConvMIOptionT_Blank = <%= AspxTools.JsString(E_sProdConvMIOptionT.Blank) %>;

var E_sProdSpT_array = <%= AspxTools.JsArray(E_sProdSpT.TwoUnits, 
                                   E_sProdSpT.ThreeUnits, 
                                   E_sProdSpT.FourUnits, 
                                   E_sProdSpT.Condo, 
                                   E_sProdSpT.PUD) %>;

var E_sProdSpT_Condo = <%= AspxTools.JsString(E_sProdSpT.Condo) %>;
var E_sProdSpT_PUD = <%= AspxTools.JsString(E_sProdSpT.PUD) %>;
var E_sProdSpT_Manufactured = <%= AspxTools.JsString(E_sProdSpT.Manufactured) %>;

var E_sProdSpStructureT_Attached = <%= AspxTools.JsString(E_sProdSpStructureT.Attached) %>;
var E_sProdSpStructureT_Detached = <%= AspxTools.JsString(E_sProdSpStructureT.Detached) %>;

var E_sProd3rdPartyUwResultT_NA = <%= AspxTools.JsString(E_sProd3rdPartyUwResultT.NA) %>;

var E_sLPurposeT_Purchase = <%= AspxTools.JsString(E_sLPurposeT.Purchase) %>;
var E_sLPurposeT_Refin = <%= AspxTools.JsString(E_sLPurposeT.Refin) %>;
var E_sLPurposeT_RefinCashout = <%= AspxTools.JsString(E_sLPurposeT.RefinCashout) %>;
var E_sLPurposeT_FhaStreamlinedRefinance = <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance) %>;
var E_sLPurposeT_VaIrrrl = <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl) %>;
var E_sLPurposeT_HomeEquity = <%= AspxTools.JsString(E_sLPurposeT.HomeEquity) %>;
var E_sLPurposeT_Construct = <%= AspxTools.JsString(E_sLPurposeT.Construct) %>;
var E_sLPurposeT_ConstructPerm = <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm) %>;

var E_aOccT_Investment = <%= AspxTools.JsString(E_aOccT.Investment) %>;
var E_aOccT_PrimaryResidence = <%= AspxTools.JsString(E_aOccT.PrimaryResidence) %>;

var JsMessages_NumFinancedPropertiesMustBePositive = <%= AspxTools.JsString(JsMessages.NumFinancedPropertiesMustBePositive) %>;

var E_sLtv80TestResultT_Over80 = <%= AspxTools.JsString(E_sLtv80TestResultT.Over80) %>;

var E_sLienPosT_Second = <%= AspxTools.JsString(E_sLienPosT.Second) %>;

var E_sProdConvMIOptionT_NoMI = <%= AspxTools.JsString(E_sProdConvMIOptionT.NoMI) %>;

var E_sProd3rdPartyUwResultT_DU = <%= AspxTools.JsArray( E_sProd3rdPartyUwResultT.DU_ApproveEligible,
                                                E_sProd3rdPartyUwResultT.DU_ApproveIneligible,
                                                E_sProd3rdPartyUwResultT.DU_ReferEligible,
                                                E_sProd3rdPartyUwResultT.DU_ReferIneligible,
                                                E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible,
                                                E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible,
                                                E_sProd3rdPartyUwResultT.DU_EAIEligible,
                                                E_sProd3rdPartyUwResultT.DU_EAIIEligible,
                                                E_sProd3rdPartyUwResultT.DU_EAIIIEligible) %>;
                                                
var E_sSubFinT_Heloc = <%= AspxTools.JsString(E_sSubFinT.Heloc) %>;
var E_sSubFinT_CloseEnd = <%= AspxTools.JsString(E_sSubFinT.CloseEnd) %>;

var E_sOriginatorCompensationPaymentSourceT_BorrowerPaid = <%= AspxTools.JsString(E_sOriginatorCompensationPaymentSourceT.BorrowerPaid) %>;

var E_sPriorSalesPropertySellerT_Blank = <%= AspxTools.JsString(E_sPriorSalesPropertySellerT.Blank) %>;

var E_sProdDocT_Full = <%= AspxTools.JsString(E_sProdDocT.Full) %>;

var ConstructionPurpose_ConstructionAndLotPurchase = <%= AspxTools.JsString(DataAccess.Core.Construction.ConstructionPurpose.ConstructionAndLotPurchase) %>;

var E_sConvSplitMIRT_Blank = <%= AspxTools.JsString(E_sConvSplitMIRT.Blank) %>;

var E_sTitleInsuranceCostT_Manual = <%= AspxTools.JsString(E_sTitleInsuranceCostT.Manual) %>;
var E_sTitleInsuranceCostT_ManualTitleRecTrans = <%= AspxTools.JsString(E_sTitleInsuranceCostT.ManualTitleRecTrans) %>;

var ConstApp_MAX_NUMBER_CUSTOM_PML_FIELDS = <%= AspxTools.JsString(ConstApp.MAX_NUMBER_CUSTOM_PML_FIELDS) %>;
</script>

<!-- for calculate link -->
<input type="hidden" id="sProHoAssocDuesPe" data-calc />
<input type="hidden" id="sProHazInsPe" data-calc />
<input type="hidden" id="sProMInsPe" data-calc />
<input type="hidden" id="sProOHExpDescPe" data-calc />
<input type="hidden" id="sProRealETxPe" data-calc />
<input type="hidden" id="sProOHExpPe" data-calc />

<!-- for background calculation -->
<input type="hidden" id="sProdCalcEntryT" />

<!-- setting sProdConvMI -->
<input type="hidden" id="sLtv80TestResultT" data-calc />

<!-- smart text -->
<input type="hidden" id="sProdRLckdExpiredDLabel" data-calc />

<div class="LoanFilePricer">
<fieldset id="PropertyInformation">
    <legend>Property Information</legend>
    <div id="StreetAddressPanel">
        <label for="sSpAddr" class="singleLine">Street Address</label>
        <input type="text" id="sSpAddr" class="wide" data-calc/>
        <img id="sSpAddrValidator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    
    <div>
        <label for="sSpZip" class="singleLine">Zip Code</label>
        <input type="text" id="sSpZip" preset="zipcode" maxlength="5" data-calc/>
        <img id="sSpZipValidator" src="../images/error_pointer.gif" alt="This is a required field. Must be five digits"/>
        
        <div id="StatePanel" class="inline">
            <label id="sSpStatePeLabel" for="sSpStatePe" class="singleLine normal">State</label>
            <select id="sSpStatePe" data-calc data-refresh></select>
            <img id="sSpStatePeValidator" class="state" src="../images/error_pointer.gif" alt="This is a required field."/>
        </div>
    </div>
    
    <div>
        <label for="sSpCounty" class="singleLine">County</label>
        <select id="sSpCounty" class="wide" name="sSpCounty" data-calc>
        </select>
        <img id="sSpCountyValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>
    
    <div>
        <label for="sSpCity" class="singleLine">City</label>
        <input type="text" id="sSpCity"  class="wide"  data-calc />
        <img id="sSpCityValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>

    <div>
        <label for="sProdIsSpInRuralArea" class="singleLine">In Rural Area?</label>
        <input type="checkbox" id="sProdIsSpInRuralArea"  data-calc/><label class="normal">Yes</label>
        <a id="explainInRuralArea" href="#">explain</a>
    </div>
    
    <div>
        <label for="sOccTPe" class="singleLine">Property Use</label>
        <select id="sOccTPe" class="mediumSelect" data-calc></select>
    </div>
    
    <div id="investmentPanel">
        <label for="sSpGrossRentPe" class="singleLine">Gross Rent</label>
        <input type="text" id="sSpGrossRentPe" preset="money-nocss" data-calc/>
        <div id="OccupancyRatePanel" class="inline">
            <label id="sOccRPeLabel" for="sOccRPe">Occupancy Rate (%)</label>
            <input type="text" id="sOccRPe" preset="percent-nocss"  data-calc/>
        </div>
    </div>
    
    <div id="HasNonOccupantCoborrower">
        <label for="sHasNonOccupantCoborrowerPe">Has Non-Occupant Co-Borrower?</label>
        <input type="checkbox" id="sHasNonOccupantCoborrowerPe" data-calc/><label class="normal">Yes</label>
    </div>
    
    <div>
        <label for="sProdSpT" class="singleLine">Property Type</label>
        <select id="sProdSpT" class="mediumSelect" data-calc data-refresh></select>
    </div>
    <div class="manufactured-datapoint">
        <label for="sIsNotPermanentlyAffixed" class="singleLine">Not Permanently Affixed?</label>
        <input type="checkbox" id="sIsNotPermanentlyAffixed" data-calc data-refresh /><label class="normal">Yes</label>
    </div>
    <div class="manufactured-datapoint">
        <label for="sHomeIsMhAdvantageTri" class="singleLine">Is home MH Advantage?</label>
        <select id="sHomeIsMhAdvantageTri" data-calc data-refresh></select>
    </div>
    <div>
        <label for="sProdSpStructureT" class="singleLine">Structure Type</label>
        <select id="sProdSpStructureT" class="mediumSelect" data-calc></select>
    </div>

    <div>
        <label for="sIsNewConstruction">New Construction?</label>
        <input type="checkbox" id="sIsNewConstruction" data-calc/><label class="normal">Yes</label>
    </div>
    
    <div id="NonWarrantableProjCondotelRow">
        <span id="NonWarrantableProj">
            <label for="sProdIsNonwarrantableProj">Non-Warrantable Project?</label>
            <input type="checkbox" id="sProdIsNonwarrantableProj" data-calc/><label class="normal">Yes</label>
        </span>
        <div id="CondotelPanel" class="inline">
            <label id="sProdIsCondotelLabel" for="sProdIsCondotel" class="singleLine">Condotel?</label>
            <input type="checkbox" id="sProdIsCondotel" data-calc/><label class="normal">Yes</label>
        </div>        
    </div>
    
    <div id="CondoPanel">
        <label for="sFhaCondoApprovalStatusT">FHA Condo Approval Status</label>
        <select id="sFhaCondoApprovalStatusT" data-calc></select>
        <div id="CondoStoriesPanel" class="inline">
            <label for="sProdCondoStories" id="sProdCondoStoriesLabel" class="singleLine normal">Condo Stories</label>
            <input type="text" id="sProdCondoStories" maxlength="3" data-calc />
            <img id="sProdCondoStoriesValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        </div>
        
    </div>
    
    <div>
        <label for="sMonthlyPmtPe">Additional Monthly Housing Expenses</label>
        <input type="text" id="sMonthlyPmtPe" readonly="readonly" preset="money" data-calc />
        <a id="calculateAdditionalMonHousingExpenses" href="#">calculate</a>
    </div>
    
    <div id="TitleInsuranceCostPanel">
        <div>
            <label >Owner's Title Insurance</label>
            <input type="radio" id="ObtainCostFromTitleQuote" name="sTitleInsuranceCostT" value=<%= AspxTools.JsString(E_sTitleInsuranceCostT.UseIntegration) %> data-calc />
            <label class="normal">Use estimated title cost.</label>
        </div>
        <div>
            <label></label>
            <input type="radio" id="UseCostQuotedByBorrowerRealtor" name="sTitleInsuranceCostT" value=<%= AspxTools.JsString(E_sTitleInsuranceCostT.Manual) %> data-calc />
            <label class="verticalRadioListLabel normal">Use cost quoted by borrower/realtor.</label>
        </div>
        <div id="ManualTitlePanel">
            <label></label>
            <input type="text" id="sOwnerTitleInsFPe" preset="money-allowblank" data-calc />
            <img id="sOwnerTitleInsFPeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        </div>
    </div>

</fieldset>
<fieldset id="LoanInformation">
    <legend>Loan Information</legend>
    <div id="RenovationLoanPanel">
        <label for="sIsRenovationLoan">Is Renovation Loan?</label>
        <input type="checkbox" id="sIsRenovationLoan"  data-calc data-refresh /><label class="normal">Yes</label>
        <div id="TotalRenovationCosts">
            <label id="sTotalRenovationCostsLabel" for="sTotalRenovationCosts">Total Renovation Costs</label>
            <input type="text" id="sTotalRenovationCosts" preset="money" data-calc data-refresh />
            <img id="sTotalRenovationCostsValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
            <input type="checkbox" id="sTotalRenovationCostsLckd" data-calc data-refresh />
            <div id="determineTotalRenovationCosts" class="inline">
                <a id="determineTotalRenovationCostsLink" onclick="f_determineTotalRenovationCosts();">determine</a>
            </div>
        </div>     
    </div>
    
<asp:PlaceHolder runat="server" ID="IsPurchaseLoanToggleHolder" Visible="false">
    <div id="IsPurchaseLoanPanel">
        <label class="singleLine">Loan Purpose</label>
        <input type="radio" id="bYesIsPurchaseLoan" name="IsPurchaseLoan" value="True"  class="IsPurchaseLoanRadio"/>
        <label id="bYesIsPurchaseLoanLabel" class="normal">Purchase</label>
        <input type="radio" id="bNoIsPurchaseLoan"  name="IsPurchaseLoan" value="False" class="IsPurchaseLoanRadio"/>
        <label id="bNoIsPurchaseLoanLabel" class="normal">Refinance</label>
    </div>
</asp:PlaceHolder>   
    
    <div id="FTHBContainer" runat="server">
        <label class="singleLine">First Time Home Buyer?</label>
        <input type="checkbox" id="aBTotalScoreIsFthbQP" data-calc data-refresh/>
        <a href="#" id="FthbExplain">explain</a>
        
        <div id="HasHousingHistContainer" style="display:inline-block;">
            <label class="singleLine">Has Housing History?</label>
            <input type="checkbox" id="aBHasHousingHistQP" data-calc data-refresh/>
        </div>
    </div> 

    <div id="RefinanceTypePanel">
        <label for="sLPurposeTPe">Refinance Type</label>
        <select id="sLPurposeTPe" class="wide" data-calc data-refresh></select>
    </div>

    <div id="sIsStudentLoanCashoutRefiPanel">
        <label for="sIsStudentLoanCashoutRefi" class="singleLine">Student Loan Cashout?</label>
        <input type="checkbox" id="sIsStudentLoanCashoutRefi" data-calc/>
    </div>
    
    <div class="Texas50a6Panel">
        <label for="sProdIsTexas50a6Loan">New Loan is Texas 50(a)(6)?</label>    
        <input type="checkbox" id="sProdIsTexas50a6Loan" data-calc />
        <label class="normal">Yes</label>
        <a id="explainTexas50a6" href="#">explain</a>
    </div>

    <div class="Texas50a6Panel">
        <label for="sPreviousLoanIsTexas50a6Loan">Prior Loan is Texas 50(a)(6)?</label>    
        <input type="checkbox" id="sPreviousLoanIsTexas50a6Loan" data-calc />
        <label class="normal">Yes</label>
    </div>
    
    <div id="EndorsedPanel">
        <label for="sProdIsLoanEndorsedBeforeJune09">Was existing loan endorsed on or before May 31, 2009?</label>
        <input type="checkbox" id="sProdIsLoanEndorsedBeforeJune09" data-calc data-refresh/><label class="normal">Yes</label>
    </div>
    <div id="CashoutAmtPanel">
        <label for="sProdCashoutAmt" class="singleLine">Cashout Amount</label>
        <input type="text" id="sProdCashoutAmt" preset="money" data-calc/>
        <img id="sProdCashoutAmtValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>

    
    <div id="CurrPIPmtMIPMoPanel">
        <div>
            <label for="sProdCurrPIPmt">Current Loan P&amp;I Payment</label>
            <input type="text" id="sProdCurrPIPmt" preset="money" data-calc/>
            <img id="sProdCurrPIPmtValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
            <input type="checkbox" id="sProdCurrPIPmtLckd" data-calc data-refresh />
            <label for="sProdCurrPIPmtLckd" class="normal">Modify</label>
        </div>
        <div id="CurrentMIPMon">
            <label id="sProdCurrMIPMoLabel" for="sProdCurrMIPMo" class="singleLine">Current MIP/Month</label>
            <input type="text" id="sProdCurrMIPMo" preset="money" data-calc/>
            <img id="sProdCurrMIPMoValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        </div>
    </div>
    
    <div id="FHAStreamlineVAIRRRLPanel">
        <div>
            <label for="sIsCreditQualifying">Credit Qualifying?</label>
            <input type="checkbox" id="sIsCreditQualifying" data-calc/><label class="normal">Yes</label>
            <a id="explainCreditQualifying" href="#">explain</a>
            <div id="AppraisalPanel" class="inline">
                <label for="sHasAppraisal" class="normal singleLine">Appraisal?</label>
                <input type="checkbox" id="sHasAppraisal" data-calc/><label class="normal">Yes</label>
            </div>
            
        </div>

        <div>
            <label for="sSpLien">Outstanding Principal Balance</label>
            <input type="text" id="sSpLien" preset="money" data-calc/>
            <img id="sSpLienValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
            <a id="explainOutstandingPrincipalBalance" href="#">explain</a>
        </div>
    </div>
    
    <div id="UpfrontMIPRefundPanel">
        <label for="sFHASalesConcessions" class="singleLine">Upfront MIP refund</label>
        <input type="text" id="sFHASalesConcessions" preset="money" data-calc/>
        <img id="sFHASalesConcessionsValidator" src="../images/error_pointer.gif" alt="Must be positive."/>
    </div>

    <div id="SellerCreditPanel">
        <label for="sSellerCreditT">Seller Credit</label>
        <select id="sSellerCreditT" data-calc></select>
    </div>
    
    <div id="StandAloneSecondPanel">
        <div>
            <label for="sLpIsNegAmortOtherLien">1st Lien has Negative Amort.</label>
            <input type="checkbox" id="sLpIsNegAmortOtherLien" data-calc /><label class="normal">Yes</label>
        </div>
        <div>
            <label for="sOtherLFinMethT">1st Lien Amort. Type</label>
            <select id="sOtherLFinMethT" data-calc></select>
        </div>
        <div>
            <label for="sProOFinPmtPe">1st Lien Payment</label>
            <input type="text" id="sProOFinPmtPe" preset="money" data-calc />
        </div>
    </div>

    <div>
        <label for="sProdImpound" class="singleLine">Impound?</label>
        <input type="checkbox" id="sProdImpound" checked="checked"  data-calc/><label class="normal">Yes</label>
    </div>
    
    <div>
        <label for="sProdDocT" class="singleLine">Doc Type</label>
        <select id="sProdDocT" class="wide" data-calc></select>
    </div>
    
    <div id="OriginalAppraisedValuePanel">
        <label for="sOriginalAppraisedValue">Original Appraised Value</label>
        <input type="text" id="sOriginalAppraisedValue" preset="money" data-calc data-refresh />
        <img id="sOriginalAppraisedValueValidator" src="../images/error_pointer.gif" alt="Must be greater than or equal to 1."/>
    </div>
    
    <div id="ApprValPePanel">
        <label id="sApprValPeLabel" for="sApprValPe" class="singleLine">As-Completed Value</label>
        <input type="text" id="sApprValPe" preset="money" data-calc data-refresh />
        <img id="sApprValPeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>
    
    <div id="sHouseValPePanel">
        <label id="sHouseValPeLabel" for="sHouseValPe" class="singleLine">Sales Price</label>
        <input type="text" id="sHouseValPe" preset="money" data-calc data-refresh />
        <img id="sHouseValPeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>
    
    <div id="InducementPurchPrice">
        <label for="sInducementPurchPrice">Inducements to Sales Price</label>
        <input type="text" id="sInducementPurchPrice" preset="money" data-calc data-refresh />
    </div>        
    <div id="PurchPriceLessInducement">
        <label for="sPurchPriceLessInducement">Sales Price Less Inducements</label>
        <input type="text" id="sPurchPriceLessInducement" preset="money" readonly="readonly" data-calc />
    </div>   

    <div id="sAsIsAppraisedValuePevalPanel">
        <label for="sAsIsAppraisedValuePeval">As-Is Value</label>
        <input type="text" id="sAsIsAppraisedValuePeval" preset="money" data-calc data-refresh />
        <img id="sAsIsAppraisedValuePevalValidator" src="../images/error_pointer.gif" alt="This is a non-negative field."/>
        <a id="sApprValExplainLabel" onclick="return f_openHelp('Q00017.html', 300, 200);" href="#" >explain</a>
    </div>
    
    
    <div>
        <label id="EquityDownPmtLabel" for="sDownPmtPcPe" class="singleLine">Down Payment</label>
        <input type="text" id="sDownPmtPcPe" preset="percent" data-calc data-refresh />&nbsp;
        <input type="text" id="sEquityPe" preset="money" data-calc data-refresh />
    </div>
    
    <div id="1stLienPanel">
        <label for="sLtvRPe" class="singleLine">1st Lien</label>
        <span id="FirstLtvBal">
            <input type="text" id="sLtvRPe" preset="percent" data-calc data-refresh />&nbsp;
            <input type="text" id="sLAmtCalcPe" preset="money" data-calc data-refresh />
            <img id="sLAmtCalcPeValidator" src="../images/error_pointer.gif" alt="Must be greater than or equal to 1."/>
        </span>
    </div>
    <div id="IsLineOfCreditLineAmountPanel">
        <label for="sCreditLineAmt">Line Amount</label>
        <input type="text" id="sCreditLineAmt" preset="money" data-calc data-refresh />
    </div>
    <div>
        <label class="singleLine">2nd Financing?</label>
        <input type="radio" id="bNo2ndFinancing" name="sHasSecondFinancingT" checked="checked"  value="False" class="SecondFinancingRadio" data-calc/>
        <label id="bNo2ndFinancingLabel" class="normal">No</label>
        <input type="radio" id="bYes2ndFinancing" name="sHasSecondFinancingT" value="True" class="SecondFinancingRadio" data-calc/>
        <label id="bYes2ndFinancingLabel" class="normal">Yes</label>
    </div>
    
    <div id="SecondFinancingTypePanel">
        <label class="singleLine">2nd Financing Type</label>
        <input type="radio" id="bCloseEnd2ndFinancing" name="sSubFinT" checked="checked" value=<%= AspxTools.JsString(E_sSubFinT.CloseEnd) %> class="normal nomargin SecondFinancingTypeRadio"  data-calc />
        <label id="bCloseEnd2ndFinancingLabel" class="normal">Closed-end</label>
        <input type="radio" id="bHeloc2ndFinancing" name="sSubFinT" value=<%= AspxTools.JsString(E_sSubFinT.Heloc) %> class="normal nomargin SecondFinancingTypeRadio"  data-calc/>
        <label id="bHeloc2ndFinancingLabel" class="normal">HELOC</label>
    </div>
    
    <div id="SecondFinancingFieldsPanel">
        <span id="SecondFinancingIsNew">
            <label>2nd Financing is New?</label>
            <input type="checkbox" id="sIsOFinNewPe" data-calc />
            <a id="explainLinkedSecondLien" href="#" style="display: none;">explain</a>
            <br />
        </span>
        <label id="SecondFinancingAmountLabel" for ="sLtvROtherFinPe" class="singleLine">Initial Draw Amount</label>
        <span id="SecondLtvBal">
            <input type="text" id="sLtvROtherFinPe" preset="percent" data-calc data-refresh />&nbsp;&nbsp;
            <input type="text" id="sProOFinBalPe" preset="money" data-calc data-refresh />
            <img id="sProOFinBalPeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        </span>
        <br />
        <label for="sCltvRPe" class="singleLine">CLTV</label>
        <input type="text" id="sCltvRPe" preset="percent" data-calc data-refresh />
    </div>
    
    <div id="SecondFinancingHelocFieldsPanel">
        <label for="sSubFinToPropertyValue" class="singleLine">Line Amount</label>
        <input type="text" id="sSubFinToPropertyValue" preset="percent" data-calc data-refresh />&nbsp;&nbsp;
        <input type="text" id="sSubFinPe" preset="money" data-calc data-refresh />
        <img id="sSubFinPeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        <br />
        <label for="sHCLTVRPe" class="singleLine">HCLTV</label>
        <input type="text" id="sHCLTVRPe" preset="percent" data-calc data-refresh />
    </div>

    <div id="RequestCommunityOrAffordableSecondsPanel">
        <label>Community / Affordable Seconds?</label>
        <input type="checkbox" id="sRequestCommunityOrAffordableSeconds" data-calc data-refresh/>
        <br />        
    </div>

    <div>
        <label class="singleLine">Rate Lock Period</label>
        <select id="sProdRLckdDaysDDL"></select>
        <input type="text" id="sProdRLckdDays" data-calc data-refresh/><label class="normal">days</label>
        <img id="sProdRLckdDaysValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>
    
    <div class="rateLockSmartText">
        Rate Lock Expiration Date:<br />
        <span id="RateLockSmartText"></span>
    </div>
</fieldset>    
<fieldset id="OtherInformation">
    <legend>Other Information</legend>
    <div id="PriceGroupPanel">
        <label>Price Group</label>
        <input type="text" id="sProdLpePriceGroupNm"/>
    </div>
<asp:PlaceHolder runat="server" ID="sLeadSrcDescHolder" Visible="false">
    <div>
        <label class="singleLine">Lead Source</label>
        <select id="sLeadSrcDesc" data-calc></select>
    </div>
</asp:PlaceHolder>

    <div id="sCorrespondentProcessTContainer" data-field-id="sCorrespondingProcessTContainer" runat="server">
        <label>Underwriting Type</label>
        <input type="radio" id="sCorrespondentProcessT_Delegated" data-calc data-field-id="sCorrespondingProcessT_Delegated"  value=<%= AspxTools.JsString(E_sCorrespondentProcessT.Delegated) %> name="sCorrespondentProcessT"/>
        <label for="sCorrespondentProcessT_Delegated" class="normal">Delegated</label>
        <input type="radio" id="sCorrespondentProcessT_PriorApproved" data-calc data-field-id="sCorrespondingProcessT_PriorApproved" value=<%= AspxTools.JsString(E_sCorrespondentProcessT.PriorApproved) %>  name="sCorrespondentProcessT"/>  
        <label for="sCorrespondentProcessT_PriorApproved" class="normal">Prior Approved</label>    
        <img id="sCorrespondentProcessTValidator" src="../images/error_pointer.gif" alt="This is a required field"/>
    </div>
    
    <div id="sGfeIsTPOTransactionContainer"  runat="server"  >
        <label>Is TPO Loan?</label>
        <input type="checkbox" ID="sGfeIsTPOTransaction" data-calc="" />
        <label class="normal">Yes</label>
    </div>

    <div id="OriginatorCompensationPanel" class="OriginatorCompensationPanel" runat="server">
        <label>Loan Originator is Paid By</label>
        <input type="radio" id="sOriginatorCompensationPaymentSourceT_Lender" name="sOriginatorCompensationPaymentSourceT" value=<%= AspxTools.JsString(E_sOriginatorCompensationPaymentSourceT.LenderPaid) %> data-calc/>
        <label for="sOriginatorCompensationPaymentSourceT_Lender" class="normal">Lender</label>
        <input type="radio" id="sOriginatorCompensationPaymentSourceT_Borrower" name="sOriginatorCompensationPaymentSourceT" value=<%= AspxTools.JsString(E_sOriginatorCompensationPaymentSourceT.BorrowerPaid) %> data-calc/>
        <label for="sOriginatorCompensationPaymentSourceT_Borrower" class="normal">Borrower</label>
        <br />
        <div id="LoanOriginatorSection">
            <input type="text" id="sOriginatorCompensationBorrPaidPc" preset="percent" data-calc data-refresh />&nbsp;of&nbsp;
            <select id="sOriginatorCompensationBorrPaidBaseT" data-calc data-refresh ></select>&nbsp;+&nbsp;
            <input type="text" id="sOriginatorCompensationBorrPaidMb" preset="money-nocss" data-calc data-refresh />
            <img class="sOriginatorCompensationBorrTotalAmountValidator" src="../images/error_pointer.gif" alt="Borrower-paid compensation may not exceed lender-paid compensation."/>
            <span id="BorrowerPaidCompExceedsLenderPaidCompMessage" class="sOriginatorCompensationBorrTotalAmountValidator"></span>
        </div>        
    </div>

    <div runat="server" id="EnableLenderFeeBuyout" visible="false">
        <label for="sLenderFeeBuyoutRequestedT">Lender Fee Buyout Requested?</label>
        <select id="sLenderFeeBuyoutRequestedT" data-calc data-refresh name="sLenderFeeBuyoutRequestedT"></select>
    </div>
    
    <div>
        <label for="sProd3rdPartyUwResultT">Expected AUS Response</label>
        <select id="sProd3rdPartyUwResultT" data-calc></select>
    </div>
    
    <div id="DURefiPlusPanel">
        <label class="singleLine" for="sProdIsDuRefiPlus">Is DU Refi Plus?</label>
        <input type="checkbox" id="sProdIsDuRefiPlus" data-calc data-refresh /><label class="normal">Yes</label>
    </div>
    
    <div id="VisiblePriorToOrderingCredit1">
        <label for="sCreditScoreEstimatePe">Estimated Credit Score</label>
        <input type="text" id="sCreditScoreEstimatePe" data-calc />
        <img id="sCreditScoreEstimatePeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        <div id="TotalMonthlyIncomePanel">
            <label id="sPrimAppTotNonspIPeLabel" for="sPrimAppTotNonspIPe">Total Monthly Income</label>
            <input type="text" id="sPrimAppTotNonspIPe" preset="money-allowblank-nocss" data-calc />
            <img id="sPrimAppTotNonspIPeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        </div>
    </div>
    
    <div>
        <label for="sNumFinancedProperties">Number of Financed Properties</label>
        <input type="text" id="sNumFinancedProperties" data-calc/>
        <img id="sNumFinancedPropertiesValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        &nbsp;<a id="explainNumFinancedProperties" href="#">explain</a>
    </div>
    
    <div id="VisiblePriorToOrderingCredit2">
        <label for="sAppTotLiqAsset" class="singleLine">Total Liquid Assets</label>
        <input type="text" id="sAppTotLiqAsset" preset="money-allowblank" data-calc />
        <img id="sAppTotLiqAssetValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
        &nbsp;<a id="explainTotalLiquidAssets" href="#">explain</a>
    </div>
    
    <div id="PresentHousingExpensePanel">
        <label for="sPresOHExpPe">Present Housing Expense</label>
        <input type="text" id="sPresOHExpPe" preset="money" data-calc />
        <img id="sPresOHExpPeValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>
    
    <div id="MasterPriorSalesPanel">
        <div>
            <label for="sPriorSalesD" class="singleLine">Prior Sales Date</label>
            <input type="text" id="sPriorSalesD" style="width: 90px;" data-calc />
            <img id="sPriorSalesDValidator" src="../images/error_pointer.gif" alt="This is a required field."/>&nbsp;
            <a id="explainPriorSalesDate" href="#">explain</a>
        </div>
        <div id="PriorSalesExtraInfoPanel">
            <div>
                <label for="sPriorSalesPrice" class="singleLine">Prior Sales Price</label>
                <input type="text" id="sPriorSalesPrice" preset="money" data-calc />
                <img id="sPriorSalesPriceValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
            </div>
            
            <div>
                <label for="sPriorSalesSellerT" class="singleLine">Property Seller</label>
                <select id="sPriorSalesSellerT" data-calc></select>
                <img id="sPriorSalesSellerTValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
            </div>
        </div>
    </div>

    <div class="MiNeedSection">
        <label for="sConvLoanLtvForMiNeed" >MI Need LTV</label>
        <input preset="percent" ID="sConvLoanLtvForMiNeed" readonly />
        <a href="javascript:void(0)" class="mi-need-explanation">?</a>
    </div>
    
    <div id="ConvLoanPMIPanel">
        <label for="sProdConvMIOptionT">Conv Loan PMI Type</label>
        <select id="sProdConvMIOptionT" data-calc></select>
        <img id="sProdConvMIOptionTValidator" src="../images/error_pointer.gif" alt="This value cannot be blank"/>
    </div>

    <div id="SplitMIUpfrontPremiumPanel">
        <label for="sConvSplitMIRT">Split MI Upfront Premium</label>
        <select id="sConvSplitMIRT" data-calc></select>
        <img id="sConvSplitMIRTValidator" src="../images/error_pointer.gif" alt="This is a required field."/>
    </div>
    
    <div>
        <label for="sProdIsUFMIPFFFinanced">Is UFMIP/FF Financed?</label>
        <input type="checkbox" id="sProdIsUFMIPFFFinanced" checked="checked"  data-calc data-refresh/>
        <label class="normal">Yes</label>
        
        <div style="display: inline;">
            <label for="sProdOverrideUFMIPFF" >Override Auto-Calculated UFMIP/FF?</label>
            <input type="checkbox" id="sProdOverrideUFMIPFF"  data-calc data-refresh/>
            <label class="normal">Yes</label>
        </div>
        <div style="clear: both; height: 0px;"></div>
    </div>
    
    <div id="RequireOverridenUFMIPFFToEdit">
        <div class="floatLeftField">
            <label id="sProdFhaUfmipLabel" for="sProdFhaUfmip" class="singleLine normal">FHA UFMIP</label>
            <input type="text" id="sProdFhaUfmip" preset="percent" data-calc />
        </div>
        
        <div class="floatRightField">
            <label for="sProdVaFundingFee" class="singleLine normal">VA Funding Fee</label>
            <input type="text" id="sProdVaFundingFee" preset="percent" data-calc />
        </div>
        <div style="clear: both; height: 0px;"></div>
        <div class="floatRightField">
            <label for="sProdUSDAGuaranteeFee">USDA Rural Guarantee Fee</label>
            <input type="text" id="sProdUSDAGuaranteeFee" preset="percent" data-calc />
        </div>
    </div>

    <div id="CustomPMLFieldsContainer">
    </div>
</fieldset>    
<div id="ButtonPanel">
    <input id="PriceBtn" type="button" value="Price" />
    <input id="ProcOrderCrBtn" type="button" value="Proceed to Order Credit" />
</div>

</div>