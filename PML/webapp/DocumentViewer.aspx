﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentViewer.aspx.cs" Inherits="PriceMyLoan.webapp.DocumentViewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Viewer</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        This browser does not support displaying PDFs. Click the button below to download the PDF manually.
        <br />
        <asp:Button runat="server" ID="DownloadPdfButton" Text="Download PDF" OnClick="DownloadPdfButton_Click" />
    </div>
    </form>
</body>
</html>
