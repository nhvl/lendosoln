﻿<%@ Page Language="c#" CodeBehind="Loan1003AddLiability.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.webapp.Loan1003AddLiability" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head id="Head1" runat="server">
    <title>Add Liability</title>
</head>
<body>
<form id="AddLiability" method="post" runat="server">
    <div lqb-popup class="modal-app-info modal-add-liability">
        <div class="modal-header">
            <h4 class="modal-title">Add Liability</h4>
        </div>
        <div class="modal-body">
            <div class="table table-modal-padding-bottom">
                <div><div class="text-grey text-right nowrap"><label>Ownership</label></div><div>
                    <asp:DropDownList ID="OwnerT" Width="180px" runat="server">
                        <asp:ListItem Value="0" id="owner1" Selected="True">Borrower</asp:ListItem>
                        <asp:ListItem Value="1" id="owner2">Coborrower</asp:ListItem>
                        <asp:ListItem Value="2">Joint</asp:ListItem>
                    </asp:DropDownList>
                </div></div>
                <div><div class="text-grey text-right nowrap"><label>Debt type</label></div><div><asp:DropDownList ID="DebtT" onchange="f_ddlOnChange();" class="select-w100" runat="server"/></div></div>                
                <div><div class="text-grey text-right nowrap"><label>Company Name</label></div><div><asp:TextBox ID="ComNm" CssClass="company-address" runat="server"/></div></div>
                <div>
                    <div class="text-grey text-right nowrap"><label>Company Address</label></div>
                    <div><asp:TextBox ID="ComAddr" runat="server" class="company-address" /></div>
                </div>
                <div>
                    <div></div>
                    <div>
                        <div class="table table-dialogs">
                            <div>
                                <div><asp:TextBox ID="ComCity" class="input-w140" runat="server"/></div>
                                <div><ml:StateDropDownList ID="ComState" runat="server"/></div>
                                <div><ml:ZipcodeTextBox ID="ComZip" runat="server" preset="zipcode" Width="40px"/></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div><div class="text-grey text-right nowrap"><label>Account Holder Name</label></div><div><asp:TextBox ID="AccNm" Width="180px" runat="server"/></div></div>
                <div><div class="text-grey text-right nowrap"><label>Account Number</label></div><div><asp:TextBox ID="AccNum" Width="180px" runat="server"/></div></div>
                <div><div class="text-grey text-right nowrap"><label>Balance</label></div><div><ml:MoneyTextBox ID="Bal" Width="90px" runat="server" Value="$0.00" preset="money"/></div></div>
                <div><div class="text-grey text-right nowrap"><label>Payment</label></div><div><ml:MoneyTextBox ID="Pmt" Width="90px" runat="server" Value="$0.00" preset="money"/></div></div>
                <div id="selectReoDiv"><div class="text-grey text-right nowrap"><label>Select REO</label></div>
                    <div>
                        <asp:dropdownlist id="matchedReoRecordId" runat="server"></asp:dropdownlist>
                    </div>
                </div>
                <div>
                    <div></div>
                    <div><asp:CheckBox ID="WillBePdOff" onclick="f_cboxOnClick();" runat="server" Text="Will be paid off"/></div>
                </div>
                <div>
                    <div></div>
                    <div><asp:CheckBox ID="UsedInRatio" runat="server" Checked="True" Text="Used in ratio"/></div>
                </div>
                <!-- <div><div><label>x</label></div><div>y</div></div> -->
            </div>
            
            

            <p><ml:EncodedLabel ID="m_error" runat="server" Visible="false" class="text-danger"/></p>
        </div>
        <div class="modal-footer">
            <button type="button" onclick="f_onCancel()" class="btn-flat">Cancel</button>
            <asp:Button id="m_btnOK" runat="server" CssClass="btn btn-flat" OnClick="BtnOK_Click" OnClientClick="return f_btnClick();" Text="Add" />
        </div>
    </div>
</form>

<script type="text/javascript">
    var IsReadOnly      = <%= AspxTools.JsBool(IsReadOnly) %>;
    var ele_Pmt         = <%= AspxTools.JsGetElementById(Pmt        ) %>;
    var ele_Bal         = <%= AspxTools.JsGetElementById(Bal        ) %>;
    var ele_OwnerT      = <%= AspxTools.JsGetElementById(OwnerT     ) %>;
    var ele_DebtT       = <%= AspxTools.JsGetElementById(DebtT      ) %>;
    var ele_ComNm       = <%= AspxTools.JsGetElementById(ComNm      ) %>;
    var ele_WillBePdOff = <%= AspxTools.JsGetElementById(WillBePdOff) %>;
    var ele_UsedInRatio = <%= AspxTools.JsGetElementById(UsedInRatio) %>;
    var ele_ComAddr     = <%= AspxTools.JsGetElementById(ComAddr    ) %>;
    var ele_ComCity     = <%= AspxTools.JsGetElementById(ComCity    ) %>;
    var ele_ComState    = <%= AspxTools.JsGetElementById(ComState   ) %>;
    var ele_ComZip      = <%= AspxTools.JsGetElementById(ComZip     ) %>;
    var ele_AccNm       = <%= AspxTools.JsGetElementById(AccNm      ) %>;
    var ele_AccNum      = <%= AspxTools.JsGetElementById(AccNum     ) %>;
</script>

<script type="text/javascript">
;(function($) {
    var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    var iframeSelf = new IframeSelf();
    iframeSelf.popup = popup;
    if (window.autoClose) {
        iframeSelf.resolve(true);
    }

    function _init() {
        TPOStyleUnification.Components();
        resizeForIE6And7(400, 390);
        if (IsReadOnly)
        {
            [
                ele_Pmt        ,
                ele_Bal        ,
                ele_OwnerT     ,
                ele_DebtT      ,
                ele_ComNm      ,
                ele_WillBePdOff,
                ele_UsedInRatio,
                ele_ComAddr    ,
                ele_ComCity    ,
                ele_ComState   ,
                ele_ComZip     ,
                ele_AccNm      ,
                ele_AccNum     ,
            ].forEach(function(ele){ ele.disabled = true; });
        }

        var isMortgage = getDropDownValue(ele_DebtT) == 'Mortgage';
        $j("#selectReoDiv").toggle(isMortgage);
    }

    var checkDoubleClick = 0;

    function f_btnClick() {
        if (++checkDoubleClick > 1) {
            $j("#m_btnOK").prop('disabled', true);
            return false;
        }

        [
            ele_ComNm  ,
            ele_ComAddr,
            ele_ComCity,
            ele_AccNm  ,
            ele_AccNum ,
        ].forEach(function(ele){ ele.value = cleanString(ele.value); });

        function cleanString(txt) {
            return txt.replace(/[^a-z0-9_\(\)\s]/gi,'');
        }
    }

    function f_cboxOnClick() {
        var isMortgage = getDropDownValue(ele_DebtT) == 'Mortgage';

        if (ele_WillBePdOff.checked)
            ele_UsedInRatio.checked = false;
        else if (!isMortgage)
            ele_UsedInRatio.disabled = false;
    }

    function f_ddlOnChange() {
        var isMortgage = getDropDownValue(ele_DebtT) == 'Mortgage';
        if (isMortgage) {
            ele_UsedInRatio.checked = false;
            ele_UsedInRatio.disabled = true;
        }
        else {
            ele_UsedInRatio.disabled = false;
        }
        $j("#selectReoDiv").toggle(isMortgage);
    }

    function f_onCancel(){
        iframeSelf.resolve(false);
    }

    function getDropDownValue(ddl) {
        return $j.trim($j(ddl.options[ddl.selectedIndex]).text());
    }

    Object.assign(window, {
        _init:_init,
        f_btnClick:f_btnClick,
        f_cboxOnClick:f_cboxOnClick,
        f_ddlOnChange:f_ddlOnChange,
        f_onCancel:f_onCancel
    });
})(jQuery);
</script>
</body>
</html>
