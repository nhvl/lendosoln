using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Rolodex;
using LendersOffice.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PriceMyLoan.webapp
{
    public partial class RolodexListV2 : PriceMyLoan.UI.BasePage
    {
        private AbstractUserPrincipal BrokerUser => PrincipalFactory.CurrentPrincipal;

        private bool ShowAssignedEmployees => RequestHelper.GetBool("ass");

        [WebMethod]
        public static Dictionary<string, object>[] GetDataSet(int tab, string nameFilter, int typeFilter0, Guid? typeFilter1, int statusFilter, Guid? sLId)
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;

            if (!BrokerUser.HasPermission(Permission.AllowReadingFromRolodex)) return null;

            if (tab == 0) return GetDataTabContactEntries(nameFilter, typeFilter0);
            if (tab == 1)
            {
                Role aRole = !typeFilter1.HasValue ? null : Role.LendingQBRoles.FirstOrDefault(role => role.Id == typeFilter1);
                     
                E_AgentRoleT? aType = 
                    !typeFilter1.HasValue ? (E_AgentRoleT?) null : (
                    (aRole == null || !AgentRoleTFromLabel.ContainsKey(aRole.Desc)) ? E_AgentRoleT.Other : (
                    AgentRoleTFromLabel[aRole.Desc] ));

                return GetDataTabInternalEmployees(
                    nameFilter,
                    aRole?.ModifiableDesc,
                    statusFilter,
                    aType
                );
            }
            if (tab == 2) return GetDataTabPmlUsers(nameFilter);
            if (tab == 3) return sLId.HasValue ? GetDataTabOfficialContacts(sLId.Value) : null;

            return null;
        }

        static Dictionary<string, object>[] GetDataTabContactEntries(string nameFilter, int typeFilter)
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;
            var parameters = new[] {
                new SqlParameter("@BrokerID"        , BrokerUser.BrokerId),
                new SqlParameter("@NameFilter"      , string.IsNullOrEmpty(nameFilter) ? DBNull.Value : (object) nameFilter),
                new SqlParameter("@TypeFilter"      , (typeFilter == -1) ? DBNull.Value : (object) typeFilter),
                new SqlParameter("@IsApprovedFilter", true),
            };

            DataSet ds = new DataSet();
            DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListRolodexByBrokerID", parameters);

            var items = DataSetToDictionary(ds);
            AddAgentTypeDesc(items);

            foreach (var item in items)
            {
                var returnData = GetClientReturnValue(item);

                returnData["CompanyStreetAddr"         ] = item["AgentAddr"];
                returnData["CompanyCity"               ] = item["AgentCity"];
                returnData["CompanyState"              ] = item["AgentState"];
                returnData["CompanyZip"                ] = item["AgentZip"];

                returnData["CompanyId"                   ] = item["CompanyId"                        ];
                returnData["BranchName"                  ] = item["AgentBranchName"                  ];
                returnData["PayToBankName"               ] = item["AgentPayToBankName"               ];
                returnData["PayToBankCityState"          ] = item["AgentPayToBankCityState"          ];
                returnData["PayToABANumber"              ] = item["AgentPayToABANumber"              ];
                returnData["PayToAccountName"            ] = item["AgentPayToAccountName"            ];
                returnData["PayToAccountNumber"          ] = item["AgentPayToAccountNumber"          ];
                returnData["FurtherCreditToAccountName"  ] = item["AgentFurtherCreditToAccountName"  ];
                returnData["FurtherCreditToAccountNumber"] = item["AgentFurtherCreditToAccountNumber"];

                returnData["BrokerLevelAgentID"          ] = item["AgentID"];
                returnData["ShouldMatchBrokerContact"    ] = true;

                item["returnData"] = returnData;
            }

            return items;
        }
        static Dictionary<string, object>[] GetDataTabInternalEmployees(string nameFilter, string typeFilter, int statusFilter, E_AgentRoleT? agentRoleT)
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;

            var parameters =
                new[]
                {
                    new SqlParameter("@BrokerID", BrokerUser.BrokerId),
                    new SqlParameter("@UserType", SqlDbType.Char, 1) {Value = 'B'},
                    new SqlParameter("@NameFilter", string.IsNullOrEmpty(nameFilter) ? DBNull.Value : (object) nameFilter),
                    new SqlParameter("@TypeFilter", string.IsNullOrEmpty(typeFilter) ? DBNull.Value : (object) typeFilter),
                    new SqlParameter("@StatusFilter", (statusFilter == -1) ? DBNull.Value : (object)statusFilter),
                    // OPM 55304 - If user cannot assign loans to agents of any branch and
                    // does not have Corporate level access, then limit employees list to only those
                    // employees from the same branch as user.
                    (!BrokerUser.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch) && !BrokerUser.HasPermission(Permission.BrokerLevelAccess))
                        ? new SqlParameter("@BranchId", BrokerUser.BranchId)
                        : null
                }.Where(p => p != null);

            DataSet ds = new DataSet();
            DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListRolodexUsingEmployeeByBrokerID", parameters);

            if (agentRoleT.HasValue)
            {
                foreach (DataTable dT in ds.Tables)
                {
                    foreach (DataRow dR in dT.Rows)
                    {
                        // 8/16/2005 kb - Fixup the rolodex binding to put
                        // the equivalent agent type enum value in the type
                        // column for internal employees.  Because each
                        // employee can have multiple roles, we take the
                        // currently selected role and use that.  If all
                        // roles are shown, then we take the highest ranking
                        // (non-administrator) role and map it to the agent
                        // type equivalent.

                        dR["AgentType"] = agentRoleT;
                    }
                }
            }
            else
            {
                // All employees are shown, so map each type to the
                // corresponding agent type.  We use the most important
                // role the employee maintains.

                var brokerLoanAssignmentTable = new BrokerLoanAssignmentTable();
                brokerLoanAssignmentTable.Retrieve(BrokerUser.BrokerId);

                foreach (DataTable dT in ds.Tables)
                {
                    foreach (DataRow dR in dT.Rows)
                    {
                        // 8/16/2005 kb - Fixup the rolodex binding to put
                        // the equivalent agent type enum value in the type
                        // column for internal employees.  Because each
                        // employee can have multiple roles, we take the
                        // currently selected role and use that.  If all
                        // roles are shown, then we take the highest ranking
                        // (non-administrator) role and map it to the agent
                        // type equivalent.



                        var aRole = brokerLoanAssignmentTable[(Guid) dR["AgentId"]]
                            .Where(role => role.RoleT != E_RoleT.Administrator)
                            .OrderByDescending(role => role.ImportanceRank)
                            .FirstOrDefault();
                        var sRoleDesc = aRole == null ? string.Empty : aRole.Desc;

                        dR["AgentType"] = AgentRoleTFromLabel.ContainsKey(sRoleDesc) ? AgentRoleTFromLabel[sRoleDesc] : E_AgentRoleT.Other;
                    }
                }
            }

            var items = DataSetToDictionary(ds);
            AddAgentTypeDesc(items);

            foreach (var item in items)
            {
                bool bUseBranchInfoForLoans = (bool)item["UseBranchInfoForLoans"];

                var returnData = GetClientReturnValue(item);

                returnData["AgentCompanyName"         ] = item["CompanyDisplayName"];
                returnData["EmployeeIDInCompany" ] = item["EmployeeIDInCompany"];
                returnData["CompanyStreetAddr"    ] = !bUseBranchInfoForLoans ? item["AddrOfCompany"      ] : item["AddrOfBranch"       ];
                returnData["CompanyCity"       ] = !bUseBranchInfoForLoans ? item["CityOfCompany"      ] : item["CityOfBranch"       ];
                returnData["CompanyState"      ] = !bUseBranchInfoForLoans ? item["StateOfCompany"     ] : item["StateOfBranch"      ];
                returnData["CompanyZip"        ] = !bUseBranchInfoForLoans ? item["ZipOfCompany"       ] : item["ZipOfBranch"        ];
                returnData["CompanyLoanOriginatorIdentifier"] = !bUseBranchInfoForLoans ? item["BrokerLosIdentifier"] : item["BranchLosIdentifier"];
                returnData["LoanOriginatorIdentifier"       ] = item["LosIdentifier"];

                if (bUseBranchInfoForLoans)
                {
                    returnData["PhoneOfCompany"] = item["PhoneOfBranch"];
                    returnData["FaxOfCompany"] = item["FaxOfBranch"];
                }

                var companyLicenseInfoList = new LicenseInfoList();
                companyLicenseInfoList.Retrieve(!bUseBranchInfoForLoans
                    ? item["BrokerLicenseXml"] as string
                    : item["BranchLicenseXml"] as string);
                SetLicensexml(returnData, item, companyLicenseInfoList);

                item["returnData"] = returnData;
            }

            return items;
        }
        static Dictionary<string, object>[] GetDataTabPmlUsers(string nameFilter)
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;

            DataSet ds = new DataSet();

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerID", BrokerUser.BrokerId),
                new SqlParameter("@UserType", SqlDbType.Char, 1) {Value = 'P'},
                new SqlParameter("@NameFilter", string.IsNullOrEmpty(nameFilter) ? DBNull.Value : (object) nameFilter)
            };
            DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListRolodexUsingEmployeeByBrokerID", parameters);
            var items = DataSetToDictionary(ds);
            AddAgentTypeDesc(items);

            foreach (var item in items)
            {
                var returnData = GetClientReturnValue(item);

                try
                {
                    var pmlBroker = PmlBroker.RetrievePmlBrokerByName(item["AgentComNm"] as string, BrokerUser.BrokerId);
                    returnData["AgentCompanyName"] = pmlBroker.NmlsName;

                    returnData["CompanyStreetAddr"] = pmlBroker.Addr;
                    returnData["CompanyCity"] = pmlBroker.City;
                    returnData["CompanyState"] = pmlBroker.State;
                    returnData["CompanyZip"] = pmlBroker.Zip;

                    returnData["PhoneOfCompany"] = pmlBroker.Phone;
                    returnData["FaxOfCompany"] = pmlBroker.Fax;

                    returnData["LoanOriginatorIdentifier"] = item["LosIdentifier"];
                    returnData["CompanyLoanOriginatorIdentifier"] = pmlBroker.NmLsIdentifier;

                    var companyLicenseInfoList = pmlBroker.LicenseInfoList;
                    SetLicensexml(returnData, item, companyLicenseInfoList);
                }
                catch { }

                item["returnData"] = returnData;
            }

            return items;
        }
        static Dictionary<string, object>[] GetDataTabOfficialContacts(Guid sLId)
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;

            DataSet ds = new DataSet();

            var parameters = new[]
            {
                new SqlParameter("@sLId", sLId),
                new SqlParameter("@BrokerId", BrokerUser.BrokerId)
            };
            DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListRolodexByLoanId", parameters);

            CPageData data = CPageData.CreateUsingSmartDependency(sLId, typeof(RolodexList));
            data.InitLoad();

            var rows = Enumerable.Range(0, data.sAgentCollection.GetAgentRecordCount())
                .Select(i => data.sAgentCollection.GetAgentFields(i))
                .Select(agentData =>
                {
                    DataRow row = ds.Tables[0].NewRow();
                    row["AgentTypeDesc"] = agentData.AgentRoleDescription;
                    row["AgentNm"] = agentData.AgentName;
                    row["AgentPhone"] = agentData.Phone;
                    row["AgentPhone"] = agentData.Phone;
                    row["AgentComNm"] = agentData.CompanyName;
                    row["PhoneOfCompany"] = agentData.PhoneOfCompany;
                    row["AgentEmail"] = agentData.EmailAddr;
                    return row;
                });
            foreach (var row in rows)
            {
                ds.Tables[0].Rows.Add(row);
            }

            var items = DataSetToDictionary(ds);

            foreach (var item in items)
            {
                var returnData = GetClientReturnValue(item);

                returnData["EmployeeId"] = Guid.Empty;

                item["returnData"] = returnData;
            }

            return items;
        }
        static void SetLicensexml(Dictionary<string, object> returnData, Dictionary<string, object> item, LicenseInfoList companyLicenseInfoList) {


            LicenseInfoList userLicenseInfoList = new LicenseInfoList();
            userLicenseInfoList.Retrieve(item["LicenseXmlContent"] as string);

            item["agentLicense"] = LicenseInfoList.JsonSerialize(userLicenseInfoList);
            item["companyLicense"] = LicenseInfoList.JsonSerialize(companyLicenseInfoList);

            string sLicenseState = item["AgentState"] as string;

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["loanid"]))
            {
                Guid loanId = RequestHelper.GetGuid("loanid");
                if (loanId != Guid.Empty)
                {
                    CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(RolodexList));
                    dataLoan.InitLoad();
                    sLicenseState = dataLoan.sSpState;
                }
            }

            returnData["AgentLicensesPanel"] = LicenseInfoList.JsonSerialize(userLicenseInfoList);
            returnData["CompanyLicensesPanel"] = LicenseInfoList.JsonSerialize(companyLicenseInfoList);
            returnData["AgentLicenseNumber"] = CAgentFields.GetLendingLicenseByState(sLicenseState, userLicenseInfoList);
            returnData["CompanyLicenseNumber"] = CAgentFields.GetLendingLicenseByState(sLicenseState, companyLicenseInfoList);
        }
        static Dictionary<string, object> GetClientReturnValue(Dictionary<string, object> item)
        {
            var m_losConvert = new LosConvert();

            var IsAgentAltPhoneForMultiFactorOnly = item.ContainsKey("IsAgentAltPhoneForMultiFactorOnly") && (bool) item["IsAgentAltPhoneForMultiFactorOnly"];
            var agentId =
                item.ContainsKey("AgentID") ? item["AgentID"] :
                item.ContainsKey("AgentId") ? item["AgentId"] :
                null;

            var data = new Dictionary<string, object>
            {
                { "AgentName"                      , item["AgentNm"   ] },
                { "AgentType"                      , item["AgentType" ] },
                { "AgentStreetAddr"                , item["AgentAddr" ] },
                { "AgentCity"                      , item["AgentCity" ] },
                { "AgentState"                     , item["AgentState"] },
                { "AgentZip"                       , item["AgentZip"  ] },
                { "AgentAltPhone"                  , IsAgentAltPhoneForMultiFactorOnly ? string.Empty : item["AgentAltPhone"] },
                { "AgentCompanyName"               , item["AgentComNm"] }, // 1=>CompanyDisplayName 2=>pmlBroker.NmlsName
                { "AgentEmail"                     , item["AgentEmail"           ] },
                { "AgentFax"                       , item["AgentFax"             ] },
                { "AgentPhone"                     , item["AgentPhone"           ] },
                { "AgentTitle"                     , item["AgentTitle"           ] },
                { "AgentDepartmentName"            , item["AgentDepartmentName"  ] },
                { "AgentPager"                     , item["AgentPager"           ] },
                { "AgentLicenseNumber"             , item["AgentLicenseNumber"   ] }, // 1,2 => CAgentFields.GetLendingLicenseByState(sLicenseState, userLicenseInfoList) },
                { "CompanyLicenseNumber"           , item["CompanyLicenseNumber" ] }, // 1,2 => CAgentFields.GetLendingLicenseByState(sLicenseState, companyLicenseInfoList) },
                { "PhoneOfCompany"                 , item["PhoneOfCompany"       ] }, // 1 => bUseBranchInfoForLoans => PhoneOfBranch   2 pmlBroker.Phone },
                { "FaxOfCompany"                   , item["FaxOfCompany"         ] }, // 1 =>                           FaxOfBranch     2 pmlBroker.Fax },
                { "IsNotifyWhenLoanStatusChange"   , item["IsNotifyWhenLoanStatusChange"] },
                { "CompanyStreetAddr"              , string.Empty                  }, // 0 => AgentAddr   1=>AddrOfCompany bUseBranchInfoForLoans ? AddrOfBranch  2=>pmlBroker.Addr//25 },
                { "CompanyCity"                    , string.Empty                  }, // 0 => AgentCity   1=>CityOfCompany CityOfBranch  2=>pmlBroker.City//26 },
                { "CompanyState"                   , string.Empty                  }, // 0 => AgentState  1=>StateOfCompany StateOfBranch    2 => pmlBroker.State//27 },
                { "CompanyZip"                     , string.Empty                  }, // 0 => AgentZip    1=>ZipOfCompany ZipOfBranch 2 pmlBroker.Zip //28 },
                { "CommissionPointOfLoanAmount"    , m_losConvert.ToRateString ((decimal)item["CommissionPointOfLoanAmount" ]) },
                { "CommissionPointOfGrossProfit"   , m_losConvert.ToRateString ((decimal)item["CommissionPointOfGrossProfit"]) },
                { "CommissionMinBase"              , m_losConvert.ToMoneyString((decimal)item["CommissionMinBase"           ], FormatDirection.ToRep) },
                { "LoanOriginatorIdentifier"       , string.Empty }, // 1,2 => LosIdentifier // 32 },
                { "CompanyLoanOriginatorIdentifier", string.Empty }, // 1=>BrokerLosIdentifier|BranchLosIdentifier  2=>pmlBroker.NmLsIdentifier//33 },
                { "AgentLicensesPanel"             , "[]"         }, // 1,2=>LendingLicensesPanel.SerializeLicenseInfoList(userLicenseInfoList) },
                { "CompanyLicensesPanel"           , "[]"         }, // 1,2=>LendingLicensesPanel.SerializeLicenseInfoList(companyLicenseInfoList) },
                { "EmployeeId"                     , agentId      }, // 3=>Guid.Empty
                { "EmployeeIDInCompany"            , string.Empty }, // 1=>EmployeeIDInCompany//38 },
                { "BranchName"                     , string.Empty }, // 0=>AgentBranchName  // 39 },
                { "PayToBankName"                  , string.Empty }, // 0=>AgentPayToBankName // 40 },
                { "PayToBankCityState"             , string.Empty }, // 0=>AgentPayToBankCityState // 41 },
                { "PayToABANumber"                 , string.Empty }, // 0=>AgentPayToABANumber // 42 },
                { "PayToAccountName"               , string.Empty }, // 0=>AgentPayToAccountName // 43 },
                { "PayToAccountNumber"             , string.Empty }, // 0=>AgentPayToAccountNumber // 44 },
                { "FurtherCreditToAccountName"     , string.Empty }, // 0=>AgentFurtherCreditToAccountName // 45 },
                { "FurtherCreditToAccountNumber"   , string.Empty }, // 0=>AgentFurtherCreditToAccountNumber // 46 },
                { "BrokerLevelAgentID"             , Guid.Empty   }, // 0=>AgentID // 47 },
                { "ShouldMatchBrokerContact"       , false        }, // 0=>true },
            };
            return data;
        }

        static void AddAgentTypeDesc(Dictionary<string, object>[] items)
        {
            foreach(var item in items)
            {
                item["AgentTypeDesc"] = RolodexDB.GetTypeDescription((E_AgentRoleT) item["AgentType"]);
            }
        }
        static Dictionary<string, object>[] DataSetToDictionary(DataSet ds)
        {
            var table = ds.Tables[0];
            return table.Rows.Cast<DataRow>().Select(row =>
            {
                var dict = new Dictionary<string, object>();
                foreach (DataColumn column in table.Columns)
                {
                    dict[column.ColumnName] = row[column];
                }
                return dict;
            }).ToArray();
        }

        static readonly Dictionary<string, E_AgentRoleT> AgentRoleTFromLabel = new Dictionary<string, E_AgentRoleT>(StringComparer.OrdinalIgnoreCase)
        {
            { "manager"             , E_AgentRoleT.Manager              },
            { "lockdesk"            , E_AgentRoleT.Underwriter          },
            { "underwriter"         , E_AgentRoleT.Underwriter          },
            { "processor"           , E_AgentRoleT.Processor            },
            { "loanopener"          , E_AgentRoleT.LoanOpener           },
            { "agent"               , E_AgentRoleT.LoanOfficer          },
            { "lenderaccountexec"   , E_AgentRoleT.Lender               },
            { "realestateagent"     , E_AgentRoleT.SellingAgent         },
            { "telemarketer"        , E_AgentRoleT.CallCenterAgent      },
            { "accountant"          , E_AgentRoleT.Underwriter          },
            { "funder"              , E_AgentRoleT.Funder               },
            { "shipper"             , E_AgentRoleT.Shipper              },
            { "postcloser"          , E_AgentRoleT.PostCloser           },
            { "insuring"            , E_AgentRoleT.Insuring             },
            { "collateralagent"     , E_AgentRoleT.CollateralAgent      },
            { "docdrawer"           , E_AgentRoleT.DocDrawer            },
            { "creditauditor"       , E_AgentRoleT.CreditAuditor        },
            { "disclosuredesk"      , E_AgentRoleT.DisclosureDesk       },
            { "juniorprocessor"     , E_AgentRoleT.JuniorProcessor      },
            { "juniorunderwriter"   , E_AgentRoleT.JuniorUnderwriter    },
            { "legalauditor"        , E_AgentRoleT.LegalAuditor         },
            { "loanofficerassistant", E_AgentRoleT.LoanOfficerAssistant },
            { "purchaser"           , E_AgentRoleT.Purchaser            },
            { "qccompliance"        , E_AgentRoleT.QCCompliance         },
            { "secondary"           , E_AgentRoleT.Secondary            },
            { "servicing"           , E_AgentRoleT.Servicing            },
        };

        /// <summary>
        /// Initialize this page.
        /// </summary>
        protected void PageLoad(object sender, System.EventArgs a )
        {
            RegisterJsObjectWithJsonNetAnonymousSerializer("rolodexListViewModel", new
            {
                typeFilterOptions0 = RolodexDB.GetAgentTypeOptions().Select(t => new {label = RolodexDB.GetTypeDescription(t), value = t}),
                typeFilterOptions1 = Role.LendingQBRoles.Select((role, i) => new {label = role.ModifiableDesc, value = role.Id}),
                AllowReadingFromRolodex = BrokerUser.HasPermission(Permission.AllowReadingFromRolodex),
                AllowWritingToRolodex = BrokerUser.HasPermission(Permission.AllowWritingToRolodex),
                HasFeaturesPriceMyLoan = BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan),
                ShowPmlUsers = false, // Don't want to show these for now.
                ShowInternalEmployees = false, // Don't want to show these for now.
                ShowAssignedEmployees,
            });
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            RegisterJsScript("/webapp/common.js");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            // this.PreRender += new System.EventHandler(this.PagePreRender);
        }
        #endregion
    }
}
