﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using common;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.TPO;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a picker to add pipeline reports.
    /// </summary>
    public partial class PipelineReportPicker : UI.BasePage
    {
        /// <summary>
        /// Gets the jquery version for the page.
        /// </summary>
        /// <returns>
        /// The jquery version for the page.
        /// </returns>
        protected override E_JqueryVersion GetJQueryVersion() => E_JqueryVersion._1_12_4;

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.Equals("B", this.PriceMyLoanUser.Type, StringComparison.OrdinalIgnoreCase))
            {
                this.BindRetailUserCustomReports();
            }
            else
            {
                this.BindTpoUserCustomReports();
            }
        }

        /// <summary>
        /// Binds custom reports for the picker user a retail user's report list.
        /// </summary>
        private void BindRetailUserCustomReports()
        {
            var selectedReports = Tools.GetMainTabsSettingXmlContent(this.PriceMyLoanUser.ConnectionInfo, this.PriceMyLoanUser.UserId).Tabs
                .Where(tab => tab.ReportId != Guid.Empty)
                .Select(tab => new KeyValuePair<Guid, string>(tab.ReportId, tab.TabName));

            var existingReportIds = new HashSet<Guid>(selectedReports.Select(kvp => kvp.Key));
            var allReports = Tools.GetAllLenderCustomReports(this.PriceMyLoanUser.BrokerId)
                .Where(kvp => !existingReportIds.Contains(kvp.Key))
                .OrderBy(kvp => kvp.Value, StringComparer.Ordinal);

            var settings = new LeftRightPickerSettings()
            {
                AvailableSource = allReports,
                AvailableLabel = "Available Reports",
                AssignedSource = selectedReports,
                AssignedLabel = "Selected Reports",
                DataValueField = "Key",
                DataTextField = "Value"
            };

            this.Picker.SetPickerSettings(settings);
            this.Picker.DataBind();
        }

        /// <summary>
        /// Binds custom reports for the picker user a TPO user's report list.
        /// </summary>
        private void BindTpoUserCustomReports()
        {
            var pipelineSettings = new TpoUserPipelineSettings(this.PriceMyLoanUser);
            var selectedReports = pipelineSettings.Retrieve().Select(report => new KeyValuePair<Guid, string>(report.QueryId, report.Name));

            var existingReportIds = new HashSet<Guid>(selectedReports.Select(kvp => kvp.Key));
            var allReports = OriginatingCompanyUserPipelineSettings.ListAll(this.PriceMyLoanUser.BrokerId)
                .Where(setting => !existingReportIds.Contains(setting.QueryId) && this.IsSettingAvailableForPortalMode(setting))
                .OrderBy(setting => setting.DisplayOrder)
                .Select(setting =>
                {
                    var displayName = setting.OverridePipelineReportName ? setting.PortalDisplayName : setting.PipelineReportName;
                    return new KeyValuePair<Guid, string>(setting.QueryId, displayName);
                });

            var settings = new LeftRightPickerSettings()
            {
                AvailableSource = allReports,
                AvailableLabel = "Available Reports",
                AssignedSource = selectedReports,
                AssignedLabel = "Selected Reports",
                DataValueField = "Key",
                DataTextField = "Value"
            };

            this.Picker.SetPickerSettings(settings);
            this.Picker.DataBind();
        }

        /// <summary>
        /// Determines whether the specified <paramref name="setting"/>
        /// is available for the current portal mode.
        /// </summary>
        /// <param name="setting">
        /// The setting to check.
        /// </param>
        /// <returns>
        /// True if the setting is available, false otherwise.
        /// </returns>
        private bool IsSettingAvailableForPortalMode(OriginatingCompanyUserPipelineSettings setting)
        {
            switch (this.PriceMyLoanUser.PortalMode)
            {
                case E_PortalMode.Broker:
                    return setting.AvailableForBrokerPortalMode;
                case E_PortalMode.MiniCorrespondent:
                    return setting.AvailableForMiniCorrespondentPortalMode;
                case E_PortalMode.Correspondent:
                    return setting.AvailableForCorrespondentPortalMode;
                case E_PortalMode.Blank:
                case E_PortalMode.Retail:
                default:
                    throw new UnhandledEnumException(this.PriceMyLoanUser.PortalMode);
            }
        }
    }
}