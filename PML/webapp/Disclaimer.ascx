﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Disclaimer.ascx.cs" Inherits="PriceMyLoan.webapp.Disclaimer" %>
<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
    jQuery(function($j) {
        
        function getHasRefiWithNoPayoffLiability() {
            if (PML.model.loan["sHasMortgageLiabilityThatWillBePaidOffAtClosing"] != 'False') {
                return false;
            }

            var loanPurpose = PML.model.loan["sLPurposeTPe"];
            if (loanPurpose == <%= AspxTools.JsString(E_sLPurposeT.Purchase) %> ||
                loanPurpose == <%= AspxTools.JsString(E_sLPurposeT.HomeEquity) %>) {
                return false;
            }

            return PML.model.loan["sIsRefinancing"] === 'True';
        }

        function updateDisclaimers() {
            var disclaimersVisible = false;
            var pricingNotUpToDate = Results.AreStale && Results.AreDisplayed;
            $j('#DisclaimerLabelRepriceMessage').toggle(pricingNotUpToDate == true);
            if (pricingNotUpToDate) {
                $j('#resultsIframe')[0].contentWindow.f_displayBlockMask(true);
            }
            disclaimersVisible |= pricingNotUpToDate;

            var noAppsOnFile = PML.model.apps.length == 0;
            $j('#DisclaimerLabelAddAppsMessage').toggle(noAppsOnFile == true);
            disclaimersVisible |= noAppsOnFile;

            var appsMissingCredit = false;
            var DisclaimerLabelAppsMissingCreditHtml = '';
            var tabsHaveIncompleteInfo = false;
            var IncompleteInfoOnTabHtml = '';
            if(PML.model.apps)
            {
                var appsLength = PML.model.apps.length;
                for (var appNum = 0; appNum < appsLength; appNum++) {
                    if (model.apps[appNum]["aIsCreditReportOnFile"] != 'True') {
                        DisclaimerLabelAppsMissingCreditHtml += '<label>&bull; Credit has not been ordered for Application #'
                        + (parseInt(appNum) + 1) + '. Please order credit for more accurate results.</label>';
                        appsMissingCredit = true;
                    }
                    if (!model.apps[appNum]["isValid"]) {
                        IncompleteInfoOnTabHtml += '<label>&bull; Missing required data on "Application #'
                            + (parseInt(appNum) + 1) + '" tab. Please complete in order to price.</label>';
                        tabsHaveIncompleteInfo = true;
                    }
                }
            }
            $j('#DisclaimerLabelAppsMissingCredit').html(DisclaimerLabelAppsMissingCreditHtml);
            disclaimersVisible |= appsMissingCredit;
            if (!model.loan["isValid"]) {
                IncompleteInfoOnTabHtml += '<label>&bull; Missing required data on "Property & Loan Info" tab. Please complete in order to price.</label>';
                tabsHaveIncompleteInfo = true;
            }
            $j('#DisclaimerLabelIncompleteInfoOnTab').html(IncompleteInfoOnTabHtml);
            disclaimersVisible |= tabsHaveIncompleteInfo;

            var checkTotalIncome = (PML.model.loan["sLPurposeTPe"] != <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance) %>
                && PML.model.loan["sLPurposeTPe"] != <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl) %>)
                || PML.model.loan["sIsCreditQualifying"] == 'True';
            
            var totalIncome = 0;
            if (checkTotalIncome){
                if( PML.model.apps.length > 0 ) {
                    for (var appNum in PML.model.apps) {
                        var aBMonInc = PML.model.apps[appNum]["aBMonInc"];
                        if (aBMonInc)
                            totalIncome += Number(aBMonInc.replace(/[^0-9\.]+/g, ""));
                            
                        var aCMonInc = PML.model.apps[appNum]["aCMonInc"];
                        if (aCMonInc)
                            totalIncome += Number(aCMonInc.replace(/[^0-9\.]+/g, ""));
                    }
                } else {
                    totalIncome = Number(PML.model.loan["sPrimAppTotNonspIPe"].replace(/[^0-9\.]+/g, ""));
                }
            }
            
            var isIncomeZero = checkTotalIncome == true && totalIncome <= 0;
            $j('#DisclaimerLabelIncomeIsZero').toggle(isIncomeZero == true);
            disclaimersVisible |= isIncomeZero;

            var EstCreditScore = Number(PML.model.loan["sCreditScoreEstimatePe"].replace(/[^0-9\.]+/g, ""));
            var isCreditZero =  PML.model.apps.length == 0 && EstCreditScore <= 0;
            $j('#DisclaimerLabelCreditIsZero').toggle(isCreditZero == true);
            disclaimersVisible |= isCreditZero;

            var hasRefiWithNoPayoffLiability = getHasRefiWithNoPayoffLiability();
            $j('#DisclaimerLabelNoRefiLiabilityToBePaidOff').toggle(hasRefiWithNoPayoffLiability == true);
            disclaimersVisible |= hasRefiWithNoPayoffLiability;

            var isRenovationLoan = PML.model.loan['sIsRenovationLoan'] == "True";
            $j('#DisclaimerLabelRenovationLoanLtvChangeAfterSubmission').toggle(isRenovationLoan);
            disclaimersVisible |= isRenovationLoan;

            $j('#DisclaimerContainer').toggle(disclaimersVisible == true);
        }

        $j(window).on('PageIsValid', updateDisclaimers);
        $j(window).on('loan/price_started', updateDisclaimers);
        $j(window).on('loan/price_done', updateDisclaimers);
    });
</script>

<style type="text/css">
#DisclaimerContainer
{
	border: 1px solid gainsboro;
	background-color: #FFF;
	padding-bottom: 4px;
}
#DisclaimerContainer label
{
	float:none;
	width:auto;
	padding:3px;
	color:Red;
	display:block;
}
#DisclaimerContainer .SectionHeader
{
	font-weight:bold;
    text-decoration:underline;	
    font-size: 1em;
    color: Black;
}

.DisclaimerSectionTitle
{
    font-size: 1.2em;
    font-weight: bold;
    color:#ff9933;
    padding: 3px;
}
</style>

<div id="DisclaimerContainer" class="container">
    <div class="DisclaimerSectionTitle">Alert Messages</div>
    <label id="DisclaimerLabelConvertToLeadOrLoan" runat="server" visible="false">
        &bull; To add applicant info, and/or register a specific program, please click the <asp:PlaceHolder runat="server" ID="CreateLeadOrLoanText">“Create Lead” or “Create Loan” buttons.</asp:PlaceHolder><asp:PlaceHolder runat="server" ID="CreateLoanText">“Create Loan” button.</asp:PlaceHolder>
    </label>
    <label id="DisclaimerLabelRepriceMessage">
        &bull; Displayed results are based on previously input data. Please reprice the scenario.
    </label>
    <label id="DisclaimerLabelAddAppsMessage">
        &bull; Pricing subject to change with the addition of applicant/credit data.
    </label>
    <div id="DisclaimerLabelAppsMissingCredit">
    </div>
      
    <div id="DisclaimerLabelIncompleteInfoOnTab">
    </div>
    <label id="DisclaimerLabelIncomeIsZero">
        &bull; Total income for this file is currently $0.00.
    </label>
    <label id="DisclaimerLabelCreditIsZero">
        &bull; Estimated credit score is 0. If the borrower has a credit history, please enter an estimated score for more accurate results.
    </label>
    <label id="DisclaimerLabelNoRefiLiabilityToBePaidOff">
        &bull; There currently is no mortgage liability marked to be paid off with refinance funds. Cash to borrower and reserve months calculations will be artificially high until refi payoffs are on file.
    </label>
    <label id="DisclaimerLabelRenovationLoanLtvChangeAfterSubmission">
        &bull; For renovation scenarios, the LTV may change after submission.
    </label>
</div>