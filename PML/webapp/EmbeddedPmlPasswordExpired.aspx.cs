﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.UI.Themes;

    /// <summary>
    /// Provides a page that notifies embedded PML users that their
    /// password has expired.
    /// </summary>
    public partial class EmbeddedPmlPasswordExpired : UI.BasePage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        /// <remarks>
        /// The styling for this page must be set up here instead of using
        /// the style provider due to requests for those pages being blocked
        /// when a user's password has expired.
        /// </remarks>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.MainBody.Attributes["class"] += "add-padding";

            var compiledTheme = ThemeManager.GetCssForColorTheme(
                this.PriceMyLoanUser.BrokerId,
                SassDefaults.LqbDefaultColorTheme.Id,
                ThemeCollectionType.TpoPortal);

            var tag = $"<style type='text/css'>{compiledTheme}</style>";
            this.ClientScript.RegisterClientScriptBlock(typeof(EmbeddedPmlPasswordExpired), "ColorTheme", tag);
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.LoanID == Guid.Empty)
            {
                // When no loan ID exists, the user is running embedded PML
                // on a QuickPricer 2 loan file.
                this.RegisterJsGlobalVariables("IsQuickPricerLoan", true);
                return;
            }

            var loan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(EmbeddedPmlPasswordExpired));
            loan.AllowLoadWhileQP2Sandboxed = true;
            loan.InitLoad();

            this.RegisterJsGlobalVariables("LoanId", this.LoanID);
            this.RegisterJsGlobalVariables("IsQuickPricerLoan", loan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed);
            this.RegisterJsGlobalVariables("IsLead", Tools.IsStatusLead(loan.sStatusT));
            this.RegisterJsGlobalVariables("IsEditLeadsInFullLoanEditor", this.PriceMyLoanUser.BrokerDB.IsEditLeadsInFullLoanEditor);
        }

        /// <summary>
        /// Gets the forced compatibility mode.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;
    }
}