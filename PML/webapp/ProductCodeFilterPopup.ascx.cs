﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using UI;

    /// <summary>
    /// Popup to filter by product codes.
    /// </summary>
    public partial class ProductCodeFilterPopup : PriceMyLoan.UI.BaseUserControl
    {
        /// <summary>
        /// Gets or sets a value indicating whether the broker has the advanced filter options enabled for pricing.
        /// </summary>
        /// <value>Whether the broker has the advanced options for pricing enabled.</value>
        public bool EnableAdvancedFilterOptionsForPriceEngineResults
        {
            get;
            set;
        }

        /// <summary>
        /// Binds the data to the loan.
        /// </summary>
        /// <param name="service">The service to use.</param>
        /// <param name="dataLoan">The data loan to bind to.</param>
        /// <param name="principal">The principal to use.</param>
        public static void BindData(BaseSimpleServiceXmlPage service, CPageData dataLoan, AbstractUserPrincipal principal)
        {
            string productCodeJson = service.GetString("sSelectedProductCodeFilter", null);
            if (productCodeJson != null && dataLoan.BrokerDB.IsAdvancedFilterOptionsForPricingEnabled(principal))
            {
                dataLoan.sSelectedProductCodeFilter = SerializationHelper.JsonNetDeserialize<Dictionary<string, bool>>(productCodeJson);
            }
        }

        /// <summary>
        /// Loads the loan data and adds it to the dictioanry.
        /// </summary>
        /// <param name="loanData">The dictionary to add to.</param>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="principal">The principal to use.</param>
        public static void LoadLoan(Dictionary<string, string> loanData, CPageData dataLoan, AbstractUserPrincipal principal)
        {
            if (dataLoan.BrokerDB.IsAdvancedFilterOptionsForPricingEnabled(principal))
            {
                loanData.Add("sProductCodesByFileType", SerializationHelper.JsonNetSerialize(dataLoan.sProductCodesByFileType));
                loanData.Add("sAvailableProductCodeFilter", SerializationHelper.JsonNetSerialize(dataLoan.sAvailableProductCodeFilter));
                loanData.Add("sSelectedProductCodeFilter", SerializationHelper.JsonNetSerialize(dataLoan.sSelectedProductCodeFilter));
            }
        }

        /// <summary>
        /// Loads data for the popup.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageLoad(object sender, EventArgs e)
        {
            if (!this.EnableAdvancedFilterOptionsForPriceEngineResults)
            {
                return;
            }

            var page = this.Page as UI.BasePage;
            page.RegisterJsScript("ProductCodeFilterPopup.js");
            page.RegisterCSS("jquery-ui-1.11.css");
            page.RegisterJsScript("jquery-ui-1.11.4.min.js");
            page.EnableJqueryMigrate = false;
            page.EnableJquery = false;
        }

        /// <summary>
        /// Initializes the popup.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            var page = this.Page as UI.BasePage;
            if (page == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "ProductCodeFilterPopup can only be used on a BasePage");
            }
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        ///	the contents of this method with the code editor.
        /// </summary>
        /// <param name="e">Event argument parameter.</param>
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///	Required method for Designer support - do not modify
        ///	the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}