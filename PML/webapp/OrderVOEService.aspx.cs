﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EDocs;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.Templates;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Service page for ordering VOE.
    /// </summary>
    public partial class OrderVOEService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Runs a service method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            if (!PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableVOXConfiguration)
            {
                return;
            }

            switch (methodName)
            {
                case nameof(this.LoadLenderService):
                    this.LoadLenderService();
                    break;
                case nameof(this.RunAudit):
                    this.RunAudit();
                    break;
                case nameof(this.RunRequest):
                    this.RunRequest();
                    break;
                case "RefreshOrder":
                    this.SendSubsequentRequest(VOXRequestT.Refresh);
                    break;
                case "PollOrder":
                    this.SendSubsequentRequest(VOXRequestT.Get);
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Invalid method {methodName}.");
            }
        }

        /// <summary>
        /// Loads a lender service to update the UI.
        /// </summary>
        private void LoadLenderService()
        {
            Guid loanId = GetGuid("LoanId");
            int lenderServiceId = GetInt("LenderServiceId");
            VOXLenderService lenderService = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, lenderServiceId);
            VOXVendor vendor = lenderService.VendorForVendorAndServiceData;
            AbstractVOXVendorService service;
            if (vendor.Services.TryGetValue(VOXServiceT.VOE, out service))
            {
                VOEVendorService voeService = (VOEVendorService)service;
                this.SetResult("AllowPaymentByCreditCard", lenderService.AllowPaymentByCreditCard);
                this.SetResult("RequiresAccountId", lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId);
                this.SetResult("UsesAccountId", lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId);
                this.SetResult("VoeVerificationT", voeService.VerificationType.ToString("D"));
                this.SetResult("AskForSalaryKey", voeService.AskForSalaryKey);
                this.SetResult("UseSpecificEmployerRecordSearch", lenderService.VoeUseSpecificEmployerRecordSearch);
                this.SetResult("EnforceUseSpecificEmployerRecordSearch", lenderService.VoeEnforceUseSpecificEmployerRecordSearch);
                this.SetResult("VoeAllowRequestWithoutEmployment", lenderService.VoeAllowRequestWithoutEmployment);
                this.SetResult("VoeEnforceRequestsWithoutEmployment", lenderService.VoeEnforceRequestsWithoutEmployment);

                var principal = PrincipalFactory.CurrentPrincipal;
                var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderVOEService));
                dataLoan.InitLoad();

                var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.Verifications)
                    .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == lenderService.VendorForProtocolAndTransmissionData.VendorId);
                if (serviceCredential != null)
                {
                    this.SetResult("ServiceCredentialId", serviceCredential.Id);
                    this.SetResult("ServiceCredentialHasAccountId", !string.IsNullOrEmpty(serviceCredential.AccountId));
                }

                this.SetResult("Success", true);
            }
            else
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to load Service Provider");
            }
        }

        /// <summary>
        /// Runs the audit on an initial VOE request.
        /// </summary>
        private void RunAudit()
        {
            ServiceCredential usedServiceCredentials;
            VOERequestData requestData = this.GetVOEInitialRequestData(out usedServiceCredentials);
            if (requestData == null)
            {
                return;
            }

            VOERequestHandler requestHandler = new VOERequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>> auditResultsForUi = new List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>>();
            foreach (var section in auditResults.SectionNames)
            {
                KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>> errorsForSection = new KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>(section, auditResults.GetErrorsForSection(section));
                auditResultsForUi.Add(errorsForSection);
            }

            SetResult("Success", true);
            SetResult("AuditResults", SerializationHelper.JsonNetSerialize(auditResultsForUi));
            SetResult("AuditPassed", !auditResults.HasErrors);
        }

        /// <summary>
        /// Runs the request.
        /// </summary>
        private void RunRequest()
        {
            ServiceCredential usedServiceCredentials;
            VOERequestData requestData = this.GetVOEInitialRequestData(out usedServiceCredentials);
            if (requestData == null)
            {
                // The errors have already been set in GetVOEInitialRequestData
                return;
            }

            VOERequestHandler requestHandler = new VOERequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            if (auditResults.HasErrors)
            {
                List<string> errorsFromVendor = new List<string>()
                {
                    "Invalid data. Please run the audit again."
                };

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsFromVendor));
                return;
            }

            var result = requestHandler.SubmitRequest(doAudit: false);
            IEnumerable<string> errors = result?.Errors;
            var initialOrders = result?.Orders;
            if (initialOrders == null || !initialOrders.Any() ||
                (errors != null && errors.Any()))
            {
                List<string> requestErrors = new List<string>() { "Unable to fulfill order request" };
                if (errors != null && errors.Any())
                {
                    requestErrors = errors.ToList();
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(requestErrors));
                return;
            }

            List<VOEOrderViewModel> orderViewModels = new List<VOEOrderViewModel>();
            List<Guid> edocIdsToStitch = new List<Guid>();
            foreach (VOEOrder order in initialOrders)
            {
                ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
                {
                    RequiresAccountId = requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId,
                    UsesAccountId = requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId,
                    AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(requestData.LoanId)
                };

                if (usedServiceCredentials != null)
                {
                    extraInfo.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(usedServiceCredentials.AccountId);
                    extraInfo.ServiceCredentialId = usedServiceCredentials.Id;
                }

                VOEOrderViewModel model = order.CreateViewModel(extraInfo);
                orderViewModels.Add(model);

                if (model.AssociatedEdocs.Any())
                {
                    edocIdsToStitch.AddRange(model.AssociatedEdocs.Where((edoc) => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select((edoc) => edoc.EDocId));
                }
            }

            SetResult("Orders", SerializationHelper.JsonNetSerialize(orderViewModels));
            SetResult("Success", true);

            if (edocIdsToStitch.Any())
            {
                var joinedIds = string.Join("|", edocIdsToStitch);
                var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                SetResult("Key", key);
            }
        }

        /// <summary>
        /// Runs a subsequent request (poll/refresh).
        /// </summary>
        /// <param name="type">The type of request to run.</param>
        private void SendSubsequentRequest(VOXRequestT type)
        {
            string accountId = this.GetString("AccountId", string.Empty);
            string username = this.GetString("Username", string.Empty);
            string password = this.GetString("Password", string.Empty);
            Guid loanId = this.GetGuid("LoanId");
            int previousOrderId = this.GetInt("OrderId");
            int orderId = this.GetInt("OrderId");

            VOEOrder previousOrder = VOEOrder.GetOrderForLoan(PrincipalFactory.CurrentPrincipal.BrokerId, loanId, orderId);
            if (previousOrder == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { "Order not found." }));
                return;
            }

            if (previousOrder.UpdateForLenderService() && previousOrder.CurrentStatus == VOXOrderStatusT.Error)
            {
                VOEOrderViewModel errorModel = previousOrder.CreateViewModel(null);
                SetResult("UpdatedOrder", SerializationHelper.JsonNetSerialize(errorModel));
                SetResult("Success", true);
                return;
            }

            VOERequestData requestData = new VOERequestData(PrincipalFactory.CurrentPrincipal, loanId, type, previousOrder)
            {
                AccountId = accountId,
                Username = username,
                Password = password
            };

            var principal = PrincipalFactory.CurrentPrincipal;
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderVOEService));
            dataLoan.InitLoad();

            var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.Verifications)
                                            .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == requestData.LenderService.VendorForProtocolAndTransmissionData.VendorId);
            if (serviceCredential != null)
            {
                requestData.Username = serviceCredential.UserName;
                requestData.Password = serviceCredential.UserPassword.Value;
                requestData.AccountId = string.IsNullOrEmpty(serviceCredential.AccountId) ? accountId : serviceCredential.AccountId;
            }

            string errors;
            if (!requestData.ValidateRequestData(out errors))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { errors }));
                return;
            }

            VOERequestHandler requestHandler = new VOERequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            if (auditResults.HasErrors)
            {
                List<string> errorsFromVendor = new List<string>();
                foreach (var auditErrors in auditResults.GetAllErrors())
                {
                    errorsFromVendor.Add(auditErrors.ErrorMessage);
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsFromVendor));
                return;
            }

            var result = requestHandler.SubmitRequest(doAudit: false);
            IEnumerable<string> requestErrors = result?.Errors;
            VOEOrder updatedOrder = result?.Orders?.FirstOrDefault() as VOEOrder;
            if (updatedOrder == null ||
                (requestErrors != null && requestErrors.Any()))
            {
                List<string> errorsForUi = new List<string>() { "Unable to fulfill order request." };
                if (requestErrors != null && requestErrors.Any())
                {
                    errorsForUi = requestErrors.ToList();
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsForUi));
                return;
            }

            var lenderService = requestData.LenderService;
            if (updatedOrder.LenderServiceId != requestData.LenderServiceId)
            {
                lenderService = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, updatedOrder.LenderServiceId);
            }

            ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
            {
                RequiresAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId,
                UsesAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId,
                AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(requestData.LoanId)
            };

            if (serviceCredential != null)
            {
                extraInfo.ServiceCredentialId = serviceCredential.Id;
                extraInfo.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(serviceCredential.AccountId);
            }

            VOEOrderViewModel model = updatedOrder.CreateViewModel(extraInfo) as VOEOrderViewModel;
            SetResult("UpdatedOrder", SerializationHelper.JsonNetSerialize(model));
            SetResult("Success", true);

            if (model.AssociatedEdocs.Count > 0)
            {
                var joinedIds = string.Join("|", model.AssociatedEdocs.Where(edoc => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select(edoc => edoc.EDocId));
                var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                SetResult("Key", key);
            }
        }

        /// <summary>
        /// Gets the data for an initial VOE request.
        /// </summary>
        /// <param name="usedServiceCredential">The service credential used.</param>
        /// <returns>The request data if valid. Null otherwise.</returns>
        private VOERequestData GetVOEInitialRequestData(out ServiceCredential usedServiceCredential)
        {
            usedServiceCredential = null;

            Guid loanId = this.GetGuid("LoanId");
            int lenderService = this.GetInt("LenderService");
            string notificationEmail = this.GetString("NotificationEmail");
            string accountId = this.GetString("AccountId");
            string userName = this.GetString("UserName");
            string password = this.GetString("Password");
            bool payWithCreditCard = this.GetBool("PayWithCreditCard", false);
            string billingFirstName = this.GetString("BillingFirstName", string.Empty);
            string billingMiddleName = this.GetString("BillingMiddleName", string.Empty);
            string billingLastName = this.GetString("BillingLastName", string.Empty);
            string billingCardNumber = this.GetString("BillingCardNumber", string.Empty);
            string billingCVV = this.GetString("BillingCVV", string.Empty);
            string billingStreetAddress = this.GetString("BillingStreetAddress", string.Empty);
            string billingCity = this.GetString("BillingCity", string.Empty);
            string billingState = this.GetString("BillingState", string.Empty);
            string billingZipcode = this.GetString("BillingZipcode", string.Empty);
            int billingExpirationMonth = this.GetInt("BillingExpirationMonth", 0);
            int billingExpirationYear = this.GetInt("BillingExpirationYear", 0);
            bool verifyIncome = this.GetBool("VerifyIncome", false);
            bool useSpecificEmployerRecordSearch = this.GetBool("UseSpecificEmployerRecordSearch", false);
            bool verifiesBorrower = this.GetBool("VerifiesBorrower");

            Guid? borrowerAppId = null;
            E_BorrowerModeT? borrowerType = null;
            string borrowerInfo = this.GetString("BorrowerInfo");
            if (!string.IsNullOrEmpty(borrowerInfo))
            {
                var split = borrowerInfo.Split('_');
                borrowerAppId = split[0].ToNullable<Guid>(Guid.TryParse);
                borrowerType = split[1].TryParseEnum<E_BorrowerModeT>();
            }

            List<VOEEmploymentData> employmentData = new List<VOEEmploymentData>();
            List<EmploymentRecordModel> recordModels = SerializationHelper.JsonNetDeserialize<List<EmploymentRecordModel>>(GetString("EmploymentInfo"));
            if (recordModels != null)
            {
                foreach (var model in recordModels)
                {
                    employmentData.Add(new VOEEmploymentData(model.AppId, model.EmploymentRecordId, model.BorrowerType == E_BorrowerModeT.Coborrower));
                }
            }

            List<VOXBorrowerAuthDoc> docPickerInfo = new List<VOXBorrowerAuthDoc>();
            string docPickerString = this.GetString("DocPickerInfo");
            if (!string.IsNullOrEmpty(docPickerString))
            {
                foreach (var info in docPickerString.Split(','))
                {
                    var infoSplit = info.Split('|');
                    E_BorrowerModeT borrower;
                    if (!Enum.TryParse(infoSplit[2], out borrower) && Enum.IsDefined(typeof(E_BorrowerModeT), borrower))
                    {
                        SetResult("Success", false);
                        SetResult("Errors", "Invalid asset owner type.");
                        return null;
                    }

                    docPickerInfo.Add(new VOXBorrowerAuthDoc(Guid.Parse(infoSplit[0]), Guid.Parse(infoSplit[1]), borrower == E_BorrowerModeT.Coborrower));
                }
            }

            List<SalaryKeyModel> salaryKeyModels = SerializationHelper.JsonNetDeserialize<List<SalaryKeyModel>>(this.GetString("SalaryKeys"));
            List<VOESalaryKey> salaryKeys = new List<VOESalaryKey>();
            if (salaryKeyModels != null)
            {
                foreach (var model in salaryKeyModels)
                {
                    salaryKeys.Add(new VOESalaryKey(model.AppId, model.BorrowerType == E_BorrowerModeT.Coborrower, model.SalaryKey));
                }
            }

            List<SelfEmploymentModel> selfEmploymentModels = SerializationHelper.JsonNetDeserialize<List<SelfEmploymentModel>>(this.GetString("SelfEmploymentInfo"));
            List<VOESelfEmploymentData> selfEmploymentData = new List<VOESelfEmploymentData>();
            if (selfEmploymentModels != null)
            {
                foreach (var model in selfEmploymentModels)
                {
                    var data = new VOESelfEmploymentData(model.AppId, model.BorrowerType == E_BorrowerModeT.Coborrower)
                    {
                        CompanyName = model.CompanyName,
                        Notes = model.Notes,
                        TaxCity = model.City,
                        TaxesSelfPrepared = model.TaxesSelfPrepared,
                        TaxFaxNumber = model.FaxNumber,
                        TaxPhoneNumber = model.PhoneNumber,
                        TaxState = model.State,
                        TaxStreetAddress = model.StreetAddress,
                        TaxZipcode = model.Zipcode
                    };

                    selfEmploymentData.Add(data);
                }
            }

            VOXLenderService lenderServiceSettings = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, lenderService);
            if ((!lenderServiceSettings.VoeAllowRequestWithoutEmployment && lenderServiceSettings.VoeEnforceRequestsWithoutEmployment && verifiesBorrower)
                || (lenderServiceSettings.VoeAllowRequestWithoutEmployment && lenderServiceSettings.VoeEnforceRequestsWithoutEmployment && !verifiesBorrower))
            {
                SetResult("Success", false);
                SetResult("Errors", "Invalid request.");
                return null;
            }

            VOERequestData requestData = new VOERequestData(PrincipalFactory.CurrentPrincipal, lenderService, loanId, VOXRequestT.Initial)
            {
                AccountId = accountId,
                BillingExpirationYear = billingExpirationYear,
                BillingCardNumber = billingCardNumber,
                BillingCity = billingCity,
                BillingCVV = billingCVV,
                BillingExpirationMonth = billingExpirationMonth,
                BillingFirstName = billingFirstName,
                BillingMiddleName = billingMiddleName,
                BillingLastName = billingLastName,
                BillingState = billingState,
                BillingStreetAddress = billingStreetAddress,
                BorrowerAuthDocs = docPickerInfo,
                Password = password,
                PayWithCreditCard = payWithCreditCard,
                Username = userName,
                BillingZipcode = billingZipcode,
                NotificationEmail = notificationEmail,
                SalaryKeyData = salaryKeys,
                SelfEmploymentData = selfEmploymentData,
                ShouldVerifyIncome = verifyIncome,
                UserRequestedUseSpecificEmployerRecordSearch = useSpecificEmployerRecordSearch,
                EmploymentData = employmentData,
                VerifiesBorrower = verifiesBorrower,
                BorrowerAppId = borrowerAppId,
                BorrowerType = borrowerType
            };

            var principal = PrincipalFactory.CurrentPrincipal;
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderVOEService));
            dataLoan.InitLoad();

            var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.Verifications)
                                            .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == requestData.LenderService.VendorForProtocolAndTransmissionData.VendorId);
            if (serviceCredential == null && (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password)))
            {
                SetResult("Success", false);
                SetResult("Errors", "Invalid credentials.");
                return null;
            }

            if (serviceCredential != null)
            {
                usedServiceCredential = serviceCredential;
                requestData.Username = serviceCredential.UserName;
                requestData.Password = serviceCredential.UserPassword.Value;
                requestData.AccountId = string.IsNullOrEmpty(serviceCredential.AccountId) ? accountId : serviceCredential.AccountId;
            }

            string errors;
            if (!requestData.ValidateRequestData(out errors))
            {
                SetResult("Success", false);
                SetResult("Errors", errors);
                return null;
            }

            return requestData;
        }

        /// <summary>
        /// View model for salary key data.
        /// </summary>
        private class SalaryKeyModel
        {
            /// <summary>
            /// Gets or sets the salary key.
            /// </summary>
            /// <value>The salary key.</value>
            public string SalaryKey
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the app id.
            /// </summary>
            /// <value>The app id.</value>
            public Guid AppId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the borrower type.
            /// </summary>
            /// <value>The borrower type.</value>
            public E_BorrowerModeT BorrowerType
            {
                get; set;
            }
        }

        /// <summary>
        /// View model for self employment info.
        /// </summary>
        private class SelfEmploymentModel
        {
            /// <summary>
            /// Gets or sets the app id.
            /// </summary>
            /// <value>The app id.</value>
            public Guid AppId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the borrower type.
            /// </summary>
            /// <value>The borrower type.</value>
            public E_BorrowerModeT BorrowerType
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the notes.
            /// </summary>
            /// <value>The notes.</value>
            public string Notes
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the taxes are self prepared.
            /// </summary>
            /// <value>Whether the taxes are self prepared.</value>
            public bool TaxesSelfPrepared
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the company name.
            /// </summary>
            /// <value>The company name.</value>
            public string CompanyName
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the street address.
            /// </summary>
            /// <value>The street address.</value>
            public string StreetAddress
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the city.
            /// </summary>
            /// <value>The city specified.</value>
            public string City
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the state.
            /// </summary>
            /// <value>The state.</value>
            public string State
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the zipcode.
            /// </summary>
            /// <value>The zipcode.</value>
            public string Zipcode
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the phone number.
            /// </summary>
            /// <value>The phone number.</value>
            public string PhoneNumber
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the fax number.
            /// </summary>
            /// <value>The fax number.</value>
            public string FaxNumber
            {
                get; set;
            }
        }

        /// <summary>
        /// View model for employment record info.
        /// </summary>
        private class EmploymentRecordModel
        {
            /// <summary>
            /// Gets or sets the app id.
            /// </summary>
            /// <value>The app id.</value>
            public Guid AppId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the employment record id.
            /// </summary>
            /// <value>The employment record id.</value>
            public Guid EmploymentRecordId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the borrower type.
            /// </summary>
            /// <value>The borrower type.</value>
            public E_BorrowerModeT BorrowerType
            {
                get; set;
            }
        }
    }
}