﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QualRateCalculationPopup.aspx.cs" Inherits="PriceMyLoan.webapp.QualRateCalculationPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Qualifying Rate Calculation</title>
    <style type="text/css">
        #QualRateBody {
            padding: 0px;
        }
        .align-bottom-right {
            position: fixed;
            bottom: 0;
            right: 0;
            padding: 0px;
        }
    </style>
</head>
<body id="QualRateBody">
    <form id="form1" runat="server">
    <div class="modal-header">
        <h4 class="modal-title">Qualifying Rate Calculation</h4>
    </div>
    <div id="QualRateCalcPopup" class="qual-rate-popup modal-body" runat="server">
    <table class="qual-rate-table">
        <tr>
            <td colspan="3">
                Use Qual Rate? &nbsp;
                <input type="checkbox" runat="server" id="sUseQualRate" NoHighlight="true" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Calculation Type &nbsp;
                <input type="radio" name="sQualRateCalculationT" class="sQualRateCalculationT" id="QualRateCalculationFlatValue" runat="server" data-field-id="sQualRateCalculationT" /><label for="QualRateCalculationFlatValue">Flat Value</label> 
                <input type="radio" name="sQualRateCalculationT" class="sQualRateCalculationT" id="QualRateCalculationMaxOf" runat="server" data-field-id="sQualRateCalculationT" /><label for="QualRateCalculationMaxOf">Max of</label>  
            </td>
        </tr>
        <tr class="inset">
            <td>
                <asp:DropDownList runat="server" ID="sQualRateCalculationFieldT1" CssClass="QualRateCalculationField"></asp:DropDownList> +
            </td>
            <td>
                <ml:percenttextbox id="sQualRateCalculationAdjustment1" runat="server" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox> =
            </td>
            <td>
                <ml:PercentTextBox ID="QualRateCalculationResult1" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
            </td>
        </tr>
        <tr id="SecondCalculationRow" class="inset">
            <td>
                <asp:DropDownList runat="server" ID="sQualRateCalculationFieldT2" CssClass="QualRateCalculationField"></asp:DropDownList> +
            </td>
            <td>
                <ml:percenttextbox id="sQualRateCalculationAdjustment2" runat="server" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox> =
            </td>
            <td>
                <ml:PercentTextBox ID="QualRateCalculationResult2" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Qual Rate = <ml:PercentTextBox ID="sQualIR" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
            </td>
        </tr>
    </table>
    <div class="align-bottom-right">
        <input type="button" class="btn btn-flat" id="QualRateCalculationPopup_Close" value="Cancel" onclick="onQualCancel();" runat="server" />
            <input type="button" class="btn btn-flat" id="QualRateCalculationPopup_Ok" value="Save" onclick="onQualSave();" runat="server" />
    </div>
</div>
    </form>
</body>
</html>
