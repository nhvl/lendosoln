﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="TPOPortalClosingCosts.aspx.cs" Inherits="PriceMyLoan.webapp.TPOPortalClosingCosts" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Originator Portal Fee Editor</title>
</head>
<body>
<noscript>
    For full functionality of this site it is necessary to enable JavaScript. Here are
    the <a href="http://www.enable-javascript.com/" target="_blank">instructions how to
            enable JavaScript in your web browser</a>.
</noscript>

<tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />

<form runat="server">
    <div class="content-detail" name="content-detail">
        <div class="warp-section warp-section-tabs" loan-static-tab>
            <div class="static-tabs"><div class="static-tabs-inner">
                <table id="TitleTable" class="title pull-right" runat="server">
                    <tr>
                        <td class="t_fee" align="left">
                            <label>
                            </label>
                        </td>
                        <td class="t_runPricing" align="right">
                            <button type="button" id="runPricingBtn" class="btn btn-default">Run Pricing</button>
                        </td>
                    </tr>
                </table>

                <header class="page-header" id="ContentHeader" runat="server">Closing Costs</header>

                <div ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>

                <ul id="tabs" class="nav nav-tabs nav-tabs-panels">
                    <li class="first-tab" id="BorrowerHud1" runat="server">
                        <a href="#sec-closingcost" value="False" class="selected">Borrower-Responsible Closing Costs</a>
                    </li>
                    <li runat="server" id="BorrowerEstimate">
                        <a value="False" href="#sec-closingcost8">Borrower-Responsible Closing Costs</a>
                    </li>
                    <li runat="server" id="expensesTab">
                        <a id="expensesTabLink" href="#sec-housing">Non-P&I Housing Expenses</a>
                    </li>
                    <li runat="server" id="QMTab">
                        <a id="QMTabLink" href="#sec-calculation">QM/HPML Calculations</a>
                    </li>
                    <li id="SellerHud1" runat="server">
                        <a value="True" href="#sec-closingcost">Non Borrower-Responsible Closing Costs</a>
                    </li>
                    <li runat="server" id="SellerEstimate">
                        <a value="True" href="#sec-closingcost8">Non Borrower-Responsible Closing Costs</a>
                    </li>
                </ul>
            </div></div>
            <div class="content-tabs">
                <div id="container" class="content-closing-cost-qm"></div>
            </div>
        </div>
    </div>

    <script id="CalculationModalContent" type="text/ractive">
        <div class="{{loan.IsUsingSellerFees ? 'sellerclosingcostset' : 'borrowerclosingcostset'}}">
            <div class="{{section.SectionType}} {{section.HudLineStart}}">
                <p data-id="{{typeid}}">
                    {{#if (fee.t == 1 || fee.t == 2 || fee.t == 3)}}
                        <div class="table table-modal">
                            <div>
                                <div><label>Type:</label></div>
                                <div>
                                    <div class="select-box select-box-noimage">
                                        <select id="feeType" value="{{fee.t}}" data-path-name="FormulaT" on-change="fee_t_change">
                                            <option value="3">Flat Amount</option>
                                            <option value="1">Full</option>
                                            <option value="2">Percent Only</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{/if}}

                    {{#if (fee.t == 0 || fee.t == 4 || fee.t == 5 || fee.t == 17 || fee.t == 18)}}
                        Not Supported
                    {{elseif (fee.t == 1 || fee.t == 8)}}
                        <div class="table table-modal">
                            <div>
                                <div>Percent</div>
                                <div><input type="text" value="{{fee.p}}" preset="percent" initMask-in class="money mask form-control-percent" data-path-name="Percent"></div>
                                <div>of</div>
                                <div>
                                    <div class="select-box select-box-noimage">
                                        <select class="BaseAmount" NotEditable="true" value="{{fee.pt}}" data-path-name="PercentBaseT">
                                            <asp:Repeater runat="server" ID="PercentRepeater"><ItemTemplate>
                                                <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                    <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %>
                                                </option>
                                            </ItemTemplate></asp:Repeater>
                                        </select>
                                    </div>
                                </div>
                                <div>+</div>
                                <div><input type="text" value="{{fee.base}}" preset="money" initMask-in class="money mask form-control-money" data-path-name="BaseAmount"></div>
                            </div>
                        </div>
                    {{elseif (fee.t == 2)}}
                        <div class="table table-modal">
                            <div>
                                <div>Percent</div>
                                <div><input type="text" class="mask form-control-percent"  value="{{fee.p}}" preset="percent" initMask-in data-path-name="Percent"></div>
                                <div>of</div>
                                <div>
                                    <div class="select-box select-box-noimage">
                                        <select class="BaseAmount" NotEditable="true" value="{{fee.pt}}" data-path-name="PercentBaseT">
                                            <asp:Repeater runat="server" ID="PercentRepeater2"><ItemTemplate>
                                                <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                            </ItemTemplate></asp:Repeater>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{elseif (fee.t == 3)}}
                        <input type="text" value="{{fee.base}}" preset="money" class="mask money form-control-money"  initMask-in data-path-name="BaseAmount">
                    {{elseif (fee.t == 6)}}
                        <input type="text" value="{{fee.period}}" initMask-in preset="numeric" style="width: 40px;" data-path-name="NumberOfPeriods"> period
                    {{elseif (fee.t == 7)}}
                        <div class="table">
                            <div>
                                <div>
                                    <input type="text" value="{{loan.sIPiaDy}}" readonly="{{!loan.sIPiaDyLckd}}" class="mask money" initMask-in preset="numeric">
                                    <label class="lock-checkbox"><input on-change="recalc" type="checkbox" checked="{{loan.sIPiaDyLckd}}"></label>
                                </div>
                                <div>
                                    days @
                                </div>
                                <div>
                                    <input type="text" value="{{loan.sIPerDay}}" readonly="{{!loan.sIPerDayLckd}}" decimalDigits="6" class="mask money form-control-money" preset="money" initMask-in />
                                    <label class="lock-checkbox"><input on-change="recalc" type="checkbox" checked="{{loan.sIPerDayLckd}}"></label>
                                </div>
                                <div>per day</div>
                            </div>
                        </div>
                    {{elseif (fee.t == 9 || fee.t == 10)}}
                        <input type="text" value="{{fee.period}}" initMask-in preset="numeric" data-path-name="NumberOfPeriods"> months @
                        <input type="text" value="{{fee.base}}" class="mask money form-control-money" preset="money" initMask-in readonly="true"> / month
                    {{elseif (fee.t == 11)}}
                        <input type="text" value="{{fee.period}}" initMask-in preset="numeric" readonly="true"> periods
                    {{elseif (fee.t == 12 || fee.t == 13 || fee.t == 15 || fee.t == 16)}}
                        <input type="text" value="{{fee.period}}" initMask-in preset="numeric" data-path-name="NumberOfPeriods"> months @
                        <input type="text" readonly="true" value="{{fee.base}}" class="mask money form-control-money" preset="money" initMask-in> / month
                    {{/if}}
                </p>
            </div>
        </div>
    </script>

    <script id="ExpenseTemplate" type="text/ractive">
        <div class-hidden="!isVisible">
            <div class="div-title" id="{{i}}_title">
                <a id="{{i}}_header" class="HousingHeader" on-click="['HousingHeaderClick', i+'_title']">
                    <div class="div-title-housing">
                        <div class="housing-title-1st">
                            <i class="material-icons">&#xE5CC;</i>

                            {{#if !!expDesc && expName != expDesc}}
                                {{expDesc}} -
                            {{/if}}

                            {{expName}}
                        </div>
                        <div class="housing-title-2nd">
                            {{monTotPITI}}/month
                        </div>
                        <div class="housing-title-3rd">{{#if isEscrowed}}Escrowed{{else}}Not Escrowed{{/if}}</div>
                    </div>
                </a>
            </div>
            <div class="housing-content" id="{{i}}_content">
                <div class="clear-content">
                    <div class="div-housing-border">
                        <header class="header header-border header-border-rate-lock">Summary</header>
                        <div class="flex-grid">
                            <div class="none-margin">
                                <div class="table table-inline ">
                                    <div>
                                        <div><label>Description</label></div>
                                        <div><input value="{{expDesc}}" type="text" id="{{i}}_expDesc" class="w-210"></div>
                                    </div>

                                    {{#if IsTax}}<div>
                                        <div>Tax Type</div>
                                        <div>
                                            <div class="select-box select-box-noimage">
                                                <select recalc id="{{i}}_taxT" value="{{taxT}}">
                                                    <asp:Repeater runat="server" ID="taxTypeRepeater"><ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate></asp:Repeater>
                                                </select>
                                            </div>
                                        </div>
                                    </div>{{/if}}

                                    <div>
                                        <div><label>Calculation Source</label></div>
                                        <div>
                                            <div class="select-box select-box-noimage">
                                                <select recalc id="{{i}}_annAmtCalcT" value="{{annAmtCalcT}}">
                                                    <asp:Repeater runat="server" ID="annAmtCalcTRepeater"><ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate></asp:Repeater>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div><label>Annual Amount</label></div>
                                        <div><input readonly="true" value="{{annAmtTot}}" type="text" id="{{i}}_annAmtTot" preset="money" class="input-w100"></div>
                                    </div>
                                    <div>
                                        <div><label>Monthly Amount (PITI)</label></div>
                                        <div><input readonly="true" value="{{monTotPITI}}" type="text" id="{{i}}_monTotPITI" preset="money" class="input-w100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="none-margin">
                                <div class="table table-inline">
                                    <div>
                                        <div>
                                            <span>Prepaid Amount for</span>
                                            <input readonly="true" value="{{prepMnth}}" type="text" id="{{i}}_prepMnthSumm" class="input-w30" preset="numeric">
                                            months
                                        </div>
                                        <div>
                                            <input readonly="true" value="{{prepAmt}}" type="text" id="{{i}}_prepAmtSumm" preset="money" class="input-w100">
                                        </div>
                                    </div>
                                    <div>
                                        <div>Monthly Amount (Servicing)</div>
                                        <div><input readonly="true" value="{{monTotServ}}" type="text" id="{{i}}_monTotServ" preset="money" class="input-w100"></div>
                                    </div>
                                    <div>
                                        <div>Reserves Amount for
                                            <input readonly="true" value="{{rsrvMon}}" type="text" id="{{i}}_rsrvMonSumm" class="input-w30" preset="numeric">
                                            months
                                        </div>
                                        <div><input readonly="true" value="{{rsrvAmt}}" type="text" id="{{i}}_rsrvAmtSumm" preset="money" class="input-w100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="div-housing-border">
                        <header class="header header-border header-border-rate-lock">Calculator</header>
                        <div class="table table-inline  table-calculator">
                            <div>
                                <div>
                                    <label>Monthly Amount (PITI)</label>
                                </div>
                                <div>
                                    <div class="table">
                                        <div>
                                            <div>
                                                ((
                                                <input readonly="{{annAmtCalcT == 1}}" recalc value="{{annAmtPerc}}" type="text" id="{{i}}_annAmtPerc" preset="percent" class="input-w100">
                                                of
                                                <div class="select-box select-box-noimage">
                                                    <select id="{{i}}_annAmtCalcT" disabled="{{annAmtCalcT == 1}}" recalc value="{{annAmtBase}}">
                                                        <asp:Repeater runat="server" ID="annAmtBaseRepeater"><ItemTemplate>
                                                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                                <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                        </ItemTemplate></asp:Repeater>
                                                    </select>
                                                </div>
                                                ) / 12 ) +
                                                <input readonly="{{annAmtCalcT == 1}}" recalc value="{{monAmtFixed}}" type="text" id="{{i}}_monAmtFixed" preset="money" {{expenseType == 0? "decimaldigits='4'" : ""}} class="input-w100">
                                                =
                                                <input readonly="true" value="{{monTotPITI}}" id="{{i}}_monTotPITI" type="text" preset="money" class="input-w100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div><label>Prepaid?</label></div>
                                <div>
                                    <input recalc checked="{{isPrepaid}}" type="checkbox" id="{{i}}_isPrepaid"><label for="{{i}}_isPrepaid">Yes</label>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <label>Prepaid Months</label>
                                </div>
                                <div>
                                    <div class="table table-font-default">
                                        <div>
                                            <div>
                                                <input readonly="{{annAmtCalcT == 1 || !isPrepaid}}" recalc value="{{prepMnth}}" type="text" id="{{i}}_prepMnthCalc" class="input-w30">
                                            </div>
                                            <div class="w-334"></div>
                                            <div><label>Prepaid Amount</label></div>
                                            <div><input readonly="true" value="{{prepAmt}}" type="text" id="{{i}}_prepAmtCalc" preset="money" class="input-w100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <label>Escrowed?</label>
                                </div>
                                <div>
                                    <div class="table table-default table-font-default">
                                        <div>
                                            <div><input recalc checked="{{isEscrowed}}" type="checkbox" id="{{i}}_isEscrowed"><label for="{{i}}_isEscrowed">Yes</label>
                                            </div>
                                            {{#if (sDisclosureRegulationT != 2 && IsCustom)}}
                                            <div colspan="11">
                                                <div class="select-box select-box-noimage">
                                                <select on-change="checkCustomSettings" on-blur="domblur" id="{{i}}_customLineNum" value="{{customLineNum}}">
                                                    <option value="0" {{isEscrowed? "disabled='disabled'" : ""}}>None</option>
                                                    <option value="5" {{sClosingCostFeeVersionT !== 2? "disabled='disabled'" : ""}}>Any</option>
                                                    <option value="1">1008</option>
                                                    <option value="2">1009</option>
                                                    <option value="3">1010</option>
                                                    <option value="4">1011</option>
                                                </select></div>
                                            </div>
                                            {{/if}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    Payments repeat
                                </div>
                                <div>
                                    <div class="select-box select-box-noimage">
                                        <select id="{{i}}_RepInterval" recalc value="{{RepInterval}}">
                                            <asp:Repeater runat="server" ID="disbRepIntervalRepeater"><ItemTemplate>
                                                <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                    <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                            </ItemTemplate></asp:Repeater>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div style="vertical-align: bottom;">
                                    <label>Disbursement Schedule Months</label>
                                </div>
                                <div>
                                    <div class="table table-font-default table-padding-right-p14-closing-costs">
                                        <div>
                                            <div>
                                                <label>Jan</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Jan}}" type="text" id="{{i}}_Jan" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Feb</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Feb}}" type="text" id="{{i}}_Feb" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Mar</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Mar}}" type="text" id="{{i}}_Mar" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Apr</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Apr}}" type="text" id="{{i}}_Apr" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>May</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{May}}" type="text" id="{{i}}_May" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Jun</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Jun}}" type="text" id="{{i}}_Jun" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Jul</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Jul}}" type="text" id="{{i}}_Jul" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Aug</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Aug}}" type="text" id="{{i}}_Aug" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Sep</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Sep}}" type="text" id="{{i}}_Sep" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Oct</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Oct}}" type="text" id="{{i}}_Oct" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Nov</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Nov}}" type="text" id="{{i}}_Nov" class="input-w30" preset="numeric">
                                            </div>
                                            <div>
                                                <label>Dec</label>
                                                <input recalc readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{Dec}}" type="text" id="{{i}}_Dec" class="input-w30" preset="numeric">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    Reserve Months Cushion
                                </div>
                                <div>
                                    <input recalc readonly="{{annAmtCalcT == 1}}" value="{{Cush}}" type="text" id="{{i}}_Cush" class="input-w30" preset="numeric"> months
                                </div>
                                <div colspan="9">
                                    <div class="notif-housing">
                                        {{#if sSchedDueD1 == ""}}
                                        Note: Reserve months calculation requires a 1st Payment Date
                                        {{/if}}
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <label>Reserve Months</label>
                                </div>
                                <div>
                                    <div class="table table-font-default">
                                        <div>
                                            <div>
                                                <div class="f-left"><input recalc readonly="{{!rsrvMonLckd}}" id="{{i}}_rsrvMon" value="{{rsrvMon}}" type="text" class="input-w30" preset="numeric"></div>
                                                <div class="f-left"><label class="lock-checkbox"><input recalc checked="{{rsrvMonLckd}}" type="checkbox" id="{{i}}_rsrvMonLckd"></label></div>
                                                <span class="h-22">months</span>
                                            </div>
                                            <div class="w-244"></div>
                                            <div>
                                                <label>Initial Reserve Amount</label>
                                            </div>
                                            <div>
                                                <input readonly="true" value="{{rsrvAmt}}" id="{{i}}_rsrvAmt" type="text" preset="money" class="input-w100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="div-housing-border">
                        <header class="header header-border header-border-rate-lock">Disbursements</header>
                        <div>
                            <div class="flex-space-between table-disbursements" id="DisbSection" cellpadding="0" cellspacing="0" border="0" width="100%">
                                <div><div class="w-117">Paid At</div></div>
                                <div><div class="w-123">Due date</div></div>
                                <div><div class="w-100">Due amount</div></div>
                                <div><div class="w-30" >Mo.</div></div>
                                <div><div class="w-123">Paid date</div></div>
                                <div><div class="w-137">Payment source</div></div>
                                <div><div class="w-123">Paid from date</div></div>
                                <div><div class="w-123">Paid to date</div></div>
                                <div><div class="w-22" ></div></div>
                            </div>

                            {{#each Disbursements:num}}
                                {{>ExpenseDisbursementTemplate}}
                            {{/each}}

                            {{#if (annAmtCalcT == 1)}}
                                <div class="flex-space-between table-disbursements" id="addDispRow">
                                    <div>
                                        <div id="{{i}}_addDispBtn" class="div-image">
                                            <a on-click="['addDisb', i]">
                                                <i class="material-icons large-material-icons">&#xE146;</i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            {{/if}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script id="ExpenseDisbursementTemplate" type="text/ractive">
        <div class="flex-space-between table-disbursements">
            <div><div>
                <div class="select-box select-box-noimage">
                    <select class="w-117" id="{{i}}_{{num}}_PaidDT" value="{{PaidDT}}" disabled="{{annAmtCalcT == 0}}">
                        <asp:Repeater runat="server" ID="PaidDateTRep"><ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate></asp:Repeater>
                    </select></div>
            </div></div>
            <div><div>
                <input recalc readonly="{{annAmtCalcT == 0}}" type="text" class="mask date" preset="date" id="{{i}}_{{num}}_DueD" datepickerTransition-in value="{{DueD}}">
            </div></div>
            <div><div>
                <input readonly="{{annAmtCalcT == 0}}" type="text" value="{{DueAmt}}" class="mask money" numericTransition-in="true"
                    preset="money" id="{{i}}_{{num}}_DueAmt" recalc>
            </div></div>
            <div><div>
                <input recalc style="width: 30px;" readonly="{{annAmtCalcT == 0}}" type="text" value="{{DisbMon}}" class="mask" id="{{i}}_{{num}}_DisbMon">
            </div></div>
            <div><div>
                <input readonly="{{annAmtCalcT == 0}}" type="text" class="mask date" preset="date" id="{{i}}_{{num}}_DisbPaidD" datepickerTransition-in value="{{DisbPaidD}}">
            </div></div>
            <div><div>
                <div class="select-box select-box-noimage">
                    <select class="w-137" id="{{i}}_{{num}}_PaySource" recalc value="{{PaySource}}" numericTransition-in="true" disabled="{{annAmtCalcT == 0}}">
                        <asp:Repeater runat="server" ID="PaySrcRep"><ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate></asp:Repeater>
                    </select>
                </div>
            </div></div>
            <div><div>
                <input readonly="{{annAmtCalcT == 0}}" type="text" class="mask date" preset="date"
                    id="{{i}}_{{num}}_PaidFromD" datepickerTransition-in value="{{PaidFromD}}">
            </div></div>
            <div><div>
                <input readonly="{{annAmtCalcT == 0}}" type="text" class="mask date" preset="date"
                    id="{{i}}_{{num}}_PaidToD" datepickerTransition-in value="{{PaidToD}}">
            </div></div>
            <div><div class="w-22">
                {{#if (annAmtCalcT == 1)}}
                    <a on-click="['removeDisb', i, num]" id="{{i}}_{{num}}_RemBtn">
                        <i class="material-icons">&#xE909;</i>
                    </a>
                {{/if}}
            </div></div>
        </div>
    </script>

    <script id="ClosingCostSectionFeeTemplate" type="text/ractive">
        <tr class="{{ (j % 2 == 0) ? 'odd' : 'even'}}" data-id="{{typeid}}">
            <td class="show-hide">
                <div>
                    <a on-click="toggleDetail">details</a>
                    <a on-click="toggleDetail"><i class="material-icons {{#if isShown}}rotate{{/if}}">&#xE5CC;</i></a>
                </div>
            </td>
            <td colspan="2" class="col-description">
                <table class="table-description-memo-paid-to">
                    <tr>
                        <td>
                            <div class="{{IsUsingSellerFees ? "description-col" : "w-183"}} flex-display">
                                <textarea class="textarea-container" readonly="{{sIsRequireFeesFromDropDown || f.t === 20 || f.t === 18}}" data-path-name="Description">{{desc}}</textarea>
                                {{#if (desc != org_desc)}}
                                    <div class="label-type">Type: {{org_desc}}</div>
                                {{/if}}
                            </div>
                        </td>
                        <td>
                            <div class="w-240">
                                <div class="RactiveDDLContainer wrap-combo">
                                    <select class="RactiveDDL" data-path-name="Beneficiary" on-change="['selectDDL', i, j]">
                                        {{#each Beneficiaries as o}}
                                            <option value="{{o.value}}" {{#if bene == o.value}}selected{{/if}}>{{o.key}}</option>
                                        {{/each}}k
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class-hidden='!isShown'>
                        <td colspan="2">
                            <div class="table table-checkboxes">
                                <div>
                                    <div>
                                        <label>Title</label>
                                        <!-- <MdlCheckbox value="{{is_title}}" disabled="{{sIsRequireFeesFromDropDown}}" data-path-name="IsTitleFee"/> -->
                                        <input type="checkbox" checked="{{is_title}}" disabled="{{sIsRequireFeesFromDropDown}}" data-path-name="IsTitleFee" NoLabel>
                                    </div>
                                    <div>
                                        <label>APR</label>
                                        <input type="checkbox" checked="{{apr}}" disabled="{{sIsRequireFeesFromDropDown}}" readonly="{{sIsRequireFeesFromDropDown}}" data-path-name="IsApr" NoLabel>
                                    </div>
                                    <div>
                                        <label>FHA</label>
                                        <input type="checkbox" checked="{{fha}}" disabled="{{sIsRequireFeesFromDropDown}}" readonly="{{sIsRequireFeesFromDropDown}}" data-path-name="IsFhaAllowable" NoLabel>
                                    </div>
                                    {{#if CanReadDFLP}}
                                        <div>
                                            <label>DFLP</label>
                                            <input type="checkbox" disabled="{{!CanSetDFLP}}" checked="{{dflp}}" data-path-name="Dflp" NoLabel/>
                                        </div>
                                    {{/if}}
                                    <div>
                                        <label>TP</label>
                                        <input type="checkbox" checked="{{tp}}" disabled="{{!sIsManuallySetThirdPartyAffiliateProps && DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>')}}" on-click="['tpClick', i, j]" data-path-name="IsThirdParty" NoLabel>
                                    </div>
                                    <div>
                                        <label>AFF</label>
                                        <input type="checkbox" checked="{{aff}}" disabled="{{!tp || (!sIsManuallySetThirdPartyAffiliateProps && DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>'))}}" data-path-name="IsAffiliate" NoLabel>
                                    </div>
                                    <div>
                                        <label>Can Shop</label>
                                        <input type="checkbox" checked="{{can_shop}}" on-change="['CanShopChange', i, j]" disabled='{{sIsRequireFeesFromDropDown || disc_sect == 2 || disc_sect == 3}}' data-path-name="CanShop" NoLabel>
                                    </div>
                                    <div>
                                        <label>Did Shop</label>
                                        <input type="checkbox" checked="{{did_shop}}" disabled="{{!can_shop}}" data-path-name="DidShop" NoLabel>
                                    </div>
                                    <div>
                                        <label>Optional</label>
                                        <input type="checkbox" checked="{{is_optional}}" disabled="{{sIsRequireFeesFromDropDown || SectionName.indexOf("H - ") == -1}}" readonly="{{sIsRequireFeesFromDropDown || SectionName.indexOf("H - ") == -1}}" data-path-name="IsOptional" NoLabel>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <div class="w-22"><div class="table">
                {{#if (SectionName.indexOf("G - ") == -1 && f.t !== 0 && f.t !== 4 && f.t !== 5 && f.t !== 14 && f.t !== 17 && f.t !== 18 && f.t !== 19 && f.t !== 20 && f.t !== 21)}}
                    <div><div>
                        <a on-click="['updateCalc', i, j]" class="calculator-svg" data-path-name="TotalAmount"><!-- #include file="~/images/Calculator.svg" --></a>
                    </div></div>
                {{/if}}
                </div></div>
            </td>
            <td class="Payments">
                <div class="w-100"><div class="table">
                    <div {{#if (pmts && pmts.length == 1)}} data-number="0" {{/if}}>
                        <div class="flex-display">
                            {{#if (pmts && pmts.length > 1 && isShown)}}
                                <div class="payment-branch">&nbsp;</div>
                            {{/if}}
                            {{#if f.t === 3}}
                                <input type="text" value="{{f.base}}" class="mask money" preset="money" numericTransition-in="true" data-path-name="TotalAmount">
                            {{else}}
                                <input type="text" value="{{total}}" class="money" preset="money"  readonly="readonly" data-path-name="TotalAmount">
                            {{/if}}
                        </div>
                    </div>
                    {{#if (pmts != null && pmts.length > 1 && isShown)}}
                        {{#each pmts as pmt:k}}
                            <div data-id="{{pmt.id}}" data-number="{{k}}">
                                <div>
                                    {{#if (k < pmts.length - 1)}}<div class="payment-branch">&nbsp;</div>{{/if}}
                                    <hr class="split-payment-branch"/>
                                    <input readonly="{{pmt.is_system || pmt.mipFinancedPmt || pmt.cashPdPmt}}" type="text" value="{{pmt.amt}}" class="mask money" numericTransition-in="true" data-path-name="TotalAmount" preset="money">
                                </div>
                            </div>
                        {{/each}}

                        {{#each pmts as pmt}}
                            {{#if (pmt.is_system && !pmt.mipFinancedPmt && !pmt.cashPdPmt)}}
                                <div>
                                    <div class="system-tooltip">
                                        <a on-click="['AddSplitPayment', i, j]" class="add-split-pmt" data-toggle="tooltip" data-placement="bottom" title="Add split payment">
                                            <i class="material-icons">&#xE146;</i>
                                        </a>
                                    </div>
                                </div>
                            {{/if}}
                        {{/each}}
                    {{/if}}
                </div></div>
            </td>
            <td class="Payments">
                <div class="w-70">
                    {{#if (pmts != null && pmts.length > 0)}}
                        {{#if (pmts.length < 2)}}
                            <div class="select-box select-box-noimage" data-id="{{pmts[0].id}}" data-number="0">
                                <select disabled="{{!IsUsingSellerFees}}" value="{{pmts[0].paid_by}}" on-change="['updatePaidBy', i, j, total]" data-path-name="PaidByT">
                                    {{#if !IsUsingSellerFees && ((sClosingCostFeeVersionT == 2) && (f.t != 5 && f.t != 21))}}
                                        <option value="-1">--split--</option>
                                    {{/if}}
                                    {{#if !IsUsingSellerFees}}
                                    <option value="1">borr pd</option>
                                    {{/if}}
                                    <option value="2">seller</option>
                                    {{#if !IsUsingSellerFees}}
                                    <option value="3">borr fin</option>
                                    {{/if}}
                                    {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                                        <option value="4">lender</option>
                                    {{/if}}
                                    <option value="5">broker</option>
                                </select>
                            </div>
                        {{else}}
                            <div class="table">
                                <div class="closing-cost-div-spacer">
                                        {{#if isShown}}
                                            <div class="spacer split-title">split</div>
                                        {{else}}
                                            <div class="spacer split-caption">split<br/>(see details)</div>
                                        {{/if}}
                                </div>
                                {{#if isShown}} {{#each pmts:k}}
                                    <div data-id="{{id}}" data-number="{{k}}">
                                        <div class="select-box select-box-noimage">
                                            <select value="{{paid_by}}" on-change="updatePaidBySplit" disabled="{{(is_system || mipFinancedPmt) && !IsUsingSellerFees}}" data-path-name="PaidByT">
                                                {{#if !IsUsingSellerFees}}
                                                    <option value="1">borr pd</option>
                                                {{/if}}
                                                <option value="2">seller</option>
                                                {{#if !cashPdPmt && !IsUsingSelleFees}}
                                                    <option value="3">borr fin</option>
                                                {{/if}}
                                                {{#if (typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %>)}}
                                                    <option value="4">lender</option>
                                                {{/if}}
                                                <option value="5">broker</option>
                                            </select>
                                        </div>
                                    </div>
                                {{/each}} {{/if}}
                            </div>
                        {{/if}}
                    {{/if}}
                </div>
            </td>
            <td class="Payments">
                <div class="w-134">
                    {{#if (pmts != null && pmts.length > 0)}}
                        {{#if (pmts.length < 2)}}
                            <div class="select-box select-box-noimage" data-id="{{pmts[0].id}}" data-number="0">
                            <select disabled="{{pmts[0].paid_by == 3}}" value="{{pmts[0].pmt_at}}" on-change="recalc" data-path-name="GfeClosingCostFeePaymentTimingT">
                                <option value="1">at closing</option>
                                <option value="2">outside of closing</option>
                            </select></div>
                        {{else}}
                            <div class="table">
                                <div class="closing-cost-div-spacer"><div><div class="spacer">&nbsp;</div></div></div>
                                {{#if isShown}} {{#pmts:k}}
                                    <div data-id="{{id}}" data-number="{{k}}">
                                        <div class="select-box select-box-noimage">
                                            <select value="{{pmt_at}}" disabled="{{is_system || paid_by == 3 || mipFinancedPmt}}" on-change="recalc" data-path-name="GfeClosingCostFeePaymentTimingT">
                                                <option value="1">at closing</option>
                                                <option value="2">outside of closing</option>
                                            </select>
                                        </div>
                                    </div>
                                {{/pmts}} {{/if}}
                            </div>
                        {{/if}}
                    {{/if}}
                </div>
            </td>
            <td class="col-date-paid payments">
                {{#if (pmts != null && pmts.length > 0)}}
                    {{#if (pmts.length < 2)}}
                        <div data-id="{{pmts[0].id}}" data-number="0">
                            <input type="text" preset="date" class="mask date" id="d{{i}}{{j}}" datepickerTransition-in recalc value="{{pmts[0].pmt_dt}}" readonly="{{pmts[0].pmt_at == 1}}" data-path-name="PaymentDate"/>
                        </div>
                    {{else}}
                        <div class="table" class-hidden='!isShown'>
                            <div class="closing-cost-div-spacer">
                                <div>
                                    <div class="spacer">&nbsp;</div>
                                </div>
                            </div>
                            {{#if (isShown)}} {{#each pmts:k}}
                            <div class-hidden='!isShown' data-id="{{id}}" data-number="{{k}}">
                                <div class="nowrap system-tooltip">
                                    <input type="text" preset="date" class="mask date" id="d{{i}}{{j}}{{k}}" datepickerTransition-in recalc value="{{pmt_dt}}" readonly="{{is_system || pmt_at == 1}}" data-path-name="PaymentDate"/>

                                    {{#if (!is_system && !mipFinancedPmt && !cashPdPmt)}}
                                        <a
                                            class="split-payment-link"
                                            on-click="['removePayment', i, j, k, amt, paid_by, desc, org_desc]"
                                            data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Remove split payment">
                                            <i class="material-icons">&#xE909;</i>
                                        </a>
                                    {{/if}}
                                </div>
                            </div>
                            {{/each}} {{/if}}
                        </div>
                    {{/if}}
                {{/if}}
            </td>
        {{#if IsUsingSellerFees}}
            <td class="col-responsible">
                {{#if (pmts != null && pmts.length > 0)}}
                    {{#if (pmts.length < 2)}}
                     <select disabled="true" value="{{pmts[0].responsible}}" data-path-name="ResponsiblePartyT" >
                        <option value="2">seller</option>
                        <option value="3">lender</option>
                        <option value="4">broker</option>
                     </select>
                    {{else}}
                        {{#pmts:k}}
                            <select disabled="true" value="{{pmts[k].responsible}}" >
                                <option value="2">seller</option>
                                <option value="3">lender</option>
                                <option value="4">broker</option>
                             </select>
                        {{/pmts}}
                    {{/if}}
                {{/if}}
            </td>
        {{/if}}
            <td class="col-delete">
                <div class="table">
                    {{#if f.t !== 17 && f.t !== 4 && f.t !== 5 && f.t !== 21 && SectionName.indexOf("G - ") == -1 && f.t !== 20 && typeid !== 'f17c6573-e1bb-447d-ba89-31183be07be8' && (typeid !== <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId) %> || total == '$0.00')}}
                        <div class="closing-cost-div-spacer tooltip-remove-closing-cost">
                            <div>
                                <div class="spacer system-tooltip">
                                    <a on-click="['delete', i,  j, desc, f.t === 3 ? f.base : total, org_desc]" data-toggle="tooltip" data-placement="bottom" title="Remove fee" data-path-name="Remove">
                                        <i class="material-icons">&#xE909;</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    {{/if}}
                </div>
            </td>
        </tr>
    </script>

    <script id="ClosingCostFeeTemplate" type="text/ractive">
        <tr class="{{ (j % 2 == 0) ? 'odd' : 'even'}}" data-id="{{typeid}}">
            <td class="show-hide">
                <div>
                    <a on-click="toggleDetail">details</a>
                    <a on-click="toggleDetail">
                        <i class="material-icons" class-rotate="isShown">&#xE5CC;</i>
                    </a>
                </div>
            </td>
            <td>
                <input type="text" value="{{hudline}}" maxlength="4"
                    data-hud-start="{{HudLineStart}}" data-hud-end="{{HudLineEnd}}" data-is-sub-fee="{{(!!sourceid)}}"
                    class="hudline" style="width:50px;"
                    readonly="{{(is_system && !editableSystem) || sIsRequireFeesFromDropDown}}"
                    data-path-name="HudLine">
            </td>
            <td colspan="3" class="col-description">
                <table class="table-description-memo-paid-to">
                    <tr>
                        <td class="wrap-description-memo">
                            <div class="description-memo flex-display">
                               <div class="textarea-container">
                                    <textarea readonly="{{sIsRequireFeesFromDropDown || f.t === 20 || f.t === 18}}" data-path-name="Description">{{desc}}</textarea>
                                    <div class="textarea-size"></div>
                                </div>
                                {{#if desc != org_desc}}
                                    <div class="label-type">Type: {{org_desc}}</div>
                                {{/if}}
                            </div>
                        </td>
                        <td>
                            <div class="w-57">
                                <div class="select-box select-box-noimage">
                                    <select class="w-57" disabled="{{sIsRequireFeesFromDropDown}}" sectionIntro-in="{hudline, isSystem:is_system, gfeGroup:gfeGrps }" value="{{section}}" data-path-name="GfeSectionT">
                                        <asp:Repeater runat="server" ID="SectionRepeater"><ItemTemplate>
                                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                        </ItemTemplate></asp:Repeater>
                                    </select>
                                </div>
                            </div>
                        </td>
                        <td class="col-paid-to">
                            <div class="w-187">
                                <div class="RactiveDDLContainer wrap-combo">
                                    <select class="RactiveDDL" data-path-name="Beneficiary" on-change="['selectDDL', i, j]">
                                        {{#each Beneficiaries as o}}
                                            <option value="{{o.value}}" {{#if bene == o.value}}selected{{/if}}>{{o.key}}</option>
                                        {{/each}}
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class-hidden="!isShown">
                        <td colspan="3">
                            <div class="table table-checkboxes">
                                <div>
                                    <div>
                                        <label>APR</label>
                                        <input type="checkbox" checked="{{apr}}" disabled="{{sIsRequireFeesFromDropDown}}" readonly="{{sIsRequireFeesFromDropDown}}" data-path-name="IsApr" NoLabel>
                                    </div>
                                    <div>
                                        <label>FHA</label>
                                    <input type="checkbox" checked="{{fha}}" disabled="{{sIsRequireFeesFromDropDown}}" readonly="{{sIsRequireFeesFromDropDown}}" data-path-name="IsFhaAllowable" NoLabel>
                                    </div>
                                    {{#if CanReadDFLP}}
                                        <div>
                                            <label>DFLP</label>
                                            <input type="checkbox" disabled="{{!CanSetDFLP}}" checked="{{dflp}}" data-path-name="Dflp" NoLabel/>
                                        </div>
                                    {{/if}}
                                    <div>
                                        <label>TP</label>
                                        <input type="checkbox" checked="{{tp}}" disabled = "{{!sIsManuallySetThirdPartyAffiliateProps && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>')}}" on-click="['tpClick', i, j]" data-path-name="IsThirdParty" NoLabel>
                                    </div>
                                    <div>
                                        <label>AFF</label>
                                    <input type="checkbox" checked="{{aff}}" disabled = "{{!tp || (!sIsManuallySetThirdPartyAffiliateProps && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>'))}}" data-path-name="IsAffiliate" NoLabel>
                                    </div>
                                    <div>
                                        <label>Can Shop</label>
                                        <input type="checkbox" checked="{{can_shop}}" on-change="['CanShopChange', i, j]" disabled="{{sIsRequireFeesFromDropDown || disc_sect == 2 || disc_sect == 3}}" data-path-name="CanShop" NoLabel>
                                    </div>
                                    <div>
                                        <label>Did Shop</label>
                                        <input type="checkbox" checked="{{did_shop}}" disabled="{{!can_shop}}" data-path-name="DidShop" NoLabel>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <div class="w-22"><div class="table">
                    {{#if SectionName.indexOf("1000") == -1 && f.t !== 0 && f.t !== 4 && f.t !== 5 && f.t !== 14 && f.t !== 17 && f.t !== 18 && f.t!== 19 && f.t !== 20 && f.t !== 21}}
                        <div>
                            <div>
                                <a on-click="['updateCalc', i, j]" class="calculator-svg" data-path-name="TotalAmount"><!-- #include file="~/images/Calculator.svg" --></a>
                            </div>
                        </div>
                    {{/if}}
                </div></div>
            </td>
            <td class="Payments">
                <div class="table">
                    <div {{#if (pmts && pmts.length == 1)}} data-id="{{pmts[0].id}}" data-number="0" {{/if}}>
                        <div>
                            {{#if ( pmts && pmts.length > 1 && isShown)}}
                                <div class="payment-branch">&nbsp;</div>
                            {{/if}}
                            {{#if f.t === 3}}
                                <input type="text" value="{{f.base}}" class="mask money" preset="money" numericTransition-in="true" data-path-name="TotalAmount">
                            {{else}}
                                <input type="text" value="{{total}}" class="money" preset="money"  disabled="disabled" data-path-name="TotalAmount">
                            {{/if}}
                        </div>
                    </div>
                    {{#if (pmts && pmts.length > 1)}}
                        {{#each pmts:k}}
                            <div class-hidden="!isShown" data-id="{{id}}" data-number="{{k}}">
                                <div>
                                    {{#if k < pmts.length - 1}}<div class="payment-branch">&nbsp;</div>{{/if}}
                                    <hr class="split-payment-branch"/>
                                    <input readonly="{{is_system || mipFinancedPmt || cashPdPmt}}" type="text" value="{{amt}}" class="mask money" numericTransition-in="true" data-path-name="TotalAmount"/>
                                </div>
                            </div>
                        {{/each}}

                        {{#each pmts}}
                            {{#if (is_system && !mipFinancedPmt && !cashPdPmt)}}
                                <div class-hidden="!isShown">
                                    <div class="system-tooltip">
                                        <a on-click="['AddSplitPayment', i, j]" class="add-split-pmt" data-toggle="tooltip" data-placement="bottom" title="Add split payment">
                                            <i class="material-icons">&#xE146;</i>
                                        </a>
                                    </div>
                                </div>
                            {{/if}}
                        {{/each}}
                    {{/if}}
                </div>
            </td>
            <td class="Payments">
                {{#if (pmts != null && pmts.length > 0)}}
                    {{#if (pmts.length < 2)}}
                        <div class="select-box select-box-noimage" data-id="{{pmts[0].id}}" data-number="0">
                        <select value="{{pmts[0].paid_by}}" on-change="['updatePaidBy', i, j, total]" data-path-name="PaidByT">
                            {{#if sClosingCostFeeVersionT == 2 && (f.t != 5 && f.t != 21) && !IsUsingSellerFees}}
                                <option value="-1">--split--</option>
                            {{/if}}
                            {{#if !IsUsingSellerFees}}
                            <option value="1">borr pd</option>
                            {{/if}}
                            <option value="2">seller</option>
                            {{#if !IsUsingSellerFees}}
                            <option value="3">borr fin</option>
                            {{/if}}
                            {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                                <option value="4">lender</option>
                            {{/if}}
                            <option value="5">broker</option>
                        </select></div>
                    {{else}}
                        <div class="table">
                            <div class="closing-cost-div-spacer">
                                <div>
                                    {{#if isShown}}
                                        <div class="spacer split-title">split</div>
                                    {{else}}
                                        <div class="spacer split-caption">split <br>(see details)</div>
                                    {{/if}}
                                </div>
                            </div>
                            {{#if isShown}} {{#each pmts:k}}
                                <div data-id="{{id}}" data-number="{{k}}">
                                    <div class="select-box select-box-noimage">
                                        <select value="{{paid_by}}" on-change="updatePaidBySplit" disabled="{{is_system || mipFinancedPmt}}" data-path-name="PaidByT">
                                            {{#if !IsUsingSellerFees}}
                                                <option value="1">borr pd</option>
                                            {{/if}}
                                            <option value="2">seller</option>
                                            {{#if !cashPdPmt && !IsUsingSellerFees}}
                                                <option value="3">borr fin</option>
                                            {{/if}}
                                            {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                                                <option value="4">lender</option>
                                            {{/if}}
                                            <option value="5">broker</option>
                                        </select>
                                    </div>
                                </div>
                            {{/each}} {{/if}}
                        </div>
                    {{/if}}
                {{/if}}
            </td>
            <td class="Payments">
                {{#if (pmts != null && pmts.length > 0)}}
                    {{#if (pmts.length < 2)}}
                        <div class="select-box select-box-noimage" data-id="{{pmts[0].id}}" data-number="0">
                        <select disabled="{{pmts[0].paid_by == 3}}" NotEditable="true" value="{{pmts[0].pmt_at}}" on-change="recalc" data-path-name="GfeClosingCostFeePaymentTimingT">
                            <option value="1">at closing</option>
                            <option value="2">outside of closing</option>
                        </select></div>
                    {{else}}
                        <div class="table">
                            <div class="closing-cost-div-spacer">
                                <div>
                                    <div class="spacer">&nbsp;</div>
                                </div>
                            </div>
                            {{#if (isShown)}} {{#each pmts:k}}
                                <div data-id="{{id}}" data-number="{{k}}">
                                    <div class="select-box select-box-noimage">
                                    <select value="{{pmt_at}}" disabled="{{is_system || paid_by == 3 || mipFinancedPmt}}" on-change="recalc" data-path-name="GfeClosingCostFeePaymentTimingT">
                                        <option value="1">at closing</option>
                                        <option value="2">outside of closing</option>
                                    </select>
                                    </div>
                                </div>
                            {{/each}} {{/if}}
                        </div>
                    {{/if}}
                {{/if}}
            </td>
            <td class="col-date-paid payments">
                {{#if (pmts != null && pmts.length > 0)}}
                    {{#if (pmts.length < 2)}}
                        <div data-id="{{pmts[0].id}}" data-number="0">
                            <input type="text" preset="date" class="mask date" id="d{{i}}{{j}}" datepickerTransition-in recalc value="{{pmts[0].pmt_dt}}" readonly="{{pmts[0].pmt_at == 1}}" data-path-name="PaymentDate"/>
                        </div>
                    {{else}}
                        <div class-hidden="!isShown">
                            <div class="closing-cost-div-spacer"><div><div class="spacer">&nbsp;</div></div></div>
                            {{#pmts:k}}
                            <div class-hidden="!isShown" data-id="{{id}}" data-number="{{k}}">
                                <div class="nowrap system-tooltip">
                                    <input type="text" preset="date" class="mask date" id="d{{i}}{{j}}{{k}}" datepickerTransition-in recalc value="{{pmt_dt}}" readonly="{{is_system || pmt_at == 1}}" data-path-name="PaymentDate"/>

                                    {{#if !is_system && !mipFinancedPmt && !cashPdPmt}}
                                        <a
                                            class="split-payment-link"
                                            on-click="['removePayment', i, j, k, amt, paid_by, desc, org_desc]"
                                            data-toggle="tooltip"
                                            data-placement="bottom"
                                            title="Remove split payment">
                                            <i class="material-icons">&#xE909;</i>
                                        </a>
                                    {{/if}}
                                </div>
                            </div>
                            {{/pmts}}
                        </div>
                    {{/if}}
                {{/if}}
            </td>
        {{#if IsUsingSellerFees}}
            <td class="col-responsible">
                {{#if (pmts != null && pmts.length > 0)}}
                    {{#if (pmts.length < 2)}}
                     <select disabled="true" value="{{pmts[0].responsible}}" data-path-name="ResponsiblePartyT">
                        <option value="2">seller</option>
                        <option value="3">lender</option>
                        <option value="4">broker</option>
                     </select>
                    {{else}}
                        {{#pmts:k}}
                            <select disabled="true" value="{{pmts[k].responsible}}" >
                                <option value="2">seller</option>
                                <option value="3">lender</option>
                                <option value="4">broker</option>
                             </select>
                        {{/pmts}}
                    {{/if}}
                {{/if}}
            </td>
        {{/if}}
            <td class="col-delete">
                <div class="table">
                    {{#if f.t !== 17 && f.t !== 4 && f.t !== 5 && f.t !== 21 && SectionName.indexOf("1000") == -1 && f.t !== 20 && typeid !== 'f17c6573-e1bb-447d-ba89-31183be07be8' && (typeid !== <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId) %> || total == '$0.00')}}
                        <div class="closing-cost-div-spacer tooltip-remove-closing-cost">
                            <div>
                                <div class="spacer system-tooltip">
                                    <a on-click="['delete',  i, j, desc, f.t === 3 ? f.base : total, org_desc]" data-toggle="tooltip" data-placement="bottom" title="Remove fee" data-path-name="Remove">
                                        <i class="material-icons">&#xE909;</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    {{/if}}
                </div>
            </td>
        </tr>
    </script>

    <script id="ClosingCostTemplate" type="text/ractive">
        {{#if (sDisclosureRegulationT == 0 || sDisclosureRegulationT == 1)}}
            <div id="sec-closingcost" class="div-main LenderTypeHud1 {{IsUsingSellerFees ? 'sellerclosingcostset' : 'borrowerclosingcostset'}}">
                {{! Ractive did not enjoy having the Seller-Responsible fees replace the Borrower-Responsible fees directly.  To work around that, a separate section was created for
                Seller-Responsible fees, even though the sections have the exact same structure. }}

                {{#if IsUsingSellerFees}}
                    {{#SellerList:i}}
                    <header class="header">{{SectionName}}</header>

                    <table border="0" cellpadding="2" cellspacing="0" class="{{HudLineStart}} primary-table table-closingcost none-hover-table none-selected-table wide" >
                         {{#if ClosingCostFeeList.length > 0}}
                            <thead class="header">
                                <tr>
                                    <th></th>
                                    <th>HUD</th>
                                    <th colspan="3" class="col-description">
                                        <table class="table-description-memo-paid-to">
                                            <tr>
                                                <td class="wrap-description-memo">
                                                    <div class="description-memo">Description/Memo</div>
                                                </td>
                                                <td>
                                                    <div class="w-57">GFE Box</div>
                                                </td>
                                                <td class="col-paid-to">
                                                    <div class="w-187">Paid to</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                    <th>

                                    </th>
                                    <th>
                                        Amount
                                    </th>
                                    <th>
                                        Paid by
                                    </th>
                                    <th>
                                        Payable
                                    </th>
                                    <th class="col-date-paid">
                                        Date paid
                                    </th>
                                    <th class="allow-wrap">Responsible party</th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>

                               {{/if}}
                            <tbody>
                                {{#ClosingCostFeeList:j}}
                                    {{>ClosingCostFeeTemplate}}
                                {{/ClosingCostFeeList}}
                            </tbody>
                            {{#if SectionName.indexOf("1000") == -1}}
                            <tfooter>
                                <tr>
                                    <td>
                                    <div class="div-image system-tooltip">
                                            <a on-click="['addFee', i]" data-toggle="tooltip" data-placement="bottom" class='AddFeeToSection' title="Add new fee" data-path-name="Add">
                                                <i class="material-icons">&#xE146;</i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </tfooter>
                            {{/if}}
                    </table>
                    {{/SellerList}}
                {{/if}}
                {{#if !IsUsingSellerFees}}
                     {{#SectionList:i}}
                    <header class="header">{{SectionName}}</header>

                    <table border="0" cellpadding="2" cellspacing="0" class="{{HudLineStart}} primary-table table-closingcost none-hover-table none-selected-table wide" >
                     {{#if ClosingCostFeeList.length > 0}}
                        <thead class="header">
                            <tr>
                                <th></th>
                                <th>HUD</th>
                                <th colspan="3" class="col-description">
                                    <table class="table-description-memo-paid-to">
                                        <tr>
                                            <td class="wrap-description-memo">
                                                <div class="description-memo">Description/Memo</div>
                                            </td>
                                            <td>
                                                <div class="w-57">GFE Box</div>
                                            </td>
                                            <td class="col-paid-to">
                                                <div class="w-187">Paid to</div>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                                <th>

                                </th>
                                <th>
                                    Amount
                                </th>
                                <th>
                                    Paid by
                                </th>
                                <th>
                                    Payable
                                </th>
                                <th class="col-date-paid">
                                    Date paid
                                </th>
                                <th>

                                </th>
                            </tr>
                        </thead>

                           {{/if}}
                        <tbody>
                            {{#ClosingCostFeeList:j}}
                                {{>ClosingCostFeeTemplate}}
                            {{/ClosingCostFeeList}}
                        </tbody>
                        {{#if SectionName.indexOf("1000") == -1}}
                        <tfooter>
                            <tr>
                                <td>
                                <div class="div-image system-tooltip">
                                        <a on-click="['addFee', i]" data-toggle="tooltip" data-placement="bottom" class='AddFeeToSection' title="Add new fee" data-path-name="Add">
                                            <i class="material-icons">&#xE146;</i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tfooter>
                        {{/if}}
                    </table>
                    {{/SectionList}}
                {{/if}}
            </div>
        {{elseif (sDisclosureRegulationT == 2)}}
            <div id="sec-closingcost8" class="div-main LenderTypeEstimate {{IsUsingSellerFees ? 'sellerclosingcostset' : 'borrowerclosingcostset'}}">
            {{#if IsUsingSellerFees}}
                {{#each SellerList:i}}
                    <header class="header">{{SectionName}}</header>
                    <table border="0" cellpadding="2" cellspacing="0" class="{{SectionTypeRep}} primary-table table-closingcost none-hover-table none-selected-table wide" >
                        {{#if (ClosingCostFeeList.length > 0)}}
                            <thead class="header">
                                <tr>
                                    <th></th>
                                    <th colspan="2" class="col-description">
                                        <table class="table-description-memo-paid-to">
                                            <tr>
                                                <td>
                                                    <div class="{{IsUsingSellerFees ? "description-col" : "w-183"}}">Description/Memo</div>
                                                </td>
                                                <td>
                                                    <div class="w-240">Paid to</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                    <th>

                                    </th>
                                    <th>
                                        Amount
                                    </th>
                                    <th>
                                        Paid by
                                    </th>
                                    <th>
                                        Payable
                                    </th>
                                    <th class="col-date-paid">
                                        Date paid
                                    </th>
                                    {{#if IsUsingSellerFees}}
                                    <th class="allow-wrap">
                                        Responsible Party
                                    </th>
                                    {{/if}}
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                        {{/if}}

                        <tbody>
                            {{#ClosingCostFeeList:j}}
                                {{>ClosingCostSectionFeeTemplate}}
                            {{/ClosingCostFeeList}}
                        </tbody>

                        {{#if (SectionName.indexOf("G - ") == -1)}}
                            <tfooter>
                                <tr>
                                    <td>
                                        <div class="div-image system-tooltip">
                                            <a on-click="['addFee', i]" data-toggle="tooltip" data-placement="bottom" class='AddFeeToSection' title="Add new fee" data-path-name="Add">
                                                <i class="material-icons">&#xE146;</i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </tfooter>
                        {{/if}}
                    </table>
                {{/each}}
            {{else}}
                {{#each SectionList:i}}
                    <header class="header">{{SectionName}}</header>
                    <table border="0" cellpadding="2" cellspacing="0" class="{{SectionTypeRep}} primary-table table-closingcost none-hover-table none-selected-table wide">
                        {{#if (ClosingCostFeeList.length > 0)}}
                            <thead class="header">
                                <tr>
                                    <th></th>
                                    <th colspan="2" class="col-description">
                                        <table class="table-description-memo-paid-to">
                                            <tr>
                                                <td>
                                                    <div class="{{IsUsingSellerFees ? "description-col" : "w-183"}}">Description/Memo</div>
                                                </td>
                                                <td>
                                                    <div class="w-240">Paid to</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                    <th></th>
                                    <th>Amount</th>
                                    <th>Paid by</th>
                                    <th>Payable</th>
                                    <th class="col-date-paid">Date paid</th>
                                    <th></th>
                                </tr>
                            </thead>
                        {{/if}}

                        <tbody>
                            {{#ClosingCostFeeList:j}}
                                {{>ClosingCostSectionFeeTemplate}}
                            {{/ClosingCostFeeList}}
                        </tbody>

                        {{#if (SectionName.indexOf("G - ") == -1)}}
                        <tfooter>
                            <tr>
                                <td>
                                    <div class="div-image system-tooltip">
                                        <a on-click="addFee" data-toggle="tooltip" data-placement="bottom" class='AddFeeToSection' title="Add new fee" data-path-name="Add">
                                            <i class="material-icons">&#xE146;</i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tfooter>
                        {{/if}}
                    </table>
                {{/each}}
            {{/if}}
            </div>
        {{/if}}

        <asp:Placeholder id="QMContent" runat="server">
            <div id="sec-calculation" class="div-main">
                <div class="flex-space-between padding-bottom-p8">
                    <div>
                        <div>
                            <label>Borrower Rate Lock Date</label>
                            <input type="text" recalc  preset="date" readonly="{{!sRLckdDLck}}" value="{{sRLckdD}}" ID="sRLckdD">
                            <label class="lock-checkbox"><input type="checkbox" recalc checked="{{sRLckdDLck}}" ID="sRLckdDLck"/></label>
                        </div>
                         <div>
                            <label>QM DTI</label>
                            <input type="text" readonly  preset="percent" value="{{sQMQualBottom}}" ID="sQMQualBottom">
                        </div>
                        <div>
                            <label>FHA UFMIP at Closing</label>
                            <input type="text" recalc value="{{sQMFhaUfmipAtClosing}}" readonly="{{!sQMFhaUfmipAtClosingLck}}"  preset="money" ID="sQMFhaUfmipAtClosing">
                            <label class="lock-checkbox"><input recalc checked="{{sQMFhaUfmipAtClosingLck}}" type="checkbox"  ID="sQMFhaUfmipAtClosingLck"/></label>
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>QM Qual Rate</label>
                            <input type="text" readonly value="{{sQMQualR}}" preset="percent" ID="sQMQualR">
                        </div>
                        <div>
                            <label>Non-Excludable Discount Points</label>
                            <input type="text" recalc preset="percent" value="{{sQMNonExcludableDiscountPointsPc}}" ID="sQMNonExcludableDiscountPointsPc">
                        </div>
                        <div>
                            <label>Par Rate</label>
                            <input recalc type="text" value="{{sQMParR}}" preset="percent" ID="sQMParR">
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>Excludable Discount Points</label>
                            <input type="text" recalc value="{{sQMExclDiscntPnt}}" readonly="{{!sQMExclDiscntPntLck}}" preset="percent" ID="sQMExclDiscntPnt">
                            <label class="lock-checkbox"><input type="checkbox" recalc checked="{{sQMExclDiscntPntLck}}" ID="sQMExclDiscntPntLck"/></label>
                        </div>
                        <div>
                            <label>APOR</label>
                            <input type="text" recalc value="{{sQMAveragePrimeOfferR}}" readonly="{{!sQMAveragePrimeOfferRLck}}" preset="percent" ID="sQMAveragePrimeOfferR">
                            <label class="lock-checkbox"><input checked="{{sQMAveragePrimeOfferRLck}}" recalc type="checkbox"  ID="sQMAveragePrimeOfferRLck"/></label>
                        </div>
                        <div>
                            <label>Financed QM Fee Amount</label>
                            <input readonly type="text" value="{{sQMTotFinFeeAmount}}" preset="money" ID="sQMTotFinFeeAmount">
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>Loan Closing Date</label>
                            <input type="text" recalc value="{{sDocMagicClosingD}}" readonly="{{!sDocMagicClosingDLck}}" preset="date" ID="sDocMagicClosingD">
                            <label class="lock-checkbox"><input type="checkbox" recalc checked="{{sDocMagicClosingDLck}}" ID="sDocMagicClosingDLck"/></label>
                        </div>
                        <div>
                            <label>Total QM Fee Amount</label>
                            <input readonly type="text" value="{{sQMTotFeeAmount}}"  preset="money" ID="sQMTotFeeAmount">
                        </div>
                        <div>
                            <label>Maximum Prepayment Penalty</label>
                            <input recalc type="text" value="{{sQMMaxPrePmntPenalty}}" preset="money" ID="sQMMaxPrePmntPenalty">
                        </div>
                    </div>
                </div>
                <div></div>

                <div class="grey-box-border">
                    <header class="header header-border header-border-rate-lock">Intermediate Calculations</header>
                    <table class="table-calculation-2 primary-table wide none-hover-table">
                            <thead class="header">
                                <tr>
                                    <th>Description</th>
                                    <th>Value</th>
                                    <th>Calculation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>APR Rate Spread</td>
                                    <td><label  ID="sQMAprRSpread">{{sQMAprRSpread}}</label></td>
                                    <td><label  ID="sQMAprRSpreadCalcDesc">{{sQMAprRSpreadCalcDesc}}</label></td>
                                </tr>
                                <tr>
                                    <td>Par Rate Spread</td>
                                    <td><label  ID="sQMParRSpread">{{sQMParRSpread}}</label></td>
                                    <td><label  ID="sQMParRSpreadCalcDesc">{{sQMParRSpreadCalcDesc}}</label></td>
                                </tr>
                                <tr>
                                    <td>Excess Upfront MIP</td>
                                    <td><label  ID="sQMExcessUpfrontMIP">{{sQMExcessUpfrontMIP}}</label></td>
                                    <td><label  ID="sQMExcessUpfrontMIPCalcDesc">{{sQMExcessUpfrontMIPCalcDesc}}</label></td>
                                </tr>
                                <tr>
                                    <td>Excess Discount Points</td>
                                    <td><label  ID="sQMExcessDiscntFPc">{{sQMExcessDiscntFPc}}</label></td>
                                    <td><label  ID="sQMExcessDiscntFCalcDesc">{{{sQMExcessDiscntFCalcDesc}}}</label></td>
                                </tr>
                                <tr>
                                    <td>Discount Buydown Rate</td>
                                    <td><label  ID="sQMDiscntBuyDownR">{{sQMDiscntBuyDownR}}</label></td>
                                    <td><label  ID="sQMDiscntBuyDownRCalcDesc">{{sQMDiscntBuyDownRCalcDesc}}</label></td>
                                </tr>
                                <tr>
                                    <td>QM Total Loan Amount</td>
                                    <td><label  ID="sQMLAmt">{{sQMLAmt}}</label></td>
                                    <td><label  ID="sQMLAmtCalcDesc">{{sQMLAmtCalcDesc}}</label></td>
                                </tr>
                            </tbody>
                        </table>
                </div>
                <div class="flex-grid">
                    <div class="qm-points-fee none-margin">
                        <!--Left-->
                        <div class="grey-box-border">
                            <header class="header header-border header-border-rate-lock">QM Points and Fees Calculation</header>
                            <table class="table-calculation-2 primary-table wide none-hover-table">
                                <thead class="header">
                                    <tr>
                                        <th>Description</th>
                                        <th></th>
                                        <th class="text-right">Amount</th>
                                        <th class="text-right">Percent</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>QM Total Loan Amount</td>
                                        <td></td>
                                        <td class="text-right">
                                            <label  ID="Calculations_sQMLAmt_1">{{sQMLAmt}}</label>
                                        </td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Allowable QM Points and Fees</td>
                                        <td></td>
                                        <td class="text-right">
                                            <label  ID="sQMMaxPointAndFeesAllowedAmt">{{sQMMaxPointAndFeesAllowedAmt}}</label>
                                        </td>
                                        <td class="text-right">
                                            <label  ID="sQMMaxPointAndFeesAllowedPc">{{sQMMaxPointAndFeesAllowedPc}}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td-empty"></td>
                                        <td></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Total QM Fee Amount</td>
                                        <td></td>
                                        <td class="text-right">
                                           <label  ID="sQMTotFeeAmount_1">{{sQMTotFeeAmount}}</label>
                                        </td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Origination Compensation</td>
                                        <td>+</td>
                                        <td class="text-right">
                                           <label  ID="sGfeOriginatorCompF">{{sGfeOriginatorCompF}}</label>
                                        </td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Excess Discount Points</td>
                                        <td>+</td>
                                        <td class="text-right">
                                           <label  ID="sQMExcessDiscntF">{{sQMExcessDiscntF}}</label>
                                        </td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Excess Upfront MIP</td>
                                        <td>+</td>
                                        <td class="text-right">
                                           <label  ID="sQMExcessUpfrontMIP_1">{{sQMExcessUpfrontMIP}}</label>
                                        </td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Maximum Prepayment Penalty</td>
                                        <td>+</td>
                                        <td class="text-right">
                                           <label  ID="sQMMaxPrePmntPenalty_1">{{sQMMaxPrePmntPenalty}}</label>
                                        </td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Total QM Points and Fees</td>
                                        <td></td>
                                        <td class="text-right">
                                           <label  ID="sQMTotFeeAmt">{{sQMTotFeeAmt}}</label>
                                        </td>
                                        <td class="text-right">
                                            <label  ID="sQMTotFeePc">{{sQMTotFeePc}}</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="lists-qm-page">
                        <!--Right-->
                        <div>
                            <span><img src="{{sQMLoanPassesPointAndFeesTest? '../images/yes.png' : '../images/no.png'}}"/></span>
                            <input checked="{{sQMLoanPassesPointAndFeesTest}}" disabled type="checkbox"  ID="sQMLoanPassesPointAndFeesTest" Checked="True" Class="checkbox-hidden"/>
                            Meets points and fees test
                        </div>
                        <div>
                            <span><img src="{{sQMLoanDoesNotHaveNegativeAmort? '../images/yes.png' : '../images/no.png'}}"/></span>
                            <input checked="{{sQMLoanDoesNotHaveNegativeAmort}}" disabled type="checkbox"  ID="sQMLoanDoesNotHaveNegativeAmort" Checked="True" Class="checkbox-hidden"/>
                            Does not have negative amortization or interest-only features
                        </div>
                        <div>
                            <span><img src="{{sQMLoanDoesNotHaveBalloonFeature? '../images/yes.png' : '../images/no.png'}}"/></span>
                            <input checked="{{sQMLoanDoesNotHaveBalloonFeature}}" disabled type="checkbox"  ID="sQMLoanDoesNotHaveBalloonFeature" Checked="True" Class="checkbox-hidden"/>
                            Does not have a balloon feature
                        </div>
                        <div>
                            <span><img src="{{sQMLoanDoesNotHaveAmortTermOver30Yr? '../images/yes.png' : '../images/no.png'}}"/></span>
                            <input checked="{{sQMLoanDoesNotHaveAmortTermOver30Yr}}" disabled type="checkbox"  ID="sQMLoanDoesNotHaveAmortTermOver30Yr" Checked="True" Class="checkbox-hidden"/>
                            Does not have an amortization term in excess of 30 years
                        </div>
                        <div>
                            <span><img src="{{sQMLoanHasDtiLessThan43Pc? '../images/yes.png' : '../images/no.png'}}"/></span>
                            <input checked="{{sQMLoanHasDtiLessThan43Pc}}" disabled type="checkbox"  ID="sQMLoanHasDtiLessThan43Pc" Checked="False" Class="checkbox-hidden"/>
                            Has QM DTI less than 43%
                        </div>
                        <div>
                            <input recalc checked="{{sQMIsVerifiedIncomeAndAssets}}" type="checkbox"  ID="sQMIsVerifiedIncomeAndAssets"/>
                            <label for="sQMIsVerifiedIncomeAndAssets">Used verified income and assets</label>
                        </div>
                        <div>
                            <input recalc checked="{{sQMIsEligibleByLoanPurchaseAgency}}" type="checkbox"  ID="sQMIsEligibleByLoanPurchaseAgency"/>
                            <label for="sQMIsEligibleByLoanPurchaseAgency">Eligible for purchase or guarantee by a qualifying agency. </label>
                            <div class="select-box select-box-noimage">
                                <select recalc value="{{sQMLoanPurchaseAgency}}">
                                    <asp:Repeater runat="server" ID="sQMLoanPurchaseAgencyRepeater"><ItemTemplate>
                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                    </ItemTemplate></asp:Repeater>
                                </select>
                            </div>
                        </div>
                        <div class="hr-qm"><hr/></div>
                        <div>
                            <div class="table table-inline">
                                <div>
                                    <div>
                                        <label>QM Status</label>
                                        <div class="select-box select-box-noimage">
                                            <select recalc class="QMDDL" value="{{sQMStatusT}}" disabled="disabled">
                                                <asp:Repeater runat="server" ID="sQMStatusTRepeater"><ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate></asp:Repeater>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="higher-priced-indicator">
                                        <label>Higher Priced Indicator</label>
                                        <div class="select-box select-box-noimage">
                                            <select recalc class="QMDDL" disabled="{{!sHighPricedMortgageTLckd}}" value="{{sHighPricedMortgageT}}">
                                                <asp:Repeater runat="server" ID="sHighPricedMortgageTRepeater"><ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate></asp:Repeater>
                                            </select>
                                        </div>
                                        <label class="lock-checkbox"><input recalc checked="{{sHighPricedMortgageTLckd}}" type="checkbox"  ID="sHighPricedMortgageTLckd"/></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Placeholder>

        <asp:Placeholder id="HousingExpenseTab" runat="server">
            <div id="sec-housing">
                <div class="flex-space-between flex-min-w-non-pi">
                    <div>
                        <div>
                            <label>Estimated Closing Date</label>
                            <input type="text" readonly="{{!sEstCloseDLckd}}" preset="date" class="mask date" id="sEstCloseD" datepickerTransition-in
                               recalc value="{{sEstCloseD}}">
                            <label class="lock-checkbox"><input recalc checked="{{sEstCloseDLckd}}" type="checkbox" id="sEstCloseDLckd"></label>
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>1st Payment Date</label>
                            <input type="text" readonly="{{!sSchedDueD1Lckd}}" preset="date" class="mask date"
                                id="sSchedDueD1" datepickerTransition-in recalc value="{{sSchedDueD1}}">
                            <label class="lock-checkbox"><input recalc checked="{{sSchedDueD1Lckd}}" type="checkbox" id="sSchedDueD1Lckd"></label>
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>Aggregate Adjustment</label>
                            <input recalc type="text" value="{{sAggregateAdjRsrv}}" readonly="{{!sAggregateAdjRsrvLckd}}"
                                preset="money" id="sAggregateAdjRsrv" class="input=w100"/>
                            <label class="lock-checkbox"><input recalc checked="{{sAggregateAdjRsrvLckd}}" type="checkbox" id="sAggregateAdjRsrvLckd"></label>
                            <div class="padding-top-p8">
                                {{#if sSchedDueD1 == ""}}
                                <div class="text-danger">Note: Aggregate Adjustment calculation requires a 1st Payment Date</div>
                                {{/if}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>Total Escrow Collected at Closing</label>
                            <input type="text" value="{{sGfeInitialImpoundDeposit}}" readonly="true" preset="money"
                                id="sGfeInitialImpoundDeposit">
                        </div>
                    </div>
                </div>

                <div class="div-title" id="mi_title">
                    <a id="mi_header" class="HousingHeader" on-click="['HousingHeaderClick', 'mi_title']">
                        <div class="div-title-housing">
                            <div class="housing-title-1st">
                                <i class="material-icons">&#xE5CC;</i> Mortgage Insurance
                            </div>
                            <div class="housing-title-2nd" id="MIdispMonAmt">{{sProMIns}}/month</div>
                            <div class="housing-title-3rd" id="MIdispEscrowed">{{#if sMInsRsrvEscrowedTri}}Escrowed{{else}}Not Escrowed{{/if}}</div>
                        </div>
                    </a>
                </div>
                <div class="housing-content" id="mi_content">
                    <div class="d-flex padding-top-p8">
                        <div class="d-flex-left">
                            <div class="div-housing-border">
                                <header class="header header-border header-border-rate-lock">Borrower Paid Upfront MIP/FF</header>
                                <div class="table table-padding-right-p6-closing-costs table-aligment-disclosures">
                                    <div>
                                        <div class="min-w-328">
                                            <span class="total-label">Total UFMIP/FF</span>
                                            <input value="{{sFfUfmipR}}" recalc type="text" id="sFfUfmipR" preset="percent" decimaldigits="6" class="input-w100"/>
                                        </div>
                                        <div>
                                            <input recalc type="text" id="sFfUfmip1003" preset="money" class="input-w100"
                                                value="{{sFfUfmip1003}}" readonly="{{!sFfUfmip1003Lckd}}">
                                            <label class="lock-checkbox"><input recalc type="checkbox" id="sFfUfmip1003Lckd" checked="{{sFfUfmip1003Lckd}}"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="table table-padding-right-p6-closing-costs">
                                    <div>
                                        <div>
                                            <input recalc id="sMipFrequency_0" type="radio" name="{{sMipFrequency}}" value="0">
                                            <label for="sMipFrequency_0">Simple Payment</label>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <input recalc id="sMipFrequency_1" type="radio" name="{{sMipFrequency}}" value="1">
                                            <label for="sMipFrequency_1">Annual rate for</label>
                                        </div>
                                        <div>
                                            <input recalc type="text" readonly="{{sMipFrequency == 0}}" class="input-w30" preset="numeric"  id="sMipPiaMon" value="{{sMipPiaMon}}"> months
                                        </div>
                                    </div>
                                </div>
                                <div class="table table-inline table-is-UFMIP-FF table-none-padding-bottom table-aligment-disclosures">
                                    <div>
                                        <div class="min-w-328"><label>Is UFMIP/FF being financed?</label></div>
                                        <div>
                                            <input recalc id="sFfUfMipIsBeingFinanced_0" type="radio" name="{{sFfUfMipIsBeingFinanced}}" value="1">
                                            <label for="sFfUfMipIsBeingFinanced_0">Yes</label>
                                            <input recalc id="sFfUfMipIsBeingFinanced_1" type="radio" name="{{sFfUfMipIsBeingFinanced}}" value="0">
                                            <label for="sFfUfMipIsBeingFinanced_1">No</label>
                                        </div>
                                    </div>
                                    <div class="min-w-328">
                                        <div><label>Is UFMIP/FF financed amount</label></div>
                                        <div><input type="text" readonly="true" id="sFfUfmipFinanced" preset="money" class="input-w100"
                                                value="{{sFfUfmipFinanced}}"></div>
                                    </div>
                                    <div class="min-w-328">
                                        <div><label>UFMIP/FF paid in cash amount</label></div>
                                        <div>
                                            <input recalc type="text" value="{{sUfCashPd}}" preset="money" id="sUfCashPd" readonly="{{!sUfCashPdLckd}}">
                                            <label class="lock-checkbox" class-disabled="sLt == 1 || !sFfUfMipIsBeingFinanced"><input recalc type="checkbox" id="sUfCashPdLckd" checked="{{sUfCashPdLckd}}" disabled="{{sLt == 1 || !sFfUfMipIsBeingFinanced}}"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="div-housing-border">
                                <header class="header header-border header-border-rate-lock">Borrower Paid Monthly MIP Renewal and Cancellation</header>
                                <div class="table table-inline  table-none-padding-bottom table-aligment-disclosures">
                                    <div>
                                        <div class="min-w-328"><label>Term of initial rate</label></div>
                                        <div>
                                            <input type="text" recalc id="sProMInsMon" class="input-w30" value="{{sProMInsMon}}" preset="numeric">
                                            months
                                        </div>
                                    </div>
                                    <div>
                                        <div class="min-w-328"><label>Term of renewal annual rate</label></div>
                                        <div>
                                            <input recalc type="text" id="sProMIns2Mon" class="input-w30" value="{{sProMIns2Mon}}" preset="numeric">
                                            months
                                        </div>
                                    </div>
                                    <div>
                                        <div class="min-w-328"><label>Renewal annual premium rate</label></div>
                                        <div>
                                            <input type="text" recalc id="sProMInsR2" preset="percent" value="{{sProMInsR2}}" decimaldigits="6" class="input-w100">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="min-w-328"><label>Renewal monthly amount</label></div>
                                        <div>
                                            <input type="text" readonly="readonly" id="sProMIns2" value="{{sProMIns2}}" preset="money" class="input-w100">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="min-w-328">
                                            <span>Cancel at LTV</span>
                                            <input recalc type="text" id="sProMInsCancelLtv" value="{{sProMInsCancelLtv}}" preset="percent">
                                        </div>
                                        <div>
                                            <input id="sProMInsMidptCancel" type="checkbox" recalc checked="{{sProMInsMidptCancel}}"><label for="sProMInsMidptCancel">Cancel at midpoint</label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="min-w-328"><label>Minimum number of payments before cancellation</label></div>
                                        <div>
                                            <input recalc type="text" id="sProMInsCancelMinPmts" value="{{sProMInsCancelMinPmts}}" class="input-w30" preset="numeric">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="div-housing-border">
                                <header class="header header-border header-border-rate-lock">Lender Paid Single Premium</header>
                                <div class="table table-padding-right-p6-closing-costs table-none-padding-bottom table-aligment-disclosures">
                                    <div>
                                        <div class="min-w-328">
                                            <span class="total-label">Single Premium</span>
                                            <input recalc type="text" id="sLenderUfmipR" preset="percent" value="{{sLenderUfmipR}}" decimaldigits="6" class="input-w100">
                                        </div>
                                        <div>
                                            <input recalc type="text" readonly="{{!sLenderUfmipLckd}}" id="sLenderUfmip" value="{{sLenderUfmip}}" preset="money" class="input-w100">
                                            <label class="lock-checkbox"><input recalc id="sLenderUfmipLckd" type="checkbox" checked="{{sLenderUfmipLckd}}" /></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex-right">
                            <div class="div-housing-border h-275">
                                <header class="header header-border header-border-rate-lock">Borrower Paid Monthly MIP</header>
                                <div class="table table-inline table-borrower-paid-monthly-mip table-aligment-disclosures">
                                    <div>
                                        <div>
                                            <label>Annual Premium Rate</label>
                                        </div>
                                        <div>
                                            <input recalc type="text" id="sProMInsR" preset="percent" value="{{sProMInsR}}" decimaldigits="6" class="input-w100">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="padding-left-p8">
                                            <span>of</span>
                                            <div class="select-box select-box-noimage">
                                                <select recalc value="{{sProMInsT}}">
                                                    <asp:Repeater runat="server" ID="sProMInsTRepeater"><ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate></asp:Repeater>
                                                </select></div>
                                        </div>
                                        <div>
                                            <input type="text" readonly="readonly" id="sProMInsBaseAmt" value="{{sProMInsBaseAmt}}" preset="money" class="input-w100">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Base monthly premium</label>
                                        </div>
                                        <div>
                                            <input type="text" readonly="readonly" id="sProMInsBaseMonthlyPremium" value="{{sProMInsBaseMonthlyPremium}}" preset="money" class="input-w100">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Monthly premium adjustment</label>
                                        </div>
                                        <div>
                                            <input type="text" recalc id="sProMInsMb" value="{{sProMInsMb}}" preset="money" class="input-w100">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Monthly premium</label>
                                        </div>
                                        <div>
                                            <input recalc type="text" id="sProMIns" value="{{sProMIns}}" readonly="{{!sProMInsLckd}}" preset="money" class="input-w100">
                                            <label class="lock-checkbox"><input id="sProMInsLckd" type="checkbox" checked="{{sProMInsLckd}}"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="div-housing-border h-369">
                                <header class="header header-border header-border-rate-lock">MI Policy Details</header>
                                <div class="table table-inline  table-borrower-paid-monthly-mip table-aligment-disclosures">
                                    <div>
                                        <div>
                                            <label>Mortgage insurance policy provider</label>
                                        </div>
                                        <div>
                                            <div class="select-box select-box-noimage">
                                                <select recalc value="{{sMiCompanyNmT}}">
                                                    <asp:Repeater runat="server" ID="sMiCompanyNmTRepeater"><ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate></asp:Repeater>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Mortgage insurance coverage %</label>
                                        </div>
                                        <div>
                                            <input type="text" recalc id="sMiLenderPaidCoverage" value="{{sMiLenderPaidCoverage}}" preset="percent" decimaldigits="4" style="width: 70px;">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Mortgage insurance type</label>
                                        </div>
                                        <div>
                                            <div class="select-box select-box-noimage">
                                                <select recalc value="{{sMiInsuranceT}}">
                                                    <asp:Repeater runat="server" ID="sMiInsuranceTRepeater"><ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate></asp:Repeater>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Commitment requested date</label>
                                        </div>
                                        <div>
                                            <input type="text" preset="date" class="mask date" id="sMiCommitmentRequestedD" datepickerTransition-in recalc value="{{sMiCommitmentRequestedD}}">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Commitment received date</label>
                                        </div>
                                        <div>
                                            <input type="text" preset="date" class="mask date" id="sMiCommitmentReceivedD" datepickerTransition-in recalc value="{{sMiCommitmentReceivedD}}">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Commitment expiration date</label>
                                        </div>
                                        <div>
                                            <input type="text" preset="date" class="mask date" id="sMiCommitmentExpirationD"
                                                datepickerTransition-in recalc value="{{sMiCommitmentExpirationD}}">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>Mortgage insurance certificate ID</label>
                                        </div>
                                        <div>
                                            <input class="w-140" type="text" id="sMiCertId" value="{{sMiCertId}}">
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <label>UFMIP is refundable on a pro-rata basis?</label>
                                        </div>
                                        <div>
                                            <input id="sUfmipIsRefundableOnProRataBasis" type="checkbox" checked="{{sUfmipIsRefundableOnProRataBasis}}"><label for="sUfmipIsRefundableOnProRataBasis">Yes</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="div-housing-border">
                        <header class="header header-border header-border-rate-lock">MIP Impound Setup</header>
                        <div class="table">
                            <div>
                                <div class="w-215">
                                    <label>Escrowed?</label>
                                </div>
                                <div>
                                    <input id="sMInsRsrvEscrowedTri" type="checkbox" recalc checked="{{sMInsRsrvEscrowedTri}}" disabled="{{sIssMInsRsrvEscrowedTriReadOnly}}"><label for="sMInsRsrvEscrowedTri">Yes</label>
                                </div>
                            </div>
                        </div>
                        <div class="table table-inline">
                            <div>
                                <div style="vertical-align: bottom;">
                                    <label>Disbursement Schedule Months</label>
                                </div>
                                <div>
                                    <div class="table table-font-default table-padding-right-p14-closing-costs">
                                        <div>
                                            <div>
                                                <label>Jan</label>
                                                <input recalc type="text" id="MIJan" value="{{MIJan}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Feb</label>
                                                <input recalc type="text" id="MIFeb" value="{{MIFeb}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Mar</label>
                                                <input recalc type="text" id="MIMar" value="{{MIMar}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Apr</label>
                                                <input recalc type="text" id="MIApr" value="{{MIApr}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>May</label>
                                                <input recalc type="text" id="MIMay" value="{{MIMay}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Jun</label>
                                                <input recalc type="text" id="MIJun" value="{{MIJun}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Jul</label>
                                                <input recalc type="text" id="MIJul" value="{{MIJul}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Aug</label>
                                                <input recalc type="text" id="MIAug" value="{{MIAug}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Sep</label>
                                                <input recalc type="text" id="MISep" value="{{MISep}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Oct</label>
                                                <input recalc type="text" id="MIOct" value="{{MIOct}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Nov</label>
                                                <input recalc type="text" id="MINov" value="{{MINov}}" class="input-w30">
                                            </div>
                                            <div>
                                                <label>Dec</label>
                                                <input recalc type="text" id="MIDec" value="{{MIDec}}" class="input-w30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <label>Reserve Months Cushion</label>
                                </div>
                                <div>
                                    <input recalc type="text" id="MICush" value="{{MICush}}" class="input-w30"> months
                                </div>
                                {{#if !sSchedDueD1}}<div class="notif-housing">
                                    Note: Reserve months calculation requires a 1st Payment Date
                                </div>{{/if}}
                            </div>
                            <div>
                                <div>
                                    <label>Reserve Months</label>
                                </div>
                                <div>
                                    <div class="table table-font-default">
                                        <div>
                                            <div>
                                                <div class="f-left"><input recalc id="sMInsRsrvMon" value="{{sMInsRsrvMon}}" readonly="{{!sMInsRsrvMonLckd}}" type="text" class="input-w30"></div>
                                                <div class="f-left"><label class="lock-checkbox"><input recalc id="sMInsRsrvMonLckd" checked="{{sMInsRsrvMonLckd}}" type="checkbox"></label></div>
                                                <span class="h-22">months</span>
                                            </div>
                                            <div class="w-244"></div>
                                            <div>
                                                <label>Initial Reserve Amount</label>
                                            </div>
                                            <div>
                                                <input id="sMInsRsrv" value="{{sMInsRsrv}}" type="text" readonly="readonly" class="input-w100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{#each Expenses:i}}
                    {{>ExpenseTemplate}}
                {{/each}}
            </div>
        </asp:Placeholder>
    </script>
</form>
<div id="calculationModal"></div>
</body>
</html>
