﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEDoc.aspx.cs" Inherits="PriceMyLoan.UI.Main.ViewEDoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style>
    .ErrorMessageStyle {
      padding-top:20px;
      text-align:center;
      width:100%;
      font-weight:bold;
      color:#EA1C0D;
      font-size:18px;
    }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div class="ErrorMessageStyle">
      <ml:EncodedLiteral ID="m_errorMsg" runat="server" />
    </div>
    </form>
</body>
</html>
