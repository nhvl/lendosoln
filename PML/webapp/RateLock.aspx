﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RateLock.aspx.cs" Inherits="PriceMyLoan.webapp.RateLock" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rate Lock</title>
</head>
<body id="MainBody">
    <form id="form1" runat="server">
    <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />

    <div id="RateLockDiv" class="content-detail" name="content-detail">
        <div ng-controller="RateLockController" class="warp-section" >
            <header class="page-header">Rate Lock</header>
            <div generic-framework-vendor-links></div>
            <div class="half-width pull-left grey-box-border">
                <table class="row-spacer">
                    <tr>
                        <td><label>Loan Program</label><input id="sLpTemplateNm" type="text" readonly="readonly" value="{{vm.sLpTemplateNm}}" class="full-width-input" /></td>
                    </tr>
                    <tr>
                        <td><label>Registered Loan Program</label><input id="sLpTemplateNmSubmitted" type="text" readonly="readonly" value="{{vm.sLpTemplateNmSubmitted}}" class="full-width-input" /></td>
                    </tr>
                    <tr>
                        <td><label>Registered Date</label><input id="sSubmitD" type="text" readonly="readonly" value="{{vm.sSubmitD}}" class="form-control-date" /></td>
                    </tr>
                    <tr>
                        <td><label>Registration Comments</label><textarea  id="sSubmitN"  class="full-width-input textarea-rate-lock" readonly>{{vm.sSubmitN}}</textarea></td>
                    </tr>
                </table>
            </div>

            <div class="half-width-right half-width-right-fix-height pull-right grey-box-border" id="loanStatus">
                <div class="table wide table-rate-lock" ng-if="!vm.HideRateLockPageLoanStatusInOriginatorPortal">
                    <div>
                        <div>
                            <label>Loan Status</label><input id="sStatusT" type="text" readonly="readonly" value="{{vm.sStatusT}}" class="full-width-input" />
                        </div>
                    </div>
                </div>
                <div class="table table-rate-lock">
                    <div>
                        <div>
                            <label>Lock Status</label>
                            <input id="sRateLockStatusT" type="text" readonly="readonly" value="{{vm.sRateLockStatusT}}" class="input-w200" />
                        </div>
                        <div class="vertical-align-bottom">
                            <button id="DownloadLockConfirmationButton" nohighlight="true" type="button" ng-show="vm.displayRateLockDownloadButton" class="btn btn-default" ng-click="downloadLockConfirmation();" >Download Lock Confirmation</button>
                        </div>
                    </div>
                </div>
                <div class="flex-alignment flex-rate-lock">
                    <div class="col-xs-4">
                        <div>
                            <label>Rate Locked Date</label>
                            <input id="sRLckdD" type="text" readonly="readonly" value="{{vm.sRLckdD}}" class="form-control-date" />
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div>
                            <label>Lock Period</label><input style="display: inline-block;" id="sRLckdDays" type="text" readonly="readonly" value="{{vm.sRLckdDays}}" class="input-w45" /> days
                        </div>
                    </div>
                </div>
                <div class="table table-rate-lock">
                    <div>
                        <div>
                            <label>Rate Lock Comments</label><input id="sRLckdN" type="text" readonly="readonly" value="{{vm.sRLckdN}}" class="input-w320" />
                        </div>
                    </div>
                </div>
                <div class="flex-alignment flex-rate-lock">
                    <div class="col-xs-4">
                        <div>
                            <label>Lock Expiration Date</label><input id="sRLckdExpiredD" type="text" readonly="readonly" value="{{vm.sRLckdExpiredD}}" class="form-control-date" />
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div>
                            <label>Lock Expiration Comments</label><input id="sRLckdExpiredN" type="text" readonly="readonly" value="{{vm.sRLckdExpiredN}}" class="input-w320" />
                        </div>
                    </div>
                </div>
                <div class="ratelock-buttons" ng-class="{ 'ratelock-buttons-reduce': vm.sIsRateLockExtentionAllowed && vm.sIsRateLockFloatDownAllowed && vm.sIsRateReLockAllowed}" >
                    <div ng-show="vm.sIsRateLockFloatDownAllowed"><button id="RateRenegotiationButton" type="button" nohighlight="true" class="btn btn-default" ng-click="renegotiateRate();">Rate Renegotiation</button></div>
                    <div ng-show="vm.sIsRateLockExtentionAllowed"><button id="ExtendRateLockButton" type="button" nohighlight="true" class="btn btn-default" ng-click="extendRate();" >Extend Rate Lock</button></div>
                    <div ng-show="vm.sIsRateReLockAllowed"><button id="RateReLockButton" type="button" nohighlight="true"  class="btn btn-default" ng-click="relockRate();" >Rate Re-Lock</button></div>
                </div>
            </div>

            <div id="origCompDiv" class="half-width pull-left grey-box-border">
                <div class="flex-space-between padding-bottom-p8">
                    <div><div>
                        Originator Compensation Information
                    </div></div>
                    <div><div>
                        <a id="lenderCompPlanLink" href="#" ng-click="displayLenderCompPlan();" ng-hide="vm.lenderCompPlan == null" class="pull-right">view lender comp plan details</a>
                    </div></div>
                </div>
                <div class="flex-space-between flex-space-between-none-bottom">
                    <div>
                        <div>
                            <label>Amount</label><input id="sOriginatorCompensationAmount" type="text" readonly="readonly" value="{{vm.sOriginatorCompensationAmount}}" class="half-width-input" />
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>Net Points</label><input id="sOriginatorCompNetPoints" type="text" readonly="readonly" value="{{vm.sOriginatorCompNetPoints}}" class="half-width-input" />
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>Paid By</label>
                            <select id="sOriginatorCompensationPaymentSourceT" disabled="disabled" class="half-width-input" data-mask-ignore="true">
                                <option ng-selected="vm.sOriginatorCompensationPaymentSourceT === 0">Not Specified</option>
                                <option ng-selected="vm.sOriginatorCompensationPaymentSourceT === 1">Borrower</option>
                                <option ng-selected="vm.sOriginatorCompensationPaymentSourceT === 2">Lender</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clear">&nbsp;</div>

            <lender-comp-table ng-if="vm.lenderCompPlan !== null" plan="vm.lenderCompPlan"></lender-comp-table>

            <div class="half-width pull-left">
                <div class="grey-box-border">
                    <header class="header header-border header-border-rate-lock">Final Price Breakdown</header>
                    <table id="pricing" class="primary-table none-hover-table">
                        <thead>
                            <tr class="header">
                                <th>&nbsp;</th>
                                <th class="text-right">Rate</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Fee</th>
                                <th class="text-right" ng-show="vm.sFinMethT == 1">Margin</th>
                                <th class="text-right" ng-show="vm.sIsOptionArm">Teaser Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Base Price</td>
                                <td class="text-right">{{vm.sBrokerLockBrokerBaseNoteIR}}</td>
                                <td class="text-right">{{vm.sBrokerLockBrokerBaseBrokComp1PcPrice}}</td>
                                <td class="text-right">{{vm.sBrokerLockBrokerBaseBrokComp1PcFee}}</td>
                                <td class="text-right" ng-show="vm.sFinMethT == 1">{{vm.sBrokerLockBrokerBaseRAdjMarginR}}</td>
                                <td class="text-right" ng-show="vm.sIsOptionArm">{{vm.sBrokerLockBrokerBaseOptionArmTeaserR}}</td>
                            </tr>
                            <tr>
                                <td>Total Adjustments</td>
                                <td class="text-right">{{vm.sBrokerLockTotVisibleAdjNoteIR}}</td>
                                <td class="text-right">{{vm.sBrokerLockTotVisibleAdjBrokComp1PcPrice}}</td>
                                <td class="text-right">{{vm.sBrokerLockTotVisibleAdjBrokComp1PcFee}}</td>
                                <td class="text-right" ng-show="vm.sFinMethT == 1">{{vm.sBrokerLockTotVisibleAdjRAdjMarginR}}</td>
                                <td class="text-right" ng-show="vm.sIsOptionArm">{{vm.sBrokerLockTotVisibleAdjOptionArmTeaserR}}</td>
                            </tr>
                            <tr>
                                <td>Originator Price</td>
                                <td class="text-right">{{vm.sNoteIR}}</td>
                                <td class="text-right">{{vm.sBrokerLockFinalBrokComp1PcPrice}}</td>
                                <td class="text-right">{{vm.sBrokComp1Pc}}</td>
                                <td class="text-right" ng-show="vm.sFinMethT == 1">{{vm.sRAdjMarginR}}</td>
                                <td class="text-right" ng-show="vm.sIsOptionArm">{{vm.sOptionArmTeaserR}}</td>
                            </tr>
                            <tr>
                                <td>Originator Compensation</td>
                                <td class="text-right">{{vm.sBrokerLockOriginatorCompAdjNoteIR}}</td>
                                <td class="text-right">{{vm.sBrokerLockOriginatorCompAdjBrokComp1PcPrice}}</td>
                                <td class="text-right">{{vm.sBrokerLockOriginatorCompAdjBrokComp1PcFee}}</td>
                                <td class="text-right" ng-show="vm.sFinMethT == 1">{{vm.sBrokerLockOriginatorCompAdjRAdjMarginR}}</td>
                                <td class="text-right" ng-show="vm.sIsOptionArm">{{vm.sBrokerLockOriginatorCompAdjOptionArmTeaserR}}</td>
                            </tr>
                            <tr>
                                <td>Final Price</td>
                                <td class="text-right">{{vm.sBrokerLockOriginatorPriceNoteIR}}</td>
                                <td class="text-right">{{vm.sBrokerLockOriginatorPriceBrokComp1PcPrice}}</td>
                                <td class="text-right">{{vm.sBrokerLockOriginatorPriceBrokComp1PcFee}}</td>
                                <td class="text-right" ng-show="vm.sFinMethT == 1">{{vm.sBrokerLockOriginatorPriceRAdjMarginR}}</td>
                                <td class="text-right" ng-show="vm.sIsOptionArm">{{vm.sBrokerLockOriginatorPriceOptionArmTeaserR}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="half-width-right pull-right">
                <div class="grey-box-border">
                    <header class="header header-border header-border-rate-lock">Total Adjustments Breakdown</header>
                    <table id="adjustments" class="primary-table none-hover-table">
                        <thead>
                            <tr class="header">
                                <th>Description</th>
                                <th class="text-right">Rate</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Fee</th>
                                <th class="text-right" ng-show="vm.sFinMethT == 1">Margin</th>
                                <th class="text-right" ng-show="vm.sIsOptionArm">Teaser Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="adjustment in vm.adjustments">
                                <td>{{adjustment.Description}}</td>
                                <td class="text-right">{{adjustment.Rate}}</td>
                                <td class="text-right">{{adjustment.Price}}</td>
                                <td class="text-right">{{adjustment.Fee}}</td>
                                <td class="text-right" ng-show="vm.sFinMethT == 1">{{adjustment.Margin}}</td>
                                <td class="text-right" ng-show="vm.sIsOptionArm">{{adjustment.TeaserRate}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="clear">&nbsp;</div>

            <div class="full-width grey-box-border" ng-if="!vm.hideRateLockHistory">
                <header class="header header-border header-border-rate-lock">Rate Lock History</header>
                <table id="rateLockHistory" class="primary-table none-hover-table">
                    <thead>
                        <tr class="header">
                            <th>Date</th>
                            <th>Action</th>
                            <th>Agent</th>
                            <th class="text-right">Rate</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Fee</th>
                            <th class="text-right" ng-show="vm.sFinMethT == 1">Margin</th>
                            <th class="text-right" ng-show="vm.sIsOptionArm">Teaser Rate</th>
                            <th>Program Name</th>
                            <th>Lock Date</th>
                            <th>Expiration Date</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="rateLockHistoryItem in vm.rateLockHistory">
                            <td>{{rateLockHistoryItem.EventDate}}</td>
                            <td>{{rateLockHistoryItem.Action}}</td>
                            <td>{{rateLockHistoryItem.DoneBy}}</td>
                            <td class="text-right">{{rateLockHistoryItem.sNoteIR}}</td>
                            <td class="text-right">{{rateLockHistoryItem.sBrokerLockFinalBrokComp1PcPrice}}</td>
                            <td class="text-right">{{rateLockHistoryItem.sBrokComp1Pc}}</td>
                            <td class="text-right" ng-show="vm.sFinMethT == 1">{{rateLockHistoryItem.sRAdjMarginR}}</td>
                            <td class="text-right" ng-show="vm.sIsOptionArm">{{rateLockHistoryItem.sOptionArmTeaserR}}</td>
                            <td>{{rateLockHistoryItem.sLpTemplateNm}}</td>
                            <td>{{rateLockHistoryItem.sRLckdD}}</td>
                            <td>{{rateLockHistoryItem.sRLckdExpiredD}}</td>
                            <td>{{rateLockHistoryItem.sRLckBreakReasonDesc}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
