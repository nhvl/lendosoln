﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Action.ascx.cs" Inherits="PriceMyLoan.webapp.Action" %>

<style type="text/css">
    .CreditReferenceNumber {
        font-weight: bold;
    }

    .CopyButton {
        width: 50px;
    }

    .WarningIcon {
        vertical-align: bottom;
    }
</style>

<div style="display:none;" id="dialog-convertNameError" title="Duplicate Loan Number">
    <p style="width: 100%">An existing loan file has the same loan number. Converting this lead to a loan requires assigning a new loan number. Do you want to continue?</p>
</div>

<div id="ActionContainer" class="container">
    <div id="AllActions" class="Hidden">
        <%-- Submit to AUS engines --%>
        <%-- We'll need to save before submitting. Always. --%>
        <%-- The Convert Lead->Loan button is moved into the Compare Pinned div for expanded lead editor --%>
    <asp:PlaceHolder runat="server" ID="ConvertLeadToLoanContainer">
        <button type="button" id="ConvertLeadToLoan" class="Action btn-reset" >
            Convert Lead to Loan File
            <img src="../images/warning20x20.png" class="WarningIcon" />
        </button>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="ConvertQP2ToLeadContainer">
        <input type="button" id="ConvertQP2FileToLead" value="Create Lead" Visible="false" />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="ConvertQP2ToLoanContainer">
        <input type="button" id="ConvertQP2FileToLoan" value="Create Loan" Visible="false" />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="ActionButtonsContainer">
        <button type="button" class="Action Hidden btn-reset" id="DesktopOriginator">
            Run Scenario in DO
            <img src="../images/warning20x20.png" class="WarningIcon" />
        </button>
        <button type="button" class="Action Hidden btn-reset" id="DesktopUnderwriter_Seamless">
            Submit to DU (Seamless)
            <img src="../images/warning20x20.png" class="WarningIcon" />
        </button>
        <button type="button" class="Action Hidden btn-reset" id="DesktopUnderwriter">
            Run Scenario in DU
            <img src="../images/warning20x20.png" class="WarningIcon" />
        </button>
        <button type="button" class="Action Hidden btn-reset" id="LoanProductAdvisor_Seamless">
            Submit to LPA (Seamless)
            <img src="../images/warning20x20.png" class="WarningIcon" />
        </button>
        <button type="button" class="Action Hidden btn-reset" id="LoanProspector">
            Submit to LPA
            <img src="../images/warning20x20.png" class="WarningIcon" />
        </button>
        <input type="button" class="Action Hidden" id="FHATotal" value="Submit to FHA Total"/>
    </asp:PlaceHolder>
    </div>
    <div id="ComparePinnedContainer" class="FullHeight">
        <%-- Pinned results --%>
        <input type="button" runat="server" class="EditClosingCostsBtn" id="EditClosingCostsBtn" visible="false" value="Edit Closing Costs" /><input type="button" id="ComparePinned" value="No Pinned Results to Compare" />
    </div>
    <div id="WaitDialog">
        <span id="WaitMessage"></span>
    </div>
</div>

<script>
    $("[id*='EditClosingCostsBtn']").click(function() {
    if ($("#IsUsing2015GFEUITPO").val() == "True") {
        document.location = ML.VirtualRoot + '/webapp/TPOPortalClosingCosts.aspx?loanid=' + PML.getLoanId() + '&src=pipeline';
        }
        else {
            document.location = ML.VirtualRoot + "/webapp/TPOFeeEditor.aspx?loanid=" + PML.getLoanId();
        }
    });

    function copyToClipboard(str) {
        if (window.clipboardData) {
            window.clipboardData.setData('Text', str);
        } else {
            alert('Copying to clipboard not supported in current browser.');
        }
    }
</script>
