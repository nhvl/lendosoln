﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.UI.Workflow;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    [DataContract]
    public class SampleClosingCostFeeViewModel
    {

        public SampleClosingCostFeeViewModel()
        {
            VRoot = Tools.VRoot;
            this.Expenses = new List<HousingExpenseViewModel>();
        }

        [DataMember]
        public string sLAmtCalc { get; set; }

        [DataMember]
        public bool sLAmtLckd { get; set; }

        [DataMember]
        public IEnumerable<BorrowerClosingCostFeeSection> SectionList { get; set; }
        [DataMember]
        public IEnumerable<SellerClosingCostFeeSection> SellerList { get; set; }
        [DataMember]
        public bool IsUsingSellerFees { get; set; }
        [DataMember]
        public bool WillUseSellerFees { get; set; }

        [DataMember]
        public bool sIsRequireFeesFromDropDown { get; set; }

        [DataMember]
        public string VRoot
        {
            get;
            set;
        }

        [DataMember]
        public Guid sLId { get; set; }

        [DataMember]
        public string sRLckdD { get; set; }
        [DataMember]
        public bool sRLckdDLck { get; set; }
        [DataMember]
        public string sQMQualR { get; set; }
        [DataMember]
        public string sQMExclDiscntPnt { get; set; }
        [DataMember]
        public bool sQMExclDiscntPntLck { get; set; }
        [DataMember]
        public string sDocMagicClosingD { get; set; }
        [DataMember]
        public bool sDocMagicClosingDLck { get; set; }
        [DataMember]
        public string sQMQualBottom { get; set; }
        [DataMember]
        public string sQMNonExcludableDiscountPointsPc { get; set; }
        [DataMember]
        public string sQMAveragePrimeOfferR { get; set; }
        [DataMember]
        public bool sQMAveragePrimeOfferRLck { get; set; }
        [DataMember]
        public string sQMTotFeeAmount { get; set; }
        [DataMember]
        public string sQMFhaUfmipAtClosing { get; set; }
        [DataMember]
        public bool sQMFhaUfmipAtClosingLck { get; set; }
        [DataMember]
        public string sQMParR { get; set; }
        [DataMember]
        public string sQMTotFinFeeAmount { get; set; }
        [DataMember]
        public string sQMMaxPrePmntPenalty { get; set; }
        [DataMember]
        public string sQMAprRSpread { get; set; }
        [DataMember]
        public string sQMAprRSpreadCalcDesc { get; set; }
        [DataMember]
        public string sQMParRSpread { get; set; }
        [DataMember]
        public string sQMParRSpreadCalcDesc { get; set; }
        [DataMember]
        public string sQMExcessUpfrontMIP { get; set; }
        [DataMember]
        public string sQMExcessUpfrontMIPCalcDesc { get; set; }
        [DataMember]
        public string sQMExcessDiscntFPc { get; set; }
        [DataMember]
        public string sQMExcessDiscntFCalcDesc { get; set; }
        [DataMember]
        public string sQMDiscntBuyDownR { get; set; }
        [DataMember]
        public string sQMDiscntBuyDownRCalcDesc { get; set; }
        [DataMember]
        public string sQMLAmt { get; set; }
        [DataMember]
        public string sQMLAmtCalcDesc { get; set; }
        [DataMember]
        public string sQMMaxPointAndFeesAllowedAmt { get; set; }
        [DataMember]
        public string sQMMaxPointAndFeesAllowedPc { get; set; }
        [DataMember]
        public string sGfeOriginatorCompF { get; set; }
        [DataMember]
        public string sQMExcessDiscntF { get; set; }
        [DataMember]
        public string sQMTotFeeAmt { get; set; }
        [DataMember]
        public string sQMTotFeePc { get; set; }
        [DataMember]
        public bool sQMLoanPassesPointAndFeesTest { get; set; }
        [DataMember]
        public bool sQMLoanDoesNotHaveNegativeAmort { get; set; }
        [DataMember]
        public bool sQMLoanDoesNotHaveBalloonFeature { get; set; }
        [DataMember]
        public bool sQMLoanDoesNotHaveAmortTermOver30Yr { get; set; }
        [DataMember]
        public bool sQMLoanHasDtiLessThan43Pc { get; set; }
        [DataMember]
        public bool sQMIsVerifiedIncomeAndAssets { get; set; }
        [DataMember]
        public bool sQMIsEligibleByLoanPurchaseAgency { get; set; }
        [DataMember]
        public E_sQMLoanPurchaseAgency sQMLoanPurchaseAgency { get; set; }
        [DataMember]
        public E_sQMStatusT sQMStatusT { get; set; }
        [DataMember]
        public E_HighPricedMortgageT sHighPricedMortgageT { get; set; }
        [DataMember]
        public bool sHighPricedMortgageTLckd { get; set; }

        [DataMember]
        public E_ClosingCostViewT viewT { get; set; }

        [DataMember]
        public string sConsummationD { get; set; }

        [DataMember]
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }

        [DataMember]
        public bool sIPiaDyLckd { get; set; }
        [DataMember]
        public string sIPiaDy { get; set; }
        [DataMember]
        public bool sIPerDayLckd { get; set; }
        [DataMember]
        public string sIPerDay { get; set; }
        [DataMember]
        public bool sIsManuallySetThirdPartyAffiliateProps { get; set; }

        [DataMember]
        public E_sDisclosureRegulationT sDisclosureRegulationT { get; set; }
        
        [DataMember]
        public string sEstCloseD { get; set; }
        [DataMember]
        public bool sEstCloseDLckd { get; set; }
        [DataMember]
        public string sAggregateAdjRsrv { get; set; }
        [DataMember]
        public bool sAggregateAdjRsrvLckd { get; set; }
        [DataMember]
        public string sSchedDueD1 { get; set; }
        [DataMember]
        public bool sSchedDueD1Lckd { get; set; }
        [DataMember]
        public string sGfeInitialImpoundDeposit { get; set; }
        [DataMember]
        public string sFfUfmipR { get; set; }
        [DataMember]
        public string sFfUfmip1003 { get; set; }
        [DataMember]
        public bool sFfUfmip1003Lckd { get; set; }
        [DataMember]
        public E_MipFrequency sMipFrequency { get; set; }
        [DataMember]
        public string sMipPiaMon { get; set; }
        [DataMember]
        public bool sFfUfMipIsBeingFinanced { get; set; }
        [DataMember]
        public string sFfUfmipFinanced { get; set; }
        [DataMember]
        public string sUfCashPd { get; set; }
        [DataMember]
        public bool sUfCashPdLckd { get; set; }
        [DataMember]
        public E_sLT sLt { get; set; }
        [DataMember]
        public string sLenderUfmipR { get; set; }
        [DataMember]
        public string sLenderUfmip { get; set; }
        [DataMember]
        public bool sLenderUfmipLckd { get; set; }
        [DataMember]
        public E_sMiCompanyNmT sMiCompanyNmT { get; set; }
        [DataMember]
        public string sMiLenderPaidCoverage { get; set; }
        [DataMember]
        public E_sMiInsuranceT sMiInsuranceT { get; set; }
        [DataMember]
        public string sMiCommitmentRequestedD { get; set; }
        [DataMember]
        public string sMiCommitmentReceivedD { get; set; }
        [DataMember]
        public string sMiCommitmentExpirationD { get; set; }
        [DataMember]
        public string sMiCertId { get; set; }
        [DataMember]
        public bool sUfmipIsRefundableOnProRataBasis { get; set; }
        [DataMember]
        public string sProMInsR { get; set; }
        [DataMember]
        public E_PercentBaseT sProMInsT { get; set; }
        [DataMember]
        public string sProMInsBaseAmt { get; set; }
        [DataMember]
        public string sProMInsBaseMonthlyPremium { get; set; }
        [DataMember]
        public string sProMInsMb { get; set; }
        [DataMember]
        public bool sProMInsLckd { get; set; }
        [DataMember]
        public string sProMIns { get; set; }
        [DataMember]
        public string sProMInsMon { get; set; }
        [DataMember]
        public string sProMIns2Mon { get; set; }
        [DataMember]
        public string sProMInsR2 { get; set; }
        [DataMember]
        public string sProMIns2 { get; set; }
        [DataMember]
        public string sProMInsCancelLtv { get; set; }
        [DataMember]
        public bool sProMInsMidptCancel { get; set; }
        [DataMember]
        public bool sMInsRsrvEscrowedTri { get; set; }
        [DataMember]
        public bool sIssMInsRsrvEscrowedTriReadOnly { get; set; }
        [DataMember]
        public string sMInsRsrvMon { get; set; }
        [DataMember]
        public bool sMInsRsrvMonLckd { get; set; }
        [DataMember]
        public string sMInsRsrv { get; set; }
        [DataMember]
        public string sProMInsCancelMinPmts { get; set; }
        [DataMember]
        public string MIJan { get; set; }
        [DataMember]
        public string MIFeb { get; set; }
        [DataMember]
        public string MIMar { get; set; }
        [DataMember]
        public string MIApr { get; set; }
        [DataMember]
        public string MIMay { get; set; }
        [DataMember]
        public string MIJun { get; set; }
        [DataMember]
        public string MIJul { get; set; }
        [DataMember]
        public string MIAug { get; set; }
        [DataMember]
        public string MISep { get; set; }
        [DataMember]
        public string MIOct { get; set; }
        [DataMember]
        public string MINov { get; set; }
        [DataMember]
        public string MIDec { get; set; }
        [DataMember]
        public string MICush { get; set; }
        [DataMember]
        public List<HousingExpenseViewModel> Expenses { get; set; }
        [DataMember]
        public bool DisableTPAffIfHasContact { get; set; }
        [DataMember]
        public IEnumerable<KeyValuePair<string, E_AgentRoleT>> Beneficiaries { get; set; }
        [DataMember]
        public bool CanReadDFLP { get; set; }
        [DataMember]
        public bool CanSetDFLP { get; set; }
    }

    [DataContract]
    public class HousingExpenseViewModel
    {
        public HousingExpenseViewModel()
        {
            this.Disbursements = new List<DisbursementViewModel>();
            this.isVisible = true;
        }

        [DataMember]
        public bool isVisible { get; set; }
        [DataMember]
        public E_HousingExpenseTypeT expenseType { get; set; }
        [DataMember]
        public E_TaxTableTaxT taxT { get; set; }
        [DataMember]
        public bool isPrepaid { get; set; }
        [DataMember]
        public bool IsCustom { get; set; }
        [DataMember]
        public bool IsInsurance { get; set; }
        [DataMember]
        public bool IsTax { get; set; }
        [DataMember]
        public bool IsOther { get; set; }
        [DataMember]
        public E_CustomExpenseLineNumberT customLineNum { get; set; }
        [DataMember]
        public string removedDisbs { get; set; }
        [DataMember]
        public E_AnnualAmtCalcTypeT calcMode { get; set; }
        [DataMember]
        public bool flippedOnce { get; set; }
        [DataMember]
        public string expName { get; set; }
        [DataMember]
        public string expDesc { get; set; }
        [DataMember]
        public bool isEscrowed { get; set; }
        [DataMember]
        public E_AnnualAmtCalcTypeT annAmtCalcT { get; set; }
        [DataMember]
        public string annAmtTot { get; set; }
        [DataMember]
        public string monTotPITI { get; set; }
        [DataMember]
        public string prepMnth { get; set; }
        [DataMember]
        public string prepAmt { get; set; }
        [DataMember]
        public string monTotServ { get; set; }
        [DataMember]
        public string rsrvMon { get; set; }
        [DataMember]
        public bool rsrvMonLckd { get; set; }
        [DataMember]
        public string rsrvAmt { get; set; }
        [DataMember]
        public string annAmtPerc { get; set; }
        [DataMember]
        public E_PercentBaseT annAmtBase { get; set; }
        [DataMember]
        public string monAmtFixed { get; set; }
        [DataMember]
        public string Jan { get; set; }
        [DataMember]
        public string Feb { get; set; }
        [DataMember]
        public string Mar { get; set; }
        [DataMember]
        public string Apr { get; set; }
        [DataMember]
        public string May { get; set; }
        [DataMember]
        public string Jun { get; set; }
        [DataMember]
        public string Jul { get; set; }
        [DataMember]
        public string Aug { get; set; }
        [DataMember]
        public string Sep { get; set; }
        [DataMember]
        public string Oct { get; set; }
        [DataMember]
        public string Nov { get; set; }
        [DataMember]
        public string Dec { get; set; }
        [DataMember]
        public string Cush { get; set; }
        [DataMember]
        public E_DisbursementRepIntervalT RepInterval { get; set; }
        [DataMember]
        public List<DisbursementViewModel> Disbursements { get; set; }
    }

    [DataContract]
    public class DisbursementViewModel
    {
        [DataMember]
        public E_DisbursementTypeT DisbT { get; set; }
        [DataMember]
        public E_DisbPaidDateType PaidDT { get; set; }
        [DataMember]
        public string DueD { get; set; }
        [DataMember]
        public string DueAmt { get; set; }
        [DataMember]
        public string DisbMon { get; set; }
        [DataMember]
        public string DisbPaidD { get; set; }
        [DataMember]
        public E_DisbursementPaidByT PaySource { get; set; }
        [DataMember]
        public string PaidFromD { get; set; }
        [DataMember]
        public string PaidToD { get; set; }
        [DataMember]
        public string DisbId { get; set; }
        [DataMember]
        public bool IsPaidFromEscrow { get; set; }
    }

    public partial class TPOPortalClosingCosts : PriceMyLoan.UI.BasePage
    {
        private void BindGfeSectionDropDown()
        {
            var items = new[]
            {
                Tools.CreateEnumListItem("A1", E_GfeSectionT.B1),
                Tools.CreateEnumListItem("A2", E_GfeSectionT.B2),
                Tools.CreateEnumListItem("B3", E_GfeSectionT.B3),
                Tools.CreateEnumListItem("B4", E_GfeSectionT.B4),
                Tools.CreateEnumListItem("B5", E_GfeSectionT.B5),
                Tools.CreateEnumListItem("B6", E_GfeSectionT.B6),
                Tools.CreateEnumListItem("B7", E_GfeSectionT.B7),
                Tools.CreateEnumListItem("B8", E_GfeSectionT.B8),
                Tools.CreateEnumListItem("B9", E_GfeSectionT.B9),
                Tools.CreateEnumListItem("B10", E_GfeSectionT.B10),
                Tools.CreateEnumListItem("B11", E_GfeSectionT.B11),
                Tools.CreateEnumListItem("N/A", E_GfeSectionT.NotApplicable)
            };
            SectionRepeater.DataSource = items;
            SectionRepeater.DataBind();
        }

        private void Bind_PercentRepeater()
        {
            var items = new[]
            {
                Tools.CreateEnumListItem("Loan Amount", E_PercentBaseT.LoanAmount),
                Tools.CreateEnumListItem("Purchase Price", E_PercentBaseT.SalesPrice),
                Tools.CreateEnumListItem("Appraisal Value", E_PercentBaseT.AppraisalValue),
                Tools.CreateEnumListItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount)
            };
            PercentRepeater.DataSource = items;
            PercentRepeater.DataBind();
            PercentRepeater2.DataSource = items;
            PercentRepeater2.DataBind();
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return this.PriceMyLoanUser.EnableNewTpoLoanNavigation ? E_XUAComaptibleValue.Edge : base.GetForcedCompatibilityMode();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            base.m_loadDefaultStylesheet = false;
            RegisterService("cfpb_utils", "/webapp/TPOPortalClosingCostsService.aspx");
        }

        public static void SetSectionList(CPageData dataLoan, bool IsUsingSellerFees, out IEnumerable<BorrowerClosingCostFeeSection> borrowerList, out IEnumerable<SellerClosingCostFeeSection> sellerList)
        {
            borrowerList = null;
            sellerList = null;
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE || dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.Blank)
            {
                if (IsUsingSellerFees)
                {
                    sellerList = dataLoan.sSellerResponsibleClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.SellerResponsibleLoanHud1);
                }
                else
                {
                    borrowerList = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanHud1);
                }
            }
            else
            {
                if (IsUsingSellerFees)
                {
                    sellerList = dataLoan.sSellerResponsibleClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate);
                }
                else
                {
                    borrowerList = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanClosingCost);
                }
            }
        }

        private static HousingExpenseViewModel CreateHousingExpenseModel(BaseHousingExpense exp, string remDisbs, bool flippedOnce)
        {
            var disbList = exp.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues
                ? (IEnumerable<SingleDisbursement>)exp.ProjectedDisbursements.OrderBy(o => o.DueDate)
                : exp.ActualDisbursementsList;

            return new HousingExpenseViewModel
            {
                IsInsurance = exp.IsInsurance,
                IsTax = exp.IsTax,
                IsOther = !exp.IsTax && !exp.IsInsurance,
                expenseType = exp.HousingExpenseType,
                taxT = exp.TaxType,
                isPrepaid = exp.IsPrepaid,
                IsCustom = exp.CanHaveCustomLineNum,
                removedDisbs = remDisbs,
                customLineNum = exp.CustomExpenseLineNum,
                calcMode = exp.AnnualAmtCalcType,
                flippedOnce = flippedOnce,
                expName = exp.DefaultExpenseDescription,
                expDesc = exp.ExpenseDescription,
                isEscrowed = exp.IsEscrowedAtClosing == E_TriState.Yes,
                annAmtCalcT = exp.AnnualAmtCalcType,
                annAmtTot = exp.AnnualAmt_rep,
                monTotPITI = exp.MonthlyAmtTotal_rep,
                prepMnth = exp.PrepaidMonths_rep,
                prepAmt = exp.PrepaidAmt_rep,
                monTotServ = exp.MonthlyAmtServicing_rep,
                rsrvMon = exp.ReserveMonths_rep,
                rsrvMonLckd = exp.ReserveMonthsLckd,
                rsrvAmt = exp.ReserveAmt_rep,
                annAmtPerc = exp.AnnualAmtCalcBasePerc_rep,
                annAmtBase = exp.AnnualAmtCalcBaseType,
                monAmtFixed = exp.MonthlyAmtFixedAmt_rep,
                RepInterval = exp.DisbursementRepInterval,
                Cush = exp.DisbursementScheduleMonths[0].ToString(),
                Jan = exp.DisbursementScheduleMonths[1].ToString(),
                Feb = exp.DisbursementScheduleMonths[2].ToString(),
                Mar = exp.DisbursementScheduleMonths[3].ToString(),
                Apr = exp.DisbursementScheduleMonths[4].ToString(),
                May = exp.DisbursementScheduleMonths[5].ToString(),
                Jun = exp.DisbursementScheduleMonths[6].ToString(),
                Jul = exp.DisbursementScheduleMonths[7].ToString(),
                Aug = exp.DisbursementScheduleMonths[8].ToString(),
                Sep = exp.DisbursementScheduleMonths[9].ToString(),
                Oct = exp.DisbursementScheduleMonths[10].ToString(),
                Nov = exp.DisbursementScheduleMonths[11].ToString(),
                Dec = exp.DisbursementScheduleMonths[12].ToString(),
                Disbursements = disbList.Select(CreateDisbursementModel).ToList()
            };
        }

        private static DisbursementViewModel CreateDisbursementModel(SingleDisbursement disb)
        {
            return new DisbursementViewModel
            {
                DisbT = disb.DisbursementType,
                PaidDT = disb.DisbPaidDateType,
                DueD = disb.DueDate_rep,
                DueAmt = disb.DisbursementAmt_rep,
                DisbMon = disb.CoveredMonths_rep,
                DisbPaidD = disb.PaidDate_rep,
                PaySource = disb.PaidBy,
                PaidFromD = disb.BillingPeriodStartD_rep,
                PaidToD = disb.BillingPeriodEndD_rep,
                DisbId = disb.DisbId.ToString(),
                IsPaidFromEscrow = disb.IsPaidFromEscrowAcc
            };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterCSS("/webapp/tabStyle_TPO.css");
            this.RegisterCSS("/webapp/jquery-ui.css");
            this.RegisterCSS("/webapp/TPOPortalClosingCosts.css");

            this.RegisterJsScript("jquery-ui.js"); ;
            this.RegisterJsScript("/mask.js");
            this.RegisterJsScript("/webapp/jquery.mask.js");
            this.RegisterJsScript("/webapp/autoNumeric.js");
            this.RegisterJsScript("WorkflowPageDisplay.js");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TPOPortalClosingCosts));
            dataLoan.InitLoad();

            this.BindDropdowns(dataLoan);
            this.SetConditionalPageDisplay(dataLoan);

            var viewModel = this.CreateViewModel(dataLoan);
            RegisterJsObject("ClosingCostData", viewModel);

            ClientScript.RegisterHiddenField("IsReadOnly", "false");
            ClientScript.RegisterHiddenField("PipelineUrl",  this.VirtualRoot + "/Main/Pipeline.aspx");
            ClientScript.RegisterHiddenField("IsNewPmlUIEnabled", PrincipalFactory.CurrentPrincipal.BrokerDB.IsNewPmlUIEnabled.ToString());
            this.RegisterJsGlobalVariables("DisplayQmPage", this.PriceMyLoanUser.EnableNewTpoLoanNavigation && RequestHelper.GetBool("DisplayQM"));
        }

        private void BindDropdowns(CPageData dataLoan)
        {
            //Need to do it this way so I dont have the ID element...
            BindGfeSectionDropDown();
            Bind_PercentRepeater();

            DropDownList ddl = new DropDownList();
            Tools.Bind_sQMStatusT(ddl);
            sQMStatusTRepeater.DataSource = ddl.Items;
            sQMStatusTRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_sHighPricedMortgageT(ddl);
            sHighPricedMortgageTRepeater.DataSource = ddl.Items;
            sHighPricedMortgageTRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_sQMLoanPurchaseAgency(ddl);
            sQMLoanPurchaseAgencyRepeater.DataSource = ddl.Items;
            sQMLoanPurchaseAgencyRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_sMiCompanyNmT(ddl, dataLoan.sLT);
            sMiCompanyNmTRepeater.DataSource = ddl.Items;
            sMiCompanyNmTRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_sMiInsuranceT(ddl);
            sMiInsuranceTRepeater.DataSource = ddl.Items;
            sMiInsuranceTRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_MipCalcType_rep(ddl, dataLoan.sLT);
            sProMInsTRepeater.DataSource = ddl.Items;
            sProMInsTRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_AnnAmtCalcType(ddl);
            annAmtCalcTRepeater.DataSource = ddl.Items;
            annAmtCalcTRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_PercentBaseT(ddl);
            annAmtBaseRepeater.DataSource = ddl.Items;
            annAmtBaseRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_DisbPaidDateType(ddl);
            PaidDateTRep.DataSource = ddl.Items;
            PaidDateTRep.DataBind();

            ddl.Items.Clear();
            Tools.Bind_DisbursementPaidByT(ddl);
            PaySrcRep.DataSource = ddl.Items;
            PaySrcRep.DataBind();

            ddl.Items.Clear();
            Tools.Bind_DisbursementReptIntervalT(ddl);
            disbRepIntervalRepeater.DataSource = ddl.Items;
            disbRepIntervalRepeater.DataBind();

            ddl.Items.Clear();
            Tools.Bind_sTaxTableT(ddl);
            taxTypeRepeater.DataSource = ddl.Items;
            taxTypeRepeater.DataBind();
        }

        private void SetConditionalPageDisplay(CPageData dataLoan)
        {
            var displayNonQmTabs = true;
            var enableNewUi = this.PriceMyLoanUser.EnableNewTpoLoanNavigation;
            var location = LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationClosingCosts;

            this.RegisterJsGlobalVariables("EnableNewTpoLoanNavigation", enableNewUi);
            if (enableNewUi)
            {
                this.TitleTable.Visible = false;
                this.QMTab.Visible = false;

                if (RequestHelper.GetBool("DisplayQM"))
                {
                    displayNonQmTabs = false;
                    location = LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationQm;
                    ContentHeader.InnerText = "QM";
                }
                else
                {
                    this.QMContent.Visible = false;
                }
            }
            else
            {
                var enableQmDisplay = PrincipalFactory.CurrentPrincipal.BrokerDB.AllowQMEditTPO;

                this.QMTab.Visible = enableQmDisplay;
                this.QMContent.Visible = enableQmDisplay;
            }

            this.LoanNavHeader.SetDataFromLoan(dataLoan, location);

            BorrowerEstimate.Visible = displayNonQmTabs && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            BorrowerHud1.Visible = displayNonQmTabs &&
                (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE ||
                dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.Blank);

            BorrowerHud1.Attributes.Add("value", "" + (int)E_ClosingCostViewT.LoanHud1);
            BorrowerEstimate.Attributes.Add("value", "" + (int)E_ClosingCostViewT.LoanClosingCost);

            SellerEstimate.Visible = displayNonQmTabs && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            SellerHud1.Visible = displayNonQmTabs &&
                (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE || dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.Blank);

            SellerHud1.Attributes.Add("value", "" + (int)E_ClosingCostViewT.LoanHud1);
            SellerEstimate.Attributes.Add("value", "" + (int)E_ClosingCostViewT.LoanClosingCost);

            expensesTab.Visible = displayNonQmTabs && dataLoan.sIsHousingExpenseMigrated;
            HousingExpenseTab.Visible = displayNonQmTabs && dataLoan.sIsHousingExpenseMigrated;
        }

        private static void BindHousingExpenses(CPageData dataLoan, List<HousingExpenseViewModel> expenses)
        {
            foreach (HousingExpenseViewModel viewModel in expenses)
            {
                BaseHousingExpense expense = GetExpenseFromLoan(dataLoan, viewModel);

                expense.IsEscrowedAtClosing = viewModel.isEscrowed ? E_TriState.Yes : E_TriState.No;
                expense.AnnualAmtCalcBasePerc_rep = viewModel.annAmtPerc;
                expense.AnnualAmtCalcBaseType = viewModel.annAmtBase;
                expense.MonthlyAmtFixedAmt_rep = viewModel.monAmtFixed;
                expense.IsPrepaid = viewModel.isPrepaid;
                expense.PrepaidMonths_rep = viewModel.prepMnth;
                expense.ReserveMonthsLckd = viewModel.rsrvMonLckd;
                expense.ReserveMonths_rep = viewModel.rsrvMon;
                expense.SetCustomLineNumber(viewModel.customLineNum);
                expense.TaxType = viewModel.taxT;
                expense.ExpenseDescription = viewModel.expDesc;
                expense.DisbursementRepInterval = viewModel.RepInterval;
                int result;
                expense.DisbursementScheduleMonths[0] = int.TryParse(viewModel.Cush, out result) ? result : 0;
                expense.DisbursementScheduleMonths[1] = int.TryParse(viewModel.Jan, out result) ? result : 0;
                expense.DisbursementScheduleMonths[2] = int.TryParse(viewModel.Feb, out result) ? result : 0;
                expense.DisbursementScheduleMonths[3] = int.TryParse(viewModel.Mar, out result) ? result : 0;
                expense.DisbursementScheduleMonths[4] = int.TryParse(viewModel.Apr, out result) ? result : 0;
                expense.DisbursementScheduleMonths[5] = int.TryParse(viewModel.May, out result) ? result : 0;
                expense.DisbursementScheduleMonths[6] = int.TryParse(viewModel.Jun, out result) ? result : 0;
                expense.DisbursementScheduleMonths[7] = int.TryParse(viewModel.Jul, out result) ? result : 0;
                expense.DisbursementScheduleMonths[8] = int.TryParse(viewModel.Aug, out result) ? result : 0;
                expense.DisbursementScheduleMonths[9] = int.TryParse(viewModel.Sep, out result) ? result : 0;
                expense.DisbursementScheduleMonths[10] = int.TryParse(viewModel.Oct, out result) ? result : 0;
                expense.DisbursementScheduleMonths[11] = int.TryParse(viewModel.Nov, out result) ? result : 0;
                expense.DisbursementScheduleMonths[12] = int.TryParse(viewModel.Dec, out result) ? result : 0;

                // If has been flipped, then we are a state where there should be no actual disbursements.
                if (viewModel.flippedOnce)
                {
                    expense.ClearDisbursements();
                }

                // If was in Disbursement mode, then bind the disbursements.
                if (viewModel.calcMode == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    BindDisbursements(viewModel.Disbursements, expense);
                }

                // Remove any lingering disbursements
                string[] removedDisbs = viewModel.removedDisbs.Split(',');
                RemoveDisbursements(removedDisbs, expense);

                expense.AnnualAmtCalcType = viewModel.annAmtCalcT;

                // Set flipped bit somewhere here

                // Ensure that no actual disbursements exist while in calc mode
                if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues || viewModel.calcMode == E_AnnualAmtCalcTypeT.LoanValues)
                {
                    expense.ClearDisbursements();
                }

                // If switching from calc to disbursement, then transform projected to actual disbursements.
                if (viewModel.calcMode == E_AnnualAmtCalcTypeT.LoanValues && expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    ConvertProjectedToActual(viewModel.Disbursements, expense);
                }
            }
        }

        private static void ConvertProjectedToActual(List<DisbursementViewModel> disbs, BaseHousingExpense expense)
        {
            foreach (DisbursementViewModel dview in disbs)
            {
                if (dview.DisbT == E_DisbursementTypeT.Actual)
                {
                    continue;
                }

                var disb = new SingleDisbursement
                {
                    DisbursementType = E_DisbursementTypeT.Actual,
                    DisbursementAmtLckd = true
                };
                expense.AddDisbursement(disb);

                disb.DisbPaidDateType = dview.PaidDT;
                disb.DueDate_rep = dview.DueD;
                disb.DisbursementAmt_rep = dview.DueAmt;
                disb.CoveredMonths_rep = dview.DisbMon;
                disb.PaidDate_rep = dview.DisbPaidD;
                disb.PaidBy = dview.PaySource;
                disb.BillingPeriodStartD_rep = dview.PaidFromD;
                disb.BillingPeriodEndD_rep = dview.PaidToD;
                disb.IsPaidFromEscrowAcc = dview.IsPaidFromEscrow;
            }
        }

        private static BaseHousingExpense GetExpenseFromLoan(CPageData dataLoan, HousingExpenseViewModel expense)
        {
            switch (expense.expenseType)
            {
                case E_HousingExpenseTypeT.HazardInsurance:
                    return dataLoan.sHazardExpense;
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return dataLoan.sCondoHO6Expense;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return dataLoan.sFloodExpense;
                case E_HousingExpenseTypeT.GroundRent:
                    return dataLoan.sGroundRentExpense;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return dataLoan.sHOADuesExpense;
                case E_HousingExpenseTypeT.OtherTaxes1:
                    return dataLoan.sOtherTax1Expense;
                case E_HousingExpenseTypeT.OtherTaxes2:
                    return dataLoan.sOtherTax2Expense;
                case E_HousingExpenseTypeT.OtherTaxes3:
                    return dataLoan.sOtherTax3Expense;
                case E_HousingExpenseTypeT.OtherTaxes4:
                    return dataLoan.sOtherTax4Expense;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return dataLoan.sRealEstateTaxExpense;
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return dataLoan.sSchoolTaxExpense;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return dataLoan.sWindstormExpense;
                case E_HousingExpenseTypeT.Unassigned:
                    switch (expense.customLineNum)
                    {
                        case E_CustomExpenseLineNumberT.Line1008:
                            return dataLoan.sUnassigned1008Expense;
                        case E_CustomExpenseLineNumberT.Line1009:
                            return dataLoan.sUnassigned1009Expense;
                        case E_CustomExpenseLineNumberT.Line1010:
                            return dataLoan.sUnassigned1010Expense;
                        case E_CustomExpenseLineNumberT.Line1011:
                            return dataLoan.sUnassigned1011Expense;
                        default:
                            throw new UnhandledEnumException(expense.customLineNum);
                    }
                default:
                    throw new UnhandledEnumException(expense.expenseType);
            }
        }

        private static void RemoveDisbursements(string[] removedDisbs, BaseHousingExpense expense)
        {
            foreach (string disbId in removedDisbs)
            {
                if (string.IsNullOrEmpty(disbId))
                {
                    continue;
                }

                Guid id = new Guid(disbId);
                expense.RemoveDisbursement(id);
            }
        }

        private static void BindDisbursements(List<DisbursementViewModel> disbs, BaseHousingExpense expense)
        {
            foreach (DisbursementViewModel dview in disbs)
            {
                if (dview.DisbT == E_DisbursementTypeT.Projected)
                {
                    continue;
                }

                SingleDisbursement disb = expense.GetDisbursementById(new Guid(dview.DisbId));
                if (disb == null && (dview.DisbT == E_DisbursementTypeT.Actual))
                {
                    disb = new SingleDisbursement
                    {
                        DisbursementType = E_DisbursementTypeT.Actual,
                        DisbursementAmtLckd = true
                    };
                    expense.AddDisbursement(disb);
                }

                disb.DisbPaidDateType = dview.PaidDT;
                if (string.IsNullOrEmpty(dview.DueD))
                {
                    disb.DueDate = expense.GetStartDateWindow();
                }
                else
                {
                    disb.DueDate_rep = dview.DueD;
                }
                disb.DisbursementAmt_rep = dview.DueAmt;
                disb.CoveredMonths_rep = dview.DisbMon;
                disb.PaidDate_rep = dview.DisbPaidD;
                disb.PaidBy = dview.PaySource;
                disb.BillingPeriodStartD_rep = dview.PaidFromD;
                disb.BillingPeriodEndD_rep = dview.PaidToD;
                disb.IsPaidFromEscrowAcc = dview.IsPaidFromEscrow;
            }
        }

        private static SampleClosingCostFeeViewModel BindToDataLoan(CPageData dataLoan, string viewModelJson, Guid? idToAddPayment, bool? saveByPassBgCalcForGfeAsDefault)
        {
            SampleClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SampleClosingCostFeeViewModel>(viewModelJson);

            if (viewModel.IsUsingSellerFees)
            {
                dataLoan.sSellerResponsibleClosingCostSet.UpdateWith(viewModel.SellerList);
            }
            else
            {
                dataLoan.sClosingCostSet.UpdateWith(viewModel.SectionList);
            }

            dataLoan.sIPiaDyLckd = viewModel.sIPiaDyLckd;
            dataLoan.sIPiaDy_rep = viewModel.sIPiaDy;
            dataLoan.sIPerDayLckd = viewModel.sIPerDayLckd;
            dataLoan.sIPerDay_rep = viewModel.sIPerDay;

            dataLoan.sLAmtCalc_rep = viewModel.sLAmtCalc;
            dataLoan.sLAmtLckd = viewModel.sLAmtLckd;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;

            dataLoan.sRLckdD_rep = viewModel.sRLckdD;
            dataLoan.sRLckdIsLocked = viewModel.sRLckdDLck;
            dataLoan.sQMExclDiscntPnt_rep = viewModel.sQMExclDiscntPnt;
            dataLoan.sQMExclDiscntPntLck = viewModel.sQMExclDiscntPntLck;
            dataLoan.sDocMagicClosingD_rep = viewModel.sDocMagicClosingD;
            dataLoan.sDocMagicClosingDLckd = viewModel.sDocMagicClosingDLck;
            dataLoan.sQMNonExcludableDiscountPointsPc_rep = viewModel.sQMNonExcludableDiscountPointsPc;
            dataLoan.sQMAveragePrimeOfferR_rep = viewModel.sQMAveragePrimeOfferR;
            dataLoan.sQMAveragePrimeOfferRLck = viewModel.sQMAveragePrimeOfferRLck;
            dataLoan.sQMFhaUfmipAtClosing_rep = viewModel.sQMFhaUfmipAtClosing;
            dataLoan.sQMFhaUfmipAtClosingLck = viewModel.sQMFhaUfmipAtClosingLck;
            dataLoan.sQMParR_rep = viewModel.sQMParR;
            dataLoan.sQMMaxPrePmntPenalty_rep = viewModel.sQMMaxPrePmntPenalty;
            dataLoan.sQMIsVerifiedIncomeAndAssets = viewModel.sQMIsVerifiedIncomeAndAssets;
            dataLoan.sQMIsEligibleByLoanPurchaseAgency = viewModel.sQMIsEligibleByLoanPurchaseAgency;
            dataLoan.sQMLoanPurchaseAgency = viewModel.sQMLoanPurchaseAgency;
            dataLoan.sHighPricedMortgageT = viewModel.sHighPricedMortgageT;
            dataLoan.sHighPricedMortgageTLckd = viewModel.sHighPricedMortgageTLckd;

            return viewModel;
        }

        private static SampleClosingCostFeeViewModel BindToDataLoanExpenses(CPageData dataLoan, string viewModelJson, Guid? idToAddPayment, bool? saveByPassBgCalcForGfeAsDefault)
        {
            SampleClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SampleClosingCostFeeViewModel>(viewModelJson);

            BindHousingExpenses(dataLoan, viewModel.Expenses);

            // Housing Expenses part
            dataLoan.sEstCloseD_rep = viewModel.sEstCloseD;
            dataLoan.sEstCloseDLckd = viewModel.sEstCloseDLckd;
            dataLoan.sAggregateAdjRsrv_rep = viewModel.sAggregateAdjRsrv;
            dataLoan.sAggregateAdjRsrvLckd = viewModel.sAggregateAdjRsrvLckd;
            dataLoan.sSchedDueD1_rep = viewModel.sSchedDueD1;
            dataLoan.sSchedDueD1Lckd = viewModel.sSchedDueD1Lckd;
            dataLoan.sFfUfmipR_rep = viewModel.sFfUfmipR;
            dataLoan.sFfUfmip1003_rep = viewModel.sFfUfmip1003;
            dataLoan.sFfUfmip1003Lckd = viewModel.sFfUfmip1003Lckd;
            dataLoan.sMipFrequency = viewModel.sMipFrequency;
            dataLoan.sMipPiaMon_rep = viewModel.sMipPiaMon;
            dataLoan.sFfUfMipIsBeingFinanced = viewModel.sFfUfMipIsBeingFinanced;
            dataLoan.sUfCashPd_rep = viewModel.sUfCashPd;
            dataLoan.sUfCashPdLckd = viewModel.sUfCashPdLckd;
            dataLoan.sLenderUfmipR_rep = viewModel.sLenderUfmipR;
            dataLoan.sLenderUfmip_rep = viewModel.sLenderUfmip;
            dataLoan.sLenderUfmipLckd = viewModel.sLenderUfmipLckd;
            dataLoan.sMiCompanyNmT = viewModel.sMiCompanyNmT;
            dataLoan.sMiLenderPaidCoverage_rep = viewModel.sMiLenderPaidCoverage;
            dataLoan.sMiInsuranceT = viewModel.sMiInsuranceT;
            dataLoan.sMiCommitmentRequestedD_rep = viewModel.sMiCommitmentRequestedD;
            dataLoan.sMiCommitmentReceivedD_rep = viewModel.sMiCommitmentReceivedD;
            dataLoan.sMiCommitmentExpirationD_rep = viewModel.sMiCommitmentExpirationD;
            dataLoan.sMiCertId = viewModel.sMiCertId;
            dataLoan.sUfmipIsRefundableOnProRataBasis = viewModel.sUfmipIsRefundableOnProRataBasis;
            dataLoan.sProMInsR_rep = viewModel.sProMInsR;
            dataLoan.sProMInsT = viewModel.sProMInsT;
            dataLoan.sProMInsMb_rep = viewModel.sProMInsMb;
            dataLoan.sProMInsLckd = viewModel.sProMInsLckd;
            dataLoan.sProMIns_rep = viewModel.sProMIns;
            dataLoan.sProMInsMon_rep = viewModel.sProMInsMon;
            dataLoan.sProMIns2Mon_rep = viewModel.sProMIns2Mon;
            dataLoan.sProMInsR2_rep = viewModel.sProMInsR2;
            dataLoan.sProMInsCancelLtv_rep = viewModel.sProMInsCancelLtv;
            dataLoan.sProMInsMidptCancel = viewModel.sProMInsMidptCancel;
            dataLoan.sProMInsCancelMinPmts_rep = viewModel.sProMInsCancelMinPmts;
            dataLoan.sMInsRsrvEscrowedTri = viewModel.sMInsRsrvEscrowedTri == true ? E_TriState.Yes : E_TriState.No;
            int result;
            dataLoan.sHousingExpenses.MIEscrowSchedule[1] = int.TryParse(viewModel.MIJan, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[2] = int.TryParse(viewModel.MIFeb, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[3] = int.TryParse(viewModel.MIMar, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[4] = int.TryParse(viewModel.MIApr, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[5] = int.TryParse(viewModel.MIMay, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[6] = int.TryParse(viewModel.MIJun, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[7] = int.TryParse(viewModel.MIJul, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[8] = int.TryParse(viewModel.MIAug, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[9] = int.TryParse(viewModel.MISep, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[10] = int.TryParse(viewModel.MIOct, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[11] = int.TryParse(viewModel.MINov, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[12] = int.TryParse(viewModel.MIDec, out result) ? result : 0;
            dataLoan.sHousingExpenses.MIEscrowSchedule[0] = int.TryParse(viewModel.MICush, out result) ? result : 0;
            dataLoan.sMInsRsrvMon_rep = viewModel.sMInsRsrvMon;
            dataLoan.sMInsRsrvMonLckd = viewModel.sMInsRsrvMonLckd;

            return viewModel;
        }

        private SampleClosingCostFeeViewModel CreateViewModel(CPageData dataLoan)
        {
            IEnumerable<BorrowerClosingCostFeeSection> sectionList;
            IEnumerable<SellerClosingCostFeeSection> sellerList;
            SetSectionList(dataLoan, false, out sectionList, out sellerList);

            var viewModel = new SampleClosingCostFeeViewModel
                {
                    sIPiaDyLckd = dataLoan.sIPiaDyLckd,
                    sIPiaDy = dataLoan.sIPiaDy_rep,
                    sIPerDayLckd = dataLoan.sIPerDayLckd,
                    sIPerDay = dataLoan.sIPerDay_rep,
                    SectionList = sectionList,
                    SellerList = sellerList,
                    Beneficiaries = RolodexDB.GetAgentRolesByTypeDescription().OrderBy(kvp => kvp.Key),
                    CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead),
                    CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite),
                };

            if (this.PriceMyLoanUser.BrokerDB.EnablePageDisplayChangesViaWorkflow)
            {
                this.RegisterWorkflowResults(dataLoan);
            }

            viewModel.IsUsingSellerFees = false;
            viewModel.sDisclosureRegulationT = dataLoan.sDisclosureRegulationT;

            viewModel.viewT = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID ? E_ClosingCostViewT.LoanClosingCost : E_ClosingCostViewT.LoanHud1;

            viewModel.sLAmtCalc = dataLoan.sLAmtCalc_rep;
            viewModel.sLAmtLckd = dataLoan.sLAmtLckd;
            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sLId = LoanID;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewModel.sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps;
            viewModel.sRLckdD = dataLoan.sRLckdD_rep;
            viewModel.sRLckdDLck = dataLoan.sRLckdIsLocked;
            viewModel.sDocMagicClosingD = dataLoan.sDocMagicClosingD_rep;
            viewModel.sDocMagicClosingDLck = dataLoan.sDocMagicClosingDLckd;
            viewModel.sGfeOriginatorCompF = dataLoan.sGfeOriginatorCompF_rep;

            viewModel.sQMQualR = dataLoan.sQMQualR_rep;
            viewModel.sQMExclDiscntPnt = dataLoan.sQMExclDiscntPnt_rep;
            viewModel.sQMExclDiscntPntLck = dataLoan.sQMExclDiscntPntLck;
            viewModel.sQMQualBottom = dataLoan.sQMQualBottom_rep;
            viewModel.sQMNonExcludableDiscountPointsPc = dataLoan.sQMNonExcludableDiscountPointsPc_rep;
            viewModel.sQMAveragePrimeOfferR = dataLoan.sQMAveragePrimeOfferR_rep;
            viewModel.sQMAveragePrimeOfferRLck = dataLoan.sQMAveragePrimeOfferRLck;
            viewModel.sQMTotFeeAmount = dataLoan.sQMTotFeeAmount_rep;
            viewModel.sQMFhaUfmipAtClosing = dataLoan.sQMFhaUfmipAtClosing_rep;
            viewModel.sQMFhaUfmipAtClosingLck = dataLoan.sQMFhaUfmipAtClosingLck;
            viewModel.sQMParR = dataLoan.sQMParR_rep;
            viewModel.sQMTotFinFeeAmount = dataLoan.sQMTotFinFeeAmount_rep;
            viewModel.sQMMaxPrePmntPenalty = dataLoan.sQMMaxPrePmntPenalty_rep;
            viewModel.sQMAprRSpread = dataLoan.sQMAprRSpread_rep;
            viewModel.sQMAprRSpreadCalcDesc = dataLoan.sQMAprRSpreadCalcDesc;
            viewModel.sQMParRSpread = dataLoan.sQMParRSpread_rep;
            viewModel.sQMParRSpreadCalcDesc = dataLoan.sQMParRSpreadCalcDesc;
            viewModel.sQMExcessUpfrontMIP = dataLoan.sQMExcessUpfrontMIP_rep;
            viewModel.sQMExcessUpfrontMIPCalcDesc = dataLoan.sQMExcessUpfrontMIPCalcDesc;
            viewModel.sQMExcessDiscntFPc = dataLoan.sQMExcessDiscntFPc_rep;
            viewModel.sQMExcessDiscntFCalcDesc = dataLoan.sQMExcessDiscntFCalcDesc;
            viewModel.sQMDiscntBuyDownR = dataLoan.sQMDiscntBuyDownR_rep;
            viewModel.sQMDiscntBuyDownRCalcDesc = dataLoan.sQMDiscntBuyDownRCalcDesc;
            viewModel.sQMLAmt = dataLoan.sQMLAmt_rep;
            viewModel.sQMLAmtCalcDesc = dataLoan.sQMLAmtCalcDesc;
            viewModel.sQMMaxPointAndFeesAllowedAmt = dataLoan.sQMMaxPointAndFeesAllowedAmt_rep;
            viewModel.sQMMaxPointAndFeesAllowedPc = dataLoan.sQMMaxPointAndFeesAllowedPc_rep;
            viewModel.sQMExcessDiscntF = dataLoan.sQMExcessDiscntF_rep;
            viewModel.sQMTotFeeAmt = dataLoan.sQMTotFeeAmt_rep;
            viewModel.sQMTotFeePc = dataLoan.sQMTotFeePc_rep;
            viewModel.sQMLoanPassesPointAndFeesTest = dataLoan.sQMLoanPassesPointAndFeesTest;
            viewModel.sQMLoanDoesNotHaveNegativeAmort = dataLoan.sQMLoanDoesNotHaveNegativeAmort;
            viewModel.sQMLoanDoesNotHaveBalloonFeature = dataLoan.sQMLoanDoesNotHaveBalloonFeature;
            viewModel.sQMLoanDoesNotHaveAmortTermOver30Yr = dataLoan.sQMLoanDoesNotHaveAmortTermOver30Yr;
            viewModel.sQMLoanHasDtiLessThan43Pc = dataLoan.sQMLoanHasDtiLessThan43Pc;
            viewModel.sQMIsVerifiedIncomeAndAssets = dataLoan.sQMIsVerifiedIncomeAndAssets;
            viewModel.sQMIsEligibleByLoanPurchaseAgency = dataLoan.sQMIsEligibleByLoanPurchaseAgency;
            viewModel.sQMLoanPurchaseAgency = dataLoan.sQMLoanPurchaseAgency;
            viewModel.sQMStatusT = dataLoan.sQMStatusT;

            viewModel.sHighPricedMortgageT = dataLoan.sHighPricedMortgageT;
            viewModel.sHighPricedMortgageTLckd = dataLoan.sHighPricedMortgageTLckd;

            // Housing Expenses part
            viewModel.sEstCloseD = dataLoan.sEstCloseD_rep;
            viewModel.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            viewModel.sAggregateAdjRsrv = dataLoan.sAggregateAdjRsrv_rep;
            viewModel.sAggregateAdjRsrvLckd = dataLoan.sAggregateAdjRsrvLckd;
            viewModel.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            viewModel.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;
            viewModel.sGfeInitialImpoundDeposit = dataLoan.sGfeInitialImpoundDeposit_rep;
            viewModel.sFfUfmipR = dataLoan.sFfUfmipR_rep;
            viewModel.sFfUfmip1003 = dataLoan.sFfUfmip1003_rep;
            viewModel.sFfUfmip1003Lckd = dataLoan.sFfUfmip1003Lckd;
            viewModel.sMipFrequency = dataLoan.sMipFrequency;
            viewModel.sMipPiaMon = dataLoan.sMipPiaMon_rep;
            viewModel.sFfUfMipIsBeingFinanced = dataLoan.sFfUfMipIsBeingFinanced;
            viewModel.sFfUfmipFinanced = dataLoan.sFfUfmipFinanced_rep;
            viewModel.sUfCashPd = dataLoan.sUfCashPd_rep;
            viewModel.sUfCashPdLckd = dataLoan.sUfCashPdLckd;
            viewModel.sLt = dataLoan.sLT;
            viewModel.sLenderUfmipR = dataLoan.sLenderUfmipR_rep;
            viewModel.sLenderUfmip = dataLoan.sLenderUfmip_rep;
            viewModel.sLenderUfmipLckd = dataLoan.sLenderUfmipLckd;
            viewModel.sMiCompanyNmT = dataLoan.sMiCompanyNmT;

            viewModel.sMiLenderPaidCoverage = dataLoan.sMiLenderPaidCoverage_rep;
            viewModel.sMiInsuranceT = dataLoan.sMiInsuranceT;

            viewModel.sMiCommitmentRequestedD = dataLoan.sMiCommitmentRequestedD_rep;
            viewModel.sMiCommitmentReceivedD = dataLoan.sMiCommitmentReceivedD_rep;
            viewModel.sMiCommitmentExpirationD = dataLoan.sMiCommitmentExpirationD_rep;
            viewModel.sMiCertId = dataLoan.sMiCertId;
            viewModel.sUfmipIsRefundableOnProRataBasis = dataLoan.sUfmipIsRefundableOnProRataBasis;
            viewModel.sProMInsR = dataLoan.sProMInsR_rep;
            viewModel.sProMInsT = dataLoan.sProMInsT;

            viewModel.sProMInsBaseAmt = dataLoan.sProMInsBaseAmt_rep;
            viewModel.sProMInsBaseMonthlyPremium = dataLoan.sProMInsBaseMonthlyPremium_rep;
            viewModel.sProMInsMb = dataLoan.sProMInsMb_rep;
            viewModel.sProMInsLckd = dataLoan.sProMInsLckd;
            viewModel.sProMIns = dataLoan.sProMIns_rep;
            viewModel.sProMInsMon = dataLoan.sProMInsMon_rep;
            viewModel.sProMIns2Mon = dataLoan.sProMIns2Mon_rep;
            viewModel.sProMInsR2 = dataLoan.sProMInsR2_rep;
            viewModel.sProMIns2 = dataLoan.sProMIns2_rep;
            viewModel.sProMInsCancelLtv = dataLoan.sProMInsCancelLtv_rep;
            viewModel.sProMInsMidptCancel = dataLoan.sProMInsMidptCancel;

            viewModel.sMInsRsrvEscrowedTri = dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes ? true : false;
            viewModel.sIssMInsRsrvEscrowedTriReadOnly = dataLoan.sIssMInsRsrvEscrowedTriReadOnly;
            viewModel.sProMInsCancelMinPmts = dataLoan.sProMInsCancelMinPmts_rep;

            viewModel.MIJan = dataLoan.sHousingExpenses.MIEscrowSchedule[1].ToString();
            viewModel.MIFeb = dataLoan.sHousingExpenses.MIEscrowSchedule[2].ToString();
            viewModel.MIMar = dataLoan.sHousingExpenses.MIEscrowSchedule[3].ToString();
            viewModel.MIApr = dataLoan.sHousingExpenses.MIEscrowSchedule[4].ToString();
            viewModel.MIMay = dataLoan.sHousingExpenses.MIEscrowSchedule[5].ToString();
            viewModel.MIJun = dataLoan.sHousingExpenses.MIEscrowSchedule[6].ToString();
            viewModel.MIJul = dataLoan.sHousingExpenses.MIEscrowSchedule[7].ToString();
            viewModel.MIAug = dataLoan.sHousingExpenses.MIEscrowSchedule[8].ToString();
            viewModel.MISep = dataLoan.sHousingExpenses.MIEscrowSchedule[9].ToString();
            viewModel.MIOct = dataLoan.sHousingExpenses.MIEscrowSchedule[10].ToString();
            viewModel.MINov = dataLoan.sHousingExpenses.MIEscrowSchedule[11].ToString();
            viewModel.MIDec = dataLoan.sHousingExpenses.MIEscrowSchedule[12].ToString();
            viewModel.MICush = dataLoan.sHousingExpenses.MIEscrowSchedule[0].ToString();

            viewModel.sMInsRsrvMon = dataLoan.sMInsRsrvMon_rep;
            viewModel.sMInsRsrvMonLckd = dataLoan.sMInsRsrvMonLckd;
            viewModel.sMInsRsrv = dataLoan.sMInsRsrv_rep;

            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            BaseHousingExpense hazExp = dataLoan.sHazardExpense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(hazExp, "", false));

            BaseHousingExpense floodExp = dataLoan.sFloodExpense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(floodExp, "", false));

            BaseHousingExpense windExp = dataLoan.sWindstormExpense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(windExp, "", false));

            BaseHousingExpense condoExp = dataLoan.sCondoHO6Expense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(condoExp, "", false));

            BaseHousingExpense realExp = dataLoan.sRealEstateTaxExpense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(realExp, "", false));

            BaseHousingExpense scholExp = dataLoan.sSchoolTaxExpense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(scholExp, "", false));

            BaseHousingExpense tax1Exp = dataLoan.sOtherTax1Expense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(tax1Exp, "", false));

            BaseHousingExpense tax2Exp = dataLoan.sOtherTax2Expense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(tax2Exp, "", false));

            BaseHousingExpense tax3Exp = dataLoan.sOtherTax3Expense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(tax3Exp, "", false));

            BaseHousingExpense tax4Exp = dataLoan.sOtherTax4Expense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(tax4Exp, "", false));

            BaseHousingExpense hoaExp = dataLoan.sHOADuesExpense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(hoaExp, "", false));

            BaseHousingExpense groundExp = dataLoan.sGroundRentExpense;
            viewModel.Expenses.Add(CreateHousingExpenseModel(groundExp, "", false));

            BaseHousingExpense l1008Exp = dataLoan.sHousingExpenses.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1008);
            if (l1008Exp != null && l1008Exp == dataLoan.sHousingExpenses.GetExpenseByLineNum(E_CustomExpenseLineNumberT.Line1008))
            {
                HousingExpenseViewModel l1008model = CreateHousingExpenseModel(l1008Exp, "", false);
                viewModel.Expenses.Add(l1008model);
            }

            BaseHousingExpense l1009Exp = dataLoan.sHousingExpenses.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1009);
            if (l1009Exp != null && l1009Exp == dataLoan.sHousingExpenses.GetExpenseByLineNum(E_CustomExpenseLineNumberT.Line1009))
            {
                HousingExpenseViewModel l1009model = CreateHousingExpenseModel(l1009Exp, "", false);
                viewModel.Expenses.Add(l1009model);
            }

            BaseHousingExpense l1010Exp = dataLoan.sHousingExpenses.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1010);
            if (l1010Exp != null && l1010Exp == dataLoan.sHousingExpenses.GetExpenseByLineNum(E_CustomExpenseLineNumberT.Line1010))
            {
                HousingExpenseViewModel l1010model = CreateHousingExpenseModel(l1010Exp, "", false);
                viewModel.Expenses.Add(l1010model);
            }

            BaseHousingExpense l1011Exp = dataLoan.sHousingExpenses.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1011);
            if (l1011Exp != null && l1011Exp == dataLoan.sHousingExpenses.GetExpenseByLineNum(E_CustomExpenseLineNumberT.Line1011))
            {
                HousingExpenseViewModel l1011model = CreateHousingExpenseModel(l1011Exp, "", false);
                viewModel.Expenses.Add(l1011model);
            }

            return viewModel;
        }

        private void RegisterWorkflowResults(CPageData dataloan)
        {
            var generator = new WorkflowPageDisplayModelGenerator(dataloan.LoanFileWorkflowRules);
            var model = generator.Generate();
            this.RegisterJsObjectWithJsonNetSerializer("WorkflowPageDisplayModel", model);

            var constants = new WorkflowPageDisplayConstants();
            this.RegisterJsObjectWithJsonNetSerializer("WorkflowPageDisplayConstants", constants);
        }

        private static SampleClosingCostFeeViewModel CreateViewModel(CPageData dataLoan, SampleClosingCostFeeViewModel oldModel)
        {
            SampleClosingCostFeeViewModel viewModel = new SampleClosingCostFeeViewModel();

            List<HousingExpenseViewModel> oldExpensesModel = oldModel.Expenses;

            foreach (HousingExpenseViewModel oldExpViewModel in oldExpensesModel)
            {
                BaseHousingExpense expense = GetExpenseFromLoan(dataLoan, oldExpViewModel);

                bool flipped = oldExpViewModel.flippedOnce || (oldExpViewModel.calcMode == E_AnnualAmtCalcTypeT.Disbursements && expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues);
                HousingExpenseViewModel newExpViewModel = CreateHousingExpenseModel(expense, oldExpViewModel.removedDisbs, flipped);
                newExpViewModel.isVisible = oldExpViewModel.isVisible;
                viewModel.Expenses.Add(newExpViewModel);
            }

            //viewModel.SectionList = SectionList(dataLoan, oldModel.WillUseSellerFees);
            IEnumerable<BorrowerClosingCostFeeSection> sectionList;
            IEnumerable<SellerClosingCostFeeSection> sellerList;
            SetSectionList(dataLoan, oldModel.WillUseSellerFees, out sectionList, out sellerList);
            viewModel.SectionList = sectionList;
            viewModel.SellerList = sellerList;
            viewModel.IsUsingSellerFees = oldModel.WillUseSellerFees;
            viewModel.WillUseSellerFees = oldModel.WillUseSellerFees;

            viewModel.Beneficiaries = oldModel.Beneficiaries;

            viewModel.sDisclosureRegulationT = dataLoan.sDisclosureRegulationT;

            viewModel.sLAmtCalc = dataLoan.sLAmtCalc_rep;
            viewModel.sLAmtLckd = dataLoan.sLAmtLckd;
            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.viewT = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID ? E_ClosingCostViewT.LoanClosingCost : E_ClosingCostViewT.LoanHud1;
            viewModel.sLId = dataLoan.sLId;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewModel.sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps;
            viewModel.sRLckdD = dataLoan.sRLckdD_rep;
            viewModel.sRLckdDLck = dataLoan.sRLckdIsLocked;
            viewModel.sQMQualR = dataLoan.sQMQualR_rep;
            viewModel.sQMExclDiscntPnt = dataLoan.sQMExclDiscntPnt_rep;
            viewModel.sQMExclDiscntPntLck = dataLoan.sQMExclDiscntPntLck;
            viewModel.sDocMagicClosingD = dataLoan.sDocMagicClosingD_rep;
            viewModel.sDocMagicClosingDLck = dataLoan.sDocMagicClosingDLckd;
            viewModel.sQMQualBottom = dataLoan.sQMQualBottom_rep;
            viewModel.sQMNonExcludableDiscountPointsPc = dataLoan.sQMNonExcludableDiscountPointsPc_rep;
            viewModel.sQMAveragePrimeOfferR = dataLoan.sQMAveragePrimeOfferR_rep;
            viewModel.sQMAveragePrimeOfferRLck = dataLoan.sQMAveragePrimeOfferRLck;
            viewModel.sQMTotFeeAmount = dataLoan.sQMTotFeeAmount_rep;
            viewModel.sQMFhaUfmipAtClosing = dataLoan.sQMFhaUfmipAtClosing_rep;
            viewModel.sQMFhaUfmipAtClosingLck = dataLoan.sQMFhaUfmipAtClosingLck;
            viewModel.sQMParR = dataLoan.sQMParR_rep;
            viewModel.sQMTotFinFeeAmount = dataLoan.sQMTotFinFeeAmount_rep;
            viewModel.sQMMaxPrePmntPenalty = dataLoan.sQMMaxPrePmntPenalty_rep;
            viewModel.sQMAprRSpread = dataLoan.sQMAprRSpread_rep;
            viewModel.sQMAprRSpreadCalcDesc = dataLoan.sQMAprRSpreadCalcDesc;
            viewModel.sQMParRSpread = dataLoan.sQMParRSpread_rep;
            viewModel.sQMParRSpreadCalcDesc = dataLoan.sQMParRSpreadCalcDesc;
            viewModel.sQMExcessUpfrontMIP = dataLoan.sQMExcessUpfrontMIP_rep;
            viewModel.sQMExcessUpfrontMIPCalcDesc = dataLoan.sQMExcessUpfrontMIPCalcDesc;
            viewModel.sQMExcessDiscntFPc = dataLoan.sQMExcessDiscntFPc_rep;
            viewModel.sQMExcessDiscntFCalcDesc = dataLoan.sQMExcessDiscntFCalcDesc;
            viewModel.sQMDiscntBuyDownR = dataLoan.sQMDiscntBuyDownR_rep;
            viewModel.sQMDiscntBuyDownRCalcDesc = dataLoan.sQMDiscntBuyDownRCalcDesc;
            viewModel.sQMLAmt = dataLoan.sQMLAmt_rep;
            viewModel.sQMLAmtCalcDesc = dataLoan.sQMLAmtCalcDesc;
            viewModel.sQMMaxPointAndFeesAllowedAmt = dataLoan.sQMMaxPointAndFeesAllowedAmt_rep;
            viewModel.sQMMaxPointAndFeesAllowedPc = dataLoan.sQMMaxPointAndFeesAllowedPc_rep;
            viewModel.sGfeOriginatorCompF = dataLoan.sGfeOriginatorCompF_rep;
            viewModel.sQMExcessDiscntF = dataLoan.sQMExcessDiscntF_rep;
            viewModel.sQMTotFeeAmt = dataLoan.sQMTotFeeAmt_rep;
            viewModel.sQMTotFeePc = dataLoan.sQMTotFeePc_rep;
            viewModel.sQMLoanPassesPointAndFeesTest = dataLoan.sQMLoanPassesPointAndFeesTest;
            viewModel.sQMLoanDoesNotHaveNegativeAmort = dataLoan.sQMLoanDoesNotHaveNegativeAmort;
            viewModel.sQMLoanDoesNotHaveBalloonFeature = dataLoan.sQMLoanDoesNotHaveBalloonFeature;
            viewModel.sQMLoanDoesNotHaveAmortTermOver30Yr = dataLoan.sQMLoanDoesNotHaveAmortTermOver30Yr;
            viewModel.sQMLoanHasDtiLessThan43Pc = dataLoan.sQMLoanHasDtiLessThan43Pc;
            viewModel.sQMIsVerifiedIncomeAndAssets = dataLoan.sQMIsVerifiedIncomeAndAssets;
            viewModel.sQMIsEligibleByLoanPurchaseAgency = dataLoan.sQMIsEligibleByLoanPurchaseAgency;
            viewModel.sQMLoanPurchaseAgency = dataLoan.sQMLoanPurchaseAgency;
            viewModel.sQMStatusT = dataLoan.sQMStatusT;
            viewModel.sHighPricedMortgageT = dataLoan.sHighPricedMortgageT;
            viewModel.sHighPricedMortgageTLckd = dataLoan.sHighPricedMortgageTLckd;

            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;
            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

            // Housing Expenses part
            viewModel.sEstCloseD = dataLoan.sEstCloseD_rep;
            viewModel.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            viewModel.sAggregateAdjRsrv = dataLoan.sAggregateAdjRsrv_rep;
            viewModel.sAggregateAdjRsrvLckd = dataLoan.sAggregateAdjRsrvLckd;
            viewModel.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            viewModel.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;
            viewModel.sGfeInitialImpoundDeposit = dataLoan.sGfeInitialImpoundDeposit_rep;
            viewModel.sFfUfmipR = dataLoan.sFfUfmipR_rep;
            viewModel.sFfUfmip1003 = dataLoan.sFfUfmip1003_rep;
            viewModel.sFfUfmip1003Lckd = dataLoan.sFfUfmip1003Lckd;
            viewModel.sMipFrequency = dataLoan.sMipFrequency;
            viewModel.sMipPiaMon = dataLoan.sMipPiaMon_rep;
            viewModel.sFfUfMipIsBeingFinanced = dataLoan.sFfUfMipIsBeingFinanced;
            viewModel.sFfUfmipFinanced = dataLoan.sFfUfmipFinanced_rep;
            viewModel.sUfCashPd = dataLoan.sUfCashPd_rep;
            viewModel.sUfCashPdLckd = dataLoan.sUfCashPdLckd;
            viewModel.sLt = dataLoan.sLT;
            viewModel.sLenderUfmipR = dataLoan.sLenderUfmipR_rep;
            viewModel.sLenderUfmip = dataLoan.sLenderUfmip_rep;
            viewModel.sLenderUfmipLckd = dataLoan.sLenderUfmipLckd;
            viewModel.sMiCompanyNmT = dataLoan.sMiCompanyNmT;
            viewModel.sMiLenderPaidCoverage = dataLoan.sMiLenderPaidCoverage_rep;
            viewModel.sMiInsuranceT = dataLoan.sMiInsuranceT;
            viewModel.sMiCommitmentRequestedD = dataLoan.sMiCommitmentRequestedD_rep;
            viewModel.sMiCommitmentReceivedD = dataLoan.sMiCommitmentReceivedD_rep;
            viewModel.sMiCommitmentExpirationD = dataLoan.sMiCommitmentExpirationD_rep;
            viewModel.sMiCertId = dataLoan.sMiCertId;
            viewModel.sUfmipIsRefundableOnProRataBasis = dataLoan.sUfmipIsRefundableOnProRataBasis;
            viewModel.sProMInsR = dataLoan.sProMInsR_rep;
            viewModel.sProMInsT = dataLoan.sProMInsT;
            viewModel.sProMInsBaseAmt = dataLoan.sProMInsBaseAmt_rep;
            viewModel.sProMInsBaseMonthlyPremium = dataLoan.sProMInsBaseMonthlyPremium_rep;
            viewModel.sProMInsMb = dataLoan.sProMInsMb_rep;
            viewModel.sProMInsLckd = dataLoan.sProMInsLckd;
            viewModel.sProMIns = dataLoan.sProMIns_rep;
            viewModel.sProMInsMon = dataLoan.sProMInsMon_rep;
            viewModel.sProMIns2Mon = dataLoan.sProMIns2Mon_rep;
            viewModel.sProMInsR2 = dataLoan.sProMInsR2_rep;
            viewModel.sProMIns2 = dataLoan.sProMIns2_rep;
            viewModel.sProMInsCancelLtv = dataLoan.sProMInsCancelLtv_rep;
            viewModel.sProMInsMidptCancel = dataLoan.sProMInsMidptCancel;
            viewModel.sMInsRsrvEscrowedTri = dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes ? true : false;
            viewModel.sIssMInsRsrvEscrowedTriReadOnly = dataLoan.sIssMInsRsrvEscrowedTriReadOnly;
            viewModel.sProMInsCancelMinPmts = dataLoan.sProMInsCancelMinPmts_rep;
            viewModel.MIJan = dataLoan.sHousingExpenses.MIEscrowSchedule[1].ToString();
            viewModel.MIFeb = dataLoan.sHousingExpenses.MIEscrowSchedule[2].ToString();
            viewModel.MIMar = dataLoan.sHousingExpenses.MIEscrowSchedule[3].ToString();
            viewModel.MIApr = dataLoan.sHousingExpenses.MIEscrowSchedule[4].ToString();
            viewModel.MIMay = dataLoan.sHousingExpenses.MIEscrowSchedule[5].ToString();
            viewModel.MIJun = dataLoan.sHousingExpenses.MIEscrowSchedule[6].ToString();
            viewModel.MIJul = dataLoan.sHousingExpenses.MIEscrowSchedule[7].ToString();
            viewModel.MIAug = dataLoan.sHousingExpenses.MIEscrowSchedule[8].ToString();
            viewModel.MISep = dataLoan.sHousingExpenses.MIEscrowSchedule[9].ToString();
            viewModel.MIOct = dataLoan.sHousingExpenses.MIEscrowSchedule[10].ToString();
            viewModel.MINov = dataLoan.sHousingExpenses.MIEscrowSchedule[11].ToString();
            viewModel.MIDec = dataLoan.sHousingExpenses.MIEscrowSchedule[12].ToString();
            viewModel.MICush = dataLoan.sHousingExpenses.MIEscrowSchedule[0].ToString();
            viewModel.sMInsRsrvMon = dataLoan.sMInsRsrvMon_rep;
            viewModel.sMInsRsrvMonLckd = dataLoan.sMInsRsrvMonLckd;
            viewModel.sMInsRsrv = dataLoan.sMInsRsrv_rep;

            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            return viewModel;
        }

        [WebMethod]
        public static string GetAvailableClosingCostFeeTypes(Guid loanId, string viewModelJson, string sectionName)
        {
            try
            {
                CPageData dataLoan = SelectCorrectLoanData(loanId);
                dataLoan.InitLoad();

                BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

                SampleClosingCostFeeViewModel viewModel = BindToDataLoan(dataLoan, viewModelJson, null, null);

                FeeSetupClosingCostSet closingCostSet = db.GetUnlinkedClosingCostSet();

                E_ClosingCostViewT brokerFeeSetupView = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID ? E_ClosingCostViewT.LenderTypeEstimate : E_ClosingCostViewT.LenderTypeHud1;

                FeeSetupClosingCostFeeSection section = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(brokerFeeSetupView, sectionName);

                var fees = viewModel.IsUsingSellerFees
                    ? viewModel.SellerList .SelectMany(sellerSection  => sellerSection .FilteredClosingCostFeeList.Cast<SellerClosingCostFee  >(), (_, fee) => fee.ConvertToFeeSetupFee())
                    : viewModel.SectionList.SelectMany(closingSection => closingSection.FilteredClosingCostFeeList.Cast<BorrowerClosingCostFee>(), (_, fee) => fee.ConvertToFeeSetupFee())
                    ;

                section.SetExcludeFeeList(fees, dataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);

                BaseClosingCostSet set;
                if (viewModel.IsUsingSellerFees)
                {
                    set = dataLoan.sSellerResponsibleClosingCostSet;
                }
                else
                {
                    set = dataLoan.sClosingCostSet;
                }

                List<object> subFeesBySourceFeeTypeId = section.FilteredClosingCostFeeList
                    .Select(f => new {
                        SourceFeeTypeId = f.ClosingCostFeeTypeId,
                        SubFees = set.GetFees(fee => fee.SourceFeeTypeId == f.ClosingCostFeeTypeId).ToList()
                    }).ToList<object>();

                return SerializationHelper.JsonNetAnonymousSerialize(new { Section = section, SubFeesBySourceFeeTypeId = subFeesBySourceFeeTypeId });
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateData(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = SelectCorrectLoanData(loanId);
                dataLoan.InitLoad();

                SampleClosingCostFeeViewModel oldModel = BindToDataLoan(dataLoan, viewModelJson, null, null);

                var updatedModel = CreateViewModel(dataLoan, oldModel);
                return ObsoleteSerializationHelper.JsonSerialize(updatedModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string Save(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = SelectCorrectLoanData(loanId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                SampleClosingCostFeeViewModel oldModel = BindToDataLoan(dataLoan, viewModelJson, null, true);

                dataLoan.Save();

                WorkflowPageDisplayViewModel updatedWorkflowModel = null;
                if (dataLoan.BrokerDB.EnablePageDisplayChangesViaWorkflow)
                {
                    // The workflow UI framework will need to be updated after a loan save
                    // in case the lender has workflow rules for fees that depend on the state
                    // of other fees. ResolvePermission is called here to reload the loan's field
                    // validator and its list of write field and protect field rules.
                    dataLoan.ResolvePermission(PrincipalFactory.CurrentPrincipal);

                    var generator = new WorkflowPageDisplayModelGenerator(dataLoan.LoanFileWorkflowRules);
                    updatedWorkflowModel = generator.Generate();
                }

                var updatedClosingCostModel = CreateViewModel(dataLoan, oldModel);
                return ObsoleteSerializationHelper.JsonSerialize(Tuple.Create(updatedClosingCostModel, updatedWorkflowModel));
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateDataExpenses(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = SelectCorrectLoanData(loanId);
                dataLoan.InitLoad();

                SampleClosingCostFeeViewModel oldModel = BindToDataLoanExpenses(dataLoan, viewModelJson, null, null);

                var updatedModel = CreateViewModel(dataLoan, oldModel);
                return ObsoleteSerializationHelper.JsonSerialize(updatedModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string SaveExpenses(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = SelectCorrectLoanData(loanId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                SampleClosingCostFeeViewModel oldModel = BindToDataLoanExpenses(dataLoan, viewModelJson, null, true);

                dataLoan.Save();

                WorkflowPageDisplayViewModel updatedWorkflowModel = null;
                if (dataLoan.BrokerDB.EnablePageDisplayChangesViaWorkflow)
                {
                    // The workflow UI framework will need to be updated after a loan save
                    // in case the lender has workflow rules for fees that depend on the state
                    // of other fees. ResolvePermission is called here to reload the loan's field
                    // validator and its list of write field and protect field rules.
                    dataLoan.ResolvePermission(PrincipalFactory.CurrentPrincipal);

                    var generator = new WorkflowPageDisplayModelGenerator(dataLoan.LoanFileWorkflowRules);
                    updatedWorkflowModel = generator.Generate();
                }

                var updatedClosingCostModel = CreateViewModel(dataLoan, oldModel);
                return ObsoleteSerializationHelper.JsonSerialize(Tuple.Create(updatedClosingCostModel, updatedWorkflowModel));
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        public static CPageData SelectCorrectLoanData(Guid loanId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(TPOPortalClosingCosts));
            return dataLoan;
        }
    }
}
