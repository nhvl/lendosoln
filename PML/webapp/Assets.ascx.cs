﻿//-----------------------------------------------------------------------
// <summary>The Assets Aspx page contains the html for displaying the Assets.  It's logic, and loading and saving, or handled
// in the loan1003.popup.js an dloan1003.js.</summary>
// <author>Vietnam(html) / Eduardo Michel</author>
//-----------------------------------------------------------------------

namespace _1003page.BorrowerInfo
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Loads the drop down list values.
    /// </summary>
    public partial class Assets : PriceMyLoan.UI.BaseUserControl
    {
        /// <summary>
        /// Loads the drop down list values.
        /// </summary>
        /// <param name="sender">The object that caused the event.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Tools.Bind_ReoTypeT(this.Type);
            Tools.Bind_AssetT(this.AssetT);
            
            Zip.SmartZipcode(City, AssetST);

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(RequestHelper.GetGuid("applicationid", Guid.Empty));

            var borrowerName = dataApp.aBFirstNm + " " + dataApp.aBLastNm;
            var coborrowerName = dataApp.aCFirstNm + " " + dataApp.aCLastNm;
            if (borrowerName.Equals(" "))
                borrowerName = "Borrower";
            if (coborrowerName.Equals(" "))
                coborrowerName = "Co-Borrower";
            owner1.Text = borrowerName;
            owner2.Text = coborrowerName;
        }
    }
}