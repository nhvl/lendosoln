﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace PriceMyLoan.webapp
{
    public partial class TPOPortalClosingCostsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }

        protected BrokerDB Broker
        {
            get { return BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId); }
        }

        private CPageData CreatePageData()
        {
            Guid loanID = GetGuid("loanid");
            return CPageData.CreateUsingSmartDependency(loanID, typeof(TPOPortalClosingCostsService));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "GetAgentIdByAgentType":
                    GetAgentIdByAgentType();
                    break;
            }
        }

        private void GetAgentIdByAgentType()
        {
            E_AgentRoleT agentType = (E_AgentRoleT)GetInt("AgentType");

            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Guid recordId = Guid.Empty;
            string companyName = "";

            CAgentFields agent = dataLoan.GetAgentOfRole(agentType, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (agent != CAgentFields.Empty)
            {
                recordId = agent.RecordId;
                companyName = agent.CompanyName;
            }

            SetResult("RecordId", recordId.ToString());
            SetResult("CompanyName", companyName);
        }
    }
}
