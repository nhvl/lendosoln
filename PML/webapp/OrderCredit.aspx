﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderCredit.aspx.cs" Inherits="PriceMyLoan.webapp.OrderCredit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import namespace="LendersOffice.CreditReport" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="System.Collections.Generic" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body onload="init();">
	<h4 class="page-header">Credit Report</h4>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td valign="top" style="PADDING-LEFT: 5px">
        <table cellSpacing="0" cellPadding="5" border="0">
	        <% if (false) { %>
	        <tr>
		        <td style="BORDER-RIGHT: #ffcc99 1px dashed; BORDER-TOP: #ffcc99 1px dashed; FONT-WEIGHT: bold; BORDER-LEFT: #ffcc99 1px dashed; COLOR: red; BORDER-BOTTOM: #ffcc99 1px dashed; BACKGROUND-COLOR: #ffffcc">CAUTION:&nbsp; 
			        Your data in the following steps has been updated by the system to prepare for 
			        re-running the pricing engine.&nbsp; Data that is not storable in the loan file 
			        may have been reset to a default value.&nbsp; Please check each field for 
			        accuracy.</td>
	        </tr>
	        <% } %>
	        <tr>
		        <td>
			        To proceed, please answer the following questions:<br>
		        </td>
	        </tr>
        </table>
        <table cellSpacing="0" cellPadding="0">
	        <TBODY>
		        <tbody id="BureauPanel">
			        <tr>
				        <td class="FieldLabel">Bureau&nbsp;&nbsp;<asp:customvalidator id="BureauValidator" runat="server" ErrorMessage="At least one bureau is required." ClientValidationFunction="f_validateBureau"></asp:customvalidator></td>
			        </tr>
			        <tr>
				        <td class="FieldLabel"><asp:checkbox id="IsExperian" onclick="f_onValidateRequiredFields();" tabIndex="1" Text="Experian" runat="server"></asp:checkbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					        <asp:checkbox id="IsEquifax" onclick="f_onValidateRequiredFields();" tabIndex="1" Text="Equifax" runat="server"></asp:checkbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox id="IsTransUnion" onclick="f_onValidateRequiredFields();" tabIndex="1" Text="TransUnion" runat="server"></asp:checkbox>&nbsp;&nbsp;&nbsp;&nbsp;
					        </td>
			        </tr>
			        <tr>
				        <td><IMG height="10" src="../images/spacer.gif"></td>
			        </tr>
		        </tbody>
        </table>
        <table id="ManualCreditPanel">
	        <tr>
		        <td class="SubHeader" colSpan="7">Credit Information</td>
	        </tr>
	        <tr>
		        <td class="FieldLabel">Applicant Credit Scores</td>
		        <td class="FieldLabel" title="Experian">XP:</td>
		        <td><asp:textbox id="aBExperianScorePe" tabIndex="1" runat="server" Width="52px"></asp:textbox></td>
		        <td class="FieldLabel" title="TransUnion">TU:</td>
		        <td><asp:textbox id="aBTransUnionScorePe" tabIndex="1" runat="server" Width="52px"></asp:textbox></td>
		        <td class="FieldLabel" title="Equifax">EF:</td>
		        <td><asp:textbox id="aBEquifaxScorePe" tabIndex="1" runat="server" Width="52px"></asp:textbox></td>
	        </tr>
	        <tr>
                <td class=FieldLabel noWrap colSpan=7>Has Co-Applicant?&nbsp;&nbsp;<asp:checkbox id=aBHasSpouse onclick=f_confirmSpouseClick(this); runat="server" text="Yes"></asp:checkbox></td>
            </tr>
	        <tr>
		        <td class="FieldLabel">Co-Applicant Credit Scores</td>
		        <td class="FieldLabel" title="Experian">XP:</td>
		        <td><asp:textbox id="aCExperianScorePe" tabIndex="1" runat="server" Width="52px"></asp:textbox></td>
		        <td class="FieldLabel" title="TransUnion">TU:</td>
		        <td><asp:textbox id="aCTransUnionScorePe" tabIndex="1" runat="server" Width="52px"></asp:textbox></td>
		        <td class="FieldLabel" title="Equifax">EF:</td>
		        <td><asp:textbox id="aCEquifaxScorePe" tabIndex="1" runat="server" Width="52px"></asp:textbox></td>
	        </tr>
	        <TR>
		        <TD class="FieldLabel" colSpan="7">&nbsp;</TD>
	        </TR>
	        <TR>
		        <TD class="Subheader" colSpan="7">Mortgage Delinquency 12 Month Look-Back</TD>
	        </TR>
	        <tr>
		        <td style="PADDING-LEFT: 4px; COLOR: red; PADDING-TOP: 10px" noWrap colSpan="7">How 
			        many mortgage lates does the applicant have?&nbsp; (Assume rolling lates are <u>not</u>
			        allowed.)&nbsp; <a  id="lnkHelpMortLate" onclick="return f_openHelp('Q00007.html', 720, 510);" tabindex="-1" href="#">
				        Explain</a></td>
	        </tr>
	        <TR>
		        <TD class="FieldLabel" colSpan="7">
			        <TABLE id="Table1" width="100%" border="0">
				        <TR>
					        <TD class="FieldLabel" noWrap><asp:dropdownlist id="sProdCrManualNonRolling30MortLateCount" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;x30</TD>
					        <TD class="FieldLabel" noWrap><asp:dropdownlist id="sProdCrManual60MortLateCount" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;x60</TD>
					        <TD class="FieldLabel" noWrap><asp:dropdownlist id="sProdCrManual90MortLateCount" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;x90</TD>
					        <TD class="FieldLabel" noWrap><asp:dropdownlist id="sProdCrManual120MortLateCount" tabIndex="1" runat="server"></asp:dropdownlist>&nbsp;x120</TD>
					        <TD class="FieldLabel" noWrap><asp:dropdownlist id="sProdCrManual150MortLateCount" tabIndex="1" runat="server"></asp:dropdownlist>&nbsp;x150</TD>
				        </TR>
				        <tr>
					        <td style="COLOR: red; PADDING-TOP: 10px" noWrap colSpan="5">How many mortgage 
						        lates would there be if rolling lates <u>were</u> allowed?&nbsp; <a id="lnkHelpMortRolling" onclick="f_openHelp('Q00007.html', 720, 510);" tabIndex="-1" href="#">
							        Explain</a></td>
				        </tr>
				        <tr>
					        <TD noWrap colSpan="5"><asp:dropdownlist id="sProdCrManual30MortLateCount" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;<b>x30</b>
						        <asp:customvalidator id="sProdCrManual30MortLateCountValidator" runat="server" ClientValidationFunction="f_validateRolling30" Display="Dynamic" ControlToValidate="sProdCrManual30MortLateCount"></asp:customvalidator><span id="rollingHint" style="DISPLAY: none">&nbsp; 
							        (<b>Hint:</b>&nbsp; If there are no rolling lates, the x30 count is the same as 
							        in the line above.)</span> <span id="nonrollingHint" style="DISPLAY: none">&nbsp; 
							        (<b>Hint:</b>&nbsp; The x30 total above -- where rolling is <u>not</u> allowed 
							        -- must be at least this many.)</span></TD>
				        </tr>
				        <TR>
					        <TD noWrap colSpan="5"><asp:dropdownlist id="sProdCrManualRolling60MortLateCount" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;<b>x60</b>
						        <asp:customvalidator id="sProdCrManualRolling60MortLateCountValidator" runat="server" ClientValidationFunction="f_validateRolling60" Display="Dynamic" ControlToValidate="sProdCrManualRolling60MortLateCount"></asp:customvalidator><span id="rolling60Hint" style="DISPLAY: none">&nbsp; 
							        (<b>Hint:</b>&nbsp; If there are no rolling lates, the x60 count is the same as 
							        in the line above.)</span> <span id="nonrolling60Hint" style="DISPLAY: none">&nbsp; 
							        (<b>Hint:</b>&nbsp; The x60 total above -- where rolling is <u>not</u> allowed 
							        -- must be at least this many.)</span>
					        </TD>
				        </TR>
				        <TR>
					        <TD noWrap colSpan="5"><asp:dropdownlist id="sProdCrManualRolling90MortLateCount" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;<b>x90</b>
						        <asp:customvalidator id="sProdCrManualRolling90MortLateCountValidator" runat="server" ClientValidationFunction="f_validateRolling90" Display="Dynamic" ControlToValidate="sProdCrManualRolling90MortLateCount"></asp:customvalidator><span id="rolling90Hint" style="DISPLAY: none">&nbsp; 
							        (<b>Hint:</b>&nbsp; If there are no rolling lates, the x90 count is the same as 
							        in the line above.)</span> <span id="nonrolling90Hint" style="DISPLAY: none">&nbsp; 
							        (<b>Hint:</b>&nbsp; The x90 total above -- where rolling is <u>not</u> allowed 
							        -- must be at least this many.)</span>
					        </TD>
				        </TR>

			        </TABLE>
		        </TD>
	        </TR>
	        <TR>
		        <TD class="FieldLabel" colSpan="7">&nbsp;</TD>
	        </TR>
	        <TR>
		        <TD class="Subheader" colSpan="7">Public Records</TD>
	        </TR>
	        <tr>
		        <td style="COLOR: red" noWrap colSpan="7">(Use the most current public record only)</td>
	        </tr>
	        <TR>
		        <TD class="FieldLabel" colSpan="7">
			        <TABLE id="Table2" width="100%" border="0">
				        <TR>
					        <TD class="FieldLabel" noWrap colSpan="2">Check if applicable</TD>
					        <TD class="FieldLabel" noWrap colSpan="2">File Date
					        </TD>
					        <TD class="FieldLabel" noWrap>Status</TD>
					        <TD class="FieldLabel" noWrap colSpan="2">Satisfied Date
					        </TD>
				        </TR>
				        <TR>
					        <TD class="FieldLabel" noWrap>Foreclosure/<span title="Notice of Default">NOD</span>:</TD>
					        <TD noWrap><asp:checkbox id="sProdCrManualForeclosureHas" onclick="f_onManualForeclosureClick();" tabIndex="1" runat="server"></asp:checkbox></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualForeclosureRecentFileMon" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;/
						        <asp:dropdownlist id="sProdCrManualForeclosureRecentFileYr" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist></TD>
					        <TD noWrap><asp:customvalidator id="sProdCrManualForeclosureRecentFileValidator" runat="server" ClientValidationFunction="f_validateRecentFileDate" Display="Dynamic"></asp:customvalidator></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualForeclosureRecentStatusT" tabIndex="1" runat="server" onchange="f_onManualForeclosureClick();"></asp:dropdownlist></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualForeclosureRecentSatisfiedMon" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;/
						        <asp:dropdownlist id="sProdCrManualForeclosureRecentSatisfiedYr" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist><asp:customvalidator id="sProdCrManualForeclosureRecentSatisfiedValidator" runat="server" ClientValidationFunction="f_validateSatisfiedDate" Display="Dynamic" ControlToValidate="sProdCrManualForeclosureRecentStatusT"></asp:customvalidator></TD>
					        <TD noWrap></TD>
				        </TR>
				        <TR>
					        <TD class="FieldLabel" noWrap>Chapter 7:</TD>
					        <TD noWrap><asp:checkbox id="sProdCrManualBk7Has" onclick="f_onManualBk7Click();" tabIndex="1" runat="server"></asp:checkbox></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualBk7RecentFileMon" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;/
						        <asp:dropdownlist id="sProdCrManualBk7RecentFileYr" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist></TD>
					        <TD noWrap><asp:customvalidator id="sProdCrManualBk7RecentFileValidator" runat="server" ClientValidationFunction="f_validateRecentFileDate" Display="Dynamic"></asp:customvalidator></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualBk7RecentStatusT" tabIndex="1" runat="server" onchange="f_onManualBk7Click();"></asp:dropdownlist></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualBk7RecentSatisfiedMon" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;/
						        <asp:dropdownlist id="sProdCrManualBk7RecentSatisfiedYr" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist><asp:customvalidator id="sProdCrManualBk7RecentSatisfiedValidator" runat="server" ClientValidationFunction="f_validateSatisfiedDate" Display="Dynamic" ControlToValidate="sProdCrManualBk7RecentStatusT"></asp:customvalidator></TD>
					        <TD noWrap></TD>
				        </TR>
				        <TR>
					        <TD class="FieldLabel" noWrap>Chapter 13:</TD>
					        <TD noWrap><asp:checkbox id="sProdCrManualBk13Has" onclick="f_onManualBk13Click();" tabIndex="1" runat="server"></asp:checkbox></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualBk13RecentFileMon" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;/
						        <asp:dropdownlist id="sProdCrManualBk13RecentFileYr" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist></TD>
					        <TD noWrap><asp:customvalidator id="sProdCrManualBk13RecentFileValidator" runat="server" ClientValidationFunction="f_validateRecentFileDate" Display="Dynamic"></asp:customvalidator></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualBk13RecentStatusT" tabIndex="1" runat="server" onchange="f_onManualBk13Click();"></asp:dropdownlist></TD>
					        <TD noWrap><asp:dropdownlist id="sProdCrManualBk13RecentSatisfiedMon" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist>&nbsp;/
						        <asp:dropdownlist id="sProdCrManualBk13RecentSatisfiedYr" tabIndex="1" runat="server" onchange="f_onValidateRequiredFields();"></asp:dropdownlist><asp:customvalidator id="sProdCrManualBk13RecentSatisfiedValidator" runat="server" ClientValidationFunction="f_validateSatisfiedDate" Display="Dynamic" ControlToValidate="sProdCrManualBk13RecentStatusT"></asp:customvalidator></TD>
					        <TD noWrap></TD>
				        </TR>
			        </TABLE>
		        </TD>
	        </TR>
        </table>
        <table id="CreditProviderPanel" cellSpacing="0" cellPadding="0" border="0">
	        <TBODY>
		        <tr>
			        <td class="SubHeader" colSpan="2">Credit Provider Information</td>
		        </tr>
		        <tr>
			        <td colSpan="2"><IMG height="10" src="../images/spacer.gif"></td>
		        </tr>
		        <tbody id="FannieMaeCreditPanel">
			        <tr>
				        <td class="FieldLabel">DU UserID</td>
				        <td><asp:textbox id="DUUserID" tabIndex="1" runat="server"></asp:textbox><asp:requiredfieldvalidator id="DUUserIDValidator" runat="server" Display="Dynamic" controltovalidate="DUUserID"></asp:requiredfieldvalidator></td>
			        </tr>
			        <tr>
				        <td class="FieldLabel">DU Password</td>
				        <td><asp:textbox id="DUPassword" tabIndex="1" runat="server" TextMode="Password"></asp:textbox><asp:requiredfieldvalidator id="DUPasswordValidator" runat="server" Display="Dynamic" controltovalidate="DUPassword"></asp:requiredfieldvalidator></td>
			        </tr>
		        </tbody>
		        <tbody id="AccountIDPanel">
			        <tr>
				        <td class="FieldLabel" noWrap><span id="AccountIDLabel">Account&nbsp;ID</span></td>
				        <td noWrap><asp:textbox id="AccountIdentifier" onkeyup="f_onValidateRequiredFields();" tabIndex="1" runat="server"></asp:textbox><asp:requiredfieldvalidator id="AccountIdentifierValidator" runat="server" controltovalidate="AccountIdentifier"></asp:requiredfieldvalidator></td>
			        </tr>
		        </tbody>
		        <tbody id="LoginPanel">
			        <tr>
				        <td class="FieldLabel" noWrap><span id="LoginLabel">Login Name</span></td>
				        <td noWrap><asp:textbox id="LoginName" onkeyup="f_onValidateRequiredFields();" tabIndex="1" runat="server"></asp:textbox><asp:requiredfieldvalidator id="LoginNameValidator" runat="server" controltovalidate="LoginName"></asp:requiredfieldvalidator></td>
			        </tr>
			        <tr>
				        <td class="FieldLabel" noWrap>Password</td>
				        <td noWrap><asp:textbox id="Password" onkeyup="f_onValidateRequiredFields();" tabIndex="1" runat="server" textmode="Password"></asp:textbox><asp:requiredfieldvalidator id="PasswordValidator" runat="server" controltovalidate="Password"></asp:requiredfieldvalidator></td>
			        </tr>
			        <tr>
				        <td></td>
				        <td class="FieldLabel"><asp:checkbox id="RememberLoginNameCB" runat="server"></asp:checkbox><span id="RememberLoginNameLabel" onclick="f_onClickRememberLoginNameLabel();">Remember Login Name</span></td>
			        </tr>
		        </tbody>
		        <tbody id="KrollFDPanel">
			        <tr>
				        <td class="FieldLabel" noWrap>Office Code</td>
				        <td noWrap><asp:textbox id="FD_OfficeCode" onkeyup="f_onValidateRequiredFields();" tabIndex="1" runat="server" width="82px"></asp:textbox><asp:requiredfieldvalidator id="FD_OfficeCodeValidator" runat="server" controltovalidate="FD_OfficeCode"></asp:requiredfieldvalidator></td>
			        </tr>
			        <tr>
				        <td class="FieldLabel" noWrap>Client Code</td>
				        <td noWrap><asp:textbox id="FD_ClientCode" onkeyup="f_onValidateRequiredFields();" tabIndex="1" runat="server" width="81px"></asp:textbox><asp:requiredfieldvalidator id="FD_ClientCodeValidator" runat="server" controltovalidate="FD_ClientCode"></asp:requiredfieldvalidator></td>
			        </tr>
		        </tbody>
		        <tbody id="ReportIDPanel">
			        <tr>
				        <td class="FieldLabel" id="ReportIDLabel" noWrap>Report ID</td>
				        <td noWrap><asp:textbox id="ReportID" onkeyup="f_onValidateRequiredFields();" tabIndex="1" runat="server"></asp:textbox><asp:requiredfieldvalidator id="ReportIDValidator" runat="server" controltovalidate="ReportID"></asp:requiredfieldvalidator>&nbsp;<a id="lnkHelpReportId" onclick="return f_openHelp('Q00002.html', 730, 600);" tabindex="-1" href="#">Where 
						        is the file # on the credit report?</a></td>
			        </tr>
		        </tbody>
		        <tbody id="MclInstantIDPanel">
			        <tr>
				        <td class="FieldLabel" noWrap>Instant View Password&nbsp;&nbsp;&nbsp;</td>
				        <td noWrap><asp:textbox id="InstantViewID" TextMode="password" onkeyup="f_onValidateRequiredFields();" tabIndex="1" runat="server"></asp:textbox><asp:requiredfieldvalidator id="InstantViewIDValidator" runat="server" controltovalidate="InstantViewID"></asp:requiredfieldvalidator>&nbsp;<a id="lnkHelpInstantView" onclick="return f_openHelp('Q00003.html', 650, 550);" tabindex="-1" href="#">What 
						        is an Instant View Password?</a></td>
			        </tr>
		        </tbody>
		<tbody id="MortgageOnlyPanel">
		  <tr>
		    <td class="FieldLabel" colspan="2">
		      <asp:CheckBox ID="IsMortgageOnlyCB" runat="server" Text="Mortgage Only Report"/>
		    </td>
		  </tr>
		</tbody>	
        </table>
        <table id="MsgTablePanel" cellSpacing="0" cellPadding="0">
		        <tbody id="UpgradeMsgPanel">
			        <tr>			   
				        <td style="COLOR: red" class="FieldLabel" noWrap><br>Upgrade feature is only available for MCL credit agencies</td>
			        </tr>
		        </tbody>
        </table>
        <table id="BorrowerPanel" cellSpacing="0" cellPadding="0">
	        <TBODY>
		        <tr>
			        <td colSpan="5">&nbsp;</td>
		        </tr>
		        <tr>
			        <td colSpan="5"><span id="appInfoMustMatch" style="FONT-STYLE: italic">* Note: 
					        Applicant information must match with credit report.</span></td>
		        </tr>
					            <tr>
	                <td class="FieldLabel">Applicant Date of Birth</td>
	                <td> <asp:TextBox runat="server" ID="aBDob" TabIndex="1" onblur="f_onValidateRequiredFields();"  ></asp:TextBox>
        	        
	                </td>
	                <td width="10">&nbsp;</td>
	                <td class="FieldLabel">Co-Applicant Date of Birth</td>
                    <td> <asp:TextBox runat="server" ID="aCDob" TabIndex="10" onblur="f_onValidateRequiredFields();" ></asp:TextBox>
	                </td>
	            </tr>
	            <tr>
	                <td></td>
	                <td>
                    <asp:CompareValidator ID="aBDobValidator" CssClass="FieldLabel" Display="Dynamic" Type="Date" runat="server" Operator="DataTypeCheck" ControlToValidate="aBDob" ErrorMessage="Please enter a valid date."></asp:CompareValidator>
        	        
	                </td>
	                   <td width="10">&nbsp;</td>
	                <td></td>
	                <td>
                    <asp:CompareValidator ID="aCDobValidator"  CssClass="FieldLabel" Display="Dynamic" Type="Date" runat="server" Operator="DataTypeCheck" ControlToValidate="aCDob" ErrorMessage="Please enter a valid date."></asp:CompareValidator>
        	        
	                </td>
	            </tr>
		        <tbody id="PresentAddressPanel">
			        <tr style="PADDING-TOP: 10px">
				        <td class="FieldLabel">Present Address</td>
				        <td colSpan="4"><asp:textbox id="aBAddr" onkeyup="f_onValidateRequiredFields();" tabIndex="20" runat="server" Width="392px"></asp:textbox><asp:requiredfieldvalidator id="aBAddrValidator" runat="server" controltovalidate="aBAddr"></asp:requiredfieldvalidator></td>
			        </tr>
			        <tr>
				        <td></td>
				        <td class="FieldLabel" colSpan="4">Zip&nbsp;<ml:zipcodetextbox id="aBZip" onkeyup="f_onValidateRequiredFields();" tabIndex="20" runat="server" width="50" preset="zipcode" onchange="f_onValidateRequiredFields();"></ml:zipcodetextbox><asp:requiredfieldvalidator id="aBZipValidator" runat="server" Display="Dynamic" controltovalidate="aBZip"></asp:requiredfieldvalidator>&nbsp;City&nbsp;<asp:textbox id="aBCity" onkeyup="f_onValidateRequiredFields();" tabIndex="20" runat="server" width="178px"></asp:textbox><asp:requiredfieldvalidator id="aBCityValidator" runat="server" Display="Dynamic" controltovalidate="aBCity"></asp:requiredfieldvalidator>&nbsp;State&nbsp;<ml:statedropdownlist id="aBState" tabIndex="20" runat="server" onchange="f_onValidateRequiredFields();"></ml:statedropdownlist><asp:customvalidator id="aBStateValidator" runat="server" ClientValidationFunction="f_validateState" display="Dynamic"></asp:customvalidator></td>
			        </tr>
		        </tbody>
        </table>
        <table id="OrderCreditPanel" width="85%">
            <tr>
		        <td align="center">
	                <input style="width:130px; margin-top:1em" id="oCreditBtn" type="button" tabindex="60" class="ButtonStyle" value="Order Credit" onclick="f_orderCreditClick(event);">
	                <input value="Close" type="button" tabindex="61" class="ButtonStyle" onclick="parent.LQBPopup.Hide();" />
		        </td>
	        </tr>
        </table>
        <div class="ErrorMessage"><ml:PassthroughLiteral id="ErrorMessage" runat="server" EnableViewState="False"></ml:PassthroughLiteral></div>
        </td>
		</tr>
        </table>
        <script type="text/javascript">
        <!--
        var creditHash = new Object();
        <% foreach (KeyValuePair<Guid, int> item in m_nonMclProtocolList) { %>
        creditHash[<%= AspxTools.JsString(item.Key) %>] = <%=AspxTools.JsNumeric(item.Value) %>;
        <% } %>

        var g_isStoredCredentials = <%= AspxTools.JsBool(m_isStoredCredentials) %>; 

        var g_bIsReadOnly = <%= AspxTools.JsBool(IsReadOnly) %>;
        <%-- Define javascript function pointers that will display layout depend on credit protocol. --%>

        var oDisplayFPT = new Object();
        oDisplayFPT[-1] = f_displayEmpty; 
        oDisplayFPT[0]  = f_displayMcl;
        oDisplayFPT[1]  = f_displayMismo21;
        oDisplayFPT[2]  = f_displayLandSafe;
        oDisplayFPT[4]  = f_displayKroll;
        oDisplayFPT[5]  = f_displayCredco;
        oDisplayFPT[7]  = f_displayUniversalCredit;
        oDisplayFPT[8]  = f_displayFiserv;
        oDisplayFPT[9]  = f_displayFannieMae;
        oDisplayFPT[10] = f_displayMismo21;
        oDisplayFPT[11] = f_displaySharperLending;
        oDisplayFPT[12] = f_displayCreditInterlink;
        oDisplayFPT[13] = f_displayCreditCSC;
        oDisplayFPT[14] = f_displayCreditCSD;
        oDisplayFPT[15] = f_displayCreditCBC;
        oDisplayFPT[16] = f_displayCreditFundingSuite;
        oDisplayFPT[17] = f_displayInformativeResearch;
        oDisplayFPT[18] = f_displayEquifax;
        oDisplayFPT[255] = f_displayCreditProxy;

        var oValidateFPT = new Object();
        oValidateFPT[-1] = f_validateEmpty;
        oValidateFPT[1]  = f_validateMismo21;
        oValidateFPT[0] = f_validateMcl;
        oValidateFPT[2] = f_validateLandSafe;
        oValidateFPT[4] = f_validateKroll;
        oValidateFPT[5] = f_validateCredco;
        oValidateFPT[7] = f_validateUniversalCredit;
        oValidateFPT[8] = f_validateFiserv;
        oValidateFPT[9] = f_validateFannieMae;
        oValidateFPT[10] = f_validateMismo21;
        oValidateFPT[11] = f_validateSharperLending;
        oValidateFPT[12] = f_validateCreditInterlink;
        oValidateFPT[13] = f_validateCreditCSC;
        oValidateFPT[14] = f_validateCreditCSD;
        oValidateFPT[15] = f_validateCreditCBC;
        oValidateFPT[16] = f_validateCreditFundingSuite;
        oValidateFPT[17] = f_validateInformativeResearch;
        oValidateFPT[18] = f_validateEquifax;
        oValidateFPT[255] = f_validateCreditProxy;

        function f_getCreditAction() {
            return <%=AspxTools.JsNumeric(CreditAction) %>;
        }
        function f_prePostBack(event, callback) {
          <% if (!IsReadOnly && m_hasLiabilities && PriceMyLoanUser.Type == "B") { %>
          var creditAction = f_getCreditAction();
          
          if (creditAction == "1" || creditAction == "2" || creditAction == "4") {
            <%-- // 3/29/2007 nw - OPM 10105 - let users know the CRA they want to pull or reissue credit is having technical difficulty --%>
            var args = new Object();
            args["comId"] = <%= AspxTools.JsString(CreditProtocol) %>;
            var warningResult = gService.main.call("CheckCRAWarningMessage", args);
            if (!warningResult.error)
            {
              if ((null != warningResult.value["IsWarningOn"]) && warningResult.value["IsWarningOn"])
              {
                var sWarningMsg = warningResult.value["WarningMsg"];
                var sWarningStartD = warningResult.value["WarningStartD"];
                var sURL = "/main/DisplayWarningMessage.aspx?WarningMsg=" + sWarningMsg + "&WarningStartD=" + sWarningStartD;
                showModal(sURL, null, null, null, function(ret){
                	if (ret.Logout) f_logout();
	                if (ret.choice != 0)	<%-- // if user clicks "Cancel" or closes the modal dialog window --%>
	                {
	                    f_enableAllButtons(true);
	                    if(event) {
	                        event.returnValue = false;
	                    }
	                  callback(false);
                        return;
	                }

                    f_prePostBackCallback(event, callback);
                },{hideCloseButton:true});

              return;
              }
            }
            else
            {
              var errMsg = 'Error occurred while checking for CRA warning message.';
              <%= AspxTools.JsGetElementById(ErrorMessage) %>.Text = errMsg;
                f_enableAllButtons(true);
                if(event) {
                    event.returnValue = false;
                }
              callback(false);
                return;
            }

              f_prePostBackCallback(event, callback);
              return;
          }

            <% } %>

            callback(true) ;
            return;
        }

            function f_prePostBackCallback(event, callback) {
                showModal("/main/ExistingLiabilitiesConfirmation.aspx", null, null, null, function(ret){
                    if (ret.Logout) f_logout();
                    if (ret.choice == 0 || ret.choice == 1) {
                        if (ret.choice == 0) document.forms[0]["DeleteLiabilities"].value = "1";
                        else document.forms[0]["DeleteLiabilities"].value = "0";
                        callback(true);
                        return;
                    }
                    else {
                        var $oCreditBtn = jQuery('#oCreditBtn');
                        $oCreditBtn.prop( 'disabled', false);
                        $oCreditBtn.prop( 'value', $oCreditBtn.prop("oldValue"));


                        f_enableAllButtons(true);    
                        if(event) {
                            event.returnValue =false;
                        }
	                
                        callback(false);
                        return;
                    }
                },{hideCloseButton:true});
            }
        
        var gDisabledSsn = false;
        var gCurrentSsnId = null;
        function f_validateSsn(source, args) {
          var o = document.getElementById(source.controltovalidate);
          if (gDisabledSsn && gCurrentSsnId == source.controltovalidate)
            source.innerHTML = "";
          else
            source.innerHTML = "Invalid format of SSN";
          
          if (null != o && o.value != "") {
            args.IsValid = o.value.length == 11;
            return;
          }
          args.IsValid = true;
          
        }
        function f_ssn(o) {
          if (o.value.length < 11) {
            gDisabledSsn = true;
            gCurrentSsnId = o.id;
          }

          f_onValidateRequiredFields();
          gDisabledSsn = false;
            
        }
        function f_keydown_ssn() { }
        function f_validateBureau(src, args) {
          var bIsOrderNew = f_getCreditAction() == "2"; <%-- //New  --%>
          var bIsExperian = <%= AspxTools.JsGetElementById(IsExperian) %>.checked;
          var bIsEquifax = <%= AspxTools.JsGetElementById(IsEquifax) %>.checked;
          var bIsTransUnion = <%= AspxTools.JsGetElementById(IsTransUnion) %>.checked;
          args.IsValid = !bIsOrderNew || (bIsExperian || bIsEquifax || bIsTransUnion);
        }
        function f_validateState(src, args) {
          var bIsOrderNew = f_getCreditAction() == "2";

          args.IsValid = !bIsOrderNew || <%= AspxTools.JsGetElementById(aBState) %>.value != "";
        }
        function retrieveProtocolType(protocolID) {
          if ('00000000-0000-0000-0000-000000000000' == protocolID)
            return -1;
            
          var o = creditHash[protocolID];
          if (null == o)
            return 0; <%-- // By default return MCL protocol. --%>
          else
            return o;
        }
        function f_enableCreditOrder(bEnabled) {
            $j('<%= AspxTools.JSelector(LoginName,Password,FD_OfficeCode,FD_ClientCode,ReportID,InstantViewID) %>').readOnly(!bEnabled);
        }
        function f_validateEmpty() {
        }
        function f_displayEmpty() {
          f_displayPanel(10); <%-- // Display CreditProviderPanel --%>
        }

        function f_displayMismo21() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
          
          if( creditAction == "4" ){
            visiblePanels.push(12);
            visiblePanels.push(10);
          }
          else{
            if (creditAction == "2") {
                visiblePanels.push(1); <%-- // Display BureauPanel --%>
                visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
            } 
            else {
                visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
            }
                f_addLoginPanel(visiblePanels);
                f_addAccountIDPanel(visiblePanels);
                visiblePanels.push(6); <%-- // Display LoginPanel --%>
                visiblePanels.push(9); <%-- // Display AccountIDPanel     --%>
              
              visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
              f_addCreditProviderPanel(visiblePanels);
          }
          
          f_displayPanelByArray(visiblePanels);
          
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");

          var protocolType = retrieveProtocolType(<%= AspxTools.JsString(CreditProtocol) %>);
            
            
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
          
        }

        function f_displayEquifax()
        {
            var creditAction = f_getCreditAction();

            var visiblePanels = new Array();
          
            if( creditAction == "4" ){
                visiblePanels.push(12);
                visiblePanels.push(10);
            }
            else 
            {
                if (creditAction == "2") 
                {
                    visiblePanels.push(1); <%-- // Display BureauPanel --%>
                    visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
                } 
                else 
                {
                    visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
                }

                f_addLoginPanel(visiblePanels);
                visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addCreditProviderPanel(visiblePanels);
            }
          
            f_displayPanelByArray(visiblePanels);
          
            if (creditAction != "2")
            {
                $j(document.getElementById("ReportIDLabel")).text("Report ID");
            }

            $j(document.getElementById("LoginLabel")).text("Login Name");

            var protocolType = retrieveProtocolType(<%= AspxTools.JsString(CreditProtocol) %>);
            
            $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
        }

        function f_displaySharperLending() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
           if( creditAction == "4" ){
	        //visiblePanels.push(12);
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
               f_addLoginPanel(visiblePanels);
               f_addAccountIDPanel(visiblePanels);
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>	
               f_addCreditProviderPanel(visiblePanels);
          }
          else{
	        if (creditAction == "2" ) {
		        visiblePanels.push(1); <%-- // Display BureauPanel --%>
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
	        } else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
	        }
               f_addLoginPanel(visiblePanels);
               f_addAccountIDPanel(visiblePanels);
                   visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
               f_addCreditProviderPanel(visiblePanels);
         }
          f_displayPanelByArray(visiblePanels);
          
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Reference #");
          $j(document.getElementById("LoginLabel")).text("User Name");
          $j(document.getElementById("AccountIDLabel")).text("Client Number");

          var protocolType = retrieveProtocolType(<%= AspxTools.JsString(CreditProtocol) %>);
           
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
          
        }

        function f_displayUniversalCredit() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "2") {
		        visiblePanels.push(1); <%-- // Display BureauPanel --%>
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
	        }  else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
	        }
                f_addLoginPanel(visiblePanels);
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addCreditProviderPanel(visiblePanels);
        }
          f_displayPanelByArray(visiblePanels);
          
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
          
          
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
        }

        function f_displayFannieMae() {
          var creditAction = f_getCreditAction();
          var bHasBrokerDUInfo = <%= AspxTools.JsBool(m_hasBrokerDUInfo) %>;

          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
        else{
	        if (creditAction == "1") {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
        	  
	        } 
	        if (!bHasBrokerDUInfo)
		        visiblePanels.push(11); <%-- // Display Fannie Mae Credit Panel --%>
        	  
	        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addLoginPanel(visiblePanels);
                f_addCreditProviderPanel(visiblePanels);
        }
          f_displayPanelByArray(visiblePanels);
            
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");

          
          <%-- // 7/26/2006 nw - OPM 4455 - If user selects "CREDCO (FANNIE MAE)", change "Login Name" to "Account Number". --%>
          var value = <%= AspxTools.JsString(CreditProtocol) %>;
          if (value == "44bd6469-4330-4891-87c2-9ab82787d6b6")
            $j(document.getElementById("LoginLabel")).text("Account Number");
          <%-- // 3/12/2007 nw - OPM 5099 - If user selects "Kroll Factual Data (052)", change "Login Name" to "Account Number" --%>
          else if (value == "da66612f-d574-408a-9785-6eafbf1a57dd")
            $j(document.getElementById("LoginLabel")).text("Account Number");
          <%-- // nw - OPM 5099 - If user selects "Info1/LandAmerica (018)", change "Login Name" to "Client ID" --%>
          else if (value == "7c6e1bdd-a963-48aa-8eca-2a4c0b355132")
            $j(document.getElementById("LoginLabel")).text("Client ID");
          <%-- // nw - OPM 5099 - If user selects "NMR E-MERGE (046)", change "Login Name" to "Company ID" --%>
          else if (value == "55e6cc10-2ae9-4772-88ec-ad2dfeb730bb")
            $j(document.getElementById("LoginLabel")).text("Company ID");
          
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
        }
        function f_displayCreditProxy() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "1") {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
        	  
	        } 
	        if (creditAction == "2") {
	            visiblePanels.push(1); <%-- Display BureauPanel Check --%>
	        }
	        visiblePanels.push(7); <%-- // Display PresentAddressPanel   --%>

	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
	        visiblePanels.push(10); <%-- // Display CreditProviderPanel   --%>
         }
          f_displayPanelByArray(visiblePanels);
        }
        function f_displayMcl() {
          var creditAction = f_getCreditAction();
          
          var visiblePanels = new Array();
          if (creditAction == "2") {
            visiblePanels.push(1); <%-- // Display BureauPanel --%>
              visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
              f_addLoginPanel(visiblePanels);
            visiblePanels.push(0); <%-- // Display BorrowerPanel --%>

          } else if(creditAction == "4" ){
              visiblePanels.push(7);
            f_addLoginPanel(visiblePanels);
	        visiblePanels.push(8);
	        visiblePanels.push(0);
          }else {

            visiblePanels.push(3); <%-- // Display MCLInstantIDPanel --%>
            visiblePanels.push(8); <%-- // Display ReportIDPanel --%>

          }
            f_addCreditProviderPanel(visiblePanels);
          f_displayPanelByArray(visiblePanels);
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("File ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
            

        }

        <%-- // 12/26/2006 nw - OPM 9069 - Credit Interlink --%>
        function f_displayCreditInterlink() {
          var creditAction = f_getCreditAction();
          var visiblePanels = new Array();
          if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "2") {
		        visiblePanels.push(1); <%-- // Display BureauPanel --%>
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
	        }
	        else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
	        }
              f_addLoginPanel(visiblePanels);
                  visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
              f_addCreditProviderPanel(visiblePanels);
         }
          f_displayPanelByArray(visiblePanels);
          
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
        }

        function f_displayCreditCSC() {
	        var creditAction = f_getCreditAction();
	        var visiblePanels = new Array();
	        if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "2") {
		        visiblePanels.push(1); <%-- // Display BureauPanel --%>
	        }
	        else 
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
	            visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
	            f_addLoginPanel(visiblePanels);
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
	            f_addCreditProviderPanel(visiblePanels);
         }
	        f_displayPanelByArray(visiblePanels);
        	  
	        if (creditAction != "2")
		        $j(document.getElementById("ReportIDLabel")).text("Report ID");
	        $j(document.getElementById("AccountIDLabel")).text("Account ID");
	        $j(document.getElementById("LoginLabel")).text("Login Name");
            $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
        }

        function f_displayCredco() {
            var creditAction = f_getCreditAction();
            var visiblePanels = new Array();

            if (creditAction == "2") 
            {
                visiblePanels.push(1); <%-- // Display BureauPanel --%>
            } 
            else 
            {
                visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
            }
    
            visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
            f_addLoginPanel(visiblePanels);
                visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
            f_addCreditProviderPanel(visiblePanels);
    
            f_displayPanelByArray(visiblePanels);
      
            if (creditAction != "2")
            {
                $j(document.getElementById("ReportIDLabel")).text("Report ID");
            }
            $j(document.getElementById("AccountIDLabel")).text("Account ID");
            $j(document.getElementById("LoginLabel")).text("Account Number");
            $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
        }

        <%-- //OPM 18327 --%>
        function f_displayCreditCSD() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "2") {
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
        	    
	        } 
	        else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
        	  
	        }
	        <%-- //visiblePanels.push(1); // Display BureauPanel --%>
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addLoginPanel(visiblePanels);
                f_addCreditProviderPanel(visiblePanels);
         }
          f_displayPanelByArray(visiblePanels);
            
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
          
        }

        <%-- //OPM 19664 --%>
        function f_displayCreditCBC() {
            var creditAction = f_getCreditAction();
           var visiblePanels = [];
            if (creditAction == "2") {
                visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
            } 
	        else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>  
	        }
	        <%-- //visiblePanels.push(1); // Display BureauPanel --%>
            visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
            f_addLoginPanel(visiblePanels);
            f_addAccountIDPanel(visiblePanels);
            f_addCreditProviderPanel(visiblePanels);
            visiblePanels.push(13); <%-- // MortgageOnlyPanel --%>          
            f_displayPanelByArray(visiblePanels);
            
            if (creditAction != "2")
                $j(document.getElementById("ReportIDLabel")).text("Report ID");
          
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
          
        }

        function f_displayLandSafe() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "2") {
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
        	    
	        } else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
        	  
	        }
	        visiblePanels.push(1); <%-- // Display BureauPanel --%>
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addLoginPanel(visiblePanels);
                f_addCreditProviderPanel(visiblePanels);
        }
          f_displayPanelByArray(visiblePanels);
            
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
          
        }
        function f_displayKroll() {
          var creditAction = f_getCreditAction();
          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "2") {
		        visiblePanels.push(1); <%-- // Display BureauPanel --%>
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel   --%>
	        }else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel   --%>
	        }  
	        visiblePanels.push(2); <%-- // Display KrollFDPanel --%>
                visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addLoginPanel(visiblePanels);
                f_addCreditProviderPanel(visiblePanels);
         }
          f_displayPanelByArray(visiblePanels);
            
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
            
        }
        function f_displayFiserv() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
          
	        if (creditAction == "2") {
            
	        } else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
        	  
	        }
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
        	  
	        visiblePanels.push(1); <%-- // Display BureauPanel --%>
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addLoginPanel(visiblePanels);
                f_addCreditProviderPanel(visiblePanels);
        }
          f_displayPanelByArray(visiblePanels);
            
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
         }

        function f_displayCreditFundingSuite() {
          var creditAction = f_getCreditAction();

          var visiblePanels = new Array();
            if( creditAction == "4" ){
	        visiblePanels.push(12);
	        visiblePanels.push(10);
          }
          else{
	        if (creditAction == "2") {
		        visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
        	    
	        } else {
		        visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
        	  
	        }
	        visiblePanels.push(1); <%-- // Display BureauPanel --%>
	        visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
                f_addLoginPanel(visiblePanels);
                f_addCreditProviderPanel(visiblePanels);
        }
          f_displayPanelByArray(visiblePanels);
            
          if (creditAction != "2")
            $j(document.getElementById("ReportIDLabel")).text("Report ID");
          $j(document.getElementById("AccountIDLabel")).text("Account ID");
            
          $j(document.getElementById("LoginLabel")).text("Login Name");
          $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
          
        }

        function f_displayInformativeResearch() {
            var creditAction = f_getCreditAction();
            var visiblePanels = new Array();

            if (creditAction == "2") 
            {
                visiblePanels.push(1); <%-- // Display BureauPanel --%>
            } 
            else 
            {
	            visiblePanels.push(8); <%-- // Display ReportIDPanel --%>
            }
            
            visiblePanels.push(0); <%-- // Display BorrowerPanel --%>
            f_addLoginPanel(visiblePanels);
            f_addAccountIDPanel(visiblePanels);
            visiblePanels.push(7); <%-- // Display PresentAddressPanel --%>
            f_addCreditProviderPanel(visiblePanels);
            
            f_displayPanelByArray(visiblePanels);
              
            if (creditAction != "2")
            {
                $j(document.getElementById("ReportIDLabel")).text("Report ID");
            }
            $j(document.getElementById("AccountIDLabel")).text("Client ID");
            $j(document.getElementById("LoginLabel")).text("User ID");
            $j(document.getElementById("RememberLoginNameLabel")).text("Remember " + $j(document.getElementById("LoginLabel")).text());
        }

        function f_validateMcl() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2" || creditAction =="4";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          var bIsUpgrade = creditAction == "4";

          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = (bIsOrderNew || bIsUpgrade) && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = (bIsOrderNew || bIsUpgrade) && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = (bIsUpgrade || bIsOrderNew) && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = (bIsUpgrade || bIsOrderNew) && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = (bIsUpgrade || bIsReissue) && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = ( bIsReissue) && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            =  (bIsUpgrade || bIsOrderNew) && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            =  (bIsUpgrade || bIsOrderNew) && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             =  (bIsUpgrade || bIsOrderNew) && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;
        }
        function f_validateMismo21() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";

          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = true && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;
        }
        function f_validateEquifax() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";

          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;
        }
        function f_validateSharperLending() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2" || creditAction == "4";
          var bIsReissue = creditAction == "1" || creditAction == "4";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = true && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;

        }

        function f_validateUniversalCredit() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;

        }

        <%-- // 12/26/2006 nw - OPM 9069 - Credit Interlink --%>
        function f_validateCreditInterlink() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;
        }

        function f_validateCreditProxy() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = false && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = false && !g_bIsReadOnly;
        }
        function f_validateKroll() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";  
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = bIsReissueOrNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;  
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;
          
        }

        function f_validateLandSafe() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;  
        }

        function f_validateCreditCSD() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;  
        }

        function f_validateCreditCBC() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;  
        }

        function f_validateCreditCSC() {
	        var creditAction = f_getCreditAction();
	        var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
	        var bIsReissue = creditAction == "1";
	        var bIsOrderNew = creditAction == "2";
        	
	        var oLoginName = <%= AspxTools.JsGetElementById(LoginName) %>;
	        var oReportId = <%= AspxTools.JsGetElementById(ReportID) %>;
	        oLoginName.value = oLoginName.value.toUpperCase();
	        oReportId.value = oReportId.value.toUpperCase();
            <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
	        <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
	        <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
	        <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
	        <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
	        <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
	        <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
	        <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
	        <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;  
        }

        function f_validateCredco() {
            var creditAction = f_getCreditAction();
            var bIsReissueOrNew = creditAction == "1" || creditAction == "2" || creditAction =="4";
            var bIsReissue = creditAction == "1";
            var bIsOrderNew = creditAction == "2";
            var bIsUpgrade = creditAction == "4";

            <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = (bIsUpgrade || bIsReissue) && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false;
            <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false;
            <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false; 
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false;
            <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;
          
          <%-- // 7/26/2006 nw - OPM 4455 - strip out '-' in Report ID when reissuing Credco credit reports. --%>
          if (bIsReissue)
            <%= AspxTools.JsGetElementById(ReportID) %>.value = <%= AspxTools.JsGetElementById(ReportID) %>.value.replace(/\-/g, "");
        }
        function f_validateFiserv() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;  
        }
        function f_validateFannieMae() {
          var creditAction = f_getCreditAction();
          var bHasBrokerDUInfo = <%= AspxTools.JsBool(m_hasBrokerDUInfo) %>;
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(DUUserIDValidator) %>.enabled          = bIsReissueOrNew && !bHasBrokerDUInfo && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(DUPasswordValidator) %>.enabled        = bIsReissueOrNew && !bHasBrokerDUInfo && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = false && !g_bIsReadOnly;
          
          <%-- // 7/26/2006 nw - OPM 4455 - strip out '-' in Report ID when reissuing Credco credit reports. --%>
          var value = <%= AspxTools.JsString(CreditProtocol) %>;
          if (bIsReissue && value == "44bd6469-4330-4891-87c2-9ab82787d6b6")
            <%= AspxTools.JsGetElementById(ReportID) %>.value = <%= AspxTools.JsGetElementById(ReportID) %>.value.replace(/\-/g, "");
        }


        function f_validateCreditFundingSuite() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = bIsReissue && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsOrderNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;  
        }

        function f_validateInformativeResearch() {
          var creditAction = f_getCreditAction();
          var bIsReissueOrNew = creditAction == "1" || creditAction == "2" || creditAction =="4";
          var bIsReissue = creditAction == "1";
          var bIsOrderNew = creditAction == "2";
          var bIsUpgrade = creditAction == "4";

          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled         = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
            <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled          = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled          = (bIsUpgrade || bIsReissue) && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled     = false;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled            = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled             = bIsReissueOrNew && !g_bIsReadOnly;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled     = false;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled     = false;
            <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = bIsReissueOrNew && !g_bIsReadOnly && !g_isStoredCredentials;
          <%= AspxTools.JsGetElementById(BureauValidator) %>.enabled            = true && !g_bIsReadOnly;
        }

        function f_onClickRememberLoginNameLabel() {
          <%-- // 12/28/2006 nw - OPM 5083 - Remember login name
          // Since I can't manipulate the check box's text, I will replace it with a label that looks and acts like its text --%>
          <%= AspxTools.JsGetElementById(RememberLoginNameCB) %>.checked = !<%= AspxTools.JsGetElementById(RememberLoginNameCB) %>.checked;
        }

        function f_onCreditProtocolChange() {
          f_disableAllValidators();
          var protocolType = retrieveProtocolType(<%= AspxTools.JsString(CreditProtocol) %>);
            oDisplayFPT[protocolType]();

          f_onValidateRequiredFields();
          return ;
        }
        function f_onchange_CreditAction() {
          f_disableAllValidators();

          var creditAction = f_getCreditAction();
          
          var manualCreditWarning = document.getElementById("ManualCreditWarning");
          
          if (null != manualCreditWarning)
            manualCreditWarning.style.display = "none";
            
          var appInfoMustMatch = document.getElementById("appInfoMustMatch");
          if (null != appInfoMustMatch) {
	        if (creditAction == "1" ) appInfoMustMatch.style.visibility = "visible"; <%-- // need to show it again if it's back to re-issue --%>
	        else if (creditAction == "2"  ) appInfoMustMatch.style.visibility = "hidden"; <%-- // note text doesn't apply to new orders --%>
          }
            
          if (creditAction == "0") { <%-- // Skip --%>
            f_displayPanel(3); <%-- // Display MclInstantIDPanel --%>
            f_enableCreditOrder(false);
          } else if (creditAction == "-1") { <%-- // Credit report already order. --%>
            f_displayPanel(3, 8, 10); <%-- // Display MclInstantIDPanel, ReportIDPanel, CreditProviderPanel --%>
            f_enableCreditOrder(false);
          } else if (creditAction == "3") { <%-- // Manual Credit --%>
            f_displayPanel(5); <%-- // Display ManualCreditPanel --%>
            f_enableCreditOrder(false);
            f_onManualForeclosureClick();
            f_onManualBk7Click();
            f_onManualBk13Click();
            <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentSatisfiedValidator) %>.enabled = true && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentSatisfiedValidator) %>.enabled = true && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentSatisfiedValidator) %>.enabled = true && !g_bIsReadOnly;    
            <%= AspxTools.JsGetElementById(sProdCrManual30MortLateCountValidator) %>.enabled = true && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(sProdCrManualRolling60MortLateCountValidator) %>.enabled = true && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(sProdCrManualRolling90MortLateCountValidator) %>.enabled = true && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentFileValidator) %>.enabled = true && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentFileValidator) %>.enabled = true && !g_bIsReadOnly;
            <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentFileValidator) %>.enabled = true && !g_bIsReadOnly;
            

            if (null != manualCreditWarning)
              manualCreditWarning.style.display = "";
            
          
          } else {
            if (creditAction == "2"  ) {
              <%-- // OPM: 2396. Check 3 bureau by default when order new credit. --%>
              <%= AspxTools.JsGetElementById(IsEquifax) %>.checked = true;
              <%= AspxTools.JsGetElementById(IsExperian) %>.checked = true;
              <%= AspxTools.JsGetElementById(IsTransUnion) %>.checked = true;
              
            }
            f_enableCreditOrder(true);
            f_onCreditProtocolChange();    
          }
          f_onValidateRequiredFields();  
        }
        function f_disableAllValidators() {
          <%= AspxTools.JsGetElementById(aBDobValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(aCDobValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(LoginNameValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(PasswordValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(ReportIDValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(InstantViewIDValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(aBAddrValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(aBCityValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(aBZipValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(FD_ClientCodeValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(FD_OfficeCodeValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(AccountIdentifierValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(DUUserIDValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(DUPasswordValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentSatisfiedValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentSatisfiedValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentSatisfiedValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentSatisfiedValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManual30MortLateCountValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentFileValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentFileValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentFileValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualRolling60MortLateCountValidator) %>.enabled = false;
          <%= AspxTools.JsGetElementById(sProdCrManualRolling90MortLateCountValidator) %>.enabled = false;
            
        }
        function init() {
            f_displayOrderCreditPanel();
            f_onchange_CreditAction();     
            var hasSpouse = $j('<%= AspxTools.JSelector(aBHasSpouse) %>').prop('checked');
            var cbScores = $j('<%= AspxTools.JSelector(aCExperianScorePe,aCTransUnionScorePe,aCEquifaxScorePe) %>').readOnly(!(hasSpouse && !g_bIsReadOnly));
            if(!hasSpouse){
                cbScores.val('0');
            }
        }

        function f_onManualForeclosureClick() {
          f_enablePublicRecord(<%= AspxTools.JsGetElementById(sProdCrManualForeclosureHas) %>, 
                <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentFileMon) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentFileYr) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentSatisfiedMon) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentSatisfiedYr) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualForeclosureRecentStatusT) %>);
          f_onValidateRequiredFields();      
        }
        function f_onManualBk7Click() {
          f_enablePublicRecord(<%= AspxTools.JsGetElementById(sProdCrManualBk7Has) %>, 
                <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentFileMon) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentFileYr) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentSatisfiedMon) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentSatisfiedYr) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk7RecentStatusT) %>);
          f_onValidateRequiredFields();          
        }
        function f_onManualBk13Click() {
          f_enablePublicRecord(<%= AspxTools.JsGetElementById(sProdCrManualBk13Has) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentFileMon) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentFileYr) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentSatisfiedMon) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentSatisfiedYr) %>,
                <%= AspxTools.JsGetElementById(sProdCrManualBk13RecentStatusT) %>);
          f_onValidateRequiredFields();          
        }
        function f_enablePublicRecord(has, recentFileMon, recentFileYr, recentSatisfiedMon, recentSatisfiedYr, recentStatusT) {
          var bEnabled = has.checked
          recentFileMon.disabled = !bEnabled;
          recentFileYr.disabled = !bEnabled;
          recentSatisfiedMon.disabled = !bEnabled;
          recentSatisfiedYr.disabled = !bEnabled;
          recentStatusT.disabled = !bEnabled;

          if (!bEnabled) {
            recentFileMon.selectedIndex = 0;
            recentFileYr.selectedIndex = 0;
            recentSatisfiedMon.selectedIndex = 0;
            recentSatisfiedYr.selectedIndex = 0;
            recentStatusT.selectedIndex = 0;
            
          } else {
            if (recentStatusT.value == "0") {
              recentSatisfiedMon.disabled = true;
              recentSatisfiedMon.value = "";
              recentSatisfiedYr.disabled = true;
              recentSatisfiedYr.value = "";
            }
          }
          f_onValidateRequiredFields();
        }
        function f_validateRolling30(source, args) {
          <%-- OPM 3493 - If non-rolling 30 is selected then rolling 30 is a required field --%>
          var rolling30 = <%= AspxTools.JsGetElementById(sProdCrManual30MortLateCount) %>;
          var nonRolling30 = <%= AspxTools.JsGetElementById(sProdCrManualNonRolling30MortLateCount) %>;
          
          args.IsValid = true;
          document.getElementById('rollingHint').style.display = "none";
          document.getElementById('nonrollingHint').style.display = "none";
          
          if (nonRolling30.value != "0" && rolling30.value == "0") {
	          args.IsValid = false;
	          document.getElementById('rollingHint').style.display = "";
          } else if (parseInt(rolling30.value) > parseInt(nonRolling30.value)) {
            args.IsValid = false;
            document.getElementById('nonrollingHint').style.display = "";
          }
        }
        function f_validateRolling60(source, args) {
          <%-- OPM 3493 - If non-rolling 30 is selected then rolling 30 is a required field --%>
          var rolling60 = <%= AspxTools.JsGetElementById(sProdCrManualRolling60MortLateCount) %>;
          var nonRolling60 = <%= AspxTools.JsGetElementById(sProdCrManual60MortLateCount) %>;
          
          args.IsValid = true;
          document.getElementById('rolling60Hint').style.display = "none";
          document.getElementById('nonrolling60Hint').style.display = "none";
          if (nonRolling60.value != "0" && rolling60.value == "0") {
	          args.IsValid = false;
	          document.getElementById('rolling60Hint').style.display = "";
          } else  if (parseInt(rolling60.value) > parseInt(nonRolling60.value)) {
            args.IsValid = false;
            document.getElementById('nonrolling60Hint').style.display = "";
          }
        }
        function f_validateRolling90(source, args) {
          <%-- OPM 8485 - If non-rolling 90 is selected then rolling 90 is a required field --%>
          var rolling90 = <%= AspxTools.JsGetElementById(sProdCrManualRolling90MortLateCount) %>;
          var nonRolling90 = <%= AspxTools.JsGetElementById(sProdCrManual90MortLateCount) %>;
          
          args.IsValid = true;
          document.getElementById('rolling90Hint').style.display = "none";
          document.getElementById('nonrolling90Hint').style.display = "none";
          if (nonRolling90.value != "0" && rolling90.value == "0") {
	          args.IsValid = false;
	          document.getElementById('rolling90Hint').style.display = "";
          } else  if (parseInt(rolling90.value) > parseInt(nonRolling90.value)) {
            args.IsValid = false;
            document.getElementById('nonrolling90Hint').style.display = "";
          }
        }
        function f_validateSatisfiedDate(source, args) {
          var id = source.controltovalidate;
          
          var yr_id = id.replace(/StatusT/, 'SatisfiedYr');
          var mon_id = id.replace(/StatusT/, 'SatisfiedMon');

          var o = document.getElementById(id);
          var yr = document.getElementById(yr_id);
          var mon = document.getElementById(mon_id);
          
          if (null != o && o.value != "0") {
            args.IsValid = yr.value != "" && mon.value != "";
          } else {
            args.IsValid = true;
          }
        }
        function f_validateRecentFileDate(source, args) {

          <%-- //var id = source.controltovalidate; --%>
          var id = source.id;
          var yr_id = id.replace(/RecentFileValidator/, 'RecentFileYr');
          var mon_id = id.replace(/RecentFileValidator/, 'RecentFileMon');
          id = id.replace(/RecentFileValidator/, 'Has');
          var o = document.getElementById(id);
          var yr = document.getElementById(yr_id);
          var mon = document.getElementById(mon_id);
          if (null != o && o.checked) {
            <%-- //alert('o is not null, yr=' + yr + ' mon=' + mon); --%>
            args.IsValid = yr.value != "" && mon.value != "";
          } else {
            args.IsValid = true;
          }
        }
        function f_validatePage() {
          <% if (m_displayCreditOptions) { %>
          var creditAction = f_getCreditAction();
          var bReissueOrReorder = creditAction == "1" ||creditAction == "2" || creditAction =="4";
          
          if (bReissueOrReorder) {
            var protocolType = retrieveProtocolType(<%= AspxTools.JsString(CreditProtocol) %>);
            
            if (protocolType == -1)
              return false;
              
            oValidateFPT[protocolType]();    

          }
          <% } %>

          var bValid = false;
          
          if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
            bValid = Page_ClientValidate();  
          <%-- //return !bReissueOrReorder || bValid; --%>
          return bValid;
        }
        function f_onValidateRequiredFields() { 
          if (typeof(f_validatePage) != "function") return; 
          
          var bValid = f_validatePage();
          document.getElementById('oCreditBtn').disabled = !bValid;
          f_enableAllButtons(bValid); 
        }

        <%--opm 4382 fs 07/23/08 --%>
        function f_confirmSpouseClick(cb)
        {
            var hasSpouse = $j(cb).prop('checked');
            if (hasSpouse && !f_SpouseHasEmptyFields()){
                if (confirm ("Are you sure you want to delete the co-applicant credit information?"))
                    f_onHasSpouseClick(cb);
                else cb.checked = $j(cb).prop('checked', true);
            }
          else{
             f_onHasSpouseClick(cb);
            }
        }

        function f_orderCreditClick(event)
        {
            jQuery('#oCreditBtn').prop( 'disabled', true);
            jQuery('.ErrorMessage').hide();
            var $oCreditBtn = jQuery('#oCreditBtn');
            $oCreditBtn.prop( 'oldValue', $oCreditBtn.prop('value'));
            $oCreditBtn.prop( 'value', 'Processing...')
            f_triggerAction(event, 'order');
        }
        
        function f_triggerAction(event)
        {
            if (f_validatePage())
            {
                f_prePostBack(event, function(bPostBack){
                    if (bPostBack)
                        __doPostBack('', 'order'); 
                });
            }
        }

        function f_displayOrderCreditPanel() {
            var creditAction = f_getCreditAction();
            var show = creditAction != "-1";
            var oCreditBtn = document.getElementById("oCreditBtn");
            var panel = document.getElementById("OrderCreditPanel");
            
            var str = "";
            if (creditAction == "1")
                str = "Re-Issue Credit";
            if (creditAction == "2")
                str = "Order Credit";
            if (creditAction == "3")
                str = "Enter Credit";
            if (creditAction == "4")
                str = "Upgrade Credit";
            
            if (oCreditBtn != null)
                oCreditBtn.value = str;
            if (null != panel)
                panel.style.display = show ? "" : "none";
        }

        function f_SpouseHasEmptyFields()
        {
         if (g_bIsReadOnly)
	        return true;
        	
          var v1 = <%= AspxTools.JsGetElementById(aCExperianScorePe) %>;
          var v2 = <%= AspxTools.JsGetElementById(aCTransUnionScorePe) %>;  
          var v3 = <%= AspxTools.JsGetElementById(aCEquifaxScorePe) %>;  
          
          if ( v1 == null || v2 == null || v3 == null )
	        return false;
           if ( (v1.value == "" && v2.value == "" && v3.value == "") || (v1.value == "0" && v2.value == "0" && v3.value == "0") )
	        return true;

        }
        function f_onHasSpouseClick(cb)
        {
          var hasSpouse = $j(cb).prop('checked');
          var cbScores = $j('<%= AspxTools.JSelector(aCExperianScorePe,aCTransUnionScorePe,aCEquifaxScorePe) %>').readOnly(!(hasSpouse && !g_bIsReadOnly));
          if (!(hasSpouse && !g_bIsReadOnly )) {
              cbScores.val('0');
          }

          f_onValidateRequiredFields();   
        }

        var oPanels = [document.getElementById("BorrowerPanel"), <%-- // 0 --%>
                        document.getElementById("BureauPanel"),  <%-- // 1 --%>
                        document.getElementById("KrollFDPanel"), <%-- // 2 --%>
                        document.getElementById("MclInstantIDPanel"), <%-- // 3 --%>
                        document.getElementById("AccountIDPanel"), <%-- // 4 --%>
                        document.getElementById("ManualCreditPanel"), <%-- // 5 --%>
                        document.getElementById("LoginPanel"), <%-- // 6 --%>
                        document.getElementById("PresentAddressPanel"), <%-- // 7 --%>
                        document.getElementById("ReportIDPanel"), <%-- // 8 --%>
                        document.getElementById("AccountIDPanel"), <%-- // 9 --%>
                        document.getElementById("CreditProviderPanel"), <%-- // 10 --%>
                        document.getElementById("FannieMaeCreditPanel"), <%-- // 11 --%>
                        document.getElementById("UpgradeMsgPanel"), <%-- // 12 --%>
                        document.getElementById("MortgageOnlyPanel") <%-- // 13 --%>                           
                      ];
        <%-- // Usage: Pass in list 0-index of panels to be visible.
        // i.e: f_displayPanel(1) // Display BureauPanel
        //      f_displayPanel(1, 3, 5) // Display BureauPanel, MclInstantIDPanel, ManualCreditPanel
        //      f_displayPanel(); // Hide all panels --%>

        function f_displayPanel() {

          for (var i = 0; i < oPanels.length; i++) {
            oPanels[i].style.display = "none";
          }
          if (null != arguments) {
            for (var i = 0; i < arguments.length; i++) {
              oPanels[arguments[i]].style.display = ""; 
            }   
          }
        } 
        function f_displayPanelByArray(list) {
          for (var i = 0; i < oPanels.length; i++) {
            oPanels[i].style.display = "none";
          }
          if (null != list) {
            for (var i = 0; i < list.length; i++) {
              oPanels[list[i]].style.display = ""; 
            }   
          }
        }

        function f_addLoginPanel(panelList)
        {
            if (!g_isStoredCredentials )
            {
                panelList.push(6); <%-- // Display LoginPanel --%>
                
            }            
        }   
     
        function f_addAccountIDPanel(panelList)
        {
            if (!g_isStoredCredentials )
            {
                panelList.push(9); <%-- // Display AccountIDPanel --%>
                
            }            
        } 

        function f_addCreditProviderPanel(panelList)
        {
            <%--  // OPM 450477.  We have to hide the entire credit provider
                  // section if we have removed all entries due to stored credentials.
             --%>
             if ( !g_isStoredCredentials ||
                   $j.inArray(11,panelList) !== -1 <%-- // Display FannieMaeCreditPanel --%>
                    || $j.inArray(4,panelList) !== -1 <%-- // Display AccountIDPanel --%>
                    || $j.inArray(9,panelList) !== -1 <%-- // Display AccountIDPanel (again) --%>
                    || $j.inArray(6,panelList) !== -1 <%-- // Display LoginPanel --%>
                    || $j.inArray(2,panelList) !== -1 <%-- // Display KrollFDPanel --%>
                    || $j.inArray(8,panelList) !== -1 <%-- // Display ReportIDPanel --%>
                    || $j.inArray(3,panelList) !== -1 <%-- // Display MclInstantIDPanel --%>
                )                   
            {
                panelList.push(10); <%-- // Display CreditProviderPanel --%>
            }
            
        }

        function f_openHelp(page, w, h){
	        var url = gVirtualRoot + "/help/" + page;
	        LQBPopup.Show(url, {
	            width : w,
	            height: h
	        });
	        return false;
        }
        
        function f_enableAllButtons(bEnabled) { }
        
        function f_return(errMsg) {
            if (errMsg) {
                var args = {
                    'errMsg': errMsg
                };

                var Url = 'FriendlyErrorPopup.aspx';

                parent.LQBPopup.Show(Url,
                    {
                        width: 300,
                        height: 50
                    }, args);
            } else {
                parent.LQBPopup.Return();
            }
        }
        
        //-->
        </script>
    </form>
</body>
</html>
