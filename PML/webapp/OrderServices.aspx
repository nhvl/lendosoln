﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderServices.aspx.cs" Inherits="PriceMyLoan.UI.Main.OrderServices" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Order Services</title>
    <style type="text/css">
        div#secGenericFramework > ul li {
            margin-right: 1.5em;
            padding-bottom: 10px;
            display: block;
            padding-left: 1.5em;
        }
        .NoShow {
            display: none;
        }
        #VoeSalaryKeyHelpIcon {
            color: #f0a200;
        }
        .ContactPicker {
            color: #f0a200;
        }
    </style>
</head>
<body>
<script>
    var EdocsUrl = <%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %>;
</script>
<noscript>
    For full functionality of this site it is necessary to enable JavaScript. Here are
    the <a href="http://www.enable-javascript.com/" target="_blank">instructions how to
        enable JavaScript in your web browser</a>.
</noscript>
<div class="warp">
    <form id="Form1" runat="server">
        <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />

        <div class="content-detail" name="content-detail">
            <div class="warp-section warp-section-tabs order-service-page" loan-static-tab>
                <div class="static-tabs"><div class="static-tabs-inner">
                    <header class="page-header table-flex table-flex-app-info">
                        <div>Order Services</div>
                        <div class="btnDiv text-right">
                            <button runat="server" type="button" class="btn btn-default" id="btnView">View 1003</button>
                            <button type="button" NOHIGHLIGHT runat="server" class="btn btn-default" id="btnEdit">Edit Closing Costs</button>
                            <button type="button" runat="server" id="btnRun" class="btn btn-default">Run Pricing</button>
                        </div>
                    </header>
                    <div ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>
                    <ul id="tabs" class="nav nav-tabs nav-tabs-panels">
                        <li id="Tab4506T" class="first-tab" runat="server"><a id="link1" href="#sec4506T" class="selected">4506-T</a></li>
                        <li><a href="#secAppraisals">Appraisals</a></li>
                        <li id="VoaTab" runat="server"><a href="#secVoa">VOA</a></li>
                        <li id="VoeTab" runat="server"><a href="#secVoe">VOE</a></li>
                        <li id="TabGenericFramework" runat="server"><a href="#secGenericFramework">Other Services</a></li>
                    </ul>
                </div></div>
                <div class="content-tabs">
                    <div class="table-service">
                        <div>
                            <div class="table-service-right">
                                <div id="sec4506T" runat="server" class="section-order-services">
                                    <asp:Repeater runat="server" ID="Orders4506T" OnItemDataBound="Orders4506T_OnItemDataBound">
                                        <HeaderTemplate>
                                            <table id="Orders4506TTable" class="tb-order-service primary-table">
                                                <thead class="header">
                                                    <tr>
                                                        <th runat="server" id="ApplicationColumn">Application</th>
                                                        <th runat="server" id="VendorColumn">Vendor</th>
                                                        <th runat="server" id="OrderNumColumn">Order #</th>
                                                        <th runat="server" id="OrderedDColumn">Ordered Date</th>
                                                        <th runat="server" id="StatusColumn">Status</th>
                                                        <th runat="server" id="StatusDColumn">Status Date</th>
                                                        <th runat="server" id="UploadedDocColumn">Uploaded Documents</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr runat="server" id="Row">
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="Application"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="Vendor"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="OrderNum"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="OrderedD"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <div style="margin-bottom:4px;"><ml:EncodedLiteral runat="server" ID="Status"></ml:EncodedLiteral> <a href="#" id="StatusDetailsLink" runat="server" visible="false" onclick="toggleStatusDetails(this); return false;">details</a><div id="StatusDetailsContainer"></div></div>
                                                    <asp:Button id="CheckStatus" runat="server" Text="Check Status" CssClass="btn btn-default" />
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="StatusD"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <div style="overflow-y: scroll; overflow-x: hidden;">
                                                    <asp:Repeater ID="UploadedDocuments" runat="server" OnItemDataBound="UploadedDocuments_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <a runat="server" id="UploadedDocLink"></a><br />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                                </tbody>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <button type="button" NOHIGHLIGHT runat="server" class="btn btn-default" ID="btnAdd">Add Order</button> <span class="RequiredSpan secInvalidPermission">You do not currently have permission to order this service for this file.</span>
                                    <div class="div-4506" id="div-4506">
                                        <div>
                                            <table width="100%">
                                                <tr>
                                                    <td>

                                                    </td>
                                                    <td align="right">
                                                        <button type="button" runat="server" class="btn btn-default" ID="btnCancel" Width="150" >Cancel Order</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <header class="header">Order Request Information</header>
                                        <div class="padding-left-content">
                                            <div class="cont">
                                                <div class="table table-vendor wide">
                                                    <div>
                                                        <div>
                                                            <span class="label-application">Application:</span>
                                                            <asp:DropDownList runat="server" ID="ApplicationName" CssClass="input-w270">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div>
                                                            Do you have a signed copy of the 4506-T form for this application?<span class="RequiredSpan">*</span>
                                                            <input type="radio" runat="server" id="Signed_Yes" name="Signed" value="Yes" label="Yes" />
                                                            <input type="radio" runat="server" id="Signed_No" name="Signed" value="No" label="No"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Yes_Cont" class="cont">
                                                <hr />
                                                <div class="table">
                                                    <div>
                                                        <div>
                                                            <label>4506-T Vendor<span class="RequiredSpan">*</span></label>
                                                            <asp:DropDownList runat="server" ID="Vendor4506"><asp:ListItem/></asp:DropDownList>
                                                        </div>
                                                        <div>
                                                            <div class="table table-default table-font-default">
                                                                <div id="loginInfo">
                                                                    <div>
                                                                        <label>Login<span class="RequiredSpan">*</span></label>
                                                                        <input runat="server" type="text" id="login_4506" class="input-w100" />
                                                                    </div>
                                                                    <div>
                                                                        <label>Password<span class="RequiredSpan">*</span></label>
                                                                        <input runat="server" type="password" id="password_4506" class="input-w100" />
                                                                    </div>
                                                                    <div>
                                                                        <label id="accID_4506Label">Account ID<span id="accID_4506Asterisk" class="RequiredSpan">*</span></label>
                                                                        <input runat="server" type="text" id="accID_4506" class="input-w100" />
                                                                    </div>
                                                                </div>
                                                                <div id="loginEmpty"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table wide">
                                                    <div>
                                                        <div>
                                                            <label>Signed Request Form <span class="RequiredSpan">*</span></label>
                                                            <input class="order-service-signReqForm" id="SignReqForm" runat="server" type="text" disabled="True" />
                                                            <a id="SelectEdocs" runat="server">select from EDocs</a>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="pull-right">
                                                            <button id="PlaceOrder" class="btn btn-default" type="button" NOHIGHLIGHT runat="server" disabled="true">Place Order</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="No_Cont" class="cont">
                                                <hr />
                                                <label class="text-danger margin-bottom">Please complete the order request information below and download the 4506-T form.
                                                    Once it has been signed by the borrower/s the order can be placed.</label>
                                                <div class="table">
                                                    <div>
                                                        <div>
                                                            <label>Borrower Name</label>
                                                            <input runat="server" id="aB4506TNm" type="text" value=""/>
                                                            <label class="lock-checkbox">
                                                                <input runat="server" id="aB4506TNmLckd" type="checkbox" />
                                                            </label>
                                                        </div>
                                                        <div>
                                                            <label>Borrower SSN, TIN, or EIN</label>
                                                            <ml:SSNTextBox runat="server" ID="aB4506TSsnTinEin" value="000-00-0000" preset="ssn" />
                                                        </div>
                                                        <div class="spacer-w5"></div>
                                                        <div>
                                                            <label>Co-Borrower Name</label>
                                                            <input runat="server" id="aC4506TNm" type="text"/>
                                                            <label class="lock-checkbox">
                                                                <input runat="server" id="aC4506TNmLckd" type="checkbox" />
                                                            </label>
                                                        </div>
                                                        <div>
                                                            <label>Co-Borrower SSN, TIN, or EIN</label>
                                                            <ml:SSNTextBox runat="server" ID="aC4506TSsnTinEin" value="000-00-0000" preset="ssn" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table">
                                                    <div>
                                                        <div>
                                                            <label>Previous Address</label>
                                                            <div class="table">
                                                                <div>
                                                                    <div>
                                                                        <label>Street</label>
                                                                        <input id="a4506TPrevStreetAddr" type="text" runat="server" class="form-control-address" />
                                                                    </div>
                                                                    <div>
                                                                        <label>ZIPCode</label>
                                                                        <ml:ZipcodeTextBox runat="server" ID="a4506TPrevZip" Width="60" />
                                                                    </div>
                                                                    <div>
                                                                        <label>City</label>
                                                                        <input id="a4506TPrevCity" type="text" runat="server" />
                                                                    </div>
                                                                    <div>
                                                                        <label>State</label>
                                                                        <ml:StateDropDownList runat="server" ID="a4506TPrevState" Value="CA" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="div-height">
                                                    <label class="margin-bottom">
                                                        If transcript or tax information is to be mailed to a third party, enter the third party's information:</label>
                                                    <div class="table">
                                                        <div>
                                                            <div>
                                                                <label>Name</label>
                                                                <input id="a4506TThirdPartyName" runat="server" />
                                                            </div>
                                                            <div>
                                                                <label>Phone Number</label>
                                                                <ml:PhoneTextBox runat="server" ID="a4506TThirdPartyPhone" preset="phone" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table">
                                                        <div>
                                                            <div>
                                                                <label>Address</label>
                                                                <div class="table">
                                                                    <div>
                                                                        <div>
                                                                            <label>Street</label>
                                                                            <input id="a4506TThirdPartyStreetAddr" type="text" runat="server" class="form-control-address" />
                                                                        </div>
                                                                        <div>
                                                                            <label>ZIPCode</label>
                                                                            <ml:ZipcodeTextBox runat="server" ID="a4506TThirdPartyZip" Width="60" />
                                                                        </div>
                                                                        <div>
                                                                            <label>City</label>
                                                                            <input id="a4506TThirdPartyCity" type="text" runat="server" />
                                                                        </div>
                                                                        <div>
                                                                            <label>State</label>
                                                                            <ml:StateDropDownList runat="server" ID="a4506TThirdPartyState" Value="CA" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div>
                                                    <label>
                                                        <strong>Transcript requested</strong>.</label>&nbsp; Tax form number
                                                        (1040, 1065, 1120, etc.):
                                                        <input id="a4506TTranscript" type="text" runat="server" />
                                                </div>
                                                <div class="transcript-3">
                                                    <input runat="server" type="checkbox" id="a4056TIsReturnTranscript" />
                                                    <label for="a4056TIsReturnTranscript"><strong>a. Return Transcript,</strong> which includes most of the line items of a tax return
                                                    as filed with the IRS</label><br />
                                                    <input runat="server" type="checkbox" id="a4506TIsAccountTranscript" />
                                                    <label for="a4506TIsAccountTranscript"><strong>b. Account Transcript,</strong> which contains information on the financial status
                                                    of the account</label><br />
                                                    <input runat="server" type="checkbox" id="a4056TIsRecordAccountTranscript" />
                                                    <label for="a4056TIsRecordAccountTranscript"><strong>c. Record of Account,</strong> which is a combination of the line item information
                                                    and later adjustments to the account</label>
                                                </div>
                                                <div class="div-height">
                                                    <input runat="server" type="checkbox" id="a4506TVerificationNonfiling" />
                                                    <label for="a4506TVerificationNonfiling"><strong>Verification of Nonfiling,</strong> which is proof from the IRS that you did not file
                                                    a return for the year</label><br />
                                                    <input runat="server" type="checkbox" id="a4506TSeriesTranscript" />
                                                    <label for="a4506TSeriesTranscript"><strong>Form W-2, Form 1099 series, Form 1098 series, or Form 5498 series transcript</strong></label>
                                                </div>
                                                <hr />
                                                <div class="div-height">
                                                    <label>
                                                        Ending date of year or period requested (mm/dd/yyyy)</label>&nbsp;&nbsp;&nbsp;
                                                    <input type="text" runat="server" id="a4506TYear1" preset="date" class="input-w120" />&nbsp;&nbsp;&nbsp;
                                                    <input type="text" runat="server" id="a4506TYear2" preset="date" class="input-w120" />&nbsp;&nbsp;&nbsp;
                                                    <input type="text" runat="server" id="a4506TYear3" preset="date" class="input-w120" />&nbsp;&nbsp;&nbsp;
                                                    <input type="text" runat="server" id="a4506TYear4" preset="date" class="input-w120" /><br />
                                                    <div class="transcript-3">
                                                        <input runat="server" type="checkbox" id="a4506TRequestYrHadIdentityTheft" />&nbsp;<label for="a4506TRequestYrHadIdentityTheft">One of the years requested
                                                        involved identity theft on the federal tax return.</label>
                                                    </div>
                                                </div>
                                                <div class="div-order-service-download">
                                                    <button type="button" NOHIGHLIGHT class="btn btn-default" id="DownloadSign" runat="server">Download & Sign Request Form</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="secAppraisals" runat="server" class="section-order-services">
                                    <asp:Repeater runat="server" ID="Orders" OnItemDataBound="Orders_OnItemDataBound">
                                        <HeaderTemplate>
                                            <table id="OrdersTable" class="tb-order-service primary-table">
                                                <thead class="header">
                                                    <tr>
                                                        <th runat="server" id="ViewColumn"></th>
                                                        <th runat="server" id="VendorColumn">Vendor</th>
                                                        <th runat="server" id="OrderNumColumn">Order #</th>
                                                        <th runat="server" id="OrderedDColumn">Ordered Date</th>
                                                        <th runat="server" id="StatusColumn">Status</th>
                                                        <th runat="server" id="StatusDColumn">Status Date</th>
                                                        <th runat="server" id="UploadedDocColumn" class="text-center">Uploaded Documents</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="GridItem" runat="server" id="Row">
                                                <td class="order-service-firstcol">
                                                    <a runat="server" visible="false" href="#" class="RefreshLink" id="RefreshLink">refresh</a><a runat="server" href="#" class="MenuLinks" id="ViewLink">view</a> <span class="span-linkalike">/</span> <a runat="server" href="#" class="MenuLinks" id="EditLink">edit</a>  <span class="span-linkalike">/</span> <a runat="server" href="#" class="MenuLinks" id="AttachDocsLink">attach docs</a> <a runat="server" href="#" class="MenuLinks" id="SendEmailLink">/ send email</a>
                                                </td>
                                                <td>
                        <ml:EncodedLiteral runat="server" ID="Vendor"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                        <ml:EncodedLiteral runat="server" ID="OrderNum"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                        <ml:EncodedLiteral runat="server" ID="OrderedD"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                        <ml:EncodedLiteral runat="server" ID="Status"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                        <ml:EncodedLiteral runat="server" ID="StatusD"></ml:EncodedLiteral>
                                                </td>
                                                <td class="text-center">
                                                    <div class="col-uploaded-documents">
                                                    <asp:Repeater ID="UploadedDocuments" runat="server" OnItemDataBound="UploadedDocuments_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <a runat="server" href="#" id="UploadedDocLink"></a>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                                </tbody>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>

                                    <div><button type="button" NOHIGHLIGHT runat="server" class="btn btn-default" ID="btnAdd_Appraisal">Add Order</button></div>
                                    <span class="RequiredSpan secInvalidPermission">You do not currently have permission to order this service for this file.</span>
                                    <div class="div-4506" id="div-Appraisal">
                                        <div class="div-order-request">
                                            <table width="100%">
                                                <tr>
                                                    <td>

                                                    </td>
                                                    <td align="right">
                                                        <button type="button" runat="server" class="btn btn-default" ID="btnCancel_Appraisal" Width="150">Cancel Order</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <header class="header">Order Request Information</header>
                                        <div class="cont">
                                            <div class="padding-left-content margin-bottom">
                                                <div class="table">
                                                    <div>
                                                        <div>
                                                            <label>AMC Vendor<span class="RequiredSpan">*</span></label>
                                                            <asp:DropDownList runat="server" ID="SelectedAMC">
                                                                <asp:ListItem></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="AMCLogin">
                                                            <div class="LoginRow">
                                                                <div class="table table-default table-font-default">
                                                                    <div>
                                                                        <div>
                                                                            <label>Login<span class="RequiredSpan">*</span></label>
                                                                            <input runat="server" type="text" id="login" />
                                                                        </div>
                                                                        <div>
                                                                            <label>Password<span class="RequiredSpan">*</span></label>
                                                                            <input runat="server" type="password" id="password" />
                                                                        </div>
                                                                        <div class="vertical-align-bottom">
                                                                            <label id="accIDLabel">Account ID<span class="RequiredSpan" id="accIdAsterisk">*</span></label>
                                                                            <input runat="server" type="text" id="accID" />
                                                                        </div>
                                                                        <div class="vertical-align-bottom">
                                                                            <button class="btn btn-default" id="AppraisalLoginBtn" type="button">Connect to Vendor</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="AMC">
                                                <div id="AMCInner">
                                                <header class="header">Loan Information</header>
                                                <div class="padding-left-content margin-bottom">
                                                    <div class="flex-space-between none-margin-bottom padding-bottom-p16">
                                                        <div class="flex-alignment  flex-alignment-w-order-services-section-1">
                                                            <div class="col-xs-4">
                                                                <div>
                                                                    <div>
                                                                        <label>Loan Number</label>
                                                                        <input runat="server" id="sLNmInfo" disabled />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div>
                                                                    <div>
                                                                        <label>Case Number</label>
                                                                        <input runat="server" id="sAgencyCaseNum" disabled/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div>
                                                                    <div>
                                                                        <label amc="1i">Loan Type</label>
                                                                        <input runat="server" id="sLT" amc="1i" disabled />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-182">
                                                            <div>
                                                                <label amc="1i">Intended Use</label>
                                                                <input runat="server" type="text" amc="1i" id="sLPurposeT" disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-space-between none-margin-bottom padding-bottom-p16">
                                                        <div class="flex-alignment  flex-alignment-w-order-services-section-1">
                                                            <div class="col-xs-4" amc="1o">
                                                                <div>
                                                                    <div>
                                                                        <label>Occupancy Type</label>
                                                                        <input runat="server" type="text" id="aOccT"  disabled />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4" amc="1o">
                                                                <div>
                                                                    <div>
                                                                        <label>Property Type</label>
                                                                        <input runat="server" type="text" id="sGseSpT"  disabled />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div>
                                                                    <div>
                                                                        <label>Loan Officer Full Name</label>
                                                                        <input runat="server" type="text" id="LoanOfficerName"  disabled />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-182">
                                                            <div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <header class="header">
                                                    Borrower / Property Info
                                                </header>
                                                <div class="padding-left-content margin-bottom">
                                                    <div class="flex-space-between none-margin-bottom padding-bottom-p16">
                                                        <div class="flex-alignment  flex-alignment-w-order-services-section-1">
                                                            <div class="col-xs-4">
                                                                <div>
                                                                    <div>
                                                                        <label>Borrower Name</label>
                                                                        <input runat="server" id="BorrowerName" class="PropertyDetails" disabled />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <div>
                                                                    <div>
                                                                        <label>Borrower Email</label>
                                                                        <input runat="server" id="BorrowerEmail" class="PropertyDetails" disabled />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4"></div>
                                                        </div>
                                                        <div class="w-182">
                                                            <div></div>
                                                        </div>
                                                    </div>
                                                    <div class="table">
                                                        <div>
                                                            <div>
                                                                <label>Property Address</label>
                                                                <div>
                                                                    <div>
                                                                        <label>Select Property</label>
                                                                    </div>
                                                                    <div>
                                                                        <asp:DropDownList runat="server" ID="PropertySelector"></asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="table">
                                                                    <div>
                                                                        <div>
                                                                            <label>Street <span class="RequiredSpan">*</span></label>
                                                                            <input id="PropertyAddress" type="text" runat="server" class="PropertyDetails form-control-address" autocomplete="false" />
                                                                        </div>
                                                                        <div>
                                                                            <label>ZIPCode</label>
                                                                            <ml:ZipcodeTextBox runat="server" CssClass="PropertyDetails" ID="PropertyZip" Width="60" autocomplete="false" />
                                                                        </div>
                                                                        <div>
                                                                            <label>City</label>
                                                                            <input id="PropertyCity" type="text" runat="server" class="PropertyDetails" autocomplete="false" />
                                                                        </div>
                                                                        <div>
                                                                            <label>State <span class="RequiredSpan">*</span></label>
                                                                            <ml:StateDropDownList runat="server" ID="PropertyState" Value="CA" CssClass="PropertyDetails" autocomplete="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <header class="header">
                                                    Property Access/Contact Information</header>
                                                <div class="padding-left-content margin-bottom">
                                                    <div class="flex-space-between none-margin-bottom padding-bottom-p16">
                                                        <div class="flex-alignment  flex-alignment-w-order-services-section-3">
                                                            <div class="col-xs-6">
                                                                <div>
                                                                    <div>
                                                                        <label>Contact Name<span class="RequiredSpan">*</span></label>
                                                                        <input runat="server" id="ContactName" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div>
                                                                    <div>
                                                                        <label>Contact Email</label>
                                                                        <input runat="server" id="ContactEmail" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-449">
                                                            <div>
                                                                <div>
                                                                    <label>Additional Emails (up to 8 and separated by ;)</label>
                                                                    <asp:TextBox ID="AdditionalEmail" class="fivecol additionalEmail w-449" runat="server" /><br />
                                                                    <span class="additionalEmailValidator text-danger">Please limit the number of additional emails to 8.</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-space-between none-margin-bottom padding-bottom-p16">
                                                        <div class="flex-alignment  flex-alignment-w-order-services-section-3">
                                                            <div class="col-xs-6">
                                                                <div>
                                                                    <div>
                                                                        <label>Phone Number<span class="RequiredSpan">*</span></label>
                                                                        <ml:PhoneTextBox runat="server" preset="phone" ID="PhoneNumber" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div>
                                                                    <div>
                                                                        <label>Work Number<span class="RequiredSpan">*</span></label>
                                                                        <ml:PhoneTextBox runat="server" preset="phone" ID="WorkNumber" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-449">
                                                            <div>
                                                                <div>
                                                                    <label>Other Number<span class="RequiredSpan">*</span></label>
                                                                    <ml:PhoneTextBox runat="server" preset="phone" ID="OtherNumber" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <header class="header">
                                                    <span amc="1o">Order Information</span> <span amc="2o">Assignment Information</span></header>
                                                <div class="padding-left-content margin-bottom">
                                                    <table class="table-vendor" amc="2o">
                                                        <tr>
                                                            <td>
                                                                Property Type
                                                            </td>
                                                            <td class="box-20">
                                                                <asp:DropDownList runat="server" ID="PropertyType" />
                                                            </td>
                                                            <td class="box-70">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="table-vendor">
                                                        <tr>
                                                            <td>
                                                                Report Type <span class="RequiredSpan">*</span>
                                                            </td>
                                                            <td class="box-70" nowrap>
                                                                <asp:DropDownList runat="server" ID="ReportType" class="ReportType ProductList" />
                                                            </td>
                                                            <td class="box-20">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Report Type 2
                                                            </td>
                                                            <td class="box-70">
                                                                <asp:DropDownList runat="server" ID="ReportType2" class="ReportType ProductList"  />
                                                            </td>
                                                            <td class="box-20">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Report Type 3
                                                            </td>
                                                            <td class="box-70">
                                                                <asp:DropDownList runat="server" ID="ReportType3" class="ReportType ProductList"  />
                                                            </td>
                                                            <td class="box-20">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Report Type 4
                                                            </td>
                                                            <td class="box-70">
                                                                <asp:DropDownList runat="server" ID="ReportType4" class="ReportType ProductList"  />
                                                            </td>
                                                            <td class="box-20">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Report Type 5
                                                            </td>
                                                            <td class="box-70">
                                                                <asp:DropDownList runat="server" ID="ReportType5" class="ReportType ProductList"  />
                                                            </td>
                                                            <td class="box-20">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="appraisal-need-date" amc="1o">
                                                        <div class="table table-none-padding-bottom">
                                                            <div>
                                                                <div>
                                                                    <label>Appraisal Needed Date: <span class="RequiredSpan AppraisalNeededDIndicator" id="AppraisalNeededDRequired">*</span></label>
                                                                    <asp:TextBox runat="server" preset="date" ID="AppraisalNeededD" class="hasDatepicker" />
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="table table-none-padding-bottom">
                                                                    <div>
                                                                        <div><label>Rush Order?</label></div>
                                                                        <div><asp:CheckBox runat="server" ID="RushOrder" />Yes</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table wide appraisal-intended-use" amc="2o">
                                                        <div>
                                                            <div>
                                                                <label>Intended Use <span class="RequiredSpan">*</span></label>
                                                                <asp:DropDownList runat="server" ID="IntendedUse" />
                                                            </div>
                                                            <div>
                                                                <label>Loan Type</label>
                                                                <asp:DropDownList runat="server" ID="LoanType" />
                                                            </div>
                                                            <div>
                                                                <label>Occupancy Type</label>
                                                                <asp:DropDownList runat="server" ID="OccupancyType" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <header class="header" amc="2o">
                                                    Client Information</header>
                                                <div class="padding-left-content margin-bottom">
                                                    <div class="table" amc="2o">
                                                        <div class="appraisal-need-date">
                                                            <div>
                                                                <label>Appraisal Needed Date<span class="RequiredSpan AppraisalNeededDIndicator">*</span></label>
                                                                <asp:TextBox runat="server"  class="hasDatepicker" preset="date" ID="AppraisalNeededDGDMS"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div>
                                                                <label>Client 2</label>
                                                                <asp:DropDownList runat="server" ID="Client2" />
                                                            </div>
                                                            <div>
                                                                <label>Processor</label>
                                                                <asp:DropDownList runat="server" ID="Processor" />
                                                            </div>
                                                            <div>
                                                                <label>Processor 2</label>
                                                                <asp:DropDownList runat="server" ID="Processor2" />
                                                            </div>
                                                            <div>
                                                                <label>Lender</label>
                                                                <asp:TextBox runat="server" ID="Lender"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <header class="header">
                                                    Billing Information</header>
                                                <div class="padding-left-content margin-bottom">
                                                    <div class="table">
                                                        <div>
                                                            <div>
                                                                <label>Billing Method <span class="RequiredSpan">*</span></label>
                                                                <asp:DropDownList runat="server" ID="BillingMethod" CssClass="input-w200">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div id="creditInfo">
                                                            <div>
                                                                <div class="table table-default table-font-default">
                                                                    <div>
                                                                        <div>
                                                                            <label>Billing Name: <span class="RequiredSpan">*</span></label>
                                                                            <asp:TextBox runat="server" ID="BillingName"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="table table table-default table-font-default">
                                                                    <div>
                                                                        <div>
                                                                            <label>Billing Address </label>
                                                                            <div class="table table-billing-address">
                                                                                <div>
                                                                                    <div>
                                                                                        <label>Street <span class="RequiredSpan">*</span></label>
                                                                                        <asp:TextBox ID="BillingAdd" runat="server" CssClass="form-control-address"></asp:TextBox>
                                                                                    </div>
                                                                                    <div>
                                                                                        <label>ZIPCode</label>
                                                                                        <ml:ZipcodeTextBox runat="server" ID="BillingZip" Width="60" />
                                                                                    </div>
                                                                                    <div>
                                                                                        <label>City</label>
                                                                                        <asp:TextBox runat="server" ID="BillingCity"></asp:TextBox>
                                                                                    </div>
                                                                                    <div>
                                                                                        <label>State <span class="RequiredSpan">*</span></label>
                                                                                        <ml:StateDropDownList runat="server" ID="BillingState" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="table table table-default table-font-default">
                                                                    <div>
                                                                        <div>
                                                                            <label>Card Type <span class="RequiredSpan">*</span></label>
                                                                            <asp:DropDownList runat="server" ID="CardType" CssClass="input-w200">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="table table-default table-font-default">
                                                                    <div>
                                                                        <div>
                                                                            <label>Card Number <span class="RequiredSpan">*</span></label>
                                                                            <asp:TextBox id="CCNumber" runat="server"></asp:TextBox>
                                                                        </div>
                                                                        <div>
                                                                            <label>Card Expiration (mm/yyyy) <span class="RequiredSpan">*</span></label>
                                                                            <asp:TextBox runat="server" ID="Expiration_Month" CssClass="input-w30"
                                                                                preset="month"></asp:TextBox>&nbsp;/&nbsp;<asp:TextBox runat="server" ID="Expiration_Year" CssClass="input-w30" preset="year"></asp:TextBox>
                                                                        </div>
                                                                        <div>
                                                                            <label>CVV / CCID # <span class="RequiredSpan">*</span></label>
                                                                            <asp:TextBox runat="server" ID="Expiration_CVV" CssClass="input-w30"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <header class="header">
                                                    Extra Information</header>
                                                <div class="padding-left-content ">
                                                    <div class="table" amc="2o">
                                                        <div>
                                                            <div>
                                                                <label>Attachment Type</label>
                                                                <asp:DropDownList runat="server" ID="AttachmentType" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- http://jsfiddle.net/arunpjohny/WgBAc/ --%>
                                                    <div class="table wide">
                                                        <div>
                                                            <div class="documents-order-services">
                                                                <label>Documents <a class="pull-right" id="attachDocument">+ attach document to order</a></label>
                                                                <ul class="document-list background-disabled" id="documents" runat="server"></ul>
                                                            </div>
                                                            <div>
                                                                <label>Notes</label>
                                                                <asp:TextBox runat="server" TextMode="MultiLine" Rows="5" ID="Notes" CssClass="input-h5"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-right">
                                                    <button type="button" NOHIGHLIGHT runat="server" class="btn btn-default" ID="btnPlaceOrder">Place Order</button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="secGenericFramework" runat="server" class="section-order-services">
                                    <ul>
                                    <asp:Repeater ID="GenericFrameworkLinkRepeater" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <a id="GenericFrameworkVendorLink" class="GenericFrameworkVendor" runat="server" href="#"/>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    </ul>
                                </div>
                                <div id="secVoa" class="section-order-services VOXTable VoxSection" runat="server">
                                    <table class="tb-order-service primary-table OrdersTable">
                                        <thead class="header">
                                            <tr>
                                                <th>Institution</th>
                                                <th>Account Numbers</th>
                                                <th>Service Provider</th>
                                                <th>Order Number</th>
                                                <th>Status</th>
                                                <th>Results</th>
                                                <th>Action</th>
                                                <th>Date Ordered</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <br />
                                    <input type="button" class="btn btn-default VOXOrderBtn" id="VOA_OrderBtn" nohighlight value="Order New VOA/VOD" />
                                    <span class="RequiredSpan secInvalidPermission">You do not currently have permission to order this service for this file.</span>
                                    <div class="VOXOrderingDiv">
                                        <div>
                                            <table width="100%">
                                                <tr>
                                                    <td></td>
                                                    <td align="right">
                                                        <input type="button" class="btn btn-default VOXCancelBtn" Width="150" nohighlight value="Cancel Order" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <header class="header">Order Verification of Asset</header>
                                        <div>
                                            <div class="padding-left-content margin-bottom">
                                                <div class="table">
                                                    <div>
                                                        <div>
                                                            <label>Service Provider</label>
                                                            <asp:DropDownList runat="server" CssClass="LenderServices" ID="VOA_LenderServices"></asp:DropDownList>
                                                            <span class="RequiredSpan">*</span>
                                                        </div>
                                                        <div class="CredentialsRow CredentialItem">
                                                            <div class="table table-default table-font-default">
                                                                <div>
                                                                    <div class="AccountIdRow CredentialItem">
                                                                        <label>AccountId</label>
                                                                        <input type="text" class="AccountId" />
                                                                        <span class="RequiredSpan AccountIdRequiredSpan">*</span>
                                                                    </div>
                                                                    <div class="UsernameRow CredentialItem">
                                                                        <label>Username</label>
                                                                        <input type="text" class="Username" />
                                                                        <span class="RequiredSpan UsernameRequiredSpan">*</span>
                                                                    </div>
                                                                    <div class="PasswordRow CredentialItem">
                                                                        <label>Password</label>
                                                                        <input runat="server" type="password" class="Password"/>
                                                                        <span class="RequiredSpan PasswordRequiredSpan">*</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <header class="header">
                                                        <span class="PickerSection AssetPickerSection">Select assets to verify</span>
                                                        <span class="PickerSection BorrowerPickerSection NoShow">Select borrower to verify</span>
                                                        <span class="SectionToggleSection FloatRight NoShow">
                                                            <input type="radio" name="SectionToggle" class="SectionToggle AssetToggle" checked="checked" value="Select assets to verify" />
                                                            <input type="radio" name="SectionToggle" class="SectionToggle BorrowerToggle" value="Select borrower to verify" />
                                                        </span>
                                                    </header>
                                                    <div class="PickerSection AssetPickerSection">
                                                        <table class="tb-order-service primary-table AssetsTable">
                                                            <thead>
                                                                <tr class="header">
                                                                    <td>
                                                                        <input type="checkbox" class="CheckAllAssetsCB" />
                                                                    </td>
                                                                    <td>Institution</td>
                                                                    <td>Account Number</td>
                                                                    <td>Owner</td>
                                                                    <td>Asset Type</td>
                                                                    <td>Description</td>
                                                                    <td>Value</td>
                                                                    <td>Order Status</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <asp:Repeater ID="AssetsRepeater" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="checkbox" class="AssetsCB" />
                                                                                <input type="hidden" class="AssetId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssetId").ToString()) %>" />
                                                                                <input type="hidden" class="AppId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AppId").ToString()) %>" />
                                                                                <input type="hidden" class="OwnerT" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OwnerT").ToString()) %>" />
                                                                                <input type="hidden" class="Key" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Key").ToString()) %>" />
                                                                            </td>
                                                                            <td>
                                                                                <span class="Institution"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Institution")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="AccountNumber"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AccountNum")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Owner"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Owner")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="AssetType"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssetType")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Description"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Value"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Value")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="OrderStatus"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OrderStatus")) %></span>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tbody>
                                                        </table>
                                                        <div class="text-right">
                                                            <a class="AssetsLink">Please enter assets on the Application Information page</a>
                                                        </div>
                                                    </div>
                                                    <div class="PickerSection BorrowerPickerSection padding-bottom-20px NoShow">
                                                        <asp:DropDownList runat="server" CssClass="BorrowerList" ID="VOA_BorrowerList"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <header class="header">Order Options</header>
                                                <div class="table table-default table-font-default">
                                                    <div>
                                                        <div class="NotificationEmailRow">
                                                            <label>Notification Email</label>
                                                            <input type="text" class="RequiredInput NotificationEmail"/>
                                                            <span class="RequiredSpan">*</span>
                                                            <a class="tooltips" data-html="true" data-toggle="tooltip" data-placement="bottom" title="Choose from contacts">
                                                                <i class="material-icons ContactPicker NotificationEmailPicker">assignment_ind</i>
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <label>Account History</label>
                                                            <select class="AccountHistoryOptions"></select>
                                                        </div>
                                                        <div>
                                                            <label>Refresh Period</label>
                                                            <select class="RefreshPeriodOptions"></select>
                                                        </div>
                                                        <div class="CreditCardRow vertical-align-bottom Hidden">
                                                            <label>
                                                                <input type="checkbox" class="AllowPayWithCreditCard" />Pay with credit card</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="BorrowerAuthDocsSection">
                                                    <hr />
                                                    <header class="header">Borrower Authorization Documents</header>
                                                    <div>
                                                        <table class="DocumentTable"></table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="buttonPanel">
                                            <input type="button" class="btn btn-default VOXPlaceOrderBtn" value="Place Order"/>
                                        </div>
                                    </div>
                                </div>
                                <div id="secVoe" class="section-order-services VOXTable VoxSection" runat="server">
                                    <table class="tb-order-service primary-table OrdersTable">
                                        <thead class="header">
                                            <tr>
                                                <th>Borrower</th>
                                                <th>Employer</th>
                                                <th>Service Provider</th>
                                                <th>Order Number</th>
                                                <th>Status</th>
                                                <th>Results</th>
                                                <th>Action</th>
                                                <th>Date Ordered</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <br />
                                    <input type="button" class="btn btn-default VOXOrderBtn" id="VOE_OrderBtn" nohighlight value="Order New VOE/VOI" />
                                    <span class="RequiredSpan secInvalidPermission">You do not currently have permission to order this service for this file.</span>
                                    <div class="VOXOrderingDiv">
                                        <div>
                                            <table width="100%">
                                                <tr>
                                                    <td></td>
                                                    <td align="right">
                                                        <input type="button" class="btn btn-default VOXCancelBtn" Width="150" nohighlight value="Cancel Order" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <header class="header">Order Verification of Employment</header>
                                        <div>
                                            <div class="padding-left-content margin-bottom">
                                                <div class="table">
                                                    <div>
                                                        <div>
                                                            <label>Service Provider</label>
                                                            <asp:DropDownList runat="server" CssClass="LenderServices" ID="VOE_LenderServices"></asp:DropDownList>
                                                            <span class="RequiredSpan">*</span>
                                                        </div>
                                                        <div class="CredentialsRow CredentialItem">
                                                            <div class="table table-default table-font-default">
                                                                <div>
                                                                    <div class="AccountIdRow CredentialItem">
                                                                        <label>AccountId</label>
                                                                        <input type="text" class="AccountId" />
                                                                        <span class="RequiredSpan AccountIdRequiredSpan">*</span>
                                                                    </div>
                                                                    <div class="UsernameRow CredentialItem">
                                                                        <label>Username</label>
                                                                        <input type="text" class="Username" />
                                                                        <span class="RequiredSpan UsernameRequiredSpan">*</span>
                                                                    </div>
                                                                    <div class="PasswordRow CredentialItem">
                                                                        <label>Password</label>
                                                                        <input runat="server" type="password" class="Password"/>
                                                                        <span class="RequiredSpan PasswordRequiredSpan">*</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <header class="header">
                                                        <span class="PickerSection EmploymentPickerSection">Select employment to verify</span>
                                                        <span class="PickerSection BorrowerPickerSection NoShow">Select borrower to verify</span>
                                                        <span class="SectionToggleSection FloatRight NoShow">
                                                            <input type="radio" name="SectionToggle" class="SectionToggle EmploymentToggle" checked="checked" value="Verify employment records" />
                                                            <input type="radio" name="SectionToggle" class="SectionToggle BorrowerToggle" value="Verify borrower employment" />
                                                        </span>
                                                    </header>
                                                    <div class="PickerSection EmploymentPickerSection">
                                                        <table class="tb-order-service primary-table EmploymentTable">
                                                            <thead>
                                                                <tr class="header">
                                                                    <td><input type="checkbox" class="CheckAllCB" /></td>
                                                                    <td>Borrower</td>
                                                                    <td>Employer</td>
                                                                    <td>Self</td>
                                                                    <td>Current</td>
                                                                    <td>End Date</td>
                                                                    <td>Years on Job</td>
                                                                    <td>Position/Title</td>
                                                                    <td>Order Status</td>
                                                                    <td>Type</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <asp:Repeater ID="EmploymentRepeater" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="checkbox" class="EmploymentCB" />
                                                                                <input type="hidden" class="EmploymentRecordId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmploymentRecordId").ToString()) %>" />
                                                                                <input type="hidden" class="AppId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AppId").ToString()) %>" />
                                                                                <input type="hidden" class="BorrowerType" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerType").ToString()) %>" />
                                                                                <input type="hidden" class="Key" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Key")) %>" />
                                                                            </td>
                                                                            <td>
                                                                                <span class="Borrower"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Borrower")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Employer"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Employer")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="IsSelfEmployment"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IsSelfEmployment")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="IsCurrent"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IsCurrent")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="EmploymentEndDate"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmploymentEndDate")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="YrsOnJob"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "YrsOnJob")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="PositionTitle"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PositionTitle")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="OrderStatus"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OrderStatus")) %></span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Type"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Type")) %></span>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tbody>
                                                        </table>
                                                        <div class="text-right">
                                                            <a class="EmploymentLink">Please enter employment on the Application Information page</a>
                                                        </div>
                                                    </div>
                                                    <div class="PickerSection BorrowerPickerSection padding-bottom-20px NoShow">
                                                        <asp:DropDownList runat="server" CssClass="BorrowerList" ID="VOE_BorrowerList"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <header class="header">Order Options</header>
                                            <div class="table table-default table-font-default" style="margin-bottom: 20px;">
                                                <div>
                                                    <div class="NotificationEmailRow">
                                                        <label>Notification Email</label>
                                                        <input type="text" class="RequiredInput NotificationEmail" />
                                                        <span class="RequiredSpan">*</span>
                                                        <a class="tooltips" data-html="true" data-toggle="tooltip" data-placement="bottom" title="Choose from contacts">
                                                            <i class="material-icons ContactPicker NotificationEmailPicker">assignment_ind</i>
                                                        </a>
                                                        
                                                    </div>
                                                    <div class="VerifyIncomeRow vertical-align-bottom">
                                                        <label>
                                                            <input type="checkbox" class="VerifyIncome" />Verify Income
                                                        </label>
                                                    </div>
                                                    <div class="CreditCardRow vertical-align-bottom Hidden">
                                                        <label>
                                                            <input type="checkbox" class="AllowPayWithCreditCard" />Pay with credit card
                                                        </label>
                                                    </div>
                                                    <div class="UseSpecificEmployerRecordSearchRow vertical-align-bottom">
                                                        <label>
                                                            <input type="checkbox" class="UseSpecificEmployerRecordSearch" />Use Specific Employer Record Search?
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="BorrowerAuthDocsSection padding-bottom-20px">
                                                <header class="header">Borrower Authorization Documents</header>
                                                <div>
                                                    <table class="DocumentTable"></table>
                                                </div>
                                            </div>
                                            <div class="SalaryKeySection padding-bottom-20px">
                                                <header class="header">Salary Key&nbsp;
                                                    <a class="tooltips" data-html="true" data-toggle="tooltip" data-placement="bottom" title="A salary key is a single-use, six digit number and is a form of authorization to pull an employment/income verification from The Work Number.">
                                                        <i class="material-icons" id="VoeSalaryKeyHelpIcon">&#xE887;</i>
                                                    </a>
                                                </header>
                                                <div>
                                                    <table class="SalaryKeyTable"></table>
                                                </div>
                                            </div>
                                            <div class="SelfEmploymentTarget table table-default table-font-default"></div>
                                        </div>
                                        <div class="buttonPanel">
                                            <input type="button" class="btn btn-default VOXPlaceOrderBtn" value="Place Order"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField runat="server" ID="AttachedDocumentsIds"/>

                    <div id="DocumentPickerDiv" runat="server" class="HiddenDiv">
                        <div class="table">
                            <div>
                                <div>
                                    <label class="text-grey">Search:</label>
                                </div>
                                <div>
                                    <input type="text" name="Query" id="Query" />
                                </div>
                                &nbsp;&nbsp;
                                <div>
                                    <label class="text-grey">Folder:</label>
                                </div>
                                <div>
                                    <asp:DropDownList runat="server" ID="DocTypeFilter" />
                                </div>
                            </div>
                        </div>

                        <div style="overflow-x:hidden">
                            <asp:Repeater runat="server" ID="m_repeater">
                                <HeaderTemplate>
                                    <table id="DocTable" class="tb-order-service primary-table modal-table-scrollbar none-hover-table select-document" width="100%">
                                        <thead class="header">
                                            <tr>
                                                <th class="header"><span class="DocPickerCheckbox"><input type="checkbox" name="selectAll" id="selectAll" /></span></th>
                                                <th><a>Folder</a></th>
                                                <th><a>DocType</a></th>
                                                <th><a>Description</a></th>
                                                <th><a>Last Modified</a></th>
                                                <th class="header">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <span class="DocPickerCheckbox">
                                                <input class=" picked" type="checkbox" data-docid='<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString())  %>'
                                                data-docname='<%# AspxTools.HtmlString(((EDocument)Container.DataItem).Folder.FolderNm) %> : <%# AspxTools.HtmlString(((EDocument)Container.DataItem).DocTypeName) %>' />
                                            </span>
                                            <a class="DocPickerLink " onclick="return selectEdoc('<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString() ) %>','<%# AspxTools.HtmlString(((EDocument)Container.DataItem).Folder.FolderNm) %> : <%# AspxTools.HtmlString(((EDocument)Container.DataItem).DocTypeName) %>');">select</a>
                                        </td>
                                        <td><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).Folder.FolderNm ) %></td>
                                        <td><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocTypeName ) %></td>
                                        <td><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).PublicDescription ) %></td>
                                        <td><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).LastModifiedDate.ToString("M/d/yy h:mm tt")) %></td>
                                        <td><a href="#" onclick="viewEdoc('<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString() ) %>'); return false;" class="view"  >view</a></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody></table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div id="DocumentAttachDiv" class="HiddenDiv">
            <div id="AttachmentTypeSection" runat="server">
                <asp:DropDownList runat="server" ID="DocAttachmentType">
                    <asp:ListItem Value="-1" Text="<-- Select Document Type -->"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <!-- This id was AttachedDocumentIds-->
                <asp:HiddenField runat="server" ID="HiddenField1" />
                <asp:TextBox runat="server" ID="AttachedDocuments" cssclass="fillRow textarea-attach-doc" TextMode="MultiLine" ReadOnly="true"></asp:TextBox><br />
            </div>
            <div class="text-right">
                <a onclick="if(!this.disabled)selectAttachmentDoc();">select documents</a>
            </div>
        </div>

        <div id="LoginPromptDiv" class="HiddenDiv">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></button>
            </div>
            <div class="modal-body">
                <table class="table-vendor">
                    <tr>
                        <td>
                            <label>
                                <strong>Login:</strong></label>
                        </td>
                        <td>
                            <input runat="server" type="text" id="LoginPrompt" />
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <label>
                                <strong>Password:</strong></label>
                        </td>
                        <td>
                            <input runat="server" type="password" id="PasswordPrompt" />
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <label>
                                <strong>Account ID:</strong></label>
                        </td>
                        <td>
                            <input runat="server" type="text" id="AccountIDPrompt" />
                        </td>
                        </tr><tr>
                    </tr>
                </table>
                <div id="ErrorDiv" class='text-danger'></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-flat" id="UpdateCredentialsBtn" type="button">Update</button>
            </div>
        </div>

        <div class="Hidden" id="CredentialsPopup">
            <div id="Credentials">
                <div class="modal-header">
                    <div class="modal-title">Credentials</div>
                </div>
                <div class="modal-body">
                    <table>
                        <tr id="AccountIdRow">
                            <td>
                                <label>Account ID</label>
                            </td>
                            <td>
                                <input type="text" class="RequiredInput" id="AccountId" />
                                <span class="RequiredSpan" id="AccountIdRequiredSpan">*</span>
                            </td>
                        </tr>
                        <tr id="UsernameRow">
                            <td>
                                <label>Username</label>
                            </td>
                            <td>
                                <input type="text" class="RequiredInput" id="Username" />
                                <span class="RequiredSpan" id="UsernameRequiredSpan">*</span>
                            </td>
                        </tr>
                        <tr id="PasswordRow">
                            <td>
                                <label>Password</label>
                            </td>
                            <td>
                                <input type="password" class="RequiredInput" id="Password" />
                                <span class="RequiredSpan" id="PasswordRequiredSpan">*</span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-flat" id="OkCredentialsBtn" value="OK" />
                    <input type="button" class="btn btn-flat" id="CancelCredentialsBtn" value="Cancel" onclick="LQBPopup.Return(null);"/>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
