﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalEmailLog.aspx.cs" Inherits="PriceMyLoan.UI.Main.OrderAppraisalEmailLog" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Email Log</title>
    <style type="text/css">
        input
        {
            border: default;
            padding: 0;
            height: 1.5em;
        }
        h2
        {
            margin: 0 0 2px 0;
            padding: 2px;
        }
        img
        {
            vertical-align: top;
        }
        .row
        {
            width: 100%;
            margin: 0 auto;
            overflow: hidden;
            font-size: 11px;
        }
        #TableContainer
        {
            height: 480px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
        .GridHeader
        {
            height: 4px;
        }
        .EmailInfoRow
        {
            height: 2em;
        }
        #footer
        {
            height:10px;
        }
        .InsetBorder {
            padding: 3px;
            margin: 3px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField id="VendorId" runat="server" />
        <asp:HiddenField id="AppraisalOrderNumber" runat="server" />
        <asp:HiddenField id="ErrorMessage" runat="server" />
        <div lqb-popup>
            <div class="modal-header">
                <button type="button" class="close"><i class="material-icons">&#xE5CD;</i></button>
                <h4 class="modal-title">Email Log</h4>
            </div>
            <div class="modal-body">
                <div id="logBody">
                    <div id="TableContainer" class="InsetBorder border-section">
                        <asp:Repeater id="EmailLog" runat="server" OnItemDataBound="EmailLog_OnItemDataBound">
                            <HeaderTemplate>
                                <table id="EmailLogTable" class="primary-table tb-order-service" cellpadding="2" cellspacing="1" style="width:100%;">
                                    <thead class="header">
                                        <tr>
                                            <th style="width:50%" runat="server" id="SubjectColumn">Subject</th>
                                            <th style="width:15%" runat="server" id="StatusColumn">From</th>
                                            <th style="width:25%">Date</th>
                                            <th style="width:10%">&nbsp;</th>
                                        </tr>
                                    </thead>
                                <tbody>
                            </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="odd EmailInfoRow" runat="server" id="Row">
                                        <td><ml:EncodedLiteral id="EmailSubject" runat="server"></ml:EncodedLiteral></td>
                                        <td><ml:EncodedLiteral id="EmailFrom" runat="server"></ml:EncodedLiteral></td>
                                        <td><ml:EncodedLiteral id="EmailDate" runat="server"></ml:EncodedLiteral></td>
                                        <td>
                                            <a href="#" id="ViewLink" title="View Email Message" runat="server">view</a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr class="even EmailInfoRow" runat="server" id="Row">
                                        <td><ml:EncodedLiteral id="EmailSubject" runat="server"></ml:EncodedLiteral></td>
                                        <td><ml:EncodedLiteral id="EmailFrom" runat="server"></ml:EncodedLiteral></td>
                                        <td><ml:EncodedLiteral id="EmailDate" runat="server"></ml:EncodedLiteral></td>
                                        <td>
                                            <a href="#" id="ViewLink" title="View Email Message" runat="server">view</a>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-flat" id="CloseWindowBtn" value="" >Cancel</button>
                <button class="btn btn-flat" id="SendEmailToOfficeBtn"  onclick="sendEmail('Office'); return false;"> Email Office</button>
                <button class="btn btn-flat" id="SendEmailToAppraiserBtn"  onclick="sendEmail('Appraiser'); return false;"> Email Appraiser</button>
            </div>
        </div>
    </form>
</body>
<script type="text/javascript">
    $(window).ready(function() {
        if ($.trim($('#ErrorMessage').val()) != '') {
            alert($('#ErrorMessage').val());
        }
        
    
    $("#CloseWindowBtn, .close").click(function()
    {
        parent.LQBPopup.Hide();
    });
    });
    $(window).on(".load", function() {
        $('.EmailMessageRow').hide();
    });
    var options = "Height=600px, Width=700px, center=yes, resizable=yes, scroll=no, status=no, help=no,";
    function viewEmail(target)
    {
        window.location = target;
    }
    function sendEmail(recipient)
    {
        var url = gVirtualRoot + '/webapp/OrderAppraisalEmailMessage.aspx'
                    + '?loanid='+ <%= AspxTools.JsString( LoanID ) %>
                    + '&mode=' + 'send'
                    + '&vendorID='+ VendorID
                    + '&orderNumber='+ OrderNumber
                    + '&sendTo='+ recipient;
        window.location = url,"_blank",options;
        return false;
    }
</script>
</html>
