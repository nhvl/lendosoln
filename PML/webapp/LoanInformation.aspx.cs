﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;

    /// <summary>
    /// Provides a means for viewing the terms of a loan and 
    /// its registered program.
    /// </summary>
    public partial class LoanInformation : UI.BasePage
    {
        /// <summary>
        /// Registers the view model for the specified <paramref name="dataloan"/>.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing program data for the page.
        /// </param>
        public static object GetViewModel(CPageData dataloan)
        {
            return new
            {
                sLId = dataloan.sLId,
                sStatusT = dataloan.sStatusT,
                sPmlSubmitStatusT = dataloan.sPmlSubmitStatusT,
                mainData = GetMainLoanData(dataloan),
                armData = GetArmData(dataloan),
                helocData = GetHelocData(dataloan),
                constants = GetConstants()
            };
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the initialization.
        /// </param>
        /// <param name="e">
        /// The arguments for the initialization.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("/json.js");
            this.RegisterJsScript("/webapp/LoanInformation.js");
            this.RegisterJsScript("/webapp/combobox.js");
            this.RegisterJsScript("geocode.js");
            this.RegisterService("geocode", "/webapp/GeoCodeService.aspx");
            this.RegisterJsScript("widgets/jquery.downpayment.js");

            this.RegisterCSS("bootstrap.min.css");
            this.RegisterCSS("/webapp/main.css");
            this.RegisterCSS("/webapp/LoanInformation.css");

            this.sLId.Value = this.LoanID.ToString();

            this.LoanTermsTabLink.Attributes.Add(
                "onclick",
                "changeTab(this, " + AspxTools.JsString(UpdateUrlQuery("tab", nameof(this.LoanTerms)).PathAndQuery) + ")");
            this.FannieMaeAddendumSectionsTabLink.Attributes.Add(
                "onclick",
                "changeTab(this, " + AspxTools.JsString(UpdateUrlQuery("tab", nameof(this.FannieMaeAddendumSections)).PathAndQuery) + ")");
            this.FreddieMacAddendumSectionsTabLink.Attributes.Add(
                "onclick",
                "changeTab(this, " + AspxTools.JsString(UpdateUrlQuery("tab", nameof(this.FreddieMacAddendumSections)).PathAndQuery) + ")");

            string currentTab = RequestHelper.GetQueryString("tab") ?? nameof(this.LoanTerms);
            this.RegisterJsGlobalVariables("LoanInformationTabId", currentTab);
            if (currentTab == nameof(this.LoanTerms))
            {
                this.LoanTerms.Visible = true;
            }
            else if (currentTab == nameof(this.FannieMaeAddendumSections))
            {
                this.FannieMaeAddendumSections.Visible = true;
            }
            else if (currentTab == nameof(this.FreddieMacAddendumSections))
            {
                this.FreddieMacAddendumSections.Visible = true;
            }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the load.
        /// </param>
        /// <param name="e">
        /// The arguments for the load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var dataloan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(LoanInformation));
            dataloan.InitLoad();

            this.LoanNavHeader.SetDataFromLoan(dataloan, LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationLoanTerms);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("LoanInformationViewModel", GetViewModel(dataloan));

            if (this.FannieMaeAddendumSections.Visible)
            {
                this.FannieMaeAddendumSections.LoadData(dataloan);
            }

            if (this.FreddieMacAddendumSections.Visible)
            {
                this.FreddieMacAddendumSections.LoadData(dataloan);
            }
        }

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return this.FannieMaeAddendumSections.AdditionalWorkflowOperations
                .Concat(this.FreddieMacAddendumSections.AdditionalWorkflowOperations);
        }

        public override IEnumerable<WorkflowOperation> GetReasonsForOp() => this.GetAdditionalOperationsThatNeedChecks();

        /// <summary>
        /// Updates the current request's URL's query string to have the specified name value pair set.
        /// </summary>
        /// <param name="name">The key of the query argument.</param>
        /// <param name="value">The value of the query argument.</param>
        /// <returns>A new <see cref="Uri"/> instance with the specified key set.</returns>
        private Uri UpdateUrlQuery(string name, string value)
        {
            var uriBuilder = new UriBuilder(this.Request.Url);
            System.Collections.Specialized.NameValueCollection queryArguments = System.Web.HttpUtility.ParseQueryString(uriBuilder.Query);
            queryArguments.Set(name, value);
            uriBuilder.Query = queryArguments.ToString(); // Actually of type HttpValueCollection, so this uses a custom ToString.
            return uriBuilder.Uri;
        }

        /// <summary>
        /// Obtains the main data for the loan, namely the lien position, 
        /// total loan amount, and other primary data points.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing data to pull from.
        /// </param>
        /// <returns>
        /// A complex type containing the primary loan data for the view model
        /// or null if the loan is a HELOC.
        /// </returns>
        private static object GetMainLoanData(CPageData dataloan)
        {
            if (dataloan.sIsLineOfCredit)
            {
                return null;
            }

            return new
            {
                sLpTemplateNm = dataloan.sLpTemplateNm,
                sLpTemplateNmSubmitted = dataloan.sLpTemplateNmSubmitted,
                sLienPosT = dataloan.sLienPosT,
                sFinalLAmt = dataloan.sFinalLAmt_rep,
                sFinMethT = dataloan.sFinMethT,
                sDaysInYr = dataloan.sDaysInYr_rep,
                sNoteIR = dataloan.sNoteIR_rep,
                sQualIR = dataloan.sQualIR_rep,
                sTerm = dataloan.sTerm_rep,
                sDue = dataloan.sDue_rep,
                sIOnlyMon = dataloan.sIOnlyMon_rep
            };
        }

        /// <summary>
        /// Obtains ARM data for the loan.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing data to pull from.
        /// </param>
        /// <returns>
        /// A complex type containing the ARM data for the view model or
        /// null if the loan is a HELOC or not using ARM financing.
        /// </returns>
        private static object GetArmData(CPageData dataloan)
        {
            if (dataloan.sIsLineOfCredit || dataloan.sFinMethT != E_sFinMethT.ARM)
            {
                return null;
            }

            return new
            {
                sRAdj1stCapR = dataloan.sRAdj1stCapR_rep,
                sRAdj1stCapMon = dataloan.sRAdj1stCapMon_rep,
                sRAdjCapR = dataloan.sRAdjCapR_rep,
                sRAdjCapMon = dataloan.sRAdjCapMon_rep,
                sRAdjLifeCapR = dataloan.sRAdjLifeCapR_rep,
                sRAdjMarginR = dataloan.sRAdjMarginR_rep,
                sArmIndexNameVstr = dataloan.sArmIndexNameVstr,
                sRAdjIndexR = dataloan.sRAdjIndexR_rep,
                sFullyIndexedR = dataloan.sFullyIndexedR_rep,
                sRAdjFloorR = dataloan.sRAdjFloorR_rep,
                sRAdjRoundT = dataloan.sRAdjRoundT,
                sRAdjRoundToR = dataloan.sRAdjRoundToR_rep,
                sIsConvertibleMortgage = dataloan.sIsConvertibleMortgage
            };
        }

        /// <summary>
        /// Obtains HELOC data for the loan.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing data to pull from.
        /// </param>
        /// <returns>
        /// A complex type containing the HELOC data for the view model
        /// or null if the loan is not a HELOC.
        /// </returns>
        private static object GetHelocData(CPageData dataloan)
        {
            if (!dataloan.sIsLineOfCredit)
            {
                return null;
            }

            return new
            {
                sLpTemplateNm = dataloan.sLpTemplateNm,
                sLpTemplateNmSubmitted = dataloan.sLpTemplateNmSubmitted,
                sLienPosT = dataloan.sLienPosT,
                sCreditLineAmt = dataloan.sCreditLineAmt_rep,
                sLAmtCalc = dataloan.sLAmtCalc_rep,
                sTerm = dataloan.sTerm_rep,
                sDue = dataloan.sDue_rep,
                sHelocDraw = dataloan.sHelocDraw_rep,
                sHelocRepay = dataloan.sHelocRepay_rep,
                sDaysInYr = dataloan.sDaysInYr_rep,
                sNoteIR = dataloan.sNoteIR_rep,
                sDailyPeriodicR = dataloan.sDailyPeriodicR_rep,
                sRAdj1stCapMon = dataloan.sRAdj1stCapMon_rep,
                sInitialRateT = dataloan.sInitialRateT,
                sArmIndexNameVstr = dataloan.sArmIndexNameVstr,
                sRAdjIndexR = dataloan.sRAdjIndexR_rep,
                sRAdjMarginR = dataloan.sRAdjMarginR_rep,
                sRLifeCapR = dataloan.sRLifeCapR_rep,
                sFullyIndexedR = dataloan.sFullyIndexedR_rep,
                sDailyPeriodicFullyIndexR = dataloan.sDailyPeriodicFullyIndexR_rep,
                sQualIR = dataloan.sQualIR_rep,
                sHelocAnnualFee = dataloan.sHelocAnnualFee_rep,
                sHelocDrawFee = dataloan.sHelocDrawFee_rep,
                sHelocMinimumAdvanceAmt = dataloan.sHelocMinimumAdvanceAmt_rep,
                sHelocReturnedCheckFee = dataloan.sHelocReturnedCheckFee_rep,
                sHelocStopPaymentFee = dataloan.sHelocStopPaymentFee_rep,
                sHelocTerminationFeePeriod = dataloan.sHelocTerminationFeePeriod_rep,
                sHelocTerminationFee = dataloan.sHelocTerminationFee_rep,
                sHelocMinimumPayment = dataloan.sHelocMinimumPayment_rep,
                sHelocMinimumPaymentPc = dataloan.sHelocMinimumPaymentPc_rep
            };
        }

        /// <summary>
        /// Obtains the constants for the view model.
        /// </summary>
        /// <returns>
        /// A complex type containing the constants for the view model.
        /// </returns>
        private static object GetConstants()
        {
            return new
            {
                LoanOpen = E_sStatusT.Loan_Open,
                NeverSubmittedInPml = E_sPmlSubmitStatusT.NeverSubmitted
            };
        }
    }
}
