﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmbeddedPmlPasswordExpired.aspx.cs" Inherits="PriceMyLoan.webapp.EmbeddedPmlPasswordExpired" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Your Password Has Expired</title>
    <style type="text/css">
        .container {
            padding-top: 25%;
        }

        .backBtn .pipeline-icon {
            -moz-transform: scaleX(-1);
            -ms-transform: scaleX(-1);
            -o-transform: scaleX(-1);
            -webkit-transform: scaleX(-1);
            transform: scaleX(-1);
        }
    </style>
    <script type="text/javascript">
        function backToLqb() {
            if (ML.IsQuickPricerLoan) {
                top.close();
                return;
            }

            var trueVirtualRoot = ML.VirtualRoot.replace(/\/embeddedpml/i, '');

            // Workaround implemented to preserve window management
            // when switching to and from the new PML UI
            try {
                var winOpener = parent.window.opener;
                if (null != winOpener && !winOpener.closed && winOpener.getSkipWindowManagement()) {
                    winOpener.gSkipWindowManagement = true;
                }
            }
            catch (e) { }

            if (ML.IsLead && !ML.IsEditLeadsInFullLoanEditor) {
                top.location.href = trueVirtualRoot + '/los/lead/leadmainframe.aspx?loanid=' + ML.LoanId;
            } else {
                top.location.href = trueVirtualRoot + '/newlos/loanapp.aspx?loanid=' + ML.LoanId;
            }
        }
    </script>
</head>
<body runat="server" id="MainBody">
    <form id="form1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-2">
                Your password is currently expired. You must update your password within your profile in order to proceed with running pricing.
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-xs-offset-5">
                <a class="backBtn" href="#" onclick="backToLqb();">
                    <i class="material-icons pipeline-icon">&#xE879;</i>
                    Go back to LendingQB
                </a>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
