﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Loan1003pg1.ascx.cs" Inherits="PriceMyLoan.webapp.Loan1003pg1" %>
<%@Register Src="~/webapp/Employment.ascx" TagName="Employment" TagPrefix="uc" %>

<div class="flex-checkbox-app-info">
    <div>
        <div><input type="checkbox" id="sMultiApps"   name="sMultiApps"   runat="server" /><label for="Loan1003pg1_sMultiApps">The income or assets of a person other than the "Borrower" will be used…</label></div>
    </div>
    <div>
        <div><input type="checkbox" id="aSpouseIExcl" name="aSpouseIExcl" runat="server" /><label for="Loan1003pg1_aSpouseIExcl">The income or assets of the Borrower's spouse will not be used…</label></div>
    </div>
</div>

<accordion><accordion-header>I. TYPE OF MORTGAGE AND TERMS OF LOAN</accordion-header><accordion-content>
    <div class="flex-space-between none-margin-bottom padding-left-p16 padding-right-p16 padding-bottom-p16">
        <div class="flex-alignment flex-alignment-w-pg1-section-1">
            <div class="col-xs-6">
                <div>
                    <div>
                        <label>Mortgage Applied for:</label>
                        <div class="table table-select-combine-input">
                            <div>
                                <div>
                                    <asp:DropDownList class="form-control-w170" ID="sLT" name="sLT" onchange="showHideCashOut();" Update="true" runat="server"/>
                                </div>
                                <div><input type="text" id="sLTODesc" name="sLTODesc" class="input-w120" runat="server" Update="true" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div>
                    <div>
                        <label>Agency Case Number</label>
                        <input type="text" id="sAgencyCaseNum" name="sAgencyCaseNum" class="input-w220" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="w-278">
            <div>
                <label>Lender Case Number</label>
                <input type="text" id="sLenderCaseNum" name="sLenderCaseNum" class="input-w220" runat="server"/>
            </div>
        </div>
    </div>

    <div class="grey-box-border margin-bottom" id="box-section1">
        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-1">
                <div class="col-xs-6">
                    <div class="flex-alignment-sub">
                        <div class="col-xs-6">
                            <div>
                                <div>
                                    <label>Purchase Price</label>
                                    <ml:MoneyTextBox ID="sPurchPrice" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                                </div>
                                <div>
                                    <label> Upfront MIP / FF</label>
                                    <ml:MoneyTextBox ID="sFfUfmipFinanced" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" readOnly="true"/>
                                </div>
                                <div>
                                    <label>Term</label>
                                    <input type="text" id="sTerm" name="sTerm" value="" class="form-control-w110" runat="server" Update="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div>
                                <div>
                                    <label>Down Payment %</label>
                                    <ml:PercentTextBox ID="sDownPmtPc" runat="server" preset="percent" CssClass="input-w60 input-textleft" value="0.000%" onchange="on_sDownPmtPc_change();" />
                                </div>
                                <div>
                                    <label>Total Loan Amt</label>
                                    <ml:MoneyTextBox ID="sFinalLAmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" readOnly="true"/>
                                </div>
                                <div>
                                    <label>Due</label>
                                    <input type="text" id="sDue" name="sDue" value="" class="form-control-w110" runat="server"  Update="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="flex-alignment-sub">
                        <div class="col-xs-6">
                            <div>
                                <div>
                                    <label>Equity / Down Pmt</label>
                                    <ml:MoneyTextBox ID="sEquity" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" Update="true" />
                                </div>
                                <div>
                                    <label>Note Rate</label>
                                    <ml:PercentTextBox ID="sNoteIR" runat="server" preset="percent" CssClass="input-w60 input-textleft" value="0.000%"  Update="true" />
                                </div>
                                <div>
                                    <label>Monthly Payment</label>
                                    <input type="text" id="sProThisMPmt" name="sProThisMPmt" runat="server" class="input-w100" readOnly="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div>
                                <div>
                                    <label>Loan Amt</label>
                                    <ml:MoneyTextBox ID="sLAmt" Update="true" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"/>
                                    <label class="lock-checkbox"><input type="checkbox" id="sLAmtLckd" name="sLAmtLckd" runat="server"  Update="true"/></label>
                                </div>
                                <div>
                                    <label><a id="QualRateCalcLink" onclick="calculateQualRate(this);">Qualifying Rate</a></label>
                                    <ml:PercentTextBox ID="sQualIR" runat="server" preset="percent" CssClass="input-w60 input-textleft" value="0.000%" Update="true"/>
                                    <label class="lock-checkbox"><input type="checkbox" id="sQualIRLckd" name="sQualIRLckd" runat="server"  Update="true"/></label>
                                </div>
                                <div id="other" class="nowrap">
                                    <label>Other Print Desc.</label>
                                    <input type="text" id="sFinMethPrintAsOtherDesc" name="sFinMethPrintAsOtherDesc" runat="server" class="input-w200" />
                                    <label class="lock-checkbox"><input type="checkbox" id="sFinMethodPrintAsOther" name="sFinMethodPrintAsOther" runat="server"  Update="true"/></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-278">
                <div>
                    <label>Amort. Type</label>
                    <div class="table table-select-combine-input">
                        <div>
                            <div>
                                <asp:DropDownList  class="select-w100" id="sFinMethT" runat="server" name="sFinMethT" Update="true"/>
                            </div>
                            <div><input type="text" id="sFinMethDesc" name="sFinMethDesc" class="input-w170" runat="server" Update="true"/></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</accordion-content></accordion>

<accordion><accordion-header>II. PROPERTY INFORMATION AND PURPOSE OF LOAN</accordion-header><accordion-content>
    <div class="box-section-border-red" id="box-section2">
        <div class="max-width-app-info-pg1">
            <div class="table">
                <div>
                    <div>
                       <label>Address</label>
                       <div class="table">
                            <div>
                                <div>
                                    <label>Street</label>
                                    <input type="text" id="sSpAddr" name="sSpAddr" class="form-control-address" runat="server" />
                                </div>
                                <div>
                                    <label>City</label>
                                    <input type="text" id="sSpCity" name="sSpCity" value="" class="input-w140" runat="server" />
                                </div>
                                <div>
                                    <label>County</label>
                                    <asp:DropDownList class="select-w145" id="sSpCounty" name="sSpCounty" runat="server"/>
                                </div>
                                <div>
                                    <label>State</label>
                                    <ml:StateDropDownList ID="sSpState" runat="server" />
                                </div>
                                <div>
                                    <label>Zip Code</label>
                                    <ml:ZipcodeTextBox ID="sSpZip" runat="server" Width="60" />
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="flex-space-between none-margin-bottom">
                <div class="flex-alignment flex-alignment-w-pg1-section-1">
                    <div class="col-xs-6">
                        <div class="flex-alignment-sub padding-bottom-p16">
                            <div class="col-xs-6">
                                <div>
                                    <label>No. of Units</label>
                                    <input value="" type="text" id="sUnitsNum" name="sUnitsNum" class="form-control-zipcode" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div>
                                    <label>Year Built</label>
                                    <input type="text" id="sYrBuilt" name="sYrBuilt" class="form-control-zipcode" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-278">
                    <div></div>
                </div>
            </div>
            <div class="table table-app-info wide">
                <div>
                    <div id="LegalDescription">
                        <label>Legal Description</label>
                        <textarea cols="1" rows="1" id="sSpLegalDesc" name="sSpLegalDesc" runat="server"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-section-border-red" id="box2-section2">
        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2">
                <div class="col-xs-3-5">
                    <div class="flex-alignment-sub">
                        <div class="col-xs-8">
                            <div>
                                <label>Purpose of Loan</label>
                                <div class="table table-select-combine-input">
                                    <div>
                                        <div>
                                            <asp:DropDownList class="form-control-w170" ID="sLPurposeT" name="sLPurposeT" runat="server" Update="true"/>
                                        </div>
                                        <div><input type="text" id="sOLPurposeDesc" name="sOLPurposeDesc" class="input-w120" runat="server" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div>
                                <label>Property will be</label>
                                <asp:DropDownList class="input-w145" id="aOccT" name="aOccT" runat="server"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2-5">
                    <div class="flex-alignment-sub">
                        <div class="col-xs-6">
                            <div>
                                <label>Rental occupancy rate</label>
                                <ml:PercentTextBox ID="sOccR" runat="server" preset="percent" Width="60" value="0.000%" />
                                <label class="lock-checkbox"><input type="checkbox" id="sOccRLckd" name="sOccRLckd" runat="server"  Update="true"/></label>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div>
                                <label> Gross rent</label>
                                <ml:MoneyTextBox ID="sSpGrossRent" runat="server" preset="money" Width="100" value="$0.00" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-128">
                <div></div>
            </div>
        </div>
    </div>
    <fieldset class="box-section-border-red" id="box3-section2">
        <header class="text-danger padding-bottom-p16">Complete this section if it is a construction or construction-permanent loan</header>
        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2">
                <div class="col-xs-1-5"><div>
                    <label>Year Acquired </label>
                    <input type="text" class="form-control-money" id="sLotAcqYr" name="sLotAcqYr" runat="server" />
                </div></div>
                <div class="col-xs-1-5"><div>
                    <label>Original Cost</label>
                    <ml:MoneyTextBox ID="sLotOrigC" runat="server" preset="money" Width="100" value="$0.00" />
                </div></div>
                <div class="col-xs-1-5"><div>
                    <label>Existing Liens </label>
                    <ml:MoneyTextBox ID="sLotLien" runat="server" preset="money" Width="100" value="$0.00" />
                </div></div>
                <div class="col-xs-1-5"><div>
                    <label>(a) Lot Value</label>
                    <ml:MoneyTextBox ID="sLotVal" runat="server" preset="money" Width="100" value="$0.00"  Update="true" />
                </div></div>
                <div class="col-xs-1-5"><div>
                    <label>(b) Improvements</label>
                    <ml:MoneyTextBox ID="sLotImprovC" runat="server" preset="money" Width="100" value="$0.00"  Update="true" />
                </div></div>
            </div>
            <div class="w-128"><div>
                <label>Total (a + b)</label>
                <ml:MoneyTextBox ID="sLotWImprovTot" runat="server" preset="money" Width="100" value="$0.00" ReadOnly="True" />
            </div></div>
        </div>
    </fieldset>
    <fieldset class="box-section-border-red" id="box4-section2">
        <header class="text-danger padding-bottom-p16">Complete this section if it is a refinance loan</header>
        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2 padding-bottom-p16">
                <div class="col-xs-1-5"><div>
                        <label>Year Acquired</label>
                        <input type="text" class="form-control-money" id="sSpAcqYr" name="sSpAcqYr" runat="server" />
                </div></div>
                <div class="col-xs-1-5"><div>
                        <label>Original Cost</label>
                        <ml:MoneyTextBox ID="sSpOrigC" runat="server" preset="money" Width="100" value="$0.00" />
                </div></div>
                <div class="col-xs-1-5"><div>
                        <label>Existing Liens</label>
                        <ml:MoneyTextBox ID="sSpLien" runat="server" preset="money" Width="100" value="$0.00" />
                </div></div>
                <div class="col-xs-2-5"><div>
                        <label>Purpose of Refinance</label>
                        <ml:ComboBox runat="server" ID="sRefPurpose" CssClass="select-w300">
                        </ml:ComboBox>
                </div></div>
            </div>
            <div class="w-128">
                <div></div>
            </div>
        </div>
        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2">
                <div class="col-xs-3-5">
                    <div>
                        <label>Describe Improvements</label>
                        <div class="table table-select-combine-input">
                            <div>
                                <div>
                                    <input type="text" id="sSpImprovDesc" name="sSpImprovDesc" class="describe-improvements" runat="server" />
                                </div>
                                <div class="improvements-drop-down">
                                    <asp:DropDownList class="select-w100" id="sSpImprovTimeFrameT" name="sSpImprovTimeFrameT" runat="server"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2-5">
                    <div>
                        <label>Cost</label>
                        <input type="text" id="sSpImprovC" name="sSpImprovC" value="$0.00" class="input-w100 input-textright" runat="server" />
                    </div>
                </div>
            </div>
            <div class="w-128">
                <div></div>
            </div>
        </div>
    </fieldset>
    <div class="box-section-border-red" id="box5-section2">
        <label>Name(s) in which Title will be held</label>
        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2 padding-bottom-p16">
                <div class="col-xs-3-5">
                    <div>
                        <input type="text" id="aTitleNm1" name="aTitleNm1" class="form-control-w380" runat="server"/>
                        <label class="lock-checkbox"><input runat="server" type="checkbox" id="aTitleNm1Lckd" name="aTitleNm1Lckd"  Update="true" /></label>
                    </div>
                </div>
                <div class="col-xs-2-5">
                    <div>
                        <input type="text" id="aTitleNm2" name="aTitleNm2" class="w-295"  runat="server" />
                        <label class="lock-checkbox"><input type="checkbox" id="aTitleNm2Lckd" name="aTitleNm2Lckd" runat="server"  Update="true"/></label>
                    </div>
                </div>
            </div>
            <div class="w-128">
                <div></div>
            </div>
        </div>

        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2 padding-bottom-p16">
                <div class="col-xs-3-5">
                    <div>
                        <label>Manner in which Title will be held</label>
                        <ml:ComboBox runat="server" ID="aManner" CssClass="w-444"></ml:ComboBox>
                    </div>
                </div>
                <div class="col-xs-1-5">
                    <div>
                        <label>Estate will be held in</label>
                        <asp:DropDownList class="form-control-w110" id="sEstateHeldT" name="sEstateHeldT" runat="server"  Update="true" />
                    </div>
                </div>
                <div class="col-xs-1-5">
                    <div>
                        <label>Expiration date</label>
                        <ml:DateTextBox runat="server" preset="date" Width="80" ID="sLeaseHoldExpireD" IsDisplayCalendarHelper="false" />
                        <%--<input type="text" id="sLeaseHoldExpireD" name="sLeaseHoldExpireD" class="input-w80" /><img src="images/icondate.jpg" alt="" class="icondate" />--%>
                    </div>
                </div>
            </div>
            <div class="w-128">
                <div></div>
            </div>
        </div>

        <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2">
                <div class="col-xs-3-5">
                    <div>
                        <label>Source of Down Payment</label>
                        <ml:ComboBox runat="server" ID="sDwnPmtSrc" CssClass="w-444"></ml:ComboBox>
                    </div>
                </div>
                <div class="col-xs-2-5">
                    <div>
                        <label>Explanation of Down Payment Source</label>
                        <input type="text" id="sDwnPmtSrcExplain" name="sDwnPmtSrcExplain" runat="server" class="w-444" />
                    </div>
                </div>
            </div>
            <div class="w-128">
                <div></div>
            </div>
        </div>
    </div>
</accordion-content></accordion>

<accordion><accordion-header>III. BORROWER INFORMATION</accordion-header><accordion-content>
    <fieldset class="box-section-border-red" id="box-section3">
        <header class="header header-border header-border-app-info">Borrower</header>
        <div class="table">
            <div>
                <div>
                    <label>Name</label>
                    <div class="table">
                        <div>
                            <div>
                                <label>First Name</label>
                                <input type="text" id="aBFirstNm" name="aBFirstNm" runat="server" class="input-w200"  onchange="updateBorrowerName();" Update="true" />
                            </div>
                            <div>
                                 <label>Middle Name</label>
                                <input runat="server" type="text" id="aBMidNm" name="aBMidNm" class="input-w200" Update="true"/>
                            </div>
                            <div>
                                <label>Last Name</label>
                                <input runat="server" type="text" id="aBLastNm" name="aBLastNm" class="input-w200"  onchange="updateBorrowerName();" Update="true"/>
                            </div>
                            <div>
                                <label>Suffix</label>
                                <ml:ComboBox runat="server" CssClass="select-w40" ID="aBSuffix"></ml:ComboBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table table-phone">
            <div>
                <div>
                    <label>Phone</label>
                    <div class="table">
                        <div>
                            <div>
                                <label>Home</label>
                                <input preset="phone" type="text" runat="server" id="aBHPhone" name="aBHPhone" class="input-w140" />
                            </div>
                            <div>
                                <label>Work</label>
                                <input preset="phone" runat="server" type="text" id="aBBusPhone" name="aBBusPhone" class="input-w140" />
                            </div>
                            <div>
                                <label>Cell</label>
                                <input preset="phone" runat="server" type="text" id="aBCellphone" name="aBCellphone" class="input-w140" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div class="ssn-app-info-pg1">
                    <label>SSN</label>
                    <input preset="ssn" runat="server" type="text" id="aBSsn" name="aBSsn" class="input-w100" />
                </div>
                <div class="date-app-info-pg1">
                    <label>Date of Birth</label>
                    <ml:DateTextBox runat="server" ID="aBDob" preset="date" CssClass="input-w80" IsDisplayCalendarHelper="false" />
                </div>
                <div>
                    <label>Age</label>
                    <input runat="server" type="text" id="aBAge" name="aBAge" class="input-w40" />
                </div>
                <div>
                    <label>Yrs. School</label>
                    <input runat="server" type="text" id="aBSchoolYrs" name="aBSchoolYrs" class="input-w40" />
                </div>
                <div>
                    <label>Email</label>
                    <input type="text" runat="server" id="aBEmail" name="aBEmail" class="input-w300" />
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div class="marital-status-app-info-pg1">
                    <label>Marital Status</label>
                    <asp:DropDownList runat="server" ID="aBMaritalStatT" CssClass="form-control-ssn" />
                </div>
                <div class="no-of-deps-app-info-pg1">
                    <label>No. of Deps</label>
                    <input type="text" runat="server" id="aBDependNum" name="aBDependNum" class="input-w40" />
                </div>
                <div>
                    <label>Dependents' Ages</label>
                    <input type="text" runat="server" id="aBDependAges" name="aBDependAges" class="input-w140" />
                </div>
            </div>
        </div>
        <header class="header-include-btn">
            <label>Present Address</label>
            <button type="button" onclick=" fillPresentAddress(); " class="btn btn-default">Copy from property address</button>
        </header>
        <div class="table">
            <div>
                <div>
                    <!-- <label>Address</label> -->
                    <div class="table">
                        <div>
                            <div>
                                <label>Street</label>
                                <input type="text" runat="server" id="aBAddr" name="aBAddr" class="input-w300" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>City</label>
                                <input type="text" runat="server" id="aBCity" name="aBCity" value="" class="input-w140" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aBState" runat="server" CssClass="" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aBZip" runat="server" CssClass="input-w60" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>Own/Rent</label>
                                <asp:DropDownList ID="aBAddrT" runat="server" CssClass="form-control-w140" onchange="syncMailingAddress();"/>
                            </div>
                            <div>
                                <label>No. Yrs</label>
                                <input runat="server" type="text" id="aBAddrYrs" name="aBAddrYrs" value="" class="input-w40" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="table">
            <div>
                <div><label>Mailing Address</label></div>
                <div><asp:DropDownList ID="aBAddrMailSourceT" name="aBAddrMailSourceT" onchange="syncMailingAddress();" runat="server"/></div>
            </div>
        </div>

        <div class="table">
            <div>
                <div>
                    <div class="table">
                        <div>
                            <div>
                                <label>Street</label>
                                <input type="text" runat="server" id="aBAddrMail" name="aBAddrMail" runat="server" class="input-w300" />
                            </div>
                            <div>
                                <label>City</label>
                                <input type="text" runat="server" id="aBCityMail" name="aBCityMail" runat="server" value="" class="input-w140" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aBStateMail" runat="server" CssClass="" />
                                <%--<select class="select-w40" id="aBStateMail" name="aBStateMail"><option>IA</option></select>--%>
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aBZipMail" runat="server" CssClass="input-w60" />
                                <%--<input type="text" id="aBZipMail" name="aBZipMail" value="92626" class="input-w60" />--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="table table-app-info">
            <div>
                <div>
                    <label>Former Addresses</label>
                    <div class="table">
                        <div>
                            <div>
                                <label>Street</label>
                                <input value="" runat="server" type="text" id="aBPrev1Addr" name="aBPrev1Addr" class="input-w300" />
                            </div>
                            <div>
                                <label>City</label>
                                <input type="text" runat="server" id="aBPrev1City" name="aBPrev1City" value="" class="input-w140" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aBPrev1State" runat="server" CssClass="" />
                                <%--<select class="select-w40" id="aBPrev1State" name="aBPrev1State"><option>AR</option></select>--%>
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aBPrev1Zip" runat="server" CssClass="input-w60" />
                                <%--<input type="text" id="aBPrev1Zip" name="aBPrev1Zip" value="21" class="input-w60" />--%>
                            </div>
                            <div>
                                <label>Own/Rent</label>
                                <asp:DropDownList ID="aBPrev1AddrT" runat="server" CssClass="form-control-w140"/>
                            </div>
                            <div>
                                <label>No. Yrs</label>
                                <input runat="server" type="text" id="aBPrev1AddrYrs" name="aBPrev1AddrYrs" class="input-w40" />
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Street</label>
                                <input value="" runat="server" type="text" id="aBPrev2Addr" name="aBPrev2Addr" class="input-w300" />
                            </div>
                            <div>
                                <label>City</label>
                                <input runat="server" type="text" id="aBPrev2City" name="aBPrev2City" value="" class="input-w140" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aBPrev2State" runat="server" CssClass="" />
                                <%--<select class="select-w40" id="aBPrev2State" name="aBPrev2State"><option>AR</option></select>--%>
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aBPrev2Zip" runat="server" CssClass="input-w60" />
                                <%--<input type="text" id="aBPrev2Zip" name="aBPrev2Zip" value="21" class="input-w60" />--%>
                            </div>
                            <div>
                                <label>Own/Rent</label>
                                <asp:DropDownList ID="aBPrev2AddrT" runat="server" CssClass="form-control-w140"/>
                            </div>
                            <div>
                                <label>No. Yrs</label>
                                <input type="text" id="aBPrev2AddrYrs" name="aBPrev2AddrYrs" class="input-w40" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="box-section-border-red" id="box-section3">
        <header class="header header-border header-border-app-info header-include-btn">
            <label>Co-borrower</label>
            <button type="button" onclick=" onCopyBorrowerClick(); " class="btn btn-default">Copy from borrower</button>
        </header>
        <div class="table">
            <div>
                <div>
                    <label>Name</label>
                    <div class="table">
                        <div>
                            <div>
                                <label>First Name</label>
                                <input type="text" id="aCFirstNm" name="aCFirstNm" class="input-w200" runat="server"  Update="true" />
                            </div>
                            <div>
                                <label>Middle Name</label>
                                <input runat="server" type="text" id="aCMidNm" name="aCMidNm" class="input-w200"  Update="true" />
                            </div>
                            <div>
                                <label>Last Name</label>
                                <input runat="server" type="text" id="aCLastNm" name="aCLastNm" class="input-w200"  Update="true" />
                            </div>
                            <div>
                                <label>Suffix</label>
                                <ml:ComboBox runat="server" CssClass="select-w40" ID="aCSuffix"  Update="true" ></ml:ComboBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div>
                    <label>Phone</label>
                    <div class="table table-phone">
                        <div>
                            <div>
                                <label>Home</label>
                                <input preset="phone" runat="server" type="text" id="aCHPhone" name="aCHPhone" class="input-w140" />
                            </div>
                            <div>
                                <label>Work</label>
                                <input preset="phone" runat="server" type="text" id="aCBusPhone" name="aCBusPhone" class="input-w140" />
                            </div>
                            <div>
                                <label>Cell</label>
                                <input preset="phone" runat="server" type="text" id="aCCellphone" name="aCCellphone" class="input-w140" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div class="ssn-app-info-pg1">
                    <label>SSN</label>
                    <input preset="ssn" runat="server" type="text" id="aCSsn" name="aCSsn" class="input-w100" />
                </div>
                <div class="date-app-info-pg1">
                    <label>Date of Birth</label>
                    <ml:DateTextBox runat="server" preset="date" CssClass="input-w80" ID="aCDob" IsDisplayCalendarHelper="false" />
                </div>
                <div>
                    <label>Age</label>
                    <input runat="server" type="text" id="aCAge" name="aCAge" class="input-w40" />
                </div>
                <div>
                    <label>Yrs. School</label>
                    <input runat="server" type="text" id="aCSchoolYrs" name="aCSchoolYrs" class="input-w40" />
                </div>
                <div>
                    <label>Email</label>
                    <input runat="server" type="text" id="aCEmail" name="aCEmail" class="input-w300" />
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div class="marital-status-app-info-pg1">
                    <label>Marital Status</label>
                    <asp:DropDownList runat="server" ID="aCMaritalStatT" CssClass="form-control-ssn" />
                </div>
                <div class="no-of-deps-app-info-pg1">
                    <label>No. of Deps</label>
                    <input type="text" id="aCDependNum" runat="server" name="aCDependNum" class="input-w40" />
                </div>
                <div>
                    <label>Dependents' Ages</label>
                    <input runat="server" type="text" id="aCDependAges" name="aCDependAges" class="input-w140" />
                </div>
            </div>
        </div>
        <header class="header-include-btn">
            <label>Present Address</label>
        </header>
        <div class="table">
            <div>
                <div>
                    <!-- <label>Address</label> -->
                    <div class="table">
                        <div>
                            <div>
                                <label>Street</label>
                                <input type="text" runat="server" id="aCAddr" name="aCAddr" class="input-w300" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>City</label>
                                <input runat="server" type="text" id="aCCity" name="aCCity" value="" class="input-w140" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aCState" runat="server" CssClass="" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aCZip" runat="server" CssClass="input-w60" onchange="syncMailingAddress();" />
                            </div>
                            <div>
                                <label>Own/Rent</label>
                                <asp:DropDownList ID="aCAddrT" runat="server" CssClass="form-control-w140"/>
                            </div>
                            <div>
                                <label>No. Yrs</label>
                                <input runat="server" type="text" id="aCAddrYrs" name="aCAddrYrs" value="" class="input-w40" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div><label>Mailing Address</label></div>
                <div><asp:DropDownList ID="aCAddrMailSourceT" name="aCAddrMailSourceT" onchange="syncMailingAddress();" runat="server"/></div>
            </div>
        </div>
        <div class="table">
            <div>
                <div>
                    <div class="table">
                        <div>
                            <div>
                                <label>Street</label>
                                <input runat="server" type="text" id="aCAddrMail" name="aCAddrMail" runat="server" class="input-w300" />
                            </div>
                            <div>
                                <label>City</label>
                                <input runat="server" type="text" id="aCCityMail" name="aCCityMail" value="" runat="server" class="input-w140" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aCStateMail" runat="server" CssClass="" />
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aCZipMail" runat="server" CssClass="input-w60" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table table-app-info">
            <div>
                <div>
                    <label>Former Addresses</label>
                    <div class="table">
                        <div>
                            <div>
                                <label>Street</label>
                                <input runat="server" value="" type="text" id="aCPrev1Addr" name="aCPrev1Addr" class="input-w300" />
                            </div>
                            <div>
                                <label>City</label>
                                <input runat="server" type="text" id="aCPrev1City" name="aCPrev1City" value="" class="input-w140" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aCPrev1State" runat="server" CssClass="" />
                                <%--<select class="select-w40" id="aCPrev1State" name="aCPrev1State"> <option>AR</option> </select>--%>
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aCPrev1Zip" runat="server" CssClass="input-w60" />
                            </div>
                            <div>
                                <label>Own/Rent</label>
                                <asp:DropDownList ID="aCPrev1AddrT" runat="server" CssClass="form-control-w140"/>
                            </div>
                            <div>
                                <label>No. Yrs</label>
                                <input runat="server" type="text" id="aCPrev1AddrYrs" name="aCPrev1AddrYrs" class="input-w40" />
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Street</label>
                                <input runat="server" value="" type="text" id="aCPrev2Addr" name="aCPrev2Addr" class="input-w300" />
                            </div>
                            <div>
                                <label>City</label>
                                <input runat="server" type="text" id="aCPrev2City" name="aCPrev2City" value="" class="input-w140" />
                            </div>
                            <div>
                                <label>State</label>
                                <ml:StateDropDownList ID="aCPrev2State" runat="server" CssClass="" />
                                <%--<select class="select-w40" id="aCPrev2State" name="aCPrev2State"> <option>AR</option> </select>--%>
                            </div>
                            <div>
                                <label>ZIPCode</label>
                                <ml:ZipcodeTextBox ID="aCPrev2Zip" runat="server" CssClass="input-w60" />
                                <%--<input type="text" id="aCPrev2Zip" name="aCPrev2Zip" value="21" class="input-w60" />--%>
                            </div>
                            <div>
                                <label>Own/Rent</label>
                                <asp:DropDownList ID="aCPrev2AddrT" runat="server" CssClass="form-control-w140"/>
                            </div>
                            <div>
                                <label>No. Yrs</label>
                                <input runat="server" type="text" id="aCPrev2AddrYrs" name="aCPrev2AddrYrs" class="input-w40" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
</accordion-content></accordion>

<accordion><accordion-header>IV. EMPLOYMENT INFORMATION</accordion-header><accordion-content>
    <div class="box-section-border-red" id="box-section4">
        <p class="none-mbt">
            <a onclick="loadEmploymentPage(); DialogHandler('e0'); return false;">Go to Employment</a>
        </p>
    </div>
</accordion-content></accordion>

<div id="EmploymentDialog" class=" modal-app-info modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></button>
                <h4 class="modal-title">Employment</h4>
            </div>
            <div class="modal-body">
                <uc:Employment ID="Employment" runat="server" />
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

