﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Web.UI;
    using global::DataAccess;
    using global::DataAccess.LoanComparison;
    using LendersOffice.Common;
    using LendersOffice.Admin;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.QuickPricer;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using PriceMyLoan.UI;

    public partial class pml : PriceMyLoan.UI.BasePage
    {

        /// <summary>
        /// This field denotes whether we came from LO or PML. Possible values are "LO" and "PML"
        /// </summary>
        protected string Source
        {
            get
            {
                return RequestHelper.GetSafeQueryString("source");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is an encompass user.
        /// </summary>
        /// <value>A bool determining if the user is using encompass.</value>
        protected bool IsEncompass
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.IsEncompassIntegrationEnabled || PrincipalFactory.CurrentPrincipal.IsSpecialRemnEncompassTotalAccount;
            }
        }

        protected bool IsLead
        {
            get;
            set;
        }

        private Guid m_LoanID;
        private bool NeedLoanIDRedirect
        {
            get
            {
                if (base.LoanID != Guid.Empty)
                {
                    return false;
                }

                return true;
            }
        }

        private bool? allowRegister = new bool?();
        private bool IsAllowRegisterRateLock
        {
            get
            {
                if (allowRegister.HasValue == false)
                {

                    allowRegister = this.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ||
                        this.CurrentUserCanPerform(WorkflowOperations.RegisterLoan);
                }

                return allowRegister.Value;
            }
        }

        private bool? allowRequestRateLock = null;
        private bool IsAllowRequestingRateLock
        {
            get
            {
                if (allowRequestRateLock.HasValue == false)
                {
                    allowRequestRateLock = this.CurrentUserCanPerform(WorkflowOperations.RequestRateLockRequiresManualLock)
                        || this.CurrentUserCanPerform(WorkflowOperations.RequestRateLockCausesLock);
                }

                return allowRequestRateLock.Value;
            }
        }


        protected IEnumerable<string> ReasonsCannotRegister
        {
            get
            {
                if (IsAllowRegisterRateLock)
                {
                    return new List<string>();
                }

                return this.GetReasonsUserCannotPerformOperation(WorkflowOperations.RegisterLoan);
            }
        }

        protected IEnumerable<string> ReasonsCannotLock
        {
            get
            {
                if (IsAllowRequestingRateLock)
                    return new List<string>(); // User is not blocked--no reasons
                return this.GetReasonsUserCannotPerformOperation(WorkflowOperations.RequestRateLockRequiresManualLock, WorkflowOperations.RequestRateLockCausesLock);
            }
        }

        public override Guid LoanID
        {
            get
            {
                if (m_LoanID == Guid.Empty)
                {
                    if (base.LoanID == Guid.Empty)
                    {
                        if (PriceMyLoanUser.IsActuallyUsePml2AsQuickPricer)
                        {
                            try
                            {
                                m_LoanID = QuickPricer2LoanPoolManager.GetSandboxedUnusedLoanAndMarkAsInUse();
                            }
                            catch (Exception e)
                            {
                                Tools.LogErrorWithCriticalTracking(e);
                                throw;
                            }
                        }
                        else
                        {
                            var usrMsg = "May not run PML without a loan id.";
                            var devMsg = string.Format(
                                "User {0} attempted to run PML without a LoanID",
                                string.Format(
                                "UserID: {0}, UserType: {1},  LoginName: {2}, Broker {3}",
                                PriceMyLoanUser.UserId, // {0}
                                PriceMyLoanUser.Type, // {1}
                                PriceMyLoanUser.LoginNm, // {2}
                                PriceMyLoanUser.BrokerDB.Name)); // {3}
                            throw new CBaseException(usrMsg, devMsg);
                        }
                    }
                    else
                    {
                        m_LoanID = base.LoanID;
                    }
                }
                return m_LoanID;
            }
        }

        protected bool IsNew
        {
            get
            {
                string isNew = RequestHelper.GetSafeQueryString("isNew");
                if (!string.IsNullOrEmpty(isNew) && isNew.ToLower() == "t")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected bool DisplayPriceGroup { get; set; } //OPM 110135
        protected bool RequireEmail { get; set; } //OPM 112368
        protected bool EnableMultipleApps { get; set; } //OPM 112419
        protected bool HideOrderNewCredit { get; set; } //OPM 113201
        protected bool IsEditLeadsInFullLoanEditor { get; set; } // OPM 233487
        protected bool IsLineOfCredit { get; set; }
        protected bool UserDoesntNeedCreditToPrice
        {
            get { return PriceMyLoanUser.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport); }
        }

        protected bool IsStandAloneSecond { get; set; } // OPM 107986

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            List<WorkflowOperation> current = new List<WorkflowOperation> {
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.UseOldSecurityModelForPml,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock,
                WorkflowOperations.RunDo,
                WorkflowOperations.RunDu,
                WorkflowOperations.RunLp,
                WorkflowOperations.LeadToLoanConversion,
            };

            return current;
        }

        public override IEnumerable<WorkflowOperation> GetReasonsForOp()
        {

            List<WorkflowOperation> current = new List<WorkflowOperation>() {
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock,
                WorkflowOperations.RunDo,
                WorkflowOperations.RunDu,
                WorkflowOperations.RunLp,
                WorkflowOperations.LeadToLoanConversion,
            };

            return current;
        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected void PageInit(object sender, EventArgs e)
        {
            if (!this.CurrentUserCanPerformPricing())
            {
                PriceMyLoanRequestHelper.AccessDenial(LoanID);
            }

            RegisterJsScript("webapp/pmlutils.js");
        }

        protected bool IsAssignable(E_RoleT roleT, E_BranchChannelT branchChannelT)
        {
            if (branchChannelT == E_BranchChannelT.Correspondent)
            {
                if (PriceMyLoanConstants.IsEmbeddedPML && roleT != E_RoleT.Pml_Secondary)
                {
                    return false;
                }
                else if (roleT != E_RoleT.Pml_Secondary && roleT != E_RoleT.Pml_PostCloser &&
                    roleT != E_RoleT.LoanOfficer && roleT != E_RoleT.Pml_BrokerProcessor)
                {
                    return false;
                }
            }
            else
            {
                if (PriceMyLoanConstants.IsEmbeddedPML && roleT != E_RoleT.LoanOfficer)
                {
                    return false;
                }
                else if (roleT != E_RoleT.LoanOfficer && roleT != E_RoleT.Pml_BrokerProcessor)
                {
                    return false;
                }
            }

            return true;
        }

        protected string GetUserPickerUrl(E_RoleT roleT, E_BranchChannelT branchChannelT)
        {
            if (!IsAssignable(roleT, branchChannelT))
            {
                return "";
            }

            var role = Role.Get(roleT);

            var roleFriendlyDescription = role.ModifiableDesc;

            if (!PriceMyLoanConstants.IsEmbeddedPML)
            {
                roleFriendlyDescription = roleFriendlyDescription
                    .Replace("(External)", "")
                    .TrimWhitespaceAndBOM();
            }

            var rolePickerSettings = new PmlEmployeeRoleControlValues()
            {
                LoanId = LoanID,
                OwnerId = PriceMyLoanUser.UserId,
                RoleDesc = roleFriendlyDescription,
                RoleId = role.Id,
                ShowMode = "fullsearch",
                Role = role.Desc,
                SearchFilter = "",
                EmployeeId = Guid.Empty
            };

            if (PriceMyLoanConstants.IsEmbeddedPML)
            {
                rolePickerSettings.ShowMode = "full";
            }

            var settingsKey = PmlEmployeeRoleControlValues.Save(
                rolePickerSettings,
                PriceMyLoanConstants.CookieExpireMinutes);

            return Page.ResolveClientUrl("~/main/RoleSearch.aspx?k=" + settingsKey);
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            TransformData(true); // Transform LO data to PML data

            if (this.IsEncompass)
            {
                Body.Attributes.Add("class", "encompass");
                RegisterJsGlobalVariables("IsEncompass", "true");
            }

            if( RequestHelper.GetBool("WithinPipeline"))
            {
                this.Body.Attributes["class"] = this.Body.Attributes["class"] + " within-pipeline";
            }

            BrokerDB CurrentBroker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(LoanID, typeof(pml));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            bool isEnablePmlResults3 = CurrentBroker.IsEnablePmlResults3Ui(PrincipalFactory.CurrentPrincipal);
            this.UiTypeSwitchEmbeddedPml.Visible = isEnablePmlResults3 && PriceMyLoanConstants.IsEmbeddedPML && !CurrentBroker.IsForcePML3;
            this.UiTypeSwitchRetailPortal.Visible = isEnablePmlResults3 && !PriceMyLoanConstants.IsEmbeddedPML && !CurrentBroker.IsForcePML3;
            this.Pml3RetailModeSpacer.Visible = this.UiTypeSwitchRetailPortal.Visible;
            HeaderSpacerRow.Visible = !isEnablePmlResults3;
            isEnablePmlResults3 = (isEnablePmlResults3 && RequestHelper.GetBool("isBeta")) || CurrentBroker.IsForcePML3;

            if(isEnablePmlResults3){
                this.Body.Attributes["class"] = this.Body.Attributes["class"] + " pml-body-v3";
            }

            PricingResultsContainer.Visible = isEnablePmlResults3;
            Results1.Visible = !isEnablePmlResults3;

            ClientScript.RegisterHiddenField("IsUsing2015GFEUITPO", (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy).ToString());

            if (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent &&
                PriceMyLoanConstants.IsEmbeddedPML)
            {
                SecondaryEmployeeLabel.Text = "(External) Secondary";
            }

            IsLead = Tools.IsStatusLead(dataLoan.sStatusT);
            IsEditLeadsInFullLoanEditor = CurrentBroker.IsEditLeadsInFullLoanEditor;
            DisplayPriceGroup = CurrentBroker.IsShowPriceGroupInEmbeddedPml;
            RequireEmail = CurrentBroker.IsaBEmailRequiredInPml;
            EnableMultipleApps = CurrentBroker.IsPricingMultipleAppsSupported;
            HideOrderNewCredit = !CurrentBroker.AllowPmlUserOrderNewCreditReport;
            IsStandAloneSecond = dataLoan.sIsStandAlone2ndLien; // || (dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sIsLineOfCredit);
            IsLineOfCredit = dataLoan.sIsLineOfCredit;

            var isQP2File = E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT;
            // control load happens after page load, but control init happens before page init.
            // all of these are kind of hacky.  Why not just share the loan across the controls?
            Action1.IsQP2File = isQP2File;
            Disclaimer1.IsQP2File = isQP2File;
            DataInput1.IsQP2File = isQP2File;
            ResultsFilter1.EnableAdvancedFilterOptionsForPriceEngineResults = CurrentBroker.IsAdvancedFilterOptionsForPricingEnabled(PrincipalFactory.CurrentPrincipal);

            DataInput1.IsLineOfCredit = dataLoan.sIsLineOfCredit;

            var userHasQP2Enabled = this.PriceMyLoanUser.IsActuallyUsePml2AsQuickPricer;

            var ddls = ExtractDDLs(this as Control, dataLoan);
            var hiddenOptions = ExtractHiddenOptions(this as Control);
            var citizenshipDropdownConfigurations = GetAllCitizenshipDropdownConfigurations(dataLoan.sGseTargetApplicationT);

            if (CurrentBroker.RemoveMessageToLenderTPOPortal && false == PriceMyLoanConstants.IsEmbeddedPML || (CurrentBroker.RemoveMessageToLenderEmbeddedPML && PriceMyLoanConstants.IsEmbeddedPML))
            {
                messageBtnContainer.Visible = false;
            }

            this.LoanNavHeader.Visible = !isQP2File && this.PriceMyLoanUser.EnableNewTpoLoanNavigation;
            this.ContentHeader.Visible = this.LoanNavHeader.Visible;
            this.Header2017UI.Visible = this.PriceMyLoanUser.EnableNewTpoLoanNavigation;
            this.Header2017Title.InnerText = isQP2File ? "Quick Pricer" : "Pricing";

            if (isQP2File)
            {
                HeaderInfo.Visible = false;
                this.Body.Attributes["class"] += " qp2";
                if (false == PriceMyLoanConstants.IsEmbeddedPML)
                {
                    HeaderForTPOQP2.Visible = true;
                }
            }
            else if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                this.HeaderInfo.Visible = false;
                this.HeaderForTPOQP2.Visible = false;

                this.LoanNavHeader.SetDataFromLoan(dataLoan, LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationPricing);
            }
            else
            {
                // QP2 elements are invisible by default.
            }

            var pmlData = new
            {
                Dropdowns = ddls,
                HidenOptions = hiddenOptions,
                CitizenshipDropdownConfigurations = citizenshipDropdownConfigurations,
                Source = Source,
                IsLead = IsLead,
                IsEditLeadsInFullLoanEditor = IsEditLeadsInFullLoanEditor,
                IsNew = IsNew,
                UserDoesntNeedCreditToPrice = UserDoesntNeedCreditToPrice,
                IsReadOnly = IsReadOnly,
                DisplayPriceGroup = DisplayPriceGroup,
                RequireEmail = RequireEmail,
                EnableMultipleApps = EnableMultipleApps,
                HideOrderNewCredit = HideOrderNewCredit,
                IsStandAloneSecond = IsStandAloneSecond,
                IsLineOfCredit = IsLineOfCredit,
                IsEnableHELOC = CurrentBroker.IsEnableHELOC,
                UserHasQP2Enabled = userHasQP2Enabled,
                IsEmbeddedPML = PriceMyLoanConstants.IsEmbeddedPML,
                BrokerIsAllowExternalUserToCreateNewLoan = CurrentBroker.IsAllowExternalUserToCreateNewLoan,
                IsQP2 = isQP2File,


                Permissions = new
                {
                    RunPMLToStep3 = CurrentUserCanPerform(WorkflowOperations.RunPmlToStep3), // step 3 is the property and loan info tab
                    RunPMLRegisteredLoans = CurrentUserCanPerform(WorkflowOperations.RunPmlForRegisteredLoans),
                    RunPMLAllLoans = CurrentUserCanPerform(WorkflowOperations.RunPmlForAllLoans)
                },
                Constants = new
                {
                    E_sLPurposeT = new
                    {
                        Purchase = E_sLPurposeT.Purchase,
                        Refin = E_sLPurposeT.Refin,
                        RefinCashout = E_sLPurposeT.RefinCashout,
                        Construct = E_sLPurposeT.Construct,
                        ConstructPerm = E_sLPurposeT.ConstructPerm,
                        Other = E_sLPurposeT.Other,
                        FhaStreamlinedRefinance = E_sLPurposeT.FhaStreamlinedRefinance,
                        VaIrrrl = E_sLPurposeT.VaIrrrl,
                        HomeEquity = E_sLPurposeT.HomeEquity
                    },
                    E_CustomPmlFieldType = new
                    {
                        Blank = E_CustomPmlFieldType.Blank,
                        Dropdown = E_CustomPmlFieldType.Dropdown,
                        NumericGeneric = E_CustomPmlFieldType.NumericGeneric,
                        NumericMoney = E_CustomPmlFieldType.NumericMoney,
                        NumericPercent = E_CustomPmlFieldType.NumericPercent
                    },
                    E_CustomPmlFieldVisibilityT = new
                    {
                        AllLoans = E_CustomPmlFieldVisibilityT.AllLoans,
                        PurchaseLoans = E_CustomPmlFieldVisibilityT.PurchaseLoans,
                        RefinanceLoans = E_CustomPmlFieldVisibilityT.RefinanceLoans,
                        HomeEquityLoans = E_CustomPmlFieldVisibilityT.HomeEquityLoans
                    },
                    E_sLoanFileT = new
                    {
                        Loan = E_sLoanFileT.Loan,
                        Sandbox = E_sLoanFileT.Sandbox,
                        QuickPricer2Sandboxed = E_sLoanFileT.QuickPricer2Sandboxed
                    },
                    E_BranchChannelT = new
                    {
                        Wholesale = E_BranchChannelT.Wholesale,
                        Correspondent = E_BranchChannelT.Correspondent
                    }
                },
                SecondaryPickerUrl = this.GetUserPickerUrl(E_RoleT.Pml_Secondary, dataLoan.sBranchChannelT),
                PostCloserPickerUrl = this.GetUserPickerUrl(E_RoleT.Pml_PostCloser, dataLoan.sBranchChannelT),
                CustomPMLFields = CurrentBroker.CustomPmlFieldList.ValidFieldsSortedByRank,
                ResultsDirtyBlacklist = new string[] { "sHorizonOfBorrowerInterestInMonths" },
                IsAllowHelocZeroInitialDraw = CurrentBroker.AllowHelocZeroInitialDraw_141171,
                BorrPdCompMayNotExceedLenderPdComp = CurrentBroker.BorrPdCompMayNotExceedLenderPdComp,
                LoanOfficerPickerUrl = this.GetUserPickerUrl(E_RoleT.LoanOfficer, dataLoan.sBranchChannelT),
                ProcessorPickerUrl = this.GetUserPickerUrl(E_RoleT.Pml_BrokerProcessor, dataLoan.sBranchChannelT),
                IsDisableAutoCalcOfTexas50a6 = CurrentBroker.IsDisableAutoCalcOfTexas50a6,
                EnableNewTpoLoanNavigation = this.PriceMyLoanUser.EnableNewTpoLoanNavigation,
                HasLinkedSecond = dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sLinkedLoanInfo.IsLoanLinked
            };

            var serializedData = LendersOffice.Common.ObsoleteSerializationHelper.JavascriptJsonSerialize(pmlData);

            ClientScript.RegisterStartupScript(typeof(pml), "pmlData", "var pmlData = " + serializedData, true);
            Action1.IsLead = IsLead;

            // OPM 231555 - Variables needed for lead to loan conversion
            bool hasArchives = false;
            if (CurrentBroker.IsGFEandCoCVersioningEnabled == true
                && ((dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy && dataLoan.GFEArchives.Any())
                || (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sClosingCostArchive.Any())))
            {
                hasArchives = true;
            }

            this.RegisterJsGlobalVariables("IsUlad2019", dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019);
            RegisterJsGlobalVariables("HasArchives", hasArchives);
            RegisterJsGlobalVariables("IsForceLeadToLoanTemplate", CurrentBroker.IsForceLeadToLoanTemplate);
            RegisterJsGlobalVariables("IsForceLeadToUseLoanCounter", CurrentBroker.IsForceLeadToUseLoanCounter);
            RegisterJsGlobalVariables("LoanInfo_DuplicateLoanNumber", JsMessages.LoanInfo_DuplicateLoanNumber);
            RegisterJsGlobalVariables("LeadHasArchives_ForceTemplate", JsMessages.LeadHasArchives_ForceTemplate);
            RegisterJsGlobalVariables("LeadHasArchives", JsMessages.LeadHasArchives);

            RegisterJsGlobalVariables("CanRunDo", this.CurrentUserCanPerform(WorkflowOperations.RunDo));
            RegisterJsGlobalVariables("CanRunDu", this.CurrentUserCanPerform(WorkflowOperations.RunDu));
            RegisterJsGlobalVariables("CanRunLp", this.CurrentUserCanPerform(WorkflowOperations.RunLp));
            this.RegisterJsGlobalVariables("CanConvertLead", this.CurrentUserCanPerform(WorkflowOperations.LeadToLoanConversion));

            RegisterJsGlobalVariables("RunDoDenialReason", this.GetReasonsUserCannotPerformOperation(WorkflowOperations.RunDo).FirstOrDefault());
            RegisterJsGlobalVariables("RunDuDenialReason", this.GetReasonsUserCannotPerformOperation(WorkflowOperations.RunDu).FirstOrDefault());
            RegisterJsGlobalVariables("RunLpDenialReason", this.GetReasonsUserCannotPerformOperation(WorkflowOperations.RunLp).FirstOrDefault());
            this.RegisterJsGlobalVariables("CanConvertLeadDenialReason", string.Join("\n", this.GetReasonsUserCannotPerformOperation(WorkflowOperations.LeadToLoanConversion)));

            if (isEnablePmlResults3)
            {
                this.RegisterService("main", "/main/mainservice.aspx");
                this.RegisterCSS("PricingResults.min.css");
                this.RegisterJsScript("angular-1.4.8.min.js");
                this.RegisterJsScript("angular-sanitize-1.4.8.min.js");
                this.RegisterJsScript("PricingResults.v3.js");
                this.RegisterJsScript("Pml_Submit.js");

                PmlResults3FieldRegisterer.RegisterFields(dataLoan, this, false, this.HasDuplicate(dataLoan), ReasonsCannotLock, ReasonsCannotRegister);
            }
        }

        private bool? hasDuplicate = null;
        protected bool HasDuplicate(CPageData dataLoan)
        {
                if (!hasDuplicate.HasValue)
                {
                    hasDuplicate = DuplicateFinder.FindDuplicateLoansPer105723(dataLoan.sLId, dataLoan.sBrokerId, dataLoan.sSpAddr).Count() > 0;
                }
                return hasDuplicate.Value;
        }

        /// <summary>
        /// Gets a map for citizenship dictionary configurations based on citizenship and residency declarations.
        /// </summary>
        /// <param name="targetApplicationType">
        /// The target application type for the loan file.
        /// </param>
        /// <remarks>
        /// The dropdown options for a citizenship and residency declaration are accessed map[citizenship][residency].
        /// </remarks>
        /// <returns>The map with all dropdown configurations.</returns>
        private Dictionary<string, Dictionary<string, List<KeyValuePair<string, string>>>> GetAllCitizenshipDropdownConfigurations(GseTargetApplicationT targetApplicationType)
        {
            var configurations = new Dictionary<string, Dictionary<string, List<KeyValuePair<string, string>>>>();
            var options = new[] { "", "N", "Y" };

            foreach (var citizenship in options)
            {
                configurations[citizenship] = new Dictionary<string, List<KeyValuePair<string, string>>>();
                foreach (var residency in options)
                {
                    var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);
                    var entries = Tools.ExtractDDLEntries(
                        ddl => Tools.Bind_aProdCitizenT(ddl, config.ValidValues));
                    configurations[citizenship][residency] = entries;
                }
            }

            return configurations;
        }

        /// <summary>
        /// Recurse through the control tree for IPopulateDropDownsControls,
        /// and return all the dropdowns.
        /// </summary>
        /// <param name="root">
        /// The root control.
        /// </param>
        /// <param name="loan">
        /// The loan used to control population of certain dropdowns.
        /// </param>
        /// <returns>
        /// The extracted dropdown entries.
        /// </returns>
        private Dictionary<string, object> ExtractDDLs(Control root, CPageData loan)
        {
            var ddls = new Dictionary<string, object>();
            if (root as IPopulateDropDownsControl != null)
            {
                if (!root.Visible)
                {
                    return ddls; // Return an empty dictionary if the control is not visible
                }

                var currentDDLs = (root as IPopulateDropDownsControl).PopulateDDLs(loan);

                // merge dictionaries
                foreach (var item in currentDDLs)
                {
                    if (!ddls.ContainsKey(item.Key))
                    {
                        ddls.Add(item.Key, item.Value);
                    }
                }
            }

            foreach (Control child in root.Controls)
            {
                var childDDLs = ExtractDDLs(child, loan);

                // merge dictionaries
                foreach (var item in childDDLs)
                {
                    if (!ddls.ContainsKey(item.Key))
                    {
                        ddls.Add(item.Key, item.Value);
                    }
                }
            }
            return ddls; // return global dict
        }

        /// <summary>
        /// Recurse through the control tree for IPopulateDropDownsControls,
        /// and return all the dropdowns' hidden options list.
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private Dictionary<string, object> ExtractHiddenOptions(Control root)
        {
            var ddls = new Dictionary<string, object>();
            if (root as IPopulateDropDownsControl != null)
            {
                if (!root.Visible)
                {
                    return ddls; // Return an empty dictionary if the control is not visible
                }

                var currentDDLs = (root as IPopulateDropDownsControl).GetHiddenOptions();

                // merge dictionaries
                foreach (var item in currentDDLs)
                {
                    if (!ddls.ContainsKey(item.Key))
                    {
                        ddls.Add(item.Key, item.Value);
                    }
                }
            }

            foreach (Control child in root.Controls)
            {
                var childDDLs = ExtractHiddenOptions(child);

                // merge dictionaries
                foreach (var item in childDDLs)
                {
                    if (!ddls.ContainsKey(item.Key))
                    {
                        ddls.Add(item.Key, item.Value);
                    }
                }
            }
            return ddls; // return global dict
        }

        protected override void OnPreInit(EventArgs e)
        {
            if (NeedLoanIDRedirect)
            {
                var currAbsoluteUri = Request.Url.AbsoluteUri;
                string urlWithLoanID = currAbsoluteUri + (currAbsoluteUri.Contains("?") ? "&" : "?") + "LoanID=" + LoanID;

                // ideally use false and IsDoingRedirect to true and then block all event handlers from running.
                try
                {
                    Response.Redirect(urlWithLoanID, true);
                }
                catch (ThreadAbortException tae)
                {
                    throw new ExpectedThreadAbortException("redirecting PML.", tae);
                }

                //Response.SuppressContent = true;
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //IsDoingRedirect = true;
                //return;
            }
            base.OnPreInit(e);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            this.RegisterCSS("/webapp/pml.css");
            this.RegisterCSS("/webapp/grid.css");

            this.RegisterJsGlobalVariables("sLId", LoanID);
            this.RegisterJsGlobalVariables("appid", RequestHelper.GetGuid("appid", Guid.Empty));
            BrokerDB CurrentBroker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);

            // Forcing all users to best price display mode
            // This is strictly for the initial beta test phase only
            // TODO: Remove this and honor user and broker permissions once all programs display mode is ready
            this.RegisterJsGlobalVariables("displayTypeEnabled", false);
            this.RegisterJsGlobalVariables("bestPriceDisabled", false);
            //this.RegisterJsGlobalVariables("displayTypeEnabled", CurrentBroker.IsBestPriceEnabled && !PriceMyLoanUser.IsAlwaysUseBestPrice);
            //this.RegisterJsGlobalVariables("bestPriceDisabled", !CurrentBroker.IsBestPriceEnabled);            // jquery implicitly included
            this.RegisterJsScript("jquery.tmpl.js");


            this.RegisterJsScript("webapp/pml.js");
            this.RegisterJsScript("mask.js");

            this.RegisterService("PML", "/webapp/PMLService.aspx");

            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
