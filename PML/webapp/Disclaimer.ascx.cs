﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.Common;
using LendersOffice.Admin;
using PriceMyLoan.Security;
using DataAccess;

namespace PriceMyLoan.webapp
{
    public partial class Disclaimer : System.Web.UI.UserControl
    {
        public bool IsQP2File { get; set; }

        protected PriceMyLoanPrincipal PriceMyLoanUser
        {
            get
            {
                if (Page is pml)
                {
                    return ((pml)Page).PriceMyLoanUser;
                }

                if (null == Context || null == Context.User)
                {
                    return null;
                }
                if (Context.User is PriceMyLoanPrincipal)
                {
                    return (PriceMyLoanPrincipal)Context.User;
                }
                if (Context.User is System.Security.Principal.GenericPrincipal)
                {
                    return null;
                }

                Tools.LogBug("Unexpected principal type: " + Context.User.GetType().ToString());
                return null;
            }
        }

        protected BrokerDB CurrentBroker
        {
            get
            {
                return BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsQP2File)
            {
                if (PriceMyLoanConstants.IsEmbeddedPML || CurrentBroker.IsAllowExternalUserToCreateNewLoan)
                {
                    DisclaimerLabelConvertToLeadOrLoan.Visible = true;
                }
                
                CreateLeadOrLoanText.Visible = PriceMyLoanConstants.IsEmbeddedPML;
                CreateLoanText.Visible = false == PriceMyLoanConstants.IsEmbeddedPML;
            }
            else
            {
                // QP2.0 specific elements default to Visible false.
            }
        }
    }
}