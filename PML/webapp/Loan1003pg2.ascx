<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Loan1003pg2.ascx.cs" Inherits="PriceMyLoan.webapp.Loan1003pg2" %>
<%@ Register TagPrefix="uc" TagName="Assets" Src="~/webapp/Assets.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<div onload="p2_init();">
<accordion><accordion-header>V. MONTHLY INCOME AND COMBINED HOUSING EXPENSE INFORMATION</accordion-header><accordion-content>
    <fieldset class="box-section-border-red" id="box-section5">
        <header class="header header-border header-border-app-info">Gross Monthly Income </header>
        <div class="table table-inline table-app-info-pg2 table-monthly-income">
          <div>
            <div></div>
            <div><label>Borrower</label></div>
            <div><label>Co-borrower</label></div>
            <div><label>Total</label></div>
          </div>
          <div>
            <div>
              <label>Base Income</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBBaseI"  Update="true" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" />
              <%--<input value="$0.00" type="text" id="aBBaseI" name="aBBaseI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aCBaseI"  Update="true" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" />
              <%--<input value="$0.00" type="text" id="aCBaseI" name="aCBaseI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotBaseI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
              <%--<input value="$0.00" type="text" id="aTotBaseI" name="aTotBaseI" class="input-w100 input-textright" />--%>
            </div>
          </div>
          <div>
            <div>
              <label>Over Time</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBOvertimeI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
              <%--<input value="$0.00" type="text" id="aBOvertimeI" name="aBOvertimeI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aCOvertimeI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
              <%--<input value="$0.00" type="text" id="aCOvertimeI" name="aCOvertimeI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotOvertimeI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
              <%--<input value="$0.00" type="text" id="aTotOvertimeI" name="aTotOvertimeI" class="input-w100 input-textright" />--%>
            </div>
          </div>
          <div>
            <div>
              <label>Bonuses</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBBonusesI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
              <%--<input value="$0.00" type="text" id="aBBonusesI" name="aBBonusesI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aCBonusesI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
              <%--<input value="$0.00" type="text" id="aCBonusesI" name="aCBonusesI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotBonusesI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
              <%--<input value="$0.00" id="aTotBonusesI" name="aTotBonusesI" name="" class="input-w100 input-textright" />--%>
            </div>
          </div>
          <div>
            <div>
              <label>Commissions</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBCommisionI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
              <%--<input value="$0.00" type="text" id="aBCommisionI" name="aBCommisionI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aCCommisionI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
              <%--<input value="$0.00" type="text" id="aCCommisionI" name="aCCommisionI" class="input-w100 input-textright" />--%>
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotCommisionI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
              <%--<input value="$0.00" type="text" id="aTotCommisionI" name="aTotCommisionI" class="input-w100 input-textright" />--%>
            </div>
          </div>
          <div>
            <div>
              <label>Dividends/Interest</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBDividendI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
            </div>
            <div>
              <ml:MoneyTextBox ID="aCDividendI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotDividendI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
          </div>
          <div>
            <div>
              <label>Net Rental Income </label>
            </div>
            <div>
              <ml:MoneyTextBox readonly ID="aBNetRentI1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
            </div>
            <div>
              <ml:MoneyTextBox readonly ID="aCNetRentI1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotNetRentI1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
          </div>
          <div>
            <div>
              <label>Subject Net Cash</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBSpPosCf" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True" />
            </div>
            <div>
              <ml:MoneyTextBox ID="aCSpPosCf" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True" />
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotSpPosCf" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
          </div>
          <div>
            <div>
              <label>Other Income</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBTotOI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
            <div>
              <ml:MoneyTextBox ID="aCTotOI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotOI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
          </div>
          <div>
            <div>
              <label>Total</label>
            </div>
            <div>
              <ml:MoneyTextBox ID="aBTotI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
            <div>
              <ml:MoneyTextBox ID="aCTotI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
            <div>
              <ml:MoneyTextBox ID="aTotI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
            </div>
          </div>
        </div>
    </fieldset>
    <fieldset class="box-section-border-red" id="box2-section5">
        <input name="OtherIncomeJson" type="hidden" id="OtherIncomeJson" runat="server" value="[{&quot;Amount&quot;:0.00,&quot;Desc&quot;:&quot;&quot;,&quot;IsForCoBorrower&quot;:false},{&quot;Amount&quot;:0.00,&quot;Desc&quot;:&quot;&quot;,&quot;IsForCoBorrower&quot;:false},{&quot;Amount&quot;:0.00,&quot;Desc&quot;:&quot;&quot;,&quot;IsForCoBorrower&quot;:false}]" />
        <header class="header header-border header-border-app-info">Describe Other Income </header>
        <p>
            <table id="Table13" class="other-income" >
                <thead>
                    <tr>
                        <td>B/C</td>
                        <td>Description</td>
                        <td>Monthly Income</td>
                    </tr>
                </thead>
                <tbody id="OtherIncomeContent">
                <tr><td>&nbsp;</td></tr>
                </tbody>
                <tfoot><tr><td class="none-bottom"colspan='3'>
                    <p class="last-box2-section5 none-mbt">
                        <a runat="server" id="AddMoreOtherInc" onclick="onAddMoreIncomeClick()"><i class="material-icons large-material-icons">&#xE146;</i></a>
                    </p>
                </td></tr></tfoot>
            </table>
            <span style="display: none" ><span style="display:none" ><ml:ComboBox runat="server" id="OtherIncomeCombo" style="display:none" /></span></span>
        </p>
    </fieldset>

    <fieldset class="box-section-border-red" id="box3-section5">
        <header class="header header-border header-border-app-info">Combined Monthly Housing Expense</header>

        <table class="combined-monthly-housing-expense-app-info">
	        <tr>
                <td>&nbsp;</td>
		        <td colspan="5">
			        Present
		        </td>
		        <td>
			        Proposed
		        </td>
	        <tr>
	        <tr>
                <td colspan="2">&nbsp;</td>
		        <td>
			        Base type
		        </td>
		        <td>
			        Base amount
		        </td>
		        <td>
			        Rate
		        </td>
		        <td>
			        Mon min. base
		        </td>
		        <td>
			        Amount
		        </td>
	        </tr>
	        <tr>
		        <td>
			        Rent
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresRent" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
	        </tr>
	        <tr>
		        <td>
			        1st Mort (P&I)
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPres1stM" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
                <td colspan="4">&nbsp;</td>
		        <td class="w-138">
			        <ml:MoneyTextBox ID="sProFirstMPmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
		        </td>
	        </tr>
	        <tr>
		        <td>
			        Other Fin (P&I)
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresOFin" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
                <td colspan="3">&nbsp;</td>
		        <td class="label-subfinancing">
			        <span>Subfinancing</span>
		        </td>
		        <td class="w-138">
			        <ml:MoneyTextBox ID="sProSecondMPmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
		        </td>
	        </tr>
	        <tr>
		        <td>
			        Haz Ins / Notes
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresHazIns" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
		        <td>
			        <asp:PlaceHolder runat="server" ID="sProHazInsTHolder">
				        <asp:DropDownList class="input-w150" id="sProHazInsT" name="sProHazInsT"  runat="server"  Update="true"></asp:DropDownList>
			        </asp:PlaceHolder>
		        </td>
		        <td>
			        <asp:PlaceHolder runat="server" ID="sProHazInsBaseAmtHolder">
				        <ml:MoneyTextBox ID="sProHazInsBaseAmt" runat="server" preset="money" CssClass="input-w100" value="$0.00" ReadOnly="True"/>
			        </asp:PlaceHolder>
		        </td>
		        <td class="nowrap">
			        <asp:PlaceHolder runat="server" ID="sProHazInsRHolder">
				        <ml:PercentTextBox ID="sProHazInsR" runat="server" preset="percent" CssClass="input-w60" value="0.000%"  Update="true" />
			        </asp:PlaceHolder>
			        <asp:PlaceHolder runat="server" ID="sProHazInsPlusHolder">
				        <span class="plus-combined-monthly-housing-expense">+</span>
			        </asp:PlaceHolder>
		        </td>
		        <td>
			        <asp:PlaceHolder runat="server" ID="sProHazInsMbHolder">
				        <ml:MoneyTextBox ID="sProHazInsMb" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
			        </asp:PlaceHolder>
		        </td>
		        <td class="w-138">
			        <ml:MoneyTextBox ID="sProHazIns" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
		        </td>
	        </tr>
	        <tr>
		        <td>
			        RE Tax / Notes
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresRealETx" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
		        <td>
			        <asp:PlaceHolder runat="server" ID="sProRealETxTHolder">
				        <asp:DropDownList class="input-w150" id="sProRealETxT" name="sProRealETxT"  runat="server"  Update="true"></asp:DropDownList>
			        </asp:PlaceHolder>
		        </td>
		        <td>
			        <asp:PlaceHolder runat="server" ID="sProRealETxBaseAmtHolder">
				        <ml:MoneyTextBox ID="sProRealETxBaseAmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
			        </asp:PlaceHolder>
		        </td>
		        <td class="nowrap">
			        <asp:PlaceHolder runat="server" ID="sProRealETxRHolder">
				        <ml:PercentTextBox ID="sProRealETxR" runat="server" preset="percent" CssClass="input-w60" value="0.000%"  Update="true" />
			        </asp:PlaceHolder>
			        <asp:PlaceHolder runat="server" ID="sProRealETxPlusHolder">
				        <span class="plus-combined-monthly-housing-expense">+</span>
			        </asp:PlaceHolder>
		        </td>
		        <td>
			        <asp:PlaceHolder runat="server" ID="sProRealETxMbHolder">
				        <ml:MoneyTextBox ID="sProRealETxMb" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
			        </asp:PlaceHolder>
		        </td>
		        <td class="w-138">
			        <ml:MoneyTextBox ID="sProRealETx" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
		        </td>
	        </tr>
	        <tr>
		        <td>
			        Mort Ins / Notes
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresMIns" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
                <td colspan="4">&nbsp;</td>
		        <td class="w-138">
			        <ml:MoneyTextBox ID="sProMIns" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
		        </td>
	        </tr>
	        <tr>
		        <td>
			        HOA
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresHoAssocDues" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
                <td colspan="4">&nbsp;</td>
		        <td class="w-138">
			        <ml:MoneyTextBox ID="sProHoAssocDues" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
	        </tr>
	        <tr>
		        <td>
			        <input type="text" class="input-w100 input-textright" id="sProOHExpDesc" runat="server" name="sProOHExpDesc" />
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresOHExp" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
		        </td>
                <td colspan="4">&nbsp;</td>
		        <td class="nowrap w-138">
			        <ml:MoneyTextBox ID="sProOHExp" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
			        <label class="lock-checkbox less-left-margin"><input type="checkbox" id="sProOHExpLckd" name="sProOHExpLckd"  runat="server"  Update="true" /></label>
		        </td>
	        </tr>
	        <tr>
		        <td>
			        Total
		        </td>
		        <td>
			        <ml:MoneyTextBox ID="aPresTotHExp" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
		        </td>
                <td colspan="4">&nbsp;</td>
		        <td class="w-138">
			        <ml:MoneyTextBox ID="sMonthlyPmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
		        </td>
	        </tr>
        </table>
    </fieldset>
</accordion-content></accordion>

<accordion><accordion-header>VI. ASSETS AND LIABILITIES</accordion-header><accordion-content>
    <div class="padding-left-p16">
      <div class="table">
        <div>
          <div><label>Completed</label></div>
          <div><asp:RadioButton runat="server" Checked="true" ID="aAsstLiaCompletedNotJointly_0" GroupName="aAsstLiacompletedNotJointly" value="0" Text="Jointly" /></div>
          <div><asp:RadioButton runat="server" ID="aAsstLiaCompletedNotJointly_1" GroupName="aAsstLiacompletedNotJointly" value="1" Text="Not Jointly" /></div>
        </div>
      </div>
    </div>
    <fieldset class="box-section-border-red" id="box-section6">
          <header class="header header-border header-border-app-info">Assets</header>
          <div class="table f-left">
            <div>
              <div>
                <label>Cash Deposit</label>
                <div class="table">
                  <div>
                    <div>
                      <div class="table">
                         <div>
                           <div>
                             <asp:TextBox runat="server" ID="CashDeposit1Desc" title="Enter Description" class="input-w270"></asp:TextBox>
                           </div>
                           <div>
                             <ml:MoneyTextBox runat="server" ID="CashDeposit1Val" preset="money" title="Enter cash/market value ($)"></ml:MoneyTextBox>
                           </div>
                         </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div>
                      <div class="table">
                         <div>
                           <div>
                             <asp:TextBox runat="server" ID="CashDeposit2Desc" title="Enter Description" class="input-w270"></asp:TextBox>
                           </div>
                           <div>
                             <ml:MoneyTextBox runat="server" ID="CashDeposit2Val" preset="money" title="Enter cash/market value ($)"></ml:MoneyTextBox>
                           </div>
                         </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="f-right">
            <div>
                <button type="button" class="btn btn-default" onclick="loadNonRealEstateAssets(); DialogHandler('a0'); return false;">EDIT ASSETS</button>
                <button type="button" class="btn btn-default" onclick="loadRealEstateRecords(); DialogHandler('a0'); return false;">EDIT REOS</button>
            </div>
          </div>
          <div class="flex-space-between none-margin-bottom clear">
            <div class="flex-alignment flex-alignment-w-pg1-section-2 padding-bottom-p16">
                <div class="col-xs-4"><div>
                  <label>Life Insurance Cash Value</label>
                  <ml:MoneyTextBox runat="server" ID="LifeInsCashValue" preset="money" title="Life insurance net cash value ($)"></ml:MoneyTextBox>
                </div></div>
                <div class="col-xs-4"><div>
                  <label>Life Insurance Face Amount</label>
                  <ml:MoneyTextBox runat="server" ID="LifeInsFaceAmt" preset="money" title="Life insurance face amount ($)"></ml:MoneyTextBox>
                </div></div>
                <div class="col-xs-4"><div>
                  <label>Retirement Funds</label>
                  <ml:MoneyTextBox runat="server" ID="RetirementFunds" preset="money" title="Vested interest in retirement fund ($)"></ml:MoneyTextBox>
                </div></div>
            </div>
            <div class="w-128"><div>
              <label>Business</label>
              <ml:MoneyTextBox runat="server" ID="BusinessVal" preset="money" title="Net worth of business(es) owned ($)"></ml:MoneyTextBox>
            </div></div>
          </div>
          <hr />
          <p class="padding-bottom-p16">
              <label>
                  Non-Real Estate Assets
              </label>
          </p>
          <div class="flex-space-between none-margin-bottom">
            <div class="flex-alignment flex-alignment-w-pg1-section-2">
                <div class="col-xs-4"><div>
                  <label>Liquid</label>
                  <ml:MoneyTextBox ID="aAsstLiqTot" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True" />
                </div></div>
                <div class="col-xs-4"><div>
                  <label>Non-liquid</label>
                  <ml:MoneyTextBox ID="aAsstNonReSolidTot" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True" />
                </div></div>
                <div class="col-xs-4"><div>
                  <label>
                      Real Estate Owned
                  </label>
                  <ml:MoneyTextBox ID="aReTotVal" runat="server" preset="money" CssClass="input-w100 input-textright"
                      value="$0.00" ReadOnly="True" />
                </div></div>
            </div>
            <div class="w-128"><div>
              <label>
                  (a) Total Assets
              </label>
              <ml:MoneyTextBox ID="aAsstValTot" runat="server" preset="money" CssClass="input-w100 input-textright"
                  value="$0.00" ReadOnly="True" />
            </div></div>
          </div>
    </fieldset>
    <fieldset class="box-section-border-red" id="box-section6">
        <header class="header header-border header-border-app-info">Liabilities </header>
        <div class="flex-space-between none-margin-bottom">
          <div class="flex-alignment flex-alignment-w-pg1-section-2">
              <div class="col-xs-4"><div>
                <label>
                    Monthly Payments
                </label>
                <ml:MoneyTextBox ID="aLiaMonTot" runat="server" preset="money" CssClass="input-w100 input-textright"
                    value="$0.00" ReadOnly="True" />
              </div></div>
              <div class="col-xs-4"><div>
                <label>
                    (b) Total Liabilities
                </label>
                <ml:MoneyTextBox ID="aLiaBalTot" runat="server" preset="money" CssClass="input-w100 input-textright"
                    value="$0.00" ReadOnly="True" />
              </div></div>
              <div class="col-xs-3">
                  <div>
                    <label>
                        Net Worth (a - b)
                    </label>
                    <ml:MoneyTextBox ID="aNetWorth" runat="server" preset="money" CssClass="input-w100 input-textright"
                        value="$0.00" ReadOnly="True" />
                  </div>
              </div>
              <div class="col-xs-1">
                  <div class="auto-margin">
                      <button type="button" class="btn btn-default" onclick="f_editLiability()">EDIT LIABILITIES</button>
                  </div>
              </div>
          </div>
        </div>
    </fieldset>
</accordion-content></accordion>
</div>

<div id="AssetDialog" class="modal-app-info modal-assets-edit modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <uc:Assets ID="Assets" runat="server" />
        </div>
    </div>
</div>
