﻿namespace PriceMyLoan.UI.Main
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using EDocs;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.GDMS.LookupMethods;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.GenericFramework;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial class OrderServices : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// Indicates whether the current request to the page is a download. 
        /// </summary>
        private bool isDownloadRequest = false;

        private string passwordPlaceholder = "************";

        private bool ValidLogin = false;
        public override bool IsReadOnly
        {
            get
            {
                if (CurrentUserCanPerform(WorkflowOperations.OrderAppraisal) || CurrentUserCanPerform(WorkflowOperations.OrderVoa) || CurrentUserCanPerform(WorkflowOperations.OrderVoe))
                {
                    return false;
                }

                return base.IsReadOnly;
            }
        }

        protected string PipelineUrl
        {
            get { return this.VirtualRoot + "/Main/Pipeline.aspx"; }
        }

        protected string TabSection
        {
            get { return RequestHelper.GetSafeQueryString("tabSection"); }
        }

        protected string OrderNumber
        {
            get;
            set;
        }

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new WorkflowOperation[] 
            {
                WorkflowOperations.OrderAppraisal,
                WorkflowOperations.OrderVoe,
                WorkflowOperations.OrderVoa
            };
        }

        public override IEnumerable<WorkflowOperation> GetReasonsForOp()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.OrderVoe,
                WorkflowOperations.OrderVoa
            };
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                return E_XUAComaptibleValue.Edge;
            }

            return base.GetForcedCompatibilityMode();
        }

        protected void Page_Init(object sender, System.EventArgs e)
        {
            base.m_loadDefaultStylesheet = false;
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this, includeSimplePopupDependencies: false);
        }

        private string GetGDMSOccupancyType(E_aOccT t)
        {
            switch (t)
            {
                case E_aOccT.Investment:
                    return "Investment";
                case E_aOccT.PrimaryResidence:
                    return "Primary Residence";
                case E_aOccT.SecondaryResidence:
                    return "Second Home";
                default: return string.Empty;
            }
        }

        private bool HandleDownloadOrderRequest()
        {
            string docKey = RequestHelper.GetSafeQueryString("docKey");
            string orderNumber4506T = RequestHelper.GetSafeQueryString("orderNumber4506T");
            isDownloadRequest = !string.IsNullOrEmpty(docKey) || !string.IsNullOrEmpty(orderNumber4506T);

            if (isDownloadRequest)
            {
                if (!string.IsNullOrEmpty(orderNumber4506T))
                {
                    Irs4506TOrderInfo order = Irs4506TOrderInfo.Retrieve(
                        RequestHelper.GetGuid("vendorId"), 
                        LoanID, 
                        RequestHelper.GetSafeQueryString("orderNumber4506T"), 
                        PrincipalFactory.CurrentPrincipal.BrokerId);
                    if (order != null)
                    {
                        Irs4506TOrderInfo.Irs4506TDocument doc = order.UploadedFiles.FirstOrDefault(file => file.FileDBKey == docKey);
                        if(doc != null)
                        {
                            doc.UseFile(file =>
                            {
                                RequestHelper.SendFileToClient(this.Context, file.FullName, "application/pdf", doc.DocumentName);
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            });
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    var orders = LQBAppraisalOrder.GetOrdersForLoan(LoanID);

                    foreach (var order in orders)
                    {
                        var requested = order.UploadedFiles.FirstOrDefault(
                            a => a.FileDBKey.Equals(docKey, StringComparison.OrdinalIgnoreCase));

                        if (requested != default(LQBAppraisalOrder.AppraisalDocument))
                        {
                            requested.UseFile(fi =>
                            {
                                RequestHelper.SendFileToClient(this.Context, fi.FullName, "application/pdf", requested.DocumentName);
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            });
                            return true;
                        }
                    }

                    isDownloadRequest = false;
                }
            }

            return isDownloadRequest;
        }

        private void PopulatePropertyInfo(CPageData dataLoan)
        {
            var propertyInfoDetails = Tools.PopulatePropertyInfoDetails(dataLoan, this.PropertySelector, includeSubjPropInDDL: true);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("PropertyInfoDetails", propertyInfoDetails);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (HandleDownloadOrderRequest())
            {
                return;
            }

            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            RegisterJsScript("/webapp/orderServices.js");
            RegisterJsScript("/webapp/jquery.placeholder.js");
            RegisterJsScript("/webapp/autoNumeric.js");
            RegisterJsScript("/webapp/jquery.mask.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterJsScript("jquery.tablesorter.min.js");
            this.RegisterJsScript("genericframework.js");
            RegisterCSS("/webapp/tabStyle_TPO.css");
            RegisterCSS("jquery-ui-1.11.css");

            RegisterJsScript("/bootstrap.min.js");
            RegisterService("main", "/webapp/OrderServicesService.aspx");

            a4506TPrevZip.SmartZipcode(a4506TPrevCity, a4506TPrevState);
            a4506TThirdPartyZip.SmartZipcode(a4506TThirdPartyCity, a4506TThirdPartyState);
            BillingZip.SmartZipcode(BillingCity, BillingState);

            ClientScript.RegisterHiddenField("IsNewPmlUIEnabled", (PriceMyLoanUser.BrokerDB.IsNewPmlUIEnabled || PriceMyLoanUser.IsNewPmlUIEnabled).ToString());

            OrderNumber = RequestHelper.GetSafeQueryString("orderNumber");

            BrokerUserPermissions buP = new BrokerUserPermissions(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);

            Tools.Bind_CreditCards(CardType);
            
            if (!string.IsNullOrEmpty(OrderNumber))
            {
                ClientScript.RegisterHiddenField("tabSection", "#secAppraisals");
            }
            if (!string.IsNullOrEmpty(TabSection))
            {

                ClientScript.RegisterHiddenField("tabSection", "#" + TabSection);
            }
            else
            {
                if (buP.HasPermission(Permission.AllowOrder4506T))
                {
                    ClientScript.RegisterHiddenField("tabSection", "#sec4506T");
                }
                else
                {
                    ClientScript.RegisterHiddenField("tabSection", "#secAppraisals");
                }
            }

            var vendors = AppraisalVendorFactory.GetAvailableVendorsForBroker(PriceMyLoanUser.BrokerId);
            if (vendors.Count() == 1)
            {
                SelectedAMC.Items.Clear(); //Remove empty option
            }

            var orderContainers = new List<OrderContainer>();

            List<string> AllowOnlyOnePrimaryProduct = new List<string>(); //Javascript object formatting for each AppraisalVendor
            List<string> HideAppraisalNeededDate = new List<string>(); //Javascript object formatting for each AppraisalVendor
            List<string> AppraisalNeededDateRequired = new List<string>(); //Javascript object formatting for each AppraisalVendor
            List<string> HideNotes = new List<string>(); //Javascript object formatting for each AppraisalVendor
            List<string> UsesAccountId = new List<string>();
            foreach (AppraisalVendorConfig vendor in vendors)
            {
                ListItem listItem = new ListItem(vendor.VendorName, vendor.VendorId.ToString());
                listItem.Attributes.Add("UsesGlobalDMS", "" + vendor.UsesGlobalDMS);
                SelectedAMC.Items.Add(listItem);

                HideAppraisalNeededDate.Add(
                    string.Format("'{0}':{1}",
                        vendor.VendorId.ToString(),
                        vendor.AppraisalNeededDateOption == E_AppraisalNeededDateOptions.Hidden ? "true" : "false"));
                AppraisalNeededDateRequired.Add(
                    string.Format("'{0}':{1}",
                        vendor.VendorId.ToString(),
                        vendor.AppraisalNeededDateOption == E_AppraisalNeededDateOptions.Required ? "true" : "false"));
                HideNotes.Add(
                    string.Format("'{0}':{1}",
                        vendor.VendorId.ToString(),
                        vendor.HideNotes ? "true" : "false"));
                AllowOnlyOnePrimaryProduct.Add(
                    string.Format("'{0}':{1}",
                        vendor.VendorId.ToString(),
                        vendor.AllowOnlyOnePrimaryProduct ? "true" : "false"));
                UsesAccountId.Add(
                   string.Format("'{0}':{1}",
                       vendor.VendorId.ToString(),
                       vendor.UsesAccountId ? "true" : "false"));
            }

            string UsesAccountIdScript = "var UsesAccountIdScript = {" + String.Join(", ", UsesAccountId.ToArray()) + "};" + Environment.NewLine +
                "var HideAppraisalNeededDate = {" + String.Join(", ", HideAppraisalNeededDate.ToArray()) + "};" + Environment.NewLine +
                "var AppraisalNeededDateRequired = {" + String.Join(", ", AppraisalNeededDateRequired.ToArray()) + "};" + Environment.NewLine;
            ClientScript.RegisterStartupScript(typeof(OrderServices), "UISettings", UsesAccountIdScript, true);

            // Load the loan data
            Guid loanId = RequestHelper.GetGuid("loanid");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderServices));
            dataLoan.InitLoad();

            this.LoanNavHeader.SetDataFromLoan(dataLoan);

            if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                this.btnView.Visible = false;
                this.btnEdit.Visible = false;
                this.btnRun.Visible = false;

                this.SetUpGenericFrameworkSection(dataLoan.sLNm);
            }
            else
            {
                this.TabGenericFramework.Visible = false;
                this.btnView.Visible = PriceMyLoanUser.BrokerDB.Enable1003EditorInTPOPortal;
                this.btnEdit.Visible = !PriceMyLoanUser.BrokerDB.DisableFeeEditorInTPOPortalCase183419;
            }

            ClientScript.RegisterHiddenField("IsUsing2015GFEUITPO", (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy).ToString());
            ClientScript.RegisterHiddenField("Allow4506T", (!base.IsReadOnly && buP.HasPermission(Permission.AllowOrder4506T)).ToString());

            bool allowAppraisal = buP.HasPermission(Permission.AllowOrderingGlobalDMSAppraisals) || CurrentUserCanPerform(WorkflowOperations.OrderAppraisal);
            ClientScript.RegisterHiddenField("AllowAppraisal", allowAppraisal.ToString());

            GlobalDMSOrderLoader gdmsOrderLoader = new GlobalDMSOrderLoader(loanId, PriceMyLoanUser, loadDocAttachmentTypes: true, shouldQueueNewFiles: false);
            orderContainers = gdmsOrderLoader.Retrieve();
            ClientScript.RegisterHiddenField("IsValidGDMSCred", gdmsOrderLoader.HasValidLogin ? "True" : "False");
            this.ValidLogin = gdmsOrderLoader.HasValidLogin;
            if (this.ValidLogin)
            {
                foreach (var docAttachmentType in gdmsOrderLoader.DocAttachmentTypes.CoalesceWithEmpty())
                {
                    this.DocAttachmentType.Items.Add(new ListItem(docAttachmentType.Item2, docAttachmentType.Item1.ToString()));
                }
            }

            var otherInfo = LQBAppraisalOrder.GetOrdersForLoan(LoanID);
            foreach (LQBAppraisalOrder o in otherInfo)
            {
                var orderDetail = new OrderContainer();
                orderDetail.VendorID = o.VendorId;
                orderDetail.LQBAppraisalOrder = o;
                orderDetail.VendorName = AppraisalVendorConfig.Retrieve(o.VendorId).VendorName;
                orderContainers.Add(orderDetail);
            }

            if (orderContainers.Count > 0)
            {
                Orders.DataSource = orderContainers;
                Orders.DataBind();
            }

            CAppData dataApp;
            if (dataLoan.GetAppNames().Count > 0)
            {
                dataApp = dataLoan.GetAppData(0);

                ClientScript.RegisterHiddenField("OriginalaBNm", dataApp.aBNm);
                ClientScript.RegisterHiddenField("OriginalaCNm", dataApp.aCNm);

                aOccT.Value = dataApp.aOccT_rep;
                ClientScript.RegisterHiddenField("LoadOccupancyType", GetGDMSOccupancyType(dataApp.aOccT));
            }
            else
            {
                ClientScript.RegisterHiddenField("LoadOccupancyType", "");
            }

            ClientScript.RegisterHiddenField("LoadPropertyType", dataLoan.sGseSpT_rep);
            ClientScript.RegisterHiddenField("LoadIntendedUse", dataLoan.sLPurposeT_rep);
            ClientScript.RegisterHiddenField("LoadLoanType", dataLoan.sLT_rep);
            ClientScript.RegisterHiddenField("sLId", loanId.ToString());
            ClientScript.RegisterHiddenField("sFileVersion", dataLoan.sFileVersion_rep);

            sLNmInfo.Value = dataLoan.sLNm;
            sAgencyCaseNum.Value = dataLoan.sAgencyCaseNum;

            sLT.Value = dataLoan.sLT_rep;
            sLPurposeT.Value = dataLoan.sLPurposeT_rep;
            sGseSpT.Value = dataLoan.sGseSpT_rep;
            LoanOfficerName.Value = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject).AgentName;

            this.RegisterJsGlobalVariables("sLId", dataLoan.sLId);

            //Save UI settings for each vendor in javascript objects for determining UI
            string script = "var AllowOnlyOnePrimaryProduct = {" + string.Join(", ", AllowOnlyOnePrimaryProduct.ToArray()) + "};";
            ClientScript.RegisterStartupScript(typeof(OrderServices), "AccountIdSettings", script, true);


            if (OrderNumber == null)
                OrderNumber = string.Empty;
            ClientScript.RegisterHiddenField("OrderNumber", OrderNumber);

            this.PopulatePropertyInfo(dataLoan);
            if (!string.IsNullOrEmpty(OrderNumber))
            {
                LQBAppraisalOrder order = LQBAppraisalOrder.Load(LoanID.ToString(), OrderNumber);
                if (order == null)
                    throw new CBaseException("Order Number not found", "Failed to load order number " + OrderNumber);

                Tools.SetDropDownListValue(this.PropertySelector, order.LinkedReoId ?? Guid.Empty);
                SelectedAMC.SelectedValue = order.VendorId.ToString();

                ContactName.Value = order.AppraisalOrder.ContactInfo.ContactName;
                PhoneNumber.Text = order.AppraisalOrder.ContactInfo.ContactPhone;
                WorkNumber.Text = order.AppraisalOrder.ContactInfo.ContactWork;
                OtherNumber.Text = order.AppraisalOrder.ContactInfo.ContactOther;
                ContactEmail.Value = order.AppraisalOrder.ContactInfo.ContactEmail;
                AdditionalEmail.Text = order.AppraisalOrder.ContactInfo.AdditionalEmail;
                var products = order.AppraisalOrder.OrderInfo.OrderedProductList;
                if (products.Count > 0)
                    ClientScript.RegisterHiddenField("ReportTypeEdit", products[0].ProductName);
                if (products.Count > 1)
                    ClientScript.RegisterHiddenField("ReportTypeEdit2", products[1].ProductName);
                if (products.Count > 2)
                    ClientScript.RegisterHiddenField("ReportTypeEdit3", products[2].ProductName);
                if (products.Count > 3)
                    ClientScript.RegisterHiddenField("ReportTypeEdit4", products[3].ProductName);
                if (products.Count > 4)
                    ClientScript.RegisterHiddenField("ReportTypeEdit5", products[4].ProductName);
                DateTime neededD;
                if (DateTime.TryParseExact(
                    order.AppraisalOrder.OrderInfo.AppraisalNeededDate
                    , "dd-MM-yyyy"
                    , null
                    , System.Globalization.DateTimeStyles.None
                    , out neededD))
                {
                    AppraisalNeededD.Text = neededD.ToShortDateString();
                }
                RushOrder.Checked = order.AppraisalOrder.OrderInfo.RushOrderIndicator == LQBAppraisal.E_YNIndicator.Y;
                BillingMethod.SelectedValue = order.AppraisalOrder.Billing.BillingMethodName;
                BillingName.Text = order.AppraisalOrder.Billing.BillingInfo.BillingName;
                BillingAdd.Text = order.AppraisalOrder.Billing.BillingInfo.BillingStreet;
                BillingCity.Text = order.AppraisalOrder.Billing.BillingInfo.BillingCity;
                BillingState.Value = order.AppraisalOrder.Billing.BillingInfo.BillingState;
                BillingZip.Text = order.AppraisalOrder.Billing.BillingInfo.BillingZIP;
                CardType.SelectedValue = order.AppraisalOrder.Billing.CCInfo.CCType;
                CCNumber.Text = order.AppraisalOrder.Billing.CCInfo.CCNumber;
                Expiration_Month.Text = order.AppraisalOrder.Billing.CCInfo.CCExpMonth;
                Expiration_Year.Text = order.AppraisalOrder.Billing.CCInfo.CCExpYear;
                Expiration_CVV.Text = order.AppraisalOrder.Billing.CCInfo.CCSecurityCode;
                Notes.Text = order.AppraisalOrder.OrderInfo.OrderNotes;

                btnPlaceOrder.InnerText = "Edit Order";
                btnCancel_Appraisal.InnerText = "Cancel Edit";
                GetAppraisalCredentials(order.VendorId);
            }

            // Beginning of T-4506 code
            foreach (Irs4506TVendorConfiguration vendor in Irs4506TVendorConfiguration.ListActiveVendorByBrokerId(PriceMyLoanUser.BrokerId))
            {
                ListItem item = new ListItem(vendor.VendorName, vendor.VendorId.ToString());
                item.Attributes.Add("requiresAccId", vendor.IsUseAccountId.ToString());
                Vendor4506.Items.Add(item);
            }

            for (int i = 0; i < dataLoan.GetAppNames().Count; i++)
            {
                CAppData appData = dataLoan.GetAppData(i);

                var appIdString = appData.aAppId.ToString();
                if (appData.aHasSpouse == false)
                {
                    ApplicationName.Items.Add(new ListItem(appData.aBNm, appIdString));
                }
                else
                {
                    if (appData.aIs4506TFiledTaxesSeparately)
                    {
                        // TODO: make this more closely match what's on the Order 4506T page by giving coborrower option.
                        ApplicationName.Items.Add(new ListItem(appData.aBNm, appIdString));
                    }
                    else
                    {
                        // Borrower & Spouse file jointly
                        ApplicationName.Items.Add(new ListItem(appData.aBNm_aCNm, appIdString));
                    }
                }
            }

            IEnumerable<Irs4506TOrderInfo> orderList = Irs4506TOrderInfo.RetrieveByLoanId(LoanID);

            if (orderList.Count() > 0)
            {
                Orders4506T.DataSource = orderList;
                Orders4506T.DataBind();
            }

            // Load the Document Picker
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            IList<EDocument> docs = repo.GetDocumentsByLoanId(LoanID);

            List<DocType> p = EDocumentDocType.GetStackOrderedDocTypesByBroker(PriceMyLoanUser.BrokerDB).ToList();
            Dictionary<string, int> docTypeIndeces = new Dictionary<string, int>();

            for (int i = 0; i < p.Count; i++)
            {
                docTypeIndeces.Add(p[i].DocTypeId, i);
            }
            m_repeater.DataSource = docs.OrderBy(y => GetIndex(y.DocumentTypeId.ToString(), docTypeIndeces));
            m_repeater.DataBind();

            var docTypeFolders = new List<string>();
            docTypeFolders.Add("All");
            docTypeFolders.AddRange(p.Select(d => d.Folder.FolderNm).Distinct());
            DocTypeFilter.DataSource = docTypeFolders;
            DocTypeFilter.DataBind();

            this.LoadVox(dataLoan);
        }

        // ==== BEGIN VOX ====
        private void LoadVox(CPageData dataLoan)
        {
            this.RegisterJsScript("/jquery.tmpl.js");
            this.RegisterJsScript("/VerificationDashboard.js");
            this.RegisterJsScript("/VOXOrderCommon.js");
            this.RegisterJsGlobalVariables("VOXTimeoutInMilliseconds", LendersOffice.Constants.ConstStage.VOXTimeoutInMilliseconds == 0 ? 0 : LendersOffice.Constants.ConstStage.VOXTimeoutInMilliseconds + 1000);
            this.RegisterJsGlobalVariables("NotificationEmailDefault", this.PriceMyLoanUser.Email);

            var loaderFactory = new VOXLoaderFactory(this.PriceMyLoanUser, this.LoanID);

            var canOrderVoa = this.CurrentUserCanPerform(WorkflowOperations.OrderVoa);
            this.LoadVOAComponents(canOrderVoa, dataLoan, loaderFactory.VoaLoader);

            var canOrderVoe = this.CurrentUserCanPerform(WorkflowOperations.OrderVoe);
            this.LoadVOEComponents(canOrderVoe, dataLoan, loaderFactory.VoeLoader);
        }

        private void LoadVOAComponents(bool canOrderVoa, CPageData loanData, VOALoader loader)
        {
            var orderViewModels = loader.LoadOrderViewModels();

            this.RegisterService("vod", "/webapp/OrderVOAService.aspx");
            this.RegisterJsScript("/VOAOrdering.js");

            // Get VOA lender services for dropdown
            var voaServices = loader.GetLenderServicesAvailableToUser().Where((service) => !service.AllowPaymentByCreditCard).ToList();

            if (voaServices.Count == 0)
            {
                this.VoaTab.Visible = false;
                this.secVoa.Visible = false;
                this.RegisterJsGlobalVariables("VoaTabEnabled", false);
                return;
            }

            this.RegisterJsGlobalVariables("VoaTabEnabled", true);
            this.RegisterJsGlobalVariables("CanOrderVoa", canOrderVoa);
            this.RegisterJsGlobalVariables("OrderVoaDenialReason", string.Join(",", this.GetReasonsUserCannotPerformOperation(WorkflowOperations.OrderVoa)));
            this.RegisterJsGlobalVariables("MaxOptionValue", VOAVendorService.MaxOption);
            this.RegisterJsGlobalVariables("MinOptionValue", VOAVendorService.MinOption);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("VOAOrders", orderViewModels.Cast<VOAOrderViewModel>());

            if (canOrderVoa)
            {
                this.PopulateLenderServiceDDL(this.VOA_LenderServices, voaServices);

                // Load Assets
                var assets = loader.GetAvailableAssets();

                this.AssetsRepeater.DataSource = assets;
                this.AssetsRepeater.DataBind();

                var borrowers = loader.GetAvailableBorrowers();
                foreach (var borrower in borrowers)
                {
                    this.VOA_BorrowerList.Items.Add(new ListItem(borrower.Name, borrower.Key));
                }

                if (this.VOA_BorrowerList.Items.Count > 1)
                {
                    this.VOA_BorrowerList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
                }
            }
        }

        private void LoadVOEComponents(bool canOrderVoe, CPageData loanData, VOELoader loader)
        {
            var orderViewModels = loader.LoadOrderViewModels();

            this.RegisterService("voe", "/webapp/OrderVOEService.aspx");
            this.RegisterJsScript("/VOEOrdering.js");

            var voeServices = loader.GetLenderServicesAvailableToUser().Where((service) => !service.AllowPaymentByCreditCard).ToList();

            if (voeServices.Count == 0)
            {
                this.VoeTab.Visible = false;
                this.secVoe.Visible = false;
                this.RegisterJsGlobalVariables("VoeTabEnabled", false);
                return;
            }

            List<string> states = new List<string>(StateInfo.StatesAndTerritories.Keys);
            states.Insert(0, string.Empty);
            this.RegisterJsObjectWithJsonNetSerializer("States", states);
            this.RegisterJsGlobalVariables("VoeTabEnabled", true);
            this.RegisterJsGlobalVariables("CanOrderVoe", canOrderVoe);
            this.RegisterJsGlobalVariables("OrderVoeDenialReason", string.Join(",", this.GetReasonsUserCannotPerformOperation(WorkflowOperations.OrderVoa)));
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("VOEOrders", orderViewModels.Cast<VOEOrderViewModel>());

            if (canOrderVoe)
            {
                this.PopulateLenderServiceDDL(this.VOE_LenderServices, voeServices);
                var employmentRecords = loader.GetEmploymentRecords();
                this.EmploymentRepeater.DataSource = employmentRecords;
                this.EmploymentRepeater.DataBind();

                var borrowers = loader.GetBorrowers().ToList();
                if (borrowers.Count > 1)
                {
                    borrowers.Insert(0, new KeyValuePair<string, string>(string.Empty, string.Empty));
                }

                this.VOE_BorrowerList.DataTextField = nameof(KeyValuePair<string, string>.Value);
                this.VOE_BorrowerList.DataValueField = nameof(KeyValuePair<string, string>.Key);
                this.VOE_BorrowerList.DataSource = borrowers;
                this.VOE_BorrowerList.DataBind();
            }
        }

        private void PopulateLenderServiceDDL(DropDownList ddl, IEnumerable<VOXLenderService> services)
        {
            foreach (var service in services)
            {
                ddl.Items.Add(new ListItem(service.DisplayName, service.LenderServiceId.ToString()));
            }

            if (ddl.Items.Count > 1)
            {
                ddl.Items.Insert(0, new ListItem(string.Empty, string.Empty));
            }
        }


        // ==== END VOX ====
        private void GetAppraisalCredentials(Guid vendorId)
        {

            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);

            if (vendor.UsesGlobalDMS)
            {
                GDMSCredentials credentials;
                string errorMessage;
                credentials = vendor.GetGDMSCredentials(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId, out errorMessage);
                if (credentials == null)
                {
                    return;
                }

                login.Value = credentials.UserName;
                accID.Value = credentials.CompanyId.ToString();

                if (string.IsNullOrEmpty(credentials.Password))
                {
                    password.Value = "";
                }
                else
                {
                    password.Attributes.Add("PlaceholderVal", passwordPlaceholder);
                }
            }
            else
            {
                AppraisalVendorCredentials credentials = new AppraisalVendorCredentials(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId, vendorId);

                login.Value = credentials.UserName;
                accID.Value = credentials.AccountId;

                if (string.IsNullOrEmpty(credentials.Password))
                {
                    password.Value = "";
                }
                else
                {
                    password.Value = passwordPlaceholder;
                    password.Attributes.Add("PlaceholderVal", passwordPlaceholder);
                }
            }

            password.Attributes.Add("DummyPass", "true");

        }

        private void SetUpGenericFrameworkSection(string loanName)
        {
            Predicate<GenericFrameworkVendor> shouldVendorDisplayInList = vendor =>
                vendor.LaunchLinkConfig.IsDisplayLinkForLoanAndUser(this.LoanID, LinkLocation.TpoLoanNavigationOrderServices) ||
                vendor.LaunchLinkConfig.IsDisplayLinkForLoanAndUser(this.LoanID, LinkLocation.TPOPipeline);

            var genericFrameworkVendors = from vendor in GenericFrameworkVendor.LoadVendors(this.PriceMyLoanUser.BrokerId)
                                          where shouldVendorDisplayInList(vendor)
                                          select vendor;

            if (!genericFrameworkVendors.Any())
            {
                this.TabGenericFramework.Visible = false;
                this.secGenericFramework.Visible = false;
            }
            else
            {
                this.GenericFrameworkLinkRepeater.DataSource = genericFrameworkVendors;
                this.GenericFrameworkLinkRepeater.ItemDataBound += this.GenericFrameworkLinkRepeater_ItemDataBound;
                this.GenericFrameworkLinkRepeater.DataBind();
            }
        }

        protected void GenericFrameworkLinkRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HtmlAnchor link = (HtmlAnchor)e.Item.FindControl("GenericFrameworkVendorLink");
            var vendor = (GenericFrameworkVendor)e.Item.DataItem;
            link.Attributes.Add("data-providerid", vendor.ProviderID);

            LinkLocation linkLocation;
            string linkText;

            // Use the "Order Services" location if the link was configured for that page. 
            // If not, fall back to the pipeline configuration settings.
            if (vendor.LaunchLinkConfig.IsDisplayLinkForUser(LinkLocation.TpoLoanNavigationOrderServices))
            {
                linkLocation = LinkLocation.TpoLoanNavigationOrderServices;
                linkText = vendor.LaunchLinkConfig.LinkDisplayName(LinkLocation.TpoLoanNavigationOrderServices);
            }
            else
            {
                linkLocation = LinkLocation.TPOPipeline;
                linkText = vendor.LaunchLinkConfig.LinkDisplayName(LinkLocation.TPOPipeline);
            }

            link.Attributes.Add("onclick", "f_genericFramework(" + AspxTools.JsString(this.LoanID) + "," + AspxTools.JsString(vendor.ProviderID) + "," + AspxTools.JsString(linkLocation) + ")");
            link.InnerText = linkText;
        }

        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            if (this.isDownloadRequest == false)
            {
                base.RaisePostBackEvent(sourceControl, eventArgument);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.isDownloadRequest == false)
            {
                base.Render(writer);
            }
        }

        protected void Orders_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            HtmlAnchor viewLink = args.Item.FindControl("ViewLink") as HtmlAnchor;
            HtmlAnchor editLink = args.Item.FindControl("EditLink") as HtmlAnchor;
            HtmlAnchor attachDocsLink = args.Item.FindControl("AttachDocsLink") as HtmlAnchor;
            HtmlAnchor sendEmailLink = args.Item.FindControl("SendEmailLink") as HtmlAnchor;
            EncodedLiteral vendor = args.Item.FindControl("Vendor") as EncodedLiteral;
            EncodedLiteral orderNum = args.Item.FindControl("OrderNum") as EncodedLiteral;
            EncodedLiteral orderedD = args.Item.FindControl("OrderedD") as EncodedLiteral;
            EncodedLiteral status = args.Item.FindControl("Status") as EncodedLiteral;
            EncodedLiteral statusD = args.Item.FindControl("StatusD") as EncodedLiteral;
            Repeater docsRepeater = args.Item.FindControl("UploadedDocuments") as Repeater;
            HtmlTableRow row = args.Item.FindControl("Row") as HtmlTableRow;

            HtmlAnchor refreshLink = args.Item.FindControl("RefreshLink") as HtmlAnchor;

            OrderContainer orderInfo = args.Item.DataItem as OrderContainer;
            if (orderInfo == null) return;

            ClientScript.RegisterHiddenField("EdocUrl", AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/webapp/ViewEdocPdf.aspx"))));

            row.Attributes.Add("class", args.Item.ItemType == ListItemType.Item ? "odd" : "even");
            editLink.Visible = false;
            attachDocsLink.Visible = false;
            sendEmailLink.Visible = false;

            if (orderInfo.ClientOrder != null) //GDMS Appraisal
            {

                if (orderInfo.ClientOrder.StatusName == "Error Accessing Order")
                {
                    viewLink.Visible = false;
                    HtmlAnchor actionLink = args.Item.FindControl("ActionLink") as HtmlAnchor;
                    if (actionLink != null)
                        actionLink.Disabled = true;

                    refreshLink.Visible = true;
                    refreshLink.Attributes.Add("vendorId", orderInfo.VendorID.ToString());
                    viewLink.Visible = false;
                    editLink.Visible = false;
                    attachDocsLink.Visible = false;
                    sendEmailLink.Visible = false;

                    vendor.Text = orderInfo.VendorName;
                    orderNum.Text = orderInfo.ClientOrder.FileNumber.ToString();

                    docsRepeater.DataSource = orderInfo.UploadedFiles;
                    docsRepeater.DataBind();
                }
                else
                {
                    var pdf = new LendersOffice.Pdf.CAppraisalOrderPDF();
                    string link =
                        $"{VirtualRoot}/pdf/{AspxTools.HtmlString(pdf.Name)}.aspx?loanid={AspxTools.HtmlString(LoanID)}&filenumber={AspxTools.HtmlString(orderInfo.ClientOrder.FileNumber)}&vendorID={AspxTools.HtmlString(orderInfo.VendorID)}";
                    viewLink.Attributes.Add("onclick", $"LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload('{link}');");

                    string url;

                    link = string.Format(
                        "javascript:attachDocsToAppraisal('{0}', '{1}', 'True');",
                        AspxTools.JsNumeric(orderInfo.ClientOrder.FileNumber),
                        AspxTools.JsStringUnquoted(orderInfo.VendorID.ToString()));
                    attachDocsLink.HRef = link;
                    attachDocsLink.Visible = true;

                    url = string.Format(
                        "'{3}/webapp/OrderAppraisalEmailLog.aspx?loanid={0}&vendorID={1}&orderNumber={2}'",
                        AspxTools.JsStringUnquoted(LoanID.ToString()),
                        AspxTools.JsStringUnquoted(orderInfo.VendorID.ToString()),
                        AspxTools.JsStringUnquoted(orderInfo.ClientOrder.FileNumber.ToString()),
                        VirtualRoot);
                    link = string.Format(
                        "javascript:launch({0},'SendEmail');",
                        url);
                    sendEmailLink.HRef = link;
                    sendEmailLink.Visible = true;
                }
                vendor.Text = orderInfo.VendorName;
                orderNum.Text = orderInfo.ClientOrder.FileNumber.ToString();
                orderedD.Text = orderInfo.ClientOrder.CreationDate_String.Split(' ')[0]; // Get rid of the time
                status.Text = orderInfo.ClientOrder.StatusName;
                statusD.Text = orderInfo.ClientOrder.AppraiserLastModifiedDate_String.Split(' ')[0];

                docsRepeater.DataSource = orderInfo.UploadedFiles;
                docsRepeater.DataBind();



            }
            else if (orderInfo.LQBAppraisalOrder != null) //Framework Appraisal
            {
                var order = orderInfo.LQBAppraisalOrder;
                vendor.Text = orderInfo.VendorName;
                orderNum.Text = order.OrderNumber;
                orderedD.Text = order.OrderedDate_rep;
                status.Text = string.IsNullOrEmpty(order.Status) ? "Unassigned" : order.Status;
                statusD.Text = order.StatusDate_rep;

                string link;
                if (string.IsNullOrEmpty(order.Status)) //only allow edit if no status updates yet
                {
                    link = string.Format("{0}/webapp/OrderServices.aspx?loanid={1}&orderNumber={2}",
                        VirtualRoot,
                        AspxTools.HtmlString(LoanID),
                        AspxTools.HtmlString(order.OrderNumber));
                    editLink.HRef = link;
                    editLink.Visible = true;
                }

                var pdf = new LendersOffice.Pdf.CLQBAppraisalOrderPDF();
                link = string.Format(
                    "{0}/pdf/{1}.aspx?loanid={2}&ordernumber={3}",
                    VirtualRoot,
                    AspxTools.HtmlString(pdf.Name),
                    AspxTools.HtmlString(LoanID),
                    AspxTools.HtmlString(order.OrderNumber));
                viewLink.Attributes.Add("onclick", $"LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload('{link}');");

                link = string.Format(
                            "javascript:attachDocsToAppraisal('{0}', '{1}', 'False');",
                            AspxTools.JsStringUnquoted(order.OrderNumber),
                            AspxTools.JsStringUnquoted(order.VendorId.ToString()));
                attachDocsLink.HRef = link;
                attachDocsLink.Visible = true;

                // 10/18/13 TimothyJ:
                //add EDocs that have been sent to the appraiser to the uploaded documents field.  This will match the way it is with GDMS
                var frameworkDocs = new List<FrameworkDocumentItem>();
                foreach (var appraisalDoc in order.UploadedFiles)
                {
                    frameworkDocs.Add(new FrameworkDocumentItem(appraisalDoc));
                }
                if (order.EmbeddedDocumentIds != null)
                {
                    foreach (Guid docId in order.EmbeddedDocumentIds)
                    {
                        frameworkDocs.Add(new FrameworkDocumentItem(docId));
                    }
                }

                docsRepeater.DataSource = frameworkDocs;
                docsRepeater.DataBind();
            }

        }

        public int GetIndex(string id, Dictionary<string, int> indeces)
        {
            int index;
            if (indeces.TryGetValue(id, out index))
            {
                return index;
            }

            return int.MaxValue;
        }

        protected void Orders4506T_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            EncodedLiteral application = args.Item.FindControl("Application") as EncodedLiteral;
            EncodedLiteral vendor = args.Item.FindControl("Vendor") as EncodedLiteral;
            EncodedLiteral orderNum = args.Item.FindControl("OrderNum") as EncodedLiteral;
            EncodedLiteral orderedD = args.Item.FindControl("OrderedD") as EncodedLiteral;
            EncodedLiteral status = args.Item.FindControl("Status") as EncodedLiteral;
            EncodedLiteral statusD = args.Item.FindControl("StatusD") as EncodedLiteral;
            Repeater docsRepeater = args.Item.FindControl("UploadedDocuments") as Repeater;
            HtmlTableRow row = args.Item.FindControl("Row") as HtmlTableRow;
            Button checkStatusButton = args.Item.FindControl("CheckStatus") as Button;
            HtmlAnchor statusDetailsLink = args.Item.FindControl("StatusDetailsLink") as HtmlAnchor;

            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                row.Attributes.Add("class", args.Item.ItemType == ListItemType.Item ? "odd" : "even");

                Irs4506TOrderInfo orderInfo = args.Item.DataItem as Irs4506TOrderInfo;
                if (orderInfo == null) return;

                Irs4506TVendorConfiguration vendorInfo = Irs4506TVendorConfiguration.RetrieveById(orderInfo.VendorId);
                application.Text = orderInfo.ApplicationName;
                vendor.Text = vendorInfo.VendorName;
                orderNum.Text = orderInfo.OrderNumber;
                orderedD.Text = orderInfo.OrderDate.ToShortDateString().Split(' ')[0]; // Get rid of the time
                statusD.Text = orderInfo.StatusDate.ToShortDateString().Split(' ')[0];
                status.Text = orderInfo.StatusT == Irs4506TOrderStatus.Unknown ? orderInfo.StatusDescription : orderInfo.StatusT.ToString("G");

                if (!string.IsNullOrWhiteSpace(orderInfo.StatusDescription) && !orderInfo.StatusDescription.Equals(orderInfo.StatusT.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    statusDetailsLink.Visible = true;
                    statusDetailsLink.Title = orderInfo.StatusDescription;
                }

                checkStatusButton.Visible =
                    Irs4506TVendorConfiguration.RetrieveById(orderInfo.VendorId).CommunicationModel == Irs4506TCommunicationModel.EquifaxVerificationServices
                    && (orderInfo.StatusT == Irs4506TOrderStatus.Pending
                        || (orderInfo.StatusT == Irs4506TOrderStatus.Completed && !orderInfo.UploadedFiles.Any()));
                checkStatusButton.OnClientClick = $"Check4506TStatus(this, {AspxTools.JsString(orderInfo.OrderNumber)}, {AspxTools.JsString(orderInfo.VendorId)}); return false;";
                if (orderInfo.StatusT == Irs4506TOrderStatus.Completed)
                {
                    checkStatusButton.Text = "Retrieve Document";
                }

                docsRepeater.DataSource = orderInfo.UploadedFiles.Select(doc => new Tuple<Irs4506TOrderInfo, Irs4506TOrderInfo.Irs4506TDocument>(orderInfo, doc));
                docsRepeater.DataBind();
            }
        }

        protected void UploadedDocuments_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            HtmlAnchor viewLink = args.Item.FindControl("UploadedDocLink") as HtmlAnchor;

            if (args.Item.DataItem is Tuple<Irs4506TOrderInfo, Irs4506TOrderInfo.Irs4506TDocument>)
            {
                Tuple<Irs4506TOrderInfo, Irs4506TOrderInfo.Irs4506TDocument> tuple = args.Item.DataItem as Tuple<Irs4506TOrderInfo, Irs4506TOrderInfo.Irs4506TDocument>;
                viewLink.HRef = "OrderServices.aspx?loanid=" + AspxTools.HtmlString(LoanID) + "&orderNumber4506T=" + AspxTools.HtmlString(tuple.Item1.OrderNumber) + "&vendorId=" + AspxTools.HtmlString(tuple.Item1.VendorId.ToString()) + "&docKey=" + AspxTools.HtmlString(tuple.Item2.FileDBKey);
                viewLink.InnerText = tuple.Item2.DocumentName;
                return;
            }

            OrderFileObject dataItem = args.Item.DataItem as OrderFileObject;

            if (dataItem == null || !dataItem.viewableByClient)
            {
                FrameworkDocumentItem generalDoc = args.Item.DataItem as FrameworkDocumentItem;

                if (generalDoc != null)
                {
                    if (generalDoc.AppraisalDoc != null)
                    {
                        viewLink.InnerText = generalDoc.AppraisalDoc.DocumentName;
                        viewLink.HRef = "OrderServices.aspx?loanid=" + AspxTools.HtmlString(LoanID) + "&docKey=" + AspxTools.HtmlString(generalDoc.AppraisalDoc.FileDBKey);
                    }
                    else if (generalDoc.EDocID != Guid.Empty)
                    {
                        EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                        EDocument edoc;
                        try
                        {
                            edoc = repo.GetDocumentById(generalDoc.EDocID);
                            viewLink.InnerText = edoc.FolderAndDocTypeName + ".pdf";
                            viewLink.HRef = string.Format("javascript:viewEdoc('{0}');", AspxTools.JsStringUnquoted(generalDoc.EDocID.ToString()));
                        }
                        catch (NotFoundException)
                        {
                            edoc = repo.GetDeletedDocumentById(generalDoc.EDocID);
                            viewLink.InnerText = "[Deleted] " + edoc.FolderAndDocTypeName + ".pdf";
                            viewLink.Title = "To restore, go to EDocs\\Document List and click \"Restore deleted docs...\"";
                        }
                    }
                }

            }
            else
            {
                if (dataItem.fileName.EndsWith(".xml"))
                {
                    viewLink.Visible = false;
                }
                viewLink.HRef = AspxTools.HtmlString(dataItem.url);
                viewLink.InnerText = dataItem.fileName;
                viewLink.Target = "_blank";

            }

            if (viewLink.Visible)
            {
                args.Item.Controls.Add(new PassthroughLiteral() { Text = "<br />" });
            }
        }

        private class FrameworkDocumentItem
        {
            private LQBAppraisalOrder.AppraisalDocument appraisalDoc;
            private Guid edocID;

            public LQBAppraisalOrder.AppraisalDocument AppraisalDoc
            {
                get { return appraisalDoc; }
                set
                {
                    appraisalDoc = value;
                    if (appraisalDoc != null)
                        edocID = Guid.Empty;
                }
            }
            public Guid EDocID
            {
                get { return edocID; }
                set
                {
                    edocID = value;
                    if (edocID != Guid.Empty)
                        appraisalDoc = null;
                }
            }

            public FrameworkDocumentItem(LQBAppraisalOrder.AppraisalDocument appraisalDocument)
            {
                appraisalDoc = appraisalDocument;
            }
            public FrameworkDocumentItem(Guid eDocIdAttachedToOrder)
            {
                edocID = eDocIdAttachedToOrder;
            }
        }
    }
}
