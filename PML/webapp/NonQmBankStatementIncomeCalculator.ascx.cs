﻿namespace PriceMyLoan.webapp
{
    using System;
    using global::DataAccess;
    using PriceMyLoan.UI;

    /// <summary>
    /// Non-QM Bank Statement Income Calculator user control
    /// </summary>
    public partial class NonQmBankStatementIncomeCalculator : BaseUserControl
    {
        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event args.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Tools.Bind_YesOrNo(sIsSelfEmployed);
        }
    }
}