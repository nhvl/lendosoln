﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Disclosures.aspx.cs" Inherits="PriceMyLoan.webapp.Disclosures" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Disclosures</title>
</head>
<body>
<form runat="server">
    <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />

    <div id="DisclosuresDiv" ng-bootstrap="Disclosures" class="content-detail" name="content-detail">
        <div class="warp-section disclosures-page" ng-view></div>
    </div>
    <div id="DisclosuresPopup">
        <div class="ae-data body">
            <div>Name:  <span id="AeName"></span></div>
            <div>Email: &nbsp;<span id="AeEmail"></span></div>
            <div>Phone: <span id="AePhone"></span></div>
        </div>
    </div>

    <div>
        <div id="DisclosuresLoading" style="display: none" class="text-center">
            <div class="padding-bottom-1em" id="LoadingText"></div>
            <div id="LoadingIcon"/>
        </div>
    </div>

    <div id="DisclosureRequestPopup">
        <div>Please add a message to the lender: <span class="text-danger">* </span></div>
        <div><textarea id="DisclosureRequestNotes" class="disclosure-request"></textarea></div>
    </div>
</form>
</body>
</html>
