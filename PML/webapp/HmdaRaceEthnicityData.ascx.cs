﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Web.UI.HtmlControls;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.UI.DataContainers;

    public partial class HmdaRaceEthnicityData : PriceMyLoan.UI.BaseUserControl
    {
        /// <summary>
        /// Gets or sets the race ethnicity data that will populate this control.
        /// </summary>
        /// <value>The race ethnicity data that will populate this control.</value>
        public HmdaRaceEthnicityDataContainer RaceEthnicityData { get; set; } = null;

        /// <summary>
        /// Gets or sets a value indicating whether this control should be loaded.
        /// </summary>
        /// <value>Whether this control should be loaded.</value>
        public bool ShouldLoad { get; set; } = false;

        /// <summary>
        /// Sets the HMDA data.
        /// </summary>
        /// <param name="dataApp">The app to set to.</param>
        /// <param name="serviceItem">The background service item.</param>
        /// <param name="controlPrefix">The prefix that the control will have.</param>
        public static void BindHmdaRaceEthnicityData(CAppData dataApp, AbstractBackgroundServiceItem serviceItem, string controlPrefix)
        {
            dataApp.aBIsMexican = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsMexican"));
            dataApp.aCIsMexican = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsMexican"));
            dataApp.aBIsPuertoRican = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsPuertoRican"));
            dataApp.aCIsPuertoRican = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsPuertoRican"));
            dataApp.aBIsCuban = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsCuban"));
            dataApp.aCIsCuban = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsCuban"));
            dataApp.aBIsOtherHispanicOrLatino = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsOtherHispanicOrLatino"));
            dataApp.aCIsOtherHispanicOrLatino = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsOtherHispanicOrLatino"));
            dataApp.aBDoesNotWishToProvideEthnicity = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBDoesNotWishToProvideEthnicity"));
            dataApp.aCDoesNotWishToProvideEthnicity = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCDoesNotWishToProvideEthnicity"));
            dataApp.aBIsAmericanIndian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsAmericanIndian"));
            dataApp.aCIsAmericanIndian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsAmericanIndian"));
            dataApp.aBIsAsian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsAsian"));
            dataApp.aCIsAsian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsAsian"));
            dataApp.aBIsAsianIndian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsAsianIndian"));
            dataApp.aCIsAsianIndian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsAsianIndian"));
            dataApp.aBIsChinese = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsChinese"));
            dataApp.aCIsChinese = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsChinese"));
            dataApp.aBIsFilipino = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsFilipino"));
            dataApp.aCIsFilipino = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsFilipino"));
            dataApp.aBIsJapanese = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsJapanese"));
            dataApp.aCIsJapanese = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsJapanese"));
            dataApp.aBIsKorean = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsKorean"));
            dataApp.aCIsKorean = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsKorean"));
            dataApp.aBIsVietnamese = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsVietnamese"));
            dataApp.aCIsVietnamese = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsVietnamese"));
            dataApp.aBIsOtherAsian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsOtherAsian"));
            dataApp.aCIsOtherAsian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsOtherAsian"));
            dataApp.aBIsWhite = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsWhite"));
            dataApp.aCIsWhite = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsWhite"));
            dataApp.aBDoesNotWishToProvideRace = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBDoesNotWishToProvideRace"));
            dataApp.aCDoesNotWishToProvideRace = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCDoesNotWishToProvideRace"));
            dataApp.aBIsBlack = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsBlack"));
            dataApp.aCIsBlack = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsBlack"));
            dataApp.aBIsPacificIslander = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsPacificIslander"));
            dataApp.aCIsPacificIslander = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsPacificIslander"));
            dataApp.aBIsNativeHawaiian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsNativeHawaiian"));
            dataApp.aCIsNativeHawaiian = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsNativeHawaiian"));
            dataApp.aBIsGuamanianOrChamorro = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsGuamanianOrChamorro"));
            dataApp.aCIsGuamanianOrChamorro = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsGuamanianOrChamorro"));
            dataApp.aBIsSamoan = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsSamoan"));
            dataApp.aCIsSamoan = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsSamoan"));
            dataApp.aBIsOtherPacificIslander = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBIsOtherPacificIslander"));
            dataApp.aCIsOtherPacificIslander = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCIsOtherPacificIslander"));

            dataApp.aBOtherHispanicOrLatinoDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aBOtherHispanicOrLatinoDescription"));
            dataApp.aCOtherHispanicOrLatinoDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aCOtherHispanicOrLatinoDescription"));
            dataApp.aBOtherAmericanIndianDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aBOtherAmericanIndianDescription"));
            dataApp.aCOtherAmericanIndianDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aCOtherAmericanIndianDescription"));
            dataApp.aBOtherAsianDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aBOtherAsianDescription"));
            dataApp.aCOtherAsianDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aCOtherAsianDescription"));
            dataApp.aBOtherPacificIslanderDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aBOtherPacificIslanderDescription"));
            dataApp.aCOtherPacificIslanderDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "aCOtherPacificIslanderDescription"));

            dataApp.aBEthnicityCollectedByObservationOrSurname = serviceItem.GetEnum<E_TriState>("aBEthnicityCollectedByObservationOrSurname");
            dataApp.aCEthnicityCollectedByObservationOrSurname = serviceItem.GetEnum<E_TriState>("aCEthnicityCollectedByObservationOrSurname");
            dataApp.aBSexCollectedByObservationOrSurname = serviceItem.GetEnum<E_TriState>("aBSexCollectedByObservationOrSurname");
            dataApp.aCSexCollectedByObservationOrSurname = serviceItem.GetEnum<E_TriState>("aCSexCollectedByObservationOrSurname");
            dataApp.aBRaceCollectedByObservationOrSurname = serviceItem.GetEnum<E_TriState>("aBRaceCollectedByObservationOrSurname");
            dataApp.aCRaceCollectedByObservationOrSurname = serviceItem.GetEnum<E_TriState>("aCRaceCollectedByObservationOrSurname");
            dataApp.aBInterviewMethodT = serviceItem.GetEnum<E_aIntrvwrMethodT>("aBInterviewMethodT");
            dataApp.aCInterviewMethodT = serviceItem.GetEnum<E_aIntrvwrMethodT>("aCInterviewMethodT");
            dataApp.aBHispanicT = serviceItem.GetEnum<E_aHispanicT>("aBHispanicT");
            dataApp.aCHispanicT = serviceItem.GetEnum<E_aHispanicT>("aCHispanicT");
            dataApp.aBGender = serviceItem.GetEnum<E_GenderT>("aBGender");
            dataApp.aCGender = serviceItem.GetEnum<E_GenderT>("aCGender");

            dataApp.aBHispanicTFallback = serviceItem.GetEnum<E_aHispanicT>(Tools.ConstructControlKey(controlPrefix, "aBHispanicTFallback"));
            dataApp.aCHispanicTFallback = serviceItem.GetEnum<E_aHispanicT>(Tools.ConstructControlKey(controlPrefix, "aCHispanicTFallback"));
            dataApp.aBGenderFallback = serviceItem.GetEnum<E_GenderT>(Tools.ConstructControlKey(controlPrefix, "aBGenderFallback"));
            dataApp.aCGenderFallback = serviceItem.GetEnum<E_GenderT>(Tools.ConstructControlKey(controlPrefix, "aCGenderFallback"));

            dataApp.aBNoFurnish = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBNoFurnish"));
            dataApp.aBNoFurnishLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aBNoFurnishLckd"));
            dataApp.aCNoFurnish = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCNoFurnish"));
            dataApp.aCNoFurnishLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "aCNoFurnishLckd"));
        }

        /// <summary>
        /// Loads the HMDA data.
        /// </summary>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The background service item.</param>
        /// <param name="controlPrefix">The prefix that the control will have.</param>
        public static void LoadHmdaRaceEthnicityData(CAppData dataApp, AbstractBackgroundServiceItem serviceItem, string controlPrefix)
        {
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsMexican"), dataApp.aBIsMexican);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsMexican"), dataApp.aCIsMexican);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsPuertoRican"), dataApp.aBIsPuertoRican);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsPuertoRican"), dataApp.aCIsPuertoRican);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsCuban"), dataApp.aBIsCuban);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsCuban"), dataApp.aCIsCuban);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsOtherHispanicOrLatino"), dataApp.aBIsOtherHispanicOrLatino);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsOtherHispanicOrLatino"), dataApp.aCIsOtherHispanicOrLatino);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBDoesNotWishToProvideEthnicity"), dataApp.aBDoesNotWishToProvideEthnicity);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCDoesNotWishToProvideEthnicity"), dataApp.aCDoesNotWishToProvideEthnicity);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsAmericanIndian"), dataApp.aBIsAmericanIndian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsAmericanIndian"), dataApp.aCIsAmericanIndian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsAsian"), dataApp.aBIsAsian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsAsian"), dataApp.aCIsAsian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsAsianIndian"), dataApp.aBIsAsianIndian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsAsianIndian"), dataApp.aCIsAsianIndian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsChinese"), dataApp.aBIsChinese);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsChinese"), dataApp.aCIsChinese);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsFilipino"), dataApp.aBIsFilipino);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsFilipino"), dataApp.aCIsFilipino);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsJapanese"), dataApp.aBIsJapanese);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsJapanese"), dataApp.aCIsJapanese);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsKorean"), dataApp.aBIsKorean);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsKorean"), dataApp.aCIsKorean);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsVietnamese"), dataApp.aBIsVietnamese);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsVietnamese"), dataApp.aCIsVietnamese);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsOtherAsian"), dataApp.aBIsOtherAsian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsOtherAsian"), dataApp.aCIsOtherAsian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsWhite"), dataApp.aBIsWhite);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsWhite"), dataApp.aCIsWhite);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBDoesNotWishToProvideRace"), dataApp.aBDoesNotWishToProvideRace);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCDoesNotWishToProvideRace"), dataApp.aCDoesNotWishToProvideRace);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsBlack"), dataApp.aBIsBlack);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsBlack"), dataApp.aCIsBlack);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsPacificIslander"), dataApp.aBIsPacificIslander);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsPacificIslander"), dataApp.aCIsPacificIslander);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsNativeHawaiian"), dataApp.aBIsNativeHawaiian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsNativeHawaiian"), dataApp.aCIsNativeHawaiian);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsGuamanianOrChamorro"), dataApp.aBIsGuamanianOrChamorro);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsGuamanianOrChamorro"), dataApp.aCIsGuamanianOrChamorro);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsSamoan"), dataApp.aBIsSamoan);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsSamoan"), dataApp.aCIsSamoan);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBIsOtherPacificIslander"), dataApp.aBIsOtherPacificIslander);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCIsOtherPacificIslander"), dataApp.aCIsOtherPacificIslander);

            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBOtherHispanicOrLatinoDescription"), dataApp.aBOtherHispanicOrLatinoDescription);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCOtherHispanicOrLatinoDescription"), dataApp.aCOtherHispanicOrLatinoDescription);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBOtherAmericanIndianDescription"), dataApp.aBOtherAmericanIndianDescription);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCOtherAmericanIndianDescription"), dataApp.aCOtherAmericanIndianDescription);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBOtherAsianDescription"), dataApp.aBOtherAsianDescription);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCOtherAsianDescription"), dataApp.aCOtherAsianDescription);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBOtherPacificIslanderDescription"), dataApp.aBOtherPacificIslanderDescription);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCOtherPacificIslanderDescription"), dataApp.aCOtherPacificIslanderDescription);

            serviceItem.SetResult("aBEthnicityCollectedByObservationOrSurname", dataApp.aBEthnicityCollectedByObservationOrSurname.ToString("D"));
            serviceItem.SetResult("aCEthnicityCollectedByObservationOrSurname", dataApp.aCEthnicityCollectedByObservationOrSurname.ToString("D"));
            serviceItem.SetResult("aBSexCollectedByObservationOrSurname", dataApp.aBSexCollectedByObservationOrSurname.ToString("D"));
            serviceItem.SetResult("aCSexCollectedByObservationOrSurname", dataApp.aCSexCollectedByObservationOrSurname.ToString("D"));
            serviceItem.SetResult("aBRaceCollectedByObservationOrSurname", dataApp.aBRaceCollectedByObservationOrSurname.ToString("D"));
            serviceItem.SetResult("aCRaceCollectedByObservationOrSurname", dataApp.aCRaceCollectedByObservationOrSurname.ToString("D"));
            serviceItem.SetResult("aBInterviewMethodT", dataApp.aBInterviewMethodT.ToString("D"));
            serviceItem.SetResult("aCInterviewMethodT", dataApp.aCInterviewMethodT.ToString("D"));
            serviceItem.SetResult("aBHispanicT", dataApp.aBHispanicT.ToString("D"));
            serviceItem.SetResult("aCHispanicT", dataApp.aCHispanicT.ToString("D"));
            serviceItem.SetResult("aBGender", dataApp.aBGender.ToString("D"));
            serviceItem.SetResult("aCGender", dataApp.aCGender.ToString("D"));

            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBHispanicTFallback"), dataApp.aBHispanicTFallback.ToString("D"));
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCHispanicTFallback"), dataApp.aCHispanicTFallback.ToString("D"));
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBGenderFallback"), dataApp.aBGenderFallback.ToString("D"));
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCGenderFallback"), dataApp.aCGenderFallback.ToString("D"));

            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBNoFurnish"), dataApp.aBNoFurnish);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aBNoFurnishLckd"), dataApp.aBNoFurnishLckd);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCNoFurnish"), dataApp.aCNoFurnish);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "aCNoFurnishLckd"), dataApp.aCNoFurnishLckd);
        }

        /// <summary>
        /// Initializes the popup.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            var page = this.Page as UI.BasePage;
            if (page == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "HmdaRaceEthnicityData can only be used on a BaseLoanPage");
            }


            this.aBInterviewMethodT_ei.Value = E_aIntrvwrMethodT.Internet.ToString("D");
            this.aBInterviewMethodT_fm.Value = E_aIntrvwrMethodT.ByMail.ToString("D");
            this.aBInterviewMethodT_ftf.Value = E_aIntrvwrMethodT.FaceToFace.ToString("D");
            this.aBInterviewMethodT_ti.Value = E_aIntrvwrMethodT.ByTelephone.ToString("D");
            this.aBInterviewMethodT_bl.Value = E_aIntrvwrMethodT.LeaveBlank.ToString("D");

            this.aCInterviewMethodT_ei.Value = E_aIntrvwrMethodT.Internet.ToString("D");
            this.aCInterviewMethodT_fm.Value = E_aIntrvwrMethodT.ByMail.ToString("D");
            this.aCInterviewMethodT_ftf.Value = E_aIntrvwrMethodT.FaceToFace.ToString("D");
            this.aCInterviewMethodT_ti.Value = E_aIntrvwrMethodT.ByTelephone.ToString("D");
            this.aCInterviewMethodT_bl.Value = E_aIntrvwrMethodT.LeaveBlank.ToString("D");

            Tools.Bind_aHispanicT(this.aBHispanicTFallback, shouldIncludeBothOption: false);
            Tools.Bind_GenderT(this.aBGenderFallback, shouldIncludeBothOption: false);
            Tools.Bind_aHispanicT(this.aCHispanicTFallback, shouldIncludeBothOption: false);
            Tools.Bind_GenderT(this.aCGenderFallback, shouldIncludeBothOption: false);
        }

        /// <summary>
        /// Loads data for the popup.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageLoad(object sender, EventArgs e)
        {
            if (!this.ShouldLoad)
            {
                return;
            }

            if (this.RaceEthnicityData == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "RaceEthnicityData must be set.");
            }

            var page = this.Page as UI.BasePage;
            page.EnableJqueryMigrate = false;
            page.EnableJquery = false;
            page.RegisterJsScript("/webapp/HmdaRaceEthnicityData.js");
            
            this.aBIsMexican.Checked = this.RaceEthnicityData.aBIsMexican;
            this.aCIsMexican.Checked = this.RaceEthnicityData.aCIsMexican;
            this.aBIsPuertoRican.Checked = this.RaceEthnicityData.aBIsPuertoRican;
            this.aCIsPuertoRican.Checked = this.RaceEthnicityData.aCIsPuertoRican;
            this.aBIsCuban.Checked = this.RaceEthnicityData.aBIsCuban;
            this.aCIsCuban.Checked = this.RaceEthnicityData.aCIsCuban;
            this.aBIsOtherHispanicOrLatino.Checked = this.RaceEthnicityData.aBIsOtherHispanicOrLatino;
            this.aCIsOtherHispanicOrLatino.Checked = this.RaceEthnicityData.aCIsOtherHispanicOrLatino;
            this.aBOtherHispanicOrLatinoDescription.Value = this.RaceEthnicityData.aBOtherHispanicOrLatinoDescription;
            this.aCOtherHispanicOrLatinoDescription.Value = this.RaceEthnicityData.aCOtherHispanicOrLatinoDescription;
            this.aBDoesNotWishToProvideEthnicity.Checked = this.RaceEthnicityData.aBDoesNotWishToProvideEthnicity;
            this.aCDoesNotWishToProvideEthnicity.Checked = this.RaceEthnicityData.aCDoesNotWishToProvideEthnicity;
            this.aBIsAmericanIndian.Checked = this.RaceEthnicityData.aBIsAmericanIndian;
            this.aCIsAmericanIndian.Checked = this.RaceEthnicityData.aCIsAmericanIndian;
            this.aBOtherAmericanIndianDescription.Value = this.RaceEthnicityData.aBOtherAmericanIndianDescription;
            this.aCOtherAmericanIndianDescription.Value = this.RaceEthnicityData.aCOtherAmericanIndianDescription;
            this.aBIsAsian.Checked = this.RaceEthnicityData.aBIsAsian;
            this.aCIsAsian.Checked = this.RaceEthnicityData.aCIsAsian;
            this.aBIsAsianIndian.Checked = this.RaceEthnicityData.aBIsAsianIndian;
            this.aCIsAsianIndian.Checked = this.RaceEthnicityData.aCIsAsianIndian;
            this.aBIsChinese.Checked = this.RaceEthnicityData.aBIsChinese;
            this.aCIsChinese.Checked = this.RaceEthnicityData.aCIsChinese;
            this.aBIsFilipino.Checked = this.RaceEthnicityData.aBIsFilipino;
            this.aCIsFilipino.Checked = this.RaceEthnicityData.aCIsFilipino;
            this.aBIsJapanese.Checked = this.RaceEthnicityData.aBIsJapanese;
            this.aCIsJapanese.Checked = this.RaceEthnicityData.aCIsJapanese;
            this.aBIsKorean.Checked = this.RaceEthnicityData.aBIsKorean;
            this.aCIsKorean.Checked = this.RaceEthnicityData.aCIsKorean;
            this.aBIsVietnamese.Checked = this.RaceEthnicityData.aBIsVietnamese;
            this.aCIsVietnamese.Checked = this.RaceEthnicityData.aCIsVietnamese;
            this.aBIsOtherAsian.Checked = this.RaceEthnicityData.aBIsOtherAsian;
            this.aCIsOtherAsian.Checked = this.RaceEthnicityData.aCIsOtherAsian;
            this.aBOtherAsianDescription.Value = this.RaceEthnicityData.aBOtherAsianDescription;
            this.aCOtherAsianDescription.Value = this.RaceEthnicityData.aCOtherAsianDescription;
            this.aBIsBlack.Checked = this.RaceEthnicityData.aBIsBlack;
            this.aCIsBlack.Checked = this.RaceEthnicityData.aCIsBlack;
            this.aBIsPacificIslander.Checked = this.RaceEthnicityData.aBIsPacificIslander;
            this.aCIsPacificIslander.Checked = this.RaceEthnicityData.aCIsPacificIslander;
            this.aBIsNativeHawaiian.Checked = this.RaceEthnicityData.aBIsNativeHawaiian;
            this.aCIsNativeHawaiian.Checked = this.RaceEthnicityData.aCIsNativeHawaiian;
            this.aBIsGuamanianOrChamorro.Checked = this.RaceEthnicityData.aBIsGuamanianOrChamorro;
            this.aCIsGuamanianOrChamorro.Checked = this.RaceEthnicityData.aCIsGuamanianOrChamorro;
            this.aBIsSamoan.Checked = this.RaceEthnicityData.aBIsSamoan;
            this.aCIsSamoan.Checked = this.RaceEthnicityData.aCIsSamoan;
            this.aBIsOtherPacificIslander.Checked = this.RaceEthnicityData.aBIsOtherPacificIslander;
            this.aCIsOtherPacificIslander.Checked = this.RaceEthnicityData.aCIsOtherPacificIslander;
            this.aBOtherPacificIslanderDescription.Value = this.RaceEthnicityData.aBOtherPacificIslanderDescription;
            this.aCOtherPacificIslanderDescription.Value = this.RaceEthnicityData.aCOtherPacificIslanderDescription;
            this.aBIsWhite.Checked = this.RaceEthnicityData.aBIsWhite;
            this.aCIsWhite.Checked = this.RaceEthnicityData.aCIsWhite;
            this.aBDoesNotWishToProvideRace.Checked = this.RaceEthnicityData.aBDoesNotWishToProvideRace;
            this.aCDoesNotWishToProvideRace.Checked = this.RaceEthnicityData.aCDoesNotWishToProvideRace;

            this.aBEthnicityCollectedByObservationOrSurname_False.Checked = this.RaceEthnicityData.aBEthnicityCollectedByObservationOrSurname == E_TriState.No;
            this.aBEthnicityCollectedByObservationOrSurname_True.Checked = this.RaceEthnicityData.aBEthnicityCollectedByObservationOrSurname == E_TriState.Yes;
            this.aBSexCollectedByObservationOrSurname_False.Checked = this.RaceEthnicityData.aBSexCollectedByObservationOrSurname == E_TriState.No;
            this.aBSexCollectedByObservationOrSurname_True.Checked = this.RaceEthnicityData.aBSexCollectedByObservationOrSurname == E_TriState.Yes;
            this.aBRaceCollectedByObservationOrSurname_False.Checked = this.RaceEthnicityData.aBRaceCollectedByObservationOrSurname == E_TriState.No;
            this.aBRaceCollectedByObservationOrSurname_True.Checked = this.RaceEthnicityData.aBRaceCollectedByObservationOrSurname == E_TriState.Yes;

            this.aCEthnicityCollectedByObservationOrSurname_False.Checked = this.RaceEthnicityData.aCEthnicityCollectedByObservationOrSurname == E_TriState.No;
            this.aCEthnicityCollectedByObservationOrSurname_True.Checked = this.RaceEthnicityData.aCEthnicityCollectedByObservationOrSurname == E_TriState.Yes;
            this.aCSexCollectedByObservationOrSurname_False.Checked = this.RaceEthnicityData.aCSexCollectedByObservationOrSurname == E_TriState.No;
            this.aCSexCollectedByObservationOrSurname_True.Checked = this.RaceEthnicityData.aCSexCollectedByObservationOrSurname == E_TriState.Yes;
            this.aCRaceCollectedByObservationOrSurname_False.Checked = this.RaceEthnicityData.aCRaceCollectedByObservationOrSurname == E_TriState.No;
            this.aCRaceCollectedByObservationOrSurname_True.Checked = this.RaceEthnicityData.aCRaceCollectedByObservationOrSurname == E_TriState.Yes;

            this.aBInterviewMethodT_ei.Checked = this.RaceEthnicityData.aBInterviewMethodT == E_aIntrvwrMethodT.Internet;
            this.aBInterviewMethodT_fm.Checked = this.RaceEthnicityData.aBInterviewMethodT == E_aIntrvwrMethodT.ByMail;
            this.aBInterviewMethodT_ftf.Checked = this.RaceEthnicityData.aBInterviewMethodT == E_aIntrvwrMethodT.FaceToFace;
            this.aBInterviewMethodT_ti.Checked = this.RaceEthnicityData.aBInterviewMethodT == E_aIntrvwrMethodT.ByTelephone;
            this.aBInterviewMethodT_bl.Checked = this.RaceEthnicityData.aBInterviewMethodT == E_aIntrvwrMethodT.LeaveBlank;

            this.aCInterviewMethodT_ei.Checked = this.RaceEthnicityData.aCInterviewMethodT == E_aIntrvwrMethodT.Internet;
            this.aCInterviewMethodT_fm.Checked = this.RaceEthnicityData.aCInterviewMethodT == E_aIntrvwrMethodT.ByMail;
            this.aCInterviewMethodT_ftf.Checked = this.RaceEthnicityData.aCInterviewMethodT == E_aIntrvwrMethodT.FaceToFace;
            this.aCInterviewMethodT_ti.Checked = this.RaceEthnicityData.aCInterviewMethodT == E_aIntrvwrMethodT.ByTelephone;
            this.aCInterviewMethodT_bl.Checked = this.RaceEthnicityData.aCInterviewMethodT == E_aIntrvwrMethodT.LeaveBlank;

            this.aBHispanicT_is.Checked = this.RaceEthnicityData.aBHispanicT == E_aHispanicT.BothHispanicAndNotHispanic || this.RaceEthnicityData.aBHispanicT == E_aHispanicT.Hispanic;
            this.aBHispanicT_not.Checked = this.RaceEthnicityData.aBHispanicT == E_aHispanicT.BothHispanicAndNotHispanic || this.RaceEthnicityData.aBHispanicT == E_aHispanicT.NotHispanic;

            this.aCHispanicT_is.Checked = this.RaceEthnicityData.aCHispanicT == E_aHispanicT.BothHispanicAndNotHispanic || this.RaceEthnicityData.aCHispanicT == E_aHispanicT.Hispanic;
            this.aCHispanicT_not.Checked = this.RaceEthnicityData.aCHispanicT == E_aHispanicT.BothHispanicAndNotHispanic || this.RaceEthnicityData.aCHispanicT == E_aHispanicT.NotHispanic;

            this.SetGenderCheckboxes(this.aBGender_f, this.aBGender_m, this.aBGender_u, this.RaceEthnicityData.aBGender);
            this.SetGenderCheckboxes(this.aCGender_f, this.aCGender_m, this.aCGender_u, this.RaceEthnicityData.aCGender);

            Tools.SetDropDownListValue(this.aBHispanicTFallback, this.RaceEthnicityData.aBHispanictFallback);
            Tools.SetDropDownListValue(this.aBGenderFallback, this.RaceEthnicityData.aBGenderFallback);
            Tools.SetDropDownListValue(this.aCHispanicTFallback, this.RaceEthnicityData.aCHispanicTFallback);
            Tools.SetDropDownListValue(this.aCGenderFallback, this.RaceEthnicityData.aCGenderFallback);

            this.aBNoFurnish.Checked = this.RaceEthnicityData.aBNoFurnish;
            this.aBNoFurnishLckd.Checked = this.RaceEthnicityData.aBNoFurnishLckd;
            this.aCNoFurnish.Checked = this.RaceEthnicityData.aCNoFurnish;
            this.aCNoFurnishLckd.Checked = this.RaceEthnicityData.aCNoFurnishLckd;
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// <param name="e">Event argument parameter.</param>
        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Sets the gender checkboxes.
        /// </summary>
        /// <param name="femaleCb">The checkbox for the female option.</param>
        /// <param name="maleCb">The checkbox for the male option.</param>
        /// <param name="unfurnishedCb">The checkbox for the unfurnished option.</param>
        /// <param name="genderValue">The gender value.</param>
        private void SetGenderCheckboxes(HtmlInputCheckBox femaleCb, HtmlInputCheckBox maleCb, HtmlInputCheckBox unfurnishedCb, E_GenderT genderValue)
        {
            femaleCb.Checked = genderValue == E_GenderT.Female || genderValue == E_GenderT.MaleAndFemale || genderValue == E_GenderT.FemaleAndNotFurnished || genderValue == E_GenderT.MaleFemaleNotFurnished;
            maleCb.Checked = genderValue == E_GenderT.Male || genderValue == E_GenderT.MaleAndFemale || genderValue == E_GenderT.MaleAndNotFurnished || genderValue == E_GenderT.MaleFemaleNotFurnished;
            unfurnishedCb.Checked = genderValue == E_GenderT.Unfurnished || genderValue == E_GenderT.MaleFemaleNotFurnished || genderValue == E_GenderT.MaleAndNotFurnished || genderValue == E_GenderT.FemaleAndNotFurnished;
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
    }
}