﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TotalRenovationCostCalculator.aspx.cs" Inherits="PriceMyLoan.webapp.TotalRenovationCostCalculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Total Renovation Cost Calculator</title>
    <style type="text/css">
        body {
            background-color: white !important;
            margin: 8px !important;
        }
        h1 {
            margin: 0px;
        }
        .width20 {
            width: 20px;
        }
        .width40 {
            width: 40px;
        }
        .width90Important {
            width: 90px !important;
        }
        .width120 {
            width: 120px;
        }
        .width150 {
            width: 150px;
        }
        .width240 {
            width: 240px;
        }
        th {
            background-color: grey;
            color: white;
        }
        .width960 {
            width: 960px;
        }
        .width860 {
            width: 860px;
        }
        .alignLeft {
            text-align: left;
        }
        .alignCenter {
            text-align: center;
        }
        .alignRight {
            text-align: right;
            float: right;
        }
        .alignButtons {
            position: absolute;
            right: 22px;
        }
        .solidBorder {
            border: 1px solid;
            margin: 10px;
            border-collapse: collapse;
        }
        table {
            margin-left: 20px;
            margin-right: 20px;
            vertical-align: central;            
        }
        .stepTable td, .stepTable th {
            border: .5px solid;
            border-color: grey;
            border-width: 1px;
            padding-top: 2px;
            padding-right: 4px;
            padding-left: 4px;
        }
        .padLeft {
            padding-left: 20px;
        }
        .stepTable {
            margin: 0px;
            padding: 0px;
        }
        input[readonly], textarea[readonly] {
            background-color: lightgrey;
            border-width: 1px;
        }
        td, th {
            height: 30px;
            vertical-align: middle;
        }
        .Hidden {
            display: none;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        $j(document).ready(function () {
            checkMonth(document.getElementById('sRenoMtgPmntRsrvMnth'));
            lockField(document.getElementById('sFinanceMortgagePmntRsrvTgtValLckd'), 'sFinanceMortgagePmntRsrvTgtVal');
            setReadonlyFieldDisplay();
        });

        function f_SaveTotalRenovationCostCalculator() {
            var args = getAllFormValues(null);
            args["loanId"] = parent.ML.sLId;
            var result = gService.TotalRenovationCostCalculatorService.call("SaveData", args);
            f_SaveTotalRenovationCostCalculatorCallback(result);
        }

        function f_SaveTotalRenovationCostCalculatorCallback(result) {
            if (!result.error) {
                parent.LQBPopup.Return({ OK: true, sTotalRenovationCosts: result.value["sTotalRenovationCosts"] });

            }
            else {
                alert("Error while saving data.");
            }
        }

        function f_CancelTotalRenovationCostCalculator() {
            parent.LQBPopup.Return({ OK: false });
        }

        function refreshCalculation() {
            var args = getAllFormValues(null);
            args["loanId"] = parent.ML.sLId;
            var result = gService.TotalRenovationCostCalculatorService.call("RefreshCalc", args);
            refreshCalculationCallback(result);
        }

        function refreshCalculationCallback(result) {
            if (!result.error) {
                populateForm(result.value);
                setReadonlyFieldDisplay();
            }            
        }

        function checkMonth(month) {
            if (month.value > 6) {
                $j("#sRenoMtgPmntRsrvMnth").val(6);
            }
        }

        function setReadonlyFieldDisplay() {
            var isFha203kTypeLimited = $j("#sFHA203kType").val() == 1;
            $j("#sFeasibilityStudyFee").prop('readonly', isFha203kTypeLimited);
            $j("#sRenoMtgPmntRsrvMnth").prop('readonly', isFha203kTypeLimited);
            $j("#sFinanceMortgagePmntRsrvTgtValLckd").prop('disabled', isFha203kTypeLimited);

            if (isFha203kTypeLimited) {
                $j("#sArchitectEngineerFee").prop('readonly', true);
                $j("#sConsultantFee").prop('readonly', true);
            }
            else {
                lockField(document.getElementById("sArchitectEngineerFeeLckd"), "sArchitectEngineerFee");
                lockField(document.getElementById("sConsultantFeeLckd"), "sConsultantFee");
            }

            lockField(document.getElementById("sRenovationConstrInspectFeeLckd"), "sRenovationConstrInspectFee");
            lockField(document.getElementById("sTitleUpdateFeeLckd"), "sTitleUpdateFee");
            lockField(document.getElementById("sPermitFeeLckd"), "sPermitFee");
        }
    </script>
    <h4 class="page-header">Total Renovation Cost Calculator</h4>
    <form id="form1" runat="server">
    <div class="stepTable">
        <table class="width960 solidBorder">
            <tbody>
                <tr>
                    <td>
                        203(k) Type <i>(FHA only)</i>
                    </td>
                    <td colspan="2" class="alignCenter">
                        <asp:DropDownList ID="sFHA203kType" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        a. Hard Costs <i>(Labor/Materials)</i>
                    </td>
                    <td class="width120">
                    </td>
                    <td class="width120">
                        <ML:MoneyTextBox id="sCostConstrRepairsRehab" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        b. Contingency Reserve <i>(if applicable and financed)</i>
                    </td>
                    <td>
                        <ML:PercentTextBox id="sContingencyRsrvPcRenovationHardCost" runat="server" class="width90Important" onchange="refreshCalculation();"></ML:PercentTextBox>
                    </td>
                    <td>
                        <ML:MoneyTextBox id="sFinanceContingencyRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        c. Architect/Engineer Fees
                    </td>
                    <td>
                    </td>
                    <td>
                        <input type="checkbox" id="sArchitectEngineerFeeLckd" runat="server" class="Hidden" />
                        <ML:MoneyTextBox id="sArchitectEngineerFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        d. Consultant Fees
                    </td>
                    <td>
                    </td>
                    <td>
                        <input type="checkbox" id="sConsultantFeeLckd" runat="server" class="Hidden" />
                        <ML:MoneyTextBox id="sConsultantFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        e. Inspections
                    </td>
                    <td>
                    </td>
                    <td>
                        <input type="checkbox" id="sRenovationConstrInspectFeeLckd" runat="server" class="Hidden" />
                        <ML:MoneyTextBox id="sRenovationConstrInspectFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        f. Title Updates
                    </td>
                    <td>
                    </td>
                    <td>
                        <input type="checkbox" id="sTitleUpdateFeeLckd" runat="server" class="Hidden" />
                        <ML:MoneyTextBox id="sTitleUpdateFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        g. Permits
                    </td>
                    <td>
                    </td>
                    <td>
                        <input type="checkbox" id="sPermitFeeLckd" runat="server" class="Hidden" />
                        <ML:MoneyTextBox id="sPermitFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        h. Payment Reserve <i>(Months not occupied x Monthly Payment - Not to exceed 6 months)</i>
                    </td>
                    <td>
                    </td>
                    <td>
                        <ML:MoneyTextBox id="sFinanceMortgagePmntRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="padLeft">
                            Target Value
                        </span>
                        <span class="alignRight">
                            <asp:TextBox ID="sRenoMtgPmntRsrvMnth" runat="server" class="width20" maxlength="1" onchange="checkMonth(this); refreshCalculation();"></asp:TextBox>
                            months x 
                            <ML:MoneyTextBox ID="sMonthlyPmt" runat="server" readonly="true"></ML:MoneyTextBox>
                            =
                        </span>                                
                    </td>                            
                    <td>
                        <ML:MoneyTextBox id="sFinanceMortgagePmntRsrvTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                        <asp:CheckBox ID="sFinanceMortgagePmntRsrvTgtValLckd" class="alignCheckBox" runat="server" onchange="lockField(document.getElementById('sFinanceMortgagePmntRsrvTgtValLckd'), 'sFinanceMortgagePmntRsrvTgtVal');"/>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="padLeft">
                            Maximum Financeable Mortgage Payment Reserves
                        </span>
                    </td>
                    <td>
                        <ML:MoneyTextBox id="sMaxFinanceMortgagePmntRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        i. Other&nbsp;
                        <span>
                            <asp:TextBox id="sOtherRenovationCostDescription" runat="server" class="width240"></asp:TextBox>
                        </span>
                    </td>
                    <td>
                    </td>
                    <td class="width120">
                        <ML:MoneyTextBox id="sOtherRenovationCost" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        j. Financeable Origination Fee
                    </td>
                    <td>
                    </td>
                    <td>
                        <ML:MoneyTextBox id="sFinanceOriginationFee" runat="server" readonly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="padLeft">
                            Target Value
                        </span>
                    </td>
                    <td>
                        <span>
                            <ML:MoneyTextBox id="sFinanceOriginationFeeTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                        </span>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="padLeft">
                            Maximum Financeable Origination Fee               
                        </span>
                    </td>
                    <td>
                        <span>
                            <ML:MoneyTextBox id="sMaxFinanceOriginationFee" runat="server" readonly="true"></ML:MoneyTextBox>
                        </span>
                    </td>
                    <td>
                    </td>                            
                </tr>
                <tr>
                    <td>
                        k. Discount Points
                    </td>
                    <td>
                    </td>
                    <td>
                        <ML:MoneyTextBox id="sDiscntPntOnRepairCostFee" runat="server" readonly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td> 
                        <span class="padLeft">
                            Target Value
                        </span>                               
                    </td>
                    <td>
                        <span>
                            <ML:MoneyTextBox id="sDiscntPntOnRepairCostFeeTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                        </span>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="padLeft"> 
                            Maximum Financeable Discount Points
                        </span>
                    </td>
                    <td>
                        <span>
                            <ML:MoneyTextBox id="sMaxDiscntPntOnRepairCostFee" runat="server" readonly="true"></ML:MoneyTextBox>
                        </span>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        l. Feasibility Study
                    </td>
                    <td>
                    </td>
                    <td>
                        <ML:MoneyTextBox id="sFeasibilityStudyFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>                                
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel">
                            Total Renovation Cost <i>(Total of a to l)</i>
                        </label>
                    </td>
                    <td>
                        <span class="alignRight">
                            =
                        </span>
                    </td>
                    <td>
                        <ML:MoneyTextBox id="sTotalRenovationCosts" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
            </tbody>
        </table>                    
    </div>    
    <div class="alignButtons">
        <input type="button" id="saveButton" value="Save" onclick="f_SaveTotalRenovationCostCalculator();" />
        <input type="button" id="cancelButton" value="Cancel" onclick="f_CancelTotalRenovationCostCalculator();" />
    </div>  
    </form>
</body>
</html>
