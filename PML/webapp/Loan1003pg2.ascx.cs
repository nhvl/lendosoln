﻿using System;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
namespace PriceMyLoan.webapp
{
    public partial class Loan1003pg2 : PriceMyLoan.UI.BaseUserControl
    {
        public void LoadData()
        {
            Tools.Bind_aOIDescT(OtherIncomeCombo);

            BrokerDB CurrentBroker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg2));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(RequestHelper.GetGuid("applicationid", Guid.Empty));

            //BindOtherIncomeDescription(OtherIncomeCombo);
            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);

            var incomeFields = new[]
            {
                aBBaseI,
                aBOvertimeI,
                aBBonusesI,
                aBCommisionI,
                aBDividendI,
                aCBaseI,
                aCOvertimeI,
                aCBonusesI,
                aCCommisionI,
                aCDividendI,
            };

            foreach (var incomeField in incomeFields)
            {
                incomeField.ReadOnly = dataLoan.sIsIncomeCollectionEnabled;
            }

            AddMoreOtherInc.Visible = !dataLoan.sIsIncomeCollectionEnabled;

            aBBaseI.Text = dataApp.aBBaseI_rep;
            aBOvertimeI.Text = dataApp.aBOvertimeI_rep;
            aBBonusesI.Text = dataApp.aBBonusesI_rep;
            aBCommisionI.Text = dataApp.aBCommisionI_rep;
            aBDividendI.Text = dataApp.aBDividendI_rep;
            aBNetRentI1003.Text = dataApp.aBNetRentI1003_rep;
            aBSpPosCf.Text = dataApp.aBSpPosCf_rep;
            aBTotOI.Text = dataApp.aBTotOI_rep;
            aBTotI.Text = dataApp.aBTotI_rep;

            aCBaseI.Text = dataApp.aCBaseI_rep;
            aCOvertimeI.Text = dataApp.aCOvertimeI_rep;
            aCBonusesI.Text = dataApp.aCBonusesI_rep;
            aCCommisionI.Text = dataApp.aCCommisionI_rep;
            aCDividendI.Text = dataApp.aCDividendI_rep;
            aCNetRentI1003.Text = dataApp.aCNetRentI1003_rep;
            aCSpPosCf.Text = dataApp.aCSpPosCf_rep;
            aCTotOI.Text = dataApp.aCTotOI_rep;
            aCTotI.Text = dataApp.aCTotI_rep;

            aTotBaseI.Text = dataApp.aTotBaseI_rep;
            aTotOvertimeI.Text = dataApp.aTotOvertimeI_rep;
            aTotBonusesI.Text = dataApp.aTotBonusesI_rep;
            aTotCommisionI.Text = dataApp.aTotCommisionI_rep;
            aTotDividendI.Text = dataApp.aTotDividendI_rep;
            aTotNetRentI1003.Text = dataApp.aTotNetRentI1003_rep;
            aTotSpPosCf.Text = dataApp.aTotSpPosCf_rep;
            aTotOI.Text = dataApp.aTotOI_rep;
            aTotI.Text = dataApp.aTotI_rep;

            OtherIncomeJson.Value = ObsoleteSerializationHelper.JsonSerialize(dataApp.aOtherIncomeList);

            aPresRent.Text = dataApp.aPresRent_rep;
            aPres1stM.Text = dataApp.aPres1stM_rep;
            aPresOFin.Text = dataApp.aPresOFin_rep;

            sProFirstMPmt.Text = dataLoan.sProFirstMPmt_rep;
            sProSecondMPmt.Text = dataLoan.sProSecondMPmt_rep;

            bool isHazExpCalcVisible = !dataLoan.sHazardExpenseInDisbursementMode;
            aPresHazIns.Text = dataApp.aPresHazIns_rep;
            //sProHazInsT.Value =""+ dataLoan.sProHazInsT;
            sProHazInsBaseAmt.Text = dataLoan.sProHazInsBaseAmt_rep;
            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            sProHazIns.Text = dataLoan.sProHazIns_rep;
            sProHazInsBaseAmtHolder.Visible = isHazExpCalcVisible;
            sProHazInsRHolder.Visible = isHazExpCalcVisible;
            sProHazInsMbHolder.Visible = isHazExpCalcVisible;
            sProHazInsTHolder.Visible = isHazExpCalcVisible;
            sProHazInsPlusHolder.Visible = isHazExpCalcVisible;

            bool isRealEstTaxCalcVisible = !dataLoan.sRealEstateTaxExpenseInDisbMode;
            aPresRealETx.Text = dataApp.aPresRealETx_rep;
            //sProRealETxT.Value = "" + dataLoan.sProRealETxT;
            sProRealETxBaseAmt.Text = dataLoan.sProRealETxBaseAmt_rep;
            sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            sProRealETx.Text = dataLoan.sProRealETx_rep;
            sProRealETxTHolder.Visible = isRealEstTaxCalcVisible;
            sProRealETxBaseAmtHolder.Visible = isRealEstTaxCalcVisible;
            sProRealETxRHolder.Visible = isRealEstTaxCalcVisible;
            sProRealETxMbHolder.Visible = isRealEstTaxCalcVisible;
            sProRealETxPlusHolder.Visible = isRealEstTaxCalcVisible;

            OtherIncomeJson.Value = ObsoleteSerializationHelper.JsonSerialize(dataApp.aOtherIncomeList);

            Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);

            aPresMIns.Text = dataApp.aPresMIns_rep;
            sProMIns.Text = dataLoan.sProMIns_rep;

            aPresHoAssocDues.Text = dataApp.aPresHoAssocDues_rep;
            sProHoAssocDues.Text = dataLoan.sProHoAssocDues_rep;

            sProOHExpDesc.Value = dataLoan.sProOHExpDesc;
            aPresOHExp.Text = dataApp.aPresOHExp_rep;
            sProOHExpLckd.Checked = dataLoan.sProOHExpLckd;
            sProOHExp.Text = dataLoan.sProOHExp_rep;

            aPresTotHExp.Text = dataApp.aPresTotHExp_rep;
            sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep;

            aAsstLiaCompletedNotJointly_1.Checked = dataApp.aAsstLiaCompletedNotJointly;
            aAsstLiaCompletedNotJointly_0.Checked = !dataApp.aAsstLiaCompletedNotJointly;

            IAssetCollection assetCollection = dataApp.aAssetCollection;

            var cashDeposit1 = assetCollection.GetCashDeposit1(false);
            CashDeposit1Desc.Text = cashDeposit1?.Desc;
            CashDeposit1Val.Text = cashDeposit1?.Val_rep;

            var cashDeposit2 = assetCollection.GetCashDeposit2(false);
            CashDeposit2Desc.Text = cashDeposit2?.Desc;
            CashDeposit2Val.Text = cashDeposit2?.Val_rep;

            var lifeIns = assetCollection.GetLifeInsurance(false);
            LifeInsCashValue.Text = lifeIns?.Val_rep;
            LifeInsFaceAmt.Text = lifeIns?.FaceVal_rep;

            RetirementFunds.Text = assetCollection.GetRetirement(false)?.Val_rep;

            BusinessVal.Text = assetCollection.GetBusinessWorth(false)?.Val_rep;

            aAsstLiqTot.Text = dataApp.aAsstLiqTot_rep;
            aAsstNonReSolidTot.Text = dataApp.aAsstNonReSolidTot_rep;
            aReTotVal.Text = dataApp.aReTotVal_rep;
            aAsstValTot.Text = dataApp.aAsstValTot_rep;

            aLiaMonTot.Text = dataApp.aLiaMonTot_rep;
            aLiaBalTot.Text = dataApp.aLiaBalTot_rep;
            aNetWorth.Text = dataApp.aNetWorth_rep;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            
        }
    }
}