﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using global::DataAccess.CounselingOrganization;
    using global::DataAccess.HomeownerCounselingOrganizations;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.ChecklistGeneration;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.DocumentGeneration;
    using LendersOffice.ObjLib.Events;
    using LendersOffice.ObjLib.TPO;
    using LendersOffice.Security;
    using PriceMyLoan.Security;

    /// <summary>
    /// Provides the implementation for the "Disclosures" page service.
    /// </summary>
    public class DisclosuresServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Gets the principal for the current user.
        /// </summary>
        /// <value>
        /// The principal for the current user.
        /// </value>
        private PriceMyLoanPrincipal CurrentPrincipal
        {
            get
            {
                return LendersOffice.Security.PrincipalFactory.CurrentPrincipal as PriceMyLoanPrincipal;
            }
        }

        /// <summary>
        /// Processes the method with the specified name.
        /// </summary>
        /// <param name="methodName">
        /// The name of the method.
        /// </param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(GetDisclosureViewModel):
                    this.GetDisclosureViewModel(bindData: false);
                    return;
                case nameof(RefreshCalculation):
                    this.RefreshCalculation();
                    return;
                case nameof(SaveAntiSteeringDisclosure):
                    this.SaveAntiSteeringDisclosure();
                    return;
                case nameof(SaveLoanDates):
                    this.SaveLoanDates();
                    return;
                case nameof(RequestLenderToCompleteOrder):
                    this.RequestLenderToCompleteOrder();
                    break;
                case nameof(CancelDisclosureRequest):
                    this.CancelDisclosureRequest();
                    break;
                case nameof(LoadAntiSteeringDisclosure):
                    this.LoadAntiSteeringDisclosure();
                    break;
                case nameof(LoadRequestReview):
                    this.LoadRequestReview();
                    return;
                case nameof(LoadDocumentAudit):
                    this.LoadDocumentAudit();
                    return;
                case "PlaceDocumentPreviewOrder":
                    this.PlaceOrder(isPreview: true);
                    return;
                case "PlaceActualDocumentOrder":
                    this.PlaceOrder(isPreview: false);
                    return;
                default:
                    throw new InvalidOperationException("Unhandled method " + methodName);
            }
        }

        /// <summary>
        /// Creates the loan object for the page.
        /// </summary>
        /// <param name="loanId">
        /// The ID for the loan.
        /// </param>
        /// <returns>
        /// The loan object.
        /// </returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(DisclosuresServiceItem));
            dataLoan.ReportAllWorkflowFailureMessages = true;
            return dataLoan;
        }

        /// <summary>
        /// Binds data to the specified loan and application from the page.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan to bind data.
        /// </param>
        /// <param name="dataApp">
        /// This parameter is not used.
        /// </param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sAppSubmittedD_rep = this.GetString(nameof(dataLoan.sAppSubmittedD));
            dataLoan.sAppSubmittedDLckd = this.GetBool(nameof(dataLoan.sAppSubmittedDLckd));
            dataLoan.sIntentToProceedD_rep = this.GetString("sIntentToProceedD");
            dataLoan.sEstCloseD_rep = this.GetString(nameof(dataLoan.sEstCloseD));
            dataLoan.sEstCloseDLckd = this.GetBool(nameof(dataLoan.sEstCloseDLckd));
        }

        private void BindAntiSteeringData(CPageData dataloan)
        {
            switch (dataloan.sFinMethT)
            {
                case E_sFinMethT.ARM:
                    dataloan.sNoteIRSafeHarborARMWithRiskyFeatures_rep = this.GetString("sNoteIRSafeHarborWithRiskyFeatures");
                    dataloan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep = this.GetString("sNoteIRSafeHarborWithoutRiskyFeatures");
                    dataloan.sNoteIRSafeHarborARMLowestCost_rep = this.GetString("sNoteIRSafeHarborLowestCost");

                    dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep = this.GetString("sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures");
                    dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep = this.GetString("sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures");
                    dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep = this.GetString("sTotalDiscountPointOriginationFeeSafeHarborLowestCost");
                    break;
                case E_sFinMethT.Fixed:
                case E_sFinMethT.Graduated:
                    dataloan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep = this.GetString("sNoteIRSafeHarborWithRiskyFeatures");
                    dataloan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep = this.GetString("sNoteIRSafeHarborWithoutRiskyFeatures");
                    dataloan.sNoteIRSafeHarborFixedLowestCost_rep = this.GetString("sNoteIRSafeHarborLowestCost");

                    dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep = this.GetString("sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures");
                    dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep = this.GetString("sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures");
                    dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep = this.GetString("sTotalDiscountPointOriginationFeeSafeHarborLowestCost");
                    break;
                default: throw new UnhandledEnumException(dataloan.sFinMethT);
            }
        }

        /// <summary>
        /// Loads data for the page from the specified loan and application.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan to load data.
        /// </param>
        /// <param name="dataApp">
        /// This parameter is not used.
        /// </param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult(nameof(dataLoan.sAppSubmittedD), dataLoan.sAppSubmittedD_rep);
            this.SetResult(nameof(dataLoan.sAppSubmittedDLckd), dataLoan.sAppSubmittedDLckd);
            this.SetResult("sIntentToProceedD", dataLoan.sIntentToProceedD_rep);
            this.SetResult(nameof(dataLoan.sEstCloseD), dataLoan.sEstCloseD_rep);
            this.SetResult(nameof(dataLoan.sEstCloseDLckd), dataLoan.sEstCloseDLckd);
        }

        private void LoadAntiSteeringData(CPageData dataloan)
        {
            switch (dataloan.sFinMethT)
            {
                case E_sFinMethT.ARM:
                    this.SetResult("sNoteIRSafeHarborWithRiskyFeatures)", dataloan.sNoteIRSafeHarborARMWithRiskyFeatures_rep);
                    this.SetResult("sNoteIRSafeHarborWithoutRiskyFeatures)", dataloan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep);
                    this.SetResult("sNoteIRSafeHarborLowestCost)", dataloan.sNoteIRSafeHarborARMLowestCost_rep);

                    this.SetResult("sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures)", dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep);
                    this.SetResult("sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures)", dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep);
                    this.SetResult("sTotalDiscountPointOriginationFeeSafeHarborLowestCost)", dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep);
                    break;
                case E_sFinMethT.Fixed:
                case E_sFinMethT.Graduated:                    
                    this.SetResult("sNoteIRSafeHarborWithRiskyFeatures)", dataloan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep);
                    this.SetResult("sNoteIRSafeHarborWithoutRiskyFeatures)", dataloan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep);
                    this.SetResult("sNoteIRSafeHarborLowestCost)", dataloan.sNoteIRSafeHarborFixedLowestCost_rep);

                    this.SetResult("sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures)", dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep);
                    this.SetResult("sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures)", dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep);
                    this.SetResult("sTotalDiscountPointOriginationFeeSafeHarborLowestCost)", dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep);
                    break;
                default: throw new UnhandledEnumException(dataloan.sFinMethT);
            }
        }

        /// <summary>
        /// Refreshes the data for the page.
        /// </summary>
        private void RefreshCalculation()
        {
            try
            {
                this.GetDisclosureViewModel(bindData: true);
            }
            catch (FieldInvalidValueException exc)
            {
                this.SetResult(nameof(exc.UserMessage), exc.UserMessage);
                this.SetResult(nameof(exc.IsWorkflowError), exc.IsWorkflowError);
            }
        }

        private void SaveAntiSteeringDisclosure()
        {
            try
            {
                CPageData dataLoan = ConstructPageDataClass(sLId);
                dataLoan.InitSave(sFileVersion);

                BindAntiSteeringData(dataLoan);

                dataLoan.Save();

                SetResult("sFileVersion", dataLoan.sFileVersion);
                LoadAntiSteeringData(dataLoan);

                AfterSaveAndLoadPageDataCallback(dataLoan);
            }
            catch (CBaseException exc)
            {
                this.SetResult(nameof(exc.UserMessage), exc.UserMessage);
            }
        }

        /// <summary>
        /// Saves the data entered by the user for the "Important Loan Dates" section.
        /// </summary>
        /// <remarks>
        /// This method is designed to support displaying the error message to the user 
        /// as a modal instead of forcing a page redirect, similar to the saving mechanism
        /// used by the "Application Information" page.
        /// </remarks>
        private void SaveLoanDates()
        {
            try
            {
                this.SaveData();
            }
            catch (CBaseException exc)
            {
                this.SetResult(nameof(exc.UserMessage), exc.UserMessage);
                this.SetResult(nameof(exc.IsWorkflowError), exc.IsWorkflowError);
            }
        }

        /// <summary>
        /// Adds a request to the loan file for the lender to complete the order.
        /// </summary>
        private void RequestLenderToCompleteOrder()
        {
            try
            {
                var dataloan = this.ConstructPageDataClass(this.sLId);
                dataloan.ReportAllWorkflowFailureMessages = true;
                dataloan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (!this.CanOrderInitialDisclosures(dataloan))
                {
                    throw new InvalidOperationException(ErrorMessages.TpoDocumentOrder.InitialDisclosure.InitialDisclosureAlreadyExists);
                }

                var source = this.GetString("Source");
                var blockReason = this.GetString("BlockReason", defaultValue: "Other");
                var supplementalData = this.GetString("SupplementalData", defaultValue: null);
                var notes = this.GetString("Notes");

                var newEventSettings = new LoanEventAdditionSettings<TpoRequestForInitialDisclosureGenerationEventType>
                {
                    EventType = TpoRequestForInitialDisclosureGenerationEventType.Requested,
                    Notes = notes,
                    Source = source,
                    CreationReason = blockReason,
                    SupplementalData = supplementalData
                };

                dataloan.sTpoRequestForInitialDisclosureGenerationEventCollection.AddEvent(
                    this.CurrentPrincipal,
                    newEventSettings);

                dataloan.Save();

                this.SetResult("HasError", false);
            }
            catch (InvalidOperationException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", true);
                this.SetResult("UserMessage", exc.Message);
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", false);
                this.SetResult(nameof(exc.UserMessage), exc.UserMessage);
                this.SetResult(nameof(exc.IsWorkflowError), exc.IsWorkflowError);
            }
        }

        /// <summary>
        /// Cancels a request for the lender to complete a disclosure order.
        /// </summary>
        private void CancelDisclosureRequest()
        {
            try
            {
                var dataloan = this.ConstructPageDataClass(this.sLId);
                dataloan.InitSave(ConstAppDavid.SkipVersionCheck);

                var type = this.GetString("Type");
                var notes = this.GetString("Notes");
                switch (type)
                {
                    case "Initial Disclosure Request":
                        var initialDisclosureSettings = new LoanEventAdditionSettings<TpoRequestForInitialDisclosureGenerationEventType>
                        {
                            EventType = TpoRequestForInitialDisclosureGenerationEventType.Cancelled,
                            Notes = notes
                        };

                        dataloan.sTpoRequestForInitialDisclosureGenerationEventCollection.AddEvent(
                            this.CurrentPrincipal, 
                            initialDisclosureSettings);
                        break;

                    case "Redisclosure Request":
                        var id = this.GetGuid("Id");
                        var redisclosureSettings = new LoanEventAdditionSettings<TpoRequestForRedisclosureGenerationEventType>
                        {
                            EventType = TpoRequestForRedisclosureGenerationEventType.Cancelled,
                            Notes = notes
                        };

                        dataloan.sTpoRequestForRedisclosureGenerationEventCollection
                            .FirstOrDefault(collection => collection.HasEvent(id))
                            ?.AddEvent(this.CurrentPrincipal, redisclosureSettings);
                        break;

                    case "Initial Closing Disclosure Request":
                        var initialClosingDisclosureSettings = new LoanEventAdditionSettings<TpoRequestForInitialClosingDisclosureGenerationEventType>
                        {
                            EventType = TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled,
                            Notes = notes
                        };

                        dataloan.sTpoRequestForInitialClosingDisclosureGenerationEventCollection.AddEvent(
                            this.CurrentPrincipal, 
                            initialClosingDisclosureSettings);
                        break;
                    default:
                        throw new GenericUserErrorMessageException("Unhandled disclosure request cancellation type " + type);
                }

                dataloan.Save();
                this.SetResult("HasError", false);
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult(nameof(exc.UserMessage), exc.UserMessage);
                this.SetResult(nameof(exc.IsWorkflowError), exc.IsWorkflowError);
            }
        }

        /// <summary>
        /// Obtains the view model for the specified <paramref name="dataloan"/>.
        /// </summary>
        /// <param name="bindData">
        /// True if data from the page should be bound to the loan first,
        /// false otherwise.
        /// </param>
        private void GetDisclosureViewModel(bool bindData)
        {
            var dataloan = this.ConstructPageDataClass(this.sLId);
            dataloan.InitLoad();

            if (bindData)
            {
                this.BindData(dataloan, dataApp: null);
            }

            Dictionary<Guid, ClosingCostArchive> loanArchives = null;
            IEnumerable<MembershipGroup> membershipGroups = null;
            bool canPerformRedisclosure = false, canPerformInitialClosingDisclosure = false;

            if (dataloan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                loanArchives = dataloan.sClosingCostArchive.ToDictionary(archive => archive.Id);

                if (string.Equals("P", this.CurrentPrincipal.Type, StringComparison.OrdinalIgnoreCase))
                {
                    membershipGroups = from groupDb in GroupDB.GetGroupMembershipsForPmlBroker(this.CurrentPrincipal.BrokerId, this.CurrentPrincipal.PmlBrokerId)
                                       where groupDb.IsInGroup == 1
                                       select groupDb;
                }
                else
                {
                    membershipGroups = from groupDb in GroupDB.GetGroupMembershipsForBranch(this.CurrentPrincipal.BrokerId, this.CurrentPrincipal.BranchId)
                                       where groupDb.IsInGroup == 1
                                       select groupDb;
                }

                var disclosureWorkflowPrivileges = this.GetDisclosureOperationWorkflowMappings();
                canPerformRedisclosure = disclosureWorkflowPrivileges[WorkflowOperations.RequestRedisclosureInTpoPortal];
                canPerformInitialClosingDisclosure = disclosureWorkflowPrivileges[WorkflowOperations.RequestInitialClosingDisclosureInTpoPortal];
            }

            var viewModel = new
            {
                sLId = dataloan.sLId,
                sDisclosureRegulationT = dataloan.sDisclosureRegulationT,
                orderInitialDisclosuresButtonModel = this.GetOrderInitialDisclosureButtonModel(dataloan, loanArchives),
                requestRedisclosureButtonModel = this.GetRequestRedisclosureButtonModel(dataloan, canPerformRedisclosure, loanArchives, membershipGroups),
                requestInitialClosingDisclosureButtonModel = this.GetRequestInitialClosingDisclosureButtonModel(dataloan, canPerformInitialClosingDisclosure, loanArchives, membershipGroups),
                lenderCompletionRequests = this.CreateLenderDisclosureCompletionRequests(dataloan),
                accountExecutiveData = this.CreateAccountExecutiveData(dataloan),
                importantDateSection = this.CreateImportantDatesModel(dataloan),
                deadlineSection = this.CreateDeadlineSection(dataloan),
                loanEstimateSection = this.CreateLoanEstimateSection(dataloan, loanArchives),
                closingDisclosureSection = this.CreateClosingDisclosureSection(dataloan, loanArchives),
                activityLogSection = this.CreateActivityLogSection(dataloan),
                constants = this.CreateConstants(dataloan)
            };

            this.SetResult("DisclosuresViewModel", SerializationHelper.JsonNetAnonymousSerialize(viewModel));
        }

        /// <summary>
        /// Creates a model for the "Order Initial Disclosures" button on the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing relevant data.
        /// </param>
        /// <param name="closingCostArchives">
        /// A dictionary containing archive id to closing cost archives 
        /// or null if the loan's regulation type is not TRID.
        /// </param>
        /// <returns>
        /// A complex type containing the model.
        /// </returns>
        private object GetOrderInitialDisclosureButtonModel(CPageData dataloan, Dictionary<Guid, ClosingCostArchive> closingCostArchives)
        {
            if (!this.CurrentPrincipal.EnableOrderInitialDisclosuresButtonOnTpoDisclosuresPage)
            {
                return new { Hidden = true, Enabled = false };
            }

            return new
            {
                Hidden = false,
                Enabled = this.CanOrderInitialDisclosures(dataloan, closingCostArchives)
            };
        }

        /// <summary>
        /// Creates a model for the "Request CoC / Redisclosure" button on the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing relevant data.
        /// </param>
        /// <param name="hasRedisclosureWorkflowPrivilege">
        /// True if the user has redisclosure workflow privilege, false otherwise.
        /// </param>
        /// <param name="loanArchives">
        /// A dictionary containing archive id to closing cost archives 
        /// or null if the loan's regulation type is not TRID.
        /// </param>
        /// <param name="membershipGroups">
        /// The groups that the user's originating company or branch belongs to or null
        /// if the loan's regulation type is not TRID.
        /// </param>
        /// <returns>
        /// A complex type containing the model.
        /// </returns>
        private object GetRequestRedisclosureButtonModel(
            CPageData dataloan, 
            bool hasRedisclosureWorkflowPrivilege, 
            Dictionary<Guid, ClosingCostArchive> loanArchives, 
            IEnumerable<MembershipGroup> membershipGroups)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID ||
                !Tools.IsTpoDisclosureEnabled(this.CurrentPrincipal.BrokerDB, membershipGroups, TpoDisclosureType.Redisclosure))
            {
                return new { Hidden = true, Enabled = false };
            }

            if (!hasRedisclosureWorkflowPrivilege)
            {
                return new
                {
                    Hidden = false,
                    Enabled = false,
                    Tooltip = "You do not currently have permission to perform this operation on this file."
                };
            }

            var hasInitialLoanEstimate = dataloan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any(date =>
                date.IsInitial && (date.ArchiveId == Guid.Empty || loanArchives[date.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid));
            if (!hasInitialLoanEstimate)
            {
                return new
                {
                    Hidden = false,
                    Enabled = false,
                    Tooltip = "Redisclosure cannot be requested until there is an Initial Loan Estimate on file."
                };
            }

            var hasValidClosingDisclosure = dataloan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any(date =>
                date.ArchiveId == Guid.Empty || loanArchives[date.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid);
            if (hasValidClosingDisclosure)
            {
                return new
                {
                    Hidden = false,
                    Enabled = false,
                    Tooltip = "Redisclosure cannot be requested once there is a Closing Disclosure on file."
                };
            }

            return new { Hidden = false, Enabled = true, };
        }

        /// <summary>
        /// Creates a model for the "Request Initial Closing Disclosure" button on the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing relevant data.
        /// </param>
        /// <param name="hasInitialClosingDisclosureWorkflowPrivilege">
        /// True if the user has initial closing disclosure workflow privilege, false otherwise.
        /// </param>
        /// <param name="loanArchives">
        /// A dictionary containing archive id to closing cost archives 
        /// or null if the loan's regulation type is not TRID.
        /// </param>
        /// <param name="membershipGroup">
        /// The groups that the user's originating company or branch belongs to or null
        /// if the loan's regulation type is not TRID.
        /// </param>
        /// <returns>
        /// A complex type containing the model.
        /// </returns>
        private object GetRequestInitialClosingDisclosureButtonModel(
            CPageData dataloan, 
            bool hasInitialClosingDisclosureWorkflowPrivilege,
            Dictionary<Guid, ClosingCostArchive> loanArchives, 
            IEnumerable<MembershipGroup> membershipGroup)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID ||
                !Tools.IsTpoDisclosureEnabled(this.CurrentPrincipal.BrokerDB, membershipGroup, TpoDisclosureType.InitialClosingDisclosure))
            {
                return new { Hidden = true, Enabled = false };
            }

            if (!hasInitialClosingDisclosureWorkflowPrivilege)
            {
                return new
                {
                    Hidden = false,
                    Enabled = false,
                    Tooltip = "You do not currently have permission to perform this operation on this file."
                };
            }

            var latestStatus = dataloan.sTpoRequestForInitialClosingDisclosureGenerationStatusT;
            if (latestStatus == TpoRequestForInitialClosingDisclosureGenerationEventType.Active ||
                latestStatus == TpoRequestForInitialClosingDisclosureGenerationEventType.Completed)
            {
                return new
                {
                    Hidden = false,
                    Enabled = false
                };
            }

            var hasInitialLoanEstimate = dataloan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any(date =>
                date.IsInitial && (date.ArchiveId == Guid.Empty || loanArchives[date.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid));
            if (!hasInitialLoanEstimate)
            {
                return new
                {
                    Hidden = false,
                    Enabled = false,
                    Tooltip = "Initial Closing Disclosure cannot be requested until there is an Initial Loan Estimate on file."
                };
            }

            var hasValidClosingDisclosure = dataloan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any(date =>
                date.ArchiveId == Guid.Empty || loanArchives[date.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid);
            if (hasValidClosingDisclosure)
            {
                return new
                {
                    Hidden = false,
                    Enabled = false,
                    Tooltip = "There is already a Closing Disclosure on this file."
                };
            }

            return new { Hidden = false, Enabled = true, };
        }

        /// <summary>
        /// Gets a mapping between disclosure workflow operations and whether
        /// the user can perform the operation.
        /// </summary>
        /// <returns>
        /// The operation mapping.
        /// </returns>
        private Dictionary<WorkflowOperation, bool> GetDisclosureOperationWorkflowMappings()
        {
            var operations = new[]
            {
                WorkflowOperations.RequestRedisclosureInTpoPortal,
                WorkflowOperations.RequestInitialClosingDisclosureInTpoPortal
            };

            var evaluator = new LoanValueEvaluator(this.CurrentPrincipal.ConnectionInfo, this.CurrentPrincipal.BrokerId, this.sLId, operations);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.CurrentPrincipal));

            var mappings = new Dictionary<WorkflowOperation, bool>(operations.Length);
            foreach (var operation in operations)
            {
                mappings.Add(operation, LendingQBExecutingEngine.CanPerform(operation, evaluator));
            }

            return mappings;
        }

        /// <summary>
        /// Creates the model for the status of any requests for the lender
        /// to complete the initial disclosure order.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to load data.
        /// </param>
        /// <returns>
        /// A list of complex objects containing the disclosure requests.
        /// </returns>
        private List<object> CreateLenderDisclosureCompletionRequests(CPageData dataloan)
        {
            var sectionList = new List<object>();

            var mostRecentInitialDisclosureGenerationRequestStatus = dataloan.sTpoRequestForInitialDisclosureGenerationStatusT;
            if (mostRecentInitialDisclosureGenerationRequestStatus != TpoRequestForInitialDisclosureGenerationEventType.NoRequest)
            {
                sectionList.Add(new
                {
                    Type = "Initial Disclosure Request",
                    Status = dataloan.sTpoRequestForInitialDisclosureGenerationStatusT_rep,
                    EnableActionButton = mostRecentInitialDisclosureGenerationRequestStatus == TpoRequestForInitialDisclosureGenerationEventType.Requested,
                    Timestamp = dataloan.sTpoRequestForInitialDisclosureGenerationStatusD_rep,
                    Notes = dataloan.sTpoRequestForInitialDisclosureGenerationStatusNotes
                });
            }

            var redisclosureRequests = dataloan.sTpoRequestForRedisclosureGenerationEventCollection
                .Where(collection => collection.LatestEventStatus != TpoRequestForRedisclosureGenerationEventType.NoRequest)
                .Select(collection => new
                {
                    Id = collection.LatestEventId,
                    Type = "Redisclosure Request",
                    Status = collection.LatestEventStatusRep,
                    EnableActionButton = collection.LatestEventStatus == TpoRequestForRedisclosureGenerationEventType.Active,
                    Timestamp = collection.LatestEventDatetimeRep,
                    Notes = collection.LatestEventNotes
                });

            sectionList.AddRange(redisclosureRequests);

            var mostRecentInitialClosingDisclosureGenerationRequestStatus = dataloan.sTpoRequestForInitialClosingDisclosureGenerationStatusT;
            if (mostRecentInitialClosingDisclosureGenerationRequestStatus != TpoRequestForInitialClosingDisclosureGenerationEventType.NoRequest)
            {
                sectionList.Add(new
                {
                    Type = "Initial Closing Disclosure Request",
                    Status = dataloan.sTpoRequestForInitialClosingDisclosureGenerationStatusT_rep,
                    EnableActionButton = mostRecentInitialClosingDisclosureGenerationRequestStatus == TpoRequestForInitialClosingDisclosureGenerationEventType.Active,
                    Timestamp = dataloan.sTpoRequestForInitialClosingDisclosureGenerationStatusD_rep,
                    Notes = dataloan.sTpoRequestForInitialClosingDisclosureGenerationStatusNotes
                });
            }

            return sectionList;
        }

        /// <summary>
        /// Creates the model for the account executive link for non-TRID loans.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing account executive data.
        /// </param>
        /// <returns>
        /// A complex type containing the model.
        /// </returns>
        private object CreateAccountExecutiveData(CPageData dataloan)
        {
            if (dataloan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                return null;
            }

            return new
            {
                IsValid = dataloan.sEmployeeLenderAccExec.IsValid,
                Name = dataloan.sEmployeeLenderAccExec.FullName,
                Email = dataloan.sEmployeeLenderAccExec.Email,
                Phone = dataloan.sEmployeeLenderAccExec.Phone
            };
        }

        /// <summary>
        /// Creates the model for the "Important Dates" section.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing disclosure data for the section.
        /// </param>
        /// <returns>
        /// A complex type containing the model.
        /// </returns>
        private object CreateImportantDatesModel(CPageData dataloan)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                return null;
            }

            return new
            {
                sAppSubmittedD = dataloan.sAppSubmittedD_rep,
                sAppSubmittedDLckd = dataloan.sAppSubmittedDLckd,
                sSubmitD = dataloan.sSubmitD_rep,
                sUseInitialLeSignedDateAsIntentToProceedDate = dataloan.sUseInitialLeSignedDateAsIntentToProceedDate,
                sIntentToProceedD = dataloan.sIntentToProceedD_rep,
                sEstCloseD = dataloan.sEstCloseD_rep,
                sEstCloseDLckd = dataloan.sEstCloseDLckd
            };
        }

        private object CreateAntiSteeringDisclosureModel(CPageData dataloan)
        {
            switch (dataloan.sFinMethT)
            {
                case E_sFinMethT.ARM:
                    return new
                    {
                        sNoteIRSafeHarborWithRiskyFeatures = dataloan.sNoteIRSafeHarborARMWithRiskyFeatures_rep,
                        sNoteIRSafeHarborWithoutRiskyFeatures = dataloan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep,
                        sNoteIRSafeHarborLowestCost = dataloan.sNoteIRSafeHarborARMLowestCost_rep,

                        sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures = dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep,
                        sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures = dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep,
                        sTotalDiscountPointOriginationFeeSafeHarborLowestCost = dataloan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep,

                        sNoteIR = dataloan.sNoteIR_rep,
                        sSumOfFees = dataloan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID ? dataloan.sSumOfTridAFees_rep : dataloan.sSumOfGEFA1FeesAndDiscountPoints_rep
                    };
                case E_sFinMethT.Fixed:
                case E_sFinMethT.Graduated:
                    return new
                    {
                        sNoteIRSafeHarborWithRiskyFeatures = dataloan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep,
                        sNoteIRSafeHarborWithoutRiskyFeatures = dataloan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep,
                        sNoteIRSafeHarborLowestCost = dataloan.sNoteIRSafeHarborFixedLowestCost_rep,

                        sTotalDiscountPointOriginationFeeSafeHarborWithRiskyFeatures = dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep,
                        sTotalDiscountPointOriginationFeeSafeHarborWithoutRiskyFeatures = dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep,
                        sTotalDiscountPointOriginationFeeSafeHarborLowestCost = dataloan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep,

                        sNoteIR = dataloan.sNoteIR_rep,
                        sSumOfFees = dataloan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID ? dataloan.sSumOfTridAFees_rep : dataloan.sSumOfGEFA1FeesAndDiscountPoints_rep
                    };
                default: throw new UnhandledEnumException(dataloan.sFinMethT);
            }
        }

        /// <summary>
        /// Creates the model for the "Deadlines" section.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing disclosure data for the section.
        /// </param>
        /// <returns>
        /// A complex type containing the model.
        /// </returns>
        private object CreateDeadlineSection(CPageData dataloan)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                return null;
            }

            return new
            {
                sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD = dataloan.sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD_rep,
                sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD = dataloan.sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD_rep,
                sInitialClosingDisclosureMailedDeadlineD = dataloan.sInitialClosingDisclosureMailedDeadlineD_rep,
                sInitialClosingDisclosureReceivedDeadlineD = dataloan.sInitialClosingDisclosureReceivedDeadlineD_rep
            };
        }

        /// <summary>
        /// Creates the model for the "Loan Estimate" section.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing disclosure data for the section.
        /// </param>
        /// <param name="closingCostArchives">
        /// A dictionary containing archive id to closing cost archives 
        /// or null if the loan's regulation type is not TRID.
        /// </param>
        /// <returns>
        /// A list of complex types containing the loan estimate data.
        /// </returns>
        private IEnumerable<object> CreateLoanEstimateSection(CPageData dataloan, Dictionary<Guid, ClosingCostArchive> closingCostArchives)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                return null;
            }

            return from date in dataloan.sLoanEstimateDatesInfo.LoanEstimateDatesList
                   where date.ArchiveId == Guid.Empty || closingCostArchives[date.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid
                   orderby date.IssuedDate.Date
                   select new
                   {
                       IssuedDate = date.IssuedDate == DateTime.MinValue ? string.Empty : date.IssuedDate.ToShortDateString(),
                       DeliveryMethod = date.DeliveryMethod_rep,
                       ReceivedDate = date.ReceivedDate == DateTime.MinValue ? string.Empty : date.ReceivedDate.ToShortDateString(),
                       IsInitial = date.IsInitial
                   };
        }

        /// <summary>
        /// Creates the model for the "Closing Disclosure" section.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing disclosure data for the section.
        /// </param>
        /// <param name="closingCostArchives">
        /// A dictionary containing archive id to closing cost archives 
        /// or null if the loan's regulation type is not TRID.
        /// </param>
        /// <returns>
        /// A list of complex types containing the closing disclosure data.
        /// </returns>
        private IEnumerable<object> CreateClosingDisclosureSection(CPageData dataloan, Dictionary<Guid, ClosingCostArchive> closingCostArchives)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                return null;
            }

            return from date in dataloan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                   where date.ArchiveId == Guid.Empty || closingCostArchives[date.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid
                   orderby date.IssuedDate.Date
                   select new
                   {
                       IssuedDate = date.IssuedDate == DateTime.MinValue ? string.Empty : date.IssuedDate.ToShortDateString(),
                       DeliveryMethod = date.DeliveryMethod_rep,
                       ReceivedDate = date.ReceivedDate == DateTime.MinValue ? string.Empty : date.ReceivedDate.ToShortDateString(),
                       IsPreview = date.IsPreview,
                       IsInitial = date.IsInitial,
                       IsFinal = date.IsFinal,
                       IsPostClosing = date.IsPostClosing
                   };
        }

        /// <summary>
        /// Creates the model for the "Activity Log" section.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing disclosure data for the section.
        /// </param>
        /// <returns>
        /// A list of complex types containing the activity log data.
        /// </returns>
        /// <remarks>
        /// The version of Angular used for the "Disclosures" page does not
        /// yet support custom comparators. To allow the "Activity Log" section
        /// to be sorted by the user, we include both the <seealso cref="DateTime"/>
        /// and string versions of the created date, with the former used by the
        /// default comparator and the latter used to display on the page.
        /// </remarks>
        private IEnumerable<object> CreateActivityLogSection(CPageData dataloan)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                return null;
            }

            return from auditItem in AuditManager.RetrieveAuditList(dataloan.sLId)
                   where auditItem.CategoryT == E_AuditItemCategoryT.DisclosureESign
                   select new
                   {
                       Id = auditItem.ID,
                       Time = auditItem.Timestamp,
                       TimestampDescription = auditItem.TimestampDescription,
                       User = auditItem.UserName,
                       Description = auditItem.Description
                   };
        }

        /// <summary>
        /// Creates the model for page constants.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing constants data for the section.
        /// </param>
        /// <returns>
        /// A complex type containing the constants.
        /// </returns>
        private object CreateConstants(CPageData dataloan)
        {
            return new
            {
                TRID2015 = E_sDisclosureRegulationT.TRID
            };
        }

        private void LoadAntiSteeringDisclosure()
        {
            try
            {
                var dataloan = CPageData.CreateUsingSmartDependency(this.sLId, typeof(DisclosuresServiceItem));
                dataloan.InitLoad();

                if (!this.CanOrderInitialDisclosures(dataloan))
                {
                    throw new InvalidOperationException(ErrorMessages.TpoDocumentOrder.InitialDisclosure.InitialDisclosureAlreadyExists);
                }

                var viewModel = new
                {
                    antiSteeringData = this.CreateAntiSteeringDisclosureModel(dataloan)
                };

                this.SetResult("HasError", false);
                this.SetResult("LoadAntiSteeringDisclosureViewModel", SerializationHelper.JsonNetAnonymousSerialize(viewModel));
            }
            catch (InvalidOperationException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", true);
                this.SetResult("ErrorMessage", exc.Message);
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", false);
                this.SetResult("ErrorMessage", ErrorMessages.TpoDocumentOrder.InitialDisclosure.FailedToRetrieveAntiSteeringDisclosure());
            }
        }

        /// <summary>
        /// Loads data for reviewing the request order.
        /// </summary>
        private void LoadRequestReview()
        {
            try
            {
                var dataloan = CPageData.CreateUsingSmartDependency(this.sLId, typeof(DisclosuresServiceItem));
                dataloan.InitLoad();

                if (!this.CanOrderInitialDisclosures(dataloan))
                {
                    throw new InvalidOperationException(ErrorMessages.TpoDocumentOrder.InitialDisclosure.InitialDisclosureAlreadyExists);
                }

                this.UpdateLoanHomeownerCounselingOrganizations(dataloan.GetAppData(0).aBZip, dataloan.sHomeownerCounselingOrganizationCollection);

                var viewModel = new
                {
                    checklist = this.GetRequestCheckList(),
                    borrowers = this.GetBorrowers(dataloan),
                    loanOfficer = this.GetLoanOfficerData(dataloan)
                };

                this.SetResult("HasError", false);
                this.SetResult("RequestReviewViewModel", SerializationHelper.JsonNetAnonymousSerialize(viewModel));
            }
            catch (InvalidOperationException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", true);
                this.SetResult("ErrorMessage", exc.Message);
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", false);
                this.SetResult("ErrorMessage", ErrorMessages.TpoDocumentOrder.InitialDisclosure.FailedToGenerateRequestReview());
                this.SetResult(nameof(exc.IsWorkflowError), exc.IsWorkflowError);
            }
        }

        /// <summary>
        /// Obtains the checklist of requirements that must all pass for
        /// the request to be made.
        /// </summary>
        /// <returns>
        /// A <seealso cref="WorkflowResultChecklist"/> containing the 
        /// checklist and an indicator for whether there are any failures.
        /// </returns>
        private WorkflowResultChecklist GetRequestCheckList()
        {
            var checklistGenerator = new WorkflowResultChecklistGenerator(this.CurrentPrincipal, WorkflowOperations.OrderInitialDisclosureInTpoPortal, this.sLId);
            return checklistGenerator.Generate();
        }

        /// <summary>
        /// Obtains the borrower information for the request.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to load data.
        /// </param>
        /// <returns>
        /// An <see cref="IEnumerable{T}"/> of complex types 
        /// containing the borrower data.
        /// </returns>
        private IEnumerable<object> GetBorrowers(CPageData dataloan)
        {
            var borrowerList = new List<object>(dataloan.nApps);

            for (var i = 0; i < dataloan.nApps; i++)
            {
                var app = dataloan.GetAppData(i);

                borrowerList.Add(new { Name = app.aBNm, Email = app.aBEmail });

                if (app.aBHasSpouse)
                {
                    borrowerList.Add(new { Name = app.aCNm, Email = app.aCEmail });
                }
            }

            return borrowerList;
        }

        /// <summary>
        /// Obtains the loan officer information for the request.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to load data.
        /// </param>
        /// <returns>
        /// A complex type containing the loan officer data.
        /// </returns>
        private object GetLoanOfficerData(CPageData dataloan)
        {
            var loanOfficer = dataloan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            return new
            {
                Name = loanOfficer.AgentName,
                Nmls = loanOfficer.LoanOriginatorIdentifier,
                LicenseId = loanOfficer.LicenseNumOfAgent,
                Email = loanOfficer.EmailAddr,
                Phone = loanOfficer.Phone
            };
        }

        /// <summary>
        /// Updates the loan's list of Homeowner Counseling Organizations if the
        /// collection is out of date.
        /// </summary>
        /// <param name="zipCode">
        /// The ZIP code for the HCO collection.
        /// </param>
        /// <param name="existingCollection">
        /// The existing HCO collection for the loan file.
        /// </param>
        private void UpdateLoanHomeownerCounselingOrganizations(string zipCode, HomeownerCounselingOrganizationCollection existingCollection)
        {
            try
            {
                string cfpbData;
                string errors;
                if (!CounselingOrganizationUtilities.FetchDataFromCFPB(zipCode, out errors, out cfpbData))
                {
                    Tools.LogWarning("CFPB responded with invalid data: " + cfpbData + Environment.NewLine + "Errors: " + errors);
                    return;
                }

                var cfpbAgencyList = CounselingOrganizationUtilities.CreateAgencyListFromJson(cfpbData);

                var organizationCollection = new HomeownerCounselingOrganizationCollection();
                cfpbAgencyList.CounselingAgencies.ForEach(agencyData => organizationCollection.Add(CounselingOrganizationUtilities.CreateHomeownerCounselingOrgFromCfpbData(agencyData)));

                if (existingCollection.Equals(organizationCollection, ignoreId: true))
                {
                    Tools.LogInfo("CFPB responded with HCOs identical to those already on the loan file, skipping save. Received data: " + cfpbData);
                    return;
                }

                // Workflow read access will have been checked when loading the initial loan, so we
                // can safely use a security bypass to perform the automation.
                var fields = new[] { nameof(CPageData.sHomeownerCounselingOrganizationCollection), nameof(CPageData.sHomeownerCounselingOrganizationLastModifiedD) };
                var automationLoan = new CFullAccessPageData(this.sLId, fields);
                automationLoan.ByPassFieldSecurityCheck = true;
                automationLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                automationLoan.sHomeownerCounselingOrganizationCollection = organizationCollection;
                automationLoan.sHomeownerCounselingOrganizationLastModifiedD_rep = DateTime.Today.ToString("MM/dd/yyyy");

                automationLoan.Save();
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                this.SetResult(nameof(exc.IsWorkflowError), exc.IsWorkflowError);
            }
        }

        /// <summary>
        /// Loads document vendor audit data for the loan.
        /// </summary>
        private void LoadDocumentAudit()
        {
            DocumentVendorResult<Audit> auditResult = null;
            try
            {
                var dataloan = this.ConstructPageDataClass(this.sLId);
                dataloan.InitLoad();

                if (!this.CanOrderInitialDisclosures(dataloan))
                {
                    throw new InvalidOperationException(ErrorMessages.TpoDocumentOrder.InitialDisclosure.InitialDisclosureAlreadyExists);
                }

                auditResult = this.GetAuditResult(dataloan, LendersOffice.Security.PrincipalFactory.CurrentPrincipal);

                this.SetResult("HasError", auditResult.HasError);
                this.SetResult("BlockReason", auditResult.ErrorMessage);
                this.SetResult("ErrorMessage", auditResult.HasError ? ErrorMessages.TpoDocumentOrder.InitialDisclosure.FailedToRetrieveDocumentAudit() : null);
                this.SetResult("HasFatal", auditResult.Result?.Results.Any(auditLine => auditLine.Severity == AuditSeverity.Fatal) ?? false);

                var auditLines = auditResult.Result?.Results.OrderByDescending(auditLine => auditLine.Severity).Select(auditLine => new
                {
                    Severity = auditLine.Severity.ToString(),
                    Message = auditLine.Message
                });

                this.SetResult("DocumentAuditResults", SerializationHelper.JsonNetAnonymousSerialize(auditLines));
            }
            catch (SystemException exc) when (exc is InvalidOperationException || exc is NotSupportedException)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", exc is InvalidOperationException);
                this.SetResult("ErrorMessage", exc.Message);
                this.SetResult("BlockReason", exc is NotSupportedException ? exc.Message : null);
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("ErrorMessage", ErrorMessages.TpoDocumentOrder.InitialDisclosure.FailedToRetrieveDocumentAudit());
                this.SetResult(nameof(exc.IsWorkflowError), exc.IsWorkflowError);
            }
        }

        /// <summary>
        /// Obtains the audit result for the loan.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to load audit data.
        /// </param>
        /// <returns>
        /// The audit result for the loan.
        /// </returns>
        /// <remarks>
        /// The core auditing code may throw a generic <seealso cref="Exception"/>
        /// depending on the vendor's status. To provide the user with a friendly
        /// message, this method will rethrow a naked <seealso cref="Exception"/>
        /// as a <seealso cref="GenericUserErrorMessageException"/>.
        /// </remarks>
        private DocumentVendorResult<Audit> GetAuditResult(CPageData dataloan, AbstractUserPrincipal principal)
        {
            try
            {
                var auditSettings = this.CurrentPrincipal.BrokerDB.TpoInitialDisclosureVendorSettings;

                string planCode;
                if (auditSettings.Vendor.Config.PlatformType == E_DocumentVendor.DocMagic)
                {
                    // The plan code for loans using DocMagic is set using SAE rules run during pricing.
                    planCode = dataloan.sDocMagicPlanCodeNm;
                }
                else
                {
                    var availablePlanCodesResult = auditSettings.Vendor.GetAvailablePlanCodes(this.sLId, principal);
                    var availablePlanCodesDictionary = availablePlanCodesResult.Result;
                    var planCodeList = availablePlanCodesDictionary?.FirstOrDefault().Value;

                    if (availablePlanCodesResult.HasError || availablePlanCodesDictionary.Count != 1 || planCodeList.Count() != 1)
                    {
                        throw new NotSupportedException(ErrorMessages.TpoDocumentOrder.FailedToRetrievePlanCode);
                    }

                    planCode = planCodeList.First().Code;
                }

                var auditor = new DocumentAuditor(auditSettings.Vendor, this.sLId, this.aAppId, enforcePermissions: false);
                return auditor.Audit(auditSettings.Package, planCode, dataloan.sDocMagicTransferToInvestorCode, dataloan.sDocMagicTransferToInvestorNm, LendersOffice.Security.PrincipalFactory.CurrentPrincipal);
            }
            catch (Exception exc) when (exc is CBaseException || exc is NotSupportedException)
            {
                throw;
            }
            catch (Exception exc)
            {
                throw new GenericUserErrorMessageException(exc);
            }
        }

        /// <summary>
        /// Places the preview order with the document vendor and returns
        /// the PDF FileDB key.
        /// </summary>
        /// <param name="isPreview">
        /// True if the order is for preview documents, false otherwise.
        /// </param>
        private void PlaceOrder(bool isPreview)
        {
            try
            {
                var dataloan = this.ConstructPageDataClass(this.sLId);
                dataloan.InitLoad();

                if (!this.CanOrderInitialDisclosures(dataloan))
                {
                    throw new InvalidOperationException(ErrorMessages.TpoDocumentOrder.InitialDisclosure.InitialDisclosureAlreadyExists);
                }

                var orderSettings = this.CurrentPrincipal.BrokerDB.TpoInitialDisclosureVendorSettings;

                var packageInfoResult = orderSettings.Vendor.GetAvailableDocumentPackages(this.sLId, LendersOffice.Security.PrincipalFactory.CurrentPrincipal);
                if (packageInfoResult.HasError)
                {
                    throw new CBaseException(packageInfoResult.ErrorMessage, packageInfoResult.ErrorMessage);
                }

                var documentPackage = packageInfoResult.Result.FirstOrDefault(package => string.Equals(package.Type, orderSettings.Package, StringComparison.OrdinalIgnoreCase));
                if (default(DocumentPackage).Equals(documentPackage))
                {
                    throw new GenericUserErrorMessageException("Could not locate document preview package number " + orderSettings.Package);
                }

                var permResponse = orderSettings.Vendor.GetPermissions(this.sLId, documentPackage.AllowEPortal, documentPackage.DisableESign, documentPackage.AllowEClose, documentPackage.AllowManualFulfillment, orderSettings.Package, LendersOffice.Security.PrincipalFactory.CurrentPrincipal);
                if (permResponse.HasError)
                {
                    var message = "There was an error retrieving your permissions. " + permResponse.ErrorMessage;
                    throw new CBaseException(message, message);
                }

                var documentGenerator = new DocumentGenerator(orderSettings.Vendor, this.sLId, this.aAppId, enforcePermissions: false);

                var generationResult = documentGenerator.Generate(
                    packageTypeId: orderSettings.Package,
                    loanProgramId: dataloan.sDocMagicPlanCodeNm,
                    permissions: permResponse.Result,
                    isEDisclosureRequested: orderSettings.EnableEDisclosure,
                    isECloseRequested: false,
                    isESignRequested: orderSettings.EnableESign,
                    isMersRegistrationEnabled: false,
                    principal: LendersOffice.Security.PrincipalFactory.CurrentPrincipal,
                    isPreviewOrder: isPreview);

                if (generationResult.HasError)
                {
                    throw new CBaseException(generationResult.ErrorMessage, generationResult.ErrorMessage);
                }

                if (!string.IsNullOrWhiteSpace(generationResult.Result.ErrorMessage))
                {
                    throw new CBaseException(generationResult.Result.ErrorMessage, generationResult.Result.ErrorMessage);
                }

                this.SetResult("HasError", false);
                this.SetResult("PdfFileDbKey", generationResult.Result.PdfFileDbKey);
            }
            catch (InvalidOperationException exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                this.SetResult("AbortOrder", true);
                this.SetResult("ErrorMessage", exc.Message);
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                this.SetResult("HasError", true);
                if(exc is CBaseException)
                {
                    var cBaseError = exc as CBaseException;
                    this.SetResult(nameof(cBaseError.IsWorkflowError), cBaseError.IsWorkflowError);
                }
                this.SetResult("BlockReason", exc is CBaseException ? ((CBaseException)exc).UserMessage : ErrorMessages.Generic);
                this.SetResult("AbortOrder", false);
                this.SetResult("ErrorMessage", ErrorMessages.TpoDocumentOrder.InitialDisclosure.FailedToPlaceDocumentOrder(isPreview));
            }
        }

        /// <summary>
        /// Gets a value indicating whether the user can order initial disclosures.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing relevant data.
        /// </param>
        /// <param name="closingCostArchives">
        /// An optional parameter for the dictionary containing archive id to closing cost archives.
        /// If unspecified, the archives will be pulled from the loan.
        /// </param>
        /// <returns>
        /// True if ordering is allowed, false otherwise.
        /// </returns>
        private bool CanOrderInitialDisclosures(CPageData dataloan, Dictionary<Guid, ClosingCostArchive> closingCostArchives = null)
        {
            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                // The user should have been blocked from viewing any part of this page,
                // but check here just in case.
                return false;
            }

            if (closingCostArchives == null)
            {
                closingCostArchives = dataloan.sClosingCostArchive.ToDictionary(archive => archive.Id);
            }

            var mostRecentTpoRequestStatus = dataloan.sTpoRequestForInitialDisclosureGenerationStatusT;
            if (mostRecentTpoRequestStatus == TpoRequestForInitialDisclosureGenerationEventType.Requested ||
                mostRecentTpoRequestStatus == TpoRequestForInitialDisclosureGenerationEventType.Completed)
            {
                return false;
            }

            if (dataloan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any(dateInfo => dateInfo.ArchiveId == Guid.Empty || closingCostArchives[dateInfo.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid))
            {
                return false;
            }

            return !dataloan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any(dateInfo =>
                dateInfo.ArchiveId == Guid.Empty ||
                closingCostArchives[dateInfo.ArchiveId].Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid);
        }
    }

    /// <summary>
    /// Provides a means of loading and saving data for the "Disclosures" page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public partial class DisclosuresService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes the service.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new DisclosuresServiceItem());
        }
    }
}