﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Employment.ascx.cs" Inherits="PriceMyLoan.webapp.Employment" %>

<div class="warp-content" id="employ_tabs">
    <div class="content">
        <div>
            <ul class="nav nav-tabs nav-tabs-panels" id="employ">
                <li><a id="employTab1" runat="server"  href="#employ-1">Borrower</a></li>
                <li><a id="employTab2" runat="server"  href="#employ-2">Co-borrower</a></li>
            </ul>
            <div id="employ-1" class="employment">
                <div id="aBForm">
                    <div class="wrap-borrower">
                        <header class="header">Current Primary Employment</header>
                        <div class="employment-content">
                            <div class="table">
                                <div>
                                    <div>
                                        <label>Self Employed?</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="aBPrimaryIsSelfEmplmt" id="aBPrimaryIsSelfEmplmt1" value="true"/><label for="aBPrimaryIsSelfEmplmt1">Yes</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="aBPrimaryIsSelfEmplmt" id="aBPrimaryIsSelfEmplmt2" value="false" checked/><label for="aBPrimaryIsSelfEmplmt2">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="table">
                                <div>
                                    <div>
                                        <label>Employer Name        </label>
                                        <input id="aBPrimaryEmplrNm" type="text" class="form-control" />
                                    </div>
                                    <div>
                                        <label>Employer Phone Number</label>
                                        <input id="aBPrimaryEmplrBusPhone" type="text" preset="phone" class="form-control" />
                                    </div>
                                </div>

                            </div>
                            <div class="table">
                                <div>
                                    <div><label>Employer Address </label><input                id="aBPrimaryEmplrAddr" type="text" class="form-control"/></div>
                                    <div><label>Employer Zip Code</label><ml:zipcodetextbox    id="aBPrimaryEmplrZip"   runat="server" preset="zipcode" width="50" CssClass="form-control"/></div>
                                    <div><label>Employer City    </label><asp:TextBox          ID="aBPrimaryEmplrCity"  runat="server" CssClass="form-control"/></div>
                                    <div><label>Employer State   </label><ml:StateDropDownList ID="aBPrimaryEmplrState" runat="server" /></div>
                                </div>
                            </div>
                            <div class="table margin-bottom">
                                <div>
                                    <div><label>Position / Job Title </label><input          id="aBPrimaryJobTitle" type="text" class="form-control"   /></div>
                                    <div><label>Job Start Date       </label><ml:DateTextBox ID="aBPrimaryEmpltStartD" runat="server" CssClass="mask" Width="75" preset="date" IsDisplayCalendarHelper="false"/></div>
                                    <div><label>Profession Start Date</label><ml:DateTextBox ID="aBPrimaryProfStartD"  runat="server" CssClass="mask" Width="75" preset="date" IsDisplayCalendarHelper="false"/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="aB_prev"></div>
                    <p><a onclick="addEmployment(true)" class="addEmployment">+ add previous employment position</a></p>
                    <div>
                        <div class="bottomButtons">
                            <button type="button" id="aB_btnCancel" class="btn btn-flat btn-next" onclick="onCancel('EmploymentDialog');">Cancel</button>
                            <button type="button" id="ab_btnSave"   class="btn btn-flat"          onclick="onSave('EmploymentDialog');"  >Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="employ-2" class="employment">
                <div id="aCForm">
                    <div class="wrap-borrower">
                        <header class="header">Current Primary Employment</header>
                        <div class="employment-content">
                            <div class="table">
                                <div>
                                    <div>
                                        <label>Self Employed?</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="aCPrimaryIsSelfEmplmt" id="aCPrimaryIsSelfEmplmt1" value="true"/><label for="aCPrimaryIsSelfEmplmt1">Yes</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="aCPrimaryIsSelfEmplmt" id="aCPrimaryIsSelfEmplmt2" value="false" checked/><label for="aCPrimaryIsSelfEmplmt2">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="table">
                                <div>
                                    <div><label>Employer Name        </label><input id="aCPrimaryEmplrNm" type="text" class="form-control"   /></div>
                                    <div><label>Employer Phone Number</label><input id="aCPrimaryEmplrBusPhone" type="text" preset="phone" class="form-control"   /></div>
                                </div>
                            </div>
                            <div class="table">
                                <div>
                                    <div><label>Employer Address </label><input                id="aCPrimaryEmplrAddr" type="text" class="form-control" /></div>
                                    <div><label>Employer Zip Code</label><ml:zipcodetextbox    id="aCPrimaryEmplrZip"   runat="server" preset="zipcode" width="50" CssClass="form-control"/></div>
                                    <div><label>Employer City    </label><asp:Textbox          id="aCPrimaryEmplrCity"  runat="server" class="form-control" /></div>
                                    <div><label>Employer State   </label><ml:StateDropDownList ID="aCPrimaryEmplrState" runat="server" /></div>
                                </div>
                            </div>
                            <div class="table margin-bottom">
                                <div>
                                    <div><label>Position / Job Title </label><input          id="aCPrimaryJobTitle" type="text" class="form-control"   /></div>
                                    <div><label>Job Start Date       </label><ml:DateTextBox ID="aCPrimaryEmpltStartD" runat="server" CssClass="mask" Width="75" preset="date" IsDisplayCalendarHelper="false"/></div>
                                    <div><label>Profession Start Date</label><ml:DateTextBox ID="aCPrimaryProfStartD"  runat="server" CssClass="mask" Width="75" preset="date" IsDisplayCalendarHelper="false"/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="aC_prev"></div>
                    <p class="pull-left"><a onclick="addEmployment(false)">+ add previous employment position</a></p>
                    <div>
                        <div class="bottomButtons">
                            <button type="button" id="aC_btnCancel" class="btn btn-flat btn-next" onclick="onCancel('EmploymentDialog');">Cancel</button>
                            <button type="button" id="aC_btnSave"   class="btn btn-flat"          onclick="onSave('EmploymentDialog');">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="prev_template" class="employment">
                <div class="form-horizontal">
                    <header class="header table-flex">
                        <div class="employment-intro">Secondary/Prior Employment</div>
                        <div class="text-right"><a onclick="removeEmployment(this);" class="font-default">- remove this employment position</a></div>
                    </header>
                    <input type="hidden" id="RecordID" />
                    <div class="employment-content">
                        <div class="table">
                            <div>
                                <div>
                                    <label>Self Employed?</label>
                                </div>
                                <div>
                                    <input type="radio" name="IsSelfEmplmt" id="IsSelfEmplmt1" value="true"/><label for="IsSelfEmplmt1">Yes</label>
                                </div>
                                <div>
                                    <input type="radio" name="IsSelfEmplmt" id="IsSelfEmplmt2" value="false"/><label for="IsSelfEmplmt2">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="table">
                            <div>
                                <div><label>Employer Name        </label><input                id="EmplrNm" type="text" class="form-control"   /></div>
                                <div><label>Employer Phone Number</label><input                id="EmplrBusPhone" type="text" preset="phone" class="form-control"   /></div>
                            </div>
                        </div>
                        <div class="table">
                            <div>
                                <div><label>Employer Address     </label><input                id="EmplrAddr" type="text" class="form-control" /></div>
                                <div><label>Employer Zip Code    </label><ml:zipcodetextbox    id="EmplrZip" runat="server" preset="zipcode" width="50" CssClass="form-control"/></div>
                                <div><label>Employer City        </label><asp:Textbox          id="EmplrCity" runat="server" class="form-control" /></div>
                                <div><label>Employer State       </label><ml:StateDropDownList ID="EmplrState" runat="server" /></div>
                            </div>
                        </div>
                        <div class="table margin-bottom">
                            <div>
                                <div><label>Position / Job Title </label><input                id="JobTitle" type="text" class="form-control"   /></div>
                                <div><label>Job Start Date       </label><ml:DateTextBox       ID="EmplmtStartD" runat="server" CssClass="mask picker-template" Width="75" preset="date" IsDisplayCalendarHelper="false"/></div>
                                <div><label>Job End Date         </label><ml:DateTextBox       ID="EmplmtEndD" runat="server" CssClass="mask picker-template" Width="75" preset="date" IsDisplayCalendarHelper="false"/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
