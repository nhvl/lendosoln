﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.Common;
using DataAccess;

namespace PriceMyLoan.webapp
{
    public partial class ResultsFilter : PriceMyLoan.UI.BaseUserControl
    {
        public bool EnableAdvancedFilterOptionsForPriceEngineResults
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ProductCodeFilterPopup.EnableAdvancedFilterOptionsForPriceEngineResults = this.EnableAdvancedFilterOptionsForPriceEngineResults;
            ProductCodeFilterPopup.Visible = this.EnableAdvancedFilterOptionsForPriceEngineResults;
            advancedFilterRow.Visible = this.EnableAdvancedFilterOptionsForPriceEngineResults;

            /*string sortValue;
            string SortCookieName = "ResultSortOrder";

            if (CurrentBroker.IsBestPriceEnabled)
            {
                if (CurrentBroker.IsAlwaysUseBestPrice || BestPricingRb.Checked)
                {
                    sortValue = PriceMyLoanConstants.SortType_BestPrice;
                }
                else
                {
                    sortValue = PriceMyLoanConstants.SortType_ByName;
                }
            }
            else
            {
                switch (resultSort.SelectedIndex)
                {
                    case 0: sortValue = PriceMyLoanConstants.SortType_ByNoteRate; break;
                    case 1: sortValue = PriceMyLoanConstants.SortType_ByName; break;
                    default:
                        Tools.LogBug("Unhandled sort option.");
                        sortValue = PriceMyLoanConstants.SortType_ByName;
                        break;
                }
            }

            // 10/10/07 mf. OPM 17339. We are extending the life of this crumb.
            // The sort order will live until user chooses to clear our cookie.
            LendersOffice.Common.RequestHelper.StoreToCookie(SortCookieName, sortValue, DateTime.MaxValue);*/
        }
    }
}