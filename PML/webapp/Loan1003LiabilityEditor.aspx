﻿<%@ Page Language="c#" CodeBehind="Loan1003LiabilityEditor.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.webapp.Loan1003LiabilityEditor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head id="HEAD1" runat="server">
    <style type="text/css">
        #m_dg .btn-resize {
            width: 110px;
            height: 20px;
            padding-top: 4px;
            padding-left: 2px;
            padding-right: 2px;
        }
    </style>
    <title>LiabilityList</title>
</head>

<body>
<script>
    var VirtualRoot = <%=AspxTools.JsString(VirtualRoot) %>;
    var LoanID = <%= AspxTools.JsString(LoanID) %>;
    var CurrentAppId = <%= AspxTools.JsString(CurrentAppId) %>;
    var sFileVersion = <%= AspxTools.JsString(sFileVersion) %>;
    var IsReadOnly = <%= AspxTools.JsBool(IsReadOnly) %>;
    const iframeSelf = new IframeSelf();
</script>
<script>
    $j(function(){
        const popup = new LqbPopup(document.querySelector("div[lqb-popup]"));

        iframeSelf.popup = popup;

        TPOStyleUnification.Components();
        $j('#cancelBtn').click(function(){
            iframeSelf.resolve(null);
        });

        window.save = function save() {
            var args = getAllFormValues();
            args.LoanId = ML.LoanId;
            args.AppId = ML.AppId;

            var result = gService.Loan1003.call('SaveLiabilityRecords', args, false/*keepQuiet*/, false/*bUseParentForError*/, true/*dontHandleError*/);
            if (result.error) {
                simpleDialog.alert(result.UserMessage);
            }
            else {
                iframeSelf.resolve(result.value);
            }
        }

        window.f_onPaidOffCheck = function f_onPaidOffCheck(cb) {
            var ratio_cb_id = cb.id.replace(/pdoff_/, "ratio_");
            var ratio_cb = document.getElementById(ratio_cb_id);
            if (null != ratio_cb) {
                if (cb.checked) {
                    ratio_cb.checked = false;
                } else {
                    ratio_cb.disabled = false;
                }
            }
        };

        window.f_addLiability = function f_addLiability() {
            if (IsReadOnly) return;

            IframePopup.show(VirtualRoot + '/webapp/Loan1003AddLiability.aspx?loanid=' + LoanID + '&appid=' + CurrentAppId + '&version=' + sFileVersion, {})
            .then(function(pair){
                var error = pair[0];
                var result = pair[1];
                if (error != null) {
                    console.error(error);
                    simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
                } else {
                    if (!!result) window.location.reload();
                }

                $j("#LiabilityList").show();
            });

            $j("#LiabilityList").hide();
        };
    });
    
    function onSelect(x) {
        window.setTimeout(function() {
            iframeSelf.resolve(x);
        });
    };

    function linkReo(recordId) {      
        var sFileVersion = null;
        if (document.getElementById("sFileVersion") != null) {
            sFileVersion = document.getElementById("sFileVersion").value;
        }
      
        LQBPopup.Show(VirtualRoot + '/webapp/Loan1003LinkLiabilityWithReo.aspx?loanid=' + LoanID + '&appid=' + CurrentAppId + '&version=' + sFileVersion, {
            onReturn: function (returnArgs) {
                if (typeof (returnArgs) != 'undefined' && returnArgs != null && returnArgs.OK) {
                    var args = new Object();
                    args["LoanID"] = ML.LoanId;
                    args["ApplicationID"] = ML.AppId;
                    args["ReoId"] = returnArgs.ReoId;
                    args["sFileVersion"] = sFileVersion;
                    args["LiabilityId"] = recordId;

                    var result = gService.Loan1003.callAsyncSimple("LinkLiabilityWithReo", args, function(result) {
                        if (!result.error) {
                            document.getElementById("sFileVersion").value = result.value["sFileVersion"];
                            alert("The liability record has been linked with selected REO record.");
                            window.location.reload();
                        }
                        else {
                            alert("Error updating balance and payment info to REO.");
                        }
                    });
                }
            },
            hideCloseButton: true,
            width: 320,
            height: 155
        });
    }
</script>
<form id="LiabilityList" method="post" runat="server">
    <div lqb-popup class="modal-app-info modal-assets-edit">
        <div class="modal-header">
            <button type="button" class="close" onclick="$j('#cancelBtn').click();"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title table-flex table-flex-app-info">
                <div>
                    <span class="liability-list">Liabilities Editor</span> <button id="AddLiability" type="button" onclick="f_addLiability();" class="btn btn-default" nohighlight>Add Liability</button>
                </div>
                <div></div>
            </h4>
        </div>
        <div class="modal-body">
            <div id="MainLiabilityFrame" runat="server" class="margin-bottom">
                <table class="table-flex table-flex-app-info">
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            Exclude from ratio
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            Monthly Payment
                        </td>
                        <td>
                            Months Left
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Alimony
                        </td>
                        <td>
                            <input type="checkbox" id="Alimony_NotUsedInRatio" runat="server" />
                            <label for="Alimony_NotUsedInRatio">Exclude</label> 
                        </td>
                        <td>
                            <input type="text" id="Alimony_OwedTo" runat="server" />
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="Alimony_MonthlyPayment" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                        </td>
                        <td>
                            <input type="text" id="Alimony_RemainingMonths" runat="server" class="input-w100" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Child Support
                        </td>
                        <td>
                            <input type="checkbox" id="ChildSupport_NotUsedInRatio" runat="server" />
                            <label for="ChildSupport_NotUsedInRatio">Exclude</label> 
                        </td>
                        <td>
                            <input type="text" id="ChildSupport_OwedTo" runat="server" />
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="ChildSupport_MonthlyPayment" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                        </td>
                        <td>
                            <input type="text" id="ChildSupport_RemainingMonths" runat="server" class="input-w100" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Job Expense
                        </td>
                        <td>
                            <input type="checkbox" id="JobRelated1_NotUsedInRatio" runat="server" />
                            <label for="JobRelated1_NotUsedInRatio">Exclude</label> 
                        </td>
                        <td>
                            <input type="text" id="JobRelated1_ExpenseDescription" runat="server" />
                        </td>
                        <td colspan="2">
                            <ml:MoneyTextBox ID="JobRelated1_MonthlyPayment" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <input type="checkbox" id="JobRelated2_NotUsedInRatio" runat="server" />
                            <label for="JobRelated2_NotUsedInRatio">Exclude</label> 
                        </td>
                        <td>
                            <input type="text" id="JobRelated2_ExpenseDescription" runat="server" />
                        </td>
                        <td colspan="2">
                            <ml:MoneyTextBox ID="JobRelated2_MonthlyPayment" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                        </td>
                    </tr>
                </table>
                <br />

                <ml:CommonDataGrid ID="m_dg" runat="server" CssClass="DataGrid margin-bottom none-hover-table modal-table-scrollbar">
                    <AlternatingItemStyle CssClass="" VerticalAlign="Top"/>
                    <ItemStyle CssClass="" VerticalAlign="Top"/>
                    <HeaderStyle CssClass="GridHeader"/>

                    <Columns>
                        <asp:TemplateColumn HeaderText="Ownership">
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(DisplayOwnerName(Eval("OwnerT").ToString()))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Debt Type">
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(DisplayLiabilityType(Eval("DebtT").ToString()))%>                                
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Company">
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(Eval("ComNm").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Balance" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(DisplayMoneyString(Eval("Bal").ToString())) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Payment" HeaderStyle-Wrap="false" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(DisplayMoneyString(Eval("Pmt").ToString())) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Paid Off" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <%# AspxTools.HtmlControl(CreatePayoffCheckbox(Container.DataItem)) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Used in Ratio" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <%# AspxTools.HtmlControl(CreateUsedInRatioCheckbox(Container.DataItem)) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Link with REO" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <%# AspxTools.HtmlControl(CreateReoLink(Container.DataItem)) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </ml:CommonDataGrid>
            </div>
            <div id="divErrorMessage" class="ErrorMessage">
      <ml:EncodedLiteral ID="m_errorMessage" runat="server" />
            </div>
            <span id="spanInformation" runat="server">
                *** Mortgage payments should be calculated into either the Total Income or Other Properties' Negative Cash Flow amount.</span>
        </div>
        <div class="modal-footer">
            <button type="button" id="cancelBtn" class="btn btn-flat">Cancel</button>
            <button type="button" id="saveButton" class="btn btn-flat" onclick="save();">Save</button>
        </div>
    </div>
</form>


</body>
</html>
