﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HmdaRaceEthnicityData.ascx.cs" Inherits="PriceMyLoan.webapp.HmdaRaceEthnicityData" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>

<style type="text/css">
    .Bold
    {
        font-weight: bold;
    }
    .NoPaddingTable
    {
        border-spacing: 0;
        border-collapse: collapse;
        padding: 0;
    }
    .DemographicInfo
    {
        overflow: auto;
        width: 100%;
        height: 100%;
        display: table;
    }
    .OtherOption
    {
        width: 300px;
    }
    .SectionHeader
    {
        padding: 2px;
    }
    .FurnishLabel
    {
        padding-left: 0;
    }
</style>
<div id="DemographicDiv" class="demographic-data">
    <div>
        <span class="Bold">The purpose of collecting this information</span> is to help ensure that all applicants are treated fairly and that the housing needs of communities and neighborhoods are being fulfilled. 
        For residential mortgage lending, federal law requires that we ask applicants for their demographic information (ethnicity, sex, and race) in order to monitor our compliance 
        with equal credit opportunity, fair housing, and home mortgage disclosure laws. You are not required to provide this information, but are encouraged to do so. <span class="Bold">The law provides that we may not discriminate</span> 
        on the basis of this information, or on whether you choose to provide it. However, if you choose not to provide the information and you have made 
        this application in person, federal  regulations require us to note your ethnicity, sex, and race on the basis of visual observation or surname. The law also provides that we may 
        not discriminate on the basis of age or marital status information you provide in this application. 
        <br />
        <span class="Bold">Instructions to the borrower:</span> You may select one or more "Hispanic or Latino" origins and one or more designations for "Race". If you do not wish to provide some or all of this information, select the applicable check box.
    </div>
    <br />
    <fieldset class="box-section-border-red box-section-no-bottom-padding">
        <header class="header header-border header-border-app-info header-border-app-info-pg3 header-border-no-bottom-margin">
            The Demographic Information Was Provided Through:
        </header>
        <div class="Table">
            <div class="TableCell">
                <span class="text-app-info">Borrower</span>
                <div class="TopBottomBorder">
                    <ul class="UndecoratedList">
                        <li class="mdc-form-field-auto-height">
                            <input SkipMe type="radio" class="RefreshCalculation" name="aBInterviewMethodT" runat="server" id="aBInterviewMethodT_ftf"/>
                            <label for="<%= AspxTools.ClientId(aBInterviewMethodT_ftf) %>">Face-to-Face Interview <span class="Italics">(includes Electronic Media w/ Video Component)</span></label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aBInterviewMethodT" runat="server" id="aBInterviewMethodT_ti"/><label for="<%= AspxTools.ClientId(aBInterviewMethodT_ti) %>">Telephone Interview</label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aBInterviewMethodT" runat="server" id="aBInterviewMethodT_fm"/><label for="<%= AspxTools.ClientId(aBInterviewMethodT_fm) %>">Fax or Mail</label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aBInterviewMethodT" runat="server" id="aBInterviewMethodT_ei"/><label for="<%= AspxTools.ClientId(aBInterviewMethodT_ei) %>">Email or Internet</label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aBInterviewMethodT" runat="server" id="aBInterviewMethodT_bl"/><label for="<%= AspxTools.ClientId(aBInterviewMethodT_bl) %>">Leave Blank</label>
                        </li>
                    </ul>
                </div>
                <div>
                    <div>
                        <div class="text-app-info field-label-spacer">Ethnicity</div>
                        <ul class="UndecoratedList">
                            <li>
                                <input SkipMe type="checkbox" value="1" runat="server" id="aBHispanicT_is" /><label for="<%= AspxTools.ClientId(aBHispanicT_is) %>">Hispanic or Latino</label>
                                <div class="Indent">
                                    <table class="NoPaddingTable">
                                        <tr>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsMexican" />    <label for="<%= AspxTools.ClientId(aBIsMexican) %>">Mexican</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsPuertoRican" /><label for="<%= AspxTools.ClientId(aBIsPuertoRican) %>">Puerto Rican</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsCuban" />      <label for="<%= AspxTools.ClientId(aBIsCuban) %>">Cuban</label>
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="checkbox" runat="server" id="aBIsOtherHispanicOrLatino" /><label for="<%= AspxTools.ClientId(aBIsOtherHispanicOrLatino) %>">Other Hispanic or Latino - <span class="Italics">Enter origin:</span></label>
                                    <div class="padding-left-30-pg3 padding-top-p16">
                                        <input type="text" class="OtherOption" runat="server" id="aBOtherHispanicOrLatinoDescription"/><br />
                                        <span class="Italics">Examples: Argentinean, Colombian, Dominican, Nicaraguan, Salvadoran, Spaniard, etc.</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <input SkipMe type="checkbox" value="0" runat="server" id="aBHispanicT_not" /><label for="<%= AspxTools.ClientId(aBHispanicT_not) %>">Not Hispanic or Latino</label>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aBDoesNotWishToProvideEthnicity" class="RefreshCalculation"/><label for="<%= AspxTools.ClientId(aBDoesNotWishToProvideEthnicity) %>">I do not wish to provide this information</label>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="text-app-info field-label-spacer">Race</div>
                        <ul class="UndecoratedList">
                            <li>
                                    <label class="d-flex"><div><input type="checkbox" runat="server" id="aBIsAmericanIndian" /></div><div>American Indian or Alaska Native - <span class="Italics">Enter name of enrolled or principal tribe:</span></div></label>
                                    <div class="padding-left-26-pg3 padding-top-p16">
                                        <input type="text" class="OtherOption" runat="server" id="aBOtherAmericanIndianDescription"/>
                                    </div>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aBIsAsian"/><label for="<%= AspxTools.ClientId(aBIsAsian) %>">Asian</label>
                                <div class="Indent">
                                    <table class="NoPaddingTable">
                                        <tr>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsAsianIndian" /><label for="<%= AspxTools.ClientId(aBIsAsianIndian) %>">Asian Indian</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsChinese" />    <label for="<%= AspxTools.ClientId(aBIsChinese) %>">Chinese</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsFilipino" />   <label for="<%= AspxTools.ClientId(aBIsFilipino) %>">Filipino</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsJapanese" />   <label for="<%= AspxTools.ClientId(aBIsJapanese) %>">Japanese</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsKorean" />     <label for="<%= AspxTools.ClientId(aBIsKorean) %>">Korean</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aBIsVietnamese" /> <label for="<%= AspxTools.ClientId(aBIsVietnamese) %>">Vietnamese</label>
                                            </td>
                                        </tr>
                                    </table>
                                        <input type="checkbox" runat="server" id="aBIsOtherAsian"/><label for="<%= AspxTools.ClientId(aBIsOtherAsian) %>">Other Asian - <span class="Italics">Enter race:</span></label>
                                        <div class="padding-left-30-pg3 padding-top-p16">
                                            <input type="text" class="OtherOption" runat="server" id="aBOtherAsianDescription"/><br />
                                            <span class="Italics">Examples: Hmong, Laotian, Thai, Pakistani, Cambodian, etc.</span>
                                        </div>
                                </div>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aBIsBlack"/><label for="<%= AspxTools.ClientId(aBIsBlack) %>">Black or African American</label>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aBIsPacificIslander"/><label for="<%= AspxTools.ClientId(aBIsPacificIslander) %>">Native Hawaiian or Other Pacific Islander</label>
                                <div class="Indent">
                                    <table class="NoPaddingTable">
                                        <tr>
                                            <td class="nowrap">
                                                <input type="checkbox" runat="server" id="aBIsNativeHawaiian" />     <label for="<%= AspxTools.ClientId(aBIsNativeHawaiian) %>">Native Hawaiian</label>
                                            </td>
                                            <td class="nowrap">
                                                <input type="checkbox" runat="server" id="aBIsGuamanianOrChamorro" /><label for="<%= AspxTools.ClientId(aBIsGuamanianOrChamorro) %>">Guamanian or Chamorro</label>
                                            </td>
                                            <td class="nowrap">
                                                <input type="checkbox" runat="server" id="aBIsSamoan" />             <label for="<%= AspxTools.ClientId(aBIsSamoan) %>">Samoan</label>
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="checkbox" runat="server" id="aBIsOtherPacificIslander" /><label for="<%= AspxTools.ClientId(aBIsOtherPacificIslander) %>">Other Pacific Islander - <span class="Italics">Enter race:</span></label>
                                    <div class="padding-left-30-pg3 padding-top-p16">
                                        <input type="text" class="OtherOption" runat="server" id="aBOtherPacificIslanderDescription"/><br />
                                        <span class="Italics">Examples: Fijian, Tongan, etc.</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aBIsWhite"/><label for="<%= AspxTools.ClientId(aBIsWhite) %>">White</label>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aBDoesNotWishToProvideRace" class="RefreshCalculation"/><label for="<%= AspxTools.ClientId(aBDoesNotWishToProvideRace) %>">I do not wish to provide this information</label>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="text-app-info field-label-spacer">Sex</div>
                        <ul class="UndecoratedList none-padding-bottom">
                            <li>
                                <input SkipMe type="checkbox" runat="server" id="aBGender_f"/><label for="<%= AspxTools.ClientId(aBGender_f) %>">Female</label>
                            </li>
                            <li>
                                <input SkipMe type="checkbox" runat="server" id="aBGender_m" /><label for="<%= AspxTools.ClientId(aBGender_m) %>">Male</label>
                            </li>
                            <li>
                                <input SkipMe type="checkbox" runat="server" id="aBGender_u" class="RefreshCalculation" /><label for="<%= AspxTools.ClientId(aBGender_u) %>">I do not wish to provide this information</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="TableCell BorderLeft">
                <span class="text-app-info">Co-Borrower</span>
                <div class="TopBottomBorder">
                    <ul class="UndecoratedList">
                        <li class="mdc-form-field-auto-height">
                            <input SkipMe type="radio" class="RefreshCalculation" name="aCInterviewMethodT" runat="server" id="aCInterviewMethodT_ftf"/><label for="<%= AspxTools.ClientId(aCInterviewMethodT_ftf) %>">Face-to-Face Interview <span class="Italics">(includes Electronic Media w/ Video Component)</span></label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aCInterviewMethodT" runat="server" id="aCInterviewMethodT_ti"/><label for="<%= AspxTools.ClientId(aCInterviewMethodT_ti) %>">Telephone Interview</label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aCInterviewMethodT" runat="server" id="aCInterviewMethodT_fm"/><label for="<%= AspxTools.ClientId(aCInterviewMethodT_fm) %>">Fax or Mail</label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aCInterviewMethodT" runat="server" id="aCInterviewMethodT_ei"/><label for="<%= AspxTools.ClientId(aCInterviewMethodT_ei) %>">Email or Internet</label>
                        </li>
                        <li>
                            <input SkipMe type="radio" class="RefreshCalculation" name="aCInterviewMethodT" runat="server" id="aCInterviewMethodT_bl"/><label for="<%= AspxTools.ClientId(aCInterviewMethodT_bl) %>">Leave Blank</label>
                        </li>
                    </ul>
                </div>
                <div>
                    <div>
                        <div class="text-app-info field-label-spacer">Ethnicity</div>
                        <ul class="UndecoratedList">
                            <li>
                                <input SkipMe type="checkbox" runat="server" value="1" id="aCHispanicT_is"/><label for="<%= AspxTools.ClientId(aCHispanicT_is) %>">Hispanic or Latino</label>
                                <div class="Indent">
                                    <table class="NoPaddingTable">
                                        <tr>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsMexican" /><label for="<%= AspxTools.ClientId(aCIsMexican) %>">Mexican</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsPuertoRican" /><label for="<%= AspxTools.ClientId(aCIsPuertoRican) %>">Puerto Rican</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsCuban" /><label for="<%= AspxTools.ClientId(aCIsCuban) %>">Cuban</label>
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="checkbox" runat="server" id="aCIsOtherHispanicOrLatino" /><label for="<%= AspxTools.ClientId(aCIsOtherHispanicOrLatino) %>">Other Hispanic or Latino - <span class="Italics">Enter origin:</span></label>
                                    <div class="padding-left-30-pg3 padding-top-p16">
                                        <input type="text" class="OtherOption" runat="server" id="aCOtherHispanicOrLatinoDescription"/><br />
                                        <span class="Italics">Examples: Argentinean, Colombian, Dominican, Nicaraguan, Salvadoran, Spaniard, etc.</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <input SkipMe type="checkbox" runat="server" value="0" id="aCHispanicT_not" /><label for="<%= AspxTools.ClientId(aCHispanicT_not) %>">Not Hispanic or Latino</label>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aCDoesNotWishToProvideEthnicity" class="RefreshCalculation"/><label for="<%= AspxTools.ClientId(aCDoesNotWishToProvideEthnicity) %>"> I do not wish to provide this information</label>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="text-app-info field-label-spacer">Race</div>
                        <ul class="UndecoratedList">
                            <li>
                                <label class="d-flex"><div><input type="checkbox" runat="server" id="aCIsAmericanIndian" /></div><div>American Indian or Alaska Native - <span class="Italics">Enter name of enrolled or principal tribe:</span></div></label>
                                <div class="padding-left-26-pg3 padding-top-p16">
                                    <input type="text" class="OtherOption" runat="server" id="aCOtherAmericanIndianDescription"/>
                                </div>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aCIsAsian"/><label for="<%= AspxTools.ClientId(aCIsAsian) %>">Asian</label>
                                <div class="Indent">
                                    <table class="NoPaddingTable">
                                        <tr>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsAsianIndian" /><label for="<%= AspxTools.ClientId(aCIsAsianIndian) %>">Asian Indian</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsChinese" />    <label for="<%= AspxTools.ClientId(aCIsChinese) %>">Chinese</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsFilipino" />   <label for="<%= AspxTools.ClientId(aCIsFilipino) %>">Filipino</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsJapanese" />   <label for="<%= AspxTools.ClientId(aCIsJapanese) %>">Japanese</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsKorean" />     <label for="<%= AspxTools.ClientId(aCIsKorean) %>">Korean</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" runat="server" id="aCIsVietnamese" /> <label for="<%= AspxTools.ClientId(aCIsVietnamese) %>">Vietnamese</label>
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="checkbox" runat="server" id="aCIsOtherAsian" /><label for="<%= AspxTools.ClientId(aCIsOtherAsian) %>">Other Asian - <span class="Italics">Enter race:</span></label>
                                    <div class="padding-left-30-pg3 padding-top-p16">
                                        <input type="text" class="OtherOption" runat="server" id="aCOtherAsianDescription"/><br />
                                        <span class="Italics">Examples: Hmong, Laotian, Thai, Pakistani, Cambodian, etc.</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aCIsBlack"/><label for="<%= AspxTools.ClientId(aCIsBlack) %>">Black or African American</label>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aCIsPacificIslander"/><label for="<%= AspxTools.ClientId(aCIsPacificIslander) %>">Native Hawaiian or Other Pacific Islander</label>
                                <div class="Indent">
                                    <table class="NoPaddingTable">
                                        <tr>
                                            <td class="nowrap">
                                                <input type="checkbox" runat="server" id="aCIsNativeHawaiian" />     <label for="<%= AspxTools.ClientId(aCIsNativeHawaiian) %>">Native Hawaiian</label>
                                            </td>
                                            <td class="nowrap">
                                                <input type="checkbox" runat="server" id="aCIsGuamanianOrChamorro" /><label for="<%= AspxTools.ClientId(aCIsGuamanianOrChamorro) %>">Guamanian or Chamorro</label>
                                            </td>
                                            <td class="nowrap">
                                                <input type="checkbox" runat="server" id="aCIsSamoan" />             <label for="<%= AspxTools.ClientId(aCIsSamoan) %>">Samoan</label>
                                            </td>
                                        </tr>
                                    </table>
                                        <input type="checkbox" runat="server" id="aCIsOtherPacificIslander" /><label for="<%= AspxTools.ClientId(aCIsOtherPacificIslander) %>">Other Pacific Islander - <span class="Italics">Enter race:</span></label>
                                        <div class="padding-left-30-pg3 padding-top-p16">
                                            <input type="text" class="OtherOption" runat="server" id="aCOtherPacificIslanderDescription"/><br />
                                            <span class="Italics">Examples: Fijian, Tongan, etc.</span>
                                        </div>
                                </div>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aCIsWhite"/><label for="<%= AspxTools.ClientId(aCIsWhite) %>">White</label>
                            </li>
                            <li>
                                <input type="checkbox" runat="server" id="aCDoesNotWishToProvideRace" class="RefreshCalculation"/><label for="<%= AspxTools.ClientId(aCDoesNotWishToProvideRace) %>">I do not wish to provide this information</label>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="text-app-info field-label-spacer">Sex</div>
                        <ul class="UndecoratedList none-padding-bottom">
                            <li>
                                <input SkipMe type="checkbox" runat="server" id="aCGender_f" /><label for="<%= AspxTools.ClientId(aCGender_f) %>">Female</label>
                            </li>
                            <li>
                                <input SkipMe type="checkbox" runat="server" id="aCGender_m" /><label for="<%= AspxTools.ClientId(aCGender_m) %>">Male</label>
                            </li>
                            <li>
                                <input SkipMe type="checkbox" runat="server" id="aCGender_u" class="RefreshCalculation" /><label for="<%= AspxTools.ClientId(aCGender_u) %>">I do not wish to provide this information</label>
                            </li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="box-section-border-red box-section-no-bottom-padding">
        <header class="header header-border header-border-app-info header-border-app-info-pg3 header-border-no-bottom-margin">
            To Be Completed by Financial Institution <span class="Italics">(for application taken in person)</span>
        </header>
        <div class="Table">
            <div class="TableCell">
                <div>
                    <table>
                        <tr>
                            <td>
                                Was the ethnicity of the Borrower collected on the basis of visual observation or surname?
                            </td>
                            <td nowrap>
                                <input SkipMe type="radio" class="BFtFCollected" name="aBEthnicityCollectedByObservationOrSurname" value="2" runat="server" id="aBEthnicityCollectedByObservationOrSurname_False"/><label for="<%= AspxTools.ClientId(aBEthnicityCollectedByObservationOrSurname_False) %>">No</label>
                                <input SkipMe type="radio" class="BFtFCollected" name="aBEthnicityCollectedByObservationOrSurname" value="1" runat="server" id="aBEthnicityCollectedByObservationOrSurname_True"/><label for="<%= AspxTools.ClientId(aBEthnicityCollectedByObservationOrSurname_True) %>">Yes</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Was the sex of the Borrower collected on the basis of visual observation or surname?
                            </td>
                            <td nowrap>
                                <input SkipMe type="radio" class="BFtFCollected" name="aBSexCollectedByObservationOrSurname" value="2" runat="server" id="aBSexCollectedByObservationOrSurname_False"/><label for="<%= AspxTools.ClientId(aBSexCollectedByObservationOrSurname_False) %>">No</label>
                                <input SkipMe type="radio" class="BFtFCollected" name="aBSexCollectedByObservationOrSurname" value="1" runat="server" id="aBSexCollectedByObservationOrSurname_True"/><label for="<%= AspxTools.ClientId(aBSexCollectedByObservationOrSurname_True) %>">Yes</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Was the race of the Borrower collected on the basis of visual observation or surname?
                            </td>
                            <td nowrap>
                                <input SkipMe type="radio" class="BFtFCollected" name="aBRaceCollectedByObservationOrSurname" value="2" runat="server" id="aBRaceCollectedByObservationOrSurname_False"/><label for="<%= AspxTools.ClientId(aBRaceCollectedByObservationOrSurname_False) %>">No</label>
                                <input SkipMe type="radio" class="BFtFCollected" name="aBRaceCollectedByObservationOrSurname" value="1" runat="server" id="aBRaceCollectedByObservationOrSurname_True"/><label for="<%= AspxTools.ClientId(aBRaceCollectedByObservationOrSurname_True) %>">Yes</label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="TableCell BorderLeft">
                <div>
                    <table>
                        <tr>
                            <td>
                                Was the ethnicity of the Borrower collected on the basis of visual observation or surname?
                            </td>
                            <td nowrap>
                                <input SkipMe type="radio" class="CFtFCollected" name="aCEthnicityCollectedByObservationOrSurname" value="2" runat="server" id="aCEthnicityCollectedByObservationOrSurname_False"/><label for="<%= AspxTools.ClientId(aCEthnicityCollectedByObservationOrSurname_False) %>">No</label>
                                <input SkipMe type="radio" class="CFtFCollected" name="aCEthnicityCollectedByObservationOrSurname" value="1" runat="server" id="aCEthnicityCollectedByObservationOrSurname_True" /><label for="<%= AspxTools.ClientId(aCEthnicityCollectedByObservationOrSurname_True) %>">Yes</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Was the sex of the Borrower collected on the basis of visual observation or surname?
                            </td>
                            <td nowrap>
                                <input SkipMe type="radio" class="CFtFCollected" name="aCSexCollectedByObservationOrSurname" value="2" runat="server" id="aCSexCollectedByObservationOrSurname_False"/><label for="<%= AspxTools.ClientId(aCSexCollectedByObservationOrSurname_False) %>">No</label>
                                <input SkipMe type="radio" class="CFtFCollected" name="aCSexCollectedByObservationOrSurname" value="1" runat="server" id="aCSexCollectedByObservationOrSurname_True" /><label for="<%= AspxTools.ClientId(aCSexCollectedByObservationOrSurname_True) %>">Yes</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Was the race of the Borrower collected on the basis of visual observation or surname?
                            </td>
                            <td nowrap>
                                <input SkipMe type="radio" class="CFtFCollected" name="aCRaceCollectedByObservationOrSurname" value="2" runat="server" id="aCRaceCollectedByObservationOrSurname_False" /><label for="<%= AspxTools.ClientId(aCRaceCollectedByObservationOrSurname_False) %>">No</label>
                                <input SkipMe type="radio" class="CFtFCollected" name="aCRaceCollectedByObservationOrSurname" value="1" runat="server" id="aCRaceCollectedByObservationOrSurname_True" /><label for="<%= AspxTools.ClientId(aCRaceCollectedByObservationOrSurname_True) %>">Yes</label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="box-section-border-red box-section-no-bottom-padding">
        <header class="header header-border header-border-app-info header-border-app-info-pg3 header-border-no-bottom-margin">
            Fallbacks for Old Exports
        </header>
        <div class="Table">
            <div class="TableCell">
                <div id="BorrowerFallbacks">
                    <table>
                        <tr id="BEthnicityFallbackRow">
                            <td class="FieldLabel FallbackLabel">Ethnicity</td>
                            <td>
                                <asp:DropDownList runat="server" ID="aBHispanicTFallback"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="BGenderFallbackRow">
                            <td class="FieldLabel FallbackLabel">Sex</td>
                            <td>
                                <asp:DropDownList runat="server" ID="aBGenderFallback"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="FurnishLabel">Do not wish to furnish for 1003</td>
                            <td>
                                <input type="checkbox" runat="server" id="aBNoFurnish" class="aBNoFurnish" /><label for="<%= AspxTools.ClientId(aBNoFurnish) %>"></label>
                                <label class="lock-checkbox"><input type="checkbox" runat="server" id="aBNoFurnishLckd" class="RefreshCalculation" /></label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="TableCell BorderLeft">
                <div id="CoborrowerFallbacks">
                    <table>
                        <tr id="CEthnicityFallbackRow">
                            <td class="FieldLabel FallbackLabel">Ethnicity</td>
                            <td>
                                <asp:DropDownList runat="server" ID="aCHispanicTFallback"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="CGenderFallbackRow">
                            <td class="FieldLabel FallbackLabel">Sex</td>
                            <td>
                                <asp:DropDownList runat="server" ID="aCGenderFallback"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="FurnishLabel">Do not wish to furnish for 1003</td>
                            <td>
                                <input type="checkbox" runat="server" id="aCNoFurnish" class="aCNoFurnish" /><label for="<%= AspxTools.ClientId(aCNoFurnish) %>"></label>
                                <label class="lock-checkbox"><input type="checkbox" runat="server" id="aCNoFurnishLckd" class="RefreshCalculation" /></label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
