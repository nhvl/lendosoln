<%@ Page language="c#" Codebehind="RolodexList.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.webapp.RolodexList" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>Contacts</title>
	</HEAD>
	<body class="EditBackground" scroll="yes" MS_POSITIONING="FlowLayout">
	
		<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"> </script>
		<script src="../inc/webapp/common.js"></script>
		<script language="javascript">
    <!--
      function _init() {
        <% if ( !Page.IsPostBack ) { %>
        resize( 760 , 400 );
        <% } %>
      }
      
      function chooseEmail(email)
      {
        var arg = window.dialogArguments;
        arg.OK = true;        
        arg.AgentEmail = email;      
        window.close();
      }
      
      function select(name, type, streetAddr, city, state, zip, altPhone, companyName, email, fax, phone, title, department, pager, agentLicenseNumber, companyLicenseNumber, phoneOfCompany, faxOfCompany, isNotifyWhenLoanStatusChange, addressOfBranch, cityOfBranch, stateOfBranch, zipOfBranch, phoneOfBranch, faxOfBranch, addressOfCompany, cityOfCompany, stateOfCompany, zipOfCompany, commissionPointOfLoanAmount, commissionPointOfGrossProfit, commissionMinBase, losIdentifier, companyLosIdentifier, licensesXml, companyLicensesXml, employeeId, companyId, employeeIDInCompany,
                      branchName, payToBankName, payToBankCityState, payToABANumber, payToAccountName, payToAccountNumber, furtherCreditToAccountName, furtherCreditToAccountNumber, brokerLevelAgentID, contactIsFromRolodex ) {
        var arg = window.dialogArguments;
        arg.OK = true;        
        arg.AgentName = name;
        arg.AgentType = type;
        arg.AgentAltPhone = altPhone;
        arg.AgentCompanyName = companyName;
        arg.AgentStreetAddr = streetAddr;
		arg.AgentCity = city;
		arg.AgentState = state;
		arg.AgentZip = zip;
        arg.AgentEmail = email;
        arg.AgentFax = fax;
        arg.AgentPhone = phone;
        arg.AgentTitle = title;
        arg.AgentDepartmentName = department;
        arg.AgentPager = pager;
        arg.AgentLicenseNumber = agentLicenseNumber;
        arg.CompanyStreetAddr = addressOfCompany;
		arg.CompanyCity = cityOfCompany;
		arg.CompanyState = stateOfCompany;
		arg.CompanyZip = zipOfCompany;
		arg.PhoneOfCompany = phoneOfCompany;
		arg.FaxOfCompany = faxOfCompany;
        arg.CompanyLicenseNumber = companyLicenseNumber;
        arg.IsNotifyWhenLoanStatusChange = isNotifyWhenLoanStatusChange;        
        arg.CommissionPointOfLoanAmount = commissionPointOfLoanAmount;
        arg.CommissionPointOfGrossProfit = commissionPointOfGrossProfit;
        arg.CommissionMinBase = commissionMinBase;
        arg.LoanOriginatorIdentifier = losIdentifier;
        arg.CompanyLoanOriginatorIdentifier = companyLosIdentifier;                        
        arg.AgentLicensesPanel = licensesXml;
        arg.CompanyLicensesPanel = companyLicensesXml;
        arg.EmployeeIDInCompany = employeeIDInCompany;
        arg.BrokerLevelAgentID = brokerLevelAgentID;
        arg.ShouldMatchBrokerContact = contactIsFromRolodex == "1" ? true : false;
        if(arg.ShouldMatchBrokerContact)
        {
            arg.AgentSourceT = 2; // from contact list
        }
        
        if(<%= AspxTools.JsNumeric(selectedTabIndex) %> === 0)
        {
            arg.CompanyStreetAddr = streetAddr;
			arg.CompanyCity = city;
			arg.CompanyState = state;
			arg.CompanyZip = zip;
			arg.CompanyId = companyId;
        }
        arg.EmployeeId  = employeeId;
        
        // OPM 109299
        arg.BranchName = branchName;
        arg.PayToBankName = payToBankName;
        arg.PayToBankCityState = payToBankCityState;
        arg.PayToABANumber = payToABANumber;
        arg.PayToAccountName = payToAccountName;
        arg.PayToAccountNumber = payToAccountNumber;
        arg.FurtherCreditToAccountNumber = furtherCreditToAccountNumber;
        arg.FurtherCreditToAccountName = furtherCreditToAccountName;
        window.close();
      }

<%-- 02/17/06 MF. OPM 4052. Now allow users to add and edit entries from here.
     Instead of standard refresh after, recreate search string so type filter does
     not revert back to original view. --%>

    function onAddClick() {
        showModal('/los/rolodex/rolodex.aspx', null, null, null, function(args){
            if (args.OK) {
                window.location.search = '?type=' + encodeURIComponent(<%= AspxTools.JsGetElementById(m_typeFilter) %>.value);
            }
        });
    }

    function onEditClick(agentid) {
        showModal('/los/rolodex/rolodex.aspx?cmd=edit&agentid=' + agentid, null, null, null, function(args){
            if (args.OK) {
                window.location.search = '?type=' + encodeURIComponent(<%= AspxTools.JsGetElementById(m_typeFilter) %>.value);
            }
        });
    }


    //-->
		</script>
        <h4 class="page-header">Choose from Contacts</h4>
		<form id="RolodexList" method="post" runat="server">
			<table class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
				
					<td class="FormTableHeader" style="PADDING-TOP: 5px" colSpan="2">
						<div class="Tabs">
						    <ul class="tabnav">
						        <li id="tab0" runat="server"><a onclick="__doPostBack('changeTab', 0)"; href="#">Contact Entries</a></li>
						        <li id="tab1" runat="server"><a onclick="__doPostBack('changeTab', 1)"; href="#">Internal Employees</a></li>
						        <li id="tab2" runat="server"><a onclick="__doPostBack('changeTab', 2)"; href="#">PML Users</a></li>
						        <li id="tab3" runat="server"><a onclick="__doPostBack('changeTab', 3)"; href="#">Assigned/Official Contacts</a></li>
						    </ul>
						</div>
						
						</td>
						
				</tr>
				<tr height="30" width="100%">
					<td class="FieldLabel">&nbsp;<asp:PlaceHolder runat="server" ID="SearchControls" >
					    <span style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px">Search for: 
							&nbsp;
							<asp:textbox id="m_searchFilter" runat="server" Width="100"></asp:textbox><asp:button id="m_searchButton" runat="server" Width="60" Text="Search"></asp:button>&nbsp; 
							Type: &nbsp;
							<asp:dropdownlist id="m_typeFilter" runat="server" AutoPostBack="true" Width="1.5in"></asp:dropdownlist>&nbsp;
							<ml:EncodedLiteral ID="m_statusLiteral" Text="Status: " runat="server" />
							<asp:DropDownList ID="m_statusFilter" runat="server" AutoPostBack="true" />&nbsp;
						    </asp:PlaceHolder> 
						</span>
					</td>
					<td align="right"><asp:panel id="m_addPanelTop" style="DISPLAY: inline" runat="server" Width="100%">
							<INPUT onclick="onAddClick();" type="button" value="Add new entry">
						</asp:panel><input style="WIDTH: 60px" onclick="window.close();" type="button" value="Close">
					</td>
					</tr>
				<tr style="display:none">
					<asp:Panel ID="m_importFrom" runat="server" Visible="False">
						<TD>
							<TABLE class="FormTable">
								<TR>
									<TD class="FieldLabel" vAlign="middle">Populate Address/Phone Info from:
									</TD>
									<TD class="FieldLabel" vAlign="bottom">
										<asp:radiobuttonlist id="m_importFromRadio" Runat="server" RepeatDirection="Horizontal">
											<asp:ListItem Text="Employee" Value="E" Selected="True"></asp:ListItem>
											<asp:ListItem Text="Employee's Branch" Value="B" Selected="False"></asp:ListItem>
											<asp:ListItem Text="Company" Value="C" Selected="False"></asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</TD>
					</asp:Panel>
				</tr>
				<tr>
					<td align="center" colSpan="2"><ml:commondatagrid id="m_rolodexGrid" OnItemDataBound="RolodexItemBound" runat="server" DataKeyField="AgentID">
							<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
							<itemstyle cssclass="GridItem"></itemstyle>
							<headerstyle cssclass="GridHeader"></headerstyle>
							<columns>
								<asp:TemplateColumn>
									<itemtemplate>
									    <%# AspxTools.HtmlControl(GenerateSelectLink(Container.DataItem)) %>
									</itemtemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<a href="#" onclick='onEditClick("<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AgentID")) %>");'>
											edit </a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="AgentTypeDesc" SortExpression="AgentTypeDesc" HeaderText="Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:BoundColumn DataField="AgentNm" SortExpression="AgentNm" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:BoundColumn DataField="AgentTitle" SortExpression="AgentTitle" HeaderText="Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:boundcolumn datafield="AgentPhone" HeaderText="Agent Phone" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:boundcolumn>
								<asp:BoundColumn DataField="AgentComNm" SortExpression="AgentComNm" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:TemplateColumn SortExpression="AgentComNmBranchNm" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
								    <ItemTemplate>
								        <%# AspxTools.HtmlString(GetSafeCompanyNmBranchNm(Container.DataItem)) %>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="NameOfBranch" HeaderText="Branch Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
								    <ItemTemplate>
								        <%# AspxTools.HtmlString(GetSafeBranchNm(Container.DataItem)) %>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="PhoneOfCompany" HeaderText="Company Phone" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Email" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
								    <ItemTemplate>
								        <a runat="server"   id="EmailLink"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem,"AgentEmail")) %> </a>
								    </ItemTemplate>
								</asp:TemplateColumn>
							</columns>
						</ml:commondatagrid><asp:panel id="m_rolodexDenied" style="PADDING-RIGHT: 60px; PADDING-LEFT: 60px; PADDING-BOTTOM: 60px; FONT: 12px arial; COLOR: red; PADDING-TOP: 60px; TEXT-ALIGN: center" runat="server">Access denied.&nbsp; You do not have 
      permission to view contact entries.&nbsp; Please consult your account 
      administrator if you have any questions. </asp:panel><asp:panel id="m_chooseSearchTerm" style="PADDING-RIGHT: 60px; PADDING-LEFT: 60px; PADDING-BOTTOM: 60px; FONT: 12px arial; COLOR: red; PADDING-TOP: 60px; TEXT-ALIGN: center" runat="server">Please choose a search term above. 
      </asp:panel></td>
				</tr>
				<tr height="30">
					<td align="right" colSpan="2"><asp:panel id="m_addPanelBottom" style="DISPLAY: inline" runat="server" Width="100%">
							<INPUT onclick="onAddClick();" type="button" value="Add new entry">
						</asp:panel><input style="WIDTH: 60px" onclick="window.close();" type="button" value="Close">
						<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
