﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PipelineReportPicker.aspx.cs" Inherits="PriceMyLoan.webapp.PipelineReportPicker" %>
<%@ Register TagPrefix="lrp" TagName="LeftRightPicker" Src="~/main/LeftRightPicker.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add/Remove Originator Portal Pipeline Report</title>
    <style type="text/css">
        body { overflow: hidden; }
        .move-arrow { cursor: pointer; }
    </style>
    <script type="text/javascript">
        $(function () {
            TPOStyleUnification.Components();
        });

        function onCancelClick() {
            parent.LQBPopup.Hide();
        }

        function selectReports() {
            var selectedReports = $.map($('.assigned-items option'), function (option) {
                return {
                    Key: option.value,
                    Value: option.text
                };
            });

            var args = {
                OK: true,
                SelectedReportsJson: JSON.stringify(selectedReports)
            };

            parent.LQBPopup.Return(args);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div lqb-popup>
        <div class="modal-header">
            <button type="button" class="close" onclick="onCancelClick()"><i class="material-icons">&#xE5CD;</i></button>
            <h4>Add/Remove Pipeline Reports</h4>
        </div>
        <div class="modal-body">
            <lrp:LeftRightPicker runat="server" id="Picker"></lrp:LeftRightPicker>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="selectReports();">OK</button>
        </div>
    </div>
    </form>
</body>
</html>
