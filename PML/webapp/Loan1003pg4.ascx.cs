﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
namespace PriceMyLoan.webapp
{
    public partial class Loan1003pg4 : PriceMyLoan.UI.BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void LoadData()
        {
            BrokerDB CurrentBroker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg4));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(RequestHelper.GetGuid("applicationid", Guid.Empty));

            a1003ContEditSheet.Value = dataApp.a1003ContEditSheet;
        }
    }
}