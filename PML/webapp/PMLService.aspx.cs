﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.OriginatorCompensation;
    using LendersOffice.RatePrice;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using PriceMyLoan.Security;
    using LqbGrammar.DataTypes;

    public partial class PMLService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        List<Guid> appIds = new List<Guid>();
        /// <summary>
        /// A cached copy of the lender-paid compensation plan.
        /// </summary>
        /// <remarks>
        /// In the case that a file does not have a comp plan set, this will come
        /// from the assigned loan officer. We cache the value to avoid hitting
        /// the Employee table for every background calculation.
        /// </remarks>
        private OriginatorCompensationPlan lenderPaidOriginatorCompPlan = null;

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CreateApp":
                    CreateApp();
                    break;
                case "DeleteApp":
                    DeleteApp();
                    break;
                case "SaveApp":
                    SaveApp();
                    break;
                case "LoadData":
                    LoadData();
                    break;
                case "CalculateData":
                    CalculateData();
                    break;
                case "SaveData":
                    SaveData();
                    break;
                case "ConvertQP2FileToLead":// the user sees it as Create Lead, but we as devs know what's really going on.
                    ConvertLeadToLoan(convertFromQP2File: true, convertToLead: true);
                    break;
                case "ConvertQP2FileToLoan": // the user sees it as Create Loan, but we as devs know what's really going on.
                    ConvertLeadToLoan(convertFromQP2File: true, convertToLead: false);
                    break;
                case "ConvertLeadToLoan":
                    ConvertLeadToLoan(convertFromQP2File: false, convertToLead: false);
                    break;
                case "AddPinState":
                    AddPinState();
                    break;
                case "DeletePinState":
                    DeletePinState();
                    break;
                case "GetPinCount":
                    GetPinCount();
                    break;
                case "SaveAgents":
                    SaveAgents();
                    break;
                case nameof(SaveRetailAgent):
                    this.SaveRetailAgent();
                    break;
                case "GetNumberOfLoanProgramsFor1st":
                    this.GetNumberOfLoanProgramsFor1st();
                    break;
                case "GetOrderCreditPermission":
                    this.GetOrderCreditPermission();
                    break;
                case "ImportLpaLoan":
                    this.ImportLpaLoan();
                    break;
                default:
                    Tools.LogBug(string.Format("Service method \"{0}\" not found!", methodName));
                    break;
            }
        }

        private void ImportLpaLoan()
        {
            var lpaLoanId = this.GetString("LpaLoanId", string.Empty);
            var lpaUserId = this.GetString("LpaUserId", null);
            var lpaUserPassword = this.GetString("LpaUserPassword", null);
            var lpaSellerNumber = this.GetString("LpaSellerNumber", null);
            var lpaPassword = this.GetString("LpaPassword", null);
            var lpaAusKey = this.GetString("LpaAusKey", string.Empty);
            var lpaNotpNum = this.GetString("LpaNotpNum", string.Empty);
            var importCredit = this.GetBool("ImportCredit");
            var portalMode = this.GetEnum<E_PortalMode>("PortalMode");
            var isLead = this.GetBool("IsLead");

            if (string.IsNullOrEmpty(lpaLoanId))
            {
                this.SetResult("Error", "Please provide an LPA Loan Id.");
                return;
            }

            var principal = (PriceMyLoanPrincipal)Context.User;
            var pmlTemplateId = isLead && principal.BrokerDB.CreateLeadFilesFromQuickpricerTemplate ? principal.BrokerDB.QuickPricerTemplateId : principal.BrokerDB.GetPmlTemplateIdByPortalMode(portalMode);

            var request = new LoanTransferRequestInput();
            request.Principal = principal;
            request.LoanResolution = LoanTransferLoanResolver.NewLoan(createLead: isLead, templateId: pmlTemplateId);
            request.LpaUserId = lpaUserId;
            request.LpaUserPassword = lpaUserPassword;
            request.ShouldImportCredit = importCredit;
            request.LpaSellerNumber = lpaSellerNumber;
            request.LpaSellerPassword = lpaPassword;
            request.LpaLoanId = lpaLoanId;
            request.LpaAusKey = lpaAusKey;
            request.LpaNotpNumber = lpaNotpNum;
            request.LoanBranchId = Guid.Empty;

            LoanTransferRequestOutput result = LoanTransferRequest.RunLoanTransferRequest(request);
            if (result.Success)
            {
                var loanId = result.LoanId;
                LendersOffice.ObjLib.Task.TaskUtilities.EnqueueTasksDueDateUpdate(principal.BrokerId, loanId);

                this.SetResult("LoanId", loanId.ToString());
            }
            else
            {
                this.SetResult("Error", string.Join("; ", result.Errors));
            }
        }

        private void GetNumberOfLoanProgramsFor1st()
        {
            E_sLienQualifyModeT sLienQualifyModeT = E_sLienQualifyModeT.ThisLoan;
            Guid firstLienLpId = Guid.Empty;
            Guid loanID = GetGuid("loanID");

            CPageData dataLoan = new CCheckFor2ndLoan(loanID);
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            dataLoan.sProdFilterDue10Yrs = GetBool("sProdFilterDue10Yrs"); 
            dataLoan.sProdFilterDue15Yrs = GetBool("sProdFilterDue15Yrs"); 
            dataLoan.sProdFilterDue20Yrs = GetBool("sProdFilterDue20Yrs");  
            dataLoan.sProdFilterDue25Yrs = GetBool("sProdFilterDue25Yrs"); 
            dataLoan.sProdFilterDue30Yrs = GetBool("sProdFilterDue30Yrs"); 
            dataLoan.sProdFilterDueOther = GetBool("sProdFilterDueOther");
            dataLoan.sProdFilterFinMethFixed = GetBool("sProdFilterFinMethFixed");
            dataLoan.sProdFilterFinMeth3YrsArm = GetBool("sProdFilterFinMeth3YrsArm");
            dataLoan.sProdFilterFinMeth5YrsArm = GetBool("sProdFilterFinMeth5YrsArm");
            dataLoan.sProdFilterFinMeth7YrsArm = GetBool("sProdFilterFinMeth7YrsArm"); 
            dataLoan.sProdFilterFinMeth10YrsArm = GetBool("sProdFilterFinMeth10YrsArm");
            dataLoan.sProdFilterFinMethOther = GetBool("sProdFilterFinMethOther");
            dataLoan.sProdIncludeNormalProc = GetBool("sProdIncludeNormalProc");
            dataLoan.sProdIncludeMyCommunityProc = GetBool("sProdIncludeMyCommunityProc");
            dataLoan.sProdIncludeHomePossibleProc = GetBool("sProdIncludeHomePossibleProc");
            dataLoan.sProdIncludeFHATotalProc = GetBool("sProdIncludeFHATotalProc");
            dataLoan.sProdIncludeVAProc = GetBool("sProdIncludeVAProc");
            dataLoan.sProdIncludeUSDARuralProc = GetBool("sProdIncludeUSDARuralProc");
            dataLoan.sIsRenovationLoan = GetBool("sIsRenovationLoan", false);
            dataLoan.sProdFilterPmtTPI = GetBool("sProdFilterPmtTPI");
            dataLoan.sProdFilterPmtTIOnly = GetBool("sProdFilterPmtTIOnly");

            ProductCodeFilterPopup.BindData(this, dataLoan, (PriceMyLoanPrincipal)Context.User);

            LoanProgramByInvestorSet lpSet;

            try
            {
                lpSet = dataLoan.GetLoanProgramsToRunLpe((PriceMyLoanPrincipal)Context.User, sLienQualifyModeT, firstLienLpId);

                SetResult("NumLPs", lpSet.LoanProgramsCount);
            }
            catch (CLPERunModeException e)
            {
                SetResult("ErrorMessage", e.UserMessage);
                return;
            }
        }

        private void GetOrderCreditPermission()
        {
            var craIdStr = GetString("craId");
            if (craIdStr.Length == 0 || craIdStr == Guid.Empty.ToString())
            {
                SetResult("Error", "No valid CRA.");
                return;
            }

            var craId = Guid.Parse(craIdStr);
           
            Guid loanId = GetGuid("loanId");

            string reasons;
            var canOrderCredit = Tools.CanOrderCredit((PriceMyLoanPrincipal)Context.User, loanId, craId, out reasons);

            SetResult("CanOrderCredit", canOrderCredit ? "1" : "0");
            SetResult("CanOrderCreditReason", reasons);
        }

        private void GetPinCount()
        {
            Guid sLId = GetGuid("sLId");
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            SetResult("sPinCount", dataLoan.sPinCount);
        }

        private void AddPinState()
        {
            Guid sLId = GetGuid("sLId");
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // OPM 244915. Before applying the fee service for this pin, we need to know if any of
            // the additional monthly housing expenses that can be set in PML were set at the time.
            // (Would like to put this and the fee service call deeper, but Smartdependency will not
            // know to load the fields.  PMLService type loads every field any of these service
            // methods might need, so it might be a good idea to migrate away.)
            var sProRealETxLocked = dataLoan.sProRealETx != 0;
            var sProHazInsLocked = dataLoan.sProHazIns != 0;
            var sProHoAssocDuesLocked = dataLoan.sProHoAssocDues != 0;
            var sProOHExpLocked = dataLoan.sProOHExp != 0;

            ILoanProgramTemplate productData = LendersOfficeApp.los.RatePrice.Utils.RetrieveLoanProgramTemplate(dataLoan.BrokerDB, GetGuid("lLpTemplateId"));

            ILoanProgramTemplate secondProductData = null;
            Guid secondLoanTemplateId = GetGuid("secondlLpTemplateId", Guid.Empty);
            string secondSelectedRate = GetString("secondSelectedRate", null);
            string secondLoanMonthlyPayment = GetString("secondLoanMonthlyPayment", null);

            if (secondLoanTemplateId != Guid.Empty && !string.IsNullOrEmpty(secondSelectedRate))
            {
                secondProductData = LendersOfficeApp.los.RatePrice.Utils.RetrieveLoanProgramTemplate(dataLoan.BrokerDB, secondLoanTemplateId);
            }

            try
            {
                // The fee service can affect values that are used in the Loan Comparison.  It must be applied.
                dataLoan.ApplyFeeService(productData);

                Guid id = dataLoan.AddPricingState(
                    productData,
                    GetInt("IsRateMerge") == 1,
                    GetString("SelectedRate"),
                    sProRealETxLocked,
                    sProHazInsLocked,
                    sProHoAssocDuesLocked,
                    sProOHExpLocked,
                    secondProductData,
                    secondSelectedRate,
                    secondLoanMonthlyPayment
                    );

                // We are only saving the pricing state.  Saving the loan after applying the Fee Service would save unwanted data.
                dataLoan.SavePricingStates();

                SetResult("NewPinId", id);
            }
            catch (CBaseException ex)
            {
                SetResult("Error", ex.UserMessage);
            }

            SetResult("sPinCount", dataLoan.sPinCount);
        }

        public void DeletePinState()
        {
            Guid sLId = GetGuid("sLId");
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.DeletePricingState(GetGuid("PinId"));
            dataLoan.Save();
            SetResult("sPinCount", dataLoan.sPinCount);
        }

        private void SavePrimaryAgent(
            CPageData dataLoan,
            E_AgentRoleT agentRoleT,
            Guid newEmployeeId,
            Guid existingEmployeeId,
            System.Action assignNewEmployeeId)
        {
            if (newEmployeeId == Guid.Empty)
            {
                return;
            }

            bool assignBranch = false;

            var agent = dataLoan.GetAgentOfRole(
                agentRoleT,
                E_ReturnOptionIfNotExist.CreateNew);
            assignBranch = existingEmployeeId != newEmployeeId;
            assignNewEmployeeId();

            if (assignBranch)
            {
                this.AssignBranch(newEmployeeId, dataLoan);
            }

            dataLoan.RecalculateTpoValue();
            SetAgentData(agent, newEmployeeId);
            agent.Update();
        }

        private void SaveAgent(
            CPageData dataLoan,
            E_AgentRoleT roleT,
            Guid newEmployeeId,
            System.Action assignEmployeeToLoan)
        {
            if (newEmployeeId == Guid.Empty)
            {
                return;
            }

            var agent = dataLoan.GetAgentOfRole(
                roleT,
                E_ReturnOptionIfNotExist.CreateNew);
            assignEmployeeToLoan();
            SetAgentData(agent, newEmployeeId);
            agent.Update();
        }

        private void AssignBranch(Guid employeeID, CPageData dataLoan)
        {
            if (employeeID == Guid.Empty)
            {
                return;
            }

            var emp = new EmployeeDB(employeeID, ((PriceMyLoanPrincipal)Context.User).BrokerId);
            emp.Retrieve();

            Guid branchToAssign;

            if (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                if (dataLoan.sCorrespondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                {
                    branchToAssign = emp.MiniCorrespondentBranchID;
                }
                else
                {
                    branchToAssign = emp.CorrespondentBranchID;
                }
            }
            else
            {
                branchToAssign = emp.BranchID;
            }

            if (branchToAssign != Guid.Empty)
            {
                dataLoan.AssignBranch(branchToAssign);
            }
        }

        private void SaveAgentsForCorrespondentModes(
            CPageData dataLoan,
            Guid newSecondaryId,
            Guid newPostCloserId,
            Guid newLoanOfficerId,
            Guid newProcessorId)
        {
            this.SavePrimaryAgent(
                dataLoan,
                E_AgentRoleT.ExternalSecondary,
                newSecondaryId,
                dataLoan.sEmployeeExternalSecondaryId,
                () => dataLoan.sEmployeeExternalSecondaryId = newSecondaryId);

            if (!PriceMyLoanConstants.IsEmbeddedPML)
            {
                this.SaveAgent(
                    dataLoan,
                    E_AgentRoleT.ExternalPostCloser,
                    newPostCloserId,
                    () => dataLoan.sEmployeeExternalPostCloserId = newPostCloserId);

                this.SaveAgent(
                    dataLoan,
                    E_AgentRoleT.LoanOfficer,
                    newLoanOfficerId,
                    () => dataLoan.sEmployeeLoanRepId = newLoanOfficerId);

                this.SaveAgent(
                    dataLoan,
                    E_AgentRoleT.BrokerProcessor,
                    newProcessorId,
                    () => dataLoan.sEmployeeBrokerProcessorId = newProcessorId);
            }
        }

        private void SaveAgentsForNonCorrespondentModes(
            CPageData dataLoan,
            Guid newLoanOfficerId,
            Guid newProcessorId)
        {
            this.SavePrimaryAgent(
                dataLoan,
                E_AgentRoleT.LoanOfficer,
                newLoanOfficerId,
                dataLoan.sEmployeeLoanRepId,
                () => dataLoan.sEmployeeLoanRepId = newLoanOfficerId);

            if (!PriceMyLoanConstants.IsEmbeddedPML)
            {
                this.SaveAgent(
                    dataLoan,
                    E_AgentRoleT.BrokerProcessor,
                    newProcessorId,
                    () => dataLoan.sEmployeeBrokerProcessorId = newProcessorId);
            }
        }

        /// <summary>
        /// Currently assumes you cannot clear assignments in PML.
        /// </summary>
        private void SaveAgents()
        {
            // We can't put this in BindLoan because the agent fields REQUIRE initsave
            Guid sLId = GetGuid("sLId");
            Guid newLoanOfficerId = GetGuid("LoanOfficer_EmployeeId", Guid.Empty);
            Guid newProcessorId = GetGuid("Processor_EmployeeId", Guid.Empty);
            Guid newSecondaryId = GetGuid("Secondary_EmployeeId", Guid.Empty);
            Guid newPostCloserId = GetGuid("PostCloser_EmployeeId", Guid.Empty);

            if (Guid.Empty == newLoanOfficerId && Guid.Empty == newProcessorId &&
                Guid.Empty == newSecondaryId && Guid.Empty == newPostCloserId)
            {
                return;
            }
            
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                this.SaveAgentsForCorrespondentModes(
                    dataLoan,
                    newSecondaryId,
                    newPostCloserId,
                    newLoanOfficerId,
                    newProcessorId);
            }
            else
            {
                this.SaveAgentsForNonCorrespondentModes(
                    dataLoan,
                    newLoanOfficerId,
                    newProcessorId);
            }

            dataLoan.Save();

            //OPM 110135: Price Group might change when officer is updated; PML needs the new value
            dataLoan.InitLoad();
            if(dataLoan.BrokerDB.IsShowPriceGroupInEmbeddedPml)
            {
                SetResult("sProdLpePriceGroupNm", dataLoan.sProdLpePriceGroupNm);
            }
        }

        /// <summary>
        /// Saves retail agent data.
        /// </summary>
        private void SaveRetailAgent()
        {
            var loanId = this.GetGuid("loanId");
            var newEmployeeId = this.GetGuid("employeeId");
            var roleId = this.GetEnum<E_RoleT>("roleId");

            var loan = CPageData.CreatePmlPageDataUsingSmartDependency(loanId, typeof(PMLService));
            loan.AllowSaveWhileQP2Sandboxed = true;
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            switch (roleId)
            {
                case E_RoleT.LoanOfficer:
                    this.SavePrimaryAgent(loan, E_AgentRoleT.LoanOfficer, newEmployeeId, loan.sEmployeeLoanRepId, () => loan.sEmployeeLoanRepId = newEmployeeId);
                    break;
                case E_RoleT.JuniorProcessor:
                    this.SaveAgent(loan, E_AgentRoleT.JuniorProcessor, newEmployeeId, () => loan.sEmployeeJuniorProcessorId = newEmployeeId);
                    break;
                case E_RoleT.Processor:
                    this.SaveAgent(loan, E_AgentRoleT.Processor, newEmployeeId, () => loan.sEmployeeProcessorId = newEmployeeId);
                    break;
                case E_RoleT.JuniorUnderwriter:
                    this.SaveAgent(loan, E_AgentRoleT.JuniorUnderwriter, newEmployeeId, () => loan.sEmployeeJuniorUnderwriterId = newEmployeeId);
                    break;
                case E_RoleT.Underwriter:
                    this.SaveAgent(loan, E_AgentRoleT.Underwriter, newEmployeeId, () => loan.sEmployeeUnderwriterId = newEmployeeId);
                    break;
                case E_RoleT.Manager:
                    this.SaveAgent(loan, E_AgentRoleT.Manager, newEmployeeId, () => loan.sEmployeeManagerId = newEmployeeId);
                    break;
                default:
                    throw new UnhandledEnumException(roleId);
            }

            loan.Save();
        }

        protected void CreateApp()
        {
            Guid sLId = GetGuid("sLId");
            int numApps = GetInt("numApps");
            int appNum;

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            if (numApps == 0) // Then we are pretending that app 0 does not exist. Unpretend.
            {
                appNum = 0;
            }
            else
            {
                appNum = dataLoan.AddNewApp();
            }
            dataLoan.InitLoad(); // Populate the App's default data
            var dataApp = dataLoan.GetAppData(appNum);
            var app = LoadApp(dataLoan.sBrokerId, dataApp);

            if (appNum == 0)
            {
                // These values need to be changed from the auto-calculated version
                // because sHasAnyAppsFilled will still be false, but now that the app tab
                // will be shown we want to provide the default values.
                
                if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V1_CalcVaLoanElig_Opm204251))
                {
                    app["aIsBVAElig"] = false.ToString();
                }

                app["aBVServiceT"] = E_aVServiceT.Blank.ToString("d");
                app["aBVEnt"] = E_aVEnt.Blank.ToString("d");

                // Import total liquid assets and monthly income for first application 
                // from the property and loan info tab.
                if (dataLoan.sAppTotLiqAssetExists) 
                {
                    app["aLiquidAssetsPe"] = dataLoan.sAppTotLiqAsset_rep;
                }
                app["aBMonInc"] = dataLoan.sPrimAppTotNonspIPe_rep;

                app["aOpNegCfPe"] = dataLoan.sOpNegCfPe_rep;
            }

            app["aBHasSpouse"] = false.ToString();


            // Always want the monthly income to be blank on app creation

            var serializedApp = ObsoleteSerializationHelper.JavascriptJsonSerialize(app);
            SetResult("app", serializedApp);
            SetResult("appNum", appNum); // The index of the created app
        }

        protected void DeleteApp()
        {
            Guid sLId = GetGuid("sLId");
            int appNum = GetInt("appNum");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.InitSave(sFileVersion);

            bool success = false;
            if (dataLoan.nApps == 1 && appNum == 0)
            {
                // Pretend that app 0 does not exist by removing the first borrower info.
                CAppData dataApp = dataLoan.GetAppData(0);
                // Fully clear out the borrower and coborrower information.
                dataApp.aBFirstNm = "";
                dataApp.aBMidNm = "";
                dataApp.aBLastNm = "";
                dataApp.aBSuffix = "";
                dataApp.aBSsn = "";
                dataApp.aProdBCitizenT = E_aProdCitizenT.USCitizen;
                dataApp.aBEmail = "";
                // Don't want to reset the aBBaseIPe_rep, as this will wipe out the 
                // value stored in the Property and Loan Info tab.
                dataApp.aBIsSelfEmplmt = false;
                dataApp.aBTotalScoreIsFthb = false;
                dataApp.aBHasHousingHist = true;
                if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V1_CalcVaLoanElig_Opm204251))
                {
                    dataApp.aIsBVAElig = false;
                    dataApp.aIsCVAElig = false;
                }
                dataApp.aIsBVAFFEx = false;
                dataApp.aBVServiceT = E_aVServiceT.Blank;
                dataApp.aBVEnt = E_aVEnt.Blank;
                dataApp.aBExperianScorePe_rep = dataLoan.sCreditScoreEstimatePe_rep;
                dataApp.aBTransUnionScorePe_rep = dataLoan.sCreditScoreEstimatePe_rep;
                dataApp.aBEquifaxScorePe_rep = dataLoan.sCreditScoreEstimatePe_rep;
                // Coborrower
                dataApp.aCFirstNm = "";
                dataApp.aCMidNm = "";
                dataApp.aCLastNm = "";
                dataApp.aCSuffix = "";
                dataApp.aCSsn = "";
                dataApp.aProdCCitizenT = E_aProdCitizenT.USCitizen;
                dataApp.aCEmail = "";
                dataApp.aCTotIPe_rep = "";
                dataApp.aCIsSelfEmplmt = false;
                dataApp.aCTotalScoreIsFthb = false;
                dataApp.aCHasHousingHist = true;
                dataApp.aIsCVAFFEx = false;
                dataApp.aCVServiceT = E_aVServiceT.Blank;
                dataApp.aCVEnt = E_aVEnt.Blank;
                dataApp.aCExperianScorePe = 0;
                dataApp.aCTransUnionScorePe = 0;
                dataApp.aCEquifaxScorePe = 0;

                // General
                dataApp.aOpNegCfPe_rep = "";

                dataLoan.Save();
                success = true;
            }
            else
            {
                try
                {
                    success = dataLoan.DelApp(appNum);
                }
                catch (DeleteApplicationPermissionDenied exc)
                {
                    SetResult("Error", exc.UserMessage);
                    success = false;
                }
            }
            SetResult("success", success);

            return;
        }

        protected void SaveApp()
        {
            Guid sLId = GetGuid("sLId");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.InitSave(sFileVersion);

            BindApp(dataLoan);

            dataLoan.Save();
        }

        private BrokerDB x_brokerDB = null;
        private BrokerDB CurrentBroker
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(((PriceMyLoanPrincipal)Context.User).BrokerId);
                }

                return x_brokerDB;
            }
        }

        /// <summary>
        /// Gathers UI info, calls LeadConversion with those parameters, sets UI info.
        /// </summary>
        private void ConvertLeadToLoan(bool convertFromQP2File, bool convertToLead)
        {
            var parameters = new LeadConversionParameters();
            parameters.LeadId = GetGuid("LeadID");
            parameters.FileVersion = GetInt("FileVersion");
            parameters.TemplateId = GetGuid("TemplateId", Guid.Empty);
            parameters.CreateFromTemplate = GetBool("ConvertWithTemplate");
            parameters.RemoveLeadPrefix = GetBool("RemoveLeadPrefix");
            parameters.ConvertFromQp2File = convertFromQP2File;
            parameters.ConvertToLead = convertToLead;
            parameters.IsEmbeddedPML = PriceMyLoanConstants.IsEmbeddedPML;
            parameters.IsNonQm = false;

            var results = new LeadConversionResults();

            try
            {
                results = LeadConversion.ConvertLeadToLoan_Pml(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, parameters);
            }
            catch (SystemException e)
            {
                Tools.LogError("Error while converting lead to loan", e);
                SetResult("Message", ErrorMessages.UnableToConvertLead);
                return;
            }

            if (!string.IsNullOrEmpty(results.MersErrors))
            {
                SetResult("MersErrors", results.MersErrors);
            }

            if (results.ConvertedLoanId == Guid.Empty)
            {
                SetResult("Error", true);
                SetResult("Message", results.Message);
                return;
            }

            SetResult("Error", false);
            SetResult("LoanId", results.ConvertedLoanId.ToString());

            if (results.Message != null)
            {
                SetResult("Message", results.Message);
                SetResult("Warning", true);
            }
        }

        protected void BindLoan(CPageData dataLoan)
        {
            dataLoan.sProdCalcEntryT = (E_sProdCalcEntryT)GetInt("sProdCalcEntryT");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpStatePe = GetString("sSpStatePe");
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sIsRenovationLoan = GetBool("sIsRenovationLoan");
            dataLoan.sLPurposeTPe = (E_sLPurposeT)GetInt("sLPurposeTPe", (int)dataLoan.sLPurposeTPe);

            dataLoan.sRequestCommunityOrAffordableSeconds = GetBool("sRequestCommunityOrAffordableSeconds");

            if (dataLoan.sIsRenovationLoan)
            {
                if (dataLoan.BrokerDB.EnableRenovationLoanSupport && 
                    dataLoan.sLPurposeTPe.EqualsOneOf(E_sLPurposeT.Purchase, E_sLPurposeT.Construct, E_sLPurposeT.ConstructPerm))
                {
                    dataLoan.sInducementPurchPrice_rep = GetString("sInducementPurchPrice");
                }

                dataLoan.sTotalRenovationCostsLckd = GetBool("sTotalRenovationCostsLckd");
                dataLoan.sTotalRenovationCosts_rep = GetString("sTotalRenovationCosts");

                if (dataLoan.sTotalRenovationCostsLckd)
                {
                    // Users manually enter renovation costs when sTotalRenovationCosts is locked, causing a potential
                    // mismatch between the pricing engine value and the manually-entered value.
                    // Update the value here, similar to how the renovation costs popup syncs
                    // data when the user saves the more detailed cost breakdown.
                    dataLoan.SyncTotalRenovationCostSnapshot();
                }
            }

            dataLoan.sOriginalAppraisedValue_rep = GetString("sOriginalAppraisedValue");

            if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
            {
                if (CurrentBroker.AddLeadSourceDropdownToInternalQuickPricer && PriceMyLoanConstants.IsEmbeddedPML)
                {
                    dataLoan.sLeadSrcDesc = GetString("sLeadSrcDesc");
                }

                if (ConstStage.AllowLoanPurposeToggleInQP2 && false == dataLoan.sIsLineOfCredit) // if the toggle is visible.
                {
                    var sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT", (int)dataLoan.sLPurposeT);
                    if (sLPurposeT == E_sLPurposeT.Purchase)
                    {
                        dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
                        dataLoan.sLPurposeTPe = E_sLPurposeT.Purchase;
                    }
                    else
                    {
                        dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeTPe", (int)dataLoan.sLPurposeTPe);
                        dataLoan.sLPurposeTPe = (E_sLPurposeT)GetInt("sLPurposeTPe", (int)dataLoan.sLPurposeTPe);
                    }
                }
            }

            dataLoan.sIsStudentLoanCashoutRefi = GetBool("sIsStudentLoanCashoutRefi");
            dataLoan.sProdCashoutAmt_rep = GetString("sProdCashoutAmt");
            dataLoan.sProdCurrPIPmt_rep = GetString("sProdCurrPIPmt");
            dataLoan.sProdCurrPIPmtLckd = GetBool("sProdCurrPIPmtLckd");
            dataLoan.sProdCurrMIPMo_rep = GetString("sProdCurrMIPMo");

            dataLoan.sApprValPe_rep = GetString("sApprValPe");
            dataLoan.sHouseValPe_rep = GetString("sHouseValPe");

            dataLoan.sAsIsAppraisedValuePeval_rep = GetString("sAsIsAppraisedValuePeval");
            dataLoan.sDownPmtPcPe_rep = GetString("sDownPmtPcPe");
            dataLoan.sEquityPe_rep = GetString("sEquityPe");
            dataLoan.sLtvRPe_rep = GetString("sLtvRPe");
            dataLoan.sLAmtCalcPe_rep = GetString("sLAmtCalcPe");
            dataLoan.sLtvROtherFinPe_rep = GetString("sLtvROtherFinPe");
            dataLoan.sProOFinBalPe_rep = GetString("sProOFinBalPe");
            dataLoan.sHas2ndFinPe = GetBool("sHasSecondFinancingT");
            dataLoan.sProdSpT = (E_sProdSpT)GetInt("sProdSpt");
            dataLoan.sHomeIsMhAdvantageTri = this.GetEnum<E_TriState>("sHomeIsMhAdvantageTri");
            dataLoan.sIsNotPermanentlyAffixed = GetBool("sIsNotPermanentlyAffixed");
            
            dataLoan.sHasNonOccupantCoborrowerPe = GetBool("sHasNonOccupantCoborrowerPe");
            dataLoan.sIsNewConstruction = GetBool("sIsNewConstruction");
            dataLoan.sProdImpound = GetBool("sProdImpound");
            dataLoan.sCreditScoreEstimatePe_rep = GetString("sCreditScoreEstimatePe");
            dataLoan.sPrimAppTotNonspIPe_rep = GetString("sPrimAppTotNonspIPe");
            
            dataLoan.sAppTotLiqAsset_rep = GetString("sAppTotLiqAsset");
            
            dataLoan.sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)GetInt("sOriginatorCompensationPaymentSourceT");
            dataLoan.sOriginatorCompensationBorrPaidPc_rep = GetString("sOriginatorCompensationBorrPaidPc");
            dataLoan.sOriginatorCompensationBorrPaidBaseT = (E_PercentBaseT)GetInt("sOriginatorCompensationBorrPaidBaseT");
            dataLoan.sOriginatorCompensationBorrPaidMb_rep = GetString("sOriginatorCompensationBorrPaidMb");
            dataLoan.sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT)GetInt("sProd3rdPartyUwResultT");
            dataLoan.sProdIsDuRefiPlus = GetBool("sProdIsDuRefiPlus");
            dataLoan.sProdRLckdDays_rep = GetString("sProdRLckdDays");
            dataLoan.sOccTPe = (E_sOccT)GetInt("sOccTPe");
            if (dataLoan.nApps == 1)
            {
                dataLoan.GetAppData(0).aOccT = (E_aOccT)GetInt("sOccTPe");
            }

            // "calculate" link fields
            dataLoan.sProMInsPe_rep = GetString("sProMInsPe");

            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sProdIsSpInRuralArea = GetBool("sProdIsSpInRuralArea");
            dataLoan.sSpGrossRentPe_rep = GetString("sSpGrossRentPe");
            dataLoan.sOccRPe_rep = GetString("sOccRPe");
            dataLoan.sProdIsNonwarrantableProj = GetBool("sProdIsNonwarrantableProj");
            dataLoan.sProdIsCondotel = GetBool("sProdIsCondotel");
            dataLoan.sFhaCondoApprovalStatusT = (E_sFhaCondoApprovalStatusT)GetInt("sFhaCondoApprovalStatusT");
            dataLoan.sProdCondoStories_rep = GetString("sProdCondoStories");
            dataLoan.sIsCreditQualifying = GetBool("sIsCreditQualifying");
            dataLoan.sProdSpStructureT = (E_sProdSpStructureT)GetInt("sProdSpStructureT");
            dataLoan.sHasAppraisal = GetBool("sHasAppraisal");
            dataLoan.sSpLien_rep = GetString("sSpLien");
            dataLoan.sFHASalesConcessions_rep = GetString("sFHASalesConcessions");
            dataLoan.sSellerCreditT = (E_SellerCreditT)GetInt("sSellerCreditT");
            dataLoan.sProdDocT = (E_sProdDocT)GetInt("sProdDocT");
            dataLoan.sIsOFinNewPe = GetBool("sIsOFinNewPe");
            dataLoan.sSubFinT = (E_sSubFinT)GetInt("sSubFinT");
            dataLoan.sCltvRPe_rep = GetString("sCltvRPe");
            dataLoan.sSubFinPe_rep = GetString("sSubFinPe");
            // OPM 107986 - sSubFinToPropertyValue has to be set after sSubFinPe_rep for calculation to work as expected.
            dataLoan.sSubFinToPropertyValue_rep = GetString("sSubFinToPropertyValue");
            dataLoan.sHCLTVRPe_rep = GetString("sHCLTVRPe");
            dataLoan.sNumFinancedProperties_rep = GetString("sNumFinancedProperties");
            dataLoan.sPriorSalesD_rep = GetString("sPriorSalesD");
            dataLoan.sPriorSalesPrice_rep = GetString("sPriorSalesPrice");
            dataLoan.sPriorSalesPropertySellerT = (E_sPriorSalesPropertySellerT)GetInt("sPriorSalesSellerT", (int)dataLoan.sPriorSalesPropertySellerT);
            dataLoan.sProdConvMIOptionT = (E_sProdConvMIOptionT)GetInt("sProdConvMIOptionT");
            dataLoan.sConvSplitMIRT = (E_sConvSplitMIRT)GetInt("sConvSplitMIRT");
            dataLoan.sProdIsUFMIPFFFinanced = GetBool("sProdIsUFMIPFFFinanced");
            dataLoan.sProdOverrideUFMIPFF = GetBool("sProdOverrideUFMIPFF");
            // The following 4 fields depend on the value of sProdOverrideUFMIPFF
            dataLoan.sProdIsLoanEndorsedBeforeJune09 = GetBool("sProdIsLoanEndorsedBeforeJune09");
            dataLoan.sProdFhaUfmip_rep = GetString("sProdFhaUfmip");
            dataLoan.sProdVaFundingFee_rep = GetString("sProdVaFundingFee");
            dataLoan.sProdUSDAGuaranteeFee_rep = GetString("sProdUSDAGuaranteeFee");
            if (dataLoan.sOccTPe != E_sOccT.PrimaryResidence) // sk 107305, shouldn't be setting sPresOHExpPe if it's not visible.
            {
                if (dataLoan.sPresOHExpPe_rep != GetString("sPresOHExpPe"))
                {
                    dataLoan.sPresOHExpPe_rep = GetString("sPresOHExpPe");
                }
            }

            dataLoan.sTitleInsuranceCostT = (E_sTitleInsuranceCostT)GetInt("sTitleInsuranceCostT");
            dataLoan.sOwnerTitleInsFPe_rep = GetString("sOwnerTitleInsFPe");

            // Results Filter
            dataLoan.sProdFilterDue10Yrs = GetBool("sProdFilterDue10Yrs");
            dataLoan.sProdFilterDue15Yrs = GetBool("sProdFilterDue15Yrs");
            dataLoan.sProdFilterDue20Yrs = GetBool("sProdFilterDue20Yrs");
            dataLoan.sProdFilterDue25Yrs = GetBool("sProdFilterDue25Yrs");
            dataLoan.sProdFilterDue30Yrs = GetBool("sProdFilterDue30Yrs");
            dataLoan.sProdFilterDueOther = GetBool("sProdFilterDueOther");
            dataLoan.sProdFilterFinMethFixed = GetBool("sProdFilterFinMethFixed");
            dataLoan.sProdFilterFinMeth3YrsArm = GetBool("sProdFilterFinMeth3YrsArm");
            dataLoan.sProdFilterFinMeth5YrsArm = GetBool("sProdFilterFinMeth5YrsArm");
            dataLoan.sProdFilterFinMeth7YrsArm = GetBool("sProdFilterFinMeth7YrsArm");
            dataLoan.sProdFilterFinMeth10YrsArm = GetBool("sProdFilterFinMeth10YrsArm");
            dataLoan.sProdFilterFinMethOther = GetBool("sProdFilterFinMethOther");
            dataLoan.sProdIncludeNormalProc = GetBool("sProdIncludeNormalProc");
            dataLoan.sProdIncludeMyCommunityProc = GetBool("sProdIncludeMyCommunityProc");
            dataLoan.sProdIncludeHomePossibleProc = GetBool("sProdIncludeHomePossibleProc");
            dataLoan.sProdIncludeFHATotalProc = GetBool("sProdIncludeFHATotalProc");
            dataLoan.sProdIncludeVAProc = GetBool("sProdIncludeVAProc");
            dataLoan.sProdIncludeUSDARuralProc = GetBool("sProdIncludeUSDARuralProc");
            dataLoan.sProdFilterPmtTPI = GetBool("sProdFilterPmtTPI");
            dataLoan.sProdFilterPmtTIOnly = GetBool("sProdFilterPmtTIOnly");

            ProductCodeFilterPopup.BindData(this, dataLoan, (PriceMyLoanPrincipal)Context.User);
            
            // Agents saved in SaveAgents method

            // Custom PML Fields
            dataLoan.sCustomPMLField1_rep = GetString("sCustomPMLField1", "");
            dataLoan.sCustomPMLField2_rep = GetString("sCustomPMLField2", "");
            dataLoan.sCustomPMLField3_rep = GetString("sCustomPMLField3", "");
            dataLoan.sCustomPMLField4_rep = GetString("sCustomPMLField4", "");
            dataLoan.sCustomPMLField5_rep = GetString("sCustomPMLField5", "");
            dataLoan.sCustomPMLField6_rep = GetString("sCustomPMLField6", "");
            dataLoan.sCustomPMLField7_rep = GetString("sCustomPMLField7", "");
            dataLoan.sCustomPMLField8_rep = GetString("sCustomPMLField8", "");
            dataLoan.sCustomPMLField9_rep = GetString("sCustomPMLField9", "");
            dataLoan.sCustomPMLField10_rep = GetString("sCustomPMLField10", "");
            dataLoan.sCustomPMLField11_rep = GetString("sCustomPMLField11", "");
            dataLoan.sCustomPMLField12_rep = GetString("sCustomPMLField12", "");
            dataLoan.sCustomPMLField13_rep = GetString("sCustomPMLField13", "");
            dataLoan.sCustomPMLField14_rep = GetString("sCustomPMLField14", "");
            dataLoan.sCustomPMLField15_rep = GetString("sCustomPMLField15", "");
            dataLoan.sCustomPMLField16_rep = GetString("sCustomPMLField16", "");
            dataLoan.sCustomPMLField17_rep = GetString("sCustomPMLField17", "");
            dataLoan.sCustomPMLField18_rep = GetString("sCustomPMLField18", "");
            dataLoan.sCustomPMLField19_rep = GetString("sCustomPMLField19", "");
            dataLoan.sCustomPMLField20_rep = GetString("sCustomPMLField20", "");

            dataLoan.sHorizonOfBorrowerInterestInMonths_rep = GetString("sHorizonOfBorrowerInterestInMonths");

            // OPM 107986 - Add support for standalone second liens.
            dataLoan.sProOFinPmtPe_rep = GetString("sProOFinPmtPe");
            dataLoan.sLpIsNegAmortOtherLien = GetBool("sLpIsNegAmortOtherLien");
            dataLoan.sOtherLFinMethT = (E_sFinMethT)GetInt("sOtherLFinMethT");
            if (dataLoan.sLienPosT == E_sLienPosT.First)
            {
                dataLoan.sCreditLineAmt_rep = GetString("sCreditLineAmt");
            }

            dataLoan.sProdIsTexas50a6Loan = GetBool("sProdIsTexas50a6Loan");
            dataLoan.sPreviousLoanIsTexas50a6Loan = GetBool("sPreviousLoanIsTexas50a6Loan");

            if (dataLoan.BrokerDB.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT))
            {
                dataLoan.sLenderFeeBuyoutRequestedT = (E_sLenderFeeBuyoutRequestedT)GetInt("sLenderFeeBuyoutRequestedT");
            }

            var serializedCompensationPlan = GetString("sOriginatorCompensationPlan");

            if (!string.IsNullOrEmpty(serializedCompensationPlan))
            {
                this.lenderPaidOriginatorCompPlan = ObsoleteSerializationHelper.JavascriptJsonDeserializer<OriginatorCompensationPlan>(serializedCompensationPlan);
            }

            var sCorrespondentProcessT = GetString("sCorrespondentProcessT");
            if (!string.IsNullOrEmpty(sCorrespondentProcessT))
            {
                dataLoan.sCorrespondentProcessT = (E_sCorrespondentProcessT)Int32.Parse(sCorrespondentProcessT);
            }

            var sGfeIsTPOTransaction = GetString("sGfeIsTPOTransaction");
            if (!string.IsNullOrEmpty(sGfeIsTPOTransaction))
            {
                dataLoan.sGfeIsTPOTransaction = GetBool("sGfeIsTPOTransaction");
            }
        }

        private void SetAgentData(CAgentFields agent, Guid employeeId)
        {
            agent.ClearExistingData();

            if (employeeId == Guid.Empty)
            {
                //no need to look up
                return;
            }
            CommonFunctions.CopyEmployeeInfoToAgent(((PriceMyLoanPrincipal)Context.User).BrokerId, agent, employeeId);
            return;
        }

        private void UpdateRateLockForDDL(BrokerDB CurrentBroker, CPageData dataLoan)
        {
            // If the current rate lock is 0, but 0 is not an option for the broker
            // let it be 0 and have the validation catch it.
            if (dataLoan.sProdRLckdDays == 0)
            {
                return;
            }

            string[] options = CurrentBroker.AvailableLockPeriodOptionsCSV.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
            var numericOptions = from s in options select UInt32.Parse(s);
            var sortedOptions = from n in numericOptions orderby n select n;

            uint sProdRLckdDaysValue = (uint)dataLoan.sProdRLckdDays;
            foreach (uint option in sortedOptions)
            {
                if (option >= sProdRLckdDaysValue)
                {
                    dataLoan.sProdRLckdDays = (int)option;
                    break;
                }
            }
        }

        protected Dictionary<string, string> LoadLoan(CPageData dataLoan)
        {
            var loanData = new Dictionary<string, string>();
            loanData.Add("sLNm", dataLoan.sLNm);
            // This check is in place to honor the "Allow running PML without credit report" permission
            loanData.Add("sAllAppsHasCreditReportOnFile", dataLoan.sAllAppsHasCreditReportOnFile ? "1" : "0");
            
            loanData.Add("sIsRefinancing", dataLoan.sIsRefinancing.ToString());

            // Want the value that was set in LO
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            loanData.Add("sLPurposeT", dataLoan.sLPurposeT.ToString("d"));
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            // Certain fields/options will need to be hidden on production until working correctly.
            // This is a temporary hack for determining if the code is being executed on author.
            loanData.Add("IsProduction", Tools.IsNonAuthorProduction.ToString());

            loanData.Add("UseBpmiSingle", CurrentBroker.EnabledBPMISinglePremium_Case115836.ToString() );
            loanData.Add("UseBpmiSplit", CurrentBroker.EnableBPMISplitPremium_Case115836.ToString() );


            // If they have the rate lock DDL enabled, then the rate lock value needs
            // to be migrated to the smallest larger value
            loanData.Add("IsEnabledLockPeriodDropDown", CurrentBroker.IsEnabledLockPeriodDropDown.ToString());
            if (CurrentBroker.IsEnabledLockPeriodDropDown)
            {
                UpdateRateLockForDDL(CurrentBroker, dataLoan);
                loanData.Add("AvailableLockPeriodOptionsCSV", CurrentBroker.AvailableLockPeriodOptionsCSV);
            }
            loanData.Add("sProdRLckdDays", dataLoan.sProdRLckdDays_rep);
            loanData.Add("sProdRLckdExpiredDLabel", dataLoan.sProdRLckdExpiredDLabel);

            loanData.Add("sIsStudentLoanCashoutRefi", dataLoan.sIsStudentLoanCashoutRefi.ToString());
            loanData.Add("sSpAddr", dataLoan.sSpAddr);
            loanData.Add("sSpZip", dataLoan.sSpZip);
            loanData.Add("sSpStatePe", dataLoan.sSpStatePe); 
            loanData.Add("sSpCounty", dataLoan.sSpCounty);
            loanData.Add("sSpCity", dataLoan.sSpCity);
            loanData.Add("sProdIsSpInRuralArea", dataLoan.sProdIsSpInRuralArea.ToString());
            loanData.Add("sOccTPe", dataLoan.sOccTPe.ToString("d"));
            loanData.Add("sSpGrossRentPe", dataLoan.sSpGrossRentPe_rep);
            loanData.Add("sOccRPe", dataLoan.sOccRPe_rep);
            loanData.Add("sHasNonOccupantCoborrowerPe", dataLoan.sHasNonOccupantCoborrowerPe.ToString());
            loanData.Add("sIsNewConstruction", dataLoan.sIsNewConstruction.ToString());
            loanData.Add("sProdSpT", dataLoan.sProdSpT.ToString("d"));
            loanData.Add("sHomeIsMhAdvantageTri", dataLoan.sHomeIsMhAdvantageTri.ToString("d"));
            loanData.Add("sIsNotPermanentlyAffixed", dataLoan.sIsNotPermanentlyAffixed.ToString());
            loanData.Add("sProdSpStructureT", dataLoan.sProdSpStructureT.ToString("d"));
            loanData.Add("sProdIsNonwarrantableProj", dataLoan.sProdIsNonwarrantableProj.ToString());
            loanData.Add("sProdIsCondotel", dataLoan.sProdIsCondotel.ToString());
            loanData.Add("sFhaCondoApprovalStatusT", dataLoan.sFhaCondoApprovalStatusT.ToString("d"));
            loanData.Add("sProdCondoStories", dataLoan.sProdCondoStories_rep);
            loanData.Add("sRequestCommunityOrAffordableSeconds", dataLoan.sRequestCommunityOrAffordableSeconds.ToString());
            loanData.Add("sIsRenovationLoan", dataLoan.sIsRenovationLoan.ToString());
            loanData.Add("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep);
            loanData.Add("sTotalRenovationCostsLckd", dataLoan.sTotalRenovationCostsLckd.ToString());
            if (dataLoan.BrokerDB.EnableRenovationLoanSupport)
            {
                loanData.Add("sInducementPurchPrice", dataLoan.sInducementPurchPrice_rep);
                loanData.Add("sPurchPriceLessInducement", dataLoan.sPurchPriceLessInducement_rep);
            }
            loanData.Add("sLPurposeTPe", dataLoan.sLPurposeTPe.ToString("d"));
            if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT && 
                CurrentBroker.AddLeadSourceDropdownToInternalQuickPricer &&
                PriceMyLoanConstants.IsEmbeddedPML)
            {
                loanData.Add("sLeadSrcDesc", dataLoan.sLeadSrcDesc);
            }
            loanData.Add("sProdIsLoanEndorsedBeforeJune09", dataLoan.sProdIsLoanEndorsedBeforeJune09.ToString());
            loanData.Add("sProdCashoutAmt", dataLoan.sProdCashoutAmt_rep);
            loanData.Add("sProdCurrPIPmt", dataLoan.sProdCurrPIPmt_rep);
            loanData.Add("sProdCurrPIPmtLckd", dataLoan.sProdCurrPIPmtLckd.ToString());
            loanData.Add("sProdCurrMIPMo", dataLoan.sProdCurrMIPMo_rep);
            loanData.Add("sIsCreditQualifying", dataLoan.sIsCreditQualifying.ToString());
            loanData.Add("sHasAppraisal", dataLoan.sHasAppraisal.ToString());
            loanData.Add("sSpLien", dataLoan.sSpLien_rep);
            loanData.Add("sFHASalesConcessions", dataLoan.sFHASalesConcessions_rep);
            loanData.Add("sSellerCreditT", dataLoan.sSellerCreditT.ToString("d"));
            loanData.Add("sProdImpound", dataLoan.sProdImpound.ToString());
            // Streamline is not a valid option in the DDL, but when sLPurposeT is FHA Streamline
            // or VA IRRRL, the docType is hard-coded to return Streamline. Work around this by 
            // sending the default of Full and make the DDL read-only.
            var docType = (dataLoan.sProdDocT == E_sProdDocT.Streamline) ? E_sProdDocT.Full : dataLoan.sProdDocT;
            loanData.Add("sProdDocT", docType.ToString("d"));
            loanData.Add("sOriginalAppraisedValue", dataLoan.sOriginalAppraisedValue_rep);
            loanData.Add("sApprValPe", dataLoan.sApprValPe_rep);
            loanData.Add("sHouseValPe", dataLoan.sHouseValPe_rep);
            loanData.Add("sAsIsAppraisedValuePeval", dataLoan.sAsIsAppraisedValuePeval_rep);
            loanData.Add("sDownPmtPcPe", dataLoan.sDownPmtPcPe_rep);
            loanData.Add("sEquityPe", dataLoan.sEquityPe_rep);
            loanData.Add("sLtvRPe", dataLoan.sLtvRPe_rep);
            loanData.Add("sLAmtCalcPe", dataLoan.sLAmtCalcPe_rep);

            bool has2nd = dataLoan.sHas2ndFinPe || (dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sSubFinT == E_sSubFinT.Heloc && (dataLoan.sSubFin > 0 || dataLoan.sConcurSubFin > 0)); // OPM 145405 Helocs can have $0 Other Balance
            loanData.Add("sHasSecondFinancingT", has2nd.ToString());
            loanData.Add("sSubFinT", dataLoan.sSubFinT.ToString("d"));
            loanData.Add("sLienPosT", dataLoan.sLienPosT.ToString("d"));
            loanData.Add("sIsOFinNewPe", dataLoan.sIsOFinNewPe.ToString());
            loanData.Add("sLtvROtherFinPe", dataLoan.sLtvROtherFinPe_rep);
            loanData.Add("sProOFinBalPe", dataLoan.sProOFinBalPe_rep);
            loanData.Add("sCltvRPe", dataLoan.sCltvRPe_rep);
            loanData.Add("sSubFinToPropertyValue", dataLoan.sSubFinToPropertyValue_rep);
            loanData.Add("sSubFinPe", dataLoan.sSubFinPe_rep);
            loanData.Add("sHCLTVRPe", dataLoan.sHCLTVRPe_rep);
            loanData.Add("sOriginatorCompensationPaymentSourceT", dataLoan.sOriginatorCompensationPaymentSourceT.ToString("d"));
            loanData.Add("sOriginatorCompensationBorrPaidPc", dataLoan.sOriginatorCompensationBorrPaidPc_rep);
            loanData.Add("sOriginatorCompensationBorrPaidBaseT", dataLoan.sOriginatorCompensationBorrPaidBaseT.ToString("d"));
            loanData.Add("sOriginatorCompensationBorrPaidMb", dataLoan.sOriginatorCompensationBorrPaidMb_rep);
            loanData.Add("sProd3rdPartyUwResultT", dataLoan.sProd3rdPartyUwResultT.ToString("d"));
            loanData.Add("sProdIsDuRefiPlus", dataLoan.sProdIsDuRefiPlus.ToString());
            loanData.Add("sCreditScoreEstimatePe", dataLoan.sCreditScoreEstimatePe_rep);
            loanData.Add("sPrimAppTotNonspIPe", dataLoan.sPrimAppTotNonspIPe_rep);
            loanData.Add("sNumFinancedProperties", dataLoan.sNumFinancedProperties_rep);
            loanData.Add("sCreditScoreType2", dataLoan.sCreditScoreType2_rep);

            
            loanData.Add("sAppTotLiqAsset", dataLoan.sAppTotLiqAsset_rep);
            
            
            loanData.Add("sMonthlyPmtPe", dataLoan.sMonthlyPmtPe_rep);
            string priorSalesD = (dataLoan.sPriorSalesD_rep.TrimWhitespaceAndBOM() == "") ? "mm/dd/yyyy" : dataLoan.sPriorSalesD_rep;
            loanData.Add("sPriorSalesD", priorSalesD);
            loanData.Add("sPriorSalesPrice", dataLoan.sPriorSalesPrice_rep);
            loanData.Add("sPriorSalesSellerT", dataLoan.sPriorSalesPropertySellerT.ToString("d"));
            loanData.Add("sProdConvMIOptionT", dataLoan.sProdConvMIOptionT.ToString("d"));
            loanData.Add("sConvLoanLtvForMiNeed", dataLoan.sConvLoanLtvForMiNeed_rep);
            loanData.Add("sConvSplitMIRT", dataLoan.sConvSplitMIRT.ToString("d"));
            loanData.Add("sProdIsUFMIPFFFinanced", dataLoan.sProdIsUFMIPFFFinanced.ToString());
            loanData.Add("sProdOverrideUFMIPFF", dataLoan.sProdOverrideUFMIPFF.ToString());
            // This auto-calculation is here to avoid affecting how the old PML handled 
            // this value and to avoid a circular dependency when trying to auto-calc for
            // the new UI. When these hard-coded values change, check to see if
            // setter of sProdIsLoanEndorsed needs to be updated as well.
            if (dataLoan.sProdOverrideUFMIPFF)
            {
                loanData.Add("sProdFhaUfmip", dataLoan.sProdFhaUfmip_rep);
            }
            else
            {
                decimal autoCalcFhaUfmip = dataLoan.sProdIsLoanEndorsedBeforeJune09 ? 0.01M : 1.75M;
                loanData.Add("sProdFhaUfmip", m_losConvert.ToRateString(autoCalcFhaUfmip));
            }
            loanData.Add("sProdVaFundingFee", dataLoan.sProdVaFundingFee_rep);
            loanData.Add("sProdUSDAGuaranteeFee", dataLoan.sProdUSDAGuaranteeFee_rep);

            loanData.Add("sPresOHExpPe", dataLoan.sPresOHExpPe_rep);

            bool displayOriginatorCompensation = (CurrentBroker.IsDisplayCompensationChoiceInPml &&
                                                 dataLoan.sIsQualifiedForOriginatorCompensation);
            loanData.Add("sDisplayOriginatorCompensation", displayOriginatorCompensation.ToString());

            // Fields used for "calculate" link
            loanData.Add("sProHoAssocDuesPe", dataLoan.sProHoAssocDuesPe_rep);
            loanData.Add("sProHazInsPe", dataLoan.sProHazInsPe_rep);
            loanData.Add("sProMInsPe", dataLoan.sProMInsPe_rep);
            loanData.Add("sProOHExpDescPe", dataLoan.sProOHExpDesc);
            loanData.Add("sProRealETxPe", dataLoan.sProRealETxPe_rep);
            loanData.Add("sProOHExpPe", dataLoan.sProOHExpPe_rep);   

            loanData.Add("sProdCalcEntryT", dataLoan.sProdCalcEntryT.ToString("d"));
            loanData.Add("sLtv80TestResultT", dataLoan.sLtv80TestResultT.ToString("d"));

            loanData.Add("IsEnableRenovationCheckboxInPML", this.CurrentBroker.IsEnableRenovationCheckboxInPML.ToString());

            loanData.Add("sTitleInsuranceCostT", dataLoan.sTitleInsuranceCostT.ToString("d"));
            loanData.Add("sOwnerTitleInsFPe", dataLoan.sOwnerTitleInsFPe_rep);

            // Results Filter
            loanData.Add("sProdFilterDue10Yrs", dataLoan.sProdFilterDue10Yrs.ToString());
            loanData.Add("sProdFilterDue15Yrs", dataLoan.sProdFilterDue15Yrs.ToString());
            loanData.Add("sProdFilterDue20Yrs", dataLoan.sProdFilterDue20Yrs.ToString());
            loanData.Add("sProdFilterDue25Yrs", dataLoan.sProdFilterDue25Yrs.ToString());
            loanData.Add("sProdFilterDue30Yrs", dataLoan.sProdFilterDue30Yrs.ToString());
            loanData.Add("sProdFilterDueOther", dataLoan.sProdFilterDueOther.ToString());
            loanData.Add("sProdFilterFinMethFixed", dataLoan.sProdFilterFinMethFixed.ToString());
            loanData.Add("sProdFilterFinMeth3YrsArm", dataLoan.sProdFilterFinMeth3YrsArm.ToString());
            loanData.Add("sProdFilterFinMeth5YrsArm", dataLoan.sProdFilterFinMeth5YrsArm.ToString());
            loanData.Add("sProdFilterFinMeth7YrsArm", dataLoan.sProdFilterFinMeth7YrsArm.ToString());
            loanData.Add("sProdFilterFinMeth10YrsArm", dataLoan.sProdFilterFinMeth10YrsArm.ToString());
            loanData.Add("sProdFilterFinMethOther", dataLoan.sProdFilterFinMethOther.ToString());
            loanData.Add("sProdIncludeNormalProc", dataLoan.sProdIncludeNormalProc.ToString());
            loanData.Add("sProdIncludeMyCommunityProc", dataLoan.sProdIncludeMyCommunityProc.ToString());
            loanData.Add("sProdIncludeHomePossibleProc", dataLoan.sProdIncludeHomePossibleProc.ToString());
            loanData.Add("sProdIncludeFHATotalProc", dataLoan.sProdIncludeFHATotalProc.ToString());
            loanData.Add("sProdIncludeVAProc", dataLoan.sProdIncludeVAProc.ToString());
            loanData.Add("sProdIncludeUSDARuralProc", dataLoan.sProdIncludeUSDARuralProc.ToString());
            loanData.Add("sProdFilterPmtTPI", dataLoan.sProdFilterPmtTPI.ToString());
            loanData.Add("sProdFilterPmtTIOnly", dataLoan.sProdFilterPmtTIOnly.ToString());

            ProductCodeFilterPopup.LoadLoan(loanData, dataLoan, (PriceMyLoanPrincipal)Context.User);

            loanData.Add("sStatusT", dataLoan.sStatusT_rep);
            loanData.Add("sFileVersion", ConstAppDavid.SkipVersionCheck.ToString());
            loanData.Add("sPinCount", dataLoan.sPinCount.ToString());

            loanData.Add("LoanOfficer_EmployeeId", dataLoan.sEmployeeLoanRep.EmployeeId.ToString());
            loanData.Add("LoanOfficer_FullName", dataLoan.sEmployeeLoanRep.FullName);
            loanData.Add("Processor_EmployeeId", dataLoan.sEmployeeBrokerProcessor.EmployeeId.ToString());
            loanData.Add("Processor_FullName", dataLoan.sEmployeeBrokerProcessor.FullName);

            loanData.Add("Secondary_EmployeeId", dataLoan.sEmployeeExternalSecondary.EmployeeId.ToString());
            loanData.Add("Secondary_FullName", dataLoan.sEmployeeExternalSecondary.FullName);
            loanData.Add("PostCloser_EmployeeId", dataLoan.sEmployeeExternalPostCloser.EmployeeId.ToString());
            loanData.Add("PostCloser_FullName", dataLoan.sEmployeeExternalPostCloser.FullName);
            
            // Custom PML Fields
            loanData.Add("sCustomPMLField1", dataLoan.sCustomPMLField1_rep);
            loanData.Add("sCustomPMLField2", dataLoan.sCustomPMLField2_rep);
            loanData.Add("sCustomPMLField3", dataLoan.sCustomPMLField3_rep);
            loanData.Add("sCustomPMLField4", dataLoan.sCustomPMLField4_rep);
            loanData.Add("sCustomPMLField5", dataLoan.sCustomPMLField5_rep);
            loanData.Add("sCustomPMLField6", dataLoan.sCustomPMLField6_rep);
            loanData.Add("sCustomPMLField7", dataLoan.sCustomPMLField7_rep);
            loanData.Add("sCustomPMLField8", dataLoan.sCustomPMLField8_rep);
            loanData.Add("sCustomPMLField9", dataLoan.sCustomPMLField9_rep);
            loanData.Add("sCustomPMLField10", dataLoan.sCustomPMLField10_rep);
            loanData.Add("sCustomPMLField11", dataLoan.sCustomPMLField11_rep);
            loanData.Add("sCustomPMLField12", dataLoan.sCustomPMLField12_rep);
            loanData.Add("sCustomPMLField13", dataLoan.sCustomPMLField13_rep);
            loanData.Add("sCustomPMLField14", dataLoan.sCustomPMLField14_rep);
            loanData.Add("sCustomPMLField15", dataLoan.sCustomPMLField15_rep);
            loanData.Add("sCustomPMLField16", dataLoan.sCustomPMLField16_rep);
            loanData.Add("sCustomPMLField17", dataLoan.sCustomPMLField17_rep);
            loanData.Add("sCustomPMLField18", dataLoan.sCustomPMLField18_rep);
            loanData.Add("sCustomPMLField19", dataLoan.sCustomPMLField19_rep);
            loanData.Add("sCustomPMLField20", dataLoan.sCustomPMLField20_rep);

            loanData.Add("sHorizonOfBorrowerInterestInMonths", dataLoan.sHorizonOfBorrowerInterestInMonths_rep);

            if (dataLoan.BrokerDB.IsShowPriceGroupInEmbeddedPml)
                loanData.Add("sProdLpePriceGroupNm", dataLoan.sProdLpePriceGroupNm);

            // OPM 107986 - Add support for standalone second liens.
            loanData.Add("sProOFinPmtPe", dataLoan.sProOFinPmtPe_rep);
            loanData.Add("sLpIsNegAmortOtherLien", dataLoan.sLpIsNegAmortOtherLien.ToString());
            loanData.Add("sOtherLFinMethT", dataLoan.sOtherLFinMethT.ToString("d"));

            loanData.Add("sCreditLineAmt", dataLoan.sCreditLineAmt_rep);

            loanData.Add("sProdIsTexas50a6Loan", dataLoan.sProdIsTexas50a6Loan.ToString());
            loanData.Add("sPreviousLoanIsTexas50a6Loan", dataLoan.sPreviousLoanIsTexas50a6Loan.ToString());

            if (dataLoan.BrokerDB.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT))
            {
                loanData.Add("sLenderFeeBuyoutRequestedT", dataLoan.sLenderFeeBuyoutRequestedT.ToString("d"));
            }

            loanData.Add("sOriginatorCompensationBorrTotalAmount", dataLoan.sOriginatorCompensationBorrTotalAmount_rep);

            if (this.lenderPaidOriginatorCompPlan == null)
            {
                this.lenderPaidOriginatorCompPlan = dataLoan.sLenderPaidOriginatorCompensationPlan;
            }

            decimal lenderPaidCompensation = dataLoan.GetOriginatorCompensationFromPlan(this.lenderPaidOriginatorCompPlan);
            bool borrowerPaidCompExceedsLenderPaidComp = dataLoan.DoesBorrPaidOriginatorCompExceedAmount(lenderPaidCompensation);

            var losConvert = new LosConvert();
            loanData.Add("sOriginatorCompensationPlan", ObsoleteSerializationHelper.JavascriptJsonSerialize(this.lenderPaidOriginatorCompPlan));
            loanData.Add("LenderPaidCompForBorrPaidCompCheck", losConvert.ToMoneyString(lenderPaidCompensation, FormatDirection.ToRep));
            loanData.Add("BorrowerPaidCompExceedsLenderPaidComp", borrowerPaidCompExceedsLenderPaidComp.ToString());

            loanData.Add("sLoanFileT", dataLoan.sLoanFileT.ToString("d"));

            loanData.Add("sCorrespondentProcessT", "" + (int)dataLoan.sCorrespondentProcessT);

            loanData.Add("sBranchChannelT", "" + (int)dataLoan.sBranchChannelT);

            loanData.Add("sGfeIsTPOTransaction", dataLoan.sGfeIsTPOTransaction.ToString());

            loanData.Add("sHasMortgageLiabilityThatWillBePaidOffAtClosing", (!CurrentBroker.EnableCashToCloseAndReservesDebugInPML ? true : dataLoan.sHasMortgageLiabilityThatWillBePaidOffAtClosing).ToString());

            loanData.Add("IsBeyondV1_CalcVaLoanElig", LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V1_CalcVaLoanElig_Opm204251).ToString());

            return loanData;
        }

        protected List<Dictionary<string, string>> LoadApps(CPageData dataLoan)
        {
            var apps = new List<Dictionary<string, string>>();

            if (!dataLoan.sHasAnyAppsFilled)
            {
                if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
                {
                    Dictionary<string, string> app = new Dictionary<string, string>();
                    //While a loan is a QuickPricer file, it always contains a single application.
                    CAppData qpDataApp = dataLoan.GetAppData(0);
                    app.Add("aBTotalScoreIsFthbQP", qpDataApp.aBTotalScoreIsFthb.ToString());
                    app.Add("aBHasHousingHistQP", qpDataApp.aBHasHousingHist.ToString());

                    apps.Add(app);
                }
                return apps;
            }

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                var app = LoadApp(dataLoan.sBrokerId, dataApp);
                apps.Add(app);
            }

            return apps;
        }

        protected void BindApp(CPageData dataLoan)
        {
            if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
            {
                //While a loan is a QuickPricer file, it always contains a single application.
                CAppData qpDataApp = dataLoan.GetAppData(0);
                qpDataApp.aBTotalScoreIsFthb = GetBool("aBTotalScoreIsFthbQP");
                qpDataApp.aBHasHousingHist = GetBool("aBHasHousingHistQP");
            }

            int currentAppNum = GetInt("CurrentApp");
            if (currentAppNum < 0)
                return;
            CAppData dataApp = dataLoan.GetAppData(currentAppNum);
            if (dataApp == null)
                return;

            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aProdBCitizenT = (E_aProdCitizenT)GetInt("aProdBCitizenT");
            dataApp.aBEmail = GetString("aBEmail");
            dataApp.aBTotIPe_rep = GetString("aBMonInc");
            dataApp.aBIsSelfEmplmt = GetBool("aBIsSelfEmplmt");
            dataApp.aBTotalScoreIsFthb = GetBool("aBTotalScoreIsFthb");
            dataApp.aBHasHousingHist = GetBool("aBHasHousingHist");
            dataApp.aIsBVAElig = GetBool("aIsBVAElig");
            dataApp.aIsBVAFFEx = GetBool("aIsBVAFFEx");
            dataApp.aBVServiceT = (E_aVServiceT)GetInt("aBVServiceT");
            dataApp.aBVEnt = (E_aVEnt)GetInt("aBVEnt");
            dataApp.aBExperianScorePe = GetInt("aBExperianScorePe");
            dataApp.aBTransUnionScorePe = GetInt("aBTransUnionScorePe");
            dataApp.aBEquifaxScorePe = GetInt("aBEquifaxScorePe");
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V1_CalcVaLoanElig_Opm204251))
            {
                // Only available for modification if migrated.
                dataApp.aBIsVeteran = GetBool("aBIsVeteran");
                dataApp.aBIsSurvivingSpouseOfVeteran = GetBool("aBIsSurvivingSpouseOfVeteran");
                dataApp.aCIsVeteran = GetBool("aCIsVeteran");
                dataApp.aCIsSurvivingSpouseOfVeteran = GetBool("aCIsSurvivingSpouseOfVeteran");
            }

            // If the user has unchecked "Has Co-Applicant", then we want to
            // clear the fields that aBHasSpouse depends on. - OPM 111071 gf
            if (GetBool("aBHasSpouse"))
            {
                dataApp.aCFirstNm = GetString("aCFirstNm");
                dataApp.aCLastNm = GetString("aCLastNm");
                dataApp.aCSsn = GetString("aCSsn");
                dataApp.aCTotIPe_rep = GetString("aCMonInc");
            }
            else
            {
                dataApp.aCFirstNm = string.Empty;
                dataApp.aCLastNm = string.Empty;
                dataApp.aCSsn = string.Empty;
                dataApp.aCTotIPe_rep = string.Empty;
            }

            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aProdCCitizenT = (E_aProdCitizenT)GetInt("aProdCCitizenT");
            dataApp.aCEmail = GetString("aCEmail");
            dataApp.aCIsSelfEmplmt = GetBool("aCIsSelfEmplmt");
            dataApp.aCTotalScoreIsFthb = GetBool("aCTotalScoreIsFthb");
            dataApp.aCHasHousingHist = GetBool("aCHasHousingHist");
            dataApp.aIsCVAElig = GetBool("aIsCVAElig");
            dataApp.aIsCVAFFEx = GetBool("aIsCVAFFEx");
            dataApp.aCVServiceT = (E_aVServiceT)GetInt("aCVServiceT");
            dataApp.aCVEnt = (E_aVEnt)GetInt("aCVEnt");
            dataApp.aCExperianScorePe = GetInt("aCExperianScorePe");
            dataApp.aCTransUnionScorePe = GetInt("aCTransUnionScorePe");
            dataApp.aCEquifaxScorePe = GetInt("aCEquifaxScorePe");

            dataApp.aTransmOMonPmtPe_rep = GetString("aTransmOMonPmtPe");

            // If the user hasn't filled these in yet, don't fill it in for them
            if (!string.IsNullOrEmpty(GetString("aOpNegCfPe")))
            {
                dataApp.aOpNegCfPe_rep = GetString("aOpNegCfPe");
                dataLoan.UpdateSOpNegCfPe();
            }

            if (!string.IsNullOrEmpty(GetString("aLiquidAssetsPe")))
            {
                dataApp.aLiquidAssetsPe_rep = GetString("aLiquidAssetsPe");
            }
            else
            {
                dataApp.aLiquidAssetsPe = 0.0M;
                dataApp.aLiquidAssetsExists = false;
            }
        }

        private Dictionary<string, string> LoadApp(Guid brokerId, CAppData dataApp)
        {
            var app = new Dictionary<string, string>();
            
            
            app.Add("HasEdocs", appIds.Contains(dataApp.aAppId).ToString());
            

            app.Add("aBHPhone", dataApp.aBHPhone);
            app.Add("aBBusPhone", dataApp.aBBusPhone);
            app.Add("aBCellPhone", dataApp.aBCellPhone);

            app.Add("aBFirstNm", dataApp.aBFirstNm);
            app.Add("aBMidNm", dataApp.aBMidNm);
            app.Add("aBLastNm", dataApp.aBLastNm);
            app.Add("aBNm", dataApp.aBNm);
            app.Add("aBSuffix", dataApp.aBSuffix);
            app.Add("aBSsn", dataApp.aBSsn);
            app.Add("aBEmail", dataApp.aBEmail);
            app.Add("aBMonInc", dataApp.aBTotIPe_rep);
            app.Add("aProdBCitizenT", dataApp.aProdBCitizenT.ToString("d"));
            app.Add("aBDecCitizen", dataApp.aBDecCitizen);
            app.Add("aBDecResidency", dataApp.aBDecResidency);
            app.Add("aBIsSelfEmplmt", dataApp.aBIsSelfEmplmt.ToString());
            app.Add("aBTotalScoreIsFthb", dataApp.aBTotalScoreIsFthb.ToString());
            app.Add("aBTotalScoreIsFthbReadOnly", dataApp.aBTotalScoreIsFthbReadOnly.ToString());
            app.Add("aBHasHousingHist", dataApp.aBHasHousingHist.ToString());
            app.Add("aIsBVAElig", dataApp.aIsBVAElig.ToString());
            app.Add("aIsBVAFFEx", dataApp.aIsBVAFFEx.ToString());
            app.Add("aBVServiceT", dataApp.aBVServiceT.ToString("d"));
            app.Add("aBVEnt", dataApp.aBVEnt.ToString("d"));
            app.Add("aBExperianScorePe", dataApp.aBExperianScorePe.ToString());
            app.Add("aBTransUnionScorePe", dataApp.aBTransUnionScorePe.ToString());
            app.Add("aBEquifaxScorePe", dataApp.aBEquifaxScorePe.ToString());
            app.Add("aBIsVeteran", dataApp.aBIsVeteran.ToString());
            app.Add("aBIsSurvivingSpouseOfVeteran", dataApp.aBIsSurvivingSpouseOfVeteran.ToString());
            app.Add("aCIsVeteran", dataApp.aCIsVeteran.ToString());
            app.Add("aCIsSurvivingSpouseOfVeteran", dataApp.aCIsSurvivingSpouseOfVeteran.ToString());

            app.Add("aBHasSpouse", (dataApp.aBHasSpouse && !dataApp.aCTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)).ToString());
            app.Add("aCFirstNm", dataApp.aCFirstNm);
            app.Add("aCMidNm", dataApp.aCMidNm);
            app.Add("aCLastNm", dataApp.aCLastNm);
            app.Add("aCNm", dataApp.aCNm);
            app.Add("aCSuffix", dataApp.aCSuffix);
            app.Add("aCSsn", dataApp.aCSsn);
            app.Add("aCMonInc", dataApp.aCTotIPe_rep);
            app.Add("aCEmail", dataApp.aCEmail);
            app.Add("aProdCCitizenT", dataApp.aProdCCitizenT.ToString("d"));
            app.Add("aCDecCitizen", dataApp.aCDecCitizen);
            app.Add("aCDecResidency", dataApp.aCDecResidency);
            app.Add("aCIsSelfEmplmt", dataApp.aCIsSelfEmplmt.ToString());
            app.Add("aCTotalScoreIsFthb", dataApp.aCTotalScoreIsFthb.ToString());
            app.Add("aCTotalScoreIsFthbReadOnly", dataApp.aCTotalScoreIsFthbReadOnly.ToString());
            app.Add("aCHasHousingHist", dataApp.aCHasHousingHist.ToString());
            app.Add("aIsCVAElig", dataApp.aIsCVAElig.ToString());
            app.Add("aIsCVAFFEx", dataApp.aIsCVAFFEx.ToString());
            app.Add("aCVServiceT", dataApp.aCVServiceT.ToString("d"));
            app.Add("aCVEnt", dataApp.aCVEnt.ToString("d"));
            app.Add("aCExperianScorePe", dataApp.aCExperianScorePe.ToString());
            app.Add("aCTransUnionScorePe", dataApp.aCTransUnionScorePe.ToString());
            app.Add("aCEquifaxScorePe", dataApp.aCEquifaxScorePe.ToString());

            app.Add("aTransmOMonPmtPe", dataApp.aTransmOMonPmtPe_rep);
            app.Add("aOpNegCfPe", dataApp.aOpNegCfPe_rep);

           
            app.Add("aLiquidAssetsPe", dataApp.aLiquidAssetsPe_rep);
            app.Add("aAsstLiqTot", dataApp.aAsstLiqTot_rep);
            app.Add("aAssetCollectionCount", (dataApp.aAssetCollection.CountRegular + dataApp.aAssetCollection.CountSpecial).ToString());
            

            app.Add("aAppId", dataApp.aAppId.ToString());
            app.Add("aIsCreditReportOnFile", dataApp.aIsCreditReportOnFile.ToString());
            app.Add("aDisplayPublicRecords", (dataApp.aPublicRecordCollection.CountRegular > 0).ToString());

            if (dataApp.aIsCreditReportOnFile)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@ApplicationID", dataApp.aAppId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveCreditReport", parameters))
                {
                    if (reader.Read())
                    {
                        app.Add("ReportID", (string)reader["ExternalFileID"]);

                        if (reader["CrAccProxyId"] is DBNull)
                            app.Add("LastCreditReportingAgency", reader["ComId"].ToString());
                        else
                            app.Add("LastCreditReportingAgency", reader["CrAccProxyId"].ToString());
                    }
                }
            }

            return app;
        }

        /// <summary>
        /// Bind data from the page to the loan object
        /// </summary>
        protected void BindData(CPageData dataLoan)
        {
            BindLoan(dataLoan);
            BindApp(dataLoan);
        }

        /// <summary>
        /// Load data from the loan to the page
        /// </summary>
        protected void LoadData()
        {
            var user = (AbstractUserPrincipal)Context.User;

            Guid sLId = GetGuid("sLId");
            List<SqlParameter> parameters = new List<SqlParameter>();
            SqlParameter param = new SqlParameter("@sLId", sLId);
            parameters.Add(param);
            appIds = new List<Guid>();

            using (DbDataReader sr =  StoredProcedureHelper.ExecuteReader(user.BrokerId, "ListAppIdsWithEdocsByLoanId", parameters))
            {
                while (sr.Read())
                {
                    appIds.Add((Guid)sr["aAppId"]);
                }
            }


            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            LoadData(dataLoan);
        }

        // Returned object:
        // value.loan = JSON serialized {object containing loan values}
        // value.apps = JSON serialized [list, of, apps]
        //           -> {each app is an object containing app values}
        // These fields are deserialized using JSON.parse when we get them back
        protected void LoadData(CPageData dataLoan)
        {
            var loanData = LoadLoan(dataLoan);
            var serializedLoanData = ObsoleteSerializationHelper.JavascriptJsonSerialize(loanData);
            SetResult("loan", serializedLoanData);

            var apps = LoadApps(dataLoan);
            var serializedApps = ObsoleteSerializationHelper.JavascriptJsonSerialize(apps);
            SetResult("apps", serializedApps);
        }

        /// <summary>
        /// Binds data from the page to the loan object, which will affect calculated fields.
        /// Then, all the required fields are loaded from the loan 
        /// </summary>
        protected void CalculateData()
        {
            Guid sLId = GetGuid("sLId");

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            BindData(dataLoan);
            LoadData(dataLoan);
        }

        protected void SaveData()
        {
            Guid sLId = GetGuid("sLId");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLService));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(sFileVersion);

            BindLoan(dataLoan);

            BindApp(dataLoan);
            try
            {
                dataLoan.Save();
                LoadData(dataLoan);
            }
            catch (LoanFieldWritePermissionDenied ex)
            {
                SetResult("PermissionError", ex.UserMessage);
            }
        }

        private void GetResults()
        {
            Guid requestID = Guid.Empty;
            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(requestID);

            if (resultItem == null)
            {
                SetResult("Waiting", "True");
                return; 
            }

            if (resultItem.IsErrorMessage)
            {
                SetResult("Error", resultItem.ErrorMessage);
                return;
            }
        }
    }
}