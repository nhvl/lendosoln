﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.UI;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using PriceMyLoan.Security;
using LendersOffice.Common;
using System.Web.Services;
using CommonProjectLib.Common.Lib;
using System.Text;
using System.Xml;
using DataAccess.LoanComparison;
using System.IO;
using LendersOffice.Security;
using LendersOffice.Constants;
using System.Xml.Xsl;
using System.Xml.Linq;
using LendersOffice.Admin;
using LendersOffice.Email;
using LendersOffice.Audit;
using LendersOffice.ObjLib.Resource;
using LendersOffice.ObjLib.ComparisonReport;

namespace PriceMyLoan.webapp
{

    public partial class RunLoanComparison : PriceMyLoan.UI.BasePage
    {
        private enum E_CacheKeySuffix
        {
            StateAndRequestId = 0,
            StateXml = 1,
            LoanComparisonXml = 2
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            RegisterService("Service", "/main/MainService.aspx");
            ClientScript.RegisterHiddenField("LoanId", LoanID.ToString());

            BrokerDB db = BrokerDB.RetrieveById(GetPrincipal().BrokerId);

            Guid resultId = RequestHelper.GetGuid("ResultId", Guid.Empty);
            Guid requestId = RequestHelper.GetGuid("RequestId", Guid.Empty);
            if (resultId != Guid.Empty)
            {
                RenderResults(resultId, requestId, RequestHelper.GetInt("Xml", 0), db.BrokerID);
            }
            else
            {
                RequestResults();
            }
        }

        /// <summary>
        /// Running Pricing.
        /// </summary>
        private void RequestResults()
        {
            TransformData();
            AbstractUserPrincipal principal = GetPrincipal();
            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(RunLoanComparison));
            data.AllowLoadWhileQP2Sandboxed = true;
            data.InitLoad();
            data.TransformDataToPml(E_TransformToPmlT.FromScratch);
            var states = data.GetPricingStates();
            bool runningPricing ;
            ComparisonRunData runData = this.GetPinstateRequestInfo(states, data.sProdLpePriceGroupId, out runningPricing);


            if (runningPricing)
            {
                Guid requestId = Guid.NewGuid();
                CachePricingStates(states, requestId);
                CacheNewComparisonRunData(runData, requestId);
                ClientScript.RegisterHiddenField("RequestId", requestId.ToString("N"));
                RunningLoanComparison.Visible = true;
                NoPinnnedStates.Visible = false;
            }
            else
            {
                NoPinnnedStates.Visible = true;
                RunningLoanComparison.Visible = false;
            }
        }

        private static AbstractUserPrincipal GetPrincipal()
        {
            return LendersOffice.Security.PrincipalFactory.CurrentPrincipal;
        }

        private static string GetTextKey(Guid userId, Guid id, E_CacheKeySuffix cachesuffix)
        {
            string suffix;

            switch (cachesuffix)
            {
                case E_CacheKeySuffix.StateAndRequestId:
                    suffix = "StateAndRequestXml";
                    break;
                case E_CacheKeySuffix.StateXml:
                    suffix = "StateXml";
                    break;
                case E_CacheKeySuffix.LoanComparisonXml:
                    suffix = "LoanComparison";
                    break;
                default:
                    throw new UnhandledEnumException(cachesuffix);  
            }
            return String.Format("{0:N}_{1:N}_{2}", userId, id, suffix );
        }

        private static void CacheNewComparisonRunData(ComparisonRunData runData, Guid id)
        {
            var principal = GetPrincipal();
            string serializerdata = SerializationHelper.JsonNetSerialize(runData);
            AutoExpiredTextCache.AddToCache(serializerdata, TimeSpan.FromMinutes(5), GetTextKey(principal.UserId, id, E_CacheKeySuffix.StateAndRequestId));
        }


        private static void CacheExistingComparisonRunData(ComparisonRunData runData, Guid id)
        {
            var principal = GetPrincipal();
            string cacheKey = GetTextKey(principal.UserId, id, E_CacheKeySuffix.StateAndRequestId);
        
            string serializerdata = SerializationHelper.JsonNetSerialize(runData);
            AutoExpiredTextCache.UpdateCache(serializerdata, cacheKey);
            AutoExpiredTextCache.RefreshCacheDuration(cacheKey, TimeSpan.FromMinutes(5));
        }

        private static ComparisonRunData LoadComparisonRunDataFromCache(Guid id)
        {
            var principal = GetPrincipal();
            string requestIdsData = AutoExpiredTextCache.GetFromCache(GetTextKey(principal.UserId, id, E_CacheKeySuffix.StateAndRequestId));

            if (string.IsNullOrEmpty(requestIdsData))
            {
                return null;
            }

            var items = SerializationHelper.JsonNetDeserialize<ComparisonRunData>(requestIdsData); 
            return items;

        }

        private ComparisonRunData GetPinstateRequestInfo(PricingStates states, Guid sProdLpePriceGroupId,  out bool runLoanComparison)
        {
            var principal = GetPrincipal();
            IDictionary<Guid, LoanProgramByInvestorSet> investorSetsByState = states.GetLoan1stLoanQualifyingProgramsToRunLpe(sProdLpePriceGroupId, principal.BrokerDB);
            List<PinStateRequestInfo> pinStateRequests = new List<PinStateRequestInfo>();
            runLoanComparison = false;
            foreach (var state in states.States)
            {
                var lpset = investorSetsByState[state.Id];
                if (lpset.LoanProgramsCount <= 0)
                {
                    pinStateRequests.Add(new PinStateRequestInfo() { StateId = state.Id, FirstLoanRequestId = Guid.Empty, HasSecondLoanInfo = false });
                }
                else
                {
                    Guid requestID = DistributeUnderwritingEngine.SubmitPinToEngine(
                        principal, LoanID, sProdLpePriceGroupId, state, lpset, state.SecondLoanMonthlyPayment);
                    pinStateRequests.Add(new PinStateRequestInfo() { StateId = state.Id, FirstLoanRequestId = requestID, HasSecondLoanInfo = state.IsComboScenario});

                    runLoanComparison = true;
                }
            }
            ComparisonRunData data = new ComparisonRunData(sProdLpePriceGroupId, pinStateRequests);
            return data;
        }

        private void RenderResults(Guid resultId, Guid requestId, int xmlVersion, Guid brokerId)
        {
            string xml = LoadLoanComparisonXmlFromCache(resultId);
            Response.Clear();
            if (xmlVersion == 1)
            {
                byte[] data = UTF8Encoding.UTF8.GetBytes(xml);
                Response.ContentEncoding = UTF8Encoding.UTF8;
                Response.ContentType = "text/xml";
                Response.OutputStream.Write(data, 0, data.Length);
                Response.End();
                return;
            }
            else if (xmlVersion == 2)
            {
                var principal = GetPrincipal();
                string xmlData = AutoExpiredTextCache.GetFromCache(GetTextKey(principal.UserId, requestId, E_CacheKeySuffix.StateXml));
                var xElement = XElement.Parse(xmlData);
                
                byte[] data = UTF8Encoding.UTF8.GetBytes(xElement.ToString());
                Response.ContentEncoding = UTF8Encoding.UTF8;
                Response.ContentType = "text/xml";
                Response.OutputStream.Write(data, 0, data.Length);
                Response.End();
                return;
            }

            string path = ResourceManager.Instance.GetResourcePath(ResourceType.LoanComparisonCertXslt, brokerId);
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("VirtualRoot", "", Tools.VRoot);
            args.AddParam("IsEmail", "", false.ToString());
            args.AddParam("ShowAddendum", "", true.ToString());
            args.AddParam("JQuerySrc", "", Page.ResolveClientUrl("~/inc/jquery-1.12.4.min.js"));
            args.AddParam("JSONSrc", "", Page.ResolveClientUrl("~/inc/json.js"));
            args.AddParam("JQueryUISrc", "", Page.ResolveClientUrl("~/inc/jquery-ui-1.11.4.min.js"));
            args.AddParam("JQueryUICss", "", Page.ResolveClientUrl("~/css/jquery-ui-1.11.custom.css"));
            XslTransformHelper.Transform(path, xml, Response.OutputStream, args);
            Response.End();

        }

        private static void CachePricingStates(PricingStates states, Guid id)
        {
            var principal = GetPrincipal();
            using (StringWriter8 stringwriter = new StringWriter8())
            using (XmlWriter xmlwriter = XmlWriter.Create(stringwriter))
            {
                states.WriteXml(xmlwriter);
                xmlwriter.Flush();
                AutoExpiredTextCache.AddToCache(stringwriter.ToString(), TimeSpan.FromMinutes(5), GetTextKey(principal.UserId, id, E_CacheKeySuffix.StateXml));
            }
        }

        private static PricingStates LoadPricingStatesFromCache(Guid id)
        {
            var principal = GetPrincipal();
            string xmlData = AutoExpiredTextCache.GetFromCache(GetTextKey(principal.UserId, id, E_CacheKeySuffix.StateXml));

            PricingStates pricingStates = new PricingStates();
            using (StringReader sr = new StringReader(xmlData))
            using (XmlReader reader = XmlReader.Create(sr))
            {
                pricingStates.ReadXml(reader);
            }

            return pricingStates;
        }

        private static string LoadLoanComparisonXmlFromCache(Guid id)
        {
            var principal = GetPrincipal();
            return AutoExpiredTextCache.GetFromCache(GetTextKey(principal.UserId, id, E_CacheKeySuffix.LoanComparisonXml));
        }

        private static Guid GenerateAndCacheResults(ComparisonRunData data, Guid loanId, Guid runDataKey, PricingStates pricingStates, ComparisonRunData runData)
        {
            Guid resutId = Guid.NewGuid();
            var principal = GetPrincipal();

            Dictionary<Guid, UnderwritingResultItem> underwritingResultsByResultId = new Dictionary<Guid, UnderwritingResultItem>();

            foreach (PinStateRequestInfo request in data.Requests)
            {
                if (request.FirstLoanRequestId == Guid.Empty)
                {
                    continue;
                }
                var underwritingResult = LpeDistributeResults.RetrieveResultItem(request.FirstLoanRequestId, deleteRequest: true);
                underwritingResultsByResultId.Add(request.FirstLoanRequestId, underwritingResult);

                if (request.SecondLoanRequestId.HasValue && request.SecondLoanRequestId != Guid.Empty)
                {
                    underwritingResult = LpeDistributeResults.RetrieveResultItem(request.SecondLoanRequestId.Value, deleteRequest: true);
                    underwritingResultsByResultId.Add(request.SecondLoanRequestId.Value, underwritingResult);
                }
            }

            using (StringWriter8 writer = new StringWriter8())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    LoanComparisonXml.Generate(xmlWriter, principal, loanId, runDataKey, resutId, pricingStates, underwritingResultsByResultId, runData);
                    xmlWriter.Flush();
                }

                AutoExpiredTextCache.AddToCache(writer.ToString(), TimeSpan.FromMinutes(30), GetTextKey(principal.UserId, resutId, E_CacheKeySuffix.LoanComparisonXml)); //todo ask dw how long to keep results
            }

            return resutId;
        }


        [WebMethod]
        public static object DeletePin(Guid resultId, Guid pinId, Guid loanId)
        {
            try
            {
                CPageData data = CPageData.CreateUsingSmartDependency(loanId, typeof(RunLoanComparison));
                data.AllowSaveWhileQP2Sandboxed = true;
                data.InitSave(ConstAppDavid.SkipVersionCheck);
                data.DeletePricingState(pinId);
                data.Save();

                string xml = LoadLoanComparisonXmlFromCache(resultId);
                Guid newResultId = Guid.NewGuid();

                string newXml = LoanComparisonXml.DeletePinFromCert(newResultId, xml, pinId);
                AutoExpiredTextCache.ExpireCache(GetTextKey(GetPrincipal().UserId, resultId, E_CacheKeySuffix.LoanComparisonXml));
                AutoExpiredTextCache.AddToCache(newXml, TimeSpan.FromMinutes(30), GetTextKey(GetPrincipal().UserId, newResultId, E_CacheKeySuffix.LoanComparisonXml));

                return new
                {
                    Url = string.Format("{0}/webapp/RunLoanComparison.aspx?loanid={1:N}&ResultId={2:N}",
                        Tools.VRoot, loanId, newResultId)
                };
            }
            catch (Exception e)
            {
                // WebMethods need logging of exceptions   .
                Tools.LogErrorWithCriticalTracking("DeletePin failed on loan comparison report.",e);

                // For security, not exposing the call stack to the user.
                //HttpContext.Current.Response.StatusCode = 500; // There might be a better way to trigger the ajax error handling.
                //return ErrorMessages.Generic;

                // can't do the above b/c if we allowed not to rethrow, we potentially allow silent failures.
                var msg = "There was a problem with pin deletion.";
                throw new CBaseException(msg, msg);
            }
        }
        [WebMethod]
        public static object GetResult(Guid loanId, Guid requestId)
        {
            var principal = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;

            string status = "ExpiredRunAgain";
            string resultId = "";
            PricingStates pricingStates = LoadPricingStatesFromCache(requestId);
            bool recacheComparisonRunData = false;
            var runData = LoadComparisonRunDataFromCache(requestId);

            if (runData == null)
            {
                return new { Status = status, ResultId = resultId }; 
            }

            foreach (PinStateRequestInfo requestInfo in runData.Requests)
            {
                //could not run first loan just skip it.
                if (requestInfo.FirstLoanRequestId == Guid.Empty)
                {
                    status = "NotReady";
                    continue;
                }

                if (!LpeDistributeResults.IsResultAvailable(requestInfo.FirstLoanRequestId))
                {
                    status = "NotReady";
                    continue;
                }

                // The first pricing run is 
                if (requestInfo.HasSecondLoanInfo)
                {
                    if (requestInfo.SecondLoanRequestId.HasValue)
                    {
                        if (!LpeDistributeResults.IsResultAvailable(requestInfo.SecondLoanRequestId.Value))
                        {
                            status = "NotReady";
                            continue;
                        }
                    }
                    else
                    {
                        PricingState state = pricingStates.GetState(requestInfo.StateId);
                        LoanProgramByInvestorSet set = pricingStates.GetLoan2ndLoanQualifyingProgramsToRunLpeForState(runData.sProdLpePriceGroupId, principal.BrokerDB, requestInfo.StateId);
                        // we need to run second loan pricing using first.

                        UnderwritingResultItem item = LpeDistributeResults.RetrieveResultItem(requestInfo.FirstLoanRequestId, false);
                        var xml = item.GetEligibleLoanProgramList().Where(p => p.lLpTemplateId == state.lLpTemplateId).FirstOrDefault();

                        // First Program was not eligible? 
                        if (xml == null)
                        {
                            continue;
                        }

                        LosConvert c = new LosConvert();
                        var rateOption = xml.ApplicantRateOptions.Where(p => p.Rate_rep == state.SelectedNoteRate).FirstOrDefault();
                        decimal firstPmt = c.ToMoney(rateOption.FirstPmtAmt_rep);
                        decimal firstNoteRate = rateOption.Rate;
                        decimal firstPoint = rateOption.PointIn100;
                        string tempLpeTaskMessageXml = DistributeUnderwritingEngine.GenerateTemporaryAcceptFirstLoanMessage(principal, loanId, state.lLpTemplateId, rateOption.RequestedRateOption, runData.sProdLpePriceGroupId, string.Empty, "0", E_RenderResultModeT.Regular);
                        
                        Guid secondLoanRequestId = DistributeUnderwritingEngine.SubmitPinToEngine(principal, loanId, runData.sProdLpePriceGroupId, tempLpeTaskMessageXml, state, set, state.lLpTemplateId, firstNoteRate, firstPoint, firstPmt);
                        requestInfo.SecondLoanRequestId = secondLoanRequestId;
                        status = "NotReady";
                        recacheComparisonRunData = true;
                    }
                }
            }

            if (status != "NotReady")
            {
                status = "Ready";
                resultId = GenerateAndCacheResults(runData, loanId, requestId, pricingStates, runData).ToString("N");
            }

            if (recacheComparisonRunData)
            {
                CacheExistingComparisonRunData(runData, requestId);
            }

            return new { Status = status, ResultId = resultId};
        }

        [WebMethod]
        public static object GetCertificate(Guid loanId, Guid pinid, string version, string checksum , Guid productid, string lienQualifyModeT, string FirstLienNoteRate, string FirstLienPoint, string FirstLienLpId, string FirstLienMPmt)
        {
            string url = string.Format("{0}/Main/QualifiedLoanProgramPrintFrame.aspx?loanid={1:N}&productid={2:N}&lienqualifymodet={6}&version={3}&uniqueChecksum={4}&PinStateId={5:N}&B=1&FirstLienNoteRate={7}&FirstLienPoint={8}&FirstLienLpId={9}&FirstLienMPmt={10}",
                Tools.VRoot, loanId, productid, version, checksum, pinid, lienQualifyModeT, FirstLienNoteRate, FirstLienPoint, FirstLienLpId, FirstLienMPmt);
            return new { Url = url };
        }


        [WebMethod]
        public static object GetConfirmationUrl(Guid pinid, Guid resultId, bool isLock)
        {
            string xml = LoadLoanComparisonXmlFromCache(resultId);
            if (string.IsNullOrEmpty(xml))
            {
                return new { Status = "Expired" };
            }

            XDocument doc = XDocument.Parse(xml);

            ConfirmationUrlGenerator generator = new ConfirmationUrlGenerator();
            string path = $"{Tools.VRoot}/Main/ConfirmationPage.aspx";
            string url = generator.GenerateUrl(doc, pinid, isLock, path);

            if (string.IsNullOrEmpty(url))
            {
                Tools.LogErrorWithCriticalTracking(String.Format("Pin Id {0} not found in {1}", pinid, xml));
                return new { Status = "Expired" };
            }

           
            return new { Status = "Ok", Url = url };
        }


        [WebMethod]
        public static object SendReportToBorrower(Guid loanId, Guid resultId, Guid requestId, string[] emails, string msgToBorrower, bool includeAddendum)
        {
            var principal = GetPrincipal();
            EmployeeDB db = EmployeeDB.RetrieveById(principal.BrokerId, principal.EmployeeId);
            BrokerDB bdb = BrokerDB.RetrieveById(principal.BrokerId);
            string pricingStateXml = AutoExpiredTextCache.GetFromCache(GetTextKey(principal.UserId, requestId, E_CacheKeySuffix.StateXml));
            string xml = LoadLoanComparisonXmlFromCache(resultId);
            string redirectUrl = RemoveIneligiblePins(resultId, loanId, ref xml);
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("IsEmail", "", "True");
            args.AddParam("ShowAddendum", "", includeAddendum.ToString());
            string path = ResourceManager.Instance.GetResourcePath(ResourceType.LoanComparisonCertXslt, db.BrokerID);
            string htmlForAudit = XslTransformHelper.Transform(path, xml, args);
            byte[] pdfBytes = CPageBase.ConvertToPdf(htmlForAudit);
            CBaseEmail email = new CBaseEmail(principal.BrokerId);
            email.From = db.Email;
            email.Subject = "Loan Comparison Report";
            email.IsHtmlEmail = false;
            email.Message = msgToBorrower;
            email.To = String.Join(",", emails);
            email.AddAttachment("LoanComparison.pdf", pdfBytes);
            email.Send();

            var dataLoan = new CFullAccessPageData(loanId, new string[] { "sLoanFileT" });
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            if (E_sLoanFileT.QuickPricer2Sandboxed != dataLoan.sLoanFileT)
            {
                LoanComparisonSentAuditItem auditItem = new LoanComparisonSentAuditItem(principal, email.From, email.To, pricingStateXml, xml, htmlForAudit, msgToBorrower);
                AuditManager.RecordAudit(loanId, auditItem);
            }

            return new { Status = "Sent", Url = redirectUrl };
        }

        public static string RemoveIneligiblePins(Guid resultId, Guid loanId, ref string xml)
        {
            if (!LoanComparisonXml.HasIneligiblePins(xml))
                return "";

            Guid newResultId = Guid.NewGuid();

            Guid[] removedPinIds;
            xml = LoanComparisonXml.RemoveIneligiblePins(newResultId, xml, out removedPinIds);
            AutoExpiredTextCache.ExpireCache(GetTextKey(GetPrincipal().UserId, resultId, E_CacheKeySuffix.LoanComparisonXml));
            AutoExpiredTextCache.AddToCache(xml, TimeSpan.FromMinutes(30), GetTextKey(GetPrincipal().UserId, newResultId, E_CacheKeySuffix.LoanComparisonXml));

            // Delete removed pins from pricing state
            CPageData data = CPageData.CreateUsingSmartDependency(loanId, typeof(RunLoanComparison));
            data.AllowSaveWhileQP2Sandboxed = true;
            data.InitSave(ConstAppDavid.SkipVersionCheck);

            foreach (Guid pinId in removedPinIds)
            {
                data.DeletePricingState(pinId);
            }

            data.Save();

            return string.Format("{0}/webapp/RunLoanComparison.aspx?loanid={1:N}&ResultId={2:N}",
                    Tools.VRoot, loanId, newResultId);
        }



    }
}
