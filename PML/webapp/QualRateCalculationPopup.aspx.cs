﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using LendersOffice.ObjLib.Loan;
    using LendersOffice.Security;

    /// <summary>
    /// Qualification Rate Calculation class.
    /// The page is originated from newlos\forms\qualratecalculationpopup.ascx.cs
    /// </summary>
    public partial class QualRateCalculationPopup : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event Args.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterService("QualRateCalculationPopupService", "/webapp/QualRateCalculationPopupService.aspx");

            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            this.RegisterJsScript("/webapp/QualRateCalculationPopup.js");
            this.RegisterJsScript("common.js");

            this.RegisterCSS("jquery-ui-1.11.css");
            this.RegisterCSS("/webapp/style.css");
            this.RegisterCSS("/webapp/main.css");

            this.RegisterJsGlobalVariables("FlatValue", QualRateCalculationT.FlatValue.ToString("D"));
            this.RegisterJsGlobalVariables("MaxOf", QualRateCalculationT.MaxOf.ToString("D"));
            this.RegisterJsGlobalVariables("sLID", this.LoanID);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("QualRateFieldIds", this.GetQualRateFieldIds());

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(QualRateCalculationPopup));
            dataLoan.InitLoad();

            Tools.Bind_QualRateCalculationFieldT(this.sQualRateCalculationFieldT1);
            Tools.Bind_QualRateCalculationFieldT(this.sQualRateCalculationFieldT2);

            this.sQualIR.Text = dataLoan.sQualIR_rep;
            this.sUseQualRate.Checked = dataLoan.sUseQualRate;

            this.QualRateCalculationFlatValue.Checked = dataLoan.sQualRateCalculationT == QualRateCalculationT.FlatValue;
            this.QualRateCalculationMaxOf.Checked = dataLoan.sQualRateCalculationT == QualRateCalculationT.MaxOf;

            Tools.SetDropDownListValue(this.sQualRateCalculationFieldT1, dataLoan.sQualRateCalculationFieldT1);
            this.sQualRateCalculationAdjustment1.Text = dataLoan.sQualRateCalculationAdjustment1_rep;

            Tools.SetDropDownListValue(this.sQualRateCalculationFieldT2, dataLoan.sQualRateCalculationFieldT2);
            this.sQualRateCalculationAdjustment2.Text = dataLoan.sQualRateCalculationAdjustment2_rep;

            var calculationOptions = new QualRateCalculationOptions()
            {
                NoteRate = dataLoan.sNoteIR,
                IndexRate = dataLoan.sRAdjIndexR,
                MarginRate = dataLoan.sRAdjMarginR
            };

            var calculationResult1 = Tools.GetQualRateCalculationValue(calculationOptions, dataLoan.sQualRateCalculationFieldT1, dataLoan.sQualRateCalculationAdjustment1);
            this.QualRateCalculationResult1.Text = dataLoan.m_convertLos.ToRateString(calculationResult1);

            if (dataLoan.sQualRateCalculationT == QualRateCalculationT.MaxOf)
            {
                var calculationResult2 = Tools.GetQualRateCalculationValue(calculationOptions, dataLoan.sQualRateCalculationFieldT2, dataLoan.sQualRateCalculationAdjustment2);
                this.QualRateCalculationResult2.Text = dataLoan.m_convertLos.ToRateString(calculationResult2);
            }
            else
            {
                this.QualRateCalculationResult2.Text = dataLoan.m_convertLos.ToRateString(0M);
            }
        }

        /// <summary>
        /// Gets the client IDs for the qual rate calculation IDs.
        /// </summary>
        /// <returns>
        /// A complex type with the qual rate field IDs.
        /// </returns>
        private object GetQualRateFieldIds()
        {
            return new
            {
                QualRateCalcPopup = this.QualRateCalcPopup.ClientID,
                QualRateCalculationPopup_Ok = this.QualRateCalculationPopup_Ok.ClientID,
                QualRateCalculationPopup_Close = this.QualRateCalculationPopup_Close.ClientID,
                ShouldUseQualRate = this.sUseQualRate.ClientID,
                QualRate = this.sQualIR.ClientID,
                QualRateCalculationResult1 = this.QualRateCalculationResult1.ClientID,
                QualRateCalculationResult2 = this.QualRateCalculationResult2.ClientID,
                QualRateCalculationFlatValue = this.QualRateCalculationFlatValue.ClientID,
                QualRateCalculationMaxOf = this.QualRateCalculationMaxOf.ClientID,
                QualRateCalculationFieldT1 = this.sQualRateCalculationFieldT1.ClientID,
                QualRateCalculationFieldT2 = this.sQualRateCalculationFieldT2.ClientID,
                QualRateCalculationAdjustment1 = this.sQualRateCalculationAdjustment1.ClientID,
                QualRateCalculationAdjustment2 = this.sQualRateCalculationAdjustment2.ClientID
            };
        }
    }
}