﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a means for editing a pipeline report's title.
    /// </summary>
    public partial class EditPipelineReportTitle : UI.BasePage
    {
        /// <summary>
        /// Gets the jquery version for the page.
        /// </summary>
        /// <returns>
        /// The jquery version for the page.
        /// </returns>
        protected override E_JqueryVersion GetJQueryVersion() => E_JqueryVersion._1_12_4;

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.Equals("B", this.PriceMyLoanUser.Type, StringComparison.OrdinalIgnoreCase))
            {
                throw new AccessDenied();
            }

            var queryId = RequestHelper.GetGuid("reportId");
            this.ReportTitle.InnerText = Query.GetQueryNameById(this.PriceMyLoanUser.BrokerId, queryId);
        }
    }
}