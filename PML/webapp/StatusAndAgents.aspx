﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StatusAndAgents.aspx.cs" Inherits="PriceMyLoan.webapp.StatusAndAgents" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Status and Agents</title>
</head>
<body id="MainBody">
<form runat="server">
    <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />

    <div class="content-detail" name="content-detail">
        <div class="warp-section extra-side-margin" ng-bootstrap="StatusAndAgents">
            <status-and-agents></status-and-agents>
        </div>
    </div>
</form>
</body>
</html>
