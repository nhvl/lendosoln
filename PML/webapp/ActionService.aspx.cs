﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Data.SqlClient;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;

    /// <summary>
    /// Handles backend processing for the Action control.
    /// </summary>
    public partial class ActionServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Creates a loan data object.
        /// </summary>
        /// <param name="sLId">The loan ID.</param>
        /// <returns>A loan data object.</returns>
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ActionServiceItem));
        }

        /// <summary>
        /// Selects a method to run based on the given string.
        /// </summary>
        /// <param name="methodName">The name of the method to invoke.</param>
        protected override void Process(string methodName)
        {
            switch(methodName)
            {
                case "RetrieveCreditReferenceData":
                    RetrieveCreditReferenceData();
                    break;
            }
        }

        /// <summary>
        /// Retrieves borrower name and credit reference numbers to display in the UI.
        /// </summary>
        private void RetrieveCreditReferenceData()
        {
            Guid loanId = GetGuid("loanId");
            string creditReferenceData = "<br />" + CreditReportUtilities.RetrieveCreditReferenceDataPerApplication(loanId);
            SetResult("CreditReferenceString", creditReferenceData);
        }

        /// <summary>
        /// Loads data for the page.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="dataApp">An application object.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }

        /// <summary>
        /// Binds data for the page.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="dataApp">An application object.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            
        }
    }

    /// <summary>
    /// Initializes the service for the Action control.
    /// </summary>
    public partial class ActionService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes an instance of <see cref="ActionServiceItem"/>.
        /// </summary>
        protected override void Initialize()
        {
            AddBackgroundItem(string.Empty, new ActionServiceItem());
        }
    }
}