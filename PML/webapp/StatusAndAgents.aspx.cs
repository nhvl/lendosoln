﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.ObjLib.PMLNavigation;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides status and agent information for a loan.
    /// </summary>
    public partial class StatusAndAgents : UI.BasePage
    {
        /// <summary>
        /// Provides a mapping between loan statuses and abbreviated status names 
        /// for display in the progress bar.
        /// </summary>
        private static readonly IReadOnlyDictionary<E_sStatusT, string> AbbreviatedStatusNames = new Dictionary<E_sStatusT, string>
        {
            [E_sStatusT.Loan_Prequal] = "Pre-Qual",
            [E_sStatusT.Loan_DocumentCheck] = "Doc Check",
            [E_sStatusT.Loan_DocumentCheckFailed] = "Doc Check Failed",
            [E_sStatusT.Loan_LoanSubmitted] = "Submitted",
            [E_sStatusT.Loan_PreUnderwriting] = "Pre-UW",
            [E_sStatusT.Loan_Underwriting] = "In UW",
            [E_sStatusT.Loan_Preapproval] = "Pre-Approved",
            [E_sStatusT.Loan_ConditionReview] = "Cond Review",
            [E_sStatusT.Loan_FinalUnderwriting] = "Final UW",
            [E_sStatusT.Loan_ClearToClose] = "CTC",
            [E_sStatusT.Loan_FundingConditions] = "Funding Cond",
            [E_sStatusT.Loan_SubmittedForPurchaseReview] = "Submitted for PR",
            [E_sStatusT.Loan_InPurchaseReview] = "In PR",
            [E_sStatusT.Loan_PrePurchaseConditions] = "Pre-PC",
            [E_sStatusT.Loan_SubmittedForFinalPurchaseReview] = "Submit'd for FPR",
            [E_sStatusT.Loan_InFinalPurchaseReview] = "In FPR",
            [E_sStatusT.Loan_ClearToPurchase] = "CTP",
            [E_sStatusT.Loan_ReadyForSale] = "Ready for Sale",
            [E_sStatusT.Loan_InvestorConditions] = "Investor Cond",
            [E_sStatusT.Loan_InvestorConditionsSent] = "Inv Cond Sent",
        };

        /// <summary>
        /// Provides a mapping between loan statuses and expanded status names
        /// that differ from the version returned from <see cref="CPageBase.sStatusT_map_rep(E_sStatusT)"/>.
        /// </summary>
        private static readonly IReadOnlyDictionary<E_sStatusT, string> CorrectedStatusNames = new Dictionary<E_sStatusT, string>
        {
            [E_sStatusT.Loan_Prequal] = "Pre-Qual",
            [E_sStatusT.Loan_LoanSubmitted] = "Submitted",
            [E_sStatusT.Loan_Preapproval] = "Pre-Approved",
            [E_sStatusT.Loan_SubmittedForPurchaseReview] = "Submitted for Purchase Review",
            [E_sStatusT.Loan_SubmittedForFinalPurchaseReview] = "Submitted for Final Purchase Review",
            [E_sStatusT.Loan_ClearToPurchase] = "Clear to Purchase",
            [E_sStatusT.Loan_ReadyForSale] = "Ready for Sale"
        };

        /// <summary>
        /// Gets the additional workflow operations that need checks for the page
        /// for subsequent message retrieval.
        /// </summary>
        /// <returns>
        /// The list of messages for additional workflow operations that need checks for the page.
        /// </returns>
        public override IEnumerable<WorkflowOperation> GetReasonsForOp()
        {
            return new[] { WorkflowOperations.LeadToLoanConversion };
        }

        /// <summary>
        /// Gets additional workflow operations that need checks for the page.
        /// </summary>
        /// <returns>
        /// The list of additional workflow operations that need checks for the page.
        /// </returns>
        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new[] { WorkflowOperations.LeadToLoanConversion };
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the initialization.
        /// </param>
        /// <param name="e">
        /// The arguments for the initialization.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            this.RegisterCSS("/webapp/StatusAndAgents.css");
            this.RegisterCSS("/webapp/style.css");
            this.RegisterCSS("/webapp/main.css");

            this.RegisterService("PML", "/webapp/PMLService.aspx");

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this, includeSimplePopupDependencies: false);
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the load.
        /// </param>
        /// <param name="e">
        /// The arguments for the load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var dataloan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(StatusAndAgents));
            dataloan.InitLoad();

            this.LoanNavHeader.SetDataFromLoan(dataloan, LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationStatusAndAgents);
            this.RegisterViewModel(dataloan);
        }

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Registers the view model for the specified <paramref name="dataloan"/>.
        /// </summary>
        /// <param name="dataloan">
        /// The loan containing status and agent data for the page.
        /// </param>
        private void RegisterViewModel(CPageData dataloan)
        {
            var isRetailMode = this.PriceMyLoanUser.PortalMode == E_PortalMode.Retail;
            var isLead = Tools.IsStatusLead(dataloan.sStatusT);

            var viewModel = new
            {
                sLId = dataloan.sLId,
                sStatusT = dataloan.sStatusTForTpoPortal_rep,
                sStatusD = dataloan.sStatusDForTpoPortal_rep,
                sBranchChannelT = dataloan.sBranchChannelT,
                isInactiveStatus = Tools.IsInactiveStatus(dataloan.sStatusTForTpoPortal),
                hasSuspenseNotice = dataloan.sStatusTForTpoPortal == E_sStatusT.Loan_Suspended && dataloan.sHasSuspenseNoticePdf,
                hasApprovalCertificate = this.GetApprovalCertificateVisibility(dataloan),
                hasCreditDenial = dataloan.sStatusTForTpoPortal == E_sStatusT.Loan_Rejected || dataloan.sStatusTForTpoPortal == E_sStatusT.Loan_Canceled,
                loanStatuses = this.GenerateLoanStatusesForProgressBar(dataloan),
                isRetailMode = isRetailMode,
                assignedOcAgents = isRetailMode ? null : this.GenerateAssignedOcAgents(dataloan),
                assignedLenderAgents = this.GenerateAssignedLenderAgents(isRetailMode, dataloan),
                isLead = isLead,
                constants = this.GetViewModelConstants(),
                canConvertLead = isLead && this.CurrentUserCanPerform(WorkflowOperations.LeadToLoanConversion),
                convertLeadBlockMessage = isLead ? string.Join("\n", this.GetReasonsUserCannotPerformOperation(WorkflowOperations.LeadToLoanConversion)) : null,
                this.PriceMyLoanUser.BrokerDB.HideStatusSubmissionButtonsInOriginatorPortal
            };

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("StatusAndAgentsViewModel", viewModel);
        }

        /// <summary>
        /// Determines whether the "view approval certificate" link will be visible
        /// on the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull data from to determine visibility.
        /// </param>
        /// <returns>
        /// True if the approval certificate link should be visible, false otherwise.
        /// </returns>
        private bool GetApprovalCertificateVisibility(CPageData dataloan)
        {
            return dataloan.sHasApprovalCertPdf &&
                CPageBase.s_OrderByStatusT[dataloan.sStatusTForTpoPortal] >= CPageBase.s_OrderByStatusT[E_sStatusT.Loan_Approved] &&
                CPageBase.s_OrderByStatusT[dataloan.sStatusTForTpoPortal] < CPageBase.s_OrderByStatusT[E_sStatusT.Loan_Canceled];
        }

        /// <summary>
        /// Generates a list of loan statuses for the progress bar on the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull status information from.
        /// </param>
        /// <returns>
        /// An <see cref="IEnumerable{T}"/> containing the loan statuses to register
        /// for the page.
        /// </returns>
        private IEnumerable<LoanStatusItem> GenerateLoanStatusesForProgressBar(CPageData dataloan)
        {
            var applicableStatuses = this.GetApplicableStatuses(dataloan);

            var expandedStatuses = this.GetExpandedStatuses(dataloan, applicableStatuses);

            var latestStatus = this.GetLatestStatus(dataloan, applicableStatuses);

            return from loanStatus in applicableStatuses
                   select this.CreateLoanStatusItem(dataloan, loanStatus, latestStatus, expandedStatuses);
        }

        /// <summary>
        /// Obtains a list of the applicable statuses to display on the page based on broker settings.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull statuses from.
        /// </param>
        /// <returns>
        /// A list of the applicable statuses in completion order.
        /// </returns>
        private List<E_sStatusT> GetApplicableStatuses(CPageData dataloan)
        {
            var statusList = Tools.sStatusTs_For_PmlSearchCriteria(
                this.PriceMyLoanUser.BrokerId,
                this.PriceMyLoanUser.PortalMode,
                dataloan.sCorrespondentProcessT);

            var brokerEnabledStatuses = this.PriceMyLoanUser.BrokerDB.GetEnabledStatusesByChannelAndProcessType(
                dataloan.sBranchChannelT,
                dataloan.sCorrespondentProcessT);

            Predicate<E_sStatusT> statusSelector = status =>
                brokerEnabledStatuses.Contains(status) &&
                !Tools.IsInactiveStatus(status) &&
                (status != E_sStatusT.Loan_DocumentCheckFailed ||
                status == dataloan.sStatusTForTpoPortal ||
                !string.IsNullOrWhiteSpace(dataloan.GetStatusDateRepFromStatus(status)));

            return (from status in statusList
                    where statusSelector(status)
                    orderby CPageBase.s_OrderByStatusT[status]
                    select status).ToList();
        }

        /// <summary>
        /// Gets the set of statuses that are expanded on the page to
        /// display progress.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull status data from.
        /// </param>
        /// <param name="applicableStatuses">
        /// The list of applicable statuses for the loan.
        /// </param>
        /// <returns>
        /// The set of expanded statuses: the first, last, current, previous,
        /// and next status for the loan.
        /// </returns>
        private HashSet<E_sStatusT> GetExpandedStatuses(CPageData dataloan, List<E_sStatusT> applicableStatuses)
        {
            var firstStatus = applicableStatuses.First();
            var lastStatus = applicableStatuses.Last();

            var expandedStatuses = new HashSet<E_sStatusT>
            {
                firstStatus,
                dataloan.sStatusTForTpoPortal,
                lastStatus,
            };

            if (dataloan.sStatusTForTpoPortal != firstStatus && applicableStatuses.Contains(dataloan.sStatusTForTpoPortal))
            {
                expandedStatuses.Add(applicableStatuses[applicableStatuses.IndexOf(dataloan.sStatusTForTpoPortal) - 1]);
            }

            if (dataloan.sStatusTForTpoPortal != lastStatus && applicableStatuses.Contains(dataloan.sStatusTForTpoPortal))
            {
                expandedStatuses.Add(applicableStatuses[applicableStatuses.IndexOf(dataloan.sStatusTForTpoPortal) + 1]);
            }

            return expandedStatuses;
        }

        /// <summary>
        /// Obtains the latest status that the loan is in.
        /// </summary>
        /// <remarks>
        /// If the loan is in an inactive status, this method will return the last
        /// active status with a non-blank status date for the page to display since
        /// the status bar will only reflect active statuses.
        /// </remarks>
        /// <param name="dataloan">
        /// The loan to pull data from.
        /// </param>
        /// <param name="applicableStatuses">
        /// The list of applicable statuses that the loan can be in.
        /// </param>
        /// <returns>
        /// The latest status that the loan is in.
        /// </returns>
        private E_sStatusT GetLatestStatus(CPageData dataloan, IEnumerable<E_sStatusT> applicableStatuses)
        {
            if (Tools.IsInactiveStatus(dataloan.sStatusTForTpoPortal))
            {
                // If the loan is in an inactive status, we want to display the most
                // recent status prior to when the loan was made inactive by working
                // backwards from the channel's terminal status. To prevent display 
                // errors due to picking up a later status, we will use these hardcoded 
                // terminal statuses defined in the spec in case the broker has not been 
                // set up correctly.
                var searchEndPoint = dataloan.sBranchChannelT == E_BranchChannelT.Correspondent ? E_sStatusT.Loan_Purchased : E_sStatusT.Loan_Funded;
                return dataloan.GetMostRecentStatusPriorTo(searchEndPoint, searchActiveStatuses: true) ?? applicableStatuses.First();
            }

            return dataloan.sStatusTForTpoPortal;
        }

        /// <summary>
        /// Creates a loan status model to display on the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull status data from.
        /// </param>
        /// <param name="loanStatus">
        /// The loan status to create a model.
        /// </param>
        /// <param name="latestStatus">
        /// The latest status that the loan is in.
        /// </param>
        /// <param name="expandedStatuses">
        /// The set of expanded statuses for the page.
        /// </param>
        /// <returns>
        /// A loan status model for page display.
        /// </returns>
        private LoanStatusItem CreateLoanStatusItem(CPageData dataloan, E_sStatusT loanStatus, E_sStatusT latestStatus, HashSet<E_sStatusT> expandedStatuses)
        {
            var statusIsCompleted = CPageBase.s_OrderByStatusT[loanStatus] <= CPageBase.s_OrderByStatusT[latestStatus];

            var item = new LoanStatusItem()
            {
                Active = dataloan.sStatusTForTpoPortal == loanStatus,
                Collapsed = !expandedStatuses.Contains(loanStatus),
                Complete = statusIsCompleted,
                Tooltip = this.GetStatusTooltip(dataloan, statusIsCompleted, loanStatus)
            };

            if (expandedStatuses.Contains(loanStatus))
            {
                item.Name = this.GetStatusName(loanStatus);
                item.Date = statusIsCompleted ? dataloan.GetStatusDateRepFromStatus(loanStatus) : null;
            }

            return item;
        }

        /// <summary>
        /// Gets the name of specified <paramref name="loanStatus"/>.
        /// </summary>
        /// <param name="loanStatus">
        /// The <see cref="E_sStatusT"/> status.
        /// </param>
        /// <returns>
        /// The name of the status.
        /// </returns>
        private string GetStatusName(E_sStatusT loanStatus)
        {
            string itemName;
            if (!AbbreviatedStatusNames.TryGetValue(loanStatus, out itemName))
            {
                itemName = this.GetTooltipText(loanStatus);
            }

            return itemName;
        }

        /// <summary>
        /// Gets the tooltip for the specified <paramref name="loanStatus"/>.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull data from.
        /// </param>
        /// <param name="statusIsCompleted">
        /// True if the loan has completed the status, false otherwise.
        /// </param>
        /// <param name="loanStatus">
        /// The status to generate a tooltip.
        /// </param>
        /// <returns>
        /// The tooltip for the loan status.
        /// </returns>
        private string GetStatusTooltip(CPageData dataloan, bool statusIsCompleted, E_sStatusT loanStatus)
        {
            if (!statusIsCompleted)
            {
                return this.GetTooltipText(loanStatus);
            }

            var statusDate = dataloan.GetStatusDateRepFromStatus(loanStatus);

            return $"{this.GetTooltipText(loanStatus)} <br /> {statusDate}";
        }

        /// <summary>
        /// Obtains the tooltip text for the specified <paramref name="loanStatus"/>.
        /// </summary>
        /// <param name="loanStatus">
        /// The status to obtain tooltip text.
        /// </param>
        /// <returns>
        /// The tooltip text for the status.
        /// </returns>
        private string GetTooltipText(E_sStatusT loanStatus)
        {
            string tooltipText;
            if (!CorrectedStatusNames.TryGetValue(loanStatus, out tooltipText))
            {
                tooltipText = CPageBase.sStatusT_map_rep(loanStatus);
            }

            return tooltipText;
        }

        /// <summary>
        /// Generates the data for the "Assigned Originating Company Agents" section
        /// of the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to pull assigned originating company agent information from.
        /// </param>
        /// <returns>
        /// A complex type containing the agents and name for the section.
        /// </returns>
        private object GenerateAssignedOcAgents(CPageData dataloan)
        {
            var agents = new List<AgentData>(4)
            {
                this.CreateAgentData(dataloan.sEmployeeLoanRep, Role.Get(E_RoleT.LoanOfficer)),
                this.CreateAgentData(dataloan.sEmployeeBrokerProcessor, Role.Get(E_RoleT.Pml_BrokerProcessor))
            };

            if (dataloan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeExternalSecondary, Role.Get(E_RoleT.Pml_Secondary)));
                agents.Add(this.CreateAgentData(dataloan.sEmployeeExternalPostCloser, Role.Get(E_RoleT.Pml_PostCloser)));
            }

            return new
            {
                sectionName = PmlBroker.RetrievePmlBrokerNameById(this.PriceMyLoanUser.PmlBrokerId, this.PriceMyLoanUser.BrokerId),
                agents = agents
            };
        }

        /// <summary>
        /// Generates the data for the "Assigned Lender Agents" section
        /// of the page.
        /// </summary>
        /// <param name="isRetailMode">
        /// True if the page is being displayed in the retail portal,
        /// false otherwise.
        /// </param>
        /// <param name="dataloan">
        /// The loan to pull assigned lender agent information from.
        /// </param>
        /// <returns>
        /// A complex type containing the agents and name for the section.
        /// </returns>
        private object GenerateAssignedLenderAgents(bool isRetailMode, CPageData dataloan)
        {
            var broker = this.PriceMyLoanUser.BrokerDB;
            var agents = new List<AgentData>(6);

            if (isRetailMode)
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeLoanRep, Role.Get(E_RoleT.LoanOfficer)));
            }

            if (isRetailMode || broker.ShowJuniorProcessorInPMLCertificate)
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeJuniorProcessor, Role.Get(E_RoleT.JuniorProcessor)));
            }

            if (isRetailMode || broker.ShowProcessorInPMLCertificate)
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeProcessor, Role.Get(E_RoleT.Processor)));
            }

            if (isRetailMode || broker.ShowJuniorUnderwriterInPMLCertificate)
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeJuniorUnderwriter, Role.Get(E_RoleT.JuniorUnderwriter)));
            }

            if (isRetailMode || broker.ShowUnderwriterInPMLCertificate)
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeUnderwriter, Role.Get(E_RoleT.Underwriter)));
            }

            if (isRetailMode)
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeManager, Role.Get(E_RoleT.Manager)));
            }
            else
            {
                agents.Add(this.CreateAgentData(dataloan.sEmployeeLenderAccExec, Role.Get(E_RoleT.LenderAccountExecutive)));
            }

            return new { sectionName = broker.Name, agents = agents };
        }

        /// <summary>
        /// Generates an agent model for the specified <paramref name="agent"/>.
        /// </summary>
        /// <param name="agent">
        /// The <see cref="CEmployeeFields"/> to pull data from.
        /// </param>
        /// <param name="role">
        /// The <see cref="Role"/> for the model.
        /// </param>
        /// <returns>
        /// An <see cref="AgentData"/> model for the agent.
        /// </returns>
        private AgentData CreateAgentData(CEmployeeFields agent, Role role)
        {
            var roleDescription = role.ModifiableDesc.Replace("(External)", string.Empty);

            var agentData = new AgentData()
            {
                RoleId = role.RoleT.ToString("D"),
                RoleName = roleDescription,
                Assigned = agent.IsValid,
                AssignmentLink = this.GetUserPickerUrl(role, roleDescription)
            };

            if (!agent.IsValid)
            {
                return agentData;
            }

            agentData.Name = agent.FullName;
            agentData.Email = agent.Email;
            agentData.Phone = agent.Phone;

            return agentData;
        }

        /// <summary>
        /// Obtains the URL for the agent assignment picker of the 
        /// specified <paramref name="role"/>.
        /// </summary>
        /// <param name="role">
        /// The <see cref="Role"/> to be assigned.
        /// </param>
        /// <param name="description">
        /// The description of the role.
        /// </param>
        /// <returns>
        /// The URL for the picker.
        /// </returns>
        private string GetUserPickerUrl(Role role, string description)
        {
            var rolePickerSettings = new PmlEmployeeRoleControlValues()
            {
                LoanId = this.LoanID,
                OwnerId = this.PriceMyLoanUser.UserId,
                RoleDesc = description,
                RoleId = role.Id,
                ShowMode = "fullsearch",
                Role = role.Desc,
                SearchFilter = string.Empty,
                EmployeeId = Guid.Empty
            };

            var settingsKey = PmlEmployeeRoleControlValues.Save(
                rolePickerSettings,
                PriceMyLoanConstants.CookieExpireMinutes);

            return this.Page.ResolveClientUrl("~/main/RoleSearchV2.aspx?k=" + settingsKey);
        }

        /// <summary>
        /// Obtains the constants for the view model.
        /// </summary>
        /// <returns>
        /// A complex type with the constants for the view model.
        /// </returns>
        private object GetViewModelConstants()
        {
            return new
            {
                loanOfficerRoleId = E_RoleT.LoanOfficer.ToString("D"),
                brokerProcessorRoleId = E_RoleT.Pml_BrokerProcessor.ToString("D"),
                externalSecondaryRoleId = E_RoleT.Pml_Secondary.ToString("D"),
                externalPostCloserRoleId = E_RoleT.Pml_PostCloser.ToString("D"),
                correspondentChannelMode = E_BranchChannelT.Correspondent,
                documentCheckStatus = E_sStatusT.Loan_DocumentCheck,
                conditionReviewStatus = E_sStatusT.Loan_ConditionReview,
            };
        }
    }
}
