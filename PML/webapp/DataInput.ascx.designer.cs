﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace PriceMyLoan.webapp {
    
    
    public partial class DataInput {
        
        /// <summary>
        /// GeneralInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PriceMyLoan.webapp.GeneralInfo GeneralInfo;
        
        /// <summary>
        /// LoanApplication control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PriceMyLoan.webapp.LoanApplication LoanApplication;
        
        /// <summary>
        /// LoanFilePricer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PriceMyLoan.webapp.LoanFilePricer LoanFilePricer;
    }
}
