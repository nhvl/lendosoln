<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="TPOFeeEditor.aspx.cs" Inherits="TPOFeeEditor.TPOFeeEditor" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Originator Portal Fee Editor</title>

    <script>
        var $ = jQuery;
        $(document).ready(function() {
            init();
            $('#InitEscrow').hide();
            toggleRecFFields();
            TPOStyleUnification.Components();
        });
        var loanId;
        var BrokerId;
        var sIsRequireFeesFromDropDown = false;
        var clientID = "";
        var HasSchedDueD1 = true;
        var IsNewPmlUIEnabled = true;
    </script>
</head>
<body>

<script>
    loanId = <%=AspxTools.JsString(LoanId) %>;
    BrokerId = <%=AspxTools.JsString(BrokerID) %>;
    sIsRequireFeesFromDropDown = <%= AspxTools.JsBool(IsRequireFeesFromDropDown)%>;
    HasSchedDueD1 = <%= AspxTools.JsBool(HasSchedDueD1)%>;
    IsNewPmlUIEnabled = <%= AspxTools.JsBool(IsNewPmlUIEnabled)%>;

      function toggleRecFFields()
      {
          var sRecFLckd = <%= AspxTools.JsGetElementById(sRecFLckd)%>;
          var div1201 = $("#div1201");
          var sRecDeed = <%= AspxTools.JsGetElementById(sRecDeed)%>;
          var sRecMortgage = <%= AspxTools.JsGetElementById(sRecMortgage)%>;
          var sRecRelease = <%= AspxTools.JsGetElementById(sRecRelease)%>;

          if( sRecFLckd.checked )
          {
            div1201.show();
            sRecDeed.readOnly = true;
            sRecMortgage.readOnly = true;
            sRecRelease.readOnly = true;
          }
          else
          {
            div1201.hide();
            sRecDeed.readOnly = false;
            sRecMortgage.readOnly = false;
            sRecRelease.readOnly = false;
          }
      }
</script>

<noscript>
    For full functionality of this site it is necessary to enable JavaScript. Here are
    the <a href="http://www.enable-javascript.com/" target="_blank">instructions how to
        enable JavaScript in your web browser</a>.
</noscript>
<div class="warp">
    <form runat="server">
        <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />
        <div name="content-detail" class="content-detail">
            <div class="warp-section warp-section-tabs legacy-closing-costs" loan-static-tab>
                <div class="static-tabs"><div class="static-tabs-inner">
                    <header class="page-header table-flex table-flex-app-info">
                        <div>Closing Costs</div>
                        <div id="LegendTable" runat="server" class="btnDiv t_runPricing">
                            <button type="button" id="runPricingBtn" runat="server" class="btn">Run Pricing</button>
                        </div>
                    </header>
                    <div ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>
                    <div class="text-right">
                        
                        <div id="tab-menu"><ul id="tabs" class="nav nav-tabs nav-tabs-panels">
                            <li class="first-tab"><a id="link1" href="#sec-800" class="selected">Section 800</a></li>
                            <li><a href="#sec-900">Section 900</a></li>
                            <li><a href="#sec-1000">Section 1000</a></li>
                            <li><a href="#sec-1100">Section 1100</a></li>
                            <li><a href="#sec-1200">Section 1200</a></li>
                            <li><a href="#sec-1300">Section 1300</a></li>
                        </ul></div>
                    </div>
                </div></div>
                <div class="content-tabs">
                    <div class="content-TPOFeeEditor">
                        <div class="notif text-danger text-right">
                            <span>POC = Paid Outside of Closing</span>
                            <span id="bsp_notif" class="bsp_notif">BSP = Borrower Selected Provider</span>
                        </div>
                        <table class="tb-tabs primary-table wide none-hover-table">
                            <thead class="header">
                                <tr>
                                    <th class="fix-width">
                                        Line #
                                    </th>
                                    <th id="header_escrowed" class="fix-width">
                                        Escrowed?
                                    </th>
                                    <th class="head-left">
                                        Fee Description
                                    </th>
                                    <th class="fix-width text-right">
                                        Fee Amount
                                    </th>
                                    <th id="lockSection1000"></th>
                                    <th class="fix-width">
                                        GFE Block
                                    </th>
                                    <th class="fix-width">
                                        APR Fee?
                                    </th>
                                    <th class="fix-width head-checkbox">
                                        POC?
                                    </th>
                                    <th class="fix-width">
                                        Paid By
                                    </th>
                                    <th id="header_paidTo" class="head-paidto">
                                        Paid To
                                    </th>
                                    <th id="header_bsp" class="fix-width head-checkbox">
                                        BSP?
                                    </th>
                                </tr>
                            </thead>
                            <!-- Section 800 -->
                            <tbody id="sec-800">
                                <tr>
                                    <td>
                                        801
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Loan origination fee</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sLOrigFPc"
                                                type="text" value="0.000 %" />&nbsp;+&nbsp;<ml:MoneyTextBox Width="80" class="input-textright"
                                                    runat="server" ID="sLOrigFMb" type="text" value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <label runat="server" id="sLOrigF">
                                            $ 0.00</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sLOrigFPage2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sLOrigFAPR" type="checkbox" value="Yes" />
                                        <input runat="server" disabled style="display: none;" id="sLOrigFFHA" type="checkbox"
                                            value="Yes" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sLOrigFBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList class="Hidden" runat="server" ID="sLOrigFPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft Hidden" runat="server" ID="sLOrigFPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr runat="server" id="sGfeIsTPOTransactionRow">
                                    <td></td>
                                    <td>
                                        <span class="FloatLeft">
                                            <label for="sGfeIsTPOTransaction" >This transaction involves a TPO?</label>
                                            <input type="checkbox" runat="server" id="sGfeIsTPOTransaction" />
                                        </span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Originator compensation</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sGfeOriginatorCompFPc"
                                                type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sGfeOriginatorCompFBaseT">
                                                </asp:DropDownList>
                                            &nbsp;+&nbsp;<ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sGfeOriginatorCompFMb" type="text" value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sGfeOriginatorCompF">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sGfeOriginatorCompPage2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sGfeOriginatorCompAPR" type="checkbox" /><label for="sGfeOriginatorCompAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sGfeOriginatorCompFHA"
                                            type="checkbox" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sGfeOriginatorCompBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList class="Hidden" runat="server" ID="sGfeOriginatorCompPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft Hidden" runat="server" ID="sGfeOriginatorCompPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        802
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Credit (-) or Charge (+)</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sLDiscntPc"
                                                               type="text" value="0.000 %" />
                                            &nbsp;of&nbsp;
                                            <asp:DropDownList runat="server" ID="sLDiscntBaseT"/>
                                            &nbsp;+&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sLDiscntFMb"
                                                type="text" value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sLDiscnt">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sLDiscntPage2">
                                            <asp:ListItem>A2</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Credit for lender paid fees</label>
                                        <div class="c-right">
                                            <asp:DropDownList runat="server" ID="sGfeCreditLenderPaidItemT"/>

                                            &nbsp;<ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sGfeCreditLenderPaidItemF"
                                                type="text" value="$ 0.00" disabled="disabled" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <label>
                                            &nbsp;</label>
                                    </td>
                                    <td>
                                        <label>
                                            &nbsp;</label>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            General Lender Credit</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sGfeLenderCreditFPc"
                                                type="text" value="0.000 %" disabled="disabled" />&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sGfeLenderCreditF"
                                                type="text" value="$ 0.00" disabled="disabled" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <label>
                                            &nbsp;</label>
                                    </td>
                                    <td>
                                        <label>
                                            &nbsp;</label>
                                    </td>
                                    <td>
                                        <input runat="server" id="sGfeLenderCreditAPR" type="checkbox" /><label for="sGfeLenderCreditAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sGfeLenderCreditFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Discount points</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sGfeDiscountPointFPc"
                                                type="text" value="0.000 %" disabled="disabled" />&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sGfeDiscountPointF"
                                                type="text" value="$ 0.00" disabled="disabled" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <label>
                                            &nbsp;</label>
                                    </td>
                                    <td>
                                        <label>
                                            &nbsp;</label>
                                    </td>
                                    <td>
                                        <input runat="server" id="sGfeDiscountPointAPR" type="checkbox" /><label for="sGfeDiscountPointAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sGfeDiscountPointFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sGfeDiscountPointBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        804
                                    </td>
                                    <td class="table-inside-description">
                                        <label>
                                            Appraisal fee</label>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sApprF" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sApprPage2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sApprAPR" type="checkbox" /><label for="sApprAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sApprFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sApprPOC" type="checkbox" /><label for="sApprPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sApprBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sApprPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sApprPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        805
                                    </td>
                                    <td class="table-inside-description">
                                        <label>
                                            Credit report fee</label>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sCrF" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sCrPage2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sCrAPR" type="checkbox" /><label for="sCrAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sCrFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sCrPOC" type="checkbox" /><label for="sCrPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sCrBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sCrPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sCrPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        806
                                    </td>
                                    <td class="table-inside-description">
                                        <label>
                                            Tax service fee</label>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sTxServF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sTxServPage2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sTxServAPR" type="checkbox" /><label for="sTxServAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sTxServFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sTxServPOC" type="checkbox" /><label for="sTxServPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sTxServBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sTxServPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sTxServPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        807
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Flood Certification</label>
                                        <div class="c-right">
                                            <asp:DropDownList runat="server" ID="sFloodCertificationDeterminationT">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sFloodCertificationF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sFloodCertificationPage2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sFloodCertificationAPR" type="checkbox" /><label for="sFloodCertificationAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sFloodCertificationFHA"
                                            type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sFloodCertificationPOC" type="checkbox" /><label for="sFloodCertificationPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sFloodCertificationBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sFloodCertificationPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sFloodCertificationPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        808
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Mortgage broker fee</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sMBrokFPc"
                                                type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sMBrokFBaseT">
                                                </asp:DropDownList>
                                            &nbsp;+&nbsp;<ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sMBrokFMb"
                                                type="text" value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sMBrokF">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sMBrokPage2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sMBrokAPR" type="checkbox" /><label for="sMBrokAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sMBrokFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sMBrokPOC" type="checkbox" /><label for="sMBrokPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sMBrokBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList class="Hidden" runat="server" ID="sMBrokPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft Hidden" runat="server" ID="sMBrokPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        809
                                    </td>
                                    <td class="table-inside-description">
                                        <label>
                                            Lender's inspection fee</label>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sInspectF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sInspectPage2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sInspectAPR" type="checkbox" /><label for="sInspectAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sInspectFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sInspectPOC" type="checkbox" /><label for="sInspectPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sInspectBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sInspectPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sInspectPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        810
                                    </td>
                                    <td class="table-inside-description">
                                        <label>
                                            Processing fee</label>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProcF" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sProcPage2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sProcAPR" type="checkbox" /><label for="sProcAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sProcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sProcPOC" type="checkbox" /><label for="sProcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sProcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sProcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sProcPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        811
                                    </td>
                                    <td class="table-inside-description">
                                        <label>
                                            Underwriting fee</label>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sUwF" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sUwPage2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sUwAPR" type="checkbox" /><label for="sUwAPR">Yes</label>
                                        <input runat="server" disabled style="display: none;" id="sUwFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sUwPOC" type="checkbox" /><label for="sUwPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sUwBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sUwPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sUwPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        812
                                    </td>
                                    <td class="table-inside-description">
                                        <label>
                                            Wire transfer</label>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sWireF" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sWirePage2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sWireAPR" type="checkbox" /><label for="sWireAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sWireFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sWirePOC" type="checkbox" /><label for="sWirePOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sWireBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sWirePaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sWirePaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        813
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="800" runat="server" ID="s800U1FDesc"></ml:ComboBox><asp:DropDownList
                                            Section="800" ID="s800U1FDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s800U1F" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U1Page2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U1APR" type="checkbox" /><label for="s800U1APR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="s800U1FHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U1POC" type="checkbox" /><label for="s800U1POC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U1By">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U1PaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="s800U1PaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        814
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="800" runat="server" ID="s800U2FDesc"></ml:ComboBox><asp:DropDownList
                                            Section="800" ID="s800U2FDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s800U2F" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U2Page2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U2APR" type="checkbox" /><label for="s800U2APR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="s800U2FHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U2POC" type="checkbox" /><label for="s800U2POC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U2By">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U2PaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="s800U2PaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        815
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="800" runat="server" ID="s800U3FDesc"></ml:ComboBox><asp:DropDownList
                                            Section="800" ID="s800U3FDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s800U3F" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U3Page2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U3APR" type="checkbox" /><label for="s800U3APR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="s800U3FHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U3POC" type="checkbox" /><label for="s800U3POC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U3By">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U3PaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="s800U3PaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        816
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="800" runat="server" ID="s800U4FDesc"></ml:ComboBox><asp:DropDownList
                                            Section="800" ID="s800U4FDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s800U4F" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U4Page2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U4APR" type="checkbox" /><label for="s800U4APR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="s800U4FHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U4POC" type="checkbox" /><label for="s800U4POC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U4By">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U4PaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="s800U4PaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        817
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="800" runat="server" ID="s800U5FDesc"></ml:ComboBox><asp:DropDownList
                                            Section="800" ID="s800U5FDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s800U5F" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U5Page2">
                                            <asp:ListItem Value="1">A1</asp:ListItem>
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U5APR" type="checkbox" /><label for="s800U5APR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="s800U5FHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s800U5POC" type="checkbox" /><label for="s800U5POC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U5By">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s800U5PaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="s800U5PaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Section 900 -->
                            <tbody id="sec-900">
                                <tr>
                                    <td>
                                        901
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Prepaid Interest</label>
                                        <div class="c-right">
                                            <asp:TextBox preset="numeric" runat="server" ID="sIPiaDy" type="text" class="input-w30 input-textright"
                                                disabled="disabled" value="0" />&nbsp;days, at&nbsp;
                                            <ml:MoneyTextBox Width="100" class="input-textright" runat="server" ID="sIPerDay"
                                                type="text" decimalPlace="6" value="$ 0.0000" disabled="disabled" />&nbsp;per
                                            day
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sIPia">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sIPage2">
                                            <asp:ListItem>B10</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sIAPR" type="checkbox" /><label for="sIAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sIFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sIPOC" type="checkbox" /><label for="sIPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sIBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" class="Hidden" ID="sIPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft Hidden" runat="server" ID="sIPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        902
                                    </td>
                                    <td class="table-inside-description">
                                        Mortgage Insurance Premium
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sMipPia">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sMipPage2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sMipAPR" type="checkbox" /><label for="sMipAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sMipFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sMipPOC" type="checkbox" /><label for="sMipPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sMipBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sMipPaidToDDL">
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sMipPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        903
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Haz. Ins.</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" runat="server" ID="sProHazInsR" type="text" class="input-w60 input-textright"
                                                value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sProHazInsT">
                                                </asp:DropDownList>
                                            &nbsp;+&nbsp;<ml:MoneyTextBox Width="80" runat="server" ID="sProHazInsMb" type="text"
                                                class="input-w80 input-textright" value="$ 0.00" />
                                            &nbsp;for&nbsp;<asp:TextBox preset="numeric" runat="server" ID="sHazInsPiaMon" type="text"
                                                class="input-w30 input-textright" value="0" />&nbsp;months
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sHazInsPia">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sProHazInsPage2">
                                            <asp:ListItem>B10</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sHazInsAPR" type="checkbox" /><label for="sHazInsAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sHazInsFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sHazInsPOC" type="checkbox" /><label for="sHazInsPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sHazInsBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sHazInsPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sHazInsPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        904
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="900" runat="server" ID="s904PiaDesc"></ml:ComboBox><asp:DropDownList
                                            Section="900" ID="s904PiaDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s904Pia" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s904Page2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                            <asp:ListItem Value="11">B11</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s904APR" type="checkbox" /><label for="s904APR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="s904FHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s904POC" type="checkbox" /><label for="s904POC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s904By">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList class="Hidden" runat="server" ID="s904PaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft Hidden" runat="server" ID="s904PaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        905
                                    </td>
                                    <td class="table-inside-description">
                                        VA Funding Fee
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sVaFf">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="VAFundPage2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sVaAPR" type="checkbox" /><label for="sVaAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sVaFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sVaPOC" type="checkbox" /><label for="sVaPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sVaBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sVaPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sVaPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        906
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="900" runat="server" ID="s900U1PiaDesc"></ml:ComboBox><asp:DropDownList
                                            Section="900" ID="s900U1PiaDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s900U1Pia"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s900U1Page2">
                                            <asp:ListItem Value="3">B3</asp:ListItem>
                                            <asp:ListItem Value="11">B11</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s900U1APR" type="checkbox" /><label for="s900U1APR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="s900U1FHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s900U1POC" type="checkbox" /><label for="s900U1POC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s900U1By">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" class="Hidden" ID="s900U1PaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft Hidden" runat="server" ID="s900U1PaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Section 1000 -->
                            <tbody id="sec-1000">
                                <tr>
                                    <td>
                                        1002
                                    </td>
                                    <td>
                                        <input runat="server" id="sHazInsRsrvEscrowedTri" type="checkbox" /><label for="sHazInsRsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Hazard Insurance Reserve</label>
                                        <div class="c-right">

                                            <asp:TextBox preset="numeric" runat="server" ID="sHazInsRsrvMon" disabled="disabled"
                                                type="text" class="input-w30 input-textright" value="0" />
                                            <label class="lock-checkbox"><input runat="server" id="sHazInsRsrvMonLckd" type="checkbox" /></label>
                                            months, at&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProHazIns"
                                                type="text" value="$ 0.00" disabled="disabled" />&nbsp;/month
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sHazInsRsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sHazInsRsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sHazInsRsrvAPR" type="checkbox" /><label for="sHazInsRsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sHazInsRsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sHazInsRsrvPOC" type="checkbox" /><label for="sHazInsRsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sHazInsRsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1003
                                    </td>
                                    <td>
                                        <input runat="server" id="sMInsRsrvEscrowedTri" type="checkbox" /><label for="sMInsRsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Mortgage Insurance Reserve</label>
                                        <div class="c-right">
                                            <asp:TextBox preset="numeric" runat="server" ID="sMInsRsrvMon" disabled="disabled"
                                                type="text" class="input-w30 input-textright" value="" />
                                            <label class="lock-checkbox"><input runat="server" id="sMInsRsrvMonLckd" type="checkbox" /></label>
                                            months, at&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProMIns"
                                                type="text" value="" disabled="disabled" />&nbsp;/month
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sMInsRsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sMInsRsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sMInsRsrvAPR" type="checkbox" /><label for="sMInsRsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sMInsRsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sMInsRsrvPOC" type="checkbox" /><label for="sMInsRsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sMInsRsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1004
                                    </td>
                                    <td>
                                        <input runat="server" id="sRealETxRsrvEscrowedTri" type="checkbox" /><label for="sRealETxRsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Real Estate Tax Reserve</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sProRealETxR"
                                                type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sProRealETxT">
                                                </asp:DropDownList>
                                            &nbsp;+&nbsp;<ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProRealETxMb"
                                                type="text" value="$ 0.00" />&nbsp;for&nbsp;
                                            <asp:TextBox preset="numeric" runat="server" ID="sRealETxRsrvMon" disabled="disabled" type="text" class="input-w30 input-textright" value="0" />
                                            <label class="lock-checkbox"><input runat="server" id="sRealETxRsrvMonLckd" type="checkbox" /></label>
                                            months
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sRealETxRsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sRealETxRsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sRealETxRsrvAPR" type="checkbox" /><label for="sRealETxRsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sRealETxRsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sRealETxRsrvPOC" type="checkbox" /><label for="sRealETxRsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sRealETxRsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1005
                                    </td>
                                    <td>
                                        <input runat="server" id="sSchoolTxRsrvEscrowedTri" type="checkbox" /><label for="sSchoolTxRsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            School Taxes</label>
                                        <div class="c-right">
                                            <asp:TextBox preset="numeric" runat="server" ID="sSchoolTxRsrvMon" disabled="disabled"
                                                type="text" class="input-w30 input-textright" value="0" />
                                            <label class="lock-checkbox"><input runat="server" id="sSchoolTxRsrvMonLckd" type="checkbox" /></label>
                                            months, at&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProSchoolTx"
                                                type="text" value="$ 0.00" />&nbsp;/month
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sSchoolTxRsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sSchoolTxRsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sSchoolTxRsrvAPR" type="checkbox" /><label for="sSchoolTxRsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sSchoolTxRsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sSchoolTxRsrvPOC" type="checkbox" /><label for="sSchoolTxRsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sSchoolTxRsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1006
                                    </td>
                                    <td>
                                        <input runat="server" id="sFloodInsRsrvEscrowedTri" type="checkbox" /><label for="sFloodInsRsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Flood Ins Reserve</label>
                                        <div class="c-right">
                                            <asp:TextBox preset="numeric" runat="server" ID="sFloodInsRsrvMon" disabled="disabled"
                                                type="text" class="input-w30 input-textright" value="0" />
                                            <label class="lock-checkbox"><input runat="server" id="sFloodInsRsrvMonLckd" type="checkbox" /></label>
                                            months, at&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProFloodIns"
                                                type="text" value="$ 0.00" />&nbsp;/month
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sFloodInsRsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sFloodInsRsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sFloodInsRsrvAPR" type="checkbox" /><label for="sFloodInsRsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sFloodInsRsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sFloodInsRsrvPOC" type="checkbox" /><label for="sFloodInsRsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sFloodInsRsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1007
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            Aggregate Adjustment</label>
                                        <button type="button" runat="server" nohighlight
                                            id="btnInitEscrow" class="c-right btn btn-default">Setup Initial Escrow Account</button>
                                    </td>
                                    <td class="table-inside-amount">
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sAggregateAdjRsrv"
                                                disabled="disabled" type="text" value="$ 0.00" />
                                    </td>
                                    <td class="none-paddingLeft">
                                            <label class="lock-checkbox"><input runat="server" id="sAggregateAdjRsrvLckd" type="checkbox" /></label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sAggregateAdjRsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sAggregateAdjRsrvAPR" type="checkbox" /><label for="sAggregateAdjRsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sAggregateAdjRsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sAggregateAdjRsrvPOC" type="checkbox" /><label for="sAggregateAdjRsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sAggregateAdjRsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1008
                                    </td>
                                    <td>
                                        <input runat="server" id="s1006RsrvEscrowedTri" type="checkbox" /><label for="s1006RsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-style">
                                        <table>
                                            <tr>
                                                <td class="tb-left none-padding-left">
                                                    <ml:ComboBox CssClass="FeeTypeComboBox" Section="1000" runat="server" ID="s1006ProHExpDesc"></ml:ComboBox><asp:DropDownList
                                                        Section="1000" ID="s1006ProHExpDescDDL" class="ComboBoxDescDDL" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tb-right">
                                                    <asp:TextBox preset="numeric" runat="server" ID="s1006RsrvMon" disabled="disabled"
                                                        type="text" class="input-w30 input-textright" value="0" />
                                                    <label class="lock-checkbox"><input runat="server" id="s1006RsrvMonLckd" type="checkbox" /></label>
                                                    months, at&nbsp;
                                                    <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s1006ProHExp"
                                                        type="text" value="$ 0.00" />&nbsp;/month
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="s1006Rsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s1006RsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s1006RsrvAPR" type="checkbox" /><label for="s1006RsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="s1006RsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s1006RsrvPOC" type="checkbox" /><label for="s1006RsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s1006RsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1009
                                    </td>
                                    <td>
                                        <input runat="server" id="s1007RsrvEscrowedTri" type="checkbox" /><label for="s1007RsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-style">
                                        <table>
                                            <tr>
                                                <td class="tb-left none-padding-left">
                                                    <ml:ComboBox CssClass="FeeTypeComboBox" Section="1000" runat="server" ID="s1007ProHExpDesc"></ml:ComboBox><asp:DropDownList
                                                        Section="1000" ID="s1007ProHExpDescDDL" class="ComboBoxDescDDL" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tb-right">
                                                    <asp:TextBox preset="numeric" runat="server" ID="s1007RsrvMon" disabled="disabled"
                                                        type="text" class="input-w30 input-textright" value="0" />
                                                    <label class="lock-checkbox"><input runat="server" id="s1007RsrvMonLckd" type="checkbox" /></label>
                                                    months, at&nbsp;
                                                    <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="s1007ProHExp"
                                                        type="text" value="$ 0.00" />&nbsp;/month
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="s1007Rsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s1007RsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="s1007RsrvAPR" type="checkbox" /><label for="s1007RsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="s1007RsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="s1007RsrvPOC" type="checkbox" /><label for="s1007RsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="s1007RsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="row1010">
                                    <td>
                                        1010
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3RsrvEscrowedTri" type="checkbox" /><label for="sU3RsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-style">
                                        <table>
                                            <tr>
                                                <td class="tb-left none-padding-left">
                                                    <ml:ComboBox CssClass="FeeTypeComboBox" Section="1000" runat="server" ID="sU3RsrvDesc"></ml:ComboBox><asp:DropDownList
                                                        Section="1000" ID="sU3RsrvDescDDL" class="ComboBoxDescDDL" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tb-right">
                                                    <asp:TextBox preset="numeric" runat="server" ID="sU3RsrvMon" disabled="disabled"
                                                        type="text" class="input-w30 input-textright" value="0" />
                                                    <label class="lock-checkbox"><input runat="server" id="sU3RsrvMonLckd" type="checkbox" /></label>
                                                    months, at&nbsp;
                                                    <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProU3Rsrv"
                                                        type="text" value="$ 0.00" />&nbsp;/month
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sU3Rsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3RsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3RsrvAPR" type="checkbox" /><label for="sU3RsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sU3RsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3RsrvPOC" type="checkbox" /><label for="sU3RsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3RsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="row1011">
                                    <td>
                                        1011
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4RsrvEscrowedTri" type="checkbox" /><label for="sU4RsrvEscrowedTri">Yes</label>
                                    </td>
                                    <td class="table-style">
                                        <table>
                                            <tr>
                                                <td class="tb-left none-padding-left">
                                                    <ml:ComboBox CssClass="FeeTypeComboBox" Section="1000" runat="server" ID="sU4RsrvDesc"></ml:ComboBox><asp:DropDownList
                                                        Section="1000" ID="sU4RsrvDescDDL" class="ComboBoxDescDDL" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tb-right">
                                                    <asp:TextBox preset="numeric" runat="server" ID="sU4RsrvMon" disabled="disabled"
                                                        type="text" class="input-w30 input-textright" value="0" />
                                                    <label class="lock-checkbox"><input runat="server" id="sU4RsrvMonLckd" type="checkbox" /></label>
                                                    months, at&nbsp;
                                                    <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sProU4Rsrv"
                                                        type="text" value="$ 0.00" />&nbsp;/month
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sU4Rsrv">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4RsrvPage2">
                                            <asp:ListItem>B9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4RsrvAPR" type="checkbox" /><label for="sU4RsrvAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sU4RsrvFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4RsrvPOC" type="checkbox" /><label for="sU4RsrvPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4RsrvBy">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Section 1100 -->
                            <tbody id="sec-1100">
                                <tr>
                                    <td>
                                        1102
                                    </td>
                                    <td class="table-inside-description">
                                        Closing / Escrow Fee
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sEscrowF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sEscrowPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sEscrowAPR" type="checkbox" /><label for="sEscrowAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sEscrowFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sEscrowPOC" type="checkbox" /><label for="sEscrowPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sEscrowBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sEscrowPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sEscrowPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sEscrowBSP" type="checkbox" /><label for="sEscrowBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1103
                                    </td>
                                    <td class="table-inside-description">
                                        Owner's Title Insurance
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sOwnerTitleInsF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sOwnerTitleInsPage2">
                                            <asp:ListItem>B5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sOwnerTitleInsAPR" type="checkbox" /><label for="sOwnerTitleInsAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sOwnerTitleInsFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sOwnerTitleInsPOC" type="checkbox" /><label for="sOwnerTitleInsPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sOwnerTitleInsBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sOwnerTitleInsPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sOwnerTitleInsPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sOwnerTitleInsBSP" type="checkbox" /><label for="sOwnerTitleInsBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1104
                                    </td>
                                    <td class="table-inside-description">
                                        Lender's Title Insurance
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sTitleInsF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sTitleInsPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sTitleInsAPR" type="checkbox" /><label for="sTitleInsAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sTitleInsFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sTitleInsPOC" type="checkbox" /><label for="sTitleInsPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sTitleInsBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sTitleInsPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sTitleInsPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sTitleInsBSP" type="checkbox" /><label for="sTitleInsBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1109
                                    </td>
                                    <td class="table-inside-description">
                                        Document Preparation Fee
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sDocPrepF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sDocPrepPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sDocPrepAPR" type="checkbox" /><label for="sDocPrepAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sDocPrepFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sDocPrepPOC" type="checkbox" /><label for="sDocPrepPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sDocPrepBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sDocPrepPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sDocPrepPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sDocPrepBSP" type="checkbox" /><label for="sDocPrepBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1110
                                    </td>
                                    <td class="table-inside-description">
                                        Notary Fees
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sNotaryF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sNotaryPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sNotaryAPR" type="checkbox" /><label for="sNotaryAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sNotaryFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sNotaryPOC" type="checkbox" /><label for="sNotaryPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sNotaryBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sNotaryPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sNotaryPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sNotaryBSP" type="checkbox" /><label for="sNotaryBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1111
                                    </td>
                                    <td class="table-inside-description">
                                        Attorney Fees
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sAttorneyF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sAttorneyPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sAttorneyAPR" type="checkbox" /><label for="sAttorneyAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sAttorneyFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sAttorneyPOC" type="checkbox" /><label for="sAttorneyPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sAttorneyBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sAttorneyPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sAttorneyPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sAttorneyBSP" type="checkbox" /><label for="sAttorneyBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1112
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1100" runat="server" ID="sU1TcDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1100" ID="sU1TcDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU1Tc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1TcPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1TcAPR" type="checkbox" /><label for="sU1TcAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU1TcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1TcPOC" type="checkbox" /><label for="sU1TcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1TcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1TcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU1TcPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1TcBSP" type="checkbox" /><label for="sU1TcBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1113
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1100" runat="server" ID="sU2TcDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1100" ID="sU2TcDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU2Tc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2TcPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2TcAPR" type="checkbox" /><label for="sU2TcAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU2TcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2TcPOC" type="checkbox" /><label for="sU2TcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2TcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2TcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU2TcPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2TcBSP" type="checkbox" /><label for="sU2TcBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1114
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1100" runat="server" ID="sU3TcDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1100" ID="sU3TcDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU3Tc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3TcPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3TcAPR" type="checkbox" /><label for="sU3TcAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU3TcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3TcPOC" type="checkbox" /><label for="sU3TcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3TcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3TcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU3TcPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3TcBSP" type="checkbox" /><label for="sU3TcBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1115
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1100" runat="server" ID="sU4TcDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1100" ID="sU4TcDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU4Tc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4TcPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4TcAPR" type="checkbox" /><label for="sU4TcAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU4TcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4TcPOC" type="checkbox" /><label for="sU4TcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4TcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4TcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU4TcPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4TcBSP" type="checkbox" /><label for="sU4TcBSP">Yes</label>
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Section 1200 -->
                            <tbody id="sec-1200">
                                <tr>
                                    <td>
                                        1201
                                    </td>
                                    <td class="table-inside-description">
                                        <div class="c-left">
                                            <label>Recording Fees</label>
                                            <input runat="server" id="sRecFLckd" type="checkbox" onclick="toggleRecFFields();"/>
                                        </div>
                                        <div id="div1201" class="c-right padding-top10">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sRecFPc"
                                                type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sRecBaseT">
                                                </asp:DropDownList>
                                            &nbsp;+&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sRecFMb" type="text"
                                                value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sRecF">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sRecFPage2">
                                            <asp:ListItem>B7</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sRecFAPR" type="checkbox" /><label for="sRecFAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sRecFFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sRecFPOC" type="checkbox" /><label for="sRecFPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sRecFBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sRecFPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sRecFPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1202
                                    </td>
                                    <td class="table-inside-description">
                                        <div class="c-right">
                                            Deed <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sRecDeed" type="text" value="$ 0.00" />
                                            Mortgage <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sRecMortgage" type="text" value="$ 0.00" />
                                            Release <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sRecRelease" type="text" value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        1204
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            City/County Tax Stamps</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sCountyRtcPc"
                                                type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sCountyRtcBaseT">
                                                </asp:DropDownList>
                                            &nbsp;+&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sCountyRtcMb"
                                                type="text" value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sCountyRtc">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sCountyRtcPage2">
                                            <asp:ListItem Value="8">B8</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sCountyRtcAPR" type="checkbox" /><label for="sCountyRtcAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sCountyRtcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sCountyRtcPOC" type="checkbox" /><label for="sCountyRtcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sCountyRtcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sCountyRtcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sCountyRtcPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1205
                                    </td>
                                    <td class="table-inside-description">
                                        <label class="c-left">
                                            State Tax Stamps</label>
                                        <div class="c-right">
                                            <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sStateRtcPc"
                                                type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sStateRtcBaseT">
                                                </asp:DropDownList>
                                            &nbsp;+&nbsp;
                                            <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sStateRtcMb"
                                                type="text" value="$ 0.00" />
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sStateRtc">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sStateRtcPage2">
                                            <asp:ListItem Value="8">B8</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sStateRtcAPR" type="checkbox" /><label for="sStateRtcAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sStateRtcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sStateRtcPOC" type="checkbox" /><label for="sStateRtcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sStateRtcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sStateRtcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sStateRtcPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1206
                                    </td>
                                    <td class="table-style none-padding-left">
                                        <table>
                                            <tr>
                                                <td class="tb-left">
                                                    <ml:ComboBox CssClass="FeeTypeComboBox" Section="1200" runat="server" ID="sU1GovRtcDesc"></ml:ComboBox><asp:DropDownList
                                                        Section="1200" ID="sU1GovRtcDescDDL" class="ComboBoxDescDDL" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tb-right">
                                                    <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sU1GovRtcPc"
                                                        type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sU1GovRtcBaseT">
                                                        </asp:DropDownList>
                                                    &nbsp;+&nbsp;
                                                    <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU1GovRtcMb"
                                                        type="text" value="$ 0.00" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sU1GovRtc">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1GovRtcPage2">
                                            <asp:ListItem Value="7">B7</asp:ListItem>
                                            <asp:ListItem Value="8">B8</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1GovRtcAPR" type="checkbox" /><label for="sU1GovRtcAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sU1GovRtcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1GovRtcPOC" type="checkbox" /><label for="sU1GovRtcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1GovRtcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1GovRtcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU1GovRtcPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1207
                                    </td>
                                    <td class="table-style none-padding-left">
                                        <table>
                                            <tr>
                                                <td class="tb-left">
                                                    <ml:ComboBox CssClass="FeeTypeComboBox" Section="1200" runat="server" ID="sU2GovRtcDesc"></ml:ComboBox><asp:DropDownList
                                                        Section="1200" ID="sU2GovRtcDescDDL" class="ComboBoxDescDDL" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tb-right">
                                                    <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sU2GovRtcPc"
                                                        type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sU2GovRtcBaseT">
                                                        </asp:DropDownList>
                                                    &nbsp;+&nbsp;
                                                    <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU2GovRtcMb"
                                                        type="text" value="$ 0.00" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="c-right">
                                        </div>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sU2GovRtc">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2GovRtcPage2">
                                            <asp:ListItem Value="7">B7</asp:ListItem>
                                            <asp:ListItem Value="8">B8</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2GovRtcAPR" type="checkbox" /><label for="sU2GovRtcAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sU2GovRtcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2GovRtcPOC" type="checkbox" /><label for="sU2GovRtcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2GovRtcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2GovRtcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU2GovRtcPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1208
                                    </td>
                                    <td class="table-style none-padding-left">
                                        <table>
                                            <tr>
                                                <td class="tb-left">
                                                    <ml:ComboBox CssClass="FeeTypeComboBox" Section="1200" runat="server" ID="sU3GovRtcDesc"></ml:ComboBox><asp:DropDownList
                                                        Section="1200" ID="sU3GovRtcDescDDL" class="ComboBoxDescDDL" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tb-right">
                                                    <ml:PercentTextBox Width="60" class="input-textright" runat="server" ID="sU3GovRtcPc"
                                                        type="text" value="0.000 %" />&nbsp;of&nbsp;<asp:DropDownList runat="server" ID="sU3GovRtcBaseT">
                                                        </asp:DropDownList>
                                                    &nbsp;+&nbsp;
                                                    <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU3GovRtcMb"
                                                        type="text" value="$ 0.00" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:EncodedLabel preset="money" runat="server" ID="sU3GovRtc">$ 0.00</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3GovRtcPage2">
                                            <asp:ListItem  Value="7">B7</asp:ListItem>
                                            <asp:ListItem  Value="8">B8</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3GovRtcAPR" type="checkbox" /><label for="sU3GovRtcAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sU3GovRtcFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3GovRtcPOC" type="checkbox" /><label for="sU3GovRtcPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3GovRtcBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3GovRtcPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU3GovRtcPaidTo"
                                            type="text" />
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Section 1300 -->
                            <tbody id="sec-1300">
                                <tr>
                                    <td>
                                        1302
                                    </td>
                                    <td class="table-inside-description">
                                        Pest Inspection
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sPestInspectF"
                                            type="text" value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sPestInspectPage2">
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sPestInspectAPR" type="checkbox" /><label for="sPestInspectAPR">Yes</label><input runat="server"
                                            disabled style="display: none;" id="sPestInspectFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sPestInspectPOC" type="checkbox" /><label for="sPestInspectPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sPestInspectBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sPestInspectPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sPestInspectPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sPestInspectBSP" type="checkbox" /><label for="sPestInspectBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1303
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1300" runat="server" ID="sU1ScDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1300" ID="sU1ScDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU1Sc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1ScPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1ScAPR" type="checkbox" /><label for="sU1ScAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU1ScFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1ScPOC" type="checkbox" /><label for="sU1ScPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1ScBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU1ScPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU1ScPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU1ScBSP" type="checkbox" /><label for="sU1ScBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1304
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1300" runat="server" ID="sU2ScDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1300" ID="sU2ScDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU2Sc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2ScPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2ScAPR" type="checkbox" /><label for="sU2ScAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU2ScFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2ScPOC" type="checkbox" /><label for="sU2ScPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2ScBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU2ScPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU2ScPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU2ScBSP" type="checkbox" /><label for="sU2ScBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1305
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1300" runat="server" ID="sU3ScDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1300" ID="sU3ScDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU3Sc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3ScPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3ScAPR" type="checkbox" /><label for="sU3ScAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU3ScFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3ScPOC" type="checkbox" /><label for="sU3ScPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3ScBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU3ScPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU3ScPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU3ScBSP" type="checkbox" /><label for="sU3ScBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1306
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1300" runat="server" ID="sU4ScDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1300" ID="sU4ScDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU4Sc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4ScPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4ScAPR" type="checkbox" /><label for="sU4ScAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU4ScFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4ScPOC" type="checkbox" /><label for="sU4ScPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4ScBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU4ScPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU4ScPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU4ScBSP" type="checkbox" /><label for="sU4ScBSP">Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1307
                                    </td>
                                    <td class="table-inside-description-stretch">
                                        <ml:ComboBox CssClass="FeeTypeComboBox" Section="1300" runat="server" ID="sU5ScDesc"></ml:ComboBox><asp:DropDownList
                                            Section="1300" ID="sU5ScDescDDL" class="ComboBoxDescDDL" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="table-inside-amount">
                                        <ml:MoneyTextBox Width="80" class="input-textright" runat="server" ID="sU5Sc" type="text"
                                            value="$ 0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU5ScPage2">
                                            <asp:ListItem Value="4">B4</asp:ListItem>
                                            <asp:ListItem Value="6">B6</asp:ListItem>
                                            <asp:ListItem Value="12">N/A</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <input runat="server" id="sU5ScAPR" type="checkbox" /><label for="sU5ScAPR">Yes</label><input runat="server" disabled
                                            style="display: none;" id="sU5ScFHA" type="checkbox" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU5ScPOC" type="checkbox" /><label for="sU5ScPOC">Yes</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU5ScBy">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="sU5ScPaidToDDL">
                                            <asp:ListItem Value="0">Lender</asp:ListItem>
                                            <asp:ListItem Value="1">Broker</asp:ListItem>
                                            <asp:ListItem Value="2">Other</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox value="" class="input-w200 input-textleft" runat="server" ID="sU5ScPaidTo"
                                            type="text" />
                                    </td>
                                    <td>
                                        <input runat="server" id="sU5ScBSP" type="checkbox" /><label for="sU5ScBSP">Yes</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="btnright">
                            <button type="button" class="btn btn-default" runat="server"
                                id="btn_prev" onmouseup="$(this).blur();">Previous Section</button>&nbsp;&nbsp;
                            <button type="button" class="btn btn-default" runat="server"
                                id="btn_next" onmouseup="$(this).blur();">Next Section</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---------------------------------DIV POPUP------------------------------------------>
        <div runat="server" id="InitEscrow" title="Initial Escrow Account Setup">
            <div class="dialog_title margin-bottom">
                <div class="table table-modal initial-escrow-acc-setup">
                    <div>
                        <div>
                            <label>Estimated Closing Date</label>
                            <asp:TextBox Width="80" preset="date" runat="server" ID="sEstCloseD" type="text"
                                disabled="disabled" class="input-w80 input-textleft" />
                            <label class="lock-checkbox"><input runat="server" id="sEstCloseDLckd" type="checkbox" /></label>
                        </div>
                        <div>
                            <label>First Payment Date</label>
                            <asp:TextBox Width="80" preset="date" runat="server" ID="sSchedDueD1" type="text"
                                disabled="disabled" class="input-w80 input-textleft" />
                            <label class="lock-checkbox"><input runat="server" id="sSchedDueD1Lckd" type="checkbox" /></label>
                        </div>
                    </div>
                </div>
                <p class="input-redtext input-textleft clearBoth">
                    Note: First Payment Date is required to calculate the reserve months and aggregate
                    escrow adjustment.
                </p>
            </div>
            <hr />
            <label class="input-textleft input-redtext">
                Instructions: For each row, enter the number of months worth of premium/tax due
                within a column's specified month.</label>
            <table width="100%" class="tb-tabs table-description-initial-esc-acc-setup">
                <thead>
                    <tr>
                        <th class="head-left">
                            Description
                        </th>
                        <th>
                            Cushion<br />
                            (months)
                        </th>
                        <th class="fix-width">
                            Jan
                        </th>
                        <th class="fix-width">
                            Feb
                        </th>
                        <th class="fix-width">
                            Mar
                        </th>
                        <th class="fix-width">
                            Apr
                        </th>
                        <th class="fix-width">
                            May
                        </th>
                        <th class="fix-width">
                            Jun
                        </th>
                        <th class="fix-width">
                            Jul
                        </th>
                        <th class="fix-width">
                            Aug
                        </th>
                        <th class="fix-width">
                            Sep
                        </th>
                        <th class="fix-width">
                            Oct
                        </th>
                        <th class="fix-width">
                            Nov
                        </th>
                        <th class="fix-width">
                            Dec
                        </th>
                        <th>
                            Recommended Reserve Months
                        </th>
                    </tr>
                </thead>
                <tbody id="Tbody1">
                    <tr>
                        <td class="dialog-description">
                            Hazard Insurance
                        </td>
                        <td>
                            <asp:TextBox autofocus="autofocus" runat="server" ID="HazIns0" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns1" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns2" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns3" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns4" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns5" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns6" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns7" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns8" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns9" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns10" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns11" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="HazIns12" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sHazInsRsrvMonRecommended" disabled="disabled" type="text"
                                class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dialog-description">
                            Mortgage Insurance
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns0" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns1" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns2" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns3" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns4" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns5" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns6" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns7" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns8" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns9" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns10" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns11" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="MortIns12" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sMInsRsrvMonRecommended" disabled="disabled" type="text"
                                class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <!--nhvl-->
                    <tr>
                        <td class="dialog-description">
                            Real Estate Taxes
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax0" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax1" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax2" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax3" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax4" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax5" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax6" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax7" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax8" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax9" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax10" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax11" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RETax12" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sRealETxRsrvMonRecommended" disabled="disabled" type="text"
                                class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dialog-description">
                            School Taxes
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax0" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax1" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax2" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax3" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax4" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax5" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax6" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax7" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax8" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax9" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax10" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax11" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="SchoTax12" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sSchoolTxRsrvMonRecommended" disabled="disabled"
                                type="text" class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dialog-description">
                            Flood Insurance
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns0" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns1" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns2" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns3" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns4" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns5" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns6" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns7" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns8" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns9" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns10" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns11" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FloIns12" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sFloodInsRsrvMonRecommended" disabled="disabled"
                                type="text" class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dialog-description">
                            <span id="s1006ProHExpDescSpan" runat="server">User Defined 1</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef10" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef11" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef12" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef13" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef14" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef15" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef16" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef17" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef18" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef19" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef110" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef111" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef112" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="s1006RsrvMonRecommended" disabled="disabled" type="text"
                                class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dialog-description">
                            <span id="s1007ProHExpDescSpan" runat="server">User Defined 2</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef20" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef21" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef22" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef23" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef24" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef25" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef26" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef27" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef28" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef29" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef210" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef211" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef212" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="s1007RsrvMonRecommended" disabled="disabled" type="text"
                                class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <tr id="row1010Popup" runat="server">
                        <td class="dialog-description">
                            <span id="sU3RsrvDescSpan" runat="server">User Defined 3</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef30" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef31" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef32" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef33" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef34" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef35" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef36" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef37" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef38" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef39" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef310" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef311" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef312" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sU3RsrvMonRecommended" disabled="disabled" type="text"
                                class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                    <tr id="row1011Popup" runat="server">
                        <td class="dialog-description">
                            <span id="sU4RsrvDescSpan" runat="server">User Defined 4</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef40" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef41" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef42" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef43" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef44" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef45" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef46" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef47" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef48" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef49" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef410" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef411" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="UsrDef412" type="text" class="input-w30 input-textright"
                                value="" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sU4RsrvMonRecommended" disabled="disabled" type="text"
                                class="input-w30 input-textright" value="" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div runat="server" id="InitialDatesDiv" style="display: none; overflow-y: auto"
            title="Set Estimated Closing and First Payment Dates">
            <div class="dialog_title">
                <p class="input-redtext padding-bottom-1em">
                    Please supply the following dates to allow for more accurate escrow calculations.
                </p>
                <div>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Estimated Closing Date</strong>
                                </td>
                                <td>
                                    <strong>First Payment Date</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox  preset="date" runat="server" ID="sEstCloseDInitial" type="text"
                                    disabled="disabled" />
                                    <label class="lock-checkbox"><input runat="server" id="sEstCloseDLckdInitial" type="checkbox" /></label>
                                </td>
                                <td>
                                    <asp:TextBox  preset="date" runat="server" ID="sSchedDueD1Initial" type="text"
                                        disabled="disabled" />
                                    <label class="lock-checkbox"><input runat="server" id="sSchedDueD1LckdInitial" type="checkbox" /></label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="ErrorMessageDiv">
    <span id="ErrorMessageSpan"></span>
</div>
</body>
</html>
