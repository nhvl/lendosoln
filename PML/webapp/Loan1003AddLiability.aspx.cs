﻿//-----------------------------------------------------------------------
// <author>Eduardo Michel</author>
// <summary>Generates the popup to add Liability Editors in the 1003 Editor.</summary>
//-----------------------------------------------------------------------

namespace PriceMyLoan.webapp
{
    using System;
    using System.Drawing;
    using global::DataAccess;
    using LendersOffice.Common;
    using PriceMyLoan.los.DataAccess;
    using System.Web.UI.WebControls;

    /// <summary>Generates the popup to add Liability Editors in the 1003 Editor.</summary>
    public partial class Loan1003AddLiability : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// The current application ID.
        /// </summary>
        private Guid appId = Guid.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether the web controls should be disabled.  Determined by whether there's a valid loan ID.
        /// </summary>
        /// <value>True of False.</value>
        protected bool Skip
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The object that caused the event.</param>
        /// <param name="e">Event arguments.</param>
        protected void PageInit(object sender, System.EventArgs e)
        {
            this.ComZip.SmartZipcode(this.ComCity, this.ComState);
            this.RegisterCSS("/webapp/jquery-ui.css");
            this.RegisterCSS("/webapp/main.css");
            this.RegisterService("Loan1003", "/webapp/Loan1003PopupService.aspx");
        }

        /// <summary>
        /// Loads the page data.
        /// </summary>
        /// <param name="sender">The object that caused the event.</param>
        /// <param name="e">Event arguments.</param>
        protected void PageLoad(object sender, System.EventArgs e)
        {
            string loanAppId = null;
            try
            {
                loanAppId = RequestHelper.GetSafeQueryString("appid");
                this.appId = new Guid(loanAppId);
            }
            catch (Exception exc)
            {
                if (loanAppId == null)
                {
                    loanAppId = "null";
                }

                Tools.LogError("PML Add Liability. Error trying to parse 'appid' guid from query string. Found value: '" + loanAppId + "'.");
                throw new CBaseException("Cannot add a liability without a valid application id.", exc);
            }

            if (!Page.IsPostBack)
            {
                Tools.Bind_DebtT(this.DebtT);
                this.Skip = this.LoanID == Guid.Empty;

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003AddLiability));
                dataLoan.InitLoad();
                CAppData dataApp = dataLoan.GetAppData(appId);

                IReCollection recoll = dataApp.aReCollection;
                ISubcollection subcoll = recoll.GetSubcollection(true, E_ReoGroupT.All);

                matchedReoRecordId.Items.Add(new ListItem("<-- Select a matched REO -->", Guid.Empty.ToString()));

                foreach (IRealEstateOwned reField in subcoll)
                {
                    string addr = string.Format(@"{0}, {1}, {2} {3}", reField.Addr, reField.City, reField.State, reField.Zip);
                    matchedReoRecordId.Items.Add(new ListItem(addr, reField.RecordId.ToString()));
                }

                var borrowerName = dataApp.aBFirstNm + " " + dataApp.aBLastNm;
                var coborrowerName = dataApp.aCFirstNm + " " + dataApp.aCLastNm;
                if (borrowerName.Equals(" "))
                    borrowerName = "Borrower";
                if (coborrowerName.Equals(" "))
                    coborrowerName = "Co-Borrower";
                owner1.Text = borrowerName;
                owner2.Text = coborrowerName;

                if (this.Skip)
                {
                    this.DisableWebControls();
                }
            }

            m_error.Visible = false;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        #endregion

        /// <summary>
        /// Saves the new Liability.
        /// </summary>
        /// <param name="sender">The object that caused the event.</param>
        /// <param name="e">Event arguments.</param>
        protected void BtnOK_Click(object sender, System.EventArgs e)
        {
            if (this.Skip)
            {
                PriceMyLoan.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] { "OK=true" });
                return;
            }

            try
            {
                int version = RequestHelper.GetInt("version");
                PmlAddLiabilityData newLiability = new PmlAddLiabilityData(LoanID, this.appId, PriceMyLoanUser);
                newLiability.Add(
                    version,
                    (E_DebtRegularT)int.Parse(DebtT.Items[DebtT.SelectedIndex].Value),
                    (E_LiaOwnerT)OwnerT.SelectedIndex,
                    Bal.Text,
                    Pmt.Text,
                    ComNm.Text,
                    ComAddr.Text,
                    ComCity.Text,
                    ComState.Value,
                    ComZip.Text,
                    AccNm.Text,
                    AccNum.Text,
                    UsedInRatio.Checked,
                    WillBePdOff.Checked,
                    Guid.Parse(matchedReoRecordId.SelectedValue));

                ClientScript.RegisterStartupScript(this.GetType(), "autoclose", "var autoClose = true;", true);
            }
            catch (VersionMismatchException exc)
            {
                m_error.Visible = true;
                m_error.Text = exc.UserMessage;
                m_btnOK.Enabled = false;
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                m_error.Visible = true;
                m_error.Text = exc.UserMessage;
                m_btnOK.Enabled = false;
            }
            catch (CBaseException)
            {
                m_error.Visible = true;
                m_error.Text = "The liability could not be added.";
                m_btnOK.Enabled = false;
            }
        }

        /// <summary>
        /// Disables the web controls if the loan ID is invalid.
        /// </summary>
        private void DisableWebControls()
        {
            m_btnOK.Enabled = false;
            OwnerT.Enabled = false;
            DebtT.Enabled = false;
            ComNm.Enabled = false;
            Bal.Enabled = false;
            Pmt.Enabled = false;
            WillBePdOff.Enabled = false;
            UsedInRatio.Enabled = false;
            ComAddr.Enabled = false;
            ComCity.Enabled = false;
            ComState.Enabled = false;
            ComZip.Enabled = false;
            AccNm.Enabled = false;
            AccNum.Enabled = false;

            ComCity.BackColor = Color.LightGray;
            ComAddr.BackColor = Color.LightGray;
            Pmt.BackColor = Color.LightGray;
            Bal.BackColor = Color.LightGray;
            ComNm.BackColor = Color.LightGray;
            ComZip.BackColor = Color.LightGray;
            AccNm.BackColor = Color.LightGray;
            AccNum.BackColor = Color.LightGray;
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += this.PageInit;
            this.Load += this.PageLoad;
        }
    }
}
