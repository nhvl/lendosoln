﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Loan1003pg4.ascx.cs" Inherits="PriceMyLoan.webapp.Loan1003pg4" %>
<div>
    <div class="box-section-border-red" id="box3-section10">
        <div class="padding-bottom-p16">
            Use this continuation sheet if you need more space to complete the Residential
                Loan Application.
        </div>
        <label>Mark B for Borrower or C for Co-Borrower.</label>
        <textarea class="textarea-app-info-pg4" cols="1" rows="1" id="a1003ContEditSheet" runat="server" name="a1003ContEditSheet"></textarea>
    </div>
</div>
<!--end section X-->
