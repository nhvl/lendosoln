﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalEmailMessage.aspx.cs" Inherits="PriceMyLoan.UI.Main.OrderAppraisalEmailMessage" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Send Email</title>
    <style type="text/css">
        #EmailContent {
            width: 100%;
        }
        #SubjectText
        {
            width: 100%;
        }
        #MessageText
        {
            width: 100%;
            height: 410px;
        }
        .infoLine
        {
            display:block;
            margin-top:10px;
        }
        td
        {
            text-align: left;
        }
        .messageRow
        {
            margin-bottom: 0px;
        }
        .w-78 {
            width: 78px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField id="VendorId" runat="server" />
    <asp:HiddenField id="AppraisalOrderNumber" runat="server" />
    <asp:HiddenField ID="ErrorMessage" runat="server" />
    <div lqb-popup>
        <div class="modal-header">
            <h4 class="modal-title"><ml:EncodedLiteral ID="PageName" runat="server"></ml:EncodedLiteral></h4>
        </div>
        <div class="modal-body">
            <table id="EmailContent">
                <tbody>
                    <tr id="ReceivedEmailFromRow" runat="server">
                        <td class="infoLabel w-78">From</td>
                        <td class="infoValue"><ml:EncodedLiteral ID="ReceivedFromField" runat="server"></ml:EncodedLiteral></td>
                        <td class="infoLabel">Date</td>
                        <td class="infoValue"><ml:EncodedLiteral ID="ReceivedDateField" runat="server"></ml:EncodedLiteral></td>
                    </tr>
                    <tr>
                        <td class="infoLabel text-grey text-right w-78">To</td>
                        <td class="infoValue">
                            <ml:EncodedLiteral ID="ReceivedToField" runat="server"></ml:EncodedLiteral>
                            <asp:DropDownList  ID="SendEmailTo" runat="server"></asp:DropDownList>
                        </td>
                        <td class="infoLabel" style="display:none;"></td>
                        <td class="infoValue" style="display:none;"></td>
                    </tr>
                    <tr>
                        <td class="infoLabel text-grey text-right w-78">Subject</td>
                        <td class="infoValue"><ml:EncodedLiteral ID="ReceivedSubjectField" runat="server"></ml:EncodedLiteral><asp:TextBox id="SubjectText" TextMode="SingleLine" runat="server"></asp:TextBox></td>
                        <td class="infoLabel" style="display:none;"></td>
                        <td class="infoValue" style="display:none;"></td>
                    </tr>
                    <tr class="messageRow">
                        <td valign="top" class="infoLabel text-grey text-right w-78">Message</td>
                        <td colspan="3"><asp:TextBox id="MessageText" TextMode="MultiLine" Wrap="true" runat="server"></asp:TextBox></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <div id="BtnContainer">
                <input type="button" class="btn btn-flat" id="ExitBtn" value="Cancel" onclick="goBack();" runat="server" />
                <input type="button" class="btn btn-flat" id="SendBtn" value="Send" runat="server" />
            </div>
        </div>
    </div>
    </form>
</body>
<script type="text/javascript">
    $(window).ready(function() {
        if ($.trim($('#ErrorMessage').val()) != '') {
            alert($('#ErrorMessage').val());
        }

        TPOStyleUnification.Components();
    });
    $(window).on(".load", function() {
        $('#SendBtn').prop('disabled', true);
    });

    $('#SubjectText').keyup(function() {
        if ($('#SubjectText').val() !== '' && $('#MessageText').val() !== '') {
            $('#SendBtn').prop('disabled', false);
        } else {
            $('#SendBtn').prop('disabled', true);
        }
    });
    $('#MessageText').keyup(function() {
        if ($('#SubjectText').val() !== '' && $('#MessageText').val() !== '') {
            $('#SendBtn').prop('disabled', false);
        } else {
            $('#SendBtn').prop('disabled', true);
        }
    });
    $('#SendBtn').click(function() {
        var args = {
            VendorId: $('#VendorId').val(),
            AppraisalOrderNumber: $('#AppraisalOrderNumber').val(),
            ToField: $('#SendEmailTo').val(),
            SubjectField: $('#SubjectText').val(),
            MessageField: $('#MessageText').val()
        }, result;
        result = gService.OrderAppraisal.call("SendEmail", args);
        var showGenericErrorMessage = true;
        if (result && result.value && result.value.ErrorMessage) {
            alert(result.value.ErrorMessage);
            showGenericErrorMessage = false;
        }
        if (result && result.value && result.value.Success && result.value.Success === "True") {
            goBack();
        }
        else if (showGenericErrorMessage) {
            alert("Sending message failed.");
        }
    });

    function goBack() {
        window.location = gVirtualRoot + '/webapp/OrderAppraisalEmailLog.aspx?loanid='+ encodeURIComponent($("#sLId").val()) +'&vendorID='+ encodeURIComponent($('#VendorId').val()) +'&orderNumber=' + encodeURIComponent($('#AppraisalOrderNumber').val());
    }
</script>
</html>
