﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonQmQuickPricer.ascx.cs" Inherits="PriceMyLoan.webapp.NonQmQuickPricer" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<table id="QuickPricerTable" class="content-detail">
    <tr>
        <td>
            <header class="page-header">Quick Price</header>
        </td>
    </tr>
    <tr>
        <td class="align-top">
            <div class="section">
                <header class="header">Loan</header>
                <div class="loan-info-padding-left">
                    <div class="table table-bottom">
                        <div>
                            <div>
                                <label>Loan Amount<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <ml:MoneyTextBox ID="sLAmtCalcPe" class="loan-amount" runat="server" onchange="invalidatePricing(); validateQpInput();"></ml:MoneyTextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>LTV %<span class="text-danger">*</span></label>                                   
                            </div>
                            <div>                                        
                                <ml:PercentTextBox ID="sLtvRPe" runat="server" onchange="invalidatePricing(); validateQpInput();"></ml:PercentTextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Loan Purpose</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sLPurposeT" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Occupancy</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sOccTPe" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Property Type</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sProdSpT" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Property State<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <ml:StateDropDownList ID="sSpStatePe" IncludeTerritories="false" runat="server" onchange="invalidatePricing(); validateQpInput();"></ml:StateDropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <header class="header">Borrower</header>
                <div class="loan-info-padding-left">
                    <div class="table table-bottom">
                        <div>
                            <div>
                                <label>Credit Score<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <asp:TextBox ID="sCreditScoreEstimatePe" runat="server" onchange="invalidatePricing(); validateQpInput();" MaxLength="3" CssClass="credit-score-field"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Housing History</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sManuallyEnteredHousingHistory" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Housing Events</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sManuallyEnteredHousingEvents" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Bankruptcy</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sManuallyEnteredBankruptcy" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Citizenship</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="aProdBCitizenT" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Income Doc Type</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sProdDocT" runat="server" onchange="invalidatePricing()"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
        <td class="align-top">
            <div id="PricingResultsContainer" ng-controller="pricingResultsController">
                <div id="ResultsContainer" class="mask-when-pricing"></div>
                <div id="NoResultsMsg">
                    We couldn't find any programs right now.
                    <br />
                    Please&nbsp;<a onclick="noResultSubmitScenarioClick()">submit this scenario</a>&nbsp;and we will get back to you directly!
                </div>
            </div>            
        </td>
    </tr>
    <tr>
        <td colspan="3" class="center">
            <button type="button" class="btn btn-default price-button" onclick="onPriceClick()">Price</button>
        </td>
    </tr>
</table>