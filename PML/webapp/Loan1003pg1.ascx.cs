﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace PriceMyLoan.webapp
{
    public partial class Loan1003pg1 : PriceMyLoan.UI.BaseUserControl
    {


        protected void PageInit()
        {
            Tools.Bind_sLT(sLT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sEstateHeldT(sEstateHeldT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sSpImprovTimeFrameT(sSpImprovTimeFrameT);
            Tools.Bind_aOccT(aOccT);
            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
            
            Tools.Bind_aMaritalStatT(aBMaritalStatT);
            Tools.Bind_aMaritalStatT(aCMaritalStatT);
            Tools.Bind_aAddrT(aBAddrT);
            Tools.Bind_aAddrT(aBPrev1AddrT);
            Tools.Bind_aAddrT(aBPrev2AddrT);
            Tools.Bind_aAddrT(aCAddrT);
            Tools.Bind_aAddrT(aCPrev1AddrT);
            Tools.Bind_aAddrT(aCPrev2AddrT);
            aBZip.SmartZipcode(aBCity, aBState);
            aBZipMail.SmartZipcode(aBCityMail, aBStateMail);
            aCZip.SmartZipcode(aCCity, aCState);
            aCZipMail.SmartZipcode(aCCityMail, aCStateMail);
            aBPrev1Zip.SmartZipcode(aBPrev1City, aBPrev1State);
            aBPrev2Zip.SmartZipcode(aBPrev2City, aBPrev2State);
            aCPrev1Zip.SmartZipcode(aCPrev1City, aCPrev1State);
            aCPrev2Zip.SmartZipcode(aCPrev2City, aCPrev2State);

            Tools.Bind_aAddrMailSourceT(aBAddrMailSourceT);
            Tools.Bind_aAddrMailSourceT(aCAddrMailSourceT);

            sDwnPmtSrc.Items.Add("Checking/Savings");
            sDwnPmtSrc.Items.Add("Gift Funds");
            sDwnPmtSrc.Items.Add("Stocks & Bonds");
            sDwnPmtSrc.Items.Add("Lot Equity");
            sDwnPmtSrc.Items.Add("Bridge Loan");
            sDwnPmtSrc.Items.Add("Trust Funds");
            sDwnPmtSrc.Items.Add("Retirement Funds");
            sDwnPmtSrc.Items.Add("Life Insurance Cash Value");
            sDwnPmtSrc.Items.Add("Sale of Chattel");
            sDwnPmtSrc.Items.Add("Trade Equity");
            sDwnPmtSrc.Items.Add("Sweat Equity");
            sDwnPmtSrc.Items.Add("Cash on Hand");
            sDwnPmtSrc.Items.Add("Deposit on Sales Contract");
            sDwnPmtSrc.Items.Add("Equity from Pending Sale");
            sDwnPmtSrc.Items.Add("Equity from Subject Property");
            sDwnPmtSrc.Items.Add("Equity on Sold Property");
            sDwnPmtSrc.Items.Add("Other Type of Down Payment");
            sDwnPmtSrc.Items.Add("Rent with Option to Purchase");
            sDwnPmtSrc.Items.Add("Secured Borrowed Funds");
            sDwnPmtSrc.Items.Add("Unsecured Borrowed Funds");
            sDwnPmtSrc.Items.Add("Loan Proceeds"); // 2/27/2004 dd - Request by customer kbagley 
            sDwnPmtSrc.Items.Add("FHA - Gift - Source N/A");
            sDwnPmtSrc.Items.Add("FHA - Gift - Source Relative");
            sDwnPmtSrc.Items.Add("FHA - Gift - Source Government Assistance");
            sDwnPmtSrc.Items.Add("FHA - Gift - Source Employer");
            sDwnPmtSrc.Items.Add("FHA - Gift - Source Nonprofit/Religious/Community - Seller Funded");
            sDwnPmtSrc.Items.Add("FHA - Gift - Source Nonprofit/Religious/Community - Non-Seller Funded");
            sDwnPmtSrc.Items.Add("Forgivable Secured Loan");

            Tools.BindComboBox_aManner(aManner);

            sRefPurpose.Items.Add("No Cash-Out Rate/Term");
            sRefPurpose.Items.Add("Limited Cash-Out Rate/Term");
            sRefPurpose.Items.Add("Cash-Out/Home Improvement");
            sRefPurpose.Items.Add("Cash-Out/Debt Consolidation");
            sRefPurpose.Items.Add("Cash-Out/Other");


            // 01-14-08 av 18913 
            sSpState.Attributes.Add("onchange", "UpdateCounties(this,$('#" + sSpCounty.ClientID + "')[0]);");

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
        }

        public void LoadData()
        {
            PageInit();

            BrokerDB CurrentBroker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg1));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(RequestHelper.GetGuid("applicationid", Guid.Empty));

            sLenderCaseNum.Disabled = !dataLoan.sLenderCaseNumLckd;

            sMultiApps.Checked = dataLoan.sMultiApps;
            aSpouseIExcl.Checked = dataApp.aSpouseIExcl;

            sLTODesc.Value = dataLoan.sLTODesc;
            sAgencyCaseNum.Value = dataLoan.sAgencyCaseNum;
            sLenderCaseNum.Value = dataLoan.sLenderCaseNum;

            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sTerm.Value = dataLoan.sTerm_rep;
            sDownPmtPc.Text = dataLoan.sDownPmtPc_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sDue.Value = dataLoan.sDue_rep;
            sEquity.Text = dataLoan.sEquityCalc_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sProThisMPmt.Value = dataLoan.sProThisMPmt_rep;
            sLAmt.Text = dataLoan.sLAmtCalc_rep;
            sLAmtLckd.Checked = dataLoan.sLAmtLckd;
            sQualIR.Text = dataLoan.sQualIR_rep;
            sQualIRLckd.Checked = dataLoan.sQualIRLckd;
            sFinMethDesc.Value = dataLoan.sFinMethDesc;
            sFinMethT.Enabled = !(dataLoan.sIsRateLocked || IsReadOnly);
            sFinMethPrintAsOtherDesc.Value = dataLoan.sFinMethPrintAsOtherDesc;
            sFinMethodPrintAsOther.Checked = dataLoan.sFinMethodPrintAsOther;

            sSpAddr.Value = dataLoan.sSpAddr;
            sSpCity.Value = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
            sUnitsNum.Value = dataLoan.sUnitsNum_rep;
            sYrBuilt.Value = dataLoan.sYrBuilt;
            sSpLegalDesc.Value = dataLoan.sSpLegalDesc;

            sOLPurposeDesc.Value = dataLoan.sOLPurposeDesc;
            sOccRLckd.Checked = dataLoan.sOccRLckd;
            sOccR.Text = dataLoan.sOccR_rep;
            sSpGrossRent.Text = dataLoan.sSpGrossRent_rep;

            sLotAcqYr.Value = dataLoan.sLotAcqYr;
            sLotOrigC.Text = dataLoan.sLotOrigC_rep;
            sLotLien.Text = dataLoan.sLotLien_rep;
            sLotVal.Text = dataLoan.sLotVal_rep;
            sLotImprovC.Text = dataLoan.sLotImprovC_rep;
            sLotWImprovTot.Text = dataLoan.sLotWImprovTot_rep;

            sSpAcqYr.Value = dataLoan.sSpAcqYr;
            sSpOrigC.Text = dataLoan.sSpOrigC_rep;
            sSpLien.Text = dataLoan.sSpLien_rep;
            sRefPurpose.Text = dataLoan.sRefPurpose;
            sSpImprovDesc.Value = dataLoan.sSpImprovDesc;
            sSpImprovC.Value = dataLoan.sSpImprovC_rep;

            aTitleNm1.Value = dataApp.aTitleNm1;
            aTitleNm1Lckd.Checked = dataApp.aTitleNm1Lckd;
            aTitleNm2.Value = dataApp.aTitleNm2;
            aTitleNm2Lckd.Checked = dataApp.aTitleNm2Lckd;
            aManner.Text = dataApp.aManner;
            sLeaseHoldExpireD.Text = dataLoan.sLeaseHoldExpireD_rep;
            sDwnPmtSrc.Text = dataLoan.sDwnPmtSrc;
            sDwnPmtSrcExplain.Value = dataLoan.sDwnPmtSrcExplain;

            aBFirstNm.Value = dataApp.aBFirstNm;
            aBMidNm.Value = dataApp.aBMidNm;
            aBLastNm.Value = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSsn.Value = dataApp.aBSsn;
            aBHPhone.Value = dataApp.aBHPhone;
            aBBusPhone.Value = dataApp.aBBusPhone;
            aBCellphone.Value = dataApp.aBCellPhone;
            aBDob.Text = dataApp.aBDob_rep;
            aBAge.Value = dataApp.aBAge_rep;
            aBSchoolYrs.Value = dataApp.aBSchoolYrs_rep;
            aBMaritalStatT.SelectedValue = "" + (int)dataApp.aBMaritalStatT;
            aBDependNum.Value = dataApp.aBDependNum_rep;
            aBDependAges.Value = dataApp.aBDependAges;
            aBEmail.Value = dataApp.aBEmail;
            aBAddr.Value = dataApp.aBAddr;
            aBCity.Value = dataApp.aBCity;
            aBState.Value = dataApp.aBState;
            aBZip.Text = dataApp.aBZip;
            aBAddrT.SelectedValue = "" + (int)dataApp.aBAddrT;
            aBAddrYrs.Value = dataApp.aBAddrYrs;
            aBAddrMail.Value = dataApp.aBAddrMail;
            aBCityMail.Value = dataApp.aBCityMail;
            aBZipMail.Text = dataApp.aBZipMail;
            aBPrev1Addr.Value = dataApp.aBPrev1Addr;
            aBPrev1City.Value = dataApp.aBPrev1City;
            aBPrev1State.Value = dataApp.aBPrev1State;
            aBPrev1Zip.Text = dataApp.aBPrev1Zip;
            aBPrev1AddrT.SelectedValue = "" + (int)dataApp.aBPrev1AddrT;
            aBPrev1AddrYrs.Value = dataApp.aBPrev1AddrYrs;
            aBPrev2Addr.Value = dataApp.aBPrev2Addr;
            aBPrev2City.Value = dataApp.aBPrev2City;
            aBPrev2State.Value = dataApp.aBPrev2State;
            aBPrev2Zip.Text = dataApp.aBPrev2Zip;
            aBPrev2AddrT.SelectedValue = "" + (int)dataApp.aBPrev2AddrT;
            aBPrev2AddrYrs.Value = dataApp.aBPrev2AddrYrs;

            aCFirstNm.Value = dataApp.aCFirstNm;
            aCMidNm.Value = dataApp.aCMidNm;
            aCLastNm.Value = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSsn.Value = dataApp.aCSsn;
            aCHPhone.Value = dataApp.aCHPhone;
            aCBusPhone.Value = dataApp.aCBusPhone;
            aCCellphone.Value = dataApp.aCCellPhone;
            aCDob.Text = dataApp.aCDob_rep;
            aCAge.Value = dataApp.aCAge_rep;
            aCSchoolYrs.Value = dataApp.aCSchoolYrs_rep;
            aCMaritalStatT.SelectedValue = "" + (int)dataApp.aCMaritalStatT;
            aCDependNum.Value = dataApp.aCDependNum_rep;
            aCDependAges.Value = dataApp.aCDependAges;
            aCEmail.Value = dataApp.aCEmail;
            aCAddr.Value = dataApp.aCAddr;
            aCCity.Value = dataApp.aCCity;
            aCState.Value = dataApp.aCState;
            aCZip.Text = dataApp.aCZip;
            aCAddrT.SelectedValue = "" + (int)dataApp.aCAddrT;
            aCAddrYrs.Value = dataApp.aCAddrYrs;
            aCAddrMail.Value = dataApp.aCAddrMail;
            aCCityMail.Value = dataApp.aCCityMail;
            aCZipMail.Text = dataApp.aCZipMail;
            //aCAddrMailUsePresentAddr.Checked = dataApp.aCAddrMailUsePresentAddr;
            aCPrev1Addr.Value = dataApp.aCPrev1Addr;
            aCPrev1City.Value = dataApp.aCPrev1City;
            aCPrev1State.Value = dataApp.aCPrev1State;
            aCPrev1Zip.Text = dataApp.aCPrev1Zip;
            aCPrev1AddrT.SelectedValue = "" + (int)dataApp.aCPrev1AddrT;
            aCPrev1AddrYrs.Value = dataApp.aCPrev1AddrYrs;
            aCPrev2Addr.Value = dataApp.aCPrev2Addr;
            aCPrev2City.Value = dataApp.aCPrev2City;
            aCPrev2State.Value = dataApp.aCPrev2State;
            aCPrev2Zip.Text = dataApp.aCPrev2Zip;
            aCPrev2AddrT.SelectedValue = "" + (int)dataApp.aCPrev2AddrT;
            aCPrev2AddrYrs.Value = dataApp.aCPrev2AddrYrs;





            Tools.Bind_sSpCounty(dataLoan.sSpState, sSpCounty, true);
            Tools.SetDropDownListCaseInsensitive(sSpCounty, dataLoan.sSpCounty);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sEstateHeldT, dataLoan.sEstateHeldT);
            Tools.SetDropDownListValue(sSpImprovTimeFrameT, dataLoan.sSpImprovTimeFrameT);
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);


            Tools.SetDropDownListValue(aBMaritalStatT, dataApp.aBMaritalStatT);
            Tools.SetDropDownListValue(aBAddrT, dataApp.aBAddrT);
            Tools.SetDropDownListValue(aBPrev1AddrT, dataApp.aBPrev1AddrT);
            Tools.SetDropDownListValue(aBPrev2AddrT, dataApp.aBPrev2AddrT);
            Tools.SetDropDownListValue(aCMaritalStatT, dataApp.aCMaritalStatT);
            Tools.SetDropDownListValue(aCAddrT, dataApp.aCAddrT);

            Tools.SetDropDownListValue(aBAddrMailSourceT, dataApp.aBAddrMailSourceT);
            Tools.SetDropDownListValue(aCAddrMailSourceT, dataApp.aCAddrMailSourceT);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            


        }
    }
}