﻿namespace PriceMyLoan.webapp
{
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Loan;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class QualRateCalculationPopupServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "RefreshData":
                    SavePage(refreshOnly: true);
                    break;
                case "SavePage":
                    SavePage(refreshOnly: false);
                    break;
            }
        }

        private void SavePage(bool refreshOnly)
        {
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = null;

            BindData(dataLoan, dataApp);

            if (!refreshOnly)
            {
                try
                {
                    dataLoan.Save();
                }
                catch (PageDataAccessDenied exc)
                {
                    SetResult("ErrorMessage", exc.UserMessage);
                }
                catch (LoanFieldWritePermissionDenied exc)
                {
                    SetResult("ErrorMessage", exc.UserMessage);
                }
            }            

            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, dataApp);
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(QualRateCalculationPopupServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sUseQualRate = GetBool(nameof(dataLoan.sUseQualRate));
            dataLoan.sQualIR_rep = GetString(nameof(dataLoan.sQualIR));

            dataLoan.sQualRateCalculationFieldT1 = GetEnum<QualRateCalculationFieldT>(nameof(dataLoan.sQualRateCalculationFieldT1));
            dataLoan.sQualRateCalculationAdjustment1_rep = GetString(nameof(dataLoan.sQualRateCalculationAdjustment1));

            dataLoan.sQualRateCalculationT = GetEnum<QualRateCalculationT>(nameof(dataLoan.sQualRateCalculationT));
            if (dataLoan.sQualRateCalculationT == QualRateCalculationT.MaxOf)
            {
                var calculationField2 = GetEnum<QualRateCalculationFieldT>(nameof(dataLoan.sQualRateCalculationFieldT2));
                if (dataLoan.sUseQualRate && dataLoan.sQualRateCalculationFieldT1 == calculationField2)
                {
                    throw new CBaseException(ErrorMessages.QualRateFieldsMustBeMutuallyExclusive, ErrorMessages.QualRateFieldsMustBeMutuallyExclusive);
                }

                dataLoan.sQualRateCalculationFieldT2 = calculationField2;
                dataLoan.sQualRateCalculationAdjustment2_rep = GetString(nameof(dataLoan.sQualRateCalculationAdjustment2));
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("HasQualRatePopup", true);
            SetResult(nameof(dataLoan.sQualIR), dataLoan.sQualIR_rep);
            SetResult(nameof(dataLoan.sUseQualRate), dataLoan.sUseQualRate);
            SetResult(nameof(dataLoan.sQualRateCalculationT), dataLoan.sQualRateCalculationT);
            SetResult(nameof(dataLoan.sQualRateCalculationFieldT1), dataLoan.sQualRateCalculationFieldT1);
            SetResult(nameof(dataLoan.sQualRateCalculationAdjustment1), dataLoan.sQualRateCalculationAdjustment1_rep);
            SetResult(nameof(dataLoan.sQualRateCalculationFieldT2), dataLoan.sQualRateCalculationFieldT2);
            SetResult(nameof(dataLoan.sQualRateCalculationAdjustment2), dataLoan.sQualRateCalculationAdjustment2_rep);

            var calculationOptions = new QualRateCalculationOptions()
            {
                NoteRate = dataLoan.sNoteIR,
                IndexRate = dataLoan.sRAdjIndexR,
                MarginRate = dataLoan.sRAdjMarginR
            };

            var firstCalculationResult = Tools.GetQualRateCalculationValue(calculationOptions, dataLoan.sQualRateCalculationFieldT1, dataLoan.sQualRateCalculationAdjustment1);
            SetResult("QualRateCalculationResult1", dataLoan.m_convertLos.ToRateString(firstCalculationResult));

            var secondCalculationResult = Tools.GetQualRateCalculationValue(calculationOptions, dataLoan.sQualRateCalculationFieldT2, dataLoan.sQualRateCalculationAdjustment2);
            SetResult("QualRateCalculationResult2", dataLoan.m_convertLos.ToRateString(secondCalculationResult));
        }
    }

    public partial class QualRateCalculationPopupService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {

        protected override void Initialize()
        {
            AddBackgroundItem("QualRateCalculationPopup", new QualRateCalculationPopupServiceItem());
        }
    }
}