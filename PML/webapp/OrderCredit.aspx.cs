﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using DataAccess;
using PriceMyLoan;
using PriceMyLoan.UI;
using PriceMyLoan.DataAccess;
using PriceMyLoan.Common;
using CommonLib;
using LendersOffice.Security;
using LendersOffice.CreditReport.UniversalCreditSystems;
using LendersOffice.Constants;
using DataAccess.LoanValidation;
using LendersOffice.Admin;
using LendersOffice.Audit;
using PriceMyLoan.main;
using System.Web.UI;
using LendersOffice.ObjLib.ServiceCredential;
using LendersOffice.AntiXss;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.webapp
{
    public partial class OrderCredit : PriceMyLoan.UI.BasePage
    {
        private const string MASK_INSTANT_VIEW_PW = "*********";
        private const string MCL_CREDIT_SERVER = "https://credit.meridianlink.com";

        private string m_reportID = "";
        private bool m_hasCreditReport = false;
        //protected bool m_displayWarning = false;

        private Hashtable m_craHash = new Hashtable();

        #region Protected Member Variables
        #endregion

        protected bool m_hasBrokerDUInfo = false;
        private string m_brokerDUUserID = "";
        private string m_brokerDUPassword = "";
        
        private BrokerDB m_brokerDB;
        protected BrokerDB CurrentBroker
        {
            get
            {
                if (null == m_brokerDB)
                {
                    m_brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
                }
                return m_brokerDB;
            }
        }

        public int CreditAction
        {
            get
            {
                //  1 - Reissue
                //  2 - Order New
                //  3 - Manual Credit
                //  4 - Upgrade
                // -1 - Use Credit On File
                return RequestHelper.GetInt("CreditAction");
            }
        }

        public Guid CreditProtocol
        {
            get
            {
                return RequestHelper.GetGuid("CreditProtocol", Guid.Empty);
            }
        }

        public int AppNum
        {
            get
            {
                return RequestHelper.GetInt("AppNum");
            }

        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

        protected bool m_hasLiabilities = false;
        protected bool m_allowOrderNew = true;
        protected bool m_displayCreditOptions = true;
        protected bool m_isStoredCredentials = false;

        protected void PageInit(object sender, System.EventArgs e)
        {
            ClientScript.GetPostBackEventReference(this, "");
            this.RegisterService("main", "/main/mainservice.aspx");

            SetRequiredValidatorMessage(LoginNameValidator);
            SetRequiredValidatorMessage(PasswordValidator);
            SetRequiredValidatorMessage(InstantViewIDValidator);
            SetRequiredValidatorMessage(ReportIDValidator);
            SetRequiredValidatorMessage(aBAddrValidator);
            SetRequiredValidatorMessage(aBCityValidator);
            SetRequiredValidatorMessage(aBZipValidator);
            SetRequiredValidatorMessage(aBStateValidator);
            SetRequiredValidatorMessage(FD_ClientCodeValidator);
            SetRequiredValidatorMessage(FD_OfficeCodeValidator);
            SetRequiredValidatorMessage(DUUserIDValidator);
            SetRequiredValidatorMessage(DUPasswordValidator);
            SetRequiredValidatorMessage(AccountIdentifierValidator);
            SetRequiredValidatorMessage(sProdCrManualBk13RecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManualBk7RecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManualForeclosureRecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManual30MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualRolling60MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualRolling90MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualBk13RecentFileValidator);
            SetRequiredValidatorMessage(sProdCrManualBk7RecentFileValidator);
            SetRequiredValidatorMessage(sProdCrManualForeclosureRecentFileValidator);
            Tools.BindLateDatesDropDown(sProdCrManual30MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual60MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual90MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual120MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual150MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualNonRolling30MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualRolling60MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualRolling90MortLateCount);

            Tools.BindMonthDropDown(sProdCrManualBk13RecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualBk13RecentSatisfiedMon);
            Tools.BindMonthDropDown(sProdCrManualBk7RecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualBk7RecentSatisfiedMon);
            Tools.BindMonthDropDown(sProdCrManualForeclosureRecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualForeclosureRecentSatisfiedMon);

            Tools.BindYearDropDown(sProdCrManualBk13RecentFileYr);
            Tools.BindYearDropDown(sProdCrManualBk13RecentSatisfiedYr);
            Tools.BindYearDropDown(sProdCrManualBk7RecentFileYr);
            Tools.BindYearDropDown(sProdCrManualBk7RecentSatisfiedYr);
            Tools.BindYearDropDown(sProdCrManualForeclosureRecentFileYr);
            Tools.BindYearDropDown(sProdCrManualForeclosureRecentSatisfiedYr);

            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk13RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk7RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualForeclosureRecentStatusT);

            aBZip.SmartZipcode(aBCity, aBState);

            List<Guid> denyCraList = MasterCRAList.RetrieveHiddenCras(this.CurrentBroker, E_ApplicationT.PriceMyLoan);
            // 9/21/2004 dd - Append special credit.meridianlink.com CRA
            m_craHash.Add("MCL_CREDIT_SERVER", new TemporaryCRAInfo("MCL", MCL_CREDIT_SERVER, Guid.Empty, CreditReportProtocol.Mcl, ""));

            m_nonMclProtocolList = new List<KeyValuePair<Guid, int>>();
            foreach (var proxy in CreditReportAccountProxy.Helper.ListCreditProxyByBrokerID(this.PriceMyLoanUser.BrokerId))
            {
                if (denyCraList.Contains(proxy.CrAccProxyId))
                {
                    continue;
                }

                Guid craId = proxy.CraId;
                string craUserName = proxy.CraUserName;
                string craPassword = proxy.CraPassword;
                string craAccId = proxy.CraAccId;
                string vendorName = proxy.CrAccProxyDesc;

                // Add special code for CRA Mapping
                m_nonMclProtocolList.Add(new KeyValuePair<Guid, int>(proxy.CrAccProxyId, (int)CreditReportProtocol.LenderMappingCRA));

                TemporaryCRAInfo tempCraInfo = new TemporaryCRAInfo(vendorName, "", craId, CreditReportProtocol.LenderMappingCRA, ""); //lender mappings have their provider id set by ordercredit
                tempCraInfo.UserName = craUserName;
                tempCraInfo.Password = craPassword;
                tempCraInfo.AccountID = craAccId;
                tempCraInfo.IsEquifaxPulled = proxy.IsEquifaxPulled;
                tempCraInfo.IsExperianPulled = proxy.IsExperianPulled;
                tempCraInfo.IsTransUnionPulled = proxy.IsTransUnionPulled;
                tempCraInfo.IsFannieMaePulled = proxy.IsFannieMaePulled;
                tempCraInfo.CreditMornetPlusUserId = proxy.CreditMornetPlusUserId;
                tempCraInfo.CreditMornetPlusPassword = proxy.CreditMornetPlusPassword;
                m_craHash.Add(proxy.CrAccProxyId.ToString(), tempCraInfo);
            }

            var masterCraList = LendersOffice.CreditReport.MasterCRAList.RetrieveAvailableCras(this.CurrentBroker);
            foreach (LendersOffice.CreditReport.CRA o in masterCraList)
            {
                if (denyCraList.Contains(o.ID))
                    continue;

                m_craHash.Add(o.ID.ToString(),
                    new TemporaryCRAInfo(o.VendorName, o.Url, o.ID, o.Protocol, o.ProviderId));

                if (o.Protocol != CreditReportProtocol.Mcl)
                {
                    // 9/20/2004 dd - Add non-mcl protocol to javascript hash.
                    m_nonMclProtocolList.Add(new KeyValuePair<Guid, int>(o.ID, (int)o.Protocol));
                }
            }
        }

        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected List<KeyValuePair<Guid, int>> m_nonMclProtocolList = new List<KeyValuePair<Guid, int>>();
        private bool m_canRunLpeWithoutCreditReport
        {
            get { return PriceMyLoanUser.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport); }
        }

        private void DetermineBrokerDULoginInfo(BrokerDB broker)
        {
            m_brokerDUUserID = broker.CreditMornetPlusUserID;
            m_brokerDUPassword = broker.CreditMornetPlusPassword.Value;
            m_hasBrokerDUInfo = m_brokerDUUserID != "" && m_brokerDUPassword != "";
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (Page.IsPostBack)
            {
                try
                {
                    SaveData();
                    ClientScript.RegisterStartupScript(typeof(Page), "closePage", "f_return()", true);
                }
                catch (LoanFieldWritePermissionDenied ex)
                {
                    ClientScript.RegisterStartupScript(typeof(Page), "closePage", "f_return('" + ex.UserMessage + "');", true);
                }
                catch (NonCriticalException) { }
            }
            else
            {
                LoadData();
            }
        }

        protected bool HasCreditReport
        {
            get { return m_hasCreditReport; }
        }

        private void RetrieveCreditReport(Guid applicationID)
        {

            SqlParameter[] parameters = { new SqlParameter("@ApplicationID", applicationID) };

            string lastCra = null;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.PriceMyLoanUser.BrokerId, "RetrieveCreditReport", parameters))
            {
                if (reader.Read())
                {
                    m_reportID = (string)reader["ExternalFileID"];

                    if (reader["CrAccProxyId"] is DBNull)
                        lastCra = reader["ComId"].ToString();
                    else
                    {
                        m_reportID = ""; // OPM 2147 - Hide credit reference number for credit pull from proxy account.
                        lastCra = reader["CrAccProxyId"].ToString();
                    }

                    // 10/24/2005 dd - if LastCra == POINT Dummy then set lastCra = null
                    if (new Guid(lastCra) == ConstAppDavid.DummyPointServiceCompany)
                        lastCra = null;

                }
            }
        }
        public void LoadData()
        {
            DetermineBrokerDULoginInfo(this.CurrentBroker);
            m_allowOrderNew = this.CurrentBroker.AllowPmlUserOrderNewCreditReport; // 2/16/07 OPM 9780.

            CPageData dataLoan = new CEnterCreditData(LoanID);
            dataLoan.InitLoad();
            // 11/18/2005 dd - Display data change warning if user visit this step before.
            // 12/7/2006 dd - Remove the warning only in step1.
            //m_displayWarning = dataLoan.sPml1stVisitCreditStepD_rep != "" && !Page.IsPostBack;

            if (dataLoan.sPml1stVisitCreditStepD_rep == "")
            {
                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitCreditStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();
                dataLoan = new CEnterCreditData(LoanID);
                dataLoan.InitLoad();
            }
            sFileVersion = dataLoan.sFileVersion;
            CAppData dataApp = dataLoan.GetAppData(AppNum);
            m_hasLiabilities = dataApp.aLiaCollection.CountRegular != 0;


            m_hasCreditReport = dataApp.aIsCreditReportOnFile;

            RetrieveCreditReport(dataApp.aAppId);
            ReportID.Text = m_reportID;

            aBExperianScorePe.Text = dataApp.aBExperianScorePe_rep;
            aBTransUnionScorePe.Text = dataApp.aBTransUnionScorePe_rep;
            aBEquifaxScorePe.Text = dataApp.aBEquifaxScorePe_rep;
            aBAddr.Text = dataApp.aBAddr;
            aBCity.Text = dataApp.aBCity;
            aBState.Value = dataApp.aBState;
            aBZip.Text = dataApp.aBZip;
            aBHasSpouse.Checked = dataApp.aBHasSpouse; //opm 4382 fs 07/23/08
            aBDob.Text = dataApp.aBDob_rep;

            aCExperianScorePe.Text = dataApp.aCExperianScorePe_rep;
            aCTransUnionScorePe.Text = dataApp.aCTransUnionScorePe_rep;
            aCEquifaxScorePe.Text = dataApp.aCEquifaxScorePe_rep;
            sProdCrManualBk13Has.Checked = dataLoan.sProdCrManualBk13Has;
            sProdCrManualBk7Has.Checked = dataLoan.sProdCrManualBk7Has;
            sProdCrManualForeclosureHas.Checked = dataLoan.sProdCrManualForeclosureHas;
            aCDob.Text = dataApp.aCDob_rep;

            Tools.SetDropDownListValue(sProdCrManual120MortLateCount, dataLoan.sProdCrManual120MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual150MortLateCount, dataLoan.sProdCrManual150MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual30MortLateCount, dataLoan.sProdCrManual30MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual60MortLateCount, dataLoan.sProdCrManual60MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual90MortLateCount, dataLoan.sProdCrManual90MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualNonRolling30MortLateCount, dataLoan.sProdCrManualNonRolling30MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualRolling60MortLateCount, dataLoan.sProdCrManualRolling60MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualRolling90MortLateCount, dataLoan.sProdCrManualRolling90MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileMon, dataLoan.sProdCrManualBk13RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileYr, dataLoan.sProdCrManualBk13RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedMon, dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedYr, dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileMon, dataLoan.sProdCrManualBk7RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileYr, dataLoan.sProdCrManualBk7RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedMon, dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedYr, dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileMon, dataLoan.sProdCrManualForeclosureRecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileYr, dataLoan.sProdCrManualForeclosureRecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedMon, dataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedYr, dataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep);

            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentStatusT, dataLoan.sProdCrManualForeclosureRecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentStatusT, dataLoan.sProdCrManualBk13RecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentStatusT, dataLoan.sProdCrManualBk7RecentStatusT);

            if (PriceMyLoanUser.Type == "B")
            {
                Page.ClientScript.RegisterHiddenField("DeleteLiabilities", "0");
            }
            else if (PriceMyLoanUser.Type == "P")
            {
                // 10/21/2005 dd - For "P" user Type always delete any existing liabilities.
                Page.ClientScript.RegisterHiddenField("DeleteLiabilities", "1");
            }

            // 12/27/2006 nw - OPM 5083 - Remember login name
            if (PriceMyLoanUser.LastUsedCreditLoginNm != "")
            {
                LoginName.Text = PriceMyLoanUser.LastUsedCreditLoginNm;
                RememberLoginNameCB.Checked = true;
            }

            // OPM 450477.
            var thisUserType = PriceMyLoanUser.Type.ToUpper() == "P" ? UserType.TPO : UserType.LQB;
            var savedCredentials = ServiceCredential.ListAvailableServiceCredentials(PriceMyLoanUser, dataLoan.sBranchId, ServiceCredentialService.CreditReports)
                .FirstOrDefault(credential => credential.ServiceProviderId.HasValue && credential.ServiceProviderId == CreditProtocol);

            m_isStoredCredentials = savedCredentials != null;

        }

        /// <summary>
        /// UI function to save the info provided by the user as a manual credit report. 
        /// </summary>
        private void SaveManualCreditReport()
        {

            CPageData dataLoan = new CManualCreditData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(AppNum);

            // 5/12/2006 dd - Need to delete the credit report before saving manual credit data. This way
            // the cache value will pick up from manual value correctly.
            // Delete Credit report.
            CreditReportServer.DeleteCreditReport(dataLoan.sBrokerId, dataApp.aAppId, LoanID, CreditReportDeletedAuditItem.E_CreditReportDeletionT.Manual, false);


            dataApp.aBExperianScorePe_rep = aBExperianScorePe.Text;
            dataApp.aBTransUnionScorePe_rep = aBTransUnionScorePe.Text;
            dataApp.aBEquifaxScorePe_rep = aBEquifaxScorePe.Text;
            dataApp.aCExperianScorePe_rep = aCExperianScorePe.Text;
            dataApp.aCTransUnionScorePe_rep = aCTransUnionScorePe.Text;
            dataApp.aCEquifaxScorePe_rep = aCEquifaxScorePe.Text;
            
            dataLoan.sProdCrManual120MortLateCount_rep = sProdCrManual120MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual150MortLateCount_rep = sProdCrManual150MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual30MortLateCount_rep = sProdCrManual30MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual60MortLateCount_rep = sProdCrManual60MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual90MortLateCount_rep = sProdCrManual90MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualNonRolling30MortLateCount_rep = sProdCrManualNonRolling30MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualRolling60MortLateCount_rep = sProdCrManualRolling60MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualRolling90MortLateCount_rep = sProdCrManualRolling90MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualBk13Has = sProdCrManualBk13Has.Checked;
            dataLoan.sProdCrManualBk13RecentFileMon_rep = sProdCrManualBk13RecentFileMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = sProdCrManualBk13RecentFileYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = sProdCrManualBk13RecentSatisfiedMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = sProdCrManualBk13RecentSatisfiedYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualBk13RecentStatusT);
            dataLoan.sProdCrManualBk7Has = sProdCrManualBk7Has.Checked;
            dataLoan.sProdCrManualBk7RecentFileMon_rep = sProdCrManualBk7RecentFileMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = sProdCrManualBk7RecentFileYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = sProdCrManualBk7RecentSatisfiedMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = sProdCrManualBk7RecentSatisfiedYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualBk7RecentStatusT);
            dataLoan.sProdCrManualForeclosureHas = sProdCrManualForeclosureHas.Checked;
            dataLoan.sProdCrManualForeclosureRecentFileMon_rep = sProdCrManualForeclosureRecentFileMon.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentFileYr_rep = sProdCrManualForeclosureRecentFileYr.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep = sProdCrManualForeclosureRecentSatisfiedMon.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep = sProdCrManualForeclosureRecentSatisfiedYr.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualForeclosureRecentStatusT);

            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;

        }
        /// <summary>
        /// Pulls the credit and save. 
        /// </summary>
        /// <param name="isReissue"></param>
        /// <param name="isOrderNew"></param>
        /// <param name="isUpgrade"></param>
        private void PullCreditAndSave(bool isReissue, bool isOrderNew, bool isUpgrade)
        {
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(AppNum);
            dataApp.aBAddr = aBAddr.Text;
            dataApp.aBCity = aBCity.Text;
            dataApp.aBState = aBState.Value;
            dataApp.aBZip = aBZip.Text;
            if (isOrderNew || isUpgrade)
            {
                dataApp.aBDob_rep = aBDob.Text;
                dataApp.aCDob_rep = aCDob.Text;
            }
            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;

            string protocolID = CreditProtocol.ToString();

            TemporaryCRAInfo craInfo = (TemporaryCRAInfo)m_craHash[protocolID];
            if (craInfo == null)
            {
                return;// 9/20/2004 dd - ERROR. This CRA info is missing. -- Moved By antonio was in empty else statement 
            }
            Guid craID = new Guid(protocolID);

            // 12/27/2006 nw - OPM 5083 - Remember login name
            CreditReportUtilities.UpdateLastUsedCra(PriceMyLoanUser.BrokerId, PriceMyLoanUser.LastUsedCreditLoginNm, RememberLoginNameCB.Checked ? LoginName.Text : "", null, null, PriceMyLoanUser.LastUsedCreditProtocolID, craID, PriceMyLoanUser.UserId);

            CreditRequestData requestData = CreateRequestData(LoanID, dataApp.aAppId, craID, dataLoan.sBranchId);
            requestData.ReferenceNumber = dataLoan.sLNm;

            var pollingIntervalInSeconds = 10;
            var maxRetryCount = 20;
            CreditReportRequestHandler requestHandler = new CreditReportRequestHandler(requestData, principal: PriceMyLoanUser, shouldReissueOnResponseNotReady: true, retryIntervalInSeconds: pollingIntervalInSeconds, maxRetryCount: 20, loanId: this.LoanID);
            if (ConstStage.DisableCreditOrderingViaBJP)
            {
                // TODO CREDIT_BJP: Temporary code. Remove once we have confirmed credit is working via the BJP.
                this.SubmitCreditSynchronously(requestHandler, maxRetryCount, isReissue, isUpgrade, dataApp, requestData);
                return;
            }

            var initialResult = requestHandler.SubmitToProcessor();
            if (initialResult.Status == CreditReportRequestResultStatus.Failure)
            {
                var errorMessage = string.Join(", ", initialResult.ErrorMessages);
                SetErrorMessage(errorMessage);
                throw new NonCriticalException(errorMessage);
            }
            else
            {
                bool hasReceivedResults = false;
                Guid publicJobId = initialResult.PublicJobId.Value;
                for (int attemptCount = 0; attemptCount < maxRetryCount; attemptCount++)
                {
                    var result = CreditReportRequestHandler.CheckForRequestResults(publicJobId);
                    if (result.Status == CreditReportRequestResultStatus.Failure)
                    {
                        var errorMessage = string.Join(", ", result.ErrorMessages);
                        SetErrorMessage(errorMessage);
                        throw new NonCriticalException(errorMessage);
                    }
                    else if (result.Status == CreditReportRequestResultStatus.Completed)
                    {
                        var creditResponse = result.CreditReportResponse;
                        var finalCreditRequestData = result.CreditRequestData;

                        if (null == creditResponse)
                        {
                            SetErrorMessage("If are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.");
                            throw new NonCriticalException("Probably timeout issue. Already send email to developer.");
                        }

                        if (creditResponse.HasError)
                        {
                            // 7/28/2004 dd - Need to do something.
                            // 12/15/2006 nw - OPM 8360 - Provide a more descriptive message for FNMA "User not authenticated" error
                            if (creditResponse.ErrorMessage.ToLower() == "user not authenticated")
                            {
                                if (PriceMyLoanConstants.IsEmbeddedPML)
                                    SetErrorMessage("The FannieMae DU login information is incorrect. Please confirm your FannieMae DU login and password. To verify your account, contact FannieMae Customer Care at (877)722-6757.");
                                else
                                    SetErrorMessage("The FannieMae DU login information is incorrect. Please contact your lender administrator and ask them to confirm their FannieMae DU login and password.");
                            }
                            else
                                SetErrorMessage(creditResponse.ErrorMessage);
                            throw new NonCriticalException("Error with credit report. Safely Ignore.");
                        }

                        E_CreditReportAuditSourceT requestT = E_CreditReportAuditSourceT.OrderNew;
                        if (isReissue)
                        {
                            requestT = E_CreditReportAuditSourceT.Reissue;
                        }
                        if (isUpgrade)
                        {
                            requestT = E_CreditReportAuditSourceT.Upgrade;
                        }

                        if (CreditReportUtilities.IsBorrowerInformationMatch(LoanID, dataApp.aAppId, finalCreditRequestData, creditResponse) || (isReissue && String.IsNullOrEmpty(dataApp.aBNm)))
                        {
                            sFileVersion = CreditReportUtilities.SaveXmlCreditReport(creditResponse, PriceMyLoanUser, LoanID, dataApp.aAppId, finalCreditRequestData.ActualCraID, finalCreditRequestData.ProxyId, requestT);
                            ImportLiabilities(true);
                        }
                        else
                        {
                            SetErrorMessage(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            throw new NonCriticalException(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                        }

                        hasReceivedResults = true;
                        break;
                    }
                    else
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(pollingIntervalInSeconds * 1000);
                    }
                }

                if (!hasReceivedResults)
                {
                    SetErrorMessage(ErrorMessages.AsyncReportTakingTooLongToProcess);
                    throw new CBaseException(ErrorMessages.AsyncReportTakingTooLongToProcess, "Async credit report request take too long to process.");
                }
            }
        }

        private void SubmitCreditSynchronously(CreditReportRequestHandler requestHandler, int maxRetryCount, bool isReissue, bool isUpgrade, CAppData dataApp, CreditRequestData requestData)
        {
            bool hasReceivedResults = false;
            for (int attemptCount = 0; attemptCount < maxRetryCount; attemptCount++)
            {
                var result = requestHandler.SubmitSynchronously();
                if (result.Status == CreditReportRequestResultStatus.Failure)
                {
                    var errorMessage = string.Join(", ", result.ErrorMessages);
                    SetErrorMessage(errorMessage);
                    throw new NonCriticalException(errorMessage);
                }
                else if (result.Status == CreditReportRequestResultStatus.Completed)
                {
                    var creditResponse = result.CreditReportResponse;
                    if (null == creditResponse)
                    {
                        SetErrorMessage("If are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.");
                        throw new NonCriticalException("Probably timeout issue. Already send email to developer.");
                    }
                    if (creditResponse.HasError)
                    {
                        // 7/28/2004 dd - Need to do something.
                        // 12/15/2006 nw - OPM 8360 - Provide a more descriptive message for FNMA "User not authenticated" error
                        if (creditResponse.ErrorMessage.ToLower() == "user not authenticated")
                        {
                            if (PriceMyLoanConstants.IsEmbeddedPML)
                                SetErrorMessage("The FannieMae DU login information is incorrect. Please confirm your FannieMae DU login and password. To verify your account, contact FannieMae Customer Care at (877)722-6757.");
                            else
                                SetErrorMessage("The FannieMae DU login information is incorrect. Please contact your lender administrator and ask them to confirm their FannieMae DU login and password.");
                        }
                        else
                            SetErrorMessage(creditResponse.ErrorMessage);
                        throw new NonCriticalException("Error with credit report. Safely Ignore.");
                    }

                    if (creditResponse.IsReady)
                    {
                        E_CreditReportAuditSourceT requestT = E_CreditReportAuditSourceT.OrderNew;
                        if (isReissue)
                        {
                            requestT = E_CreditReportAuditSourceT.Reissue;
                        }
                        if (isUpgrade)
                        {
                            requestT = E_CreditReportAuditSourceT.Upgrade;
                        }

                        if (CreditReportUtilities.IsBorrowerInformationMatch(LoanID, dataApp.aAppId, requestData, creditResponse) || (isReissue && String.IsNullOrEmpty(dataApp.aBNm)))
                        {
                            sFileVersion = CreditReportUtilities.SaveXmlCreditReport(creditResponse, PriceMyLoanUser, LoanID, dataApp.aAppId, requestData.ActualCraID, requestData.ProxyId, requestT);
                            this.ImportLiabilities(true);
                        }
                        else
                        {
                            SetErrorMessage(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            throw new NonCriticalException(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                        }

                        hasReceivedResults = true;
                        break;
                    }
                    else if (requestData.CreditProtocol == CreditReportProtocol.Mcl)
                    {
                        requestData.ReportID = creditResponse.ReportID;
                        requestData.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                    }
                    //av 1212008 Removed lender mapping check heres the guid new way should set up the right protocol e04d2398-e614-42a0-9a50-3276c14ab740 
                    else if (requestData.CreditProtocol == CreditReportProtocol.InfoNetwork)
                    {
                        // 8/9/2005 dd - Information Network support async request.
                        requestData.ReportID = creditResponse.ReportID;
                        requestData.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
                    }

                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10000); // Wait for 10 seconds before reissue. 
                }
            }

            if (!hasReceivedResults)
            {
                SetErrorMessage(ErrorMessages.AsyncReportTakingTooLongToProcess);
                throw new CBaseException(ErrorMessages.AsyncReportTakingTooLongToProcess, "Async credit report request take too long to process.");
            }
        }

        private void ImportLiabilities(bool skip0Balance)
        {
            CPageData dataLoan = new CPmlImportLiabilityData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);
            sFileVersion = CreditReportUtilities.ImportLiabilities(dataLoan, dataLoan.GetAppData(AppNum), skip0Balance, Request.Form["DeleteLiabilities"] == "1", importBorrowerInfo: true);
        }

        public void SaveData()
        {
            DetermineBrokerDULoginInfo(this.CurrentBroker);

            bool isReissue = CreditAction == 1;
            bool isOrderNew = CreditAction == 2;
            bool isUpgrade = CreditAction == 4;

            if (CreditAction == 3)
            {
                SaveManualCreditReport();
            }
            else if (isReissue || isOrderNew || isUpgrade)
            {
                PullCreditAndSave(isReissue, isOrderNew, isUpgrade);
            }
        }

        /// <summary>
        /// Sets the borrower info into the given object. Sets either borrower or co based on isCoborrower
        /// if its the main borrower it will also set his address.
        /// </summary>
        /// <param name="borrower"></param>
        /// <param name="IsCoborrower"></param>
        private void SetBorrowerInfo(LendersOffice.CreditReport.BorrowerInfo borrower, bool isCoborrower, CreditReportProtocol protocol)
        {
            borrower.DOB = isCoborrower ? aCDob.Text : aBDob.Text;

            CPageData dataLoan = new CEnterCreditData(LoanID);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(AppNum);

            borrower.FirstName = isCoborrower ? dataApp.aCFirstNm : dataApp.aBFirstNm;
            borrower.MiddleName = isCoborrower ? dataApp.aCMidNm : dataApp.aBMidNm;
            borrower.LastName = isCoborrower ? dataApp.aCLastNm : dataApp.aBLastNm;
            borrower.Suffix = isCoborrower ? dataApp.aCSuffix : dataApp.aBSuffix;

            if (isCoborrower)
            {
                borrower.Ssn = dataApp.aCSsn;

                // OPM 451238 - Set mailing address for coborrower for protocols that require coborrower address.
                if (protocol == CreditReportProtocol.CSD || protocol == CreditReportProtocol.InformativeResearch)
                {
                    if (!string.IsNullOrEmpty(dataApp.aCAddrMail) || !string.IsNullOrEmpty(dataApp.aCCityMail)
                        || !string.IsNullOrEmpty(dataApp.aCStateMail) || !string.IsNullOrEmpty(dataApp.aCZipMail))
                    {
                        Address coMailingAddress = new Address();
                        coMailingAddress.StreetAddress = dataApp.aCAddrMail;
                        coMailingAddress.City = dataApp.aCCityMail;
                        coMailingAddress.State = dataApp.aCStateMail;
                        coMailingAddress.Zipcode = dataApp.aCZipMail;
                        borrower.MailingAddress = coMailingAddress;
                    }
                }

                return;
            }
            else
            {
                borrower.Ssn = dataApp.aBSsn;
            }

            Address address = new Address();
            address.City = aBCity.Text;
            address.State = aBState.Value;
            address.Zipcode = aBZip.Text;

            try
            {
                address.ParseStreetAddress(aBAddr.Text);
            }
            catch (Exception e)
            {
                address.StreetAddress = aBAddr.Text;
                Tools.LogWarning("Unable to parse borrower address: " + aBAddr.Text, e);
            }
            finally
            {
                borrower.CurrentAddress = address;
            }

            // Set Mailing address
            Address mailingAddress = new Address();
            mailingAddress.StreetAddress = dataApp.aAddrMail;
            mailingAddress.City = dataApp.aCityMail;
            mailingAddress.State = dataApp.aStateMail;
            mailingAddress.Zipcode = dataApp.aZipMail;
            borrower.MailingAddress = mailingAddress;
        }

        private CreditRequestData CreateRequestData(Guid loanId, Guid appId, Guid craId, Guid branchId)
        {
            bool isReissueRequest = CreditAction == 1;
            bool isNewRequest = CreditAction == 2;
            bool isUpgradeRequest = CreditAction == 4;

            // OPM 450477. If there is any stored credentials that match use them.
            var storedCredential = ServiceCredential.ListAvailableServiceCredentials(PriceMyLoanUser, branchId, ServiceCredentialService.CreditReports)
                .FirstOrDefault(credential => credential.ServiceProviderId.HasValue && credential.ServiceProviderId == craId);
            
            CreditRequestData data = CreditRequestData.Create(loanId, appId);

            data.IncludeEquifax = IsEquifax.Checked;
            data.IncludeExperian = IsExperian.Checked;
            data.IncludeTransUnion = IsTransUnion.Checked;
            data.ReportID = isNewRequest ? "" : ReportID.Text;
            data.RequestingPartyName = PriceMyLoanUser.DisplayName;

            if (storedCredential != null)
            {
                data.LoginInfo.UserName = storedCredential.UserName;
                data.LoginInfo.Password = storedCredential.UserPassword.Value;
            }
            else
            {
                data.LoginInfo.UserName = LoginName.Text;
                data.LoginInfo.Password = Password.Text;
            }

            data.UpdateCRAInfoFrom(craId, PriceMyLoanUser.BrokerId, true);

            SetBorrowerInfo(data.Borrower, false, data.CreditProtocol);
            SetBorrowerInfo(data.Coborrower, true, data.CreditProtocol);

            //If its lender mapping instnat view wont work. 
            if (!isNewRequest && !isUpgradeRequest && !data.WasLenderMapping)
            {
                data.InstantViewID = InstantViewID.Text;
            }
            switch (data.CreditProtocol)
            {
                case CreditReportProtocol.SharperLending:
                    if (storedCredential != null)
                    {
                        data.LoginInfo.AccountIdentifier = storedCredential.AccountId;
                    }
                    else
                    {
                        data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    }
                    break;
                case CreditReportProtocol.KrollFactualData:
                    if (storedCredential != null)
                    {
                        data.LoginInfo.AccountIdentifier = storedCredential.AccountId;
                    }
                    else
                    {
                        data.LoginInfo.AccountIdentifier = FD_OfficeCode.Text + FD_ClientCode.Text;
                    }
                    break;
                case CreditReportProtocol.FannieMae:
                    data.LoginInfo.FannieMaeCreditProvider = data.Url;
                    if (m_hasBrokerDUInfo)
                    {
                        data.LoginInfo.FannieMaeMORNETUserID = m_brokerDUUserID;
                        data.LoginInfo.FannieMaeMORNETPassword = m_brokerDUPassword;
                    }
                    else if (!data.WasLenderMapping) //lender mapping should set the password and username cause ui doesnt show that info when lendermapping is selected.
                    {
                        data.LoginInfo.FannieMaeMORNETUserID = DUUserID.Text;
                        data.LoginInfo.FannieMaeMORNETPassword = DUPassword.Text;
                    }
                    break;
                case CreditReportProtocol.CBC:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    if (storedCredential != null)
                    {
                        data.LoginInfo.AccountIdentifier = storedCredential.AccountId;
                    }
                    else
                    {
                        data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    }
                    break;
                case CreditReportProtocol.Mcl:
                    data.IsPdfViewNeeded = CreditReportUtilities.IsCraPdfEnabled(CreditReportProtocol.Mcl, PriceMyLoanUser.BrokerId, data.ActualCraID);
                    if (isUpgradeRequest)
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Upgrade;
                    }
                    else if (isNewRequest)
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.New;
                    }
                    else
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                    }
                    break;
                case CreditReportProtocol.InformativeResearch:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    data.LoginInfo.AccountIdentifier = storedCredential?.AccountId ?? AccountIdentifier.Text;
                    break;
                case CreditReportProtocol.Credco:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    break;
                default:
                    if (storedCredential != null)
                    {
                        data.LoginInfo.AccountIdentifier = storedCredential.AccountId;
                    }
                    else
                    {
                        data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    }
                    break;

            }

            data.IsMortgageOnly = IsMortgageOnlyCB.Checked;
            if (isReissueRequest)
            {
                data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Reissue;
            }

            // The UI reads "Upgrade existing report to Tri-Merge Report"
            if (isUpgradeRequest)
            {
                data.IncludeEquifax = true;
                data.IncludeExperian = true;
                data.IncludeTransUnion = true;
            }

            return data;
        }
        public void SetErrorMessage(string message)
        {
            ErrorMessage.Text = message;
        }

        protected void SetRequiredValidatorMessage(System.Web.UI.WebControls.BaseValidator v)
        {
            HtmlImage img = new HtmlImage();
            img.Src = ResolveUrl("~/images/error_pointer.gif");
            v.Controls.Add(img);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
