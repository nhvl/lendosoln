﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonQmBankStatementIncomeCalculator.ascx.cs" Inherits="PriceMyLoan.webapp.NonQmBankStatementIncomeCalculator" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<table id="BankStatementIncomeCalculatorTable">
    <tr>
        <td>
            <header class="page-header">Bank Statement Income</header>
        </td>
    </tr>
    <tr class="selectedRateRow">
        <td>
            <div class="selectedRate">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <header class="header">Attach Bank Statements and Other Relevant Documents</header>
            <div id="DragDropZoneContainer_BankStatement" class="InsetBorder full-width">
                <div id="DragDropZone_BankStatement" class="drag-drop-container" extensions=".pdf" limit="10" ></div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="section">
                <header class="header">Loan</header>
                <div class="loan-info-padding-left">
                    <div class="table table-bottom">                        
                        <div>                                        
                            <div>
                                <label>Borrower First Name<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <asp:TextBox ID="aBFirstNm" runat="server" onchange="validateBankStatementInput()"></asp:TextBox>
                        </div>
                        </div>
                        <div>
                            <div>
                                <label>Borrower Last Name<span class="text-danger">*</span></label>
                            </div>
                            <div>                                        
                                <asp:TextBox ID="aBLastNm" runat="server" onchange="validateBankStatementInput()"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label>Self Employed?</label>                                        
                            </div>
                            <div>                                        
                                <asp:DropDownList ID="sIsSelfEmployed" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <header class="header">Message to Lender</header>
            <div class="box-section-border-red" id="box3-section10">
                <textarea cols="1" rows="1" id="MsgToLender" runat="server" placeholder="Add your comments or questions here..."></textarea>
                </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <button type="button" class="btn btn-flat" onclick="returnToQp(true)">Back</button>
            <button type="button" class="btn btn-default submitButton" onclick="onSubmit(true)">Submit</button>
        </td>
    </tr>
</table>