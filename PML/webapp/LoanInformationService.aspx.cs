﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using PriceMyLoan.webapp.Partials.LoanInformation;

namespace PriceMyLoan.webapp
{
    public partial class LoanInformationService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            this.AddBackgroundItem("LoanTerms", new LoanTermsServiceItem());
            this.AddBackgroundItem("FannieMaeAddendumSections", new FannieAddendumServiceItem());
            this.AddBackgroundItem("FreddieMacAddendumSections", new FreddieAddendumServiceItem());
        }

        private class LoanTermsServiceItem : AbstractBackgroundServiceItem
        {
            protected override CPageData ConstructPageDataClass(Guid sLId)
            {
                return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanTermsServiceItem));
            }

            protected override void LoadData(CPageData dataLoan, CAppData dataApp)
            {
                this.SetResult("LoanInformationViewModel", SerializationHelper.JsonNetAnonymousSerialize(LoanInformation.GetViewModel(dataLoan)));
            }

            protected override void BindData(CPageData dataLoan, CAppData dataApp)
            {
                // This page is readonly
            }
        }

        private class FreddieAddendumServiceItem : AbstractBackgroundServiceItem
        {
            protected override CPageData ConstructPageDataClass(Guid sLId)
            {
                return CPageData.CreateUsingSmartDependency(sLId, typeof(FreddieAddendumServiceItem));
            }

            protected override void LoadData(CPageData dataLoan, CAppData dataApp)
            {
                this.SetResult(nameof(CPageData.sSpIsInPud), dataLoan.sSpIsInPud);
                this.SetResult(nameof(CPageData.sBuildingStatusT), dataLoan.sBuildingStatusT);
                this.SetResult(nameof(CPageData.sFreddieConstructionT), dataLoan.sFreddieConstructionT);
                this.SetResult(nameof(CPageData.sSpMarketVal), dataLoan.sSpMarketVal_rep);

                this.SetResult(nameof(CPageData.sPayingOffSubordinate), dataLoan.sPayingOffSubordinate);
                this.SetResult(nameof(CPageData.sProdCashoutAmt), dataLoan.sProdCashoutAmt_rep);
                this.SetResult(nameof(CPageData.sProdCashoutAmtReadOnly), dataLoan.sProdCashoutAmtReadOnly);
                this.SetResult(nameof(CPageData.sHelocCreditLimit), dataLoan.sHelocCreditLimit_rep);
                this.SetResult(nameof(CPageData.sHelocBal), dataLoan.sHelocBal_rep);
                this.SetResult(nameof(CPageData.sFHASalesConcessions), dataLoan.sFHASalesConcessions_rep);

                this.SetResult(nameof(CPageData.sBuydown), dataLoan.sBuydown);
                this.SetResult(nameof(CPageData.sBuydownContributorT), dataLoan.sBuydownContributorT);

                this.SetResult(nameof(CPageData.sFredAffordProgId), dataLoan.sFredAffordProgId);
                this.SetResult(nameof(CPageData.sFredieReservesAmt), dataLoan.sFredieReservesAmt_rep);
                this.SetResult(nameof(CPageData.sFredieReservesAmtLckd), dataLoan.sFredieReservesAmtLckd);
                this.SetResult(nameof(CPageData.sNegAmortT), dataLoan.sNegAmortT);
                this.SetResult(nameof(CPageData.sFreddieArmIndexT), dataLoan.sFreddieArmIndexT);
                this.SetResult(nameof(CPageData.sFreddieArmIndexTLckd), dataLoan.sFreddieArmIndexTLckd);
                this.SetResult(nameof(CPageData.sFreddieFHABorrPdCc), dataLoan.sFreddieFHABorrPdCc_rep);
                this.SetResult(nameof(CPageData.sFHAFinancedDiscPtAmt), dataLoan.sFHAFinancedDiscPtAmt_rep);

                this.SetResult("DwnSrc", dataLoan.sIsUseDUDwnPmtSrc ? FreddieAddendumSections.nameof_DUDownPaymentSourceJSON : FreddieAddendumSections.nameof_DUDownPaymentSource1003);
                this.SetResult(nameof(CPageData.sDUDwnPmtSrc), dataLoan.sDUDwnPmtSrc.Serialize());
            }

            protected override void BindData(CPageData dataLoan, CAppData dataApp)
            {
                dataLoan.sSpIsInPud = this.GetBool(nameof(CPageData.sSpIsInPud));
                dataLoan.sBuildingStatusT = this.GetEnum<E_sBuildingStatusT>(nameof(CPageData.sBuildingStatusT));
                dataLoan.sFreddieConstructionT = this.GetEnum<E_sFreddieConstructionT>(nameof(CPageData.sFreddieConstructionT));
                dataLoan.sSpMarketVal_rep = this.GetString(nameof(CPageData.sSpMarketVal));

                dataLoan.sPayingOffSubordinate = this.GetBool(nameof(CPageData.sPayingOffSubordinate));
                dataLoan.sProdCashoutAmt_rep = this.GetString(nameof(CPageData.sProdCashoutAmt));
                dataLoan.sHelocCreditLimit_rep = this.GetString(nameof(CPageData.sHelocCreditLimit));
                dataLoan.sHelocBal_rep = this.GetString(nameof(CPageData.sHelocBal));
                dataLoan.sFHASalesConcessions_rep = this.GetString(nameof(CPageData.sFHASalesConcessions));

                dataLoan.sBuydown = this.GetBool(nameof(CPageData.sBuydown));
                dataLoan.sBuydownContributorT = this.GetEnum<E_sBuydownContributorT>(nameof(CPageData.sBuydownContributorT));

                dataLoan.sFredAffordProgId = this.GetString(nameof(CPageData.sFredAffordProgId));
                dataLoan.sFredieReservesAmt_rep = this.GetString(nameof(CPageData.sFredieReservesAmt));
                dataLoan.sFredieReservesAmtLckd = this.GetBool(nameof(CPageData.sFredieReservesAmtLckd));
                dataLoan.sNegAmortT = this.GetEnum<E_sNegAmortT>(nameof(CPageData.sNegAmortT));
                dataLoan.sFreddieArmIndexT = this.GetEnum<E_sFreddieArmIndexT>(nameof(CPageData.sFreddieArmIndexT));
                dataLoan.sFreddieArmIndexTLckd = this.GetBool(nameof(CPageData.sFreddieArmIndexTLckd));
                ////dataLoan.sFreddieFHABorrPdCc_rep = this.GetString(nameof(CPageData.sFreddieFHABorrPdCc)); // readonly field
                dataLoan.sFHAFinancedDiscPtAmt_rep = this.GetString(nameof(CPageData.sFHAFinancedDiscPtAmt));

                dataLoan.sIsUseDUDwnPmtSrc = this.GetString("DwnSrc") == FreddieAddendumSections.nameof_DUDownPaymentSourceJSON;
                DownPaymentGiftSet downpaymentSource = dataLoan.sDUDwnPmtSrc;
                downpaymentSource.Import(this.GetString("sDUDwnPmtSrc"));
                dataLoan.sDUDwnPmtSrc = downpaymentSource;
            }
        }
    }
}