namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.BrokerAdmin;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using PriceMyLoan.UI;
    using global::DataAccess;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public partial class LoanApplication : PriceMyLoan.UI.BaseUserControl, IPopulateDropDownsControl
    {
        protected string Source
        {
            get
            {
                return RequestHelper.GetSafeQueryString("source");
            }
        }

        protected bool m_canRunLpeWithoutCreditReport
        {
            get { return PriceMyLoanUser.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            List<Guid> denyCraList = MasterCRAList.RetrieveHiddenCras(this.CurrentBroker, E_ApplicationT.PriceMyLoan);
            CreditProtocol.Items.Clear();
            CreditProtocol.Items.Add(new ListItem("<-- Select Credit Provider -->", Guid.Empty.ToString()));
            bool hasLenderMappingCRA = false;
            foreach (var proxy in CreditReportAccountProxy.Helper.ListCreditProxyByBrokerID(this.BrokerID))
            {
                if (denyCraList.Contains(proxy.CrAccProxyId))
                {
                    continue;
                }

                CreditProtocol.Items.Add(new ListItem(proxy.CrAccProxyDesc, proxy.CrAccProxyId.ToString()));
                hasLenderMappingCRA = true;
            }

            if (hasLenderMappingCRA)
            {
                CreditProtocol.Items.Add(new ListItem("---------------------------", Guid.Empty.ToString()));
            }

            var masterCraList = LendersOffice.CreditReport.MasterCRAList.RetrieveAvailableCras(this.CurrentBroker);
            foreach (LendersOffice.CreditReport.CRA o in masterCraList)
            {
                if (denyCraList.Contains(o.ID))
                    continue;

                CreditProtocol.Items.Add(new ListItem(o.VendorName, o.ID.ToString()));
            }

            // OPM 450477. If there is any login at the branch or broker level,
            // pre-populate the first one in the list. Otherwise, use old method.
            // It is intentional that we use the branch of loan rather than the user.
            
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanApplication));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            var thisUserType = PriceMyLoanUser.Type.ToUpper() == "P" ? UserType.TPO : UserType.LQB;

            var savedCredential = ServiceCredential.ListAvailableServiceCredentials(PrincipalFactory.CurrentPrincipal, dataLoan.sBranchId, ServiceCredentialService.CreditReports).FirstOrDefault();

            if (savedCredential != null)
            {
                Tools.SetDropDownListValue(CreditProtocol,savedCredential.ServiceProviderId.ToString());
            }
            else
            {
                Tools.SetDropDownListValue(CreditProtocol, PriceMyLoanUser.LastUsedCreditProtocolID.ToString());
            }

            Loan1003Link.Visible = !PriceMyLoanUser.EnableNewTpoLoanNavigation && !PriceMyLoanConstants.IsEmbeddedPML && CurrentBroker.Enable1003EditorInTPOPortal;
        }

        #region IPopulateDropDownsControl Members

        Dictionary<string, object> IPopulateDropDownsControl.PopulateDDLs(CPageData loan)
        {
            var ddls = new Dictionary<string, object>();

            ddls.Add("aBVServiceT", Tools.ExtractDDLEntries(Tools.Bind_aVServiceT));
            ddls.Add("aCVServiceT", Tools.ExtractDDLEntries(Tools.Bind_aVServiceT));
            ddls.Add("aBVEnt", Tools.ExtractDDLEntries(Tools.Bind_aVEnt));
            ddls.Add("aCVEnt", Tools.ExtractDDLEntries(Tools.Bind_aVEnt));
            ddls.Add("aProdBCitizenT", Tools.ExtractDDLEntries(Tools.Bind_aProdBCitizenT));
            ddls.Add("aProdCCitizenT", Tools.ExtractDDLEntries(Tools.Bind_aProdCCitizenT));

            return ddls;
        }

        Dictionary<string, object> IPopulateDropDownsControl.GetHiddenOptions()
        {
            var hiddenOptions = new Dictionary<string, object>();
            AbstractUserPrincipal p = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;

            hiddenOptions.Add("aBVServiceT", BrokerOptionConfiguration.Instance.GetOptionsFor("aBVServiceT", p.BrokerId));
            hiddenOptions.Add("aCVServiceT", BrokerOptionConfiguration.Instance.GetOptionsFor("aCVServiceT", p.BrokerId));
            hiddenOptions.Add("aBVEnt", BrokerOptionConfiguration.Instance.GetOptionsFor("aBVEnt", p.BrokerId));
            hiddenOptions.Add("aCVEnt", BrokerOptionConfiguration.Instance.GetOptionsFor("aCVEnt", p.BrokerId));
            hiddenOptions.Add("aProdBCitizenT", BrokerOptionConfiguration.Instance.GetOptionsFor("aProdBCitizenT", p.BrokerId));
            hiddenOptions.Add("aProdCCitizenT", BrokerOptionConfiguration.Instance.GetOptionsFor("aProdCCitizenT", p.BrokerId));

            return hiddenOptions;
        }

        #endregion
    }
}