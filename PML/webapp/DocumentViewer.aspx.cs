﻿#region Generated Code
namespace PriceMyLoan.webapp
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using global::DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a cross-browser compatible means for viewing PDF documents.
    /// </summary>
    public partial class DocumentViewer : UI.BasePage
    {
        /// <summary>
        /// Loads the page, sending the PDF to the client's browser for viewing.
        /// </summary>
        /// <param name="sender">
        /// The sender for the page load.
        /// </param>
        /// <param name="e">
        /// The event arguments for the page load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var path = this.GetPdfPath();

            this.Response.Clear();
            this.Response.ContentType = "application/pdf";
            this.Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            this.Response.Cache.SetLastModified(DateTime.Now);
            this.Response.WriteFile(path);
        }

        /// <summary>
        /// Handles the "Download PDF" button click event.
        /// </summary>
        /// <param name="sender">
        /// The sender for the event.
        /// </param>
        /// <param name="e">
        /// The arguments for the event.
        /// </param>
        protected void DownloadPdfButton_Click(object sender, EventArgs e)
        {
            RequestHelper.SendFileToClient(HttpContext.Current, this.GetPdfPath(), "application/pdf", "Initial Disclosure Document Preview");
        }

        /// <summary>
        /// Obtains the path to the PDF.
        /// </summary>
        /// <returns>
        /// The path to the PDF.
        /// </returns>
        private string GetPdfPath()
        {
            var key = RequestHelper.GetSafeQueryString("key");
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new GenericUserErrorMessageException("Missing document key in query string.");
            }

            if (FileDBTools.DoesFileExist(E_FileDB.Temp, key))
            {
                return FileDBTools.CreateCopy(E_FileDB.Temp, key);
            }

            throw new GenericUserErrorMessageException("Could not locate PDF.");
        }
    }
}