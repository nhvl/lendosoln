﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NonQm.aspx.cs" Inherits="PriceMyLoan.webapp.NonQm" %>
<%@ Register src="~/webapp/NonQmQuickPricer.ascx" tagname="QuickPricer" tagprefix="uc" %>
<%@ Register src="~/webapp/NonQmBankStatementIncomeCalculator.ascx" tagname="BankStatementIncomeCalculator" tagprefix="uc" %>
<%@ Register src="~/webapp/NonQmScenarioRequestForm.ascx" tagname="ScenarioRequestForm" tagprefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<form runat="server">
<div class="non-qm-container">
    <uc:QuickPricer id="QuickPricer" runat="server"/>
    <uc:BankStatementIncomeCalculator id="BankStatementIncomeCalculator" runat="server"/>
    <uc:ScenarioRequestForm ID="ScenarioRequestForm" runat="server" />
</div>
</form>
</body>
</html>
