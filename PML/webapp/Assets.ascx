﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Assets.ascx.cs" Inherits="_1003page.BorrowerInfo.Assets" %>
<div class="modal-header">
    <button type="button" class="close" onclick="onCancel('AssetDialog');"><i class="material-icons">&#xE5CD;</i></button>
    <h4 class="modal-title">Assets Editor</h4>
</div>
<div class="modal-body">

<div class="warp-content">
    <div class="content">
        <div class="pu-asset employment">
            <header class="header table-flex table-flex-app-info non-reo">
                <div class="header-assets-edit"><span>Non-Real Estate Assets</span></div>
                <div class="text-grey text-right nowrap">
                    <div class="table table-assets-edit wide">
                        <div><div><label class="text-grey text-right nowrap">Total Liquid Assets:    </label></div><div id="TotalLiquidAssetValue"></div></div>
                        <div><div><label class="text-grey text-right nowrap">Total Non-Liquid Assets:</label></div><div id="TotalNonLiquidAssetValue"></div></div></div>
                    </div>
                </div>
            </header>
            <div class="employment-content non-reo">
                <table class="DataGrid margin-bottom wide none-hover-table">
                    <thead><tr class="GridHeader">
                        <th>                  </th>
                        <th>Ownership         </th>
                        <th>Account/Asset Type</th>
                        <th>Institution       </th>
                        <th>Account Number    </th>
                        <th class="text-right">Value             </th>
                        <th>                  </th>
                    </tr></thead>
                    <tbody id="tNonRealEstateAssets">
                        <tr>
                            <td><a class="btn btn-link"></a></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a>delete</a> <a>cancel</a></td>
                        </tr>
                    </tbody>
                </table><br/><br/>
            </div>

            <header class="header table-flex table-flex-app-info reo">
                <div class="header-assets-edit"><span>Real Estate Assets</span></div>
                <div class="text-grey text-right nowrap">
                    <div class="table table-assets-edit wide">
                        <div><div><label class="text-grey text-right nowrap">Total Real-Estate Assets:</label></div><div id="TotalRealEstateAssetValue"></div></div>
                    </div>
                </div>
            </header>
            <div class="employment-content">
                <table class="DataGrid margin-bottom wide none-hover-table reo">
                    <thead><tr class="GridHeader">
                        <th>                    </th>
                        <th>Ownership           </th>
                        <th>Property Address    </th>
                        <th>Status              </th>
                        <th class="text-right">Market Value        </th>
                        <th class="text-right">Gross Rent          </th>
                        <th class="text-right">Ins., Maint. & Taxes</th>
                        <th>                    </th>
                    </tr></thead>
                    <tbody id="tRealEstateAssets"></tbody>
                </table><br/>
                <div id="assetForm" class="table table-app-info-asets">
                    <div>
                        <div class="text-grey text-right nowrap"><label>Ownership</label></div>
                        <div>
                            <asp:DropDownList ID="OwnerT" runat="server">
                                <asp:ListItem ID="owner1"  Value="0" Selected="True"/>
                                <asp:ListItem ID="owner2" Value="1"/>
                                <asp:ListItem Value="2">Joint</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="asset-type">
                        <div class="text-grey text-right nowrap"><label>Account/Asset Type</label></div>
                        <div>
                            <asp:DropDownList ID="AssetT" onchange="AssetsChanged();" runat="server">
                                <asp:ListItem Value="-1">Real Estate</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="Other_group">
                        <div class="text-grey text-right nowrap"><label>Description</label></div>
                        <div><input id="Desc" type="text" /></div>
                    </div>


                    <!--show if AssetType = Real Estate-->
                    <div name="RE_group">
                        <div></div>
                        <div>
                                <input id="IsPrimaryResidence" type="checkbox" />
                                <label for="IsPrimaryResidence">This property is primary residence.</label>
                        </div>
                    </div>
                    <div name="RE_group">
                        <div></div>
                        <div>
                            <!--hide if loan Purpose = Purchase a home-->
                                <input id="IsSubjectProp" type="checkbox" />
                                <label for="IsSubjectProp">This property is the subject property for this loan.</label>
                        </div>
                    </div>
                    <div name="RE_group">
                        <div class="text-grey text-right nowrap"><label>Property Address </label></div>
                        <div><input id="Addr" type="text" class="form-control-address" /></div>
                    </div>
                    <div name="RE_group">
                        <div class="text-grey text-right nowrap"><label>Property Zip Code </label></div>
                        <div><ml:zipcodetextbox id="Zip" runat="server" preset="zipcode" width="50" /></div>
                    </div>
                    <div name="RE_group">
                        <div class="text-grey text-right nowrap"><label>Property City </label></div>
                        <div><asp:TextBox runat="server" ID="City" type = "text" class="form-control-city"/></div>
                    </div>
                    <div name="RE_group">
                        <div class="text-grey text-right nowrap"><label>Property State </label></div>
                        <div><ml:StateDropDownList ID="AssetST" runat="server"  /></div>
                    </div>
                    <div name="RE_group">
                        <div class="text-grey text-right nowrap"><label>Property Type </label></div>
                        <div><asp:DropDownList ID="Type" runat="server"/></div>
                    </div>
                    <div name="RE_group">
                        <div class="text-grey text-right nowrap"><label>Property Status </label></div>
                        <div><asp:DropDownList ID="StatT" onchange="PropStatusChanged();" runat="server">
                                    <asp:ListItem Value="S">Sold</asp:ListItem>
                                    <asp:ListItem Value="PS">Pending Sale</asp:ListItem>
                                    <asp:ListItem Value="R">Rental</asp:ListItem>
                                    <asp:ListItem Value="" Selected="True">Retained</asp:ListItem>
                                </asp:DropDownList>
                        </div>
                    </div>
                    <div name="RE_group" class="Rental_group">
                        <!--show if Property Status = Rental-->
                        <div class="text-grey text-right nowrap"><label>Gross Rent </label></div>
                        <div><input type="text" id="GrossRentI" preset="money" value="$0.00" /><span> / month</span></div>
                    </div>
                    <div name="RE_group" class="Rental_group">
                        <div class="text-grey text-right nowrap"><label>Occupancy Rate</label></div>
                        <div><input class="text-right" type="text" id="OccR" /><span>%</span></div>
                    </div>
                    <div name="RE_group">
                        <div class="text-grey text-right nowrap"><label>Insurance, Maintenance, and Taxes </label></div>
                        <div>
                            <input type="text" id="HExp" preset="money" value="$0.00" /><span> / month</span>
                        </div>
                    </div>                    

                    <!--hide if AssetType = Auto | Other Non | Real Estate-->
                    <div name="Financial_group">
                        <div class="text-grey text-right nowrap"><label>Financial Institution Name </label></div>
                        <div><input id="ComNm" type="text" /></div>
                    </div>
                    <div name="Financial_group">
                        <div class="text-grey text-right nowrap"><label>Account Number</label></div>
                        <div><input id="AccNum" type="text" /></div>
                    </div>

                    <div>
                        <div class="text-grey text-right nowrap"><label>Asset Value</label></div>
                        <div><input type="text" id="Val" preset="money" value="$0.00" /></div>
                    </div>

                    <div>
                        <div></div>
                        <div class="btn-add-asset">
                            <button type="submit" class="btn btn-default" id="btnAsset">Add Asset</button>
                            <input name="resetform" type="reset" id="resetform" value="Reset" style="display: none" />
                        </div>
                    </div>
                    <input type="hidden" id="recordID" />
                </div>
            </div>
            <hr class="hr-asset">
        </div>
    </div>
</div>

<div class="modal-footer">
    <div class="table-flex table-flex-app-info">
        <div class="text-left">
            <div class="table table-assets-edit">
                <div>
                    <div class="text-grey text-right nowrap">
                        <label>Total Assets:</label>
                    </div>
                    <div>
                        <span id="TotalAssetValue"></span>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <button type="button" id="btnCancel" class="btn btn-flat btn-next" onclick="onCancel('AssetDialog');">Cancel</button>
            <button type="button" id="btnSave"   class="btn btn-flat">Save</button>
        </div>
    </div>
</div>

