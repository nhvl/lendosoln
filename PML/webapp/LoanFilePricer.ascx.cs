﻿namespace PriceMyLoan.webapp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.BrokerAdmin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using PriceMyLoan.UI;
    using global::DataAccess;
    using LqbGrammar.DataTypes;

    public partial class LoanFilePricer : BaseUserControl, IPopulateDropDownsControl
    {
        public bool IsQP2File { get; set; }
        public bool IsLineOfCredit { get; set; }
        private BrokerDB Broker
        {
            get { return BrokerDB.RetrieveById(BrokerID); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                return;
            }

            var page = this.Parent.Page as PriceMyLoan.UI.BasePage;
            page.RegisterJsScript("webapp/LoanFilePricer.js");
            page.RegisterCSS("webapp/LoanFilePricer.css");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanFilePricer));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            (this.Page as BasePage).RegisterJsGlobalVariables("IsPmiEnabled", dataLoan.BrokerDB.HasEnabledPMI);
            FTHBContainer.Visible = E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT;

            if (Broker.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT))
            {
                EnableLenderFeeBuyout.Visible = true;
            }
            if (IsQP2File)
            {
                if (Broker.AddLeadSourceDropdownToInternalQuickPricer && PriceMyLoanConstants.IsEmbeddedPML)
                {
                    sLeadSrcDescHolder.Visible = true;
                }

                if(ConstStage.AllowLoanPurposeToggleInQP2 && false == IsLineOfCredit)
                {
                    IsPurchaseLoanToggleHolder.Visible = true;
                }
            }
            else
            {
                // the QP 2.0 holders are defaulted to not visible.
            }

            //get the portal mode from the cookie
            E_PortalMode portalMode = PriceMyLoanUser.PortalMode;
            bool isCorrespondent = portalMode.Equals(E_PortalMode.Correspondent) || portalMode.Equals(E_PortalMode.MiniCorrespondent);

            sCorrespondentProcessTContainer.Visible = isCorrespondent && !portalMode.Equals(E_PortalMode.MiniCorrespondent);

            sGfeIsTPOTransactionContainer.Visible = isCorrespondent;

            OriginatorCompensationPanel.Visible = !isCorrespondent;

            Page.ClientScript.RegisterHiddenField("EnableRenovationLoanSupport", dataLoan.BrokerDB.EnableRenovationLoanSupport.ToString());
        }

        #region IPopulateDropDownsControl Members

        Dictionary<string, object> IPopulateDropDownsControl.PopulateDDLs(CPageData loan)
        {
            var ddls = new Dictionary<string, object>();

            ddls.Add("sOccTPe", Tools.ExtractDDLEntries(Tools.Bind_aOccT));
            ddls.Add("sProdSpT", Tools.ExtractDDLEntries(Tools.Bind_sProdSpT));
            ddls.Add("sHomeIsMhAdvantageTri", Tools.ExtractDDLEntries(Tools.Bind_TriState));
            ddls.Add("sProdSpStructureT", Tools.ExtractDDLEntries(Tools.Bind_sProdSpStructureT));
            ddls.Add("sFhaCondoApprovalStatusT", Tools.ExtractDDLEntries(Tools.Bind_sFhaCondoApprovalStatusT));
            ddls.Add("sLPurposeTPe", Tools.ExtractDDLEntries(Tools.Bind_sLPurposeTPe));
            ddls.Add("sSellerCreditT", Tools.ExtractDDLEntries(Tools.Bind_sSellerCreditT));
            ddls.Add("sProdDocT", Tools.ExtractDDLEntries(ddl => Tools.Bind_sProdDocT(ddl, this.Broker, loan.sProdDocT)));
            ddls.Add("sOriginatorCompensationBorrPaidBaseT", Tools.ExtractDDLEntries(Tools.Bind_PercentBaseLoanAmountsT));
            ddls.Add("sPriorSalesSellerT", Tools.ExtractDDLEntries(Tools.Bind_sPriorSalesPropertySeller));
            DropDownList tempDdl = new DropDownList();
            Tools.Bind_sProd3rdPartyUwResultT(tempDdl);
            var items = new List<KeyValuePair<string, string>>();
            foreach (ListItem item in tempDdl.Items)
            {
                items.Add(new KeyValuePair<string, string>(item.Value, item.Text));
            }
            ddls.Add("sProd3rdPartyUwResultT", items);
            ddls.Add("sProdConvMIOptionT", Tools.ExtractDDLEntries(Tools.Bind_sProdConvMIOptionT));
            ddls.Add("sConvSplitMIRT", Tools.ExtractDDLEntries(Tools.Bind_sConvSplitMIRT));
            ddls.Add("sOtherLFinMethT", Tools.ExtractDDLEntries(Tools.Bind_sFinMethT));
            ddls.Add("sLenderFeeBuyoutRequestedT", Tools.ExtractDDLEntries(Tools.Bind_sLenderFeeBuyoutRequestedT));            if (IsQP2File && Broker.AddLeadSourceDropdownToInternalQuickPricer && PriceMyLoanConstants.IsEmbeddedPML)            {
                var broker = BrokerDB.RetrieveById(BrokerID);
                ddls.Add("sLeadSrcDesc", Tools.ExtractDDLEntries((ddl) => Tools.Bind_sLeadSrcDesc(broker, ddl)));
            }

            return ddls;
        }

        Dictionary<string, object> IPopulateDropDownsControl.GetHiddenOptions()
        {
            var hiddenOptions = new Dictionary<string, object>();

            AbstractUserPrincipal p = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;

            hiddenOptions.Add("sOccTPe", BrokerOptionConfiguration.Instance.GetOptionsFor("sOccTPe", p.BrokerId));
            hiddenOptions.Add("sProdSpT", BrokerOptionConfiguration.Instance.GetOptionsFor("sProdSpT", p.BrokerId));
            hiddenOptions.Add("sHomeIsMhAdvantageTri", BrokerOptionConfiguration.Instance.GetOptionsFor("sHomeIsMhAdvantageTri", p.BrokerId));
            hiddenOptions.Add("sProdSpStructureT", BrokerOptionConfiguration.Instance.GetOptionsFor("sProdSpStructureT", p.BrokerId));
            hiddenOptions.Add("sFhaCondoApprovalStatusT", BrokerOptionConfiguration.Instance.GetOptionsFor("sFhaCondoApprovalStatusT", p.BrokerId));
            hiddenOptions.Add("sLPurposeTPe", BrokerOptionConfiguration.Instance.GetOptionsFor("sLPurposeTPe", p.BrokerId));
            hiddenOptions.Add("sSellerCreditT", BrokerOptionConfiguration.Instance.GetOptionsFor("sSellerCreditT", p.BrokerId));
            hiddenOptions.Add("sProdDocT", BrokerOptionConfiguration.Instance.GetOptionsFor("sProdDocT", p.BrokerId));
            hiddenOptions.Add("sOriginatorCompensationBorrPaidBaseT", BrokerOptionConfiguration.Instance.GetOptionsFor("sOriginatorCompensationBorrPaidBaseT", p.BrokerId));
            hiddenOptions.Add("sPriorSalesSellerT", BrokerOptionConfiguration.Instance.GetOptionsFor("sPriorSalesSellerT", p.BrokerId));
            hiddenOptions.Add("sProd3rdPartyUwResultT", BrokerOptionConfiguration.Instance.GetOptionsFor("sProd3rdPartyUwResultT", p.BrokerId));
            hiddenOptions.Add("sProdConvMIOptionT", BrokerOptionConfiguration.Instance.GetOptionsFor("sProdConvMIOptionT", p.BrokerId));
            hiddenOptions.Add("sConvSplitMIRT", BrokerOptionConfiguration.Instance.GetOptionsFor("sConvSplitMIRT", p.BrokerId));
            hiddenOptions.Add("sOtherLFinMethT", BrokerOptionConfiguration.Instance.GetOptionsFor("sOtherLFinMethT", p.BrokerId));
            hiddenOptions.Add("sLenderFeeBuyoutRequestedT", BrokerOptionConfiguration.Instance.GetOptionsFor("sLenderFeeBuyoutRequestedT", p.BrokerId));

            return hiddenOptions;
        }

        #endregion
    }
}