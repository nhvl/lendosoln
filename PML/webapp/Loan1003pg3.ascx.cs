﻿using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Migration;

namespace PriceMyLoan.webapp
{
    public partial class Loan1003pg3 : PriceMyLoan.UI.BaseUserControl
    {
        protected bool m_isPurchase;

        /// <summary>
        /// Saves data to the loan from this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to bind to.</param>
        /// <param name="dataApp">The app to bind to.</param>
        /// <param name="serviceItem">The service item calling this method.</param>
        internal static void BindDataFromControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.BindHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}$");
        }

        /// <summary>
        /// Loads the data from the loan to this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The service item.</param>
        internal static void LoadDataForControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.LoadHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}$");
        }

        protected void PageInit()
        {
            Tools.Bind_aIntrvwrMethodT(aIntrvwrMethodT);
            Tools.Bind_aDecPastOwnedPropT(aBDecPastOwnedPropT);
            Tools.Bind_aDecPastOwnedPropTitleT(aBDecPastOwnedPropTitleT);
            Tools.Bind_aDecPastOwnedPropT(aCDecPastOwnedPropT);
            Tools.Bind_aDecPastOwnedPropTitleT(aCDecPastOwnedPropTitleT);
            BrokerZip.SmartZipcode(BrokerCity, BrokerState);
        }

        private void BindOtherCreditDescription(MeridianLink.CommonControls.ComboBox cb)
        {
            cb.Items.Add("Cash Deposit on sales contract");
            cb.Items.Add("Seller Credit");
            cb.Items.Add("Lender Credit");
            cb.Items.Add("Relocation Funds");
            cb.Items.Add("Employer Assisted Housing");
            cb.Items.Add("Lease Purchase Fund");
            cb.Items.Add("Borrower Paid Fees");
            cb.Items.Add("Paid Outside of Closing");
            cb.Items.Add("Broker Credit");
        }

        public void LoadData()
        {
            PageInit();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg3));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(RequestHelper.GetGuid("applicationid", Guid.Empty));

            aReTotVal.Text = dataApp.aReTotVal_rep;
            aReTotMAmt.Text = dataApp.aReTotMAmt_rep;
            aReTotGrossRentI.Text = dataApp.aReTotGrossRentI_rep;
            aReTotMPmt.Text = dataApp.aReTotMPmt_rep;
            aReTotHExp.Text = dataApp.aReTotHExp_rep;
            aReTotNetRentI.Text = dataApp.aReTotNetRentI_rep;

            aAltNm1.Value = dataApp.aAltNm1;
            aAltNm1CreditorNm.Value = dataApp.aAltNm1CreditorNm;
            aAltNm1AccNum.Value = dataApp.aAltNm1AccNum.Value;

            aAltNm2.Value = dataApp.aAltNm2;
            aAltNm2CreditorNm.Value = dataApp.aAltNm2CreditorNm;
            aAltNm2AccNum.Value = dataApp.aAltNm2AccNum.Value;

            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sPurchPrice.ReadOnly = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;

            m_isPurchase = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;
            sONewFinBal.Text = dataLoan.sONewFinBal_rep;

            sAltCost.Text = dataLoan.sAltCost_rep;
            this.sAltCostLckd.Checked = dataLoan.sAltCostLckd;
            this.sAltCostLckd.Disabled = IsReadOnly || !dataLoan.sIsRenovationLoan;

            sTotCcPbs.Text = dataLoan.sTotCcPbs_rep;
            this.sTotCcPbsLocked.Checked = dataLoan.sTotCcPbsLocked;

            sLandCost.Text = dataLoan.sLandCost_rep;

            sOCredit1Desc.Text = dataLoan.sOCredit1Desc;
            sOCredit1Amt.Text = dataLoan.sOCredit1Amt_rep;
            this.sOCredit1Lckd.Checked = dataLoan.sOCredit1Lckd;

            sOCredit2Desc.Text = dataLoan.sOCredit2Desc;
            sOCredit2Amt.Text = dataLoan.sOCredit2Amt_rep;

            sOCredit3Desc.Text = dataLoan.sOCredit3Desc;
            sOCredit3Amt.Text = dataLoan.sOCredit3Amt_rep;

            sOCredit4Desc.Text = dataLoan.sOCredit4Desc;
            sOCredit4Amt.Text = dataLoan.sOCredit4Amt_rep;

            if (dataLoan.sLoads1003LineLFromAdjustments)
            {
                this.sOCredit1Lckd.Disabled = true;
                sOCredit2Amt.ReadOnly = true;
                sOCredit2Desc.ReadOnly = true;
                sOCredit3Amt.ReadOnly = true;
                sOCredit3Desc.ReadOnly = true;
                sOCredit4Amt.ReadOnly = true;
                sOCredit4Desc.ReadOnly = true;
            }

            sOCredit5Amt.Text = dataLoan.sOCredit5Amt_rep;

            sRefPdOffAmt1003.Text = dataLoan.sRefPdOffAmt_rep;
            this.sRefPdOffAmt1003Lckd.Checked = dataLoan.sRefPdOffAmt1003Lckd;

            sTotEstPp1003.Text = dataLoan.sTotEstPp1003_rep;
            this.sTotEstPp1003Lckd.Checked = dataLoan.sTotEstPp1003Lckd;

            if (dataLoan.sIsIncludeProrationsIn1003Details && dataLoan.sIsIncludeProrationsInTotPp)
            {
                this.sTotEstPp.Text = dataLoan.sTotEstPp_rep;
                this.sTotalBorrowerPaidProrations.Text = dataLoan.sTotalBorrowerPaidProrations_rep;
            }
            else
            {
                this.ProrationsPaidByBorrowerRow.Visible = false;
                // this.PrepaidsProrationsSpacerRow.Visible = false;
                this.PrepaidsAndReservesRow.Visible = false;
            }

            sTotEstCcNoDiscnt1003.Text = dataLoan.sTotEstCcNoDiscnt1003_rep;
            this.sTotEstCc1003Lckd.Checked = dataLoan.sTotEstCc1003Lckd;

            if (dataLoan.sIsIncludeONewFinCcInTotEstCc)
            {
                this.sTotEstCcNoDiscnt.Text = dataLoan.sTotEstCcNoDiscnt_rep;
                this.sONewFinCc2.Text = dataLoan.sONewFinCc_rep;
                // this.OtherFinancingClosingCostsPaddingRow.Visible = false;
                this.OtherFinancingClosingCostsLabelRow.Visible = false;
                this.OtherFinancingClosingCostsAmountRow.Visible = false;
                // this.LenderCreditPaddingRow.Visible = false;
            }
            else
            {
                this.ThisLoanClosingCostsRow.Visible = false;
                // this.ThisLoanCcOtherFinCcSpacerRow.Visible = false;
                this.OtherFinancingClosingCosts2Row.Visible = false;
            }

            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            this.sFfUfmip1003Lckd.Checked = dataLoan.sFfUfmip1003Lckd;

            sLDiscnt1003.Text = dataLoan.sLDiscnt1003_rep;
            this.sLDiscnt1003Lckd.Checked = dataLoan.sLDiscnt1003Lckd;

            sONewFinCc.Text = dataLoan.sONewFinCc_rep;
            sTotTransC.Text = dataLoan.sTotTransC_rep;

            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            this.sLAmtLckd.Checked = dataLoan.sLAmtLckd;

            sFfUfmipFinanced1.Text = dataLoan.sFfUfmipFinanced_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;

            sTransNetCash.Text = dataLoan.sTransNetCash_rep;
            this.sTransNetCashLckd.Checked = dataLoan.sTransNetCashLckd;

            aBDecJudgment.Value = dataApp.aBDecJudgment;
            aBDecBankrupt.Value = dataApp.aBDecBankrupt;
            aBDecForeclosure.Value = dataApp.aBDecForeclosure;
            aBDecLawsuit.Value = dataApp.aBDecLawsuit;
            aBDecObligated.Value = dataApp.aBDecObligated;
            aBDecDelinquent.Value = dataApp.aBDecDelinquent;
            aBDecAlimony.Value = dataApp.aBDecAlimony;
            aBDecBorrowing.Value = dataApp.aBDecBorrowing;
            aBDecEndorser.Value = dataApp.aBDecEndorser;
            aBDecCitizen.Value = dataApp.aBDecCitizen;
            aBDecResidency.Value = dataApp.aBDecResidency;
            aBDecForeignNational.Value = dataApp.aBDecForeignNational;
            aBDecOcc.Value = dataApp.aBDecOcc;
            aBDecPastOwnership.Value = dataApp.aBDecPastOwnership;

            aCDecJudgment.Value = dataApp.aCDecJudgment;
            aCDecBankrupt.Value = dataApp.aCDecBankrupt;
            aCDecForeclosure.Value = dataApp.aCDecForeclosure;
            aCDecLawsuit.Value = dataApp.aCDecLawsuit;
            aCDecObligated.Value = dataApp.aCDecObligated;
            aCDecDelinquent.Value = dataApp.aCDecDelinquent;
            aCDecAlimony.Value = dataApp.aCDecAlimony;
            aCDecBorrowing.Value = dataApp.aCDecBorrowing;
            aCDecEndorser.Value = dataApp.aCDecEndorser;
            aCDecCitizen.Value = dataApp.aCDecCitizen;
            aCDecResidency.Value = dataApp.aCDecResidency;
            aCDecOcc.Value = dataApp.aCDecOcc;
            aCDecPastOwnership.Value = dataApp.aCDecPastOwnership;

            a1003InterviewD.Text = dataApp.a1003InterviewD_rep;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                a1003InterviewDLckd.Checked = dataApp.a1003InterviewDLckd;
            }
            else
            {
                a1003InterviewDLckd.Visible = false;
                a1003InterviewDLckdLabel.Visible = false;
            }

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            LoanOfficerName.Value = interviewer.PreparerName;
            LoanOfficerPhone.Value = interviewer.Phone;
            LoanOfficerLicenseNumber.Value = interviewer.LicenseNumOfAgent; // 11/15/06 mf. OPM 7970
            LoanOfficerCompanyLoanOriginatorIdentifier.Value = interviewer.CompanyLoanOriginatorIdentifier;
            LoanOfficerLoanOriginatorIdentifier.Value = interviewer.LoanOriginatorIdentifier;
            BrokerLicenseNumber.Value = interviewer.LicenseNumOfCompany;
            BrokerName.Value = interviewer.CompanyName;
            BrokerStreetAddr.Value = interviewer.StreetAddr;
            BrokerCity.Value = interviewer.City;
            BrokerState.Value = interviewer.State;
            BrokerZip.Text = interviewer.Zip;
            BrokerPhone.Value = interviewer.PhoneOfCompany;
            BrokerFax.Value = interviewer.FaxOfCompany;

            if (!interviewer.IsLocked)
            {
                LoanOfficerName.Attributes.Add("readonly", "true");
                LoanOfficerPhone.Attributes.Add("readonly", "true");
                LoanOfficerLicenseNumber.Attributes.Add("readonly", "true");
                LoanOfficerCompanyLoanOriginatorIdentifier.Attributes.Add("readonly", "true");
                LoanOfficerLoanOriginatorIdentifier.Attributes.Add("readonly", "true");
                BrokerName.Attributes.Add("readonly", "true");
                BrokerLicenseNumber.Attributes.Add("readonly", "true");
                BrokerStreetAddr.Attributes.Add("readonly", "true");
                BrokerCity.Attributes.Add("readonly", "true");
                BrokerState.Enabled = false;
                BrokerZip.Attributes.Add("readonly", "true");
                BrokerPhone.Attributes.Add("readonly", "true");
                BrokerFax.Attributes.Add("readonly", "true");
            }

            Page.ClientScript.RegisterHiddenField("IsLocked", interviewer.IsLocked.ToString());

            BindOtherCreditDescription(sOCredit1Desc);
            BindOtherCreditDescription(sOCredit2Desc);
            BindOtherCreditDescription(sOCredit3Desc);
            BindOtherCreditDescription(sOCredit4Desc);

            Tools.SetDropDownListValue(aBDecPastOwnedPropT, dataApp.aBDecPastOwnedPropT);
            Tools.SetDropDownListValue(aCDecPastOwnedPropT, dataApp.aCDecPastOwnedPropT);
            Tools.SetDropDownListValue(aBDecPastOwnedPropTitleT, dataApp.aBDecPastOwnedPropTitleT);
            Tools.SetDropDownListValue(aCDecPastOwnedPropTitleT, dataApp.aCDecPastOwnedPropTitleT);
            Tools.SetDropDownListValue(aIntrvwrMethodT, dataApp.aIntrvwrMethodT);
            aIntrvwrMethodTLckd.Checked = dataApp.aIntrvwrMethodTLckd;
            this.HREData.ShouldLoad = true;
            this.HREData.RaceEthnicityData = dataApp.ConstructHmdaRaceEthnicityData();

            var decExplnDataContainer = dataApp.ConstructDeclarationExplanationData();
            var serializedDecExplnData = SerializationHelper.JsonNetSerialize(decExplnDataContainer);
            this.Parent.Page.ClientScript.RegisterHiddenField("serializedDecExplnData", serializedDecExplnData);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           

        }
    }
}
