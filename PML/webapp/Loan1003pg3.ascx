﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Loan1003pg3.ascx.cs" Inherits="PriceMyLoan.webapp.Loan1003pg3" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="HRED" TagName="HmdaRaceEthnicityData" Src="~/webapp/HmdaraceEthnicitydata.ascx" %>

<script>
function lock1003Fields() {
    var a1003InterviewDLckdCb = <%= AspxTools.JsGetElementById(a1003InterviewDLckd) %>;
    if (a1003InterviewDLckdCb) {
        lockField(a1003InterviewDLckdCb, "a1003InterviewD");
    }

    var isPurchase = <%=AspxTools.JsBool(this.m_isPurchase)%>;
    var isRenovationLoan = ML.IsRenovationLoan;

    if (!isPurchase || isRenovationLoan) {
        <%= AspxTools.JQuery(this.sLAmtLckd) %>.prop('disabled', true).parent().addClass('disabled');
    }

    var altCostLockbox = <%=AspxTools.JQuery(this.sAltCostLckd)%>;
    if (altCostLockbox.prop('disabled')) {
        altCostLockbox.parent().addClass('disabled');
    }

    var otherCredit1Lockbox = <%=AspxTools.JQuery(this.sOCredit1Lckd)%>;
    if (otherCredit1Lockbox.prop('disabled')) {
        otherCredit1Lockbox.parent().addClass('disabled');
    }

    lockField(<%=AspxTools.JsGetElementById(this.sAltCostLckd)%>, 'sAltCost');
    lockField(<%=AspxTools.JsGetElementById(this.sTotCcPbsLocked)%>, 'sTotCcPbs');
    lockField(<%=AspxTools.JsGetElementById(this.sOCredit1Lckd)%>, 'sOCredit1Desc');
    lockField(<%=AspxTools.JsGetElementById(this.sOCredit1Lckd)%>, 'sOCredit1Amt');
    lockField(<%=AspxTools.JsGetElementById(this.sRefPdOffAmt1003Lckd)%>, 'sRefPdOffAmt1003');
    lockField(<%=AspxTools.JsGetElementById(this.sTotEstPp1003Lckd)%>, 'sTotEstPp1003');
    lockField(<%=AspxTools.JsGetElementById(this.sTotEstCc1003Lckd)%>, 'sTotEstCcNoDiscnt1003');
    lockField(<%=AspxTools.JsGetElementById(this.sFfUfmip1003Lckd)%>, 'sFfUfmip1003');
    lockField(<%=AspxTools.JsGetElementById(this.sLDiscnt1003Lckd)%>, 'sLDiscnt1003');
    lockField(<%=AspxTools.JsGetElementById(this.sLAmtLckd)%>, 'sLAmtCalc');
    lockField(<%=AspxTools.JsGetElementById(this.sTransNetCashLckd)%>, 'sTransNetCash');
}

function p3_init() {
	if (typeof(ContactFieldMapper_OnInit) == 'function') ContactFieldMapper_OnInit(m_Loan1003pg3_CFM);
	if (null == oRolodex)
		oRolodex = new cRolodex();

	setForeignNational();
    <%= AspxTools.JsGetElementById(aIntrvwrMethodT) %>.disabled = !<%= AspxTools.JsGetElementById(aIntrvwrMethodTLckd) %>.checked;

    lock1003Fields();
    TPOStyleUnification.Components();
    setTimeout(function(){ $("#tabs-3").removeClass("hidden"); });
}
</script>

<accordion><accordion-header>VI. ASSETS AND LIABILITIES (cont.)</accordion-header><accordion-content>
    <div class="box-section-border-red" id="box2-section6">
        <header class="header header-border header-border-app-info">Schedule of Real Estate Owned</header>
        <div class="schedule-of-real-estate-owned-pg3">
            <div class="label-totals">Totals</div>
            <div class="flex-alignment-totals">
                <div class="flex-space-between none-margin-bottom">
                    <div class="flex-alignment flex-alignment-w-pg1-section-2">
                        <div class="col-xs-1-5">
                            <div>
                                <label>Market Value</label>
                                <ml:MoneyTextBox ID="aReTotVal" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
                            </div>
                        </div>
                        <div class="col-xs-1-5"><div><label>Amt of Mort.</label>
                        <ml:MoneyTextBox ID="aReTotMAmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
                        </div></div>
                        <div class="col-xs-1-5"><div><label>Gross Rental Income</label>
                        <ml:MoneyTextBox ID="aReTotGrossRentI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
                        </div></div>
                        <div class="col-xs-1-5"><div><label>Mort. Payments</label>
                        <ml:MoneyTextBox ID="aReTotMPmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
                        </div></div>
                        <div class="col-xs-1-5"><div><label>Ins/Tax/Misc</label>
                        <ml:MoneyTextBox ID="aReTotHExp" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
                        </div></div>
                    </div>
                    <div class="w-128"><div><label>Net Rental Income </label>
                    <ml:MoneyTextBox ID="aReTotNetRentI" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
                    </div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-section-border-red" id="box3-section6">
        <header class="padding-bottom-p16">
            List any additional names under which credit has previously been received and indicate
            appropriate creditor name(s) and account number(s):</header>
        <div class="schedule-of-real-estate-owned-pg3">
            <div class="label-totals"></div>
            <div class="flex-alignment-totals">
            <div class="flex-space-between none-margin-bottom">
                <div class="flex-alignment flex-alignment-w-pg1-section-2">
                    <div class="col-xs-2-5">
                        <div class="flex-alignment-sub">
                            <div class="col-xs-12">
                                <div>
                                    <div><label>Alternate name</label><input type="text" id="aAltNm1" runat="server" name="aAltNm1" class="input-w270" /></div>
                                    <div><input type="text" id="aAltNm2" runat="server" name="aAltNm2" class="input-w270" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2-5">
                        <div class="flex-alignment-sub">
                            <div class="col-xs-12">
                                <div>
                                    <div><label>Creditor name</label><input type="text" id="aAltNm1CreditorNm" runat="server" name="aAltNm1CreditorNm" class="input-w270" /></div>
                                    <div><input type="text" id="aAltNm2CreditorNm" runat="server" name="aAltNm2CreditorNm" class="input-w270" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-1-5">
                        <div class="flex-alignment-sub">
                            <div class="col-xs-12">
                                <div>
                                    <div><label>Account number</label><input type="text" id="aAltNm1AccNum" runat="server" name="aAltNm1AccNum" class="input-w270" /></div>
                                    <div><input type="text" id="aAltNm2AccNum" runat="server" name="aAltNm2AccNum" class="input-w270" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-128">
                    <div></div>
                </div>
                </div>
            </div>
        </div>
    </div>
</accordion-content></accordion>

<accordion><accordion-header>VII. DETAILS OF TRANSACTION</accordion-header><accordion-content>
    <div class="box-section-border-red" id="box-section7">
        <div class="flex-space-between flex-space-between-app-info-pg3 details-of-transaction-section">
            <div class="left">
                <table class="details-transaction-app-info-pg3" border="0" cellspacing="0" cellpadding="0">
                    <tr><td>a. Purchase price</td>
                        <td><ml:MoneyTextBox Update="true" ID="sPurchPrice" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"/></td>
                    </tr>
                    <tr><td>b. Alterations, improvements, repairs</td>
                        <td>
                            <ml:MoneyTextBox ID="sAltCost" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sAltCostLckd" update="true"/></label>
                        </td>
                    </tr>
                    <tr><td>c. Land (if acquired separately)</td>
                        <td><ml:MoneyTextBox ID="sLandCost" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" /></td>
                    </tr>
                    <tr><td>d. Refi (incl. debts to be paid off)</td>
                        <td>
                            <ml:MoneyTextBox ID="sRefPdOffAmt1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" data-lockable="true" Update="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sRefPdOffAmt1003Lckd" update="true"/></label>
                        </td>
                    </tr>
                    <tr><td>e. Estimated prepaid items</td>
                        <td>
                            <ml:MoneyTextBox ID="sTotEstPp1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sTotEstPp1003Lckd" update="true"/></label>
                        </td>
                    </tr>
                    <tr runat="server" id="PrepaidsAndReservesRow">
                        <td class="indented">Prepaids and reserves</td>
                        <td><ml:MoneyTextBox readonly ID="sTotEstPp" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" /></td>
                    </tr>
                    <tr runat="server" id="ProrationsPaidByBorrowerRow">
                        <td class="indented">Prorations paid by borrower</td>
                        <td><ml:MoneyTextBox readonly ID="sTotalBorrowerPaidProrations" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true"/></td>
                    </tr>
                    <tr><td>f. Estimated closing costs</td>
                        <td>
                            <ml:MoneyTextBox ID="sTotEstCcNoDiscnt1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sTotEstCc1003Lckd" update="true"/></label>
                        </td>
                    </tr>
                    <tr runat="server" id="ThisLoanClosingCostsRow">
                        <td class="indented">This loan closing costs</td>
                        <td><ml:MoneyTextBox readonly ID="sTotEstCcNoDiscnt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" /></td>
                    </tr>
                    <tr runat="server" id="OtherFinancingClosingCosts2Row">
                        <td class="indented">Other financing closing costs</td>
                        <td><ml:MoneyTextBox ID="sONewFinCc2" name="sONewFinCc" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" /></td>
                    </tr>
                    <tr><td>g. PMI, MIP, Funding Fee</td>
                        <td>
                            <ml:MoneyTextBox ID="sFfUfmip1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sFfUfmip1003Lckd" update="true"/></label>
                        </td>
                    </tr>
                    <tr><td>h. Discount (if Borrower will pay)</td>
                        <td>
                            <ml:MoneyTextBox ID="sLDiscnt1003" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sLDiscnt1003Lckd" update="true"/></label>
                        </td>
                    </tr>
                    <tr><td><b>i. Total costs (add items a to h)</b></td>
                        <td><ml:MoneyTextBox ID="sTotTransC" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/></td>
                    </tr>
                </table>
            </div>
            <div class="right">
                <table class="details-transaction-app-info-pg3" border="0" cellspacing="0" cellpadding="0">
                    <tr><td>j. Subordinate financing</td>
                        <td><ml:MoneyTextBox ID="sONewFinBal" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/></td>
                    </tr>
                    <tr><td>k. Borrower's closing costs paid by Seller</td>
                        <td>
                            <ml:MoneyTextBox ID="sTotCcPbs" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sTotCcPbsLocked" update="true"/></label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table class="lender-credit-app-info lender-credit-app-info-left" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>l.</td>
                                    <td>
                                        <ml:ComboBox runat="server" ID="sOCredit1Desc" CssClass="select-w280" data-lockable="true"/>
                                        <label class="lock-checkbox"><input type="checkbox" runat="server" id="sOCredit1Lckd" update="true"/></label>
                                    </td>
                                </tr>
                                <tr><td></td><td>
                                    <ml:ComboBox runat="server" ID="sOCredit2Desc" CssClass="select-w280"/>
                                </td></tr>
                                <tr><td></td><td>
                                    <ml:ComboBox runat="server" ID="sOCredit3Desc" CssClass="select-w280"/>
                                </td></tr>
                                <tr><td></td><td>
                                    <ml:ComboBox runat="server" ID="sOCredit4Desc" CssClass="select-w280"/>
                                </td></tr>
                                <tr><td></td><td>Lender credit</td></tr>
                                <tr runat="server" id="OtherFinancingClosingCostsLabelRow"><td></td><td>
                                    Other financing closing costs
                                </td></tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table class="lender-credit-app-info" width="180" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="intermediateTD"></td>
                                    <td>
                                    <ml:MoneyTextBox readonly  Update="true" ID="sOCredit1Amt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" />
                                </td></tr>
                                <tr>
                                    <td class="intermediateTD"></td>
                                    <td>
                                    <ml:MoneyTextBox ID="sOCredit2Amt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                                </td></tr>
                                <tr>
                                    <td class="intermediateTD"></td>
                                    <td>
                                    <ml:MoneyTextBox ID="sOCredit3Amt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                                </td></tr>
                                <tr>
                                    <td class="intermediateTD"></td>
                                    <td>
                                    <ml:MoneyTextBox ID="sOCredit4Amt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                                </td></tr>
                                <tr>
                                    <td class="intermediateTD"></td>
                                    <td>
                                    <ml:MoneyTextBox Update="true" ID="sOCredit5Amt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/>
                                </td></tr>
                                <tr runat="server" id="OtherFinancingClosingCostsAmountRow">
                                    <td class="intermediateTD"> - </td>
                                    <td>
                                        <ml:MoneyTextBox ID="sONewFinCc" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td>m. Loan amount (exclude PMI, MIP, FF financed)</td>
                        <td>
                            <ml:MoneyTextBox ID="sLAmtCalc" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sLAmtLckd" update="true"/></label>
                        </td>
                    </tr>
                    <tr><td>n. PMI, MIP, Funding Fee financed</td>
                        <td><ml:MoneyTextBox  Update="true" ID="sFfUfmipFinanced1" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True" /></td>
                    </tr>
                    <tr><td>o. Loan amount (add m & n)</td>
                        <td><ml:MoneyTextBox  Update="true" ID="sFinalLAmt" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00" ReadOnly="True"/></td>
                    </tr>
                    <tr><td>p. Cash from / to Borr (subtract j, k, l & o from i)</td>
                        <td>
                            <ml:MoneyTextBox ID="sTransNetCash" runat="server" preset="money" CssClass="input-w100 input-textright" value="$0.00"  Update="true" data-lockable="true" />
                            <label class="lock-checkbox"><input type="checkbox" runat="server" id="sTransNetCashLckd" update="true"/></label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</accordion-content></accordion>

<accordion><accordion-header>VIII. DECLARATIONS</accordion-header><accordion-content>
    <div class="box-section-border-red" id="box-section8">
        <table class="details-transaction-app-info-pg3 declarations" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    If you answer "Yes" to any questions a through i, please provide the explanation below
                </td>
                <td width="60">
                    Borr
                </td>
                <td width="60">
                    Co-borr
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    Y/N
                </td>
                <td>
                    Y/N
                </td>
            </tr>
            <tr>
                <td>
                    a. Are there any outstanding judgments against you?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecJudgment" name="aBDecJudgment" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecJudgment" name="aCDecJudgment" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    b. Have you been declared bankrupt within the past 7 years?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecBankrupt" name="aBDecBankrupt" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecBankrupt" name="aCDecBankrupt" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    c. Have you had property foreclosed upon or given title or deed in lieu thereof
                    in the last 7 years?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecForeclosure" name="aBDecForeclosure" class="input-w40"
                           onblur=" unhighlightRow(this); " onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecForeclosure" name="aCDecForeclosure" class="input-w40"
                           onblur=" unhighlightRow(this); " onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    d. Are you a party to a lawsuit?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecLawsuit" name="aBDecLawsuit" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecLawsuit" name="aCDecLawsuit" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    e. Have you directly or indirectly been obligated on any loan which resulted in
                    foreclosure, transfer of title in lieu of foreclosure, or judgment? (This would
                    include such loans as home mortgage loans, SBA loans, home improvement loans, educational
                    loans, manufactured (mobile) home loans, any mortgage, financial obligation, bond,
                    or loan guarantee. If "Yes", provide details, including if any, and reasons for
                    the action.)
                </td>
                <td valign="top">
                    <input type="text" runat="server" id="aBDecObligated" name="aBDecObligated" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td valign="top">
                    <input type="text" runat="server" id="aCDecObligated" name="aCDecObligated" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    f. Are you presently delinquent or in default on any Federal debt or any other loan,
                    mortgage, financial obligation bond, or loan guarantee? If "Yes", give details as
                    described in the preceding question.
                </td>
                <td valign="top">
                    <input type="text" runat="server" id="aBDecDelinquent" name="aBDecDelinquent" class="input-w40"
                           onblur=" unhighlightRow(this); " onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td valign="top">
                    <input type="text" runat="server" id="aCDecDelinquent" name="aCDecDelinquent" class="input-w40"
                           onblur=" unhighlightRow(this); " onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    g. Are you obligated to pay alimony, child support, or separate maintenance?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecAlimony" name="aBDecAlimony" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecAlimony" name="aCDecAlimony" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    h. Is any part of the down payment borrowed?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecBorrowing" name="aBDecBorrowing" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecBorrowing" name="aCDecBorrowing" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    i. Are you a co-maker or endorser on a note?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecEndorser" name="aBDecEndorser" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecEndorser" name="aCDecEndorser" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    j. Are you a U.S. citizen?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecCitizen" name="aBDecCitizen" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecCitizen" name="aCDecCitizen" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    k. Are you a permanent resident alien?
                </td>
                <td>
                    <input type="text" id="aBDecResidency" runat="server" name="aBDecResidency" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecResidency" name="aCDecResidency" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr style='display:none;'>
                <td>
                    - Are you a non-resident alien (foreign national)? (For PriceMyLoan)
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecForeignNational" name="aBDecForeignNational" class="input-w40"
                           onblur=" unhighlightRow(this); " onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    l. Do you intend to occupy the property as your primary residence? If "Yes," complete
                    question m below
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecOcc" name="aBDecOcc" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecOcc" name="aCDecOcc" class="input-w40" onblur=" unhighlightRow(this); "
                           onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    m. Have you had an ownership interest in a property in the last three years?
                </td>
                <td>
                    <input type="text" runat="server" id="aBDecPastOwnership" name="aBDecPastOwnership" class="input-w40"
                           onblur=" unhighlightRow(this); " onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
                <td>
                    <input type="text" runat="server" id="aCDecPastOwnership" name="aCDecPastOwnership" class="input-w40"
                           onblur=" unhighlightRow(this); " onkeyup=" validateYesNo(this); " onfocus=" highlightRow(this); " />
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="15">
                                        </td>
                                        <td>
                                            (1) Type of property: (PR - Principal Residence, SH - Second Home, IP - Investment
                                            Property)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                <asp:DropDownList runat="server" class="select-w60" id="aBDecPastOwnedPropT" name="aBDecPastOwnedPropT" onblur=" unhighlightRow(this); " onfocus=" highlightRow(this); "></asp:DropDownList>
                </td>
                <td>
                <asp:DropDownList runat="server" class="select-w60" id="aCDecPastOwnedPropT" name="aCDecPastOwnedPropT" onblur=" unhighlightRow(this); " onfocus=" highlightRow(this); "></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="15">
                                        </td>
                                        <td>
                                            (2) Title held: (S - Solely by Yourself, SP - Jointly w/ Spouse, O - Jointly w/
                                            Another Person)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                <asp:DropDownList runat="server" class="select-w60" id="aBDecPastOwnedPropTitleT" name="aBDecPastOwnedPropTitleT" onblur=" unhighlightRow(this); " onfocus=" highlightRow(this); "></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" class="select-w60" id="aCDecPastOwnedPropTitleT" name="aCDecPastOwnedPropTitleT" onblur=" unhighlightRow(this); " onfocus=" highlightRow(this); "></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <p class="pull-right"><button id="explnBtn" type="button" onclick="addExplanations();">Explanations</button></p>
                </td>
            </tr>
        </table>
    </div>
</accordion-content></accordion>

<accordion><accordion-header>X. INFORMATION FOR GOVERNMENT MONITORING PURPOSES</accordion-header><accordion-content>
    <HRED:HmdaRaceEthnicityData runat="server" ID="HREData" />
    <fieldset class="box-section-border-red" id="box2-section10">
        <header class="header header-border header-border-app-info">To be Completed by Loan Originator</header>
        <div class="table">
            <div>
                <div>
                    <label>This application was taken by</label>
                    <asp:DropDownList  runat="server"  ID="aIntrvwrMethodT" CssClass="select-w170"></asp:DropDownList>
                    <label class="lock-checkbox"><input type="checkbox" runat="server" id="aIntrvwrMethodTLckd" update="true"/></label>
                </div>
                <div>
                    <label>
                        Interview Date
                    </label>
                    <ml:DateTextBox runat="server" preset="date" CssClass="input-w80" ID="a1003InterviewD" IsDisplayCalendarHelper="false"/>
                    <label class="lock-checkbox" runat="server" id="a1003InterviewDLckdLabel"><input type="checkbox" runat="server" ID="a1003InterviewDLckd" onchange="refreshCalculation();" /></label>
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div>
                    <label>
                        Loan Originator's Name
                    </label>
                    <input type="text" id="LoanOfficerName" name="LoanOfficerName" class="input-w270" runat="server"/>
                </div>
                <div class="loan-originator-app-info-pg3">
                    <label>
                        Loan Originator NMLS ID
                    </label>
                    <input type="text" id="LoanOfficerLoanOriginatorIdentifier" name="LoanOfficerLoanOriginatorIdentifier"
                           class="input-w120" runat="server"/>
                </div>
                <div>
                    <label>
                        Loan Originator's License Number
                    </label>
                    <input type="text" id="LoanOfficerLicenseNumber" name="LoanOfficerLicenseNumber"
                           class="input-w100" runat="server"/>
                </div>
                <div>
                    <label>
                        Loan Originator's Phone
                    </label>
                    <input  preset="phone" type="text" id="LoanOfficerPhone" name="LoanOfficerPhone" class="input-w100" runat="server">
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div>
                    <label>
                        Loan Origination Company's Name
                    </label>
                    <input type="text" id="BrokerName" name="BrokerName" class="input-w270" runat="server"/>
                </div>
                <div>
                    <label>
                        Loan Origination Company NMLS ID
                    </label>
                    <input type="text" id="LoanOfficerCompanyLoanOriginatorIdentifier" name="LoanOfficerCompanyLoanOriginatorIdentifier"
                           class="input-w120" runat="server"/>
                </div>
                <div>
                    <label>Loan Origination Company's License Number</label>
                    <input type="text" id="BrokerLicenseNumber" name="BrokerLicenseNumber" class="input-w100" runat="server"/>
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div>
                   <label>Loan Origination Company's Address</label>
                   <div class="table">
                        <div>
                            <div>
                                <label>Street</label><input type="text" id="BrokerStreetAddr" name="BrokerStreetAddr" class="input-w420" runat="server"/>
                            </div>
                            <div>   
                                <label>City</label><input type="text" id="BrokerCity" name="BrokerCity" class="input-w200" runat="server"/>
                            </div>
                            <div>
                                <label>State</label><ml:StateDropDownList ID="BrokerState" runat="server"  runat="server"/>
                            </div>
                            <div>
                                <label>ZIPCode</label><ml:ZipcodeTextBox ID="BrokerZip" runat="server" CssClass="input-w60" runat="server"/>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>
        <div class="table table-app-info-pg2">
            <div>
                <div>
                    <label>
                        Loan Origination Company's Phone
                    </label>
                    <input preset="phone" type="text" id="BrokerPhone" name="BrokerPhone" class="input-w100" runat="server"/>
                </div>
                <div>
                    <label>Fax</label>
                    <input preset="phone" type="text" id="BrokerFax" name="BrokerFax" class="input-w100" runat="server"/>
                </div>
            </div>
        </div>
    </fieldset>
</accordion-content></accordion>
