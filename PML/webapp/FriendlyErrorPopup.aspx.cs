﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PriceMyLoan.webapp
{
    public partial class FriendlyErrorPopup : PriceMyLoan.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
        }
    }
}
