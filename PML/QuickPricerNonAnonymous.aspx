﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuickPricerNonAnonymous.aspx.cs" Inherits="PriceMyLoan.QuickPricerNonAnonymous" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="PriceMyLoan.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register src="main/QuickPricer.ascx" tagname="QuickPricer" tagprefix="uc1" %>
<%@ Register src="main/QuickPricerResult.ascx" tagname="QuickPricerResult" tagprefix="uc1" %>
<html>
<head runat="server">
    <title>Quick Pricer</title>
    <style type="text/css">
      label,
      input,
      select {margin: 0 0 .75em;}
    </style>    
</head>
<body style="margin:0px">

<script type="text/javascript">
  function f_logout() {
      self.location = <%= AspxTools.JsString(ResolveUrl(PriceMyLoanConstants.LogoutUrl)) %>;
  }
  function f_goToPipeline() {
		  self.location = <%= AspxTools.JsString(ResolveUrl("~/Main/Pipeline.aspx")) %>;
  }
  
  <% if (!PriceMyLoanConstants.IsEmbeddedPML) { %>
  function f_toggleCreate( bShow )
  {
    <%= AspxTools.JQuery(CreateLoan_BorrFirstNameValidator) %>.prop('enabled', bShow);
    <%= AspxTools.JQuery(CreateLoan_BorrLastNameValidator) %>.prop('enabled', bShow);
    <%= AspxTools.JQuery(CreateLoan_BorrSsnValidator) %>.prop('enabled', bShow);

    $j('#QuickPricerTable').toggle(bShow == false);
    $j('#CreateLoanTable').toggle(bShow);

    f_onValidateRequiredFields();
  }
  
  function f_createLoanFromBorrower()
  {
  if (!f_onValidateRequiredFields())
    return;
   var args = f_getAllFormValues(); 
    var result = gService.main.call("ConvertToLoanFromBorrower", args);
    if (!result.error) 
    {
        if ( result.value['DuplicatesExist'] == '1')
        {
            var key = result.value['DupeKey'];
            if(parent != null)
            {
                parent.location = <%= AspxTools.JsString(VirtualRoot) %> + '/main/pipeline.aspx?eventid=' + key;
            }
            else
            {
                location.href = <%= AspxTools.JsString(VirtualRoot) %> + '/main/pipeline.aspx?eventid=' + key;
            }
        }
        else
        {
            sLId = result.value["sLId"];
            if(parent != null)
            {
                parent.location = <%= AspxTools.JsString(PmlUrl) %> +'?loanid=' + sLId + '&source=PML';
            }
            else
            {
                location.href = <%= AspxTools.JsString(PmlUrl) %> + '?loanid=' + sLId + '&source=PML';
            }
       }
    }
    else
    {
        alert('System Error. Please try again later.');
    }
  }
<% } %>

jQuery(function($){
    /*<%-- See OPM 85723 - if we don't double-set the checkboxes and radiobuttons, 
    the print preview is messed up due to a bug in IE 8 Standards Mode. --%>*/
    $('input[type="checkbox"]').click(function(){
        if($(this).is(':checked'))
        {
            $(this).attr('checked', 'checked');
        }
        else
        {
            $(this).removeAttr('checked');
        }
    });
    
    $('input[type="radio"]').click(function(){
        var name = $(this).attr('name');
        var group = $('input[name="' + name + '"]');
        group.attr('checked', false);
        $(this).attr('checked', true);
    });
});
</script>
    <form id="form1" runat="server">
    <div>
      <table width="100%" cellspacing="0" cellpadding="5" runat="server" id="m_topLinkPanel" enableviewstate="false">
        <tr class="TopHeaderBackgroundColor">
          <td align="left"><input type="button" class="ButtonStyle" value="Pipeline" onclick="f_goToPipeline();" runat="server"/></td>
          <td align="right"><input type="button" class="ButtonStyle" value="LOG OFF" onclick="f_logout();" runat="server"/></td>
        </tr>
      </table>
      <table id="QuickPricerTable">
        <tr>
          <td valign="top" align="left">
            <uc1:QuickPricer ID="QuickPricer1" runat="server" />
          </td>
          <td valign="top" align="left">
            <uc1:QuickPricerResult ID="QuickPricerResult1" runat="server" />
          </td>
        </tr>
      </table>
      <table id="CreateLoanTable" style="display: none" runat="server">
        <tr>
          <td colspan="2" class="Subheader">Applicant Information</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="FieldLabel">Applicant First Name</td>
          <td>
            <asp:TextBox ID="CreateLoan_BorrFirstName" runat="server" />
            <asp:RequiredFieldValidator ID="CreateLoan_BorrFirstNameValidator" runat="server" ControlToValidate="CreateLoan_BorrFirstName" ForeColor="" CssClass="RequireValidatorMessage" Enabled="false"></asp:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel">Applicant Last Name</td>
          <td>
            <asp:TextBox ID="CreateLoan_BorrLastName" runat="server" />
            <asp:RequiredFieldValidator ID="CreateLoan_BorrLastNameValidator" runat="server" ControlToValidate="CreateLoan_BorrLastName" ForeColor="" CssClass="RequireValidatorMessage" Enabled="false"></asp:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel">Applicant SSN</td>
          <td>
            <ml:SSNTextBox ID="CreateLoan_BorrSsn" runat="server" Width="130px" preset="ssn" onkeyup="f_onValidateRequiredFields();" />
            <asp:RequiredFieldValidator ID="CreateLoan_BorrSsnValidator" runat="server" ControlToValidate="CreateLoan_BorrSsn" ForeColor="" CssClass="RequireValidatorMessage" Enabled="false"></asp:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td colspan="2"><input class="ButtonStyle" onclick="f_toggleCreate(false);" type="button" value=" Cancel " nohighlight> <input class="ButtonStyle" onclick="f_createLoanFromBorrower();" type="button" value=" Next " nohighlight> </td>
        </tr>
      </table>

    </div>
    </form>
</body>
</html>
