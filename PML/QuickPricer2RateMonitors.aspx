﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="QuickPricer2RateMonitors.aspx.cs" Inherits="PriceMyLoan.QuickPricer2RateMonitors" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        div.Content
        {
            width: 80%;
        }
        .AlignLeft
        {
            text-align:left;
        }
        .AlignCenter
        {
            text-align:center;
        }
        table.rateMonitorSets
        {
            text-decoration: none;
            border-spacing: 0px;
            border-collapse: collapse;
            width: 100%;
            border-bottom: solid 1px black;
            text-align:center;
        }
        table.rateMonitorSets > thead > tr > th
        {
            padding:8px;
            background-color: rgb(204,204,204);
            border: solid 1px black;
            font-size: 14px;
        }
        table.rateMonitorSets > tbody > tr > td > a
        {
            text-decoration: none;
        }
        table.rateMonitorSets > tbody > tr > td
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            padding:8px;
            font-size: 13px;
            color:black;
        }
        table.rateMonitorSets tbody tr.regularRow
        {
            background-color:rgb(204,204,204);
        }
        input.Close
        {
            margin-left: 45%;
            margin-top:5px;
        }
        div.MainDiv
        {
            max-height:150px;
            overflow-y: auto;
            overflow-x: hidden;
            border: solid 1px black;
            margin-left: 5px;
            margin-right: 5px;
        }
        div.TableDescription
        {
            font-size:16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Content">
        <div class="TableDescription">Monitored Scenarios</div>	<br />
        <div class="MainDiv">
            <table class="rateMonitorSets">
                <thead>
                    <tr>
                        <th class="AlignCenter" style="width:50px"></th>
                        <th class="AlignLeft">Scenario Name</th>
                        <th class="AlignCenter" style="width:50px"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    </form>
    
    <script type="text/javascript">
        $(document).on('ready', function() {
            var rateMonitorScenarioIDGetter = function(rateMonitorSet) {
                return rateMonitorSet.LoanID;
            }

            var theTableUpdater = new tableUpdater(
             $('table.rateMonitorSets'),
             $('#rateMonitorSetRowTemplate'),
             rateMonitorSets,
             rateMonitorScenarioIDGetter);

            RateMonitorSet.prototype.config(window.location.href, function(result, args) { theTableUpdater.deleteRow({ LoanID: args.loanID }) }, null);
            var RateMonitorSetsByID = {};
            for (var monitorIndex in rateMonitorSets) {
                var monitor = rateMonitorSets[monitorIndex];
                RateMonitorSetsByID[monitor.LoanID] = new RateMonitorSet(monitor.LoanID, monitor.Name);
            }
            var w = screen.availWidth - 10;
            var h = screen.availHeight - 100;
            var options = 'width=' + w + ',height=' + h + ',left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes';

            $('table.rateMonitorSets').on('click', '.viewLink', function(event) {
                event.preventDefault();
                window.parent.location.href = $(this).data('url');
            });
            $('table.rateMonitorSets').on('click', '.deleteLink', function(event) {
                event.preventDefault();
                var loanId = $(this).closest('tr').attr('id');
                RateMonitorSetsByID[loanId].deleteSet();
            });
        });
    </script>
    <script id="rateMonitorSetRowTemplate" type="text/x-jquery-tmpl">
	<tr id="${LoanID}">
		<td>
			<a href="#" class="viewLink" data-url="${URL}">view</a>
		</td>
		<td class="AlignLeft">
		    ${Name}
		</td>
		<td>
			<a href="#" class="deleteLink">delete</a>
		</td>
	</tr>
    </script>  
</body>
</html>
