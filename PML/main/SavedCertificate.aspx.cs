using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using LendersOffice.Common;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using PriceMyLoan.DataAccess;
using DataAccess;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOffice.ObjLib.Resource;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.main
{

    public partial class SavedCertificate : LendersOffice.Common.BaseXsltPage
    {
		private string m_pmlLenderSiteId;

		private Guid BrokerID 
		{
			get { return ((PriceMyLoan.Security.PriceMyLoanPrincipal) Page.User).BrokerId; }
		}

        protected override string XsltFileLocation 
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.PmlCertificateXslt, this.BrokerID); ; }
        }
        private Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }
		protected override void InitXslt() 
		{

			SqlParameter[] parameters = { new SqlParameter("@BrokerID", BrokerID) };
            Guid brokerPmlSiteId = Guid.Empty;

			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "RetrieveLenderSiteIDByBrokerID", parameters)) 
			{
				if (reader.Read()) 
				{
                    brokerPmlSiteId = (Guid) reader["BrokerPmlSiteID"];
				}
			}
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, LoanID);
		}
		protected override XsltArgumentList XsltParams 
		{
			get 
			{ 
				XsltArgumentList args = new XsltArgumentList();
				args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
				args.AddParam("VirtualRoot", "", Tools.VRoot);
                args.AddParam("Timing", "", "0");
				// 10/19/06 mf - OPM 7610.  We want to mask SSN in PML
				if ( ! PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML )
					args.AddParam( "MaskSsn", "", "True" );

                PriceMyLoanPrincipal principal = Page.User as PriceMyLoanPrincipal;
                // 1/26/2006 dd - OPM 3825 - Allow users with Admin & LockDesk role to see hidden adjustments.
                if (principal.ApplicationType ==  E_ApplicationT.LendersOffice && ( 
                    principal.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms) ||
                    principal.HasRole(E_RoleT.Administrator) || principal.HasRole(E_RoleT.LockDesk))) 
                {
                    args.AddParam("ShowHiddenAdj", "", "True");
                }

                if (LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerDB.HidePmlCertSensitiveFields)
                {
                    args.AddParam("HideSensitiveFields", "", "True");
                }

                // 10/11/07 mf. OPM 18269 - Users with new permission can see hidden stips (conditions)
                if (principal.HasPermission( LendersOffice.Security.Permission.CanViewHiddenInformation ) ||
                principal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
				{
					args.AddParam("ShowHiddenConditions", "", "True");
				}


				return args;

			}
		}

        protected override void GenerateXmlData(XmlWriter writer) 
        {
            try
            {
                CPageData dataLoan = new CSavedCertificateData(LoanID);
                dataLoan.InitLoad();

                string sPmlCertXmlContent = dataLoan.sPmlCertXmlContent.Value;
                if (null == sPmlCertXmlContent || "" == sPmlCertXmlContent)
                {
                    sPmlCertXmlContent = "<NoData/>";
                    writer.WriteRaw(sPmlCertXmlContent);
                }
                else
                {

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sPmlCertXmlContent);
                    doc = UnderwritingResultItem.DedupAdjustmentDescription(doc);

                    doc.WriteTo(writer);
                }

            }

            catch (PageDataLoadDenied)
            {
                writer.WriteRaw("<NoPermission/>");
            }
        }
    }
}
