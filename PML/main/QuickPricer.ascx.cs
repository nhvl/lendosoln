﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using MeridianLink.CommonControls;
using PriceMyLoan.UI;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Collections;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using LendersOffice.Constants;
using System.Linq;
using LqbGrammar.DataTypes;

namespace PriceMyLoan.main
{
    public partial class QuickPricer : BaseUserControl
    {
        protected DropDownList sLPurposeTPe;
        protected TextBox sLAmtCalcPe;
        protected TextBox sHouseValPe;
        protected HtmlSelect sSpCounty;
        protected TextBox sCreditScoreType1;
        protected Button PriceBtn;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected CustomValidator sCreditScoreType1Validator;

        protected E_sLtv80TestResultT sLtv80TestResultT;

        protected Guid m_quickPricerTemplateId = Guid.Empty;

        protected string m_quickPricerNoResultMessage;
        protected bool m_isAutoPmiOrNewPml = false;

        private bool m_isAllowExternalUserToCreateNewLoan = true;

        private PriceMyLoanPrincipal GetPrincipal()
        {
            return ((PriceMyLoan.UI.BasePage)this.Page).CustomPrincipal;
        }

        public bool IsForAnonymousQp
        {
            get;
            set;
        }

        protected bool m_allowGoToPipeline
        {
            get
            {
                PriceMyLoanPrincipal principal = GetPrincipal();
                return principal.IsAllowGoToPipeline;
            }
        }
        protected bool m_userCanCreateLoans
        {
            get 
            {
                PriceMyLoanPrincipal principal = GetPrincipal();
                if (principal == null)
                {
                    return false;
                }

                return principal.HasRole(E_RoleT.LoanOfficer) || 
                    principal.HasRole(E_RoleT.Pml_LoanOfficer) ||
                    principal.HasRole(E_RoleT.Pml_BrokerProcessor) || 
                    principal.HasRole(E_RoleT.Pml_Secondary) || 
                    principal.HasRole(E_RoleT.Pml_PostCloser); 
            }
        }

        protected bool m_allowCreateLoan
        {
            get
            {
                BrokerUserPermissions buP = new BrokerUserPermissions(GetPrincipal().BrokerId , EmployeeID);

                var loanCreationAllowedForPortalMode = false;
                var portalMode = GetPrincipal().PortalMode;
                switch (portalMode)
                {
                    case E_PortalMode.Blank:
                        loanCreationAllowedForPortalMode = true;
                        break;
                    case E_PortalMode.Retail:
                        loanCreationAllowedForPortalMode = buP.HasPermission(Permission.AllowCreatingNewLoanFiles);
                        break;
                    case E_PortalMode.Broker:
                        loanCreationAllowedForPortalMode = buP.HasPermission(Permission.AllowCreatingWholesaleChannelLoans);
                        break;
                    case E_PortalMode.MiniCorrespondent:
                        loanCreationAllowedForPortalMode = buP.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans);
                        break;
                    case E_PortalMode.Correspondent:
                        loanCreationAllowedForPortalMode = buP.HasPermission(Permission.AllowCreatingCorrChannelLoans);
                        break;
                    default:
                        throw new UnhandledEnumException(PriceMyLoanUser.PortalMode);

                }

                var loanCreationAllowedForUser = (m_allowGoToPipeline && m_isAllowExternalUserToCreateNewLoan) ||
                    portalMode == E_PortalMode.Retail ||
                    PriceMyLoanConstants.IsEmbeddedPML;

                return loanCreationAllowedForUser && loanCreationAllowedForPortalMode;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            this.Load += new EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        protected bool m_isAnonymous = false;


        protected void PageInit(object sender, System.EventArgs e)
        {
            sSpZip.SmartZipcode(null, sSpStatePe, sSpCounty);
        }
        private void LoadLockPeriodOptionsAndSetTextboxValue(CPageData pageData)
        {
            // get the rate lock option periods
            // adds them to the drop down
            // and "migrates" the old value to one of the drop down values, giving it the smallest larger value.
            if (CurrentBroker.IsEnabledLockPeriodDropDown)
            {
                string[] options = CurrentBroker.AvailableLockPeriodOptionsCSV.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var numericOptions = from s in options
                                     select UInt32.Parse(s);
                var sortedOptions = from n in numericOptions
                                    orderby n
                                    select n;

                uint sProdRLckdDaysValue = (uint)pageData.sProdRLckdDays;
                sProdRLckdDaysDDL.Items.Clear();
                sProdRLckdDaysDDL.Items.Add(new ListItem("None", "0"));
                int index = 1;
                sProdRLckdDaysDDL.SelectedIndex = 0;
                sProdRLckdDays.Text = "0";
                bool isSelected = (sProdRLckdDaysValue == 0);
                foreach (uint option in sortedOptions)
                {
                    sProdRLckdDaysDDL.Items.Add(new ListItem(option.ToString(), option.ToString()));
                    if (!isSelected && option >= sProdRLckdDaysValue)
                    {
                        sProdRLckdDaysDDL.SelectedIndex = index;
                        sProdRLckdDays.Text = sProdRLckdDaysDDL.SelectedValue;
                        isSelected = true;
                    }
                    index++;
                }
                sProdRLckdDays.CssClass += " hidden";
            }
        }

        private void PageLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AbstractUserPrincipal principal = GetPrincipal();
                m_isAnonymous = this.IsForAnonymousQp; //principal.IsPmlQuickPricerAnonymousLogin;

                Tools.Bind_sSimpleLPurposeT(sLPurposeTPe);
                Tools.Bind_sProdSpT(sProdSpT);
                Tools.Bind_aOccT(sOccTPe);
                Tools.Bind_sProdMIOptionT(sProdMIOptionT);
                Tools.Bind_sProdConvMIOptionT(sProdConvMIOptionT, CurrentBroker.EnabledBPMISinglePremium_Case115836, CurrentBroker.EnableBPMISplitPremium_Case115836 );
                Tools.Bind_sConvSplitMIRT(sConvSplitMIRT);
                SetRequiredValidatorMessage(RequiredFieldValidator2);
                SetRequiredValidatorMessage(sCreditScoreType1Validator);
                SetRequiredValidatorMessage(sProdRLckdDaysValidator);
                SetRequiredValidatorMessage(sLAmtCalcPeValidator);
                SetRequiredValidatorMessage(sProdMIOptionTValidator);
                SetRequiredValidatorMessage(sProdConvMIOptionTValidator);
                SetRequiredValidatorMessage(sApprValPeValidator);
                SetRequiredValidatorMessage(sConvSplitMIRTValidator);

                
                if (null == principal)
                {
                    Response.Redirect("~/SessionExpired.aspx");
                }
                BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
                if (broker.IsAsk3rdPartyUwResultInPml)
                {
                    Tools.Bind_sProd3rdPartyUwResultT(sProd3rdPartyUwResultT);
                }
                sProd3rdPartyUwResultTPlaceHolder.Visible = broker.IsAsk3rdPartyUwResultInPml;
                m_isAllowExternalUserToCreateNewLoan = broker.IsAllowExternalUserToCreateNewLoan && m_userCanCreateLoans;
                m_quickPricerNoResultMessage = broker.QuickPricerNoResultMessage;
                bool showLeadSrcDesc = PriceMyLoanConstants.IsEmbeddedPML && broker.AddLeadSourceDropdownToInternalQuickPricer;
                sLeadSrcDescPlaceHolder.Visible = showLeadSrcDesc;
                if (showLeadSrcDesc)
                {
                    Tools.Bind_sLeadSrcDesc(broker, sLeadSrcDesc);
                }

                m_quickPricerTemplateId = broker.QuickPricerTemplateId;
                if (Guid.Empty == m_quickPricerTemplateId)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Lender does not specify QuickPricer Template");
                }
                CPageData dataLoan = new CQuickPricerLoanData(m_quickPricerTemplateId);
                dataLoan.InitLoad();
                dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

                sLAmtCalcPe.Text = dataLoan.sLAmtCalcPe_rep;
                sLtvRPe.Text = dataLoan.sLtvRPe_rep; //28139 fs 03/12/09
                sHouseValPe.Text = dataLoan.sHouseValPe_rep;
                
                //opm 32479 fs 07/21/09
                sProdRLckdDays.Text = dataLoan.sProdRLckdDays_rep;
                LoadLockPeriodOptionsAndSetTextboxValue(dataLoan);
                sProdImpound.Checked = dataLoan.sProdImpound;
                sDownPmtPcPe.Text = dataLoan.sDownPmtPcPe_rep;
                sEquityPe.Text = dataLoan.sEquityPe_rep;
                sLtvROtherFinPe.Text = dataLoan.sLtvROtherFinPe_rep;
                sProOFinBalPe.Text = dataLoan.sProOFinBalPe_rep;
                sCltvRPe.Text = dataLoan.sCltvRPe_rep;
                sProdIsDuRefiPlus.Checked = dataLoan.sProdIsDuRefiPlus;
                sApprValPe.Text = dataLoan.sApprValPe_rep; //av opm    32746 9/2011

                sLtv80TestResultT = dataLoan.sLtv80TestResultT;

                Tools.SetDropDownListValue(sLPurposeTPe, dataLoan.sLPurposeTPe);
                Tools.SetDropDownListValue(sProdMIOptionT, dataLoan.sProdMIOptionT);
                Tools.SetDropDownListValue(sProdConvMIOptionT, dataLoan.sProdConvMIOptionT);
                Tools.SetDropDownListValue(sConvSplitMIRT, dataLoan.sConvSplitMIRT);

                sSpStatePe.Value = dataLoan.sSpStatePe;
                if ("" != dataLoan.sSpState) {
                    ArrayList list = StateInfo.Instance.GetCountiesFor(dataLoan.sSpStatePe);
                    foreach (string county in list)
                    {
                        sSpCounty.Items.Add(county);
                    }
                }
                sSpCounty.Value = dataLoan.sSpCounty;

                if (dataLoan.sProdSpT == E_sProdSpT.Townhouse) // 7/8/2005 dd - OPM 2270.
                    Tools.SetDropDownListValue(sProdSpT, E_sProdSpT.PUD);
                else
                    Tools.SetDropDownListValue(sProdSpT, dataLoan.sProdSpT);

                Tools.SetDropDownListValue(sOccTPe, dataLoan.sOccTPe);

                m_isAutoPmiOrNewPml = broker.HasEnabledPMI || dataLoan.IsNewPMLEnabled;

                //checkboxes
                SetRequiredValidatorMessage(sProdFilterFinMethValidator);
                SetRequiredValidatorMessage(sProdFilterDueValidator);
                SetRequiredValidatorMessage(sProdFilterTypeValidator);

                sProdIncludeMyCommunityProc.Checked = dataLoan.sProdIncludeMyCommunityProc;
                sProdIncludeHomePossibleProc.Checked = dataLoan.sProdIncludeHomePossibleProc;
                sProdIncludeNormalProc.Checked = dataLoan.sProdIncludeNormalProc;
                sProdIncludeFHATotalProc.Checked = dataLoan.sProdIncludeFHATotalProc;
                sProdIncludeVAProc.Checked = dataLoan.sProdIncludeVAProc;
                sProdIncludeUSDARuralProc.Checked = dataLoan.sProdIncludeUSDARuralProc;
                sProdFilterDue10Yrs.Checked = dataLoan.sProdFilterDue10Yrs;
                sProdFilterDue15Yrs.Checked = dataLoan.sProdFilterDue15Yrs;
                sProdFilterDue20Yrs.Checked = dataLoan.sProdFilterDue20Yrs;
                sProdFilterDue25Yrs.Checked = dataLoan.sProdFilterDue25Yrs;
                sProdFilterDue30Yrs.Checked = dataLoan.sProdFilterDue30Yrs;                
                sProdFilterDueOther.Checked = dataLoan.sProdFilterDueOther;
                sProdFilterFinMethFixed.Checked = dataLoan.sProdFilterFinMethFixed;                
                sProdFilterFinMeth3YrsArm.Checked = dataLoan.sProdFilterFinMeth3YrsArm;
                sProdFilterFinMeth5YrsArm.Checked = dataLoan.sProdFilterFinMeth5YrsArm;
                sProdFilterFinMeth7YrsArm.Checked = dataLoan.sProdFilterFinMeth7YrsArm;
                sProdFilterFinMeth10YrsArm.Checked = dataLoan.sProdFilterFinMeth10YrsArm;
                sProdFilterFinMethOther.Checked = dataLoan.sProdFilterFinMethOther;
                sProdFilterPmtTPI.Checked = dataLoan.sProdFilterPmtTPI;
                sProdFilterPmtTIOnly.Checked = dataLoan.sProdFilterPmtTIOnly;
                //end checkboxes

                Page.ClientScript.RegisterHiddenField("sProdCalcEntryT", dataLoan.sProdCalcEntryT.ToString("D"));

                // 3/28/14 gf - opm 119246, added an estimated credit score to PML default values page
                // for PML 2, and this field should be used in the quickpricer.
                sCreditScoreType1.Text = dataLoan.sCreditScoreEstimatePe_rep;

                sProdRLckdExpiredDLabel.Text = dataLoan.sProdRLckdExpiredDLabel;
                if (broker.IsAsk3rdPartyUwResultInPml)
                {
                    // 3/5/2012 dd - OPM 72667
                    Tools.SetDropDownListValue(sProd3rdPartyUwResultT, dataLoan.sProd3rdPartyUwResultT);
                }

                if (showLeadSrcDesc && !SetLeadSrcFromCookie(principal))
                {
                    Tools.SetDropDownListValue(sLeadSrcDesc, dataLoan.sLeadSrcDesc);
                }
            }
        }

        // opm 185732 - moving this method to LosUtils as I need it for quickpricer 2.0
        //private void BindLeadSources()
        //{
        //    var broker = BrokerDB.RetrieveById(GetPrincipal().BrokerId);

        //    // 5/13/2014 gf - opm 172380, always include a blank option.
        //    sLeadSrcDesc.Items.Add(new ListItem("", ""));

        //    foreach (LeadSource desc in broker.LeadSources)
        //    {
        //        sLeadSrcDesc.Items.Add(new ListItem(desc.Name, desc.Name));
        //    }
        //}

        private string GetLeadSrcCookieId(AbstractUserPrincipal principal)
        {
            return string.Format("QuickPricerLeadSrc-{0}", (principal == null) ? "" : principal.UserId.ToString());
        }

        private bool SetLeadSrcFromCookie(AbstractUserPrincipal principal)
        {
            var cookieId = GetLeadSrcCookieId(principal);
            if (Request.Cookies[cookieId] != null)
            {
                sLeadSrcDesc.SelectedValue = Request.Cookies[cookieId].Value;
                return true;
            }
            return false;
        }
    }
}