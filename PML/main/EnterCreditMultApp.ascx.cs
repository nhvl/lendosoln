﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using DataAccess;
using PriceMyLoan.DataAccess;
using PriceMyLoan.Common;
using CommonLib;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Audit;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.UI.Main
{

    public partial class EnterCreditMultApp : BaseUserControl, ITabUserControl
	{
        private const string MASK_INSTANT_VIEW_PW = "*********";
        private const string MCL_CREDIT_SERVER = "https://credit.meridianlink.com";

        private string m_reportID = "";
        private bool m_hasCreditReport = false;
        protected bool m_displayWarning = false;

        private Hashtable m_craHash = new Hashtable();

        #region Protected Member Variables
        #endregion

        protected bool m_hasBrokerDUInfo = false;
        private string m_brokerDUUserID = "";
        private string m_brokerDUPassword = "";


        private Guid m_currentAppId = Guid.Empty;
        protected int m_nApps = 0;

        //protected string m_creditProtocolHashScript = "";

        //opm 4382 fs 07/23/08

        protected bool m_hasLiabilities = false;
		protected bool m_allowOrderNew = true;
		protected Guid ErrorMessageKey = Guid.Empty; // OPM 20593 

        CPageData dataLoan;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            aBSuffix.UseJqueryScript = true;
            aCSuffix.UseJqueryScript = true;
            string key = RequestHelper.GetSafeQueryString("tabkey");
            if (null != key && "" != key && key.ToUpper() != this.TabID)
            {
                return; // 3/12/2009 dd - This is a hack so that at the result screen, this code block does not execute.
            }
            SetRequiredValidatorMessage(LoginNameValidator);
            SetRequiredValidatorMessage(PasswordValidator);
            SetRequiredValidatorMessage(InstantViewIDValidator);
            SetRequiredValidatorMessage(ReportIDValidator);
            SetRequiredValidatorMessage(aBFirstNmValidator);
            SetRequiredValidatorMessage(aBLastNmValidator);
            SetRequiredValidatorMessage(aBSsnValidator);
            SetRequiredValidatorMessage(aBAddrValidator);
            SetRequiredValidatorMessage(aBCityValidator);
            SetRequiredValidatorMessage(aBZipValidator);
            SetRequiredValidatorMessage(aBStateValidator);
            SetRequiredValidatorMessage(FD_ClientCodeValidator);
            SetRequiredValidatorMessage(FD_OfficeCodeValidator);
            SetRequiredValidatorMessage(CreditProtocolValidator);
            SetRequiredValidatorMessage(DUUserIDValidator);
            SetRequiredValidatorMessage(DUPasswordValidator);
            SetRequiredValidatorMessage(AccountIdentifierValidator);
            SetRequiredValidatorMessage(sProdCrManualBk13RecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManualBk7RecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManualForeclosureRecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManual30MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualRolling60MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualRolling90MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualBk13RecentFileValidator);
            SetRequiredValidatorMessage(sProdCrManualBk7RecentFileValidator);
            SetRequiredValidatorMessage(sProdCrManualForeclosureRecentFileValidator);

            //opm 25872 fs 04/18/09 - New borrower fields validators
            SetRequiredValidatorMessage(aBFirstNmManualValidator);
            SetRequiredValidatorMessage(aBLastNmManualValidator);
            SetRequiredValidatorMessage(aBSsnManualValidator);
            SetRequiredValidatorMessage(aCFirstNmManualValidator);
            SetRequiredValidatorMessage(aCLastNmManualValidator);
            SetRequiredValidatorMessage(aCSsnManualValidator);
            m_appList.Attributes["AlwaysEnable"] = "true";

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
            Tools.BindLateDatesDropDown(sProdCrManual30MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual60MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual90MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual120MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual150MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualNonRolling30MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualRolling60MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualRolling90MortLateCount);

            Tools.BindMonthDropDown(sProdCrManualBk13RecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualBk13RecentSatisfiedMon);
            Tools.BindMonthDropDown(sProdCrManualBk7RecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualBk7RecentSatisfiedMon);
            Tools.BindMonthDropDown(sProdCrManualForeclosureRecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualForeclosureRecentSatisfiedMon);

            Tools.BindYearDropDown(sProdCrManualBk13RecentFileYr);
            Tools.BindYearDropDown(sProdCrManualBk13RecentSatisfiedYr);
            Tools.BindYearDropDown(sProdCrManualBk7RecentFileYr);
            Tools.BindYearDropDown(sProdCrManualBk7RecentSatisfiedYr);
            Tools.BindYearDropDown(sProdCrManualForeclosureRecentFileYr);
            Tools.BindYearDropDown(sProdCrManualForeclosureRecentSatisfiedYr);

            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk13RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk7RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualForeclosureRecentStatusT);

            aBZip.SmartZipcode(aBCity, aBState);

            //opm 27866 fs 04/10/09
            aBSsn.IsMaskEnabled = true;
            aCSsn.IsMaskEnabled = true;
            aBSsnManual.IsMaskEnabled = true;
            aCSsnManual.IsMaskEnabled = true;

            List<Guid> denyCraList = MasterCRAList.RetrieveHiddenCras(this.CurrentBroker, E_ApplicationT.PriceMyLoan);
            // 9/21/2004 dd - Append special credit.meridianlink.com CRA
            m_craHash.Add("MCL_CREDIT_SERVER", new TemporaryCRAInfo("MCL", MCL_CREDIT_SERVER, Guid.Empty, CreditReportProtocol.Mcl, ""));

            CreditProtocol.Items.Add(new ListItem("<-- Select Credit Provider -->", Guid.Empty.ToString()));

            m_nonMclProtocolList = new List<KeyValuePair<Guid, int>>();

            #region Add Lender Mapping CRA
            bool hasLenderMappingCRA = false;
            foreach (var proxy in CreditReportAccountProxy.Helper.ListCreditProxyByBrokerID(this.BrokerID))
            {
                if (denyCraList.Contains(proxy.CrAccProxyId))
                {
                    continue;
                }

                Guid craId = proxy.CraId;
                string craUserName = proxy.CraUserName;
                string craPassword = proxy.CraPassword;
                string craAccId = proxy.CraAccId;
                string vendorName = proxy.CrAccProxyDesc;

                CreditProtocol.Items.Add(new ListItem(vendorName, proxy.CrAccProxyId.ToString()));

                // Add special code for CRA Mapping
                m_nonMclProtocolList.Add(new KeyValuePair<Guid, int>(proxy.CrAccProxyId, (int)CreditReportProtocol.LenderMappingCRA));

                TemporaryCRAInfo craInfo = new TemporaryCRAInfo(vendorName, "", craId, CreditReportProtocol.LenderMappingCRA, ""); //lender mappings have their provider id set by ordercredit
                craInfo.UserName = craUserName;
                craInfo.Password = craPassword;
                craInfo.AccountID = craAccId;
                craInfo.IsEquifaxPulled = proxy.IsEquifaxPulled;
                craInfo.IsExperianPulled = proxy.IsExperianPulled;
                craInfo.IsTransUnionPulled = proxy.IsTransUnionPulled;
                craInfo.IsFannieMaePulled = proxy.IsFannieMaePulled;
                craInfo.CreditMornetPlusUserId = proxy.CreditMornetPlusUserId;
                craInfo.CreditMornetPlusPassword = proxy.CreditMornetPlusPassword;

                m_craHash.Add(proxy.CrAccProxyId.ToString(), craInfo);

                hasLenderMappingCRA = true;
            }

            if (hasLenderMappingCRA)
            {
                CreditProtocol.Items.Add(new ListItem("---------------------------", Guid.Empty.ToString()));
            }

            #endregion

            #region Add Normal Master list of our CRA
            var masterCraList = LendersOffice.CreditReport.MasterCRAList.RetrieveAvailableCras(this.CurrentBroker);
            foreach (LendersOffice.CreditReport.CRA o in masterCraList) 
            {
                if (denyCraList.Contains(o.ID))
                    continue;

                CreditProtocol.Items.Add(new ListItem(o.VendorName, o.ID.ToString()));
                m_craHash.Add(o.ID.ToString(), 
                    new TemporaryCRAInfo(o.VendorName, o.Url, o.ID, o.Protocol, o.ProviderId));
                
                if (o.Protocol!= CreditReportProtocol.Mcl) 
                {
                    // 9/20/2004 dd - Add non-mcl protocol to javascript hash.
                    m_nonMclProtocolList.Add(new KeyValuePair<Guid, int>(o.ID, (int)o.Protocol));
                }

            }


            #endregion

            // 7/22/2005 dd - Set Last Use CRA
            Tools.SetDropDownListValue(CreditProtocol, PriceMyLoanUser.LastUsedCreditProtocolID.ToString());

            CreditAction_ManualCredit.Visible = m_canRunLpeWithoutCreditReport;

        }
        protected List<KeyValuePair<Guid, int>> m_nonMclProtocolList = new List<KeyValuePair<Guid, int>>();
        private bool m_canRunLpeWithoutCreditReport 
        {
            get { return PriceMyLoanUser.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport); }
        }

        private void DetermineBrokerDULoginInfo( BrokerDB broker ) 
        {
            m_brokerDUUserID = broker.CreditMornetPlusUserID;
            m_brokerDUPassword = broker.CreditMornetPlusPassword.Value;
            m_hasBrokerDUInfo = m_brokerDUUserID != "" && m_brokerDUPassword != "";
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{

		}

        protected bool HasCreditReport 
        {
            get { return m_hasCreditReport; }
        }
		
        private void RetrieveCreditReport(Guid applicationID) 
        {

            SqlParameter[] parameters = { new SqlParameter("@ApplicationID", applicationID) };

            string lastCra = null;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "RetrieveCreditReport", parameters)) 
            {
                if (reader.Read()) 
                {
                    m_reportID = (string) reader["ExternalFileID"];

                    if (reader["CrAccProxyId"] is DBNull)
                        lastCra = reader["ComId"].ToString();
                    else 
                    {
                        m_reportID = ""; // OPM 2147 - Hide credit reference number for credit pull from proxy account.
                        lastCra = reader["CrAccProxyId"].ToString();
                    }

                    // 10/24/2005 dd - if LastCra == POINT Dummy then set lastCra = null
                    if (new Guid(lastCra) == ConstAppDavid.DummyPointServiceCompany)
                        lastCra = null;

                }
            }
            
            if (null != lastCra)
                Tools.SetDropDownListValue(CreditProtocol, lastCra);
        }
        public string TabID 
        { 
            get { return "CREDIT"; }
        }

        public bool HasEdocs
        {
            get
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                SqlParameter param = new SqlParameter("@sLId", LoanID);
                parameters.Add(param);
                List<Guid> appIds = new List<Guid>();

                using (DbDataReader sr = StoredProcedureHelper.ExecuteReader(BrokerID, "ListAppIdsWithEdocsByLoanId", parameters))
                {
                    while (sr.Read())
                    {
                        appIds.Add((Guid)sr["aAppId"]);
                    }
                }

                Guid AppId = RequestHelper.GetGuid("appId", Guid.Empty);
                if (AppId == Guid.Empty)
                {
                    AppId = dataLoan.GetAppData(0).aAppId;
                }

                return appIds.Contains(AppId);
            }
        }

        public void LoadData()
        {
            DetermineBrokerDULoginInfo(this.CurrentBroker);
            m_allowOrderNew = this.CurrentBroker.AllowPmlUserOrderNewCreditReport; // 2/16/07 OPM 9780.

            // OPM 20593 Check if there is an error message to report.
            Guid errorKey = RequestHelper.GetGuid("errorMsg", Guid.Empty);
            if (errorKey != Guid.Empty)
            {
                string errorMessage = AutoExpiredTextCache.GetFromCache(errorKey);
                if (errorMessage != null)
                {
                    ErrorMessage.Text = errorMessage;
                }
            }

            dataLoan = new CEnterCreditData(LoanID);
            dataLoan.InitLoad();
            // 11/18/2005 dd - Display data change warning if user visit this step before.
            // 12/7/2006 dd - Remove the warning only in step1.
            //m_displayWarning = dataLoan.sPml1stVisitCreditStepD_rep != "" && !Page.IsPostBack;

            if (dataLoan.sPml1stVisitCreditStepD_rep == "")
            {
                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitCreditStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();

                dataLoan = new CEnterCreditData(LoanID);
                dataLoan.InitLoad();
            }

            sFileVersion = dataLoan.sFileVersion;
            //Load proper dataApp
            CAppData dataApp = null;

            Guid AppId = RequestHelper.GetGuid("appId", Guid.Empty);
            if (AppId != Guid.Empty)
            {
                dataApp = dataLoan.GetAppData(AppId);
            }
            if (dataApp == null)
                dataApp = dataLoan.GetAppData(0);

            m_currentAppId = dataApp.aAppId;
            Page.ClientScript.RegisterHiddenField("currentAppId", dataApp.aAppId.ToString());

            //Populate dropdown box accordingly
            m_appList.Items.Clear();
            List<string> invalidApps = new List<string>();
            int appIndex = 0;
            m_nApps = dataLoan.nApps;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dApp = dataLoan.GetAppData(i);
                
                string name = (dApp.aAppFullNm.Length == 0) ? "Application " + (i + 1) + " of " + dataLoan.nApps : dApp.aAppFullNm;
                ListItem li = new ListItem(name, dApp.aAppId.ToString());
                
                if (dataApp.aAppId == dApp.aAppId)
                {
                    li.Selected = true;
                    appIndex = i+1;
                }
                m_appList.Items.Add(li);

                if (!isValidApp(dApp))
                {
                    invalidApps.Add("'" + dApp.aAppId + "'");
                }
            }
            
            if (invalidApps.Count != 0)
            {
                Page.ClientScript.RegisterArrayDeclaration("invalidApps", String.Join(",", invalidApps.ToArray()));
            }

            m_appTitle.Text = "Application #" + appIndex + ((dataLoan.nApps == 1) ? "" : (" of " + dataLoan.nApps));

            m_hasLiabilities = dataApp.aLiaCollection.CountRegular != 0;
            m_hasCreditReport = dataApp.aIsCreditReportOnFile;

            RetrieveCreditReport(dataApp.aAppId);
            ReportID.Text = m_reportID;
            if (m_hasCreditReport)
            {
                CreditAction_UseCreditOnFile.Checked = true;

                InstantViewID.Attributes.Add("Value", MASK_INSTANT_VIEW_PW);
                //CreditAction.Items.Insert(0, new ListItem("Use Credit Report on File", "-1"));
            }
            else
            {
                // 1/30/07 mf OPM 9780.  We now want to cancel change from 4675 and hide the Use Credit Report on file instead of disable it.
                CreditAction_UseCreditOnFile.Visible = false;
                CreditAction_UseCreditOnFile.Checked = false;
                CreditAction_Reissue.Checked = true; // 4/26/2006 dd - Always default to re-issue when there is no credit on file.
            }

            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSsn.Text = dataApp.aBSsn;
            aBExperianScorePe.Text = dataApp.aBExperianScorePe_rep;
            aBTransUnionScorePe.Text = dataApp.aBTransUnionScorePe_rep;
            aBEquifaxScorePe.Text = dataApp.aBEquifaxScorePe_rep;
            aBAddr.Text = dataApp.aBAddr;
            aBCity.Text = dataApp.aBCity;
            aBState.Value = dataApp.aBState;
            aBZip.Text = dataApp.aBZip;
            aBHasSpouse.Checked = dataApp.aBHasSpouse; //opm 4382 fs 07/23/08
            aBDob.Text = dataApp.aBDob_rep;

            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSsn.Text = dataApp.aCSsn;
            aCExperianScorePe.Text = dataApp.aCExperianScorePe_rep;
            aCTransUnionScorePe.Text = dataApp.aCTransUnionScorePe_rep;
            aCEquifaxScorePe.Text = dataApp.aCEquifaxScorePe_rep;
            aCDob.Text = dataApp.aCDob_rep;            
            sTransmOMonPmtPe.Text = dataApp.aTransmOMonPmtPe_rep;
            
            sProdCrManualBk13Has.Checked = dataApp.aProdCrManualBk13Has;
            sProdCrManualBk7Has.Checked = dataApp.aProdCrManualBk7Has;
            sProdCrManualForeclosureHas.Checked = dataApp.aProdCrManualForeclosureHas;

            //opm 25872 fs 04/18/09 - New manual borrower fields
            aBFirstNmManual.Text = dataApp.aBFirstNm;
            aBLastNmManual.Text = dataApp.aBLastNm;
            aBSsnManual.Text = dataApp.aBSsn;

            aCFirstNmManual.Text = dataApp.aCFirstNm;
            aCLastNmManual.Text = dataApp.aCLastNm;
            aCSsnManual.Text = dataApp.aCSsn;


            Tools.SetDropDownListValue(sProdCrManual120MortLateCount, dataApp.aProdCrManual120MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual150MortLateCount, dataApp.aProdCrManual150MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual30MortLateCount, dataApp.aProdCrManual30MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual60MortLateCount, dataApp.aProdCrManual60MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual90MortLateCount, dataApp.aProdCrManual90MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualNonRolling30MortLateCount, dataApp.aProdCrManualNonRolling30MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualRolling60MortLateCount, dataApp.aProdCrManualRolling60MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualRolling90MortLateCount, dataApp.aProdCrManualRolling90MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileMon, dataApp.aProdCrManualBk13RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileYr, dataApp.aProdCrManualBk13RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedMon, dataApp.aProdCrManualBk13RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedYr, dataApp.aProdCrManualBk13RecentSatisfiedYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileMon, dataApp.aProdCrManualBk7RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileYr, dataApp.aProdCrManualBk7RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedMon, dataApp.aProdCrManualBk7RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedYr, dataApp.aProdCrManualBk7RecentSatisfiedYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileMon, dataApp.aProdCrManualForeclosureRecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileYr, dataApp.aProdCrManualForeclosureRecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedMon, dataApp.aProdCrManualForeclosureRecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedYr, dataApp.aProdCrManualForeclosureRecentSatisfiedYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentStatusT, dataApp.aProdCrManualForeclosureRecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentStatusT, dataApp.aProdCrManualBk13RecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentStatusT, dataApp.aProdCrManualBk7RecentStatusT);

            if (PriceMyLoanUser.Type == "B")
            {
                Page.ClientScript.RegisterHiddenField("DeleteLiabilities", "0");
            }
            else if (PriceMyLoanUser.Type == "P")
            {
                // 10/21/2005 dd - For "P" user Type always delete any existing liabilities.
                Page.ClientScript.RegisterHiddenField("DeleteLiabilities", "1");
            }

            main _parentPage = this.Page as main;
            if (null != _parentPage)
            {
                _parentPage.UpdateLoanSummaryDisplay(dataLoan);
            }

            if (!m_hasCreditReport)
            {
                // 12/2/2004 dd - If any credit score is non zero then assumer user enter credit score mannually.
                // 11/3/2008 dd - OPM 25810 - If user is for MCL2PML then default to manual credit entry.
                if (dataApp.aIsCreditReportManual || PriceMyLoanUser.IsPmlMortgageLeagueLogin)
                {
                    if (CreditAction_ManualCredit.Visible)
                    {
                        CreditAction_ManualCredit.Checked = true;
                    }

                }
                else
                {
                    CreditAction_Reissue.Checked = true;
                }
            }

            // 12/27/2006 nw - OPM 5083 - Remember login name
            if (PriceMyLoanUser.LastUsedCreditLoginNm != "")
            {
                LoginName.Text = PriceMyLoanUser.LastUsedCreditLoginNm;
                RememberLoginNameCB.Checked = true;
            }
        }

        public void setNextAppId()
        {
            if (IsReadOnly && Page.IsPostBack)
            {
                string sUserAction = Request.Form["__EVENTARGUMENT"];
                Guid appId = Guid.Empty;
                try { appId = new Guid(Request.Form[m_appList.UniqueID]); } catch { }
                if (sUserAction != null && sUserAction == "switch")
                {
                    setParentAppId(appId);
                }
            }

        }

        private bool isValidApp(CAppData dApp)
        {
            //An app is invalid if it doesn't have a credit report on file and it's not manual
            if (!dApp.aIsCreditReportOnFile)
            {
                if (!m_canRunLpeWithoutCreditReport) //If no report and no manual permission, the app is invalid
                {
                    return false;
                }
                else
                {
                    //if no report on file and manual permission we have to assume the app is manual
                    //we cannot use "dApp.aIsCreditReportManual" because three 0 credit scores are allowed
                    //we check for the same fields as required by the GUI:
                    if (dApp.aBFirstNm.Length == 0 || dApp.aBLastNm.Length == 0 || dApp.aBSsn.Length == 0)
                    {
                        return false;
                    }
                    if (dApp.aBHasSpouse && (dApp.aCFirstNm.Length == 0 || dApp.aCLastNm.Length == 0 || dApp.aCSsn.Length == 0))
                    {
                        return false;
                    }
                    if (dApp.aProdCrManualForeclosureHas) //if foreclusre
                    {
                        if (dApp.aProdCrManualForeclosureRecentFileMon == 0 || dApp.aProdCrManualForeclosureRecentFileYr == 0) //month & year need to be set
                        {
                            return false;
                        }
                        if (dApp.aProdCrManualForeclosureRecentStatusT != E_sProdCrManualDerogRecentStatusT.NotSatisfied) //if foreclusure dismissed/discharged
                        {
                            if (dApp.aProdCrManualForeclosureRecentSatisfiedMon == 0 || dApp.aProdCrManualForeclosureRecentSatisfiedYr == 0) //month & year need to be set
                            {
                                return false;
                            }
                        }
                    }
                    if (dApp.aProdCrManualBk7Has) //if ch7 bankruptcy
                    {
                        if (dApp.aProdCrManualBk7RecentFileMon == 0 || dApp.aProdCrManualBk7RecentFileYr == 0) //month & year need to be set
                        {
                            return false;
                        }
                        if (dApp.aProdCrManualBk7RecentStatusT != E_sProdCrManualDerogRecentStatusT.NotSatisfied) //if ch7 bankruptcy dismissed/discharged
                        {
                            if (dApp.aProdCrManualBk7RecentSatisfiedMon == 0 || dApp.aProdCrManualBk7RecentSatisfiedYr == 0) //month & year need to be set
                            {
                                return false;
                            }
                        }

                    }
                    if (dApp.aProdCrManualBk13Has) //if ch13 bankruptcy
                    {
                        if (dApp.aProdCrManualBk13RecentFileMon == 0 || dApp.aProdCrManualBk13RecentFileYr == 0) //month & year need to be set
                        {
                            return false;
                        }
                        if (dApp.aProdCrManualBk13RecentStatusT != E_sProdCrManualDerogRecentStatusT.NotSatisfied) //if ch13 bankruptcy dismissed/discharged
                        {
                            if (dApp.aProdCrManualBk13RecentSatisfiedMon == 0 || dApp.aProdCrManualBk13RecentSatisfiedYr == 0) //month & year need to be set
                            {
                                return false;
                            }
                        }
                    }

                    //if late payments are present
                    if (dApp.aProdCrManualNonRolling30MortLateCount != 0 && dApp.aProdCrManual30MortLateCount == 0) //if non-rolling are 0, rolling have to be 0
                    {
                        return false;
                    }
                    else if ( dApp.aProdCrManual30MortLateCount > dApp.aProdCrManualNonRolling30MortLateCount) //non-rolling cannot be larger than rolling
                    {
                        return false;
                    }

                    if (dApp.aProdCrManual60MortLateCount != 0 && dApp.aProdCrManualRolling60MortLateCount == 0)
                    {
                        return false;
                    }
                    else if (dApp.aProdCrManualRolling60MortLateCount > dApp.aProdCrManual60MortLateCount)
                    {
                        return false;
                    }

                    if (dApp.aProdCrManual90MortLateCount != 0 && dApp.aProdCrManualRolling90MortLateCount == 0)
                    {
                        return false;
                    }
                    else if (dApp.aProdCrManualRolling90MortLateCount > dApp.aProdCrManual90MortLateCount)
                    {
                        return false;
                    }

                }
            }

            //otherwise valid application
            return true;
        }


        /// <summary>
        /// UI function to save the info provided by the user as a manual credit report. 
        /// </summary>
        private void SaveManualCreditReport()
        {
            CPageData dataLoan = new CManualCreditData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            if (m_currentAppId == Guid.Empty)
                throw new CBaseException("Current application id value could not be loaded.", "m_currentAppId cannot have an empty value. Loan id:'" + this.LoanID + "' Broker id: '" + PriceMyLoanUser.BrokerId + "'");

            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);

            // 5/12/2006 dd - Need to delete the credit report before saving manual credit data. This way
            // the cache value will pick up from manual value correctly.
            // Delete Credit report.
            CreditReportServer.DeleteCreditReport(dataLoan.sBrokerId, dataApp.aAppId, LoanID, CreditReportDeletedAuditItem.E_CreditReportDeletionT.Manual, false);


            dataApp.aBExperianScorePe_rep = aBExperianScorePe.Text;
            dataApp.aBTransUnionScorePe_rep = aBTransUnionScorePe.Text;
            dataApp.aBEquifaxScorePe_rep = aBEquifaxScorePe.Text;
            dataApp.aCExperianScorePe_rep = aCExperianScorePe.Text;
            dataApp.aCTransUnionScorePe_rep = aCTransUnionScorePe.Text;
            dataApp.aCEquifaxScorePe_rep = aCEquifaxScorePe.Text;
            
            dataApp.aBFirstNm = aBFirstNmManual.Text;
            dataApp.aBLastNm = aBLastNmManual.Text;
            if (aBSsnManual.IsUpdated)
                dataApp.aBSsn = aBSsnManual.ClearText;
            
            dataApp.aCFirstNm = aCFirstNmManual.Text;
            dataApp.aCLastNm = aCLastNmManual.Text;
            if (aCSsnManual.IsUpdated)
                dataApp.aCSsn = aCSsnManual.ClearText;
            
            dataApp.aTransmOMonPmtPe_rep = sTransmOMonPmtPe.Text;

            dataApp.aProdCrManual120MortLateCount_rep = sProdCrManual120MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManual150MortLateCount_rep = sProdCrManual150MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManual30MortLateCount_rep = sProdCrManual30MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManual60MortLateCount_rep = sProdCrManual60MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManual90MortLateCount_rep = sProdCrManual90MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManualNonRolling30MortLateCount_rep = sProdCrManualNonRolling30MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManualRolling60MortLateCount_rep = sProdCrManualRolling60MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManualRolling90MortLateCount_rep = sProdCrManualRolling90MortLateCount.SelectedItem.Value;
            dataApp.aProdCrManualBk13Has = sProdCrManualBk13Has.Checked;
            dataApp.aProdCrManualBk13RecentFileMon_rep = sProdCrManualBk13RecentFileMon.SelectedItem.Value;
            dataApp.aProdCrManualBk13RecentFileYr_rep = sProdCrManualBk13RecentFileYr.SelectedItem.Value;
            dataApp.aProdCrManualBk13RecentSatisfiedMon_rep = sProdCrManualBk13RecentSatisfiedMon.SelectedItem.Value;
            dataApp.aProdCrManualBk13RecentSatisfiedYr_rep = sProdCrManualBk13RecentSatisfiedYr.SelectedItem.Value;
            dataApp.aProdCrManualBk13RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualBk13RecentStatusT);
            dataApp.aProdCrManualBk7Has = sProdCrManualBk7Has.Checked;
            dataApp.aProdCrManualBk7RecentFileMon_rep = sProdCrManualBk7RecentFileMon.SelectedItem.Value;
            dataApp.aProdCrManualBk7RecentFileYr_rep = sProdCrManualBk7RecentFileYr.SelectedItem.Value;
            dataApp.aProdCrManualBk7RecentSatisfiedMon_rep = sProdCrManualBk7RecentSatisfiedMon.SelectedItem.Value;
            dataApp.aProdCrManualBk7RecentSatisfiedYr_rep = sProdCrManualBk7RecentSatisfiedYr.SelectedItem.Value;
            dataApp.aProdCrManualBk7RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualBk7RecentStatusT);
            dataApp.aProdCrManualForeclosureHas = sProdCrManualForeclosureHas.Checked;
            dataApp.aProdCrManualForeclosureRecentFileMon_rep = sProdCrManualForeclosureRecentFileMon.SelectedItem.Value;
            dataApp.aProdCrManualForeclosureRecentFileYr_rep = sProdCrManualForeclosureRecentFileYr.SelectedItem.Value;
            dataApp.aProdCrManualForeclosureRecentSatisfiedMon_rep = sProdCrManualForeclosureRecentSatisfiedMon.SelectedItem.Value;
            dataApp.aProdCrManualForeclosureRecentSatisfiedYr_rep = sProdCrManualForeclosureRecentSatisfiedYr.SelectedItem.Value;
            dataApp.aProdCrManualForeclosureRecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualForeclosureRecentStatusT);

            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;

        }
        /// <summary>
        /// Pulls the credit and save. 
        /// </summary>
        /// <param name="isReissue"></param>
        /// <param name="isOrderNew"></param>
        /// <param name="isUpgrade"></param>
        private void PullCreditAndSave(bool isReissue, bool isOrderNew, bool isUpgrade)
        {
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            if (m_currentAppId == Guid.Empty)
                throw new CBaseException("Current application id value could not be loaded.", "m_currentAppId cannot have an empty value. Loan id:'" + this.LoanID + "' Broker id: '" + PriceMyLoanUser.BrokerId + "'");

            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);           
            
            dataApp.aBAddr = aBAddr.Text;
            dataApp.aBCity = aBCity.Text;
            dataApp.aBState = aBState.Value;
            dataApp.aBZip = aBZip.Text;
            if (isOrderNew || isUpgrade)
            {
                dataApp.aCDob_rep = aCDob.Text;
                dataApp.aBDob_rep = aBDob.Text;
            }
            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;

            string protocolID = CreditProtocol.SelectedItem.Value;

            TemporaryCRAInfo craInfo = (TemporaryCRAInfo)m_craHash[protocolID];
            if (craInfo == null)
            {
                return;// 9/20/2004 dd - ERROR. This CRA info is missing. -- Moved By antonio was in empty else statement 
            }
            Guid craID = new Guid(protocolID);

            // 12/27/2006 nw - OPM 5083 - Remember login name
            CreditReportUtilities.UpdateLastUsedCra(PriceMyLoanUser.BrokerId, PriceMyLoanUser.LastUsedCreditLoginNm, RememberLoginNameCB.Checked ? LoginName.Text : "", null, null, PriceMyLoanUser.LastUsedCreditProtocolID, craID, UserID);

            CreditRequestData requestData = CreateRequestData(LoanID, dataApp.aAppId, craID);
            requestData.ReferenceNumber = dataLoan.sLNm;

            int maxAttemptCount = 20;
            int pollingIntervalInSeconds = 10;
            var requestHandler = new CreditReportRequestHandler(requestData, principal: PriceMyLoanUser, shouldReissueOnResponseNotReady: true, retryIntervalInSeconds: pollingIntervalInSeconds, maxRetryCount: maxAttemptCount, loanId: this.LoanID);
            if (ConstStage.DisableCreditOrderingViaBJP)
            {
                // TODO CREDIT_BJP: Temporary code. Remove once we have confirmed credit is working via the BJP.
                this.OrderCreditSynchronously(requestHandler, maxAttemptCount, isReissue, isUpgrade, dataApp, requestData);
                return;
            }

            var initialResult = requestHandler.SubmitToProcessor();
            if (initialResult.Status == CreditReportRequestResultStatus.Failure)
            {
                var errorMessage = string.Join(", ", initialResult.ErrorMessages);
                SetErrorMessage(errorMessage);
                throw new NonCriticalException(errorMessage);
            }
            else
            {
                Guid publicJobId = initialResult.PublicJobId.Value;
                bool hasReceivedResults = false;
                for (int attemptCount = 0; attemptCount < maxAttemptCount; attemptCount++)
                {
                    var finalResult = CreditReportRequestHandler.CheckForRequestResults(publicJobId);
                    if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                    {
                        var errorMessage = string.Join(", ", finalResult.ErrorMessages);
                        SetErrorMessage(errorMessage);
                        throw new NonCriticalException(errorMessage);
                    }
                    else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                    {
                        ICreditReportResponse creditResponse = finalResult.CreditReportResponse;
                        CreditRequestData finalCreditRequestData = finalResult.CreditRequestData;

                        if (null == creditResponse)
                        {
                            SetErrorMessage("If are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.");
                            throw new NonCriticalException("Probably timeout issue. Already send email to developer.");
                        }

                        if (creditResponse.HasError)
                        {
                            // 7/28/2004 dd - Need to do something.
                            // 12/15/2006 nw - OPM 8360 - Provide a more descriptive message for FNMA "User not authenticated" error
                            if (creditResponse.ErrorMessage.ToLower() == "user not authenticated")
                            {
                                if (PriceMyLoanConstants.IsEmbeddedPML)
                                {
                                    SetErrorMessage("The FannieMae DU login information is incorrect. Please confirm your FannieMae DU login and password. To verify your account, contact FannieMae Customer Care at (877)722-6757.");
                                }
                                else
                                {
                                    SetErrorMessage("The FannieMae DU login information is incorrect. Please contact your lender administrator and ask them to confirm their FannieMae DU login and password.");
                                }
                            }
                            else
                            {
                                SetErrorMessage(creditResponse.ErrorMessage);
                            }

                            throw new NonCriticalException("Error with credit report. Safely Ignore.");
                        }

                        E_CreditReportAuditSourceT requestT = E_CreditReportAuditSourceT.OrderNew;
                        if (isReissue)
                        {
                            requestT = E_CreditReportAuditSourceT.Reissue;
                        }
                        if (isUpgrade)
                        {
                            requestT = E_CreditReportAuditSourceT.Upgrade;
                        }

                        if (CreditReportUtilities.IsBorrowerInformationMatch(LoanID, dataApp.aAppId, aBFirstNm.Text, aBMidNm.Text, aBLastNm.Text, aBSuffix.Text, aBSsn.ClearText, aCFirstNm.Text, aCMidNm.Text, aCLastNm.Text, aCSuffix.Text, aCSsn.ClearText, creditResponse) || (isReissue && String.IsNullOrEmpty(dataApp.aBNm)))
                        {
                            sFileVersion = CreditReportUtilities.SaveXmlCreditReport(creditResponse, PriceMyLoanUser, LoanID, m_currentAppId, finalCreditRequestData.ActualCraID, finalCreditRequestData.ProxyId, requestT);
                            ImportLiabilities(true);
                        }
                        else
                        {
                            SetErrorMessage(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            throw new NonCriticalException(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                        }

                        hasReceivedResults = true;
                        break;
                    }
                    else
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(pollingIntervalInSeconds * 1000);
                    }
                }

                if (!hasReceivedResults)
                {
                    SetErrorMessage(ErrorMessages.AsyncReportTakingTooLongToProcess);
                    throw new CBaseException(ErrorMessages.AsyncReportTakingTooLongToProcess, "Async credit report request take too long to process.");
                }
            }
        }

        private void OrderCreditSynchronously(CreditReportRequestHandler requestHandler, int maxAttemptCount, bool isReissue, bool isUpgrade, CAppData dataApp, CreditRequestData requestData)
        {
            bool hasReceivedResults = false;
            for (int attemptCount = 0; attemptCount < maxAttemptCount; attemptCount++)
            {
                var finalResult = requestHandler.SubmitSynchronously();
                if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                {
                    var errorMessage = string.Join(", ", finalResult.ErrorMessages);
                    SetErrorMessage(errorMessage);
                    throw new NonCriticalException(errorMessage);
                }
                else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                {
                    ICreditReportResponse creditResponse = finalResult.CreditReportResponse;
                    if (creditResponse.IsReady)
                    {
                        E_CreditReportAuditSourceT requestT = E_CreditReportAuditSourceT.OrderNew;
                        if (isReissue)
                        {
                            requestT = E_CreditReportAuditSourceT.Reissue;
                        }
                        if (isUpgrade)
                        {
                            requestT = E_CreditReportAuditSourceT.Upgrade;
                        }

                        if (CreditReportUtilities.IsBorrowerInformationMatch(LoanID, dataApp.aAppId, aBFirstNm.Text, aBMidNm.Text, aBLastNm.Text, aBSuffix.Text, aBSsn.ClearText, aCFirstNm.Text, aCMidNm.Text, aCLastNm.Text, aCSuffix.Text, aCSsn.ClearText, creditResponse) || (isReissue && String.IsNullOrEmpty(dataApp.aBNm)))
                        {
                            sFileVersion = CreditReportUtilities.SaveXmlCreditReport(creditResponse, PriceMyLoanUser, LoanID, m_currentAppId, requestData.ActualCraID, requestData.ProxyId, requestT);
                            ImportLiabilities(true);
                        }
                        else
                        {
                            SetErrorMessage(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            throw new NonCriticalException(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                        }

                        hasReceivedResults = true;
                        break;
                    }
                    else if (requestData.CreditProtocol == CreditReportProtocol.Mcl)
                    {
                        requestData.ReportID = creditResponse.ReportID;
                        requestData.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                    }
                    //av 1212008 Removed lender mapping check heres the guid new way should set up the right protocol e04d2398-e614-42a0-9a50-3276c14ab740 
                    else if (requestData.CreditProtocol == CreditReportProtocol.InfoNetwork)
                    {
                        // 8/9/2005 dd - Information Network support async request.
                        requestData.ReportID = creditResponse.ReportID;
                        requestData.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
                    }

                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10000); // Wait for 10 seconds before reissue. 
                }
            }

            if (!hasReceivedResults)
            {
                SetErrorMessage(ErrorMessages.AsyncReportTakingTooLongToProcess);
                throw new CBaseException(ErrorMessages.AsyncReportTakingTooLongToProcess, "Async credit report request take too long to process.");
            }
        }

        private void ImportLiabilities(bool skip0Balance) 
        {
            CPageData dataLoan = new CPmlImportLiabilityData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            if (m_currentAppId == Guid.Empty)
                throw new CBaseException("Current application id value could not be loaded.", "m_currentAppId cannot have an empty value. Loan id:'" + this.LoanID + "' Broker id: '" + PriceMyLoanUser.BrokerId + "'");

            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);
            
            if (Request.Form["DeleteLiabilities"] == "1") 
            {
                dataApp.aLiaCollection.ClearAll();
            }

            dataApp.ImportLiabilitiesFromCreditReport(skip0Balance, true);

			// 2/27/2007 nw - OPM 10630 - import public records
			dataApp.aPublicRecordCollection.ClearAll();
			dataApp.aPublicRecordCollection.Flush();
			dataApp.ImportPublicRecordsFromCreditReport();

            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;
        }

        private Guid Add1003Application()
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);

            if (dataLoan.nApps < 5)
            {
                int appIndex = dataLoan.AddNewApp();

                CAppData dataApp = dataLoan.GetAppData(appIndex);
                return dataApp.aAppId;
            }
            else
            {
                Tools.LogBug("[PML Step 1 - Multiple Applications] Tried to add more than 5 applications in loan " + dataLoan.sLId);
                return Guid.Empty;
            }
        }

        private void Delete1003Application(Guid applicationID)
        {
            if (applicationID == Guid.Empty)
                return;

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (dataLoan.nApps > 1)
            {
                dataLoan.DelApp(applicationID);
            }
            else
            {
                Tools.LogBug("[PML Step 1 - Multiple Applications] Tried to remove the only application in loan " + dataLoan.sLId);
            }
            
        }

        private void Save1003Application()
        {
            DetermineBrokerDULoginInfo(this.CurrentBroker);

            // 7/8/2005 dd - Transform data so that data modified in LendingQB will populate correctly in PML.
            // 10/12/2005 dd - Transform data is take place in LoadData
            // TransformData();

            bool isReissue = CreditAction_Reissue.Checked;
            bool isOrderNew = CreditAction_OrderNew.Checked;
            bool isUpgrade = CreditAction_Upgrade.Checked;

            if (CreditAction_ManualCredit.Checked)
            {
                SaveManualCreditReport();
            }
            else if (isReissue || isOrderNew || isUpgrade)
            {
                PullCreditAndSave(isReissue, isOrderNew, isUpgrade);
            }

        }

        public void SaveData() 
        {
            //Decide what to do based on the user action
            string sUserAction = Request.Form["__EVENTARGUMENT"];
            Exception exception = null;
            bool bExceptionHandled = false;
            Guid appId = Guid.Empty;

            try
            {
                appId = new Guid(Request.Form[m_appList.UniqueID]);
            }
            catch { }
            m_currentAppId = Guid.Empty;
            try
            {
                m_currentAppId = new Guid(Request.Form["currentAppId"]);
            }
            catch { }
            
            if (String.IsNullOrEmpty(sUserAction))
                sUserAction = "next";   //order, save, switch, add

            try
            {
                switch (sUserAction)
                {
                    case "switch":
                        Save1003Application();
                        break;
                    case "add":
                        Save1003Application();
                        appId = Add1003Application();
                        break;
                    case "delete":
                        Delete1003Application(m_currentAppId);
                        appId = Guid.Empty;
                        break;
                    case "order":   //Save current application and load it again.
                        Save1003Application();
                        appId = m_currentAppId;
                        break;
                    default:
                        Save1003Application();
                        appId = Guid.Empty;
                        break;
                }

            }
            catch (DeleteApplicationPermissionDenied exc)
            {
                exception = exc;
                appId = m_currentAppId;
                SetErrorMessage(exc.UserMessage);
                bExceptionHandled = true;
                throw new NonCriticalException(exc.DeveloperMessage);
            }
            catch (Exception exc)
            {
                exception = exc;
                appId = m_currentAppId;
            }
            finally
            {
                setParentAppId(appId);

                if ((exception != null) && (!bExceptionHandled))
                {
                    throw exception;
                }
            }

        }

        private bool setParentAppId(Guid app)
        {
            main _parentPage = this.Page as main;
            if (null != _parentPage && app != Guid.Empty)
            {
                _parentPage.m_AppId = app;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Sets the borrower info into the given object. Sets either borrower or co based on isCoborrower
        /// if its the main borrower it will also set his address.
        /// </summary>
        /// <param name="borrower"></param>
        /// <param name="IsCoborrower"></param>
        private void SetBorrowerInfo(LendersOffice.CreditReport.BorrowerInfo borrower, bool isCoborrower, CreditReportProtocol protocol) 
        {

            borrower.FirstName = isCoborrower ? aCFirstNm.Text : aBFirstNm.Text;
            borrower.MiddleName = isCoborrower ? aCMidNm.Text : aBMidNm.Text;
            borrower.LastName = isCoborrower ? aCLastNm.Text : aBLastNm.Text;
            borrower.Suffix = isCoborrower ? aCSuffix.Text : aBSuffix.Text;
            borrower.DOB = isCoborrower ? aCDob.Text : aBDob.Text;

            CPageData dataLoan = new CEnterCreditData(LoanID);
            dataLoan.InitLoad();

            if (m_currentAppId == Guid.Empty)
                throw new CBaseException("Current application id value could not be loaded.", "m_currentAppId cannot have an empty value. Loan id:'" + this.LoanID + "' Broker id: '" + PriceMyLoanUser.BrokerId + "'");

            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);

            if (!isCoborrower)
            {
                //opm 27866 fs 03/02/09
                //The SSN might be masked; the value on file is only saved if the user edited the SSNbox
                borrower.Ssn = (aBSsn.IsUpdated) ? aBSsn.ClearText : dataApp.aBSsn;
            }
            else
            {
                borrower.Ssn = (aCSsn.IsUpdated) ? aCSsn.ClearText : dataApp.aCSsn;

                // OPM 451238 - Set mailing address for coborrower for protocols that require coborrower address.
                if (protocol == CreditReportProtocol.CSD || protocol == CreditReportProtocol.InformativeResearch)
                {
                    if (!string.IsNullOrEmpty(dataApp.aCAddrMail) || !string.IsNullOrEmpty(dataApp.aCCityMail)
                        || !string.IsNullOrEmpty(dataApp.aCStateMail) || !string.IsNullOrEmpty(dataApp.aCZipMail))
                    {
                        Address coMailingAddress = new Address();
                        coMailingAddress.StreetAddress = dataApp.aCAddrMail;
                        coMailingAddress.City = dataApp.aCCityMail;
                        coMailingAddress.State = dataApp.aCStateMail;
                        coMailingAddress.Zipcode = dataApp.aCZipMail;
                        borrower.MailingAddress = coMailingAddress;
                    }
                }

                return;
            }
            
            Address address = new Address();
            address.City = aBCity.Text;
            address.State = aBState.Value;
            address.Zipcode = aBZip.Text;

            try
            {
                address.ParseStreetAddress(aBAddr.Text);
            }
            catch( Exception e ) 
            {
                address.StreetAddress = aBAddr.Text;
                Tools.LogWarning("Unable to parse borrower address: " + aBAddr.Text, e);
            }
            finally
            {
                borrower.CurrentAddress = address;
            }

            // Set Mailing address
            Address mailingAddress = new Address();
            mailingAddress.StreetAddress = dataApp.aAddrMail;
            mailingAddress.City = dataApp.aCityMail;
            mailingAddress.State = dataApp.aStateMail;
            mailingAddress.Zipcode = dataApp.aZipMail;
            borrower.MailingAddress = mailingAddress;
        }
        
        private CreditRequestData CreateRequestData(Guid loanId, Guid appId, Guid craId)
        {
            bool isNewRequest = CreditAction_OrderNew.Checked;
			bool isUpgradeRequest = CreditAction_Upgrade.Checked;

            CreditRequestData data = CreditRequestData.Create(loanId, appId);

            data.IncludeEquifax = IsEquifax.Checked;
            data.IncludeExperian = IsExperian.Checked;
            data.IncludeTransUnion = IsTransUnion.Checked;
            data.ReportID = isNewRequest ? "" : ReportID.Text;
            data.RequestingPartyName = PriceMyLoanUser.DisplayName;

            data.LoginInfo.UserName = LoginName.Text;
            data.LoginInfo.Password = Password.Text;

            data.UpdateCRAInfoFrom(craId, BrokerID, true);

            SetBorrowerInfo(data.Borrower, false, data.CreditProtocol);
            SetBorrowerInfo(data.Coborrower, true, data.CreditProtocol); 

            //If its lender mapping instnat view wont work. 
            if (!isNewRequest && !isUpgradeRequest && !data.WasLenderMapping)
            {
                data.InstantViewID = InstantViewID.Text;
            }
            switch (data.CreditProtocol)
            {
                case CreditReportProtocol.SharperLending:
                    if (isUpgradeRequest)
                    {
                        data.IncludeEquifax = true;
                        data.IncludeExperian = true;
                        data.IncludeTransUnion = true;
                    }
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    break;
                case CreditReportProtocol.KrollFactualData:
                    data.LoginInfo.AccountIdentifier = FD_OfficeCode.Text + FD_ClientCode.Text;
                    break;
                case CreditReportProtocol.FannieMae:
                    data.LoginInfo.FannieMaeCreditProvider = data.Url; 
                    if (m_hasBrokerDUInfo)
                    {
                        data.LoginInfo.FannieMaeMORNETUserID = m_brokerDUUserID;
                        data.LoginInfo.FannieMaeMORNETPassword = m_brokerDUPassword;
                    }
                    else if ( ! data.WasLenderMapping ) //lender mapping should set the password and username cause ui doesnt show that info when lendermapping is selected.
                    {
                        data.LoginInfo.FannieMaeMORNETUserID = DUUserID.Text;
                        data.LoginInfo.FannieMaeMORNETPassword = DUPassword.Text;
                    }
                    break;
                case CreditReportProtocol.Mcl :
                    data.IsPdfViewNeeded = CreditReportUtilities.IsCraPdfEnabled(CreditReportProtocol.Mcl, BrokerID, data.ActualCraID);
                    if (isUpgradeRequest)
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Upgrade;
                    }
                    else if (isNewRequest)
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.New;
                    }
                    else
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                    }
                    break;
                case CreditReportProtocol.CBC:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    break;
                case CreditReportProtocol.InformativeResearch:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    break;
                case CreditReportProtocol.Credco:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    break;
                default :
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    break;
            
            }

            data.IsMortgageOnly = IsMortgageOnlyCB.Checked;



            return data;
        }
		public void SetErrorMessage( string message )
		{
			// OPM 20593
            ErrorMessageKey = Guid.NewGuid();
			AutoExpiredTextCache.AddToCache( message, TimeSpan.FromMinutes(5), ErrorMessageKey );		
		}
		public void SetAdditionalQueryStringValues( PmlMainQueryStringManager qsManager )
		{
			if ( ErrorMessageKey != Guid.Empty )
			{
				qsManager.AddPair( "errorMsg", ErrorMessageKey.ToString() );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
