﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Common;
using LendersOffice.ObjLib.LockPolicies;

namespace PriceMyLoan.main
{
    // **** PLEASE NOTE that there is a LQB version of this page
    // that is nearly identical to this one.  If making changes
    // here, please evaluate if the other version also needs the change.
    // LQB\los\RateLock\LockRate.aspx
    public partial class LockRate : PriceMyLoan.UI.BasePage
    {

        protected E_AutoRateLockAction m_mode;
        protected string m_pricingMethod
        {
            get
            {
                switch (m_mode)
                {
                    case E_AutoRateLockAction.Extend: return "PriceExtend";
                    case E_AutoRateLockAction.FloatDown: return "PriceFloatDown";
                    case E_AutoRateLockAction.ReLock: return "PriceReLock";
                    default:
                        throw new UnhandledEnumException(m_mode);
                }
            }
        }
        protected Guid m_loanId;
        protected string m_pointsPrice;
        protected List<string> m_newExpirationDates = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            switch (m_mode)
            {
                case E_AutoRateLockAction.Extend:
                    m_Title.Text = "Extend Rate Lock"; break;
                case E_AutoRateLockAction.FloatDown:
                    m_Title.Text = "Float Down Rate Lock"; break;
                case E_AutoRateLockAction.ReLock:
                    m_Title.Text = "Re-Lock Rate (for currently expired lock)"; break;
                default:
                    throw new UnhandledEnumException(m_mode);
            }

            if ( Page.IsPostBack == false)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanId, typeof(LockRate));
                dataLoan.InitLoad();
                dataLoan.ByPassFieldSecurityCheck = true; // CAREFUL.  Have to do this because people without permission to see the lock desk folder will need to use this.

                bool displayPointsAsPrice = dataLoan.sPriceGroup.DisplayPmlFeeIn100Format;
                m_pointsPrice = displayPointsAsPrice ? "Price" : "Points";

                string displayPoint;
                if (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                         && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                         && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                {
                    displayPoint = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep;
                }
                else
                {
                    displayPoint = dataLoan.sBrokComp1Pc_rep;
                }

                m_LoanNumber.Text = dataLoan.sLNm;
                m_Applicant.Text = dataLoan.sPrimBorrowerFullNm;

                m_reasonRow.Visible = m_mode == E_AutoRateLockAction.Extend || m_mode == E_AutoRateLockAction.ReLock;

                if (m_mode == E_AutoRateLockAction.Extend)
                {
                    m_extendScript.Visible = true;
                    m_lockDatesRow.Visible = true;
                    m_OriginalLockExpirationDate.Text = dataLoan.sOriginalLockExpirationD_rep;
                    m_CurrentLockExpirationDate.Text = dataLoan.sRLckdExpiredD_rep;

                    
                    m_timesEventDesc.Text = "Its Lock Expiration Date Extended";
                    m_timesEventOccured.Text = dataLoan.sTotalLockExtensions_rep;

                    m_extentionDaysRow.Visible = true;
                    m_extensionDays.Text = dataLoan.sTotalDaysLockExtended_rep + " days";

                    m_reasonDesc.Text = "Extension";

                    m_PricingTableDiv.Visible = true;
                    m_currentlyLockedRate.Text = dataLoan.sNoteIR_rep;
                    m_currentlyLockedPoints.Text = CApplicantRateOption.PointFeeInResult( displayPointsAsPrice, displayPoint) ;
                }
                else
                {
                    m_extendScript.Visible = false;
                    m_lockDatesRow.Visible = false;
                    m_PricingTableDiv.Visible = false;
                    m_extentionDaysRow.Visible = false;
                }

                if (m_mode == E_AutoRateLockAction.FloatDown)
                {
                    m_floatDownScript.Visible = true;
                    m_timesEventDesc.Text = "Had a Rate Lock Float Down";
                    m_floatDownMsg.Visible = true;
                    m_expirationDateSpan.Visible = true;
                    m_expirationDate.Text = dataLoan.sRLckdExpiredD_rep;
                    m_timesEventOccured.Text = dataLoan.sTotalRateLockFloatDowns_rep;

                    m_lockedRatesRow.Visible = true;
                    m_rowCurrentlyLockedRate.Text = dataLoan.sNoteIR_rep;
                    m_rowCurrentlyLockedPoints.Text = CApplicantRateOption.PointFeeInResult( displayPointsAsPrice, displayPoint);
                }
                else
                {
                    m_floatDownScript.Visible = false;
                    m_floatDownMsg.Visible = false;
                    m_expirationDateSpan.Visible = false;
                    m_lockedRatesRow.Visible = false;
                }

                if (m_mode == E_AutoRateLockAction.ReLock)
                {
                    m_relockScript.Visible = true;
                    m_reasonDesc.Text = "Re-Lock";
                    m_timesEventDesc.Text = "Been Re-Locked";
                    m_relockRow.Visible = true;
                    m_lockExpirationDate.Text = dataLoan.sRLckdExpiredD_rep;
                    m_timesEventOccured.Text = dataLoan.sTotalRateReLocks_rep;

                    m_lockDays.Items.Add(new ListItem("", ""));


                    m_newExpirationDates = new List<string>();
                    m_newExpirationDates.Add(string.Empty);
                    List<ReLockFee> relockFeeList = dataLoan.sLockPolicy.ReLockFeeTable;

                    if (dataLoan.sLockPolicy.IsCodeBased)
                    {
                        relockFeeList = dataLoan.sLockPolicy.GetCustomLockPolicy().GetReLockFeeTable(dataLoan.sLId);
                    }
                    foreach (var relockFee in relockFeeList)
                    {
                        m_lockDays.Items.Add(new ListItem( relockFee.PeriodEndDay.ToString(), relockFee.PeriodEndDay.ToString()));
                        m_newExpirationDates.Add(dataLoan.CalculateRateLockExpirationDate_rep(CDateTime.Create(DateTime.Now), relockFee.PeriodEndDay));
                    }
                }
                else
                {
                    m_relockScript.Visible = false;
                    m_relockRow.Visible = false;
                }

                this.RegisterJsGlobalVariables("showCalendarDays", dataLoan.BrokerDB.RateLockExpirationWeekendHolidayBehavior != E_RateLockExpirationWeekendHolidayBehavior.AllowWeekendHoliday);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            this.RegisterService("lockrate", "/main/LockRateService.aspx");

            string mode = RequestHelper.GetSafeQueryString("action");

            if ( string.IsNullOrEmpty(mode))
                throw new CBaseException(ErrorMessages.Generic, "Missing rate lock action.");

            switch (mode)
            {
                case "extend": m_mode = E_AutoRateLockAction.Extend; break;
                case "floatdown": m_mode = E_AutoRateLockAction.FloatDown; break;
                case "relock": m_mode = E_AutoRateLockAction.ReLock; break;
                default: throw new CBaseException(ErrorMessages.Generic, "Unknown rate lock action: " + mode + ".");
            }

            m_loanId = RequestHelper.GetGuid("loanId", Guid.Empty);
            
            if (m_loanId == Guid.Empty)
                throw new CBaseException(ErrorMessages.Generic, "Invalid loanid specified.");


        }

    }
}
