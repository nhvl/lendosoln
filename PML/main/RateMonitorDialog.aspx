﻿<%@ Page Language='C#' AutoEventWireup='true' CodeBehind='RateMonitorDialog.aspx.cs' Inherits='PriceMyLoan.UI.main.RateMonitorDialog' %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat='server'>
    <title>Monitor Rates</title>
    <style type="text/css">
        /* Error Icons hidden until submission with bad data */
        .ErrorIcon
        {
            visibility: hidden;
        }
    </style>
    <script type="text/javascript">
        // Make sure "Point" field is valid on focus change
        function validatePoint() {
            var m_point = $j("#m_point");
            var pointvalue = parseFloat(m_point.val());
            if (isNaN(pointvalue)) {
                m_point.val("0.000");
            }
            else {
                m_point.val(pointvalue.toFixed(3));
            }
        }
    </script>
</head>
<body>
    <form runat='server'>
    <table>
    <asp:PlaceHolder runat="server" ID="m_nameRowHolder" Visible="false">
        <tr>
            <td>
                <div id='m_nameErrorIcon' class='ErrorIcon' runat='server'>
                    <img src='..\images\error_pointer.gif' alt='Required Field' /></div>
            </td>
            <td noWrap>
                <label>
                    Monitor Name</label>
            </td>
            <td>
                <asp:Textbox runat="server" ID="m_name"></asp:Textbox>
            </td>
        </tr>
    </asp:PlaceHolder>
        <%-- Message Prompt --%>
        <tr>
            <td colspan='3'>
                <ml:EncodedLabel ID='m_DialogMessage' runat='server' Font-Size="small"></ml:EncodedLabel>
            </td>
        </tr>
        <tr style='height: 20px'>
        </tr>
        
        <%-- Pricing Group Name --%>
        <tr>
            <td colspan='3'>
                <ml:EncodedLabel ID='m_programName' runat='server' Style='font-weight: bold;'></ml:EncodedLabel>
            </td>
        </tr>

        <tr style='height: 5px'>
        </tr>
        
        <%-- (Error Icons & ) Input Fields --%>
        <tr>
            <td>
                <div id='m_rateErrorIcon' class='ErrorIcon' runat='server'>
                    <img src='..\images\error_pointer.gif' alt='Required Field' /></div>
            </td>
            <td>
                <label>
                    Rate</label>
            </td>
            <td>
                <ml:PercentTextBox ID='m_rate' runat='server'></ml:PercentTextBox>
            </td>
        </tr>
        
        <tr>
            <td>
                <div id='m_pointErrorIcon' class='ErrorIcon' runat='server'>
                    <img src='..\images\error_pointer.gif' alt='Required Field' /></div>
            </td>
            <td>
                <label>
                    Point</label>
            </td>
            <td>
                <asp:TextBox ID='m_point' runat='server' CausesValidation='true' style='text-align:right;width:70px;' onchange="validatePoint();"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <div id='m_emailErrorIcon' class='ErrorIcon' runat='server'>
                    <img src='..\images\error_pointer.gif' alt='Required Field' /></div>
            </td>
            <td>
                <label>
                    Email(s)</label>
            </td>
            <td>
                <asp:TextBox ID='m_email' runat='server'></asp:TextBox>
            </td>
        </tr>
        
        <%-- Error Message sent from server --%>
        <tr style='height: 25px;'>
            <td colspan='3'>
                <label id='m_ErrorMessageText' runat='server' style='color: Red;'></label>
            </td>
        </tr>
        
        <%-- Control Buttons --%>
        <tr>
            <td colspan='3'>
                <asp:Button Text='Confirm' ID='m_submit' runat='server' OnClick='ValidateRateMonitor'  />
                <asp:Button Text='Cancel' OnClientClick='self.close();' runat='server' />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
