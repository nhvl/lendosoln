﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LendersOffice.AntiXss;

using PriceMyLoan.Security;
using PriceMyLoan.UI;
using LendersOffice.LoanSearch;
using DataAccess;

namespace PriceMyLoan.main
{
    public class BasePipelineTab : BaseUserControl
    {
        public bool IsEDocsDocEnabled { get; set; }
        public bool IsNewPmlUIEnabled { get; set; }

        public static bool ConfigureStatusLink(bool canRead, HtmlAnchor statusLink, Guid sLId)
        {
            if (canRead)
            {
                statusLink.Visible = true;
                //statusLink.Attributes.Add("onclick", "f_viewLoan(" + AspxTools.JsString(sLId) + ");");
                statusLink.InnerText = "status";
                return true;
            }
            return false;
        }

        public static bool ConfigureOpenLink(Guid sLId, LendingQBPmlSearch.E_OpenLinkType linkType, HtmlAnchor link)
        {
            if (linkType == LendingQBPmlSearch.E_OpenLinkType.NONE)
            {
                link.Visible = false;
                return false;
            }

            link.Visible = true;

            if (linkType == LendingQBPmlSearch.E_OpenLinkType.OPEN)
            {
                link.InnerText = "run pricing/request lock";
            }
            else
            {
                link.InnerText = ((PriceMyLoanPrincipal)HttpContext.Current.User).IsRateLockedAtSubmission ? "request lock" : "reprice";
            }
            //link.Attributes.Add("onclick", "f_editLoan(" + AspxTools.JsString(sLId) + ");");
            return true;
        }

        public static bool ConfigureDenialLink(Guid sLId, E_sStatusT statusT, HtmlAnchor denialHyperlink)
        {
            if (statusT == E_sStatusT.Loan_Rejected || statusT == E_sStatusT.Loan_Canceled)
            {
                denialHyperlink.Visible = true;
               //denialHyperlink.Attributes.Add("onclick", "f_downloadCreditDenial(" + AspxTools.JsString(sLId) + ");");
                denialHyperlink.InnerText = "credit denial";
                return true;
            }
            return false;
        }


        public static bool ConfigurePreQualCertLink(Guid sLId, E_sStatusT status, HtmlAnchor anchor, string sPmlSubmitStatusT, E_sRateLockStatusT lockstatus, bool avoidPreQual)
        {
            if (status == E_sStatusT.Loan_Open)
            {
                return false;
            }

            if (sPmlSubmitStatusT == "0")
            {
                return false;
            }

            anchor.Visible = true;

            if (avoidPreQual)
            {

                switch (lockstatus)
                {
                    case E_sRateLockStatusT.Locked:
                    case E_sRateLockStatusT.LockRequested:
                    case E_sRateLockStatusT.LockSuspended:
                        anchor.InnerText = "lock request certificate"; //"view pre-qual certificate";
                        break;
                    case E_sRateLockStatusT.NotLocked:
                        anchor.InnerText = "registration certificate"; //"view pre-qual certificate";
                        break;
                    default:
                        throw new UnhandledEnumException(lockstatus);
                }
            }
            else
            {
                anchor.InnerText = "view pre-qual certificate";
            }
            
            //anchor.Attributes.Add("onclick", "f_displayCertificate(" + AspxTools.JsString(sLId) + ");");
            return true;
        }
    }
}
