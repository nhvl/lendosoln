/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using PriceMyLoan.DataAccess;
using LendersOffice.PmlSnapshot;
using PriceMyLoan.Common;

namespace PriceMyLoan.main
{


	public partial class ListModifiedFields : PriceMyLoan.UI.BasePage
	{

        protected string m_warningMessage;

        protected void PageLoad(object sender, System.EventArgs e)
        {
            CPageData dataLoan = new CListModifyPmlFieldsData(LoanID);
            dataLoan.InitLoad();

            PmlSnapshotDiffResult result = dataLoan.GetModifyPmlData();

            if (string.IsNullOrEmpty(result.OriginalUserName))
            {
                m_warningMessage = "CAUTION: The following data has been modified since the last time pricing was run.";
            }
            else if (result.OriginalTimestamp != DateTime.MinValue)
            {
                m_warningMessage = string.Format("CAUTION: The following data has been modified since pricing was run by {0} on {1}.", result.OriginalUserName, Tools.GetDateTimeDescription(result.OriginalTimestamp));
            }
            else
            {
                m_warningMessage = string.Format("CAUTION: The following data has been modified since pricing was run by {0}.", result.OriginalUserName);
            }

            BindDataSource(m_gvStep1, result.GetDiffByCategory(FieldDescription.Category_Credit));
            BindDataSource(m_gvStep2, result.GetDiffByCategory(FieldDescription.Category_Applicant));
            BindDataSource(m_gvStep3, result.GetDiffByCategory(FieldDescription.Category_Property));

        }
        private void BindDataSource<T>(GridView gv, List<T> list)
        {
            gv.DataSource = list;
            gv.BorderWidth = new Unit(list.Count == 0 ? 0 : 1);
            gv.DataBind();
        }

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
