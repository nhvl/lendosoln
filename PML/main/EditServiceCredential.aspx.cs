﻿namespace PriceMyLoan.main
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Service credential editor.
    /// </summary>
    public partial class EditServiceCredential : PriceMyLoan.UI.BasePage
    {
        private ServiceCredentialContext editingContext;

        /// <summary>
        /// The list of services to hide from the user.
        /// </summary>
        private List<string> servicesToHide = new List<string>();

        /// <summary>
        /// Gets a value indicating whether the broker is configured to use VOX.
        /// </summary>
        /// <value>Whether the broker is configured to use VOX.</value>
        protected bool IsEnableVOXConfiguration
        {
            get
            {
                return BrokerDB.RetrieveById(this.BrokerId).IsEnableVOXConfiguration;
            }
        }

        /// <summary>
        /// Gets the <see cref="BrokerDB.BrokerID"/> for the current credential.
        /// </summary>
        private Guid BrokerId => PrincipalFactory.CurrentPrincipal.BrokerId;

        /// <summary>
        /// Is it new.
        /// </summary>
        private bool IsNew => RequestHelper.GetSafeQueryString("cmd") == "add";

        /// <summary>
        /// Is it delete mode.
        /// </summary>
        private bool IsDelete => RequestHelper.GetSafeQueryString("cmd") == "delete";

        /// <summary>
        /// The target id.
        /// </summary>
        private int? Id
        {
            get
            {
                int id = RequestHelper.GetInt("id", 0);
                return id == 0 ? default(int?) : id;
            }
        }

        /// <summary>
        /// Gets the employee id.
        /// </summary>
        /// <value>The employee id.</value>
        private Guid? EmployeeId
        {
            get
            {
                Guid? employee = RequestHelper.GetGuid("employee", Guid.Empty);
                return employee == Guid.Empty ? default(Guid?) : employee;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this is the editor for the originating company.
        /// </summary>
        /// <value>A value indicating whether this is the editor for the originating company.</value>
        private bool IsOriginatingCompany => RequestHelper.GetBool("oc");

        /// <summary>
        /// Page init handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if ((this.IsOriginatingCompany || this.EmployeeId.HasValue) && !this.PriceMyLoanUser.IsPmlManager)
            {
                throw new AccessDenied("User must be a PML Manager to edit other users' credentials");
            }

            if (this.IsOriginatingCompany)
            {
                this.editingContext = ServiceCredentialContext.CreateEditOriginatingCompanyContextForUser(this.PriceMyLoanUser);
            }
            else if (this.EmployeeId.HasValue)
            {
                var employee = EmployeeDB.RetrieveByIdOrNull(this.BrokerId, this.EmployeeId.Value);
                this.editingContext = ServiceCredentialContext.CreateEditEmployee(employee);
            }
            else
            {
                this.editingContext = ServiceCredentialContext.CreateEditFromProfileContext(this.PriceMyLoanUser);
            }

            this.BindCraList();
            this.BindVerificationVendorList();
            this.EnableJqueryMigrate = false;

            this.RegisterJsGlobalVariables("ServicesToHide", string.Join("|", this.servicesToHide));
        }

        /// <summary>
        /// Page load handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsGlobalVariables("IsDelete", this.IsDelete); // always render this, since the page loads even from the postback, and some of the js will break if it doesn't know it's deleting

            if (this.IsNew)
            {
                return; // nothing to load
            }

            ServiceCredential credential = ServiceCredential.Load(this.Id.Value, this.BrokerId);
            if (!credential.CanEditInTpoPortal(this.editingContext))
            {
                throw new AccessDenied();
            }
            else if (this.IsOriginatingCompany && !credential.IsOriginatingCompanyCredential)
            {
                throw new AccessDenied("Credential id was not for an originating company");
            }
            else if (this.EmployeeId.HasValue && this.EmployeeId != credential.AssociatedEmployeeId)
            {
                throw new AccessDenied("Credential requested employee id did not match loaded credential's employee id.");
            }
            else if (!this.IsOriginatingCompany && !this.EmployeeId.HasValue && PrincipalFactory.CurrentPrincipal.EmployeeId != credential.AssociatedEmployeeId)
            {
                throw new AccessDenied("Credential requested must be for the currently logged in user.");
            }

            if (!this.IsPostBack)
            {
                if (this.IsDelete)
                {
                    this.RenderDelete(credential);
                }
                else if (!this.IsNew)
                {
                    this.AccountId.Value = credential.AccountId;
                    this.UserName.Value = credential.UserName;
                    this.UserPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay); // setting a password field directly does not work.
                    this.ConfirmPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
                    this.IsForCreditReport.Checked = credential.IsForCreditReports;
                    this.IsForVerifications.Checked = credential.IsForVerifications;
                    this.SetDropDownValue(this.CraList, credential.ServiceProviderId, credential.ServiceProviderName);
                    this.SetDropDownValue(this.VerificationVendorList, credential.VoxVendorId, credential.VoxVendorName);
                }
            }
        }

        /// <summary>
        /// Save click event handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void OnSaveClick(object sender, EventArgs e)
        {
            if (this.IsDelete)
            {
                ServiceCredential.DeleteServiceCredential(this.Id.Value, this.BrokerId);
            }
            else
            {
                ServiceCredential credential;
                if (this.IsNew)
                {
                    if (this.IsOriginatingCompany)
                    {
                        credential = ServiceCredential.CreateNewOriginatingCompanyCredential(this.BrokerId, PrincipalFactory.CurrentPrincipal.PmlBrokerId);
                    }
                    else
                    {
                        credential = ServiceCredential.CreateNewEmployeeCredential(this.BrokerId, this.EmployeeId ?? PrincipalFactory.CurrentPrincipal.EmployeeId);
                    }
                }
                else
                {
                    credential = ServiceCredential.Load(this.Id.Value, this.BrokerId);
                }

                credential.IsForCreditReports = this.IsForCreditReport.Checked;
                credential.IsForVerifications = this.IsForVerifications.Checked;

                credential.ServiceProviderId = credential.IsForCreditReports ? this.ChosenCraId.Value.ToNullable<Guid>(Guid.TryParse) : null;
                credential.ServiceProviderName = credential.ServiceProviderId.HasValue ? this.ChosenCraName.Value : null;

                credential.VoxVendorId = credential.IsForVerifications ? this.ChosenVerificationVendorId.Value.ToNullable<int>(int.TryParse) : null;
                credential.VoxVendorName = credential.VoxVendorId.HasValue ? this.ChosenVerificationVendorName.Value : null;

                credential.AccountId = this.CredentialUsesAccountId(credential) ? this.AccountId.Value : string.Empty;
                credential.UserName = this.UserName.Value;
                if (this.UserPassword.Text != ConstAppDavid.FakePasswordDisplay)
                {
                    credential.UserPassword = this.UserPassword.Text;
                }

                credential.UserType = PrincipalFactory.CurrentPrincipal.IsPUser ? UserType.TPO : UserType.LQB;

                try
                {
                    credential.Save();
                }
                catch (CBaseException exc)
                {
                    Tools.LogWarning(exc);
                    this.UserMessage.Text = exc.UserMessage;
                    return;
                }
            }

            // Get latest state to pass to Listing UI.
            IEnumerable<ServiceCredential> currentList;

            if (this.IsOriginatingCompany)
            {
                currentList = ServiceCredential.GetTpoEditableOriginatingCompanyServiceCredentials(this.editingContext);
            }
            else
            {
                currentList = ServiceCredential.GetTpoEditableEmployeeServiceCredentials(this.editingContext);
            }

            ClientScript.RegisterHiddenField("credentialList", ObsoleteSerializationHelper.JsonSerialize(currentList));
            ClientScript.RegisterHiddenField("autoclose", "true");
        }

        /// <summary>
        /// Indicates whether the specified CRA uses account id.
        /// </summary>
        /// <param name="cra">The CRA to check.</param>
        /// <returns><c>true</c> if the CRA uses an account id.</returns>
        private static bool CraUsesAccountId(CRA cra)
        {
            if (cra == null)
            {
                return false;
            }

            return cra.Protocol == CreditReportProtocol.MISMO_2_1
                || cra.Protocol == CreditReportProtocol.MISMO_2_3
                || cra.Protocol == CreditReportProtocol.KrollFactualData
                || cra.Protocol == CreditReportProtocol.InfoNetwork
                || cra.Protocol == CreditReportProtocol.SharperLending
                || cra.Protocol == CreditReportProtocol.CBC
                || cra.Protocol == CreditReportProtocol.InformativeResearch;
        }

        /// <summary>
        /// Indicates whether the specified CRA requires an account id.
        /// </summary>
        /// <param name="cra">The CRA to check.</param>
        /// <returns><c>true</c> if the CRA requires an account id.</returns>
        private static bool CraRequiresAccountId(CRA cra)
        {
            return CraUsesAccountId(cra) && cra.Protocol != CreditReportProtocol.CBC;
        }

        /// <summary>
        /// Determines whether the given credential should use the account ID field.
        /// </summary>
        /// <param name="credential">The credential to check.</param>
        /// <returns>Whether or not the credential uses an account ID.</returns>
        private bool CredentialUsesAccountId(ServiceCredential credential)
        {
            var verificationsUsesAcctId = credential.IsForVerifications && ServiceCredential.GetAvailableVoxVendorsForServiceCredentials(this.editingContext)[credential.VoxVendorId.Value].UsesAccountId;
            var creditReportUsesAcctId = credential.IsForCreditReports && CraUsesAccountId(MasterCRAList.FindById(credential.ServiceProviderId ?? Guid.Empty, includeInvalidCRAs: false));

            return verificationsUsesAcctId || creditReportUsesAcctId;
        }

        /// <summary>
        /// Sets the DropDownList value to a given value.
        /// </summary>
        /// <typeparam name="T">The type of backing value for the dropdown's option values.</typeparam>
        /// <param name="ddl">The dropdown to set the value of.</param>
        /// <param name="valueToSet">The value to set the dropdown value to. If null, this method does nothing.</param>
        /// <param name="nameToAdd">The name to add to the dropdown if no matching value already exists.</param>
        private void SetDropDownValue<T>(DropDownList ddl, T? valueToSet, string nameToAdd = null) where T : struct
        {
            if (valueToSet.HasValue)
            {
                ListItem foundDdlValue = ddl.Items.FindByValue(valueToSet.Value.ToString());
                if (foundDdlValue == null && nameToAdd != null)
                {
                    ddl.Items.Add(new ListItem(nameToAdd, valueToSet.ToString()));
                }
                else if (foundDdlValue == null && nameToAdd == null)
                {
                    return;
                }

                Tools.SetDropDownListValue(ddl, valueToSet.Value.ToString());
            }
        }

        /// <summary>
        /// Show the UI in the delete confirmation state.
        /// </summary>
        /// <param name="credential">The credential to render for possible deletion.</param>
        private void RenderDelete(ServiceCredential credential)
        {
            this.UserMessage.Text = $"Are you sure you want to delete {credential.ServiceProviderDisplayName}:{credential.UserName}?";
            this.SaveBtn.Text = "Yes";
            this.CancelBtn.Value = "No";
        }

        /// <summary>
        /// Bind the CRA list.
        /// </summary>
        private void BindCraList()
        {
            this.CraList.Items.Add(new ListItem(string.Empty, string.Empty));
            
            foreach (var cra in ServiceCredential.GetAvailableCreditVendorsForServiceCredentials(this.editingContext))
            {
                ListItem item = new ListItem(cra.VendorName, cra.ID.ToString());

                item.Attributes.Add("data-acctid", CraUsesAccountId(cra) ? (CraRequiresAccountId(cra) ? "r" : "y") : "n");
                if (cra.VoxVendorId.HasValue)
                {
                    item.Attributes.Add("data-voxVendorId", cra.VoxVendorId.Value.ToString());
                }
            
                this.CraList.Items.Add(item);
            }

            if (this.CraList.Items.Count == 1)
            {
                this.servicesToHide.Add("Credit");
            }
        }

        /// <summary>
        /// Binds the verification vendors list.
        /// </summary>
        private void BindVerificationVendorList()
        {
            if (!this.IsEnableVOXConfiguration)
            {
                this.servicesToHide.Add("Verification");
                return;
            }

            this.VerificationVendorList.Items.Add(new ListItem(string.Empty, string.Empty));

            HashSet<int> addedVendors = new HashSet<int>();
            foreach (var lightVendorWithId in ServiceCredential.GetAvailableVoxVendorsForServiceCredentials(this.editingContext))
            {
                int vendorId = lightVendorWithId.Key;
                LendersOffice.Integration.VOXFramework.LightVOXVendor lightVendor = lightVendorWithId.Value;
                if (!addedVendors.Contains(vendorId))
                {
                    ListItem item = new ListItem(lightVendor.CompanyName, vendorId.ToString());
                    item.Attributes.Add("data-acctid", lightVendor.UsesAccountId ? (lightVendor.RequiresAccountId ? "r" : "y") : "n");
                    this.VerificationVendorList.Items.Add(item);
                }
            }

            if (this.VerificationVendorList.Items.Count == 1)
            {
                this.servicesToHide.Add("Verification");
            }
        }
    }
}