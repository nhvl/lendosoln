﻿using System;
using System.Web.UI;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace PriceMyLoan.main
{
    public partial class LendingLicensesPanel : PriceMyLoan.UI.BaseUserControl
    {
		private LicenseInfoList	    m_LicenseInfoList       = new LicenseInfoList();
        protected string            m_prefix                = "";
		protected const int         MAX_LICENSES            = 100;
        private const string        JS_JSON_FILENAME        = "json.js";
        private const string        JS_LICENSES_FILENAME    = "licenses.js";
        private bool                isJsRegistered          = false;
        private string              m_JsonToSerialize       = "";
        private bool m_bDisplayLosIdentifier = true;

        #region Properties
        public bool Disabled { get; set; }        

        public LicenseInfoList LicenseList
        {
            set
            {
                m_LicenseInfoList = value;
                m_JsonToSerialize = LendingLicensesPanel.SerializeLicenseInfoList(m_LicenseInfoList);
            }
            get
            {
                var json = Request.Form[m_prefix + "LendingLicenseList"] ?? "{}";
                m_LicenseInfoList = LendingLicensesPanel.DeserializeLicenseInfoList(json) ?? new LicenseInfoList();

                return m_LicenseInfoList;
            }
        }

        public string NamingPrefix
        {
            get { return m_prefix; }
            set
            {
                m_prefix = string.IsNullOrEmpty(value) ? "" : Regex.Replace(value, "[^a-zA-Z0-9]", "").TrimWhitespaceAndBOM();
            }
        }
        public bool DisplayLosIdentifier 
        {
            get { return m_bDisplayLosIdentifier; }
            set { m_bDisplayLosIdentifier = value; } 
        }
        public string LosIdentifier
        {
            get { return tbNmlsIdentifier.Text; }
            //set { m_sLosIdentifier = value; }
            set { tbNmlsIdentifier.Text = value; }
        }
        public bool IsCompanyLOID { get; set;}
        #endregion

        protected void PageInit(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(m_prefix))
            {
                throw new ArgumentException("The prefix for 'LendingLicensesPanel' cannot be empty. Please initialize it by setting the NamingPrefix property.", "m_prefix");
            }

            ((BasePage)Page).RegisterJsScript(JS_JSON_FILENAME);
            ((BasePage)Page).RegisterJsScript(JS_LICENSES_FILENAME);
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{            
            pnlLsIdentifier.Visible = m_bDisplayLosIdentifier;
            if (IsCompanyLOID)
                lblLOID.InnerText = "Loan Origination Company Identifier";
        }

        protected void PagePreRender(object sender, System.EventArgs a)
        {
            try
            {
                tbNmlsIdentifier.ReadOnly = Disabled;
                // 5/13/2010 dd - Move this register startup script down so the Disable property value will have the correct value.
                Page.ClientScript.RegisterStartupScript(Page.GetType(), m_prefix + "lendingInit", @"if (this.LICENSES){ LICENSES.fn.Add('" + m_prefix + @"', " + (Disabled ? "true" : "false") + @"); }", true);

                // OPM 462714 - Get LicenseList, which loads m_LicenseInfoList, restoring data after postback.
                string json = string.IsNullOrEmpty(m_JsonToSerialize) ? LendingLicensesPanel.SerializeLicenseInfoList(this.LicenseList) : m_JsonToSerialize;
                
                RegisterJsStartupObjects(json);
            }
            catch(CBaseException exc)
            {
                Tools.LogWarning(exc);
            }

        }

        private void RegisterJsStartupObjects(string json)
        {
            if (!isJsRegistered)
            {
                isJsRegistered = true;
                string script = "if (this.LICENSES){ LICENSES.List['" + m_prefix + "'] = " + json + "; }";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "LICENSES.List[" + m_prefix + "]", script, true);
                Page.ClientScript.RegisterHiddenField(m_prefix + "LendingLicenseList", json);                
            }
        }

        public static void RegisterLicensesObject(Page page, string prefix, LicenseInfoList list)
        {
            string json = LendingLicensesPanel.SerializeLicenseInfoList(list);
            string script = "LICENSES.List['" + prefix + "'] = " + json + ";";
            page.ClientScript.RegisterStartupScript(page.GetType(), "LICENSES.List[" + prefix + "]", script, true);
        }

        public static LicenseInfoList DeserializeLicenseInfoList(string json)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            LicenseInfoList	LicenseInfoList = new LicenseInfoList();
            if (string.IsNullOrEmpty(json))
                return LicenseInfoList;

            List<List<string>> stringList = ObsoleteSerializationHelper.JsonDeserialize<List<List<string>>>(json);

            if (stringList != null)
            {
                foreach (List<string> lendingLicense in stringList)
                {
                    if (lendingLicense[0].TrimWhitespaceAndBOM().Length == 0) // Empty licenses are not allowed. Proceed to the next entry.
                    {
                        string msg = $"User \'{principal.LastName}, {principal.FirstName}\' (userid: {principal.UserId}) from broker \'{principal.BrokerDB.Name}\' (brokerid: {principal.BrokerId}) tried to add a license with an empty number.";
                        Tools.LogWarning(msg);
                        continue;
                    }

                    LicenseInfo licenseInfo = new LicenseInfo(lendingLicense[1], lendingLicense[2], lendingLicense[0]);
                    if (licenseInfo.State == "--")
                        licenseInfo.State = "";
                    if (LicenseInfoList.IsValid(licenseInfo) && !LicenseInfoList.IsDuplicateLicense(licenseInfo) && !LicenseInfoList.IsFull)
                    {
                        LicenseInfoList.Add(licenseInfo);
                    }
                    else
                    {
                        string msg = $"User \'{principal.LastName}, {principal.FirstName}\' (userid: {principal.UserId}) from broker \'{principal.BrokerDB.Name}\' (brokerid: {principal.BrokerId}) could not add the following license: \'{licenseInfo.License}\' | \'{licenseInfo.State}\' | \'{licenseInfo.ExpD}\'. ";
                        if (LicenseInfoList.IsFull)
                            msg += "The max number of licenses has been reached.";
                        else if (!LicenseInfoList.IsValid(licenseInfo))
                            msg += "Invalid license.";
                        else
                            msg += "An existing license with the same id was found.";
                        Tools.LogWarning(msg);
                    }
                }
            }

            return LicenseInfoList;
        }

        public static string SerializeLicenseInfoList(LicenseInfoList LicenseInfoList)
        {
            List<List<string>> stringList = new List<List<string>>();
            if (LicenseInfoList != null)
            {
                foreach (LicenseInfo o in LicenseInfoList)
                {
                    stringList.Add(new List<string>() { o.License, o.State, o.ExpD });
                }
            }
            
            return ObsoleteSerializationHelper.JsonSerialize(stringList);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            this.PreRender += new System.EventHandler(this.PagePreRender);
		}
		#endregion
	}
}