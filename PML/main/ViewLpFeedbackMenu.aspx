<%@ Page language="c#" Codebehind="ViewLpFeedbackMenu.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.ViewLpFeedbackMenu" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>ViewLpFeedbackMenu</title>
  </head>
  <body MS_POSITIONING="FlowLayout">
	<script language=javascript>
<!--
function f_print() {
    parent.body.window.print();
}
function f_rbClick(rb) {
  parent.body.location = <%= AspxTools.JsString("ViewLPFeedback.aspx?loanid=" + RequestHelper.LoanID + "&type=") %> + encodeURIComponent(rb.value);
}
function f_viewLpDirectly() {
  parent.body.location = <%= AspxTools.JsString("LoanProspectorMain.aspx?loanid=" + RequestHelper.LoanID + "&entrypoint=" +  ConstAppDavid.LoanProspector_EntryPoint_ViewFeedback) %>;
}
//-->
</script>		
    <form id="ViewLpFeedbackMenu" method="post" runat="server">
<table border=0 cellspacing=0 cellpadding=5 width="100%">
<tr>
<td valign=top>
<input type="radio" name="type" value="feedback" id="rbFeedback" checked onclick="f_rbClick(this);"><label for="rbFeedback">Full Feedback</label> 
<input type="radio" name="type" value="checklist" id=rbChecklist onclick="f_rbClick(this);"><label for="rbChecklist">Checklist</label>
<input type="radio" name="type" value="merged" id=rbMerged onclick="f_rbClick(this);"><label for="rbMerged">Merged Credit</label>
<input type="radio" name="type" value="infile" id=rbInfile onclick="f_rbClick(this);"><label for="rbInfile">Credit Infile</label>
<input type="radio" name="type" value="viewlp" id="rbViewLp" onclick="f_viewLpDirectly();"><label id="rbViewLpLabel" for="rbViewLp" runat="server">View Feedback in Loan Product Advisor</label>
</td>
<td align=right valign=top>
  <input type=button onclick="f_print();" value="Print">
  <input type=button onclick="top.window.close();" value="Close">
</td>
</tr>
</table>
     </form>
	
  </body>
</html>
