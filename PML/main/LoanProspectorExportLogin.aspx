<%@ Page language="c#" Codebehind="LoanProspectorExportLogin.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.LoanProspectorExportLogin" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>LoanProspectorExportLogin</title>
  </head>
<body MS_POSITIONING="FlowLayout" style="MARGIN-TOP:0px;MARGIN-LEFT:5px" >
<script type="text/javascript">
<!--
function _init() {
  resize(700, 480);

    <% if (m_sHasFreddieInfileCredit) { %>
    document.getElementById("rbOrderInfileCredit").checked = true;
    <% } %>
    f_onMergeInfileClick(document.getElementById('rbOrderInfileCredit'));  
    f_onReorderCreditClick();  
    $j('#FreddieCrcDDL').val(ML.cra);
}
function f_save() {
  var args = new Object();
  args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
  args["sFreddieLoanId"] = <%= AspxTools.JsGetElementById(sFreddieLoanId) %>.value;
  args["sFreddieTransactionId"] = <%= AspxTools.JsGetElementById(sFreddieTransactionId) %>.value;
  args["sLpAusKey"] = <%= AspxTools.JsGetElementById(sLpAusKey) %>.value;
  args["sFreddieSellerNum"] = <%= AspxTools.JsGetElementById(sFreddieSellerNum) %>.value;
  args["sFreddieTpoNum"] = <%= AspxTools.JsGetElementById(sFreddieTpoNum) %>.value;
  args["sFreddieNotpNum"] = <%= AspxTools.JsGetElementById(sFreddieNotpNum) %>.value;
  args["sFreddieLpPassword"] = <%= AspxTools.JsGetElementById(sFreddieLpPassword) %>.value;
  args["FreddieCrcDDL"] = document.getElementById('FreddieCrcDDL').value;
  args["CreditChoice"] = document.getElementById("rbOrderMergeCredit").checked ? "Merge" : "Infile";
  args["cbRequestReorderCredit"] = document.getElementById("cbRequestReorderCredit").checked ? "True" : "False";
  args["sFredProcPointT"] = <%= AspxTools.JsGetElementById(sFredProcPointT) %>.value;
  
  for (var i = 0; i < g_mcrnList.length; i++) {
    args[g_mcrnList[i]] = document.getElementById(g_mcrnList[i]).value;
    args['Reorder' + g_mcrnList[i]] = document.getElementById('Reorder' + g_mcrnList[i]).checked ? "True" : "False";
    
  }  
  var result = gService.loanedit.call("Save", args);
  
  
}
function f_isValid() {
    var bMissingRequiredFields = false;
    var errMsg = '';
    if (<%= AspxTools.JsGetElementById(sFreddieSellerNum) %>.value == '') {
      errMsg = 'Loan Product Advisor Seller Number cannot be empty.\n';
      bMissingRequiredFields = true;      
    }
    <% if (!m_hasLpPassword) { %>
    if (<%= AspxTools.JsGetElementById(sFreddieLpPassword) %>.value == '') {
      errMsg += 'Loan Product Advisor Password cannot be empty.';
      bMissingRequiredFields = true;
    }
    <% } %>
    if (bMissingRequiredFields) {
      alert(errMsg);
      return false;
    }  
  return true;
}
function f_cmdToLp(entryPoint) {

  if (!f_isValid()) {
    return;
  }
  
  f_save();
  
  parent.LQBPopup.Return(true, entryPoint);
}
function f_close() {
  parent.LQBPopup.Return(false);
}
  function f_onMergeInfileClick(rb) {
    var bIsInfile = rb.checked && rb.id == 'rbOrderInfileCredit';
    
    disableDDL(document.getElementById('FreddieCrcDDL'), bIsInfile);
    
    for (var i = 0; i < g_mcrnList.length; i++) {
      $j(document.getElementById(g_mcrnList[i])).readOnly(bIsInfile);
    }
  }
  function f_onReorderCreditClick() {
    var bIsReorder = document.getElementById('cbRequestReorderCredit').checked;
    for (var i = 0; i < g_mcrnList.length; i++) {
      document.getElementById('Reorder' + g_mcrnList[i]).disabled = !bIsReorder;
      if (document.getElementById('Reorder' + g_mcrnList[i]).disabled)
        document.getElementById('Reorder' + g_mcrnList[i]).checked = false;
    }
    
  }
  function f_sendToLp(event) {
    f_cmdToLp(<%= AspxTools.JsString(ConstAppDavid.LoanProspector_EntryPoint_SendLp) %>);
  }
  var g_mcrnList = <%= AspxTools.JsArray(m_mcrnList) %>;
//-->
</script>

<form id=LoanProspectorExportLogin method=post runat="server">
<table id=MainTable cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td colspan=2 class="SubHeader"><ml:EncodedLiteral id="headerMsg" runat="server">Send to Loan Product Advisor</ml:EncodedLiteral></td>
  </tr>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="lpLoanId" runat="server">LPA Loan Id</ml:EncodedLiteral></td>
    <td><asp:textbox id=sFreddieLoanId runat="server" /></td></tr>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="lpTransId" runat="server">LPA Transaction ID</ml:EncodedLiteral></td>
    <td><asp:textbox id=sFreddieTransactionId runat="server" /></td></tr>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="lpAusKeyNum" runat="server">LPA AUS Key Number</ml:EncodedLiteral></td>
    <td><asp:textbox id=sLpAusKey runat="server" /></td></tr>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="lpSelNum" runat="server">LPA Seller Number</ml:EncodedLiteral></td>
    <td><asp:textbox id=sFreddieSellerNum runat="server" /></td></tr>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="lpTpoNum" runat="server">LPA TPO Number</ml:EncodedLiteral></td>
    <td><asp:textbox id=sFreddieTpoNum runat="server" /></td></tr>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="lpNotpNum" runat="server">LPA NOTP Number</ml:EncodedLiteral></td>
    <td><asp:textbox id=sFreddieNotpNum runat="server" /></td></tr>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="lpPasswordRebrand" runat="server">LPA Password</ml:EncodedLiteral></td>
    <td><asp:textbox id=sFreddieLpPassword runat="server" TextMode="Password" />(Leave 
      the password blank if it's not being changed.)</td></tr>
  <TR>
    <TD class=FieldLabel>Case State Type</TD>
    <TD><asp:dropdownlist id=sFredProcPointT runat="server" /></TD></TR>
  <tr>
    <td colSpan=2>&nbsp;</td></tr>
  <tr>
    <td class=FieldLabel colSpan=2><INPUT type=radio id="rbOrderMergeCredit" name="CreditChoice" value="Merge" checked onclick='f_onMergeInfileClick(this);'><label for="rbOrderMergeCredit">Order Merged Credit</label>
    &nbsp;&nbsp;<INPUT type=radio id="rbOrderInfileCredit" name="CreditChoice" value="Infile" onclick='f_onMergeInfileClick(this);'><label for="rbOrderInfileCredit">Credit Infile</label>
    </td></tr>
        <tr><td colspan=2 class="FieldLabel" style="PADDING-LEFT:25px">Credit Reporting Company
            <select id="FreddieCrcDDL">
                <option value="">&nbsp;</option>
                <optgroup label="Main Credit Reporting Companies">
                    <asp:Repeater ID="MainCreditReportingCompanies" runat="server">
                        <ItemTemplate>
                        <option value=<%# AspxTools.HtmlAttribute((string)Eval("Key")) %> ><%# AspxTools.HtmlString(Eval("Value")) %></option>
                        </ItemTemplate>
                    </asp:Repeater>
                </optgroup>
                <optgroup label="Technical Affiliates">
                    <asp:Repeater ID="TechnicalAffiliates" runat="server">
                        <ItemTemplate>
                        <option value=<%# AspxTools.HtmlAttribute((string)Eval("Key")) %> ><%# AspxTools.HtmlString(Eval("Value")) %></option>
                        </ItemTemplate>
                    </asp:Repeater>
                </optgroup>
            </select></td></tr>
<asp:Repeater id=m_borrowerListRepeater Runat="server">
<ItemTemplate>
<tr><td class="FieldLabel" style="padding-left:25px"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerName").ToString())%></td>
<td class="FieldLabel">Reference Number: <input type="text" id=<%# AspxTools.HtmlAttribute(DataBinder.Eval(Container.DataItem, "BorrowerId").ToString()) %> name=<%# AspxTools.HtmlAttribute(DataBinder.Eval(Container.DataItem, "BorrowerId").ToString()) %> value=<%# AspxTools.HtmlAttribute(DataBinder.Eval(Container.DataItem, "Reference").ToString()) %>></td>
</tr>
</ItemTemplate>
</asp:Repeater>
<tr><td colspan=2>&nbsp;</td></tr>
<tr><td class="FieldLabel" colspan="2"><input type="checkbox" id="cbRequestReorderCredit" name="cbRequestReorderCredit" value="ReorderCredit" onclick="f_onReorderCreditClick();"><label for="cbRequestReorderCredit">Request Reorder Credit</label></td></tr>
<tr><td colspan=2>Please select the borrower(s) for whom you want the Reorder Credit Services.</td></tr>
<asp:Repeater id="m_borrowerListReorderRepeater" Runat="server">
<ItemTemplate>
<tr><td class="FieldLabel" colspan=2 nowrap style="padding-left:25px"><input type="checkbox" name=<%# AspxTools.HtmlAttribute("Reorder" + DataBinder.Eval(Container.DataItem, "BorrowerId").ToString())%> id=<%# AspxTools.HtmlAttribute("Reorder" + DataBinder.Eval(Container.DataItem, "BorrowerId").ToString()) %>>
<label for=<%# AspxTools.HtmlAttribute("Reorder" + DataBinder.Eval(Container.DataItem, "BorrowerId").ToString()) %>><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerName").ToString())%></label></td>
</tr>
</ItemTemplate>
</asp:Repeater>


<tr><td colspan=2>&nbsp;</td></tr>

        <tr><td colspan=2 align=center>
        <input type="button" id="sendToLpBtn" style="WIDTH:220px"  class="ButtonStyle" value="Send to Loan Product Advisor" onclick="f_sendToLp(event);" runat="server" />&nbsp;&nbsp;
        <input type="button" class="ButtonStyle" value="Cancel" onclick="f_close();" />
        </td></tr></TABLE></form>
	
  </body>
</html>
