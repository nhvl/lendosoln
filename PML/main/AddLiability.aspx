<%@ Import namespace="LendersOffice.Common"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="AddLiability.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.AddLiability" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head runat="server">
		<title>Add Liability</title>
	</head>
	<body MS_POSITIONING="FlowLayout" leftmargin=0 rightmargin=0 topmargin=5 >
	<script language=javascript>
<!--
function _init() {
    resizeForIE6And7(400, 390);
    if (<%= AspxTools.JsBool(IsReadOnly) %>)
	{
		<%= AspxTools.JsGetElementById(Pmt) %>.disabled = true;
		<%= AspxTools.JsGetElementById(Bal) %>.disabled = true;
		<%= AspxTools.JsGetElementById(OwnerT) %>.disabled = true;
		<%= AspxTools.JsGetElementById(DebtT) %>.disabled = true;
		<%= AspxTools.JsGetElementById(ComNm) %>.disabled = true;
		<%= AspxTools.JsGetElementById(WillBePdOff) %>.disabled = true;
		<%= AspxTools.JsGetElementById(UsedInRatio) %>.disabled = true;
		
		<%= AspxTools.JsGetElementById(ComAddr) %>.disabled = true;
		<%= AspxTools.JsGetElementById(ComCity) %>.disabled = true;
		<%= AspxTools.JsGetElementById(ComState) %>.disabled = true;
		<%= AspxTools.JsGetElementById(ComZip) %>.disabled = true;
		<%= AspxTools.JsGetElementById(AccNm) %>.disabled = true;
		<%= AspxTools.JsGetElementById(AccNum) %>.disabled = true;
	}
}

function cleanString(txt)
{
 return txt.replace(/[^a-z0-9_\(\)\s]/gi,'');
}

function f_btnClick()
{
  var ComNm = <%= AspxTools.JsGetElementById(ComNm) %>;
  var ComAddr = <%= AspxTools.JsGetElementById(ComAddr) %>;
  var ComCity = <%= AspxTools.JsGetElementById(ComCity) %>;
  var AccNm = <%= AspxTools.JsGetElementById(AccNm) %>;
  var AccNum = <%= AspxTools.JsGetElementById(AccNum) %>;
  
  ComNm.value = cleanString(ComNm.value);
  ComAddr.value = cleanString(ComAddr.value);
  ComCity.value = cleanString(ComCity.value);
  AccNm.value = cleanString(AccNm.value);
  AccNum.value = cleanString(AccNum.value);
}

function f_cboxOnClick()
{
  var pdOff = <%= AspxTools.JsGetElementById(WillBePdOff) %>;
  var uRatio = <%= AspxTools.JsGetElementById(UsedInRatio) %>;
  var debt = <%= AspxTools.JsGetElementById(DebtT) %>;
  var isMortgage = getDropDownValue(debt) == 'Mortgage';
  
	if (pdOff.checked)
	{
	  uRatio.checked = false;
	}
	else if (!isMortgage)
	  uRatio.disabled = false;
}

function f_ddlOnChange()
{
  var pdOff = <%= AspxTools.JsGetElementById(WillBePdOff) %>;
  var uRatio = <%= AspxTools.JsGetElementById(UsedInRatio) %>;
  var debt = <%= AspxTools.JsGetElementById(DebtT) %>;
  var isMortgage = getDropDownValue(debt) == 'Mortgage';
  
	if (isMortgage)
	{
	  uRatio.checked = false;
	  uRatio.disabled = true;
	}
	else
	  uRatio.disabled = false;

}

function getDropDownValue(ddl) {
      return $j.trim($j(ddl.options[ddl.selectedIndex]).text());
    }  
//-->
</script>
    <h4 class="page-header">Add Liability</h4>
	<form id="AddLiability" method="post" runat="server">
        <div id=MainFrame style="MARGIN:15px;OVERFLOW:auto;padding-left:10px;">
	        <table cellSpacing=0 cellPadding=1>
				<tr>
					<td class=FieldLabel width="150px" noWrap>Owner</td>
					<td noWrap><asp:dropdownlist id=OwnerT width="90px" runat="server">
							<asp:ListItem Value="0" Selected="True">Borrower</asp:ListItem>
							<asp:ListItem Value="1">Coborrower</asp:ListItem>
							<asp:ListItem Value="2">Joint</asp:ListItem>
						</asp:dropdownlist></td></tr>
				<tr>
					<td class=FieldLabel noWrap>Debt type&nbsp;&nbsp;</td>
					<td noWrap><asp:dropdownlist id="DebtT" onclick="f_ddlOnChange();" width="90px" runat="server"></asp:dropdownlist></td></tr>
				<tr>
					<td class=FieldLabel noWrap>Company Name</td>
					<td noWrap><asp:textbox id=ComNm width="180px" runat="server"></asp:textbox></td></tr>
				<tr>
					<td class=FieldLabel noWrap>Company Address</td>
					<td noWrap><asp:textbox id="ComAddr" width="180px" runat="server"></asp:textbox></td></tr>
				<tr>
					<td class=FieldLabel noWrap></td>
					<td noWrap><asp:textbox id="ComCity" width="96px" runat="server"></asp:textbox><ml:statedropdownlist id="ComState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="ComZip" runat="server" preset="zipcode" width="40px"></ml:zipcodetextbox>
					</td></tr>
					
				<tr>
					<td class=FieldLabel noWrap>Account Holder Name</td>
					<td noWrap><asp:textbox id="AccNm" width="180px" runat="server"></asp:textbox></td></tr>
				<tr>
					<td class=FieldLabel noWrap>Account Number</td>
					<td noWrap><asp:textbox id="AccNum" width="180px" runat="server"></asp:textbox></td></tr>
				<tr>
					<td class=FieldLabel noWrap>Balance</td>
					<td noWrap><ml:moneytextbox id=Bal width="90px" runat="server" Value="$0.00" preset="money"></ml:moneytextbox></td></tr>
				<tr>
					<td class=FieldLabel noWrap>Payment</td>
					<td noWrap><ml:moneytextbox id="Pmt" width="90px" runat="server" Value="$0.00" preset="money"></ml:moneytextbox></td></tr>
				<tr>
					<td class=FieldLabel noWrap colspan="2"><asp:CheckBox id="WillBePdOff" onclick="f_cboxOnClick();" runat="server" Text="Will be paid off"></asp:CheckBox></td></tr>
				<tr>
					<td class=FieldLabel noWrap colspan="2"><asp:CheckBox id="UsedInRatio" runat="server" Checked="True" Text="Used in ratio"></asp:CheckBox></td></tr>
				<tr>
					<td align=center colspan="2" ><asp:Label id=m_error runat="server" Visible="false" style="FONT-WEIGHT:bold;COLOR:red;font-size:12px;"></asp:Label>&nbsp;</td></tr>
			</table>
        </div>
		<table width="100%" ><tr><td align=center><asp:Button id=m_btnOK runat="server" Text="Add" CssClass="ButtonStyle" Width="70px" onclick="m_btnOK_Click"></asp:Button>&nbsp;<input type="button" style="width:70px;" onclick="parent.LQBPopup.Hide()" value="Cancel" class="ButtonStyle"></td></tr></table>
     </form>
	</body>
</html>
