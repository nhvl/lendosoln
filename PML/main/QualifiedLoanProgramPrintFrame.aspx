<%@ Page language="c#" Codebehind="QualifiedLoanProgramPrintFrame.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.QualifiedLoanProgramPrintFrame" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" > 

<html>
	<head runat="server">
		<title>PriceMyLoan.com</title>
	</head>
	<script type="text/javascript">

	<!--

	function _init() {

		<%-- //resize( 780 , 580 ); --%>

		self.focus();

	}	
	
	function openEmail()
	{
        var sLId = <%= AspxTools.JsString(RequestHelper.LoanID) %>;
        var productId = <%= AspxTools.JsString(RequestHelper.GetGuid("productid")) %>;
        var lienqualifymodet = escape(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("lienqualifymodet")) %>);
        var version = escape(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("version")) %>);
        var uniquechecksum = escape(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("uniquechecksum")) %>);
        var productRate = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productRate")) %>;
        var productPoint = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productPoint")) %>;
        var productRawId = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productRawId")) %>;
        <%--
        //since the frmBody destination url doesn't have LQBPopup due to being an XSLT document, we have to
        //implement our own.
        //It would probably be a better idea to re-write this page to make it capable of showing an LQB Popup, 
        //but that would require re-doing the frameset, since frameset replaces a <body> tag.
        --%>
        
        var emailUrl = <%= AspxTools.JsString(VirtualRoot) %> + '/Main/EmailCertificate.aspx?loanid=' + sLId + '&productid=' + productId + '&lienqualifymodet=' + lienqualifymodet + '&version=' + version + '&uniquechecksum=' + uniquechecksum + '&productRate=' + productRate + '&productPoint=' + productPoint + '&productRawId=' + productRawId;
        $j('#frmPopup').attr('src', emailUrl);
        $j('#Main').attr('rows', '0, 0, *');
	}
	
	function closeEmail()
	{
        $j('#Main').attr('rows', '40, *');	    
        $j('#frmPopup').removeAttr('src');
	}

	function f_registerMe( sName ) { try
	{
		if( window.parent != null && window.parent.opener != null )
		{
			if( window.parent.opener.closed == false )
			{
				if( window.parent.opener.f_updateChildName != null )
				{
					window.parent.opener.f_updateChildName( sName );
				}
			}
		}
	}
	finally
	{
	}}
	
	function showNormalCert() {
         <%if(  this.IsDisplayDetailCert ) {%>
	        Dialog.openUrl(<%= AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/Main/" + m_frmBodyUrl + "&detailCert=1" ) %>, 'Certificate', 800, 600 );
        <%}%>
	}


	//-->

    </script>
    <frameset id='Main' rows="40,*" frameborder="yes" onload="_init(); f_registerMe( 'qualified' );" onbeforeunload="f_registerMe( '' );">
	    <frame id="frmMenu" name="frmMenu" src=<%= AspxTools.SafeUrl(m_frmMenuUrl) %> scrolling="no">
	    <frame id="frmBody" name="frmBody" src=<%= AspxTools.SafeUrl(m_frmBodyUrl) %>>
	    <frame id='frmPopup' name='frmPopup'>
    </frameset>
</html>
