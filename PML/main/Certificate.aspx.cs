using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using System.Xml;
using System.Xml.Xsl;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.ObjLib.Resource;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.main
{
	public partial class Certificate : LendersOffice.Common.BaseXsltPage
	{
		private string m_pmlLenderSiteId;

		private Guid BrokerID 
		{
			get { return ((PriceMyLoan.Security.PriceMyLoanPrincipal) Page.User).BrokerId; }
		}
        protected override string XsltFileLocation         
        {
            get
            {
                bool forceNoCustomCert = RequestHelper.GetBool("detailCert") && (Page.User as PriceMyLoan.Security.PriceMyLoanPrincipal)?.CanViewDualCertificate == true;
                return ResourceManager.Instance.GetResourcePath(ResourceType.PmlCertificateXslt, BrokerID, forceNoCustomCert); 
            }
        }
        private bool IsShowDebugInfo
        {
            get
            {
                AbstractUserPrincipal currentPrincipal = ((PriceMyLoan.Security.PriceMyLoanPrincipal)Page.User);
                // 2014-02-13 - dd - Allow user with internal admin to see detail debugging information.
                //    This will help SAE to investigate pricing issue with CMG hosting solution.
                // 4/17/2014 dd - OPM 173155 - When we are hosting the site, only display debug info
                //                             on LOAUTH.
                bool ret = false;
                if (ConstStage.IsLendingQBHostingServer)
                {
                    ret = ConstSite.DisplayPolicyAndRuleIdOnLpeResult;
                }
                else
                {
                    // 4/17/2014 dd - For CMG hosting
                    ret = ConstSite.DisplayPolicyAndRuleIdOnLpeResult || (currentPrincipal != null && currentPrincipal.HasPermission(Permission.CanModifyLoanPrograms));
                }
                return ret;

            }
        }
        protected override XsltArgumentList XsltParams 
        {
            get 

			{ 
				XsltArgumentList args = new XsltArgumentList();
				args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
				args.AddParam("VirtualRoot", "", Tools.VRoot);
                args.AddParam("IsMoreDetailCert", "", "True");
                args.AddParam("Timing", "", RequestHelper.GetSafeQueryString("timing"));
				// 10/19/06 mf - OPM 7610.  We want to mask SSN in PML
				if ( ! PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML )
					args.AddParam("MaskSsn", "", "True");

                PriceMyLoanPrincipal principal = Page.User as PriceMyLoanPrincipal;
                // 1/26/2006 dd - OPM 3825 - Allow usres with Admin & LockDesk role to see hidden adjustments
                //bp spec do not allow pml users to see this
                if (principal.ApplicationType == E_ApplicationT.LendersOffice && (principal.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms) ||
                    principal.IsInRole(ConstApp.ROLE_ADMINISTRATOR) || principal.IsInRole(ConstApp.ROLE_LOCK_DESK))) 
                {
                    args.AddParam("ShowHiddenAdj", "", "True");
                }

				// 10/11/07 mf. OPM 18269 - Users with new permission can see hidden stips (conditions)
				if (principal.HasPermission( LendersOffice.Security.Permission.CanViewHiddenInformation ) ||
					principal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
				{
					args.AddParam("ShowHiddenConditions", "", "True");
				}

                if(LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerDB.HidePmlCertSensitiveFields)
                {
                    args.AddParam("HideSensitiveFields", "", "True");
                }

                if (IsShowDebugInfo) 
                {
                    args.AddParam("ShowDebugInfo", "", "True");
                }

                bool IsNewPmlUIEnabled;
                BrokerDB CurrentBroker = BrokerDB.RetrieveById(BrokerID);
                if (CurrentBroker.IsNewPmlUIEnabled)
                {
                    IsNewPmlUIEnabled = true;
                }
                else
                {
                    var empDB = EmployeeDB.RetrieveById(BrokerID, principal.EmployeeId);
                    IsNewPmlUIEnabled = empDB.IsNewPmlUIEnabled;
                }
                args.AddParam("IsNewPmlUIEnabled", "", IsNewPmlUIEnabled.ToString());

                return args;
			}
        }

        private Guid RequestID 
        {
            get { return RequestHelper.GetGuid("requestid"); }
        }
		protected override void InitXslt() 
		{

			SqlParameter[] parameters = { new SqlParameter("@BrokerID", BrokerID) };

            Guid brokerPmlSiteId = Guid.Empty;
			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "RetrieveLenderSiteIDByBrokerID", parameters)) 
			{
				if (reader.Read()) 
				{
					brokerPmlSiteId = (Guid) reader["BrokerPmlSiteID"];
				}
			}
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, RequestHelper.GetGuid("loanid", Guid.Empty));
		}
        protected override void GenerateXmlData(XmlWriter writer) 
        {
            bool hasInvalidRateOptionError = RequestHelper.GetBool("hasInvalidRateOptionError");
            if (hasInvalidRateOptionError) 
            {
                XmlDocument errorDoc = new XmlDocument();
                XmlElement root = errorDoc.CreateElement("InvalidRateOptionVersion");
                errorDoc.AppendChild(root);

                errorDoc.WriteTo(writer);

                return;
            }

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(RequestID);

            if (null == resultItem || resultItem.IsErrorMessage) 
            {
                if (null != resultItem && resultItem.ErrorMessage == ErrorMessages.InvalidRateOptionVersion) 
                {
                    XmlDocument errorDoc = new XmlDocument();
                    XmlElement root = errorDoc.CreateElement("InvalidRateOptionVersion");
                    errorDoc.AppendChild(root);

                    errorDoc.WriteTo(writer);
                } 
                else 
                {
                    ErrorUtilities.DisplayErrorPage(new CBaseException(null == resultItem ? ErrorMessages.Generic : resultItem.ErrorMessage,
                        null == resultItem ? ErrorMessages.Generic : resultItem.ErrorMessage), false, Guid.Empty, Guid.Empty);
                }
                return;
            }
            ArrayList results = resultItem.Results;

            if (null == results)
                throw new CBaseException(PriceMyLoan.UI.Common.PmlErrorMessages.CertificateNullResult, "DistributeUnderwritingEngine.RetrieveResults return null for RequestID = " + RequestID);

            
            XmlDocument doc = (XmlDocument) results[0];
            #region // 2/28/2012 dd - OPM 78785 Merge visible adjustments with the same description on certificate.
            if (ConstSite.DisplayPolicyAndRuleIdOnLpeResult == false)
            {
                // 2/28/2012 dd - We do need to performance adjustment merge on LOAUTH.
                doc = UnderwritingResultItem.DedupAdjustmentDescription(doc);
            }
            #endregion 
            doc.WriteTo(writer);

        }

	}
}
