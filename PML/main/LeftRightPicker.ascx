﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftRightPicker.ascx.cs" Inherits="PriceMyLoan.main.LeftRightPicker" %>

<div class="left-right-picker">
    <div class="column right-column">
        <label runat="server" id="AssignedLabel" class="column-label"></label>
        <br />
        <select runat="server" id="Assigned" multiple="true" class="assigned-items"></select>
    </div>

    <div class="column center-column">
        <div>
            <button type="button" class="btn btn-default" id="Assign">Add >></button>
        </div>
        <div>
            <button type="button" class="btn btn-default remove-button" id="Remove"><< Remove</button>
        </div>
        <div>
            <button type="button" class="btn btn-default" id="MoveUp">Move Up</button>
        </div>
        <div>
            <button type="button" class="btn btn-default" id="MoveDown">Move Down</button>
        </div>
    </div>

    <div class="column left-column">
        <label runat="server" id="AvailableLabel" class="column-label"></label>
        <br />
        <select runat="server" id="Available" multiple="true" class="available-items"></select>
    </div>
</div>
<div class="left-right-picker-clear">&nbsp;</div>