﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using PriceMyLoan.UI;

namespace PriceMyLoan.main
{
    public partial class UserInformation : BaseUserControl
    {
        /// <summary>
        /// RequireCellPhone control.
        /// </summary>
        public HiddenField RequireCellPhone;

        /// <summary>
        /// RequiredFieldMessage control.
        /// </summary>
        public MeridianLink.CommonControls.EncodedLabel RequiredFieldMessage;

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.PageLoad);
            this.Init += new EventHandler(this.PageInit);
        }

        public Guid EmployeeId
        {
            get; set;
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            if(Page.IsPostBack)
            {
                return;
            }
            if (EmployeeId != Guid.Empty)
            {
                EmployeeDB empDB = new EmployeeDB(EmployeeId, PriceMyLoanUser.BrokerId);

                if (!empDB.Retrieve())
                {
                    throw new CBaseException(ErrorMessages.FailedToLoadUser, "Failed to load specified employee.");
                }

                if (EmployeeId != PriceMyLoanUser.EmployeeId && empDB.PmlExternalManagerEmployeeId != PriceMyLoanUser.EmployeeId)
                {
                    throw new CBaseException(ErrorMessages.FailedToLoadUser + " " + ErrorMessages.GenericAccessDenied, "Pml user attempts to edit user they do not supervise.");
                }

                this.m_FirstName.Text = empDB.FirstName;
                this.m_MiddleName.Text = empDB.MiddleName;
                this.m_LastName.Text = empDB.LastName;
                this.m_Suffix.Text = empDB.Suffix;

                m_NameOnLoanDocLock.Checked = empDB.NmOnLoanDocsLckd;
                if (empDB.NmOnLoanDocsLckd)
                {
                    this.m_NameOnLoanDoc.ReadOnly = false;
                    this.m_NameOnLoanDoc.Text = empDB.NmOnLoanDocs;
                }

                this.m_Phone.Text = empDB.Phone;
                this.m_Fax.Text = empDB.Fax;
                this.m_CellPhone.Text = empDB.PrivateCellPhone;
                this.IsCellphoneForMultiFactorOnly.Checked = empDB.IsCellphoneForMultiFactorOnly;
                this.m_Pager.Text = empDB.Pager;
                this.m_Email.Text = empDB.Email;
                this.chkTaskRelatedEmail.Checked = (empDB.TaskRelatedEmailOptionT == E_TaskRelatedEmailOptionT.ReceiveEmail);

                this.RequireCellPhone.Value = empDB.EnableAuthCodeViaSms.ToString();
            }

            this.NameOnLoanDocs.Visible = this.PriceMyLoanUser.IsPmlManager;
        }

        protected void PageInit(object sender, EventArgs e)
        {
            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;
            RegularExpressionValidator1.ErrorMessage = ErrorMessages.InvalidEmailAddress;
            SetRequiredValidatorMessage(FirstNameValidator);
            SetRequiredValidatorMessage(LastNameValidator);
            SetRequiredValidatorMessage(PhoneValidator);
            SetRequiredValidatorMessage(CellPhoneValidator);
            SetRequiredValidatorMessage(EmailValidator);
            Tools.BindSuffix(m_Suffix);
        }

        public void SubmitEmployeeContactInfoToEmployee(EmployeeDB employee)
        {
            if (employee == null || !employee.Retrieve())
            {
                throw new CBaseException(ErrorMessages.FailedToLoadUser, "Failed to load employee when submitting contact information.");
            }

            SetContactInfoToEmployee(employee);

            employee.Save(PrincipalFactory.CurrentPrincipal);
        }

        /// <summary>
        /// Accepts a loaded EmployeeDB and then sets the contact information. Does not save.
        /// </summary>
        /// <param name="employee">The loaded EmployeeDB.</param>
        public void SetContactInfoToEmployee(EmployeeDB employee)
        {
            employee.FirstName = LendersOffice.AntiXss.AspxTools.HtmlString(this.m_FirstName.Text);
            employee.MiddleName = this.m_MiddleName.Text;
            employee.LastName = this.m_LastName.Text;
            employee.Suffix = this.m_Suffix.Text;
            if (PriceMyLoanUser.IsPmlManager)
            {
                employee.NmOnLoanDocs = this.m_NameOnLoanDoc.Text;
                employee.NmOnLoanDocsLckd = this.m_NameOnLoanDocLock.Checked;
            }

            employee.Phone = this.m_Phone.Text;
            employee.Fax = this.m_Fax.Text;
            employee.PrivateCellPhone = this.m_CellPhone.Text;
            employee.IsCellphoneForMultiFactorOnly = this.IsCellphoneForMultiFactorOnly.Checked;
            employee.Pager = this.m_Pager.Text;
            employee.Email = this.m_Email.Text;
            employee.TaskRelatedEmailOptionT = this.chkTaskRelatedEmail.Checked ? E_TaskRelatedEmailOptionT.ReceiveEmail : E_TaskRelatedEmailOptionT.DontReceiveEmail;
        }
    }
}