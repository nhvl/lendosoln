<%@ Page language="c#" Codebehind="BrokerNotes.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.BrokerNotes" ValidateRequest="true"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
  <head runat="server">
		<title>BrokerNotes</title>
        <style>
            .w-400 { 
                width: 400px; 
            }
            .padding4 {
                padding: 4px;
            }
        </style>
  </head>
	<body MS_POSITIONING="FlowLayout" rightmargin=0 scroll=no topmargin=5>
		<script type="text/javascript">
<!--
function _init() {
    var notes = <%= AspxTools.JsGetElementById(sLpeNotesFromBrokerToUnderwriterNew) %>;
    
    // Focus on the textbox
    try {
        notes.focus();
    } catch (e) { } // We tried to focus on a textbox that was disabled. Do nothing.
    
    notes.value = notes.value; // Move the cursor to the end of the textbox
}

function f_onkeyup_note() {
    TextAreaMaxLength(<%= AspxTools.JsGetElementById(sLpeNotesFromBrokerToUnderwriterNew) %>, <%= AspxTools.JsNumeric(m_noteLength) %>);
}
//-->
		</script>
        <h4 class="page-header">Notes</h4>
		<form id="BrokerNotes" method="post" runat="server">
			<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td noWrap class="padding4">
						<asp:TextBox id="sLpeNotesFromBrokerToUnderwriterNew" runat="server" TextMode="MultiLine" Height="187px" onkeyup="f_onkeyup_note();" CssClass="w-400"></asp:TextBox></TD>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td noWrap align="center">
						<asp:Button id="m_btnOK" runat="server" Text="OK" CssClass="ButtonStyle" Width="50px" onclick="m_btnOK_Click"></asp:Button>&nbsp;&nbsp;<input type="button" value="Cancel" onclick="parent.LQBPopup.Hide()" class="ButtonStyle" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
