<%@ Page language="c#" Codebehind="baseframe.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.baseframe" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE>
<html>
<head runat="server">
    <style type="text/css">
        #frmMain {
            border: none;
            width: 100%;
            height: 100%;
            display: block;
        }
    </style>
	<title>PriceMyLoan</title>
</head>
<body>    
    <iframe src=<%= AspxTools.SafeUrl(m_url) %> name="frmMain" id="frmMain" onunload="document.cookie = '.PMLUSER=0; path=/; expires=Fri, 31 Dec 1999 23:59:59 GMT;';"></iframe>
    <div id="CalculationOverlay" class="Hidden"></div>
</body>
</html>
