using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using DataAccess;
using DataAccess.LoanComparison;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.ObjLib.Relationships;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOfficeApp.newlos.Template;
using PriceMyLoan.Common;
using PriceMyLoan.DataAccess;
using Toolbox;
using LODataAccess = DataAccess;
using LendersOffice.ObjLib.Resource;
using MeridianLink.CommonControls;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.main
{

    public partial class ConfirmationPage : PriceMyLoan.UI.BasePage
    {
        private const string UnableToSubmitErrorMessage = "Unable to submit loan program for the following reasons:<br>";
        private const string RateNotFoundErrorMessage = "Due to the new DTI, the selected rate option is no longer valid. Please select a different rate option.";


        protected int m_height = 380;

        protected bool m_sRAdjMarginRVisible = false;
        protected bool m_marginVisible = false;
        protected int m_noteLength = 700;
        private Guid m_secondLoanID;
        private string m_secondLoanName;
        private string m_firstLoanName;
        protected bool m_isRateOptionBlockForLock = false;

        private CPageData x_LoanWithPriceGroupData = null;
        /// <summary>
        /// a loan with price group data, and it is initloaded;
        /// </summary>
        private CPageData LoanWithPriceGroupData
        {
            get
            {
                if (x_LoanWithPriceGroupData == null)
                {
                    var loan = new CPageData(LoanID, new string[] { "sPriceGroup" });
                    loan.InitLoad();
                    x_LoanWithPriceGroupData = loan;
                }
                return x_LoanWithPriceGroupData;
            }
        }
        private bool DisplayPmlFeeIn100Format
        {
            get { return LoanWithPriceGroupData.sPriceGroup.DisplayPmlFeeIn100Format; }
        }

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new WorkflowOperation[] { 
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.UseOldSecurityModelForPml,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock,
                WorkflowOperations.RequestRateLockRequiresManualLock
            };
        }

        protected bool m_rateLockRequested
        {
            //Default to "false" if we weren't passed a value for requestratelock in the querystring
            get { return RequestHelper.GetBool("requestratelock"); }
        }

        protected bool m_isRateLockedAtSubmission 
        {
            get { return PriceMyLoanUser.IsRateLockedAtSubmission; }
        }
        protected bool m_isReturnToPipelineAfterClose = true;

        protected bool IsEnablePmlResults3Ui
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB.IsForcePML3 || (PrincipalFactory.CurrentPrincipal.Type == "B" && CurrentBroker.IsEnablePmlResults3Ui(PrincipalFactory.CurrentPrincipal) && (RequestHelper.GetBool("isBeta") || ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost || ConstAppDavid.CurrentServerLocation == ServerLocation.Development || ConstAppDavid.CurrentServerLocation == ServerLocation.DevCopy));
            }
        }


        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

        private bool? x_DisplayPmlFeeIn100Format;
        private bool m_displayPmlFeeIn100Format 
        { 
            // OPM 19525
            get 
            {
                if (x_DisplayPmlFeeIn100Format == null)
                {
                    var loan = new CPageData(LoanID, new string[] { "sPriceGroup" });
                    loan.InitLoad();
                    x_DisplayPmlFeeIn100Format = loan.sPriceGroup.DisplayPmlFeeIn100Format;
                }
                return x_DisplayPmlFeeIn100Format.Value;
            }
        } 

        public override bool IsReadOnly 
        {
            get { return false; }
        }

        private Guid BrokerID 
        {
            get { return PriceMyLoanUser.BrokerId; }
        }

        private BrokerDB x_currentBroker = null;
        private BrokerDB CurrentBroker 
        {
            get 
            {
                if (null == x_currentBroker) 
                {
                    x_currentBroker = BrokerDB.RetrieveById(BrokerID);
                }
                return x_currentBroker;
            }
        }

        private List<string> x_RateLockQuestions = null;
        private List<string> RateLockQuestions
        {
            get
            {
                if (null == x_RateLockQuestions)
                {
                    x_RateLockQuestions = CurrentBroker.RateLockQuestionsJson;
                }
                return x_RateLockQuestions;
            }
        }
        private PricingState m_pricingState = null; //filled on init
        private Guid EmployeeID 
        {
            get { return PriceMyLoanUser.EmployeeId; }
        }

        protected bool m_isDTIError 
        {
            get { return string.Equals(RequestHelper.GetSafeQueryString("dtierror"), "True", StringComparison.OrdinalIgnoreCase); }
        }
    
        protected bool m_has2ndLoan 
        {
            get { return string.Equals(RequestHelper.GetSafeQueryString("Has2ndLoan"), "True", StringComparison.OrdinalIgnoreCase); }
        }
        protected bool m_skip2ndLoan 
        {
            get { return string.Equals(RequestHelper.GetSafeQueryString("skip2nd"), "True", StringComparison.OrdinalIgnoreCase); }
        }

        protected bool m_isRerunMode 
        {
            get { return this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct; }
        }
        protected string m_secondLienMPmt
        {
            get
            {
                if (m_has2ndLoan && m_skip2ndLoan == false)
                {
                    return RequestHelper.GetSafeQueryString("payment");
                }
                return string.Empty;
            }
        }
        protected bool m_isBlockedRateLockSubmission 
        {
            get { return PriceMyLoanUser.IsRateLockedAtSubmission && (RequestHelper.GetBool("blocksubmit") || RequestHelper.GetBool("1stblocksubmit")); }
        }
        protected bool m_hasRateLockSubmissionUserWarningMessage 
        {
            get { return PriceMyLoanUser.IsRateLockedAtSubmission && m_rateLockSubmissionUserWarningMessage != ""; }
        }
        protected string m_rateLockSubmissionUserWarningMessage 
        {
            get 
            {
                // 12/13/2007 dd - Display warning message from 1st product first. If 1st product has no warning then look at warning for 2nd product.
                string str = RequestHelper.GetSafeQueryString("blocksubmitmsg");
                if (str == "" || str == null)
                    str = RequestHelper.GetSafeQueryString("1stblocksubmitmsg");

                if (null == str)
                    return "";

                return RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(LoanWithPriceGroupData.sPriceGroup.LockPolicyID.Value, PriceMyLoanUser.LpeRsExpirationByPassPermission, str, BrokerID);
            }
        }

        protected bool IsCitiNoSubmissionAllowed
        {
            get { return !CurrentBroker.IsPmlSubmissionAllowed; }
        }

        protected bool m_isPmlSubmissionAllowed  
        {
            // 1/24/2008 dd - OPM 19783 - Citi specific feature.
            // 7/17/2009 dd - We do not allow user to submit the loan if it is running inside Encompass EPass

            get { return CurrentBroker.IsPmlSubmissionAllowed && !IsEncompassIntegration; }
        }
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected bool m_isOptionARMUsedInPml 
        {
            get { return CurrentBroker.IsOptionARMUsedInPml; }
        }

        protected bool m_sIsIncomeAssetsNonRequiredFhaStreamline = false; //opm 43255 fs 12/08/09

        protected string m_url 
        {
            get 
            {
                // 1/25/2008 dd - Return url to launch citi website. OPM 19783
                return Utilities.SafeJsLiteralString(CurrentBroker.Url);
            }
        }

        private bool? x_allowSubmitForLock = null;
        protected bool m_allowSubmitForLock
        {
            get
            {
                if (x_allowSubmitForLock.HasValue == false)
                {
                        x_allowSubmitForLock = CurrentUserCanPerform(WorkflowOperations.RequestRateLockRequiresManualLock)
                            || CurrentUserCanPerform(WorkflowOperations.RequestRateLockCausesLock);
                }

                return x_allowSubmitForLock.Value;
            }

        }
        bool? x_useOldEngine = null;
        protected bool m_useOldEngine
        {
            get
            {
                if (x_useOldEngine.HasValue == false)
                {
                    x_useOldEngine = CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml);
                }

                return x_useOldEngine.Value;
            }
        }

        private List<string> m_denialReasons = null;
        private List<string> m_denialReasons2nd = null;

        protected bool HasDenialReasons
        {
            get { return (m_denialReasons != null && m_denialReasons.Count() > 0); }
        }

        protected bool HasDenialReasons2nd
        {
            get { return (m_denialReasons2nd != null && m_denialReasons2nd.Count() > 0); }
        }


        protected string m_pointPriceLabel
        {
            get { return m_displayPmlFeeIn100Format ? "Price" : "Point"; }
        }

        private LosConvert convert = new LosConvert();

        public decimal RequestRate
        {
            get
            {
                return convert.ToRate(RequestHelper.GetSafeQueryString("rate"));
            }
        }

        public decimal RequestPoint
        {
            get
            {
                return convert.ToRate(RequestHelper.GetSafeQueryString("point"));
            }
        }

        public decimal Request1stRate
        {
            get
            {
                return convert.ToRate(RequestHelper.GetSafeQueryString("1stRate"));
            }
        }

        public decimal Request1stMPmt
        {
            get
            {
                return convert.ToRate(RequestHelper.GetSafeQueryString("1stMPmt"));
            }
        }

        private string DisplayRatioString(string str) 
        {
            if (str == "-1.000")
                return "N/A";
            else
                return str;
        }

        private string BuildRequestedRate(string rate, string fee, string id) 
        {
            return string.Format("{0}:{1}:{2}", rate, fee, id);
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            ClientScript.GetPostBackEventReference(this, "");
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            if (RequestHelper.GetGuid("pinid", Guid.Empty) != Guid.Empty)
            {
                CCheckFor2ndLoan secondLoan = new CCheckFor2ndLoan(LoanID);
                secondLoan.InitLoad();
                var states = secondLoan.GetPricingStates();
                m_pricingState = states.GetState(RequestHelper.GetGuid("pinid"));

                if (m_pricingState == null)
                {
                    throw new CBaseException("The Pin State No Longer Exists", "Pin state is missing " + RequestHelper.GetGuid("pinid") + " for loan id" + LoanID);
                }

            }
            MakeSureNotDuplicate();


            
            #region OPM 6525 - Display Certificate Warning Stipulations.

            if (m_isDTIError == false
                && RequestHelper.GetSafeQueryString("submitmanually") != "True"
                && ( Page.IsPostBack == false
                || PriceMyLoanUser.HasPermission(Permission.CanApplyForIneligibleLoanPrograms)))
            {
                string uniqueChecksum = RequestHelper.GetSafeQueryString("uniqueChecksum");
                string version = RequestHelper.GetSafeQueryString("version");
                List<string> stipulationWarnings = ParseCertificate(version, uniqueChecksum);
                if (stipulationWarnings.Count > 0)
                {
                    this.StipulationWarnings.DataSource = stipulationWarnings;
                    this.StipulationWarnings.DataBind();
                }
                else
                {
                    this.StipulationWarnings.DataSource = new List<string> { "NO WARNING." };
                    this.StipulationWarnings.DataBind();
                }

                BindDenialReasons();
            }
            #endregion

            // OPM 67455

            // If there is at least one question, make sure all questions have answers when trimmed
            var questions = from q in RateLockQuestions
                            select new
                            {
                                Question = q
                            };

            if (questions.Count() > 0)
            {
                LockDeskQuestionsPanel.Visible = true;
                LockDeskQuestions.DataSource = questions;
                LockDeskQuestions.DataBind();
                foreach (RepeaterItem control in LockDeskQuestions.Items)
                {
                    var validator = control.FindControl("LockDeskAnswerValidator");
                    HtmlImage img = new HtmlImage();
                    img.Src = ResolveUrl("~/images/error_pointer.gif");
                    validator.Controls.Add(img);
                }
            }

            // 4/21/2009 dd - Disable all the ViewState for ml:PassThroughLiteral and ml:EncodedLiteral controls.
            DisableViewstateForLiteral(this);


            
        }
        private void DisableViewstateForLiteral(Control parentControl)
        {
            if (null == parentControl)
                return;

            foreach (Control c in parentControl.Controls)
            {
                if (c is Literal)
                {
                    c.EnableViewState = false;
                }
                else
                {
                    DisableViewstateForLiteral(c);
                }

            }
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (!m_isPmlSubmissionAllowed) 
            {
                SetupPmlSubmissionNotAllowed();
            }

            if ( PriceMyLoanConstants.IsEmbeddedPML ) 
            {
                // OPM 2113: Allow AE to enter Estimated Close Date
                // 05/07/07 mf OPM 10596. Corrected so all embedded users can save
                if (Page.IsPostBack)
                {
                    var selectStatementProvider = CSelectStatementProvider.GetProviderFor(
                        E_ReadDBScenarioType.InputTargetFields_NeedTargetFields,
                        new[] { nameof(CPageBase.sEstCloseDLckd), nameof(CPageBase.sEstCloseD) });
                    var dataLoan = new CPmlPageData(this.LoanID, "ConfirmationPage", selectStatementProvider);
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.sEstCloseDLckd = sEstCloseDLckd.Checked;
                    dataLoan.sEstCloseD_rep = sEstCloseD.Text;
                    dataLoan.Save();
                }
            }

            bool isSubmitManually = RequestHelper.GetSafeQueryString("submitmanually") == "True";

            if (isSubmitManually) 
            {
                SubmitManually();
                this.AddInitScriptFunction("f_displayCertificate");
                return;
            }
            string uniqueChecksum = RequestHelper.GetSafeQueryString("uniqueChecksum");


            string version = RequestHelper.GetSafeQueryString("version");

            m_height = m_has2ndLoan ? 860 : 750; // Height of this window.

            //bool is2ndSubmitManually = RequestHelper.GetSafeQueryString("manual2nd") == "True";
            string debugResultStartTicks = RequestHelper.GetSafeQueryString("debugResultStartTicks");

            if (!Page.IsPostBack)
            {
                if (RequestHelper.GetGuid("pinid", Guid.Empty) != Guid.Empty)
                {

                    CancelBtn.Value = "Cancel / Back To Comp Report";
                    CancelBtn2.Value = "Cancel / Back To Comp Report";
                    CancelBtn.Attributes.Add("onclick", "javascript:history.go(-1);");
                    CancelBtn2.Attributes.Add("onclick", "javascript:history.go(-1);");
                }
                #region When Page Is Load
                // Display Broker LpeSubmitAgreement
                BrokerDB broker = CurrentBroker;
                if (m_isDTIError) 
                {

                    RenderDtiErrorMessage(broker);
                    return; // Process can end here.
                }

                //opm 45311 fs 07/23/10
                LpeSubmitAgreement.InnerHtml = AspxTools.HtmlString(broker.LpeSubmitAgreement);

                // Display Note to underwriter.
                CPageData noteDataLoan = new CLpeBrokerNotesData(LoanID);
                noteDataLoan.InitLoad();

                m_sIsIncomeAssetsNonRequiredFhaStreamline = noteDataLoan.sIsIncomeAssetsNonRequiredFhaStreamline;
                sProdRLckdDays.Text = noteDataLoan.sProdRLckdDays_rep + " Days";    //av opm 41983
                sProdRLckdExpiredDLabel.Text = noteDataLoan.sProdRLckdExpiredDLabel;
                sLpeNotesFromBrokerToUnderwriterNew.Text = noteDataLoan.sLpeNotesFromBrokerToUnderwriterNew;
                sLpeNotesFromBrokerToUnderwriterHistory.Text = noteDataLoan.sLpeNotesFromBrokerToUnderwriterHistory;
                m_noteLength = Math.Min(700, 1001 - noteDataLoan.sLpeNotesFromBrokerToUnderwriterHistory.Length);

                // OPM 19783 - When submission is not allow then hide Message to Lender if it is empty.
                MessageToLenderPanel.Visible = m_isPmlSubmissionAllowed || noteDataLoan.sLpeNotesFromBrokerToUnderwriterNew != "";
                PreviousMessageUnderwriterPanel.Visible = noteDataLoan.sLpeNotesFromBrokerToUnderwriterHistory != "";
                if (PreviousMessageUnderwriterPanel.Visible) 
                {
                    m_height += 120; // Increase the window height by 120 if there broker has previous history.
                }

                if (PriceMyLoanConstants.IsEmbeddedPML) 
                {
                    sEstCloseD.Text = noteDataLoan.sEstCloseD_rep;
                    sEstCloseDLckd.Checked = noteDataLoan.sEstCloseDLckd;

                    // 9/19/2006 dd - OPM 5005 - Allow user to choose if they want to email to assign role
                    CAgentFields agent = noteDataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    RenderEmailCheckbox(IsEmailLoanOfficer, agent.EmailAddr, "Loan Officer");
                    CAgentFields brokerProcessor = noteDataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    RenderEmailCheckbox(IsEmailBrokerProcessor, brokerProcessor.EmailAddr, "Processor (External)");
                    if (!brokerProcessor.IsValid)
                    {
                        IsEmailBrokerProcessor.Visible = false;
                        IsEmailBrokerProcessor.Checked = false;
                    }
                    RenderEmailCheckbox(IsEmailLenderAccExec, noteDataLoan.sEmployeeLenderAccExecEmail, "Account Executive");
                    RenderEmailCheckbox(IsEmailUnderwriter, noteDataLoan.sEmployeeUnderwriterEmail, "Underwriter");
                    RenderEmailCheckbox(IsEmailProcessor, noteDataLoan.sEmployeeProcessorEmail, "Processor");
                    RenderEmailCheckbox(IsEmailLockDesk, noteDataLoan.sEmployeeLockDeskEmail, "Lock Desk");

                    var externalSecondary = noteDataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.ExternalSecondary, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    RenderEmailCheckbox(IsEmailExternalSecondary, externalSecondary.EmailAddr, "Secondary (External)");
                    if (!externalSecondary.IsValid)
                    {
                        IsEmailExternalSecondary.Visible = false;
                        IsEmailExternalSecondary.Checked = false;
                    }

                    var externalPostCloser = noteDataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.ExternalPostCloser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    RenderEmailCheckbox(IsEmailExternalPostCloser, externalPostCloser.EmailAddr, "Post-Closer (External)");
                    if (!externalPostCloser.IsValid)
                    {
                        IsEmailExternalPostCloser.Visible = false;
                        IsEmailExternalPostCloser.Checked = false;
                    }
                }

                bool isLockAction = RequestHelper.GetBool("requestratelock");
                // 09/07/06 mf - OPM 7024 Check default submission type
                if ( m_isRerunMode == false && m_isRateLockedAtSubmission == true )
                {
                    if (isLockAction)
                    {
                        RequestingRateLock.SelectedIndex = 1; // Lock
                    }
                    else
                    {
                        RequestingRateLock.SelectedIndex = 0; // Register
                    }

                }
                else if( m_isRerunMode)
                {
                    RequestingRateLock.SelectedIndex = isLockAction ? 1 : 0;
                }

                PriceMyLoan.UI.BasePage p = (PriceMyLoan.UI.BasePage )Page;

                if ( false == p.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml)  && false == p.CurrentUserCanPerform(WorkflowOperations.RegisterLoan))
                {
                    RequestingRateLock.SelectedIndex = 1;
                    RequestingRateLock.Items[0].Attributes.Add("disabled", "disabled");
                    RequestingRateLock.Items[0].Enabled = false;
                }

                if (m_skip2ndLoan)
                {
                    #region Skip 2nd Loan Scenario
                    CPageData dataLoan = new CHackGetFirstLoanSubmitInformation(LoanID);
                    dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                    dataLoan.InitLoad();

                    Guid firstLienLpId = RequestHelper.GetGuid("1stProductId", Guid.Empty);
                    decimal firstLienPoint = 0;

                    if (decimal.TryParse(RequestHelper.GetSafeQueryString("1stPoint"), out firstLienPoint) == false)
                    {
                        firstLienPoint = 0;
                    }

                    dataLoan.sNoteIR = this.Request1stRate;
                    dataLoan.sNoteIRSubmitted = this.Request1stRate;
                    dataLoan.sLOrigFPcSubmitted = firstLienPoint;

                    //if (m_isRerunMode) 
                    //{

                    //    // 7/19/2006 dd - If rerun mode, then display data pass from query string. In rerun mode I did not execute TemporaryAcceptFirstLoan
                    //    // therefore these value will be missing.
                    //    if (null != RequestHelper.GetSafeQueryString("1stRate")) 
                    //    {
                    //        dataLoan.sNoteIR_rep = RequestHelper.GetSafeQueryString("1stRate");
                    //        dataLoan.sNoteIRSubmitted_rep = RequestHelper.GetSafeQueryString("1stRate");
                    //    }

                    //    if (null != RequestHelper.GetSafeQueryString("1stPoint")) 
                    //    {
                    //        dataLoan.sLOrigFPcSubmitted_rep = RequestHelper.GetSafeQueryString("1stPoint");
                    //    }
                    //    if (null != RequestHelper.GetSafeQueryString("1stRawId")) 
                    //    {
                    //        dataLoan.sLpeRateOptionIdOf1stLienIn8020 = RequestHelper.GetSafeQueryString("1stRawId");
                    //    }


                    //} 

                    ProductName.Text = DistributeUnderwritingEngine.RetrieveLpTemplateName(firstLienLpId);
                    Rate.Text = dataLoan.sNoteIRSubmitted_rep;
                    Point.Text = CApplicantRateOption.PointFeeInResult(dataLoan.sPriceGroup.DisplayPmlFeeIn100Format, dataLoan.sLOrigFPcSubmitted_rep);
                    Payment.Text = dataLoan.sProThisMPmt_rep;
                    QRate.Text = dataLoan.sQualIR_rep;
                    TeaserRate.Text = dataLoan.sOptionArmTeaserR_rep;
                    if (dataLoan.sLpIsArmMarginDisplayed) 
                    {
                        m_marginVisible = true;
                        Margin.Text = dataLoan.sRAdjMarginR_rep;
                    }
                    Bottom.Text = DisplayRatioString(dataLoan.sQualBottomR_rep);


                    ClientScript.RegisterHiddenField("FirstLoanProductID", firstLienLpId.ToString());

                    
                    string requestedRate = BuildRequestedRate(dataLoan.sNoteIRSubmitted_rep, dataLoan.sLOrigFPcSubmitted_rep, dataLoan.sLpeRateOptionIdOf1stLienIn8020);
                    ClientScript.RegisterHiddenField("FirstLoanRequestedRate", requestedRate);
                    #endregion
                    return;
                }
                else if (m_has2ndLoan)
                {
                    #region Has 2nd Loan Scenario
                    CPageData dataLoan = new CHackGetFirstLoanSubmitInformation(LoanID);
                    dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                    dataLoan.InitLoad();

                    // 1/17/2011 dd - TODO: Load first lp id, first lien rate, first lien point.

                    Guid firstLienLpId = RequestHelper.GetGuid("1stProductId", Guid.Empty);

                    decimal firstLienNoteRate = 0;
                    decimal firstLienPoint = 0;

                    if (decimal.TryParse(RequestHelper.GetSafeQueryString("1stRate"), out firstLienNoteRate) == false)
                    {
                        firstLienNoteRate = 0;
                    }

                    if (decimal.TryParse(RequestHelper.GetSafeQueryString("1stPoint"), out firstLienPoint) == false)
                    {
                        firstLienPoint = 0;
                    }

                    dataLoan.sNoteIR = firstLienNoteRate;
                    dataLoan.sNoteIRSubmitted = firstLienNoteRate;
                    dataLoan.sLOrigFPcSubmitted = firstLienPoint;

                    


                    // 8/22/2005 dd - If user can apply ineligible loan program, then don't bother with checking if 1st loan program is valid.
                    if (!PriceMyLoanUser.HasPermission(LendersOffice.Security.Permission.CanApplyForIneligibleLoanPrograms)) 
                    {
                        //Guid sLpTemplateId = dataLoan.sLpTemplateId;
                        // Before display confirmation info, check to see if 
                        // user select second loan program is valid for 1st loan program.

                        string sTempLpeTaskMessageXml = "";

                        //if (m_isRerunMode) 
                        //{
                        //    sTempLpeTaskMessageXml = dataLoan.sTempLpeTaskMessageXml;
                        //}
                        //Tools.LogError("TODO: Look at sTempLpeTaskMessageXml");

                        Guid requestID = DistributeUnderwritingEngine.VerifyFirstLoan(PriceMyLoanUser, 
                            LoanID, firstLienLpId, firstLienNoteRate, firstLienPoint, Utilities.SafeHtmlString(RequestHelper.GetSafeQueryString("qualPmt")), sTempLpeTaskMessageXml, 
                            debugResultStartTicks, E_RenderResultModeT.Regular);

                        try 
                        {
                            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestID);
                            if (null == resultItem || resultItem.IsErrorMessage) 
                            {
                                throw new CBaseException(null == resultItem ? ErrorMessages.Generic : resultItem.ErrorMessage, null == resultItem ? ErrorMessages.Generic : resultItem.ErrorMessage);
                            }

                            ArrayList results = resultItem.Results;

                            if (null != results && results.Count > 0) 
                            {
                                
                                CApplicantPriceXml applicantPrice = (CApplicantPriceXml) results[0];

                                if (applicantPrice.Status != E_EvalStatus.Eval_Eligible) 
                                {
                                    string disqualifiedRules = "1st Loan Program Ineligible. Reasons:<br><br>";

                                    foreach (CStipulation s in applicantPrice.DenialReasons) 
                                    {
                                        disqualifiedRules += "<br>* " + Utilities.SafeHtmlString(s.Description);
                                    }

                                    // 10/15/2004 dd - Display error message to user indicate first loan is now ineligible.
                                    this.AddInitScriptFunctionWithArgs("f_displayError", AspxTools.JsString(disqualifiedRules));
                                    return;
                                }
                            }

                        } 
                        catch (DistributeTimeoutException) 
                        {
                            this.AddInitScriptFunctionWithArgs("f_displayError", AspxTools.JsString(JsMessages.LpeResultsTimeout));
                            return;
                        }                    
                    }



                    string requestedRate = "";

                    sLpTemplateNm.Text = DistributeUnderwritingEngine.RetrieveLpTemplateName(firstLienLpId);
                    //if (m_isRerunMode) 
                    //{

                    //    // 7/19/2006 dd - If rerun mode, then display data pass from query string. In rerun mode I did not execute TemporaryAcceptFirstLoan
                    //    // therefore these value will be missing.
                    //    if (null != RequestHelper.GetSafeQueryString("1stRate")) 
                    //    {
                    //        dataLoan.sNoteIR_rep = RequestHelper.GetSafeQueryString("1stRate");
                    //        dataLoan.sNoteIRSubmitted_rep = RequestHelper.GetSafeQueryString("1stRate");
                    //    }

                    //    if (null != RequestHelper.GetSafeQueryString("1stPoint")) 
                    //    {
                    //        dataLoan.sLOrigFPcSubmitted_rep = RequestHelper.GetSafeQueryString("1stPoint");
                    //    }
                    //    if (null != RequestHelper.GetSafeQueryString("1stRawId")) 
                    //    {
                    //        dataLoan.sLpeRateOptionIdOf1stLienIn8020 = RequestHelper.GetSafeQueryString("1stRawId");
                    //    }


                    //} 

                    dataLoan.sProOFinPmtPe_rep = Utilities.SafeHtmlString(RequestHelper.GetSafeQueryString("qualPmt"));

                    sNoteIRSubmitted.Text = dataLoan.sNoteIRSubmitted_rep;
                    sLOrigFPcSubmitted.Text = CApplicantRateOption.PointFeeInResult(dataLoan.sPriceGroup.DisplayPmlFeeIn100Format, dataLoan.sLOrigFPcSubmitted_rep);
                    sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
                    sQualIR.Text = dataLoan.sQualIR_rep;
                    sOptionArmTeaserR.Text = dataLoan.sOptionArmTeaserR_rep;
                    if (dataLoan.sLpIsArmMarginDisplayed) 
                    {
                        m_sRAdjMarginRVisible = true;
                        sRAdjMarginR.Text = dataLoan.sRAdjMarginR_rep;
                    }
                    sQualBottomR.Text = DisplayRatioString(dataLoan.sQualBottomR_rep);

                    requestedRate = BuildRequestedRate(dataLoan.sNoteIRSubmitted_rep, dataLoan.sLOrigFPcSubmitted_rep, dataLoan.sLpeRateOptionIdOf1stLienIn8020);

                    ClientScript.RegisterHiddenField("FirstLoanProductID", firstLienLpId.ToString());
                    ClientScript.RegisterHiddenField("FirstLoanRequestedRate", requestedRate);
                    #endregion
                }

                bool is2ndSubmitManually = RequestHelper.GetSafeQueryString("manual2nd") == "True";
                if (is2ndSubmitManually) 
                {
                    ProductName.Text = CPageBase.SubmitManuallyProgramName;
                } 
                else 
                {
                    ProductName.Text = RequestHelper.GetSafeQueryString("productName");
                    Rate.Text = this.RequestRate.ToString();
                    Point.Text = CApplicantRateOption.PointFeeInResult(DisplayPmlFeeIn100Format, this.RequestPoint.ToString());
                    Payment.Text = RequestHelper.GetSafeQueryString("payment");
                    QRate.Text = RequestHelper.GetSafeQueryString("qrate");
                    TeaserRate.Text = RequestHelper.GetSafeQueryString("teaserrate");
                    if (RequestHelper.GetSafeQueryString("margin") != "0.000") 
                    {
                        m_marginVisible = true;
                        Margin.Text = RequestHelper.GetSafeQueryString("margin");
                    }
                    Bottom.Text = DisplayRatioString(RequestHelper.GetSafeQueryString("bottom"));
                }
                #endregion

            } 
            else // if Page.IsPostBack
            {
                PerformActualSubmission(version, debugResultStartTicks, uniqueChecksum);

                // OPM 216377.
                VerifyAprMatch();
                VerifyCashToCloseMatch();
            }
        }

        private List<string> ParseCertificate(string version, string uniqueChecksum)
        {
            CPageData dataLoan = new CCheckFor2ndLoan(LoanID);
            dataLoan.InitLoad();

            string sTempLpeTaskMessageXml = "";

            string firstUniqueChecksum = RequestHelper.GetSafeQueryString("1stUniqueChecksum");

            if (m_isRerunMode)
            {
                sTempLpeTaskMessageXml = dataLoan.sTempLpeTaskMessageXml;
            }

            Guid lpePriceGroupId = dataLoan.sProdLpePriceGroupId;

            // 4/28/2009 dd - ProductIdList will contains a list of two product ids. The first will be loan product id of first lien.
            // The second will be loan product id of second lien.
            Guid[] productIdList = new Guid[] { Guid.Empty, Guid.Empty };
            string[] uniqueChecksumList = new string[] { String.Empty, String.Empty };

            if (m_has2ndLoan)
            {
                productIdList[0] = RequestHelper.GetGuid("1stProductId", Guid.Empty);
                productIdList[1] = RequestHelper.GetGuid("productid", Guid.Empty);

                uniqueChecksumList[0] = RequestHelper.GetSafeQueryString("1stUniqueChecksum");
                uniqueChecksumList[1] = uniqueChecksum;
            }
            else if (m_skip2ndLoan)
            {
                productIdList[0] = RequestHelper.GetGuid("1stProductId", Guid.Empty);
                uniqueChecksumList[0] = RequestHelper.GetSafeQueryString("1stUniqueChecksum");
            }
            else
            {
                productIdList[0] = RequestHelper.GetGuid("productid");
                uniqueChecksumList[0] = uniqueChecksum;
            }
            List<string> list = new List<string>();

            for (int i = 0; i < productIdList.Length; i++)
            {
                if (i > 2) throw new CBaseException(ErrorMessages.Generic, "Unsupport productIdList length in GetWarningStipulationOnCertificate");

                Guid requestID = Guid.Empty;

                Guid productId = productIdList[i];
                string checksum = uniqueChecksumList[i];
                
                if (productId == Guid.Empty)
                    continue;

                E_sLienQualifyModeT sLienQualifyModeT = i == 0 ? E_sLienQualifyModeT.ThisLoan : E_sLienQualifyModeT.OtherLoan;

                if (sLienQualifyModeT == E_sLienQualifyModeT.ThisLoan)
                {
                    string requestedRate = BuildRequestedRate(this.RequestRate.ToString(), this.RequestPoint.ToString(), RequestHelper.GetSafeQueryString("rawid"));
                    requestID = DistributeUnderwritingEngine.SubmitToEngineForRenderCertificate(this.PriceMyLoanUser, LoanID, productId, requestedRate,
                        sLienQualifyModeT, lpePriceGroupId, sTempLpeTaskMessageXml, version, E_RenderResultModeT.Regular, E_sPricingModeT.RegularPricing, checksum, m_pricingState, RequestHelper.GetSafeQueryString("parId"));
                }
                else
                {
                    decimal firstLienNoteRate = convert.ToRate(RequestHelper.GetSafeQueryString("1stRate"));
                    decimal firstLienPoint = convert.ToDecimal(RequestHelper.GetSafeQueryString("1stPoint"));
                    decimal firstLienMPmt = convert.ToMoney(RequestHelper.GetSafeQueryString("1stMPmt"));

                    string requestedRate = BuildRequestedRate(this.RequestRate.ToString(), this.RequestPoint.ToString(), RequestHelper.GetSafeQueryString("rawid"));
                    requestID = DistributeUnderwritingEngine.SubmitToEngineForRenderCertificate(
                        this.PriceMyLoanUser, LoanID, productId,
                        requestedRate,
                        sLienQualifyModeT, lpePriceGroupId, sTempLpeTaskMessageXml, version,
                        E_RenderResultModeT.Regular, E_sPricingModeT.RegularPricing, checksum,
                        RequestHelper.GetGuid("1stProductId", Guid.Empty),
                        firstLienNoteRate,
                        firstLienPoint,
                        firstLienMPmt,                    
                        m_pricingState, RequestHelper.GetSafeQueryString("parId"));
                }

                UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestID);
                if (null == resultItem || resultItem.IsErrorMessage)
                {
                    return list;
                }

                XmlDocument doc = (XmlDocument)resultItem.Results[0];

                XmlNodeList nodeList = doc.SelectNodes("/pml_cert/underwriting/stipulations/stipulation[@category='WARNING']");
                foreach (XmlElement el in nodeList)
                {
                    if (!list.Contains(el.InnerText))
                    {
                        // 4/28/2009 dd - Do not include duplicate warning.
                        list.Add(el.InnerText);
                    }
                }

                XmlNodeList lockedRates = doc.SelectNodes("/pml_cert/pricing/rate_options/rate_option[@disable_lock='True']");
                foreach (XmlElement rateOption in lockedRates)
                {
                    foreach (XmlElement el in rateOption.ChildNodes)
                    {
                        if (el.Name == "rate" && el.InnerText == this.RequestRate.ToString() )
                        {
                            m_isRateOptionBlockForLock = true;
                            m_noLockText.Text = "(" + rateOption.Attributes["disable_lock_reason"].Value + ")";
                        }
                    }
                }

                List<string> denialReasons = new List<string>();
                if (PriceMyLoanUser.HasPermission(Permission.CanApplyForIneligibleLoanPrograms))
                {
                    // Get regular denials
                    XmlNodeList denialNodes = doc.SelectNodes("/pml_cert/underwriting/denial_reasons/denial_reason");

                    foreach (XmlElement el in denialNodes)
                    {
                        string reason = el.InnerText;
                        if (denialReasons.Contains(reason) == false)
                        {
                            denialReasons.Add(reason);
                        }
                    }
                    
                    string rateIdKey = (m_has2ndLoan && sLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) ?  "1stRawId" : "RawId";
                    string chosenRateId = RequestHelper.GetSafeQueryString(rateIdKey);
                    // Get Per-rate denials
                    if (dataLoan.BrokerDB.IsUsePerRatePricing
                        && !dataLoan.BrokerDB.UseRateOptionVariantMI)  // With rate variant PMI, this is manually done.
                    {
                        XmlNodeList deniedRates = doc.SelectNodes("/pml_cert/pricing/rate_options/rate_option[@disqualified='True']");
                        foreach (XmlElement rateOption in deniedRates)
                        {
                            foreach (XmlElement el in rateOption.ChildNodes)
                            {
                                // Add it only if it applies to the rate option they want.

                                if (el.Name == "rateOptionId" && el.InnerText == chosenRateId)
                                {
                                    string reason = rateOption.Attributes["disqualreason"].Value;
                                    if (denialReasons.Contains(reason) == false)
                                    {
                                        denialReasons.Add(rateOption.Attributes["disqualreason"].Value);
                                    }
                                }
                            }
                        }
                    }

                    if ( sLienQualifyModeT == E_sLienQualifyModeT.ThisLoan)
                    {
                        m_denialReasons = denialReasons;
                    }
                    else
                    {
                        m_denialReasons2nd = denialReasons;
                    }
                }
            }

            return list;
        }
        private void RenderDtiErrorMessage(BrokerDB broker)
        {
            StringBuilder sb = new StringBuilder();

            string oldRate = RequestHelper.GetSafeQueryString("oldrate");
            string oldPoint = RequestHelper.GetSafeQueryString("oldpoint");
            string newRate = this.RequestRate.ToString();
            string newPoint = RequestHelper.GetSafeQueryString("point");

            bool requestingRateLock = RequestHelper.GetBool("requestratelock");
            if (requestingRateLock)
            {
                RequestingRateLock.SelectedIndex = 1; // Request rate lock
            }
            else
            {
                RequestingRateLock.SelectedIndex = 0; // Request float
            }

            string prefix = "1st";
            if (m_has2ndLoan) prefix = "2nd";

            if (oldRate != "")
            {
                sb.AppendFormat("Selected rate option for {0} lien loan program has been adjusted due to the new DTI.", prefix);
                sb.AppendFormat("<table><tr><td style='padding-left:10px;color:black'>Selected: Rate =</td><td style='color:black'>{0}</td><td style='padding-left:10px;color:black'>Point = {1}</td></tr>", AspxTools.HtmlString(oldRate), AspxTools.HtmlString(CApplicantRateOption.PointFeeInResult(DisplayPmlFeeIn100Format, oldPoint)));
                sb.AppendFormat("<td style='padding-left:10px;color:black'>Adjusted: Rate =</td><td style='color:black'>{0}</td><td style='padding-left:10px;color:black'>Point = {1}</td></tr></table>", AspxTools.HtmlString(newRate), AspxTools.HtmlString(CApplicantRateOption.PointFeeInResult(DisplayPmlFeeIn100Format, newPoint)));
            }

            string old1stRate = RequestHelper.GetSafeQueryString("old1strate");
            string old1stPoint = RequestHelper.GetSafeQueryString("old1stpoint");
            string new1stRate = RequestHelper.GetSafeQueryString("new1strate");
            string new1stPoint = RequestHelper.GetSafeQueryString("new1stpoint");
            if (old1stRate != "" && m_has2ndLoan)
            {
                StringBuilder sb1 = new StringBuilder();
                sb1.Append("Selected rate option for 1st lien loan program has been adjusted due to the new DTI.");
                sb1.AppendFormat("<table><tr><td style='padding-left:10px;color:black'>Selected: Rate =</td><td style='color:black'>{0}</td><td style='padding-left:10px;color:black'>Point = {1}</td></tr>", AspxTools.HtmlString(old1stRate), AspxTools.HtmlString(CApplicantRateOption.PointFeeInResult(DisplayPmlFeeIn100Format, old1stPoint)));
                sb1.AppendFormat("<td style='padding-left:10px;color:black'>Adjusted: Rate =</td><td style='color:black'>{0}</td><td style='padding-left:10px;color:black'>Point = {1}</td></tr></table><br>", AspxTools.HtmlString(new1stRate), AspxTools.HtmlString(CApplicantRateOption.PointFeeInResult(DisplayPmlFeeIn100Format, new1stPoint)));

                sb.Insert(0, sb1.ToString());

            }


            sb.Append("<br><br>Would you like to confirm this submission with the adjusted rate option?");

            DTIErrorMessage.Text = sb.ToString();
        }

        private void SetupPmlSubmissionNotAllowed()
        {
            // 1/24/2008 dd - OPM 19783 - If submission is not allow then display loan number and borrower name at the confirmation screen.
            CPageData dataLoan = new CRequestRateLockData(LoanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            sLNm.Text = dataLoan.sLNm;
            aBNm.Text = dataLoan.GetAppData(0).aBNm_aCNm;

            if (!m_has2ndLoan)
            {
                // 1/25/2008 dd - Save the note rate, point, margin back to loan file, also apply the characteristic of a loan product. We save what user select to
                // loan file so that if user export FNMA file it will contains the rate user select. If user doing 80/20 then we
                // cannot store rate/fee to loan file. 
                // Here are assumptions we have:
                //   - Citi will only be using PML and not LendingQB
                //   - Citi does not do 80/20 combo.

                Guid productId = RequestHelper.GetGuid("productid", Guid.Empty);
                ILoanProgramTemplate product = null;

                if (productId != Guid.Empty)
                {
                    product = Utils.RetrieveLoanProgramTemplate(this.CurrentBroker, productId);
                }

                if (null != product)
                {
                    dataLoan.ApplyLoanProductTemplate(product, false);
                }
                if (null != RequestHelper.GetSafeQueryString("rate"))
                {
                    dataLoan.sNoteIR_rep = this.RequestRate.ToString();
                }

                if (null != RequestHelper.GetSafeQueryString("point"))
                {
                    dataLoan.sBrokComp1Lckd = false;
                    dataLoan.sBrokComp1MldsLckd = false;
                    dataLoan.sBrokComp1Desc = "YIELD SPREAD PREMIUM";
                    dataLoan.sBrokComp1Pc_rep = RequestHelper.GetSafeQueryString("point");
                }
                if (null != RequestHelper.GetSafeQueryString("margin"))
                {
                    dataLoan.sRAdjMarginR_rep = RequestHelper.GetSafeQueryString("margin");
                }

                dataLoan.Save();
            }
        }

        private void RenderEmailCheckbox(CheckBox cb, string email, string role) 
        {
            cb.Text = role;

            if (email != "")
            {
                cb.Checked = true;
                cb.Text += ": " + email;
            }
            else // no email
            {
                if (role == "Underwriter" || role == "Lock Desk")
                {
                    cb.Checked = true; // even if email == "", check it for Underwriters and Lock Desks (because they might be assigned after submission)
                    cb.Text += " to be assigned at submission (if any)";
                }
                else // Loan Officer, External Processor, External Secondary, External Post-Closer
                {
                    cb.Enabled = false;
                    if (role == "Loan Officer" || role == "Processor (External)" ||
                        role == "Secondary (External)" || role == "Post-Closer (External)")
                    {
                        cb.ToolTip = "No email address found for this agent";
                    }
                    else
                    {
                        cb.ToolTip = "No user assigned to this role";
                    }
                    cb.Style.Add( "cursor", "help" );
                }
            }

            if (role == "Lock Desk") // OPM 9831
            {
                cb.Checked = true;
                cb.Enabled = false;
            }
        }

        // OPM 24638
        private void SaveDataTracLoanProgramName(Guid productId)
        {
            if (Guid.Empty == productId)
            {
                return;
            }

            if (!CurrentBroker.IsDataTracIntegrationEnabled)
            {
                return;
            }

            
            CPageData dataLoan = new CDataTracLoanProgramData(LoanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveDataTracLpId", new SqlParameter("@lLpTemplateId", productId)))
                {
                    if(reader.Read())
                    {
                        dataLoan.sDataTracLpId = reader["lDataTracLpId"].ToString();
                        dataLoan.Save();
                    }
                }
            }
            catch{}
        }
        private void RerouteToDuplicatePage()
        {
            Response.Redirect("DuplicateLoanSubmissionAlertPage.htm", true);
        }

        private void MakeSureNotDuplicate()
        {
            if (m_pricingState == null)
            {
                var dataLoan = new CPageData(LoanID, new string[] { "sSpAddr" });
                dataLoan.InitLoad();
                
                if (DuplicateFinder.ExistDuplicateLoansPer105723(LoanID, BrokerID, dataLoan.sSpAddr))
                {
                    RerouteToDuplicatePage();
                }
                
            }
            else
            {
                
                if (DuplicateFinder.ExistDuplicateLoansPer105723(LoanID, BrokerID, m_pricingState))
                {
                    RerouteToDuplicatePage();
                }
                
            }
            
        }

        private void VerifySubmissionPermissions()
        {
            var isRegister = this.RequestingRateLock.SelectedItem.Value == "0";
            var isLock = this.RequestingRateLock.SelectedItem.Value == "1";

            if (isLock)
            {
                var canPerformLock = this.CurrentUserCanPerformAny(
                    WorkflowOperations.RequestRateLockCausesLock,
                    WorkflowOperations.RequestRateLockRequiresManualLock);

                if (!canPerformLock)
                {
                    throw new AccessDenied("User does not have permission to lock.");
                }
            }

            if (isRegister)
            {
                var canUseOldSecurityModel = this.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml);
                var canRegisterLoan = this.CurrentUserCanPerform(WorkflowOperations.RegisterLoan);

                if (!canUseOldSecurityModel && !canRegisterLoan)
                {
                    throw new AccessDenied("User does not have permission to register.");
                }
            }
        }

        private void UpdateProMInsInfo()
        {
            // This needs to be done, and doesn't come from UI data, so might as well give it full access.
            var dataLoan = new CFullAccessPageData(LoanID, new string[] { "sfUpdate_sProMInsFieldsPer110559" });
            dataLoan.ByPassFieldSecurityCheck = true;  // might as well make it faster.
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.Update_sProMInsFieldsPer110559();
            if (dataLoan.sLT == E_sLT.FHA)
            {
                dataLoan.Save();
            }
        }

        private void BindDenialReasons()
        {
            // 1st
            if (m_denialReasons != null && m_denialReasons.Count > 0)
            {
                var denialReasons = from r in m_denialReasons
                                    select new
                                    {
                                        DenialReason = r.Replace("<br />", System.Environment.NewLine)
            };

                DenialReasonsPanel.Visible = true;
                DenialReasonsRepeater.DataSource = denialReasons;
                DenialReasonsRepeater.DataBind();

                foreach (RepeaterItem control in DenialReasonsRepeater.Items)
                {
                    var validator = control.FindControl("ExceptionReasonValidator");
                    HtmlImage img = new HtmlImage();
                    img.Src = ResolveUrl("~/images/error_pointer.gif");
                    validator.Controls.Add(img);
                }
            }

            // 2nd
            if (m_denialReasons2nd != null && m_denialReasons2nd.Count > 0)
            {
                var denialReasons = from r in m_denialReasons2nd
                                    select new
                                    {
                                        DenialReason2nd = r.Replace("<br />", System.Environment.NewLine)
            };

                DenialReasonsPanel.Visible = true;
                DenialReasons2ndPanel.Visible = true;
                DenialReasons2ndRepeater.DataSource = denialReasons;
                DenialReasons2ndRepeater.DataBind();

                foreach (RepeaterItem control in DenialReasons2ndRepeater.Items)
                {
                    var validator = control.FindControl("ExceptionReason2ndValidator");
                    HtmlImage img = new HtmlImage();
                    img.Src = ResolveUrl("~/images/error_pointer.gif");
                    validator.Controls.Add(img);
                }
            }


        }

        private List<DenialException> GetDenialExceptionReasons()
        {
            var exceptionReasons = new List<DenialException>();

            foreach (RepeaterItem item in DenialReasonsRepeater.Items)
            {
                Label denialReasonLtl = (Label)item.FindControl("DenialReason");
                TextBox exceptionReasonTb = (TextBox)item.FindControl("ExceptionReason");
                exceptionReasons.Add(new DenialException() { DenialReason = denialReasonLtl.Text, ExceptionReason = exceptionReasonTb.Text, LienPosT= E_sLienPosT.First });
            }

            foreach (RepeaterItem item in DenialReasons2ndRepeater.Items)
            {
                Label denialReasonLtl = (Label)item.FindControl("DenialReason2nd");
                TextBox exceptionReasonTb = (TextBox)item.FindControl("ExceptionReason2nd");
                exceptionReasons.Add(new DenialException() { DenialReason = denialReasonLtl.Text, ExceptionReason = exceptionReasonTb.Text, LienPosT = E_sLienPosT.Second });
            }

            return exceptionReasons;
        }

        private void PerformActualSubmission(string version, string debugResultStartTicks, string uniqueChecksum) 
        {
            CPageData dataLoan = new CStatusOnlyData(LoanID);
            dataLoan.InitLoad();

            this.VerifySubmissionPermissions();
            MakeSureNotDuplicate(); 
            SaveNotesFromBrokerToUnderwriter();

            var denialExceptions = GetDenialExceptionReasons();

            bool submissionSucceeded = false;
            E_LpeRunModeT lpeRunModeT = dataLoan.lpeRunModeT;

            //OPM 91634: automatically lock the loan on request if the workflow engine says to do so on a rate lock request.
            bool autoLock = RequestingRateLock.SelectedItem.Value == "1" 
                && CurrentUserCanPerform(WorkflowOperations.RequestRateLockCausesLock);

            if (lpeRunModeT == E_LpeRunModeT.Full || lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
            {
                if (m_skip2ndLoan) 
                {
                    submissionSucceeded = SubmitRate(new Guid(Request.Form["FirstLoanProductID"]), Request.Form["FirstLoanRequestedRate"], RequestHelper.GetSafeQueryString("_1stVersion"), debugResultStartTicks, RequestHelper.GetSafeQueryString("_1stUniqueChecksum"), denialExceptions);

                    if (submissionSucceeded)
                    {
                        if (IsEncompassIntegration)
                        {
                            HandleEncompassIntegration();
                        }
                        else
                        {
                            this.AddInitScriptFunction("f_displayCertificate");
                        }
                    }
                } 
                else if (m_has2ndLoan) 
                {
                    string requestedRate = BuildRequestedRate(this.RequestRate.ToString(), this.RequestPoint.ToString(), RequestHelper.GetSafeQueryString("rawid"));

                    Guid firstLienLpId = RequestHelper.GetGuid("1stProductId", Guid.Empty);

                    decimal firstLienPoint = 0;

                    if (decimal.TryParse(RequestHelper.GetSafeQueryString("1stPoint"), out firstLienPoint) == false)
                    {
                        firstLienPoint = 0;
                    }

                    string firstLienUniqueChecksum = RequestHelper.GetSafeQueryString("1stUniqueChecksum");
                    string secondLienUniqueChecksum = uniqueChecksum;

                    submissionSucceeded = SubmitRate80_20(RequestHelper.GetGuid("productid", Guid.Empty), requestedRate, version, debugResultStartTicks, 
                        firstLienLpId, this.Request1stRate, firstLienPoint, this.Request1stMPmt, firstLienUniqueChecksum, secondLienUniqueChecksum, denialExceptions);

                    if (submissionSucceeded) 
                    {
                        if (IsEncompassIntegration)
                        {
                            HandleEncompassIntegration();
                        }
                        else
                        {

                            this.AddInitScriptFunctionWithArgs("f_displayTwoCertificates", string.Format("'{0}', '{1}', '{2}', '{3}'",
                                LoanID, m_secondLoanID, Utilities.SafeJsString(m_firstLoanName), Utilities.SafeJsString(m_secondLoanName)));
                        }
                    }

                }
                else 
                {
                    string requestedRate = BuildRequestedRate(this.RequestRate.ToString(), this.RequestPoint.ToString(), RequestHelper.GetSafeQueryString("rawid"));

                    submissionSucceeded = SubmitRate(RequestHelper.GetGuid("productid"), requestedRate, version, debugResultStartTicks, uniqueChecksum, denialExceptions);

                    if (submissionSucceeded) 
                    {
                        if (IsEncompassIntegration)
                        {
                            HandleEncompassIntegration();
                        }
                        else
                        {
                            this.AddInitScriptFunction("f_displayCertificate");
                        }
                    }
                }
            } 
            else 
            {
                this.AddInitScriptFunction("f_goToPipeline");
            }

            SaveDataTracLoanProgramName(RequestHelper.GetGuid("productid", Guid.Empty)); // OPM 24638

            if (submissionSucceeded && RequestingRateLock.SelectedItem.Value == "1") //lock rate
            {
                if (autoLock && !(RequestHelper.GetSafeQueryString("submitmanually") == "True" || RequestHelper.GetSafeQueryString("manual2nd") == "True"))
                {
                    Utilities.LockRate(LoanID, E_ApplicationT.PriceMyLoan);
                    if (m_has2ndLoan)
                    {
                        Utilities.LockRate(m_secondLoanID, E_ApplicationT.PriceMyLoan);
                    }
                }
            }

            UpdateProMInsInfo();
        }

        private void HandleEncompassIntegration()
        {
            if (!IsEncompassIntegration)
            {
                return; // This function only valid for Encompass integration.
            }
            CPageData dataLoan = new CStatusOnlyData(LoanID);
            // 7/30/2009 dd - When submit within Encompass integration we want to leave the loan as open.
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sStatusT = E_sStatusT.Loan_Open;
            dataLoan.sStatusLckd = true;
            dataLoan.Save();
            this.AddInitScriptFunction("f_sendToEncompass");
        }

        /// <summary>
        /// Saves the notes written in by the user, as well as the rate lock answers, if there are any
        /// </summary>
        private void SaveNotesFromBrokerToUnderwriter() 
        {
            if (sLpeNotesFromBrokerToUnderwriterNew.Text != "" || RateLockQuestions.Count > 0) 
            {

                CPageData noteDataLoan = new CLpeBrokerNotesData(LoanID);
                noteDataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (sLpeNotesFromBrokerToUnderwriterNew.Text != "")
                {
                    noteDataLoan.sLpeNotesFromBrokerToUnderwriterHistory = DateTime.Now.ToShortDateString() + Environment.NewLine +
                        Environment.NewLine + sLpeNotesFromBrokerToUnderwriterNew.Text + Environment.NewLine + "---------------" + Environment.NewLine +
                        Environment.NewLine + noteDataLoan.sLpeNotesFromBrokerToUnderwriterHistory;
                    noteDataLoan.sLpeNotesFromBrokerToUnderwriterNew = ""; // Clear out current notes.
                }

                if (RateLockQuestions.Count > 0 && RequestingRateLock.SelectedItem.Value == "1")
                {
                    var questionsAnswers = new List<string>(12);
                    int cur = 0; // The current question
                    foreach (RepeaterItem item in LockDeskQuestions.Items)
                    {
                        TextBox answerTextBox = (TextBox)item.FindControl("LockDeskAnswer");
                        string questionAnswer = string.Format(
                            "{0}. {1}" + Environment.NewLine + "* {2}" + Environment.NewLine,
                            cur + 1,
                            RateLockQuestions[cur],
                            answerTextBox.Text
                        );
                        questionsAnswers.Add(questionAnswer);
                        cur++;
                    }
                    string oldNotes = noteDataLoan.sLpeNotesFromBrokerToUnderwriterHistory;
                    noteDataLoan.sLpeNotesFromBrokerToUnderwriterHistory = DateTime.Now.ToShortDateString() + Environment.NewLine +
                        Environment.NewLine +
                        string.Join(Environment.NewLine, questionsAnswers.ToArray()) +
                        "---------------" + Environment.NewLine +
                        Environment.NewLine +
                        oldNotes;
                }

                noteDataLoan.Save();
            }

        }


        private void HandleRateOptionChange(string msg, Guid productID, string version, string requestRateLock) 
        {
            string str = msg.Substring(16);
            string[] parts = str.Split('|');
            string oldRate = parts[0];
            string oldPoint = parts[1];
            string rate = parts[2];
            string point = parts[3];
            string rawid = "";

            string old1stRate = "";
            string old1stPoint = "";
            string new1stRate = "";
            string new1stPoint = "";

            if (parts.Length == 5) 
            {
                rawid = parts[4];
            } 
            else if (parts.Length == 9) 
            {
                old1stRate = parts[4];
                old1stPoint = parts[5];
                new1stRate = parts[6];
                new1stPoint = parts[7];
                rawid = parts[8];
            }
            else 
            {
                throw new CBaseException(ErrorMessages.Generic, "HandleRateOptionChange received incorrect format string: str=" + str);
            }


            Response.Redirect(string.Format("ConfirmationPage.aspx?loanid={0}&has2ndloan={1}&rate={2}&point={3}&productid={4}&rawid={5}&dtierror=True&oldRate={6}&oldPoint={7}&old1stRate={8}&old1stPoint={9}&new1stRate={10}&new1stPoint={11}&version={12}&requestratelock={13}", 
                LoanID, RequestHelper.GetSafeQueryString("has2ndloan"), rate, point, productID,
                rawid, oldRate, oldPoint, old1stRate, old1stPoint, new1stRate, new1stPoint, version, requestRateLock));

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
        private bool SubmitRate80_20(Guid productID, string requestedRate, string version, string debugResultStartTicks, Guid firstLienLpId,
            decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt, string firstLienUniqueChecksum, string secondLienUniqueChecksum, List<DenialException> denialExceptions) 
        {

            // 6/8/2005 dd -  Case 2053 - We are remove ability the user choose between rate float and rate lock. Therefore
            // default action to rate float.
            // 8/19/2005 dd - Case 2245 Allow user to choose between rate float and rate lock.
            bool isRequestingRateLock = false;

            string requestRateLockStr = "";
            if (m_isRateLockedAtSubmission) 
            {
                isRequestingRateLock = RequestingRateLock.SelectedItem.Value == "1";
                requestRateLockStr = isRequestingRateLock ? "t" : "f";
            }

            CPageData dataLoan = new CCheckFor2ndLoan(LoanID);
            dataLoan.InitLoad();

            string sTempLpeTaskMessageXml = "";

            if (m_isRerunMode) 
            {
                sTempLpeTaskMessageXml = dataLoan.sTempLpeTaskMessageXml;
            }

            Guid lpePriceGroupId = dataLoan.sProdLpePriceGroupId;
            Guid requestID = Guid.Empty;
            try 
            {

                bool skipRateAdjError = RequestHelper.GetSafeQueryString("dtierror") == "True";
                requestID = DistributeUnderwritingEngine.SubmitRate80_20(PriceMyLoanUser, LoanID, productID,
                    requestedRate, isRequestingRateLock, lpePriceGroupId, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, skipRateAdjError, version, sTempLpeTaskMessageXml, 
                    debugResultStartTicks, E_RenderResultModeT.Regular, firstLienUniqueChecksum, secondLienUniqueChecksum, denialExceptions);
            } 
            catch (CBaseException exc) 
            {
                // 10/19/2005 dd - Only DEVMINIMUM or AUTHOR will get exception here. Distribute model does not throw exception in this method.
                if (exc.UserMessage.StartsWith("RATE_CHANGE_ERR:")) 
                {
                    HandleRateOptionChange(exc.UserMessage, productID, version, requestRateLockStr);
                    return false;
                } 
                else if (exc.UserMessage == ErrorMessages.UnableToFindRateOption) 
                {
                    Tools.LogWarning(exc);
                    this.AddInitScriptFunctionWithArgs("f_displayError", "'" + RateNotFoundErrorMessage + "'");

                    return false;
                }
                else if (exc.UserMessage == ErrorMessages.InvalidRateOptionVersion) 
                {
                    // OPM 69509 - We still have to check versions to know if DTI mismatch occur.  If versions changed, display message
                    string currentVersion = dataLoan.BrokerDB.GetLatestReleaseInfo().DataLastModifiedD.ToString();
                    if (version != currentVersion)
                    {
                        // This means a real snapshot crossing--not due to DTI
                        this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                        return false;
                    }
                    else
                    {
                        CInvalidRateVersionException rateVersionExc = exc as CInvalidRateVersionException;

                        if (rateVersionExc != null && string.IsNullOrEmpty(rateVersionExc.RateOptionChange) == false)
                        {
                            HandleRateOptionChange(rateVersionExc.RateOptionChange, productID, version, requestRateLockStr);
                        }
                        else
                        {
                            this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                        }
                        return false;
                    }
                    
                }
                else if (exc.UserMessage.StartsWith("INELIGIBLE:"))
                {
                    this.AddInitScriptFunctionWithArgs("f_displayError", "'" + exc.UserMessage + "'");
                    return false;

                }
                throw;
            }

            try 
            {
                UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestID);
                if (null == resultItem || resultItem.IsErrorMessage) 
                {
                    if (null != resultItem) 
                    {
                        if (resultItem.ErrorMessage.StartsWith("RATE_CHANGE_ERR:")) 
                        {
                            HandleRateOptionChange(resultItem.ErrorMessage, productID, version, requestRateLockStr);
                            return false;
                        }
                        else if (resultItem.ErrorMessage.StartsWith("INELIGIBLE:")) 
                        {
                            string errorMessage = UnableToSubmitErrorMessage + resultItem.ErrorMessage.Substring(11);
                            this.AddInitScriptFunctionWithArgs("f_displayError", "'"+errorMessage+"'");
                            m_isReturnToPipelineAfterClose = false;
                            return false;
                        }

                        else if (resultItem.ErrorMessage == ErrorMessages.UnableToFindRateOption) 
                        {
                            Tools.LogWarning(resultItem.ErrorMessage);
                            this.AddInitScriptFunctionWithArgs("f_displayError", "'" + RateNotFoundErrorMessage + "'");

                            return false;
                        }
                        else if (resultItem.ErrorMessage == ErrorMessages.InvalidRateOptionVersion) 
                        {
                            // OPM 69509 - We still have to check versions to know if DTI mismatch occur.  If versions changed, display message
                            string currentVersion = dataLoan.BrokerDB.GetLatestReleaseInfo().DataLastModifiedD.ToString();
                            if (version != currentVersion)
                            {
                                // This means a real snapshot crossing--not due to DTI
                                this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                                return false;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(resultItem.RateChange) == false)
                                {
                                    HandleRateOptionChange(resultItem.RateChange, productID, version, requestRateLockStr);
                                }
                                else
                                {
                                    this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                                }
                                return false;
                            }

                        }
                        else if (RateOptionExpirationResult.IsBlockedRateLockSubmissionMessage(resultItem.ErrorMessage))
                        {
                            string errMsg = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(LoanWithPriceGroupData.sPriceGroup.LockPolicyID.Value, PriceMyLoanUser.LpeRsExpirationByPassPermission, resultItem.ErrorMessage, BrokerID);
                            this.AddInitScriptFunctionWithArgs("f_displayError", "'" + Utilities.SafeJsLiteralString(errMsg) + "'");
                            return false;
                            
                        }


                    }
                    throw new CBaseException(null == resultItem ? ErrorMessages.Generic : resultItem.ErrorMessage, null == resultItem ? "ResultItem is null" : resultItem.ErrorMessage);
                }
            } 
            catch (DistributeTimeoutException) 
            {
                this.AddInitScriptFunctionWithArgs("f_displayError", "'" + Utilities.SafeJsString(JsMessages.LpeResultsTimeout) + "'");
                return false;
            }

            dataLoan = new CSubmitFirstLoanData(LoanID);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitLoad();

            m_firstLoanName = dataLoan.sLNm;
            CLinkedLoanInfo sLinkedLoanInfo = dataLoan.sLinkedLoanInfo;
            m_secondLoanName = sLinkedLoanInfo.LinkedLNm;
            m_secondLoanID = sLinkedLoanInfo.LinkedLId;

            SendCertificate(dataLoan, false, isRequestingRateLock);

            // Send certificate of the 2nd loan.
            CPageData secondLoan = new CSubmitFirstLoanData(m_secondLoanID); // Too lazy to create a new dataaccess class.
            secondLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            secondLoan.InitLoad();

            SendCertificate(secondLoan, false, isRequestingRateLock);

            return true;

        }
        private bool SubmitRate(Guid productID, string requestedRate, string version, string debugResultStartTicks, string uniqueChecksum, List<DenialException> denialExceptions) 
        {
            // 06/08/2005 dd - Case 2053 - We are remove ability the user choose between rate float and rate lock. Therefore
            // default action to rate float.
            // 8/19/2005 dd - Case 2245 Allow user to choose between rate float and rate lock.
            bool isRequestingRateLock = false;
            string requestRateLockStr = "";
            if (m_isRateLockedAtSubmission) 
            {
                isRequestingRateLock = RequestingRateLock.SelectedItem.Value == "1";
                requestRateLockStr = isRequestingRateLock ? "t" : "f";
            }

            CPageData dataLoan = new CCheckFor2ndLoan(LoanID);
            dataLoan.InitLoad();
            Guid lpePriceGroupId = dataLoan.sProdLpePriceGroupId;

            Guid requestID = Guid.Empty;
            try 
            {
                requestID = DistributeUnderwritingEngine.SubmitRate(PriceMyLoanUser, LoanID, productID, requestedRate, isRequestingRateLock, 
                    lpePriceGroupId, version, debugResultStartTicks, E_RenderResultModeT.Regular, uniqueChecksum, m_pricingState, RequestHelper.GetSafeQueryString("parId"), denialExceptions);

            } 
            catch (CBaseException exc) 
            {
                // 10/19/2005 dd - Only DEVMINIMUM or AUTHOR will get exception here. Distribute model does not throw exception in this method.
                if (exc.UserMessage.StartsWith("RATE_CHANGE_ERR:")) 
                {
                    HandleRateOptionChange(exc.UserMessage, productID, version, requestRateLockStr);
                    return false;
                } 
                else if (exc.UserMessage == ErrorMessages.UnableToFindRateOption) 
                {
                    Tools.LogWarning(exc);
                    this.AddInitScriptFunctionWithArgs("f_displayError", "'" + RateNotFoundErrorMessage + "'");

                    return false;
                }
                else if (exc.UserMessage == ErrorMessages.InvalidRateOptionVersion) 
                {

                    // OPM 69509 - We still have to check versions to know if DTI mismatch occur.  If versions changed, display message
                    string currentVersion = dataLoan.BrokerDB.GetLatestReleaseInfo().DataLastModifiedD.ToString();
                    if (version != currentVersion)
                    {
                        // This means a real snapshot crossing--not due to DTI
                        this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                        return false;
                    }
                    else
                    {
                        CInvalidRateVersionException rateVersionExc = exc as CInvalidRateVersionException;

                        if (rateVersionExc != null && string.IsNullOrEmpty (rateVersionExc.RateOptionChange) == false)
                        {
                            HandleRateOptionChange(rateVersionExc.RateOptionChange, productID, version, requestRateLockStr);
                        }
                        else
                        {
                            this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                        }
                        return false;
                    }
                }

                throw;
            }

            try 
            {
                UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestID);
                if (null == resultItem)
                {
                    throw new CBaseException(ErrorMessages.Generic, "ResultItem is null");
                }

                if (resultItem.IsErrorMessage) 
                {
                    if (resultItem.ErrorMessage.StartsWith("RATE_CHANGE_ERR:"))
                    {
                        HandleRateOptionChange(resultItem.ErrorMessage, productID, version, requestRateLockStr);
                        return false;
                    }
                    else if (resultItem.ErrorMessage.StartsWith("INELIGIBLE:"))
                    {
                        string errorMessage = UnableToSubmitErrorMessage + resultItem.ErrorMessage.Substring(11);
                        this.AddInitScriptFunctionWithArgs("f_displayError", "'" + errorMessage + "'");
                        m_isReturnToPipelineAfterClose = false;
                        return false;
                    }
                    else if (resultItem.ErrorMessage == ErrorMessages.UnableToFindRateOption)
                    {
                        Tools.LogWarning(resultItem.ErrorMessage);
                        this.AddInitScriptFunctionWithArgs("f_displayError", "'" + RateNotFoundErrorMessage + "'");

                        return false;
                    }
                    else if (resultItem.ErrorMessage == ErrorMessages.InvalidRateOptionVersion)
                    {
                        // OPM 69509 - We still have to check versions to know if DTI mismatch occur.  If versions changed, display message
                        string currentVersion = dataLoan.BrokerDB.GetLatestReleaseInfo().DataLastModifiedD.ToString();
                        if (version != currentVersion)
                        {
                            // This means a real snapshot crossing--not due to DTI
                            this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                            return false;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(resultItem.RateChange) == false)
                            {
                                HandleRateOptionChange(resultItem.RateChange, productID, version, requestRateLockStr);
                            }
                            else
                            {
                                this.AddInitScriptFunctionWithArgs("f_displayError", "'" + ErrorMessages.InvalidRateOptionVersion + "'");
                            }
                            return false;
                        }

                    }
                    else if (RateOptionExpirationResult.IsBlockedRateLockSubmissionMessage(resultItem.ErrorMessage))
                    {
                        string errMsg = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(LoanWithPriceGroupData.sPriceGroup.LockPolicyID.Value, PriceMyLoanUser.LpeRsExpirationByPassPermission, resultItem.ErrorMessage, BrokerID);
                        this.AddInitScriptFunctionWithArgs("f_displayError", "'" + Utilities.SafeJsLiteralString(errMsg) + "'");
                        return false;
                    }
                    else
                    {
                        Tools.LogBug("Unhandled resultItem.ErrorMessage! " + resultItem.ErrorMessage);
                        throw new CBaseException(ErrorMessages.Generic, "Unhandled resultItem.ErrorMessage! {" + resultItem.ErrorMessage + "}");
                    }
                }
            }
            catch (DistributeTimeoutException)
            {
                this.AddInitScriptFunctionWithArgs("f_displayError", "'" + Utilities.SafeJsString(JsMessages.LpeResultsTimeout) + "'");
                return false;
            }

            dataLoan = new CRequestRateLockData(LoanID); // TODO: Replace with simpler dataLoan with only sEmployeeLenderExecEmail
            dataLoan.InitLoad();

            SendCertificate(dataLoan, false, isRequestingRateLock);

            return true;
        }

        private void SubmitManually() 
        {
            // OPM 10290 - Save note to lender when user submit manually.
            CPageData noteDataLoan = new CLpeBrokerNotesData(LoanID);
            noteDataLoan.InitLoad();
            sLpeNotesFromBrokerToUnderwriterNew.Text = noteDataLoan.sLpeNotesFromBrokerToUnderwriterNew;
            SaveNotesFromBrokerToUnderwriter();
     
            CPageData dataLoan = new CRequestRateLockData(LoanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.SubmitFromPML(null, 0, 0, 0, 0, 0, false /* isRequestingRateLock */, null); // 12/2/2004 dd - Submit manually, always use rate float approval.
            dataLoan.Save();

            // 8/25/2005 kb - Setup the underwriter using the assigned employees'
            // team relationships.  Once we get the event framework online, we
            // can handle this processing as part of event listening.
            // also set the lock desk - av 105197
            SetAssignmentsOnSubmit(
                PriceMyLoanUser.BrokerId,
                PriceMyLoanUser.EmployeeId,
                dataLoan.sLId,
                dataLoan.sBranchChannelT,
                dataLoan.sCorrespondentProcessT);

            dataLoan = new CCertificateNoPermissionCheckData(dataLoan.sLId);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sPmlCertXmlContent = GetCertificate(dataLoan, false /* isLoadStipFromCondition */);
            dataLoan.Save();

            // Audit.
            string first_lpResult = LoadResultHtmlFromFileDB("LPEFirstResult_" + dataLoan.sLId.ToString("N"));
            string second_lpResult = "";

            LoanSubmissionAuditItem auditItem = new LoanSubmissionAuditItem(PriceMyLoanUser, 
                ConstAppDavid.AuditEvent_PmlSubmission_Registered,
                dataLoan.sLpTemplateNm, 
                "", 
                "", 
                "", 
                "", 
                RenderCertificateToHtml(dataLoan.sPmlCertXmlContent.Value),
                null, null, null, null, null, null,
                "", false, first_lpResult, second_lpResult, null);

            AuditManager.RecordAudit(dataLoan.sLId, auditItem);

            if (PrincipalFactory.CurrentPrincipal != null && PrincipalFactory.CurrentPrincipal.BrokerId.Equals(new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")))
            {
                Tools.LogInfo("OPM_225020: A Lock Submission Audit Event was created for First Tech loan with Loan Id:  " + dataLoan.sLId.ToString() + " by User Id: " + PrincipalFactory.CurrentPrincipal.UserId.ToString() +
                    " with the following Stack Trace: \n" +
                    Environment.StackTrace);
            }


            // This is bad. because 3 separate loading of the same loan file.
            dataLoan = new CRequestRateLockData(LoanID);
            dataLoan.InitLoad();
            SendCertificate(dataLoan, true, false);

        }

        private string GetCertificate(CPageData dataLoan, bool isLoadStipFromCondition) 
        {

            using (MemoryStream stream = new MemoryStream(10000)) 
            {
                XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);
                CertificateXmlData.Generate(writer, dataLoan, null, null, E_sLienQualifyModeT.ThisLoan, this.PriceMyLoanUser, null, false /* isRateLockRequested */, isLoadStipFromCondition, dataLoan.sProdLpePriceGroupId
                   ,0);
                writer.Flush();

                stream.Seek(0, SeekOrigin.Begin);
                return System.Text.Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Length);
            }

        }

        private void VerifyAprMatch()
        {
            //Case 216377 - If the APR before and after submission don't match, log an error for future investigation.
            if (CurrentBroker.ApplyClosingCostToGFE)
            {
                // If this is a combo scenario, the APR from the query string is going to be the 2nd
                // lien's APR, while the APR from the LoanID is going to be the 1st lien's. These are
                // not expected to match, so don't log in that situation. Instead of ignoring this
                // situation, it would be good to eventually check both the 1st and 2nd lien's APRs
                // against what was returned in the pricing results.
                if (this.m_has2ndLoan)
                {
                    return;
                }

                string aprStr = RequestHelper.GetSafeQueryString("apr");
                if (String.IsNullOrEmpty(aprStr) == false && aprStr != "N/A")
                {
                    aprStr = aprStr + "%";

                    CPageData dataLoan = new CRequestRateLockData(LoanID);
                    dataLoan.InitLoad();
                    dataLoan.BypassEngineAmortizationCheck = true;

                    if (!aprStr.Equals(dataLoan.sApr_rep))
                    {
                        Tools.LogError("[APR Mismatch]  The loan product's APR value, " + aprStr + ", differs from the loan's actual APR value after PML submission, " + dataLoan.sApr_rep
                            + ".  Loan: " + dataLoan.sLNm + " (" + LoanID.ToString() + "), Product: " + dataLoan.sLpTemplateNm + " (" + dataLoan.sLpTemplateId + ")");
                    }
                }
            }
        }

        private void VerifyCashToCloseMatch()
        {
            //Case 216377 - If the APR before and after submission don't match, log an error for future investigation.
            if (CurrentBroker.ApplyClosingCostToGFE)
            {
                string totalcashcloseStr = RequestHelper.GetSafeQueryString("totalcashclose");
                if (String.IsNullOrEmpty(totalcashcloseStr) == false && totalcashcloseStr != "N/A")
                {
                    CPageData dataLoan = new CRequestRateLockData(LoanID);
                    dataLoan.InitLoad();
                    dataLoan.BypassEngineAmortizationCheck = true;

                    if (!totalcashcloseStr.Equals(dataLoan.sTransNetCash_rep))
                    {
                        Tools.LogError("[Cash to Close Mismatch]  The loan product's Cash to Clos value, " + totalcashcloseStr + ", differs from the loan's actual Cash to Close value after PML submission, " + dataLoan.sTransNetCash_rep
                            + ".  Loan: " + dataLoan.sLNm + " (" + LoanID.ToString() + "), Product: " + dataLoan.sLpTemplateNm + " (" + dataLoan.sLpTemplateId + ")");
                    }
                }
            }
        }

        #region Send Certificate to official agent
        private string m_pmlLenderSiteId = null;

        private XsltArgumentList XsltParams 
        {
            get 
            {
                string css = ResourceManager.Instance.GetResourceContents(ResourceType.CertificateCss, BrokerID);


                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("VirtualRoot", "", Tools.VRoot);
                args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
                args.AddParam("GenericStyle", "", css);
                args.AddParam("HideJs", "", "True");
                args.AddParam("Email", "", "True");

                if (PrincipalFactory.CurrentPrincipal.BrokerDB.HidePmlCertSensitiveFields)
                {
                    args.AddParam("HideSensitiveFields", "", "True");
                }

                return args;  
            }
        }

        /// <summary>
        /// OPM 1850 - Email certificate to official agent
        /// </summary>
        /// <param name="dataLoan"></param>
        private void SendCertificate(CPageData dataLoan, bool isSubmitManually, bool isRateLockRequest) 
        {
            // 7/30/2009 dd - Don't send certificate if submit from Encompass integration.
            // 11/25/2009 dd - Base on eOPM 188243, I am allow certificate to be send even when submit from Encompass.
            //if (IsEncompassIntegration)
            //{
            //    return;
            //}

            if (null == m_pmlLenderSiteId)
                SetPmlLenderSiteId();
            string bodyContent = "";


            bool hasLogo = AttachLogo(dataLoan) && string.IsNullOrEmpty(m_pmlLenderSiteId) == false;
            string logoId = $"_logo_{DateTime.Now.Ticks}";
            byte[] logo = null;
            if (hasLogo)
            {
                try
                {
                    logo = FileDBTools.ReadData(E_FileDB.Normal, m_pmlLenderSiteId.ToLower() + ".logo.gif");
                }
                catch (FileNotFoundException)
                {
                    hasLogo = false;
                }
            }

            // OPM #1792 & #1850
            // Email to everyone assign to this loan include AE, Lock Desk, Underwriter etc.. and Loan Officer in official agent list.
            ArrayList uniqueEmails = new ArrayList();

            CAppData dataApp = dataLoan.GetAppData(0);
            string aBFirstNm = dataApp.aBFirstNm;
            string aBLastNm = dataApp.aBLastNm;
            string loanNm = dataLoan.sLNm;

            CAgentFields agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.EmailAddr != "") 
            {
                if ((!PriceMyLoanConstants.IsEmbeddedPML && !CurrentBroker.IsEmailPMLUserCertificateDisabled) || IsEmailLoanOfficer.Checked) 
                {
                    uniqueEmails.Add(agent.EmailAddr.ToLower());
                }
            }

            agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.EmailAddr != "")
            {
                if ((!PriceMyLoanConstants.IsEmbeddedPML && !CurrentBroker.IsEmailPMLUserCertificateDisabled) || IsEmailBrokerProcessor.Checked)
                {
                    if (!uniqueEmails.Contains(agent.EmailAddr.ToLower()))
                        uniqueEmails.Add(agent.EmailAddr.ToLower());
                }
            }

            agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.ExternalSecondary, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.EmailAddr != "")
            {
                if ((!PriceMyLoanConstants.IsEmbeddedPML && !CurrentBroker.IsEmailPMLUserCertificateDisabled) || IsEmailExternalSecondary.Checked)
                {
                    if (!uniqueEmails.Contains(agent.EmailAddr.ToLower()))
                        uniqueEmails.Add(agent.EmailAddr.ToLower());
                }
            }

            agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.ExternalPostCloser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.EmailAddr != "")
            {
                if ((!PriceMyLoanConstants.IsEmbeddedPML && !CurrentBroker.IsEmailPMLUserCertificateDisabled) || IsEmailExternalPostCloser.Checked)
                {
                    if (!uniqueEmails.Contains(agent.EmailAddr.ToLower()))
                        uniqueEmails.Add(agent.EmailAddr.ToLower());
                }
            }

            if (dataLoan.sEmployeeLenderAccExecEmail != "") 
            {
                if (!PriceMyLoanConstants.IsEmbeddedPML || IsEmailLenderAccExec.Checked) 
                {
                    if (!uniqueEmails.Contains(dataLoan.sEmployeeLenderAccExecEmail.ToLower()))
                        uniqueEmails.Add(dataLoan.sEmployeeLenderAccExecEmail.ToLower());
                }
            }

            // 8/19/2005 dd - Part of OPM 2245. If loan is submit manually for underwrite do not email Lock Desk.
            if (!isSubmitManually) 
            {
                //3/28/2013 OPM 111418 - If request will Auto-Lock and "SendAutoLockToLockDesk"==false, don't notify Lock Desk.
                bool autoLock = CurrentUserCanPerform(WorkflowOperations.RequestRateLockCausesLock);
                if(dataLoan.BrokerDB.SendAutoLockToLockDesk || !autoLock)
                {
                    if (dataLoan.sEmployeeLockDeskEmail != "") 
                    {
                        // 9/10/2009 vm - OPM 33691 Require LO users to always email the assigned lock desk when LOCKING a loan
                        if (!PriceMyLoanConstants.IsEmbeddedPML || IsEmailLockDesk.Checked || isRateLockRequest) 
                        {
                            if (!uniqueEmails.Contains(dataLoan.sEmployeeLockDeskEmail.ToLower()))
                                uniqueEmails.Add(dataLoan.sEmployeeLockDeskEmail.ToLower());
                        }

                    }

                }
            }
            if (dataLoan.sEmployeeUnderwriterEmail != "") 
            {
                if (!PriceMyLoanConstants.IsEmbeddedPML || IsEmailUnderwriter.Checked) 
                {
                    if (!uniqueEmails.Contains(dataLoan.sEmployeeUnderwriterEmail.ToLower()))
                        uniqueEmails.Add(dataLoan.sEmployeeUnderwriterEmail.ToLower());
                }

            }

            if (dataLoan.sEmployeeProcessorEmail != "") 
            {
                if (!PriceMyLoanConstants.IsEmbeddedPML || IsEmailProcessor.Checked) 
                {
                    // 9/8/2006 dd - OPM 7305 - Also send to processor.
                    if (!uniqueEmails.Contains(dataLoan.sEmployeeProcessorEmail.ToLower()) )
                        uniqueEmails.Add(dataLoan.sEmployeeProcessorEmail.ToLower());
                }
            }

            // 8/5/2010 vm - OPM 32224 - Email the person who registered or locked the loan.
            if (PriceMyLoanUser != null && PriceMyLoanUser.Email != "")
            {
                if (!PriceMyLoanConstants.IsEmbeddedPML && !CurrentBroker.IsEmailPMLUserCertificateDisabled)
                {
                    if (!uniqueEmails.Contains(PriceMyLoanUser.Email.ToLower()))
                        uniqueEmails.Add(PriceMyLoanUser.Email.ToLower());
                }
            }

            if (uniqueEmails.Count == 0)
                return; // No Email address

            var xlsparams = XsltParams;
            if( hasLogo )
            {
                xlsparams.AddParam("LogoSource", "", $"cid:{logoId}");
            }

            using (MemoryStream stream = new MemoryStream(10000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false);
                writerSettings.OmitXmlDeclaration = true;

                XmlWriter writer = XmlWriter.Create(stream, writerSettings);
                
                string sPmlCertXmlContent = dataLoan.sPmlCertXmlContent.Value;
                if (null == sPmlCertXmlContent || "" == sPmlCertXmlContent)
                {
                    sPmlCertXmlContent = "<NoData/>";
                    writer.WriteRaw(sPmlCertXmlContent);
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sPmlCertXmlContent);
                    doc = UnderwritingResultItem.DedupAdjustmentDescription(doc);
                    doc.WriteTo(writer);
                }

                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                bodyContent = XslTransformHelper.Transform(XsltFileLocation, stream, xlsparams);
            }

            // 12/20/06 mf - OPM 8833. We can now get the "From" address based on
            // the rule settings of the broker and the assignments of the loan.
            EventNotificationRulesView rView = new EventNotificationRulesView( BrokerID );
            string fromAddress = rView.GetFromAddressString( dataLoan.sLId );

            foreach (string recipient in uniqueEmails) 
            {
                try 
                {
                    string subject = loanNm + " - " + aBFirstNm + " " + aBLastNm + " - Loan Certificate";
                    
                    if (m_isRateLockedAtSubmission && !isSubmitManually) 
                    {
                        if (isRateLockRequest)
                            subject = loanNm + " - " + aBFirstNm + " " + aBLastNm + " - Rate Lock Request Certificate";
                        else
                            subject = loanNm + " - " + aBFirstNm + " " + aBLastNm + " - Loan Registration Certificate";
                    }

                    var cbe = new LendersOffice.Email.CBaseEmail(BrokerID)
                    {
                        DisclaimerType = E_DisclaimerType.NORMAL,
                        From = fromAddress,
                        To = recipient,
                        Subject = subject,
                        Message = bodyContent,
                        IsHtmlEmail = true
                    };
                    if (hasLogo && logo.Length > 0)
                    {
                        cbe.AddAttachment(logoId, logo, true);
                    }
                    cbe.Send();                                    
                } 
                catch (Exception exc) 
                {
                    Tools.LogWarning("Failed send out certificate to " + recipient, exc);
                }
            }

        }
        private string XsltFileLocation 
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.PmlCertificateXslt, this.BrokerID); }
        }
        
        private void SetPmlLenderSiteId() 
        {
            // 11/10/2004 dd - Theoretically, PmlLenderSiteId should be in loan product.
            // This way we allow independent broker to run qualification against multiple lender.
            // However we are not there yet. Retrieve from current broker.
            BrokerDB broker = BrokerDB.RetrieveById(BrokerID);

            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(broker.PmlSiteID, LoanID);
        }

        private string RenderCertificateToHtml(string xml) 
        {
            if (null == m_pmlLenderSiteId)
                SetPmlLenderSiteId();
            string ret = "";
            try 
            {
                ret = XslTransformHelper.Transform(XsltFileLocation, xml, XsltParams);
            } 
            catch (Exception exc) 
            {
                // Only log error to us but don't want user to see error.
                Tools.LogError(exc);
            }
            return ret;


        }
        #endregion


        #region Setup assignments on submit

        /// <summary>
        /// Get the assigned officer and lender account exec and find
        /// the underwriter, etc to assign to the loan at this time.
        /// We only setup assignments where they aren't currently set.
        /// Once we hookup event framework stuff, this can be done
        /// by an event listener and refactored out of this less-than-
        /// reusable-and-conspicuous spot.
        /// 
        /// WARNING: THERE IS ANOTHER DUPLICATE METHOD IN $LO/ObjLib/DistributeUnderwriting/ApplyLoanWorkerUnit.cs
        /// </summary>
        private void SetAssignmentsOnSubmit(
            Guid brokerId,
            Guid submittingEmployeeId ,
            Guid loanId,
            E_BranchChannelT channelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            // 8/25/2005 kb - Well, we need to do this assignment to
            // get underwriters hooked up.  Know this: having an uw
            // assigned typically locks out everyone else.  Some
            // lenders may not want to have their ae's locked out
            // when the loan is submitted with a float request.
            //
            // Also, we set the new assignments on the primary loan,
            // so that 80/20 submissions copy the assignments after
            // we fix them up.

            String whileDoing = "Assigning employee relationships.";

            try
            {
                var relationshipAssignmentHandler = new RelationshipAssignmentHandler(
                    this.BrokerID,
                    this.LoanID,
                    submittingEmployeeId);

                Guid? priceGroupID = null;
                bool isRequestingRateLock = false;

                relationshipAssignmentHandler.AssignSubmissionRelationships(
                    priceGroupID,
                    isRequestingRateLock,
                    channelT,
                    correspondentProcessT);
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to set assignments on submit for " + loanId + " while '" + whileDoing + "'.", e);
            }
        }

        #endregion

        private string LoadResultHtmlFromFileDB(string name) 
        {
            string ret = "";

            try 
            {
                Action<FileInfo> readHandler = delegate (FileInfo fi)
                {
                    using (StreamReader stream = new StreamReader(fi.FullName))
                    {
                        ret = stream.ReadToEnd();
                    }
                };

                FileDBTools.UseFile(E_FileDB.Temp, name, readHandler);

                FileDBTools.Delete(E_FileDB.Temp, name);
            } 
            catch (Exception exc) 
            {
                LODataAccess.Tools.LogErrorWithCriticalTracking(exc);
            }
            return ret;
        }

        /// <summary>
        /// Determines if we need to show the logo on the email cert based on   IsDisplayNmModified
        /// We need this check outside because we dont want to attach the logo if the cert doesn tneed it.
        /// </summary>
        /// <param name="sLId"></param>
        /// <returns></returns>
        private bool AttachLogo(CPageData data)
        {

            BranchDB db = new BranchDB(data.sBranchId, data.sBrokerId);
            db.Retrieve();

            return !db.IsDisplayNmModified;
        }

        /// <summary>
        /// Gets value of sEstCloseD. This is being used to avoid adding a
        /// service page just to get the updated value of sEstCloseD.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="sEstCloseDLckd">
        /// Bit indicating whether the value of sEstCloseD is locked by the user.
        /// </param>
        /// <param name="sEstCloseD">
        /// The value of sEstCloseD from the page.
        /// </param>
        /// <returns>An object with the updated value of sEstCloseD.</returns>
        [WebMethod]
        public static object GetUpdatedEstCloseD(string loanId, bool sEstCloseDLckd, string sEstCloseD)
        {
            var loan = new CPageData(
                new Guid(loanId),
                new string[] { "sEstCloseDLckd", "sEstCloseD" });
            loan.InitLoad();

            loan.sEstCloseDLckd = sEstCloseDLckd;
            loan.sEstCloseD_rep = sEstCloseD;

            return new
            {
                sEstCloseD = loan.sEstCloseD_rep
            };
        }

        protected void StipulationWarnings_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            string data = (string)args.Item.DataItem;
            PassthroughLiteral warning = (PassthroughLiteral)args.Item.FindControl("StipulationWarning");
            warning.Text = data;
        }
    }
}
