namespace PriceMyLoan.UI.Main
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using ConfigSystem;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.HttpModule;
    using LendersOffice.RatePrice;
    using LendersOffice.RatePrice.Model;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using PriceMyLoan.DataAccess;
    using PriceMyLoan.Security;

    public partial class MainService : LendersOffice.Common.BaseServiceJsonNetOptimized
	{
        public static string GetMergeKeyFrom(Guid employeeId, Guid id)
        {
            return String.Format("MergeKeys_{0}_{1}", id, employeeId);
        }

        protected override void Process(string methodName) 
        {
            try
            {
                switch (methodName)
                {
                    case "LoanInfo_CalculateData":
                        LoanInfo_CalculateData();
                        break;
                    case "LoanInfo_IsSpStateAllowPp":
                        LoanInfo_IsSpStateAllowPp();
                        break;
                    case "GetResult_SubmitUnderwriting":
                        GetResult_SubmitUnderwriting();
                        break;
                    case "GetResult_IsAvailable":
                        GetResult_IsAvailable();
                        break;
                    case "GetResult_TemporaryAcceptFirstLoan":
                        GetResult_TemporaryAcceptFirstLoan();
                        break;
                    case "GetResult_ExtendTimeout":
                        GetResult_ExtendTimeout();
                        break;
                    case "GetResult_ReportTimeout":
                        GetResult_ReportTimeout();
                        break;
                    case "GetResult_MinuteWarning":
                        GetResult_MinuteWarning();
                        break;
                    case "GetResult_GetErrorMessage":
                        GetResult_GetErrorMessage();
                        break;
                    case "GetResult_GenerateByPassTicket":
                        GetResult_GenerateByPassTicket();
                        break;
                    case "PingTime":
                        PingTime();
                        break;
                    case "EmailTiming":
                        EmailTiming();
                        break;
                    case "CheckCRAWarningMessage":
                        CheckCRAWarningMessage();
                        break;
                    case "LoanInfo_refreshsProdRLckdDays":
                        refreshsProdRLckdDays();
                        break;
                    case "RecordActivity":
                        RecordActivity();
                        break;
                    case "CalculateMonthlyExpenses":
                        CalculateMonthlyExpenses();
                        break;
                    case "SaveMonthlyExpenses":
                        SaveMonthlyExpenses();
                        break;
                    case "GetResult_SubmitUnderwritingByGrouping":
                        GetResult_SubmitUnderwritingByGrouping();
                        break;
                    case "GetResults":
                        GetResults();
                        break;
                }
            }
            catch (CLoanTemplateNotFoundException exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (CBaseException exc)
            {
                SetResult("ErrorMessage", exc.UserMessage);
                Tools.LogErrorWithCriticalTracking(exc);
            }
        }

        private void ValidateResultItem(UnderwritingResultItem resultItem)
        {
            if (resultItem == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
            }

            if (resultItem.IsErrorMessage)
            {
                throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
            }
        }

        private void DetermineDenialReasons(CPageData dataLoan, bool isAllowRegister, bool isAllowRequestLock, bool isAllowAutoLock, LoanValueEvaluator evaluator)
        {
            var reasons = new List<string>();
            var principal = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;
            bool isEncompass = principal.IsEncompassIntegrationEnabled || principal.IsSpecialRemnEncompassTotalAccount;

            if(!principal.HasPermission(Permission.CanSubmitWithoutCreditReport)
                && !dataLoan.sAllAppsHasCreditReportOnFile && !isEncompass)
            {
                reasons.Add("Allow to register/lock without a credit report");
            }

            SetResult("DenialReasons", SerializationHelper.JsonNetSerialize(reasons));

            ExecutionEngineDebugMessage registerDebugMessage = LendingQBExecutingEngine.GetExecutionEngineDebugMessage(WorkflowOperations.RegisterLoan, evaluator);
            List<string> registerDenialReasons = registerDebugMessage?.UserFriendlyMessages.ToList() ?? new List<string>();
            ConditionType registerFailedConditionType = registerDebugMessage?.FailedConditionType ?? ConditionType.Invalid;
            this.DetermineHardWorkflowStop(isAllowRegister, registerDenialReasons, "register");

            SetResult("RegisterDenialReasons", SerializationHelper.JsonNetSerialize(registerDenialReasons));
            SetResult("RegisterFailedConditionType", registerFailedConditionType);

            List<string> lockDenialReasons = new List<string>();
            ConditionType lockFailedConditionType = ConditionType.Invalid;

            if(!isAllowRequestLock && !isAllowAutoLock)
            {
                ExecutionEngineDebugMessage manualLockDebugMessage = isAllowRequestLock ? null : LendingQBExecutingEngine.GetExecutionEngineDebugMessage(WorkflowOperations.RequestRateLockRequiresManualLock, evaluator);
                List<string> manualLockDenialReasons = manualLockDebugMessage?.UserFriendlyMessages.ToList() ?? new List<string>();
                ConditionType manualLockFailedConditionType = manualLockDebugMessage?.FailedConditionType ?? ConditionType.Invalid;
                this.DetermineHardWorkflowStop(isAllowRequestLock, manualLockDenialReasons, "manual lock");

                ExecutionEngineDebugMessage autoLockDebugMessage = isAllowRequestLock ? null : LendingQBExecutingEngine.GetExecutionEngineDebugMessage(WorkflowOperations.RequestRateLockCausesLock, evaluator);
                List<string> autoLockDenialReasons = autoLockDebugMessage?.UserFriendlyMessages.ToList() ?? new List<string>();
                ConditionType autoLockFailedConditionType = autoLockDebugMessage?.FailedConditionType ?? ConditionType.Invalid;
                this.DetermineHardWorkflowStop(isAllowAutoLock, autoLockDenialReasons, "auto-lock");

                // We don't want to mix and match restraint and privilege denial reasons.
                if (manualLockFailedConditionType == ConditionType.Restraint)
                {
                    lockDenialReasons.AddRange(manualLockDenialReasons);
                    lockFailedConditionType = ConditionType.Restraint;
                }

                if (autoLockFailedConditionType == ConditionType.Restraint)
                {
                    lockDenialReasons.AddRange(autoLockDenialReasons);
                    lockFailedConditionType = ConditionType.Restraint;
                }

                if (lockFailedConditionType != ConditionType.Restraint
                    && (manualLockFailedConditionType == ConditionType.Privilege || autoLockFailedConditionType == ConditionType.Privilege))
                {
                    // NOTE: At this point, neither failed condition type is Restraint, and if either are invalid then their respective denial reasons should be empty.
                    lockDenialReasons.AddRange(manualLockDenialReasons);
                    lockDenialReasons.AddRange(autoLockDenialReasons);
                    lockFailedConditionType = ConditionType.Privilege;
                }
            }

            SetResult("LockDenialReasons", SerializationHelper.JsonNetSerialize(lockDenialReasons));
            SetResult("LockFailedConditionType", lockFailedConditionType);
        }

        private void DetermineHardWorkflowStop(bool isOperationAllowed, List<string> reasons, string action)
        {
            if(!isOperationAllowed && !reasons.Any())
            {
                reasons.Add("can " + action + "? = True");
            }
        }

        private void GetResults()
        {
            UseJsonOptimization = true;

            var principal = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;
            bool frontEndPricing = this.GetString("PricingOption") == "FrontEnd";
            bool checkEligibility = this.GetBool("IsCheckEligibility");
            bool isPricing2ndLoan = GetBool("IsPricing2ndLoan", false);
            var firstRate = GetString("FirstRate", string.Empty);
            var firstTemplateId = GetGuid("FirstTemplateId", Guid.Empty);
            var batchIds = new List<Guid>();


            Guid currentRequestId = GetGuid("CurrentRequestId", Guid.Empty);
            if( currentRequestId == Guid.Empty)
            {
                if(Tools.TryGetGuidFrom(GetString("CurrentRequestId"), out currentRequestId))
                {
                    string cacheKey = MainService.GetMergeKeyFrom(principal.EmployeeId, currentRequestId);
                    batchIds = SerializationHelper.JsonNetDeserialize<List<Guid>>(AutoExpiredTextCache.GetFromCache(cacheKey));
                }
            }

            Guid historicalRequestId = GetGuid("HistoricalRequestId", Guid.Empty);
            if(historicalRequestId == Guid.Empty)
            {

                Tools.TryGetGuidFrom(GetString("HistoricalRequestId", string.Empty), out historicalRequestId);
            }


            // Currently, PML always uses best rates pricing.
            bool prodFilterDisplayrateMerge = true;

            var getResultsUsing = PricingResultsType.Current;

            if (principal.UseHistoricalPricingAndUiEnhancements)
            {
                getResultsUsing = this.GetEnum<PricingResultsType>("GetResultsUsing");
            }

            UnderwritingResultItem resultItem = null;
            UnderwritingResultItem historicalResultItem = null;
            List<UnderwritingResultItem> batchResultItems = new List<UnderwritingResultItem>();

            if (currentRequestId != Guid.Empty)
            {
                if(batchIds.Any())
                {
                    UnderwritingResultItem batchItem;
                    foreach (Guid id in batchIds)
                    {
                        batchItem = LpeDistributeResults.RetrieveResultItem(id);
                        this.ValidateResultItem(batchItem);
                        batchResultItems.Add(batchItem);
                    }   
                }
                else
                {
                    resultItem = LpeDistributeResults.RetrieveResultItem(currentRequestId);
                    this.ValidateResultItem(resultItem);
                }
            }

            if (historicalRequestId != Guid.Empty)
            {
                historicalResultItem = LpeDistributeResults.RetrieveResultItem(historicalRequestId);
                if (historicalResultItem == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
                }

                if (historicalResultItem.IsErrorMessage)
                {
                    throw new CBaseException(historicalResultItem.ErrorMessage, historicalResultItem.ErrorMessage);
                }
            }

            Guid loanId = GetGuid("LoanID");
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(loanId, typeof(MainService));
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            dataLoan.sPricingModeT = E_sPricingModeT.RegularPricing;

            SetResult("showPrice", dataLoan.sPriceGroup.DisplayPmlFeeIn100Format);
            var isTotalIncomeZero = dataLoan.sLTotI == 0;
            SetResult("IsTotalIncomeZero", isTotalIncomeZero);

            var pricingStates = dataLoan.GetMatchingPricingStatesForScenario(dataLoan.sIs8020PricingScenario, isPricing2ndLoan, firstTemplateId, firstRate);
            var resultFormatter = ResultPricingFormatter.GetFormatter(dataLoan.BrokerDB, LendersOffice.Security.PrincipalFactory.CurrentPrincipal, dataLoan.sPricingModeT, dataLoan.sOriginatorCompensationPaymentSourceT, dataLoan.sOriginatorCompensationLenderFeeOptionT, pricingStates.ToList(), isTotalIncomeZero);
            ResultPricing result = null;

            if (batchResultItems.Any())
            {
                List<ResultPricing> results = new List<ResultPricing>();
                
                foreach (UnderwritingResultItem item in batchResultItems)
                {
                    results.Add(resultFormatter.GetMergedSingleResult(item));
                }

                result = new ResultPricing();
                result.EligibleGroups = new List<ResultGroup>();
                result.IneligibleGroups = new List<ResultGroup>();
                result.InsufficientGroups = new List<ResultGroup>();

                foreach (ResultPricing pricingResult in results)
                {
                    result.EligibleGroups.AddRange(pricingResult.EligibleGroups);
                    result.IneligibleGroups.AddRange(pricingResult.IneligibleGroups);
                    result.InsufficientGroups.AddRange(pricingResult.InsufficientGroups);
                }
            }
            else
            {
                switch (getResultsUsing)
                {
                    case PricingResultsType.Current:
                        result = prodFilterDisplayrateMerge ? resultFormatter.GetMergedSingleResult(resultItem) : resultFormatter.GetSingleResult(resultItem);
                        break;
                    case PricingResultsType.Historical:
                        result = prodFilterDisplayrateMerge ? resultFormatter.GetMergedSingleResult(historicalResultItem) : resultFormatter.GetSingleResult(historicalResultItem);
                        break;
                    case PricingResultsType.WorstCase:
                        result = resultFormatter.GetCombinedResult(resultItem, historicalResultItem);
                        break;
                    default:
                        throw new UnhandledEnumException(getResultsUsing);
                }
            }

            SetResult("IsPricing2ndLoan", isPricing2ndLoan);
            SetResult("PricingResults", result);
            SetResult("Is8020PricingScenario", dataLoan.sIs8020PricingScenario);

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.ConnectionInfo, principal.BrokerId, loanId, WorkflowOperations.RegisterLoan, WorkflowOperations.RequestRateLockCausesLock, WorkflowOperations.RequestRateLockRequiresManualLock);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

            bool canAutoLock = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockCausesLock, valueEvaluator);

            bool isUsingOldSecurity = LendingQBExecutingEngine.CanPerform(WorkflowOperations.UseOldSecurityModelForPml, valueEvaluator);
            bool isAllowRegister = isUsingOldSecurity || LendingQBExecutingEngine.CanPerform(WorkflowOperations.RegisterLoan, valueEvaluator);
            bool isAllowRequestLock = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockCausesLock, valueEvaluator) || LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockRequiresManualLock, valueEvaluator);

            DetermineDenialReasons(dataLoan, isAllowRegister, isAllowRequestLock, canAutoLock, valueEvaluator);

            E_sLienQualifyModeT lienQualifyModeT = isPricing2ndLoan && dataLoan.sIs8020PricingScenario ? E_sLienQualifyModeT.OtherLoan : E_sLienQualifyModeT.ThisLoan;
            bool isEncompass = principal.IsEncompassIntegrationEnabled || principal.IsSpecialRemnEncompassTotalAccount;
            bool IsPmlSubmissionAllowed = principal.BrokerDB.IsPmlSubmissionAllowed && !isEncompass;

            bool show2ndLienLink = false;
            bool showSelectLink = false;

            bool showLockRateLink = false;
            bool showLockUnavailable = false;
            bool showRegisterLoanLink = false;

            bool showLockUnavailableExpired = false;
            bool showRegisterLoanLinkExpired = false;

                if (dataLoan.sIs8020PricingScenario && lienQualifyModeT == E_sLienQualifyModeT.ThisLoan)
                {
                    show2ndLienLink = true;
                }
                else if (!IsPmlSubmissionAllowed && !isEncompass)
                {
                    showSelectLink = true;
                }
            else if (isUsingOldSecurity)
            {
                if(principal.IsRateLockedAtSubmission)
                {
                    switch (dataLoan.lpeRunModeT)
                    {
                        case E_LpeRunModeT.NotAllowed:
                            break;
                        case E_LpeRunModeT.OriginalProductOnly_Direct:
                            showLockRateLink = true;
                            showLockUnavailableExpired = true;
                            break;
                        case E_LpeRunModeT.Full:
                            showRegisterLoanLink = true;
                            showLockRateLink = true;
                            break;
                        default:
                            throw new UnhandledEnumException(dataLoan.lpeRunModeT);
                    }
                }
                else
                {
                    switch (dataLoan.lpeRunModeT)
                    {
                        case E_LpeRunModeT.NotAllowed:
                        case E_LpeRunModeT.OriginalProductOnly_Direct:
                            break;
                        case E_LpeRunModeT.Full:
                            showRegisterLoanLink = true;
                            break;
                        default:
                            throw new UnhandledEnumException(dataLoan.lpeRunModeT);
                    }
                }
            }
            else
            {
                switch (dataLoan.lpeRunModeT)
                {
                    case E_LpeRunModeT.Full:
                        showRegisterLoanLink = true;
                        showLockRateLink = true;
                        break;
                    case E_LpeRunModeT.OriginalProductOnly_Direct:
                        if(principal.IsRateLockedAtSubmission)
                        {
                            showLockUnavailableExpired = true;
                            showLockRateLink = true;
                        }
                        break;
                    case E_LpeRunModeT.NotAllowed:
                        break;
                    default:
                        throw new UnhandledEnumException(dataLoan.lpeRunModeT);
                }
            }

            SetResult("show2ndLienLink", show2ndLienLink);
            SetResult("showSelectLink", showSelectLink);
            SetResult("showLockRateLink", showLockRateLink);
            SetResult("showLockUnavailable", showLockUnavailable);
            SetResult("showRegisterLoanLink", showRegisterLoanLink);
            SetResult("showLockUnavailableExpired", showLockUnavailableExpired);
            SetResult("showRegisterLoanLinkExpired", showRegisterLoanLinkExpired);
            CacheAuditScript(isPricing2ndLoan);
        }

        private void CacheAuditScript(bool isPricingSecondLoan)
        {
            var data = new Dictionary<object, object>();
            OptimizedResult.ToList().ForEach(kvp => data.Add(kvp.Key, kvp.Value));
            m_result.ToList().ForEach(kvp => data.Add(kvp.Key, kvp.Value));
            //var data = m_result
            var mlJson = GetString("mlJson");
            var script = $@"<script type=""text/javascript""> 
                var ML = {mlJson};  
                var $j = jQuery;  
                $j(document).ready(function() {{ 
                    IsPricing2nd = { isPricingSecondLoan.ToString().ToLower() }; initializeAngular();
                    displayResultsFromObject({SerializationHelper.JsonNetSerialize(data)});}});
                isAuditRender = true;
            </script>";

            var resultHtml = $@"<div runat=""server"" id=""PricingResultsContainer{(isPricingSecondLoan ? 2 : 1)}""
            ng-controller=""pricingResultsController""><div id = ""ResultsContainer{(isPricingSecondLoan ? 2 : 1)}"">
            </div></div>" + script;

            Guid loanId = GetGuid("loanid");
            var name = (!isPricingSecondLoan ? "LPEFirstResult_" : "LPESecondResult_") + loanId.ToString("N");


            FileDBTools.WriteData(E_FileDB.Temp, name, Encoding.UTF8.GetBytes(resultHtml));
        }

        private void CalculateMonthlyExpenses()
        {
            Guid loanID = GetGuid("sLId");
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(loanID, typeof(MainService));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            BindMonthlyExpenses(dataLoan);
            BindLoanAmounts(dataLoan);

            SetResult("sProHazIns", dataLoan.sProHazIns_rep);
            SetResult("sProRealETx", dataLoan.sProRealETx_rep);
            SetResult("sMonthlyPmtPe", dataLoan.sMonthlyPmtPe_rep);
            SetResult("sProOHExp", dataLoan.sProOHExp_rep);
        }

        private void SaveMonthlyExpenses()
        {

            Guid loanID = GetGuid("sLId");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(loanID, typeof(MainService));
            dataLoan.AllowSaveWhileQP2Sandboxed = true;

            dataLoan.InitSave(sFileVersion);
            BindMonthlyExpenses(dataLoan);

            dataLoan.Save();

            BindLoanAmounts(dataLoan);

            SetResult("sMonthlyPmtPe", dataLoan.sMonthlyPmtPe_rep);
        }

        private void BindLoanAmounts(CPageData dataLoan)
        {
            dataLoan.sApprValPe_rep = GetString("sApprValPe");
            dataLoan.sHouseValPe_rep = GetString("sHouseValPe");
            dataLoan.sDownPmtPcPe_rep = GetString("sDownPmtPcPe");
            dataLoan.sEquityPe_rep = GetString("sEquityPe");
            dataLoan.sLtvRPe_rep = GetString("sLtvRPe");
            dataLoan.sLAmtCalcPe_rep = GetString("sLAmtCalcPe");
        }

        private void BindMonthlyExpenses(CPageData dataLoan)
        {
            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
            }

            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
            }

            dataLoan.sProHoAssocDues_rep = GetString("sProHoAssocDues");
            dataLoan.sProOHExpDesc = GetString("sProOHExpDesc");
            dataLoan.sProOHExp_rep = GetString("sProOHExp");
            dataLoan.sProOHExpLckd = GetBool("sProOHExpLckd");
        }

        private void GetResult_GenerateByPassTicket()
        {
            Guid ticket = ((AbstractUserPrincipal)this.User).GenerateByPassTicket();

            SetResult("Ticket", ticket);
        }

        private void RecordActivity()
        {
            // NO-OP. Should remove. dd - 11/6/2015.
        }

        private void EmailTiming() 
        {
            string msg = GetString("msg");
            string loanId = GetString("loanid");
            string comment = GetString("comment");
            string detailTransaction = EncryptionHelper.Decrypt(GetString("detailTransaction"));
            string issue = GetString("issue");
            string total = GetString("total");
            string userAgent = GetString("useragent");
            string debugDetail = EncryptionHelper.Decrypt(GetString("debugdetail"));

            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;

            // Stage 0 - When LpeRequest created.
            // Stage 1 - When worker thread begin to work on LpeRequest.
            // Stage 2 - When worker thread finish work on LpeRequest.
            // Stage 3 - When result are save to FileDB and insert into Result DB table.
            string emailBody = string.Format("<html><head><style>td {{font-size:11px;font-family: Arial, Helvetica, sans-serif;}}</style></head><body>{3}{0}{0}{4}{0}{0}UserAgent:{6}{0}{0}<pre>{1}</pre>{0}LoanID={2}{0}Detail:{0}<table cellpadding=5>{5}</table>{0}{0}Stage 0 - When LpeRequest created.{0}Stage 1 - When worker thread begin to work on LpeRequest.{0}Stage 2 - When worker thread finish work on LpeRequest.{0}Stage 3 - When result are save to FileDB and insert into Result DB table.{0}{0}<pre>{7}</pre></body></html>",
                "<p>", // 0
                msg, // 1
                loanId, // 2
                comment, // 3
                principal, // 4
                detailTransaction, // 5
                userAgent, // 6
                debugDetail //7
                );

            // 1/3/2013 dd - Log TO PB to search.

            string pbLogMessage = string.Format(@"PMLTimingResult - {0}. Total {1} seconds.

{2}

{3}

Stage 0 - When LpeRequest created.
Stage 1 - When worker thread begin to work on LpeRequest.
Stage 2 - When worker thread finish work on LpeRequest.
Stage 3 - When result are save to FileDB and insert into Result DB table.

{4}",
    issue // 0
    , total // 1
    , msg // 2
    , detailTransaction.Replace("<td>", "  ").Replace("</td>", "  ") // 3
    , debugDetail // 4
    );
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (item != null)
            {
                try
                {
                    item.sLId = new Guid(loanId);
                }
                catch (FormatException) { }
            }
            Tools.LogInfo(pbLogMessage);
            EmailUtilities.SendPmlTimingEmail("PML Timing Result - " + issue + ". Total " + total + " seconds.", emailBody);

        }
        private void PingTime() 
        {
            long initial = GetLong("InitialTicks");
            long duration = (DateTime.Now.Ticks - initial) / 10000L; // Return unit in seconds.

            SetResult("duration", duration);
        }

        #region Loan Info methods

        private void LoanInfo_CalculateData() 
        {
            Guid loanID = GetGuid("loanID");
			bool isStandAloneSecond = GetBool("IsStandAloneSecond",false);
            CPageData dataLoan = new CLoanInformationData(loanID);

            dataLoan.InitLoad();

            dataLoan.sProdCalcEntryT = (E_sProdCalcEntryT) GetInt("sProdCalcEntryT");
            dataLoan.sHouseValPe_rep = GetString("sHouseValPe");
            dataLoan.sDownPmtPcPe_rep = GetString("sDownPmtPcPe");
            dataLoan.sEquityPe_rep = GetString("sEquityPe");
			dataLoan.sLAmtCalcPe_rep = GetString("sLAmtCalcPe");
            dataLoan.sCltvRPe_rep = GetString("sCltvRPe");
            dataLoan.sLtvRPe_rep = GetString("sLtvRPe");
			dataLoan.sProOFinBalPe_rep = GetString("sProOFinBalPe");
			dataLoan.sLtvROtherFinPe_rep = GetString("sLtvROtherFinPe");

            dataLoan.sRequestCommunityOrAffordableSeconds = GetBool("sRequestCommunityOrAffordableSeconds");

            if (ContainsField("sIsRenovationLoan"))
            {
                dataLoan.sIsRenovationLoan = GetBool("sIsRenovationLoan");
                dataLoan.sTotalRenovationCosts_rep = GetString("sTotalRenovationCosts");
                dataLoan.sAsIsAppraisedValuePeval_rep = GetString("sAsIsAppraisedValuePeval");
            }

            dataLoan.sProOHExpPe_rep = GetString("sProOHExpPe");
            dataLoan.sProRealETxPe_rep = GetString("sProRealETxPe");
            dataLoan.sHas2ndFinPe = (GetString("secondFinancing") == "bYes2ndFinancing");
            dataLoan.sSpLien_rep = GetString("sSpLien");

            // 10/15/2010 dd - OPM 27822
            dataLoan.sHasAppraisal = GetBool("sHasAppraisal");
            dataLoan.sOriginalAppraisedValue_rep = GetString("sOriginalAppraisedValue");


            // OPM 28030 - mp
            var sProdRLckdDays = GetString("sProdRLckdDays", "");
            if (!string.IsNullOrEmpty(sProdRLckdDays))
            {
                PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal)this.User;
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                if (false == brokerDB.IsEnabledLockPeriodDropDown)
                {
                    dataLoan.sProdRLckdDays_rep = GetString("sProdRLckdDays");
                }
                else
                {
                    var lockPeriod = UInt32.Parse(GetString("sProdRLckdDays"));
                    if (brokerDB.AvailableLockPeriodOptions.Contains(lockPeriod) || lockPeriod == 0) 
                    {
                        dataLoan.sProdRLckdDays_rep = lockPeriod.ToString();

                        // SK - allowing it to be 0 since other validation should prevent it from being 0 when really don't want it to be.
                    }
                    else
                    {
                        throw new KeyNotFoundException(lockPeriod + " is not one of the available lock period options");
                    }
                }
            }

			if (!isStandAloneSecond )
			{
				// Note: there is temporary order dependency, make sure set this field last.
				dataLoan.sLPurposeTPe = (E_sLPurposeT) GetInt("sLPurposeTPe");
			}
            string sSubFinT_rep = GetString("sSubFinT", "");

            if (sSubFinT_rep == "bHeloc2ndFinancing")
            {
                dataLoan.sSubFinT = E_sSubFinT.Heloc;
            }
            else if (sSubFinT_rep == "bCloseEnd2ndFinancing")
            {
                dataLoan.sSubFinT = E_sSubFinT.CloseEnd;
            }
            else
            {
                Tools.LogWarning("invalid value for ssubfint : " + sSubFinT_rep);
            }

            if (dataLoan.sLPurposeTPe == E_sLPurposeT.Purchase)
            {
                dataLoan.sApprValPe_rep = GetString("sApprValPe");
            }
            if (dataLoan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                dataLoan.sProdIsLoanEndorsedBeforeJune09 = GetBool("sProdIsLoanEndorsedBeforeJune09");
            }


            dataLoan.sSubFinPe_rep = GetString("sSubFinPe");
            dataLoan.sSubFinToPropertyValue_rep = GetString("sSubFinToPropertyValue");
            dataLoan.sHCLTVRPe_rep = GetString("sHCLTVRPe");

            SetResult("sProdCalcEntryT", dataLoan.sProdCalcEntryT);
            SetResult("sApprValPe", dataLoan.sApprValPe_rep);
            SetResult("sHouseValPe", dataLoan.sHouseValPe_rep);
            SetResult("sDownPmtPcPe", dataLoan.sDownPmtPcPe_rep);
            SetResult("sEquityPe", dataLoan.sEquityPe_rep);
            SetResult("sLAmtCalcPe", dataLoan.sLAmtCalcPe_rep);
            SetResult("sLtvRPe", dataLoan.sLtvRPe_rep);
            SetResult("sCltvRPe", dataLoan.sCltvRPe_rep);
            SetResult("sProOFinBalPe", dataLoan.sProOFinBalPe_rep);
            SetResult("sLtvROtherFinPe", dataLoan.sLtvROtherFinPe_rep);
            SetResult("sProOHExpPe", dataLoan.sProOHExpPe_rep);
            SetResult("sProRealETxPe", dataLoan.sProRealETxPe_rep);
			SetResult("sLtv80TestResultT", dataLoan.sLtv80TestResultT);
			SetResult("sConvLoanLtvForMiNeed", dataLoan.sConvLoanLtvForMiNeed_rep);
            SetResult("sHCLTVRPe", dataLoan.sHCLTVRPe_rep);
            SetResult("sSubFinToPropertyValue", dataLoan.sSubFinToPropertyValue_rep);
            SetResult("sSubFinPe", dataLoan.sSubFinPe_rep);
            SetResult("sProdIsLoanEndorsedBeforeJune09", dataLoan.sProdIsLoanEndorsedBeforeJune09);
            SetResult("sProdFhaUfmip", dataLoan.sProdFhaUfmip_rep);
            

            // OPM 28030 - mp
            SetResult("sProdRLckdDays", dataLoan.sProdRLckdDays_rep);
            SetResult("sProdRLckdExpiredDLabel", dataLoan.sProdRLckdExpiredDLabel);

            SetResult("sRequestCommunityOrAffordableSeconds", dataLoan.sRequestCommunityOrAffordableSeconds);

            if (ContainsField("sIsRenovationLoan"))
            {
                SetResult("sIsRenovationLoan", dataLoan.sIsRenovationLoan);
                SetResult("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep);
                SetResult("sAsIsAppraisedValuePeval", dataLoan.sAsIsAppraisedValuePeval_rep);
            }

            if (dataLoan.sLPurposeTPe == E_sLPurposeT.VaIrrrl)
            {
                bool sLPurposeTPeUpdate = GetBool("sLPurposeTPeUpdate", false);
                if (sLPurposeTPeUpdate)
                {
                    dataLoan.sProdVaFundingFee_rep = "0.5%";
                    SetResult("sProdVaFundingFee", dataLoan.sProdVaFundingFee_rep);
                }
            }
            SetResult("sSpLien", dataLoan.sSpLien_rep);
        }

		//opm 11948 fs 08/11/08
		private void refreshsProdRLckdDays()
		{
			Guid loanID = GetGuid("loanID");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
			CPageData dataLoan = new CPmlLoan_UpdateLockDays(loanID);


			dataLoan.InitSave(sFileVersion);

			dataLoan.sProdRLckdDays_rep = GetString("sProdRLckdDays");		
			dataLoan.Save();
			SetResult("sProdRLckdDays", dataLoan.sProdRLckdDays_rep);

            // OPM 28030 - mp
            SetResult("sProdRLckdExpiredDLabel", dataLoan.sProdRLckdExpiredDLabel);

		}
        private void LoanInfo_IsSpStateAllowPp() 
        {
            string sSpStatePe = GetString("sSpStatePe");

            if (sSpStatePe != "") 
            {
                PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

                bool isSpStateAllowPp_1st = CPageBase.IsStateAllowPp(sSpStatePe, E_sLienPosT.First, brokerDB);
                bool isSpStateAllowPp_2nd = CPageBase.IsStateAllowPp(sSpStatePe, E_sLienPosT.Second, brokerDB);

                SetResult("IsSpStateAllowPp_1st", isSpStateAllowPp_1st);
                SetResult("IsSpStateAllowPp_2nd", isSpStateAllowPp_2nd);
            }

        }
        #endregion

        private void GetResult_ExtendTimeout() 
        {
            int cnt = 0;

            Guid requestID = GetGuid("RequestID");

            uint resultIndex = LpeDistributeResults.GetResultTableIndex(requestID);
            SqlParameter[] parameters = { new SqlParameter("@RequestBatchId", requestID) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "LPE_IsBatchProcess_" + resultIndex, parameters)) 
            {
                if (reader.Read()) 
                {
                    cnt = (int) reader["Cnt"];
                }
            }
            SetResult("IsExtendTime", cnt == 0 ? "0" : "1");
        }
        private void GetResult_TemporaryAcceptFirstLoan() 
        {
            Guid loanID = GetGuid("loanID");
            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;
            string _1stRequestedRate = GetString("_1stRequestedRate");
            string _1stRequestedPoint = GetString("_1stRequestedPoint");
            string _1stRateId = GetString("_1stRateId");
            string _1stVersion = GetString("_1stVersion");
            string debugResultStartTicks = GetString("debugResultStartTicks");
            E_RenderResultModeT renderResultModeT = (E_RenderResultModeT)GetInt("RenderResultModeT");

            Guid _1stProductID = GetGuid("_1stProductID");

            CPageData dataLoan = new CCheckFor2ndLoan(loanID);
            dataLoan.InitLoad();

            Guid lpePriceGroupId = dataLoan.sProdLpePriceGroupId;

            string requestedRate = string.Format("{0}:{1}:{2}", _1stRequestedRate, _1stRequestedPoint, _1stRateId);

            if (dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct) // Rerun scenario
            {
                // 7/18/2006 dd - We do not run TemporaryAcceptFirstLoan right away when rerun pricing. We need to store the TemporaryAcceptFirstLoan task message.
                DistributeUnderwritingEngine.SaveTemporaryAcceptFirstLoanMessage(principal, loanID, _1stProductID, requestedRate, lpePriceGroupId, _1stVersion, debugResultStartTicks, renderResultModeT);
            } 
            else 
            {
                Guid requestID = DistributeUnderwritingEngine.TemporaryAcceptFirstLoan(principal, loanID, _1stProductID, requestedRate, lpePriceGroupId, _1stVersion, debugResultStartTicks, renderResultModeT);

                int polling = DistributeUnderwritingEngine.GetSuggestedPollingInterval(requestID);
                SetResult("RequestID", requestID);
                SetResult("Polling", polling);
            }
        }

        private void GetResult_SubmitUnderwritingByGrouping()
        {
             PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;
            
            if (!principal.BrokerDB.IsPricingByRateMergeGroupEnabled)
            {
                this.GetResult_SubmitUnderwriting();
                return;
            }

            DateTime startTime = DateTime.Now;

            string debugMessage = startTime.ToLongDateString() + " - Begin GetResult_SubmitUnderwriting";
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Guid loanID = GetGuid("loanID");

            E_sLienQualifyModeT sLienQualifyModeT = (E_sLienQualifyModeT) GetInt("sLienQualifyModeT");
            E_RenderResultModeT renderResultModeT = (E_RenderResultModeT)GetInt("RenderResultModeT");
            Guid firstLienLpId = GetGuid("FirstLienLpId", GetGuid("1stProductID", Guid.Empty));
            decimal firstLienNoteRate = GetRate("FirstLienNoteRate", GetRate("1stRate", 0));
            decimal firstLienPoint = GetRate("FirstLienPoint", GetRate("1stPoint", 0));
            decimal firstLienMPmt = GetMoney("FirstLienMPmt", GetMoney("1stMPmt",0));

            decimal secondLienMPmt = GetMoney("SecondLienMPmt", 0);

            CPageData dataLoan = new CCheckFor2ndLoan(loanID);
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            LoanProgramByInvestorSet lpSet;

            try
            {
                lpSet = dataLoan.GetLoanProgramsToRunLpe(principal, sLienQualifyModeT, firstLienLpId);
            }
            catch (CLPERunModeException e)
            {
                SetResult("ErrorMessage", e.UserMessage);
                
                // This will activate a "reasons" link that shows the circumstances under which pricing can be run
                SetResult("ShowErrorReasons", true);
                return;
            }

            stopwatch.Stop();
            string tempLpeTaskMessageXml = "";
            if (dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct && sLienQualifyModeT == E_sLienQualifyModeT.OtherLoan)
            {
                tempLpeTaskMessageXml = dataLoan.sTempLpeTaskMessageXml;
            }
            DataSrc src = principal.BrokerDB.GetLatestReleaseInfo().DataSrcForSnapshot;
            List<LoanProgramByInvestorSet> rateMergeGroupSets = lpSet.GetSetsByRateMergeGroup(src, principal.BrokerDB);
            List<Guid> batchIds = new List<Guid>();

            int counter;

            if (DistributeUnderwritingSettings.ServerT == E_ServerT.AuthorTest ||
                DistributeUnderwritingSettings.ServerT == E_ServerT.DevMinimum)
            {
                // 3/21/2017 - Allow Dev mode and Author mode to run pricing quicker when a lender has Pricing by Rate Merge enable.
                //             In Dev/Author mode, we do not send pricing request to calc servers, therefore if we do not utilize parallel
                //             then pricing requests will be done in sequential order and it will take time.

                if (rateMergeGroupSets.Count > 0)
                {
                    var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
                    // The first request run with "runMigrations".
                    Guid batchId = DistributeUnderwritingEngine.SubmitToEngine(principal, loanID, rateMergeGroupSets[0], sLienQualifyModeT, dataLoan.sProdLpePriceGroupId, tempLpeTaskMessageXml, renderResultModeT, E_sPricingModeT.RegularPricing, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, secondLienMPmt, options, runMigrations: true);
                    batchIds.Add(batchId);

                    if (rateMergeGroupSets.Count > 1)
                    {
                        ConcurrentQueue<Guid> queueId = new ConcurrentQueue<Guid>();
                        // For the subsquent requests run from second item.
                        Parallel.ForEach(rateMergeGroupSets.GetRange(1, rateMergeGroupSets.Count - 1), o =>
                        {
                            Guid id = DistributeUnderwritingEngine.SubmitToEngine(principal, loanID, o, sLienQualifyModeT, dataLoan.sProdLpePriceGroupId, tempLpeTaskMessageXml, renderResultModeT, E_sPricingModeT.RegularPricing, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, secondLienMPmt, options, runMigrations: false);
                            queueId.Enqueue(id);
                        });

                        batchIds.AddRange(queueId.ToArray());
                    }
                }
            }
            else
            {
                var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
                for (counter = 0; counter < rateMergeGroupSets.Count; counter++)
                {
                    Guid batchId = DistributeUnderwritingEngine.SubmitToEngine(principal, loanID, rateMergeGroupSets[counter], sLienQualifyModeT, dataLoan.sProdLpePriceGroupId, tempLpeTaskMessageXml, renderResultModeT, E_sPricingModeT.RegularPricing, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, secondLienMPmt, options, runMigrations: counter == 0);
                    batchIds.Add(batchId);
                }
            }

            Tools.LogVerbose("user " + principal.UserId + " (" + principal.LoginNm + ") has submitted a rate merge pricing request for loan " + loanID.ToString());
            Guid requestIdentifier = Guid.NewGuid();
            string requestIdentCacheKey = string.Format("MergeKeys_{0}_{1}", requestIdentifier, principal.EmployeeId.ToString());
            AutoExpiredTextCache.AddToCache(SerializationHelper.JsonNetSerialize(batchIds), TimeSpan.FromHours(1), requestIdentCacheKey);
            int polling = DistributeUnderwritingEngine.GetSuggestedPollingInterval(batchIds.Any() ? batchIds.First() : ConstAppDavid.LPE_NoProductRequestId);
            SetResult("SetupRequestMs", stopwatch.ElapsedMilliseconds);
            SetResult("ProductCount", lpSet.LoanProgramsCount);
            SetResult("RequestID", Tools.ShortenGuid(requestIdentifier));
            SetResult("Polling", polling);

            debugMessage += Environment.NewLine + " Prepare Request " + stopwatch.ElapsedMilliseconds + " ms.";

            // 11/25/2009 dd - Perform polling on the server side. This to reduce the latency when client doing polling.

            int i = 0;
            bool hasResult = false;
            stopwatch.Reset();
            stopwatch.Start();

            int max = ConstStage.NumberOfHalfSecondPricingChecks;

            for (counter = 0; counter < batchIds.Count; counter++)
            {
                while (i < max)// 1/19/2014 dd - Increase timeout to 2 minutes.
                {
                    if (LpeDistributeResults.IsResultAvailable(batchIds[counter]))
                    {
                        break;
                    }
                    i++;
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(500); // Sleep before poll again.
                }

                if (i >= max) //a timeout happened.
                {
                    break;
                }
            }

            if (counter == batchIds.Count)
            {
                hasResult = true;
            }

            stopwatch.Stop();
            SetResult("ServerPolling", stopwatch.ElapsedMilliseconds);
            if (hasResult)
            {
                SetResult("IsAvailable", "1");
            }
            else
            {
                // Call Timeout report.
                SetResult("IsAvailable", "0");
            }
            debugMessage += Environment.NewLine + " Polling in " + stopwatch.ElapsedMilliseconds + " ms.";
            debugMessage += Environment.NewLine + Tools.GetDBCurrentDateTime().ToLongTimeString() + " - End GetResult_SubmitUnderwriting";

            if (DistributeUnderwritingSettings.ServerT == E_ServerT.AuthorTest)
            {
                debugMessage += Environment.NewLine + PerformanceStopwatch.ReportOutput;
            }
            LOCache.Set("DEBUG_" + requestIdentifier, debugMessage, DateTime.Now.AddMinutes(10));
            LOCache.Set("DEBUG_" + requestIdentifier + "_START_TICKS", startTime.Ticks, DateTime.Now.AddMinutes(10));

        }

        private void GetResult_SubmitUnderwriting() 
        {
            DateTime startTime = DateTime.Now;

            string debugMessage = Tools.GetDBCurrentDateTime().ToLongTimeString() + " - Begin GetResult_SubmitUnderwriting";
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Guid loanID = GetGuid("loanID");

            E_sLienQualifyModeT sLienQualifyModeT = (E_sLienQualifyModeT) GetInt("sLienQualifyModeT");
            E_RenderResultModeT renderResultModeT = (E_RenderResultModeT)GetInt("RenderResultModeT");
            Guid firstLienLpId = GetGuid("FirstLienLpId", GetGuid("1stProductID", Guid.Empty));
            decimal firstLienNoteRate = GetRate("FirstLienNoteRate", GetRate("1stRate", 0));
            decimal firstLienPoint = GetRate("FirstLienPoint", GetRate("1stPoint", 0));
            decimal firstLienMPmt = GetMoney("FirstLienMPmt", GetMoney("1stMPmt",0));

            decimal secondLienMPmt = GetMoney("SecondLienMPmt", 0);

            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;
            
            CPageData dataLoan = new CCheckFor2ndLoan(loanID);
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            LoanProgramByInvestorSet lpSet;

            try
            {
                lpSet = dataLoan.GetLoanProgramsToRunLpe(principal, sLienQualifyModeT, firstLienLpId);
            }
            catch (CLPERunModeException e)
            {
                SetResult("ErrorMessage", e.UserMessage);
                
                // This will activate a "reasons" link that shows the circumstances under which pricing can be run
                SetResult("ShowErrorReasons", true);
                return;
            }

            stopwatch.Stop();
            string tempLpeTaskMessageXml = "";
            if (dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct && sLienQualifyModeT == E_sLienQualifyModeT.OtherLoan)
            {
                // ^ If we are only going to rerun the registered program, we should have the
                //   task message XML on file, so we don't need to construct it for the 2nd.
                tempLpeTaskMessageXml = dataLoan.sTempLpeTaskMessageXml;
            }
            else if (sLienQualifyModeT == E_sLienQualifyModeT.OtherLoan)
            {
                // ^ Else if that is not the case and we are running the combo 2nd, we need
                //   to dummy up the 1st lien task message XML to re-apply the selected 1st
                //   to base the 2nd lien on.
                string requestedRate = string.Format($"{firstLienNoteRate}:{firstLienPoint}:{string.Empty}");

                tempLpeTaskMessageXml = DistributeUnderwritingEngine.GenerateTemporaryAcceptFirstLoanMessage(
                    principal: principal,
                    loanId: loanID,
                    productId: firstLienLpId,
                    requestedRate: requestedRate,
                    lpePriceGroupId: dataLoan.sProdLpePriceGroupId,
                    version: string.Empty,
                    debugResultStartTicks: "0",
                    renderResultModeT: renderResultModeT);
            }

            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            Guid requestID = DistributeUnderwritingEngine.SubmitToEngine(principal, loanID, lpSet, sLienQualifyModeT, dataLoan.sProdLpePriceGroupId, 
                tempLpeTaskMessageXml, renderResultModeT, E_sPricingModeT.RegularPricing, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, secondLienMPmt,
                options);

            Tools.LogVerbose("user " + principal.UserId + " (" + principal.LoginNm + ") has submitted a pricing request for loan " + loanID.ToString());

            int polling = DistributeUnderwritingEngine.GetSuggestedPollingInterval(requestID);

            SetResult("SetupRequestMs", stopwatch.ElapsedMilliseconds);
            SetResult("ProductCount", lpSet.LoanProgramsCount);
            SetResult("RequestID", requestID);
            SetResult("Polling", polling);

            debugMessage += Environment.NewLine + " Prepare Request " + stopwatch.ElapsedMilliseconds + " ms.";


            // 11/25/2009 dd - Perform polling on the server side. This to reduce the latency when client doing polling.
            
            int i = 0;
            bool hasResult = false;
            stopwatch.Reset();
            stopwatch.Start();

            int max = ConstStage.NumberOfHalfSecondPricingChecks;
            
            if (loanID == new Guid("F5CF5548-7957-4396-AF88-A3320081473D"))
            {
                // 6/25/2014 AV - 185400 Issue running PML ~ 8.4 minutes
                max = 1000;
            }

            while (i < max)// 1/19/2014 dd - Increase timeout to 2 minutes.
            {
                if (LpeDistributeResults.IsResultAvailable(requestID))
                {
                    hasResult = true;
                    break;
                }
                i++;
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(500); // Sleep before poll again.
            }
            stopwatch.Stop();
            if (hasResult)
            {
                SetResult("IsAvailable", "1");
            }
            else
            {
                // Call Timeout report.
                SetResult("IsAvailable", "0");
            }
            debugMessage += Environment.NewLine + " Polling in " + stopwatch.ElapsedMilliseconds + " ms.";
            debugMessage += Environment.NewLine + Tools.GetDBCurrentDateTime().ToLongTimeString() + " - End GetResult_SubmitUnderwriting";

            if (DistributeUnderwritingSettings.ServerT  == E_ServerT.AuthorTest)
            {
                debugMessage += Environment.NewLine + PerformanceStopwatch.ReportOutput;
            }
            LOCache.Set("DEBUG_" + requestID, debugMessage, DateTime.Now.AddMinutes(10));
            LOCache.Set("DEBUG_" + requestID + "_START_TICKS", startTime.Ticks, DateTime.Now.AddMinutes(10));
            
        }
        private void GetResult_IsAvailable() 
        {
            long duration = DateTime.Now.Ticks;
            Guid requestID = GetGuid("requestid");

            bool isAvailable = LpeDistributeResults.IsResultAvailable(requestID);

            duration = (DateTime.Now.Ticks - duration) / 10000L;
            SetResult("IsAvailable", isAvailable ? "1" : "0");
            SetResult("Duration", duration);

            string debugMsg = LOCache.Get("DEBUG_" + requestID) as string;
            if (null != debugMsg)
            {
                debugMsg += Environment.NewLine + Tools.GetDBCurrentDateTime().ToLongTimeString() + " - Finish Calling GetResult_IsAvailable";
                LOCache.Set("DEBUG_" + requestID, debugMsg, DateTime.Now.AddMinutes(10));
            }
        }
        private void GetResult_GetErrorMessage() 
        {
            Guid requestID = GetGuid("requestid");

            UnderwritingResultItem result = LpeDistributeResults.RetrieveResultItem(requestID);

            if (null != result && result.IsErrorMessage) 
            {
                SetResult("ErrorMessage", result.ErrorMessage);
            } 
            else 
            {
                SetResult("ErrorMessage", "");
            }

        }
        private void GetResult_ReportTimeout() 
        {
            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;

            try
            {
                Guid loanID = GetGuid("loanID");
                Guid[] requestIDs;
                Guid parseId;

                string id = GetString("RequestID");
                if (!Guid.TryParse(id, out parseId))
                {
                    if (Tools.TryGetGuidFrom(id, out parseId))
                    {
                        string cacheKey = GetMergeKeyFrom(principal.EmployeeId, parseId);
                        requestIDs = SerializationHelper.JsonNetDeserialize<Guid[]>(AutoExpiredTextCache.GetFromCache(cacheKey));
                    }
                    else
                    {
                        Tools.LogError("Could not report timeout for " + id);
                        return;
                    }
                }
                else
                {
                    requestIDs = new Guid[] { parseId };
                }

                string resultPollingCount = GetString("ResultPollingCount", "");
                string startedTime = GetString("StartedTime", "");

                DateTime timeoutTime = Tools.GetDBCurrentDateTime();

                StringBuilder sb = new StringBuilder();

                sb.Append("LoanId=" + loanID + ", Number Of Products=" + GetString("ProductCount", "") + ", principal=" + principal +
                    ", ResultPollingCount=" + resultPollingCount + "Started Time=" + startedTime + ", Timeout Time=" + timeoutTime);
                sb.Append(Environment.NewLine).Append(Environment.NewLine).Append("LPE_REQUEST").Append(Environment.NewLine).Append(Environment.NewLine);

                foreach (Guid requestId in requestIDs)
                {
                    SqlParameter[] parameters = { new SqlParameter("@LpeRequestBatchId", requestId) };

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "CaptureLpeRequestByBatchId", parameters))
                    {
                        while (reader.Read())
                        {
                            sb.AppendFormat("  LpeRequestBatchId={6}, CreatedDate={0}, ExpiredDate={1}, CurrentPartInBatch={2}, NumberOfRequestsInBatch={3}, LpeRequestType={4}, InvestorName={7}{5}",
                                reader["LpeRequestCreatedDate"], //0
                                reader["LpeRequestExpiredDate"], //1
                                reader["LpeRequestCurrentPartInBatch"], //2
                                reader["LpeRequestNumberOfRequestsInBatch"], //3
                                reader["LpeRequestType"], //4
                                Environment.NewLine, //5
                                requestId, //6
                                reader["InvestorName"] //7
                                );
                        }
                    }

                    sb.Append(Environment.NewLine).Append("LPE_RESULT").Append(Environment.NewLine).Append(Environment.NewLine);

                    parameters = new SqlParameter[] { new SqlParameter("@RequestBatchId", requestId) };

                    uint resultIndex = LpeDistributeResults.GetResultTableIndex(requestId);
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "CaptureLpeResultByBatchId_" + resultIndex, parameters))
                    {
                        while (reader.Read())
                        {
                            sb.AppendFormat("   ResultDebugInfo={0} ResultDoneD={1}{2}", reader["ResultDebugInfo"], reader["ResultDoneD"], Environment.NewLine);
                        }
                    }
                }

                CBaseException exc = new LPETimeoutException(sb.ToString());
                Tools.LogErrorWithCriticalTracking(exc);
            }
            catch { }
        }

        private void GetResult_MinuteWarning() 
        {
            Guid loanID = GetGuid("loanID");
            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;

            // 3/13/2006 dd - OPM 4324 - Only log as warning in PB.
            Tools.LogWarning("LPE 1 minute warning. LoanId=" + loanID + ", Number Of Products=" + GetString("ProductCount", "") + ", principal=" + principal);
        }

		// 3/29/2007 nw - OPM 10105 - let users know the CRA they want to pull or reissue credit is having technical difficulty
		private void CheckCRAWarningMessage()
		{
			Guid comId = GetGuid("comId");

			if (comId == Guid.Empty) 
			{
				throw new CBaseException("Require a valid credit report agency.", "comId is Guid.Empty");
			}

			CRA cra = MasterCRAList.FindById(comId, false);

			if (null == cra) 
			{
                PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal)this.User;

                var proxy = CreditReportAccountProxy.Helper.RetrieveCreditProxyByProxyID(principal.BrokerId, comId);
                if (proxy != null)
                {
                    comId = proxy.CraId;
                }

				cra = MasterCRAList.FindById(comId, false);
				if (null == cra) 
				{
					throw new CBaseException("Require a valid credit report agency.", "comId cannot be found");
				}
			}

			if (cra.IsWarningOn == true)
			{
				SetResult("IsWarningOn", true);
				SetResult("WarningMsg", cra.WarningMsg);
				SetResult("WarningStartD", string.Format("{0} {1} {2}", cra.WarningStartD.ToShortDateString(), cra.WarningStartD.ToShortTimeString(), TimeZone.CurrentTimeZone.IsDaylightSavingTime(cra.WarningStartD) ? "PDT" : "PST"));
			}
		}
	}
}
