<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="PriceMyLoan.Common"%>
<%@ Import Namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PropertyInfo.ascx.cs" Inherits="PriceMyLoan.UI.Main.PropertyInfo" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<asp:HiddenField runat="server" ID="sProHoAssocDuesPe" />
<asp:HiddenField runat="server" ID="sProHazInsPe" />
<asp:HiddenField runat="server" ID="sProMInsPe" />
<asp:HiddenField runat="server" ID="sProOHExpDescPe" />
<asp:HiddenField runat="server" ID="sProRealETxPe" />
<asp:HiddenField runat="server" ID="sProOHExpPe" />


<style type="text/css">
.hidden { display: none }
</style>
<script type="text/javascript">
var g_isIEBrowser = /*@cc_on!@*/false;

<%-- OPM 20881 av --%>
var isDoneLoading = false;
var gFhaStreamCredQual = <%=AspxTools.JsBool(m_showCreditQualifying) %>;
function f_prePostBack(event, callback) 
{
    callback(isDoneLoading) 
    return;
}
function f_focus_1stRequired() {
  var sSpAddr = <%= AspxTools.JsGetElementById(sSpAddr) %>;

  if (document.getElementById('_PreviousScroll').value != '') {
    document.body.scrollTop = document.getElementById('_PreviousScroll').value;
  } else if (!<%= AspxTools.JsBool(IsReadOnly) %>) {
    sSpAddr.focus();
  }
  
}



function f_isReadOnlyPageValid() {
    if (<%= AspxTools.JsBool(!m_usingNewEngine) %>) {
        return true;
    }
    var isValid = true;
    var sProdFilterDueValidator = <%= AspxTools.JsGetElementById(sProdFilterDueValidator) %>;
    var sProdRLckdDaysValidator = <%= AspxTools.JsGetElementById(sProdRLckdDaysValidator) %>;
    var sProdFilterFinMethValidator = <%= AspxTools.JsGetElementById(sProdFilterFinMethValidator) %>;
    
    if( sProdFilterDueValidator.AlwaysEnable) {
        isValid = isValid && sProdFilterDueValidator.isvalid;
    }
    if( sProdRLckdDaysValidator.AlwaysEnable ) {
        isValid = isValid && sProdRLckdDaysValidator.isvalid;
    }
    if ( sProdFilterFinMethValidator.AlwaysEnable ){
        isValid = isValid && sProdFilterFinMethValidator.isvalid;
    }
    return isValid;
}

function f_validatePage() {
  if (<%= AspxTools.JsBool(IsReadOnly) %>)
  {

    
    return f_isReadOnlyPageValid(); <%-- // There is no validation on readonly page. --%>
  } 

  //var ddl = <%= AspxTools.JsGetElementById(sSpStatePe) %>;
  //var validator = document.getElementById("sSpStatePeValidator");
  var bSpStateValid = <%= AspxTools.JQuery(sSpStatePe) %>.val() != "";
  var countyIsValid = $j("#sSpCounty").prop('selectedIndex') != -1; 
  
  var bValid = true;
  if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
    bValid = Page_ClientValidate();
  return bValid && bSpStateValid && countyIsValid;
}
function f_validatesProdFilterType(source, args)
{
  var sProdIncludeNormalProc = <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>;
  var sProdIsDuRefiPlus = <%= AspxTools.JsGetElementById(sProdIsDuRefiPlus) %>;
  var sProdIncludeMyCommunityProc = <%= AspxTools.JsGetElementById(sProdIncludeMyCommunityProc) %>;
  var sProdIncludeHomePossibleProc = <%= AspxTools.JsGetElementById(sProdIncludeHomePossibleProc) %>;
  var sProdIncludeFHATotalProc = <%= AspxTools.JsGetElementById(sProdIncludeFHATotalProc) %>;
  var sProdIncludeVAProc = <%= AspxTools.JsGetElementById(sProdIncludeVAProc) %>;
 var sProdIncludeUSDARuralProc = <%= AspxTools.JsGetElementById(sProdIncludeUSDARuralProc) %>;
 if ( sProdIncludeNormalProc.checked == true 
    || sProdIsDuRefiPlus.checked == true
    || sProdIncludeMyCommunityProc.checked == true
    || sProdIncludeHomePossibleProc.checked == true 
    || sProdIncludeFHATotalProc.checked == true 
    || sProdIncludeVAProc.checked == true
    || sProdIncludeUSDARuralProc.checked == true )
    args.IsValid = true;
  else
    args.IsValid = false;

}

<% if (m_isTotalEnabled) { %>
function f_viewTotalFindings() {
    //var ddl = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
    var isVaIRRRL = <%= AspxTools.JQuery(sLPurposeTPe) %>.val() === <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl) %>;
    //if (typeof ddl != 'undefined' && ddl != null && ddl.value){
//        isVaIRRRL = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>;
    //}
    if (isVaIRRRL) return false;

    var w = window.open('FHATotalFindings.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&winopen=t', '', 'toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=740,height=620');  
    if (null == w) {
      alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
    }
    return false;
}
<% } %>

<% if (IsAllowLpSubmission) { %>
var g_lpWindow;
function f_sendToLp(event) {
  var url = ML.VirtualRoot + '/main/LoanProspectorExportLogin.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>;
  LQBPopup.Show(url,
  {
    width: '600',
    height: '375',
    hideCloseButton: true,
    onReturn: function (ok, entryPoint) {
      if (ok) {
        document.getElementById('Launch').value = 'LP';
        document.getElementById('LpEntry').value = entryPoint;
        document.getElementById('_PreviousScroll').value = document.body.scrollTop;
        document.getElementById('sFileVersion').value = <%= AspxTools.JsString(ConstAppDavid.SkipVersionCheck) %>;
        f_switchTab(_currentIndex, true, null, event); <%-- Save current step. After saving the postback will launch DO/DU login dialog --%>
      }
    }
  });
}
function f_viewLpFeedback() {
    var w = window.open('ViewLPFeedbackFrame.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>, 'ExportLP' + <%= AspxTools.JsString(LoanID.ToString("N"))%>, 'toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600');  
    if (null == w) {
      alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
    }
    return false;

}

function f_launch(entryPoint, bIsBlockEditor) {
    var bMaximize = true;
    var bCenter = false;
  
    var w = 0;
    var h = 0;
    var left = 0;
    var top = 0;
  
    if (!bMaximize) {
      var preferedWidth = 1024;
      var preferedHeight = 768;
      w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
      h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
    } else {
      w = screen.availWidth - 10;
      h = screen.availHeight - 50;
    }

    g_lpWindow = window.open('LoanProspectorMainFrame.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&entrypoint=' + entryPoint, 'ExportLP' + <%= AspxTools.JsString(LoanID.ToString("N")) %>, 'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes');
  
    if (null == g_lpWindow) {
      alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
    } else {
      if (bIsBlockEditor) {
        f_displayMask(true, "Waiting for LP window to be closed to proceed.");
        f_detectLpWindowStatus();
      }
      
    }
    return false;
}
function f_detectLpWindowStatus() {
  if (g_lpWindow == null) {
  } else if (g_lpWindow.closed) {
    g_lpWindow = null;
    f_displayMask(false);
        f_reloadScreen();
    
  } else {
    window.setTimeout(f_detectLpWindowStatus, g_iPollingInterval);
  }
}
<% } %>

function f_onsProdIsDuRefiPlusClick(){
    try{
        var isVisible = document.getElementById('isdurefinance').style.display == '';
        var isChecked = <%= AspxTools.JsGetElementById(sProdIsDuRefiPlus) %>.checked;
        if (isVisible && isChecked){
            <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>.checked = true;
            <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>.disabled = true;
        }
        else{
            <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>.disabled = document.getElementById("sProdIncludeEnabled").value != "1" || <%= AspxTools.JsBool(m_EnableAusProcessingClick == false) %>;
        }
    }catch(e){}

}



var g_winDu = null;
var g_iPollingInterval = 1000;
var g_bIsDo = false;
var g_sUserName = null;
var g_sPassword = null;
var g_sDuCaseId = null;
var g_bHasAutoLogin = false;

function f_enableProdIsDuRefiPlus(bEnabled) {
<% if (m_isAsk3rdPartyUwResultInPml) { %>
<%= AspxTools.JsGetElementById(sProdIsDuRefiPlus) %>.disabled = !bEnabled;
<% } %>
}

function f_onsProdIsDuRefiPlusEnable() {
  var bIsReadOnly = <%= AspxTools.JsBool(IsReadOnly) %>;
  
  if (bIsReadOnly == false)
  {
    f_enableProdIsDuRefiPlus(true);
    document.getElementById("sProdIsDuRefiPlusEnabled").value = "1";
  }
  return false
}

function f_onAusProcessingClick() {
  var bEnabled = <%= AspxTools.JsBool(m_EnableAusProcessingClick) %>;
  if (bEnabled)
  {
    f_enableProdInclude(true);
  }
  return false;
}

// Enable the sProdInclude checkboxes
function f_enableProdInclude(bEnabled) {
  <% if (m_isAsk3rdPartyUwResultInPml) { %>
    <%= AspxTools.JsGetElementById(sProdIncludeMyCommunityProc) %>.disabled = !bEnabled;
    <%= AspxTools.JsGetElementById(sProdIncludeHomePossibleProc) %>.disabled = !bEnabled;
    <%= AspxTools.JsGetElementById(sProdIncludeFHATotalProc) %>.disabled = !bEnabled;
    <%= AspxTools.JsGetElementById(sProdIncludeVAProc) %>.disabled = !bEnabled;
    <%= AspxTools.JsGetElementById(sProdIncludeUSDARuralProc) %>.disabled = !bEnabled;

    try{
        var isVisible = document.getElementById('isdurefinance').style.display == '';
        var isChecked = <%= AspxTools.JsGetElementById(sProdIsDuRefiPlus) %>.checked;
        if (isVisible && isChecked){
            <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>.checked = true;
            <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>.disabled = true;
        }
        else {
            <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>.disabled = !bEnabled;
        }
    }catch(e){}
  <% } %>
  document.getElementById("sProdIncludeEnabled").value = bEnabled ? "1" : "0";
}
function f_onAusResponseClick() {
  var bIsReadOnly = <%= AspxTools.JsBool(IsReadOnly) %>;
  if (bIsReadOnly == false)
  {
    <%= AspxTools.JQuery(sProd3rdPartyUwResultT) %>.readOnly(false);
    document.getElementById("sProd3rdPartyUwResultTEnabled").value = "1";  
  }

  return false;
}

function f_displayMask(bDisplay, sWaitMsg) {
    f_displayBlockMask(bDisplay);
    <% if (PriceMyLoanConstants.IsEmbeddedPML) { %>
    parent.parent.treeview.f_displayBlockMask(bDisplay);
    parent.parent.info.f_displayBlockMask(bDisplay);
    parent.parent.header.f_disableCloseLink(bDisplay);
    <% } %>
    
    //var contentIframe = document.getElementById('contentIframe');
    //contentIframe.style.display = bDisplay ? '' : 'none';
    
    $j('#contentIframe').toggle(bDisplay);
    
    if (bDisplay) {
        if (null == sWaitMsg) {
            $j('#WaitingMessage').text( "Waiting for the " + (g_bIsDo ? "DO" : "DU") + " window to be closed to proceed.");
        } else {
            $j('#WaitingMessage').text( sWaitMsg);
        }

        var width = 300;
        var height = 150;

        var left = f_getScrollLeft() + (f_getViewportWidth() - width) / 2;
        var top = f_getScrollTop() + (f_getViewportHeight() - height) / 2;
        $j('#contentIframe').css({'width':width, 'height':height, 'position':'absolute', 'left':left, 'top':top});
    }
}



function f_submitToDoDu(event, bIsDo) {
  document.getElementById('_PreviousScroll').value = document.body.scrollTop;
  document.getElementById('Launch').value = bIsDo ? 'DO' : 'DU';
  document.getElementById('sFileVersion').value = <%= AspxTools.JsString(ConstAppDavid.SkipVersionCheck) %>;

  f_switchTab(_currentIndex, true, null, event); <%-- Save current step. After saving the postback will launch DO/DU login dialog --%>
}


function f_submitToDoDuImpl(bIsDo) {
  if (document.getElementById('_PreviousScroll').value != '') {
    document.body.scrollTop = document.getElementById('_PreviousScroll').value;
  }  
  <%-- // Launch Login --%>
  var args = { test:true};
  LQBPopup.Show(gVirtualRoot + '/main/FannieMaeExportLogin.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> +'&isdo=' + (bIsDo ? 't' : 'f'),
          {
            onReturn:_FannieMaeExportLoginCallback,
            width:580,
            height:280,
            hideCloseButton:true
          }, args);
  /*
  showModal('/main/FannieMaeExportLogin.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> +'&isdo=' + (bIsDo ? 't' : 'f'), args, 'Submit Directly To Fannie Mae');
  if (args.Logout) f_logout();
  if (args.OK) {
    g_bIsDo = args.bIsDo;
    g_sUserName = args.sUserName;
    g_sPassword = args.sPassword;
    g_sDuCaseId = args.sDuCaseId;
    g_bHasAutoLogin = args.bHasAutoLogin
    f_launchDODU(args.CacheId);
    f_displayMask(true);
    f_pollingDuWin();
  }
  document.getElementById('btnDo').focus();
  */    
}
function _FannieMaeExportLoginCallback(args)
{
  if (args.Logout) f_logout();
  if (args.OK) {
    g_bIsDo = args.IsDo;
    g_sUserName = args.sUserName;
    g_sPassword = args.sPassword;
    g_sDuCaseId = args.sDuCaseId;
    g_bHasAutoLogin = args.bHasAutoLogin
    f_launchDODU(args.CacheId);
    f_displayMask(true);
    f_pollingDuWin();
  }

}
function f_reloadScreen() {
  var previousScroll = '';
  if (document.getElementById('_PreviousScroll').value != '') {
    previousScroll = document.getElementById('_PreviousScroll').value;
  }   
  self.location = <%= AspxTools.JsString(VirtualRoot) %> + '/Main/main.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&tabkey=' + <%=AspxTools.JsString(TabID) %> + '&previousIndex= ' + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("previousIndex"))%> + '&maxVisitedIndex=' + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("maxVisitedIndex"))%> + '&_PreviousScroll=' + previousScroll;
}
function f_onViewDuFindingsClick(sIsDo) {
	var report_url = gVirtualRoot + '/Main/ViewDUFindingsFrame.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&isdo=' + sIsDo;
	var oWin = window.open(report_url, '_blank', 'width=700,height=500, location=no, menubar=no');
	if (oWin == null)
	{
	  alert('Could not open window. Your browser has popup blocker turn on.');
	}
  //_popupWindow = showSoloModeless( report_url, "dialogWidth: 700px; dialogHeight: 500px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;" );
	return false;  
}

function f_pollingDuWin() {
  if (g_winDu == null) {
  } else if (g_winDu.closed) {
    g_winDu = null;
    f_displayMask(false);
    f_downloadData();
    
  } else {
    window.setTimeout(f_pollingDuWin, g_iPollingInterval);
  }
}
function f_downloadData() {
  var args = {
    bIsDo : g_bIsDo,
    sUserName : g_sUserName,
    sPassword : g_sPassword,
    sDuCaseId : g_sDuCaseId,
    bHasAutoLogin : g_bHasAutoLogin
  };
  LQBPopup.Show(gVirtualRoot +'/main/FannieDownloadOptions.aspx?loanid=' + <%= AspxTools.JsString(LoanID)%>, 
    {
      onReturn:_downloadDataCallback,
      width:500,
      height:450,
      hideCloseButton:true
    },
    args);
}

function showOtherExpenses(){
    var args = { 
        sProRealETxPe : document.getElementById('<%= AspxTools.ClientId(sProRealETxPe)%>').value,
        sProHazInsPe : document.getElementById('<%= AspxTools.ClientId(sProHazInsPe)%>').value,
        sProHoAssocDuesPe : document.getElementById('<%= AspxTools.ClientId(sProHoAssocDuesPe)%>').value, 
        sProMInsPe: document.getElementById('<%= AspxTools.ClientId(sProMInsPe)%>').value,
        sProOHExpPe : document.getElementById('<%= AspxTools.ClientId(sProOHExpPe)%>').value, 
        sMonthlyPmtPe : document.getElementById('<%= AspxTools.ClientId(sMonthlyPmtPe)%>').value, 
        sProOHExpDescPe : document.getElementById('<%= AspxTools.ClientId(sProOHExpDescPe)%>').value 
        
    };
    LQBPopup.Show(gVirtualRoot + '/main/AdditionalMonthlyExpenses.aspx?loanid=' + <%= AspxTools.JsString(LoanID)%>,{
              onReturn:showOtherExpensesCallback,
      width:300,
      height:250,
      hideCloseButton:true
      
    }, args);
    return false;
}

function showOtherExpensesCallback(args){
    document.getElementById('<%= AspxTools.ClientId(sProRealETxPe)%>').value = args.sProRealETxPe;
    document.getElementById('<%= AspxTools.ClientId(sProHazInsPe)%>').value = args.sProHazInsPe;
    document.getElementById('<%= AspxTools.ClientId(sProHoAssocDuesPe)%>').value = args.sProHoAssocDuesPe;
    document.getElementById('<%= AspxTools.ClientId(sProMInsPe)%>').value = args.sProMInsPe;
    document.getElementById('<%= AspxTools.ClientId(sProOHExpPe)%>').value = args.sProOHExpPe;
    document.getElementById('<%= AspxTools.ClientId(sMonthlyPmtPe)%>').value = args.sMonthlyPmtPe;
    document.getElementById('<%= AspxTools.ClientId(sProOHExpDescPe)%>').value = args.sProOHExpDescPe;
}

function _downloadDataCallback(args)
{
  if (args.Logout) f_logout();
  
  if (args.OK) {
    f_reloadScreen();
  }
}
function f_launchDODU(sCacheId) {
  var bMaximize = true;
  var bCenter = false;
  
  var w = 0;
  var h = 0;
  var left = 0;
  var top = 0;
  
  if (!bMaximize) {
    var preferedWidth = 1024;
    var preferedHeight = 768;
    w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
    h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
  } else {
    w = screen.availWidth - 10;
    h = screen.availHeight - 50;
  }

  g_winDu = window.open('LaunchDODU.aspx?id=' + sCacheId, 'ExportLogin_' + <%= AspxTools.JsString(LoanID.ToString("N"))%>, 'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes');
  if (g_winDu != null) 
  {
    g_winDu.focus();
  }
  else
  {
    alert('Unable to open new window. Check your browser for popup-blocker.');
  }

}


function f_validatesProdFhaFMIP(sender, args) {
    var sProdFhaFMIP = args.Value;
    var sProdFhaFMIPNegErrorMsg = document.getElementById('sProdFhaFMIPNegErrorMsg');
    
    f_validateNegativeNonRequired(sProdFhaFMIPNegErrorMsg, !!sProdFhaFMIP.match('-'), args);
}

function f_validateNegativeNonRequired(error, isNotValid, args) {
    args.IsValid = !isNotValid;
    $j(error).toggle(isNotValid);
}

function getMoneyValue(str){
    var val = null;
    try {
       if (!str) return null;
       if (str.length == 0) return null;
       var _v = str.replace(/\$/,'').replace(/,/g,'');
       
       if (_v.match(/^\((.+\))$/)){
            _v = _v.substring(1,_v.length-2);
            _v = "-" + _v;
       }
       if (!isNaN(_v)){
           val = parseFloat(_v);
       }
    } catch (e) {}
    return val;
}

function f_validatesSpLien(source, args){
    try {
        var o = <%= AspxTools.JsGetElementById(sSpLien) %>;
        var sSpLien = getMoneyValue(o.value);
        args.IsValid = (sSpLien != null && sSpLien > 0);
    } catch (e) {
        args.IsValid = false;
    }
    visible(<%= AspxTools.JsGetElementById(ErrorsSpLienImg) %>, !args.IsValid);
}

function f_validatesFHASalesConcessions(source, args){
    try {
        var o = <%= AspxTools.JsGetElementById(sFHASalesConcessions) %>;
        var sFHASalesConcessions = getMoneyValue(o.value);
        args.IsValid = (sFHASalesConcessions != null && sFHASalesConcessions >= 0);
    } catch (e) {
        args.IsValid = false;
    }   
}

function f_validatesTotalRenovationCosts(source, args) {
     var $cb = <%= AspxTools.JQuery(sIsRenovationLoan) %>;
     if ($cb.length == 0) {
        args.IsValid = true;
        return;
     }
    try {
        var o = <%= AspxTools.JsGetElementById(sTotalRenovationCosts) %>;
        var sTotalRenovationCosts = getMoneyValue(o.value);
        args.IsValid = (sTotalRenovationCosts != null && sTotalRenovationCosts > 0);
    } catch (e) {
        args.IsValid = false;
    }
    return;
}

function f_sAsIsAppraisedValuePeval(source, args) {
    var o = <%= AspxTools.JsGetElementById(sAsIsAppraisedValuePeval) %>;
    var d = $j('#negAsIsValue');
    var data = getMoneyValue(o.value);
    args.IsValid = data >= 0;   
    d.toggle(!args.IsValid);
}

function f_validatesOriginalAppraisedValue(source, args){
    try {
        var o = <%= AspxTools.JsGetElementById(sOriginalAppraisedValue) %>;
        var sOriginalAppraisedValue = getMoneyValue(o.value);
        args.IsValid = (sOriginalAppraisedValue != null && sOriginalAppraisedValue >= 0);
    } catch (e) {
        args.IsValid = false;
    }
}

function validateCashoutAmount(source, args) {
    if (args.Value == null || args.Value.trim() === '') {
        args.IsValid = false;
        return;
    }

    const numericValue = parseMoneyFloat(args.Value);
    if (isNaN(numericValue)) {
        args.IsValid = false;
        return;
    }

    return numericValue != 0 || ML.IsStandAloneSecond;
}

function f_onValidateRequiredFields() {

  var jsReadOnly = <%= AspxTools.JsBool(IsReadOnly) %>;
  
  var validator = document.getElementById("sSpStatePeValidator");
  var bSpStateValid = <%= AspxTools.JQuery(sSpStatePe) %>.val() != "";
  $j('#sSpStatePeValidator').toggle(bSpStateValid == false && jsReadOnly == false);
  
  var countyIsValid  = $j("#sSpCounty").prop('selectedIndex') != -1;  <%--AS of opm 18913 County is required--%>
  $j('#sSpCountyValidator').toggle(countyIsValid == false && jsReadOnly == false);
  //document.getElementById("sSpCountyValidator").style.display = ( countyIsValid || <%= AspxTools.JsBool(IsReadOnly) %> ) ? "none" : "";
  <% if (!m_renderAsSecond) { %>
    var sLPurpposeTPe = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;

    var bRefiCashout = sLPurpposeTPe.value == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %>;
    <%= AspxTools.JsGetElementById(sProdCashoutAmtValidator) %>.enabled = bRefiCashout && !jsReadOnly;
    var isVaIRRRL = sLPurpposeTPe.value == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>;
  <% } else { %>
    <%= AspxTools.JsGetElementById(sProdCashoutAmtValidator) %>.enabled = ML.IsStandAloneSecond;
    var isVaIRRRL = false;
  <% } %>

  var bRequiredIncome = isIncomeRequired();
  
  <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPeValidator) %>.enabled = bRequiredIncome && !<%= AspxTools.JsBool(IsReadOnly) %> && !g_disableCredQualValidators;
  
  <% if (m_isShowsProdEstimatedResidualI) { %>
    <%= AspxTools.JsGetElementById(sProdEstimatedResidualIValidator) %>.enabled = bRequiredIncome && !<%= AspxTools.JsBool(IsReadOnly) %> && !g_disableCredQualValidators;
  <% } %>
  
  
  var bValid = true;
  if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object') {
    bValid = Page_ClientValidate();
  }
    
  var isReadOnly =  <%= AspxTools.JsBool(IsReadOnly) %> && f_isReadOnlyPageValid();

  f_enableAllButtons(bSpStateValid && bValid && countyIsValid || isReadOnly);
    <%-- // 12/26/07 mf OPM 19565 - Disable when page is readonly. --%>
    f_enableBtnById('btnDO',  bSpStateValid && bValid && !<%= AspxTools.JsBool(IsReadOnly) %> && countyIsValid);
    f_enableBtnById('btnDU',  bSpStateValid && bValid && !<%= AspxTools.JsBool(IsReadOnly) %> && countyIsValid);
    if (g_isIEBrowser)
    {
      f_enableBtnById('btnSendToLp',  bSpStateValid && bValid && !<%= AspxTools.JsBool(IsReadOnly) %> && countyIsValid);
    }
    
    <% if (m_isTotalEnabled) { %>
      f_enableBtnById('btnSendToTotal',  bSpStateValid && bValid && !<%= AspxTools.JsBool(IsReadOnly) %> && countyIsValid && !isVaIRRRL);
      var btnViewFindings = $j('#btnViewFindings');
      if (btnViewFindings.length > 0){
        btnViewFindings.html( isVaIRRRL ? "No FHA TOTAL Scorecard Findings" : "View FHA Total Scorecard Findings");
        btnViewFindings.prop('disabled', isVaIRRRL);
      }
    <% } %>
}

function isIncomeRequired(){
    var sProdDocT = <%= AspxTools.JQuery(sProdDocT) %>.val()
    var noIncomeList = <%= AspxTools.JsArray(E_sProdDocT.NINANE, E_sProdDocT.NINA, E_sProdDocT.NIVA, E_sProdDocT.NISA, E_sProdDocT.NIVANE, E_sProdDocT.DebtServiceCoverage, E_sProdDocT.NoIncome)  %>;
    var bRequiredIncome = $j.inArray(sProdDocT, noIncomeList) == -1 ;
    return bRequiredIncome;
}

function f_sOccT_onchange() {
  var sOccTPe = <%= AspxTools.JQuery(sOccTPe) %>;
  var bHousingExpense = sOccTPe.val() != "0";
  var bRental = sOccTPe.val() == "2";
  var showNonOccupantCoborrCB = (sOccTPe.val() == "0") && jQuery('#HasMultipleBorrowers').val() == "True";
  
  $j('#presentHousing').toggle(bHousingExpense);

  f_showInvestmentPanel(bRental);
  f_updateTexasCB();
  f_showNonOccupantCoborrower(showNonOccupantCoborrCB);
}
function f_showNonOccupantCoborrower(show){
    jQuery('#HasNonOccupantCoborrower').toggle(show);
}

function f_showInvestmentPanel(bVisible) {
  $j("#investmentPanel").toggle(bVisible);
  $j("#prSpace").toggle(!bVisible);
}
function visible(obj,status)
{
  if (!obj)
    return;
    
  $j(obj).toggle(status);
}

function f_uc_init() {
  <%= AspxTools.JQuery(sOccTPe) %>.change(f_sOccT_onchange);

  <%= AspxTools.JQuery(sDownPmtPcPe, sOriginatorCompensationBorrPaidPc) %>.data('sProdCalcEntryT', '0');
  <%= AspxTools.JQuery(sEquityPe) %>.data('sProdCalcEntryT', '1');
  <%= AspxTools.JQuery(sLtvRPe) %>.data('sProdCalcEntryT', '2');
  <%= AspxTools.JQuery(sLAmtCalcPe) %>.data('sProdCalcEntryT', '3');
  <%= AspxTools.JQuery(sCltvRPe) %>.data('sProdCalcEntryT', '4');
  <%= AspxTools.JQuery(sLtvROtherFinPe) %>.data('sProdCalcEntryT', '5');
  <%= AspxTools.JQuery(sProOFinBalPe) %>.data('sProdCalcEntryT', '6');
  <%= AspxTools.JQuery(sHCLTVRPe) %>.data('sProdCalcEntryT', '9');
  <%= AspxTools.JQuery(sSubFinPe) %>.data('sProdCalcEntryT', '5');
  <%= AspxTools.JQuery(sSubFinToPropertyValue) %>.data('sProdCalcEntryT', '8');
  <%= AspxTools.JQuery(sApprValPe) %>.data('sProdCalcEntryT', '7');
  <%= AspxTools.JQuery(sHouseValPe) %>.data('sProdCalcEntryT', '7');
  <%= AspxTools.JQuery(sIsRenovationLoan) %>.data('sProdCalcEntryT', '10');
  <%= AspxTools.JQuery(sTotalRenovationCosts) %>.data('sProdCalcEntryT', '10');
  <%= AspxTools.JQuery(sAsIsAppraisedValuePeval) %>.data('sProdCalcEntryT', '10');

  <%= AspxTools.JQuery(sDownPmtPcPe,sEquityPe)  %>.change(function(event)
        {
          f_onchange_sProdCalcEntryFields(event.target);
        });
        
  <% if( m_missingCounty ) { %> 
	document.getElementById("sSpCounty").selectedIndex = -1;
  <% } %>
  
  f_onchange_sLPurposeTPe(true);
  f_sOccT_onchange();
  
  <% if (m_isAutoPmiOrNewPml ) { %>
  f_setsProdConvMIOptionT(<%= AspxTools.JsNumeric((int) sLtv80TestResultT) %>, true );  
  <% } else { %>
  f_setsProdMIOptionT(<%= AspxTools.JsNumeric((int) sLtv80TestResultT) %>, true );  
  <% } %>
  
  
  f_onchange_sProdCashoutAmt(); <%-- // 04/12/07 mf OPM 12395 --%>
  f_setCalcOldValues(); <%-- // 05/04/07 mf. OPM 12557 --%>
  f_onchange_sProdDocT(); <%-- // 05/04/07 mf. OPM 16288 --%>
  f_onchange_sOriginatorCompensationPaymentSourceT(true);
  f_onchange_sIsRenovationLoan();
  
  <% if (IsAllowDoSubmission || IsAllowDuSubmission) { %>
  if (document.getElementById("sProd3rdPartyUwResultTEnabled").value == "0") {
    f_enableProdInclude(false);
  }
  if (document.getElementById("sProdIsDuRefiPlusEnabled").value == "0") {
    f_enableProdIsDuRefiPlus(false)
  }
  <% } %>
  <% if(m_renderAsSecond) { %>
  f_renderAsSecond();
  
  <% } %>
 
  $j("#btnContinue").prop('tabIndex', -1);
  //btn.tabIndex = -1;
  $j("#btnGetResult").prop('tabIndex', 100);
  //btn.tabIndex = 100;

  <% if ( RequestHelper.GetSafeQueryString("launch") != null && (RequestHelper.GetSafeQueryString("launch") == "DO" || RequestHelper.GetSafeQueryString("launch") == "DU")) { %>
    f_submitToDoDuImpl( <%= AspxTools.JsBool(RequestHelper.GetSafeQueryString("launch") == "DO") %> );
  <% } else if (RequestHelper.GetSafeQueryString("launch") != null && RequestHelper.GetSafeQueryString("launch") == "LP") {%>
    f_launch(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("LpEntry")) %>, true);
  <% } else if (RequestHelper.GetSafeQueryString("launch") != null && RequestHelper.GetSafeQueryString("launch") == "TOTAL") {%>
        <% if (m_isTotalEnabled) { %>f_launchTotal();<% } %>
  <% } %>

  document.getElementById('sSpCounty').disabled = <%= AspxTools.JsBool(IsReadOnly) %>;  
  
  f_show2ndFinancing(true);
  f_hihghlight2ndFinancing(false);
  f_updateCreditQualifyingCB();
  f_updateVaFha();
  
  isDoneLoading = true; 
  
  if (!g_isIEBrowser)
  {
  <%-- Loan Prospector does not work in non IE --%>
  $j('#btnSendToLp').prop('title', 'Send to LP only available in Internet Explorer browser.').prop('disabled', true);
  }
}

function f_onChange2ndDdl( value, type )
{
  <%--
  // OPM 4442 mf. User picked a mortgage trade from one of the dropdowns.
  // we load up the value and re-calc.  Currently we trust the value
   --%>
  if ( value.indexOf(':') > 0 )
  {
    var moneyVal = value.substr(0,value.indexOf(':') - 1);
    var controlToSet = (type == 'pmt') ? <%= AspxTools.JsGetElementById(sProOFinPmtPe) %> : <%= AspxTools.JsGetElementById(sProOFinBalPe) %>;
    if (typeof(controlToSet.sProdCalcEntryT) != 'undefined')
		document.getElementById('sProdCalcEntryT').value = controlToSet.sProdCalcEntryT;
  
    controlToSet.value = moneyVal;
    $j(controlToSet).trigger('blur');  <%-- // To format $ text --%>
    $j(controlToSet).trigger('change'); <%-- // To format $ text --%>

  }
}

function f_update2ndFinancingType(isinit){
    var closeEnd = <%= AspxTools.JsGetElementById(bCloseEnd2ndFinancing) %>,
        heloc = <%= AspxTools.JsGetElementById(bHeloc2ndFinancing) %>,
        sProOFinBalPeLabel = <%= AspxTools.JsGetElementById(sProOFinBalPeLabel) %>,
        HelocFields = document.getElementById('HelocFields'),
        sSubFinPeValidator = document.getElementById('<%= AspxTools.ClientId(sSubFinPeValidator) %>');

        $j("#RequestCommunityOrAffordableSecondsPanel").toggle(!heloc.checked);

        ValidatorEnable(sSubFinPeValidator, !closeEnd.checked);
        
        if( closeEnd.checked ){
            $j(sProOFinBalPeLabel).text('2nd Financing');
            $j(HelocFields).css('display', 'none');
        }
        else{
            $j(HelocFields).css('display', 'block');
            $j(sProOFinBalPeLabel).text('Initial Draw Amount');
        }
        
        f_onValidateRequiredFields();
        
        if (!!!isinit){
            $j('#sProdCalcEntryT').val(3);
            refreshCalculation();
        }
}


function f_show2ndFinancing(isinit)
{
  var no = <%= AspxTools.JsGetElementById(bNo2ndFinancing) %>,
      yes = <%= AspxTools.JsGetElementById(bYes2ndFinancing) %>,
      row = document.getElementById('SecondFinancingRow'),
      l2Pct = <%= AspxTools.JsGetElementById(sLtvROtherFinPe) %>,
      panel = $j('#SecondFinancingCLTVPanel'),
      StandaloneCLTVPanel = $j('#StandaloneCLTVPanel'),
      moveAttr = panel.attr('move'),
      isMoved = moveAttr === 'moved',
      sSubFinPeValidator = document.getElementById('<%= AspxTools.ClientId(sSubFinPeValidator) %>');
      
      
  if (no.checked)
  {
      ValidatorEnable(sSubFinPeValidator, false);
      row.style.display = 'none';
      if (l2Pct.value != '0.000%')
      {
        l2Pct.value = '0.000%';
        f_onchange_sProdCalcEntryFields(l2Pct);
      }   
      if( isMoved )
      {
        moveControls(panel,StandaloneCLTVPanel );
        panel.attr('move', '');
      }
  }
  else 
  {
    row.style.display = '';
    f_hihghlight2ndFinancing(false);

    if(!isMoved )
    {
        moveControls(StandaloneCLTVPanel, panel);
        panel.attr('move',  'moved');
    }
    
    f_update2ndFinancingType(isinit);
  }
}

function f_hihghlight2ndFinancing(bool)
{
  var leftRow = document.getElementById('highlight1');
  var rightRow = document.getElementById('highlight2');
  var textRow = document.getElementById('highlight3');
  
  leftRow.style.backgroundColor = bool ? '#FFFFCC' : '';
  rightRow.style.backgroundColor = bool ? '#FFFFCC' : '';
  textRow.style.display = bool ? 'inline-block' : 'none';

}

function f_renderAsSecond()
{
  <%--
  // OPM 4442 mf. 8/07/07
  // First LTV and Balance/Amt becomes "Other Financing"
  // Using the dom to move the elements on the client.
  --%>
  var sLtvRPe = <%= AspxTools.JQuery(sLtvRPe) %>;
  var sLtvROtherFinPe = <%= AspxTools.JQuery(sLtvROtherFinPe) %>;
  var FirstBal = $j('#FirstBal');
  var SecondBal = $j('#SecondBal');
  
  moveControls(sLtvRPe, sLtvROtherFinPe);
  moveControls(FirstBal, SecondBal);
  
  var tmpTabIndex = sLtvROtherFinPe.prop('tabIndex');
  sLtvROtherFinPe.prop('tabIndex', sLtvRPe.prop('tabIndex'));
  sLtvRPe.prop('tabIndex', tmpTabIndex);
  
  tmpTabIndex = <%= AspxTools.JQuery(sLAmtCalcPe) %>.prop('tabIndex');
  <%= AspxTools.JQuery(sLAmtCalcPe) %>.prop('tabIndex', <%= AspxTools.JQuery(sProOFinBalPe) %>.prop('tabIndex'));
  <%= AspxTools.JQuery(sProOFinBalPe) %>.prop('tabIndex', tmpTabIndex);
}

function moveControls( control1, control2 )
{
  var ctrl1Data = $j(control1).data();
  var ctrl2Data = $j(control2).data();
  $j(control1).removeData();
  $j(control2).removeData();
  var temp = $j(document.createTextNode('')).insertBefore(control1);
  //$j(temp).data(ctrl1Data);
  $j(control2).replaceWith(control1.data(ctrl1Data)).data(ctrl2Data).insertBefore(temp);
  
  //var tempNode = document.createTextNode("temp");
  
  //control1.replaceNode(tempNode);
  //control2.replaceNode(control1);
  //tempNode.replaceNode(control2);
}

function getSourceId(){
    var sourceId;
    try{
        if (event && event.srcElement)
            sourceId = event.srcElement.id
    }catch(e){}
    return sourceId;
}

function f_onchange_sLPurposeTPe(bInit) {
    if (!bInit) {
        var source = getSourceId();
        refreshCalculation(source);
    }

    var ddl = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
    var $isRenovationLoan = <%= AspxTools.JQuery(sIsRenovationLoan) %>;
    <% if (!m_renderAsSecond) { %> 
    f_update_PriorSalesPanel();

    var value = ddl.value;
    var sProdCashoutAmt = <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>;
    
    $j(sProdCashoutAmt).readOnly(
        <%= AspxTools.JsBool(IsReadOnly) %> ||
        (!ML.IsStandAloneSecond &&
        value != <%= AspxTools.JsString(E_sLPurposeT.Other.ToString("D")) %> &&
        value != <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %>));
   
    var isRefinance = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.Refin.ToString("D")) %> || ddl.value == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %>;
    var isPurchase = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %>;
    var isOther = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.Other.ToString("D")) %> || ddl.value == <%= AspxTools.JsString(E_sLPurposeT.Construct.ToString("D")) %> || ddl.value == <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm.ToString("D")) %>;
    var isFHARefinance = ddl.value ==  <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance.ToString("D")) %> || ddl.value ==  <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>;
    var isFHAStreamlinedRefinance = ddl.value ==  <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance.ToString("D")) %>; 
    var isVaIRRRL = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>;

    var bRefiCashout = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %>;
    <%= AspxTools.JsGetElementById(sProdCashoutAmtValidator) %>.enabled = ML.IsStandAloneSecond || bRefiCashout;
    if (!bRefiCashout && !ML.IsStandAloneSecond && ddl.value != <%= AspxTools.JsString(E_sLPurposeT.Other.ToString("D")) %>) {
        sProdCashoutAmt.value = "$0.00";        
    }
  
    if( isFHAStreamlinedRefinance ) {
        $j('#EndorsedPanel').css('display', 'block');
    } else {
        $j('#EndorsedPanel').css('display', 'none');    
    }
  
    var vaPanel = document.getElementById('VaFundingFeeSection');
    if (vaPanel) {
        vaPanel.style.display = isFHAStreamlinedRefinance ? 'none' : 'block';
        var validator = document.getElementById('<%=AspxTools.ClientId(sProdVaCustomValidator)%>');
        validator.enabled = !isFHAStreamlinedRefinance ; 
    }
    //var usdaPanel = document.getElementById('IsUsdaRuralHousingSection');
    //var sProdIsSpInRuralArea = document.getElementById('<%=AspxTools.ClientId(sProdIsSpInRuralArea) %>'); 
    //usdaPanel.style.display = isFHARefinance || isVaIRRRL || !sProdIsSpInRuralArea.checked ? 'none' : 'block'; 
    var priorSalesPanel = document.getElementById('masterPriorSalesPanel');
    priorSalesPanel.style.display = isPurchase ? '' : 'none';
  
    f_setFthb();
    
    <% } else { %>
    var isRefinance = true;
    var isPurchase = false;
    var isFHARefinance = false;
    var isVaIRRRL = false;
    <% } %>

    var $label = $j("#DownPaymentLabel");
    var $appraisedValLabel = <%= AspxTools.JQuery(sApprValPeLabel) %>;
    var $houseValueLabel = <%= AspxTools.JQuery(sHouseValPeLabel) %>;
    var $docTypeDDL = <%= AspxTools.JQuery(sProdDocT) %>;
    

    
    f_updatePurchaseAppraisalPanel(isPurchase);
    
    // Show the as-is appraised value 
    var sAsIsAppraisedValuePevalNegativeValidator = document.getElementById('<%= AspxTools.ClientId(sAsIsAppraisedValuePevalNegativeValidator) %>');
    if ($isRenovationLoan.prop('checked') && (isPurchase || isRefinance)) {
        $j('#sAsIsAppraisedValuePevalPanel').show();
        ValidatorEnable(sAsIsAppraisedValuePevalNegativeValidator, true);  
    } else {
        ValidatorEnable(sAsIsAppraisedValuePevalNegativeValidator, false);  
        $j('#sAsIsAppraisedValuePevalPanel').hide();
    }
    
    // Label changes for as-completed value
    if ($isRenovationLoan.prop('checked') && isPurchase) {
        $appraisedValLabel.text("As-Completed Value");
    } else {
        $appraisedValLabel.text("Appraised Value");
    }
  
    // Label changes for house value
    <%= AspxTools.JQuery(sHouseValPeRequireValidator) %>.toggle(true);
    if (isRefinance || isFHARefinance) {
        $houseValueLabel.text("Home Value");
        if ($isRenovationLoan.prop('checked') && isRefinance) {
            $houseValueLabel.text("As-Completed Value");
            <%= AspxTools.JQuery(sHouseValPeRequireValidator) %>.toggle(false);
        }
        $label.text("Equity");
    } else if (isPurchase) {
        $houseValueLabel.text("Sales Price");
        $label.text( "Down Payment");
    } else {
        $houseValueLabel.text("Home Value");
        $label.text("Down Payment / Equity");
    }
    
    if (isFHARefinance) {
        $docTypeDDL.prop('selectedIndex', 0);
        $docTypeDDL.readOnly(true);
        //doctypeDDL.selectedIndex = 0;
        //doctypeDDL.disabled = true;
    } else {
        $docTypeDDL.readOnly( <%= AspxTools.JsBool(IsReadOnly) %>);
    }
    
    f_onchange_sProdDocT(); <%-- //opm 27153 fs 02/10/09 --%>;
  
    duRefinance = document.getElementById('isdurefinance'); 

    f_updateTexasCB();
    f_updateCreditQualifyingCB();
    f_updateVaFha();

    if ( duRefinance === null ) {
        return; 
    }
  
    var sProdIsDuRefi = <%= AspxTools.JsGetElementById(sProdIsDuRefiPlus) %>;
    var sProdIncludeNormal = <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>;
    var sProdIncludeEnable = document.getElementById("sProdIncludeEnabled");
    try {
    
        if ( ddl != null && ddl.value == <%= AspxTools.JsString(E_sLPurposeT.Refin.ToString("D")) %> ) {
            duRefinance.style.display = ''; 
            if (!sProdIsDuRefi || !sProdIncludeNormal)
                return;
            if (sProdIsDuRefi.checked){
                if (!bInit){
                    sProdIncludeNormal.checked = true;
                }
                sProdIncludeNormal.disabled = true;
            }
        } else {
            duRefinance.style.display = 'none'; 
            if (!sProdIncludeEnable || !sProdIncludeNormal)
                return;
            sProdIncludeNormal.disabled = sProdIncludeEnable.value != "1" || <%= AspxTools.JsBool(m_EnableAusProcessingClick == false) %>;
        }
    } catch(e) {}
}

function f_updatePurchaseAppraisalPanel(isPurchase){
    var doc = document;
    var appraisalPanel = doc.getElementById('sPurchaseAppraisalPanel'); 
    <%= AspxTools.JsGetElementById(sApprValPeValidator) %>.enabled = isPurchase;
    appraisalPanel.style.display = isPurchase ? '' : 'none';
}

function f_updateVaFha(){
    var f_up_inner_line = 0;
    try{
        var v1 = <%= AspxTools.JsGetElementById(sSpLienValidator) %>;
        f_up_inner_line = 1;
        var v2 = <%= AspxTools.JsGetElementById(sProdFhaUfmipValidator) %>;
        f_up_inner_line = 2;
        var v3 = <%= AspxTools.JsGetElementById(sFHASalesConcessionsValidator) %>;
        f_up_inner_line = 3;
        var v4 = <%= AspxTools.JsGetElementById(sOriginalAppraisedValueValidator) %>;
        f_up_inner_line = 4;
        
        <% if (!m_renderAsSecond) { %>
        var ddl = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
        f_up_inner_line = 5;
        var isVaIRRRL = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl) %>;
        f_up_inner_line = 6;
        var isFhaStreamline = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance) %>;
        f_up_inner_line = 7;
        var bHasAppraisal = <%= AspxTools.JsGetElementById(sHasAppraisal) %>.checked;
        f_up_inner_line = 8;

        
        $j("#ufmipPanel").toggle(!isVaIRRRL);
        f_up_inner_line = 9;
        $j('#sSpLienPanel').toggle ( isVaIRRRL || isFhaStreamline );
        f_up_inner_line = 10;
        $j('#sFHASalesConcessionsPanel').toggle( isFhaStreamline );
        f_up_inner_line = 11;
        $j('#sHasAppraisalPanel').toggle( isFhaStreamline );
        f_up_inner_line = 12;
        $j('#sOriginalAppraisedValuePanel').toggle (isFhaStreamline && !bHasAppraisal );
        f_up_inner_line = 13;
        $j('#sHouseValPePanel').toggle ( ( isFhaStreamline && bHasAppraisal ) || !isFhaStreamline );
        f_up_inner_line = 14;
        
        if (isFhaStreamline && bHasAppraisal )
        {
            f_up_inner_line = 16;
            <%= AspxTools.JQuery(sHouseValPeLabel) %>.text( "Appraised Value");
            <%= AspxTools.JQuery(sHouseValPeRequireValidator) %>.toggle(true);
            f_up_inner_line = 17;
        }
        f_up_inner_line = -16;
        if (isVaIRRRL){
            f_up_inner_line = 18;
            <% if (m_isAsk3rdPartyUwResultInPml) { %>
                var vaBox = <%= AspxTools.JsGetElementById(sProdIncludeVAProc) %>;
                f_up_inner_line = 19;
                if (typeof vaBox != 'undefined' && vaBox != null){
                    f_up_inner_line = 20;
                    if (!vaBox.checked){
                        f_up_inner_line = 21;
                        vaBox.checked = true;
                        f_up_inner_line = 22;
                        f_onAusProcessingClick();
                        f_up_inner_line = 23;
                    }
                }
            <% }%>
        }
        f_up_inner_line = -18;
        
        if (v1) v1.enabled = ( isVaIRRRL || isFhaStreamline );
        f_up_inner_line = 24;
        if (v2) v2.enabled = !isVaIRRRL;
        f_up_inner_line = 25;
        if (v3) v3.enabled = isFhaStreamline;
        f_up_inner_line = 26;
        if (v4) v4.enabled = ( ( isFhaStreamline && bHasAppraisal ) || !isFhaStreamline );
        f_up_inner_line = 27;
        
        f_onValidateRequiredFields();
        f_up_inner_line = 28;
        <% } else { %>
        if (v1) v1.enabled = false;
        f_up_inner_line = 29;
        if (v2) v2.enabled = false;
        f_up_inner_line = 30;
        if (v3) v3.enabled = false;
        f_up_inner_line = 31;
        if (v4) v4.enabled = false;
        f_up_inner_line = 32;
        
        <% }%>
        refreshCalculation();
        f_up_inner_line = 33;
    }catch(e){
      logJSException(e, 'f_up_inner_line = ' + f_up_inner_line);
    }
}

function f_validatesLPurposeTPe( source, arguments )
{
  var nonSupportList = <%= AspxTools.JsArray(E_sLPurposeT.Other)  %>;
  
  var isNonSupportType = $j.inArray(arguments.Value, nonSupportList) >= 0;
  
  arguments.IsValid = isNonSupportType == false;
  $j('#PurposeMessage').html(isNonSupportType ? 'Not supported by the pricing engine' : '').toggle(isNonSupportType);
}

function f_validatesProdMIOptionT( source, arguments )
{
  arguments.IsValid = true; return;  <%--// OPM 109393 Everyone allowed to proceed with nomi. --%>

  var mIDdl = document.getElementById(source.controltovalidate);
  
  arguments.IsValid = ( mIDdl.disabled == true || mIDdl.value != <%= AspxTools.JsString(E_sProdMIOptionT.LeaveBlank.ToString("D")) %> );
}

function f_validatesProdConvMIOptionT( source, arguments )
{
  var mIDdl = document.getElementById(source.controltovalidate);
  
  arguments.IsValid = ( mIDdl.disabled == true || mIDdl.value != <%= AspxTools.JsString(E_sProdConvMIOptionT.Blank.ToString("D")) %> );
}

<% if ( m_isAutoPmiOrNewPml ) { %>
function f_showSplitRow()
{
    var convMi = <%= AspxTools.JsGetElementById(sProdConvMIOptionT) %>;
    var splitRow = document.getElementById('SplitPmiRow');
    if ( convMi != null )
    {
        var isSplit = convMi.value == <%= AspxTools.JsString(E_sProdConvMIOptionT.BorrPaidSplitPrem.ToString("D")) %>;
        
        splitRow.style.display = isSplit ? 'inline' : 'none';
        
        if ( isSplit == false )
        {
            var splitMi = document.getElementById('<%= AspxTools.ClientId(sConvSplitMIRT) %>');
            if (splitMi)
                splitMi.selectedIndex = 0;
        }
    }
    
    f_onValidateRequiredFields();
}

function f_validatesConvSplitMIRT( source, arguments )
{
    var splitRow = document.getElementById('SplitPmiRow');
    if ( splitRow && splitRow.style.display == 'none' )
    {
        arguments.IsValid = true;
        return;
    }

    var mIDdl = document.getElementById(source.controltovalidate);  
    arguments.IsValid = mIDdl.value != <%= AspxTools.JsString(E_sConvSplitMIRT.Blank.ToString("D")) %> ;
}
<%} %>


function f_setPurposeTPeUpdate(arg){
  try{
    var Purpid = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>.id;
    if (!arg || arg !== Purpid) return "False";
    else return "True";
  }catch(e){}
  return "False";
}

function refreshCalculation() { 
  var args = getAllFormValues(<%= AspxTools.JsGetClientIdString(this) %>);
  args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
  args["sProdCalcEntryT"] = $j('#sProdCalcEntryT').val();  
  args["sLPurposeTPeUpdate"] = f_setPurposeTPeUpdate(arguments[0]);
  
  <% if (m_renderAsSecond ) { %> args["IsStandAloneSecond"] = "True"; <% } %>
  
  var result = gService.main.call("LoanInfo_CalculateData", args);
  if (!result.error) {
    populateForm(result.value, <%= AspxTools.JsGetClientIdString(this) %>);
    f_setsProdRLckdExpiredDLabel( result.value["sProdRLckdExpiredDLabel"]);
    
    <% if (m_isAutoPmiOrNewPml) { %>
    f_setsProdConvMIOptionT( result.value["sLtv80TestResultT"], false );
    f_showSplitRow();
    <% } else { %>
    f_setsProdMIOptionT( result.value["sLtv80TestResultT"], false );
    <% } %>
    f_setCalcOldValues();
  }
  else
    f_resetVal( event.srcElement ); <%-- // 05/04/07 mf. OPM 12557 --%>
  
}

function f_onchange_sHouseValPe() {
  var SalePrice = <%= AspxTools.JQuery(sHouseValPe) %>;
  var spErrMsg = document.getElementById("negHouseVal");
  
  if (SalePrice.val() == "" || SalePrice.val() == "0")
    SalePrice.val("$0.00");
  
  if (SalePrice.val().match("-")) 
    $j('#negHouseVal').show();// spErrMsg.style.display = "inline";
  else
    $j('#negHouseVal').hide(); 
    //spErrMsg.style.display = "none";
   
  $j("#sProdCalcEntryT").val(SalePrice.data('sProdCalcEntryT'));
  
  f_onValidateRequiredFields();
  f_onchange_sProdCalcEntryFields(SalePrice);
}
function f_onchange_sPrimAppTotNonspIPe() {
  var tb = <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPe) %>;
  
  if (tb.value == "" || tb.value == "0") {
    tb.value = "$0.00";
  }
  f_onValidateRequiredFields();  
  refreshCalculation();
}

function f_validateNumFinancedProperties(source, args) {
  var tb = <%= AspxTools.JsGetElementById(sNumFinancedProperties) %>;

  var intRegex = /^\d+$/;
  if (tb.value == "" || tb.value < 1 || !intRegex.test(tb.value)) {
    args.IsValid = false;
    return;
  }
}

function f_onchange_sNumFinancedProperties() {
  var tb = <%= AspxTools.JsGetElementById(sNumFinancedProperties) %>;
  
  if (tb.value != '' && tb.value < 1) {
    alert(<%= AspxTools.JsString(JsMessages.NumFinancedPropertiesMustBePositive) %>);
  }
  
  f_onValidateRequiredFields();
}

function f_onchange_sProdCashoutAmt() {
    var tb = <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>;
  
    if (tb.value == "" || tb.value == "0") {
        tb.value = "$0.00";
    }
    if (tb.value.indexOf("-") >= 0 || tb.value.indexOf("(") >= 0) {
        alert(<%= AspxTools.JsString(JsMessages.LPE_CashoutCannotBeNegative) %>);
        tb.value = "$0.00";
    }

    <%-- // 09/19/06 mf OPM 7436 Our money box control reformats in the
         // onblur event, so we have to format early here in onchange
         // so we can validate the final value.  --%>
    if ( typeof(format_money) != 'undefined' )
        format_money(tb);
    f_updateTexasCB();
    f_onValidateRequiredFields();
}

function f_onchange_sIsRenovationLoan(e) {
    var $cb = <%= AspxTools.JQuery(sIsRenovationLoan) %>;
    
    // Show the total renovation costs
    var $renovationCostsPanel = $j('#sTotalRenovationCostsPanel');
    var renovationValidator = <%= AspxTools.JsGetElementById(sTotalRenovationCostsValidator) %>;
    if ($cb.prop('checked')) {
        $renovationCostsPanel.show();
        renovationValidator.enabled = true;
    } else {
        $renovationCostsPanel.hide();
        renovationValidator.enabled = false;
    }

    // Show the as-completed value
    <% if (!m_renderAsSecond ) { %>
    var sLPurposeTPe = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
    var isPurchase = sLPurposeTPe.value == <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %>;
    var isRateTermRefi = sLPurposeTPe.value == <%= AspxTools.JsString(E_sLPurposeT.Refin.ToString("D")) %>;
    <% } else { %>
    var isRateTermRefi = true;
    var isPurchase = false;
    <% } %>
    
    var sAsIsAppraisedValuePevalNegativeValidator = document.getElementById('<%= AspxTools.ClientId(sAsIsAppraisedValuePevalNegativeValidator) %>');
    
    var $sAsIsAppraisedValuePevalPanel = $j('#sAsIsAppraisedValuePevalPanel');
    if ($cb.prop('checked') && (isPurchase || isRateTermRefi)) { 
        $sAsIsAppraisedValuePevalPanel.show();   
        ValidatorEnable(sAsIsAppraisedValuePevalNegativeValidator, true); 
    } else {
        $sAsIsAppraisedValuePevalPanel.hide();
        ValidatorEnable(sAsIsAppraisedValuePevalNegativeValidator, false); 
       
    }

    f_onchange_sLPurposeTPe(false);
    f_onchange_sProdCalcEntryFields(e);
    f_onValidateRequiredFields();
}

function f_onchange_sTotalRenovationCosts(e) {
    f_onchange_sProdCalcEntryFields(e);
    f_onValidateRequiredFields();
}

function f_onchange_sAsCompletedValuePeval(e) {
    f_onchange_sProdCalcEntryFields(e)
    f_onValidateRequiredFields();
}

function f_onchange_sProdCalcEntryFields(obj) {
    var $obj = $j(obj);
    var l1Amt = <%= AspxTools.JQuery(sLAmtCalcPe) %>;
    var l1Pct = <%= AspxTools.JQuery(sLtvRPe) %>;
    var l2Amt = <%= AspxTools.JQuery(sProOFinBalPe) %>
    var l2Pct = <%= AspxTools.JQuery(sLtvROtherFinPe) %>;
    var DwnPmtPct = <%= AspxTools.JQuery(sDownPmtPcPe) %>;
    var DwnPmtAmt = <%= AspxTools.JQuery(sEquityPe) %>;
    var DwnPmtPctInt = 0;

    <% if (!m_renderAsSecond ) { %>
    var l1ErrMsg = document.getElementById("neg1stLien");
    var l2ErrMsg = document.getElementById("neg2ndLien");
    <% } else { %>
    var l1ErrMsg = document.getElementById("neg2ndLien");
    var l2ErrMsg = document.getElementById("neg1stLien");
    <% } %>
    var No2ndFinancing = <%= AspxTools.JsGetElementById(bNo2ndFinancing) %>;
    var SalePrice = <%= AspxTools.JQuery(sHouseValPe) %>;
    var spErrMsg = document.getElementById("negHouseVal");


      
    if (SalePrice.val().match("-")) 
        spErrMsg.style.display = "inline";
    else
        spErrMsg.style.display = "none";

    if ($obj.length != 0) {
        <%-- //Show error messages if negative values are entered --%>
        if ( ($obj == l1Amt && l1Amt.val().match("-")) || ($obj == l1Pct && l1Pct.val().match("-")) )
            l1ErrMsg.style.display = "inline-block";
        else l1ErrMsg.style.display = "none";
      
        if ( ($obj == l2Amt && l2Amt.val().match("-")) || ($obj == l2Pct && l2Pct.val().match("-")) )
            l2ErrMsg.style.display = "inline-block";
        else l2ErrMsg.style.display = "none";
      
        <%-- //Highlight 2nd Financing if 1st Lien or Down payment modified 3 times --%>
        if  ($obj == l1Amt || $obj == l1Pct || $obj == DwnPmtAmt ||  $obj == DwnPmtPct) {
            if( typeof(mod2ndCounter) == 'undefined')
                mod2ndCounter = 1;
            else mod2ndCounter++;

            if (mod2ndCounter >= 2 && No2ndFinancing.checked)
		        f_hihghlight2ndFinancing(true);
        }
    }

    var _sProdCalcEntryT = $obj.data('sProdCalcEntryT');
    if (_sProdCalcEntryT != null)
        $j("#sProdCalcEntryT").val(_sProdCalcEntryT);
    refreshCalculation();

    DwnPmtPctInt = parseFloat(DwnPmtPct.val().replace('%',''));
    if ($obj.length != 0)
    {
        <%-- //Show error message if down payment is bigger than sales price --%>
        if ( ($obj == DwnPmtPct || $obj == DwnPmtAmt || $obj == SalePrice) && !isNaN(DwnPmtPctInt) && DwnPmtPctInt > 100)
            $j('#errDownPmt').show();//.style.display = "inline-block";
        else
            $j('#errDownPmt').hide();//.style.display = "none";
    }

    f_onValidateRequiredFields();
}

function f_setsProdMIOptionT( ltv80TestResult , bInit )
{
  if (! <%= AspxTools.JsBool(IsReadOnly) %>)
  {
    var ddl = <%= AspxTools.JsGetElementById(sProdMIOptionT) %>;
    if ( ddl != null )
    {
      if ( ltv80TestResult != <%= AspxTools.JsNumeric((int) E_sLtv80TestResultT.Over80) %> )
      {
        ddl.selectedIndex = 0;
        ddl.style.backgroundColor = gReadonlyBackgroundColor;
        ddl.disabled = true;
      }
      else
      {
        ddl.style.backgroundColor = "";
        ddl.disabled = false;
      }
    }
    f_onValidateRequiredFields();
  }
}

function f_setsProdConvMIOptionT( ltv80TestResult , bInit )
{
  if (! <%= AspxTools.JsBool(IsReadOnly) %>)
  {
    var ddl = <%= AspxTools.JsGetElementById(sProdConvMIOptionT) %>;
    if ( ddl != null )
    {
      if ( ltv80TestResult != <%= AspxTools.JsNumeric((int) E_sLtv80TestResultT.Over80) %> )
      {
        ddl.selectedIndex = ddl.length -1;
        ddl.style.backgroundColor = gReadonlyBackgroundColor;
        ddl.disabled = true;
      }
      else
      {
          var $ddl = $(ddl);
          if ($ddl.prop('disabled')) {
              $ddl.val(<%= AspxTools.JsString(E_sProdConvMIOptionT.Blank.ToString("D")) %>);
          }

          ddl.style.backgroundColor = "";
        ddl.disabled = false;
      }
    }
    f_onValidateRequiredFields();
  }
}


function f_setsProdRLckdExpiredDLabel(result)
{
    <%= AspxTools.JQuery(sProdRLckdExpiredDLabel)%>.html(result);
}
<%-- 
  // 1/16/07 mf. We use the logic of OPM 7997 to determine how to
  // render the FTHB and Housing History controls.
  --%>
function f_setFthb()
{
  var bIsCreditOnFile = <%= AspxTools.JsGetElementById(m_trades) %>.value == "0" || <%= AspxTools.JsGetElementById(m_trades) %>.value == "1";
  
  var sLPurposeTPe = <%= AspxTools.JQuery(sLPurposeTPe) %>.val();
  var refiList = <%= AspxTools.JsArray(E_sLPurposeT.RefinCashout,E_sLPurposeT.Refin,E_sLPurposeT.FhaStreamlinedRefinance,E_sLPurposeT.VaIrrrl)   %>;
  
  var bIsLoanPurposeRefi = $j.inArray(sLPurposeTPe, refiList) >= 0;
  
  var bHasTradeInLast12 = <%= AspxTools.JQuery(m_trades12) %>.val() == "1";
  var bHasTradeEver = <%= AspxTools.JQuery(m_trades) %>.val() == "1";
  var sHas1stTimeBuyerPe = <%= AspxTools.JsGetElementById(sHas1stTimeBuyerPe) %>;
  var sProdHasHousingHistory = <%= AspxTools.JsGetElementById(sProdHasHousingHistory) %>;

  if (<%= AspxTools.JsBool(IsReadOnly) %>)
  {
    f_setHousingHistoryDisplay( <%= AspxTools.JsGetElementById(sHas1stTimeBuyerPe) %>.checked );
  }
  else
  {
    if ( bIsLoanPurposeRefi )
    {
    <%-- //alert("0. Loan is refi -> false & disabled"); --%>
      sHas1stTimeBuyerPe.checked = false;
    }
    else if ( ! bIsCreditOnFile )
    {
    <%-- //alert("1. Loan is not Refi, Credit not on file -> user selection"); --%>
    }
    else if ( bHasTradeInLast12 )
    {
    <%-- //alert("2. Loan is not Refi, Credit on file, has a mort trade in last 12 -> user selection");--%>
    }
    else if ( bHasTradeEver )
    {
    <%-- //alert("3. Loan is not refi, Credit on file, no mort trades in last 12, but has one in history -> user selection"); --%>
    }
    else
    {
    <%-- //alert("4. Loan is not refi, Credit on file, has no mort trades in last 12, has no trades ever -> true and disabled"); --%>
      sHas1stTimeBuyerPe.checked = true; 
    }

    f_setHousingHistoryDisplay( sHas1stTimeBuyerPe.checked );
    
  }
}
function f_setHousingHistoryDisplay( bDisplay )
{
  <%-- // Per OPM 7997, when FTHB is checked, we want to allow user selection of Housing History,
       // otherwise, it is checked and hidden. --%>
  var sProdHasHousingHistory = <%= AspxTools.JsGetElementById(sProdHasHousingHistory) %>;
  var historyCheckBox = document.getElementById('historyCheckBox');
 
  if (!<%= AspxTools.JsBool(IsReadOnly) %>)
  {
    if ( ! bDisplay )
      sProdHasHousingHistory.checked = true;
    else if ( historyCheckBox.style.display == 'none' )
      sProdHasHousingHistory.checked = false;
  }
  historyCheckBox.style.display = bDisplay ? 'inline' : 'none';
}
function f_sSpStatePe_onchange() {
  var sSpStatePe = <%= AspxTools.JsGetElementById(sSpStatePe) %>.value;
  if (sSpStatePe != "") {
    var args = new Object();
    args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
    args["sSpStatePe"] = sSpStatePe;
    var result = gService.main.call("LoanInfo_IsSpStateAllowPp", args);
    if (!result.error) {
      var IsSpStateAllowPp_1st = result.value["IsSpStateAllowPp_1st"] == "True";
      var IsSpStateAllowPp_2nd = result.value["IsSpStateAllowPp_2nd"] == "True";
      
      UpdateCounties( <%= AspxTools.JsGetElementById(sSpStatePe) %>, document.getElementById("sSpCounty"));
         
      <%= AspxTools.JsGetElementById(sSpStatePe) %>.oldValue = <%= AspxTools.JsGetElementById(sSpStatePe) %>.selectedIndex;
    }
    else 
    {
      f_resetVal( event.srcElement ); <%-- // 05/04/07 mf. OPM 12557 --%>

    }
  }
  else 
  {
		document.getElementById("sSpCounty").options.length = 0; 
		document.getElementById("sSpCounty").oldValue = -1; 
		<%= AspxTools.JsGetElementById(sSpStatePe) %>.oldValue = <%= AspxTools.JsGetElementById(sSpStatePe) %>.selectedIndex;
  }
  f_updateTexasCB();
  f_onValidateRequiredFields();
}
<%-- // 06/06/07 mf. OPM 15866.  No-Asset Doc types should have no reserve value set. --%>
function f_validateReserve(source, args) {
  var o = document.getElementById(source.controltovalidate);
  if (o != null && o.value == "-1")
  {
    args.IsValid = isNoAssetDocType();
  }
  else
    args.IsValid = true;
    
  return;
}
function f_validateLckdDays(source, args) {
  var o = <%= AspxTools.JsGetElementById(sProdRLckdDays) %>;
  if (null != o) {
    if (o.value == "")
      args.IsValid = false;
    else {
      try {
        var _v = parseInt(o.value);
        args.IsValid = _v > 0;
      } catch (e) {
        args.IsValid = false;
      }
    }
  }
}
function f_validatesProdFilterDue(source, args)
{
  var list = [
    <%= AspxTools.JsGetElementById(sProdFilterDue10Yrs) %>, 
    <%= AspxTools.JsGetElementById(sProdFilterDue15Yrs) %>,
    <%= AspxTools.JsGetElementById(sProdFilterDue20Yrs) %>,
    <%= AspxTools.JsGetElementById(sProdFilterDue25Yrs) %>,
    <%= AspxTools.JsGetElementById(sProdFilterDue30Yrs) %>,  
    <%= AspxTools.JsGetElementById(sProdFilterDueOther) %>
    ];
    
  args.IsValid = false;
  for (var i = 0, length = list.length; i < length; i++)
  {
    if (list[i].checked == true)
    {
    args.IsValid = true;
    break;
    }
  }
  return;
}
function f_validatesProdFilterFinMeth(source, args)
{
  var list = [<%= AspxTools.JsGetElementById(sProdFilterFinMethFixed) %>,
  <%= AspxTools.JsGetElementById(sProdFilterFinMeth3YrsArm) %>,
  <%= AspxTools.JsGetElementById(sProdFilterFinMeth5YrsArm) %>,
  <%= AspxTools.JsGetElementById(sProdFilterFinMeth7YrsArm) %>,
  <%= AspxTools.JsGetElementById(sProdFilterFinMeth10YrsArm) %>,
  <%= AspxTools.JsGetElementById(sProdFilterFinMethOther) %>];

  args.IsValid = false;
  for (var i = 0, length = list.length; i < length; i++)
  {
    if (list[i].checked == true)
    {
    args.IsValid = true;
    break;
    }
  }
  return;
  
}

function f_validatesProdFilterPaymentType(source, args)
{
  var list = [<%= AspxTools.JsGetElementById(sProdFilterPmtTPI) %>,
  <%= AspxTools.JsGetElementById(sProdFilterPmtTIOnly) %>];

  args.IsValid = false;
  for (var i = 0, length = list.length; i < length; i++)
  {
    if (list[i].checked == true)
    {
        args.IsValid = true;
        break;
    }
  }
  return;
  
}

function f_onchange_sProdSpT(ddl) {
  <%-- // 2-4 units, Condo, Townhouse, default selection to Attach --%>
  var v = <%= AspxTools.JQuery(sProdSpT) %>.val();
  var list = <%= AspxTools.JsArray(E_sProdSpT.TwoUnits, 
                         E_sProdSpT.ThreeUnits, 
                         E_sProdSpT.FourUnits, 
                         E_sProdSpT.Condo, 
                         E_sProdSpT.Townhouse) %>;
                
  if ($j.inArray(v, list) >= 0)
    {
    <%= AspxTools.JQuery(sProdSpStructureT) %>.val(<%= AspxTools.JsString(E_sProdSpStructureT.Attached) %>); 
  }                       
}

function f_validatesLAmtCalcPe( source, args )
{
  var bIsStandaloneSecond = <%= AspxTools.JsBool(m_renderAsSecond) %>;
  var bIs2ndFinancing = <%= AspxTools.JsGetElementById(bYes2ndFinancing) %>.checked;
  var bIsHeloc = <%= AspxTools.JsGetElementById(bHeloc2ndFinancing) %>.checked;
  var bBrokerAllowHelocZeroInitialDraw = <%= AspxTools.JsBool(CurrentBroker.AllowHelocZeroInitialDraw_141171) %>;
  var bAllowZeroLoanAmount = bIsStandaloneSecond && bIs2ndFinancing && bIsHeloc && bBrokerAllowHelocZeroInitialDraw;
  var minimumLoanAmount = bAllowZeroLoanAmount ? 0 : 1;
  var sLAmtCalcPe = <%= AspxTools.JQuery(sLAmtCalcPe) %>.val();
  args.IsValid = ( parseInt(sLAmtCalcPe.replace(/\$/,'').replace(/,/,'') ) >= minimumLoanAmount );
}
function f_update_PriorSalesPanel() 
{
    var masterPanel = document.getElementById('masterPriorSalesPanel'); 
    var sPriorSalesPriceValidator = document.getElementById('<%= AspxTools.ClientId(sPriorSalesPriceValidator)%>');
    var sPriorSalesSellerValidator = document.getElementById('<%= AspxTools.ClientId(sPriorSalesSellerValidator)%>');
    var sPriorSalesDValidator = document.getElementById('<%= AspxTools.ClientId(sPriorSalesDValidator)%>');
    var sPriorSalesD = document.getElementById('<%= AspxTools.ClientId(sPriorSalesD) %>').value;
    var priorsalespanelExtraInfo = document.getElementById('priorsalespanelExtraInfo');
   
    var purposeType = document.getElementById('<%= AspxTools.ClientId(sLPurposeTPe) %>');
    var isPurchase = purposeType.value == <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %>;  
    
    if( !isPurchase ) 
    {
        masterPanel.style.display =    'none'; 
        sPriorSalesPriceValidator.enabled = false;
        sPriorSalesSellerValidator.enabled = false; 
        sPriorSalesDValidator.enabled = false;
        return;
    }
    sPriorSalesDValidator.enabled = true; 
    
    if( sPriorSalesD.length === 0 || sPriorSalesD === 'mm/dd/yyyy'  ) 
    {
       priorsalespanelExtraInfo.style.display = 'none';
       sPriorSalesPriceValidator.enabled = false;
       sPriorSalesSellerValidator.enabled = false;
    }
    else 
    {
       priorsalespanelExtraInfo.style.display = '';
       sPriorSalesPriceValidator.enabled = true;
       sPriorSalesSellerValidator.enabled = true;
    }
}

function f_validatesPriorSalesPrice(val, args) 
{
    var sPriorSalesPrice = <%= AspxTools.JsGetElementById(sPriorSalesPrice) %>.value;
    var price = getMoneyValue( sPriorSalesPrice );
    args.IsValid = !(!price || price < 0);
}

function f_validatesPriorSalesD( val, args)
{
    var sPriorSalesD = <%= AspxTools.JsGetElementById(sPriorSalesD) %>.value;
    
    if( sPriorSalesD === '' || sPriorSalesD === 'mm/dd/yyyy') 
    {
        args.IsValid = true;
        return;
    }
    
    var parts = sPriorSalesD.match(/(\d+)/g);
    if( !parts || parts.length != 3 ) {
        args.IsValid = false; 
        return;
    }
    var months  = parseInt( parts[0] );
    var days    = parseInt( parts[1] ); 
    var year    = parseInt( parts[2] ); 
    
    if( isNaN(months) || isNaN(days) || isNaN(year) )
    {
        args.IsValid = false;
        return;
    }
    var currentDate = new Date(); 
    if( months < 0 || months > 12 || days < 0 || days > 31 || year < 1900 || year > 2050 ) 
    {
        args.IsValid = false;
        return;
    }
    var d = new Date(year, months -1, days); <%-- // months are 0-based --%>
    if( isNaN(d.getTime()) ) 
    {
        args.IsValid = false;
        return;
    }
    
    if( d > currentDate ) {
        args.IsValid = false;
        return;
    }
    args.IsValid = true;
    
}


<%-- 
  // 05/04/07 mf. OPM 12557. Store oldValue attribute for the values that can
  // produce a background calculation when changed.  Remember to add any new
  // field whose onchange event causes background calculation to this list.
--%>
function f_setCalcOldValues()
{
  <%= AspxTools.JsGetElementById(sHouseValPe) %>.oldValue = <%= AspxTools.JsGetElementById(sHouseValPe) %>.value;
  <%= AspxTools.JsGetElementById(sDownPmtPcPe) %>.oldValue = <%= AspxTools.JsGetElementById(sDownPmtPcPe) %>.value;
  <%= AspxTools.JsGetElementById(sEquityPe) %>.oldValue = <%= AspxTools.JsGetElementById(sEquityPe) %>.value;
  <%= AspxTools.JsGetElementById(sLtvRPe) %>.oldValue = <%= AspxTools.JsGetElementById(sLtvRPe) %>.value;
  <%= AspxTools.JsGetElementById(sLAmtCalcPe) %>.oldValue = <%= AspxTools.JsGetElementById(sLAmtCalcPe) %>.value;
  <%= AspxTools.JsGetElementById(sProOFinBalPe) %>.oldValue = <%= AspxTools.JsGetElementById(sProOFinBalPe) %>.value;
  <%= AspxTools.JsGetElementById(sLtvROtherFinPe) %>.oldValue = <%= AspxTools.JsGetElementById(sLtvROtherFinPe) %>.value;
  <%= AspxTools.JsGetElementById(sCltvRPe) %>.oldValue = <%= AspxTools.JsGetElementById(sCltvRPe) %>.value;
  <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPe) %>.oldValue = <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPe) %>.value;
  <%= AspxTools.JsGetElementById(sSpStatePe) %>.oldValue = <%= AspxTools.JsGetElementById(sSpStatePe) %>.selectedIndex;
  <%= AspxTools.JsGetElementById(sApprValPe) %>.oldValue = <%= AspxTools.JsGetElementById(sApprValPe) %>.value;
  
  <% if (!m_renderAsSecond) { %>
    <%= AspxTools.JsGetElementById(sLPurposeTPe) %>.oldValue = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>.selectedIndex;
  <% } else { %>
    <%= AspxTools.JsGetElementById(sProOFinPmtPe) %>.oldValue = <%= AspxTools.JsGetElementById(sProOFinPmtPe) %>.value;
  <% } %>
}
<%-- 
  // 05/04/07 mf. OPM 12557. Return control to state before change that
  // called failed background calculation.
--%>
function f_resetVal( srcEl )
{
  if (srcEl && srcEl.oldValue != null)
  {
    switch(srcEl.tagName)
    {
      case "INPUT":
        srcEl.value = srcEl.oldValue;
        break;
      case "SELECT":
        srcEl.selectedIndex = srcEl.oldValue;
        break;
    }
  }
}
<%-- 
// 06/18/07 mf. OPM 16288.  We want to hide the appropriate fields when the
// doc type is set as no income or no asset.
--%>
function f_onchange_sProdDocT()
{
  var bNoIncomeDocType = !isIncomeRequired();
  var bNoAssetDocType = isNoAssetDocType();
  
  visible(document.getElementById("IncomeSpan"),!bNoIncomeDocType);
  visible(document.getElementById("AssetSpan"),!bNoAssetDocType);
  
  f_updateCreditQualifyingCB();
  f_onValidateRequiredFields();
}

function isNoAssetDocType(){
    var sProdDocT = <%= AspxTools.JsGetElementById(sProdDocT) %>.value;
    var bNoAssetDocType = false;
    try{
        bNoAssetDocType  = (sProdDocT == <%= AspxTools.JsString(E_sProdDocT.NINANE.ToString("D")) %> ||
                            sProdDocT == <%= AspxTools.JsString(E_sProdDocT.NINA.ToString("D")) %> ||
                            sProdDocT == <%= AspxTools.JsString(E_sProdDocT.VINA.ToString("D")) %>);
    }catch(e){}
    return bNoAssetDocType;
}

<%-- //opm 11948 08/05/08 fs --%>
function f_onchange_sProdRLckdDays()
{
    var bValid = true;
    if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
        bValid = Page_ClientValidate();
    f_refreshsProdRLckdDays();
    if( <%= AspxTools.JsBool( m_usingNewEngine) %> ) {
        f_onValidateRequiredFields();
    }
}

function f_refreshsProdRLckdDays()
{
  var args = getAllFormValues(<%= AspxTools.JsGetClientIdString(this) %>);
  args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
  args["sProdCalcEntryT"] = $j("#sProdCalcEntryT").val();
  
  <% if (m_renderAsSecond ) { %> args["IsStandAloneSecond"] = "True"; <% } %>
  
  var result = gService.main.call("LoanInfo_refreshsProdRLckdDays", args);
  if (!result.error) {
    populateForm(result.value, <%= AspxTools.JsGetClientIdString(this) %>);
    f_setsProdRLckdExpiredDLabel( result.value["sProdRLckdExpiredDLabel"]);
  //  f_setsProdMIOptionT( result.value["sLtv80TestResultT"], false );
    f_setCalcOldValues();
  }
  else
    f_resetVal( event.srcElement );
  
}

g_disableCredQualValidators = false;

function f_updateCreditQualifyingCB(){
    try{
        var LoPurpose = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
        var CredQual = <%= AspxTools.JsGetElementById(sIsCreditQualifying) %>;
        var sTotIncome = <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPe) %>;
        var sResidIncome = <%= AspxTools.JsGetElementById(sProdEstimatedResidualI) %>;
        var sAvailReserves = <%= AspxTools.JsGetElementById(sProdAvailReserveMonths) %>;
        var CreditQualifyingPanel           = document.getElementById("CreditQualifyingPanel");
        var TotalIncomePanel                = document.getElementById("TotalIncomePanel");              
        var ReservesPanel                   = document.getElementById("AssetSpan");                     
        var EstimatedResidualIncomePanel    = document.getElementById("EstimatedResidualIncomePanel");  
        
        if (typeof LoPurpose === 'undefined' || LoPurpose == null) return;
        if (typeof CredQual === 'undefined' || CredQual == null) return;
        
        var isFHARefinance = LoPurpose.value ==  <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance.ToString("D")) %>;
        var isVaIrrrl = LoPurpose.value ==  <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>;
        var isCQType = isFHARefinance || isVaIrrrl;
        
        visible(CreditQualifyingPanel, isCQType);          
        
        if (isCQType && !CredQual.checked){
           
                if ((gFhaStreamCredQual && isFHARefinance) || isVaIrrrl) {
                    visible(TotalIncomePanel, false);
                    visible(ReservesPanel, false);
                    visible(EstimatedResidualIncomePanel, false);
                    var v1 = <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPeValidator) %>;
                    var v2 = <%= AspxTools.JsGetElementById(sProdEstimatedResidualIValidator) %>;
                    var v3 = <%= AspxTools.JsGetElementById(sProdAvailReserveMonthsValidator) %>;
                    
                    if (v1) v1.enabled = false;
                    if (v2) v2.enabled = false;
                    if (v3) v3.enabled = false;
                    g_disableCredQualValidators = true;
                }
                else if( gFhaStreamCredQual == false && isFHARefinance) {
                    visible(TotalIncomePanel, true);
                    visible(ReservesPanel, true);
                    visible(EstimatedResidualIncomePanel, true);
                    
                    var v1 = <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPeValidator) %>;
                    var v2 = <%= AspxTools.JsGetElementById(sProdEstimatedResidualIValidator) %>;
                    var v3 = <%= AspxTools.JsGetElementById(sProdAvailReserveMonthsValidator) %>;
                    
                    if (v1) v1.enabled = true;
                    if (v2) v2.enabled = true;
                    if (v3) v3.enabled = true;
                    g_disableCredQualValidators = true;
                }
        }
        else{ <%-- No FHA streamline refi / VA IRRRL or CQ checked--%>
            var bRequiredIncome = isIncomeRequired(); 
            visible(TotalIncomePanel, true);
            <%= AspxTools.JsGetElementById(sPrimAppTotNonspIPeValidator) %>.enabled = bRequiredIncome && !<%= AspxTools.JsBool(IsReadOnly) %>;
            visible(EstimatedResidualIncomePanel, <%= AspxTools.JsBool(m_isShowsProdEstimatedResidualI)%>);
            <% if (m_isShowsProdEstimatedResidualI) { %>
            <%= AspxTools.JsGetElementById(sProdEstimatedResidualIValidator) %>.enabled = bRequiredIncome && !<%= AspxTools.JsBool(IsReadOnly) %>;
            <% } %>
            if (!isCQType && CredQual.checked){
                CredQual.checked = false;
            }
            if (gFhaStreamCredQual || isVaIrrrl) {
                visible(ReservesPanel,!isNoAssetDocType());
                
                g_disableCredQualValidators = false;
                    
                
            
                
                var v1 = <%= AspxTools.JsGetElementById(sProdAvailReserveMonthsValidator) %>;
                if (v1) v1.enabled = true;
            }
        }
        f_onValidateRequiredFields();
    }catch(e){ }
}

function f_onchanche_sIsCredQual(){
    f_updateCreditQualifyingCB();
}

function f_updateTexasCB(){
    var texasCb = <%= AspxTools.JsGetElementById(sProdIsTexas50a6Loan) %>;
    var $texasPanel = $j(".TexasPanel");
    var loanPurpose = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
    var sSpStatePe = <%= AspxTools.JsGetElementById(sSpStatePe) %>;
    var sOccTPe = <%= AspxTools.JsGetElementById(sOccTPe) %>;
    var sProdCashoutAmt = <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>;
    
    if ($texasPanel.length == 0 || typeof texasCb == undefined || texasCb == null){
        return;
    }
    
    try{<% if (!m_renderAsSecond) { %>
        var isVisible = (loanPurpose.value != <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %> && sSpStatePe.value == "TX");
        <% if (!IsReadOnly) { %>var isLocked = (loanPurpose.value == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %> && sOccTPe.value == <%= AspxTools.JsString(E_sOccT.PrimaryResidence.ToString("D")) %>);<% } %>
        <% } else { %>
        var isVisible = sSpStatePe.value == "TX";
        <% if (!IsReadOnly) { %>var isLocked = (sOccTPe.value == <%= AspxTools.JsString(E_sOccT.PrimaryResidence.ToString("D")) %> && sProdCashoutAmt.value != "$0.00");<% } %>
        <% } %>
        <% if (!IsReadOnly) { %>
        if (isVisible){
                if (isLocked){
                    texasCb.checked = true;
                }

                $texasPanel.show();
        }
        else{
            $texasPanel.hide();
            texasCb.checked = false;
        }
        <% } else { %> $texasPanel.toggle(isVisible); <% } %>
    }catch(e){}
}

function setAUSOptionsVisibility() {
    var ltrViewDoFindings = <%= AspxTools.JsGetElementById(ltrViewDoFindings) %>;
    var lnkViewDoFindings = <%= AspxTools.JsGetElementById(lnkViewDoFindings) %>;
    var ltrViewDuFindings = <%= AspxTools.JsGetElementById(ltrViewDuFindings) %>;
    var lnkViewDuFindings = <%= AspxTools.JsGetElementById(lnkViewDuFindings) %>;
    var ltrViewLp = <%= AspxTools.JsGetElementById(ltrViewLp) %>;
    var btnViewLp = <%= AspxTools.JsGetElementById(btnViewLp) %>;
    var ltrViewFindings = <%= AspxTools.JsGetElementById(ltrViewFindings) %>;
    var btnViewFindings = <%= AspxTools.JsGetElementById(btnViewFindings) %>;

    var args = new Object();
    args.loanId = <%= AspxTools.JsString(LoanID) %>;
    var result = gService.utils.call("CheckFindingsData", args);
    if (!result.error) {    
        var DUFindingsExist = result.value.DUFindingsExist == 'True';
        var LPFindingsExist = result.value.LPFindingsExist == 'True';
        var FHAFindingsExist = result.value.FHAFindingsExist == 'True';
        if( ltrViewDoFindings ) {
            ltrViewDoFindings.style.display = DUFindingsExist ? "none" : "";
        }
        if( lnkViewDoFindings ) {
            lnkViewDoFindings.style.display = DUFindingsExist ? "" : "none";
        }
        if( ltrViewDuFindings )  {
            ltrViewDuFindings.style.display = DUFindingsExist ? "none" : "";
        }
        if( lnkViewDuFindings ) {
            lnkViewDuFindings.style.display = DUFindingsExist ? "" : "none";
        }
        
        if(ltrViewLp) {
            ltrViewLp.style.display = LPFindingsExist ? "none" : "";
        }
        if( btnViewLp ){
            btnViewLp.style.display = LPFindingsExist ? "" : "none";
        }
        if( ltrViewFindings ) {
            ltrViewFindings.style.display = FHAFindingsExist ? "none" : "";
        }
        if( btnViewFindings ) {
            btnViewFindings.style.display = FHAFindingsExist ? "" : "none";
        }
    }
}

function f_sSpZip_onchange(){
    f_updateTexasCB();
    f_onValidateRequiredFields();
}

function f_launchTotal()
{
    LQBPopup.Show(ML.VirtualRoot + '/main/FHATotalDataAudit.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>,
      {
        width:'860',
        height:'600',
        hideCloseButton: true,
        onReturn: function() { 
          setAUSOptionsVisibility();
        }
      });
    //showModal(, null, 'FHA TOTAL Scorecard');
    //setAUSOptionsVisibility();
}
function f_sendToTotal(event){
  var ddl = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
  var isVaIRRRL = false;
  if (typeof ddl != 'undefined' && ddl != null && ddl.value){
      isVaIRRRL = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>;
  }
  if (isVaIRRRL) return false;
  
  document.getElementById('Launch').value = 'TOTAL';
  document.getElementById('_PreviousScroll').value = document.body.scrollTop;
        document.getElementById('sFileVersion').value = <%= AspxTools.JsString(ConstAppDavid.SkipVersionCheck) %>;

  f_switchTab(_currentIndex, true, null, event); <%-- Save current step. After saving the postback will launch TOTAL dialog --%>
  return false;
}


function f_onchange_sApprValPe(){
  var sApprValPe = <%= AspxTools.JQuery(sApprValPe) %>;

  
  if (sApprValPe.val() == "" || sApprValPe.val() == "0")
    sApprValPe.val("$0.00");
    
  $j("#sProdCalcEntryT").val(sApprValPe.data('sProdCalcEntryT'));
  
  f_onchange_sProdCalcEntryFields(sApprValPe);
}

function f_validate_sApprValPe( source, args ) {
    var sApprValPe = <%= AspxTools.JQuery(sApprValPe) %>.val();
    var sApprValPeMoneyValue = getMoneyValue( sApprValPe );
    var negWarning = document.getElementById('negApprVal'); 
    negWarning.style.display = sApprValPeMoneyValue < 0 ? '' : 'none';
    
    <% if (!m_renderAsSecond) { %> 
    var ddl = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
    var isPurchase = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %>;
    <% } else { %>
    var isPurchase = false;
    <% } %>
    
    var $isRenovationLoan = <%= AspxTools.JQuery(sIsRenovationLoan) %>;
    
    
    if ($isRenovationLoan.prop('checked') && isPurchase && sApprValPeMoneyValue <= 0) { // here 0 is an invalid number
        args.IsValid = false; 
    } else {
        args.IsValid = sApprValPeMoneyValue >= 0; // here it's not
    }
}

function f_validatesHouseValPe (source, args) {
    var sHouseValPe = <%= AspxTools.JQuery(sHouseValPe) %>.val();
    var sHouseValPeMoneyValue = getMoneyValue( sHouseValPe );
    
    <% if (!m_renderAsSecond) { %> 
    var ddl = <%= AspxTools.JsGetElementById(sLPurposeTPe) %>;
    var isRefinance = ddl.value == <%= AspxTools.JsString(E_sLPurposeT.Refin.ToString("D")) %> || ddl.value == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %>;
    <% } else { %>
    var isRefinance = true;
    <% } %>
    
   
   //completed value for refinance has to be filled out.
    var $isRenovationLoan = <%= AspxTools.JQuery(sIsRenovationLoan) %>; 
    if ($isRenovationLoan.prop('checked') && isRefinance && sHouseValPeMoneyValue <= 0) { // here 0 is an invalid number
        args.IsValid = false;
    } else {
        args.IsValid = sHouseValPeMoneyValue >= 0; // here it's not
    }
}

function format_percent_callback(){
    if(this.id === '<%=AspxTools.ClientId(sProdFhaUfmip) %>' || this.id === '<%= AspxTools.ClientId(sProdVaFundingFee) %>'){
        f_onValidateRequiredFields();
    }
}

function f_onchange_sOriginatorCompensationPaymentSourceT( bInit )
{
 <% if (m_useOriginatorComp) { %>
    var originatorSec = document.getElementById('LoanOriginatorSection'); 
    var bShow = document.getElementById('<%= AspxTools.ClientId(sOriginatorCompensationPaymentSourceT_Borrower) %>').checked;
    if ( originatorSec )
        originatorSec.style.display = bShow ? 'block' : 'none';
    <% } %>
}

function IsValidCurrency(sender, args) {
    var input = args.Value,
        isNegative = input.indexOf("(") == 0 && input.indexOf(")") == input.length  -1,
        re = /^\$|,|\(|\)/g;
    
    
    if (isNegative) {
        args.IsValid = false;
        return;
    }
    
    input = input.replace(re, "");
    
    var amount = parseFloat(input);
    if (!amount){
        args.IsValid = false;
        return;
    }
    
    args.IsValid = true;
}

    <%if(CurrentBroker.IsEnabledLockPeriodDropDown){ %>
    function f_updateProdRLckdDaysFromDDL(ddl)
    {
        var hiddenTB = <%= AspxTools.JsGetElementById(sProdRLckdDays) %>;
        hiddenTB.value = ddl.value;
        hiddenTB.onchange();            // trigger the calculations that were on the rate lock textbox.
        f_onValidateRequiredFields();   // enable the navigation buttons.
    }
    <%} %>

function f_validate_sSubFinPe(sender, args) {
    // This must be a valid currency > 0 and greater than either sLAmtCalcPe
    // or sProOFinBalPe. These fields are swapped for first and second liens,
    // so need to check the appropriate one. OPM 107986
    var $sSubFinPe = <%= AspxTools.JQuery(sSubFinPe) %>;
    var $compareField = null;
    <% if (m_renderAsSecond) { %>
    $compareField = <%= AspxTools.JQuery(sLAmtCalcPe) %>;
    <% } else { %>
    $compareField = <%= AspxTools.JQuery(sProOFinBalPe) %>;
    <% } %>
    
    var sSubFinPeVal = getMoneyValue($sSubFinPe.val());
    var compareFieldVal = getMoneyValue($compareField.val());
    
    args.IsValid = (sSubFinPeVal != null && compareFieldVal != null && 
        sSubFinPeVal > 0 && sSubFinPeVal >= compareFieldVal);
}

</script>
 

<asp:PlaceHolder runat="server" ID="VALoanScript">
    <script type="text/javascript"  >
        function f_validatesProdVaFundingFee(sender, args) {
            var sProdVaFundingFee = args.Value; 
            var sProdVaFundingFeeNegErrorMsg = document.getElementById('sProdVaFundingFeeNegErrorMsg'); 
            
            f_validateNegativeNonRequired(sProdVaFundingFeeNegErrorMsg, !!sProdVaFundingFee.match('-'), args);
        }
    </script>
</asp:PlaceHolder>

<div class="PropertyInfoPanel" runat="server">

<p>Enter your property and loan details on this page.  All fields with a &nbsp;<IMG src="../images/error_pointer.gif"> &nbsp;icon are required.  
Remember, the more information you provide on this screen, the more accurate your results. </p>
<fieldset id="personal" name="personal">
    <legend>Property Information</legend>
    <br />
    
    <ml:EncodedLabel runat="server" AssociatedControlID="sSpAddr" Text="Property Address" />
    <asp:textbox id=sSpAddr name="address" runat="server" onchange="f_onValidateRequiredFields();" Width="200px" tabindex=1 />
    <asp:requiredfieldvalidator id="sSpAddrValidator" runat="server" ControlToValidate="sSpAddr" cssclass="RequireValidatorMessage" forecolor=""></asp:requiredfieldvalidator>  
    <br />
    
    <ml:EncodedLabel runat="server" AssociatedControlID="sSpZip" Text="Zip Code" />
    <ml:zipcodetextbox id=sSpZip name="address" runat="server" onchange="f_sSpZip_onchange();" width="70px" TabIndex=2 preset="zipcode" />
    <asp:requiredfieldvalidator id="sSpZipValidator" runat="server" ControlToValidate="sSpZip" cssclass="RequireValidatorMessage" forecolor=""></asp:requiredfieldvalidator>
    
    
    <br />
    
    <ml:EncodedLabel runat="server" AssociatedControlID="sSpCity" Text="City" />
    <asp:textbox id=sSpCity name="address" runat="server" onchange="f_onValidateRequiredFields();"  Width="130px" TabIndex=3 />
    <asp:requiredfieldvalidator id="sSpCityValidator" runat="server" ControlToValidate="sSpCity" cssclass="RequireValidatorMessage" forecolor=""></asp:requiredfieldvalidator> 
    
    <br />
    
    <ml:EncodedLabel runat="server" AssociatedControlID="sSpStatePe" Text="State" /> 
    <ml:statedropdownlist name="address" id=sSpStatePe onkeyup="if (event.keyCode == 13) f_getResult(event);" tabIndex=4 runat="server" onchange="f_sSpStatePe_onchange();" IncludeTerritories="false"></ml:statedropdownlist>
    <SPAN class=RequireValidatorMessage id=sSpStatePeValidator><IMG src="../images/error_pointer.gif" ></SPAN>
    <br />
    
	<label for="sSpCounty">County</label> 
	<select id="sSpCounty" name="sSpCounty" style="width:127px" TabIndex=5 onchange="javascript:f_onValidateRequiredFields()">
		<asp:Repeater runat="Server" id="counties" OnItemDataBound="CountyDataBound"> 
			<ItemTemplate> 
                <option runat="server" id="countyOption"></option>
			</ItemTemplate>
		</asp:Repeater>
	</select> 
	<SPAN class=RequireValidatorMessage id=sSpCountyValidator><IMG src="../images/error_pointer.gif" ></SPAN> 
	<br />
	<br />
	<label id="sOccTPeLabel" runat="server">Property Use</label> 
	<asp:dropdownlist id="sOccTPe" runat="server" Width="135px" tabindex="6" />
	<br />
	<div id="HasNonOccupantCoborrower">
	
	    <label for="<%= AspxTools.HtmlString(NonOccupantCoborrower.ClientID) %>">Has Non-Occupant Co-Borrower</label>
	    <asp:CheckBox ID="NonOccupantCoborrower" TabIndex=7 runat="server" />&nbsp;Yes
	</div>
	<label id="sProdSpTLabel" runat="server">Property Type</label> 
	<asp:dropdownlist id=sProdSpT tabIndex=7 runat="server" onchange="f_onchange_sProdSpT(this);" width="110px" />
	<br />
	<label id="sProdSpStructureTLabel" runat="server">Structure Type</label> 
    <asp:dropdownlist id=sProdSpStructureT tabIndex=8 runat="server" width="110px" />
	<br />
	<label id="sProdCondoStoriesLabel" runat="server">Condo Stories</label> 
    <asp:textbox id=sProdCondoStories tabIndex=9 runat="server" width="50px" maxlength="3" />
    <br />
    <label id="sFhaCondoApprovalStatusTLabel" runat="server">FHA Condo Approval Status</label> 
    <asp:DropDownList ID="sFhaCondoApprovalStatusT" tabIndex="10" runat="server" />
    <br />
    <label id="sProdIsSpInRuralAreaLabel" runat="server">In Rural Area?</label>
    <asp:checkbox id=sProdIsSpInRuralArea tabIndex="11" runat="server" onclick="f_onchange_sLPurposeTPe(false)"  />&nbsp;Yes&nbsp;(<a onclick="return f_openHelp('Q00014.html', 200, 100);" tabindex="19" href="#" >explain</a>)
	<br />
	<label id="sProdIsCondotelLabel" runat="server">Condotel?</label>
	<asp:checkbox id=sProdIsCondotel tabIndex="12" runat="server" />&nbsp;Yes
	<br />
	<label id="sProdIsNonwarrantableProjLabel" runat="server">Non-Warrantable Project?</label> 
    <asp:checkbox id=sProdIsNonwarrantableProj tabIndex="13" runat="server" />&nbsp;Yes
	<br />

    <ml:EncodedLabel  runat="server" AssociatedControlID="sMonthlyPmtPe" >Additional Monthly Housing Expenses </ml:EncodedLabel>
    <ml:MoneyTextBox runat="server" ReadOnly="true" id="sMonthlyPmtPe"></ml:MoneyTextBox><span runat="server" id="monthlyexpenses">(<a href="#" onclick="return showOtherExpenses();" >calculate</a>)</span>
    <br />
    
    <div id="investmentPanel">
	<br />
	<label id="sOccRPeLabel" runat="server">Occupancy Rate (%)</label> 
    <ml:percenttextbox id=sOccRPe tabIndex="16" runat="server" width="80px" preset="percent" />
	<br />
	<label id="sSpGrossRentPeLabel" runat="server">Gross Rent</label> 
    <ml:moneytextbox id=sSpGrossRentPe tabIndex="17" runat="server" width="80px" preset="money" />
    <br />
    <br />
    </div>
   </fieldset>
   
   <fieldset id="presentHousing" name="presentHousing">
    <legend>Present Housing Expense</legend>
    <br />

    <label id="aPresOHExpPeLabel" runat="server">Present Housing Expense </label>
    <ml:moneytextbox id=aPresOHExpPe tabIndex=17 runat="server" width="130px" preset="money" />
    <br />
    <br />
   </fieldset>

<% if (!m_renderAsSecond) { %> 
	<input type="hidden" id="m_trades" value="" runat="server" />
    <input type="hidden" id="m_trades12" value="" runat="server" />
<% } %> 
    
  <fieldset id="loaninformation" name="loaninformation">
    <legend>Loan Information</legend>
    <br />
    <% if (!m_renderAsSecond) { %> 
    <label id="sLPurposeTPeLabel" runat="server">Loan Purpose </label>
    <asp:dropdownlist id=sLPurposeTPe tabIndex=18 runat="server" onchange="f_onchange_sLPurposeTPe(false);" width="135px"></asp:dropdownlist>
    <asp:CustomValidator id="sLPurposeTPeValidator" runat="server" ControlToValidate="sLPurposeTPe" ClientValidationFunction="f_validatesLPurposeTPe"></asp:CustomValidator>
	<span id="PurposeMessage" class="RequireValidatorMessage" style="font-weight:normal; display: none"></span>
	<br />
	<% } %> 
	
	<div id="CreditQualifyingPanel" style="display:none;">
	<label id="sIsCreditQualifyingLabel" runat="server">Credit Qualifying?</label>
	<asp:checkbox id="sIsCreditQualifying" tabIndex="18"  onclick="f_onchanche_sIsCredQual();" runat="server" />&nbsp;Yes&nbsp;&nbsp;
	(<a onclick="return f_openHelp('Q00012.html', 500, 250);" tabIndex="18" href="#" >explain</a>)
	<br />
	</div>
	
	<div class="TexasPanel" style="display:none;">
	<ml:EncodedLabel runat="server" AssociatedControlID="sProdIsTexas50a6Loan" Text="New Loan is Texas 50(a)(6)?" />
	<asp:checkbox id=sProdIsTexas50a6Loan tabIndex=19 runat="server" />&nbsp;Yes&nbsp;&nbsp;
	(<a onclick="return f_openHelp('Q00010.html', 400, 200);" tabIndex="19" href="#" >explain</a>)
	<br />
	</div>

    <div class="TexasPanel" style="display:none;">
    	<ml:EncodedLabel runat="server" AssociatedControlID="sPreviousLoanIsTexas50a6Loan" Text="Prior Loan is Texas 50(a)(6)?" />
	    <asp:checkbox id=sPreviousLoanIsTexas50a6Loan tabIndex=19 runat="server" />&nbsp;Yes&nbsp;&nbsp;
	    <br />
	</div>
	
	<div id="sSpLienPanel" style="display:none;">
	<label id="sSpLienLabel" runat="server">Outstanding Principal Balance</label>
    <ml:moneytextbox id="sSpLien" tabIndex=19 runat="server" onchange="f_updateVaFha();" width="100px" preset="money" />
    <asp:CustomValidator id=sSpLienValidator runat="server" ControlToValidate="sSpLien" ClientValidationFunction="f_validatesSpLien"></asp:CustomValidator>
    <asp:Image ID="ErrorsSpLienImg" style="display:none;" runat="server"  ToolTip="Error" ImageUrl="~/images/error_pointer.gif"/>&nbsp;&nbsp;
    (<a onclick="return f_openHelp('Q00011.html', 400, 150);" tabindex="19" href="#" >explain</a>)
	<br />
	</div>
	
	<div id="sFHASalesConcessionsPanel" style="display:none;">
	<label id="sFHASalesConcessionsLabel" runat="server">Upfront MIP Refund</label>
    <ml:moneytextbox id="sFHASalesConcessions" tabIndex=19 runat="server" onchange="f_updateVaFha();" width="100px" preset="money" />
    <asp:CustomValidator id=sFHASalesConcessionsValidator runat="server" ControlToValidate="sFHASalesConcessions" ClientValidationFunction="f_validatesFHASalesConcessions" />
	<br />
	</div>
	
	<label id="sProdCashoutAmtLabel" runat="server">Cashout Amount</label>
    <ml:moneytextbox id=sProdCashoutAmt tabIndex=19 runat="server" onchange="f_onchange_sProdCashoutAmt();" width="100px" preset="money" />
    <asp:CustomValidator id="sProdCashoutAmtValidator" runat="server" ControlToValidate="sProdCashoutAmt" CssClass="RequireValidatorMessage" ClientValidationFunction="validateCashoutAmount" />
	<br />
	
	<div id="sIsRenovationLoanPanel" runat="server">
	<label id="sIsRenovationLoanLabel">Is Renovation Loan?</label>
	<asp:CheckBox ID="sIsRenovationLoan" onclick="f_onchange_sIsRenovationLoan(this);" runat="server" />Yes
	</div>
	
	<div id="sTotalRenovationCostsPanel">
	<label id="sTotalRenovationCostsLabel">Total Renovation Costs</label>
	<ml:MoneyTextBox ID="sTotalRenovationCosts" runat="server" width="100px"  preset="money" onchange="f_onchange_sTotalRenovationCosts(this);"></ml:MoneyTextBox>
	<asp:CustomValidator ID="sTotalRenovationCostsValidator" ValidateEmptyText="true" ControlToValidate="sTotalRenovationCosts" InitialValue="$0.00" ClientValidationFunction="f_validatesTotalRenovationCosts" cssclass="RequireValidatorMessage" runat="server"></asp:CustomValidator>
	</div>
	
	<% if (!m_renderAsSecond) { %> 
	<label id="sHas1stTimeBuyerPeLabel" runat="server">First Time Home Buyer?</label> 
    <asp:checkbox id=sHas1stTimeBuyerPe disabled onclick="f_setFthb();" tabIndex=20 runat="server" />&nbsp;Yes&nbsp;
    (<A id="lnkHelpHousingHistory" onclick="return f_openHelp('HousingHistory.aspx', 450, 360);" tabIndex=20 href="#" >explain</A>)
	<br />
    
    <div id="historyCheckBox">
    <label id="sProdHasHousingHistoryLabel" runat="server">Has Housing History</label> 
    <asp:checkbox id=sProdHasHousingHistory disabled tabIndex=21 runat="server" />&nbsp;Yes&nbsp;
    (<A id="lnkHelpHasHousingHistory" onclick="return f_openHelp('HousingHistory.aspx', 450, 360);" href="#" >explain</A>)
    <br />
    </div>
    
    <% } %>
    
    <label id="sNumFinancedPropertiesLabel" runat="server">Number of Financed Properties</label>
    <asp:TextBox ID="sNumFinancedProperties" runat="server" Width="90px" onchange="f_onchange_sNumFinancedProperties();"></asp:TextBox>
    <asp:customvalidator id="sNumFinancedPropertiesValidator" runat="server" cssclass="RequireValidatorMessage" ValidateEmptyText="true" ControlToValidate="sNumFinancedProperties" ClientValidationFunction="f_validateNumFinancedProperties" Display="Dynamic"></asp:customvalidator>

    (Include subject property)
    (<a id="lnkHelpNumFinancedProperties" onclick="return f_openHelp('NumberOfFinancedProperties.aspx', 450, 130);" href="#">explain</a>)
    <br />
      	
	<% if (m_renderAsSecond) { %>
    <label id="sLpIsNegAmortOtherLienLabel" runat="server">1st Lien has Negative Amort.</label>
    <asp:checkbox id="sLpIsNegAmortOtherLien" runat="server" />Yes (Potential)
    <br />
    
    <label id="sOtherLFinMethTLabel" runat="server">1st Lien Amortization Type</label>
    <asp:dropdownlist id="sOtherLFinMethT" runat="server" />
    <br />
    <% } %>
    
	
	<% if (!m_renderAsSecond) { %>
    <label id="sProdImpoundLabel" runat="server">Impound?</label>
    <asp:CheckBox ID="sProdImpound" TabIndex="23" runat="server" />&nbsp;Yes 
    <br />
    <% } %>
	
	<label id="sProdDocTLabel" runat="server">Doc Type</label>
    <asp:dropdownlist id=sProdDocT tabIndex=24 runat="server" onchange="f_onchange_sProdDocT();" width="290px" />
	<br />
	
	<div id="IncomeSpan" style="margin-bottom:6px;">
	
	<% if (sIsPrimaryWageEarner.Visible) { %>
	<label id="sIsPrimaryWageEarnerLabel" runat="server">Primary Wage Earner</label>
    <asp:dropdownlist id="sIsPrimaryWageEarner" tabIndex="25" runat="server" width="180px" />
	<br />
	<% } %>
	
	<div id="TotalIncomePanel">
	<label id="sPrimAppTotNonspIPeLabel" runat="server">Total Income</label> 
    <ml:moneytextbox id=sPrimAppTotNonspIPe onkeyup=f_onValidateRequiredFields(); tabIndex=25 runat="server" onchange="f_onchange_sPrimAppTotNonspIPe();" width="100px" preset="money" />&nbsp;/ month
    <asp:requiredfieldvalidator id=sPrimAppTotNonspIPeValidator runat="server" InitialValue="$0.00" cssclass="RequireValidatorMessage" forecolor=" " controltovalidate="sPrimAppTotNonspIPe" Display="Dynamic"></asp:requiredfieldvalidator>&nbsp;
    (<A id="lnkHelpTotalIncome" onclick="return f_openHelp('Q00004.html', 730, 600);" href="#" tabIndex=25 >explain</A>)
	<br />
	</div>
	
	  <div id="EstimatedResidualIncomePanel" style=<%= AspxTools.HtmlAttribute("display: " + ((m_isShowsProdEstimatedResidualI) ? "block" : "none")) %>>
      <ml:EncodedLabel runat="server" AssociatedControlID="sProdEstimatedResidualI" Text="Estimated Residual Income" />
      <ml:moneytextbox id=sProdEstimatedResidualI onchange="format_money(this); f_onValidateRequiredFields();" tabIndex=26 runat="server" width="90" preset="money"></ml:moneytextbox>&nbsp;/ month
      <asp:requiredfieldvalidator id=sProdEstimatedResidualIValidator runat="server" InitialValue="$0.00" cssclass="RequireValidatorMessage" forecolor=" " controltovalidate="sProdEstimatedResidualI" Display="Dynamic"></asp:requiredfieldvalidator>&nbsp;
      (<A id="lnkHelpResidualIncome" onclick="return f_openHelp('Q00009.html', 730, 220);" tabIndex=26 href="#" >explain</A>)
      <br />
      </div>
	
	</div>
	
    <div id="AssetSpan" style="margin-bottom:2px;">
    <label id="sProdAvailReserveMonthsLabel" runat="server">Reserves Available</label>
    <asp:dropdownlist id=sProdAvailReserveMonths tabIndex=27 runat="server" Width="100px" onchange="f_onValidateRequiredFields();" />&nbsp;
    <asp:customvalidator id=sProdAvailReserveMonthsValidator runat="server" ControlToValidate="sProdAvailReserveMonths" ClientValidationFunction="f_validateReserve"></asp:customvalidator>
	<br />
	</div>
	
	<% if (!m_renderAsSecond) { %>
    <label id="sProdRLckdDaysLabel" runat="server">Rate Lock Period </label> 
    <% if (CurrentBroker.IsEnabledLockPeriodDropDown){ %>
            <asp:DropDownList runat="server" ID="sProdRLckdDaysDDL" onchange="f_updateProdRLckdDaysFromDDL(this)"></asp:DropDownList>
    <% } %>
    <asp:textbox id=sProdRLckdDays style="TEXT-ALIGN: right" tabIndex=28 runat="server" Width="50px" onchange="f_onchange_sProdRLckdDays();"></asp:textbox>&nbsp;days&nbsp;
	<asp:customvalidator id=sProdRLckdDaysValidator runat="server" ClientValidationFunction="f_validateLckdDays"></asp:customvalidator>
	<br />
	<label>Rate Lock Expiration Date</label> 
    <ml:EncodedLabel id="sProdRLckdExpiredDLabel" runat="server" Width="250px"></ml:EncodedLabel>
    <br />
	<% } %>
	<br />
    
  </fieldset>
  
  
  <fieldset id="loanAmountCalculator" name="loanAmountCalculator">
    <legend><% if (m_renderAsSecond) { %>2nd <% } %>Loan Amount Calculator</legend>
	<br />
	
	<div id="sPurchaseAppraisalPanel">
	    <ml:EncodedLabel ID="sApprValPeLabel" runat="server" AssociatedControlID="sApprValPe" Text="Appraised Value" /> <%-- Also "As-Completed Value"--%>
	    <ml:MoneyTextBox runat="server" TabIndex=29 ID="sApprValPe" Width="100px" sProdCalcEntryT="7" onkeyup="f_onValidateRequiredFields();" onchange="f_onchange_sApprValPe();"></ml:MoneyTextBox>
	    <asp:CustomValidator runat="server" ID="sApprValPeValidator" ClientValidationFunction="f_validate_sApprValPe"  ControlToValidate="sApprValPe"></asp:CustomValidator>	    
	    <span id="negApprVal" style="display:none; color: Red;"> Negative Appraised Value is not allowed.</span>
	 
	    <br />
	</div>
	
    <div id="sHasAppraisalPanel" style="display:none;">
	<label id="sHasAppraisalLabel" runat="server">Appraisal?</label>
	<asp:checkbox id="sHasAppraisal" tabIndex="29" onclick="f_updateVaFha();"  runat="server" />&nbsp;Yes&nbsp;&nbsp;
	<br />
	</div>
	
	<div id="sOriginalAppraisedValuePanel" style="display:none;">
	<label id="sOriginalAppraisedValueLabel" runat="server">Original Appraised Value</label>
    <ml:moneytextbox id="sOriginalAppraisedValue" tabIndex=29 runat="server" onchange="f_updateVaFha();" width="100px" preset="money" />
    <asp:customvalidator id=sOriginalAppraisedValueValidator runat="server" ControlToValidate="sOriginalAppraisedValue" ClientValidationFunction="f_validatesOriginalAppraisedValue" />
	<br />
	</div>

    <div id="sHouseValPePanel" >
    <label id="sHouseValPeLabel" runat="server">Home Value</label> <%-- Also "As-Completed Value" and "Sales Price" --%>
    <ml:moneytextbox id=sHouseValPe onkeyup=f_onValidateRequiredFields(); tabIndex=29 runat="server" onchange="f_onchange_sHouseValPe();" width="100px" preset="money" sProdCalcEntryT="7" />
    <asp:CustomValidator id="sHouseValPeValidator" runat="server" ValidateEmptyText="true" ControlToValidate="sHouseValPe" InitialValue="$0.00" ClientValidationFunction="f_validatesHouseValPe" forecolor=" "></asp:CustomValidator>
    <asp:requiredfieldvalidator id="sHouseValPeRequireValidator" runat="server" ControlToValidate="sHouseValPe" InitialValue="$0.00" cssclass="RequireValidatorMessage" forecolor=" "></asp:requiredfieldvalidator>
    <span id="negHouseVal" style="display:none; color:red" >Negative Sales Price is not allowed.</span>
	<br />
	</div>
	
	<div id="sAsIsAppraisedValuePevalPanel" >
    <label id="sAsIsAppraisedValuePevalLabel" runat="server">As-Is</label>
    <ml:moneytextbox id="sAsIsAppraisedValuePeval" onkeyup="f_onValidateRequiredFields();" onchange="f_onchange_sProdCalcEntryFields($j(this));" tabIndex=29 runat="server"  width="100px" preset="money" sProdCalcEntryT="10" />
    <asp:CustomValidator ID="sAsIsAppraisedValuePevalNegativeValidator" ValidateEmptyText=true   runat="server" cssclass="RequireValidatorMessage" forecolor=" " ControlToValidate="sAsIsAppraisedValuePeval" ClientValidationFunction="f_sAsIsAppraisedValuePeval" />
    <span id="negAsIsValue" style="display:none; color:red" >Negative As-Is is not allowed.</span>
    <span id="explainAsIsAppraised" > (<a onclick="return f_openHelp('Q00017.html', 300, 200);" href="#">explain</a>)</span>
	<br />
	</div>
	
	<br />
	
	<label id=DownPaymentLabel for="rural">Down Payment / Equity</label>
    <ml:percenttextbox id=sDownPmtPcPe tabIndex=30 runat="server" width="100px" preset="percent" />&nbsp;&nbsp;
    <ml:moneytextbox id=sEquityPe tabIndex=31 runat="server" width="100px" preset="money" />
    <span id="errDownPmt" style="display:none; color:red; margin-bottom:8px;" >Down Payment cannot be greater than Sales Price.</span>
	<br />
	
	<label id="sLtvRPeLabel" runat="server">1st Lien</label>
    <ml:percenttextbox id=sLtvRPe tabIndex=32 runat="server" onchange="f_onchange_sProdCalcEntryFields(this);" width="100px" preset="percent" sProdCalcEntryT="2" />&nbsp;&nbsp;
    <span id="FirstBal">
      <ml:moneytextbox id=sLAmtCalcPe tabIndex=33 runat="server" onchange="f_onchange_sProdCalcEntryFields(this);" width="100px" preset="money" sProdCalcEntryT="3" />
      <asp:CustomValidator id="sLAmtCalcPeValidator" runat="server" ClientValidationFunction="f_validatesLAmtCalcPe" Display="Static" EnableClientScript="True" />
    </span>
    <span id="neg1stLien" style="display:none; color:red;" >Negative 1st Lien amount is not allowed.</span>
    <% if ( m_renderAsSecond && sProOFinBalPeDdl.Items.Count > 0 && !IsReadOnly) { %>
		<br />
		<label>&nbsp;</label>
		<asp:DropDownList id="sProOFinBalPeDdl" Runat="server" TabIndex="65" onchange="f_onChange2ndDdl(this.value, 'bal');"  Width="212px" />
	<% } %>
	<br />
	
	  <label id="highlight1"> 2nd Financing?</label>
	<span id="highlight2" style="display:inline-block; margin-left:-3px; padding:10px 4px 0 4px">
	  <span style="display:inline-block; width:4em"><asp:RadioButton id=bNo2ndFinancing GroupName="secondFinancing" Checked="True" onclick="f_show2ndFinancing();" CssClass="normal nomargin" tabIndex=34 Text="No" runat="server"></asp:RadioButton></span>
      <span style="display:inline-block; width:4.5em"><asp:RadioButton id=bYes2ndFinancing GroupName="secondFinancing" Checked="False" onclick="f_show2ndFinancing();" CssClass="normal nomargin" tabIndex=35 Text="Yes" runat="server"></asp:RadioButton></span>
      <em id="highlight3" style="display:none;">Select 'Yes' if interested in 2nd Financing</em>
    </span>
    
    <div id="SecondFinancingRow">
        <label id="sProOFinBalTLabel" style="display: block; float: left">2nd Financing Type</label>
        <div style="display: block; float: left; margin-left:-3px; padding: 0px 7px;">
           <span style="display:inline-block; width:8em; line-height:1.5em"><asp:RadioButton id=bCloseEnd2ndFinancing GroupName="sSubFinT" onclick="f_update2ndFinancingType();" Checked="True" CssClass="normal nomargin" tabIndex=34 Text="Closed-end" runat="server"></asp:RadioButton></span>
           <span style="display:inline-block; width:6em; line-height:1.5em"><asp:RadioButton id=bHeloc2ndFinancing GroupName="sSubFinT" onclick="f_update2ndFinancingType(); " Checked="False" CssClass="normal nomargin" tabIndex=35 Text="HELOC" runat="server"></asp:RadioButton></span>
        </div>
        <br  style="clear: both" />
     
        <label id="sProOFinBalPeLabel" runat="server">Initial Draw Amount</label>
        <ml:percenttextbox id=sLtvROtherFinPe tabIndex=36 runat="server" onchange="f_onchange_sProdCalcEntryFields(this);" width="100px" preset="percent" sProdCalcEntryT="5" />&nbsp;&nbsp;
            <span id="SecondBal">
                <ml:moneytextbox id=sProOFinBalPe tabIndex=37 runat="server" onchange="f_onchange_sProdCalcEntryFields(this);" width="100px" preset="money" sProdCalcEntryT="6" />
                <% if (m_renderAsSecond ) { %>
                <asp:requiredfieldvalidator id="sProOFinBalPeValidator" runat="server" InitialValue="$0.00" cssclass="RequireValidatorMessage" forecolor=" " controltovalidate="sProOFinBalPe" Display="Dynamic" />
                <% } %>
            </span>
            <span id="neg2ndLien" style="display:none; color:red" >Negative 2nd Financing is not allowed.</span>
        <br />
   
        <div id="SecondFinancingCLTVPanel">
        
        </div>	    
        <div id="HelocFields">
	        <label id="sSubFinPeLabel" runat="server">Line Amount</label>
            <ml:percenttextbox id=sSubFinToPropertyValue tabIndex=38  sProdCalcEntryT="8" onchange="f_onchange_sProdCalcEntryFields(this);"  runat="server"  width="100px" preset="percent"  />&nbsp;&nbsp;
            <ml:moneytextbox id=sSubFinPe tabIndex=38 runat="server" sProdCalcEntryT="5" width="100px" onchange="f_onchange_sProdCalcEntryFields(this);f_onValidateRequiredFields();"  preset="money"  />
            <asp:CustomValidator runat="server" ID="sSubFinPeValidator" ControlToValidate="sSubFinPe" ClientValidationFunction="f_validate_sSubFinPe"></asp:CustomValidator>
            <br />
            <label id="sHCLTVRPeLabel" runat="server">HCLTV</label>
            <ml:percenttextbox id=sHCLTVRPe tabIndex=38 runat="server"  sProdCalcEntryT="9" onchange="f_onchange_sProdCalcEntryFields(this);"  width="100px" preset="percent"  ReadOnly="false" />&nbsp;&nbsp;
	    </div>
        
        <div id="RequestCommunityOrAffordableSecondsPanel">
            <label>Community / Affordable Seconds?</label>
            <asp:checkbox runat="server" id="sRequestCommunityOrAffordableSeconds" />
            <br />        
        </div>
    </div>
    
    <br />
    <br />
    <div id="StandaloneCLTVPanel">
	    <label id="sCltvRPeLabel" runat="server">CLTV</label>
        <ml:percenttextbox id=sCltvRPe tabIndex=38 runat="server" onchange="f_onchange_sProdCalcEntryFields(this);" width="100px" preset="percent" sProdCalcEntryT="4" />
	    <br />
    </div>
</fieldset>

<fieldset id="otherInformation" name="otherInformation">
    <legend>Other Information</legend>
	<br />
	
	<% if (PriceMyLoanConstants.IsEmbeddedPML && m_isShowPriceGroupInEmbeddedPml) { %>
	<label id="sProdLpePriceGroupNmLabel" runat="server">Price Group</label>
    <asp:textbox id=sProdLpePriceGroupNm runat="server" Width="200px" ReadOnly="True" />
    <br />
    <br />
    <% } %>
    
     <div id="masterPriorSalesPanel" style="display:none"> 
        <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="sPriorSalesD" >Prior Sales Date</ml:EncodedLabel>
        <asp:TextBox runat="server" ID="sPriorSalesD" onkeypress="f_update_PriorSalesPanel();" tabIndex=39    Width="70" ToolTip="mm/dd/yyyy" onchange="f_update_PriorSalesPanel();f_onValidateRequiredFields();">mm/dd/yyyy</asp:TextBox>
        <asp:CustomValidator runat="server" Display="Dynamic" ClientValidationFunction="f_validatesPriorSalesD" ID="sPriorSalesDValidator"></asp:CustomValidator>
        &nbsp;&nbsp;(<a id="A1" onclick="f_openHelp('Q00013.html', 150, 150); return false;" href="#" >explain</A>) 
        <br />
        <div id="priorsalespanelExtraInfo" style="display:none;">
            <ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="sPriorSalesPrice">Prior Sales Price</ml:EncodedLabel>
            <ml:MoneyTextBox runat="server" ID="sPriorSalesPrice" onchange="f_onValidateRequiredFields();" tabIndex=39  />
            <asp:CustomValidator runat="server" Display="Dynamic" ID="sPriorSalesPriceValidator" ControlToValidate="sPriorSalesPrice" ClientValidationFunction="f_validatesPriorSalesPrice" />
            <br />
            <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="sPriorSalesSellerT">Property Seller</ml:EncodedLabel>
            <asp:DropDownList runat="server" ID="sPriorSalesSellerT" onchange="f_onValidateRequiredFields();" tabIndex=39  />
            <asp:RequiredFieldValidator runat="server" ID="sPriorSalesSellerValidator" ControlToValidate="sPriorSalesSellerT" />
            <br />
        </div>
    </div>
	
	<% if (!m_renderAsSecond ) { %>
	
	
	<%if ( m_isAutoPmiOrNewPml ) { %>
	<label id="sProdConvMIOptionTLabel" runat="server">Conv Loan PMI Type</label>
	<asp:dropdownlist id=sProdConvMIOptionT tabIndex=41 runat="server" onchange="f_showSplitRow();" />
    <asp:CustomValidator id=sProdConvMIOptionTValidator runat="server" ControlToValidate="sProdConvMIOptionT" ClientValidationFunction="f_validatesProdConvMIOptionT"></asp:CustomValidator>
    
     <div id="SplitPmiRow" style="display:none">
            <label id=sConvSplitMIRTLabel runat="server">Split MI Upfront Premium</label>
            <asp:dropdownlist id="sConvSplitMIRT" tabIndex="15" runat="server" onchange="f_onValidateRequiredFields();" ></asp:dropdownlist>
            <asp:CustomValidator id="sConvSplitMIRTValidator" runat="server" ControlToValidate="sConvSplitMIRT" ClientValidationFunction="f_validatesConvSplitMIRT"></asp:CustomValidator>
            
     </div>


    <% } else { %>
    <label id="sProdMIOptionTLabel" runat="server">Mortgage Insurance</label>
    <asp:dropdownlist id=sProdMIOptionT tabIndex=41 runat="server" onchange="f_onValidateRequiredFields();" />
    <asp:CustomValidator id=sProdMIOptionTValidator runat="server" ControlToValidate="sProdMIOptionT" ClientValidationFunction="f_validatesProdMIOptionT"></asp:CustomValidator>
    <% } %>
	<br />
    
    <div id="ufmipPanel" >
    <label id="sProdIsFhaMipFinancedLabel" runat="server">Is FHA UFMIP Financed?</label>
    <asp:checkbox id="sProdIsFhaMipFinanced" tabIndex=42 runat="server" onchange="f_onValidateRequiredFields();" />Yes
    <br />
    
    <div id="EndorsedPanel" >
    <ml:EncodedLabel runat="server"   AssociatedControlID="sProdIsLoanEndorsedBeforeJune09">Was existing loan endorsed on or before May 31, 2009?</ml:EncodedLabel>
    <asp:CheckBox runat="server" ID="sProdIsLoanEndorsedBeforeJune09" onclick="window.setTimeout(refreshCalculation,0);" />&nbsp;Yes <br />
    </div>
    <ml:EncodedLabel runat="server" AssociatedControlID="sProdFhaUfmip" ID="sProdFhaFMIPLabel">FHA UFMIP</ml:EncodedLabel>
    <asp:TextBox runat="server" Width="70px" TabIndex="42" onkeyup="f_onValidateRequiredFields();" onchange="f_onValidateRequiredFields();" ID="sProdFhaUfmip" preset="percent" ></asp:TextBox>
    <asp:CustomValidator id="sProdFhaUfmipValidator" runat="server" ControlToValidate="sProdFhaUfmip" ClientValidationFunction="f_validatesProdFhaFMIP"></asp:CustomValidator>
    <span id="sProdFhaFMIPNegErrorMsg" style="display:none; color:red" ><asp:Image runat="server"  ToolTip="Error" ImageUrl="~/images/error_pointer.gif"/>&nbsp;&nbsp;Negative FHA UFMIP is not allowed.</span>
	<br />
	</div>

    <div id="VaFundingFeeSection">
        <ml:EncodedLabel runat="server" AssociatedControlID="sProdIsVaFundingFinanced">Is VA Funding Fee Financed?</ml:EncodedLabel>
        <asp:checkbox id="sProdIsVaFundingFinanced" tabIndex="42" runat="server" ></asp:checkbox>&nbsp;Yes
	    <br />	

	 
    <ml:EncodedLabel runat="server" AssociatedControlID="sProdVaFundingFee" ID="Label1">VA Funding Fee</ml:EncodedLabel>
    <asp:TextBox runat="server" Width="70px" TabIndex="42" onkeyup="f_onValidateRequiredFields();" onchange="f_onValidateRequiredFields();" ID="sProdVaFundingFee" preset="percent" ></asp:TextBox>
    <asp:CustomValidator  runat="server" ControlToValidate="sProdVaFundingFee" ID="sProdVaCustomValidator" ClientValidationFunction="f_validatesProdVaFundingFee"></asp:CustomValidator>
    <span id="sProdVaFundingFeeNegErrorMsg" style="display:none; color:red" ><asp:Image ID="Image1" runat="server"  ToolTip="Error" ImageUrl="~/images/error_pointer.gif"/>&nbsp;&nbsp;Negative VA Funding Fee is not allowed.</span>
	<br />
	</div>

	<div id="IsUsdaRuralHousingSection">
	    <ml:EncodedLabel runat="server" AssociatedControlID="sProdIsUsdaRuralHousingFeeFinanced">Is USDA Rural Housing Guarantee Fee Financed?</ml:EncodedLabel>
	    <asp:CheckBox runat="server" TabIndex="42" ID="sProdIsUsdaRuralHousingFeeFinanced"/>&nbsp;Yes
	</div>
	
	<% } %>
	
	<% if (m_renderAsSecond) { %>      
	<label id="sProOFinPmtPeLabel" runat="server">1st Lien Payment</label>
    <ml:moneytextbox id="sProOFinPmtPe" preset="money" runat="server" Width="90px" MaxLength="36" TabIndex="41" onchange="format_money(this); f_onchange_sProdCalcEntryFields(this);"></ml:moneytextbox>&nbsp;/ month
    <asp:requiredfieldvalidator id="sProOFinPmtPeValidator" runat="server" InitialValue="$0.00" cssclass="RequireValidatorMessage" forecolor=" " controltovalidate="sProOFinPmtPe" Display="Dynamic"></asp:requiredfieldvalidator>
    <br />
    <% } %>
    
    <% if ( m_renderAsSecond && sProOFinPmtPeDdl.Items.Count > 0 && !IsReadOnly ) { %>
    <label>&nbsp;</label>
    <asp:DropDownList id="sProOFinPmtPeDdl" Runat="server" onchange="f_onChange2ndDdl(this.value, 'pmt');" TabIndex="42" Width="212px" />
    <% } %>
	<% if (m_useOriginatorComp) { %>
	<br />
	    <label>Loan Originator is Paid By</label>
	<span style="display:inline-block; margin-left:-3px">
      <span style="display:inline-block;"><asp:RadioButton id="sOriginatorCompensationPaymentSourceT_Borrower" GroupName="sOriginatorCompensationPaymentSourceT" Checked="False" CssClass="normal nomargin" tabIndex=42 Text="Borrower" runat="server" onclick="f_onchange_sOriginatorCompensationPaymentSourceT(false)"></asp:RadioButton></span>
      <span style="display:inline-block;"><asp:RadioButton id="sOriginatorCompensationPaymentSourceT_Lender" GroupName="sOriginatorCompensationPaymentSourceT" Checked="False" CssClass="normal nomargin" tabIndex=42 Text="Lender" runat="server" onclick="f_onchange_sOriginatorCompensationPaymentSourceT(false)"></asp:RadioButton></span>
      <br />
      <div id="LoanOriginatorSection">
      <ml:percenttextbox id="sOriginatorCompensationBorrPaidPc" tabIndex=42 runat="server" onchange="" width="80px" preset="percent" sProdCalcEntryT="0" />&nbsp;of&nbsp;
      <asp:dropdownlist id="sOriginatorCompensationBorrPaidBaseT" TabIndex=42 runat="server"></asp:dropdownlist>&nbsp;+&nbsp;
      <ml:moneytextbox id="sOriginatorCompensationBorrPaidMb" tabIndex="42" runat="server" width="80px" preset="money"></ml:moneytextbox>
      </div>
    </span>
    <% } %>
	<br />
</fieldset>

<asp:PlaceHolder ID="Ask3rdPartyUwPlaceHolder" runat="server">
  <fieldset id="ausOptions" name="ausOptions">
    <legend>AUS Options</legend>
	<br />
	
	<asp:PlaceHolder ID="DOSubmissionPlaceHolder" runat="server">
    <label for="btnDo">Desktop Originator</label>
    <input type="button" id="btnDo" style="width: 140px" tabindex="43" value="Run scenario in DO..." onclick="f_submitToDoDu(event, true);" class="thirdPartyBtn" nohighlight /> &nbsp;&nbsp;<a id="lnkViewDoFindings" href="#" tabindex="44" onclick="return f_onViewDuFindingsClick('t');" runat="server">View DO Findings</a>
    <ml:EncodedLabel ID="ltrViewDoFindings" runat="server" Text="No DO Findings" />
    <br /><br />
  </asp:PlaceHolder>
    
  <asp:PlaceHolder ID="DUSubmissionPlaceHolder" runat="server">
      <label for="btnDu">Desktop Underwriter</label>
      <input type="button" tabindex="45" value="Run scenario in DU..." onclick="f_submitToDoDu(event, false);" nohighlight id="btnDu" style="width: 140px;" class="thirdPartyBtn" /> &nbsp;&nbsp;<a id="lnkViewDuFindings" tabindex="46" onclick="return f_onViewDuFindingsClick('f');" href="#" runat="server">View DU Findings</a>
      <ml:EncodedLabel ID="ltrViewDuFindings" runat="server" Text="No DU Findings" />
      <br /><br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="LPSubmissionPlaceHolder" runat="server">
    <label id="btnSendToLpLabel" for="btnSendToLp" runat="server">Loan Product Advisor</label> 
    <input type="button" id="btnSendToLp" style="width:140px" tabindex="47" value="Send To LPA..." onclick="f_sendToLp(event);" class="thirdPartyBtn" NoHighlight runat="server"/>
    &nbsp;&nbsp;<a id="btnViewLp" tabindex="48" onclick="return f_viewLpFeedback('f');" href="#" runat="server"><ml:EncodedLiteral id="btnViewLpLabel" runat="server">View LPA Feedback</ml:EncodedLiteral></a>
	<ml:EncodedLabel ID="ltrViewLp" runat="server" Text="No LPA Feedback"></ml:EncodedLabel>
    <br /><br />
	</asp:PlaceHolder>
  
  <asp:PlaceHolder ID="TotalSubmissionPlaceHolder" runat="server">
    <label for="btnSendToTotal">FHA Total Scorecard</label> 
    <input type="button" id="btnSendToTotal" style="width:140px" tabindex="49" value="Submit to FHA TOTAL..." disabled="disabled" onclick="f_sendToTotal(event);" class="thirdPartyBtn" NoHighlight />
    &nbsp;&nbsp;<a id="btnViewFindings" tabindex="49" onclick="return f_viewTotalFindings();" href="#" runat="server">View FHA TOTAL Scorecard Findings</a>
	<ml:EncodedLabel ID="ltrViewFindings" runat="server" Text="No FHA TOTAL Scorecard Findings" />
    <br /><br />
	</asp:PlaceHolder>  
    
    <ml:EncodedLabel runat="server" AssociatedControlID="sProd3rdPartyUwResultT" Text="AUS Response" />
    <asp:dropdownlist id=sProd3rdPartyUwResultT TabIndex=50 runat="server" />&nbsp;&nbsp;
    <asp:PlaceHolder ID="UwResultModify" runat="server">
    (<a id="sProd3rdPartyUwResultTLink" href="#" tabindex="49" onclick="return f_onAusResponseClick();" runat="server">modify</a>)
    </asp:PlaceHolder>
    <br /><br />
	
	<div id="isdurefinance">
        <ml:EncodedLabel runat="server" AssociatedControlID="sProdIsDuRefiPlus" Text="Is DU Refi Plus" />
        <asp:CheckBox ID="sProdIsDuRefiPlus" onclick="f_onsProdIsDuRefiPlusClick();" runat="server" />Yes
        (<a id="sProdIsDuRefiPlusLink" runat="server" href="#" tabindex="50" onclick="return f_onsProdIsDuRefiPlusEnable();">modify</a>)
        <br />
        <br />
    </div>
  
    <label>AUS Processing Type</label>
    Select all that apply <asp:customvalidator id="sProdFilterTypeValidator" runat="server" ClientValidationFunction="f_validatesProdFilterType"></asp:customvalidator>
    <asp:PlaceHolder ID="ProdIncludeModify" runat="server">
    (<a id="sProdIncludeModifyLink" runat="server" href="#" tabindex="50" onclick="return f_onAusProcessingClick();" >modify</a>)
    </asp:PlaceHolder>
    <br />
    <label>&nbsp;</label>
      <span style="display:inline-block; width:9.7em">
	  <asp:CheckBox ID="sProdIncludeNormalProc" onclick="f_onValidateRequiredFields();" runat="server"  Text="Conventional" CssClass="normal nomargin" TabIndex="50" />
	  </span>
      <span style="display:inline-block; width:15em">
      <asp:CheckBox ID="sProdIncludeMyCommunityProc" onclick="f_onValidateRequiredFields();" runat="server" Text="My Community / HomeReady" CssClass="normal nomargin" TabIndex="51" />
      </span>
      <span style="display:inline-block; width:10em">
      <asp:CheckBox ID="sProdIncludeHomePossibleProc" onclick="f_onValidateRequiredFields();" runat="server" Text="Home&nbsp;Possible" CssClass="normal nomargin" TabIndex="52" />
      </span>
    <br />
        <label>&nbsp;</label>
      <span style="display:inline-block; width:9.7em">
      <asp:CheckBox ID="sProdIncludeFHATotalProc" onclick="f_onValidateRequiredFields();" runat="server" Text="FHA" CssClass="normal nomargin" TabIndex="53" />
      </span>
      <span style="display:inline-block; width:15em">
      <asp:CheckBox ID="sProdIncludeVAProc" onclick="f_onValidateRequiredFields();" runat="server" Text="VA" CssClass="normal nomargin" TabIndex="54" />
      </span>
        <span style="display:inline-block;width:10em">
    <asp:CheckBox ID="sProdIncludeUSDARuralProc" onclick="f_onValidateRequiredFields();" runat="server" Text="USDA Rural" CssClass="normal nomargin" TabIndex="54" />
    </span>

    <br />
  </fieldset>
</asp:PlaceHolder>


<fieldset id="filtersAndSorting" name="filtersAndSorting">
    <legend>Filters & Sorting</legend>
	<br />

    <label id="sProdFilterDue10YrsLabel" runat="server">Term</label>
    <span style="display:inline-block; width:7em">
	<asp:CheckBox id="sProdFilterDue10Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="10yr" CssClass="normal" TabIndex=55 />
	</span>
    <span style="display:inline-block; width:7em">
	<asp:CheckBox id="sProdFilterDue15Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="15yr" CssClass="normal" TabIndex=56 />
	</span>
	<span style="display:inline-block; width:7em">
    <asp:CheckBox id="sProdFilterDue20Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="20yr" CssClass="normal" TabIndex=57 />
    </span>
    <span style="display:inline-block; width:7em">
    <asp:CheckBox id="sProdFilterDue25Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="25yr" CssClass="normal" TabIndex=58 />
    </span>
    <label>&nbsp;</label>
	<span style="display:inline-block; width:7em">
    <asp:CheckBox id="sProdFilterDue30Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="30yr" CssClass="normal" TabIndex=59 />
    </span>	
    <span style="display:inline-block; width:6em">
    <asp:CheckBox id="sProdFilterDueOther" onclick="f_onValidateRequiredFields();" runat="server" Text="Other" CssClass="normal" TabIndex=60 />
    </span>
    <asp:customvalidator id=sProdFilterDueValidator runat="server" ClientValidationFunction="f_validatesProdFilterDue" />
	<br />
	<br />
	
	<label id="sProdFilterFinMethFixedLabel" runat="server">Type</label>
	<span style="display:inline-block; width:8em">
    <asp:CheckBox id="sProdFilterFinMethFixed" onclick="f_onValidateRequiredFields();" runat="server" Text="Fixed" CssClass="normal nomargin" TabIndex=61 />
    <asp:customvalidator id=sProdFilterFinMethValidator runat="server" ClientValidationFunction="f_validatesProdFilterFinMeth"></asp:customvalidator>    
    </span>    
    
    <span style="display:inline-block; width:8em">
    <asp:CheckBox id="sProdFilterFinMeth3YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="3yr&nbsp;ARM" CssClass="normal nomargin" TabIndex=62 />
    </span>
    
    <span style="display:inline-block; width:10em">
    <asp:CheckBox id="sProdFilterFinMeth5YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="5yr&nbsp;ARM" CssClass="normal nomargin" TabIndex=63 />
    </span>    
    <label>&nbsp;</label>
    
    <span style="display:inline-block; width:8em">
    <asp:CheckBox id="sProdFilterFinMeth7YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="7yr&nbsp;ARM" CssClass="normal nomargin" TabIndex=64 />
    </span>
    
    <span style="display:inline-block; width:8em">
    <asp:CheckBox id="sProdFilterFinMeth10YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="10yr&nbsp;ARM" CssClass="normal nomargin" TabIndex=65 />
    </span>
    
    <span style="display:inline-block; width:10em">
    <asp:CheckBox id="sProdFilterFinMethOther" onclick="f_onValidateRequiredFields();" runat="server" Text="Other" CssClass="normal nomargin" TabIndex=66 />
    </span>
	<br />
	<br />

    <label id="PaymentTypeLabel" runat="server">PaymentType</label>

    <span style="display:inline-block; width:7em">
	<asp:CheckBox id="sProdFilterPmtTPI" onclick="f_onValidateRequiredFields();" runat="server" Text="P&I" CssClass="normal" TabIndex=67 />
	</span>

    <span style="display:inline-block; width:7em">
	<asp:CheckBox id="sProdFilterPmtTIOnly" onclick="f_onValidateRequiredFields();" runat="server" Text="I/O" CssClass="normal" TabIndex=68 />
	</span>

    <asp:customvalidator id=sProdFilterPaymentTypeValidator runat="server" ClientValidationFunction="f_validatesProdFilterPaymentType"></asp:customvalidator>

    <br />
	<br />
	
    <div id="BestPricingOptionPanel" runat="server">
      <label>Display Best Prices <br />per Program?</label>
      <asp:CheckBox ID="BestPricingCb" Runat="server" TabIndex="69" />&nbsp;Yes
    </div>
    <div id="RegularPricingOptionPanel" runat="server">
      <label>Sort pricing results by</label>
      <asp:RadioButtonList id="resultSort" CellPadding="0" CellSpacing="0" RepeatDirection="Vertical" runat="server" CssClass="normal nomargin" TabIndex="70" />
    </div>
    
    

</fieldset>
	
</div>

	<div style="z-index:300; display:none;border:2px black solid;background-color:gainsboro" id="contentIframe">
	  <table width="100%" height="100%" cellpadding=0 cellspacing=0>
	    <tr><td valign=center align=center style="font-weight:bold;color:black;font-size:14px;">
		  <div id="WaitingMessage"></div>
	    </td></tr>
	  </table>
	</div>	

