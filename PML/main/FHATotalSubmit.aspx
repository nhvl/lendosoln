﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHATotalSubmit.aspx.cs" Inherits="PriceMyLoan.main.FHATotalSubmit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <script type="text/javascript" src="../inc/utilities.js" ></script>

    <style type="text/css" >
    p, a,img, ol, ul, li,
    form, label, 
    table, caption, tbody, tfoot, thead, tr, th, td {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
    }
   .nav { width:100%;  text-align: center; margin-top: 10px;}
   .nav input { margin: 0 auto;   }
   li { padding-bottom: 3px; display: block; overflow: auto;  clear: both; line-height: 20px; }
   li input { display: block; }
   body { background-color: #FFF; width: 95%;}
   li label { line-height: 20px; }
   
   ul{ list-style : none; overflow: auto; display: block; } 
   li label,  li .label { display: block;   float:left; clear:left; text-align: left; }
   #refinanceinfo li label,  #refinanceinfo li .label  { width: 170px; }
   #unit34 li label,  #unit34 li .label  { width: 345px; }
   #RateInfoList li label,  #RateInfoList  li .label  { width: 120px; text-align: left;}
   #SeconfinList li label , #SeconfinList li input { float:left; clear: none; display: inline; }
   #SeconfinList li input { margin-left: 10px; }
   .lefty  { float: left; display: block; }
   .paddingbottom { padding-bottom: 5px; overflow: auto; }
   img { margin-top: 4px; padding-left: 2px; }
   .Validation { display: none; }
   .alabel { display: block; float: left; width: 120px; }
   select { _margin-left: -3px; }
   fieldset { 
       margin: 6px;
       border-top : solid 1px #ff9933;
       border-bottom: none;
       border-right: none;
       border-left: none;
   }
   h2, legend.MainHeader
   {
    font-family:verdana,georgia, sans-serif;
    font-weight:bold; 
    color:#ff9933;  
    padding:3px;
   }
   h2 
   {
    margin: 0; 
       font-size:16px; 
       margin-bottom:5px; 
       padding-left:10px;
   }
   legend.MainHeader
   {
    font-size:14px; 
    margin-bottom:10px; 
   }
   #mainbody 
   {
        font-family : Verdana, Arial, Helvetica, sans-serif;
        font-size : 11px;
        line-height: normal;
        color: #666666;
        font-weight:normal; 
   }
   .divPopup
   {
        display: none;
        border: black 3px solid;
        padding: 5px;
        position: absolute;
        width: 420px;
        height: 120px;
        font-weight:normal;
        background-color: whitesmoke;
        font-family : Arial, Helvetica, sans-serif;
        font-size:11px;
   }
    
    .lar label, .lar input, .lar span { display: block; float: left; margin: 0;}
    .lar input { border:0px; height:14px;   padding: 0 0 0 5px;}
    .lar { overflow: hidden; margin: 0; padding: 0;}
    .chbfix, .chbfix input {  padding: 0; margin: 0 3px 0 0;  width:15px;}
    table#mortgageLiabilities { border:solid 0px #BCBCBC; clear:both; margin-left:30px; margin-top:20px; }
    table#mortgageLiabilities th { color:Black; font-weight:bold; background-color:#e2e2e2; padding-top: 2px; padding-right: 0px; padding-bottom: 2px; padding-left: 5px; }
    table#mortgageLiabilities  th { border-top:solid 1px #BCBCBC; border-bottom:solid 1px #BCBCBC; text-align:left;}
    table#mortgageLiabilities  td { border-bottom:solid 1px #BCBCBC; text-align:left;}
    table#mortgageLiabilities .col1 { text-align:center; padding-left:15px; padding-right:15px; }
    table#mortgageLiabilities th.col2 { padding-left:10px; width:240px; }
    table#mortgageLiabilities td.col2 { padding-left:20px; width:240px; }
    table#mortgageLiabilities th.col3 { text-align:left; padding-left:0px;  }
    table#mortgageLiabilities td.col3 { text-align:right; padding-left:10px; }
    table#mortgageLiabilities .col4 { padding-left:30px; padding-right:10px; }
    table#mortgageLiabilities input { padding: 0; margin: 0; margin-right: 4px; }
    table#mortgageLiabilities label { position: relative; top: -2px;}
    #GiftFundAssets {  border-collapse:collapse; border:1px solid #eee; table-layout: fixed;}
    #GiftFundAssets tr td {  border:1px solid #eee; border-right:1px solid #ddd; padding:1px; }
 
    </style>
    <title>FHA Total Scorecard Submit Form</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="NumberOfApplications" />
    <asp:HiddenField runat="server" ID="BorrowersClientId" />
    <h2>Submit to FHA TOTAL Scorecard</h2>
    <div id="mainbody" style="width:780px">

        <asp:PlaceHolder runat="server" ID="RefinanceInformation">
        <fieldset >
            <legend class="MainHeader">Refinance Information</legend>
            <div style="padding-left: 10px; padding-bottom: 10px">
             <ml:EncodedLabel runat="server" ID="RefinanceTypeLabel" AssociatedControlID="sTotalScoreRefiT" CssClass="alabel" Text="Refinance Type" />
                <asp:DropDownList CssClass="lefty" onchange="FHASubmitForm.ValidatePage();" runat="server" ID="sTotalScoreRefiT"></asp:DropDownList> 
                <img alt="Required" class="Validation lefty" src="../images/error_pointer.gif" id="RefinanceTypeR" />
                <div id="RefinanceTypeInvalidMessage" class="Validation lefty" style="color: red; padding-left: 5px;">This refinance type is not accepted by TOTAL Scorecard, please select an appropriate type.</div>
                <br style="clear:both;" />
                <br />
                <ml:EncodedLabel ID="Label5" runat="server" CssClass="alabel paddingbottom" Text="Mortgage Liabilities"></ml:EncodedLabel>
                <asp:Repeater runat="server" ID="MortgageLiabilitiesRepeater" OnItemDataBound="MortgageLiabilitiesRepeater_OnItemDataBound">
                    <HeaderTemplate> 
                        <table id="mortgageLiabilities" cellpadding="0" cellspacing="0" border="0" >
                            <thead>
                                <tr>
                                    <th class="col1">Subject prop <br />1st mortgage</th>
                                    <th class="col2">Creditor name</th>
                                    <th class="col3">Balance</th>
                                    <th class="col4">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr runat="server" id="row">
                            <td class="col1" style="text-align:center;">
                                <input type="radio" runat="server"  id="SubjProp1stMort" name="SubjProp1stMort" />
                                <asp:HiddenField runat="server" ID="MortLiabIdentifier"  />
                            </td>
                            <td class="col2">
                                <ml:EncodedLabel runat="server" ID="sCredNm" /> &nbsp;
                            </td>
                            <td class="col3">
                                <ml:EncodedLabel runat="server" ID="balance" />
                            </td>
                            <td class="col4">
                                <span>
                                    <asp:RadioButtonList runat="server" ID="PropStatus" RepeatLayout="Flow">
                                        <asp:ListItem  Value="SubjectProperty" Text="Subject Property" />
                                        <asp:ListItem  Value="OtherProperty" Text="Other Property" />
                                    </asp:RadioButtonList>
                                </span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>

            </div>
        </fieldset>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="ThreeFourUnitProperties" >
            <fieldset>
                <legend class="MainHeader">3-4 Unit Properties</legend>
                <div style="padding-left: 10px; padding-bottom: 10px" >
                <ul id="unit34">
                    <li>
                        <ml:EncodedLabel ID="Label1" runat="server"  AssociatedControlID="sTotalScoreAppraisedFairMarketRent"> Appraiser's estimate of fair market rent from all units?</ml:EncodedLabel>
                        <ml:MoneyTextBox runat="server" onchange="FHASubmitForm.ValidatePage(); FHASubmitForm.RefreshRentalIncome();" onkeyup="FHASubmitForm.ValidatePage();"   Width="75px" CssClass="lefty" ID="sTotalScoreAppraisedFairMarketRent" preset="money" ></ml:MoneyTextBox>
                        <img alt="Require" class="lefty Validation" src="../images/error_pointer.gif" id="AppraiserEstimateRequire" />
                    </li>
                    <li>
                        <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="sTotalScoreVacancyFactor">  Occupancy Rate - lower of appraisal or jurisdictional HOC </ml:EncodedLabel>
                        <ml:PercentTextBox runat="server" onchange="FHASubmitForm.ValidatePage(); FHASubmitForm.RefreshRentalIncome();" onkeyup="FHASubmitForm.ValidatePage();" ID="sTotalScoreVacancyFactor"  Width="75px" style="text-align: right;"  CssClass="lefty" preset="percent" ></ml:PercentTextBox></asp:TextBox>
                        <img alt="Require" class=" Validation lefty" src="../images/error_pointer.gif" id="VacancyR" />
                        <img alt="Error" class="lefty Validation" id="VacancyE"  src="../images/error_pointer.gif"/>
                    </li>
                    <li>
                        <ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="sTotalScoreNetRentalIncome">  Net rental income</ml:EncodedLabel>
                        <ml:MoneyTextBox runat="server" ReadOnly="true"  Width="75px" CssClass="lefty" ID="sTotalScoreNetRentalIncome" preset="money" ></ml:MoneyTextBox>
                    </li>
                </ul>
                </div>
            </fieldset>
          </asp:PlaceHolder>
        <fieldset>
            <legend class="MainHeader">Loan Information</legend>
            <div style="padding:0px 10px 0px 10px; overflow:auto;" >
                <div style="clear: both; overflow: auto; " class="lar">
                <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="sTotalScoreFhaProductT"  CssClass="alabel lefty"> FHA Program </ml:EncodedLabel>
                <asp:DropDownList style="margin-left:0px;" onchange="FHASubmitForm.ValidatePage();" runat="server" ID="sTotalScoreFhaProductT" CssClass="lefty">
                </asp:DropDownList>
                <img alt="Required" class=" lefty Validation" src="../images/error_pointer.gif" id="FhaProgramR" />
                </div>
                <div style="clear: both; overflow: auto; margin-top: 6px; " class="lar">
                    <table>
                        <tr>
                            <td style="width:300px;"><span class="lefty" >Is loan an identity of interest transaction?&nbsp;&nbsp&nbsp;&nbsp</span> </td>
                            <td style="width:110px;">
                                <asp:RadioButtonList onclick="FHASubmitForm.ValidatePage();" runat="server" RepeatLayout="Flow" CssClass="lefty" RepeatDirection="Horizontal" ID="sTotalScoreIsIdentityOfInterest">
                                    <asp:ListItem Text="Yes" Value="true" />
                                    <asp:ListItem Text="No" Value="false" />
                                </asp:RadioButtonList>
                                <img alt="Required" src="../images/error_pointer.gif" style="margin-top: 0px;" id="LoanIdentityR" class="lefty" />
                                
                            </td>
                            <td>
                                <span class="lefty">(<a id="IdentityTransactionExplainationLink" href="#" >Explain</a>)</span>
                            </td>
                        </tr>
                        <tr id="IdentityOfInterestExceptionPanel" style="">
                            <td style="padding-left:10px;padding-top:6px;">
                                <span class="lefty" >Does the transaction fall under an exception?&nbsp;&nbsp&nbsp;&nbsp</span> 
                            </td>
                            <td style="padding-top:6px;">
                                <asp:RadioButtonList onclick="FHASubmitForm.ValidatePage();" runat="server" RepeatLayout="Flow" CssClass="lefty" RepeatDirection="Horizontal" ID="sTotalScoreIsTransactionException">
                                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                </asp:RadioButtonList>
                                <img alt="Required" src="../images/error_pointer.gif" style="margin-top: 0px;" id="TransExceptionR" class="lefty" />
                            </td>
                            <td style="padding-top:6px;">
                                <span class="lefty">(<a id="IdentityExceptionExplanationLink" href="#" >Explain</a>)</span>
                            </td>
                        </tr>
                    </table>
                </div>
              
            <div id="RateInfo" style="display: block; clear:both; float: left;  width: 250px; margin-top: 10px; padding-right:60px; ">
                <ul id="RateInfoList">
                    <li>
                        <ml:EncodedLabel runat="server" ID="RateLabel" AssociatedControlID="sNoteIR" Text="Rate" />
                        <ml:PercentTextBox onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();" runat="server" ID="sNoteIR"   Width="60px" style="text-align: left;"  CssClass="lefty" preset="percent" />
                        <img class="lefty Validation" alt="Required"  src="../images/error_pointer.gif" id="RateR" />
                    </li>
                    <li>
                        <ml:EncodedLabel runat="server" ID="TermLabel" AssociatedControlID="sTerm" Text="Term" />
                        <asp:TextBox  onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();" runat="server" Width="35px" CssClass="lefty" ID="sTerm" />
                        <img alt="Error" class="lefty Validation" src="../images/error_pointer.gif" id="TermE" />
                    </li>
                    <li>
                        <ml:EncodedLabel runat="server" ID="ArmortLabel" AssociatedControlID="sFinMethT" Text="Amortization type" />
                        <asp:DropDownList onchange="FHASubmitForm.CheckFinMeth();FHASubmitForm.ValidatePage();"  runat="server" ID="sFinMethT" />
                    </li>
                    <li id="ArmSetting">
                        <ml:EncodedLabel runat="server" ID="FirstChangeLabel" AssociatedControlID="sRAdj1stCapMon" Text="1st Change" />
                        <asp:TextBox onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();" style="float:left; " runat="server" ID="sRAdj1stCapMon" Width="50px"></asp:TextBox> <span style="display: block; float:left" >mths</span>
                        <img alt="Error" class="lefty Validation" src="../images/error_pointer.gif" id="ArmError" />
                    </li>
                </ul>
            </div>
            <div id="Seconfin" style="display: block; float: left; margin-top: 10px " runat="server" >
                <ul id="SeconfinList">
                    <li style="padding-bottom: 5px">
                        <ml:EncodedLabel runat="server" ID="SecondaryFinancingSourceLabel"  CssClass="lefty">Secondary financing source</ml:EncodedLabel>
                         <img  class="lefty Validation"  style="margin-top: 0" alt="Required" src="../images/error_pointer.gif" id="SecR" />
                    </li>
                    <li>
                        <asp:RadioButton runat="server"  onclick="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();"  ID="sFHASecondaryFinancingIsGov" GroupName="SecondaryFinancingSource"
                            Text="Government" />
                    </li>
                    <li>
                        <asp:RadioButton runat="server"  onclick="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();"  ID="sFHASecondaryFinancingIsNP" GroupName="SecondaryFinancingSource"
                            Text="Non-Profit" />
                    </li>
                    <li>
                        <asp:RadioButton runat="server"  onclick="FHASubmitForm.ValidatePage();"  onkeyup="FHASubmitForm.ValidatePage();" ID="sFHASecondaryFinancingIsFamily" GroupName="SecondaryFinancingSource"
                            Text="Family" />
                    </li>
                    <li>
                        <asp:RadioButton runat="server"  onclick="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();"  ID="sFHASecondaryFinancingIsOther" GroupName="SecondaryFinancingSource"
                            Text="Other" />
                        <asp:TextBox runat="server" onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();" ID="sFHASecondaryFinancingOtherDesc"> </asp:TextBox>
                    </li>
                </ul>
        </div>
        </div>
        <br style="clear: both; "/>
        </fieldset>
        <fieldset id="BorrowerInfoFieldSet"  >
            <legend class="MainHeader">Borrower Information</legend>
            <asp:Repeater runat="server" ID="Borrowers" OnItemDataBound="Borrowers_OnItemDataBound">
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="BorrowerApplicationId" />
                    <asp:HiddenField runat="server" ID="HasCoborrower" />
                    <div style="display: block; float: left; width: 150px;padding: 10px; padding-right:0px;">
                        <%# AspxTools.HtmlString(((DataAccess.CAppData)Container.DataItem).aBLastFirstNm) %>
                    </div>
                    <div style="display: block; float: left;margin-top:5px;">
                        <table cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox runat="server" ID="aBTotalScoreIsCAIVRSAuthClear" CssClass="chbfix" Text="CAIVRS authorization is clear" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:5px;">
                                    <ml:EncodedLabel ID="Label7" runat="server" AssociatedControlID="aFHABCaivrsNum"> CAIVRS # </ml:EncodedLabel><asp:TextBox
                                        runat="server" ID="aFHABCaivrsNum"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="lar" style="padding-top:8px;">
                                <span class="lefty">
                                    <a   onclick="return Dialog.openUrl('https://www5.hud.gov/ecpcis/main/ECPCIS_List.jsp', 'LDP', 800, 600 );">LDP</a>/<a href="javascript:void(0)"  onclick="return Dialog.openUrl('https://www.sam.gov/', 'LDP', 400, 500 );" >GSA</a>
                                    </span>
                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                        ID="aFHABLdpGsaTri">
                                        <asp:ListItem Text="Yes" Value="1"> </asp:ListItem>
                                        <asp:ListItem Text="No" Value="2"> </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </div> 
                    
                    <div style="display: block; float: left; margin-left: 10px;margin-top:5px;">
                        <table cellspacing="0">
                            <tr>
                                <td>
                                   <input type="checkbox" class="chbfix" style="margin-right:0;" runat="server" ID="aBTotalScoreIsFthb" onclick="FHASubmitForm.ValidatePage();"  onchange="FHASubmitForm.ValidatePage();" /> 
                                   <ml:EncodedLabel runat="server" AssociatedControlID="aBTotalScoreIsFthb">Is first time home buyer?</ml:EncodedLabel>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="padding-top:5px;">
                                    <ml:EncodedLabel ID="aBTotalScoreFhtbCounselingTLabel" runat="server" AssociatedControlID="aBTotalScoreFhtbCounselingT">FTHB counseling completed</ml:EncodedLabel>&nbsp;
                                    <asp:DropDownList style="margin-left:0px;" runat="server" ID="aBTotalScoreFhtbCounselingT" onchange="FHASubmitForm.ValidatePage();" >

                                    </asp:DropDownList>
                                    <asp:Image style="margin-top:0px; margin-left:-3px;" runat="server" id="BFTHBR" alt="Required" src="../images/error_pointer.gif" />
                                </td>
                            </tr>
                        </table>
                    </div>
                       <asp:PlaceHolder runat="server" ID="CoborrowerFields">
                    <br style="clear: both;">
                    <br />
                  
                    <div style="display: block; float: left; width: 150px; padding: 10px;padding-right:0px;">
                        <%# AspxTools.HtmlString(((DataAccess.CAppData)Container.DataItem).aCLastFirstNm) %>
                    </div>
                    <div style="display: block; float: left;margin-top:5px;">
                        <table cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox runat="server" ID="aCTotalScoreIsCAIVRSAuthClear" CssClass="chbfix " Text="CAIVRS authorization is clear" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:5px;">
                                    <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="aFHACCaivrsNum"> CAIVRS # </ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="aFHACCaivrsNum"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td  class="lar" style="padding-top:8px;">
                                <span class="lefty">
                                    <a onclick="return Dialog.openUrl('https://www5.hud.gov/ecpcis/main/ECPCIS_List.jsp', 'LDP', 800, 600 );">LDP</a>/<a  onclick="return Dialog.openUrl('https://www.sam.gov/', 'LDP', 400, 500 );" >GSA</a>
                                    </span>
                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                        ID="aFHACLdpGsaTri">
                                        <asp:ListItem Text="Yes" Value="1"> </asp:ListItem>
                                        <asp:ListItem Text="No" Value="2"> </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="display: block; float: left; margin-left: 10px; margin-top:5px;">
                        <table cellspacing="0">
                            <tr>
                                <td>
                                <input type="checkbox" runat="server" ID="aCTotalScoreIsFthb" onclick="FHASubmitForm.ValidatePage();" class="chbfix" onchange="FHASubmitForm.ValidatePage();" />
                                <ml:EncodedLabel runat="server"  AssociatedControlID="aCTotalScoreIsFthb">Is first time home buyer? </ml:EncodedLabel>

                                </td>
                            </tr>
                            <tr>
                                <td height="20"  style="padding-top:5px;">
                                    <ml:EncodedLabel ID="aCTotalScoreFhtbCounselingTLabel" runat="server" AssociatedControlID="aCTotalScoreFhtbCounselingT" >FTHB counseling completed</ml:EncodedLabel>&nbsp;
                                    <asp:DropDownList runat="server" style="margin-left:0px;"  ID="aCTotalScoreFhtbCounselingT"  onchange="FHASubmitForm.ValidatePage();" ></asp:DropDownList>
                                   <asp:Image style="margin-top:0px;" runat="server" id="CFTHBR" src="../images/error_pointer.gif" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    </asp:PlaceHolder>
                </ItemTemplate>
                <SeparatorTemplate>
                    <hr style="clear: both;" />
                </SeparatorTemplate>
            </asp:Repeater>
        </fieldset>
        
        <fieldset runat="server" id="GiftFundPanel" visible="false" >
           <span class="span-legend">Gift Funds</span>
            <asp:Repeater runat="server" ID="GiftFundAssets" OnItemDataBound="GiftFundAssets_OnItemDataBound">
                <HeaderTemplate>
                    <table cellpadding="0" id="GiftFundAssets" cellspacing="0" width="99%" >
                        <thead>
                            <tr class="GridHeader">
                                <th>Borrower</th>
                                <th>Description</th>
                                <th style="width: 100px">Amount</th>
                                <th style="width: 120px">Source</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
                <ItemTemplate>
                    <tr class="GridItem">
                        <td><ml:EncodedLiteral runat="server" ID="BorrowerName"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral runat="server" id="Description"></ml:EncodedLiteral></td>
                        <td align="right"><ml:EncodedLiteral runat="server" ID="Amount"></ml:EncodedLiteral></td>
                        <td><asp:DropDownList runat="server" ID="Source"></asp:DropDownList>
                         <asp:RegularExpressionValidator ValidationGroup="Assets"  ValidationExpression="[1-9]" runat="server" ControlToValidate="Source" ID="Validator"/>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="AlternatingGridItem">
                        <td><ml:EncodedLiteral runat="server" ID="BorrowerName"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral></td>
                        <td align="right" ><ml:EncodedLiteral runat="server" ID="Amount"></ml:EncodedLiteral></td>
                        <td>
                            <asp:DropDownList runat="server" ID="Source"></asp:DropDownList>
                            <asp:RegularExpressionValidator  ValidationGroup="Assets" ValidationExpression="[1-9]" runat="server" ControlToValidate="Source" ID="Validator"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
            </asp:Repeater>
            <div style="width: 382px; float: left; margin: 5px 0px; line-height: 2">
            Primary Gift Funds Source: <ml:EncodedLabel runat="server" ID="sPrimaryGiftFundSource"></ml:EncodedLabel>  
            </div>
            <div style="float: left; margin: 5px; 0px; line-height: 2">
            Total from all sources:  <asp:TextBox runat="server" ReadOnly="true"  style="text-align: right; font-weight: bold;"   ID="sTotGiftFundAsset" Width="100px"></asp:TextBox>
            </div>
            <br style="clear:both;" />
        </fieldset>
        
        <fieldset>
            <legend class="MainHeader">Submission Information</legend>

            <div id="SubmissionIdDiv" runat="server" style="margin-bottom: 5px" >
                    <div runat="server" id="LenderIdDiv" style=" overflow: auto; line-height:3;" >
                        <input type="radio" group="SChoice" id="sFhaLenderIdT_RegularFhaLender" onclick="FHASubmitForm.ValidatePage();" name="sFhaLenderIdT" runat="server"  value="0" checked="true" />
                        <ml:EncodedLabel ID="Label6" runat="server"  AssociatedControlID="sFHALenderIdCode"> Lender ID</ml:EncodedLabel>
                       
                        <asp:TextBox runat="server" style="margin-left: 80px"  ID="sFHALenderIdCode" onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" Display="Static" ID="sFHALenderIdCodeRequired" ControlToValidate="sFHALenderIdCode" ValidationGroup="LenderId"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server"  ID="sFHALenderIdIs10DigitNumber" ControlToValidate="sFHALenderIdCode" ValidationGroup="LenderId" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                
                        <ml:EncodedLabel ID="Label9" runat="server" AssociatedControlID="sFHASponsorAgentIdCode">Sponsor ID</ml:EncodedLabel>
                        <asp:TextBox runat="server" ID="sFHASponsorAgentIdCode" onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();"> </asp:TextBox>
                        <asp:RegularExpressionValidator runat="server"  ID="sFHASponsorAgentIdCodeIs10Digits" ValidationExpression="[0-9]{10}" ControlToValidate="sFHASponsorAgentIdCode" ValidationGroup="SponsorId" ErrorMessage="">Sponsor id must be 10 digits.</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator runat="server" ID="sFHASponsorAgentIdCodeIsRequired"   ValidationGroup="SponsorId" ControlToValidate="sFHASponsorAgentIdCode" ></asp:RequiredFieldValidator>
                    </div>
                    <div style="overflow: auto; line-height:3">
                        <input type="radio" group="SChoice" id="sFhaLenderIdT_SponsoredOriginatorEIN" name="sFhaLenderIdT" value="1" onclick="FHASubmitForm.ValidatePage();" runat="server" />
                        <ml:EncodedLabel ID="Label10" runat="server" AssociatedControlID="sFHASponsoredOriginatorEIN" >Sponsored Originator EIN</ml:EncodedLabel>
                        <asp:TextBox runat="server" ID="sFHASponsoredOriginatorEIN" onkeyup="FHASubmitForm.ValidatePage();"></asp:TextBox>
                        <asp:RequiredFieldValidator  runat="server" ID="sFHASponsoredOriginatorEINRequired" ControlToValidate="sFHASponsoredOriginatorEIN" ValidationGroup="sFHASponsoredOriginatorEIN" />
                        <asp:RegularExpressionValidator  runat="server" ID="sFHASponsoredOriginatorEINIs9Digits" ControlToValidate="sFHASponsoredOriginatorEIN"  ValidationGroup="sFHASponsoredOriginatorEIN" ValidationExpression="[0-9]{9}"></asp:RegularExpressionValidator>
                    </div>
                    <div style=" overflow: auto; line-height:3">
                        <input type="radio" runat="server" id="sFhaLenderIdT_NoOriginatorId" name="sFhaLenderIdT" value="2" onclick="FHASubmitForm.ValidatePage();" /> No Originator Id
                    </div>
                 
                </div>
                <div style="padding-bottom: 5px; display: block; overflow: auto; clear: both; line-height: 2">
                    <ml:EncodedLabel ID="Label11" runat="server" Style="display: block; width: 120px; float: left;" AssociatedControlID="sAgencyCaseNum">FHA Case Number</ml:EncodedLabel>
                    <asp:TextBox runat="server" Width="200px" onchange="FHASubmitForm.ValidatePage();"
                        onkeyup="FHASubmitForm.ValidatePage();" ID="sAgencyCaseNum" CssClass="lefty"></asp:TextBox>
                    <img class="lefty Validation" alt="Error" src="../images/error_pointer.gif" id="AgencyE" />
                </div>
                
                <div style="clear:both; display: block; margin-top: 5px; overflow:auto; ">
                    <span style="padding-bottom: 5px;">Upload data from the following forms into CHUMS/FHA Connection</span>
                    <asp:CheckBox runat="server" Style="padding-left: 10px; padding-top:5px; margin-top: 1px; display: block; " ID="sTotalScoreUploadData1003"
                        Text="Loan 1003 (URLA)" Enabled="false" />
                </div>
            </div>
        </fieldset>
        <div class="nav">
        <input type="button" class="ButtonStyle" id="Submit" NoHighlight value="Submit to TOTAL Scorecard" onclick="FHASubmitForm.SubmitUI(this)" />
        <input type="button" class="ButtonStyle" id="BtnCancel" value="Cancel" NoHighlight onclick="FHASubmitForm.cancel()" style="margin-left: 10px" runat="server"/>
        </div>
    </div>
    <div id="IdentityTransactionExplaination" style="z-index:900;" class="divPopup">
            
        An identity-of-interest transaction is a sales transaction between parties with a family, business relationship, or other business affiliates, for the purchase of a principal residence.
        <br />
        <br />
        Note: An identify-of-interest transaction does not include an employer/employee transaction when purchasing the seller's principal residence. (HUD 4155.1 9.1.a)
        <br />
        <div class="nav" >
        [<a onclick="return hidePopup();" >close</a>]
        </div>
    </div>
    <div id="IdentityExceptionExplanation" style="z-index:900;width: 520px; height: 360px;" class="divPopup">
        Identity-of-interest transactions on principal residences are restricted to a maximum LTV ratio of <i>85 percent</i>. However, maximum financing above 85 percent LTV is permissible under the following circumstances: 
        <br />
        <br />
        <b>Family Member Purchase</b><br />
        A family member purchases another family member’s home as a principal residence. For a definition of the term “family member,” see HUD 4155.1 9. If the property is sold from one family member to another and is the seller’s investment property, this exception does not apply (the Tenant Purchase exception may apply, however. See below).<br />
        <br />
        <b>Builder’s Employee Purchase</b><br />
        An employee of a builder purchases one of the builder’s new homes or models as a principal residence.<br />
        <br />
        <b>Tenant Purchase</b><br />
        A current tenant, including a family member tenant, purchases the property where he/she has rented for at least six months immediately predating the sales contract.
        <i>Note</i>: A lease or other written evidence to verify occupancy is required.<br />
        The maximum mortgage calculation is not affected by a sales transaction between a tenant and a landlord with no identity of interest relationship.<br />
        <br />
        <b>Corporate Transfer</b><br />
        A corporation<br />
        <span style="margin-left:15px;">- transfers an employee to another location</span><br />
        <span style="margin-left:15px;">- purchases the employee’s home, and</span><br />
        <span style="margin-left:15px;">- sells the home to another employee.</span><br />
        <div class="nav" >
        [<a href="#" onclick="return hidePopup();" >close</a>]
        </div>
    </div>
    <div id="WaitMsg" style="position:absolute;  z-index: 900; top: 55%; left: 30%; font-weight:normal; BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; WIDTH: 190px; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; HEIGHT: 40px; BACKGROUND-COLOR: whitesmoke"> 
        <b>Please wait...</b>
        <br />
        <img src="../images/status.gif" alt="loading" />
    </div>
    <script type="text/javascript">
      $j(function()
      {
        $j('#IdentityTransactionExplainationLink').on('click', { popupId : '#IdentityTransactionExplaination'}, displayPopup);
        $j('#IdentityExceptionExplanationLink').on('click', { popupId : '#IdentityExceptionExplanation'}, displayPopup);
        
        $j('input[type=radio]').filter('[id$="SubjProp1stMort"]').on('click', onSubjectPropRadioClick);
      });
      function onSubjectPropRadioClick(event)
      {
        $j('input[type=radio]').filter('[id$="SubjProp1stMort"]').prop('checked', false);
        $j('input[type=radio]').filter('[name$="PropStatus"]').prop('disabled', false);
        $j(event.target).prop('checked', true);
        var sel = '[id^="'+ $j(event.target).attr('cname')+'"]';
        $j('input[type=radio]').filter(sel).prop('disabled', true);
      }
      var g_currentPopupIdSel = null;
        function displayPopup(event)
        {
          var popupIdSel = event.data.popupId;
          var $popup = $j(popupIdSel);
          var top = event.pageY - $popup.height();
          if(top < $j('body').offset().top){
            top = $j('body').offset().top;
          }
          var left = event.pageX - ($popup.width() / 2);
          showPopup(popupIdSel, top, left);
        
        }
      
        function showPopup(idSel, top, left)
        {
          hidePopup();

          $j(idSel).css({top:top, left:left}).show();
          g_currentPopupIdSel = idSel;

        }
        function hidePopup()
        {
          $j(g_currentPopupIdSel).hide();
          g_currentPopupIdSel = null;
          return false;
        }  
        var FHASubmitForm = {   
            cancel : function()  {
                parent.LQBPopup.Return({OK:false});
            },
            init : function() {
                FHASubmitForm.CheckFinMeth();
                FHASubmitForm.ValidatePage();   
                if (typeof(Page_ClientValidate) == 'function') {
                    Page_ClientValidate();
                }
            },
            
            CheckFinMeth : function() {
                var firstChange = document.getElementById('ArmSetting');
                if (<%= AspxTools.JQuery(sFinMethT) %>.val() !== "1" ) {
                   
                    firstChange.style.display = 'none';  
                    return true;
                } 
                else {
                    firstChange.style.display = '';     
                    return firstChange.value != '0'; 
                }
            },
            UpdateLenderIdSection : function(){
                if (document.getElementById('LenderIdDiv') == null)
                    return;
                    
                var sFhaLenderIdT_RegularFhaLender = document.getElementById('sFhaLenderIdT_RegularFhaLender'),
                    sFhaLenderIdT_SponsoredOriginatorEIN = document.getElementById('sFhaLenderIdT_SponsoredOriginatorEIN'),
                    sFhaLenderIdT_NoOriginatorId = document.getElementById('sFhaLenderIdT_NoOriginatorId'),
                    sFHALenderIdCode = document.getElementById('sFHALenderIdCode'),
                    SubmissionIdDiv = document.getElementById('SubmissionIdDiv'),
                    sFHALenderIdCode = document.getElementById('sFHALenderIdCode'),
                    sFHALenderIdCodeRequired = document.getElementById('sFHALenderIdCodeRequired'),
                    sFHALenderIdIs10DigitNumber = document.getElementById('sFHALenderIdIs10DigitNumber'),
                    sFHASponsoredOriginatorEINIs9Digits = document.getElementById('sFHASponsoredOriginatorEINIs9Digits'),
                    sFHASponsorAgentIdCodeIs10Digits = document.getElementById('sFHASponsorAgentIdCodeIs10Digits'),
                    sFHASponsorAgentIdCodeIsRequired = document.getElementById('sFHASponsorAgentIdCodeIsRequired'),
                    sFHASponsoredOriginatorEINRequired = document.getElementById('sFHASponsoredOriginatorEINRequired'),
                    sFHASponsoredOriginatorEIN = document.getElementById('sFHASponsoredOriginatorEIN');
                    
                    
                if(!SubmissionIdDiv ){
                    return; //nothing to do
                }
                
                
                if(sFhaLenderIdT_RegularFhaLender.checked){
               
                    $j(sFHALenderIdCode).readOnly(false);
                    sFHASponsoredOriginatorEIN.value = '';
                    $j(sFHASponsoredOriginatorEIN).readOnly(true);
                    ValidatorEnable(sFHALenderIdCodeRequired, true);
                    ValidatorEnable(sFHALenderIdIs10DigitNumber, true);
                    ValidatorEnable(sFHASponsorAgentIdCodeIs10Digits, true); 
                    ValidatorEnable(sFHASponsorAgentIdCodeIsRequired, false); 
                    ValidatorEnable(sFHASponsoredOriginatorEINIs9Digits, false);
                    ValidatorEnable(sFHASponsoredOriginatorEINRequired, false);
                    
                }else if( sFhaLenderIdT_SponsoredOriginatorEIN.checked ){
                    $j(sFHALenderIdCode).readOnly(true);
                    sFHALenderIdCode.value = '';    
                    $j(sFHASponsoredOriginatorEIN).readOnly (false);
                    ValidatorEnable(sFHASponsoredOriginatorEINIs9Digits, true);
                    ValidatorEnable(sFHASponsorAgentIdCodeIsRequired, true); 
                    ValidatorEnable(sFHASponsoredOriginatorEINRequired, true);
                    ValidatorEnable(sFHALenderIdCodeRequired, false);
                    ValidatorEnable(sFHALenderIdIs10DigitNumber, false);
                }
                else{
                    $j(sFHALenderIdCode).readOnly (true);
                    $j(sFHASponsoredOriginatorEIN).readOnly ( true);
                    sFHALenderIdCode.value = '';
                    sFHASponsoredOriginatorEIN = '';
                    sFHASponsoredOriginatorEIN.value = '';
                    ValidatorEnable(sFHALenderIdCodeRequired, false);
                    ValidatorEnable(sFHALenderIdIs10DigitNumber, false);
                    ValidatorEnable(sFHASponsorAgentIdCodeIs10Digits, true); 
                    ValidatorEnable(sFHASponsorAgentIdCodeIsRequired, true); 
                    ValidatorEnable(sFHASponsoredOriginatorEINIs9Digits, false);
                    ValidatorEnable(sFHASponsoredOriginatorEINRequired, false);
                }
           
            },
         
            ValidatePage : function() { 
                
                FHASubmitForm.UpdateLenderIdSection();
                
                
                var isPageValid = FHASubmitForm.ValidateRefinanceType(); 
                if (typeof(Page_ClientValidate) == 'function') {
                    isPageValid &= Page_ClientValidate();
                }
                isPageValid &= FHASubmitForm.Validate34UnitInfo();
                isPageValid &= FHASubmitForm.ValidateLoanInfo();
                isPageValid &= FHASubmitForm.ValidateSubmisionInfo();
                isPageValid &= FHASubmitForm.ValidateBorrowerInfo();
                var sbtn = document.getElementById('Submit');
                sbtn.disabled = !isPageValid;
            }, 
            
            ValidateRefinanceType : function() { 
            
                var sTotalScoreRefiT = <%= AspxTools.JQuery(sTotalScoreRefiT) %>;
                if (sTotalScoreRefiT.length == 0)
                {
                    return true;
                }
                var typeIsBlank = sTotalScoreRefiT.val() === <%= AspxTools.JsString(E_sTotalScoreRefiT.LeaveBlank) %>;
                var typeIsInvalid = sTotalScoreRefiT.val() === <%= AspxTools.JsString(E_sTotalScoreRefiT.Unknown) %>;
                $j('#RefinanceTypeR').toggle(typeIsBlank);
                $j("#RefinanceTypeInvalidMessage").toggle(typeIsInvalid);
                return !(typeIsBlank || typeIsInvalid);
            },
            
            Validate34UnitInfo : function() { 
                var unit34Info = document.getElementById('unit34');
                if ( unit34Info == null ) { return true; }
                var isValid = true;
                var appraiserEstimateField = document.getElementById('sTotalScoreAppraisedFairMarketRent');
                var appraiserEstimate = parseFloat(appraiserEstimateField.value.replace(/[$]/, '')); 
                var appraiserEstimateR = document.getElementById('AppraiserEstimateRequire');
                
                var vacancyFactorField = document.getElementById('<%= AspxTools.ClientId(sTotalScoreVacancyFactor) %>');
                var vacancyValue = parseFloat(vacancyFactorField.value.replace(/[%]/, ''));  
                var vacancyR = document.getElementById('VacancyR');   
                var vacancyE = document.getElementById('VacancyE');
                     
                if ( appraiserEstimateField.value === '' ) {
                      isValid = false;
                    appraiserEstimateR.style.display  = 'block'; 
                }  
                else if (isNaN(appraiserEstimate)  || appraiserEstimate <= 0.00) {
                    isValid = false;
                    appraiserEstimateR.style.display  = 'block'; 
                }
                else {
                     appraiserEstimateR.style.display  = 'none'; 
                }
                if ( vacancyFactorField.value === '' ) {
                    isValid = false;
                    vacancyR.style.display  = 'block'; 
                    vacancyE.style.display = 'none';  
                } 
                else if (isNaN(vacancyValue)  || vacancyValue <= 0.00) {
                    isValid = false;
                    vacancyR.style.display  = 'none'; 
                    vacancyE.style.display = 'block';
                } 
                else {
                    vacancyR.style.display  = 'none'; 
                    vacancyE.style.display = 'none';
                }
    
                return isValid;

            }, 
            
            ValidateLoanInfo : function() { 

               var isValid = true;
               if ( <%= AspxTools.JQuery(sTotalScoreFhaProductT) %>.val() === '' )  {
                    $j('#FhaProgramR').show();
                    isValid = false;
               } 
               else {
                  $j('#FhaProgramR').hide();
               }
               
               
               var isIdentChecked = $j('input[name="sTotalScoreIsIdentityOfInterest"]:checked').length > 0;
               var showTransactionException = false;
               if (isIdentChecked == true)
               {
                 showTransactionException = $j('input[name="sTotalScoreIsIdentityOfInterest"]:checked').val() == 'true';
               }
               /*
               var showTransactionException = false;
               for(var i = 0; i < loanIdentity.childNodes.length; i++ ) {
                if ( typeof( loanIdentity.childNodes[i].type ) === 'undefined' )  {
                    continue;
                }
                if ( loanIdentity.childNodes[i].checked ) {
                    isIdentChecked = true;
                    showTransactionException = (loanIdentity.childNodes[i].value == "true");
                    break;
                }
               }
               */
              if ( isIdentChecked )  {
                $j('#LoanIdentityR').hide();
                //loanIdentityR.style.display = 'none';
              } 
              else {
                $j('#LoanIdentityR').show();
                //loanIdentityR.style.display = ''; 
                isValid = false;
              }
              
              //var transException = document.getElementById('<%= AspxTools.ClientId(sTotalScoreIsTransactionException) %>');
              //var transExceptionR = document.getElementById('TransExceptionR'); 
              //var transExceptionPanel = document.getElementById('IdentityOfInterestExceptionPanel');
              //console.log('showTransactionException=' + showTransactionException);
              $j('#IdentityOfInterestExceptionPanel').toggle(showTransactionException);
              //transExceptionPanel.style.display = showTransactionException ? '' : 'none';
              
              if (showTransactionException){
                  //var _v = $j('input[name="sTotalScoreIsTransactionException"]:checked').val();
                  //console.log('_vvv=[' + _v + ']');
                   var isTransChecked = $j('input[name="sTotalScoreIsTransactionException"]:checked').length > 0;
                   /*
                   for(var i = 0; i < transException.childNodes.length; i++ ) {
                    if ( typeof( transException.childNodes[i].type ) === 'undefined' )  {
                        continue;
                    }
                    if ( transException.childNodes[i].checked ) {
                        isTransChecked = true;
                        break;
                    }
                   }
                   */
                  if ( isTransChecked )  {
                    $j('#TransExceptionR').hide();
                  } 
                  else {
                    $j('#TransExceptionR').show(); 
                    isValid = false;
                  }
               }
               else{
                  $j('#TransExceptionR').hide();
                    //transExceptionR.style.display = 'none';
               }

               //var rateField = document.getElementById('<%= AspxTools.ClientId(sNoteIR) %>');
               var rateValue = parseFloat(<%= AspxTools.JQuery(sNoteIR) %>.val().replace(/[%]/, ''));
               //var rateR = document.getElementById('RateR');
               
               //if ( rateField.value === '' ) {
//                    rateR.style.display = 'block';
                    //isValid = false;
               //}
               
               //else 
               if ( isNaN(rateValue) || rateValue <= 0.000  ) {
                $j('#RateR').show();
                    //rateR.style.display = 'block';
                    isValid = false;
               }
               else { 
                $j('#RateR').hide();
                    //rateR.style.display = 'none';
               }
               
               var termField = document.getElementById('<%= AspxTools.ClientId(sTerm) %>');
               var termE = document.getElementById('TermE'); 
               var termV = parseInt(termField.value);
               
               if ( termField.value === '' ) {
                    termE.style.display = 'block';
                    isValid = false;
               } 
               else if ( !termField.value.match(/^[0-9]+$/) || termV <= 0) {
                  
                    termE.style.display = 'block'; 
                    isValid = false;
               }
               else  {
                 termE.style.display = 'none';
                 
               }
               
               var armsetting = document.getElementById('ArmSetting');
              
               if ( armsetting.style.display !== 'none' ) {
                    var firstChange = document.getElementById('<%= AspxTools.ClientId(sRAdj1stCapMon) %>');
                    var firstChangeV = parseInt(firstChange.value);
                    var firstE = document.getElementById('ArmError');
                    if ( firstChange.value === '' ) {
                        isValid = false;
                        firstE.style.display = 'block';
                    } 
                    else if ( !firstChange.value.match(/^[0-9]+$/) || firstChangeV <= 0   ) {
                       
                        isValid = false;
                        firstE.style.display = 'block';
                    }
                    else {
                        firstE.style.display = 'none'; 
                    }
               }
               
               var secondfin = document.getElementById('SeconfinList');
               
               if ( secondfin != null ) {
                    var oneIsChecked = false;
                    var Rsf = document.getElementById('SecR');
                    var govSf = <%= AspxTools.JQuery(sFHASecondaryFinancingIsGov) %>.prop('checked');
                    var npSf = <%= AspxTools.JQuery(sFHASecondaryFinancingIsNP) %>.prop('checked');
                    var fSf = <%= AspxTools.JQuery(sFHASecondaryFinancingIsFamily) %>.prop('checked');
                    var oSf = <%= AspxTools.JQuery(sFHASecondaryFinancingIsOther) %>.prop('checked');
                   
                    oneIsChecked = govSf || npSf || fSf || oSf;
                    if ( oneIsChecked  ) {
                      $j('#SecR').hide();
                        //Rsf.style.display = 'none';
                    }
                    else {
                      $j('#SecR').show();
                        //Rsf.style.display = 'block';
                        isValid = false;
                    }
               }
               

               return isValid;
            },
            
            ValidateSubmisionInfo : function() { 
                var isValid = true;
               var lenderId = document.getElementById('<%= AspxTools.ClientId(sFHALenderIdCode) %>');
               
               var isShowLenderId = ( lenderId != null );
               
               var sponsor = document.getElementById('<%=AspxTools.ClientId(sFHASponsorAgentIdCode)%>');
               
               var agencyCaseN = document.getElementById('<%= AspxTools.ClientId(sAgencyCaseNum) %>');
               var agencyCaseR = document.getElementById('AgencyE');
               
               var chb = document.getElementById('<%= AspxTools.ClientId(sTotalScoreUploadData1003) %>');
               
               
               if ( agencyCaseN.value.trim().length === 0 ) {
                 agencyCaseR.style.display = 'none';    
                 chb.checked = false;
               }
               else if ( agencyCaseN.value.match(/^[0-9]{3}[-][0-9]{7}(-.*)*$/)  )  {
                    agencyCaseR.style.display = 'none';
                    chb.checked = true;
               } 
               else {               
                    agencyCaseR.style.display = 'block';
                    isValid = false;
                    chb.checked = false;        
               }
               
            
               return isValid;
            },
            
            ValidateBorrowerInfo : function() {
                var isValid = true;
                var doc = document.getElementById('BorrowerInfoFieldSet');
                var ddls = doc.getElementsByTagName('select');
                for(var i = 0; i< ddls.length; i++) {
                    var ddl = ddls[i];
                    var cbid = $j(ddl).attr('CB');
                    var cb = document.getElementById(cbid);//document.getElementById(ddl.CB);
                    var imgid= $j(ddl).attr('RI');
                    var img = document.getElementById(imgid);
                    FHASubmitForm.CheckFTHB(cb, ddl);
                    if ( ddl.options[ddl.selectedIndex].text === '' ) {
                        img.style.display = '';
                        isValid = false;
                    } 
                    else {
                        img.style.display = 'none';
                    }
                    
                    
                }
                
                return isValid;
                
            },
            
            CheckFTHB : function(chk, ddl) {  
                if (!chk.checked && this.useLegacyCounselingDefinition) {
                    if ( ddl.options.length  < 4 ) {
                        var option = new Option("Not Applicable", "3",true);
                        ddl.options.add(option);
                    }
                    
                    ddl.options[3].selected = true; 
                   ddl.style.backgroundColor = 'gainsboro';
                    ddl.disabled = true;
                }
                else  {
                    if ( ddl.options.length == 4 ) {
                        ddl.remove(3);
                         ddl.disabled = false;
                         ddl.style.backgroundColor = 'white';
                    }
                    
                }
            },
                        
            RefreshRentalIncome : function() { 
                
                var args = { 
                    loanid : <%= AspxTools.JsString(LoanID) %>,
                    sTotalScoreAppraisedFairMarketRent : <%= AspxTools.JQuery(sTotalScoreAppraisedFairMarketRent) %>.val(), 
                    sTotalScoreVacancyFactor : <%= AspxTools.JQuery(sTotalScoreVacancyFactor) %>.val()
                };
                
                var y = gService.FHATotal.call("RefreshRentalIncome", args );
                
                if ( y.error ) {
                    alert( y.UserMessage );
                }
                else {
                    <%= AspxTools.JQuery(sTotalScoreNetRentalIncome) %>.val( y.value.sTotalScoreNetRentalIncome);
                }

            },
            
            SubmitUI : function(btn) {
                btn.disabled = true;
                //Modal.Show('WaitMsg');
                 showPopup('#WaitMsg', '55%', '33%')
                btn.blur();
                window.setTimeout( function() {
                    FHASubmitForm.SubmitInfo(btn);
                }, 500);
            },
            UpdateAssetData : function(){
                var data = FHASubmitForm.GetAssetSources();
                data.loanid =  <%=AspxTools.JsString(LoanID) %>;  
                var results = gService.FHATotal.call("UpdateAssetInfo", data);
                if( results.error ){
                    alert(results.UserMessage);
                }
                else {
                    var source = document.getElementById('sPrimaryGiftFundSource');
                    source.innerText  = results.value["sPrimaryGiftFundSource"];
                }
                
                FHASubmitForm.ValidatePage();
            },
            
            GetAssetSources : function() {
                
                var table = document.getElementById('GiftFundAssets');
                if(!table){
                    return { 'AssetTot' : 0 };
                }
                var sources = table.getElementsByTagName("select"), 
                    count = sources.length,
                    select,
                    data = { 'AssetTot' : count };

                for( var i = 0; i < count; i++ ){
                     select = sources[i];
                     data["AssetId_" + i] = $j(select).attr('htmldataassetid');
                     data["AssetSource_" + i] = select.value;
                     data["AssetAppId_" + i] = $j(select).attr('htmldataappid');
                }   
                return data;
            },
            SubmitInfo : function(btn) {
                
                var args = getAllFormValues(),
                    assetData = FHASubmitForm.GetAssetSources(),
                    propName;
                  
                args["loanid"] = <%=AspxTools.JsString(LoanID) %>;  
                
       
                for( propName in assetData ){
                    if( assetData.hasOwnProperty(propName) ){
                        args[propName] = assetData[propName];
                    }
                }
                  try{
                    var results = gService.FHATotal.call("SaveAndSubmit", args);
                    if (!results)
                     throw "err";
                     
                    if ( results.error && results.UserMessage) {
                        alert( results.UserMessage );               
                    } 
                   
                    else if (results.value['Success'] === "True" ) {
                          clearDirty();
                          document.location = VRoot + "/main/FHATotalFindings.aspx?fv=1&loanid=" + <%=AspxTools.JsString(LoanID) %>;
                    }
                    else {
                          clearDirty();
                          document.location = VRoot + "/main/FHAErrorResults.aspx?loanid=" + <%=AspxTools.JsString(LoanID) %> + "&errorkey=" + results.value["ErrorKey"];
                    }
                }catch(e){
                    logJSException(e, 'problem when submitting info'); alert('System Error. Please contact us if this happens again.');
                }
                
             
              btn.disabled = false;  
              //Modal.Hide();
              }
        };
        
        
        function debug( obj )  {
           
                var result = " "; 
                for (curProperty in obj) {
                    result += curProperty + " ";
                }
                alert( result );
                
        }
        
        function _init() {
            FHASubmitForm.init(); 
            if (typeof(Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }
        }
        
        function f_saveMe() {
            var args = getAllFormValues(),
                assetData = FHASubmitForm.GetAssetSources(),
                propName;
            
            args["loanid"] = <%=AspxTools.JsString(LoanID) %>;
            for( propName in assetData ){
                if( assetData.hasOwnProperty(propName) ){
                    args[propName] = assetData[propName];
                }
            }
            
           var res = gService.FHATotal.call("Save", args);
          if ( res.error ) 
          {
            alert( res.UserMessage ); 
            return false;
          }
          else {
            clearDirty();
            return true; 
          }
        }
        
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, '');
        };
        /*
        function SetUniqueRadioButton(nameregex, current) {
                var re = new RegExp(nameregex);  
    
                for(i = 0; i < document.forms[0].elements.length; i++) {
                        var elm = document.forms[0].elements[i];
                        if ( elm.CName === 'undefined' || elm.type !== 'radio') { continue; }
                        if (re.test(elm.name))
                        {
                            elm.checked = false;
                            EnableCNames(elm.CName, true);
                        }
                }
                current.checked = true;
                EnableCNames(current.CName, false);
        }
        
        function EnableCNames(cname, enable) {
            var radioButtons = document.getElementById(cname).childNodes;

            for ( var i = 0; i < radioButtons.length; i++ ) {
                var control = radioButtons[i];
                if ( control.type !== 'radio' ) { continue; }
                if ( !enable ) {
                    if ( control.value === 'SubjectProperty' ) { control.checked = true; }
                    else { control.checked = false; }
                }
                document.getElementById(cname).disabled = !enable;
            }
        }
        */
    </script>

    </form>
</body>

</html>
