<%@ Page language="c#" Codebehind="LoanProspectorMain.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.LoanProspectorMain" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>LoanProspectorMain</title>
  </head>
  <body onload="f_submit();">
<script type="text/javascript">
<!--
  function f_submit()
  {
    <% if (m_hasAutoLogin) { %>
      try
      {
        var oHttp = new XMLHttpRequest();
        oHttp.open("GET", <%= AspxTools.JsString(this.Form.Action) %>, false );
        oHttp.setRequestHeader('Authorization', <%= AspxTools.JsString(m_basicAuthentication) %>);
        oHttp.send();
      } catch (e) { logJSException(e, 'problem with f_submit when has auto login.');
        document.getElementById("main").style.display = "none";
        document.getElementById("help").style.display = "";
        return;
      }
    <% } %>
  document.forms[0].submit();
}
//-->
</script>	
    <form name=dataForm runat="server" method="post">
    <table height="100%" width="100%" id="main">
      <tr>
        <td style="font-weight: bold" valign="middle" align="center">Please wait ...</td>
      </tr>
    </table>
        <iframe src="../help/Q00015.html" width="100%" height="100%" id="help" style="display:none"></iframe>

    </form>
	
  </body>
</html>
