namespace PriceMyLoan.UI.Main
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;

    using ConfigSystem;
    using global::DataAccess;
    using global::DataAccess.LoanComparison;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Events;
    using LendersOffice.PriceMyLoan;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using PriceMyLoan.Common;

    public enum E_ResultSortOptionT
    {
        ByProgram = 0,
        ByRate = 1,
        BestPrice = 2
    }
    public partial class GetResults : BaseUserControl, ITabUserControl
    {
        protected bool m_showRateForIneligible = false;
        protected bool m_has2ndLoan = false;
        protected E_sLienQualifyModeT m_currentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan;

        protected bool m_isWaiting = true;
        protected string m_resultHeader = "Results";
        protected bool m_hasLinkedLoan = false;
        protected bool m_hasError = false;
        protected System.Web.UI.WebControls.Repeater m_ineligibleRateRepeater;
        private Hashtable m_lIsArmMarginDisplayedHash = new Hashtable();

        protected List<object> m_gDataLpList = new List<object>();
        protected List<object> m_gDataRateOptionList = new List<object>();
        protected List<object> m_gDataList = new List<object>();

        protected bool m_isSkip2ndLoan = false;
        protected long m_renderResultDuration = 0L;
        protected string m_renderResultDurationDetail = "";
        protected long m_calcServerDuration = 0L;
        protected int m_calcServerCount = 0;

        protected bool m_isLpeManualSubmissionAllowed = true;
        protected string m_batchCreatedTime = "";
        protected string m_sensitiveData = "";
        protected string m_debugDetail = "";
        protected string m_sortString = ""; // OPM 9712

        protected bool m_showMaxDtiWarning = false;

        protected string m_waitingMessage = "Processing your request...";
        protected bool m_isLpeDisqualificationEnabled = true;
        protected bool m_isOptionARMUsedInPml = true; // 01/23/08 mf. OPM 19782
        protected bool m_displayPmlFeeIn100Format = false; // OPM 19525 02/04/08 mf 

        private bool m_isBestPriceFeatureEnabled = false;
        private bool m_isBorrowerPaidOriginatorCompAndIncludedInFees = false;
        private bool m_isLenderPaidOriginatorCompAndAdditionToFees = false;

        private CPageData x_LoanWithPriceGroupData = null;
        /// <summary>
        /// a loan with price group data, and it is initloaded;
        /// </summary>
        private CPageData LoanWithPriceGroupData
        {
            get
            {
                if (x_LoanWithPriceGroupData == null)
                {
                    var loan = new CPageData(LoanID, new string[] { "sPriceGroup" });
                    loan.AllowLoadWhileQP2Sandboxed = true;
                    loan.InitLoad();
                    x_LoanWithPriceGroupData = loan;
                }
                return x_LoanWithPriceGroupData;
            }
        }

        protected bool m_hasExpiredRate = false; // 7/29/2009 dd - OPM 22144
        protected bool m_isBestPrice
        {
            get { return m_isBestPriceFeatureEnabled && ResultSortOptionT == E_ResultSortOptionT.BestPrice; }
        }
        protected E_RenderResultModeT RenderResultModeT
        {
            get { return m_isBestPrice ? E_RenderResultModeT.BestPricesPerProgram : E_RenderResultModeT.Regular; }
        }
        protected bool m_isCitiDemo
        {
            get
            {
                return CurrentBroker.TempOptionXmlContent.Value.IndexOf("CITIDEMO") > 0;
            }
        }
        protected E_ResultSortOptionT ResultSortOptionT
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get("ResultSortOrder");

                E_ResultSortOptionT sortOption = E_ResultSortOptionT.ByProgram;

                if (cookie != null)
                {
                    switch (cookie.Value)
                    {
                        case PriceMyLoanConstants.SortType_ByNoteRate:
                            sortOption = E_ResultSortOptionT.ByRate;
                            break;
                        case PriceMyLoanConstants.SortType_ByName:
                            sortOption = E_ResultSortOptionT.ByProgram;
                            break;
                        case PriceMyLoanConstants.SortType_BestPrice:
                            if (m_isBestPriceFeatureEnabled)
                            {
                                sortOption = E_ResultSortOptionT.BestPrice;
                            }
                            else
                            {
                                sortOption = E_ResultSortOptionT.ByProgram;
                            }
                            break;
                        default:
                            Tools.LogBug("Unhandle sort option in cookie value. Cookie Value=" + cookie.Value);
                            sortOption = E_ResultSortOptionT.ByProgram;
                            break;
                    }
                }
                return sortOption;
            }
        }
        protected bool m_IsQP2File;
        protected string m_RegisterAndRateLockBlockingMessage = string.Empty;

        private DateTime m_pageInitTimestamp;

        public string TabID
        {
            get { return "RESULT"; }
        }
        private string m_cmd
        {
            get
            {
                if (RequestHelper.GetSafeQueryString("cmd") != null && RequestHelper.GetSafeQueryString("cmd") != "")
                {
                    return RequestHelper.GetSafeQueryString("cmd");
                }
                else
                {
                    return Request.Form["__EVENTTARGET"];
                }
            }
        }

        protected string m_confirmationTitle
        {
            // 02/27/08 mf. OPM 20619
            get { return IsPmlSubmissionAllowed ? "Confirmation" : "Summary"; }
        }
        private bool m_unableToSubmitWithoutCredit = false;
        protected bool UnableToSubmitWithoutCredit
        {
            get { return m_unableToSubmitWithoutCredit; }
        }

        protected bool m_isRerunMode = false;
        protected bool m_IsProgramRegistered = false;
        protected bool m_eligibleLoanProgramsExist = false;

        protected bool m_sProdDocT_NoIncome = false;
        protected bool HideQRate { get; set; }

        private bool? x_hasDuplicate = null;
        protected bool m_HasDuplicate
        {
            get
            {
                if (!x_hasDuplicate.HasValue)
                {
                    var dataLoan = new CPageData(LoanID, new string[] { "sSpAddr" });
                    dataLoan.AllowLoadWhileQP2Sandboxed = true;
                    dataLoan.InitLoad();
                    x_hasDuplicate = DuplicateFinder.FindDuplicateLoansPer105723(dataLoan.sLId, dataLoan.sBrokerId, dataLoan.sSpAddr).Count() > 0;
                }
                return x_hasDuplicate.Value;
            }
        }

        public void LoadData()
        {
            Page.ClientScript.RegisterHiddenField("ActionCmd", "Wait");

            CPageData dataLoan = new CCheckFor2ndLoan(LoanID);
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);

            // To properly render this page, we need to know what the doctype of this
            // loan is.  For performance, we piggyback on the payload of this object.

            switch (dataLoan.sProdDocT)
            {
                case E_sProdDocT.NINA:
                case E_sProdDocT.NIVA:
                case E_sProdDocT.NINANE:
                case E_sProdDocT.NIVANE:
                case E_sProdDocT.NISA:
                case E_sProdDocT.DebtServiceCoverage:
                case E_sProdDocT.NoIncome:
                    m_sProdDocT_NoIncome = true;
                    break;
            }

            bool hideDtiCol = (!CurrentBroker.IsIncomeAssetsRequiredFhaStreamline) //opm 28103 fs 04/08/10
                                && dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance
                                && (!dataLoan.sIsCreditQualifying);

            hideDtiCol |= dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl
                           && (!dataLoan.sIsCreditQualifying);

            m_sProdDocT_NoIncome |= hideDtiCol;

            m_IsQP2File = E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT;
            if (m_IsQP2File)
            {
                if (PriceMyLoanConstants.IsEmbeddedPML)
                {
                    m_RegisterAndRateLockBlockingMessage = JsMessages.LPE_CannotSubmitInQP2Mode;
                }
                else 
                {
                    if (CurrentBroker.IsAllowExternalUserToCreateNewLoan)
                    {
                        m_RegisterAndRateLockBlockingMessage = JsMessages.LPE_CannotSubmitInQP2ModeInPML;
                    }
                    else
                    {
                        m_RegisterAndRateLockBlockingMessage = JsMessages.LPE_CannotSubmitInQP2ModeInPMLAndCantCreateLoan;
                    }
                }
            }

            if (dataLoan.sIsQualifiedForOriginatorCompensation)
            {
                m_isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
                    && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                m_isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
            }

            // OPM 9712.
            switch (ResultSortOptionT)
            {
                case E_ResultSortOptionT.ByProgram:
                    m_sortString = "Results are sorted by: Loan Program Name.";
                    break;
                case E_ResultSortOptionT.ByRate:
                    m_sortString = "Results are sorted by: Note Rate.";
                    break;
                case E_ResultSortOptionT.BestPrice:
                    m_sortString = "Display best prices per program.";
                    break;
                default:
                    throw new UnhandledEnumException(ResultSortOptionT);
            }

            m_unableToSubmitWithoutCredit = !PriceMyLoanUser.HasPermission(Permission.CanSubmitWithoutCreditReport)
                && !dataLoan.sAllAppsHasCreditReportOnFile && !PriceMyLoanRequestHelper.IsEncompassIntegration;
            m_showRateForIneligible = PriceMyLoanUser.HasPermission(Permission.CanApplyForIneligibleLoanPrograms);

            if (dataLoan.sPml1stVisitResultStepD_rep == "")
            {
                if (dataLoan.sEmployeeLenderAccExecId != Guid.Empty)
                {
                    LoanPricingRan ev = new LoanPricingRan();
                    ev.Initialize(dataLoan.sLId, PriceMyLoanUser.DisplayName, dataLoan.sEmployeeLenderAccExecId);
                    ev.Send();
                }
                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.AllowSaveWhileQP2Sandboxed = true;
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitResultStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();
            }

            m_IsProgramRegistered = dataLoan.sLpTemplateId != Guid.Empty;

            main _parentPage = this.Page as main;
            if (null != _parentPage)
            {
                if (RequestHelper.GetSafeQueryString("actionCmd") == null)
                {
                    // 12/1/2006 dd - Only take snapshot of Pml data when going from step 3 to Result screen.
                    CPageData pmlSnapshot = new CPmlDataSnapshotData(LoanID);
                    pmlSnapshot.InitSave(ConstAppDavid.SkipVersionCheck);
                    pmlSnapshot.TakeSnapshotOfPmlDataBeforeRunningPricing();
                    pmlSnapshot.Save();
                }
                _parentPage.UpdateLoanSummaryDisplay(dataLoan);
            }

            //keep track of the last time we ran pml 
            //this is used for opm 115416 
            //av 2013 4/10
            CPageData resultGenerateD = new CFullAccessPageData(LoanID, new string[] { "sPmlLastGenerateResultD" });
            resultGenerateD.AllowSaveWhileQP2Sandboxed = true;
            resultGenerateD.InitSave(ConstAppDavid.SkipVersionCheck);
            resultGenerateD.UpdateLastGeneratedResultD();
            resultGenerateD.Save();

            if ((RequestHelper.GetSafeQueryString("actionCmd") != null && RequestHelper.GetSafeQueryString("actionCmd") == "Result")
                || (m_cmd == "Run2nd" && Request.Form["actionCmd"] == "Result"))
            {
                m_isWaiting = false;


                m_has2ndLoan = dataLoan.sIs8020PricingScenario;
                if (this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                {
                    m_hasLinkedLoan = false; // Always false for rerun mode.
                    m_isSkip2ndLoan = !dataLoan.sLinkedLoanInfo.IsLoanLinked;

                }
                else
                {
                    m_hasLinkedLoan = dataLoan.sLinkedLoanInfo.IsLoanLinked;
                }

                if (m_has2ndLoan)
                    m_resultHeader = "Results for First Loan";

                if (m_cmd == "Run2nd" && m_has2ndLoan)
                {
                    m_currentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan;
                    m_resultHeader = "Results for Second Loan";
                    HideQRate = dataLoan.sSubFinT == E_sSubFinT.Heloc;
                    RenderResult();
                }
                else
                {
                    m_currentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan;
                    RenderResult();
                }

            }
        }

        private void RenderResult()
        {
            m_waitingMessage = "Displaying your results ...";
            long start = DateTime.Now.Ticks;
            Guid requestID = new Guid(m_cmd == "Run2nd" ? Request.Form["RequestID"] : RequestHelper.GetSafeQueryString("RequestID"));


            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(requestID);
            long interval0 = DateTime.Now.Ticks;
            if (null == resultItem)
            {
                // 2/20/2008 dd - One of the probable cause is user click "Refresh" or "Back" from browser. Rerun pricing for them.
                m_isWaiting = true;
                return;
            }

            if (resultItem.IsErrorMessage)
            {
                m_hasError = true;
                ((BasePage)this.Page).AddInitScriptFunctionWithArgs("f_displayErrorMessage", "'" + Utilities.SafeJsString(resultItem.ErrorMessage) + "'");
                return;
            }

            ArrayList results = resultItem.Results;

            if (results == null)
                throw new CBaseException(PriceMyLoan.UI.Common.PmlErrorMessages.CertificateNullResult, "DistributeUnderwritingEngine.RetrieveResults return null for " + requestID);

            m_calcServerDuration = resultItem.CalcServerExecutionDurationInMs;
            m_calcServerCount = resultItem.CalcServerCount;
            m_sensitiveData = EncryptionHelper.Encrypt(resultItem.DetailTransactionMessage);
            m_debugDetail = EncryptionHelper.Encrypt(resultItem.DebugDetail);
            long interval1 = DateTime.Now.Ticks;

            List<CApplicantPriceXml> eligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> ineligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> insufficientList = new List<CApplicantPriceXml>();

            Hashtable m_developerErrorHash = new Hashtable();
            int currentIndex = 0;
            string idposfix = m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "0" : "1";

            m_gDataList = new List<object>();
            foreach (object o in results)
            {
                CApplicantPriceXml product = o as CApplicantPriceXml;

                if (null == product)
                    continue;

                switch (product.Status)
                {
                    case E_EvalStatus.Eval_Eligible:

                        RenderJsData(product);
                        product.TempIndex = currentIndex++;

                        eligibleList.Add(product);

                        if (!m_isBestPrice)
                        {
                            // 7/29/2009 dd - FOr Best Price rendering the code to check for expired rate is in different block.
                            if (product.AreRatesExpired)
                            {
                                m_hasExpiredRate = true;
                            }
                        }
                        break;
                    case E_EvalStatus.Eval_Ineligible:
                        RenderJsData(product);
                        product.TempIndex = currentIndex++;

                        if (product.AreRatesExpired)
                        {
                            m_hasExpiredRate = true;
                        }

                        ineligibleList.Add(product);
                        break;
                    case E_EvalStatus.Eval_InsufficientInfo:
                        insufficientList.Add(product);
                        if (product.Errors.IndexOf(ErrorMessages.LP_DisabledBySAEMessage) < 0)
                        {
                            // 6/21/2007 dd - OPM 10242 - If the LP is disable by SAE then do not send insufficient email to developer.
                            foreach (string errMsg in product.DeveloperErrors)
                            {
                                if (!m_developerErrorHash.Contains(errMsg))
                                {
                                    m_developerErrorHash.Add(errMsg, null);
                                }
                            }
                        }
                        break;
                    case E_EvalStatus.Eval_HideFromResult:
                        // Ignore. OPM 31884
                        break;
                    default:
                        throw new UnhandledEnumException(product.Status);
                }
            }
            m_eligibleLoanProgramsExist = eligibleList.Count > 0;

            if (m_developerErrorHash.Count > 0)
            {
                StringBuilder devsb = new StringBuilder();
                devsb.AppendFormat("Insufficient Information Error List for sLId={0}, LoginNm={1}{2}{2}", LoanID, PriceMyLoanUser.LoginNm, Environment.NewLine);
                foreach (DictionaryEntry entry in m_developerErrorHash)
                {
                    devsb.AppendFormat("{0}{1}", entry.Key, Environment.NewLine);
                }

                Tools.LogErrorWithCriticalTracking(devsb.ToString());
            }

            // Sort result.
            switch (ResultSortOptionT)
            {
                case E_ResultSortOptionT.ByProgram:
                case E_ResultSortOptionT.BestPrice:
                    eligibleList.Sort(new CApplicantPriceXmlNameComparer());
                    ineligibleList.Sort(new CApplicantPriceXmlNameComparer());
                    break;
                case E_ResultSortOptionT.ByRate:
                    eligibleList.Sort(new CApplicantPriceXmlRepresentativeRateComparer());
                    if (m_showRateForIneligible)
                    {
                        ineligibleList.Sort(new CApplicantPriceXmlRepresentativeRateComparer());
                    }
                    else
                    {
                        ineligibleList.Sort(new CApplicantPriceXmlNameComparer());
                    }
                    break;
                default:
                    throw new UnhandledEnumException(ResultSortOptionT);

            }

            insufficientList.Sort(new CApplicantPriceXmlNameComparer());

            m_eligibleNotFoundLiteral.Visible = !m_eligibleLoanProgramsExist && (!m_isSkip2ndLoan || !m_isRerunMode);

            long interval2 = DateTime.Now.Ticks;

            m_eligiblePlaceHolder.Visible = !m_isBestPrice;

            if (m_isBestPrice)
            {
                #region Display Best Prices Per Program View
                // 7/1/2009 dd - OPM 24899 - Pass in option whether to filter out higher rate with worst price.
                RateMergeBucket rateMergeBucket = new RateMergeBucket(CurrentBroker.IsShowRateOptionsWorseThanLowerRate);

                for (int i = 0; i < eligibleList.Count; i++)
                {
                    rateMergeBucket.Add(eligibleList[i]);
                }
                m_gDataRateOptionList = new List<object>();

                foreach (RateMergeGroup mergeGroup in rateMergeBucket.RateMergeGroups)
                {
                    if (mergeGroup.RateOptions.Count == 0)
                        continue;

                    string groupMergeName = mergeGroup.Name;
                    int representativeRateIndex = mergeGroup.RepresentativeRateIndex;

                    List<object> groupMergeItemList = new List<object>();
                    m_gDataRateOptionList.Add(groupMergeItemList);

                    groupMergeItemList.Add(groupMergeName);
                    groupMergeItemList.Add(representativeRateIndex);

                    List<object> rateOptionList = new List<object>();
                    groupMergeItemList.Add(rateOptionList);


                    foreach (CApplicantRateOption rateOption in mergeGroup.RateOptions)
                    {
                        if (rateOption.IsRateExpired)
                        {
                            m_hasExpiredRate = true;
                        }

                        // 11/12/10 mf. OPM 32422. Show warning only if there is a violation.
                        if (m_showMaxDtiWarning == false && rateOption.IsViolateMaxDti)
                        {
                            m_showMaxDtiWarning = true;
                        }

                        // OPM 64253.  If lender has a special option turned on, we want to 
                        // display the point value as adjusted for originator comp.
                        string pointRep;
                        if (CurrentBroker.IsUsePriceIncludingCompensationInPricingResult && m_isLenderPaidOriginatorCompAndAdditionToFees)
                        {
                            // OPM 64253
                            pointRep = rateOption.PointIncludingOriginatorComp_rep;
                        }
                        else
                        {
                            pointRep = rateOption.Point_rep;
                        }

                        List<object> rateOptionItem = new List<object>()
                        {
                            rateOption.Rate_rep // 0
                            , pointRep // 1
                            , rateOption.Apr_rep // 2
                            , rateOption.Margin_rep // 3
                            , rateOption.FirstPmtAmt_rep // 4
                            , "" // 12/1/2009 dd - We are no longer display top ratio in result. rateOption.TopRatio_rep // 5
                            , rateOption.BottomRatio_rep == "-1.000" || rateOption.BottomRatio_rep == "" ? "N/A" : rateOption.BottomRatio_rep // 6
                            , rateOption.RateOptionId // 7
                            , rateOption.TeaserRate_rep // 8
                            , rateOption.QRate_rep // 9
                            , rateOption.QualPmt_rep // 10
                            , CApplicantRateOption.PointFeeInResult(LoanWithPriceGroupData.sPriceGroup.DisplayPmlFeeIn100Format, pointRep) // 11
                            , rateOption.ApplicantPriceTempIndex // 12
                            , !m_sProdDocT_NoIncome && rateOption.IsViolateMaxDti ? 1 : 0 // 13
                            , rateOption.BankAmount_rep // 14
                            , rateOption.IsDisqualified ? 1 : 0 //15,
                            , rateOption.DisqualReason

                        };
                        rateOptionList.Add(rateOptionItem);


                    }
                }
                #endregion
            }
            else
            {
                #region Display regular view
                if (eligibleList.Count != 0)
                {
                    for (int i = 0; i < eligibleList.Count; i++)
                    {
                        LoanProductResultControl c = new LoanProductResultControl();
                        // 12/7/2005 dd - ID will be "_e0" + i for first lien result and "_e1" + i for second lien result.
                        // I need the distinction in viewing result in audit history.
                        c.ID = "_e" + idposfix + i; 

                        c.ApplicantPrice = eligibleList[i];
                        c.IsLoanRegistrationDisabled = !IsAllowRegisterRateLock;
                        c.Has2ndLoan = m_has2ndLoan;
                        c.CurrentLienQualifyModeT = m_currentLienQualifyModeT;
                        c.CssClassName = i % 2 == 1 ? "ElAltBg" : "ElBg";
                        c.IsNoIncome = m_sProdDocT_NoIncome;
                        c.BrokerDB = this.CurrentBroker;
                        c.CurrentLpeRunModeT = lpeRunModeT;
                        c.CanSubmitForLock = IsAllowRequestingRateLock;
                        c.IsProgramRegistered = m_IsProgramRegistered;
                        c.IsBorrowerPaidOriginatorCompAndIncludedInFees = m_isBorrowerPaidOriginatorCompAndIncludedInFees;
                        c.IsLenderPaidOriginatorCompAndAdditionToFees = m_isLenderPaidOriginatorCompAndAdditionToFees;
                        c.DisplayPmlFeeIn100Format = LoanWithPriceGroupData.sPriceGroup.DisplayPmlFeeIn100Format;
                        m_eligiblePlaceHolder.Controls.Add(c);

                    }

                }
                #endregion
            }


            if (ineligibleList.Count != 0)
            {
                if (m_showRateForIneligible)
                {
                    for (int i = 0; i < ineligibleList.Count; i++)
                    {
                        LoanProductResultControl c = new LoanProductResultControl();
                        c.ID = "_i" + idposfix + i;
                        c.ApplicantPrice = ineligibleList[i];
                        c.IsLoanRegistrationDisabled = !IsAllowRegisterRateLock;
                        c.Has2ndLoan = m_has2ndLoan;
                        c.CurrentLienQualifyModeT = m_currentLienQualifyModeT;
                        c.CssClassName = i % 2 == 1 ? "IneAltBg" : "IneBg";
                        c.IsNoIncome = m_sProdDocT_NoIncome;
                        c.BrokerDB = this.CurrentBroker;
                        c.CanSubmitForLock = IsAllowRequestingRateLock;
                        c.CurrentLpeRunModeT = lpeRunModeT;
                        c.IsProgramRegistered = m_IsProgramRegistered;
                        c.IsBorrowerPaidOriginatorCompAndIncludedInFees = m_isBorrowerPaidOriginatorCompAndIncludedInFees;
                        c.IsLenderPaidOriginatorCompAndAdditionToFees = m_isLenderPaidOriginatorCompAndAdditionToFees;
                        c.DisplayPmlFeeIn100Format = LoanWithPriceGroupData.sPriceGroup.DisplayPmlFeeIn100Format;
                        m_ineligibleRatePlaceHolder.Controls.Add(c);
                    }
                }
                else
                {
                    m_ineligibleRepeater.DataSource = ineligibleList;
                    m_ineligibleRepeater.DataBind();
                }
            }

            if (insufficientList.Count != 0)
            {
                m_insufficientRepeater.DataSource = insufficientList;
                m_insufficientRepeater.DataBind();
            }

            m_ineligibleNotFoundLiteral.Visible = ineligibleList.Count == 0;
            m_ineligibleRateNotFoundLiteral.Visible = ineligibleList.Count == 0;
            m_insufficientNotFoundLiteral.Visible = insufficientList.Count == 0;

            long interval3 = DateTime.Now.Ticks;

            SaveToTemporaryFile();

            long endTicks = DateTime.Now.Ticks;
            m_renderResultDuration = (endTicks - start) / 10000L;
            m_renderResultDurationDetail = string.Format("0-1:{0}ms, 1-2:{1}ms, 2-3:{2}ms, 3-4:{3}ms, 4-5:{4}ms", (interval0 - start) / 10000L, (interval1 - interval0) / 10000L, (interval2 - interval1) / 100000L, (interval3 - interval2) / 100000L, (endTicks - interval3) / 10000L);
            m_batchCreatedTime = resultItem.BatchCreatedDate.ToLongTimeString() + " - (PageInit):" + m_pageInitTimestamp.ToLongTimeString() + " - " + Tools.GetDBCurrentDateTime().ToLongTimeString();
            string debugMsg = LOCache.Get("DEBUG_" + requestID) as string;
            if (null != debugMsg)
            {
                m_batchCreatedTime += debugMsg;
            }

            object startTicks = LOCache.Get("DEBUG_" + requestID + "_START_TICKS");
            if (startTicks != null)
            {
                int durationMs = (int)(DateTime.Now.Ticks - (long)startTicks) / 10000;
                DistributeUnderwritingEngine.RecordPricingTotalTime(requestID, "RunPML", durationMs);

            }
        }


        /// <summary>
        /// Return two links title to used in best price pricing mode. The first link will be use for normal rate. The second link will be use when rate is expire.
        /// If RSE feature is not turn on the first link and second will be the same.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetLinkTitles()
        {

            List<string> oldLinks = ResultActionLink.GetLinkTitles_NoWorkflow(m_has2ndLoan, m_currentLienQualifyModeT, IsPmlSubmissionAllowed,
                IsEncompassIntegration, lpeRunModeT, PriceMyLoanUser.IsRateLockedAtSubmission);

            List<string> newLinks = ResultActionLink.GetLinkTitles(m_has2ndLoan, m_currentLienQualifyModeT, IsPmlSubmissionAllowed,
                IsEncompassIntegration, lpeRunModeT, PriceMyLoanUser.IsRateLockedAtSubmission, IsAllowRequestingRateLock, m_IsProgramRegistered, IsAllowRegisterRateLock);


            // In this case ? == a link to the reasons why "lock rate" does not appear.  Same actions as it being missing.
            //bool firstLinkMatch =  
            //    ( oldLinks[0] == newLinks[0] ) ||
            //    ( oldLinks[0] == "register loan" && newLinks[0] == "register loan/?" ) ||
            //    ( oldLinks[0] == "" && newLinks[0] == "?" ); 

            //bool secondLinkMatch = oldLinks[1] == newLinks[1];

            //if (firstLinkMatch == false || secondLinkMatch == false && Context.Items.Contains("PmlLockMismatchReported") )
            //{
            //    if (Context.Items["pmllockmismatchreported"] == null)
            //    {

            //        StringBuilder sb = new StringBuilder("PML ALLOWED LOCK ACTIONS MISMATCH!");
            //        sb.AppendLine();
            //        sb.AppendLine("sLId: " + LoanID);
            //        sb.AppendLine("UserId: " + PriceMyLoanUser.UserId);
            //        sb.AppendLine("New Actions: " + newLinks[0]);
            //        sb.AppendLine("Old Actions: " + oldLinks[0]);
            //        sb.AppendLine("New Unavailable: " + newLinks[1]);
            //        sb.AppendLine("Old Unavailable: " + oldLinks[1]);

            //        Context.Items["pmllockmismatchreported"] = true;
            //        Tools.LogWarning(sb.ToString());
            //    }
            //}


            // Always use old until we get no mismatches
            return UseOldSecurityVersion ? oldLinks : newLinks;
        }


        protected IEnumerable<string> ReasonsCannotRegister
        {
            get
            {
                if (IsAllowRegisterRateLock)
                {
                    return new List<string>();
                }

                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetReasonsUserCannotPerformOperation(WorkflowOperations.RegisterLoan);
            }

        }

        protected IEnumerable<string> ReasonsCannotLock
        {
            get
            {
                if (IsAllowRequestingRateLock)
                    return new List<string>(); // User is not blocked--no reasons
                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetReasonsUserCannotPerformOperation(WorkflowOperations.RequestRateLockRequiresManualLock, WorkflowOperations.RequestRateLockCausesLock);
            }
        }

        protected ConditionType CannotRegisterFailedConditionType
        {
            get
            {
                if (IsAllowRegisterRateLock)
                {
                    return ConditionType.Invalid;
                }

                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetOperationFailedConditionType(WorkflowOperations.RegisterLoan);
            }
        }

        protected ConditionType CannotLockFailedConditionType
        {
            get
            {
                if (IsAllowRequestingRateLock)
                {
                    return ConditionType.Invalid; // User is not blocked--no reasons
                }

                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetOperationFailedConditionType(WorkflowOperations.RequestRateLockRequiresManualLock, WorkflowOperations.RequestRateLockCausesLock);
            }
        }

        protected List<string> m_versionList = new List<string>(2);
        private int GetVersionIndex(string version)
        {
            for (int index = 0; index < m_versionList.Count; index++)
            {
                if (version == m_versionList[index])
                    return index;
            }
            m_versionList.Add(version);
            return m_versionList.Count - 1;
        }
        private void RenderJsData(CApplicantPriceXml applicantPrice)
        {
            List<object> list = new List<object>() {
                applicantPrice.lLpTemplateId // 0
                , applicantPrice.lLpTemplateNm // 1
                , GetVersionIndex(applicantPrice.Version) //2
                , applicantPrice.MaxDti_rep == "" ? "N/A" : applicantPrice.MaxDti_rep.Replace("%", "") //3
                , applicantPrice.IsBlockedRateLockSubmission ? 1 : 0 //4
                , applicantPrice.RateLockSubmissionUserWarningMessage //5
                , applicantPrice.AreRatesExpired ? 1 : 0 //6
                , applicantPrice.UniqueChecksum // 7
            };
            m_gDataLpList.Add(list);


            bool isIneligibleAndNoSubmit = applicantPrice.Status == E_EvalStatus.Eval_Ineligible && !m_showRateForIneligible;

            List<object> applicantPriceData = new List<object>();

            applicantPriceData.Add(new List<object>()
            {
                applicantPrice.lLpTemplateId // 0
                , isIneligibleAndNoSubmit ? "" : applicantPrice.lLpTemplateNm // 1
                , GetVersionIndex(applicantPrice.Version) // 2
                , applicantPrice.IsBlockedRateLockSubmission ? 1 : 0 // 3
                , applicantPrice.RateLockSubmissionUserWarningMessage // 4
                , applicantPrice.AreRatesExpired ? 1 : 0 // 5
                , applicantPrice.UniqueChecksum // 6

            });

            List<object> rateOptionList = new List<object>();
            applicantPriceData.Add(rateOptionList);

            m_gDataList.Add(applicantPriceData);


            if (!isIneligibleAndNoSubmit)
            {
                // 6/1/2007 dd - Only render rate option if program is Eligible or (Ineglible and ShowRate)
                CApplicantRateOption[] rateOptions = applicantPrice.ApplicantRateOptions;
                // 5/16/2007 dd - TODO Only need to render when rateOption.Length > 2.
                for (int rateOptionIndex = 0; rateOptionIndex < rateOptions.Length; rateOptionIndex++)
                {
                    CApplicantRateOption rateOption = rateOptions[rateOptionIndex];
                    rateOption.SetApplicantPrice(applicantPrice);

                    // 11/12/10 mf. OPM 32422. Show warning only if there is a violation.
                    if (m_showMaxDtiWarning == false && rateOption.IsViolateMaxDti)
                    {
                        m_showMaxDtiWarning = true;
                    }

                    // OPM 64253.  If lender has a special option turned on, we want to 
                    // display the point value as adjusted for originator comp.
                    string pointRep;
                    if (CurrentBroker.IsUsePriceIncludingCompensationInPricingResult && m_isLenderPaidOriginatorCompAndAdditionToFees)
                    {
                        // OPM 64253
                        pointRep = rateOption.PointIncludingOriginatorComp_rep;
                    }
                    else
                    {
                        pointRep = rateOption.Point_rep;
                    }
                    rateOptionList.Add(new List<object>()
                    {
                        rateOption.Rate_rep //0
                        , pointRep // 1
                        , rateOption.Apr_rep // 2
                        , applicantPrice.lIsArmMarginDisplayed ? rateOption.Margin_rep : "" // 3
                        , rateOption.FirstPmtAmt_rep // 4
                        , "" // 12/1/2009 dd - We are no longer display top ratio in result. rateOption.TopRatio_rep // 5
                        , rateOption.BottomRatio_rep == "-1.000" || rateOption.BottomRatio_rep == "" ? "N/A" : rateOption.BottomRatio_rep // 6
                        , rateOption.RateOptionId // 7
                        , rateOption.TeaserRate_rep // 8
                        , rateOption.QRate_rep // 9
                        , rateOption.QualPmt_rep // 10
                        , CApplicantRateOption.PointFeeInResult(LoanWithPriceGroupData.sPriceGroup.DisplayPmlFeeIn100Format, pointRep) // 11
                        , !m_sProdDocT_NoIncome && rateOption.IsViolateMaxDti ? 1 : 0 // 12
                        , rateOption.BankAmount_rep // 13
                        , rateOption.IsDisqualified ? 1 : 0 // 14
                        , rateOption.DisqualReason

                    });
                }
            }
        }
        public void SaveData()
        {
        }

        public void SetAdditionalQueryStringValues(PmlMainQueryStringManager qsManager)
        {
            // 02/15/08 mf. OPM 20137.  These form values need to go the query string.

            if (Request.Form["ActionCmd"] != null)
            {
                qsManager.AddPair("actionCmd", Request.Form["ActionCmd"]);
            }
            if (Request.Form["RequestID"] != null)
            {
                qsManager.AddPair("requestId", Request.Form["RequestID"]);
            }
            if (Request.Form["JsScriptStats"] != null)
            {
                qsManager.AddPair("jsScriptStats", Request.Form["JsScriptStats"]);
            }
        }


        private bool? x_allowRegister = new bool?();
        private bool IsAllowRegisterRateLock
        {
            get
            {
                if (x_allowRegister.HasValue == false)
                {
                    PriceMyLoan.UI.BasePage parentPage = this.Page as PriceMyLoan.UI.BasePage;
                    if (null == parentPage)
                    {
                        // Safety throw. If getresults control ends up on page that does not
                        // inherit from basepage, we should work in the security checks there.
                        throw new CBaseException(ErrorMessages.Generic, "GetResults is child control on page that does not inherit from Base.");
                    }
                    else
                    {
                        x_allowRegister = parentPage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ||
                            parentPage.CurrentUserCanPerform(WorkflowOperations.RegisterLoan);
                    }
                }

                return x_allowRegister.Value;
            }
        }


        private bool? x_AllowRequestRateLock = null;
        private bool IsAllowRequestingRateLock
        {
            get
            {
                if (x_AllowRequestRateLock.HasValue == false)
                {
                    PriceMyLoan.UI.BasePage parentPage = this.Page as PriceMyLoan.UI.BasePage;
                    if (null == parentPage)
                    {
                        // Safety throw. If getresults control ends up on page that does not
                        // inherit from basepage, we should work in the security checks there.
                        throw new CBaseException(ErrorMessages.Generic, "GetResults is child control on page that does not inherit from Base.");
                    }
                    else
                    {
                        x_AllowRequestRateLock = parentPage.CurrentUserCanPerform(WorkflowOperations.RequestRateLockRequiresManualLock)
                            || parentPage.CurrentUserCanPerform(WorkflowOperations.RequestRateLockCausesLock);
                    }
                }

                return x_AllowRequestRateLock.Value;
            }
        }

        private bool? x_UseOldSecurityVersion = null;
        private bool UseOldSecurityVersion
        {
            get
            {
                if (x_UseOldSecurityVersion.HasValue == false)
                {
                    PriceMyLoan.UI.BasePage parentPage = this.Page as PriceMyLoan.UI.BasePage;
                    if (null == parentPage)
                    {
                        // Safety throw. If getresults control ends up on page that does not
                        // inherit from basepage, we should work in the security checks there.
                        throw new CBaseException(ErrorMessages.Generic, "GetResults is child control on page that does not inherit from Base.");
                    }
                    else
                    {
                        x_UseOldSecurityVersion = parentPage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml);
                    }

                }

                return x_UseOldSecurityVersion.Value;
            }
        }

        protected bool IsPmlSubmissionAllowed
        {
            get
            {
                // 7/17/2009 dd - We do not allow user to submit the loan if it is running inside Encompass EPass
                return this.CurrentBroker.IsPmlSubmissionAllowed && !IsEncompassIntegration;
            }
        }
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected HtmlGenericControl CreateDisqualifiedRules(object o)
        {
            HtmlGenericControl span = new HtmlGenericControl("span");

            List<string> list = (List<string>)o;
            foreach (string s in list)
            {

                HtmlGenericControl div = new HtmlGenericControl("div");
                div.InnerText = s;
                span.Controls.Add(div);
            }
            return span;
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {

            if (RequestHelper.GetSafeQueryString("jsScriptStats") != null)
                Page.ClientScript.RegisterHiddenField("JsScriptStats", RequestHelper.GetSafeQueryString("jsScriptStats"));
            else
                Page.ClientScript.RegisterHiddenField("JsScriptStats", Request.Form["JsScriptStats"]);


            Page.ClientScript.RegisterHiddenField("RequestID", "");
            if (!Page.IsPostBack)
            {
                Page.ClientScript.RegisterHiddenField("ActionCmd", "Wait");
            }
            m_isRerunMode = this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct;

            if (Page.IsPostBack && (RequestHelper.GetSafeQueryString("cmd") == "Run2nd"))
                LoadData();

        }
        protected void PageInit(object sender, System.EventArgs e)
        {
            //main mainpage = (main)Page;
            //if (mainpage.DisableStep4)
            //{
            //    PriceMyLoanRequestHelper.AccessDenial(LoanID);
            //    return;
            //}


            m_pageInitTimestamp = Tools.GetDBCurrentDateTime();
            ((LendersOffice.Common.BasePage)this.Page).RegisterJsScript("getresult.js");

            BrokerDB broker = this.CurrentBroker;

            m_isLpeManualSubmissionAllowed = broker.IsLpeManualSubmissionAllowed && !IsEncompassIntegration;
            m_isLpeDisqualificationEnabled = broker.IsLpeDisqualificationEnabled;
            m_isOptionARMUsedInPml = broker.IsOptionARMUsedInPml;
            m_displayPmlFeeIn100Format = LoanWithPriceGroupData.sPriceGroup.DisplayPmlFeeIn100Format;
            m_isBestPriceFeatureEnabled = broker.IsBestPriceEnabled;
        }

        private void SaveToTemporaryFile()
        {
            string name = string.Format("{0}_{1}", m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "LPEFirstResult" : "LPESecondResult",
                this.LoanID.ToString("N"));

            string tempFile = TempFileUtils.NewTempFilePath();

            using (StreamWriter writer = new StreamWriter(tempFile, false))
            {
                MainResultPanel.RenderControl(new HtmlTextWriter(writer));
            }

            FileDBTools.WriteFile(E_FileDB.Temp, name, tempFile);

            try
            {
                FileOperationHelper.Delete(tempFile);
            }
            catch (IOException)
            {
                // NO-OP
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

    }
}
