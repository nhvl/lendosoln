using DataAccess;
using LendersOffice.Common;
using PriceMyLoan.UI.Common;
using System;
using System.Web.UI;

namespace PriceMyLoan.UI.Main
{
    // OPM 5068 Cord
    // Added dual functionality so this page handles access 
    // denial for both External and Embedded PML users.
    // If a user encounters a PageDataLoadDenied exception, 
    // they will be taken here, and the content displayed 
    // on this page will depend on whetherthey are an 
    // External or Embedded PML user. 

    // Embedded users will just see the old "Access Denied" 
    // page for now.

    // External users will see some loan details if supplied
    // with 'loanid' in the querystring, and they can return 
    // to their pipeline screen via a button.


    public partial class AccessDenial : PriceMyLoan.UI.BasePage
	{
		
		private String m_sLoanNumber = "";
		private String m_sBorrowerName = "";
		private bool m_bHasLoanInfo = false;
		
		
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Try to get loan information for the external user.
			if ( !Page.IsPostBack && AllowGoToPipeline )
			{
				Guid loanid = RequestHelper.GetGuid( "loanid", Guid.Empty );
				
				if ( loanid != Guid.Empty )
				{
					m_bHasLoanInfo = true;
					
					try 
					{
                        CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanid, typeof(AccessDenial));
                        dataLoan.InitLoad();
				
						m_sLoanNumber = dataLoan.sLNm;
						m_sBorrowerName = dataLoan.sPrimBorrowerFullNm;
					}
					catch ( CBaseException )
					{
						// The loan may have been deleted if the loanid 
						// is not found, so we should probably still display
						// the fact that it wasn't found here, rather than
						// nothing at all on the page.
						
						m_sLoanNumber = "Not found.";
						m_sBorrowerName = "Unknown.";
					}
				}
			}						
		}

		protected String LoanNumber
		{
			get { return m_sLoanNumber; }
		}

		protected String BorrowerName
		{
			get { return m_sBorrowerName; }
		}

		protected bool HasLoanInfo
		{
			get 
            { 
                return m_bHasLoanInfo; 
            }
		}

		protected String ErrorInformation
		{
			get 
			{ 
				if ( AllowGoToPipeline )
					return PmlErrorMessages.ExternalAccessDenied;
				else
					return "";	// PmlErrorMessage.AccessDenied (if we convert
								// the other panel to mirror this one).
			}
		}

		protected bool AllowGoToPipeline 
		{
			get 
			{
                return PriceMyLoanUser.IsAllowGoToPipeline;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
