using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using CommonLib;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CreditReport;
using LendersOffice.Security;
using PriceMyLoan.Common;
using PriceMyLoan.DataAccess;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.UI.Main
{

	public partial  class EnterCredit : BaseUserControl, ITabUserControl
	{
        private const string MASK_INSTANT_VIEW_PW = "*********";
        private const string MCL_CREDIT_SERVER = "https://credit.meridianlink.com";

        private string m_reportID = "";
        private bool m_hasCreditReport = false;
        protected bool m_displayWarning = false;

        private Hashtable m_craHash = new Hashtable();

        #region Protected Member Variables
        #endregion

        protected bool m_hasBrokerDUInfo = false;
        private string m_brokerDUUserID = "";
        private string m_brokerDUPassword = "";

        //protected string m_creditProtocolHashScript = "";

        //opm 4382 fs 07/23/08

        protected bool m_hasLiabilities = false;
		protected bool m_allowOrderNew = true;
		protected Guid ErrorMessageKey = Guid.Empty; // OPM 20593 
        protected bool m_displayCreditOptions = true;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            aBSuffix.UseJqueryScript = true;
            aCSuffix.UseJqueryScript = true;
            string key = RequestHelper.GetSafeQueryString("tabkey");
            if (null != key && "" != key && key.ToUpper() != this.TabID)
            {
                return; // 3/12/2009 dd - This is a hack so that at the result screen, this code block does not execute.
            }
            SetRequiredValidatorMessage(LoginNameValidator);
            SetRequiredValidatorMessage(PasswordValidator);
            SetRequiredValidatorMessage(InstantViewIDValidator);
            SetRequiredValidatorMessage(ReportIDValidator);
            SetRequiredValidatorMessage(aBFirstNmValidator);
            SetRequiredValidatorMessage(aBLastNmValidator);
            SetRequiredValidatorMessage(aBSsnValidator);
            SetRequiredValidatorMessage(aBAddrValidator);
            SetRequiredValidatorMessage(aBCityValidator);
            SetRequiredValidatorMessage(aBZipValidator);
            SetRequiredValidatorMessage(aBStateValidator);
            SetRequiredValidatorMessage(FD_ClientCodeValidator);
            SetRequiredValidatorMessage(FD_OfficeCodeValidator);
            SetRequiredValidatorMessage(CreditProtocolValidator);
            SetRequiredValidatorMessage(DUUserIDValidator);
            SetRequiredValidatorMessage(DUPasswordValidator);
            SetRequiredValidatorMessage(AccountIdentifierValidator);
            SetRequiredValidatorMessage(sProdCrManualBk13RecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManualBk7RecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManualForeclosureRecentSatisfiedValidator);
            SetRequiredValidatorMessage(sProdCrManual30MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualRolling60MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualRolling90MortLateCountValidator);
            SetRequiredValidatorMessage(sProdCrManualBk13RecentFileValidator);
            SetRequiredValidatorMessage(sProdCrManualBk7RecentFileValidator);
            SetRequiredValidatorMessage(sProdCrManualForeclosureRecentFileValidator);
            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
            Tools.BindLateDatesDropDown(sProdCrManual30MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual60MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual90MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual120MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManual150MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualNonRolling30MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualRolling60MortLateCount);
            Tools.BindLateDatesDropDown(sProdCrManualRolling90MortLateCount);

            Tools.BindMonthDropDown(sProdCrManualBk13RecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualBk13RecentSatisfiedMon);
            Tools.BindMonthDropDown(sProdCrManualBk7RecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualBk7RecentSatisfiedMon);
            Tools.BindMonthDropDown(sProdCrManualForeclosureRecentFileMon);
            Tools.BindMonthDropDown(sProdCrManualForeclosureRecentSatisfiedMon);

            Tools.BindYearDropDown(sProdCrManualBk13RecentFileYr);
            Tools.BindYearDropDown(sProdCrManualBk13RecentSatisfiedYr);
            Tools.BindYearDropDown(sProdCrManualBk7RecentFileYr);
            Tools.BindYearDropDown(sProdCrManualBk7RecentSatisfiedYr);
            Tools.BindYearDropDown(sProdCrManualForeclosureRecentFileYr);
            Tools.BindYearDropDown(sProdCrManualForeclosureRecentSatisfiedYr);

            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk13RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk7RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualForeclosureRecentStatusT);

            aBZip.SmartZipcode(aBCity, aBState);

            //opm 27866 fs 04/10/09
            aBSsn.IsMaskEnabled = true;
            aCSsn.IsMaskEnabled = true;

            List<Guid> denyCraList = MasterCRAList.RetrieveHiddenCras(this.CurrentBroker, E_ApplicationT.PriceMyLoan);
            // 9/21/2004 dd - Append special credit.meridianlink.com CRA
            m_craHash.Add("MCL_CREDIT_SERVER", new TemporaryCRAInfo("MCL", MCL_CREDIT_SERVER, Guid.Empty, CreditReportProtocol.Mcl, ""));

            CreditProtocol.Items.Add(new ListItem("<-- Select Credit Provider -->", Guid.Empty.ToString()));

            m_nonMclProtocolList = new List<KeyValuePair<Guid, int>>();

            #region Add Lender Mapping CRA
            bool hasLenderMappingCRA = false;
            foreach (var proxy in CreditReportAccountProxy.Helper.ListCreditProxyByBrokerID(this.BrokerID))
            {
                if (denyCraList.Contains(proxy.CrAccProxyId))
                {
                    continue;
                }

                Guid craId = proxy.CraId;
                string craUserName = proxy.CraUserName;
                string craPassword = proxy.CraPassword;
                string craAccId = proxy.CraAccId;
                string vendorName = proxy.CrAccProxyDesc;

                CreditProtocol.Items.Add(new ListItem(vendorName, proxy.CrAccProxyId.ToString()));

                // Add special code for CRA Mapping
                m_nonMclProtocolList.Add(new KeyValuePair<Guid, int>(proxy.CrAccProxyId, (int)CreditReportProtocol.LenderMappingCRA));

                TemporaryCRAInfo craInfo = new TemporaryCRAInfo(vendorName, "", craId, CreditReportProtocol.LenderMappingCRA, ""); //lender mappings have their provider id set by ordercredit
                craInfo.UserName = craUserName;
                craInfo.Password = craPassword;
                craInfo.AccountID = craAccId;
                craInfo.IsEquifaxPulled = proxy.IsEquifaxPulled;
                craInfo.IsExperianPulled = proxy.IsExperianPulled;
                craInfo.IsTransUnionPulled = proxy.IsTransUnionPulled;
                craInfo.IsFannieMaePulled = proxy.IsFannieMaePulled;
                craInfo.CreditMornetPlusUserId = proxy.CreditMornetPlusUserId;
                craInfo.CreditMornetPlusPassword = proxy.CreditMornetPlusPassword;

                m_craHash.Add(proxy.CrAccProxyId.ToString(), craInfo);

                hasLenderMappingCRA = true;
            }

            if (hasLenderMappingCRA)
            {
                CreditProtocol.Items.Add(new ListItem("---------------------------", Guid.Empty.ToString()));
            }

            #endregion

            #region Add Normal Master list of our CRA
            var masterCraList = LendersOffice.CreditReport.MasterCRAList.RetrieveAvailableCras(this.CurrentBroker);
            foreach (LendersOffice.CreditReport.CRA o in masterCraList) 
            {
                if (denyCraList.Contains(o.ID))
                    continue;

                CreditProtocol.Items.Add(new ListItem(o.VendorName, o.ID.ToString()));
                m_craHash.Add(o.ID.ToString(), 
                    new TemporaryCRAInfo(o.VendorName, o.Url, o.ID, o.Protocol, o.ProviderId));
                
                if (o.Protocol!= CreditReportProtocol.Mcl) 
                {
                    // 9/20/2004 dd - Add non-mcl protocol to javascript hash.
                    m_nonMclProtocolList.Add(new KeyValuePair<Guid, int>(o.ID, (int)o.Protocol));
                }

            }


            #endregion

            // 7/22/2005 dd - Set Last Use CRA
            Tools.SetDropDownListValue(CreditProtocol, PriceMyLoanUser.LastUsedCreditProtocolID.ToString());

            CreditAction_ManualCredit.Visible = m_canRunLpeWithoutCreditReport;

        }

        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected List<KeyValuePair<Guid, int>> m_nonMclProtocolList = new List<KeyValuePair<Guid, int>>();
        private bool m_canRunLpeWithoutCreditReport 
        {
            get { return PriceMyLoanUser.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport); }
        }

        private void DetermineBrokerDULoginInfo( BrokerDB broker ) 
        {
            m_brokerDUUserID = broker.CreditMornetPlusUserID;
            m_brokerDUPassword = broker.CreditMornetPlusPassword.Value;
            m_hasBrokerDUInfo = m_brokerDUUserID != "" && m_brokerDUPassword != "";
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{

		}

        protected bool HasCreditReport 
        {
            get { return m_hasCreditReport; }
        }
		
        private void RetrieveCreditReport(Guid applicationID) 
        {

            SqlParameter[] parameters = { new SqlParameter("@ApplicationID", applicationID) };

            string lastCra = null;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "RetrieveCreditReport", parameters)) 
            {
                if (reader.Read()) 
                {
                    m_reportID = (string) reader["ExternalFileID"];

                    if (reader["CrAccProxyId"] is DBNull)
                        lastCra = reader["ComId"].ToString();
                    else 
                    {
                        m_reportID = ""; // OPM 2147 - Hide credit reference number for credit pull from proxy account.
                        lastCra = reader["CrAccProxyId"].ToString();
                    }

                    // 10/24/2005 dd - if LastCra == POINT Dummy then set lastCra = null
                    if (new Guid(lastCra) == ConstAppDavid.DummyPointServiceCompany)
                        lastCra = null;

                }
            }
            
            if (null != lastCra)
                Tools.SetDropDownListValue(CreditProtocol, lastCra);
        }
        public string TabID 
        { 
            get { return "CREDIT"; }
        }
        public void LoadData() 
        {
            DetermineBrokerDULoginInfo( this.CurrentBroker);
			m_allowOrderNew = this.CurrentBroker.AllowPmlUserOrderNewCreditReport; // 2/16/07 OPM 9780.

			// OPM 20593 Check if there is an error message to report.
			Guid errorKey = RequestHelper.GetGuid("errorMsg", Guid.Empty );
			if( errorKey != Guid.Empty )
			{
				string errorMessage = AutoExpiredTextCache.GetFromCache( errorKey );
                if (errorMessage != null)
                {
                    ErrorMessage.Text = errorMessage;
                }
			}

            CPageData dataLoan = new CEnterCreditData(LoanID);
            dataLoan.InitLoad();
            // 11/18/2005 dd - Display data change warning if user visit this step before.
            // 12/7/2006 dd - Remove the warning only in step1.
            //m_displayWarning = dataLoan.sPml1stVisitCreditStepD_rep != "" && !Page.IsPostBack;
            
            if (dataLoan.sPml1stVisitCreditStepD_rep == "") 
            {
                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitCreditStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();
                dataLoan = new CEnterCreditData(LoanID);
                dataLoan.InitLoad();
            }
            sFileVersion = dataLoan.sFileVersion;
            CAppData dataApp = dataLoan.GetAppData(0);
            m_hasLiabilities = dataApp.aLiaCollection.CountRegular != 0;


            m_hasCreditReport = dataApp.aIsCreditReportOnFile;

            RetrieveCreditReport(dataApp.aAppId);
            ReportID.Text = m_reportID;
            if (m_hasCreditReport) 
            {
                CreditAction_UseCreditOnFile.Checked = true;

				InstantViewID.Attributes.Add("Value", MASK_INSTANT_VIEW_PW);
                //CreditAction.Items.Insert(0, new ListItem("Use Credit Report on File", "-1"));
            } 
            else 
			{
				// 1/30/07 mf OPM 9780.  We now want to cancel change from 4675 and hide the Use Credit Report on file instead of disable it.
				CreditAction_UseCreditOnFile.Visible = false;
				CreditAction_UseCreditOnFile.Checked = false;
				CreditAction_Reissue.Checked = true; // 4/26/2006 dd - Always default to re-issue when there is no credit on file.
			}

            aBFirstNm.Text                                  = dataApp.aBFirstNm;
            aBMidNm.Text                                    = dataApp.aBMidNm;
            aBLastNm.Text                                   = dataApp.aBLastNm;
            aBSuffix.Text                                   = dataApp.aBSuffix;
            aBSsn.Text                                      = dataApp.aBSsn;
            aBExperianScorePe.Text                          = dataApp.aBExperianScorePe_rep;
            aBTransUnionScorePe.Text                        = dataApp.aBTransUnionScorePe_rep;
            aBEquifaxScorePe.Text                           = dataApp.aBEquifaxScorePe_rep;
            aBAddr.Text                                     = dataApp.aBAddr;
            aBCity.Text                                     = dataApp.aBCity;
            aBState.Value                                   = dataApp.aBState;
            aBZip.Text                                      = dataApp.aBZip;
			aBHasSpouse.Checked								= dataApp.aBHasSpouse; //opm 4382 fs 07/23/08
            aBDob.Text                                      = dataApp.aBDob_rep;

            aCFirstNm.Text                                  = dataApp.aCFirstNm;
            aCMidNm.Text                                    = dataApp.aCMidNm;
            aCLastNm.Text                                   = dataApp.aCLastNm;
            aCSuffix.Text                                   = dataApp.aCSuffix;
            aCSsn.Text                                      = dataApp.aCSsn;
            aCExperianScorePe.Text                          = dataApp.aCExperianScorePe_rep;
            aCTransUnionScorePe.Text                        = dataApp.aCTransUnionScorePe_rep;
            aCEquifaxScorePe.Text                           = dataApp.aCEquifaxScorePe_rep;
            sTransmOMonPmtPe.Text                           = dataLoan.sTransmOMonPmtPe_rep;
            sProdCrManualBk13Has.Checked                    = dataLoan.sProdCrManualBk13Has;
            sProdCrManualBk7Has.Checked                     = dataLoan.sProdCrManualBk7Has;
            sProdCrManualForeclosureHas.Checked             = dataLoan.sProdCrManualForeclosureHas;
            aCDob.Text                                      = dataApp.aCDob_rep;

            Tools.SetDropDownListValue(sProdCrManual120MortLateCount, dataLoan.sProdCrManual120MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual150MortLateCount, dataLoan.sProdCrManual150MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual30MortLateCount, dataLoan.sProdCrManual30MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual60MortLateCount, dataLoan.sProdCrManual60MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual90MortLateCount, dataLoan.sProdCrManual90MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualNonRolling30MortLateCount, dataLoan.sProdCrManualNonRolling30MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualRolling60MortLateCount, dataLoan.sProdCrManualRolling60MortLateCount_rep);
			Tools.SetDropDownListValue(sProdCrManualRolling90MortLateCount, dataLoan.sProdCrManualRolling90MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileMon, dataLoan.sProdCrManualBk13RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileYr, dataLoan.sProdCrManualBk13RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedMon, dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedYr, dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileMon, dataLoan.sProdCrManualBk7RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileYr, dataLoan.sProdCrManualBk7RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedMon, dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedYr, dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileMon, dataLoan.sProdCrManualForeclosureRecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileYr, dataLoan.sProdCrManualForeclosureRecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedMon, dataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedYr, dataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep);

            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentStatusT, dataLoan.sProdCrManualForeclosureRecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentStatusT, dataLoan.sProdCrManualBk13RecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentStatusT, dataLoan.sProdCrManualBk7RecentStatusT);

            if (PriceMyLoanUser.Type == "B") 
            {
                Page.ClientScript.RegisterHiddenField("DeleteLiabilities", "0");
            } 
            else if (PriceMyLoanUser.Type == "P")
            {
                // 10/21/2005 dd - For "P" user Type always delete any existing liabilities.
                Page.ClientScript.RegisterHiddenField("DeleteLiabilities", "1");
            }

            main _parentPage = this.Page as main;
            if (null != _parentPage)
                _parentPage.UpdateLoanSummaryDisplay(dataLoan);

            if (!m_hasCreditReport)
            {
                // 12/2/2004 dd - If any credit score is non zero then assumer user enter credit score mannually.
                // 11/3/2008 dd - OPM 25810 - If user is for MCL2PML then default to manual credit entry.
                if (dataApp.aIsCreditReportManual || PriceMyLoanUser.IsPmlMortgageLeagueLogin)
                {
                    if (CreditAction_ManualCredit.Visible) 
                    {
                        CreditAction_ManualCredit.Checked = true;
                    }
                }
                else 
                {
                    CreditAction_Reissue.Checked = true;
                }
            }

			// 12/27/2006 nw - OPM 5083 - Remember login name
			if (PriceMyLoanUser.LastUsedCreditLoginNm != "")
			{
				LoginName.Text = PriceMyLoanUser.LastUsedCreditLoginNm;
				RememberLoginNameCB.Checked = true;
			}
        }

        /// <summary>
        /// UI function to save the info provided by the user as a manual credit report. 
        /// </summary>
        private void SaveManualCreditReport()
        {

            CPageData dataLoan = new CManualCreditData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(0);

            // 5/12/2006 dd - Need to delete the credit report before saving manual credit data. This way
            // the cache value will pick up from manual value correctly.
            // Delete Credit report.
            CreditReportServer.DeleteCreditReport(dataLoan.sBrokerId, dataApp.aAppId, LoanID, CreditReportDeletedAuditItem.E_CreditReportDeletionT.Manual, false);


            dataApp.aBExperianScorePe_rep = aBExperianScorePe.Text;
            dataApp.aBTransUnionScorePe_rep = aBTransUnionScorePe.Text;
            dataApp.aBEquifaxScorePe_rep = aBEquifaxScorePe.Text;
            dataApp.aCExperianScorePe_rep = aCExperianScorePe.Text;
            dataApp.aCTransUnionScorePe_rep = aCTransUnionScorePe.Text;
            dataApp.aCEquifaxScorePe_rep = aCEquifaxScorePe.Text;
            dataApp.aCSsn = aBHasSpouse.Checked ? (dataApp.aCSsn.Length == 0 ? "000-00-0000" : dataApp.aCSsn) : "";
            dataLoan.sTransmOMonPmtPe_rep = sTransmOMonPmtPe.Text;

            dataLoan.sProdCrManual120MortLateCount_rep = sProdCrManual120MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual150MortLateCount_rep = sProdCrManual150MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual30MortLateCount_rep = sProdCrManual30MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual60MortLateCount_rep = sProdCrManual60MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManual90MortLateCount_rep = sProdCrManual90MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualNonRolling30MortLateCount_rep = sProdCrManualNonRolling30MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualRolling60MortLateCount_rep = sProdCrManualRolling60MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualRolling90MortLateCount_rep = sProdCrManualRolling90MortLateCount.SelectedItem.Value;
            dataLoan.sProdCrManualBk13Has = sProdCrManualBk13Has.Checked;
            dataLoan.sProdCrManualBk13RecentFileMon_rep = sProdCrManualBk13RecentFileMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = sProdCrManualBk13RecentFileYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = sProdCrManualBk13RecentSatisfiedMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = sProdCrManualBk13RecentSatisfiedYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk13RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualBk13RecentStatusT);
            dataLoan.sProdCrManualBk7Has = sProdCrManualBk7Has.Checked;
            dataLoan.sProdCrManualBk7RecentFileMon_rep = sProdCrManualBk7RecentFileMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = sProdCrManualBk7RecentFileYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = sProdCrManualBk7RecentSatisfiedMon.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = sProdCrManualBk7RecentSatisfiedYr.SelectedItem.Value;
            dataLoan.sProdCrManualBk7RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualBk7RecentStatusT);
            dataLoan.sProdCrManualForeclosureHas = sProdCrManualForeclosureHas.Checked;
            dataLoan.sProdCrManualForeclosureRecentFileMon_rep = sProdCrManualForeclosureRecentFileMon.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentFileYr_rep = sProdCrManualForeclosureRecentFileYr.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep = sProdCrManualForeclosureRecentSatisfiedMon.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep = sProdCrManualForeclosureRecentSatisfiedYr.SelectedItem.Value;
            dataLoan.sProdCrManualForeclosureRecentStatusT = (E_sProdCrManualDerogRecentStatusT)Tools.GetDropDownListValue(sProdCrManualForeclosureRecentStatusT);

            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;

        }
        /// <summary>
        /// Pulls the credit and save. 
        /// </summary>
        /// <param name="isReissue"></param>
        /// <param name="isOrderNew"></param>
        /// <param name="isUpgrade"></param>
        private void PullCreditAndSave(bool isReissue, bool isOrderNew, bool isUpgrade)
        {
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBAddr = aBAddr.Text;
            dataApp.aBCity = aBCity.Text;
            dataApp.aBState = aBState.Value;
            dataApp.aBZip = aBZip.Text;
            if (isOrderNew || isUpgrade)
            {
                dataApp.aBDob_rep = aBDob.Text;
                dataApp.aCDob_rep = aCDob.Text;
            }
            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;

            string protocolID = CreditProtocol.SelectedItem.Value;

            TemporaryCRAInfo craInfo = (TemporaryCRAInfo)m_craHash[protocolID];
            if (craInfo == null)
            {
                return;// 9/20/2004 dd - ERROR. This CRA info is missing. -- Moved By antonio was in empty else statement 
            }
            Guid craID = new Guid(protocolID);

            // 12/27/2006 nw - OPM 5083 - Remember login name
            CreditReportUtilities.UpdateLastUsedCra(PriceMyLoanUser.BrokerId, PriceMyLoanUser.LastUsedCreditLoginNm, RememberLoginNameCB.Checked ? LoginName.Text : "", null, null, PriceMyLoanUser.LastUsedCreditProtocolID, craID, UserID);

            CreditRequestData requestData = CreateRequestData(LoanID, dataApp.aAppId, craID);
            requestData.ReferenceNumber = dataLoan.sLNm;

            int maxAttemptCount = 20;
            int pollingIntervalInSeconds = 10;
            var requestHandler = new CreditReportRequestHandler(requestData, principal: PriceMyLoanUser, shouldReissueOnResponseNotReady: true, retryIntervalInSeconds: pollingIntervalInSeconds, maxRetryCount: maxAttemptCount, loanId: this.LoanID);
            if (ConstStage.DisableCreditOrderingViaBJP)
            {
                // TODO CREDIT_BJP: Temporary code. Remove once we have confirmed credit is working via the BJP.
                this.OrderCreditSynchronously(requestHandler, dataApp, requestData, maxAttemptCount, isReissue, isUpgrade);
                return;
            }

            var initialResult = requestHandler.SubmitToProcessor();
            if (initialResult.Status == CreditReportRequestResultStatus.Failure)
            {
                var errorMessage = string.Join(", ", initialResult.ErrorMessages);
                SetErrorMessage(errorMessage);
                throw new NonCriticalException(errorMessage);
            }
            else
            {
                Guid publicJobId = initialResult.PublicJobId.Value;
                bool hasReceivedResults = false;
                for (int attemptCount = 0; attemptCount < maxAttemptCount; attemptCount++)
                {
                    var finalResult = CreditReportRequestHandler.CheckForRequestResults(publicJobId);
                    if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                    {
                        var errorMessage = string.Join(", ", finalResult.ErrorMessages);
                        SetErrorMessage(errorMessage);
                        throw new NonCriticalException(errorMessage);
                    }
                    else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                    {
                        ICreditReportResponse creditResponse = finalResult.CreditReportResponse;
                        CreditRequestData finalCreditRequestData = finalResult.CreditRequestData;

                        if (null == creditResponse)
                        {
                            SetErrorMessage("If are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.");
                            throw new NonCriticalException("Probably timeout issue. Already send email to developer.");
                        }

                        if (creditResponse.HasError)
                        {
                            // 7/28/2004 dd - Need to do something.
                            // 12/15/2006 nw - OPM 8360 - Provide a more descriptive message for FNMA "User not authenticated" error
                            if (creditResponse.ErrorMessage.ToLower() == "user not authenticated")
                            {
                                if (PriceMyLoanConstants.IsEmbeddedPML)
                                {
                                    SetErrorMessage("The FannieMae DU login information is incorrect. Please confirm your FannieMae DU login and password. To verify your account, contact FannieMae Customer Care at (877)722-6757.");
                                }
                                else
                                {
                                    SetErrorMessage("The FannieMae DU login information is incorrect. Please contact your lender administrator and ask them to confirm their FannieMae DU login and password.");
                                }
                            }
                            else
                            {
                                SetErrorMessage(creditResponse.ErrorMessage);
                            }

                            throw new NonCriticalException("Error with credit report. Safely Ignore.");
                        }

                        E_CreditReportAuditSourceT requestT = E_CreditReportAuditSourceT.OrderNew;
                        if (isReissue)
                        {
                            requestT = E_CreditReportAuditSourceT.Reissue;
                        }
                        if (isUpgrade)
                        {
                            requestT = E_CreditReportAuditSourceT.Upgrade;
                        }

                        if (CreditReportUtilities.IsBorrowerInformationMatch(LoanID, dataApp.aAppId, aBFirstNm.Text, aBMidNm.Text, aBLastNm.Text, aBSuffix.Text, aBSsn.ClearText, aCFirstNm.Text, aCMidNm.Text, aCLastNm.Text, aCSuffix.Text, aCSsn.ClearText, creditResponse) || (isReissue && String.IsNullOrEmpty(dataApp.aBNm)))
                        {
                            sFileVersion = CreditReportUtilities.SaveXmlCreditReport(creditResponse, PriceMyLoanUser, LoanID, dataApp.aAppId, finalCreditRequestData.ActualCraID, finalCreditRequestData.ProxyId, requestT);
                            ImportLiabilities(true);
                        }
                        else
                        {
                            SetErrorMessage(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            throw new NonCriticalException(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                        }

                        hasReceivedResults = true;
                        break;
                    }
                    else
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(pollingIntervalInSeconds * 1000);
                    }
                }

                if (!hasReceivedResults)
                {
                    SetErrorMessage(ErrorMessages.AsyncReportTakingTooLongToProcess);
                    throw new CBaseException(ErrorMessages.AsyncReportTakingTooLongToProcess, "Async credit report request take too long to process.");
                }
            }
        }

        private void OrderCreditSynchronously(CreditReportRequestHandler requestHandler, CAppData dataApp, CreditRequestData requestData, int maxAttemptCount, bool isReissue, bool isUpgrade)
        {
            bool hasCompleted = false;
            for (int attemptCount = 0; attemptCount < maxAttemptCount; attemptCount++)
            {
                var finalResult = requestHandler.SubmitSynchronously();
                if (finalResult.Status == CreditReportRequestResultStatus.Failure)
                {
                    var errorMessage = string.Join(", ", finalResult.ErrorMessages);
                    SetErrorMessage(errorMessage);
                    throw new NonCriticalException(errorMessage);
                }
                else if (finalResult.Status == CreditReportRequestResultStatus.Completed)
                {
                    ICreditReportResponse creditResponse = finalResult.CreditReportResponse;
                    if (null == creditResponse)
                    {
                        SetErrorMessage("If are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.");
                        throw new NonCriticalException("Probably timeout issue. Already send email to developer.");
                    }

                    if (creditResponse.HasError)
                    {
                        // 7/28/2004 dd - Need to do something.
                        // 12/15/2006 nw - OPM 8360 - Provide a more descriptive message for FNMA "User not authenticated" error
                        if (creditResponse.ErrorMessage.ToLower() == "user not authenticated")
                        {
                            if (PriceMyLoanConstants.IsEmbeddedPML)
                            {
                                SetErrorMessage("The FannieMae DU login information is incorrect. Please confirm your FannieMae DU login and password. To verify your account, contact FannieMae Customer Care at (877)722-6757.");
                            }
                            else
                            {
                                SetErrorMessage("The FannieMae DU login information is incorrect. Please contact your lender administrator and ask them to confirm their FannieMae DU login and password.");
                            }
                        }
                        else
                        {
                            SetErrorMessage(creditResponse.ErrorMessage);
                        }

                        throw new NonCriticalException("Error with credit report. Safely Ignore.");
                    }

                    if (creditResponse.IsReady)
                    {
                        E_CreditReportAuditSourceT requestT = E_CreditReportAuditSourceT.OrderNew;
                        if (isReissue)
                        {
                            requestT = E_CreditReportAuditSourceT.Reissue;
                        }
                        if (isUpgrade)
                        {
                            requestT = E_CreditReportAuditSourceT.Upgrade;
                        }

                        if (CreditReportUtilities.IsBorrowerInformationMatch(LoanID, dataApp.aAppId, aBFirstNm.Text, aBMidNm.Text, aBLastNm.Text, aBSuffix.Text, aBSsn.ClearText, aCFirstNm.Text, aCMidNm.Text, aCLastNm.Text, aCSuffix.Text, aCSsn.ClearText, creditResponse) || (isReissue && String.IsNullOrEmpty(dataApp.aBNm)))
                        {
                            sFileVersion = CreditReportUtilities.SaveXmlCreditReport(creditResponse, PriceMyLoanUser, LoanID, dataApp.aAppId, requestData.ActualCraID, requestData.ProxyId, requestT);
                            ImportLiabilities(true);
                        }
                        else
                        {
                            SetErrorMessage(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            throw new NonCriticalException(ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                        }

                        hasCompleted = true;
                        break;
                    }
                    else if (requestData.CreditProtocol == CreditReportProtocol.Mcl)
                    {
                        requestData.ReportID = creditResponse.ReportID;
                        requestData.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                    }
                    //av 1212008 Removed lender mapping check heres the guid new way should set up the right protocol e04d2398-e614-42a0-9a50-3276c14ab740 
                    else if (requestData.CreditProtocol == CreditReportProtocol.InfoNetwork)
                    {
                        // 8/9/2005 dd - Information Network support async request.
                        requestData.ReportID = creditResponse.ReportID;
                        requestData.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
                    }

                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10000); // Wait for 10 seconds before reissue. 
                }
            }

            if (!hasCompleted)
            {
                SetErrorMessage(ErrorMessages.AsyncReportTakingTooLongToProcess);
                throw new CBaseException(ErrorMessages.AsyncReportTakingTooLongToProcess, "Async credit report request take too long to process.");
            }
        }

        private void ImportLiabilities(bool skip0Balance) 
        {
            CPageData dataLoan = new CPmlImportLiabilityData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(0);
            
            if (Request.Form["DeleteLiabilities"] == "1") 
            {
                dataApp.aLiaCollection.ClearAll();
            }

            dataApp.ImportLiabilitiesFromCreditReport(skip0Balance, true);

			// 2/27/2007 nw - OPM 10630 - import public records
			dataApp.aPublicRecordCollection.ClearAll();
			dataApp.aPublicRecordCollection.Flush();
			dataApp.ImportPublicRecordsFromCreditReport();

            dataLoan.Save();
            sFileVersion = dataLoan.sFileVersion;
        }

        public void SaveData() 
        {
            DetermineBrokerDULoginInfo( this.CurrentBroker);

            // 7/8/2005 dd - Transform data so that data modified in LendingQB will populate correctly in PML.
            // 10/12/2005 dd - Transform data is take place in LoadData
            // TransformData();

            bool isReissue = CreditAction_Reissue.Checked;
            bool isOrderNew = CreditAction_OrderNew.Checked;
			bool isUpgrade = CreditAction_Upgrade.Checked;
         
            if (CreditAction_ManualCredit.Checked) 
            {
                SaveManualCreditReport();
            }
            else if (isReissue || isOrderNew || isUpgrade) 
            {
                PullCreditAndSave(isReissue, isOrderNew, isUpgrade);
            }

        }

        /// <summary>
        /// Sets the borrower info into the given object. Sets either borrower or co based on isCoborrower
        /// if its the main borrower it will also set his address.
        /// </summary>
        /// <param name="borrower"></param>
        /// <param name="IsCoborrower"></param>
        private void SetBorrowerInfo(LendersOffice.CreditReport.BorrowerInfo borrower, bool isCoborrower, CreditReportProtocol protocol) 
        {

            borrower.FirstName = isCoborrower ? aCFirstNm.Text : aBFirstNm.Text;
            borrower.MiddleName = isCoborrower ? aCMidNm.Text : aBMidNm.Text;
            borrower.LastName = isCoborrower ? aCLastNm.Text : aBLastNm.Text;
            borrower.Suffix = isCoborrower ? aCSuffix.Text : aBSuffix.Text;    
            borrower.DOB = isCoborrower ? aCDob.Text : aBDob.Text;

            CPageData dataLoan = new CEnterCreditData(LoanID);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);

            if (!isCoborrower)
            {
                //opm 27866 fs 03/02/09
                //The SSN might be masked; the value on file is only saved if the user edited the SSNbox
                borrower.Ssn = (aBSsn.IsUpdated) ? aBSsn.ClearText : dataApp.aBSsn;
            }
            else
            {
                borrower.Ssn = (aCSsn.IsUpdated) ? aCSsn.ClearText : dataApp.aCSsn;

                // OPM 451238 - Set mailing address for coborrower for protocols that require coborrower address.
                if (protocol == CreditReportProtocol.CSD || protocol == CreditReportProtocol.InformativeResearch)
                {
                    if (!string.IsNullOrEmpty(dataApp.aCAddrMail) || !string.IsNullOrEmpty(dataApp.aCCityMail)
                        || !string.IsNullOrEmpty(dataApp.aCStateMail) || !string.IsNullOrEmpty(dataApp.aCZipMail))
                    {
                        Address coMailingAddress = new Address();
                        coMailingAddress.StreetAddress = dataApp.aCAddrMail;
                        coMailingAddress.City = dataApp.aCCityMail;
                        coMailingAddress.State = dataApp.aCStateMail;
                        coMailingAddress.Zipcode = dataApp.aCZipMail;
                        borrower.MailingAddress = coMailingAddress;
                    }
                }

                return;
            }
            
            Address address = new Address();
            address.City = aBCity.Text;
            address.State = aBState.Value;
            address.Zipcode = aBZip.Text;

            try
            {
                address.ParseStreetAddress(aBAddr.Text);
            }
            catch( Exception e ) 
            {
                address.StreetAddress = aBAddr.Text;
                Tools.LogWarning("Unable to parse borrower address: " + aBAddr.Text, e);
            }
            finally
            {
                borrower.CurrentAddress = address;
            }

            // Set Mailing address
            Address mailingAddress = new Address();
            mailingAddress.StreetAddress = dataApp.aAddrMail;
            mailingAddress.City = dataApp.aCityMail;
            mailingAddress.State = dataApp.aStateMail;
            mailingAddress.Zipcode = dataApp.aZipMail;
            borrower.MailingAddress = mailingAddress;
        }
        
        private CreditRequestData CreateRequestData(Guid loanId, Guid appId, Guid craId) 
        {
            bool isNewRequest = CreditAction_OrderNew.Checked;
			bool isUpgradeRequest = CreditAction_Upgrade.Checked;

            CreditRequestData data = CreditRequestData.Create(loanId, appId);

            data.IncludeEquifax = IsEquifax.Checked;
            data.IncludeExperian = IsExperian.Checked;
            data.IncludeTransUnion = IsTransUnion.Checked;
            data.ReportID = isNewRequest ? "" : ReportID.Text;
            data.RequestingPartyName = PriceMyLoanUser.DisplayName;

            data.LoginInfo.UserName = LoginName.Text;
            data.LoginInfo.Password = Password.Text;

            data.UpdateCRAInfoFrom(craId, BrokerID, true);

            SetBorrowerInfo(data.Borrower, false, data.CreditProtocol);
            SetBorrowerInfo(data.Coborrower, true, data.CreditProtocol); 

            //If its lender mapping instnat view wont work. 
            if (!isNewRequest && !isUpgradeRequest && !data.WasLenderMapping)
            {
                data.InstantViewID = InstantViewID.Text;
            }
            switch (data.CreditProtocol)
            {
                case CreditReportProtocol.SharperLending:
                    if (isUpgradeRequest)
                    {
                        data.IncludeEquifax = true;
                        data.IncludeExperian = true;
                        data.IncludeTransUnion = true;
                    }
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;                   
                    break;
                case CreditReportProtocol.KrollFactualData:
                    data.LoginInfo.AccountIdentifier = FD_OfficeCode.Text + FD_ClientCode.Text;
                    break;
                case CreditReportProtocol.FannieMae:
                    data.LoginInfo.FannieMaeCreditProvider = data.Url; 
                    if (m_hasBrokerDUInfo)
                    {
                        data.LoginInfo.FannieMaeMORNETUserID = m_brokerDUUserID;
                        data.LoginInfo.FannieMaeMORNETPassword = m_brokerDUPassword;
                    }
                    else if ( ! data.WasLenderMapping ) //lender mapping should set the password and username cause ui doesnt show that info when lendermapping is selected.
                    {
                        data.LoginInfo.FannieMaeMORNETUserID = DUUserID.Text;
                        data.LoginInfo.FannieMaeMORNETPassword = DUPassword.Text;
                    }
                    break;
                case CreditReportProtocol.Mcl :
                    data.IsPdfViewNeeded = CreditReportUtilities.IsCraPdfEnabled(CreditReportProtocol.Mcl, BrokerID, data.ActualCraID);
                    if (isUpgradeRequest)
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Upgrade;
                    }
                    else if (isNewRequest)
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.New;
                    }
                    else
                    {
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                    }
                    break;
                case CreditReportProtocol.CBC:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    break;
                case CreditReportProtocol.InformativeResearch:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    break;
                case CreditReportProtocol.Credco:
                    if (isUpgradeRequest)
                    {
                        data.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.Upgrade;
                    }
                    break;
                default :
                    data.LoginInfo.AccountIdentifier = AccountIdentifier.Text;
                    break;
            
            }

            data.IsMortgageOnly = IsMortgageOnlyCB.Checked;
            return data;
        }
		public void SetErrorMessage( string message )
		{
			// OPM 20593
            ErrorMessageKey = Guid.NewGuid();
			AutoExpiredTextCache.AddToCache( message, TimeSpan.FromMinutes(5), ErrorMessageKey );		
		}
		public void SetAdditionalQueryStringValues( PmlMainQueryStringManager qsManager )
		{
			if ( ErrorMessageKey != Guid.Empty )
			{
				qsManager.AddPair( "errorMsg", ErrorMessageKey.ToString() );
			}
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
