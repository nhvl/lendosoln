using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using PriceMyLoan.Security;
using LendersOffice.Common;
using PriceMyLoan.Common;

namespace PriceMyLoan.main
{

	public partial class LiabilityList : PriceMyLoan.UI.BasePage
	{
        protected Guid m_currentAppId = Guid.Empty;

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        
        protected void PageLoad(object sender, System.EventArgs e)
        {

            m_currentAppId = RequestHelper.GetGuid("appid"); ;

            if (!Page.IsPostBack)
            {
                CPageData dataLoan = new CLiaData(LoanID, false);
                dataLoan.InitLoad();
       
                sFileVersion = dataLoan.sFileVersion;
                CAppData dataApp = dataLoan.GetAppData(m_currentAppId);

                ILiaCollection recordList = dataApp.aLiaCollection;

                this.SetSpecialLiabilities(recordList);

                m_dg.DataSource = recordList.SortedView;
                m_dg.DataBind();
            }
        }
        protected string displayOwnerType(string type) 
        {
            try
            {
                return CLiaFields.DisplayStringOfOwnerT( (E_LiaOwnerT) int.Parse( type ) );
            }
            catch
            {	return "B"; }// Make unknown type and default become borrower.
        }

        protected string displayLiabilityType(string type) 
        {
            if (string.IsNullOrEmpty(type))
            {
                return string.Empty;
            }
            int v;
            if (int.TryParse(type, out v))
            {
                E_DebtRegularT debtType = (E_DebtRegularT)v;
                return CLiaRegular.DisplayStringOfDebtT(debtType);
            }
            return string.Empty;
        }
        protected string displayMoneyString(string value) 
        {

            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            return value.Replace("*", "");
        }

        protected HtmlInputCheckBox CreatePayoffCheckbox(object dataItem)
        {
            DataRowView row = dataItem as DataRowView;

            HtmlInputCheckBox checkbox = null;

            if (row != null)
            {
                string recordId = row["RecordId"].ToString();
                bool willBePaidOff = row["WillBePdOff"].ToString() == "True";


                checkbox = new HtmlInputCheckBox();
                checkbox.ID = "pdoff_" + recordId;
                checkbox.Name = checkbox.ID;
                checkbox.Checked = willBePaidOff;
                checkbox.Attributes.Add("onclick", "f_onPaidOffCheck(this);");
            }
            return checkbox;
        }
        protected HtmlControl CreateUsedInRatioCheckbox(object dataItem)
        {
            DataRowView row = dataItem as DataRowView;

            HtmlControl control = null;
            if (row != null)
            {
                E_DebtT debtT = E_DebtT.Other;

                try
                {
                    debtT = (E_DebtT)int.Parse(row["DebtT"].ToString());
                }
                catch { }

                if (debtT == E_DebtT.Mortgage)
                {
                    control = new HtmlGenericControl("span");
                    ((HtmlGenericControl)control).InnerText = "***";
                }
                else
                {
                    bool notUsedInRatio = row["NotUsedInRatio"].ToString() == "True";
                    string recordId = row["RecordId"].ToString();
                    HtmlInputCheckBox cb = new HtmlInputCheckBox();
                    cb.Checked = !notUsedInRatio;
                    cb.ID = "ratio_" + recordId;
                    cb.Name = cb.ID;
                    control = cb;
                }
            }

            return control;

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion



        /// <summary>
        /// Sets the special liabilities for the page.
        /// </summary>
        /// <param name="recordList">
        /// The list of records.
        /// </param>
        private void SetSpecialLiabilities(ILiaCollection recordList)
        {
            var alimony = recordList.GetAlimony(false);
            if (alimony != null)
            {
                this.Alimony_OwedTo.Value = alimony.OwedTo;
                this.Alimony_MonthlyPayment.Text = alimony.Pmt_rep;
                this.Alimony_RemainingMonths.Value = alimony.RemainMons_rep;
                this.Alimony_NotUsedInRatio.Checked = alimony.NotUsedInRatio;
            }

            var childSupport = recordList.GetChildSupport(false);
            if (childSupport != null)
            {
                this.ChildSupport_OwedTo.Value = childSupport.OwedTo;
                this.ChildSupport_MonthlyPayment.Text = childSupport.Pmt_rep;
                this.ChildSupport_RemainingMonths.Value = childSupport.RemainMons_rep;
                this.ChildSupport_NotUsedInRatio.Checked = childSupport.NotUsedInRatio;
            }

            var jobRelatedExpense = recordList.GetJobRelated1(false);
            if (jobRelatedExpense != null)
            {
                this.JobRelated1_ExpenseDescription.Value = jobRelatedExpense.ExpenseDesc;
                this.JobRelated1_MonthlyPayment.Text = jobRelatedExpense.Pmt_rep;
                this.JobRelated1_NotUsedInRatio.Checked = jobRelatedExpense.NotUsedInRatio;
            }

            jobRelatedExpense = recordList.GetJobRelated2(false);
            if (jobRelatedExpense != null)
            {
                this.JobRelated2_ExpenseDescription.Value = jobRelatedExpense.ExpenseDesc;
                this.JobRelated2_MonthlyPayment.Text = jobRelatedExpense.Pmt_rep;
                this.JobRelated2_NotUsedInRatio.Checked = jobRelatedExpense.NotUsedInRatio;
            }
        }

        private CPageData SaveData() 
        {
            CPageData dataLoan = new CLiaData(LoanID, true);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitSave(sFileVersion);
 
            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);
            ILiaCollection recordList = dataApp.aLiaCollection;


            int count = recordList.CountRegular;
            for (int i = 0; i < count; i++) 
            {
                ILiabilityRegular record = recordList.GetRegularRecordAt(i);
                bool willBePaidOff = Request.Form["pdoff_" + record.RecordId] == "on";
                bool usedInRatio = Request.Form["ratio_" + record.RecordId] == "on";

                if (record.WillBePdOff != willBePaidOff) 
                {
                    // Use friendly description and value
					//-jM OPM 17403 added login name
                    record.FieldUpdateAudit(PriceMyLoanUser.DisplayName, PriceMyLoanUser.LoginNm, "Pd Off", willBePaidOff ? "Yes" : "No");
                }
                if (record.NotUsedInRatio == usedInRatio) 
                {
                    record.FieldUpdateAudit(PriceMyLoanUser.DisplayName, PriceMyLoanUser.LoginNm, "Used In Ratio", usedInRatio ? "Yes" : "No");
                }
                    
                record.WillBePdOff = willBePaidOff;
                record.NotUsedInRatio = !usedInRatio;
                record.Update();
            }

            var alimony = recordList.GetAlimony(true);
            alimony.OwedTo = this.Alimony_OwedTo.Value;
            alimony.Pmt_rep = this.Alimony_MonthlyPayment.Text;
            alimony.RemainMons_rep = this.Alimony_RemainingMonths.Value;
            alimony.NotUsedInRatio = this.Alimony_NotUsedInRatio.Checked;
            alimony.Update();

            var childSupport = recordList.GetChildSupport(true);
            childSupport.OwedTo = this.ChildSupport_OwedTo.Value;
            childSupport.Pmt_rep = this.ChildSupport_MonthlyPayment.Text;
            childSupport.RemainMons_rep = this.ChildSupport_RemainingMonths.Value;
            childSupport.NotUsedInRatio = this.ChildSupport_NotUsedInRatio.Checked;
            childSupport.Update();

            var jobRelated = recordList.GetJobRelated1(true);
            jobRelated.ExpenseDesc = this.JobRelated1_ExpenseDescription.Value;
            jobRelated.Pmt_rep = this.JobRelated1_MonthlyPayment.Text;
            jobRelated.NotUsedInRatio = this.JobRelated1_NotUsedInRatio.Checked;
            jobRelated.Update();

            jobRelated = recordList.GetJobRelated2(true);
            jobRelated.ExpenseDesc = this.JobRelated2_ExpenseDescription.Value;
            jobRelated.Pmt_rep = this.JobRelated2_MonthlyPayment.Text;
            jobRelated.NotUsedInRatio = this.JobRelated2_NotUsedInRatio.Checked;
            jobRelated.Update();

            dataLoan.Save();
            return dataLoan;
        }
        protected void m_btnOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                CPageData dataLoan = SaveData();

                CAppData dataApp = dataLoan.GetAppData(m_currentAppId);
                string aTransmOMonPmtPe = dataApp.aTransmOMonPmtPe_rep;
                int fileVersion = dataLoan.sFileVersion;
                ClientScript.RegisterStartupScript(this.GetType(), "autoclose", string.Format("window.parent.LQBPopup.Return({{ fileVersion: {0}, aTransmOMonPmtPe: '{1}'}});", fileVersion, aTransmOMonPmtPe), true);
            }
            catch (VersionMismatchException exc)
            {
                MainFrame.Visible = false;
                m_btnOK.Visible = false;
                m_errorMessage.Text = exc.UserMessage;
                spanInformation.Visible = false;
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                MainFrame.Visible = false;
                m_btnOK.Visible = false;
                m_errorMessage.Text = exc.UserMessage;
                spanInformation.Visible = false;
            }        
        }
	}
}
