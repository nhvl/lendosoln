using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.ObjLib.Resource;
using LendersOffice.RatePrice;
using PriceMyLoan.Security;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Xml;
using System.Xml.Xsl;

namespace PriceMyLoan.main
{

    public partial class EmailCertificateService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        private string m_pmlLenderSiteId = "";
        private string XsltFileLocation 
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.PmlCertificateXslt, this.BrokerID); }
        }



        private Guid BrokerID 
        {
            get { return ((PriceMyLoanPrincipal) Page.User).BrokerId; }
        }
        public XsltArgumentList XsltParams(string notes)
        {
            string css = ResourceManager.Instance.GetResourceContents(ResourceType.CertificateCss, BrokerID);

            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("VirtualRoot", "", Tools.VRoot);
            args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
            args.AddParam("GenericStyle", "", css);
            args.AddParam("Email", "", "True");
            args.AddParam("IsMoreDetailCert", "", "True");
            args.AddParam("Notes", "", notes);
            if (LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerDB.HidePmlCertSensitiveFields)
            {
                args.AddParam("HideSensitiveFields", "", "True");
            }
            return args;
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "SendEmail":
                    SendEmail();
                    break;
            }
        }

        private string BuildRequestedRate(string rate, string fee, string id) 
        {
            return string.Format("{0}:{1}:{2}", rate, fee, id);
        }

        private void SendEmail() 
        {
            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;

            Guid loanID = GetGuid("LoanID");
            Guid productID = GetGuid("ProductID");
            string recipient = GetString("EmailAddr").Replace(" ", "");
            string ccrecipent = GetString("CCAddr").Replace(" ", "");
		
            string subject = GetString("Subject");
            E_sLienQualifyModeT sLienQualifyModeT = (E_sLienQualifyModeT) GetInt("sLienQualifyModeT");
            string fromEmailAddr = GetString("FromEmailAddr");
            string note = AspxTools.HtmlString(GetString("Note", ""));
            string version = GetString("Version");
            string uniqueChecksum = GetString("uniquechecksum", "");

            string productRate = GetString("productRate");
            string productPoint = GetString("productPoint");
            string productRawId = GetString("productRawId");
            string requestedRate = this.BuildRequestedRate(productRate, productPoint, productRawId);
 
            if (note != "") 
            {
                note = string.Format("Note from {0}:<br><br>{1}", principal.DisplayName, note.Replace(Environment.NewLine, "<br>"));
            }
            SetPmlLenderSiteId(loanID);

            Guid sBranchId = Guid.Empty;

            bool hasLogo = AttachLogo(loanID, out sBranchId);

            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogo(new Guid(m_pmlLenderSiteId), sBranchId);

            string logoId = $"_logo_{DateTime.Now.Ticks}";
            byte[] logo = null;
            if (hasLogo)
            {
                try
                {
                    logo = FileDBTools.ReadData(E_FileDB.Normal, m_pmlLenderSiteId.ToString().ToLower() + ".logo.gif");
                }
                catch (FileNotFoundException)
                {
                    hasLogo = false;
                }
            }

            string bodyContent = "";
            string auditContent = "";

            ArrayList productIDList = new ArrayList();
            productIDList.Add(productID);

            string sTempLpeTaskMessageXml = "";

            CPageData dataLoan = new CCheckFor2ndLoan(loanID);
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            if (dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct) 
            {
                // ^ If we are only going to rerun the registered program, we should have the
                //   task message XML on file, so we don't need to construct it for the 2nd.
                sTempLpeTaskMessageXml = dataLoan.sTempLpeTaskMessageXml;
            }
            else if (sLienQualifyModeT == E_sLienQualifyModeT.OtherLoan)
            {
                // ^ Else if that is not the case and we are running the combo 2nd, we need
                //   to dummy up the 1st lien task message XML to re-apply the selected 1st
                //   to base the 2nd lien on.

                string firstLienNoteRate = GetString("FirstLienNoteRate", GetString("1stRate"));
                string firstLienPoint = GetString("FirstLienPoint", GetString("1stPoint"));
                Guid firstLienLpId = GetGuid("FirstLienLpId", GetGuid("1stProductID"));

                string requested1stLienRate = string.Format($"{firstLienNoteRate}:{firstLienPoint}:{string.Empty}");

                sTempLpeTaskMessageXml = DistributeUnderwritingEngine.GenerateTemporaryAcceptFirstLoanMessage(
                    principal: principal,
                    loanId: loanID,
                    productId: firstLienLpId,
                    requestedRate: requested1stLienRate,
                    lpePriceGroupId: dataLoan.sProdLpePriceGroupId,
                    version: string.Empty,
                    debugResultStartTicks: "0",
                    renderResultModeT: E_RenderResultModeT.Regular);
            }


            Guid lpePriceGroupId = dataLoan.sProdLpePriceGroupId;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            Guid requestID = DistributeUnderwritingEngine.SubmitToEngineForRenderCertificate(principal, loanID, productID, requestedRate,
                sLienQualifyModeT, lpePriceGroupId, sTempLpeTaskMessageXml, version, E_RenderResultModeT.Regular, E_sPricingModeT.RegularPricing, uniqueChecksum, GetString("parId", "")
                , options);
            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestID);

            if (resultItem.IsErrorMessage) 
            {
                SetResult("ErrMsg", resultItem.ErrorMessage);

                return;
            }
            ArrayList results = resultItem.Results;

            if (null == results)
                throw new CBaseException(PriceMyLoan.UI.Common.PmlErrorMessages.CertificateNullResult, "DistributeUnderwritingEngine.RetrieveResults return null for RequestID = " + requestID);

            XmlDocument _doc = (XmlDocument)results[0];
            _doc = UnderwritingResultItem.DedupAdjustmentDescription(_doc);
            if (hasLogo)
            {
                bodyContent = GenerateContent(_doc, $"cid:{logoId}", note);
                auditContent = GenerateContent(_doc, "LogoPL.aspx?id=" + m_pmlLenderSiteId, note);
            }
            else
            {
                bodyContent = auditContent = GenerateContent(_doc, null, note);
            }

            var cbe = new LendersOffice.Email.CBaseEmail(BrokerID)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = fromEmailAddr,
                To = recipient,
                Subject = subject,
                Message = bodyContent,
                IsHtmlEmail = true,
                Bcc = ccrecipent
            };
            if (hasLogo && logo.Length > 0)
            {
                cbe.AddAttachment(logoId, logo, true);
            }
            cbe.Send();
            AbstractAuditItem audit = new PmlCertificateEmailAuditItem((LendersOffice.Security.AbstractUserPrincipal) Page.User, fromEmailAddr, recipient, subject, auditContent);
            AuditManager.RecordAudit(loanID, audit);

        }

        private string GenerateContent(XmlDocument doc, string logoSource, string notes)
        {
            using (MemoryStream stream = new MemoryStream(10000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                XmlWriter writer = XmlWriter.Create(stream, writerSettings);

                doc.WriteTo(writer);

                writer.Flush();

                stream.Seek(0, SeekOrigin.Begin);

                XsltArgumentList xsltParams = XsltParams(notes);
                if (false == string.IsNullOrEmpty(logoSource))
                {
                    xsltParams.AddParam("LogoSource", "", logoSource);
                }
                return XslTransformHelper.Transform(XsltFileLocation, stream, xsltParams);
            }
        }

        private void SetPmlLenderSiteId(Guid sLId) 
        {
            // 11/10/2004 dd - Theoretically, PmlLenderSiteId should be in loan product.
            // This way we allow independent broker to run qualification against multiple lender.
            // However we are not there yet. Retrieve from current broker.
            BrokerDB broker = BrokerDB.RetrieveById(this.BrokerID);

            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(broker.PmlSiteID, sLId);
        }

        /// <summary>
        /// Determines if we need to show the logo on the email cert based on   IsDisplayNmModified
        /// We need to check outside because we dont want to attach the logo to the email if its not going to display it.
        /// </summary>
        /// <param name="sLId"></param>
        /// <returns></returns>
        private bool AttachLogo(Guid sLId, out Guid sBranchId)
        {
            CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(EmailCertificateService));
            data.AllowLoadWhileQP2Sandboxed = true;
            data.InitLoad();

            sBranchId = data.sBranchId;

            BranchDB db = new BranchDB(data.sBranchId, data.sBrokerId);
            db.Retrieve();

            return !db.IsDisplayNmModified;
        }

	}
}
