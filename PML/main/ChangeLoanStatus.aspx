﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeLoanStatus.aspx.cs" Inherits="PriceMyLoan.main.ChangeLoanStatus" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Change Loan Status</title>
    <style>
        .warning
        {
            color : #EA1C0D;
        }
        .divButtons
        {
            float : right;
        }
        .topTable td
        {
            white-space : nowrap;
            width : 15%;
        }
        .bottomTable {
            margin-bottom: 1em;
        }
        .bottomTable td
        {
            white-space : nowrap;
            padding-bottom: 0;
        }
        .spacer {
            /*
                In IE, the table created by the radio button list
                has a top margin that cannot be adjusted within impacting
                the label column to the left. This spacer element will force
                the browser to correctly align the table.
            */
            height: 0;
        }
        .table.h-loan-status {
            max-height: 480px;
            overflow-y: auto;
            display: inline-block;
        }
    </style>
</head>
<body class="wrap-dialogs">
<script>
    $(function () {
        const popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
        const iframeSelf = new IframeSelf();
        iframeSelf.popup = popup;

        function onOK() {
            const selectedStatus = $("input[type='radio']:checked");

            iframeSelf.resolve({ OK: true, status: selectedStatus.val(), statusName: $('label[for="'+selectedStatus.attr("id")+'"]').text() });
        }
        function onCancel() {
            iframeSelf.resolve({ OK: false });
        }

        $(".change").click(onOK);
        $(".cancel").click(onCancel);
        $(".modal-header button.close").click(onCancel);

        $("input[type='radio']").click(function () {
            toggleChangeButton();
        });

        toggleChangeButton();
        TPOStyleUnification.Components();

        function toggleChangeButton() {
            const selected = $("input[type='radio']:checked");

            $(".warning").toggle(selected.length == 0);
            $(".change").prop("disabled", selected.length == 0);
        }
    });
</script>

<form id="form1" runat="server">
    <div lqb-popup>
        <div class="modal-header">
            <button type="button" class="close"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title">Change Loan Status</h4>
        </div>
        <div class="modal-body">
            <asp:PlaceHolder runat="server" ID="HeaderInfo">
                <table class="topTable">
                    <tr>
                    <td><label class="FieldLabel">Loan Number: </label><ml:EncodedLiteral runat="server" ID="sLNm"/></td>
                    <td><label class="FieldLabel">Borrower Name: </label><ml:EncodedLiteral runat="server" ID="aBNm"/></td>
                    </tr>
                    <tr>
                    <td><label class="FieldLabel">Loan Amount: </label><ml:EncodedLiteral runat="server" ID="sLAmtCalc"/></td>
                    <td><label class="FieldLabel">Property Address: </label><ml:EncodedLiteral runat="server" ID="sSpAddr"/></td>
                    </tr>
                </table>
                <hr />
            </asp:PlaceHolder>
            <div class="table width-full h-loan-status">
                <div>
                    <div class="text-grey">
                        <label class="nowrap">Current Loan Status:</label>
                    </div>
                    <div>
                        <ml:EncodedLiteral runat="server" ID="sStatusT"/>
                    </div>
                </div>
                <div>
                    <div class="text-grey text-right">
                        <label class="nowrap">New Loan Status:</label>
                    </div>
                    <div>
                        <div class="spacer">&nbsp;</div>
                        <asp:RadioButtonList runat="server" ID="RadioList"></asp:RadioButtonList>
                        <span class="warning" runat="server" id="statusErrMsg"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="cancel btn btn-flat" type="button">Cancel</button>
            <button class="change btn btn-flat" type="button">Change Status</button>
        </div>
    </div>
</form>
</body>
</html>
