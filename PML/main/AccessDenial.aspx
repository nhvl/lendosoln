<%@ Page language="c#" Codebehind="AccessDenial.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.AccessDenial" validateRequest=true%>
<%@ Import namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>AccessDenial</title>
	</HEAD>

	<body  onload="f_displayButton();">
	
		<form id="AccessDenial" method="post" runat="server">
		    <table id="Table2" style="display:none" cellSpacing="3" cellPadding="0" width="100%" border="0">
			<tr id="Tr1" >
			<td colspan="2" align="right">
			    <input  style="float:right" type="button" class="ButtonStyle" onclick="f_logout();" value="LOG OFF">
			    </td>
			</tr>
		    </table>	
			<% if ( AllowGoToPipeline ) { %>
			<table id="Table1" cellSpacing="3" cellPadding="0" width="100%" border="0">
				<tr>
					<td style="COLOR: red" colSpan="2">
						<h3>Access Denied</h3>
						<hr>
					</td>
				</tr>
				<tr>
					<td style="COLOR: red" colSpan="2">
						<h4><%= AspxTools.HtmlString(ErrorInformation) %></h4>
					</td>
				</tr>
				<tr>
					<td colSpan="2">&nbsp;</td>
				</tr>
				<% if ( HasLoanInfo ) { %>
				<TR>
					<TD class="FieldLabel" style="PADDING-LEFT: 10px" colSpan="2">Loan Details</TD>
				</TR>
				<tr>
					<td style="PADDING-LEFT: 20px">
						<table>
							<TR>
								<TD class="FieldLabel">Loan Number:</TD>
								<td>&nbsp;</td>
								<td><%= AspxTools.HtmlString(LoanNumber) %></td>
							</TR>
							<TR>
								<TD class="FieldLabel">Borrower Name:</TD>
								<td>&nbsp;</td>
								<td><%= AspxTools.HtmlString(BorrowerName) %></td>
							</TR>
						</table>
					</td>
				</tr>
				<% } %>
				<tr>
					<td style="PADDING-TOP: 10px" colSpan="2"><input class="ButtonStyle" id="btnBackToPipeline" onclick="f_goToPipeline();" type="button" value="< Go Back To Start" width="160px" NoHighlight>
						&nbsp;</td>
				</tr>
			</table>
			
			<script language="javascript">
				
				function f_goToPipeline()
				{
				  var pipelineUrl = <%= AspxTools.JsString(Tools.VRoot + "/Main/Pipeline.aspx") %>;
					self.location = pipelineUrl;
				}
				


			</script>
			<% } else { %>
			<table id="EmbeddedAccessDeniedPanel" height="100%" width="100%">
				<tr>
					<td style="COLOR: red" vAlign="middle" align="center">
						<h1>Access Denied</h1>
					</td>
				</tr>
			</table>
			<% } %>
			
			<script type="text/javascript" >
			    function f_logout() {
                  if (window.dialogArguments && window.dialogArguments.opener && window.dialogArguments.opener.f_logout )
                  {
                    window.dialogArguments.opener.f_logout();
                  }
                  else
                    self.location = <%= AspxTools.JsString(Tools.VRoot + "/logout.aspx") %>;
                }
                function f_displayButton()
                {
                  <%-- // 02/14/08 mf. OPM 19669. --%>
                  <% if (  !PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML ) { %>
                  if ( (frames && top.frames["ModalFrame"]) ) return;
                  
                  document.getElementById("Table2").style.display = '';
                  <% } %>
                }
			</script>
		</form>
	</body>
</HTML>
