﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.Services;
using System.IO;
using LendersOffice.ObjLib.Task;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.Common;
using LendersOffice.Integration.GenericFramework;
using LendersOffice.Common.TextImport;
using LendersOffice.ConfigSystem.Operations;
using LqbGrammar.DataTypes;

namespace PriceMyLoan.main
{
    public partial class PipelineService : UI.BasePage
    {
        [WebMethod]
        public static object GetTasks(bool assignedToMe, E_TaskStatus taskStatus, E_sCorrespondentProcessT underwritingType, string sortExp, bool sortDesc, bool conditionMode)
        {
            var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;

            Guid userId = assignedToMe ? PriceMyLoanUser.UserId : Guid.Empty;

            var tasks = TaskUtilities.GetTasksInPipeline(
                PrincipalFactory.CurrentPrincipal,
                userId,
                taskStatus,
                sortExp,
                sortDesc ? SortOrder.Descending : SortOrder.Ascending,
                PriceMyLoanUser.PortalMode,
                Tools.sStatusTs_For_PmlSearchCriteria(
                    PriceMyLoanUser.BrokerDB.BrokerID,
                    PriceMyLoanUser.PortalMode,
                    underwritingType,
                    PriceMyLoanUser.PortalMode == E_PortalMode.Retail),
                getConditions: conditionMode
            );

            var CurrentEmployee = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
            return new
            {
                tasks,
                CurrentEmployee = new
                {
                    CurrentEmployee.TaskRelatedEmailOptionT,
                },
                BrokerDB = new
                {
                    PriceMyLoanUser.BrokerDB.EDocsEnabledStatusT,
                    PriceMyLoanUser.BrokerDB.HideStatusAndAgentsPageInOriginatorPortal
                },
                IsNewPmlUIEnabled = PriceMyLoanUser.BrokerDB.IsNewPmlUIEnabled || CurrentEmployee.IsNewPmlUIEnabled,
            };
        }

        private static object PerformTasksOperation(string taskIds, Action<Task> Op, string resolutionBlockTriggerNamesRawString, string resolutionDateFieldIdsRawString, Guid loanId, bool saveTaskAfterOperation = true)
        {
            Guid brokerId = Guid.Empty;
            List<Guid> uniqueLoanIdList = new List<Guid>();

            string[] resolutionBlockTriggerNames = resolutionBlockTriggerNamesRawString.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] resolutionDateFieldIds = resolutionDateFieldIdsRawString.Split(new [] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var resolutionTriggerProcessor = resolutionBlockTriggerNames.Length < 1 ? null :
                new ResolutionTriggerProcessor(
                    loanId,
                    brokerId,
                    PrincipalFactory.CurrentPrincipal,
                    resolutionBlockTriggerNames);

            var resolutionDateSetter = resolutionDateFieldIds.Length < 1 ? null :
                new ResolutionDateSetter(
                    loanId,
                    PrincipalFactory.CurrentPrincipal);

            List<List<string>> errorList = new List<List<string>>();

            var tasks = GetTasks(taskIds);

            foreach (Task task in tasks)
            {
                try
                {
                    // Set these properties to cache the workflow trigger results
                    // and avoid saving the loan for every resolution date field.
                    task.ResolutionTriggerProcessor = resolutionTriggerProcessor;
                    task.ResolutionDateSetter = resolutionDateSetter;

                    if (brokerId == Guid.Empty)
                    {
                        brokerId = task.BrokerId;
                    }
                    if (uniqueLoanIdList.Contains(task.LoanId) == false)
                    {
                        uniqueLoanIdList.Add(task.LoanId);
                    }

                    Op(task);

                    if (saveTaskAfterOperation)
                    {
                        task.Save(false);
                    }
                }
                catch (CBaseException e)
                {
                    errorList.Add(new List<string> {task.TaskId, task.TaskSubject,  e.UserMessage});
                }
            }

            foreach (Guid sLId in uniqueLoanIdList)
            {
                TaskUtilities.EnqueueTasksDueDateUpdate(brokerId, sLId);
            }

            if (tasks.Count > 0)
            {
                resolutionDateSetter?.SetFieldsToCurrentDateAndTime();
            }
            
            return errorList.Count > 0 ? ObsoleteSerializationHelper.JsonSerialize(errorList) : string.Empty;
        }

        [WebMethod]
        public static object SetFollowupDate(string taskIds, DateTime date, string resolutionBlockTriggerNamesRawString, string resolutionDateFieldIdsRawString, Guid loanId)
        {
            return PerformTasksOperation(
                taskIds,
                task => task.TaskFollowUpDate = date,
                resolutionBlockTriggerNamesRawString,
                resolutionDateFieldIdsRawString,
                loanId);        
        }       


        [WebMethod]
        public static object SubscribeToTasks(string taskIds, string resolutionBlockTriggerNamesRawString, string resolutionDateFieldIdsRawString, Guid loanId)
        {
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            Guid userId = PrincipalFactory.CurrentPrincipal.UserId;

            return PerformTasksOperation(
                taskIds,
                task => TaskSubscription.Subscribe(brokerId, task.TaskId, userId),
                resolutionBlockTriggerNamesRawString,
                resolutionDateFieldIdsRawString,
                loanId);
        }

        [WebMethod]
        public static object UnsubscribeTasks(string taskIds, string resolutionBlockTriggerNamesRawString, string resolutionDateFieldIdsRawString, Guid loanId)
        {
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            Guid userId = PrincipalFactory.CurrentPrincipal.UserId;

            return PerformTasksOperation(
                taskIds,
                task => TaskSubscription.Unsubscribe(brokerId, task.TaskId, userId),
                resolutionBlockTriggerNamesRawString,
                resolutionDateFieldIdsRawString,
                loanId);
        }

        [WebMethod]
        public static object Assign(string taskIds, Guid id, string type, string resolutionBlockTriggerNamesRawString, string resolutionDateFieldIdsRawString, Guid loanId)
        {
            if (GetTasks(taskIds).Any(task => task.TaskStatus == E_TaskStatus.Resolved))
            {
                return "Resolved tasks may not be re-assigned.  Re-activate any resolved tasks that you wish to re-assign and try again.";
            }

            return PerformTasksOperation(
                taskIds,
                task =>
                {

                    if (type == "user")
                    {
                        task.TaskAssignedUserId = id;
                        task.TaskToBeAssignedRoleId = Guid.Empty;
                    }
                    else
                    {
                        task.TaskAssignedUserId = Guid.Empty;
                        task.TaskToBeAssignedRoleId = id;
                    }
                },
                resolutionBlockTriggerNamesRawString,
                resolutionDateFieldIdsRawString,
                loanId);
        }

        [WebMethod]
        public static object Resolve(string taskIds, string taskVersions, string resolutionBlockTriggerNamesRawString, string resolutionDateFieldIdsRawString, Guid loanId)
        {
            List<string> ids = taskIds.Split(',').ToList();
            string[] versions = taskVersions.Split(',');

            return PerformTasksOperation(
            taskIds,
            task =>
            {
                int index = ids.IndexOf(task.TaskId);
                if (index < 0) return;

                if (task.TaskStatus != E_TaskStatus.Resolved)
                {
                    task.Resolve(versions[index]);
                }
            },
            resolutionBlockTriggerNamesRawString,
            resolutionDateFieldIdsRawString,
            loanId,
            false);

        }

        [WebMethod]
        public static string ExportAllTasksToCSV(string taskIds, bool conditionmode, bool all)
        {
            var brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            var dcc = conditionmode ? ConditionCategory.GetCategories(brokerId).ToDictionary(p => p.Id) : null;
            var taskList = GetTasks(taskIds);
            if (taskList.Count < 1)
            {
                return string.Empty;
            }

            using (var writer = new StringWriter())
            {
                if (conditionmode)
                {
                    if (all)
                    {
                        writer.WriteLine("Condition List");
                        writer.WriteLine("Condition,Category,Subject,Loan Number,Due Date, Follow-Up Date");
                    }
                    else
                    {
                        writer.WriteLine("Condition List - " + taskList[0].LoanNumCached);
                        writer.WriteLine("Condition,Category,Subject,Due Date, Follow-Up Date");
                    }
                }
                else
                {
                    if (all)
                    {
                        writer.WriteLine("Task List");
                        writer.WriteLine("Task,Subject,Loan Number,Due Date,Follow-Up Date");
                    }
                    else
                    {
                        writer.WriteLine("Task List - " + taskList[0].LoanNumCached);
                        writer.WriteLine("Task,Subject,Due Date,Follow-Up Date");
                    }
                }

                foreach (Task task in taskList)
                {
                    string taskId = DataParsing.sanitizeForCSV(task.TaskId);
                    var category = conditionmode ? DataParsing.sanitizeForCSV(dcc[task.CondCategoryId.Value].Category) : string.Empty;
                    string subject = DataParsing.sanitizeForCSV(task.TaskSubject);
                    // quotes since excel will strip leading 0s
                    string loanNumber = DataParsing.sanitizeForCSV($"\'{task.LoanNumCached}\'");
                    var dueDate = task.TaskDueDate?.ToString("MM/dd/yyyy") ?? task.TaskSingleDueDate;
                    var followupDate = task.TaskFollowUpDate?.ToString("MM/dd/yyyy") ?? string.Empty;

                    if (conditionmode)
                    {
                        writer.WriteLine(all
                            ? $"{taskId},{category},{subject},{loanNumber},{dueDate},{followupDate}"
                            : $"{taskId},{category},{subject},{dueDate},{followupDate}");
                    }
                    else
                    {
                        writer.WriteLine(all
                            ? $"{taskId},{subject},{loanNumber},{dueDate},{followupDate}"
                            : $"{taskId},{subject},{dueDate},{followupDate}");
                    }
                }

                return writer.ToString();
            }
        }

        public const int EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES = 10;

        [WebMethod]
        public static object ExportTasksToPDF(Guid loanId, string taskIDs, bool conditionmode)
        {            
            string cacheId = AutoExpiredTextCache.AddToCache(taskIDs, TimeSpan.FromMinutes(EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES));

            var pdfInstance = new LendersOffice.Pdf.CTaskListPDF(); // Create an instance just to get its name? Hmm. There should be a better way to do this.

            return $"{Tools.VRoot}/pdf/{pdfInstance.Name}.aspx?loanid={loanId}&cacheId={cacheId}{(conditionmode ? "&conditionmode=true" : string.Empty)}";
        }
        
        [WebMethod]
        public static string ExportAllTasksToPDF(Guid[] loanIDs, string taskIDs, bool conditionmode)
        {
            string tasksCacheId = AutoExpiredTextCache.AddToCache(taskIDs, TimeSpan.FromMinutes(EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES));
            string loanIDString = string.Join(",", loanIDs.Select(g => g.ToString()).ToArray());
            string loanCacheId = AutoExpiredTextCache.AddToCache(loanIDString, TimeSpan.FromMinutes(EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES));

            var pdfInstance = new LendersOffice.Pdf.CPipelineTasksPDF(); // Create an instance just to get its name? Hmm. There should be a better way to do this.

            return $"{Tools.VRoot}/pdf/{pdfInstance.Name}.aspx?loanCacheId={loanCacheId}&tasksCacheId={tasksCacheId}{(conditionmode ? "&conditionmode=true" : string.Empty)}";
        }

        [WebMethod]
        public static object GetLoanOptions(string sLId, string[] providerIDs)
        {
            if (string.IsNullOrEmpty(sLId))
            {
                return new LoanOptions();
            }

            try
            {
                var loanID = new Guid(sLId);
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanID, typeof(PipelineService));
                dataLoan.InitLoad();

                bool hasRateLockConfirmation = dataLoan.sRateLockStatusT == E_sRateLockStatusT.Locked 
                    && dataLoan.sHasRateLockConfirmationPdf;

                var principal = PrincipalFactory.CurrentPrincipal;
                var loanValueEvaluator = new LoanValueEvaluator(
                    principal.ConnectionInfo,
                    principal.BrokerId,
                    loanID, 
                    WorkflowOperations.ImportIntoExistingFile, 
                    WorkflowOperations.LoanStatusChangeToDocumentCheck, 
                    WorkflowOperations.LoanStatusChangeToConditionReview);

                loanValueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                bool hasImportPermission = false;
                try
                {
                    hasImportPermission = LendingQBExecutingEngine.CanPerform(
                        WorkflowOperations.ImportIntoExistingFile,
                        loanValueEvaluator);
                }
                catch (GenericUserErrorMessageException e)
                {
                    Tools.LogErrorWithCriticalTracking(
                        "Unexpected error determining if user is able to import into file.",
                        e);
                }

                bool canChangeStatus = false;
                try
                {
                    canChangeStatus |= LendingQBExecutingEngine.CanPerform(
                        WorkflowOperations.LoanStatusChangeToDocumentCheck,
                        loanValueEvaluator)
                        && principal.BrokerDB.GetEnabledStatusesByChannelAndProcessType(dataLoan.sBranchChannelT, dataLoan.sCorrespondentProcessT).Contains(E_sStatusT.Loan_DocumentCheck);
                }
                catch (GenericUserErrorMessageException e)
                {
                    Tools.LogErrorWithCriticalTracking(
                        "Unexpected error determining if user is able to change loan status.",
                        e);
                }

                try { 
                    canChangeStatus |= LendingQBExecutingEngine.CanPerform(
                        WorkflowOperations.LoanStatusChangeToConditionReview,
                        loanValueEvaluator)
                        && principal.BrokerDB.GetEnabledStatusesByChannelAndProcessType(dataLoan.sBranchChannelT, dataLoan.sCorrespondentProcessT).Contains(E_sStatusT.Loan_ConditionReview);
                }
                catch (GenericUserErrorMessageException e)
                {
                    Tools.LogErrorWithCriticalTracking(
                        "Unexpected error determining if user is able to change loan status.",
                        e);
                }

                var providerIDMapping = GenericFrameworkVendor.EvaluatePermissions(loanID, providerIDs, LinkLocation.TPOPipeline);

                return new LoanOptions
                {
                    HasRateLockConfirmation = hasRateLockConfirmation,
                    IsRateLockExtensionAllowed = dataLoan.sIsRateLockExtentionAllowed,
                    IsRateLockFloatDownAllowed = dataLoan.sIsRateLockFloatDownAllowed,
                    IsRateReLockAllowed = dataLoan.sIsRateReLockAllowed,
                    IsImportIntoFileAllowed = hasImportPermission,
                    IsChangeStatusAllowed = canChangeStatus,
                    ProviderIDs = providerIDMapping.ToArray()
                };
            }
            catch (FormatException) 
            { 
            }
            catch (AccessDenied)
            {
            }

            return new LoanOptions();
        }

        [WebMethod]
        public static bool CanAccessTask(string taskId, bool isCondition)
        {
            Task task;
            try
            {
                task = Task.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, taskId, PrincipalFactory.CurrentPrincipal.PortalMode, PrincipalFactory.CurrentPrincipal.PmlBrokerId);
            }
            catch (TaskNotFoundException)
            {
                return false;
            }
            catch (TaskPermissionException)
            {
                return false;
            }

            return task.TaskIsCondition == isCondition;
        }

        public static Pair GetTasksCount(bool bGetAssignedToAnybody)
        {
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            Guid userId = PrincipalFactory.CurrentPrincipal.UserId;

            var tasks = Task.GetOpenTasksByAssignedUserId(bGetAssignedToAnybody ? Guid.Empty : userId, brokerId, "TaskDueDate", string.Empty);
            return new Pair(tasks.Count, tasks.Count(task => task.TaskDueDate <= DateTime.Today));
        }

        [WebMethod]
        public static int GetResolvedOrClosedTaskCount(TaskUtilities.E_TaskConditionOption mode)
        {
            return TaskUtilities.CountRecentlyResolvedTasks(PrincipalFactory.CurrentPrincipal, mode);
        }

        [WebMethod]
        public static object GenericFrameworkVendorLaunch(string sLId, string ProviderID, LinkLocation currentLocation)
        {
            var loanId = new Guid(sLId);
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            
            string errorMessage = null;
            LaunchUrlResponse response = null;
            
            try
            {
                response = GenericFrameworkVendorServer.SubmitRequest(brokerId, ProviderID, loanId, currentLocation);
            }
            catch (CBaseException exc)
            {
                errorMessage = exc.UserMessage;
            }

            return new
            {
                ErrorMessage = response?.ErrorMessage ?? errorMessage,
                response?.Window
            };
        }

        [WebMethod]
        public static void ChangeLoanStatus(string sLId, E_sStatusT status)
        {
            if (PrincipalFactory.CurrentPrincipal.BrokerDB.HideStatusSubmissionButtonsInOriginatorPortal)
            {
                throw new AccessDenied();
            }

            var loanID = new Guid(sLId);
            CPageData dataLoan = new CFullAccessPageData(loanID, new[] { "sStatusT", "sfForceUnlockedStatus", "SetLeadStatus" });
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (Tools.IsStatusLead(dataLoan.sStatusT))
            {
                dataLoan.SetLeadStatus(status);
            }
            else
            {
                dataLoan.ForceUnlockedStatus(status);
            }

            dataLoan.Save();
        }

        [WebMethod]
        public static void ConvertToLoanFile(Guid LeadId)
        {
            var parameters = new LeadConversionParameters();
            parameters.LeadId = LeadId;
            parameters.TemplateId = Guid.Empty;
            parameters.CreateFromTemplate = false;
            parameters.RemoveLeadPrefix = false;
            parameters.ConvertFromQp2File = false;
            parameters.ConvertToLead = false;
            parameters.IsEmbeddedPML = false;
            parameters.IsNonQm = false;

            var results = LeadConversion.ConvertLeadToLoan_Pml(PrincipalFactory.CurrentPrincipal, parameters);
        }

        public class LoanOptions
        {
            public LoanOptions()
            {
                ProviderIDs = new KeyValuePair<string, bool>[0];
            }

            public bool HasRateLockConfirmation { get; set; }
            public bool IsRateLockExtensionAllowed { get; set; }
            public bool IsRateLockFloatDownAllowed { get; set; }
            public bool IsRateReLockAllowed { get; set; }
            public bool IsImportIntoFileAllowed { get; set; }
            public bool IsChangeStatusAllowed { get; set; }
            public KeyValuePair<string, bool>[] ProviderIDs { get; set; }
        }



        private static List<Task> GetTasks(string taskIds)
        {
            return GetTasks(taskIds.Split(','));
        }
        private static List<Task> GetTasks(IEnumerable<string> taskIds)
        {
            var brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            return taskIds
                .Select(taskId => Task.Retrieve(brokerId, taskId))
                .ToList();
        }
    }
}
