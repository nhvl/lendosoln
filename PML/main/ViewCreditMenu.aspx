<%@ Page language="c#" Codebehind="ViewCreditMenu.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.ViewCreditMenu" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>ViewCreditMenu</title>
  </head>
  
  <body MS_POSITIONING="FlowLayout"  bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <script type="text/javascript" >
    <!--
    function viewPDF() {
        var url = <%= AspxTools.JsString(VirtualRoot) %> + '/pdf/CreditReport.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.GetGuid("loanid")) %> + '&applicationid=' + <%= AspxTools.JsString(RequestHelper.GetGuid("applicationid")) %> + '&crack=' + new Date();
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url, parent.body);
    }
    
    function f_print() {
        lqbPrintByFrame(parent.frames["MainFrame"]);
    }
    
    function f_showPrintButton() {
      document.getElementById("btnPrint").style.display = "";
    }
    //-->
    </script>

    <form id="ViewCreditMenu" method="post" runat="server">
    <table border="0" cellspacing="0" cellpadding="5" width="100%">
      <tr>
        <td align="right" valign="top">
          <input type="button" id="btnViewPdf" onclick="viewPDF();" value="View PDF" runat="server"> 
          <input type="button" id="btnPrint" onclick="f_print();" value="Print" style="display: none;"> 
          <input type="button" onclick="window.parent.parent.LQBPopup.Hide();" value="Close"> </td>
      </tr>
    </table>
    </form>
	
  </body>
</html>
