using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LendersOffice.AntiXss;
using LendersOffice.ObjLib.PMLNavigation;

namespace PriceMyLoan.main
{
    public partial class AccordionNav : UserControl
    {
        public Guid SelectedNodeID { get; set; }

        public IEnumerable<DynaTreeNode> DataSource { get; set; }

        public HashSet<Guid> ExpandedNodes { get; set; }

        public Guid FolderId { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.UlStarter.Text = AccordionNavRender(DataSource, this.FolderId, this.ExpandedNodes, this.SelectedNodeID, this.FrameName);
        }

        /// <summary>
        /// Name of the iframe where links with witthin option are loaded.
        /// </summary>
        public string FrameName { get; set; }

        static bool IsNodeExpand(DynaTreeNode node, HashSet<Guid> ExpandedNodes, Guid SelectedNodeID)
        {
            return ((ExpandedNodes != null && ExpandedNodes.Contains(node.Id)) ||
                    (SelectedNodeID == NavigationConfiguration.SystemNode.Loans &&
                     node.Id == NavigationConfiguration.SystemNode.PipelineFolder));
        }

        static string RetrieveExternalName(DynaTreeNode node)
        {
            return node.AllowSafeHtml ? node.ExternalName : AspxTools.HtmlString(node.ExternalName);
        }

        static string AccordionNavRender(IEnumerable<DynaTreeNode> nodes, Guid FolderId, HashSet<Guid> ExpandedNodes, Guid SelectedNodeID, string FrameName, int FolderLevel = 0)
        {
            if (nodes == null) return string.Empty;

            return $@"<ul {(FolderId == Guid.Empty ? @"id='nav' class='nav-bar nav-pipeline'" : string.Empty)}>{(
                string.Join("", nodes.Select(node => node.isFolder ? (
                    $@"<li class='has_sub {GetFolderLevelClass(FolderLevel)}'>
                        <a data-fid='{node.Id}'
                            class='{(IsNodeExpand(node, ExpandedNodes, SelectedNodeID) ? "open" : "")}'>
                            <span>{RetrieveExternalName(node)}</span>
                            <span class='pull-right'>
                                <i class='material-icons'>&#xE5CC;</i>
                            </span>
                        </a>
                        {AccordionNavRender(node.children, node.Id, ExpandedNodes, SelectedNodeID, FrameName, FolderLevel+1)}
                    </li>"
                ) : (
                    node.Id == NavigationConfiguration.SystemNode.PortalMode ? (
                        $@"<li class='link'>
                            <span class='PortalModeContainer'>
                                <span>{RetrieveExternalName(node)}</span>
                            </span>
                        </li>"
                    ) : (
                        $@"<li class='link'>
                            <a id='{node.Id}' 
                                href='{AspxTools.HtmlString(node.ConfigHref)}'
                                class='{GetLeafNodeClass(node, SelectedNodeID)}'
                                {GetAdditionalAttributes(node, FrameName)}
                            >{RetrieveExternalName(node)}</a>
                        </li>"
                    )
                )))
            )}</ul>";
        }

        static string GetFolderLevelClass(int FolderLevel){
            return "level-" + (FolderLevel + 1);
        }

        private static string GetAdditionalAttributes(DynaTreeNode node, string FrameName)
        {
            var attributeString = string.Empty;

            if (node.ConfigTarget == E_NavTarget.NewWindow)
            {
                attributeString += "target='_blank' data-outside='true' ";
            }
            else if (node.ConfigTarget == E_NavTarget.Self)
            {
                attributeString += "data-outside='true' ";
            }
            else if (node.ConfigTarget == E_NavTarget.WithinPage)
            {
                attributeString += $"target='{FrameName}' data-url='{AspxTools.HtmlString(node.ConfigHref)}' ";
            }

            if (!string.IsNullOrEmpty(node.AdditionalData))
            {
                attributeString += $"data-node='{AspxTools.HtmlString(node.AdditionalData)}' ";
            }

            if (node.ProvideSystemAccess)
            {
                attributeString += $"data-send-external-endpoints='true' ";
            }

            return attributeString;
        }

        static string GetLeafNodeClass(DynaTreeNode node, Guid SelectedNodeID)
        {
            if (node == null) return string.Empty;

            return string.Join(" ", new[]
            {
                (node.Id == SelectedNodeID                   ? "selected"    : null),
                (!string.IsNullOrEmpty(node.addClass)        ? node.addClass : null),
                (node.ConfigTarget == E_NavTarget.WithinPage ? "within"      : null),
            }.Where(s => !string.IsNullOrEmpty(s)));
        }
    }
}
