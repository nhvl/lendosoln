/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Common;
using PriceMyLoan.DataAccess;
using PriceMyLoan.Common;
using LendersOffice.Security;
using PriceMyLoan.Security;
using LendersOffice.Constants;
using DataAccess.LoanValidation;
using LendersOffice.AntiXss;

namespace PriceMyLoan.main
{
	public partial class ValidationErrorPage : PriceMyLoan.UI.BasePage
	{
    
        protected bool m_allowGoToPipeline 
        {
            get { return PriceMyLoanUser.IsAllowGoToPipeline; }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            CPageData dataLoan = new CValidateForRunningPricingData(LoanID);
            dataLoan.InitLoad();

            sLNm.Text = dataLoan.sLNm;
            aBNm.Text = dataLoan.GetAppData(0).aBNm;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep.Replace(".00", "");
            // 10/8/2004 dd - Temporary Hack to remove .000;
            sLtvR.Text = dataLoan.sLtvR_rep.Replace(".000", "");
            sCltvR.Text = dataLoan.sCltvR_rep.Replace(".000", "");
            sCreditScoreType1.Text = dataLoan.sCreditScoreType1_rep;
            sCreditScoreType2.Text = dataLoan.sCreditScoreType2_rep;


            HtmlGenericControl ul = new HtmlGenericControl("ul");

            LoanValidationResultCollection resultList = dataLoan.ValidateForRunningPricing(this.PriceMyLoanUser);
            if (resultList.ErrorCount > 0)
            {
                foreach (LoanValidationResult result in resultList.ErrorList)
                {
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.InnerText = result.Message;
                    ul.Controls.Add(li);
                }
            }

            ErrorMessagePlaceHolder.Controls.Add(ul);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
