<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"> 
<%@ Page language="c#" Codebehind="Agents.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.AgentsPage" ValidateRequest="true"%>
<%@ Register TagPrefix="uc1" TagName="Agents" Src="Agents.ascx" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="Modal" Src="~/common/ModalDlg/ModelessDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanHeader" Src="~/common/LoanHeader.ascx" %>

<html>
  <head runat="server">
    <title>Agents</title>
    <style  type="text/css">
        body { margin: 0; }
        .text-center { text-align: center; }
        .audit-loan-header { margin-top: -14px; }
        .audit-loan-header ul.loan-info-bar { position: relative; width: 98%; }
    </style>
  </head>
  <body >
	<script type="text/javascript">
        $j(function(){
            $j('a.ShowWarningWindow').click(function(){
                var url = <%= AspxTools.JsString(VirtualRoot + "/main/ListModifiedFields.aspx?loanid=" + LoanID) %>;
                LQBPopup.Show(url, {width: 500, height: 300, hideCloseButton:true});
            //    loadUrlInIframe(url, 500, 300);
            });
            $j('.HelpLink').click(function() {
                LQBPopup.Show(ML.VirtualRoot + '/help/Q00006.html', { 'width': 400, 'height': 150 });
                return false;
            });
        });
        function f_validatePage(){
            if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object') {
                return Page_ClientValidate();  
            }
            
            return true;
        }

        function f_enableButton(enabled){
            var btn = document.getElementById('<%= AspxTools.ClientId(m_btnNext) %>');
            if (btn != null) {
                btn.disabled = !enabled;
            }
        }
  
        function f_goToPipeline() {
            self.location = <%= AspxTools.JsString(VirtualRoot + "/Main/Pipeline.aspx") %>;
        }

	    function f_logout() {
	        self.location = gVirtualRoot + "/logout.aspx";
	    }
    </script>


    <form id="Agents" method="post" runat="server" autocomplete="off">
    <div class="audit-loan-header">
        <div id="NavigationHeaderController" class="warp">
            <div>
                <ul class="loan-info-bar">
                    <asp:PlaceHolder runat="server" ID="GoToPipelineLink">
                    <li class="back-btn">
                        <a href="#" onclick="f_goToPipeline();">
                            <i class="material-icons pipeline-icon ng-cloak">&#xE879;</i>
                            <span class="pipeline-text">Pipeline</span>
                        </a>
                    </li>
                    </asp:PlaceHolder>
                    <li>
                        <p class="title">Loan Number: <span><ml:EncodedLiteral id="sLNm" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">Borrower Name: <span><ml:EncodedLiteral id="aBNm" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">Loan Amount: <span><ml:EncodedLiteral id="sLAmtCalc" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">
                            Primary Credit Score: 
                            <span>
                                <ml:EncodedLiteral id="sCreditScoreType1" runat="server" />
                                <a class="HelpLink" title="Help"><i class="material-icons">&#xE887;</i></a>
                            </span>
                        </p>
                    </li>
                    <li>
                        <p class="title">
                            Lowest Credit Score: 
                            <span>
                                <ml:EncodedLiteral id="sCreditScoreType2" runat="server" />
                                <a class="HelpLink" title="Help"><i class="material-icons">&#xE887;</i></a>
                            </span>
                        </p>
                    </li>
                    <li>
                        <p class="title">LTV: <span><ml:EncodedLiteral id="sLtvR" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">CLTV: <span><ml:EncodedLiteral id="sCltvR" runat="server" /></span></p>
                    </li>
                    <asp:PlaceHolder runat="server" ID="LogOutLink">
                    <li>
                        <p class="title text-center">
                            <span class="log-out">
                                <a id="LogoutButton" onclick="f_logout();" href="#"><i class="material-icons logout-icon">&#xE879;</i><span class="text log-out-text">Log Off</span></a>
                            </span>
                        </p>
                    </li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody id="tbodyError" runat="server" visible="false">
            <tr>
                <td class="ErrorMessage" style="padding: 10px;">
                    <ml:EncodedLiteral ID="m_errorMessage" runat="server" />
                    <br />
                    <br />
                    <br />
                    <input type="button" value="&lt; Go Back" onclick="self.location=self.location" class="ButtonStyle" />
                </td>
            </tr>
        </tbody>
        <tbody id="tbodyWarning" runat="server">
            <tr>
                <td style="border-right: #ffcc99 1px dashed; border-top: #ffcc99 1px dashed; font-weight: bold;
                    border-left: #ffcc99 1px dashed; color: red; border-bottom: #ffcc99 1px dashed;
                    background-color: #ffffcc">
                    <%= AspxTools.HtmlString(m_warningMsg) %>
                    <a href="#" class="ShowWarningWindow">View updated fields</a>
                </td>
            </tr>
        </tbody>
        <tbody id="tbodyMain" runat="server">
            <tr>
                <td>
                    <uc1:Agents ID="m_agents" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="top" style="padding-left: 5px; padding-top: 20px">
                    <asp:Button OnClientClick="this.disabled = true;" UseSubmitBehavior="false" ID="m_btnNext"
                        runat="server" Text="Next >" CssClass="ButtonStyle" NoHighlight OnClick="m_btnNext_Click">
                    </asp:Button>
                </td>
            </tr>
        </tbody>
    </table>
    </form>

  </body>
</HTML>
