<%@ Page language="c#" Codebehind="FannieMaeExportLogin.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.FannieMaeExportLogin" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>FannieMaeExportLogin</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" style="MARGIN-TOP:0px;MARGIN-LEFT:0px" scroll=no>
	<script language=javascript>
<!--

var g_sDoDuHeader = <%= AspxTools.JsString(m_doDuHeader) %>;  
function _init() {
  <% if (m_hasAutoLoginInformation) { %>
    f_submit();
  <% } else { %>
    //resize(540,280);
    if (<%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value != '')
      <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.focus();
    else
      <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.focus();
  <% } %>    
  $j('#HeaderLabel').text("Submit directly to " + g_sDoDuHeader)
  $j('#CaseIdLabel').text(g_sDoDuHeader + " Case ID")
  $j('#LoginLabel').text(g_sDoDuHeader + " User ID")
  $j('#PasswordLabel').text(g_sDoDuHeader + " Password")
  $j('#FannieInstitutionIdLabel').text("Lender Institution ID")
}
function f_close() {
  parent.LQBPopup.Return({OK:false});
  //self.close();
}
function f_submit() {
  f_displayWait(true);
  window.setTimeout(f_submitImpl, 200);

}
function f_displayError(errMsg) {
  document.getElementById('ErrorPanel').style.display = '';
  document.getElementById('WaitPanel').style.display = 'none';
  <%= AspxTools.JsGetElementById(ErrorMessage) %>.value = errMsg;
  <% if (!m_hasAutoLoginInformation) { %>
    document.getElementById('MainPanel').style.display = '';
    parent.LQBPopup.Resize(600, 500);
    
  <% } else { %>
    <%-- Reduce over windows height since we do not allow user to retype login information. --%>
    document.getElementById('ErrorPanelOkButton').style.display ='';
    parent.LQBPopup.Resize(600, 400);
  <% } %>
  
}
function f_displayComplete(id, sDuCaseId) {

  var args = {
  OK : true,
  IsDo: <%= AspxTools.JsBool(m_isDo) %>,
  CacheId : id,
  sUserName : <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value,
  sPassword : <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value,
  sDuCaseId : sDuCaseId,
  bHasAutoLogin : <%= AspxTools.JsBool(m_hasAutoLoginInformation) %>
  }
  //self.close();
  parent.LQBPopup.Return(args);
}
function f_submitImpl() {
  var args = {};
  args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
  
  <% if (m_hasAutoLoginInformation) { %>
    args["AutoLogin"] = "True";
  <% } else { %>
    args["FannieMaeMORNETUserID"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
    args["FannieMaeMORNETPassword"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
    args["RememberLastLogin"] = <%= AspxTools.JsGetElementById(m_rememberLoginCB) %>.checked ? 'True' : 'False';  
    args["sDuCaseId"] = <%= AspxTools.JsGetElementById(sDuCaseId) %>.value;
    args["sDuLenderInstitutionId"] = <%= AspxTools.JsGetElementById(sDuLenderInstitutionId) %>.value;
  <% } %>
  
  args["IsDo"] = <%= AspxTools.JsString(m_isDo ? "True" : "False") %>;
 
  var result = gService.loanedit.call("ExportToDuDo", args);
  if (!result.error) {
    switch (result.value["Status"]) {
      case "ERROR":
        f_displayError(result.value["ErrorMessage"]);
        break;
      case "DONE":
        f_displayComplete(result.value["id"], result.value["sDuCaseId"]);
        break;
    }
  } else {
    var errMsg = <%= AspxTools.JsString(ErrorMessages.Generic) %>;
    
    if (null != result.UserMessage)
      errMsg = result.UserMessage;
      
    f_displayError(errMsg);
  }
}
function f_displayWait(bDisplay) {
  parent.LQBPopup.Resize(500, 150);
  document.getElementById('MainPanel').style.display = 'none';
  document.getElementById('WaitPanel').style.display = '';
  document.getElementById('ErrorPanel').style.display = 'none';
}

//-->
</script>
    <h4 class="page-header"><div id="HeaderLabel"></div></h4>
    <form id="FannieMaeExportLogin" method="post" runat="server">
<table id=MainTable cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TBODY>
  <tr><td>&nbsp;</td></tr>
  <tbody id=WaitPanel style="DISPLAY: none">
  <tr>
    <td style="FONT-WEIGHT: bold; FONT-SIZE: 16px" vAlign=middle align=center colSpan=2 height=50>Please Wait ...<br><IMG height=10 src="../images/status.gif" ></td>
    </tr>
  </tbody>
  <tbody id=MainPanel>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px"><div id=LoginLabel></div></td>
    <td><asp:textbox id=FannieMaeMORNETUserID runat="server" Width="155px" /></td></tr>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px"><div id=PasswordLabel></div></td>
    <td><asp:textbox id=FannieMaeMORNETPassword runat="server" Width="155px" TextMode="Password" /></td></tr>
  <tr>
    <td class="FieldLabel" style="PADDING-LEFT: 5px"><div id="FannieInstitutionIdLabel"></div></td>
    <td><asp:textbox id="sDuLenderInstitutionId" runat="server" Width="155px" /> (Leave blank to use default)</td></tr>
      <tr>
          <td class="FieldLabel" style="padding-left: 5px">
              <div id="CaseIdLabel">
              </div>
          </td>
          <td class="FieldLabel">
              <asp:TextBox ID="sDuCaseId" runat="server" Width="155px" />
          </td>
      </tr>
  <TR>
    <TD class=FieldLabel style="PADDING-LEFT: 5px"></TD>
    <TD class=FieldLabel style="PADDING-TOP:10px"><asp:CheckBox id=m_rememberLoginCB runat="server" Text="Remember login name" /></TD></TR>
    
  <TR>
    <TD align=center colSpan=2>&nbsp;</TD></TR>
  <tr>
    <td></td>
    <td><input class=buttonstyle onclick="f_submit();" type=button value="Submit">&nbsp;<input class=buttonstyle onclick="f_close();" type=button value=Cancel> 
    </td></tr>
  <tr style="padding-top:6px">
    <td></td>
    <td><a href="#" onclick="window.open('https://profile-manager.efanniemae.com/integration/service/password/UserSelfService', 'FannieMae', 'width=740,height=420,menubar=no,status=yes,resizable=yes');">Forgot your User ID or Password?</a></td></tr>
  </tbody>
  <tbody id=ErrorPanel style="DISPLAY: none">
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px; PADDING-TOP: 10px;padding-bottom:7px;" noWrap colSpan=2>
    <% if (m_hasAutoLoginInformation) { %> 
    An error has occurred. Please provide these details to your account administrator:
    <% } else { %>
    Error Message
    <% } %>
    </td></tr>
  <tr>
    <td style="PADDING-LEFT: 5px" noWrap colSpan=2><asp:textbox id=ErrorMessage runat="server" Width="550px" TextMode="MultiLine" ReadOnly="True" Height="201px" ForeColor="Red"></asp:textbox></td></tr></tbody>
  <tbody id="ErrorPanelOkButton" style="DISPLAY:none">
  <tr><td colspan=2 align=center><input class=buttonstyle onclick="f_close();" type=button value=OK width="30px"></td></tr>
  <tr><td>&nbsp;</td></tr>
  </tbody>
      </table>
     </form>
	
  </body>
</HTML>
