<%@ Import namespace="LendersOffice.Common"%>
<%@ Page language="c#" Codebehind="LiabilityList.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.LiabilityList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>LiabilityList</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" leftmargin=0 rightmargin=0 topmargin=5  >
	<script language=javascript>
    
    $j(function(){$j('#cancelBtn').click(function(){
        parent.LQBPopup.Hide();
    });});

    function f_onPaidOffCheck(cb){

        var ratio_cb_id = cb.id.replace(/pdoff_/, "ratio_");
        var ratio_cb = document.getElementById(ratio_cb_id);
        if (null != ratio_cb) {
            if (cb.checked) {
                ratio_cb.checked = false;
            } else {
            ratio_cb.disabled = false;
            }
        }
    }

        function f_addLiability()
        {
        if (<%= AspxTools.JsBool(IsReadOnly) %>)
            return;
            
            LQBPopup.Show(<%=AspxTools.JsString(VirtualRoot) %> + '/main/AddLiability.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&appid=' + <%= AspxTools.JsString(m_currentAppId) %> + '&version=' + <%= AspxTools.JsString(sFileVersion) %>, 
                {
                    width: 400, 
                    height: 390, 
                    hideCloseButton: true, 
                    onReturn: function(){window.location.href = window.location.href;}
            });
        }

</script>
    <h4 class="page-header">Liability List</h4>
    <form id="LiabilityList" method="post" runat="server">
        &nbsp;&nbsp;&nbsp;<input type="button" onclick="f_addLiability();" value="Add Liability" class="ButtonStyle" NoHighlight>
        <div id=MainFrame style="BORDER-RIGHT:3px solid;BORDER-TOP:3px solid;MARGIN:5px;OVERFLOW:auto;BORDER-LEFT:3px solid;BORDER-BOTTOM:3px solid;HEIGHT:75%" runat="server">

        <table>
            <tr>
                <td>&nbsp;</td>
                <td>
                    Exclude from ratio
                </td>
                <td>&nbsp;</td>
                <td>
                    Monthly Payment
                </td>
                <td>
                    Months Left
                </td>
            </tr>
            <tr>
                <td>
                    Alimony
                </td>
                <td>
                    <input type="checkbox" id="Alimony_NotUsedInRatio" runat="server" />
                    <label for="Alimony_NotUsedInRatio">Exclude</label> 
                </td>
                <td>
                    <input type="text" id="Alimony_OwedTo" runat="server" />
                </td>
                <td>
                    <ml:MoneyTextBox ID="Alimony_MonthlyPayment" runat="server" preset="money" value="$0.00"  Update="true" />
                </td>
                <td>
                    <input type="text" id="Alimony_RemainingMonths" runat="server" class="input-w100" />
                </td>
            </tr>
            <tr>
                <td>
                    Child Support
                </td>
                <td>
                    <input type="checkbox" id="ChildSupport_NotUsedInRatio" runat="server" />
                    <label for="ChildSupport_NotUsedInRatio">Exclude</label> 
                </td>
                <td>
                    <input type="text" id="ChildSupport_OwedTo" runat="server" />
                </td>
                <td>
                    <ml:MoneyTextBox ID="ChildSupport_MonthlyPayment" runat="server" preset="money" value="$0.00"  Update="true" />
                </td>
                <td>
                    <input type="text" id="ChildSupport_RemainingMonths" runat="server" class="input-w100" />
                </td>
            </tr>
            <tr>
                <td>
                    Job Expense
                </td>
                <td>
                    <input type="checkbox" id="JobRelated1_NotUsedInRatio" runat="server" />
                    <label for="JobRelated1_NotUsedInRatio">Exclude</label> 
                </td>
                <td>
                    <input type="text" id="JobRelated1_ExpenseDescription" runat="server" />
                </td>
                <td colspan="2">
                    <ml:MoneyTextBox ID="JobRelated1_MonthlyPayment" runat="server" preset="money" value="$0.00"  Update="true" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <input type="checkbox" id="JobRelated2_NotUsedInRatio" runat="server" />
                    <label for="JobRelated2_NotUsedInRatio">Exclude</label> 
                </td>
                <td>
                    <input type="text" id="JobRelated2_ExpenseDescription" runat="server" />
                </td>
                <td colspan="2">
                    <ml:MoneyTextBox ID="JobRelated2_MonthlyPayment" runat="server" preset="money" value="$0.00"  Update="true" />
                </td>
            </tr>
        </table>
        <br />

        <ml:CommonDataGrid id=m_dg runat="server">
        <AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
        <ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
        <HeaderStyle CssClass="GridHeader"></HeaderStyle>
        
        <columns>
            <asp:TemplateColumn HeaderText="Owner" HeaderStyle-Width="40px" ItemStyle-Width="40px" ItemStyle-HorizontalAlign="Center">
              <itemtemplate>  
                <%# AspxTools.HtmlString(displayOwnerType(Eval("OwnerT").ToString()))%>
              </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Debt Type" HeaderStyle-Width="70px" ItemStyle-Width="70px">
              <itemtemplate>
                <%# AspxTools.HtmlString(displayLiabilityType(Eval("DebtT").ToString()))%>
              </ItemTemplate>
            </asp:TemplateColumn>
            <asp:templatecolumn headertext="Company" >
              <itemtemplate>
                <%# AspxTools.HtmlString(Eval("ComNm").ToString())%>
              </itemtemplate>
            </asp:templatecolumn>
            <asp:TemplateColumn HeaderText="Balance" HeaderStyle-Width="80px" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right">
              <itemtemplate>
                <%# AspxTools.HtmlString(displayMoneyString(Eval("Bal").ToString())) %>
              </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Payment" HeaderStyle-Width="80px" HeaderStyle-Wrap="false" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right">
              <itemtemplate>
                <%# AspxTools.HtmlString(displayMoneyString(Eval("Pmt").ToString())) %>
              </ItemTemplate>
            </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Pd Off" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
              <%# AspxTools.HtmlControl(CreatePayoffCheckbox(Container.DataItem)) %>
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Used in Ratio" HeaderStyle-Width="90px" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
            <itemtemplate>
              <%# AspxTools.HtmlControl(CreateUsedInRatioCheckbox(Container.DataItem)) %>
            </ItemTemplate>
          </asp:TemplateColumn>
        </Columns>
      </ml:CommonDataGrid>
</div>
    <div id="divErrorMessage" class="ErrorMessage">
      <ml:EncodedLiteral ID="m_errorMessage" runat="server" />
    </div>
<span style="FONT-WEIGHT:bold;FONT-SIZE:12px" id="spanInformation" runat="server">&nbsp;&nbsp;  *** Mortgage payments 
should be calculated into either the Total Income or Other Properties' Negative Cash 
Flow amount.</span>
    <table width="100%">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="center">
          <asp:Button ID="m_btnOK" runat="server" Text="OK" CssClass="ButtonStyle" Width="50px" OnClick="m_btnOK_Click"></asp:Button>&nbsp;<input type="button" id="cancelBtn"  value="Cancel" class="ButtonStyle"></td>
      </tr>
    </table>
    </form>
	
  </body>
</HTML>
