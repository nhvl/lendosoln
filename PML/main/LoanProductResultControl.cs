using System;
using System.Collections.Generic;
using System.Web.UI;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Common;
using PriceMyLoan.Common;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.PriceMyLoan;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem.Operations;

namespace PriceMyLoan.UI
{

    public class LoanProductResultControl : System.Web.UI.Control
    {
        private CApplicantPriceXml m_applicantPrice;
        private string m_cssClassName;
        private bool m_has2ndLoan;
        private E_sLienQualifyModeT m_currentLienQualifyModeT;
        private bool m_isNoIncome;
        private BrokerDB m_brokerDB = null;
        private E_LpeRunModeT m_LpeRunModeT;
        private bool m_canSubmitForLock;
        private bool m_isProgramRegistered;

        public bool IsBorrowerPaidOriginatorCompAndIncludedInFees { get; set; }

        public bool IsLenderPaidOriginatorCompAndAdditionToFees { get; set; }

        public bool DisplayPmlFeeIn100Format { get; set; }

        public CApplicantPriceXml ApplicantPrice
        {
            get { return m_applicantPrice; }
            set { m_applicantPrice = value; }
        }
        public string CssClassName
        {
            get { return m_cssClassName; }
            set { m_cssClassName = value; }
        }

        public bool Has2ndLoan
        {
            get { return m_has2ndLoan; }
            set { m_has2ndLoan = value; }
        }

        public E_sLienQualifyModeT CurrentLienQualifyModeT
        {
            get { return m_currentLienQualifyModeT; }
            set { m_currentLienQualifyModeT = value; }
        }

        // OPM 8952.  We have to hide QRate and DTI when this is set
        public bool IsNoIncome
        {
            get { return m_isNoIncome; }
            set { m_isNoIncome = value; }
        }

        public BrokerDB BrokerDB
        {
            get { return m_brokerDB; }
            set { m_brokerDB = value; }
        }

        private bool IsPmlSubmissionAllowed
        {
            get
            {
                return BrokerDB.IsPmlSubmissionAllowed;
            }
        }
        public E_LpeRunModeT CurrentLpeRunModeT
        {
            get { return m_LpeRunModeT; }
            set { m_LpeRunModeT = value; }
        }

        public bool CanSubmitForLock
        {
            get { return m_canSubmitForLock; }
            set { m_canSubmitForLock = value; }
        }

        public bool IsProgramRegistered
        {
            get { return m_isProgramRegistered; }
            set { m_isProgramRegistered = value; }
        }

        public bool IsLoanRegistrationDisabled { get; set; }

        protected override void Render(HtmlTextWriter output)
        {
            // 01/21/08 mf OPM 19782. We need to hide Teaser Rate column depending on a broker
            // condition. Refactored a little here to make conditional column rendering easier.
            // Basically, we hide the Teaser rate column if this lender has set the corporate
            // option IsOptionARMUsedInPml (OPM 19782). If this scenario has a no-income
            // doc type we hide the DTI and Qualify Rate columns (OPM 8952).

            int rateColumns = 7 - (m_isNoIncome ? 2 : 0) - (BrokerDB.IsOptionARMUsedInPml ? 0 : 1);

            string eligible = m_applicantPrice.Status == E_EvalStatus.Eval_Eligible ? "1" : "0";

            string endColumn = "<td colspan=2>&nbsp;</td>";

            // Render LP Name.
            output.Write("<tr class={1}><td colspan={2} class='PrdNm'>{0}</td>{3}</tr>",
                AspxTools.HtmlString(m_applicantPrice.lLpTemplateNm), // 0
                AspxTools.HtmlAttribute(m_cssClassName), // 1
                rateColumns + 1, // 2
                endColumn // 3
                );

            if (m_applicantPrice.Status == E_EvalStatus.Eval_Ineligible)
            {
                // 10/5/2005 dd - OPM 2804 - Display disqualifying reason for users who can apply for disqualified loans.
                output.Write("<tr class={1}><td colspan={2} style='padding-left:20px'>{0}</td></tr>",
                    Page.Server.HtmlDecode(m_applicantPrice.DisqualifiedRules), // 0
                    AspxTools.HtmlAttribute(m_cssClassName), // 1
                    rateColumns + 3
                    );
            }

            output.Write("<tbody id='L_{0}'><tr class={1}><td valign=top class='c0'>"
                + (m_applicantPrice.ApplicantRateOptions.Length == 0 ? "&nbsp;" : "<a href='#' onclick='return f00({2});'>preview</a>")
                + "</td>", this.ID, AspxTools.HtmlAttribute(m_cssClassName), m_applicantPrice.TempIndex);

            if (m_applicantPrice.ApplicantRateOptions.Length == 1)
            {
                // 10/4/2005 dd - Do not display view more checkbox if there is only 1 rate option.
                output.Write("<td class='c1'>&nbsp;</td>");
            }
            else if (m_applicantPrice.ApplicantRateOptions.Length == 0)
            {
                // 1/16/2013 OPM 105703 - moved down to rate column logic

                // 06/26/09 OPM 2950.  There can now be disqualified programs with no valid rate options.
                // output.Write("<td colspan='"
                // + (rateColumns + 2).ToString()
                // + "' class='c1'>&nbsp;</td>");
                //+ "' class='c1'>&nbsp;</td><td class='c1' >&nbsp;</td>");
            }
            else
            {
                output.Write("<td valign=top class='c1' nowrap><label for='L{0}'>View More </label><input id='L{0}' type=checkbox onclick='f_M(\"{0}\", this, {1});'></td>", this.ID, m_applicantPrice.TempIndex);
            }

            CApplicantRateOption rateOption = null;
            if (m_applicantPrice.RepresentativeRateOption.Length > 0 && m_applicantPrice.RepresentativeRateOption[0] != null)
            {
                rateOption = m_applicantPrice.RepresentativeRateOption[0];
            }
            else if (m_applicantPrice.ApplicantRateOptions.Length > 0)
            {
                rateOption = m_applicantPrice.ApplicantRateOptions[0];
            }

            if (rateOption != null)
            {
                rateOption.SetApplicantPrice(m_applicantPrice);
                string bottomRatio = rateOption.BottomRatio_rep;

                if (bottomRatio == "-1.000" || bottomRatio == "")
                {
                    // 9/11/2006 dd - OPM 7345
                    bottomRatio = "N/A";
                }
                else
                {
                    if (rateOption.IsViolateMaxDti)
                    {
                        bottomRatio = "** " + bottomRatio;
                    }
                }
                output.Write("<td class=c2>{0}</td>", RenderSubmitLink(rateOption, m_applicantPrice.IsBlockedRateLockSubmission));

                string cssClass = m_applicantPrice.AreRatesExpired ? "rie" : "ri";

                if (BrokerDB.IsOptionARMUsedInPml)
                {
                    output.Write(RenderTableCell(AspxTools.HtmlString(rateOption.TeaserRate_rep), "ri"));
                }
                output.Write(RenderTableCell(AspxTools.HtmlString(rateOption.Rate_rep), cssClass));

                // OPM 64253.  If lender has a special option turned on, we want to 
                // display the point value as adjusted for originator comp.
                string pointRep;
                if (BrokerDB.IsUsePriceIncludingCompensationInPricingResult && IsLenderPaidOriginatorCompAndAdditionToFees)
                {
                    // OPM 64253
                    pointRep = rateOption.PointIncludingOriginatorComp_rep;
                }
                else
                {
                    pointRep = rateOption.Point_rep;
                }



                output.Write(RenderTableCell(AspxTools.HtmlString(CApplicantRateOption.PointFeeInResult(DisplayPmlFeeIn100Format, pointRep)), cssClass));

                output.Write(RenderTableCell(AspxTools.HtmlString(rateOption.FirstPmtAmt_rep), cssClass));
                if (m_isNoIncome)
                {
                    output.Write(RenderTableCell(m_applicantPrice.lIsArmMarginDisplayed ? AspxTools.HtmlString(rateOption.Margin_rep) : string.Empty, cssClass));
                }
                else
                {
                    output.Write(RenderTableCell(AspxTools.HtmlString(rateOption.QRate_rep), cssClass));
                    output.Write(RenderTableCell(m_applicantPrice.lIsArmMarginDisplayed ? AspxTools.HtmlString(rateOption.Margin_rep) : string.Empty, cssClass));

                    var maxDti = m_applicantPrice.MaxDti_rep == "" ? "N/A" : m_applicantPrice.MaxDti_rep.Replace("%", "");
                    var maxDtiMessage = "<a href='#' class='displayMsgLink' displayMsg=" + AspxTools.HtmlAttribute("Max DTI: " + maxDti) + "/>" + AspxTools.HtmlString(bottomRatio) + "</a>"; 
                    output.Write(RenderTableCell(maxDtiMessage, cssClass + (rateOption.IsViolateMaxDti ? " ViolateDti" : "")));
                }
            }
            else
            {
                output.Write("<td class='c1' colspan='{0}'>&nbsp;</td>", (rateColumns + 2).ToString());
            }
            output.Write("</tr></tbody>");

            if (m_applicantPrice.ApplicantRateOptions.Length > 1)
            {
                output.Write("<tbody id='M_{0}'></tbody>", this.ID);
            }

        }
        private string RenderTableCell(string content, string cssClass)
        {
            return "<td class='" + cssClass + "'>" + (content == String.Empty ? "&nbsp;" : content) + "</td>";
        }

        private string RenderSubmitLink(CApplicantRateOption rateOption, bool isBlockedRateLockSubmission)
        {

            string margin = rateOption.Margin_rep;

            List<string> newLinks = ResultActionLink.GetLinkTitles(
                m_has2ndLoan
                , m_currentLienQualifyModeT
                , IsPmlSubmissionAllowed
                , false
                , m_LpeRunModeT
                , BrokerDB.IsRateLockedAtSubmission
                , m_canSubmitForLock
                , m_isProgramRegistered
                , !IsLoanRegistrationDisabled
                );

            List<string> oldLinks = ResultActionLink.GetLinkTitles_NoWorkflow(
                m_has2ndLoan
                , m_currentLienQualifyModeT
                , IsPmlSubmissionAllowed
                , false
                , m_LpeRunModeT
                , BrokerDB.IsRateLockedAtSubmission
                );


            // In this case ? == a link to the reasons why "lock rate" does not appear.  Same actions as it being missing.
            //bool firstLinkMatch =
            //    (oldLinks[0] == newLinks[0]) ||
            //    (oldLinks[0] == "register loan" && newLinks[0] == "register loan/?") ||
            //    (oldLinks[0] == "" && newLinks[0] == "?");

            //bool secondLinkMatch = oldLinks[1] == newLinks[1];

            //if (firstLinkMatch == false || secondLinkMatch == false )
            //{
            //    if (Context.Items["pmllockmismatchreported"] == null)
            //    {

            //        string userId = (null == Context) ? "" : ((PriceMyLoan.Security.PriceMyLoanPrincipal)Context.User).UserId.ToString();

            //        StringBuilder sb = new StringBuilder("PML ALLOWED LOCK ACTIONS MISMATCH!");
            //        sb.AppendLine();
            //        sb.AppendLine("BrokerId: " + BrokerDB.BrokerID);
            //        sb.AppendLine("UserId: " + userId);
            //        sb.AppendLine("New Actions: " + newLinks[0]);
            //        sb.AppendLine("Old Actions: " + oldLinks[0]);
            //        sb.AppendLine("New Unavailable: " + newLinks[1]);
            //        sb.AppendLine("Old Unavailable: " + oldLinks[1]);

            //        Context.Items["pmllockmismatchreported"] = true;
            //        Tools.LogWarning(sb.ToString());

            //    }
            //}

            bool useOldModel = true;
            BasePage resultsPage = this.Page as BasePage;
            if (resultsPage != null)
            {
                useOldModel = resultsPage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml);
            }
            else
            {
                // Safety throw. If getresults control ends up on page that does not
                // inherit from basepage, we should work in the security checks there.
                throw new CBaseException(ErrorMessages.Generic, "LoanProductResultControl is child control on page that does not inherit from Base.");
            }

            // Always use old until we get no mismatches
            List<string> titles = useOldModel ? oldLinks : newLinks;

            string linkTitle = null;

            if (isBlockedRateLockSubmission)
            {
                linkTitle = titles[1];
            }
            else
            {
                linkTitle = titles[0];
            }

            if (BrokerDB.IsRateLockedAtSubmission == false && m_LpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                return "&nbsp;"; // 06/11/08 mf - OPM 18996 Re-run float submission without ability to lock means no submit link


            if (!m_applicantPrice.lIsArmMarginDisplayed)
            {
                margin = "0.000";
            }
            string[] parts = linkTitle.Split('/');

            string html = string.Empty;
            if (parts[0] != "?") // No access.
            {
                if (rateOption.IsDisqualified)
                {
                    html += String.Format("&nbsp;<a href='#' class='unavailable' data-func='f01' data-msg={0} data-arg1={1} data-arg2='{2}' data-arg3='{3}' data-arg4={4}>unavailable</a>", 
                        AspxTools.HtmlAttribute(rateOption.DisqualReason),
                        AspxTools.HtmlAttribute(rateOption.RateOptionId), 
                        this.ID, m_applicantPrice.TempIndex, AspxTools.HtmlAttribute(linkTitle));
                }
                else if (parts[0] == "register loan?")
                {
                    html = @"<a href='#' onclick='return false;' disabled='disabled' >register loan</a>";
                    html += "&nbsp;<a href='#' onclick='return f_SRM();'>?</a>";
                }
                else
                {
                    html = string.Format(@"<a href='#' onclick=""return f01({1},'{2}', {3});"">{0}</a>",
                        AspxTools.HtmlString(parts[0]), // 0
                        AspxTools.JsString(rateOption.RateOptionId), // 1
                        this.ID, // 2
                        m_applicantPrice.TempIndex // 3

                        );
                }
            }
            else
            {
                html = @"<a href='#' disabled='disabled'>lock rate</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>";
            }

            if (parts.Length > 1 && rateOption.IsDisqualified == false)
            {
                if (parts[1] != "?")
                {
                    html += string.Format(@"&nbsp;&nbsp;<a href='#' onclick=""return f01({1},'{2}', {3}, true);"">{0}</a>",
                    AspxTools.HtmlString(parts[1]), // 0
                    AspxTools.JsString(rateOption.RateOptionId), // 1
                    this.ID, // 2
                    m_applicantPrice.TempIndex // 3

                    );
                }
                else
                {
                    html += @"&nbsp;&nbsp;<a href='#' disabled='disabled'>lock rate</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>";
                }
            }
            return html;
        }
    }
}
