﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditServiceCredential.aspx.cs" Inherits="PriceMyLoan.main.EditServiceCredential" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>

<html>
<head runat="server">
    <title>Edit Service Credential</title>
    <style type="text/css">
        .material-icons {
            -ms-user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }
        #MainEditData>div:first-of-type>div:first-of-type {
            vertical-align: middle;
        }
        div label {
            display: block;
        }
        .servicecredential-table-overflow>div>div {
            padding-bottom: 1.5em;
        }
    </style>
</head>
<body>
    <form id="EditServiceCredential" runat="server">
        <div lqb-popup hidden>
            <div class="modal-header">
                <button type="button" class="close" onclick="ClosePopup()"><i class="material-icons">close</i></button>
                <h4 class="modal-title">Service Credential Editor</h4>
            </div>
            <div class="modal-body">
                <div class="table servicecredential-editor">
                    <div>
                        <div class="FieldLabel">Services</div>
                        <div>
                            <div id="IsForCreditOrVerificationDiv">
                                <div class="servicecredential-Credit">
                                    <label>
                                        <input type="checkbox" class="ServiceCb" runat="server" id="IsForCreditReport" />
                                        Credit Reports
                                    </label>
                                </div>
                                <div class="servicecredential-Verification">
                                    <label>
                                        <input type="checkbox" class="ServiceCb" runat="server" id="IsForVerifications" />
                                        Verifications (VOE/VOI, VOD/VOA, SSA-89)
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="servicecredential-table-overflow servicecredential-editor">
                    <div>
                        <div id="CraContainer" class="servicecredential-Credit">
                            <label for="CraList">Credit Reporting Agency <i class="material-icons servicecredential-req-icon"></i></label>
                            <asp:DropDownList ID="CraList" runat="server"></asp:DropDownList>
                            <input type="hidden" runat="server" id="ChosenCraId" />
                            <input type="hidden" runat="server" id="ChosenCraName" />
                        </div>
                        <div id="VerificationProviderContainer" class="servicecredential-Verification">
                            <!-- When the input is small, we want these to flow into the same row.  When they are large, they should be separate.
                            To do this, Ed suggested settings a width or max-width.  We'll also need to make the labels display block so that it handles the line break. -->
                            <label for="VerificationVendorList">Verification Provider <i class="material-icons servicecredential-req-icon"></i></label>
                            <asp:DropDownList ID="VerificationVendorList" runat="server"></asp:DropDownList>
                            <input type="hidden" runat="server" id="ChosenVerificationVendorId" />
                            <input type="hidden" runat="server" id="ChosenVerificationVendorName" />
                        </div>
                    </div>
                </div>
                <div class="table servicecredential-editor">
                    <div id="AccountIdRow">
                        <div>
                            <label for="AccountId">Account ID <i class="material-icons servicecredential-req-icon"></i></label>
                            <input type="text" id="AccountId" runat="server" maxlength="100" />
                        </div>
                    </div>
                    <div>
                        <div>
                            <label for="UserName">Login <i class="material-icons servicecredential-req-icon"></i></label>
                            <input type="text" id="UserName" runat="server" maxlength="100" />
                        </div>
                        <div>
                            <label for="UserPassword">Password <i class="material-icons servicecredential-req-icon"></i></label>
                            <asp:TextBox ID="UserPassword" runat="server" TextMode="Password" MaxLength="100" />
                        </div>
                        <div>
                            <label for="ConfirmPassword">Confirm Password <i class="material-icons servicecredential-req-icon"></i></label>
                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" MaxLength="100" />
                        </div>
                    </div>
                </div>

                <div class="BottomButtons">
                    <div class="ErrorMessage">
                        <ml:EncodedLabel ID="UserMessage" runat="server"></ml:EncodedLabel>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-flat" id="CancelBtn" runat="server" onclick="ClosePopup();" value="Cancel" />
                <asp:Button runat="server" class="btn btn-flat" ID="SaveBtn" Text="Save" OnClick="OnSaveClick"></asp:Button>
            </div>
        </div>
    </form>
    <script type="text/javascript">
    jQuery(function ($) {
            <%-- This is a list of the JQuery groups of mutually exclusive service checkboxes.
             Ex. A credit credential can also be a verification credential, but not an AUS credential. --%>
            var serviceCheckboxGroups = [
                $('#IsForCreditReport, #IsForVerifications')
            ];
            
            var serviceAssociatedElementMap = {
                'IsForCreditReport': {
                    row: $('#CraContainer')
                },
                'IsForVerifications': {
                    row: $('#VerificationProviderContainer')
                }
            };

            $('#SaveBtn').click(function () {
                return PrepForSave();
            });

            $('.ServiceCb').change(function () {
                ToggleServiceExclusivity(this);
                ToggleServiceElements(this);
                UpdatePageState();
            });

            $('#CraList').change(function () {
                SetLinkedVendor();
                ToggleAccountIdVisibility();
            });

            $('#VerificationVendorList').change(function () {
                ToggleAccountIdVisibility();
            });

            var $SaveBtn = $('#SaveBtn');
            var $ReqIcons = $('.servicecredential-req-icon');
            function UpdateSaveBtnEnabled() {
                $SaveBtn.prop('disabled', $ReqIcons.is('.red:visible'));
            }

            function UpdateRequiredIcon($i, required, satisfied) {
                $i.toggle(required).addClass('material-icons')
                    .toggleClass('green', satisfied)
                    .toggleClass('red rotate-45', !satisfied)
                    .text(satisfied ? 'check_circle' : 'add_circle');
            }

            var alwaysTrue = function () { return true; };
            var notEmpty = function(element) { return !!element && !!element.value; };
            var checkIfEnabled = function (idOfEnabledCheckbox) {
                var enabledCheckbox = document.getElementById(idOfEnabledCheckbox);
                return function() { return !!enabledCheckbox && enabledCheckbox.checked; };
            };
            var isAccountIdRequired = false;
            var inputRequiredList = [
                { id: 'CraList', required: checkIfEnabled('IsForCreditReport'), satisfied: notEmpty },
                { id: 'VerificationVendorList', required: checkIfEnabled('IsForVerifications'), satisfied: notEmpty },
                { id: 'AccountId', required: function () { return isAccountIdRequired; }, satisfied: function (element) { return !isAccountIdRequired || notEmpty(element); } },
                { id: 'UserName', required: alwaysTrue, satisfied: notEmpty },
                { id: 'UserPassword', required: alwaysTrue, satisfied: notEmpty },
                { id: 'ConfirmPassword', required: alwaysTrue, satisfied: function (element) { return notEmpty(element) && element.value === document.getElementById('UserPassword').value; } }
            ];

            var requiredDataList = [];
            var updateAccountIdRequiredIndicator;
            var updateConfirmPasswordRequiredIndicator;
            var updateVerficationVendorRequiredIndicator;
            $.each(inputRequiredList, function (index, o) {
                var requiredIcon = $('label').filter(function (i, el) { return $(el).prop('for') === o.id; }).find('i');
                var inputElement = document.getElementById(o.id);
                var refreshRequiredIndicator = function () {
                    UpdateRequiredIcon(requiredIcon, o.required(inputElement), o.satisfied(inputElement));
                };
                requiredDataList.push({
                    input: inputElement,
                    refreshRequiredIndicator: refreshRequiredIndicator
                })
                if (o.id === 'AccountId') {
                    updateAccountIdRequiredIndicator = refreshRequiredIndicator;
                } else if (o.id === 'ConfirmPassword') {
                    updateConfirmPasswordRequiredIndicator = refreshRequiredIndicator;
                } else if (o.id === 'VerificationVendorList') {
                    updateVerficationVendorRequiredIndicator = refreshRequiredIndicator;
                }
                
                $(inputElement).on('input', function () {
                    refreshRequiredIndicator();
                    if (o.id === 'UserPassword' && updateConfirmPasswordRequiredIndicator) {
                        updateConfirmPasswordRequiredIndicator();
                    }

                    UpdateSaveBtnEnabled();
                });
            });

            function SetErrorMessage(message) {
                $('.ErrorMessage').eq(0).text(message);
            }

            function PrepForSave() {
                var selectedVoxVendor = $('#VerificationVendorList option:selected');
                var selectedCra = $('#CraList option:selected');

                $('#ChosenVerificationVendorId').val(selectedVoxVendor.val());
                $('#ChosenVerificationVendorName').val(selectedVoxVendor.text());
                $('#ChosenCraId').val(selectedCra.val());
                $('#ChosenCraName').val(selectedCra.text());

                if (typeof (ML.IsDelete) !== 'undefined' && ML.IsDelete) {
                    return true;
                }

                var checkedServiceBoxes = $('.ServiceCb:checked');
                if (checkedServiceBoxes.length == 0) {
                    SetErrorMessage("Please choose at least one Service.");
                    return false;
                }

                if (!ValidateExclusiveServiceGroups(checkedServiceBoxes)) {
                    SetErrorMessage('The combination of selected services is invalid. Please ensure the combination is correct.')
                    return false;
                }

                if ($('#IsForCreditReport').prop('checked') && $('#CraList').val() === '') {
                    SetErrorMessage("Please choose a Credit Reporting Agency.")
                    return false;
                }

                if ($('#IsForVerifications').prop('checked') && $('#VerificationVendorList').val() === '') {
                    SetErrorMessage("Please choose a Verification Provider.");
                    return false;
                }

                return true;
            }

            function ValidateExclusiveServiceGroups(JQCheckedBoxes) {
                var representedGroup = -1;
                for (var checkedBox = 0; checkedBox < JQCheckedBoxes.length; checkedBox++) {
                    for (var group = 0; group < serviceCheckboxGroups.length; group++) {
                        if (group === representedGroup) {
                            continue;
                        }

                        if (serviceCheckboxGroups[group].is(JQCheckedBoxes[checkedBox])) {
                            if (representedGroup === -1)
                                representedGroup = group;
                            if (group !== representedGroup)
                                return false;
                        }
                    }
                }
                return true;
            }

            function UpdatePageState() {
                $('.ServiceCb').each(function () { ToggleServiceElements(this); });
                SetLinkedVendor();
                ToggleAccountIdVisibility();
                $.each(requiredDataList, function (index, o) { o.refreshRequiredIndicator(); });
                UpdateSaveBtnEnabled();
            }

            function ToggleServiceElements(checkbox) {
                var jqCheckbox = $(checkbox);
                var elementMap = serviceAssociatedElementMap[jqCheckbox.prop('id')];
                if (elementMap) {
                    elementMap.row.toggle(jqCheckbox[0].checked);
                }
            }

            /***
             * Unchecks all the service checkboxes that aren't included in the same service group as the passed in service checkbox.
             */
            function ToggleServiceExclusivity(serviceCheckbox) {
                var checkboxGroup = $('.ServiceCb');
                for (var i = 0; i < serviceCheckboxGroups.length; i++) {
                    if (serviceCheckboxGroups[i].is(serviceCheckbox)) {
                        checkboxGroup = serviceCheckboxGroups[i];
                    }
                }
                $('.ServiceCb').not(checkboxGroup).prop('checked', false);
            };

            function SetLinkedVendor() {
                var selectedCra = $('#CraList option:selected');
                var voxVendorList = $('#VerificationVendorList');
                var voxVenderEnabled = $('#IsForVerifications').prop('checked');
                if (!$('#IsForCreditReport').prop('checked') || selectedCra.val() === '') {
                    $('#VerificationVendorList').prop('disabled', !voxVenderEnabled);
                    return;
                }

                var linkedVendor = selectedCra.attr('data-voxVendorId');
                if (typeof (linkedVendor) === 'string' &&
                    voxVendorList.find('option[value="' + linkedVendor + '"]').length !== 0) {
                    voxVendorList.val(linkedVendor);
                    voxVendorList.prop('disabled', true);
                }
                else {
                    voxVendorList.prop('disabled', !voxVenderEnabled);
                    if (voxVendorList.prop('disabled') || typeof (linkedVendor) === 'string') {
                        voxVendorList.find('option').eq(0).prop('selected', true);
                    }
                }

                updateVerficationVendorRequiredIndicator();
            }

            function ToggleAccountIdVisibility() {
                var craEnabled = $('#IsForCreditReport').prop('checked');
                var craAccountIdState = $('#CraList option:selected').attr('data-acctid');
                var craUsesAcctId = craEnabled && (craAccountIdState === 'y' || craAccountIdState === 'r');
                var craRequiresAcctId = craUsesAcctId && craAccountIdState === 'r';

                var verificationVendorEnabled = $('#IsForVerifications').prop('checked');
                var verificationVendorAcctIdState = $('#VerificationVendorList option:selected').attr('data-acctid');
                var verificationVendorUsesAcctId = verificationVendorEnabled && (verificationVendorAcctIdState === 'y' || verificationVendorAcctIdState === 'r');
                var verificationVendorReqAcctId = verificationVendorUsesAcctId && verificationVendorAcctIdState === 'r';

                var usesAccountId = craUsesAcctId || verificationVendorUsesAcctId;
                var requiresAccountId = usesAccountId && (craRequiresAcctId || verificationVendorReqAcctId);

                $('#AccountIdRow').toggle(usesAccountId);
                isAccountIdRequired = requiresAccountId;
                if (!usesAccountId) {
                    $('#AccountId').val('');
                }

                if (typeof(updateAccountIdRequiredIndicator) === 'function') {
                    updateAccountIdRequiredIndicator();
                    UpdateSaveBtnEnabled();
                }
            }

            if (typeof (ML['ServicesToHide']) === 'string') {
                var serviceClassesToHide = $.map(ML.ServicesToHide.split('|'), function (el) { return '.servicecredential-' + el; });
                $(serviceClassesToHide.join(',')).hide();
            }

            if (typeof (ML['IsDelete']) === 'boolean' && ML.IsDelete) {
                $('.servicecredential-editor').hide();
            }

            setTimeout(UpdatePageState, 100); <%-- Give the UI time to render the changes, which will affect the :visible selectors --%>
            TPOStyleUnification && TPOStyleUnification.Components();
    });

        var iframeSelf;
        function _init() { 
            iframeSelf = new IframeSelf();
            iframeSelf.popup = new LqbPopup(document.querySelector('div[lqb-popup]'));
            if (document.getElementById('autoclose') != null) {
                var results = {
                    OK: true,
                    CredentialList: document.getElementById('credentialList').value
                };

                iframeSelf.resolve(results);
            }
        }

        function ClosePopup() {
            iframeSelf.resolve(null);
        }
    </script>
</body>
</html>
