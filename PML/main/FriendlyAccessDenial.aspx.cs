﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using PriceMyLoan.Common;

namespace PriceMyLoan.main
{
    public partial class FriendlyAccessDenial : MinimalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            BtnRow.Visible = !PriceMyLoanConstants.IsEmbeddedPML;
            string isGeneric = LendersOffice.Common.RequestHelper.GetSafeQueryString("generic");
            if (!string.IsNullOrEmpty(isGeneric) && isGeneric == "t")
            {
                ErrorMessage.Text = "Access denied.";
            }
        }
    }
}
