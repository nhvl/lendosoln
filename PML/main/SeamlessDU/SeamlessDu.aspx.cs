﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.Common;

namespace PriceMyLoan.main.SeamlessDU
{
    public partial class SeamlessDu : PriceMyLoan.UI.BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("angular-route.1.4.8.min.js");
            this.RegisterJsScript("SeamlessDu.js");
            this.RegisterJsScript("angularModules/LqbForms.module.js");
            this.RegisterJsScript("angularModules/LqbAudit.module.js");

            this.RegisterCSS("SeamlessDu.css");
            this.RegisterCSS("bootstrap.min.css");
            this.RegisterCSS("font-awesome.css");
        }
    }
}