﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeamlessDu.aspx.cs" Inherits="PriceMyLoan.main.SeamlessDU.SeamlessDu" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Seamless DU Interface</title>
    <script type="text/javascript">
        var virtualRoot = gVirtualRoot;
        var seamlessFolder = virtualRoot + '/main/SeamlessDU';
    </script>
</head>
<body id="app" ng-controller="SeamlessDUController as seamless">
    <h4 class="page-header ng-cloak">{{pageTitle}}</h4>
    <span ng-show="!loaded">Loading {{pageTitle}}...<span class="fa fa-spin fa-spinner fa-fw"></span></span>
    <div class="ng-cloak" ng-show="loaded && !loadSuccess">
        Something went wrong communicating with the server:
        <br />{{errorMessage}}
        <div class="bottomButtons"><button ng-click="closeWindow()">Close</button></div>
    </div>
    <div ng-view></div>
    <script type="text/javascript">
        if (typeof($j) === 'function') {
            // Because of the multiple angular apps on the page, bootstrap explicitly.
            $j(function () {
                angular.module('SeamlessDU').config(['$compileProvider', function ($compileProvider) {
                    $compileProvider.debugInfoEnabled(<%= AspxTools.JsBool(ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost) %>);
                }]);

                angular.bootstrap(angular.element('#app'), ['SeamlessDU']);
            });
        }
    </script>
</body>
</html>
