<%@ Page language="c#" Codebehind="ValidationErrorPage.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.ValidationErrorPage" %>
<%@ Import namespace="PriceMyLoan.Common"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Validation Error Page</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
  </HEAD>
<body leftMargin=0 topMargin=0 MS_POSITIONING="FlowLayout" scroll="yes">
<script language=javascript>
<!--
function f_validatePage() {
  if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
    return Page_ClientValidate();  
  return true;
}
function f_goToPipeline() {
  self.location = <%= AspxTools.JsString(VirtualRoot) %> + '/Main/Pipeline.aspx';
}
  function f_openHelp(page, w, h)
  {
	  var url = "/help/" + page;

	  LQBPopup.Show(<%=AspxTools.JsString(VirtualRoot)%> + url, {width: w, height: h });
  }

//-->
</script>

<form id=ValidationErrorPage method=post runat="server">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td vAlign=top>
      <table class=TopHeaderBackgroundColor cellSpacing=0 cellPadding=5 
      width="100%" border=0>
        <tr>
          <td width="1%" height="1%">
            <% if (m_allowGoToPipeline) { %>
            <input class=ButtonStyle onclick=f_goToPipeline(); type=button value=START nohighlight>
            <% } %>
          </td>
          <td>
            <table cellSpacing=0 cellPadding=0 width="100%" border=0 >
              <tr class=LoanSummaryBackgroundColor>
                <td class=LoanSummaryItem>Loan #: <ml:EncodedLiteral id=sLNm runat="server"></ml:EncodedLiteral></td>
                <td class=LoanSummaryItem><ml:EncodedLiteral id=aBNm runat="server"></ml:EncodedLiteral></td>
                <td class=LoanSummaryItem>Amt: <ml:EncodedLiteral id=sLAmtCalc runat="server"></ml:EncodedLiteral></td>
                <td class=LoanSummaryItem>LTV: <ml:EncodedLiteral id=sLtvR runat="server"></ml:EncodedLiteral></td>
                <td class=LoanSummaryItem>CLTV: <ml:EncodedLiteral id=sCltvR  runat="server"></ml:EncodedLiteral></td>
                <td class=LoanSummaryItem>Primary 
                    <a title=Help style="FONT-WEIGHT: bold; COLOR: #ff9933; TEXT-DECORATION: none" onclick="f_openHelp('Q00006.html', 400, 250);" href="#" >(?)</a> : 
                    <ml:EncodedLiteral id=sCreditScoreType1  runat="server"></ml:EncodedLiteral></td>
                <td class=LoanSummaryItem>Lowest 
                    <a title=Help style="FONT-WEIGHT: bold; COLOR: #ff9933; TEXT-DECORATION: none" onclick="f_openHelp('Q00006.html', 400, 250);" href="#" >(?)</a> : 
                    <ml:EncodedLiteral id=sCreditScoreType2 runat="server"></ml:EncodedLiteral></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="padding-left:10px;padding-top:10px"><span style="COLOR: red;font-weight:bold">
    Unable to proceed with pricing due to the following errors: <br>
    <asp:PlaceHolder ID="ErrorMessagePlaceHolder" runat="server" EnableViewState="false"></asp:PlaceHolder>
    </SPAN>
      <br>
      <% if (!PriceMyLoanConstants.IsEmbeddedPML) { %>
      <span style="font-weight:bold">Please contact your account executive for assistance.</span>
      <% } %>
      </TD></TR></TABLE></FORM>
	
  </body>
</HTML>
