<%@ Page language="c#" Codebehind="ConfirmationPage.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.ConfirmationPage"  ValidateRequest="true"%>
<%@ Import namespace="PriceMyLoan.Common"%>

<%@ Import Namespace ="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
        <title>Confirmation</title>
        <style>
        .WarningStyle { BORDER-RIGHT: #ffcc99 1px dotted; PADDING-RIGHT: 5px; BORDER-TOP: #ffcc99 1px dotted; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 5px; BORDER-LEFT: #ffcc99 1px dotted; COLOR: red; PADDING-TOP: 5px; BORDER-BOTTOM: #ffcc99 1px dotted; BACKGROUND-COLOR: #ffffcc }
        .StipWarning {
          border-right: #ffcc99 1px dotted; padding-right: 5px; BORDER-TOP: #ffcc99 1px dotted; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 5px; BORDER-LEFT: #ffcc99 1px dotted; COLOR: red; PADDING-TOP: 5px; BORDER-BOTTOM: #ffcc99 1px dotted; BACKGROUND-COLOR: #ffffcc;
          width:100%;
        }
        .Field td.FieldLabel {
        	width: 200px;
        }
        .Field td.FieldValue {
        	width: 100%;
        }
        #LpeSubmitAgreement { color:black; padding-top:2px; padding-left:3px; padding-bottom:1px; padding-right:1px; font-family:Courier New; font-size:10pt; width:100%; height:60px; overflow-x:hidden; overflow-y:scroll; background-color: #D3D3D3; border:solid 1px #888; }
        </style>
</HEAD>
    <body onbeforeunload="f_onUnload();" onload="f_registerMe( 'confirm' );" MS_POSITIONING="FlowLayout">
        <script type="text/javascript">
  <!--
            function lockField(cb, tbID) {
                if(cb != null)
                {
                    var clientID = "";
                    if (document.getElementById("_ClientID") != null)
                        clientID = document.getElementById("_ClientID").value + "_";

                    var item = document.getElementById(clientID + tbID);
                    item.readOnly = !cb.checked;

                    if (!cb.checked)
                        item.style.backgroundColor = "lightgrey";
                    else
                        item.style.backgroundColor = "";

                    if (!cb.checked)
                        item.style.backgroundColor = "lightgrey";
                    else
                        item.style.backgroundColor = "";
                }
        }

    function getUpdatedEstCloseD() {
        var loanId = <%= AspxTools.JsString(LoanID) %>;
        var sEstCloseDLckd = $('#sEstCloseDLckd').prop('checked');
        var sEstCloseD = $('#sEstCloseD').val();

        var data = {
            loanId: loanId,
            sEstCloseDLckd: sEstCloseDLckd,
            sEstCloseD: sEstCloseD
        };

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: 'ConfirmationPage.aspx/GetUpdatedEstCloseD',
            async: false,
            data: JSON.stringify(data),
            dataType: 'json'
        }).done(function(result) {
            $('#sEstCloseD').val(result.d.sEstCloseD);
        }).fail(function() {
            // If the value of sEstCloseD is locked and there was an error,
            // we don't really care because checking the box should not
            // change the value. However, unchecking the box may change the
            // value, and we should alert the user of the error.
            if (!sEstCloseDLckd) {
                alert('There was an error retrieving the updated value for the estimated close date.');
            }
        });
    }

  function ConfPageResize(w, h)
  {
      w = w < screen.width ? w : screen.width - 10;
      h = h < screen.height ? h : screen.height - 60;
      var l = (screen.width - w) / 2;
      var t = (screen.height - h - 50) / 2;
      try{
          window.resizeTo(w, h);
          window.moveTo(l,t);
      }
      catch(e)
      {
        // SK Access to resize was denied, but no need to log it.
      }
  }
  function _init() {

      $('#sEstCloseDLckd').change(function() {
          lockField(this, 'sEstCloseD');
          getUpdatedEstCloseD();
      });
      lockField(document.getElementById('sEstCloseDLckd'), 'sEstCloseD');
    document.getElementById("FirstLoanInfo").style.display = <%= AspxTools.JsBool(m_has2ndLoan) %> ? '' : 'none';
    ConfPageResize(750, <%=AspxTools.JsNumeric(m_height) %>);
    f_checkAgreementEmpty();
    
    <% if (m_isDTIError) { %>
    f_displayDTIError();
    <% } %>
    f_hideLockWarning();
    <% if (m_isRerunMode && m_isRateLockedAtSubmission && !IsEnablePmlResults3Ui) { // 7/13/2006 dd - Float rate rerun user can only choose rate lock option %>
      document.getElementById('RequestingRateLock_0').disabled = true;
      document.getElementById('RequestingRateLock_0').checked = false;
      document.getElementById('RequestingRateLock_0').title = 'A rate float request has already been submitted.';
      document.getElementById('RequestingRateLock_0').style.cursor = 'help';
      document.getElementById('divFloatLockWarning').style.display = 'none'; <%-- // 8/29/2006 dd - OPM 6645 --%>
    <% } %>
    
    <% if (!m_useOldEngine && !m_allowSubmitForLock) { // 11/01/10 mf - If using new workflow, disable lock when not allowed. %>
          document.getElementById('RequestingRateLock_1').disabled = true;
          document.getElementById('RequestingRateLock_1').checked = false;      
    <% } %>
    
    <% if (IsEncompassIntegration) { %>
      // 8/10/2009 dd - Encompass integration does not support registration.
      document.getElementById('RequestingRateLock_0').disabled = true;
      document.getElementById('RequestingRateLock_0').checked = false;
      document.getElementById('RequestingRateLock_0').title = 'Encompass does not support registration at this time.';
      document.getElementById('RequestingRateLock_0').style.cursor = 'help';
      document.getElementById('divFloatLockWarning').style.display = 'none'; <%-- // 8/29/2006 dd - OPM 6645 --%>      
      document.getElementById('RequestingRateLock_1').checked = true;

    <% } %>
    <% if (m_isBlockedRateLockSubmission) { %>
      <% if (!m_isRerunMode && !IsEncompassIntegration) { %>
        document.getElementById('RequestingRateLock_0').checked = true;
      <% } else { %>
      <%= AspxTools.JsGetElementById(m_cbAgreeCheckbox) %>.disabled = true;

      <% } %>
      document.getElementById('RequestingRateLock_1').disabled = true;
      document.getElementById('RequestingRateLock_1').checked = false;      
      document.getElementById('divWarning').style.display = 'none';

    <% } %>
    
    <% if (m_isRateLockedAtSubmission) { %>
      if (document.getElementById('RequestingRateLock_1').disabled && document.getElementById('RequestingRateLock_0').disabled)
      {
        document.getElementById('btnConfirm').disabled = true;
      }
    
      <% if (PriceMyLoanConstants.IsEmbeddedPML) { %>
        var lockDesk = <%= AspxTools.JsGetElementById(IsEmailLockDesk) %>;    
        if (document.getElementById('RequestingRateLock_1').checked && !lockDesk.disabled)
        {
          lockDesk.checked = true;
          lockDesk.disabled = true;
        }
      <% } %>
    <% } %>
    
    if (document.getElementById('LockDeskQuestionsPanel') != null)
    {
      if (document.getElementById('RequestingRateLock_0').checked) {
        $('#LockDeskQuestionsPanel').hide();
      } else {
        $('#LockDeskQuestionsPanel').show();
      }
    }
  }
  function f_onUnload()
  {
    f_registerMe( '' );
    <%-- // 08/31/06 mf - OPM 7168 If we are in error status, we go to step 3
         // in Unload in case they click X in the window bar and not our "Cancel" --%>
    if ( document.getElementById("ErrorTable").style.display == "" )
    {
      f_goToStep3();
    }
  }
  
  function f_toggleRateDisplay( bShow, position )
  {
    f_displayElement( bShow, 'Qual_'+position);
    f_displayElement( bShow, 'Point_' + position);
    f_displayElement( bShow, 'Note_' + position);
    f_displayElement( bShow, 'Teaser_' + position);
    f_displayElement( bShow, 'Margin_' + position);
    f_displayElement( bShow, 'Dti_' + position);
    f_displayElement( bShow, 'Payment_' + position);
    
    if( position === 'second' ) {
        f_displayElement(bShow, 'sProdRLckdDaysRow_second');
        f_displayElement(bShow, 'sProdRLckdExpiredDLabel');
    }
  }
  
  function f_displayElement(bShow, id)
  {
    $('#' + id).toggle(bShow);
    
  }
  
  function f_hideLockWarning() {
    if (document.getElementById('RequestingRateLock_0') != null && document.getElementById('RequestingRateLock_0').checked) {
      <%-- // Rate float is checked. Hide warning. --%>
      document.getElementById('divWarning').style.visibility = 'hidden';
      if (<%= AspxTools.JsBool(m_has2ndLoan) %>)
        f_toggleRateDisplay( false, 'first' );
      f_toggleRateDisplay( false, 'second' );
    } else if (document.getElementById('RequestingRateLock_1') != null && document.getElementById('RequestingRateLock_1').checked) {
      document.getElementById('divWarning').style.visibility= 'visible';
      if (<%= AspxTools.JsBool(m_has2ndLoan) %>)
        f_toggleRateDisplay( true, 'first' );
      f_toggleRateDisplay( true, 'second' );
    }
    
    <% if (PriceMyLoanConstants.IsEmbeddedPML && m_isRateLockedAtSubmission) { %>
    var lockDesk = <%= AspxTools.JsGetElementById(IsEmailLockDesk) %>;    
    if (document.getElementById('RequestingRateLock_0').checked && lockDesk.checked)
      lockDesk.disabled = false;
    else if(document.getElementById('RequestingRateLock_1').checked && !lockDesk.disabled)
    {
      lockDesk.checked = true;
      lockDesk.disabled = true;
    }
    <% } %>
  }
  function f_checkAgreementEmpty() {
    if (<%= AspxTools.JQuery(LpeSubmitAgreement) %>.html() == "") {
      $("#AgreementPanel").hide();
      
      var bShouldDisableForRateBlock = <%=AspxTools.JsBool(m_isRateOptionBlockForLock) %> && document.getElementById('RequestingRateLock_1').checked;
      
      if ( !bShouldDisableForRateBlock )
        setConfirmButtonEnabled(true);
    }
  }
  function f_checkEmail(bChecked) {
    var list = [<%= AspxTools.JsGetElementById(IsEmailLoanOfficer) %>,<%= AspxTools.JsGetElementById(IsEmailLenderAccExec) %>
                ,<%= AspxTools.JsGetElementById(IsEmailLockDesk) %>,<%= AspxTools.JsGetElementById(IsEmailUnderwriter) %>
                ,<%= AspxTools.JsGetElementById(IsEmailProcessor) %>,<%= AspxTools.JsGetElementById(IsEmailBrokerProcessor) %>
                ,<%= AspxTools.JsGetElementById(IsEmailExternalSecondary) %>, <%= AspxTools.JsGetElementById(IsEmailExternalPostCloser) %>];
    for (var i = 0; i < list.length; i++) {
      var o = list[i];
      if (null != o && !o.disabled)
        o.checked = bChecked;
    }
    return false;
  }
  function f_registerMe( sName ) { try
  {
    if( window.parent != null && window.parent.opener != null )
    {
        if( window.parent.opener.closed == false )
        {
            if( window.parent.opener.f_updateChildName != null )
            {
                window.parent.opener.f_updateChildName( sName );
            }
        }
    }
  }
  finally
  {
  }}
  
  function f_displayDTIError() {
    $("#DTIErrorTable").show();
    $("#MainTable").hide();
  }
  
  function f_rerun1st() {
    self.close();
  }
  function f_close() {
    self.close();
  }
  function f_goToPipeline() {
      <% if (m_isReturnToPipelineAfterClose) { %>
      if(window.parent.opener.PML && window.parent.opener.PML.goBack) {
          window.parent.opener.PML.goBack();
      }
      else {
        window.parent.opener.f_goToPipeline();
      }
    <% } %>
    self.close();  
  }
  function f_goToStep3()
  {
      if (typeof(window.parent.opener.f_goToStep3) != 'undefined')
      {
        var extraArgs = {
          SecondLienMPmt : <%= AspxTools.JsString(m_secondLienMPmt) %>
        };
        window.parent.opener.f_goToStep3(extraArgs);
      }
  }
  function f_displayCertificate() {
    if (window && window.parent && window.parent.opener && window.parent.opener.goBack) {
        window.parent.opener.goBack();
    }
    var url = escape(<%= AspxTools.JsString(VirtualRoot) %> + '/main/SavedCertificate.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>);
    self.location = <%= AspxTools.JsString(VirtualRoot) %> + '/common/PrintView_Frame.aspx?isResult=1&body_url=' + url;    
  }
  function f_displayTwoCertificates(first, second, firstNm, secondNm) {
    var url = escape(<%= AspxTools.JsString(VirtualRoot) %> + '/main/SavedCertificate.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>);
    self.location = <%= AspxTools.JsString(VirtualRoot) %> + '/common/PrintView_Frame.aspx?isResult=1&body_url=' + url + '&1st=' + first + '&2nd=' + second;    
  }
  function f_qualify2ndLoan() {
    window.parent.opener.f_qualify2ndLoan();
    window.close();
  }
  
    function f_onConfirmClick() {
    if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
    {
        if ( document.getElementById('RequestingRateLock_1') != null && document.getElementById('RequestingRateLock_1').checked )
        {
          if (!Page_ClientValidate()) {
            return false;
          }
        }
        else 
        {
            if (!Page_ClientValidate("ExceptionReasonValidatorGroup")) {
            return false;
          }
        }
    }
  
    f_displayWait();
    __doPostBack('', '');
  }


  function f_displayError(msg) {
    $('#ErrorTable').show();
    $('#ErrorMessage').html(msg);
    $('#MainTable').hide();
    $('#DTIErrorTable').hide();
  }
  function f_displayWait() {
   $("#WaitTable").show();
   $("#MainTable").hide();
   $("#DTIErrorTable").hide();
    
  }
  function f_onclick_agree(cb) {
  
    var bShouldDisableForRateBlock = <%=AspxTools.JsBool(m_isRateOptionBlockForLock) %> && document.getElementById('RequestingRateLock_1').checked;
    
    if (bShouldDisableForRateBlock) {
        document.getElementById('btnConfirm').disabled = true;
    }
    else {
        setConfirmButtonEnabled(cb.checked);
    }
  }

  function setConfirmButtonEnabled(enable) {
      var registerLoanOption = document.getElementById('RequestingRateLock_0');
      var lockLoanOption = document.getElementById('RequestingRateLock_1');

      var areOptionsDisabled = (registerLoanOption != null && registerLoanOption.disabled) &&
          (lockLoanOption != null && lockLoanOption.disabled);

      document.getElementById('btnConfirm').disabled = areOptionsDisabled || !enable;
  }
  function f_onkeyup_note() {
    TextAreaMaxLength(<%= AspxTools.JsGetElementById(sLpeNotesFromBrokerToUnderwriterNew) %>, <%= AspxTools.JsNumeric(m_noteLength) %>);
  }  
  <% if (!m_isPmlSubmissionAllowed) { %>
  function f_printPage() {
    self.focus();
    window.print();
    return false;
  }
  function f_exportFNMA() {
    location.href = 'FannieExport.aspx?loanid=' + <%= AspxTools.JsString(LoanID)%>;
    return false;
  }
  function f_launchCiti() {
    var url = <%= AspxTools.JsString(m_url) %>;
    var options = 'width=800,height=600,location=yes,menubar=yes,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=yes';
    var winHandle = window.open(url, null, options);
    if (null == winHandle) alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
    
    return false;
  }
  <% } %>
  <% if (IsEncompassIntegration) { %>
  function f_sendToEncompass() {
    window.parent.opener.f_sendToEncompass(self);
    self.close();
  }
  <% } %>
  
  function f_RequestingRateLock_onClick() {
    f_hideLockWarning();
    if (document.getElementById('LockDeskQuestionsPanel') != null) {
        if (document.getElementById('RequestingRateLock_0') != null && document.getElementById('RequestingRateLock_0').checked) {
            document.getElementById('LockDeskQuestionsPanel').style.display = 'none';
        } else if (document.getElementById('RequestingRateLock_1') != null && document.getElementById('RequestingRateLock_1').checked) {
            document.getElementById('LockDeskQuestionsPanel').style.display = 'block';
        }
    }
    
    f_onclick_agree(<%= AspxTools.JsGetElementById(m_cbAgreeCheckbox) %>);
    f_checkAgreementEmpty();
  }
  
  //-->
        </script>
        <form id="ConfirmationPage" method="post" runat="server">
            <table id="WaitTable" style="DISPLAY: none" height="100%" width="100%">
                <tr>
                    <td class="WaitMessageLabel" vAlign="middle" align="center">Please Wait...</td>
                </tr>
            </table>
            <table id="ErrorTable" style="DISPLAY: none" height="100%" width="100%">
                <tr>
                    <td style="FONT-WEIGHT: bold; COLOR: red" vAlign="middle" align="center"><span id="ErrorMessage">Ineligible 
                            1st Loan Program. Choose different loan program.</span></td>
                </tr>
                <tr>
                    <td align="center"><input class="ButtonStyle" onclick="self.close();" type="button" value="Cancel" NoHighlight>
                    </td>
                </tr>
            </table>
            <table id="DTIErrorTable" style="DISPLAY: none" height="100%" width="100%">
              <tr><td style="COLOR:black">
              <table>
              <tr><td style="COLOR:black"><ml:PassthroughLiteral id="DTIErrorMessage" runat="server"></ml:PassthroughLiteral></td></tr>
              <tr><td align="center"><input class="ButtonStyle" onclick="f_onConfirmClick();" type="button" value="Yes" NoHighlight />
              &nbsp;&nbsp;<input runat="server" id="CancelBtn" class="ButtonStyle" onclick="self.close();" type="button" value="Cancel" NoHighlight /></td></tr>
              </table>
              
              </td></tr>
            </table>
            <table id="MainTable" cellSpacing="0" cellPadding="1">
                <% if (m_isPmlSubmissionAllowed) { %>
                    <tr>
                        <% 
                       string lockOrRegister;
                       if(m_rateLockRequested) 
                       {
                           lockOrRegister = "lock the rate";
                       }
                       else
                       {
                           lockOrRegister = "register this loan";
                       }
                       %>
                        <td class="FieldLabel" colSpan="2">Click Confirm to <%=AspxTools.HtmlString(lockOrRegister)%>.  Note that by doing so you may lose edit access.</td>
                    </tr>
                <% } else { %>
                    <%-- OPM 19783 - Display Loan Number & Borrower Name if IsPmlSubmissionAllowed is off --%>
                    <tr>
                      <td class="FieldLabel">Loan Number</td>
                      <td class="FieldValue"><ml:EncodedLiteral ID="sLNm" Runat="server" /></td>
                    </tr>
                    <tr>
                      <td class="FieldLabel" nowrap>Borrower Name</td>
                      <td class="FieldValue"><ml:EncodedLiteral ID="aBNm" runat="server" /></td>
                    </tr>
                <% } %>
                <tr>
                    <td colSpan="2">&nbsp;
                    </td>
                </tr>
                <tbody id="FirstLoanInfo">
                    <tr>
                        <td class="ResultProductType" colSpan="2">First Loan Rate Information</td>
                    </tr>
                    <tr class="Field">
                        <td class="FieldLabel" noWrap>Product Name</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sLpTemplateNm" runat="server" /></td>
                    </tr>
                    <tr class="Field" id="Note_first">
                        <td class="FieldLabel" >Note Rate</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sNoteIRSubmitted" runat="server" /></td>
                    </tr>
                    <% if (!m_sIsIncomeAssetsNonRequiredFhaStreamline){ %>
                    <tr class="Field" id="Qual_first">
                      <td class="FieldLabel" >Qualifying Rate</td>
                      <td class="FieldValue"><ml:EncodedLiteral ID="sQualIR" Runat="server" /></td>
                    </tr>
                    <% } %>
                    <% if (m_isOptionARMUsedInPml) { %>
                    <tr class="Field" id="Teaser_first">
                      <td class="FieldLabel" >Teaser Rate</td>
                      <td class="FieldValue"><ml:EncodedLiteral ID="sOptionArmTeaserR" Runat="server" /></td>
                    </tr>
                    <% } %>
                    <tr class="Field" id="Point_first">
                        <td class="FieldLabel" ><%= AspxTools.HtmlString(m_pointPriceLabel) %></td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sLOrigFPcSubmitted" runat="server" /></td>
                    </tr>
                    <% if (m_sRAdjMarginRVisible) { %>
                    <tr class="Field" id="Margin_first">
                        <td class="FieldLabel" >Margin</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sRAdjMarginR" runat="server" /></td>
                    </tr>
                    <% } %>
                    <tr class="Field" id="Payment_first">
                        <td class="FieldLabel" >Payment</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sProThisMPmt" runat="server" /></td>
                    </tr>
                    <% if (!m_sIsIncomeAssetsNonRequiredFhaStreamline){ %>
                    <tr class="Field" id="Dti_first">
                        <td class="FieldLabel" >DTI</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sQualBottomR" runat="server" /></td>
                    </tr>
                    <% } %>
                    <tr class="Field">
                        <td class="FieldLabel" >&nbsp;</td>
                        <td class="FieldValue"></td>
                    </tr>
                    <tr>
                        <td class="ResultProductType" colSpan="2">Second Loan Rate Information</td>
                    </tr>
                </tbody>
                    <tr class="Field">
                        <td class="FieldLabel" nowrap>Product Name&nbsp;&nbsp;</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="ProductName" runat="server" /></td>
                    </tr>
                    <tr class="Field" id="sProdRLckdDaysRow_second">
                        <td class="FieldLabel"  noWrap>Rate Lock Period</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sProdRLckdDays" runat="server" /></td>						    
                    </tr>
                    <tr class="Field" id="sProdRLckdExpiredDLabelRow">
                        <td class="FieldLabel"  noWrap>Rate Lock Expiration Date</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="sProdRLckdExpiredDLabel" runat="server"></ml:EncodedLiteral></td>
                    </tr>
                    <% if (!m_sIsIncomeAssetsNonRequiredFhaStreamline){ %>
                    <tr class="Field" id="Qual_second">
                      <td class="FieldLabel"  noWrap>Qualify Rate</td>
                      <td class="FieldValue"><ml:EncodedLiteral ID="QRate" Runat="server" /></td>
                    </tr>
                    <% } %>
                    <tr class="Field" id="Note_second">
                        <td class="FieldLabel">Note Rate</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="Rate" runat="server" /></td>
                    </tr>						
                    <% if (m_isOptionARMUsedInPml) { %>
                    <tr class="Field" id="Teaser_second">
                      <td class="FieldLabel">Teaser Rate</td>
                      <td class="FieldValue"><ml:EncodedLiteral ID="TeaserRate" Runat="server" /></td>
                    </tr>
                    <% } %>
                    <tr class="Field" id="Point_second">
                        <td class="FieldLabel" ><%= AspxTools.HtmlString(m_pointPriceLabel) %></td>
                        <td class="FieldValue"><ml:EncodedLiteral id="Point" runat="server" /></td>
                    </tr>
                    <% if (m_marginVisible) { %>
                    <TR class="Field" id="Margin_second">
                        <TD class="FieldLabel" >Margin</TD>
                        <TD class="FieldValue"><ml:EncodedLiteral ID="Margin" runat="server" /></TD>
                    </TR>
                    <% } %>
                    <tr class="Field" id="Payment_second">
                        <td class="FieldLabel" >Payment</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="Payment" runat="server" /></td>
                    </tr>
                    <% if (!m_sIsIncomeAssetsNonRequiredFhaStreamline){ %>
                    <tr class="Field" id="Dti_second">
                        <td class="FieldLabel" >DTI</td>
                        <td class="FieldValue"><ml:EncodedLiteral id="Bottom" runat="server" />&nbsp;</td>
                    </tr>
                    <% } %>
                    <% if (m_isRateLockedAtSubmission) { %>
                    <% if (m_hasRateLockSubmissionUserWarningMessage) { %>
                    <tr>
                        <td class="FieldLabel" colspan="2" style="PADDING-TOP:5px">
                            <div class="WarningStyle">
                                <%= AspxTools.HtmlString(m_rateLockSubmissionUserWarningMessage) %>
                            </div>
                        </td>
                    </tr>
                    <% } %>
                    
                    <tr>
                    <td class="FieldLabel" colspan="3" style="PADDING-TOP:5px">
                        <div id="divWarning" class="WarningStyle">
                            WARNING:&nbsp; Worst case pricing 
                            will apply if lock is broken.
                            <span id="divFloatLockWarning">
                                &nbsp; Register now and lock later if you are unsure about the 
                                closing date.
                            </span>
                        </div>
                    </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" noWrap colspan="2">Request Type&nbsp;<asp:RadioButtonList id="RequestingRateLock" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" onclick="f_RequestingRateLock_onClick();">
                                <asp:ListItem Value="0">Register Loan&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">Lock Rate</asp:ListItem>
                            </asp:RadioButtonList>
                            &nbsp;<ml:EncodedLabel id="m_noLockText" style="color:Red" runat=server ></ml:EncodedLabel>
                            </td>
                    </tr>
                    <% } // if (m_isRateLockedAtSubmission) %>
                    <% if (PriceMyLoanConstants.IsEmbeddedPML) { %>
                    <tr class="Field">
                        <td class="FieldLabel" nowrap>Estimated Closing Date&nbsp;&nbsp;</td>
                        <td class="FieldLabel">
                            <ml:DateTextBox id="sEstCloseD" runat="server" width="75" preset="date"></ml:DateTextBox>
                            <asp:CheckBox runat="server" ID="sEstCloseDLckd" />Lock
                        </td>
                    </tr>
                    <tr><td colspan=2><span class="FieldLabel">Email Certificate to &nbsp;</span>(<a href="#" onclick="return f_checkEmail(true);">check all</a>) (<a href="#" onclick="return f_checkEmail(false);">uncheck all</a>)</td></tr>
                    <tr>
                        <td colspan=2 style="PADDING-LEFT:20px">
                            <table width="100%" cellpadding="0" cellspacing="0" border=0>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id=IsEmailLoanOfficer runat="server" /></td></tr>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id=IsEmailBrokerProcessor runat="server" /></td></tr>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id="IsEmailExternalSecondary" runat="server" /></td></tr>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id="IsEmailExternalPostCloser" runat="server" /></td></tr>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id=IsEmailLenderAccExec runat="server" /></td></tr>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id=IsEmailProcessor runat="server" /></td></tr>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id=IsEmailLockDesk runat="server" /></td></tr>
                                <tr><td colspan=2 class="FieldLabel"><asp:CheckBox id=IsEmailUnderwriter runat="server" /></td></tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td colSpan="2">&nbsp;&nbsp;</td>
                    </tr>
                    <% if (IsCitiNoSubmissionAllowed && PriceMyLoan.Common.PriceMyLoanConstants.IsCitiUpdatingRates ) { %>
                      <tr><td colspan="2" style="text-align: center"><div class="WarningStyle">
                        CitiMortgage pricing is unavailable from 7:30-8:30 Central while we update today's pricing.
                      </div></td></tr>
                    <% } %>
                    
                <tbody id="DenialReasonsPanel" runat="server" visible="false">
                <tr>
                    <td colspan="2">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        
                        <% if ( HasDenialReasons ) { %>
                        <tr>
                            <td class="SubHeader" style="color:Navy"><% if ( m_has2ndLoan ) { %>1st&nbsp;<% } %>Denial Reasons:</td>
                            <td class="SubHeader" style="color:Navy">Reason for requesting denial exception:</td>
                        </tr>
                        <% } %>
                        <asp:Repeater ID="DenialReasonsRepeater" runat="server" EnableViewState="true">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        �&nbsp;<span class="FieldLabel" style="color:red; white-space: pre-wrap"><ml:PassthroughLabel runat="server" ID="DenialReason" Text='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DenialReason").ToString()) %>'></ml:PassthroughLabel></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="ExceptionReason" runat="server" Width="400px"></asp:TextBox>                                
                                        <asp:RequiredFieldValidator ID="ExceptionReasonValidator" ControlToValidate="ExceptionReason" runat="server" CssClass="RequireValidatorMessage" ValidationGroup="ExceptionReasonValidatorGroup" ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                <tbody id="DenialReasons2ndPanel" runat="server" visible="false">
                    <tr>
                        <td class="SubHeader" style="color:Navy">2nd Denial Reasons:</td>
                        <td class="SubHeader" style="color:Navy"><% if ( !HasDenialReasons ) { %> Reason for requesting denial exception: <% } %></td>
                    </tr>
                    <asp:Repeater ID="DenialReasons2ndRepeater" runat="server" EnableViewState="true">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    �&nbsp;<span class="FieldLabel" style="color:red"><ml:PassthroughLabel runat="server" ID="DenialReason2nd" Text='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DenialReason2nd").ToString()) %>'></ml:PassthroughLabel></span>
                                </td>
                                <td>
                                    <asp:TextBox ID="ExceptionReason2nd" runat="server" Width="400px"></asp:TextBox>                                
                                    <asp:RequiredFieldValidator ID="ExceptionReason2ndValidator" ControlToValidate="ExceptionReason2nd" runat="server" CssClass="RequireValidatorMessage" ValidationGroup="ExceptionReasonValidatorGroup" ></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        <tbody id="LockDeskQuestionsPanel" runat="server" visible="false">
            <tr>
                <td class="FieldLabel" colspan="2">Lock Desk Questions</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Repeater ID="LockDeskQuestions" runat="server" EnableViewState="true">
                        <HeaderTemplate>
                            <ol>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <li>
                                    <ml:PassthroughLabel runat="server" ID="LockDeskQuestion" Text='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Question").ToString()) %>'></ml:PassthroughLabel>
                                    <br />
                                    <asp:TextBox ID="LockDeskAnswer" runat="server" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="LockDeskAnswerValidator" ControlToValidate="LockDeskAnswer" runat="server" CssClass="RequireValidatorMessage"></asp:RequiredFieldValidator>
                                </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ol>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </tbody>
        
        <tbody id="PreviousMessageUnderwriterPanel" runat="server" enableviewstate="false">
              <tr><td class="FieldLabel" colspan="2">Previous Message to Lender</td></tr>
              <tr>
                <td class="FieldLabel" colspan="2">
                  <asp:TextBox ID="sLpeNotesFromBrokerToUnderwriterHistory" runat="server" ReadOnly="True" TextMode="MultiLine" Width="100%" Height="70px"></asp:TextBox></td>
              </tr>
        </tbody>
        <tbody id="MessageToLenderPanel" runat="server" enableviewstate="false">
          <tr>
            <td class="FieldLabel" colspan="2">Message to Lender</td>
          </tr>
          <tr>
            <td colspan="2">
              <asp:TextBox ID="sLpeNotesFromBrokerToUnderwriterNew" runat="server" TextMode="MultiLine" Width="100%" Height="60px" onkeyup="f_onkeyup_note();" /></td>
          </tr>
        </tbody>
        <tbody>
          <tr><td class="FieldLabel" colspan="2">Warning</td></tr>
          <tr><td colspan="2">
          <div class="StipWarning">
              <asp:Repeater ID="StipulationWarnings" runat="server" OnItemDataBound="StipulationWarnings_ItemDataBound">
                  <ItemTemplate>
                      <%-- dd 05/11/09 - This could introduce XSS bug. However I assume Stipulation warning can only populate text enter by our engineer. --%>
                      <ml:PassthroughLiteral ID="StipulationWarning" runat="server"/>
                  </ItemTemplate>
                  <SeparatorTemplate>
                      <br />--------<br />
                  </SeparatorTemplate>
              </asp:Repeater>
          </div>
          </td></tr>
        </tbody>
        <tbody id="AgreementPanel">
          <tr>
            <td align="left" colspan="2" class="FieldLabel">Agreement</td>
          </tr>
          <tr>
            <td align="left" colspan="2">
              <div id="LpeSubmitAgreement" runat="server"></div>
            </td>
          </tr>
          <% if (m_isPmlSubmissionAllowed || IsEncompassIntegration) { %>
          <tr>
            <td align="left" colspan="2" class="FieldLabel"><asp:CheckBox ID="m_cbAgreeCheckbox" runat="server" Text="I Agree" onclick="f_onclick_agree(this);" /></td>
          </tr>
          <% } %>
        </tbody>
                            <% if (m_isPmlSubmissionAllowed || IsEncompassIntegration) { %>
                            <tr>
                                <td align="center" colSpan="2">
                                    <input class="ButtonStyle" onclick="f_onConfirmClick();" type="button" value="Confirm" NoHighlight id="btnConfirm" disabled>
                                    &nbsp;&nbsp;&nbsp; 
                                    <input class="ButtonStyle" onclick="f_close();" type="button" value="Cancel" runat="server" id="CancelBtn2" NoHighlight>
                                </td>
                            </tr>
                            <% } else if (IsCitiNoSubmissionAllowed) { %>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                              <td align="center" colspan="2">
                                <a href="#" onclick="return f_printPage();">Print this page</a>&nbsp;&nbsp;&nbsp;
                                <a href="#" onclick="return f_exportFNMA();">Export in FannieMae format</a>&nbsp;&nbsp;&nbsp;
                                <a href="#" onclick="return f_launchCiti();">Launch Citi website</a>
                              </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            
                            <tr>
                              <td align="center" colspan="2">
                                <input class="ButtonStyle" onclick="f_close();" type="button" value="Close Window" NoHighlight>
                              </td>
                            </tr>
                            <% } %>
            </table>

        </form>
    </body>
</HTML>
