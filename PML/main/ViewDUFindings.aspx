<%@ Page language="c#" Codebehind="ViewDUFindings.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.ViewDUFindings" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ViewDUFindings</title>
  </head>
  <body MS_POSITIONING="FlowLayout">
    <form id="ViewDUFindings" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" height="100%" align="center" border="0">
      <tr>
        <td style="font-weight: bold; font-size: 14pt" valign="center" align="middle"><%= AspxTools.HtmlString(m_noFindingMessage) %></td>
      </tr>
    </table>
    </form>
  </body>
</html>
