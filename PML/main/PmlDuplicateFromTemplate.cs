using System;

using DataAccess;
using PriceMyLoan.DataAccess;
using LendersOffice.Constants;
namespace PriceMyLoan.main
{
	public class PmlDuplicateFromTemplate
	{

        /// <summary>
        /// Loop through all preparer information and copy NON-EMPTY preparer to source loan file. 
        /// </summary>
        /// <param name="templateID"></param>
        /// <param name="sLId"></param>
		public static void DuplicatePreparers(Guid templateID, Guid sLId)
		{
            CPageData dataLoan = new CPreparerOnlyData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CPageData templateLoan = new CPreparerOnlyData(templateID);
            templateLoan.InitLoad();

            foreach (var templatePreparer in templateLoan.PreparerCollection.Values)
            {
                if (templatePreparer.CompanyName != "" || templatePreparer.PreparerName != "") 
                {
                    // 11/17/2004 dd - Only copy to source loan file, if CompanyName and PreparerName from template is not empty.
                    IPreparerFields loanPreparer = dataLoan.GetPreparerOfForm(templatePreparer.PreparerFormT, E_ReturnOptionIfNotExist.CreateNew);

                    loanPreparer.CaseNum             = templatePreparer.CaseNum;
                    loanPreparer.CellPhone           = templatePreparer.CellPhone;
                    loanPreparer.City                = templatePreparer.City;
                    loanPreparer.CompanyName         = templatePreparer.CompanyName;
                    loanPreparer.DepartmentName      = templatePreparer.DepartmentName;
                    loanPreparer.EmailAddr           = templatePreparer.EmailAddr;
                    loanPreparer.FaxNum              = templatePreparer.FaxNum;
                    loanPreparer.FaxOfCompany        = templatePreparer.FaxOfCompany;
                    loanPreparer.LicenseNumOfAgent   = templatePreparer.LicenseNumOfAgent;
                    loanPreparer.LicenseNumOfCompany = templatePreparer.LicenseNumOfCompany;
                    loanPreparer.PagerNum            = templatePreparer.PagerNum;
                    loanPreparer.Phone               = templatePreparer.Phone;
                    loanPreparer.PhoneOfCompany      = templatePreparer.PhoneOfCompany;
                    loanPreparer.PrepareDate_rep     = templatePreparer.PrepareDate_rep;
                    loanPreparer.PreparerName        = templatePreparer.PreparerName;
                    loanPreparer.State               = templatePreparer.State;
                    loanPreparer.StreetAddr          = templatePreparer.StreetAddr;
                    loanPreparer.Title               = templatePreparer.Title;
                    loanPreparer.Zip                 = templatePreparer.Zip;
                    loanPreparer.Update();
                }
            }
            dataLoan.Save();
		}
	}
}
