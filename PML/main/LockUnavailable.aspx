﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LockUnavailable.aspx.cs" Inherits="PriceMyLoan.main.LockUnavailable" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Lock Unavailable</title>
    <style>
    .Message { 
      color:red;
      font-weight:bold;
      padding-top:50px;
      margin-left:20px;
      margin-right:20px;
      margin-bottom:50px;
      }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Message">
      <%= AspxTools.HtmlString(m_rateLockSubmissionUserWarningMessage) %>
    </div>
    			
    </form>
</body>
</html>
