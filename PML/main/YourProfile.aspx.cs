using System;
using System.Collections.Generic;
using DataAccess;
using System.Drawing;
using System.Web.Services;
using PriceMyLoan.Security;
using PriceMyLoan.Common;
using LendersOffice.Common;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.ObjLib.ServiceCredential;
using LendersOffice.ObjLib.UI.Themes;
using PrincipalFactory = LendersOffice.Security.PrincipalFactory;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.main
{
    /// <summary>
    /// Summary description for YourProfile.
    /// </summary>
    public partial class YourProfile : PriceMyLoan.UI.BasePage
    {
        protected bool m_isForcedChange;
        private EmployeeDB m_employee;

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        private static object[] ApiWrap<T>(Func<T> f)
        {
            try
            {
                string error = null;
                object result = null;

                Tools.LogAndThrowIfErrors(() =>
                {
                    result = f();
                });

                return new[] { error, result };
            }
            catch (CBaseException e)
            {
                return new[] { e.Message, null };
            }
        }

        [WebMethod]
        public static object[] RevokeClientCertificate(Guid certificateId)
        {
            return ApiWrap(() =>
            {
                var currentPrincipal = PrincipalFactory.CurrentPrincipal;
                ClientCertificate.Delete(currentPrincipal.BrokerId, currentPrincipal.UserId, certificateId);
                return ClientCertificate.ListByUser(currentPrincipal.BrokerId, currentPrincipal.UserId);
            });
        }

        [WebMethod]
        public static object[] DeleteRegisteredIp(int id)
        {
            return ApiWrap(() =>
            {
                var currentPrincipal = PrincipalFactory.CurrentPrincipal;
                var m_employee = EmployeeDB.RetrieveById(currentPrincipal.BrokerId, currentPrincipal.EmployeeId);
                if (!m_employee.EnabledMultiFactorAuthentication) return null;

                UserRegisteredIp.Delete(currentPrincipal.BrokerId, currentPrincipal.UserId, id);
                return UserRegisteredIp.ListByUserId(currentPrincipal.BrokerId, m_employee.UserID);
            });
        }

        [WebMethod]
        public static object[] GetData()
        {
            return ApiWrap(() =>
            {
                var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;
                var employee = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
                var broker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);

                return new
                {
                    BrokerDB = new
                    {
                        broker.IsEnableMobileDeviceRegistrationTPO,
                    },
                    EmployeeDB = new
                    {
                        employee.EnabledMultiFactorAuthentication,
                        employee.EnabledClientDigitalCertificateInstall,
                        employee.EnableAuthCodeViaAuthenticator
                    },

                    clientCertificates = ClientCertificate.ListByUser(employee.BrokerID, employee.UserID),
                    registeredIps = !employee.EnabledMultiFactorAuthentication ? null :
                        UserRegisteredIp.ListByUserId(broker.BrokerID, employee.UserID),
                    registeredMobileDevices = !broker.IsEnableMobileDeviceRegistrationTPO ? null :
                        MobileDeviceUtilities.RetrieveUsersMobileDevices(PriceMyLoanUser.UserId, PriceMyLoanUser.BrokerId, PriceMyLoanUser.ConnectionInfo),
                };
            });
        }


        protected void PageLoad(object sender, System.EventArgs e)
        {
            RegisterCSS("InstallClientCertificate.css");

            m_userName.Text = PriceMyLoanUser.LoginNm;
            SecurityQuestions.SetUserInfo(PriceMyLoanUser.BrokerId, PriceMyLoanUser.UserId);

            m_employee = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);

            if (!Page.IsPostBack)
            {
                if (Request["cmd"] != null && Request["cmd"].ToLower() == "forcedchange")
                {
                    m_isForcedChange = true;
                    m_passwordStatus.Text = "Your password has expired";
                    BackToPipeline.Visible = false;
                    this.SetUpPasswordChangeStyling();
                    this.tabSection.Value = "#secLoginSettings";
                }
            }

            if (RequestHelper.GetBool("if"))
            {
                LogOffBtn.Visible = false;
                BackToPipeline.Visible = false;
            }

            this.RegisterJsScript("LQBNestedFrameSupport.js");
            this.RegisterJsScript("jquery-ui.js");
            this.RegisterJsScript("/webapp/combobox.js");
            this.RegisterCSS("/webapp/jquery-ui.css");
            this.RegisterCSS("/style.css");
            RegisterCSS("/webapp/tabStyle_TPO.css");

            this.RegisterService("main", "/main/YourProfileService.aspx");

            UserInformation.EmployeeId = PriceMyLoanUser.EmployeeId;
            ServiceCredentialContext editCredentialContext = ServiceCredentialContext.CreateEditFromProfileContext(this.PriceMyLoanUser);
            if (ServiceCredential.HasAvailableServiceCredentialConfiguration(editCredentialContext))
            {
                this.RegisterJsScript("ServiceCredential.js");
                var serviceCredentials = ServiceCredential.GetTpoEditableEmployeeServiceCredentials(editCredentialContext);
                var serviceCredentialsJson = ObsoleteSerializationHelper.JsonSerialize(serviceCredentials);
                Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
            }
        }

        /// <summary>
        /// Sets up the styling on the profile page when the user needs to change their password.
        /// </summary>
        /// <remarks>
        /// Setting up the styling when a user's password needs to be changed is required since
        /// the application will redirect requests from StyleProvider.aspx to this page until
        /// the password has been changed.
        /// </remarks>
        private void SetUpPasswordChangeStyling()
        {
            this.MainBody.Attributes["class"] += "add-padding";

            var colorThemeId = PriceMyLoanConstants.IsEmbeddedPML
                ? SassDefaults.LqbDefaultColorTheme.Id
                : this.PriceMyLoanUser.TpoColorThemeId;

            var compiledTheme = ThemeManager.GetCssForColorTheme(
                this.PriceMyLoanUser.BrokerId,
                colorThemeId,
                ThemeCollectionType.TpoPortal);

            var tag = $"<style type='text/css'>{compiledTheme}</style>";
            this.ClientScript.RegisterClientScriptBlock(typeof(YourProfile), "ColorTheme", tag);
        }





        /// <summary>
        /// Restore our state from the last save operation prior
        /// to returning from posting back.
        /// </summary>
        ///
        protected override void LoadViewState(object savedState)
        {
            // Load our flags from the streamed out sequence.
            // First delagate to base to initialize.

            base.LoadViewState(savedState);

            try
            {
                // Restore our flags because we assume that the
                // page made this decision up front.

                m_isForcedChange = (bool) ViewState["IsForcedChange"];
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(e);
            }
        }

        /// <summary>
        /// Saves user control state between postbacks so that last
        /// setting is preserved.
        /// </summary>
        protected override object SaveViewState()
        {
            // Commit our flags to the persisted sequence.

            try
            {
                // Commit our flags to the persisted sequence.

                ViewState.Add("IsForcedChange", m_isForcedChange);
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(e);
            }

            // Delegate to the base implementation.

            return base.SaveViewState();
        }

        public void BackToPipeline_Click(object sender, EventArgs args)
        {
            Response.Clear();
            Response.Redirect(PriceMyLoanConstants.PipelineUrl);
        }


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }

        #endregion

        public static void Signout()
        {
            PriceMyLoanRequestHelper.Signout(PrincipalFactory.CurrentPrincipal);
        }

        /// <summary>
        /// Executed when the user clicks the "Save" button on the page. Registered as the button event handler on the aspx page.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        protected void ChangeClick(object sender, System.EventArgs e)
        {
            // 6/2/2010 dd - Base on Citi request (M8). We only accept POST request.
            if (Page.Request.HttpMethod != "POST")
            {
                return;
            }
            bool success = true;

            m_employee = new EmployeeDB(PriceMyLoanUser.EmployeeId, PriceMyLoanUser.BrokerId);

            this.UserInformation.SubmitEmployeeContactInfoToEmployee(m_employee);

            if (!m_isForcedChange) //Security questions control does not appear on the profile page in forced password change scenario
            {
                // If security question save fails, success = false.
                success = SecurityQuestions.Save() && success;
                m_securityQuestionStatus.Text += SecurityQuestions.StatusMessage;
            }

            // If either security question or password save fails, return
            if (!(ChangePassword() && success))
            {
                return;
            }

            //redirect to main page and remove password expired cookie
            if (m_isForcedChange)
            {
                // 01/08/08 mf. OPM 19655 review.  The call below issues a redirect
                // to a url pulled from a viewstate string object.  Since viewstate
                // is just a hidden form field, it is client-modifyable.  However,
                // in this case, it looks like on every postback we set the value
                // again to a static url (PriceMyLoanConstants.PipelineUrl) in load.
                // We also check the state of a context cache item, "RedirectUrl"
                // that is never used.  Perhaps we should place the url string here
                // directly and avoid using viewstate for this.

                // 08/12/2016 je - OPM 247106 - As part of our security audit, I've
                // decided to following Matt's advice and simply place the url string
                // here directly, as "RedirectUrl" does not seem to be set in any spot
                // in code that redirects to this page.

                Response.Redirect(PriceMyLoanConstants.PipelineUrl);
            }
        }

        /// <summary>
        /// Attempts to change the user password for <see cref="m_employee"/> based on page values.
        /// </summary>
        /// <returns>Whether the operation was successful. False if errors were encountered.</returns>
        private bool ChangePassword()
        {
            if (string.IsNullOrEmpty(m_previousPassword.Text.TrimWhitespaceAndBOM()) &&
                string.IsNullOrEmpty(m_newPassword.Text.TrimWhitespaceAndBOM()))
                return true; //User doesnt want to change the password

            if (!CheckPasswordRules())
            {
                return false;
            }

            // Everything is well, carry on with password changes.
            SetEmployeePassword(m_employee);
            return true;
        }

        /// <summary>
        /// Checks whether the password rules are passed by the values in the page input.
        /// </summary>
        /// <returns>True if password rules pass. False if rules did not pass.</returns>
        private bool CheckPasswordRules()
        {
            string oldPassword = m_previousPassword.Text.Trim();
            string newPassword = m_newPassword.Text.Trim();
            string confirmPassword = m_confirmPassword.Text.Trim();

            var status = new List<string>(3);

            int numBadPw = PasswordUtil.GetNumberBadPwAllowed(PriceMyLoanUser.BrokerId);

            if (string.IsNullOrWhiteSpace(oldPassword))
            {
                status.Add("Old Password is missing.");
            }
            else if (!m_employee.IsCurrentPassword(oldPassword))
            {
                status.Add("Old Password does not match.");
                int loginFailureCount = PasswordUtil.IncrementLoginFailureCount(m_employee.BrokerID, m_employee.UserID);
                if (loginFailureCount >= numBadPw)
                {
                    AllUserDB.LockAccount(m_employee.BrokerID, m_employee.UserID, PrincipalFactory.CurrentPrincipal);
                    Signout();
                }
                else
                {
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(PrincipalFactory.CurrentPrincipal, LoginSource.Website);
                }
            }

            if (newPassword != confirmPassword)
            {
                status.Add("Passwords do not match.");
            }

            string sStatus;
            if (PasswordUtil.VerifyPassword(m_employee, newPassword, out sStatus) != StrongPasswordStatus.OK)
            {
                status.Add(sStatus);
            }

            if (status.Count > 0)
            {
                m_passwordStatus.Text += string.Join("<br>", status);
            }

            return (status.Count < 1);
        }

        /// <summary>
        /// Attempts to set the password information for an employee based on the <see cref="m_newPassword"/> control on the page. Updates password status with results.
        /// </summary>
        /// <param name="employee">The <see cref="EmployeeDB"/> representing the employee to update.</param>
        private void SetEmployeePassword(EmployeeDB employee)
        {
            if (!employee.Retrieve())
            {
                m_passwordStatus.Text = "Failed to update password.";
                return;
            }

            string newPassword = m_newPassword.Text.TrimWhitespaceAndBOM();

            employee.Password = newPassword;

            // Reset password expiration date after user changed password.
            employee.ResetPasswordExpirationD();
            employee.Save(PrincipalFactory.CurrentPrincipal);

            m_passwordStatus.ForeColor = Color.Green;
            m_passwordStatus.Text = "Password Updated.";
        }
    }
}
