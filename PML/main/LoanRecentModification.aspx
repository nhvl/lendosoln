﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanRecentModification.aspx.cs" Inherits="PriceMyLoan.main.LoanRecentModification" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Loan Recent Modifications</title>
    <style>
      .StrongWarning { color:Red; font-weight:bold;margin:5px}
    </style>    
</head>
<body>
  <script type="text/javascript">

    function _init()
    {
      resize(350, 300);
    }
  </script>
    <h4 class="page-header">WARNING</h4>
    <form id="form1" runat="server">
    <div style="padding-left:10px;">
    <div class="StrongWarning">Any changes that you made to this loan may be lost.</div>
    <div style="font-weight:bold">The following user(s) have updated this loan in the past <%= AspxTools.HtmlString(ConstApp.LoanRecentModificationWarningPeriodInMinutes.ToString()) %> minutes and may still be editing it:</div>
    <br />
    <asp:Repeater ID="m_repeater" runat="server">
      <ItemTemplate>
        <div style="font-weight:bold"><%# AspxTools.HtmlString(Container.DataItem.ToString()) %></div>
      </ItemTemplate>
    </asp:Repeater>
    <br />
    <div style="margin-left:auto;margin-right:auto;width:70px">
    </div>
    </div>

    </form>
        <uc1:cModalDlg id=CModalDlg1 runat="server" />
</body>
</html>
