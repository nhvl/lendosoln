﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHAErrorResults.aspx.cs" Inherits="PriceMyLoan.main.FHAErrorResults" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>FHA TOTAL Errors</title>
	    <link href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css" >
		    fieldset.headerLine
		    {
		    	border: none;
                border-top : solid 1px #ff9933;
            }
            legend.MainHeader
            {
            	font-family:verdana,georgia, sans-serif;
        	    font-weight: bold;
        	    color: #ff9933;
        	    font-size:16px; 
        	    margin-bottom:10px; 
                padding:3px;
            }
            table.TableStyle td, table.TableStyle th{ border: none; text-align:left; vertical-align:top; }
            table.TableStyle td { padding-left: 5px; }
            table.TableStyle { border:solid 1px #BCBCBC; }
            table.TableStyle th { text-align:left; }
            table.TableStyle th:first-child { width:80px; }
            table.TableStyle td:first-child { text-align:center; }
            
            fieldset.headerLine p { padding-left: 20px; font-family : Verdana, Arial, Helvetica, sans-serif; font-size : 11px; line-height: normal; color: #666666; }
		</style>
    </head>
    <body scroll="no">
    <script type="text/javascript">
      function f_close()
      {
        parent.LQBPopup.Return({ OK: false });
      }
    </script>
    <h4 class="page-header">FHA TOTAL Errors</h4>
    <form id="form1" runat="server">
    <fieldset class="headerLine">
        <p>The following errors were returned by the FHA TOTAL Scorecard system.  Please correct them 
        and try submitting to FHA TOTAL Scorecard again.</p>
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
            <tr>
                <td style="padding-left:20px;padding-right:20px">
                    <div style="height:370px; text-align:center; overflow:auto;">
                        <ml:PassthroughLiteral id="htmlList" runat="server" EnableViewState="False"></ml:PassthroughLiteral>
                    </div>
                </td>
            </tr>
            <tr >
                <td colspan="3" align="center" style="padding-top:20px">
                    <asp:Button ID="m_okButton" CssClass="ButtonStyle" Width="90px" Text="Back" runat="server" onclick="m_okButton_Click" />&nbsp;&nbsp;
                    <input type="button" class="ButtonStyle" style="width:90px;" onclick="f_close();" value="Cancel" id="BtnCancel" runat="server"/>
                </td>
            </tr>
        </table>
        </fieldset>
    </form>
</body>
</html>
