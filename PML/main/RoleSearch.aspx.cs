﻿using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LqbGrammar.Drivers.SecurityEventLogging;
using PriceMyLoan.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PriceMyLoan.main
{

    public partial class RoleSearch : PriceMyLoan.UI.BasePage
    {

        private PmlEmployeeRoleControlValues m_controlValues;
        private string m_sState; 
        private Guid m_sPmlBrokerIdFilter = Guid.Empty;

		private string ShowMode
		{
			get
			{
                return m_controlValues.ShowMode; 
			}
		}

        private Guid EmployeeId
        {
            get
            {
                return m_controlValues.EmployeeId;
            }
        }
        
        private Guid RoleID
        {
            get
            {
                return m_controlValues.RoleId;
            }
        }

        protected string RoleDesc
        {
            get
            {
                return m_controlValues.RoleDesc;
            }
        }

		protected string SearchFilter
		{
			get
			{
                return m_controlValues.SearchFilter;
			}
		}

        protected Guid LoanId
        {
            get {
                return m_controlValues.LoanId;
            }
        }

        protected string Role
        {
            get
            {
                return m_controlValues.Role;  
            }
        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

        private void LoadControlValues()
        {
            string key = RequestHelper.GetSafeQueryString("k");
            if (string.IsNullOrEmpty(key))
            {
                throw new CBaseException(ErrorMessages.Generic, "key is missing");
            }

            m_controlValues = PmlEmployeeRoleControlValues.RetrieveFromCache(key);

            if (m_controlValues == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Could not load pml control values");
            }

            if (m_controlValues.OwnerId != PriceMyLoanUser.UserId)
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "User id does not match the stored id.");
            }
        }

        private void RestoreControlValues()
        {
            m_SearchFilter.Text = SearchFilter;
            switch (m_controlValues.EmployeeStatus)
            {
                case E_EmployeeStatusFilterT.ActiveOnly:
                    m_activeRB.Checked = true;
                    m_inactiveRB.Checked = false;
                    m_anyRB.Checked = false;
                    break;
                case E_EmployeeStatusFilterT.InactiveOnly:
                    m_activeRB.Checked = false;
                    m_inactiveRB.Checked = true;
                    m_anyRB.Checked = false;
                    break;
                case E_EmployeeStatusFilterT.All:
                    m_activeRB.Checked = false;
                    m_inactiveRB.Checked = false;
                    m_anyRB.Checked = true;
                    break;
            }
        }

        protected void Page_Load(object sender, System.EventArgs a)
        {
            LoadControlValues();

            if (IsPostBack)
            {
                return;
            }

            if (RoleDesc.Contains("Secondary"))
            {
                PluralDescription.Text = RoleDesc.Replace("Secondary", "Secondaries");
            }
            else
            {
                PluralDescription.Text = RoleDesc + "s";
            }

            //update ui from query string
            RestoreControlValues();
            m_noLOAssignedWarning.Visible = RequestHelper.GetBool("showlowarning");

            string loDesc = "";

            if (this.RoleID == CEmployeeFields.s_LoanRepRoleId)
            {
                loDesc = "Loan Officer";
            }
            else
            {
                if (PriceMyLoanConstants.IsEmbeddedPML)
                {
                    loDesc += "(External) ";
                }

                loDesc += "Secondary";
            }

            this.LoDesc.Text = loDesc;

            bool isPmlRole = RoleID == CEmployeeFields.s_LoanRepRoleId ||
                RoleID == CEmployeeFields.s_BrokerProcessorId ||
                RoleID == CEmployeeFields.s_ExternalSecondaryId ||
                RoleID == CEmployeeFields.s_ExternalPostCloserId;

            //if theres an employee id load the pml broker id from it
            if (EmployeeId != Guid.Empty && isPmlRole)
            {
                EmployeeDB db = new EmployeeDB(EmployeeId, PriceMyLoanUser.BrokerId);
                db.Retrieve();
                m_sPmlBrokerIdFilter = db.PmlBrokerId;
            }

            E_BranchChannelT? branchChannelFilter = null;
            E_sCorrespondentProcessT? correspondentProcessFilter = null;

            if (LoanId != Guid.Empty && isPmlRole)
            {
                LoanAccessInfo info = new LoanAccessInfo(LoanId);
                m_sPmlBrokerIdFilter = info.sPmlBrokerId;

                // load the state from dataloan
                CPageData data = CPageData.CreatePmlPageDataUsingSmartDependency(LoanId, typeof(RoleSearch));
                data.InitLoad();
        
                m_sState = data.sSpStatePe; 

                branchChannelFilter = data.sBranchChannelT;
                correspondentProcessFilter = data.sCorrespondentProcessT;
            }

            //check to see if the ui has to tell the user to search for words before results.
            BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();

            if (PriceMyLoanUser.HasPermission(LendersOffice.Security.Permission.AllowLoanAssignmentsToAnyBranch))
            {
                roleTable.Retrieve(
                    PriceMyLoanUser.BrokerId,
                    Guid.Empty,
                    SearchFilter,
                    Role,
                    m_controlValues.EmployeeStatus,
                    null,
                    null);
            }
            else
            {
                // PML users have a broker branch, mini-correspondent branch,
                // and correspondent branch. We need to filter based on the 
                // correct branch, which requires looking at the channel and
                // correspondent process type of the file.
                roleTable.Retrieve(
                    PriceMyLoanUser.BrokerId,
                    PriceMyLoanUser.BranchId,
                    SearchFilter,
                    Role,
                    m_controlValues.EmployeeStatus,
                    branchChannelFilter,
                    correspondentProcessFilter);
            }

            if (roleTable[Role] != null)
            {

                List<BrokerLoanAssignmentTable.Spec> ls = new List<BrokerLoanAssignmentTable.Spec>();

                foreach (BrokerLoanAssignmentTable.Spec spec in roleTable[Role])
                {
                    if (PriceMyLoanUser.ApplicationType == E_ApplicationT.LendersOffice)
                    {
                        ls.Add(spec); // The current user is an LO user, so let them pick from any PML broker
                    }
                    else if (m_sPmlBrokerIdFilter != Guid.Empty && spec.PmlBrokerId == m_sPmlBrokerIdFilter)
                    {
                        ls.Add(spec); // The current user is a PML user, so only let them pick from users of the same PML broker
                    }
                }                                                                   

                var orderedList = m_controlValues.IsDesc ?  ls.OrderByDescending(p=>p.FullName) : ls.OrderBy(p => p.FullName);
                m_SearchResults.DataSource = ViewGenerator.Generate(orderedList.ToList());
                m_SearchResults.DataBind();
            }

            if (m_SearchResults.Items.Count == 0)
            {
                m_TooMany.Visible = false;
                m_Empty.Visible = true;
            }
            else
            {
                m_TooMany.Visible = false;
                m_Empty.Visible = false;
            }
        }


        protected void SearchResults_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == System.Web.UI.WebControls.ListItemType.Header || e.Item.ItemType == System.Web.UI.WebControls.ListItemType.Footer)
                return;

            HtmlAnchor a = (HtmlAnchor)e.Item.FindControl("UserClickLink");

            DataRowView dRow = e.Item.DataItem as DataRowView;

            if (dRow == null)
            {
                return;
            }

            string[] columns = 
            {
                "employeeId",
                "fullName",
                "email",
                "addr",
                "city",
                "state",
                "zip",
                "phone",
                "fax",
                "companyName",
                "licenses",
                "pager",
                "cellphone",
            };

            //Start the js object declaration
            StringBuilder agentInfo = new StringBuilder("{ ");

            foreach (var col in columns)
            {
                var lineFmt = string.Format("{0}: {{0}}, ", col);
                string result;
                switch(col)
                {
                    case("employeeId"):
                        result = ((Guid)dRow[col]).ToString();
                        break;
                    case("licenses"):
                        LicenseInfoList list = dRow["Licenses"] as LicenseInfoList;
                        result = "";
                        if (false == String.IsNullOrEmpty(m_sState))
                        {
                            result = CAgentFields.GetLendingLicenseByState(m_sState, list);
                        }
                        break;
                    default:
                        result = (string)dRow[col];
                        break;
                }

                agentInfo.AppendFormat(lineFmt, AspxTools.JsString(result));
            }

            //IE 7 has problems with trailing commas in object declarations, so remove that (there's a space too)
            agentInfo.Length -= 2;

            //And then end the object declaration
            agentInfo.Append("}");

            string onclick = String.Format
                ("parent.LQBPopup.Return({0}); return false;", agentInfo.ToString());

            a.Attributes.Add("onclick", onclick);
            a.InnerText = (string)dRow["FullNameWithIsActiveStatus"];
        }

        protected void m_Grid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == System.Web.UI.WebControls.ListItemType.Header || e.Item.ItemType == System.Web.UI.WebControls.ListItemType.Footer)
                return;

            DataRowView dRow = e.Item.DataItem as DataRowView;

            if (dRow == null)
            {
                return;
            }

            
            LicenseInfoList list = dRow["Licenses"] as LicenseInfoList;
            string license = "";
            
            if (false == String.IsNullOrEmpty(m_sState))
            {
                license = CAgentFields.GetLendingLicenseByState(m_sState, list);
            }

            string onclick = String.Format
                ("parent.LQBPopup.Return({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}); return false;"
                , AspxTools.JsString((Guid)dRow["EmployeeId"])
                , AspxTools.JsString((string)dRow["FullName"])
                , AspxTools.JsString((string)dRow["Email"])
                , AspxTools.JsString((string)dRow["Addr"])
                , AspxTools.JsString((string)dRow["City"])
                , AspxTools.JsString((string)dRow["State"])
                , AspxTools.JsString((string)dRow["Zip"])
                , AspxTools.JsString((string)dRow["Phone"])
                , AspxTools.JsString((string)dRow["Fax"])
                , AspxTools.JsString((string)dRow["CompanyName"])
                , AspxTools.JsString(license)
                );


            HtmlAnchor linkButton = new HtmlAnchor();
            linkButton.Attributes.Add("onclick", onclick);
            linkButton.InnerText = DataBinder.Eval(e.Item.DataItem, "FullNameWithIsActiveStatus").ToString();

            e.Item.Cells[0].Controls.Add(linkButton);
        }

        protected void Search_OnClick(object sender, EventArgs a)
        {
            Response.Redirect(Request.Url.AbsolutePath + "?k=" + PersistQueryString(m_controlValues.IsDesc) + "&showlowarning=" + RequestHelper.GetBool("showlowarning"));
        }

        protected void OnSort(object sender, DataGridSortCommandEventArgs args)
        {
            Response.Redirect(Request.Url.AbsolutePath + "?k=" + PersistQueryString(!m_controlValues.IsDesc) + "&showlowarning=" + RequestHelper.GetBool("showlowarning")); 
        }

        private string PersistQueryString(bool isDescSort)
        {
            m_controlValues.SearchFilter = m_SearchFilter.Text;
            if (m_activeRB.Checked)
            {
                m_controlValues.EmployeeStatus = E_EmployeeStatusFilterT.ActiveOnly;
            }
            else if (m_anyRB.Checked)
            {
                m_controlValues.EmployeeStatus = E_EmployeeStatusFilterT.All;
            }
            else if (m_inactiveRB.Checked)
            {
                m_controlValues.EmployeeStatus = E_EmployeeStatusFilterT.InactiveOnly;
            }
            return PmlEmployeeRoleControlValues.Save(m_controlValues, PriceMyLoanConstants.CookieExpireMinutes);
        }

    }
}
