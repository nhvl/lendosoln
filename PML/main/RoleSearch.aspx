﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoleSearch.aspx.cs" Inherits="PriceMyLoan.main.RoleSearch" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html  >
<head runat="server">
       <style type="text/css">
        a { cursor: pointer; }
        
        th.headerSortUp {
            background-image: url(../images/Tri_DESC.gif);
            background-repeat: no-repeat;
            background-position: 80px 50%;
        }
        
        th.headerSortDown {
            background-image: url(../images/Tri_ASC.gif);
            background-repeat: no-repeat;
            background-position: 80px 50%;
        }
        
        #Cancel { font-weight:bold; }
        
        #m_noLOAssignedWarning {
            padding: 5px;
            font-weight:bold;
            color: Red;
        }
        .w-450 { 
            width: 450px; 
        }
        .caption {
            font: 11px arial; 
            color: dimgray; 
            padding-left: 4px;
        }
        .padding-top-4 {
            padding-top: 4px;
        }
    </style>
    
</head>
   <body  scroll="no" bgcolor="gainsboro" style="margin:0px">
        <script type="text/javascript" >
            $j(function() {
                $j('input:text').keydown(function(e){
                    var key = e.which;
                    if (key === 13 ) {
                        return false;
                    }
                });
        });
    
    
		function _init() {
		    <%= AspxTools.JsGetElementById(m_SearchFilter) %>.focus();
		}
        </script>

    <form id="form1" runat="server" >
    <table cellspacing="2" cellpadding="0" class="w-450">
        <tr>
            <td height="0" class="FormTableHeader" style="padding-right: 2px; padding-left: 2px;
                padding-bottom: 2px; padding-top: 2px">
                <ml:EncodedLiteral ID="PluralDescription" runat="server"></ml:EncodedLiteral>:
            </td>
        </tr>
        <tr>
            <td style="padding: 0px;">
                <div style="border-right: 2px groove; padding-right: 8px; border-top: 2px groove;
                    padding-left: 8px; padding-bottom: 8px; border-left: 2px groove; 
                    padding-top: 8px; border-bottom: 2px groove">
                    <table cellpadding="0" cellspacing="0" width="100%" >
                        <tr>
                            <td nowrap style="padding-right: 4px; color: black;">
                                Search for:
                            </td>
                            <td nowrap>
                                <asp:TextBox ID="m_SearchFilter" runat="server" Style="padding-left: 4px" Width="169px" />
                            </td>
                            <td class="caption">
                                ("s" for John Smith or Sam Cash, "b s" for Bob Smith)
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 4px; color: black;" nowrap>
                                 <%= AspxTools.HtmlString(RoleDesc) %> status:
                            </td>
                            <td nowrap colspan="2" style="color: black;">
                                <asp:RadioButton ID="m_activeRB" runat="server" Text="Active" GroupName="status"
                                    Checked="True" />
                                <asp:RadioButton ID="m_inactiveRB" runat="server" Text="Inactive" GroupName="status" />
                                <asp:RadioButton ID="m_anyRB" runat="server" Text="Any Status" GroupName="status" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 4px" nowrap>
                            </td>
                            <td nowrap colspan="2" class="padding-top-4">
                                <asp:Button ID="m_Search" runat="server" Text="Search"
                                 OnClick="Search_OnClick" NoHighlight="NoHighlight"  CssClass="ButtonStyle SearchBtn"  UseSubmitBehavior="false">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr valign="top">
            <td >
                <div style="padding-right: 4px; overflow-y: scroll; height: 220px;"  >
                    <asp:Repeater runat="server" ID="m_SearchResults" OnItemDataBound="SearchResults_OnItemDataBound">
                        <HeaderTemplate>
                           <table class="DataGrid" id="searchEntries" style=" border-collapse: collapse;" border="1" rules="all" cellSpacing="0" cellPadding="2" width="100%">
                                <thead>
                                    <tr>
                                        <th class="GridHeader" style="text-align: left"><%= AspxTools.HtmlString(RoleDesc) %></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <FooterTemplate>
                                </tbody>
                               </table>
                        </FooterTemplate>
                        <ItemTemplate >
                            <tr class="GridItem">
                                <td >
                                    <a id="UserClickLink" runat="server"> </a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        
                                       <AlternatingItemTemplate >
                            <tr class="GridAlternatingItem">
                                <td >
                                    <a id="UserClickLink" runat="server"> </a>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
      
                    <asp:Panel ID="m_TooMany" runat="server" Style="padding-right: 50px; padding-left: 50px;
                        padding-bottom: 50px; font: 11px arial; color: tomato; padding-top: 50px; text-align: justify"
                        Visible="False">
                        Too many employees with this role. Please refine your search using the above filters.
                        <div style="margin-top: 12px; width: 100%">
                            We match against employee's full name, e.g. "Jo", will find all the employees with
                            a first or last name that begins with "Jo". To search for employees by first *and*
                            last name, use two patterns separated by a space. For example: use "bo sm" to return
                            "Bob Smith", "Boaz Smalls", etc.
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="m_Empty" runat="server" Style="padding-right: 80px; padding-left: 80px;
                        padding-bottom: 80px; font: 11px arial; color: dimgray; padding-top: 80px; text-align: justify"
                        Visible="False">
                        No PML users who match the specified criteria were found.
                    </asp:Panel>
                </div>
            </td>
        </tr>
    </table>
    <asp:Panel ID="m_noLOAssignedWarning" runat="server">
        Warning: <ml:EncodedLiteral runat="server" ID="LoDesc">Loan Officer</ml:EncodedLiteral> assignment is required in order to run PML.
    </asp:Panel>
    </form>
</body>
</html>
