<%@ Page language="c#" Codebehind="CertificateWaitPage.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.CertificateWaitPage" %>
<%@ Import namespace="DataAccess" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
  <head runat="server">
    <title>CertificateWaitPage</title>
  </head>
  <body MS_POSITIONING="FlowLayout">
	<script type="text/javascript">
<!--
var g_sRequestID;
var g_iPollingInterval = 0;
var g_iResultTimeout = <%= AspxTools.JsNumeric(ConstAppDavid.LPE_NumberOfMinutesBeforeMessageExpire) %> * 60000;
var g_sUpdateMessage = <%= AspxTools.JsString(JsMessages.LpeResultsTimeout) %>;
var g_iStartTime = 0;
var g_iStopTime = 0;

var g_sLId = <%= AspxTools.JsString(LoanID) %>;
var g_productId = <%= AspxTools.JsString(ProductID) %>;
var g_sLienQualifyModeT = <%= AspxTools.JsString(sLienQualifyModeT.ToString("D")) %>;
function _init() {
  window.setTimeout(f_SubmitUnderwriting, 300);
}
function f_SubmitUnderwriting() {
  
  g_iStartTime = (new Date()).getTime();
  var args = new Object();
  args["loanid"] = g_sLId;
  args["sLienQualifyModeT"] = g_sLienQualifyModeT;
  args["productid"] = g_productId;
  args["version"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("version")) %>;
  args["uniquechecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("uniquechecksum")) %>;
  args["FirstLienLpId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("FirstLienLpId")) %>;
  args["FirstLienNoteRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("FirstLienNoteRate")) %>;
  args["FirstLienPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("FirstLienPoint")) %>;
  args["FirstLienMPmt"]  = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("FirstLienMPmt")) %>;
  args["PinStateId"] = <%= AspxTools.JsString(RequestHelper.GetGuid("PinStateId", Guid.Empty)) %>;
  var productRate  = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productRate")) %>;
  var productDTI = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productDTI")) %>;
  args["productRate"] = productRate;
  args["productPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productPoint")) %>;
  args["productRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productRawId")) %>;

    //kinda hacky, but placing the product dti and rate arguments here allows the values to persist through all the rate calls, and saves
    //modifying a lot of pages.
  var result = gService.main.call("SubmitUnderwriting" + "&productRate=" + productRate + "&productDTI=" + productDTI, args, true, true);
  if (!result.error) {    
    if (result.value["InvalidRateOption"] == "True") {
        window.parent.frames[0].f_onInvalidRateOption();
        self.location = "Certificate.aspx?loanid=" + g_sLId + "&hasInvalidRateOptionError=t&timing=0";
        return;
    }
    setTimeout(f_resultTimeout, g_iResultTimeout);
    setTimeout(f_minuteWarning, 60000); <%-- // 1 minute warning    --%>
    g_sRequestID = result.value["RequestID"];
    g_iPollingInterval = parseInt(result.value["Polling"]);
    
    setTimeout(f_IsAvailable, g_iPollingInterval);
  }
}
    
function f_IsAvailable() {
  var args = new Object();
  args["requestid"] = g_sRequestID;
      
  var result = gService.main.call("IsAvailable", args, true, true);
  if (!result.error) {
    if (result.value["IsAvailable"] == "0") {
      setTimeout("f_IsAvailable();", g_iPollingInterval);
      return;
    }
  } else {
    alert("System error result is unavailable please try again.");
    return;
  }
  g_iStopTime = (new Date()).getTime();
  
  var duration = (g_iStopTime - g_iStartTime) / 1000;

  var extraArg = "";
  <% if (RequestHelper.GetBool("detailCert")) {%>
    extraArg = "&detailCert=1";
  <%}%>

  self.location = "Certificate.aspx?loanid=" + g_sLId + "&lienqualifymodet=" + g_sLienQualifyModeT + "&requestid=" + g_sRequestID + "&timing=" + duration + extraArg;
      
}
function f_minuteWarning() {
  var args = new Object();
  args["loanid"] = g_sLId;
  args["productid"] = g_productId;
  result = gService.main.call("MinuteWarning", args, true, true);
  
}
function f_reportTimeout() {
  var args = new Object();
  args["LoanID"] = g_sLId;
  args["productid"] = g_productId;      
  args["RequestID"] = g_sRequestID;
  
  gService.main.call("ReportTimeout", args);
}
function f_resultTimeout() {
  f_reportTimeout();
  alert(g_sUpdateMessage);
  self.close();
}
//-->
    </script>

    <form id="CertificateWaitPage" method="post" runat="server">
    <table width="100%" height="100%" border="0" id="WaitTable">
	<tr>
		<td align=center valign=middle class="WaitMessageLabel">Please Wait ...</td>
	</tr>
</table>
     </form>
	
  </body>
</html>
