<%@ Page language="c#" Codebehind="PublicRecordList.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.PublicRecordList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="ml" TagName="ModelessDlg" Src="../Common/ModalDlg/ModelessDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>Public Records</title>
  </head>
  <body>
  	<script language=javascript>
<!--
function _init() {
  <% if ( ! Page.IsPostBack ) { %>
    resize(925, 570);
  <% } %>
  f_onresize();
}
function f_onresize() {
      var h = document.body.clientHeight;
      if ( h >= 95)
        document.getElementById('MainFrame').style.height=(h - 95) + "px";
}
function f_onEditClick( id )
{
  var url = <%= AspxTools.JsString("EditPublicRecord.aspx?loanid=" + LoanID + "&appid=" + m_currentAppId + "&recordid=") %> + id;
  LQBPopup.Show(url, {
                        width: '735', 
                        height: '530', 
                        hideCloseButton: true, 
                        onReturn: function()
                        {
                            window.location.reload();
                        } 
                      });
}
//-->
</script>
    <h4 class="page-header">Public Record List</h4>
    <form id="PublicRecordList" method="post" runat="server">
    <div id=MainFrame style="BORDER:3px solid;MARGIN:5px;OVERFLOW:auto;HEIGHT:100%;WIDTH:99%">
      <ml:CommonDataGrid id="m_PublicRecordDG" runat="server" AutoGenerateColumns="false">
        <Columns>
          <asp:TemplateColumn>
            <ItemTemplate>
              <a href="#" onclick="f_onEditClick(<%# AspxTools.JsString(Eval("RecordId").ToString()) %>);" >edit</a>
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:BoundColumn HeaderText="Court Name" DataField="CourtName" SortExpression="CourtName" />
          <asp:BoundColumn HeaderText="Type" DataField="Type" SortExpression="Type" />
          <asp:BoundColumn HeaderText="Amount" DataField="BankruptcyLiabilitiesAmount" SortExpression="BankruptcyLiabilitiesAmount" />
          <asp:BoundColumn HeaderText="Disposition" DataField="DispositionT" SortExpression="DispositionT" />
          <asp:BoundColumn HeaderText="Disposition Date" DataField="DispositionD" SortExpression="DispositionD" />
          <asp:BoundColumn HeaderText="Filed Date" DataField="BkFileD" SortExpression="BkFileD" />
          <asp:BoundColumn HeaderText="Reported Date" DataField="ReportedD" SortExpression="ReportedD" />
          <asp:BoundColumn HeaderText="Last Effective Date" DataField="LastEffectiveD" SortExpression="LastEffectiveD" />
          <asp:TemplateColumn HeaderText="Include In Pricing" SortExpression="IncludeInPricing">
            <ItemTemplate>
              <%# AspxTools.HtmlString(Eval("IncludeInPricing").ToString().ToLower() == "true" ? "Yes" : "No") %>
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:BoundColumn HeaderText="Modified?" DataField="Modified" SortExpression="Modified" />
        </Columns>
      </ml:CommonDataGrid>
    </div>
    </form>
	<ml:ModelessDlg id="ModelessDlg" runat="server"></ml:ModelessDlg>
  </body>
</html>
