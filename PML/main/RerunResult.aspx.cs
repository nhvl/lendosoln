/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using PriceMyLoan.Security;
using PriceMyLoan.Common;
using DataAccess;
using LendersOffice.Constants;
namespace PriceMyLoan.main
{
	public partial class RerunResult : PriceMyLoan.UI.BasePage
	{
    
        protected void PageInit(object sender, System.EventArgs e) 
        {
            ClientScript.GetPostBackEventReference(this, "");
            this.RegisterService("main", "/main/MainService.aspx");
        }
        protected bool m_allowGoToPipeline 
        {
            get { return PriceMyLoanUser.IsAllowGoToPipeline; }
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            if (!Page.IsPostBack) 
            {
                CPageData dataLoan = new CPmlLoanSummary_TransformData(LoanID);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                dataLoan.TransformDataToPml(E_TransformToPmlT.RerunForRateLockRequest);
                dataLoan.Save();

                sLNm.Text = dataLoan.sLNm;
                aBNm.Text = dataLoan.GetAppData(0).aBNm;
                sLAmtCalc.Text = dataLoan.sLAmtCalc_rep.Replace(".00", "");
                // 10/8/2004 dd - Temporary Hack to remove .000;
                sLtvR.Text = dataLoan.sLtvR_rep.Replace(".000", "");
                sCltvR.Text = dataLoan.sCltvR_rep.Replace(".000", "");
                sCreditScoreType1.Text = dataLoan.sCreditScoreType1_rep;
                sCreditScoreType2.Text = dataLoan.sCreditScoreType2_rep;

            }
            
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
