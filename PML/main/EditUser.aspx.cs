using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;

using PriceMyLoan.Common;
using LendersOffice.ObjLib.ServiceCredential;

namespace PriceMyLoan.main
{
    // 3/30/2006 mf - This is a port of LO's PML user editor to PML for OPM 3340.
    // PML supervisors use this limited editor to edit and create supervised users.
    // Comments made before this date were in reference to the LO PML user editor.
    //
    // 4/27/2006 mf - For the privacy of the lenders, we display the relationships
    // as read-only, so brokers cannot see the list of internal lender staff.
    //
    // 05/30/06 mf - OPM 4811. We display the currently assigned processor

    public partial class EditUser : PriceMyLoan.UI.BasePage
    {
  //OPM 1863
  //OPM 1863
  //OPM 1863
        protected Boolean expirationPeriodDisabled;
        protected Boolean cycleExpirationDisabled;
        protected Boolean expirationDateDisabled;
        private bool hasError;


        private String ErrorMessage
        {
            set { if (!string.IsNullOrEmpty(value)) {hasError = true;} ClientScript.RegisterHiddenField( "m_errorMessage" , value ); }
        }

        private bool IsUpdated
        {
            set { ClientScript.RegisterHiddenField( "m_isUpdated" , value.ToString() ); }
        }

        protected bool NewEmployee
        {
            get { return ( ViewState["employeeId"] == null ); }
        }

        protected Guid EmployeeId
        {
            get { return RequestHelper.GetGuid("employeeId"); }
        }

        private EmployeeDB employeeDB;

        /// <summary>
        /// Initialize this page.
        /// </summary>

        protected void PageInit( object sender , System.EventArgs a )
        {
            // 09/22/09 vm. OPM 40427 - User must have pml supervisor status to view.
            if (PriceMyLoanUser.IsPmlManager == false)
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Non-supervisor attempts to access edit user page.");

            SetRequiredValidatorMessage(m_R5);

            this.RegisterService("main", "/main/UserEditService.aspx");
            this.RegisterCSS("/webapp/jquery-ui.css");
            this.RegisterJsScript("jquery-ui.js");
            RegisterJsScript("/webapp/combobox.js");
        }

        private void DeleteRegisteredIp(int id)
        {
            UserRegisteredIp.Delete(PriceMyLoanUser.BrokerId, employeeDB.UserID, id);
        }

        private void RevokeClientCertificate(Guid certificateId)
        {
            ClientCertificate.Delete(employeeDB.BrokerID, employeeDB.UserID, certificateId);
        }

        private void LoadBrokerRelationships(EmployeeDB empDB)
        {
            m_LenderAcctExec             .Text  = empDB.LenderAcctExecEmployeeID    != Guid.Empty ? empDB.LenderAccExecFullName                : "<-- None -->";
            m_Underwriter                .Text  = empDB.UnderwriterEmployeeID       != Guid.Empty ? empDB.UnderwriterFullName                  : "<-- None -->";
            m_JuniorUnderwriter          .Text  = empDB.JuniorUnderwriterEmployeeID != Guid.Empty ? empDB.JuniorUnderwriterFullName            : "<-- None -->";
            m_LockDesk                   .Text  = empDB.LockDeskEmployeeID          != Guid.Empty ? empDB.LockDeskFullName                     : "<-- None -->";
            m_Manager                    .Text  = empDB.ManagerEmployeeID           != Guid.Empty ? empDB.ManagerFullName                      : "<-- None -->";
            m_Processor                  .Text  = empDB.ProcessorEmployeeID         != Guid.Empty ? empDB.ProcessorFullName                    : "<-- None -->";
            m_JuniorProcessor            .Text  = empDB.JuniorProcessorEmployeeID   != Guid.Empty ? empDB.JuniorProcessorFullName              : "<-- None -->";
            m_AssignedBrokerProcessorName.Value = empDB.BrokerProcessorEmployeeId   != Guid.Empty ? empDB.BrokerProcessorFullName              : "<-- None -->";
            m_AssignedBrokerProcessorId  .Value = empDB.BrokerProcessorEmployeeId   != Guid.Empty ? empDB.BrokerProcessorEmployeeId.ToString() : Guid.Empty.ToString();
        }

        private void LoadMiniCorrespondentRelationships(EmployeeDB empDB)
        {
            m_AssignedMiniCorrExternalPostCloserId.Value = empDB.MiniCorrExternalPostCloserEmployeeId != Guid.Empty ? empDB.MiniCorrExternalPostCloserEmployeeId.ToString() : Guid.Empty.ToString();
            m_AssignedMiniCorrExternalPostCloserName.Value = empDB.MiniCorrExternalPostCloserEmployeeId != Guid.Empty ? empDB.MiniCorrExternalPostCloserFullName : "<-- None -->";

            m_MiniCorrManager          .Text = empDB.MiniCorrManagerEmployeeId           != Guid.Empty ? empDB.MiniCorrManagerFullName           : "<-- None -->";
            m_MiniCorrProcessor        .Text = empDB.MiniCorrProcessorEmployeeId         != Guid.Empty ? empDB.MiniCorrProcessorFullName         : "<-- None -->";
            m_MiniCorrJuniorProcessor  .Text = empDB.MiniCorrJuniorProcessorEmployeeId   != Guid.Empty ? empDB.MiniCorrJuniorProcessorFullName   : "<-- None -->";
            m_MiniCorrLenderAcctExec   .Text = empDB.MiniCorrLenderAccExecEmployeeId     != Guid.Empty ? empDB.MiniCorrLenderAccExecFullName     : "<-- None -->";
            m_MiniCorrUnderwriter      .Text = empDB.MiniCorrUnderwriterEmployeeId       != Guid.Empty ? empDB.MiniCorrUnderwriterFullName       : "<-- None -->";
            m_MiniCorrJuniorUnderwriter.Text = empDB.MiniCorrJuniorUnderwriterEmployeeId != Guid.Empty ? empDB.MiniCorrJuniorUnderwriterFullName : "<-- None -->";
            m_MiniCorrCreditAuditor    .Text = empDB.MiniCorrCreditAuditorEmployeeId     != Guid.Empty ? empDB.MiniCorrCreditAuditorFullName     : "<-- None -->";
            m_MiniCorrLegalAuditor     .Text = empDB.MiniCorrLegalAuditorEmployeeId      != Guid.Empty ? empDB.MiniCorrLegalAuditorFullName      : "<-- None -->";
            m_MiniCorrLockDesk         .Text = empDB.MiniCorrLockDeskEmployeeId          != Guid.Empty ? empDB.MiniCorrLockDeskFullName          : "<-- None -->";
            m_MiniCorrPurchaser        .Text = empDB.MiniCorrPurchaserEmployeeId         != Guid.Empty ? empDB.MiniCorrPurchaserFullName         : "<-- None -->";
            m_MiniCorrSecondary        .Text = empDB.MiniCorrSecondaryEmployeeId         != Guid.Empty ? empDB.MiniCorrSecondaryFullName         : "<-- None -->";
        }

        private void LoadCorrespondentRelationships(EmployeeDB empDB)
        {
            m_AssignedCorrExternalPostCloserId.Value = empDB.CorrExternalPostCloserEmployeeId != Guid.Empty ? empDB.CorrExternalPostCloserEmployeeId.ToString() : Guid.Empty.ToString();
            m_AssignedCorrExternalPostCloserName.Value = empDB.CorrExternalPostCloserEmployeeId != Guid.Empty ? empDB.CorrExternalPostCloserFullName : "<-- None -->";

            m_CorrManager          .Text = empDB.CorrManagerEmployeeId           != Guid.Empty ? empDB.CorrManagerFullName           : "<-- None -->";
            m_CorrProcessor        .Text = empDB.CorrProcessorEmployeeId         != Guid.Empty ? empDB.CorrProcessorFullName         : "<-- None -->";
            m_CorrJuniorProcessor  .Text = empDB.CorrJuniorProcessorEmployeeId   != Guid.Empty ? empDB.CorrJuniorProcessorFullName   : "<-- None -->";
            m_CorrLenderAcctExec   .Text = empDB.CorrLenderAccExecEmployeeId     != Guid.Empty ? empDB.CorrLenderAccExecFullName     : "<-- None -->";
            m_CorrUnderwriter      .Text = empDB.CorrUnderwriterEmployeeId       != Guid.Empty ? empDB.CorrUnderwriterFullName       : "<-- None -->";
            m_CorrJuniorUnderwriter.Text = empDB.CorrJuniorUnderwriterEmployeeId != Guid.Empty ? empDB.CorrJuniorUnderwriterFullName : "<-- None -->";
            m_CorrCreditAuditor    .Text = empDB.CorrCreditAuditorEmployeeId     != Guid.Empty ? empDB.CorrCreditAuditorFullName     : "<-- None -->";
            m_CorrLegalAuditor     .Text = empDB.CorrLegalAuditorEmployeeId      != Guid.Empty ? empDB.CorrLegalAuditorFullName      : "<-- None -->";
            m_CorrLockDesk         .Text = empDB.CorrLockDeskEmployeeId          != Guid.Empty ? empDB.CorrLockDeskFullName          : "<-- None -->";
            m_CorrPurchaser        .Text = empDB.CorrPurchaserEmployeeId         != Guid.Empty ? empDB.CorrPurchaserFullName         : "<-- None -->";
            m_CorrSecondary        .Text = empDB.CorrSecondaryEmployeeId         != Guid.Empty ? empDB.CorrSecondaryFullName         : "<-- None -->";
        }

        /// <summary>
        /// Initialize page.
        /// </summary>
        protected void PageLoad( object sender , System.EventArgs a )
        {
            try
            {
                // Load up pml employee details.  Afterwards, we load up
                // the current permission set.
                m_Apply.DisableOneButton = false;
                Guid empId = Guid.Empty;


                PmlBroker pB = PmlBroker.RetrievePmlBrokerById(PriceMyLoanUser.PmlBrokerId, PriceMyLoanUser.BrokerId);
                if (!pB.Roles.Contains(E_OCRoles.Broker))
                {
                    LiTabD.Style.Add("display", "none");
                    AllowWholesaleCreateLoanRow.Style.Add("display", "none");
                    AllowWholesaleViewingLoanRow.Style.Add("display", "none");
                }
                if (!pB.Roles.Contains(E_OCRoles.MiniCorrespondent))
                {
                    LiTabG.Style.Add("display", "none");
                    AllowMiniCorrCreateLoanRow.Style.Add("display", "none");
                    AllowMiniCorrViewingLoanRow.Style.Add("display", "none");
                }
                if (!pB.Roles.Contains(E_OCRoles.Correspondent))
                {
                    LiTabH.Style.Add("display", "none");
                    AllowCorrCreateLoanRow.Style.Add("display", "none");
                    AllowCorrViewingLoanRow.Style.Add("display", "none");
                }

                if( Request[ "cmd" ] != null && Request[ "cmd" ].ToLower() == "edit" )
                {
                    // Edit an existing employee.  This is a Price My Loan
                    // employee that we pull from the db as a regular employee.

                    m_TitleBar.InnerText = "Edit User";
                    m_R5.Enabled = false;

                    try
                    {
                        empId = new Guid( Request[ "employeeId" ] );
                    }
                    catch( Exception )
                    {
                        // Oops!
                        throw new CBaseException(ErrorMessages.FailedToLoadUser, "Unable to calculate specified employee's id.");
                    }

                    employeeDB = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, EmployeeId);

                    if (Page.IsPostBack)
                    {
                        string target = this.Request.Form.Get("__EVENTTARGET");
                        if (target == "ClientCertificateRevoke")
                        {
                            Guid certificateId = new Guid(this.Request.Form["__EVENTARGUMENT"]);
                            RevokeClientCertificate(certificateId);
                        }
                        else if (target == "RegisteredIpDelete")
                        {
                            int id = int.Parse(this.Request.Form["__EVENTARGUMENT"]);
                            DeleteRegisteredIp(id);
                        }
                    }
                }

                bool enableAuthCodeSms = false;
                if ( IsPostBack == false )
                {
                    PmlEmployeeRoleControlValues loValues = new PmlEmployeeRoleControlValues()
                    {
                        LoanId = Guid.Empty,
                        OwnerId = PriceMyLoanUser.UserId,
                        RoleDesc = "Processor",
                        RoleId = CEmployeeFields.s_BrokerProcessorId,
                        ShowMode = "fullsearch",
                        Role = ConstApp.ROLE_BROKERPROCESSOR,
                        SearchFilter = "",
                        EmployeeId = PriceMyLoanUser.EmployeeId
                    };
                    m_BPPickerUrl.Value = VirtualRoot + "/main/RoleSearchV2.aspx?k=" + PmlEmployeeRoleControlValues.Save(loValues, PriceMyLoanConstants.CookieExpireMinutes);

                    var pcValues = new PmlEmployeeRoleControlValues()
                    {
                        LoanId = Guid.Empty,
                        OwnerId = PriceMyLoanUser.UserId,
                        RoleDesc = "Post-Closer",
                        RoleId = CEmployeeFields.s_ExternalPostCloserId,
                        ShowMode = "fullsearch",
                        Role = ConstApp.ROLE_EXTERNAL_POST_CLOSER,
                        SearchFilter = "",
                        EmployeeId = PriceMyLoanUser.EmployeeId
                    };
                    m_PCPickerUrl.Value = VirtualRoot + "/main/RoleSearchV2.aspx?k=" + PmlEmployeeRoleControlValues.Save(pcValues, PriceMyLoanConstants.CookieExpireMinutes);

                    // 8/23/2004 kb - We added the lending account exec role
                    // for price my loan integration.  Each broker user can
                    // be associated to one or no lending account executive
                    // employee.  Beware!  If you are related to an employee
                    // that was a lending account exec, but has since lost
                    // that role, the currently displayed employee will be
                    // the 'none' employee.  On save, we will nix the existing
                    // relationship.
                    //
                    // 10/21/2004 kb - We now do the same thing with a new
                    // employee role: lock desk agent.
                    //
                    // 7/13/2005 kb - For case 2372, we need to put in a
                    // load-all set and pull each role from it.
                    //
                    // 7/22/2005 kb - Reworked role loading to use new
                    // view.  It seems that loading each role individually
                    // is better than loading the whole broker when you
                    // expect to have *many* loan officers because of pml.
                    //
                    // 4/27/2006 mf - Related employees now displayed in read-only text boxes.
                    //
                    // 7/21/2006 mf - OPM 6365 We don't load all employees in roles anymore.


                    // Now determine if we are editing an existing, or
                    // adding a new one to the set.

                    if ( empId != Guid.Empty )
                    {

                        // Load up the user from the database.  If we can't load
                        // the employee, we punt.  Though this is an external user,
                        // the user is stored as a broker employee in the table.

                        EmployeeDB empDB = new EmployeeDB(empId, PriceMyLoanUser.BrokerId);

                        if( empDB.Retrieve() == false )
                        {
                            throw new CBaseException(ErrorMessages.FailedToLoadUser, "Failed to load specified employee.");
                        }

                        UserInformation.EmployeeId = empId;

                        // 12/28/07 mf. OPM 19614  - Check access rights of this user
                        if ( empDB.PmlExternalManagerEmployeeId != PriceMyLoanUser.EmployeeId )
                        {
                            throw new CBaseException (ErrorMessages.FailedToLoadUser + " " + ErrorMessages.GenericAccessDenied, "Pml user attempts to edit user they do not supervise.");
                        }

                        ViewState.Add( "employeeId" , empDB.ID );

                        EnableAuthCodeViaSms.Checked = empDB.EnableAuthCodeViaSms;
                        enableAuthCodeSms = empDB.EnableAuthCodeViaSms;
                        EnableAuthCodeViaAuthenticator.Checked = empDB.EnableAuthCodeViaAuthenticator;
                        DisableAuthenticator.Visible = empDB.EnableAuthCodeViaAuthenticator;

                        BrokerUserPermissions buP = new BrokerUserPermissions(employeeDB.BrokerID, employeeDB.ID);

                        m_AllowViewingCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowViewingCorrChannelLoans);
                        m_AllowViewingMiniCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowViewingMiniCorrChannelLoans);
                        m_AllowViewingWholesaleChannelLoans.Checked = buP.HasPermission(Permission.AllowViewingWholesaleChannelLoans);

                        m_AllowCreateCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowCreatingCorrChannelLoans);
                        m_AllowCreateMiniCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans);
                        m_AllowCreateWholesaleChannelLoans.Checked = buP.HasPermission(Permission.AllowCreatingWholesaleChannelLoans);

                        RetrievePasswordSettings(empDB, false);

                        // 10/4/2004 kb - Added display of simplified status.  If
                        // the pml user can login, then he is active.  Otherwise,
                        // we say he's inactive and force him so if active before
                        // saving.  Note: New users default to active.
                        m_Active.Checked = (empDB.IsActive && empDB.AllowLogin);
                        m_Inactive.Checked = !(m_Active.Checked);

                        this.LoadBrokerRelationships(empDB);

                        this.LoadMiniCorrespondentRelationships(empDB);

                        this.LoadCorrespondentRelationships(empDB);

                        m_ChangeLogin.Enabled = empDB.IsActive && empDB.AllowLogin;

                        m_LoginNotes.Attributes["hidden"] = "true";
                        m_ChangeLogin.Visible = true;

                        m_Login.ReadOnly    = true;
                        m_Password.ReadOnly = true;
                        m_Confirm.ReadOnly  = true;

                        m_Login.Text = empDB.LoginName;

                        EmployeeRoles roles = new EmployeeRoles(empDB.BrokerID, empId);
                        m_BrokerProcessor.Checked = roles.IsInRole(CEmployeeFields.s_BrokerProcessorId);
                        m_LoanOfficer.Checked = roles.IsInRole(CEmployeeFields.s_LoanRepRoleId);
                        m_ExternalSecondary.Checked = roles.IsInRole(CEmployeeFields.s_ExternalSecondaryId);
                        m_ExternalPostCloser.Checked = roles.IsInRole(CEmployeeFields.s_ExternalPostCloserId);

                        switch (empDB.PmlLevelAccess)
                        {
                            case E_PmlLoanLevelAccess.Individual:
                                m_IndividualAccess.Checked = true;
                                break;
                            case E_PmlLoanLevelAccess.Corporate:
                                m_CorporateAccess.Checked = true;
                                break;
                            case E_PmlLoanLevelAccess.Supervisor:
                            default:
                                throw new UnhandledEnumException(empDB.PmlLevelAccess);
                        }

                        //OPM 1863
                        Guid brokerPmlSiteId = Guid.Empty;
                        SqlParameter[] employeeParameters = {
                                                                new SqlParameter("@EmployeeId", empDB.ID)
                                                            };
                        using (var reader = StoredProcedureHelper.ExecuteReader(empDB.BrokerID, "RetrieveBrokerPmlSiteIdByEmployeeID", employeeParameters ))
                        {
                            if(reader.Read())
                            {
                                brokerPmlSiteId = (Guid) reader["BrokerPmlSiteId"];
                            }
                        }
                        m_IsAccountLocked.Value = "false";
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@LoginNm", empDB.LoginName),
                                                        new SqlParameter("@Type", "P"),
                                                        new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId)
                                                    };
                        using (var reader = StoredProcedureHelper.ExecuteReader(empDB.BrokerID, "GetLoginBlockExpirationDate", parameters))
                        {
                            if(reader.Read())
                            {
                                DateTime nextAvailableLoginTime = DateTime.Parse(reader["LoginBlockExpirationD"].ToString());
                                if (nextAvailableLoginTime.Equals(SmallDateTime.MaxValue))
                                    m_IsAccountLocked.Value = "true";
                            }
                        }

                        LicensesPanel.LosIdentifier = empDB.LosIdentifier;
                        LicensesPanel.LicenseList = empDB.LicenseInformationList; //opm 30840

                        ServiceCredentialContext editContext = ServiceCredentialContext.CreateEditEmployee(empDB);
                        if (!ServiceCredential.HasAvailableServiceCredentialConfiguration(editContext))
                        {
                            LiTabI.Style.Add("display", "none");
                        }
                        else
                        {
                            var serviceCredentials = ServiceCredential.GetTpoEditableEmployeeServiceCredentials(editContext);
                            var serviceCredentialsJson = ObsoleteSerializationHelper.JsonSerialize(serviceCredentials);
                            Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
                        }
                    }
                    else
                    {
                        // New user.
                        // Initialize the update panel state.

                        m_ChangeLogin.Checked = true;
                        m_ChangeLogin.Visible = false;
                        m_LoginNotes.Attributes["hidden"] = "false";

                        m_Inactive.Enabled = false;
                        m_Active  .Enabled = false;

                        // Populate the relationships by using the Supervisor's

                        EmployeeDB currentEmp = new EmployeeDB(PriceMyLoanUser.EmployeeId, PriceMyLoanUser.BrokerId);
                        if ( currentEmp.Retrieve() == false )
                        {
                            throw new CBaseException(ErrorMessages.FailedToLoadUser, "Failed to load employee");
                        }

                        // New users have this set to true by default as long as the broker level bit is set to true.
                        bool newUserEnableSmsAuth = PriceMyLoanUser.BrokerDB.IsEnableMultiFactorAuthenticationTPO;
                        enableAuthCodeSms = newUserEnableSmsAuth;
                        EnableAuthCodeViaSms.Checked = newUserEnableSmsAuth;

                        LoadBrokerRelationships(currentEmp);
                        LoadMiniCorrespondentRelationships(currentEmp);
                        LoadCorrespondentRelationships(currentEmp);

                        m_LoanOfficer.Checked = true;
                        m_IndividualAccess.Checked = true;
                        RetrievePasswordSettings(currentEmp, true);
                    }
                }
                else // (Postback)
                {
                    if ( Request.Form[ "__EVENTTARGET" ] != null
                        && ( Request.Form[ "__EVENTTARGET" ] == "m_Inactive" || Request.Form[ "__EVENTTARGET" ] == "m_Active" ) )
                    {
                        // 09/12/06 mf - We want to catch postbacks as a result of the
                        // activity radio buttons, otherwise we may not be marked
                        // as dirty (client action does not take place).

                        m_Dirty.Value = "1";
                    }

                    // 10/4/2004 kb - Combine status and change login flags
                    // to determine who should be visible and who should be
                    // readonly.

                    m_ChangeLogin.Enabled = m_Active.Checked;
                    m_LoginNotes.Attributes["hidden"] = m_Active.Checked && m_ChangeLogin.Checked ? "false" : "true";
                    if (!m_Active.Checked)
                    {
                        // 12/2/2004 kb - Lock down login editing.  Not active
                        // so no need to edit login.
                        m_ChangeLogin.Checked = false;
                        m_ChangeLogin.Visible = true;
                    }
                    m_Login   .ReadOnly =
                    m_Password.ReadOnly =
                    m_Confirm .ReadOnly = !m_Active.Checked || !m_ChangeLogin.Checked;
                    m_R5      .Enabled = m_Active.Checked && m_ChangeLogin.Checked;
                }

                BrokerDB brokerDB = PriceMyLoanUser.BrokerDB;
                IPContainer.Visible = brokerDB.IsEnableMultiFactorAuthenticationTPO;
                MfaAuthSection.Visible = brokerDB.IsEnableMultiFactorAuthenticationTPO;

                if (employeeDB != null)
                {
                    m_clientCertificateGrid.DataSource = ClientCertificate.ListByUser(employeeDB.BrokerID, employeeDB.UserID);
                    m_clientCertificateGrid.DataBind();

                    if (brokerDB.IsEnableMultiFactorAuthenticationTPO)
                    {
                        if (employeeDB.EnabledMultiFactorAuthentication)
                        {
                            m_registeredIpDataGrid.DataSource = UserRegisteredIp.ListByUserId(brokerDB.BrokerID, employeeDB.UserID);
                            m_registeredIpDataGrid.DataBind();
                        }
                    }
                }

                initPwUI();
            }
            catch( Exception e )
            {
                // Oops!

                ErrorMessage = "Unable to load web form." + e;

                Tools.LogError( e );
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        /// <summary>
        /// Handle commit of new employee.
        /// </summary>

        protected void OnApplyClick( object sender , System.EventArgs a )
        {
            // 6/2/2010 dd - Base on Citi request (M8). We only accept POST request.
            if (Page.Request.HttpMethod != "POST")
            {
                return;
            }
            // 4/7/09 PA - If the login specified is already in use, this will be sent to true when
            // the exception is found and used in the catch to produce a different error message
            bool loginTaken = false;

            // Build basic employee record and commit to the database.
            try
            {
                // Create data access object and initialize it with defaults
                // and what the user specifies.

                EmployeeDB empDB = new EmployeeDB();
                if( PriceMyLoanUser.IsPmlManager == false)
                {
                    ErrorMessage = "Permission denied.  You lack authorization to invoke this command.";

                    return;
                }

                if (!ValidatePortalModeViewCreatePermissions())
                {
                    ErrorMessage = "Unable to grant permission to create loans without granting permission to view loans in the same portal mode.";
                    return;
                }

                if( ViewState[ "employeeId" ] != null )
                {
                    empDB = new EmployeeDB( new Guid( ViewState[ "employeeId" ].ToString() ) , PriceMyLoanUser.BrokerId );
                    empDB.Retrieve();

                    if(PriceMyLoanUser.EmployeeId != empDB.PmlExternalManagerEmployeeId)
                    {
                        ErrorMessage = "Permission denied.  You lack the authority to invoke this command on this user.";
                        return;
                    }

                }
                else
                {
                    // 2/15/2005 dd - If broker has NewUserDefault_PmlUserIsMLeague options then new PML user create will have
                    // Login from MCL turn on by default. OPM #1116
                    // Pull options from Broker db table.
                    var listParams = new SqlParameter[] { new SqlParameter("@id", PriceMyLoanUser.BrokerId) };
                    string sql = "SELECT NewUserDefault_PmlUserIsMLeague FROM BROKER WHERE BrokerID=@id";

                    Action<IDataReader> readHandler = (IDataReader reader) =>
                    {
                        if (reader.Read())
                        {
                            if ((bool)reader["NewUserDefault_PmlUserIsMLeague"])
                            {
                                empDB.PmlSiteID = Guid.NewGuid();
                            }
                        }
                    };

                    DBSelectUtility.ProcessDBData(PriceMyLoanUser.BrokerId, sql, null, listParams, readHandler);
                }

                empDB.BrokerProcessorEmployeeId = new Guid(m_AssignedBrokerProcessorId.Value);
                empDB.MiniCorrExternalPostCloserEmployeeId = new Guid(m_AssignedMiniCorrExternalPostCloserId.Value);
                empDB.CorrExternalPostCloserEmployeeId = new Guid(m_AssignedCorrExternalPostCloserId.Value);
                empDB.EnableAuthCodeViaSms = EnableAuthCodeViaSms.Checked;
                empDB.PmlLevelAccess = m_CorporateAccess.Checked ? E_PmlLoanLevelAccess.Corporate : E_PmlLoanLevelAccess.Individual;

                string roles = string.Join(", ", new[]
                {
                    m_LoanOfficer.Checked ? CEmployeeFields.s_LoanRepRoleId.ToString() : null,
                    m_BrokerProcessor.Checked ? CEmployeeFields.s_BrokerProcessorId.ToString() : null,
                    m_ExternalSecondary.Checked ? CEmployeeFields.s_ExternalSecondaryId.ToString() : null,
                    m_ExternalPostCloser.Checked ? CEmployeeFields.s_ExternalPostCloserId.ToString() : null,
                }.Where(s => s != null));

                if (string.IsNullOrEmpty(roles))
                    throw new CBaseException(ErrorMessages.PmlUserMustHaveAtLeastOneRole, ErrorMessages.PmlUserMustHaveAtLeastOneRole);

                empDB.Roles = roles;

                if (this.IsDisablingAuthenticator.Value == "True")
                {
                    empDB.EnableAuthCodeViaAuthenticator = false;
                }

                // If this is a new entry, use the price group,
                // branch, company, and address of the creating supervisor,
                // otherwise we use what was already loaded.

                if (empDB.IsNew)
                {
                    // Address information comes from the supervising
                    // creator if the user is new.

                    EmployeeDB creatorEmpDB = new EmployeeDB(PriceMyLoanUser.EmployeeId, PriceMyLoanUser.BrokerId);

                    if (creatorEmpDB.Retrieve() == false)
                        throw new CBaseException(ErrorMessages.FailedToSave, "Unable to load supervising employee.");

                    empDB.Address.StreetAddress = creatorEmpDB.Address.StreetAddress;
                    empDB.Address.City          = creatorEmpDB.Address.City;
                    empDB.Address.State         = creatorEmpDB.Address.State;
                    empDB.Address.Zipcode       = creatorEmpDB.Address.Zipcode;

                    // New users are always supervised by the creating supervisor

                    empDB.IsPmlManager = false;
                    empDB.PmlExternalManagerEmployeeId = PriceMyLoanUser.EmployeeId;

                    //User PmlBrokerId is set to the same as his supervisor.
                    empDB.PmlBrokerId = creatorEmpDB.PmlBrokerId;
                    empDB.BrokerID = PriceMyLoanUser.BrokerId;

                    empDB.TPOLandingPageID = creatorEmpDB.TPOLandingPageID;

                    if (creatorEmpDB.UseOriginatingCompanyLpePriceGroupId)
                    {
                        empDB.UseOriginatingCompanyLpePriceGroupId = true;
                    }
                    else
                    {
                        empDB.LpePriceGroupID = creatorEmpDB.LpePriceGroupID;
                    }

                    if (creatorEmpDB.UseOriginatingCompanyBranchId)
                    {
                        empDB.UseOriginatingCompanyBranchId = creatorEmpDB.UseOriginatingCompanyBranchId;
                    }
                    else
                    {
                        empDB.BranchID = creatorEmpDB.BranchID;
                    }

                    // 6/1/2010 dd - Copy the relationship from the supervisor.
                    //Broker Relationships
                    empDB.LenderAcctExecEmployeeID = creatorEmpDB.LenderAcctExecEmployeeID;
                    empDB.UnderwriterEmployeeID = creatorEmpDB.UnderwriterEmployeeID;
                    empDB.LockDeskEmployeeID = creatorEmpDB.LockDeskEmployeeID;
                    empDB.ManagerEmployeeID = creatorEmpDB.ManagerEmployeeID;
                    empDB.ProcessorEmployeeID = creatorEmpDB.ProcessorEmployeeID;
                    empDB.JuniorUnderwriterEmployeeID = creatorEmpDB.JuniorUnderwriterEmployeeID;
                    empDB.JuniorProcessorEmployeeID = creatorEmpDB.JuniorProcessorEmployeeID;

                    //Corr Relationships
                    empDB.CorrLenderAccExecEmployeeId = creatorEmpDB.CorrLenderAccExecEmployeeId;
                    empDB.CorrUnderwriterEmployeeId = creatorEmpDB.CorrUnderwriterEmployeeId;
                    empDB.CorrLockDeskEmployeeId = creatorEmpDB.CorrLockDeskEmployeeId;
                    empDB.CorrManagerEmployeeId = creatorEmpDB.CorrManagerEmployeeId;
                    empDB.CorrProcessorEmployeeId = creatorEmpDB.CorrProcessorEmployeeId;
                    empDB.CorrJuniorUnderwriterEmployeeId = creatorEmpDB.CorrJuniorUnderwriterEmployeeId;
                    empDB.CorrJuniorProcessorEmployeeId = creatorEmpDB.CorrJuniorProcessorEmployeeId;

                    empDB.CorrCreditAuditorEmployeeId = creatorEmpDB.CorrCreditAuditorEmployeeId;
                    empDB.CorrLegalAuditorEmployeeId = creatorEmpDB.CorrLegalAuditorEmployeeId;
                    empDB.CorrPurchaserEmployeeId = creatorEmpDB.CorrPurchaserEmployeeId;
                    empDB.CorrSecondaryEmployeeId = creatorEmpDB.CorrSecondaryEmployeeId;

                    empDB.CorrespondentLandingPageID = creatorEmpDB.CorrespondentLandingPageID;
                    if (creatorEmpDB.UseOriginatingCompanyLpePriceGroupId)
                    {
                        empDB.UseOriginatingCompanyCorrPriceGroupId = true;
                    }
                    else
                    {
                        empDB.CorrespondentPriceGroupID = creatorEmpDB.CorrespondentPriceGroupID;
                    }
                    if (creatorEmpDB.UseOriginatingCompanyCorrBranchId)
                    {
                        empDB.UseOriginatingCompanyCorrBranchId = creatorEmpDB.UseOriginatingCompanyCorrBranchId;
                    }
                    else
                    {
                        empDB.CorrespondentBranchID = creatorEmpDB.CorrespondentBranchID;
                    }

                    //MiniCorr Relationships
                    empDB.MiniCorrLenderAccExecEmployeeId = creatorEmpDB.MiniCorrLenderAccExecEmployeeId;
                    empDB.MiniCorrUnderwriterEmployeeId = creatorEmpDB.MiniCorrUnderwriterEmployeeId;
                    empDB.MiniCorrLockDeskEmployeeId = creatorEmpDB.MiniCorrLockDeskEmployeeId;
                    empDB.MiniCorrManagerEmployeeId = creatorEmpDB.MiniCorrManagerEmployeeId;
                    empDB.MiniCorrProcessorEmployeeId = creatorEmpDB.MiniCorrProcessorEmployeeId;
                    empDB.MiniCorrJuniorUnderwriterEmployeeId = creatorEmpDB.MiniCorrJuniorUnderwriterEmployeeId;
                    empDB.MiniCorrJuniorProcessorEmployeeId = creatorEmpDB.MiniCorrJuniorProcessorEmployeeId;

                    empDB.MiniCorrCreditAuditorEmployeeId = creatorEmpDB.MiniCorrCreditAuditorEmployeeId;
                    empDB.MiniCorrLegalAuditorEmployeeId = creatorEmpDB.MiniCorrLegalAuditorEmployeeId;
                    empDB.MiniCorrPurchaserEmployeeId = creatorEmpDB.MiniCorrPurchaserEmployeeId;
                    empDB.MiniCorrSecondaryEmployeeId = creatorEmpDB.MiniCorrSecondaryEmployeeId;

                    empDB.MiniCorrespondentLandingPageID = creatorEmpDB.MiniCorrespondentLandingPageID;
                    if (creatorEmpDB.UseOriginatingCompanyLpePriceGroupId)
                    {
                        empDB.UseOriginatingCompanyMiniCorrPriceGroupId = true;
                    }
                    else
                    {
                        empDB.MiniCorrespondentPriceGroupID = creatorEmpDB.MiniCorrespondentPriceGroupID;
                    }

                    if (creatorEmpDB.UseOriginatingCompanyMiniCorrBranchId)
                    {
                        empDB.UseOriginatingCompanyMiniCorrBranchId = creatorEmpDB.UseOriginatingCompanyMiniCorrBranchId;
                    }
                    else
                    {
                        empDB.MiniCorrespondentBranchID = creatorEmpDB.MiniCorrespondentBranchID;
                    }
                }

                if( m_Active.Checked && ( empDB.IsNew || m_ChangeLogin.Checked == true ) )
                {
                    String loginTrimmed = m_Login.Text.TrimWhitespaceAndBOM();
                    empDB.LoginName = loginTrimmed;
                    try
                    {
                        if( loginTrimmed != "" )
                        {
                            BrokerDB brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
                            // 4/9/09 PA - Removed "empDB.LoginName != m_Login.Text.TrimWhitespaceAndBOM()" from if statement because it was always returning false and never letting program flow into the if statement, which is where duplicate login name checking is done.
                            if (Tools.IsPmlLoginUnique(brokerDB, m_Login.Text.TrimWhitespaceAndBOM(), empDB.UserID) == false)
                            {
                                loginTaken = true;
                                throw new CBaseException(ErrorMessages.LoginNameAlreadyInUseFunctionAlternate(m_Login.Text.TrimWhitespaceAndBOM()), ErrorMessages.LoginNameAlreadyInUse);
                            }
                        }
                        else
                        {
                            throw new CBaseException(ErrorMessages.SpecifyLoginName, ErrorMessages.SpecifyLoginName);
                        }

                        // 3/20/2006 mf - OPM 4226 - We do not change existing password if fields are blank
                        if(empDB.IsNew || m_Password.Text != "" || m_Confirm.Text != "")
                        {
                            if( m_Password.Text == "" )
                            {
                                throw new CBaseException(ErrorMessages.SpecifyPassword, ErrorMessages.SpecifyPassword);
                            }

                            if( m_Password.Text != m_Confirm.Text )
                            {
                                throw new CBaseException(ErrorMessages.PasswordsDontMatch, ErrorMessages.PasswordsDontMatch);
                            }

                            switch( empDB.IsStrongPassword( m_Password.Text ) )
                            {
                                case StrongPasswordStatus.PasswordContainsLogin:
                                    throw new CBaseException(ErrorMessages.PwCannotContainLoginOrName, ErrorMessages.PwCannotContainLoginOrName);

                                case StrongPasswordStatus.PasswordMustBeAlphaNumeric:
                                    throw new CBaseException(ErrorMessages.PasswordMustBeAlphaNumeric, ErrorMessages.PasswordMustBeAlphaNumeric);

                                case StrongPasswordStatus.PasswordTooShort:
                                    throw new CBaseException(ErrorMessages.PasswordTooShort, ErrorMessages.PasswordTooShort);

                                case StrongPasswordStatus.PasswordRecycle:
                                    throw new CBaseException(ErrorMessages.PasswordsShouldNotBeRecycled, ErrorMessages.PasswordsShouldNotBeRecycled);

                                // OPM 24614 - note: the message is too long for this page, so I've cut out the example text
                                case StrongPasswordStatus.PasswordContainsConsecutiveIdenticalChars:
                                    int index = ErrorMessages.PasswordContainsConsecIdenticalChars.IndexOf("(");
                                    string errMsg = ErrorMessages.PasswordContainsConsecIdenticalChars.Substring(0, index);
                                    throw new CBaseException(errMsg, errMsg);

                                // OPM 24614 - note: the message is too long for this page, so I've cut out the example text
                                case StrongPasswordStatus.PasswordContainsConsecutiveAlphanumericChars:
                                    index = ErrorMessages.PasswordContainsConsecCharsInAlphaOrNumericOrder.IndexOf("(");
                                    errMsg = ErrorMessages.PasswordContainsConsecCharsInAlphaOrNumericOrder.Substring(0, index);
                                    throw new CBaseException(errMsg, errMsg);
                                case StrongPasswordStatus.PasswordHasUnverifiedCharacters:
                                    throw new CBaseException(Utilities.SafeHtmlString(ErrorMessages.PasswordHasUnverifiedCharacters), Utilities.SafeHtmlString(ErrorMessages.PasswordHasUnverifiedCharacters));

                            }
                        }
                    }
                    catch( CBaseException e )
                    {
                        ErrorMessage = loginTaken
                            ? ErrorMessages.LoginNameAlreadyInUseFunctionAlternate(m_Login.Text.TrimWhitespaceAndBOM())
                            : "Failed to save employee.";

                        m_LoginErrorMessage.Text = e.UserMessage;

                        return;
                    }



                    // 3/20/2006 mf - OPM 4226 Only change the password if an approved modification occured
                    if (m_Password.Text != "" && m_Confirm.Text != "")
                        empDB.Password  = m_Password.Text;

                }

                empDB.AllowLogin = empDB.IsActive = m_Active.Checked;

                if ( empDB.IsNew )
                {
                    // 6/3/2005 kb - Per case 1904, we now default all
                    // new pml users to shareable.

                    empDB.PasswordExpirationD = DateTime.MaxValue;

                    empDB.BrokerID = PriceMyLoanUser.BrokerId;

                    empDB.IsSharable = true;

                    empDB.UserType = 'P';
                }

                try
                {
                    Boolean IsForceChangePasswordForPml = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId).IsForceChangePasswordForPml;
                    if (m_PasswordNeverExpires.Checked)
                    {
                        empDB.PasswordExpirationD = SmallDateTime.MaxValue;
                        empDB.PasswordExpirationPeriod = 0;
                    }
                    else
                    {
                        if (m_MustChangePasswordAtNextLogin.Checked && !IsForceChangePasswordForPml)
                            empDB.PasswordExpirationD = SmallDateTime.MinValue;

                        if (m_PasswordExpiresOn.Checked == true && m_ExpirationDate.Text != "")
                            empDB.PasswordExpirationD = DateTime.Parse(m_ExpirationDate.Text);

                        empDB.PasswordExpirationPeriod = m_CycleExpiration.Checked ? Int32.Parse(m_ExpirationPeriod.SelectedItem.Value) : 0;
                    }

                    if (IsForceChangePasswordForPml && m_Password.Text != "")
                        empDB.PasswordExpirationD = SmallDateTime.MinValue;
                }
                catch (CBaseException e)
                {
                    ErrorMessage = "Failed to save user.";
                    m_LoginErrorMessage.Text = e.UserMessage;

                    return;
                }

                empDB.LosIdentifier = LicensesPanel.LosIdentifier;
                empDB.LicenseInformationList = LicensesPanel.LicenseList;

                empDB.WhoDoneIt = PriceMyLoanUser.UserId;

                this.UserInformation.SetContactInfoToEmployee(empDB);

                if( empDB.Save(PrincipalFactory.CurrentPrincipal) == false )
                {
                    throw new CBaseException(ErrorMessages.FailedToSave, "Employee save returned false.");
                }
                else
                {
                    // Revise our viewstate so that we don't look new all
                    // the time.
                    bool wasNew = NewEmployee;
                    ViewState.Add( "employeeId" , empDB.ID );
                    IsUpdated = true;
                    // Save the user's permissions after we successfully saved.

                    try
                    {
                        // 12/2/2004 kb - Update all employee permissions after
                        // we save.  This way, we only commit the changes when
                        // the save is successful and we don't have to worry
                        // about whether or not the employee exists.  Add other
                        // permissions here as needed.

                        BrokerUserPermissions creatorPermissions = new BrokerUserPermissions( PriceMyLoanUser.BrokerId , PriceMyLoanUser.EmployeeId );
                        BrokerUserPermissions buP = new BrokerUserPermissions( empDB.BrokerID , empDB.ID );

                        // Set the permissions based on user who created this new user, if
                        // this is a new employee. Otherwise, leave these as is.
                        if (wasNew)
                        {
                            buP.SetPermission( Permission.CanRunPricingEngineWithoutCreditReport, creatorPermissions.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport) );
                            buP.SetPermission( Permission.CanSubmitWithoutCreditReport, creatorPermissions.HasPermission(Permission.CanSubmitWithoutCreditReport) );
                        }

                        //for some reason, they're all set to false....
                        buP.SetPermission(Permission.AllowViewingCorrChannelLoans, m_AllowViewingCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowViewingMiniCorrChannelLoans, m_AllowViewingMiniCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowViewingWholesaleChannelLoans, m_AllowViewingWholesaleChannelLoans.Checked);

                        buP.SetPermission(Permission.AllowCreatingCorrChannelLoans, m_AllowCreateCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowCreatingMiniCorrChannelLoans, m_AllowCreateMiniCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowCreatingWholesaleChannelLoans, m_AllowCreateWholesaleChannelLoans.Checked);
                        // .

                        buP.Update();
                    }
                    catch
                    {
                        // Turn off any broker user permissions if we are
                        // unable to edit and save.

                        // .
                    }
                }

                // Update the ui so we don't try to save when someone
                // clicks ok after apply.  Reason: we try to resave a
                // password change that takes place with the previous
                // apply request (but pw is blanked out).

                m_ChangeLogin.Visible = true;
                m_LoginNotes.Attributes["hidden"] = "true";

                m_ChangeLogin.Checked = false;

                m_Login.ReadOnly    = true;
                m_Password.ReadOnly = true;
                m_Confirm.ReadOnly  = true;

                Guid empId = empDB.ID;
                // Reload relationship from database. The reason it need to reload from database is because when add new user, these
                // relationship are not in the object.
                empDB = new EmployeeDB(empId, PriceMyLoanUser.BrokerId);
                empDB.Retrieve();
                m_LenderAcctExec.Text = empDB.LenderAcctExecEmployeeID != Guid.Empty ? empDB.LenderAccExecFullName : "<-- None -->";
                m_Underwriter.Text = empDB.UnderwriterEmployeeID != Guid.Empty ? empDB.UnderwriterFullName : "<-- None -->";
                m_JuniorUnderwriter.Text = empDB.JuniorUnderwriterEmployeeID != Guid.Empty ? empDB.JuniorUnderwriterFullName : "<-- None -->";
                m_LockDesk.Text = empDB.LockDeskEmployeeID != Guid.Empty ? empDB.LockDeskFullName : "<-- None -->";
                m_Manager.Text = empDB.ManagerEmployeeID != Guid.Empty ? empDB.ManagerFullName : "<-- None -->";
                m_Processor.Text = empDB.ProcessorEmployeeID != Guid.Empty ? empDB.ProcessorFullName : "<-- None -->";
                m_JuniorProcessor.Text = empDB.JuniorProcessorEmployeeID != Guid.Empty ? empDB.JuniorProcessorFullName : "<-- None -->";
                EnableAuthCodeViaAuthenticator.Checked = empDB.EnableAuthCodeViaAuthenticator;
                IsDisablingAuthenticator.Value = "";
                DisableAuthenticator.Visible = empDB.EnableAuthCodeViaAuthenticator;
                initPwUI();

                m_Dirty.Value = "0";
                HasSaved.Value = "true";
            }
            catch( Exception e )
            {
                // Oops!

                ErrorMessage = "Failed to save employee.";

                Tools.LogError( e );
            }
        }

        private bool ValidatePortalModeViewCreatePermissions()
        {
            if ((m_AllowCreateWholesaleChannelLoans.Checked && !m_AllowViewingWholesaleChannelLoans.Checked) ||
                (m_AllowCreateMiniCorrChannelLoans.Checked && !m_AllowViewingMiniCorrChannelLoans.Checked) ||
                (m_AllowCreateCorrChannelLoans.Checked && !m_AllowViewingCorrChannelLoans.Checked))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Handle commit of new employee.
        /// </summary>

        protected void OnOkClick( object sender , System.EventArgs a )
        {
            // 6/2/2010 dd - Base on Citi request (M8). We only accept POST request.
            if (Page.Request.HttpMethod != "POST")
            {
                return;
            }
            // Save current user and close the ui.

            try
            {
                // Delegate the save request as if apply were clicked.

                OnApplyClick( sender , a );

                // Now tell the ui to close. only if there was no error
                if (!hasError)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "autoclose", "var autoClose = true;", true);
                }
            }
            catch( Exception e )
            {
                // Oops!

                ErrorMessage = "Failed to save employee.";

                Tools.LogError( e );
            }
        }

        private void SetRequiredValidatorMessage(System.Web.UI.WebControls.BaseValidator v)
        {
            v.Controls.Add(new HtmlImage { Src = ResolveUrl("~/images/error_pointer.gif") });
        }

        private void RetrievePasswordSettings(EmployeeDB empDB, Boolean newEmp)
        {
            Boolean PasswordOption = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId).IsForceChangePasswordForPml;
            m_MustChangePasswordAtNextLogin.Checked = false;
            m_PasswordNeverExpires.Checked = false;
            m_PasswordExpiresOn.Checked = false;
            m_ExpirationDate.Text = "";

            if (PasswordOption)
            {
                m_MustChangePasswordAtNextLogin.Checked = true;
                m_PasswordExpiresOn.Enabled = false;
                m_PasswordNeverExpires.Enabled = false;
            }
            else
            {
                if (!newEmp)
                {
                    if (empDB.PasswordExpirationD.CompareTo(SmallDateTime.MaxValue) >= 0)
                    {
                        m_PasswordNeverExpires.Checked = true;
                        expirationPeriodDisabled = true;
                        m_CycleExpiration.Checked = false;
                        cycleExpirationDisabled = true;
                        expirationDateDisabled = true;
                    }
                    else
                    {
                        if (empDB.PasswordExpirationD.CompareTo(SmallDateTime.MinValue) <= 0)
                            m_MustChangePasswordAtNextLogin.Checked = true;
                        else
                        {
                            m_ExpirationDate.Text = empDB.PasswordExpirationD.ToShortDateString();
                            m_PasswordExpiresOn.Checked = true;
                        }
                        cycleExpirationDisabled = false;
                        expirationDateDisabled = !m_PasswordExpiresOn.Checked;
                    }
                }
                else
                    m_MustChangePasswordAtNextLogin.Checked = true;
            }

            if ((empDB.PasswordExpirationPeriod > 0) &&
                (empDB.PasswordExpirationD.CompareTo(SmallDateTime.MaxValue) < 0))
            {
                switch (empDB.PasswordExpirationPeriod)
                {
                    case 15: m_ExpirationPeriod.SelectedIndex = 0;
                        break;

                    case 30: m_ExpirationPeriod.SelectedIndex = 1;
                        break;

                    case 45: m_ExpirationPeriod.SelectedIndex = 2;
                        break;

                    case 60: m_ExpirationPeriod.SelectedIndex = 3;
                        break;
                }

                m_CycleExpiration.Checked = true;
            }
            else
                m_CycleExpiration.Checked = false;

            expirationPeriodDisabled = !m_CycleExpiration.Checked;
        }

        private void initPwUI()
        {
            cycleExpirationDisabled = (m_PasswordNeverExpires.Checked);
            expirationPeriodDisabled = (!m_CycleExpiration.Checked);
            expirationDateDisabled = (!m_PasswordExpiresOn.Checked);
        }
    }

}
