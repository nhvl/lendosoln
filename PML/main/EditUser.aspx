<%@ Register TagPrefix="uc" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="ModelessDlg" Src="../Common/ModalDlg/ModelessDlg.ascx" %>
<%@ Register TagPrefix="uc" TagName="LendingLicenses" Src="LendingLicensesPanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="UserInformation" Src="~/main/UserInformation.ascx" %>
<%@ Page Language="c#" CodeBehind="EditUser.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.EditUser" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Edit User</title>
    <style>
        .tabDiv table
        {
            width:100%;
        }

        .sectionHeading
        {
            font-weight: bold;
            color: white;
            background-color: #003366;
            width:100%;
        }

        td.sectionHeading, div.sectionHeading div
        {
            padding: 3px 0 3px 8px;
        }
    </style>
</head>
<body class="modal-iframe-body">

<form method="post" runat="server">
    <div lqb-popup hidden class="modal-edit-user">
        <div class="modal-header">
            <button type="button" class="close" onclick="onCancelClick()"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title" id="m_TitleBar" runat="server">Add New User</h4>
        </div>
        <div class="modal-body">
            <asp:HiddenField runat="server" ID="m_BPPickerUrl" />
            <asp:HiddenField runat="server" ID="m_PCPickerUrl" />
            <asp:HiddenField runat="server" ID="m_AssignedBrokerProcessorId" />
            <asp:HiddenField runat="server" ID="m_AssignedBrokerProcessorName" />
            <asp:HiddenField runat="server" ID="m_AssignedMiniCorrExternalPostCloserId" />
            <asp:HiddenField runat="server" ID="m_AssignedMiniCorrExternalPostCloserName" />
            <asp:HiddenField runat="server" ID="m_AssignedCorrExternalPostCloserId" />
            <asp:HiddenField runat="server" ID="m_AssignedCorrExternalPostCloserName" />
            <asp:HiddenField ID="HasSaved" Value="false" runat="server" />

            <ul id="tabs" class="nav nav-tabs nav-tabs-panels margin-bottom">
                <li><a id="TabA" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">User Information</a></li>
                <li><a id="TabB" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">Credentials</a></li>
                <li><a  id="TabC" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">Roles, Loan Access, & Permissions</a></li>
                <li runat="server" id="LiTabD"><a id="TabD" onkeypress="if( event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">Broker Relationships</a></li>
                <li runat="server" id="LiTabG"><a id="TabG" onkeypress="if( event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">Mini-Correspondent Relationships</a></li>
                <li runat="server" id="LiTabH"><a id="TabH" onkeypress="if( event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">Correspondent Relationships</a></li>
                <li><a id="TabE" onkeypress="if( event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">Licenses</a></li>
                <li><a id="TabF" onkeypress="if( event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">System Access</a></li>
                <li runat="server" id="LiTabI"><a id="TabI" onkeypress="if( event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }">Services</a></li>
            </ul>
            <div id="BaseA" class="tabDiv" hidden>
                <uc:UserInformation id="UserInformation" runat="server" />
            </div>
            <div id="BaseB" class="tabDiv" hidden>
                <div class="content-container">
                    <header class="header">Login Information</header>
                    <div class="profile-padding-left">
                        <asp:Panel ID="m_UnlockAccountPanel" runat="server">
                            <div class="table">
                                <div>
                                    <div>
                                        <ml:EncodedLabel ID="m_LockedAccount" runat="server" ForeColor="Red" Text="This account is locked" />
                                    </div>
                                    <div>
                                        <button type="button" onclick="onUnlockAccountClick();">Unlock</button>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="margin-bottom-edit-user">
                            <asp:CheckBox ID="m_ChangeLogin" runat="server" Text="Change login and password?" />
                            <span class="changeLoginNote" hidden>(Leave the password blank if you don't want to change it.)</span>
                        </div>

                        <div class="login-info">
                            <div class="table">
                                <div>
                                    <div>
                                        <label>Login Name <span class="RequiredSpan text-danger">*</span>
                                            <asp:RequiredFieldValidator runat="server" EnableViewState="False" ControlToValidate="m_Login" Display="None" ID="m_R5"/>
                                        </label>
                                        <asp:TextBox ID="m_Login" runat="server" Style="padding-left: 4px" MaxLength="36" onblur="validatePage();" />
                                    </div>
                                    <div>
                                        <label>Password
                                            <span id="m_LoginNotes" runat="server" style="font: 8pt arial; color: dimgray;">(<a onclick="showPasswordRules();">show rules</a>)</span>
                                            <span class="RequiredSpan text-danger">*</span>
                                        </label>
                                        <asp:TextBox ID="m_Password" runat="server" Style="padding-left: 4px" TextMode="Password" MaxLength="36" onblur="validatePage();"/>
                                    </div>
                                    <div>
                                        <label>Retype
                                            <span class="RequiredSpan text-danger">*</span>
                                        </label>
                                        <asp:TextBox ID="m_Confirm" runat="server" Style="padding-left: 4px" TextMode="Password" MaxLength="36" onblur="validatePage();"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="margin-bottom-edit-user"><asp:RadioButton TabIndex="145" ID="m_MustChangePasswordAtNextLogin" onclick="expirationPolicyClick();" runat="server" Text="Must change password at next login" GroupName="Password"/></div>
                            <div class="margin-bottom-edit-user"><asp:RadioButton ID="m_PasswordNeverExpires" TabIndex="150" onclick="expirationPolicyClick();" runat="server" Text="Password never expires" GroupName="Password"/></div>
                            <div>
                                <div class="table table-modal-padding-bottom">
                                    <div>
                                        <div>
                                            <asp:RadioButton ID="m_PasswordExpiresOn" TabIndex="155" onclick="expirationPolicyClick();" runat="server" Text="Password expires on" GroupName="Password"/>
                                        </div>
                                        <div>
                                            <ml:DateTextBox ID="m_ExpirationDate" TabIndex="160" Style="padding-left: 4px;" runat="server" preset="date" HelperAlign="AbsMiddle" Width="64" IsDisplayCalendarHelper="false"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="expire-pass margin-bottom">
                                <asp:CheckBox ID="m_CycleExpiration" TabIndex="165" onclick="expirationCycleClick();" runat="server" Text="Expire passwords every"/>
                                <span class="day-following-update">
                                    <asp:DropDownList TabIndex="170" ID="m_ExpirationPeriod" disabled runat="server">
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="30">30</asp:ListItem>
                                        <asp:ListItem Value="45">45</asp:ListItem>
                                        <asp:ListItem Value="60">60</asp:ListItem>
                                    </asp:DropDownList>
                                </span>
                                days following update
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-container">
                    <header class="header">Status</header>
                    <div class="profile-padding-left">
                        <div class="margin-bottom">
                            <div class="margin-bottom-edit-user"><asp:RadioButton ID="m_Active"   runat="server" Text="Active"   GroupName="Status" Checked="True"/></div>
                            <div class="margin-bottom-edit-user"><asp:RadioButton ID="m_Inactive" runat="server" Text="Inactive" GroupName="Status"               /></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="BaseC" class="tabDiv" hidden>
                <div class="content-container">
                    <header class="header">Roles <span class="RequiredSpan text-danger">*</span></header>
                    <div class="profile-padding-left">
                        <div class="margin-bottom">
                            <div class="margin-bottom-edit-user"><asp:CheckBox ID="m_LoanOfficer"        Text="Loan Officer" runat="server" onclick="validatePage();" /></div>
                            <div class="margin-bottom-edit-user"><asp:CheckBox ID="m_BrokerProcessor"    Text="Processor"    runat="server" onclick="f_setBrokerProcessorPickerStatus();validatePage();" /></div>
                            <div class="margin-bottom-edit-user"><asp:CheckBox ID="m_ExternalSecondary"  Text="Secondary"    runat="server" onclick="validatePage();" /></div>
                            <div class="margin-bottom-edit-user"><asp:CheckBox ID="m_ExternalPostCloser" Text="Post-Closer"  runat="server" onclick="f_setExternalPostCloserPickerStatus();validatePage();" /></div>
                        </div>
                    </div>
                </div>
                <div class="content-container">
                    <header class="header">Loan Access Level</header>
                    <div class="profile-padding-left">
                        <div class="margin-bottom">
                            <div class="margin-bottom-edit-user"><asp:RadioButton ID="m_CorporateAccess"  Text="Corporate - within company"    runat="server" GroupName="m_AccessLevel" /></div>
                            <div class="margin-bottom-edit-user"><asp:RadioButton ID="m_IndividualAccess" Text="Individual - only if assigned" runat="server" GroupName="m_AccessLevel" /></div>
                        </div>
                    </div>
                </div>
                <div class="content-container">
                    <header class="header">Permissions</header>
                    <div class="profile-padding-left">
                        <div class="margin-bottom">
                            <div class="margin-bottom-edit-user" id="AllowWholesaleViewingLoanRow" runat="server"><input type="checkbox" runat="server" id="m_AllowViewingWholesaleChannelLoans" class="viewLoan  " label="Allow viewing wholesale channel loans.          "/></div>
                            <div class="margin-bottom-edit-user" id="AllowWholesaleCreateLoanRow"  runat="server"><input type="checkbox" runat="server" id="m_AllowCreateWholesaleChannelLoans"  class="createLoan" label="Allow creating wholesale channel loans.         "/></div>
                            <div class="margin-bottom-edit-user" id="AllowMiniCorrViewingLoanRow"  runat="server"><input type="checkbox" runat="server" id="m_AllowViewingMiniCorrChannelLoans"  class="viewLoan  " label="Allow viewing mini-correspondent channel loans. "/></div>
                            <div class="margin-bottom-edit-user" id="AllowMiniCorrCreateLoanRow"   runat="server"><input type="checkbox" runat="server" id="m_AllowCreateMiniCorrChannelLoans"   class="createLoan" label="Allow creating mini-correspondent channel loans."/></div>
                            <div class="margin-bottom-edit-user" id="AllowCorrViewingLoanRow"      runat="server"><input type="checkbox" runat="server" id="m_AllowViewingCorrChannelLoans"      class="viewLoan  " label="Allow viewing correspondent channel loans.      "/></div>
                            <div class="margin-bottom-edit-user" id="AllowCorrCreateLoanRow"       runat="server"><input type="checkbox" runat="server" id="m_AllowCreateCorrChannelLoans"       class="createLoan" label="Allow creating correspondent channel loans.     "/></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="BaseD" class="tabDiv" hidden>
                <div class="content-container">
                    <header class="header">
                        Users assigned to all loans
                        <span style="text-decoration: underline">created</span>
                        by this user:
                    </header>

                    <div class="profile-padding-left">
                        <div class="table table-modal-edit-user margin-bottom">
                            <div><div>Processor</div><div>
                                <ml:EncodedLabel runat="server" ID="m_AssignedBrokerProcessor"/>
                                [
                                    <a id="m_pickerNoneLink" style="cursor: pointer" onclick="f_clearBrokerProcessor(false);">None</a> |
                                    <a id="m_pickerPickLink" style="cursor: pointer" onclick="f_assignBrokerProcessor();">Pick Processor</a>
                                ]
                            </div></div>
                            <div><div>Lender Manager          </div><div><asp:TextBox ID="m_Manager"         ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Lender Processor        </div><div><asp:TextBox ID="m_Processor"       ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Lender Junior Processor </div><div><asp:TextBox ID="m_JuniorProcessor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Lender Account Executive</div><div><asp:TextBox ID="m_LenderAcctExec"  ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                        </div>
                    </div>
                </div>

                <div class="content-container">
                    <header class="header">
                        Users assigned to all loans <span style="text-decoration: underline">registered/locked</span> by this user:
                    </header>

                    <div class="profile-padding-left">
                        <div class="table table-modal-edit-user margin-bottom">
                            <div><div>Lender Underwriter       </div><div><asp:TextBox ID="m_Underwriter"       ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Lender Junior Underwriter</div><div><asp:TextBox ID="m_JuniorUnderwriter" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Lender Lock Desk         </div><div><asp:TextBox ID="m_LockDesk"          ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="BaseG" class="tabDiv" hidden>
                <div class="content-container">
                    <header class="header">Users assigned to all loans <span style="text-decoration: underline">created</span> by this user:</header>

                    <div class="profile-padding-left">
                        <div class="table table-modal-edit-user margin-bottom">
                            <div><div>Post-Closer</div><div>
                                <ml:EncodedLabel runat="server" ID="m_AssignedMiniCorrExternalPostCloser"/>
                                [
                                    <a id="m_miniCorrExternalPostCloserPickerNoneLink" style="cursor: pointer" onclick="f_clearMiniCorrExternalPostCloser(false);">None</a>
                                    |
                                    <a id="m_miniCorrExternalPostCloserPickerPickLink" style="cursor: pointer" onclick="f_assignMiniCorrExternalPostCloser();">Pick Post-Closer</a>
                                ]
                            </div></div>
                            <div><div>Investor Manager</div><div><asp:TextBox ID="m_MiniCorrManager" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Processor</div><div><asp:TextBox ID="m_MiniCorrProcessor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Junior Processor</div><div><asp:TextBox ID="m_MiniCorrJuniorProcessor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Account Executive</div><div><asp:TextBox ID="m_MiniCorrLenderAcctExec" runat="server" ReadOnly="True" CssClass="input-w240" /></div></div>
                        </div>
                    </div>
                </div>

                <div class="content-container">
                    <header class="header">Users assigned to all loans <span style="text-decoration: underline">registered/locked</span> by this user:</header>

                    <div class="profile-padding-left">
                        <div class="table table-modal-edit-user margin-bottom">
                            <div><div>Investor Underwriter</div><div><asp:TextBox ID="m_MiniCorrUnderwriter" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Junior Underwriter</div><div><asp:TextBox ID="m_MiniCorrJuniorUnderwriter" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Credit Auditor</div><div><asp:TextBox ID="m_MiniCorrCreditAuditor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Legal Auditor</div><div><asp:TextBox ID="m_MiniCorrLegalAuditor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Lock Desk</div><div><asp:TextBox ID="m_MiniCorrLockDesk" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Purchaser</div><div><asp:TextBox ID="m_MiniCorrPurchaser" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Secondary</div><div><asp:TextBox ID="m_MiniCorrSecondary" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="BaseH" class="tabDiv" hidden>
                <div class="content-container">
                    <header class="header">Users assigned to all loans <span style="text-decoration: underline">created</span> by this user:</header>

                    <div class="profile-padding-left">
                        <div class="table table-modal-edit-user margin-bottom">
                            <div><div>Post-Closer</div><div>
                                <ml:EncodedLabel runat="server" ID="m_AssignedCorrExternalPostCloser"/>
                                [
                                    <a id="m_corrExternalPostCloserPickerNoneLink" style="cursor: pointer" onclick="f_clearCorrExternalPostCloser(false);">None</a>
                                    |
                                    <a id="m_corrExternalPostCloserPickerPickLink" style="cursor: pointer" onclick="f_assignCorrExternalPostCloser();">Pick Post-Closer</a>
                                ]
                            </div></div>
                            <div><div>Investor Manager</div><div><asp:TextBox ID="m_CorrManager" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Processor</div><div><asp:TextBox ID="m_CorrProcessor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Junior Processor</div><div><asp:TextBox ID="m_CorrJuniorProcessor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Account Executive</div><div><asp:TextBox ID="m_CorrLenderAcctExec" runat="server" ReadOnly="True" CssClass="input-w240" /></div></div>
                        </div>
                    </div>
                </div>

                <div class="content-container">
                    <header class="header">
                        Users assigned to all <span style="text-decoration: underline">prior approved</span>
                        loans <span style="text-decoration: underline">registered/locked</span> by this user:
                    </header>

                    <div class="profile-padding-left">
                        <div class="table table-modal-edit-user margin-bottom">
                            <div><div>Investor Underwriter</div><div><asp:TextBox ID="m_CorrUnderwriter" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Junior Underwriter</div><div><asp:TextBox ID="m_CorrJuniorUnderwriter" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                        </div>
                    </div>
                </div>

                <div class="content-container">
                    <header class="header">Users assigned to all loans <span style="text-decoration: underline">registered/locked</span> by this user:</header>

                    <div class="profile-padding-left">
                        <div class="table table-modal-edit-user margin-bottom">
                            <div><div>Investor Credit Auditor</div><div><asp:TextBox ID="m_CorrCreditAuditor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Legal Auditor</div><div><asp:TextBox ID="m_CorrLegalAuditor" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Lock Desk</div><div><asp:TextBox ID="m_CorrLockDesk" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Purchaser</div><div><asp:TextBox ID="m_CorrPurchaser" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                            <div><div>Investor Secondary</div><div><asp:TextBox ID="m_CorrSecondary" ReadOnly="True" runat="server" CssClass="input-w240" /></div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="BaseE" class="tabDiv" hidden>
                <uc:LendingLicenses id="LicensesPanel" NamingPrefix="pmlUser" runat="server"/>
            </div>

            <div id="BaseF" class="tabDiv">
                <div id="MfaAuthSection" runat="server" class="margin-bottom">
                    <div class="sectionheading margin-bottom-edit-user">Multi-factor Authentication</div>
                    <div class="margin-bottom-edit-user">
                        <asp:CheckBox runat="server" ID="EnableAuthCodeViaSms" Text="Enable auth code transmission via SMS text" /> <br />
                        <asp:CheckBox runat="server" ID="EnableAuthCodeViaAuthenticator" Enabled="false" Text="Authenticator App Enabled" Tooltip="This option cannot be enabled as it requires user configuration." /> 
                        <input type="button" runat="server" value="Disable enabled authenticator" id="DisableAuthenticator"/>
                        <asp:HiddenField runat="server"  id="IsDisablingAuthenticator" />
                </div>

                <div id="_IPRestrictions" runat="server">
                    <div class="content-container margin-bottom">
                        <header class="header">Client Certificate</header>
                        <div class="profile-padding-left">
                            <ml:CommonDataGrid ID="m_clientCertificateGrid" runat="server" CssClass="DataGrid none-hover-table table-cert-ip">
                                <Columns>
                                    <asp:BoundColumn DataField="Description" HeaderText="Device Name" />
                                    <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" />
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <a onclick="return revokeClientCertificate('<%# AspxTools.HtmlString(Eval("CertificateId")) %>');">revoke</a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </ml:CommonDataGrid>
                        </div>
                    </div>

                    <div runat="server" id="IPContainer" class="content-container margin-bottom">
                        <header class="header">IP Access</header>
                        <div class="profile-padding-left">
                            <ml:CommonDataGrid ID="m_registeredIpDataGrid" runat="server" CssClass="DataGrid none-hover-table table-cert-ip">
                                <Columns>
                                    <asp:BoundColumn DataField="IpAddress" HeaderText="IP Address" />
                                    <asp:BoundColumn DataField="IsRegistered" HeaderText="Registered IP" />
                                    <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" />
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <a onclick="return deleteIp('<%# AspxTools.HtmlString(Eval("Id")) %>');">remove</a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </ml:CommonDataGrid>
                        </div>
                    </div>
                </div>
            </div>

            <div id="BaseI" class="tabDiv">
                <div class="content-container margin-bottom">
                    <header class="header">Service Credentials</header>
                    <div class="profile-padding-left">
                        <table id="ServiceCredentialsTable" class="primary-table margin-bottom">
                            <tbody></tbody>
                        </table>
                        <button id="AddServiceCredentialBtn" class="btn btn-default" type="button">Add Credential</button>
                    </div>
                </div>
            </div>

            <div class="edit-user-warning">
                <strong><ml:EncodedLabel class="ErrorMessage" ID="m_LoginErrorMessage" runat="server" EnableViewState="False"/></strong>
                <strong><ml:EncodedLabel class="ErrorMessage" ID="m_RequiredFieldMessage" runat="server" EnableViewState="False"/></strong>
                <strong><span class="ErrorMessage" runat="server" id="m_LoanViewingMessage">Warning:  This user does not currently have permission to view loans in any channel available to your company.</span></strong>
                <strong><span class="ErrorMessage" runat="server" id="m_LoanCreationMessage">Warning:  This user does not currently have permission to create loans in any channel available to your company.</span></strong>
            </div>
            <uc:ModelessDlg ID="ModelessDlg" runat="server"/>
            <input id="m_CurrentTab" type="hidden" value="TabA" name="m_CurrentTab" runat="server"/>
            <input type="hidden" id="m_Dirty" runat="server" value="0"/>
            <input id="m_IsAccountLocked" type="hidden" value="false" runat="server" name="m_IsAccountLocked"/>
        </div>
        <div class="modal-footer">
            <button id="m_ApplySeen" type="button" onclick="onClientApplyClick();" class="btn btn-flat" name="m_ApplySeen" runat="server">Apply</button>
            <button id="m_Cancel" type="button" onclick="onCancelClick();" class="btn btn-flat">Cancel</button>
            <button id="m_Okay" type="button" onclick="onClientOkClick();" class="btn btn-flat none-margin-right-btn">OK</button>
            <uc:NoDoubleClickButton id="m_Apply" runat="server" Text="Apply" OnClick="OnApplyClick" hidden/>
            <uc:NoDoubleClickButton id="m_Ok"    runat="server" Text="OK"    OnClick="OnOkClick" class="ButtonStyle" hidden/>
        </div>
    </div>
</form>
<script>
    var m_AssignedBrokerProcessorId               = <%= AspxTools.JsGetElementById(m_AssignedBrokerProcessorId                ) %>;
    var m_AssignedBrokerProcessorName             = <%= AspxTools.JsGetElementById(m_AssignedBrokerProcessorName              ) %>;
    var m_AssignedCorrExternalPostCloserId        = <%= AspxTools.JsGetElementById(m_AssignedCorrExternalPostCloserId         ) %>;
    var m_AssignedCorrExternalPostCloserName      = <%= AspxTools.JsGetElementById(m_AssignedCorrExternalPostCloserName       ) %>;
    var m_AssignedMiniCorrExternalPostCloserId    = <%= AspxTools.JsGetElementById(m_AssignedMiniCorrExternalPostCloserId     ) %>;
    var m_AssignedMiniCorrExternalPostCloserName  = <%= AspxTools.JsGetElementById(m_AssignedMiniCorrExternalPostCloserName   ) %>;
    var m_BPPickerUrl                             = <%= AspxTools.JsGetElementById(m_BPPickerUrl                              ) %>;
    var m_BrokerProcessor                         = <%= AspxTools.JsGetElementById(m_BrokerProcessor                          ) %>;
    var m_CycleExpiration                         = <%= AspxTools.JsGetElementById(m_CycleExpiration                          ) %>;
    var m_Dirty                                   = <%= AspxTools.JsGetElementById(m_Dirty                                    ) %>;
    var m_ExpirationDate                          = <%= AspxTools.JsGetElementById(m_ExpirationDate                           ) %>;
    var m_ExpirationPeriod                        = <%= AspxTools.JsGetElementById(m_ExpirationPeriod                         ) %>;
    var m_ExternalPostCloser                      = <%= AspxTools.JsGetElementById(m_ExternalPostCloser                       ) %>;
    var m_IsAccountLocked                         = <%= AspxTools.JsGetElementById(m_IsAccountLocked                          ) %>;
    var m_Login                                   = <%= AspxTools.JsGetElementById(m_Login                                    ) %>;
    var m_MustChangePasswordAtNextLogin           = <%= AspxTools.JsGetElementById(m_MustChangePasswordAtNextLogin            ) %>;
    var m_PasswordExpiresOn                       = <%= AspxTools.JsGetElementById(m_PasswordExpiresOn                        ) %>;
    var m_PCPickerUrl                             = <%= AspxTools.JsGetElementById(m_PCPickerUrl                              ) %>;
    var m_UnlockAccountPanel                      = <%= AspxTools.JsGetElementById(m_UnlockAccountPanel                       ) %>;
    var RequireCellPhone                          = <%= AspxTools.JsGetElementById(this.UserInformation.RequireCellPhone      ) %>;
    var RequiredFieldMessage                      = <%= AspxTools.JsGetElementById(UserInformation.RequiredFieldMessage       ) %>;

    var expirationPeriodDisabled                  = <%= AspxTools.JsBool(expirationPeriodDisabled                             ) %>;
    var expirationDateDisabled                    = <%= AspxTools.JsBool(expirationDateDisabled                               ) %>
    var cycleExpirationDisabled                   = <%= AspxTools.JsBool(cycleExpirationDisabled                              ) %>;
    var IsPostBack                                = <%= AspxTools.JsBool(IsPostBack                                           ) %>;
    var employeeId                                = <%= AspxTools.JsString(ViewState["employeeId"] == null ? "" : ViewState["employeeId"].ToString()) %>;
    var passwordGuidelines                        = '<%= AspxTools.HtmlString(LendersOffice.Constants.ConstApp.PasswordGuidelines) %>';
</script>
<script src="../inc/EditUser.js"></script>
</body>
</html>
