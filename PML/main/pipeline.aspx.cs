namespace PriceMyLoan.UI.Main
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Script.Serialization;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using global::DataAccess.LoanComparison;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.DU;
    using LendersOffice.LoanSearch;
    using LendersOffice.ObjLib.BackgroundJobs;
    using LendersOffice.ObjLib.PMLNavigation;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.ObjLib.TPO;
    using LendersOffice.ObjLib.TPO.PipelineReport;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOfficeApp.LegacyLink.CalyxPoint;
    using PriceMyLoan.Common;
    using PriceMyLoan.DataAccess;
    using PriceMyLoan.main;
    using PriceMyLoan.ObjLib;
    using PrincipalFactory = LendersOffice.Security.PrincipalFactory;
    using LqbGrammar.DataTypes;

    public partial class pipeline : PriceMyLoan.UI.BasePage
    {
        protected bool hasDuplicate;
        protected string importLoanChoice = "";
        protected E_PortalMode portalMode = E_PortalMode.Blank;
        protected bool modeChanged;

        private bool? hasAccess;
        protected bool HasAccess
        {
            get
            {
                return hasAccess ?? (hasAccess = this.PriceMyLoanUser.CanAccessTpoPortal()).Value;
            }
        }


        private BrokerDB x_brokerDB;
        private BrokerDB CurrentBroker
        {
            get { return x_brokerDB ?? (x_brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId)); }
        }
        private int CurrentPage
        {
            get
            {
                return modeChanged ? -1 : RequestHelper.GetInt("pg", -1); // opm 150384, on first load, load the dashboard if it exists.
            }
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            if (RequestHelper.GetSafeQueryString("fax") == "1")
            {
                PriceMyLoanRequestHelper.DownloadCoverSheetList(this.LoanID);
                return;
            }

            if (string.Equals(PriceMyLoanUser.LoginNm, ConstAppDavid.PmlMortgageLeagueLogin, StringComparison.OrdinalIgnoreCase))
            {
                // 1/21/2005 dd - Generic GML account will not be able to access pipeline
                Response.Redirect("~/MortgageLeagueLogout.aspx");
            }

            // OPM 32225
            string dupeSearch = RequestHelper.GetSafeQueryString("eventid");
            if (!string.IsNullOrEmpty(dupeSearch))
            {
                try
                {
                    SearchDataKey = new Guid(dupeSearch);
                }
                catch (FormatException)
                {
                    Tools.LogErrorWithCriticalTracking("Invalid search key:" + dupeSearch);
                }
            }

            ClientScript.GetPostBackEventReference(this, ""); // NEED TO HAVE THIS LINE in order for .NET to register __doPostBack

            m_isNewPmlUIEnabled = GetIsNewPmlUIEnabled();

            this.RegisterJsScript("genericframework.js");
            this.RegisterService("PML", "/webapp/PMLService.aspx");

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this, includeSimplePopupDependencies: false);
        }

        bool GetIsNewPmlUIEnabled()
        {
            return CurrentBroker.IsNewPmlUIEnabled ||
                EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId).IsNewPmlUIEnabled;
        }


        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_welcomeLabel.Text = $"Welcome, {PriceMyLoanUser.DisplayName}!";
            DetermineAvailableModes();
            if (HasAccess)
            {
                DeterminePortalMode();

                BindNav();

                try
                {
                    if (!Page.IsPostBack || CurrentPage == -1)
                    {
                        if (CurrentBroker.IsUseNewTaskSystem)
                        {
                            Show(CurrentPage);
                        }
                    }
                }
                catch (NonCriticalException) { }

                // 9/10/2004 dd - I need to call Response.Redirect outside of try/catch to avoid ThreadAbortException.

                if (null != Context.Items["GoToLoanID"])
                {
                    string GoToLoanID = Context.Items["GoToLoanID"] as string;
                    string redirectUrl;
                    if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
                    {
                        redirectUrl = $"../webapp/Loan1003.aspx?src=pipeline&loanid={GoToLoanID}";
                    }
                    //opm 34211 fs 10/30/09 - PML FHA TOTAL Scorecard
                    else if (CurrentBroker.IsTotalScorecardEnabled
                             && PriceMyLoanUser.HasPermission(LendersOffice.Security.Permission.CanAccessTotalScorecard)
                             && Context.Items["showImportAudit"] != null)
                    {
                        redirectUrl = $"ImportLoanDataAudit.aspx?loanid={GoToLoanID}";
                    }
                    else if (m_isNewPmlUIEnabled) // PML 2.0 skips the agents page
                    {
                        redirectUrl = $"../webapp/pml.aspx?loanid={GoToLoanID}&source=PML";
                    }
                    else
                    {
                        redirectUrl = $"agents.aspx?loanid={GoToLoanID}";
                    }

                    if (null != Context.Items["isnew"])
                    {
                        Response.Redirect(redirectUrl + "&isnew=t");
                    }
                }

                if (!CurrentBroker.IsUseNewTaskSystem)
                {
                    Show(-1);
                }

                {
                    var BrokerDB = PriceMyLoanUser.BrokerDB;
                    var canOcCreateLoansInCurrentMode = CanCreateLoansInCurrentMode(PriceMyLoanUser, portalMode);
                    var canCreateLoansInCurrentMode = canOcCreateLoansInCurrentMode && PermissionToCreateLoansInCurrentMode(PriceMyLoanUser, portalMode);

                    var supervisorDB =
                        (!canOcCreateLoansInCurrentMode ||
                            canCreateLoansInCurrentMode ||
                            PriceMyLoanUser.PmlExternalManagerEmployeeID == Guid.Empty
                        ) ? null :
                        EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.PmlExternalManagerEmployeeID);

                    Lazy<IEnumerable<ServiceCredential>> ausServiceCredentials = 
                        new Lazy<IEnumerable<ServiceCredential>>(() => ServiceCredential.ListAvailableServiceCredentials(principal: PriceMyLoanUser, branchId: Guid.Empty, enabledServices: ServiceCredentialService.AusSubmission));

                    var lpaCredentials = !BrokerDB.AllowImportFromLpaInPml ? null :
                        new
                        {
                            HasLpaSellerCredentials = ausServiceCredentials.Value?.Any(credential => credential.ChosenAusOption == AusOption.LpaSellerCredential) ?? false,
                            HasLpaUserCredentials = ausServiceCredentials.Value?.Any(credential => credential.ChosenAusOption == AusOption.LpaUserLogin) ?? false
                        };

                    RegisterJsObjectWithJsonNetAnonymousSerializer("pipelineVm", new
                    {
                        BrokerDB = new
                        {
                            BrokerDB.IsAllowImportDoDuInPml,
                            BrokerDB.IsPmlPointImportAllowed,
                            BrokerDB.IsUseTasksInTpoPortal,
                            BrokerDB.AllowImportFromLpaInPml,
                            BrokerDB.HideStatusAndAgentsPageInOriginatorPortal
                        },
                        PriceMyLoanUser = new
                        {
                            PriceMyLoanUser.IsPricingMultipleAppsSupported,
                            DoDuLoginNm = (
                                BrokerDB.IsAllowImportDoDuInPml                      ? string.Empty                  : (
                                !string.IsNullOrEmpty(PriceMyLoanUser.DoLastLoginNm) ? PriceMyLoanUser.DoLastLoginNm : (
                                !string.IsNullOrEmpty(PriceMyLoanUser.DuLastLoginNm) ? PriceMyLoanUser.DuLastLoginNm : (
                                                                                       string.Empty)))),
                            CanCreateLoan = CanCreateLoan(PriceMyLoanUser),
                            canOcCreateLoansInCurrentMode,
                            canCreateLoansInCurrentMode,
                        },
                        supervisorDB = supervisorDB == null ? null : new
                        {
                            supervisorDB.Email,
                            supervisorDB.FullName,
                        },
                        LpaCredentials = lpaCredentials
                    });
                }
            }
            else
            {
                AccessDeniedMessage.Visible = true;
                AccessDeniedMessage.InnerText = "You have not been given permission to create loan files within this portal. If this is a mistake, please contact your supervisor. Otherwise, expanded portal access will be granted to you once others have added loans into your pipeline.";
            }
        }

        void BindNav()
        {
            m_PmlNavigationManager = new PMLNavigationManager(PriceMyLoanUser, Page.ResolveClientUrl);
            HttpCookie cookie = Request.Cookies["NavState"];
            if (!string.IsNullOrEmpty(cookie?.Value) && CurrentPage != -1)
            {
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var items = new HashSet<Guid>(js.Deserialize<List<Guid>>(cookie.Value));
                    items.Remove(Guid.Empty);
                    RegisterJsObject("NavState", items);
                    Nav.ExpandedNodes = items;
                }
                catch (InvalidOperationException)
                {

                }
            }

            switch (CurrentPage)
            {
                case -1:
                    Nav.SelectedNodeID =
                        m_PmlNavigationManager.TpoConfig != null
                        && (HttpContext.Current.Request.QueryString.AllKeys.Contains("islogin")
                            || HttpContext.Current.Request.QueryString.AllKeys.Contains("isLoadingDashboard")
                            || modeChanged)
                            ? NavigationConfiguration.SystemNode.Dashboard
                            : NavigationConfiguration.SystemNode.Loans;

                    // if loans and the dashboard are not visible, default to my profile.
                    if (Nav.SelectedNodeID == NavigationConfiguration.SystemNode.Loans)
                    {
                        var loanNode = m_PmlNavigationManager.GetLoansNode();

                        if (loanNode == null || !IsNodeVisible(loanNode, portalMode, PriceMyLoanUser))
                        {
                            Nav.SelectedNodeID = NavigationConfiguration.SystemNode.MyProfile;
                            var script = $"<script>function startupRedirectFunction() {{ $('#{NavigationConfiguration.SystemNode.MyProfile}').click(); }} window.onload = startupRedirectFunction;</script>";
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "startupRedirectScript", script);
                        }
                    }

                    break;
                case 0:
                    Nav.SelectedNodeID = NavigationConfiguration.SystemNode.Loans;
                    break;
                case 1:
                    Nav.SelectedNodeID = NavigationConfiguration.SystemNode.Tasks;
                    break;
                case 2:
                    Nav.SelectedNodeID = NavigationConfiguration.SystemNode.ConditionLink;
                    break;
                case 5:
                    Nav.SelectedNodeID = NavigationConfiguration.SystemNode.Leads;
                    break;
                case 6:
                    Nav.SelectedNodeID = RequestHelper.GetGuid("reportId", Guid.Empty);
                    this.Form.Attributes.Add("class", this.Form.Attributes["class"] + " custom-report-view");
                    break;
            }

            Nav.FrameName = "MainPipelineFrame";
            Nav.FolderId = Guid.Empty;

            var m_dataSource = m_PmlNavigationManager.Nodes();
            var toBeRemoved = new List<DynaTreeNode>();
            DynaTreeNode portalNode = null;
            foreach (var node in m_dataSource)
            {
                RemoveInaccessibleNodes(node, portalMode, PriceMyLoanUser);

                var isNodeVisible = (
                    IsNodeVisible(node, portalMode, PriceMyLoanUser) ||
                    NavigationConfiguration.SystemNode.BelongsToSystemNodeThatCantBeHidden(node.Id) ||
                    node.isFolder
                );

                if (!isNodeVisible)
                {
                    toBeRemoved.Add(node);
                }

                if (node.children?.Count <= 0)
                {
                    toBeRemoved.Add(node);
                }

                if (node.Id == NavigationConfiguration.SystemNode.PortalMode)
                {
                    if (this.portalMode == E_PortalMode.Retail)
                    {
                        // Hide the "Portal" label as it isn't displayed in the retail portal pipeline.
                        toBeRemoved.Add(node);
                    }
                    else
                    {
                        portalNode = node;
                    }
                }

                if (node.Id == NavigationConfiguration.SystemNode.CreateLeadFolder)
                {
                    BrokerUserPermissions buP = new BrokerUserPermissions(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
                    if (this.portalMode != E_PortalMode.Retail || !buP.HasPermission(Permission.AllowCreatingNewLeadFiles))
                    {
                        toBeRemoved.Add(node);
                    }
                }

                if (node.Id == NavigationConfiguration.SystemNode.CreateLoanFolder)
                {
                    bool hideCreateLoan;
                    BrokerUserPermissions buP = new BrokerUserPermissions(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);

                    if (this.portalMode == E_PortalMode.Retail)
                    {
                        hideCreateLoan = !buP.HasPermission(Permission.AllowCreatingNewLoanFiles);
                    }
                    else
                    {
                        var pB = PmlBroker.RetrievePmlBrokerById(PriceMyLoanUser.PmlBrokerId, PriceMyLoanUser.BrokerId);
                        hideCreateLoan =
                            (portalMode.Equals(E_PortalMode.Broker) && (!buP.HasPermission(Permission.AllowCreatingWholesaleChannelLoans) || !pB.Roles.Contains(E_OCRoles.Broker))) ||
                            (portalMode.Equals(E_PortalMode.MiniCorrespondent) && (!buP.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans) || !pB.Roles.Contains(E_OCRoles.MiniCorrespondent))) ||
                            (portalMode.Equals(E_PortalMode.Correspondent) && (!buP.HasPermission(Permission.AllowCreatingCorrChannelLoans) || !pB.Roles.Contains(E_OCRoles.Correspondent)));
                    }

                    if (hideCreateLoan)
                    {
                        toBeRemoved.Add(node);
                    }
                }

                if (node.Id == NavigationConfiguration.SystemNode.QuickpricerFolder)
                {
                    if (!IsQuickPricerFolderVisible(portalMode, PriceMyLoanUser, CanCreateLoan(PriceMyLoanUser)))
                    {
                        toBeRemoved.Add(node);
                    }
                }
            }

            if (portalNode != null)
            {
                var list = (List<DynaTreeNode>)m_dataSource;
                list.Remove(portalNode);
                list.Insert(0, portalNode);
            }

            // Append the user's pipeline reports to the end of the Pipelines folder.
            if(PrincipalFactory.CurrentPrincipal.PortalMode == E_PortalMode.Retail || OriginatingCompanyUserPipelineSettings.RetrieveNumAvailableReports(BrokerID, portalMode) > 0)
            {
                var pipelineFolderNode = m_dataSource.First(node => node.Id == NavigationConfiguration.SystemNode.PipelineFolder);
                pipelineFolderNode.children.AddRange(this.GetDynaTreeReportNodes());
            }

            Nav.DataSource = m_dataSource.Where(p => !toBeRemoved.Contains(p));
            Nav.DataBind();
        }

        private static bool IsQuickPricerFolderVisible(E_PortalMode portalMode, AbstractUserPrincipal PriceMyLoanUser, bool canCreateLoan)
        {
            if (portalMode == E_PortalMode.Retail)
            {
                var hasCreateLeadLoanPermission = PriceMyLoanUser.HasPermission(Permission.AllowCreatingNewLeadFiles) ||
                                                  PriceMyLoanUser.HasPermission(Permission.AllowCreatingNewLoanFiles);

                return canCreateLoan &&
                       PriceMyLoanUser.IsQuickPricerEnable &&
                       hasCreateLeadLoanPermission;
            }

            PmlBroker pB = PmlBroker.RetrievePmlBrokerById(PriceMyLoanUser.PmlBrokerId, PriceMyLoanUser.BrokerId);

            bool UserHasBrokerRoles = PriceMyLoanUser.HasAtLeastOneRole(new[] { E_RoleT.Pml_LoanOfficer, E_RoleT.Pml_BrokerProcessor });
            bool UserHasCorrespondentRoles = PriceMyLoanUser.Roles.Contains(Role.Get(E_RoleT.Pml_Secondary).Desc);

            return
                (portalMode == E_PortalMode.Broker            && (UserHasBrokerRoles        && pB.Roles.Contains(E_OCRoles.Broker           ))) ||
                (portalMode == E_PortalMode.MiniCorrespondent && (UserHasCorrespondentRoles && pB.Roles.Contains(E_OCRoles.MiniCorrespondent))) ||
                (portalMode == E_PortalMode.Correspondent     && (UserHasCorrespondentRoles && pB.Roles.Contains(E_OCRoles.Correspondent    )));
        }

        /// <summary>
        /// Determines whether a node is visible based on portal mode and user role.
        /// </summary>
        /// <param name="node">
        /// The node to check.
        /// </param>
        /// <param name="portalMode">
        /// The current portalMode.
        /// </param>
        /// <param name="PriceMyLoanUser">
        /// The current PML Principal.
        /// </param>
        /// <returns>
        /// True if the node is visible, false otherwise.
        /// </returns>
        private static bool IsNodeVisible(DynaTreeNode node, E_PortalMode portalMode, AbstractUserPrincipal PriceMyLoanUser)
        {
            var isAllowedByPortalMode = ((node.AllowBrokerMode   && portalMode == E_PortalMode.Broker           ) ||
                                         (node.AllowMiniCorrMode && portalMode == E_PortalMode.MiniCorrespondent) ||
                                         (node.AllowCorrMode     && portalMode == E_PortalMode.Correspondent    ) ||
                                         (node.AllowRetailMode   && portalMode == E_PortalMode.Retail           )
                                         );

            var isAllowedByRole = node.AllowAny ||
                                  (node.AllowLoanOfficer && PriceMyLoanUser.HasAtLeastOneRole(new[] { E_RoleT.LoanOfficer  , E_RoleT.Pml_LoanOfficer     })) ||
                                  (node.AllowProcessor   && PriceMyLoanUser.HasAtLeastOneRole(new[] { E_RoleT.Processor    , E_RoleT.Pml_BrokerProcessor })) ||
                                  (node.AllowSupervisor  && PriceMyLoanUser.HasAtLeastOneRole(new[] { E_RoleT.Administrator, E_RoleT.Pml_Administrator   })) ||
                                  (node.AllowSecondary   && PriceMyLoanUser.HasAtLeastOneRole(new[] { E_RoleT.Secondary    , E_RoleT.Pml_Secondary       })) ||
                                  (node.AllowPostCloser  && PriceMyLoanUser.HasAtLeastOneRole(new[] { E_RoleT.PostCloser   , E_RoleT.Pml_PostCloser      })) ||
                                  (node.AllowAccountExecutive && PriceMyLoanUser.HasAtLeastOneRole(new[] { E_RoleT.LenderAccountExecutive }));

            return isAllowedByPortalMode && isAllowedByRole;
        }

        private IEnumerable<DynaTreeNode> GetDynaTreeReportNodes()
        {
            const string PipelineReportUrl = "~/main/pipeline.aspx?pg=6&reportId=";

            IEnumerable<DynaTreeNode> nodeList;
            if (string.Equals("B", this.PriceMyLoanUser.Type, StringComparison.OrdinalIgnoreCase))
            {
                var pipelineView = Tools.GetMainTabsSettingXmlContent(this.PriceMyLoanUser.ConnectionInfo, this.PriceMyLoanUser.UserId);
                nodeList = pipelineView.Tabs.Where(tab => tab.ReportId != Guid.Empty).Select(tab => new DynaTreeNode()
                {
                    isFolder = false,
                    Id = tab.ReportId,
                    ExternalName = tab.TabName,
                    ConfigHref = this.Page.ResolveClientUrl(PipelineReportUrl + tab.ReportId),
                    ConfigTarget = E_NavTarget.Self,
                    addClass = "pipeline-report-link",
                    AdditionalData = tab.ReportId.ToString() 
                });
            }
            else
            {
                var pipelineSettings = new TpoUserPipelineSettings(this.PriceMyLoanUser);
                var selectedReports = pipelineSettings.Retrieve();

                nodeList = selectedReports.Select(report => new DynaTreeNode()
                {
                    isFolder = false,
                    Id = report.QueryId,
                    ExternalName = report.Name,
                    ConfigHref = this.Page.ResolveClientUrl(PipelineReportUrl + report.QueryId),
                    ConfigTarget = E_NavTarget.Self,
                    addClass = "pipeline-report-link",
                    AdditionalData = report.QueryId.ToString()
                });
            }

            var configureReportsNode = new DynaTreeNode()
            {
                isFolder = false,
                ExternalName = $"<i class='material-icons'>settings</i> Configure Pipelines",
                addClass = "pipeline-report-picker",
                AllowSafeHtml = true
            };

            if (nodeList.Any())
            {
                var dividerNode = new DynaTreeNode()
                {
                    isFolder = false,
                    ExternalName = "<hr />",
                    addClass = "pipeline-report-divider",
                    AllowSafeHtml = true
                };

                return new[] { dividerNode }.Concat(nodeList).Concat(new[] { configureReportsNode });
            }

            return new[] { configureReportsNode };
        }

        private void Show(int page)
        {
            //If a file is being

            if (page == -1 &&
                (modeChanged || (string.IsNullOrEmpty(Request.Form["LoanChoice"]) || Request.Form["LoanChoice"] == "none")) &&
                (m_PmlNavigationManager.TpoConfig != null
                 && (HttpContext.Current.Request.QueryString.AllKeys.Contains("islogin")
                     || HttpContext.Current.Request.QueryString.AllKeys.Contains("isLoadingDashboard")
                     || modeChanged)
                ))
            {
                RegisterJsStruct("isLoadingDashboard", true);
                RegisterJsObjectWithJsonNetAnonymousSerializer("dasboardUrl", m_PmlNavigationManager.TpoConfig.Url);
            }
        }

        protected void DeterminePortalMode()
        {
            if (string.Equals("B", this.PriceMyLoanUser.Type, StringComparison.OrdinalIgnoreCase))
            {
                if (this.PriceMyLoanUser.PortalMode != E_PortalMode.Retail)
                {
                    portalMode = E_PortalMode.Retail;
                    PriceMyLoanUser.PortalMode = portalMode;
                    EmployeeDB empDb = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
                    empDb.PortalMode = portalMode;
                    // Do not log to security event logs when you change portal mode.
                    empDb.Save();
                }
            }
            else
            {
                bool resetPortalMode = TPOMode.Items.Cast<ListItem>()
                    .All(item => item.Value != ("" + (int)PriceMyLoanUser.PortalMode));

                if (resetPortalMode)
                {
                    PriceMyLoanUser.PortalMode = E_PortalMode.Blank;
                    portalMode = (E_PortalMode)int.Parse(TPOMode.Items[0].Value);
                    EmployeeDB empDb = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
                    empDb.PortalMode = portalMode;
                    // Do not log to security event logs when you change portal mode.
                    empDb.Save();
                }
            }

            if (!string.IsNullOrEmpty(Page.Request.Form["__EVENTTARGET"]) && Page.Request.Form["__EVENTTARGET"].Equals("ModeChange"))
            {
                portalMode = (E_PortalMode)int.Parse(Request.Form["__EVENTARGUMENT"]);

                EmployeeDB empDb = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
                empDb.PortalMode = portalMode;
                // Do not log to security event logs when you change portal mode.
                empDb.Save();

                PriceMyLoanUser.PortalMode = portalMode;
                modeChanged = true;
            }
            else
            {
                if (PriceMyLoanUser.PortalMode == E_PortalMode.Blank)
                {
                    portalMode = (E_PortalMode)int.Parse(TPOMode.SelectedItem.Value);
                    EmployeeDB empDb = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
                    empDb.PortalMode = portalMode;
                    // Do not log to security event logs when you change portal mode.
                    empDb.Save();

                    PriceMyLoanUser.PortalMode = portalMode;
                }
                else
                {
                    portalMode = PriceMyLoanUser.PortalMode;
                }
            }

            TPOMode.SelectedValue = "" + (int)PriceMyLoanUser.PortalMode;
        }

        public void DetermineAvailableModes()
        {
            if (string.Equals("B", this.PriceMyLoanUser.Type, StringComparison.OrdinalIgnoreCase))
            {
                portalMode = E_PortalMode.Retail;

                this.TPOMode.Items.Clear();
                this.TPOMode.Items.Add(new ListItem("Retail", E_PortalMode.Retail.ToString("D")));
                this.TPOMode.Attributes["hidden"] = "hidden";

                return;
            }

            foreach (E_PortalMode mode in Enum.GetValues(typeof(E_PortalMode)))
            {
                TPOMode.Items.Remove(TPOMode.Items.FindByValue(mode.ToString("D")));
            }

            TPOMode.Items.AddRange(new[]
            {
                this.PriceMyLoanUser.HasPermission(Permission.AllowViewingWholesaleChannelLoans) ? new ListItem("Broker"            , E_PortalMode.Broker           .ToString("D")) : null,
                this.PriceMyLoanUser.HasPermission(Permission.AllowViewingMiniCorrChannelLoans ) ? new ListItem("Mini-Correspondent", E_PortalMode.MiniCorrespondent.ToString("D")) : null,
                this.PriceMyLoanUser.HasPermission(Permission.AllowViewingCorrChannelLoans     ) ? new ListItem("Correspondent"     , E_PortalMode.Correspondent    .ToString("D")) : null,
            }.Where(i => i != null).ToArray());

            if (TPOMode.Items.Count > 0)
            {
                string selectedPortalValue = TPOMode.Items[TPOMode.Items.Count - 1].Value;
                portalMode = (E_PortalMode)int.Parse(selectedPortalValue);
                TPOMode.SelectedValue = selectedPortalValue;
            }

            if (TPOMode.Items.Count < 2)
            {
                TPOMode.Attributes.Add("readonly", "true");
            }
        }

        protected bool m_isNewPmlUIEnabled;

        //private bool m_hasAtLeastOneRerunLink = false;

        public Guid SearchDataKey { get; set; }

        public Guid BrokerID
        {
            get
            {
                return null == Context ? Guid.Empty : PriceMyLoanUser.BrokerId;
            }
        }
        public Guid EmployeeID
        {
            get
            {
                return null == Context ? Guid.Empty : PriceMyLoanUser.EmployeeId;
            }
        }
        public Guid UserID
        {
            get
            {
                return null == Context ? Guid.Empty : PriceMyLoanUser.UserId;
            }
        }

        static string[] StreamToStrings(Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                var list = new List<string>();
                string line;
                while ((line = reader.ReadLine()) != null)
                    list.Add(line);

                return list.ToArray();
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        private PMLNavigationManager m_PmlNavigationManager;

        static DynaTreeNode RemoveInaccessibleNodes(DynaTreeNode node, E_PortalMode portalMode, AbstractUserPrincipal PriceMyLoanUser)
        {
            if (node.Id == NavigationConfiguration.SystemNode.PipelineFolder)
            {
                switch (portalMode)
                {
                    case E_PortalMode.Broker:
                        node.ExternalName = "Broker Pipelines";
                        break;
                    case E_PortalMode.MiniCorrespondent:
                        node.ExternalName = "Mini-Correspondent Pipelines";
                        break;
                    case E_PortalMode.Correspondent:
                        node.ExternalName = "Correspondent Pipelines";
                        break;
                }
            }

            if (node.children != null)
            {
                node.children = node.children
                    .Where(child => ChildNodeIsAccessible(child, portalMode, PriceMyLoanUser))
                    .Select(child => RemoveInaccessibleNodes(child, portalMode, PriceMyLoanUser))
                    .Where(child => !child.isFolder || child.children.Count > 0)
                    .ToList();
            }
            return node;
        }

        /// <summary>
        /// Gets a value indicating whether a child node is accessible.
        /// </summary>
        /// <param name="child">
        /// The child node.
        /// </param>
        /// <param name="portalMode">
        /// Current portal mode.
        /// </param>
        /// /// <param name="PriceMyLoanUser">
        /// The User Principal.
        /// </param>
        /// <returns>
        /// True if the child node is accessible, false otherwise.
        /// </returns>
        static bool ChildNodeIsAccessible(DynaTreeNode child, E_PortalMode portalMode, AbstractUserPrincipal PriceMyLoanUser)
        {
            var allowTaskNode = child.Id != NavigationConfiguration.SystemNode.Tasks || PriceMyLoanUser.BrokerDB.IsShowPipelineOfTasksForAllLoansInTpoPortal;
            var allowConditionNode = child.Id != NavigationConfiguration.SystemNode.ConditionLink || PriceMyLoanUser.BrokerDB.IsShowPipelineOfConditionsForAllLoansInTpoPortal;

            if (!allowTaskNode || !allowConditionNode)
            {
                return false;
            }

            if (child.isFolder || NavigationConfiguration.SystemNode.BelongsToSystemNodeThatCantBeHidden(child.Id))
            {
                return true;
            }

            return IsNodeVisible(child, portalMode, PriceMyLoanUser);
        }

        static bool CanCreateLoansInCurrentMode(AbstractUserPrincipal PriceMyLoanUser, E_PortalMode portalMode)
        {
            if (portalMode == E_PortalMode.Retail)
            {
                // Retail users aren't dependent on an OC's roles to create loans.
                return true;
            }

            var pB = PmlBroker.RetrievePmlBrokerById(PriceMyLoanUser.PmlBrokerId, PriceMyLoanUser.BrokerId);
            switch (portalMode)
            {
                case E_PortalMode.Broker           : return pB.Roles.Contains(E_OCRoles.Broker);
                case E_PortalMode.MiniCorrespondent: return pB.Roles.Contains(E_OCRoles.MiniCorrespondent);
                case E_PortalMode.Correspondent    : return pB.Roles.Contains(E_OCRoles.Correspondent);
                default: throw new UnhandledEnumException(portalMode);
            }
        }

        static bool PermissionToCreateLoansInCurrentMode(AbstractUserPrincipal PriceMyLoanUser, E_PortalMode portalMode)
        {
            var buP = new BrokerUserPermissions(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);

            switch (portalMode)
            {
                case E_PortalMode.Blank            :
                case E_PortalMode.Retail           : return buP.HasPermission(Permission.AllowCreatingNewLoanFiles);
                case E_PortalMode.Broker           : return buP.HasPermission(Permission.AllowCreatingWholesaleChannelLoans);
                case E_PortalMode.MiniCorrespondent: return buP.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans);
                case E_PortalMode.Correspondent    : return buP.HasPermission(Permission.AllowCreatingCorrChannelLoans);
                default: throw new UnhandledEnumException(portalMode);
            }
        }

        [WebMethod]
        public static object ListLoanDefault(int count, int? topRowsToPull, E_sCorrespondentProcessT process, E_PortalMode PortalMode, int LoanAssignedToT, bool IsLeadPipeline)
        {
            var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;

            var searchFilter = GetSearchFilter(PortalMode, process, PriceMyLoanUser.BrokerId, LoanAssignedToT, IsLeadPipeline);
            return ListLoanBase(count, topRowsToPull, PortalMode, searchFilter, PriceMyLoanUser, IsLeadPipeline);
        }

        [WebMethod]
        public static object ListLoanSearch(E_sCorrespondentProcessT process, E_PortalMode PortalMode,
            string aBLastNm, string aBSsn, string sLNm, int sStatusT, int sStatusDateT, int LoanAssignedToT, bool IsLeadPipeline)
        {
            var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;

            var searchFilter = GetSearchFilterForSearch(PortalMode, process, PriceMyLoanUser.BrokerId,
                aBLastNm, aBSsn, sLNm, sStatusT, sStatusDateT, LoanAssignedToT, IsLeadPipeline
            );
            return ListLoanBase(100, null, PortalMode, searchFilter, PriceMyLoanUser, IsLeadPipeline);
        }

        static LendingQBSearchFilter GetSearchFilterBase(E_PortalMode PortalMode, E_sCorrespondentProcessT process, Guid BrokerId, bool IsLeadPipeline)
        {
            var searchFilter = new LendingQBSearchFilter { PortalMode = PortalMode };

            if (IsLeadPipeline)
            {
                searchFilter.sStatusTFilters = new HashSet<E_sStatusT>()
                {
                    E_sStatusT.Lead_New,
                    E_sStatusT.Lead_Canceled,
                    E_sStatusT.Lead_Declined,
                    E_sStatusT.Lead_Other
                };

                searchFilter.sCorrespondentProcess = process;
            }
            else if (PortalMode == E_PortalMode.Correspondent && process == E_sCorrespondentProcessT.Blank)
            {
                searchFilter.sStatusTDictionary = new Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>>
                {
                    { E_sCorrespondentProcessT.Blank        , new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(BrokerId, PortalMode, E_sCorrespondentProcessT.Blank))},
                    { E_sCorrespondentProcessT.Delegated    , new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(BrokerId, PortalMode, E_sCorrespondentProcessT.Delegated))},
                    { E_sCorrespondentProcessT.PriorApproved, new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(BrokerId, PortalMode, E_sCorrespondentProcessT.PriorApproved))}
                };
                searchFilter.sCorrespondentProcess = null;
            }
            else
            {
                searchFilter.sStatusTFilters = new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(BrokerId, PortalMode, process));
                searchFilter.sCorrespondentProcess = process;
            }

            return searchFilter;
        }
        static LendingQBSearchFilter GetSearchFilter(E_PortalMode PortalMode, E_sCorrespondentProcessT process, Guid BrokerId, int LoanAssignedToT, bool IsLeadPipeline)
        {
            var searchFilter = GetSearchFilterBase(PortalMode, process, BrokerId, IsLeadPipeline);
            searchFilter.LoanAssignedToT = 0;
            searchFilter.sStatusT = -1;
            searchFilter.sStatusDateT = -1;
            searchFilter.LoanAssignedToT = LoanAssignedToT;

            return searchFilter;
        }
        static LendingQBSearchFilter GetSearchFilterForSearch(E_PortalMode PortalMode, E_sCorrespondentProcessT process, Guid BrokerId,
            string aBLastNm, string aBSsn, string sLNm, int sStatusT, int sStatusDateT, int LoanAssignedToT, bool IsLeadPipeline)
        {
            var searchFilter = GetSearchFilterBase(PortalMode, process, BrokerId, IsLeadPipeline);
            searchFilter.aBLastNm = aBLastNm;
            searchFilter.SsnLastFour = aBSsn; // UI only takes in last 4.
            searchFilter.sLNm = sLNm;
            searchFilter.sStatusT = sStatusT;
            searchFilter.sStatusDateT = sStatusDateT;
            searchFilter.LoanAssignedToT = LoanAssignedToT;
            return searchFilter;
        }
        static object ListLoanBase(int count, int? topRowsToPull, E_PortalMode PortalMode, LendingQBSearchFilter searchFilter, AbstractUserPrincipal PriceMyLoanUser, bool IsLeadPipeline)
        {
            using (var ds = LendingQBPmlSearch.Search(PriceMyLoanUser, searchFilter, count, topRowsToPull))
            {
                return GetResponseResult(ds, PortalMode, IsLeadPipeline);
            }
        }
        static object GetResponseResult(DataSet ds, E_PortalMode PortalMode, bool IsLeadPipeline, bool isDuplicateResult = false)
        {
            var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;
            var CurrentBroker = PriceMyLoanUser.BrokerDB;

            var loans = DataTableToLoans(ds.Tables[0]);

            //(Page as BasePage).RegisterJsObjectWithJsonNetAnonymousSerializer("PipelineLoansVm",
            return (
                new
                {
                    loans,
                    IsUseNewTaskSystem = CurrentBroker.IsUseNewTaskSystem,
                    IsUseTasksInTpoPortal = CurrentBroker.IsUseTasksInTpoPortal,
                    CurrentBroker.HideStatusAndAgentsPageInOriginatorPortal,
                    isDuplicateResult,
                    PortalMode,
                    //GenericFrameworkVendors = GenericFrameworkVendor.LoadVendors(PrincipalFactory.CurrentPrincipal.BrokerId)
                    //    .Where(vendor => vendor.LaunchLinkConfig.IsDisplayLinkForUser(LinkLocation.TPOPipeline))
                    //    .ToArray(),
                    options = new
                    {
                        sStatusD = new[]
                        {
                            new { label = "All Dates"    , value = -1 },
                            new { label = "Last 7 days"  , value =  1 },
                            new { label = "Last month"   , value =  2 },
                            new { label = "Last 2 months", value =  3 },
                            new { label = "Last 3 months", value =  4 },
                        },
                        UnderwritingType = new[] {
                            new { label = "<--Any-->"     , value = E_sCorrespondentProcessT.Blank         },
                            new { label = "Prior Approved", value = E_sCorrespondentProcessT.PriorApproved },
                            new { label = "Delegated"     , value = E_sCorrespondentProcessT.Delegated     },
                        },
                        sStatusT = new[] {
                            new { label = "<--Any-->"     , value = -1                                     },
                        }.Concat(
                            Tools.sStatusTs_ForDDL_PmlSearchCriteria(PriceMyLoanUser.BrokerId, PortalMode, E_sCorrespondentProcessT.Blank, IsLeadPipeline)
                                .Select(sStatusT => new { label = CPageBase.sStatusT_map_rep(sStatusT), value = (int)sStatusT })
                        ).ToArray(),
                    },
                });
        }
        public static IEnumerable<Dictionary<string, object>> ListLoansWithDuplicateCheck(string aBLastNm, string aBFirstNm, string aBSsn, E_sCorrespondentProcessT process, E_PortalMode PortalMode)
        {
            var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;
            var brokerId = PriceMyLoanUser.BrokerId;

            Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>> sStatusTDictionary = null;
            HashSet<E_sStatusT> sStatusTFilters = null;
            if (PortalMode == E_PortalMode.Correspondent && process == E_sCorrespondentProcessT.Blank)
            {
                sStatusTDictionary = new Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>>
                    {
                        { E_sCorrespondentProcessT.Blank        , new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(brokerId, PortalMode, E_sCorrespondentProcessT.Blank        )) },
                        { E_sCorrespondentProcessT.Delegated    , new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(brokerId, PortalMode, E_sCorrespondentProcessT.Delegated    )) },
                        { E_sCorrespondentProcessT.PriorApproved, new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(brokerId, PortalMode, E_sCorrespondentProcessT.PriorApproved)) },
                    };
            }
            else
            {
                sStatusTFilters = new HashSet<E_sStatusT>(Tools.sStatusTs_For_PmlSearchCriteria(brokerId, PortalMode, process, PortalMode == E_PortalMode.Retail));
            }

            return DuplicateFinder.FindDuplicateLoansForPmlPipeline(
                PriceMyLoanUser,
                aBFirstNm.TrimWhitespaceAndBOM(),
                aBLastNm.TrimWhitespaceAndBOM(),
                aBSsn.TrimWhitespaceAndBOM(),
                PortalMode,
                sStatusTDictionary,
                sStatusTFilters);
        }

        static bool IsStatusTApprovedNotCanceled(E_sStatusT sStatusT)
        {
            return (
                CPageBase.s_OrderByStatusT[E_sStatusT.Loan_Approved] <=
                CPageBase.s_OrderByStatusT[sStatusT] && CPageBase.s_OrderByStatusT[sStatusT]
                < CPageBase.s_OrderByStatusT[E_sStatusT.Loan_Canceled]
            );
        }

        public static Dictionary<string, object>[] DataTableToLoans(DataTable table)
        {
            return FormatLoanData(DataTableToArray(table));
        }

        public static Dictionary<string, object>[] FormatLoanData(IEnumerable<Dictionary<string, object>> data)
        {
            return data.Select(loan =>
            {
                var sStatusT = (E_sStatusT)loan["sStatusT"];
                loan["IsStatusTApprovedNotCanceled"] = IsStatusTApprovedNotCanceled(sStatusT);
                return loan;
            }).ToArray();
        }

        public static Dictionary<string, object>[] DataTableToArray(DataTable table)
        {
            return table.Rows.Cast<DataRow>().Select(row => table.Columns.Cast<DataColumn>()
                .ToDictionary(col => col.ColumnName, col => row[col.ColumnName])).ToArray();
        }



        // return tuple [ error, sLId ]: [ string | Loan[], Guid ]
        [WebMethod]
        public static object[] CreateNewLoan(
            string aBFirstNm, string aBLastNm, string aBSsn,
            string loanType,
            E_PortalMode portalMode,
            bool skipDupCheck = false,
            Guid? borrowerCacheId = null
        )
        {
            try
            {
                return Tools.LogAndThrowIfErrors<object[]>(() =>
                {
                    E_sCorrespondentProcessT sCorrespondentProcessT = E_sCorrespondentProcessT.Blank;

                    bool isSecondLien, isPurchase, isRefinance, isHeloc1stLien, isHeloc2ndLien, isLead;
                    switch (loanType)
                    {
                        case "PURCHASE": isSecondLien = false; isPurchase = true; isRefinance = false; isHeloc1stLien = false; isHeloc2ndLien = false; isLead = false; break;
                        case "REFINANCE": isSecondLien = false; isPurchase = false; isRefinance = true; isHeloc1stLien = false; isHeloc2ndLien = false; isLead = false; break;
                        case "HELOC_1STLIEN": isSecondLien = false; isPurchase = false; isRefinance = false; isHeloc1stLien = true; isHeloc2ndLien = false; isLead = false; break;
                        case "HELOC_2NDLIEN": isSecondLien = false; isPurchase = false; isRefinance = true; isHeloc1stLien = false; isHeloc2ndLien = true; isLead = false; break;
                        case "SECONDLIEN": isSecondLien = true; isPurchase = false; isRefinance = false; isHeloc1stLien = false; isHeloc2ndLien = false; isLead = false; break;

                        // OPM 468394 - Create Leads from Retail Portal.
                        case "PURCHASE_LEAD": isSecondLien = false; isPurchase = true; isRefinance = false; isHeloc1stLien = false; isHeloc2ndLien = false; isLead = true; break;
                        case "REFINANCE_LEAD": isSecondLien = false; isPurchase = false; isRefinance = true; isHeloc1stLien = false; isHeloc2ndLien = false; isLead = true; break;
                        case "HELOC_1STLIEN_LEAD": isSecondLien = false; isPurchase = false; isRefinance = false; isHeloc1stLien = true; isHeloc2ndLien = false; isLead = true; break;
                        case "HELOC_2NDLIEN_LEAD": isSecondLien = false; isPurchase = false; isRefinance = true; isHeloc1stLien = false; isHeloc2ndLien = true; isLead = true; break;
                        case "SECONDLIEN_LEAD": isSecondLien = true; isPurchase = false; isRefinance = false; isHeloc1stLien = false; isHeloc2ndLien = false; isLead = true; break;

                        default: isSecondLien = false; isPurchase = false; isRefinance = false; isHeloc1stLien = false; isHeloc2ndLien = false; isLead = false; break;
                    }

                    var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;

                    var canCreateLoan = CanCreateLoan(PriceMyLoanUser);
                    if (!canCreateLoan) return new[] { "You do not have permission to create a new file.", null };

                    if (!skipDupCheck)
                    {
                        var duplicatedLoans = SearchDuplicatedLoans(aBFirstNm, aBLastNm, aBSsn, portalMode, sCorrespondentProcessT);
                        if (duplicatedLoans.Length > 0) return new[] { duplicatedLoans, null };
                    }

                    var sLId = CreateNewLoan(aBFirstNm, aBLastNm, aBSsn,
                        isSecondLien, isPurchase, isRefinance, isHeloc1stLien, isHeloc2ndLien, isLead,
                        portalMode,
                        !borrowerCacheId.HasValue ? null : GetLoanBorrowerData(borrowerCacheId.Value));
                    TaskUtilities.EnqueueTasksDueDateUpdate(PriceMyLoanUser.BrokerId, sLId);
                    return new[] { null, (object)sLId };
                });
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return new[] { "Unable to create new loan. Please try again.", null };
            }
        }

        static Guid CreateNewLoan(
            string aBFirstNm, string aBLastNm, string aBSsn,
            bool isSecondLien, bool isPurchase, bool isRefinance,
            bool isHeloc1stLien, bool isHeloc2ndLien, bool isLead,
            E_PortalMode portalMode, LoanBorrowerData cachedBorrowerData)
        {
            var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;
            var CurrentBroker = PriceMyLoanUser.BrokerDB;
            var pmlTemplateId = isLead &&CurrentBroker.CreateLeadFilesFromQuickpricerTemplate ? CurrentBroker.QuickPricerTemplateId : CurrentBroker.GetPmlTemplateIdByPortalMode(portalMode);

            var source = pmlTemplateId == Guid.Empty ? LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank :
                LendersOffice.Audit.E_LoanCreationSource.FromTemplate;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(PriceMyLoanUser, source);
            Guid loanId = creator.BeginCreateImportBaseLoanFile(
                sourceFileId: pmlTemplateId,
                setInitialEmployeeRoles: true,
                addEmployeeOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                branchIdToUse: Guid.Empty,
                isLead: isLead);

            creator.CommitFileCreation(false);

            CPageData dataLoan;
            if (cachedBorrowerData != null &&
                (aBFirstNm == cachedBorrowerData.BorrowerFirstName
                 && aBLastNm == cachedBorrowerData.BorrowerLastName
                 && aBSsn == cachedBorrowerData.BorrowerSsn))
            {
                dataLoan = new CQuickPricerLoanData(loanId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                dataLoan.sQuickPricerLoanItem = cachedBorrowerData.LoanData;
            }
            else
            {
                dataLoan = new CPmlNewLoanData(loanId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            }

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = aBFirstNm;
            dataApp.aBLastNm = aBLastNm;
            dataApp.aBSsn = aBSsn;

            // OPM 16939
            if (isSecondLien)
            {
                dataLoan.sLienPosT = E_sLienPosT.Second;
            }

            // OPM 106694
            if (isPurchase)
            {
                dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            }
            if (isRefinance)
            {
                dataLoan.sLPurposeT = E_sLPurposeT.Refin;
            }

            if (isHeloc1stLien)
            {
                dataLoan.sIsLineOfCredit = true;

                dataLoan.sLPurposeT = CurrentBroker.IsEnableHomeEquityInLoanPurpose ? E_sLPurposeT.HomeEquity : E_sLPurposeT.RefinCashout;

                dataLoan.sLienPosT = E_sLienPosT.First;
            }

            if (isHeloc2ndLien)
            {
                dataLoan.sIsLineOfCredit = true;

                dataLoan.sLPurposeT = CurrentBroker.IsEnableHomeEquityInLoanPurpose ? E_sLPurposeT.HomeEquity : E_sLPurposeT.RefinCashout;

                dataLoan.sLienPosT = E_sLienPosT.Second;
                dataLoan.sIsOFinNew = false;
            }

            dataLoan.Save();

            return loanId;
        }

        private static bool CanCreateLoan(AbstractUserPrincipal user)
        {
            var brokerDB = user.BrokerDB;

            var canCreateTpoLoan = string.Equals("B", user.Type, StringComparison.OrdinalIgnoreCase) ||
                                   brokerDB.IsAllowExternalUserToCreateNewLoan;

            var userCanCreateLoans = user.HasAtLeastOneRole(new[]
            {
                E_RoleT.LoanOfficer,
                E_RoleT.Processor,
                E_RoleT.Secondary,
                E_RoleT.PostCloser,
                E_RoleT.Pml_LoanOfficer,
                E_RoleT.Pml_BrokerProcessor,
                E_RoleT.Pml_Secondary,
                E_RoleT.Pml_PostCloser,
                E_RoleT.LenderAccountExecutive
            });

            return canCreateTpoLoan && userCanCreateLoans;
        }


        [WebMethod]
        public static object[] ImportFannieMae(string fnmaContent, E_PortalMode portalMode, bool skipDupCheck, bool isLead)
        {
            try
            { 
                return Tools.LogAndThrowIfErrors<object[]>(() => {
                    var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;
                    var BrokerDB = PriceMyLoanUser.BrokerDB;
                    var pmlTemplateId = isLead && BrokerDB.CreateLeadFilesFromQuickpricerTemplate ? BrokerDB.QuickPricerTemplateId : BrokerDB.GetPmlTemplateIdByPortalMode(portalMode);

                    try
                    {
                        CPageData dataLoan;

                        try
                        {

                            if (!skipDupCheck)
                            {
                                var duplicatedLoans = SearchDuplicatedLoans4Fnma32(fnmaContent, portalMode);
                                if (duplicatedLoans.Length > 0) return new[] { duplicatedLoans, null };
                            }

                            dataLoan = FannieMaeImporter.Import(PriceMyLoanUser, fnmaContent.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None),
                                FannieMaeImportSource.PriceMyLoan,
                                true /* addEmployeeAsOfficialAgent */, pmlTemplateId, PriceMyLoanUser.IsPricingMultipleAppsSupported, isLead);
                        }
                        catch (InvalidFannieFileFormatException e)
                        {
                            return new[] { e.UserMessage, null };
                        }

                        if ((dataLoan.sLienPosT == E_sLienPosT.Second) && !BrokerDB.IsStandAloneSecondAllowedInPml)
                        {
                            //OPM 4442 db - Delete the file that was just created since second liens aren't allowed in this case
                            try
                            {
                                Tools.LogAndThrowIfErrors(() =>
                                {
                                    Tools.DeclareLoanFileInvalid(PriceMyLoanUser, dataLoan.sLId, false, false);
                                });
                            }
                            catch (CBaseException)
                            {
                                // 11/13/08 db - Ignore if we couldn't delete the loan file for any reason, and leave the orphan.
                            }

                            return new[] { ErrorMessages.SecondLienNotSupportedPML, null };
                        }

                        if (Guid.Empty != pmlTemplateId)
                        {
                            PriceMyLoan.main.PmlDuplicateFromTemplate.DuplicatePreparers(pmlTemplateId, dataLoan.sLId);
                        }
                        Tools.NotifyImportAssignees(dataLoan.sLId);
                        if (!string.IsNullOrEmpty(dataLoan.sDuCaseId) && (!string.IsNullOrEmpty(PriceMyLoanUser.DoAutoLoginNm) || !string.IsNullOrEmpty(PriceMyLoanUser.DuAutoLoginNm)))
                        {
                            CPageData tempData = new CFannieMae32ExporterData(dataLoan.sLId);
                            tempData.InitSave(ConstAppDavid.SkipVersionCheck);
                            tempData.sDuCaseId = "";
                            tempData.Save();
                        }

                        TaskUtilities.EnqueueTasksDueDateUpdate(PriceMyLoanUser.BrokerId, dataLoan.sLId);
                        return new[] { null, dataLoan.sLId.ToString() };

                    }
                    catch (InvalidFannieFileFormatException)
                    {
                        return new[] { "Invalid Fannie Mae file. Please try again." };
                    }
                });
            }
            catch (CBaseException e)
            {
                return new[] { e.UserMessage, null };
            }
        }

        [WebMethod]
        public static object[] ImportCalyxPoint(string calyxBase64, E_PortalMode portalMode, IEnumerable<string> cbfs, bool isLead)
        {
            try
            {
                return Tools.LogAndThrowIfErrors(() =>
                {
                    // 11/16/06 mf. OPM 4565
                    var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;
                    var BrokerID = PriceMyLoanUser.BrokerId;
                    var BrokerDB = PriceMyLoanUser.BrokerDB;

                    var pmlTemplateId = isLead && BrokerDB.CreateLeadFilesFromQuickpricerTemplate ? BrokerDB.QuickPricerTemplateId : BrokerDB.GetPmlTemplateIdByPortalMode(portalMode);

                    if (!PriceMyLoanUser.BrokerDB.IsPmlPointImportAllowed)
                    {
                        return new[] { "You do not have permission to import a Calyx Point file." };
                    }

                    // create coborrower files array if multiple apps are supported
                    var calyx = new MemoryStream(Convert.FromBase64String(calyxBase64));
                    var streams = !PriceMyLoanUser.IsPricingMultipleAppsSupported ? new Stream[0] :
                        cbfs.Select(f => new MemoryStream(Convert.FromBase64String(f))).ToArray();

            
                    CLoanFileCreator creator = CLoanFileCreator.GetCreator(PriceMyLoanUser, LendersOffice.Audit.E_LoanCreationSource.PointImport);

                    Guid sLId = creator.BeginCreateImportBaseLoanFile(
                        sourceFileId: pmlTemplateId,
                        setInitialEmployeeRoles: true,
                        addEmployeeOfficialAgent: true,
                        assignEmployeesFromRelationships: true,
                        branchIdToUse: Guid.Empty,
                        isLead: isLead);

                    CPageData dataLoan = new CImportPointData(sLId);
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    var pointImport = new PointImportHelper
                    {
                        ResetDuCaseFileId = !string.IsNullOrEmpty(PriceMyLoanUser.DoAutoLoginNm) ||
                                            !string.IsNullOrEmpty(PriceMyLoanUser.DuAutoLoginNm)
                    };

                    if (!PriceMyLoanUser.IsPricingMultipleAppsSupported || streams.Length < 1)
                    {
                        pointImport.Import((CPointData)dataLoan, BrokerID, calyx, PriceMyLoanUser.EmployeeId, Guid.Empty, true, BrokerDB.IsStandAloneSecondAllowedInPml);
                    }
                    else if (PriceMyLoanUser.IsPricingMultipleAppsSupported)
                    {
                        pointImport.Import((CPointData)dataLoan, BrokerID, PriceMyLoanUser.EmployeeId, Guid.Empty, true, BrokerDB.IsStandAloneSecondAllowedInPml, calyx, streams);
                    }

                    dataLoan.SetsLNmWithPermissionBypass(creator.LoanName);
                    dataLoan.TransformDataToPml(E_TransformToPmlT.FromPoint);

                    BlankOutFieldsFromPointImport(dataLoan);

                    dataLoan.Save();
                    creator.CommitFileCreation(false);

                    if (Guid.Empty != pmlTemplateId)
                    {
                        PmlDuplicateFromTemplate.DuplicatePreparers(pmlTemplateId, dataLoan.sLId);
                    }
                    Tools.NotifyImportAssignees(dataLoan.sLId);

                    TaskUtilities.EnqueueTasksDueDateUpdate(PriceMyLoanUser.BrokerId, sLId);
                    return new[] { null, (object)sLId };
                });
            }
            catch (BlankLoanNumberException)
            {
                return new[] { "Invalid Calyx Point File." };
            }
            catch (CBaseException e)
            {
                if (e.UserMessage != ErrorMessages.SecondLienNotSupportedPML)
                {
                    Tools.LogError("Pml Calyx Point Import Error", e);
                }
                return new[] { e.UserMessage };
            }
        }
        static void BlankOutFieldsFromPointImport(CPageData dataLoan)
        {
            #region Blank Out Certain Fields from Point import
            // 10/8/2004 dd - Blank out all status date excepted for opened date.
            dataLoan.sOpenedD_rep = DateTime.Now.ToShortDateString();
            dataLoan.sOpenedN = "";
            dataLoan.sRLckdD_rep = "";
            dataLoan.Set_sRLckdExpiredD_Manually("");
            //dataLoan.sRLckdExpiredD_rep = "";
            dataLoan.sLeadD_rep = "";
            dataLoan.sPreQualD_rep = "";
            dataLoan.sSubmitD_rep = "";
            dataLoan.sSubmitN = "";
            dataLoan.sApprovD_rep = "";
            dataLoan.sApprovN = "";
            dataLoan.sEstCloseD_rep = "";
            dataLoan.sEstCloseDLckd = false;
            dataLoan.sEstCloseN = "";
            dataLoan.sDocsD_rep = "";
            dataLoan.sDocsN = "";
            dataLoan.sFundD_rep = "";
            dataLoan.sFundN = "";
            dataLoan.sRecordedD_rep = "";
            dataLoan.sRecordedN = "";
            dataLoan.sOnHoldD_rep = "";
            dataLoan.sCanceledD_rep = "";
            dataLoan.sCanceledN = "";
            dataLoan.sRejectD_rep = "";
            dataLoan.sRejectN = "";
            dataLoan.sSuspendedD_rep = "";
            dataLoan.sSuspendedN = "";
            dataLoan.sClosedD_rep = "";
            dataLoan.sClosedN = "";
            dataLoan.sU1LStatD_rep = "";
            dataLoan.sU1LStatDesc = "";
            dataLoan.sU1LStatN = "";
            dataLoan.sU2LStatD_rep = "";
            dataLoan.sU2LStatN = "";
            dataLoan.sU2LStatDesc = "";
            dataLoan.sU3LStatD_rep = "";
            dataLoan.sU3LStatN = "";
            dataLoan.sU3LStatDesc = "";
            dataLoan.sU4LStatD_rep = "";
            dataLoan.sU4LStatN = "";
            dataLoan.sU4LStatDesc = "";
            // 11/1/2004 dd - Blank Out Trust Accounts.
            dataLoan.sTrust1Desc = "";
            dataLoan.sTrust1D_rep = "";
            dataLoan.sTrust1PayableChkNum = "";
            dataLoan.sTrust1PayableAmt_rep = "";
            dataLoan.sTrust1ReceivableChkNum = "";
            dataLoan.sTrust1ReceivableAmt_rep = "";
            dataLoan.sTrust1ReceivableN = "";
            dataLoan.sTrust2Desc = "";
            dataLoan.sTrust2D_rep = "";
            dataLoan.sTrust2PayableChkNum = "";
            dataLoan.sTrust2PayableAmt_rep = "";
            dataLoan.sTrust2ReceivableChkNum = "";
            dataLoan.sTrust2ReceivableAmt_rep = "";
            dataLoan.sTrust2ReceivableN = "";
            dataLoan.sTrust3Desc = "";
            dataLoan.sTrust3D_rep = "";
            dataLoan.sTrust3PayableChkNum = "";
            dataLoan.sTrust3PayableAmt_rep = "";
            dataLoan.sTrust3ReceivableChkNum = "";
            dataLoan.sTrust3ReceivableAmt_rep = "";
            dataLoan.sTrust3ReceivableN = "";
            dataLoan.sTrust4Desc = "";
            dataLoan.sTrust4D_rep = "";
            dataLoan.sTrust4PayableChkNum = "";
            dataLoan.sTrust4PayableAmt_rep = "";
            dataLoan.sTrust4ReceivableChkNum = "";
            dataLoan.sTrust4ReceivableAmt_rep = "";
            dataLoan.sTrust4ReceivableN = "";
            dataLoan.sTrust5Desc = "";
            dataLoan.sTrust5D_rep = "";
            dataLoan.sTrust5PayableChkNum = "";
            dataLoan.sTrust5PayableAmt_rep = "";
            dataLoan.sTrust5ReceivableChkNum = "";
            dataLoan.sTrust5ReceivableAmt_rep = "";
            dataLoan.sTrust5ReceivableN = "";
            dataLoan.sTrust6Desc = "";
            dataLoan.sTrust6D_rep = "";
            dataLoan.sTrust6PayableChkNum = "";
            dataLoan.sTrust6PayableAmt_rep = "";
            dataLoan.sTrust6ReceivableChkNum = "";
            dataLoan.sTrust6ReceivableAmt_rep = "";
            dataLoan.sTrust6ReceivableN = "";
            dataLoan.sTrust7Desc = "";
            dataLoan.sTrust7D_rep = "";
            dataLoan.sTrust7PayableChkNum = "";
            dataLoan.sTrust7PayableAmt_rep = "";
            dataLoan.sTrust7ReceivableChkNum = "";
            dataLoan.sTrust7ReceivableAmt_rep = "";
            dataLoan.sTrust7ReceivableN = "";
            dataLoan.sTrust8Desc = "";
            dataLoan.sTrust8D_rep = "";
            dataLoan.sTrust8PayableChkNum = "";
            dataLoan.sTrust8PayableAmt_rep = "";
            dataLoan.sTrust8ReceivableChkNum = "";
            dataLoan.sTrust8ReceivableAmt_rep = "";
            dataLoan.sTrust8ReceivableN = "";
            dataLoan.sTrust9Desc = "";
            dataLoan.sTrust9D_rep = "";
            dataLoan.sTrust9PayableChkNum = "";
            dataLoan.sTrust9PayableAmt_rep = "";
            dataLoan.sTrust9ReceivableChkNum = "";
            dataLoan.sTrust9ReceivableAmt_rep = "";
            dataLoan.sTrust9ReceivableN = "";
            dataLoan.sTrust10Desc = "";
            dataLoan.sTrust10D_rep = "";
            dataLoan.sTrust10PayableChkNum = "";
            dataLoan.sTrust10PayableAmt_rep = "";
            dataLoan.sTrust10ReceivableChkNum = "";
            dataLoan.sTrust10ReceivableAmt_rep = "";
            dataLoan.sTrust10ReceivableN = "";
            dataLoan.sTrust11Desc = "";
            dataLoan.sTrust11D_rep = "";
            dataLoan.sTrust11PayableChkNum = "";
            dataLoan.sTrust11PayableAmt_rep = "";
            dataLoan.sTrust11ReceivableChkNum = "";
            dataLoan.sTrust11ReceivableAmt_rep = "";
            dataLoan.sTrust11ReceivableN = "";
            dataLoan.sTrust12Desc = "";
            dataLoan.sTrust12D_rep = "";
            dataLoan.sTrust12PayableChkNum = "";
            dataLoan.sTrust12PayableAmt_rep = "";
            dataLoan.sTrust12ReceivableChkNum = "";
            dataLoan.sTrust12ReceivableAmt_rep = "";
            dataLoan.sTrust12ReceivableN = "";
            dataLoan.sTrust13Desc = "";
            dataLoan.sTrust13D_rep = "";
            dataLoan.sTrust13PayableChkNum = "";
            dataLoan.sTrust13PayableAmt_rep = "";
            dataLoan.sTrust13ReceivableChkNum = "";
            dataLoan.sTrust13ReceivableAmt_rep = "";
            dataLoan.sTrust13ReceivableN = "";
            dataLoan.sTrust14Desc = "";
            dataLoan.sTrust14D_rep = "";
            dataLoan.sTrust14PayableChkNum = "";
            dataLoan.sTrust14PayableAmt_rep = "";
            dataLoan.sTrust14ReceivableChkNum = "";
            dataLoan.sTrust14ReceivableAmt_rep = "";
            dataLoan.sTrust14ReceivableN = "";
            dataLoan.sTrust15Desc = "";
            dataLoan.sTrust15D_rep = "";
            dataLoan.sTrust15PayableChkNum = "";
            dataLoan.sTrust15PayableAmt_rep = "";
            dataLoan.sTrust15ReceivableChkNum = "";
            dataLoan.sTrust15ReceivableAmt_rep = "";
            dataLoan.sTrust15ReceivableN = "";
            dataLoan.sTrust16Desc = "";
            dataLoan.sTrust16D_rep = "";
            dataLoan.sTrust16PayableChkNum = "";
            dataLoan.sTrust16PayableAmt_rep = "";
            dataLoan.sTrust16ReceivableChkNum = "";
            dataLoan.sTrust16ReceivableAmt_rep = "";
            dataLoan.sTrust16ReceivableN = "";
            // 11/1/2004 dd - Blank Out HMDA
            dataLoan.sHmdaExcludedFromReport = false;
            dataLoan.sHmdaReportAsHomeImprov = false;
            dataLoan.sHmdaPreapprovalT = E_sHmdaPreapprovalT.LeaveBlank;
            dataLoan.sHmdaAprRateSpread = "";
            dataLoan.sHmdaReportAsHoepaLoan = false;
            dataLoan.sHmdaPurchaser2004T = E_sHmdaPurchaser2004T.LeaveBlank;
            dataLoan.sHmdaPurchaser = "";
            dataLoan.sHmdaActionTaken = "";
            dataLoan.sHmdaActionD_rep = "";
            dataLoan.sHmdaDenialReason1 = "";
            dataLoan.sHmdaDenialReason2 = "";
            dataLoan.sHmdaDenialReason3 = "";
            dataLoan.sHmdaLoanDenied = false;
            dataLoan.sHmdaLoanDeniedBy = "";
            dataLoan.sHmdaDeniedFormDone = false;
            dataLoan.sHmdaDeniedFormDoneD_rep = "";
            dataLoan.sHmdaDeniedFormDoneBy = "";
            dataLoan.sHmdaCounterOfferMade = false;
            dataLoan.sHmdaCounterOfferMadeD_rep = "";
            dataLoan.sHmdaCounterOfferMadeBy = "";
            dataLoan.sHmdaCounterOfferDetails = "";
            // 11/1/2004 dd - Blank Out Commissions
            dataLoan.sProfitGrossBorPnt_rep = "";
            dataLoan.sProfitGrossBorMb_rep = "";
            dataLoan.sProfitRebatePnt_rep = "";
            dataLoan.sProfitRebateMb_rep = "";
            #endregion
        }


        [WebMethod]
        public static object[] ImportDoDu(string userId, string password, bool rememberLogin, string sDuCaseId, bool skipDupCheck, E_PortalMode portalMode, bool isImportCreditReport, bool isLead)
        {
            try
            {
                return Tools.LogAndThrowIfErrors<object[]>(() =>
                {
                    var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;
                    var BrokerDB = PriceMyLoanUser.BrokerDB;
                    var pmlTemplateId = isLead && BrokerDB.CreateLeadFilesFromQuickpricerTemplate ? BrokerDB.QuickPricerTemplateId : BrokerDB.GetPmlTemplateIdByPortalMode(portalMode);

                    var result = ImportDoDu(userId, password, sDuCaseId, rememberLogin);
                    var error = result.Item1;
                    var response = result.Item2;
                    if (error != null)
                    {
                        return new[] { error };
                    }

                    if (!skipDupCheck)
                    {
                        var duplicatedLoans = SearchDuplicatedLoans4Fnma32(response.Fnma32Content, portalMode);
                        if (duplicatedLoans.Length > 0) return new[] { duplicatedLoans, null };
                    }

                    CLoanFileCreator creator = CLoanFileCreator.GetCreator(PriceMyLoanUser, LendersOffice.Audit.E_LoanCreationSource.ImportFromDoDu);

                    Guid loanID;
                    if (isLead)
                    {
                        loanID = creator.CreateLead(pmlTemplateId) ;
                    }
                    else
                    {
                        loanID = Guid.Empty == pmlTemplateId ? creator.CreateBlankLoanFile() : creator.CreateLoanFromTemplate(pmlTemplateId);
                    }

                    response.ImportToLoanFile(loanID, PriceMyLoanUser, true /* isImport1003 */, true /* isImportDuFindings */, isImportCreditReport);

                    // Transform Data.
                    CPageData dataLoan = new CPmlLoanSummary_TransformData(loanID);
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                    dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

                    dataLoan.Save();

                    TaskUtilities.EnqueueTasksDueDateUpdate(PriceMyLoanUser.BrokerId, loanID);
                    return new[] { null, loanID.ToString() };
                });
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking("Unable to import to PML from DU", exc);
                return new[] { exc.UserMessage };
            }
        }

        private static Tuple<string, FnmaXisCasefileExportResponse> ImportDoDu(string fannieMaeMornetUserId, string fannieMaeMornetPassword, string sDuCaseId, bool rememberLogin)
        {
            string globallyUniqueIdentifier = "";

            SaveDoDuLpLoginNm(rememberLogin ? fannieMaeMornetUserId : string.Empty);

            while (true)
            {
                AbstractFnmaXisRequest request = new FnmaXisCasefileExportRequest(Guid.Empty)
                {
                    FannieMaeMORNETUserID = fannieMaeMornetUserId,
                    FannieMaeMORNETPassword = fannieMaeMornetPassword,
                    GloballyUniqueIdentifier = globallyUniqueIdentifier,
                    MornetPlusCasefileIdentifier = sDuCaseId,
                };
                var response = DUServer.Submit(request);

                if (!response.IsReady)
                {
                    globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                }
                else
                {
                    if (response.HasError)
                    {
                        return new Tuple<string, FnmaXisCasefileExportResponse>(response.ErrorMessage, null);
                    }

                    if (response.HasBusinessError)
                    {
                        return new Tuple<string, FnmaXisCasefileExportResponse>(
                            response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog, null);
                    }

                    return new Tuple<string, FnmaXisCasefileExportResponse>(null, (FnmaXisCasefileExportResponse)response);
                }
            }
        }


        private static LoanBorrowerData GetLoanBorrowerData(Guid SearchDataKey)
        {
            if (SearchDataKey == Guid.Empty) return null;

            string borrowerData = AutoExpiredTextCache.GetFromCache(SearchDataKey);
            if (borrowerData == null)
            {
                Tools.LogWarning("Loan Data Missing from cache."); // Cache may have expired
                return null;
            }

            return SerializationHelper.XmlDeserialize<LoanBorrowerData>(borrowerData);
        }

        private static Dictionary<string, object>[] SearchDuplicatedLoans(string aBFirstNm, string aBLastNm, string aBSsn, E_PortalMode portalMode, E_sCorrespondentProcessT sCorrespondentProcessT)
        {
            IEnumerable<Dictionary<string, object>> duplicates = ListLoansWithDuplicateCheck(aBLastNm, aBFirstNm, aBSsn, sCorrespondentProcessT, portalMode);
            return FormatLoanData(duplicates);
        }

        private static Dictionary<string, object>[] SearchDuplicatedLoans4Fnma32(string Fnma32Content, E_PortalMode portalMode)
        {
            MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(Fnma32Content));
            string[] lines = StreamToStrings(stream);

            var m_mornetDoc = new LendersOffice.Conversions.MORNETPlus32.MORNETPlus32Document();
            m_mornetDoc.Load(lines);

            var Importer = new FannieMae32Importer(FannieMaeImportSource.PriceMyLoan);
            var apps = Importer.GetBorrowerBasicInfo(m_mornetDoc._1003);

            return apps.SelectMany(appData => SearchDuplicatedLoans(appData[2], appData[1], appData[0], portalMode, E_sCorrespondentProcessT.Blank))
                    .ToArray()
                ;
        }

        private static void SaveDoDuLpLoginNm(string rememberLoginNm)
        {
            var PriceMyLoanUser = PrincipalFactory.CurrentPrincipal;

            // 4/9/2008 dd - Save the login name to both DO and DU account. The reason is when we import we do not know if user enter DO or DU account.
            SqlParameter[] parameters = {
                new SqlParameter("@UserId", PriceMyLoanUser.UserId),
                new SqlParameter("@DOLastLoginNm", rememberLoginNm),
                new SqlParameter("@DULastLoginNm", rememberLoginNm)
            };
            StoredProcedureHelper.ExecuteNonQuery(PriceMyLoanUser.BrokerId, "UpdateLastDoDuLpLoginNm", 3, parameters);
        }

        [WebMethod]
        public static object RunPipelineReport(Guid reportId, E_ReportExtentScopeT reportScope)
        {
            return Tools.LogAndThrowIfErrors(() =>
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;
                var settings = new CustomReportJobSettings()
                {
                    QueryId = reportId,
                    ReportExtent = reportScope,
                    ReportRunType = CustomReportRunType.Pipeline
                };

                var jobId = CustomReportJob.CreateCustomReportJob(currentUser, settings);
                return new
                {
                    JobId = jobId,
                    PollingInterval = ConstStage.CustomReportBackgroundJobPollingIntervalSeconds,
                    MaxPollAttempts = ConstStage.CustomReportJobPollingRetryLimit
                };
            });
        }

        [WebMethod]
        public static object CheckReportStatus(Guid jobId)
        {
            return Tools.LogAndThrowIfErrors(() =>
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;
                var status = CustomReportJob.CheckJobStatus(jobId);

                OriginatorPortalPipelineReportModel reportModel = null;
                if (status.IsComplete)
                {
                    var reportName = GetReportName(status.QueryId);
                    var generator = new OriginatorPortalPipelineReportModelGenerator(status.CompletedReport, reportName);
                    reportModel = generator.Generate();
                }

                return new
                {
                    HasError = status.HasError,
                    UserLacksReportRunPermission = status.UserLacksReportRunPermission,
                    IsComplete = status.IsComplete,
                    RetailMode = currentUser.PortalMode == E_PortalMode.Retail,
                    currentUser.BrokerDB.HideStatusAndAgentsPageInOriginatorPortal,
                    ReportModel = reportModel
                };
            });
        }

        [WebMethod]
        public static void EditReportTitle(Guid reportId, string newTitle)
        {
            Tools.LogAndThrowIfErrors(() =>
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;
                if (string.Equals("P", currentUser.Type, StringComparison.OrdinalIgnoreCase))
                {
                    throw new AccessDenied();
                }

                var pipelineView = Tools.GetMainTabsSettingXmlContent(currentUser.ConnectionInfo, currentUser.UserId);
                var selectedReport = pipelineView.Tabs.FirstOrDefault(tab => tab.ReportId == reportId);

                if (selectedReport == null)
                {
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        $"Failed to update title of report {reportId} for user {currentUser.UserId} to '{newTitle}', which does not exist.");
                }

                selectedReport.TabName = newTitle;
                Tools.UpdateMainTabsSettingXmlContent(currentUser.ConnectionInfo, currentUser.UserId, pipelineView);
            });
        }

        [WebMethod]
        public static void UpdatePipelineReportView(string selectedReportsJson)
        {
            Tools.LogAndThrowIfErrors(() =>
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;
                var selectedReports = SerializationHelper.JsonNetDeserialize<List<KeyValuePair<Guid, string>>>(selectedReportsJson);

                if (string.Equals("B", currentUser.Type, StringComparison.OrdinalIgnoreCase))
                {
                    var pipelineView = Tools.GetMainTabsSettingXmlContent(currentUser.ConnectionInfo, currentUser.UserId);

                    pipelineView.Tabs.RemoveAll(tab => tab.ReportId != Guid.Empty);
                    pipelineView.Tabs.AddRange(selectedReports.Select(kvp => new CTab(E_TabTypeT.PipelinePortlet, kvp.Value, kvp.Key, kvp.Value, E_ReportExtentScopeT.Assign)));

                    Tools.UpdateMainTabsSettingXmlContent(currentUser.ConnectionInfo, currentUser.UserId, pipelineView);
                }
                else
                {
                    var newReportIds = selectedReports.Select(kvp => kvp.Key);
                    var pipelineSettings = new TpoUserPipelineSettings(currentUser);
                    pipelineSettings.Update(newReportIds);
                }
            });
        }

        /// <summary>
        /// Gets the configured report name for the given report ID.
        /// </summary>
        /// <param name="reportId">
        /// The ID of the report.
        /// </param>
        /// <returns>
        /// The configured report name.
        /// </returns>
        private static string GetReportName(Guid reportId)
        {
            var currentUser = PrincipalFactory.CurrentPrincipal;
            if (string.Equals("B", currentUser.Type, StringComparison.OrdinalIgnoreCase))
            {
                var pipelineView = Tools.GetMainTabsSettingXmlContent(currentUser.ConnectionInfo, currentUser.UserId);
                var selectedReport = pipelineView.Tabs.FirstOrDefault(tab => tab.ReportId == reportId);

                if (selectedReport == null)
                {
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        $"Failed to run report {reportId} for user {currentUser.UserId}, which does not exist.");
                }

                return selectedReport.TabName;
            }

            var pipelineSettings = new TpoUserPipelineSettings(currentUser);
            return pipelineSettings.RetrieveReportNameByReportId(reportId);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }

}
