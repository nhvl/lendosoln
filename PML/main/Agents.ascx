<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Agents.ascx.cs" Inherits="PriceMyLoan.UI.Main.Agents" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>


<script type="text/javascript">
    function wireupValidators()
    {
        addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);

        var BrokerProcessor_EmailValidator2 = <%= AspxTools.JsGetElementById(BrokerProcessor_EmailValidator2) %>;
        if (BrokerProcessor_EmailValidator2) {
            addTrimmingToAspRegExValidator(BrokerProcessor_EmailValidator2);
        }
    }

    function Page_OnValidate() {
        if ( <%= AspxTools.JsBool(IsReadOnly)%> ) {
            return;
        }
        var   bValid = f_validatePage();
        f_enableButton(bValid);
    }
</script>


<asp:PlaceHolder runat="server" ID="StandalonePMLScripts">
    <script type="text/javascript">
        $j(function(){
            var loURL = <%= AspxTools.JsString(LoPickerUrl) %>, bpURL = <%= AspxTools.JsString(BpPickerUrl) %>;
            
            
            $j('a.assignLo').click(function(){
                LQBPopup.Show(loURL,{
                    'onReturn' : f_assignLoanOfficer
                });
            });
            
            $j('a.assignBP').click(function(){
               LQBPopup.Show(bpURL,{
                    'onReturn' : f_assignBrokerProcessor
                });
            });
        });

        function _init() {
            wireupValidators();
            var loid = document.getElementById('<%= AspxTools.ClientId(LoId) %>');
            f_enableLoanOfficerFields(loid.value == '' || loid.value == '00000000-0000-0000-0000-000000000000' );
            var bpid = document.getElementById('<%= AspxTools.ClientId(BpId) %>');
            f_enableBrokerProcessorFields(bpid.value == '' || bpid.value == '00000000-0000-0000-0000-000000000000');
            <% if (m_hasRecentModification) { %>
            LQBPopup.Show(ML.VirtualRoot + '/main/LoanRecentModification.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>, {width: 350, height: 300});
            <% } %>
        }
        function f_assignLoanOfficer(agent){
            $j('#<%= AspxTools.ClientId(LoanOfficer_AgentName2) %>').text(agent.fullName + "   ");
            <%= AspxTools.JsGetElementById(LoanOfficer_Phone) %>.value = agent.phone;
            <%= AspxTools.JsGetElementById(LoanOfficer_FaxNum) %>.value = agent.fax;
            <%= AspxTools.JsGetElementById(LoanOfficer_EmailAddr) %>.value = agent.email;
            <%= AspxTools.JsGetElementById(LoId) %>.value = agent.employeeId;
            <%= AspxTools.JsGetElementById(LoanOfficer_LicenseNumOfAgent) %>.value = agent.licenses;
            <%= AspxTools.JsGetElementById(LoanOfficer_PagerNum) %>.value = agent.pager;
            <%= AspxTools.JsGetElementById(LoanOfficer_CellPhone) %>.value = agent.cellphone;
            updateDirtyBit();
            f_enableLoanOfficerFields(false);
            Page_OnValidate();
            return false;
        }  
        
        function f_assignBrokerProcessor(agent) {
           $j('#<%= AspxTools.ClientId(BrokerProcessor_FullName) %>').text(agent.fullName + "   ");
           <%= AspxTools.JsGetElementById(BrokerProcessor_Phone) %>.value = agent.phone;
           <%= AspxTools.JsGetElementById(BrokerProcessor_FaxNum) %>.value = agent.fax;
           <%= AspxTools.JsGetElementById(BrokerProcessor_EmailAddr) %>.value = agent.email;
           <%= AspxTools.JsGetElementById(BpId) %>.value = agent.employeeId;
           <%= AspxTools.JsGetElementById(BrokerProcessor_LicenseNumOfAgent) %>.value = agent.licenses;
           <%= AspxTools.JsGetElementById(BrokerProcessor_PagerNum) %>.value = agent.pager;
           <%= AspxTools.JsGetElementById(BrokerProcessor_CellPhone) %>.value = agent.cellphone;
            updateDirtyBit();
           f_enableBrokerProcessorFields(false);
           Page_OnValidate();
           return false;
        }  
    
        
        function f_enableLoanOfficerFields(clear, isUser) {
            var name = '<%=AspxTools.ClientId(LoanOfficer_AgentName2) %>';
            var inputs = [ 
              '<%= AspxTools.ClientId(LoanOfficer_Phone) %>',
              '<%= AspxTools.ClientId(LoanOfficer_FaxNum) %>',
              '<%= AspxTools.ClientId(LoanOfficer_CellPhone) %>',
              '<%= AspxTools.ClientId(LoanOfficer_PagerNum) %>',
              '<%= AspxTools.ClientId(LoanOfficer_EmailAddr) %>'];   
            var imageName = '<%= AspxTools.ClientId(MissingLOImage) %>';
            var id = '<%=AspxTools.ClientId(LoId) %>';
            
            if(clear) {
                <%= AspxTools.JsGetElementById(LoanOfficer_LicenseNumOfAgent) %>.value  = '';
            }
          return Clear(name, imageName, inputs, clear, id, isUser);                   
        }
        
        function f_enableBrokerProcessorFields(clear, isUser) {
            
                ValidatorEnable(document.getElementById('<%=AspxTools.ClientId(BrokerProcessor_PhoneValidator) %>'), !clear );
                ValidatorEnable(document.getElementById('<%=AspxTools.ClientId(BrokerProcessor_EmailValidator) %>'),!clear);
                ValidatorEnable(document.getElementById('<%=AspxTools.ClientId(BrokerProcessor_EmailValidator2) %>'),!clear);
           
            var name = '<%=AspxTools.ClientId(BrokerProcessor_FullName) %>';
            var inputs = [ 
              '<%= AspxTools.ClientId(BrokerProcessor_Phone) %>',
              '<%= AspxTools.ClientId(BrokerProcessor_FaxNum) %>',
              '<%= AspxTools.ClientId(BrokerProcessor_CellPhone) %>',
              '<%= AspxTools.ClientId(BrokerProcessor_PagerNum) %>',
              '<%= AspxTools.ClientId(BrokerProcessor_EmailAddr) %>'];
    
            var imageName = null;
            var id = '<%=AspxTools.ClientId(BpId) %>';

            if( clear ) {
                 <%= AspxTools.JsGetElementById(BrokerProcessor_LicenseNumOfAgent) %>.value  = '';
            }
            return Clear(name,imageName,inputs,clear, id, isUser);                                    
        }
        
        
        function Clear(name, imageName, inputs, clear, id, isUser){
            if (isUser) {
                updateDirtyBit();
            }
            if (clear) { 
                $j('#' + name).text('');
                document.getElementById(id).value =  '00000000-0000-0000-0000-000000000000';
            }
            
            var element;
            for (var i = 0; i<inputs.length; i++) {
                element = document.getElementById(inputs[i]);
                if( clear ) { 
                    element.value = '';
                }
                element.disabled = clear;
                element.style.backgroundColor = clear ? 'lightgrey' : 'white';
            }
            if( imageName != null ){
                var image = document.getElementById(imageName);
                image.style.display = clear ? '' : 'none';
            }
             Page_OnValidate();
             
            return false;  
        }
        
    </script>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="EmbeddedPmlScripts">
    <script type="text/javascript">
        var oRolodex;
        function _init() {
            wireupValidators();
            oRolodex = new cRolodex();
            document.getElementById('<%= AspxTools.ClientId(btnAssign) %>').disabled = <%= AspxTools.JsBool(IsReadOnly)%>;
            
        }
        
        function f_assignLoanOfficer() {
            var role = <%= AspxTools.JsString(EmbeddedLORole) %>;
            var roledesc = <%= AspxTools.JsString(EmbeddedLORoleDesc) %>;

            LQBPopup.Show(ML.VirtualRoot + '/main/EmployeeRole.aspx?role=' + role + '&roledesc=' + roledesc + '&loanid=' + <%= AspxTools.JsString(LoanID) %> + '&show=full',
            { onReturn: function(arg){
              <%= AspxTools.JsGetElementById(LoanOfficer_AgentName) %>.value = arg.EmployeeName;
              <%= AspxTools.JsGetElementById(LoanOfficer_Phone) %>.value = arg.Phone;
              <%= AspxTools.JsGetElementById(LoanOfficer_FaxNum) %>.value = arg.Fax;
              <%= AspxTools.JsGetElementById(LoanOfficer_EmailAddr) %>.value = arg.Email;
              <%= AspxTools.JsGetElementById(LoanOfficer_StreetAddr) %>.value = arg.Addr;
              <%= AspxTools.JsGetElementById(LoanOfficer_City) %>.value = arg.City;
              <%= AspxTools.JsGetElementById(LoanOfficer_State) %>.value = arg.State;
              <%= AspxTools.JsGetElementById(LoanOfficer_Zip) %>.value = arg.Zip;
              <%= AspxTools.JsGetElementById(LoanOfficer_CompanyName) %>.value = arg.sActualBrokerNm;
              <%= AspxTools.JsGetElementById(LoanOfficer_LicenseNumOfAgent) %>.value = arg.AgentLicenseNum;
              <%= AspxTools.JsGetElementById(LoanOfficer_LicenseNumOfCompany) %>.value = arg.CompanyLicenseNum;
              <%= AspxTools.JsGetElementById(fieldAgentLicenseXml) %>.value = arg.AgentLicenseXml;
              <%= AspxTools.JsGetElementById(fieldCompanyLicenseXml) %>.value = arg.CompanyLicenseXml;
              <%= AspxTools.JsGetElementById(LoanOfficer_PhoneOfCompany) %>.value = arg.LoanOfficer_PhoneOfCompany;
              <%= AspxTools.JsGetElementById(LoanOfficer_FaxOfCompany) %>.value = arg.LoanOfficer_FaxOfCompany;
              <%= AspxTools.JsGetElementById(LoId) %>.value = arg.EmployeeId;
              <%= AspxTools.JsGetElementById(CommissionPointOfLoanAmount) %>.value = arg.CommissionPointOfLoanAmount;
              <%= AspxTools.JsGetElementById(CommissionPointOfGrossProfit) %>.value = arg.CommissionPointOfGrossProfit;
              <%= AspxTools.JsGetElementById(CommissionMinBase) %>.value = arg.CommissionMinBase;
              <%= AspxTools.JsGetElementById(LoanOriginatorIdentifier) %>.value = arg.LoanOriginatorIdentifier;
              <%= AspxTools.JsGetElementById(CompanyLoanOriginatorIdentifier) %>.value = arg.CompanyLoanOriginatorIdentifier;
              document.getElementById("sFileVersion").value = arg.sFileVersion;
              },
              hideCloseButton: true
            });            
         }
    </script>
</asp:PlaceHolder>

<asp:HiddenField runat="server" ID="LoId" />
<asp:HiddenField runat="server" ID="BpId" />

<input type="hidden" runat="server" id="fieldAgentLicenseXml" enableviewstate="true" />
<input type="hidden" runat="server" id="fieldCompanyLicenseXml" enableviewstate="true"/>
      
<table id="Table1" cellspacing="0" cellpadding="5" width="700" border="0">
  <tr>
    <td nowrap class="Subheader"><ml:EncodedLiteral runat="server" ID="LoDesc">Loan Officer</ml:EncodedLiteral> Info</td>
  </tr>
  <tr>
    <td nowrap>
      <table id="Table2" cellspacing="0" cellpadding="0"  border="0">
        <% if (IsEmbeddedPML) { %>
        <tr>
          <td class="FieldLabel" nowrap></td>
          <td nowrap colspan="2">
          <input type="button" value="Assign to Loan Officer ..."  class="ButtonStyle assignLo" style="width: 180px" id="btnAssign" onclick="f_assignLoanOfficer();" runat="server"> </td>
          <td nowrap></td>
        </tr>
        <% } %>
        <tr>
          <td class="FieldLabel" width="200" nowrap>&nbsp; </td>
          <td nowrap></td>
          <td nowrap></td>
          <td nowrap></td>
        </tr>
        <tr>
      
          <td nowrap class="FieldLabel">Name</td>
          <td nowrap colspan="3">
          
            <% if( IsEmbeddedPML  ) { %>
                <asp:TextBox ID="LoanOfficer_AgentName" runat="server" Width="232px" onkeyup="Page_OnValidate();"></asp:TextBox>
                <asp:RequiredFieldValidator ID="LoanOfficer_AgentNameValidator" runat="server" CssClass="RequiredFieldValidator" ControlToValidate="LoanOfficer_AgentName"></asp:RequiredFieldValidator>
        
             <% } else {  %>
               <ml:EncodedLabel runat="server" ID="LoanOfficer_AgentName2"></ml:EncodedLabel>
               <asp:PlaceHolder runat="server" ID="LOControlLinks">
               [ <a style="cursor:pointer" onclick="return f_enableLoanOfficerFields(true, true)">None</a> | <a style="cursor:pointer" class="assignLo" runat="server">Pick <ml:EncodedLiteral runat="server" ID="LoDescAssign">Loan Officer</ml:EncodedLiteral></a> ]</asp:PlaceHolder>
                <asp:image runat="server" ID="MissingLOImage" />
            <%}  %>            
</td>
          <td nowrap></td>
        </tr>
       
        <tr>
          <td class="FieldLabel" nowrap>License #</td>
          <td nowrap colspan="3">
            <asp:TextBox ID="LoanOfficer_LicenseNumOfAgent" runat="server" Width="232px"></asp:TextBox></td>
          <td nowrap></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap><ml:EncodedLiteral runat="server" ID="LoDescPhone">Loan Officer</ml:EncodedLiteral> Phone</td>
          <td nowrap>
            <ml:PhoneTextBox ID="LoanOfficer_Phone" runat="server" preset="phone" Width="120" onkeyup="Page_OnValidate();"></ml:PhoneTextBox><asp:RequiredFieldValidator ID="LoanOfficer_PhoneValidator" runat="server" CssClass="RequiredFieldValidator" ControlToValidate="LoanOfficer_Phone"></asp:RequiredFieldValidator></td>
          <td class="FieldLabel" nowrap style="padding-left: 25px" width="150"><ml:EncodedLiteral runat="server" ID="LoDescFax">Loan Officer</ml:EncodedLiteral> Fax</td>
          <td nowrap>
            <ml:PhoneTextBox ID="LoanOfficer_FaxNum" runat="server" preset="phone" Width="120"></ml:PhoneTextBox></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap><ml:EncodedLiteral runat="server" ID="LoDescCellPhone">Loan Officer</ml:EncodedLiteral> Cell Phone&nbsp; </td>
          <td nowrap>
            <ml:PhoneTextBox ID="LoanOfficer_CellPhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox></td>
          <td class="FieldLabel" nowrap style="padding-left: 25px"><ml:EncodedLiteral runat="server" ID="LoDescPager">Loan Officer</ml:EncodedLiteral> Pager&nbsp;&nbsp;</td>
          <td nowrap>
            <ml:PhoneTextBox ID="LoanOfficer_PagerNum" runat="server" preset="phone" Width="120"></ml:PhoneTextBox></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap><ml:EncodedLiteral runat="server" ID="LoDescEmail">Loan Officer</ml:EncodedLiteral> Email</td>
          <td nowrap colspan="3">
            <asp:TextBox ID="LoanOfficer_EmailAddr" runat="server" Width="232px" onkeyup="Page_OnValidate();"></asp:TextBox><asp:RequiredFieldValidator ID="LoanOfficer_EmailAddrValidator" runat="server" CssClass="RequiredFieldValidator" ControlToValidate="LoanOfficer_EmailAddr"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="LoanOfficer_EmailAddr" Display="Dynamic" Font-Bold="True"></asp:RegularExpressionValidator></td>
        </tr>
        <% if (IsEmbeddedPML) { %>
        <tr>
          <td nowrap class="FieldLabel">Company Name</td>
          <td nowrap colspan="2">
            <asp:TextBox ID="LoanOfficer_CompanyName" runat="server" Width="232px"></asp:TextBox></td>
          <td nowrap></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Company License #</td>
          <td nowrap colspan="2">
            <asp:TextBox ID="LoanOfficer_LicenseNumOfCompany" runat="server" Width="232px"></asp:TextBox></td>
          <td nowrap></td>
        </tr>
        <tr>
          <td nowrap class="FieldLabel">Company Address</td>
          <td nowrap colspan="3">
            <asp:TextBox ID="LoanOfficer_StreetAddr" runat="server" Width="288px"></asp:TextBox></td>
        </tr>
        <tr>
          <td nowrap></td>
          <td nowrap colspan="3">
            <asp:TextBox ID="LoanOfficer_City" runat="server" Width="188px"></asp:TextBox><ml:StateDropDownList ID="LoanOfficer_State" runat="server"></ml:StateDropDownList>
            <ml:ZipcodeTextBox ID="LoanOfficer_Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox></td>
        </tr>
        <tr>
          <td nowrap class="FieldLabel">Company Phone</td>
          <td nowrap>
            <ml:PhoneTextBox ID="LoanOfficer_PhoneOfCompany" runat="server" Width="120" preset="phone"></ml:PhoneTextBox></td>
          <td class="FieldLabel" nowrap style="padding-left: 25px">Company Fax</td>
          <td nowrap>
            <ml:PhoneTextBox ID="LoanOfficer_FaxOfCompany" runat="server" Width="120" preset="phone"></ml:PhoneTextBox></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Notes</td>
          <td nowrap></td>
          <td nowrap></td>
          <td nowrap></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap colspan="4">
            <asp:TextBox ID="LoanOfficer_Notes" runat="server" Width="442px" TextMode="MultiLine" Height="118px"></asp:TextBox></td>
        </tr>
        <% } %>
      </table>
      
      <input type="hidden" ID="CommissionPointOfLoanAmount" runat="server" />      
      <input type="hidden" ID="CommissionPointOfGrossProfit" runat="server" />
      <input type="hidden" ID="CommissionMinBase" runat="server" />
      <input type="hidden" id="LoanOriginatorIdentifier" runat="server" />
      <input type="hidden" id="CompanyLoanOriginatorIdentifier" runat="server" />
    </td>
  </tr>
  <asp:PlaceHolder runat="server" ID="BPFieldSection">
      <tr>
          <td nowrap class="Subheader" colspan="4">
              <ml:EncodedLiteral runat="server" ID="ProcDesc">Processor</ml:EncodedLiteral> Info
          </td>
      </tr>
      <tr>
        <td nowrap="nowrap">
            <table id="Table3" cellspacing="0" cellpadding="0"  border="0">
                <tr>
                    <td nowrap class="FieldLabel" width="200">
                        Name
                    </td>
                    <td nowrap colspan="3">
                        <ml:EncodedLabel runat="server" ID="BrokerProcessor_FullName"></ml:EncodedLabel>
                        <asp:PlaceHolder runat="server" ID="BPControlLinks">
                        [ <a  style="cursor:pointer" runat="server" ID="BPClear" onclick="return f_enableBrokerProcessorFields(true, true)">None</a>
                        | <a style="cursor:pointer" class="assignBP" >Pick <ml:EncodedLiteral runat="server" ID="ProcDescAssign">Processor</ml:EncodedLiteral></a> ]  </asp:PlaceHolder>
                    </td>
                    <td nowrap>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap >
                       <ml:EncodedLabel runat="server"  AssociatedControlID="BrokerProcessor_LicenseNumOfAgent">                       
                        License #
                       </ml:EncodedLabel>
                        
                    </td>
                    <td nowrap colspan="3">
                        <asp:TextBox ID="BrokerProcessor_LicenseNumOfAgent" runat="server" Width="232px"></asp:TextBox>
                    </td>
                    <td nowrap>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        <ml:EncodedLabel runat="server" AssociatedControlID="BrokerProcessor_Phone"><ml:EncodedLiteral runat="server" ID="ProcDescPhone">Processor</ml:EncodedLiteral> Phone</ml:EncodedLabel> 
                    </td>
                    <td nowrap>
                        <ml:PhoneTextBox ID="BrokerProcessor_Phone" runat="server" preset="phone" Width="120" onkeyup="Page_OnValidate();"></ml:PhoneTextBox>
                        <asp:RequiredFieldValidator ID="BrokerProcessor_PhoneValidator" runat="server" CssClass="BrokerProcessor_Phone"
                            ControlToValidate="BrokerProcessor_Phone"></asp:RequiredFieldValidator>
                    </td>
                    <td class="FieldLabel" nowrap style="padding-left: 25px" width="150">
                        <ml:EncodedLabel runat="server" AssociatedControlID="BrokerProcessor_FaxNum" > <ml:EncodedLiteral runat="server" ID="ProcDescFax">Processor</ml:EncodedLiteral> Fax</ml:EncodedLabel>
                    </td>
                    <td nowrap>
                        <ml:PhoneTextBox ID="BrokerProcessor_FaxNum" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                    <ml:EncodedLabel runat="server" AssociatedControlID="BrokerProcessor_CellPhone">
                         <ml:EncodedLiteral runat="server" ID="ProcDescCellPhone">Processor</ml:EncodedLiteral> Cell Phone&nbsp;&nbsp;
                        </ml:EncodedLabel>
                    </td>
                    <td nowrap>
                        <ml:PhoneTextBox ID="BrokerProcessor_CellPhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>
                    </td>
                    <td class="FieldLabel" nowrap style="padding-left: 25px">
                        <ml:EncodedLabel runat="server" AssociatedControlID="BrokerProcessor_PagerNum">
                         <ml:EncodedLiteral runat="server" ID="ProcDescPager">Processor</ml:EncodedLiteral> Pager&nbsp;&nbsp;
                        </ml:EncodedLabel>
                    </td>
                    <td nowrap>
                        <ml:PhoneTextBox ID="BrokerProcessor_PagerNum" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        <ml:EncodedLabel runat="server" AssociatedControlID="BrokerProcessor_EmailAddr">
                         <ml:EncodedLiteral runat="server" ID="ProcDescEmail">Processor</ml:EncodedLiteral> Email
                        </ml:EncodedLabel>
                    </td>
                    <td nowrap colspan="3">
                        <asp:TextBox ID="BrokerProcessor_EmailAddr" runat="server" Width="232px" onkeyup="Page_OnValidate();"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="BrokerProcessor_EmailValidator" runat="server" CssClass="RequiredFieldValidator"
                            ControlToValidate="BrokerProcessor_EmailAddr"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator
                                ID="BrokerProcessor_EmailValidator2" runat="server" ControlToValidate="BrokerProcessor_EmailAddr"
                                Display="Dynamic" Font-Bold="True">
                                </asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </td>
      </tr>     
  </asp:PlaceHolder>

</table>

