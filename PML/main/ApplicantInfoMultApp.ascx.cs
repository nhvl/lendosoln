﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using PriceMyLoan.DataAccess;
using PriceMyLoan.UI;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using PriceMyLoan.main;
using MeridianLink.CommonControls;
using System.Text;
using LendersOffice.Constants;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace PriceMyLoan.UI.Main
{
    public partial class ApplicantInfoMultApp : BaseUserControl, ITabUserControl
    {

        protected bool aIsCreditReportOnFile;
        protected Guid m_applicationID = Guid.Empty;
        protected bool m_displayCreditReportButton = false;
        protected bool m_displayPublicRecordsEdit = false;
        protected Guid m_currentAppId = Guid.Empty;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            //opm 27866 fs 03/02/09
            // 3/20/2009 dd - Enable SSN masking for all PML Lender.
            aBSsn.IsMaskEnabled = true;
            aCSsn.IsMaskEnabled = true;

            SetRequiredValidatorMessage(aBFirstNmValidator);
            SetRequiredValidatorMessage(aBLastNmValidator);
            SetRequiredValidatorMessage(aBSsnValidator);
            SetRequiredValidatorMessage(aCFirstNmValidator);
            SetRequiredValidatorMessage(aCLastNmValidator);
            SetRequiredValidatorMessage(aCSsnValidator);
            SetRequiredValidatorMessage(aBEmailValidator);
            SetRequiredValidatorMessage(aCEmailValidator);

            m_appList.Attributes["AlwaysEnable"] = "true";

            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.InitLoad();
            CAppData dataApp = null;

            Guid appId = RequestHelper.GetGuid("appId", Guid.Empty);
            if (appId != Guid.Empty)
            {
                dataApp = dataLoan.GetAppData(appId);
            }

            if (dataApp == null)
            {
                dataApp = dataLoan.GetAppData(0);
            }

            var borrCitizenshipValues = dataApp.GetApplicableCitizenshipValues(E_BorrowerModeT.Borrower);
            Tools.Bind_aProdCitizenT(aProdBCitizenT, borrCitizenshipValues);
            var coborrCitizenshipValues = dataApp.GetApplicableCitizenshipValues(E_BorrowerModeT.Coborrower);
            Tools.Bind_aProdCitizenT(aProdCCitizenT, coborrCitizenshipValues);

            aBExperianScorePe.Attributes.Add("readonly", "readonly");
            aBTransUnionScorePe.Attributes.Add("readonly", "readonly");
            aBEquifaxScorePe.Attributes.Add("readonly", "readonly");

            aCExperianScorePe.Attributes.Add("readonly", "readonly");
            aCTransUnionScorePe.Attributes.Add("readonly", "readonly");
            aCEquifaxScorePe.Attributes.Add("readonly", "readonly");          
        }

        private void CheckExistingReport(Guid applicationID) 
        {

            SqlParameter[] parameters = { new SqlParameter("@ApplicationID", applicationID) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "RetrieveCreditReportWithAccessLevel", parameters)) 
            {
                if (reader.Read()) 
                {
                    // 8/12/2005 dd - OPM #2382 Allow user to view credit report from PML.
                    // 2/12/2007 mf - OPM 10017. We now have a setting for the connection that determines if
                    // the reports from lender-defined CRA can be viewed in PML.  Setting is null when
                    // non-lender-defined CRA was used.
                    m_displayCreditReportButton = true;
                    
                    if (!(reader["AllowPmlUserToViewReports"] is DBNull))
                    {
                        m_displayCreditReportButton = (bool) reader["AllowPmlUserToViewReports"];
                    }
                }
            }
        }
        public string TabID 
        { 
            get { return "APPINFO"; }
        }

        public void LoadData() 
        {
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.InitLoad();

            if (dataLoan.sPml1stVisitApplicantStepD_rep == "") 
            {
                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitApplicantStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();
                dataLoan = new CApplicantInfoData(LoanID);
                dataLoan.InitLoad();
            }
            sFileVersion = dataLoan.sFileVersion;
            //Load proper dataApp
            CAppData dataApp = null;

            Guid AppId = RequestHelper.GetGuid("appId", Guid.Empty);
            if (AppId != Guid.Empty)
            {
                dataApp = dataLoan.GetAppData(AppId);
            }
            if (dataApp == null)
                dataApp = dataLoan.GetAppData(0);

            m_currentAppId = dataApp.aAppId;
            Page.ClientScript.RegisterHiddenField("currentAppId", dataApp.aAppId.ToString());

            //Populate dropdown box accordingly
            m_appList.Items.Clear();
            List<string> invalidApps = new List<string>();
            int appIndex = 0;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dApp = dataLoan.GetAppData(i);

                string name = (dApp.aAppFullNm.Length == 0) ? "Application " + (i + 1) + " of " + dataLoan.nApps : dApp.aAppFullNm;
                ListItem li = new ListItem(name, dApp.aAppId.ToString());
                if (dataApp.aAppId == dApp.aAppId)
                {
                    li.Selected = true;
                    appIndex = i + 1;
                }
                m_appList.Items.Add(li);

                if (!isValidApp(dApp))
                {
                    invalidApps.Add("'" + dApp.aAppId + "'");
                }
            }

            if (invalidApps.Count != 0)
            {
                Page.ClientScript.RegisterArrayDeclaration("invalidApps", String.Join(",", invalidApps.ToArray()));
            }

            m_appTitle.Text = "Application #" + appIndex + ((dataLoan.nApps == 1) ? "" : (" of " + dataLoan.nApps));

            m_applicationID = dataApp.aAppId;
            CheckExistingReport(dataApp.aAppId);

            aBExperianScorePe.Text                 = dataApp.aBExperianScorePe_rep;
            aBTransUnionScorePe.Text               = dataApp.aBTransUnionScorePe_rep;
            aBEquifaxScorePe.Text                  = dataApp.aBEquifaxScorePe_rep;
            aCExperianScorePe.Text                 = dataApp.aCExperianScorePe_rep;
            aCTransUnionScorePe.Text               = dataApp.aCTransUnionScorePe_rep;
            aCEquifaxScorePe.Text                  = dataApp.aCEquifaxScorePe_rep;
            aBFirstNm.Text                         = dataApp.aBFirstNm;
            aBMidNm.Text                           = dataApp.aBMidNm;
            aBLastNm.Text                          = dataApp.aBLastNm;
            aBSuffix.Text                          = dataApp.aBSuffix;
            aBSsn.Text                             = dataApp.aBSsn;
            aCFirstNm.Text                         = dataApp.aCFirstNm;
            aCMidNm.Text                           = dataApp.aCMidNm;
            aCLastNm.Text                          = dataApp.aCLastNm;
            aCSuffix.Text                          = dataApp.aCSuffix;
            aCSsn.Text                             = dataApp.aCSsn;
            aBHasSpouse.Checked                    = dataApp.aBHasSpouse;
            sIsSelfEmployed.Checked                = dataApp.aBIsSelfEmplmt;
            sIsCSelfEmployed.Checked               = dataApp.aCIsSelfEmplmt;

            sOpNegCfPe.Text                        = dataApp.aOpNegCfPe_rep;
            sTransmOMonPmtPe.Text                  = dataApp.aTransmOMonPmtPe_rep;

            aBEmail.Text                           = dataApp.aBEmail;   //opm 34190 fs 08/17/09
            aCEmail.Text                           = dataApp.aCEmail;   //opm 34190 fs 08/17/09


            aIsCreditReportOnFile = dataApp.aIsCreditReportOnFile;

            // 02/27/07 OPM 9023 - Show edit link if there is public record on file
            m_displayPublicRecordsEdit = dataApp.aPublicRecordCollection.CountRegular > 0;

            Tools.SetDropDownListValue(aProdBCitizenT, dataApp.aProdBCitizenT);
            Tools.SetDropDownListValue(aProdCCitizenT, dataApp.aProdCCitizenT);

            main _parentPage = this.Page as main;
            if (null != _parentPage)
                _parentPage.UpdateLoanSummaryDisplay(dataLoan);

            aBTotalScoreIsFthb.Checked = dataApp.aBTotalScoreIsFthb;
            aBTotalScoreIsFthb.Enabled = !dataApp.aBTotalScoreIsFthbReadOnly;
            aCTotalScoreIsFthb.Checked = dataApp.aCTotalScoreIsFthb;
            aCTotalScoreIsFthb.Enabled = !dataApp.aCTotalScoreIsFthbReadOnly;
            aBHasHousingHist.Checked = dataApp.aBHasHousingHist;
            aCHasHousingHist.Checked = dataApp.aCHasHousingHist;

            if (dataLoan.sLPurposeT != E_sLPurposeT.Purchase)
            {
                aBTotalScoreIsFthbContainer.Visible = false;
                aCTotalScoreIsFthbContainer.Visible = false;
                aBHasHousingHistContainer.Visible = false;
                aCHasHousingHistContainer.Visible = false;
            }

            if (!dataApp.aBTotalScoreIsFthb)
                aBHasHousingHistContainer.Style.Add("display", "none");

            if (!dataApp.aCTotalScoreIsFthb)
                aCHasHousingHistContainer.Style.Add("display", "none");
            
        }

        private void Save1003Application()
        {
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);

            if (m_currentAppId == Guid.Empty)
                throw new CBaseException("", "");

            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);
            dataApp.aBExperianScorePe_rep = aBExperianScorePe.Text;
            dataApp.aBTransUnionScorePe_rep = aBTransUnionScorePe.Text;
            dataApp.aBEquifaxScorePe_rep = aBEquifaxScorePe.Text;
            dataApp.aCExperianScorePe_rep = aCExperianScorePe.Text;
            dataApp.aCTransUnionScorePe_rep = aCTransUnionScorePe.Text;
            dataApp.aCEquifaxScorePe_rep = aCEquifaxScorePe.Text;

            dataApp.aBFirstNm = aBFirstNm.Text;
            dataApp.aBMidNm = aBMidNm.Text;
            dataApp.aBLastNm = aBLastNm.Text;
            dataApp.aBSuffix = aBSuffix.Text;
            if (aBSsn.IsUpdated)
            {
                dataApp.aBSsn = aBSsn.ClearText;
            }
            dataApp.aCFirstNm = aCFirstNm.Text;
            dataApp.aCMidNm = aCMidNm.Text;
            dataApp.aCLastNm = aCLastNm.Text;
            dataApp.aCSuffix = aCSuffix.Text;
            if (aCSsn.IsUpdated)
            {
                dataApp.aCSsn = aCSsn.ClearText;
            }

            dataApp.aBIsSelfEmplmt = sIsSelfEmployed.Checked;
            dataApp.aCIsSelfEmplmt = sIsCSelfEmployed.Checked;

            dataApp.aOpNegCfPe_rep = sOpNegCfPe.Text;
            dataLoan.UpdateSOpNegCfPe();

            dataApp.aProdBCitizenT = (E_aProdCitizenT)Tools.GetDropDownListValue(aProdBCitizenT);
            dataApp.aProdCCitizenT = (E_aProdCitizenT)Tools.GetDropDownListValue(aProdCCitizenT);

            dataApp.aBEmail = aBEmail.Text;   //opm 34190 fs 08/17/09
            dataApp.aCEmail = aCEmail.Text;   //opm 34190 fs 08/17/09

            dataApp.aBTotalScoreIsFthb = aBTotalScoreIsFthb.Checked;
            dataApp.aCTotalScoreIsFthb = aCTotalScoreIsFthb.Checked;

            dataApp.aBHasHousingHist = aBHasHousingHist.Checked;
            dataApp.aCHasHousingHist = aCHasHousingHist.Checked;


            dataLoan.InitNewLoanForPml();
            dataLoan.Save();

        }

        public void setNextAppId()
        {
            if (IsReadOnly && Page.IsPostBack)
            {
                string sUserAction = Request.Form["__EVENTARGUMENT"];
                Guid appId = Guid.Empty;
                try { appId = new Guid(Request.Form[m_appList.UniqueID]); }
                catch { }
                if (sUserAction != null && sUserAction == "switch")
                {
                    setParentAppId(appId);
                }
            }

        }

        public void SaveData() 
        {
            //Decide what to do based on the user action
            string sUserAction = Request.Form["__EVENTARGUMENT"];
            Guid appId = Guid.Empty;

            try
            {
                appId = new Guid(Request.Form[m_appList.UniqueID]);
            }
            catch { }
            m_currentAppId = Guid.Empty;
            try
            {
                m_currentAppId = new Guid(Request.Form["currentAppId"]);
            }
            catch { }

            if (String.IsNullOrEmpty(sUserAction))
                sUserAction = "next";   //order, save, switch, add

            try
            {
                switch (sUserAction)
                {
                    case "switch":
                        Save1003Application();
                        break;
                    default:
                        Save1003Application();
                        appId = Guid.Empty;
                        break;
                }
            }
            catch (Exception)
            {
                appId= m_currentAppId;
                throw;
            }
            finally
            {
                setParentAppId(appId);
            }
            
        }

        private bool setParentAppId(Guid app)
        {
            main _parentPage = this.Page as main;
            if (null != _parentPage && app != Guid.Empty)
            {
                _parentPage.m_AppId = app;
                return true;
            }
            else
                return false;
        }


        private bool isValidApp(CAppData dApp)
        {
            if (dApp == null)
                throw new ArgumentNullException("Invalid app.");

            //All apps should be valid if we are in read-only mode
            if (IsReadOnly)
                return true;

            //If e-mail is not required
            if (!CurrentBroker.IsaBEmailRequiredInPml)
            {
                if (dApp.aBEmail.Length != 0 && !Regex.IsMatch(dApp.aBEmail, ConstAppDavid.EmailRegexValidator))
                    return false;

                if (dApp.aBHasSpouse && dApp.aCEmail.Length != 0 && !Regex.IsMatch(dApp.aCEmail, ConstAppDavid.EmailRegexValidator))
                    return false;
            }
            else //e-mail is required
            {
                //An app is invalid if the e-mail is not valid
                if (!Regex.IsMatch(dApp.aBEmail, ConstAppDavid.EmailRegexValidator))
                    return false;

                //If there's a borrower, his/her e-mail needs to be valid too
                if (dApp.aBHasSpouse && !Regex.IsMatch(dApp.aCEmail, ConstAppDavid.EmailRegexValidator))
                    return false;
            }

            //Otherwise valid application
            return true;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
