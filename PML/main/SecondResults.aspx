<%@ Register TagPrefix="uc1" TagName="GetResults" Src="GetResults.ascx" %>

<%@ Page Language="c#" CodeBehind="SecondResults.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.SecondResults" %>

<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Second Loan Program Results</title>
</head>
<body ms_positioning="FlowLayout" onbeforeunload="f_onUnload();">
    <script language="javascript" type="text/javascript">

		<!--
    var g_startRenderPage = (new Date()).getTime();		
    var _popupWindow = null;
    var _nameOfChild = null;
        
    if (typeof (initializeAngular) != "undefined") {
        initializeAngular();
    }

    function _init()
    {
        if(window.opener) {
            window.opener._popupWindow = window;
        }
        
        f_registerMe('secondresults'); 
			

      <% if (m_hasError)
        { %>
        f_displayError(<%= AspxTools.JsString(m_errorMessage) %>);
        return;
        <% } %>
      
        <% if (!Page.IsPostBack)
        { %>
            <% if (!EnablePmlUIResults3)
        { %>
        f_displayWaiting();
            <%}%>
            
        f_temporaryAcceptFirstLoan();
        <% }
        else
        { %>
        f_captureTime();
        f_displayResult();
        <% } %>
    }

        var g_iTempAcceptFirstLoanMinuteWarningId;
        var g_iTempAcceptFirstLoanTimeoutId;
    		
        function f_temporaryAcceptFirstLoan() {
            var args = {};
            //args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
            //args["_1stProductID"] = <%= AspxTools.JsString(RequestHelper.GetGuid("1stProductID", Guid.Empty)) %>;
            //args["_1stRequestedRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
            //args["_1stRequestedPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
            //args["_1stRateId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;
            //args["_1stVersion"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("version")) %>;
            //args["debugResultStartTicks"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("debugResultStartTicks")) %>;
            //args["renderResultModeT"] = g_renderResultModeT;
      
            args.FirstLienLpId = <%= AspxTools.JsString(RequestHelper.GetGuid("1stProductID", Guid.Empty)) %>;
            args.FirstLienNoteRate = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
      args.FirstLienPoint = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
      args.FirstLienMPmt = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;


      ML.EnablePmlUIResults3 ? requestPricing(args) : f_Underwriting("1", args);

        }

        function f_IsTemporaryAcceptFirstLoanComplete() {
            var args = new Object();
            args["requestid"] = g_sRequestID;
      
            var result = gService.main.call("GetResult_IsAvailable", args);
            if (!result.error) {
                if (result.value["IsAvailable"] == "0") {
                    setTimeout("f_IsTemporaryAcceptFirstLoanComplete();", g_iPollingInterval);
                    return;
                } else {
                    if(ML.EnablePmlUIResults3) {
                        displayResults(g_sRequestID);
                    }
                    else {
                        clearTimeout(g_iTempAcceptFirstLoanTimeoutId);
                        clearTimeout(g_iTempAcceptFirstLoanMinuteWarningId);
        
                        var _result1 = gService.main.call("GetResult_GetErrorMessage", args);
                        if (!_result1.error) {
                            if (_result1.value["ErrorMessage"] != "") {
                                f_displayError(_result1.value["ErrorMessage"]);
                                return;
                            }
                        }
                    }
                }
      
                f_Underwriting("1"); <%-- // E_sLienQualifyModeT.OtherLoan --%>
      
            }
        }

        function f_allowClick()
        {
            return _popupWindow == null || _popupWindow.closed == true;
        }

        function f_updateChildName( sNameOfChild )
        {
            if( sNameOfChild != null && sNameOfChild != "" )
            {
                _nameOfChild = sNameOfChild;
            }
            else
            {
                _nameOfChild = null;
            }
        }

        function f_hideNavigation()
        {
        }
        function f_goToStep3(secondLienMPmt)
        {
            if (typeof(window.opener.f_goToStep3) != 'undefined')
                window.opener.f_goToStep3(secondLienMPmt);
        }
        function f_goToPipeline( bShowCertificate )
        {
            if (typeof(window.opener.f_goToPipeline) != 'undefined') {
                window.opener.f_goToPipeline( bShowCertificate );
            }
                // This is the desired behavior for PML 2.0.
            else if (typeof(window.opener.f_onResultClose) != 'undefined') {
                window.opener.f_onResultClose();
            }
        }
		
        var bResultClosed = false;
        function f_onResultClose()
        {
            // Set flag and close window, this will un-register the current window
            // from the opener, close any child windows if needed, and navigate
            // the parent window.
            bResultClosed = true;
            window.close();
        }
		
        function f_onUnload()
        {
            f_registerMe('');
            if(window.opener)
            {
                window.opener._popupWindow = null;
            }

            if( _nameOfChild != null )
            {
                if( _nameOfChild == "printview" )
                {
                    if( _popupWindow != null && _popupWindow.closed == false )
                    {
                        _popupWindow.close();
                    }
                }
                else if( _popupWindow != null && _popupWindow.closed == false )
                {
                    _popupWindow.close();
                }
            }
	        
		    <%-- // 08/30/06 OPM 7168 mf.  If error occured, we must go to step 3 --%>
      if ( $('#'+'ErrorDiv').css('display') == '' )
          f_goToStep3();
      else if (bResultClosed)
          f_goToPipeline(false);
  }
  function f_displayError(errMsg) {
      $('#'+'ResultDiv').css('display', 'none');
      $('#'+'ErrorDiv').css('display', '');
      $('#'+'SecondResultErrorMessage').html(errMsg);
  }
  function f_registerMe( sName ) {
      if( window.opener != null) {
          if( window.opener.closed == false )
          {
              if( window.opener.f_updateChildName != null )
              {
                  window.opener.f_updateChildName( sName );
              }
          }
      }
  }
		<% if (IsEncompassIntegration)
        { %>
        function f_sendToEncompass() {
            if (typeof(window.opener.f_sendToEncompass) != 'undefined') {
                window.opener.f_sendToEncompass(  );
            }
            self.close();
        }
        <% } %>
        //-->

    </script>
    <form id="SecondResults" method="post" runat="server">
        <div id="ResultDiv">
            <uc1:GetResults ID="GetResults1" runat="server"></uc1:GetResults>


            <div runat="server" id="PricingResultsContainer" ng-app="pricingResults" ng-controller="pricingResultsController">
                <div id="LoadingMessage">
                    <img id="resultsLoadingGif" src="../images/loading.gif" />
                    <p>Loading...</p>
                </div>
                <div id="ResultsContainer"></div>
            </div>
        </div>

        <div id="ErrorDiv" style="font-color: red; display: none">
            <table width="100%" height="100%">
                <tr>
                    <td valign="center" align="center">
                        <div id="SecondResultErrorMessage" style="color: red; font-weight: bold"></div>
                        <br />
                        <br />
                        <input type="button" value="Close" onclick="self.close();" />

                    </td>
                </tr>
            </table>

        </div>

    </form>

</body>
<script language="javascript" type="text/javascript">
    var g_endRenderPage = (new Date()).getTime();
</script>
</html>
