﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuickPricer.ascx.cs" Inherits="PriceMyLoan.main.QuickPricer" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="PriceMyLoan.Common"%>
<%@ Import Namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>


<style>
.hidden { display: none }
a.ineligibleLoanProgramsLink
{
    color:#ff9933;    
}
a.ineligibleLoanProgramsLink:visited
{
    color:#ff9933;    
}
a.ineligibleLoanProgramsLink:hover
{
    color:blue;    
}
</style>
<script type="text/javascript">
<!--
var gsLtv80TestResultT = <%= AspxTools.JsNumeric((int) sLtv80TestResultT) %>;

$j(function() { 
  f_uc_init();
});

function f_uc_init()
{
  // Initialize
    f_onchange_sLPurposeTPe(true);
    
    if (!f_onValidateRequiredFields())
        <%= AspxTools.JsGetElementById(sLPurposeTPe) %>.focus();
    else
        document.getElementById("PriceBtn").focus();
 
    f_show2ndFinancing();    
    
    <% if (m_isAutoPmiOrNewPml ) { %>
    f_setsProdConvMIOptionT(gsLtv80TestResultT, true );  
    f_showSplitRow();
    <% } else { %>
    f_setsProdMIOptionT(gsLtv80TestResultT, true );  
    <% } %>

}  
function f_sSpState_onchange() {
  var sSpState = <%= AspxTools.JsGetElementById(sSpStatePe) %>;
  var sSpCounty = <%= AspxTools.JsGetElementById(sSpCounty) %>;
  
  if (sSpState.value != "") {
      UpdateCounties(sSpState, sSpCounty); 
      sSpState.oldValue = sSpState.selectedIndex;
  }
  else 
  {
      sSpCounty.options.length = 0;
      sSpCounty.oldValue = -1;
	  sSpState.oldValue = sSpState.selectedIndex;
    }

  f_onValidateRequiredFields();
}

  function f_onchange_sLPurposeTPe(bInit) {
    var $ddl = <%= AspxTools.JQuery(sLPurposeTPe) %>;
  var isPurchase = $ddl.val() == <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %>;
  var isRefiRateTerm = $ddl.val() == <%= AspxTools.JsString(E_sLPurposeT.Refin.ToString("D")) %>;
  var isVaIRRRL = $ddl.val() == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>;
  
  //var $houseValueLabel = $j("#HouseValueLabel");
  //var equityLabel = document.getElementById("DownPaymentLabel");
  //var oDURefiPlus = document.getElementById("rowDURefiPlus");
  var $cbDURefiPlus = <%= AspxTools.JQuery(sProdIsDuRefiPlus) %>;
  var $sProdIncludeNormalProc = <%= AspxTools.JQuery(sProdIncludeNormalProc) %>;
  var $sProdIncludeVAProc = <%= AspxTools.JQuery(sProdIncludeVAProc) %>;
  //var $sAppraisalValPePanel = $j('#AppraisalValuePanel');
  var $sApprValPeValidator = <%= AspxTools.JQuery(sApprValPeValidator) %>; 
  
  $j("#HouseValueLabel").text (isPurchase ? "Sales Price" : "Home Value");  
  $j("#DownPaymentLabel").text( isPurchase ? "Down Payment" : "Equity");
  
  $j('#rowDURefiPlus').toggle(isRefiRateTerm);
  
  //oDURefiPlus.style.display = isRefiRateTerm ? "" : "none";
  if (!isRefiRateTerm){
    $cbDURefiPlus.prop('checked' , false);
    $sProdIncludeNormalProc.prop('disabled', false);
  }

  if (isVaIRRRL){
    $sProdIncludeVAProc.prop({
        'checked' : true, 
        'disabled' : true
        });
  }
  else
  {
    $sProdIncludeVAProc.prop('disabled', false);
  }
  $j('#AppraisalValuePanel').toggle(isPurchase);
  //sAppraisalValPePanel.style.display = isPurchase ? '' : 'none';
  $sApprValPeValidator.attr('IsEnabled', isPurchase); 
  
  if (!bInit){
    f_onValidateRequiredFields();
    document.getElementById("sProdCalcEntryT").value = $ddl.attr('sProdCalcEntryT'); <%-- We need to refresh the calculations when the switch from purchse to something else for opm 32746 av   --%>
    Calc.rcall(); 
  }
}

function f_onValidateRequiredFields() {
  var ddl = <%= AspxTools.JsGetElementById(sSpStatePe) %>;
  var validator = document.getElementById("sSpStateValidator");
  var bSpStateValid = ddl.value != "";
  validator.style.display = (bSpStateValid || <%= AspxTools.JsBool(IsReadOnly) %>) ? "none" : "";
  
  var countyIsValid  = <%= AspxTools.JsGetElementById(sSpCounty) %>.selectedIndex != -1;  <%--AS of opm 18913 County is required--%>
  document.getElementById("sSpCountyValidator").style.display = ( countyIsValid || <%= AspxTools.JsBool(IsReadOnly) %> ) ? "none" : "";
  
  var bValid = true;
  if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
    bValid = Page_ClientValidate();

  var isPageValid = bSpStateValid && countyIsValid && bValid;
  f_enableBtnById('PriceBtn', isPageValid && !<%= AspxTools.JsBool(IsReadOnly) %>);
  
  return isPageValid;
}

function f_enableBtnById(id, bEnabled) {
    var btn = document.getElementById(id);
    if (null != btn) {
      btn.disabled = !bEnabled;
    }
}

function f_validateCScore(source, args) {
  var o = <%= AspxTools.JsGetElementById(sCreditScoreType1) %>;
  if (null != o) {
    if (o.value == "")
      args.IsValid = false;
    else {
      try {
        var _v = parseInt(o.value);
        args.IsValid = _v >= 0;
      } catch (e) {
        args.IsValid = false;
      }
    }
  }
}

var Calc = {
  
  Pct: {
  onchange: function(obj){
    if (obj == null)
        return;

    f_showErrorMsgs(obj, 1);
    
    var $obj = $j(obj);
    var isNegativeAllow = $obj.attr('NegVal') != null;
    var isNegativeValue = $obj.val().indexOf("-") >= 0 || $obj.val().indexOf("(") >= 0;

    if ($obj.val() == "" || $obj.val() == "0") {
        $obj.val("0.000%");
    }
    if (isNegativeAllow == false && isNegativeValue) {
        $obj.val("0.000%");
    }
    if ( typeof(format_percent) == 'function' )
        format_percent(obj);

    if (typeof(obj.sProdCalcEntryT) != 'undefined' && obj.sProdCalcEntryT != null)
        document.getElementById("sProdCalcEntryT").value = obj.sProdCalcEntryT;
    
    var _sProdCalcEntryT = $obj.attr('sProdCalcEntryT');
    if (_sProdCalcEntryT != null)
          document.getElementById("sProdCalcEntryT").value = _sProdCalcEntryT;    
    
    Calc.rcall();
    
    f_showErrorMsgs(obj, 2);
    Calc.validate();    
  },
    
  onkeyup: function(obj) {
    if (obj == null)
        return;
  
    var $obj = $j(obj);
    if ($obj.val() == "" || $obj.val() == "0") {
        return;
    }
    if ($obj.val().indexOf("-") >= 0 || $obj.val.indexOf("(") >= 0) {
        return;
    }
  
    var fl = parseFloat($obj.val().replace(/%$/i, ""));
    if (isNaN(fl))
        return;
    
    Calc.validate();
   }
  
  },
  
  Money: {
    onchange: function(obj){
        if (typeof obj == 'undefined' || obj == null)
            return;
        
        f_showErrorMsgs(obj, 1);
  
        var $obj = $j(obj);
        
        var isNegativeAllow = $obj.attr('NegVal') != null;
        
        var isNegativeValue = $obj.val().indexOf("-") >= 0 || $obj.val().indexOf("(") >= 0;
        
        if ($obj.val() === "" || $obj.val() === "0") {
            $obj.val("$0.00");
        }
        if (isNegativeAllow == false && isNegativeValue) {
            $obj.val("$0.00");
        }
        if ( typeof format_money  != 'undefined' )
        {
            format_money(obj);
        }

        var _sProdCalcEntryT = $obj.attr('sProdCalcEntryT');
        if (_sProdCalcEntryT != null)
          document.getElementById("sProdCalcEntryT").value = _sProdCalcEntryT;
    
        Calc.rcall();
        
        f_showErrorMsgs(obj, 2);
        
        Calc.validate();
  
    },
    
    onkeyup: function(obj){
        if (obj == null)
            return;
  
        var $obj = $j(obj);
        if ($obj.val() == "" || $obj.val() == "0") {
            return;
        }
        if ($obj.val().indexOf("-") >= 0 || $obj.val().indexOf("(") >= 0) {
            return;
        }
  
        var fl = parseFloat($obj.val());
        if (isNaN(fl))
            return;
        Calc.validate();
    }
  
  },
  
  RateLockPeriod: {
    onchange: function(obj){
        Calc.rcall();
    }
  
  },
  
  validate: function(){
    if ( (typeof(f_onValidateRequiredFields) == 'object') || (typeof(f_onValidateRequiredFields) == 'function') )
        return f_onValidateRequiredFields();
  },
  
  rcall: function(){
    var args = f_getAllFormValues();

    var result = gService.main.call("CalculateData", args);
    if (!result.error) {
        if (result.value["SessionExpired"] == "True") {
            f_onSessionExpired();
            return;
        }
        else if (typeof(result.value["ErrorMessage"]) != "undefined") {
            f_displayErrorMessage(result.value["ErrorMessage"]);
            return;
        }
        else{
            populateForm(result.value, <%= AspxTools.JsGetClientIdString(this) %>);
            f_setsProdRLckdExpiredDLabel( result.value["sProdRLckdExpiredDLabel"]);
            
            <% if (m_isAutoPmiOrNewPml ) {  %>
            f_setsProdConvMIOptionT( result.value["sLtv80TestResultT"], false );
            f_showSplitRow();
            <% } else { %>
            f_setsProdMIOptionT( result.value["sLtv80TestResultT"], false );
            <% } %>
        }
    }
  }
  
 };
 
 
 function f_showErrorMsgs(obj, phase)
 {
    var l1Amt = <%= AspxTools.JsGetElementById(sLAmtCalcPe) %>;
    var l1Pct = <%= AspxTools.JsGetElementById(sLtvRPe) %>;
    var l2Amt = <%= AspxTools.JsGetElementById(sProOFinBalPe) %>
    var l2Pct = <%= AspxTools.JsGetElementById(sLtvROtherFinPe) %>;
    
    var l1ErrMsg = document.getElementById("neg1stLien");
    var l2ErrMsg = document.getElementById("neg2ndLien");
    var SalePrice = <%= AspxTools.JsGetElementById(sHouseValPe) %>;
    var spErrMsg = document.getElementById("negHouseVal");
    var houseLabelText = $j("#HouseValueLabel").text();
    var sApprValPe = document.getElementById('<%= AspxTools.ClientId(sApprValPe) %>').value; 
    var sApprValNeg = document.getElementById('negApprVal');
    
    var DwnPmtPct = <%= AspxTools.JsGetElementById(sDownPmtPcPe) %>;
    var DwnPmtAmt = <%= AspxTools.JsGetElementById(sEquityPe) %>;
    var DwnPmtPctInt = 0;
    DwnPmtPctInt = parseFloat(DwnPmtPct.value.replace('%',''));
    
    try{
        switch(phase){
            case 1:
                $j(spErrMsg).text("Negative " + houseLabelText + " is not allowed.");
                spErrMsg.style.display = SalePrice.value.match("-") ? "inline-block" : "none";
                sApprValNeg.style.display = sApprValPe.match("-") ? "inline-block" : "none";
                if (typeof(obj) != undefined && obj != null)
                {
                  <%-- //Show error messages if negative values are entered --%>
                  if ( (obj == l1Amt && l1Amt.value.match("-")) || (obj == l1Pct && l1Pct.value.match("-")) )
                    l1ErrMsg.style.display = "inline-block";
                  else l1ErrMsg.style.display = "none";
                  
                  if ( (obj == l2Amt && l2Amt.value.match("-")) || (obj == l2Pct && l2Pct.value.match("-")) )
                    l2ErrMsg.style.display = "inline-block";
                  else l2ErrMsg.style.display = "none";
                }
                break;
                
            case 2:
                if (typeof(obj) != undefined && obj != null)
                {
                 <%-- //Show error message if down payment is bigger than sales price --%>
                 if ( (obj == DwnPmtPct || obj == DwnPmtAmt || obj == SalePrice) && !isNaN(DwnPmtPctInt) && DwnPmtPctInt > 100)
                     errDownPmt.style.display = "inline-block";
                   else errDownPmt.style.display = "none";
                 }
                break;
        }
    }catch(e){
    }    
    
 }

function f_onCreditScoreChange()
{
  Calc.validate();
}
$j(document).keydown(f_quickSubmitDebug);
function f_quickSubmitDebug(event)
{
    // ctrl+alt+D
    if (event.ctrlKey && event.altKey && event.which == 68) {
        if(f_submitPrice(true))
        {
            alert('An attempt to submit to QP is being made.');
        }
        else{
            alert('No attempt to submit to QP was made.  Please enter valid pricing parameters.');
        }
    }
}

    
function f_submitPrice(isDebugMode) {

  if (!f_onValidateRequiredFields()) return false;
  f_clearResult();
  document.getElementById("PriceBtn").disabled = true;
  var isToUseDebugMode = (typeof(isDebugMode) == 'undefined' ? false : isDebugMode)
  setTimeout(function(){__submitPrice(isToUseDebugMode);}, 500); 
  return true;
}    
function __submitPrice(isDebugMode) {
  g_requestID = null;
  g_timingTicks = 0;
  var args = f_getAllFormValues();
  args["IsDebugMode"] = typeof(isDebugMode) == 'undefined' ? false : isDebugMode;
  
  var result = gService.main.call("SubmitQuickPrice", args);
  if (!result.error) {
    if (result.value["SessionExpired"] == "True") {
      f_onSessionExpired();
      return;
    }
    else if (typeof(result.value["ErrorMessage"]) != "undefined") {
        f_displayErrorMessage(result.value["ErrorMessage"]);
        return;
      }
    g_requestID = result.value["RequestID"];
    g_timingTicks = result.value["Timing"];
    if(args["IsDebugMode"])
    {
        setTimeout("f_isResultReadyWithDebugModeTrue();", g_iPollingInterval);
    }
    else
    {
        setTimeout("f_isResultReady();", g_iPollingInterval);
    }
  }

}
function f_onSessionExpired() {
  <% if (PriceMyLoanConstants.IsEmbeddedPML) { %>
    self.location = <%= AspxTools.JsString(ResolveUrl("~/SessionExpired.aspx")) %>;
  <% } else { %>
    self.location = <%= AspxTools.JsString(ResolveUrl(PriceMyLoanConstants.LogoutUrl))  %>;
  <% } %>
}
var g_requestID = null;
var g_timingTicks = 0;
var g_iPollingInterval = 3000; // 3 seconds

function f_isResultReadyWithDebugModeTrue()
{
    f_isResultReady(true);
}
function f_isResultReady(isDebugMode) {
  var args = new Object();
  args["RequestID"] = g_requestID;
  args["Timing"] = g_timingTicks;
  args["IsDebugMode"] = typeof(isDebugMode) == 'undefined' ? false : isDebugMode;
  var result = gService.main.call("IsResultReady", args);
  if (!result.error) {
    if (result.value["IsReady"] == "False") {
      setTimeout("f_isResultReady();", g_iPollingInterval);
      return;
    } else {
      document.getElementById("PriceBtn").disabled = false;
      if (typeof(result.value["ErrorMessage"]) != "undefined") {
        f_displayErrorMessage(result.value["ErrorMessage"]);
        return;
      }
      f_renderResult(result.value["Result"], result.value["IneligibleProductList"]);
    
    }
  }
  
  
}
function f_getAllFormValues() 
{
  var args = getAllFormValues(<%= AspxTools.JsGetClientIdString(this) %>);
  args["sProdCalcEntryT"] = document.getElementById("sProdCalcEntryT").value;
  args["QuickPricerTemplateId"] = <%= AspxTools.JsString(m_quickPricerTemplateId) %>;
  args["sLtv80TestResultT"] = '' + gsLtv80TestResultT;
 <% if (!PriceMyLoanConstants.IsEmbeddedPML) { %>
  args["CreateLoan_BorrFirstName"] = document.getElementById("CreateLoan_BorrFirstName").value;
  args["CreateLoan_BorrLastName"] = document.getElementById("CreateLoan_BorrLastName").value;
  args["CreateLoan_BorrSsn"] = document.getElementById("CreateLoan_BorrSsn").value;
  <% } %>

  <% if (m_isAnonymous) { %>
  args["LenderPmlSiteId"] = <%= AspxTools.JsString(RequestHelper.GetGuid("lenderpmlsiteid", Guid.Empty)) %>;
  args["AnonymousId"] = <%= AspxTools.JsString(RequestHelper.GetGuid("anonymousid", Guid.Empty)) %>;
  <% } %>

  return args;
}
<% if(m_allowCreateLoan) { %>
function f_createLoan() {
<% if (!PriceMyLoanConstants.IsEmbeddedPML) { %>
  f_toggleCreate(true);
  <% } else { %>
  f_createLoanCore()
  <% } %>
}
function f_createLoanCore()
{
    f_clearResult();
    setTimeout("_createLoan();", 300);
}

function _createLoan() {
  var sLId;

  var args = f_getAllFormValues();
  
  var result = gService.main.call("ConvertToLoan", args);
  if (!result.error) {
    sLId = result.value["sLId"];
    <% if (!PriceMyLoanConstants.IsEmbeddedPML) { %>
      location.href = <%= AspxTools.JsString(VirtualRoot) %> + '/main/agents.aspx?loanid=' + sLId;
    <% } else { %>
      parent.opener.editLoan(sLId);
      parent.close();
      <% } %>
    
    return;
  } else {
    f_displayErrorMessage(<%= AspxTools.JsString(ErrorMessages.Generic) %>);
  }

}
<% } %>
function f_clearResult() {
  //var oHtmlDiv = f_getResultPanel();
  var statusGif = <%= AspxTools.JsString(VirtualRoot) %> + '/images/status.gif';
  $j('#ResultHtml').html( "<div class='QuickPricerWaitingIndicator'>Processing your request ...<br/><img src='" + statusGif + "'/></div>");
}
function f_renderResult(result, ineligibleResult) {
  var oHtmlDiv = f_getResultPanel();
  var oData = jQuery.parseJSON(result);      
  var bAllowCreateLoan = <%= AspxTools.JsBool(m_allowCreateLoan) %>;
  QuickPricer.RenderResult(oHtmlDiv, oData, <%=AspxTools.JsString(m_quickPricerNoResultMessage) %>,bAllowCreateLoan);
  
  var oIneligibleList = $j.parseJSON(ineligibleResult);
  var length = !!oIneligibleList ? oIneligibleList.length : 0;
  
  if (length > 0)
  {
    var html = '<div class="ineligibleSectionHeader"><a href="javascript:void(0)" class="ineligibleLoanProgramsLink">\
    <span class="ineligibleLoanProgramsHideText">- Hide</span>\
    <span class="ineligibleLoanProgramsDisplayText">+ Display</span> Ineligible Loan Programs</a></div>';
    html += '<ul class="ineligibleList ineligibleLoanProgramsTD">';
    for (var i = 0; i < length; i++)
    {
      var program = oIneligibleList[i];
      html += '<li><div class="name">' + program.Name + '</div><div class="reason">';
      
      var reasonIdx = program.Reasons.length;
      for(var j = 0; j < reasonIdx; j++)
      {
        html += program.Reasons[j] + '<br>';
      }
      html += '</div></li>';
    }
    html += '</ul>';
    var ineligibleDiv= $j(html);
  	$j(oHtmlDiv).append(ineligibleDiv);
  	$j('.ineligibleLoanProgramsLink').click(toggleIneligibleLoanPrograms);
    var bHasNoRate = true;
    var oLoanProgramList = oData[2];
    for (var i = 0; i < oLoanProgramList.length; i++)
    {
        var oLoanProgram = oLoanProgramList[i];
        var sLoanProgramName = oLoanProgram[0];
        var bIsRateExpired = oLoanProgram[1] == '1';
        var oRateOptions = oLoanProgram[2];
        if (oRateOptions.length == 0)
        {
            continue; // Skip render 0 rate.
        }
        bHasNoRate = false;            
    }        
    if(bHasNoRate)
    {
        $j('.ineligibleLoanProgramsDisplayText').hide();        
    }
    else{
        $j('.ineligibleLoanProgramsHideText').hide(); 
        $j('.ineligibleLoanProgramsTD').hide();
    }
  }
  
}
function toggleIneligibleLoanPrograms()
  {
    $j('.ineligibleLoanProgramsHideText').toggle();
    $j('.ineligibleLoanProgramsDisplayText').toggle();
    $j('.ineligibleLoanProgramsTD').toggle();
  }
function f_displayErrorMessage(msg) {
  $j('#ResultHtml').empty();
  $j('<div></div>').addClass('QuickPricerError').text(msg).appendTo('#ResultHtml');
}
function f_getResultPanel() {
  var oDiv = document.getElementById("ResultHtml");
  if (null == oDiv) {
    alert('Unable to render result');
  }
  return oDiv;
}

function f_validatesProdFilterDue(source, args)
{
  var sProdFilterDue10Yrs = <%= AspxTools.JsGetElementById(sProdFilterDue10Yrs) %>;
  var sProdFilterDue15Yrs = <%= AspxTools.JsGetElementById(sProdFilterDue15Yrs) %>;
  var sProdFilterDue20Yrs = <%= AspxTools.JsGetElementById(sProdFilterDue20Yrs) %>;
  var sProdFilterDue25Yrs = <%= AspxTools.JsGetElementById(sProdFilterDue25Yrs) %>;
  var sProdFilterDue30Yrs = <%= AspxTools.JsGetElementById(sProdFilterDue30Yrs) %>;  
  var sProdFilterDueOther = <%= AspxTools.JsGetElementById(sProdFilterDueOther) %>;
  
  if ( sProdFilterDue10Yrs.checked == true || sProdFilterDue15Yrs.checked == true
    || sProdFilterDue20Yrs.checked == true || sProdFilterDue25Yrs.checked == true
    || sProdFilterDue30Yrs.checked == true || sProdFilterDueOther.checked == true )
    args.IsValid = true;
  else
    args.IsValid = false;
}
function f_validatesProdFilterFinMeth(source, args)
{
  var sProdFilterFinMethFixed = <%= AspxTools.JsGetElementById(sProdFilterFinMethFixed) %>;
  var sProdFilterFinMeth3YrsArm = <%= AspxTools.JsGetElementById(sProdFilterFinMeth3YrsArm) %>;
  var sProdFilterFinMeth5YrsArm = <%= AspxTools.JsGetElementById(sProdFilterFinMeth5YrsArm) %>;
  var sProdFilterFinMeth7YrsArm = <%= AspxTools.JsGetElementById(sProdFilterFinMeth7YrsArm) %>;
  var sProdFilterFinMeth10YrsArm = <%= AspxTools.JsGetElementById(sProdFilterFinMeth10YrsArm) %>;
  var sProdFilterFinMethOther = <%= AspxTools.JsGetElementById(sProdFilterFinMethOther) %>;
  
  if ( sProdFilterFinMethFixed.checked == true     
    || sProdFilterFinMeth3YrsArm.checked == true || sProdFilterFinMeth5YrsArm.checked == true 
    || sProdFilterFinMeth7YrsArm.checked == true || sProdFilterFinMeth10YrsArm.checked == true 
    || sProdFilterFinMethOther.checked == true )
    args.IsValid = true;
  else
    args.IsValid = false;

}
function f_validatesProdFilterType(source, args)
{
  var sProdIncludeNormalProc = <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>;
  var sProdIsDuRefiPlus = <%= AspxTools.JsGetElementById(sProdIsDuRefiPlus) %>;
  var sProdIncludeMyCommunityProc = <%= AspxTools.JsGetElementById(sProdIncludeMyCommunityProc) %>;
  var sProdIncludeHomePossibleProc = <%= AspxTools.JsGetElementById(sProdIncludeHomePossibleProc) %>;
  var sProdIncludeFHATotalProc = <%= AspxTools.JsGetElementById(sProdIncludeFHATotalProc) %>;
  var sProdIncludeVAProc = <%= AspxTools.JsGetElementById(sProdIncludeVAProc) %>;
 var sProdIncludeUSDARuralProc = <%= AspxTools.JsGetElementById(sProdIncludeUSDARuralProc) %>;
 if ( sProdIncludeNormalProc.checked == true 
    || sProdIsDuRefiPlus.checked == true
    || sProdIncludeMyCommunityProc.checked == true
    || sProdIncludeHomePossibleProc.checked == true 
    || sProdIncludeFHATotalProc.checked == true 
    || sProdIncludeVAProc.checked == true
    || sProdIncludeUSDARuralProc.checked == true )
    args.IsValid = true;
  else
    args.IsValid = false;

}

    function f_validatesProdFilterPaymentType(source, args)
    {
        var sProdFilterPmtTPI = <%= AspxTools.JsGetElementById(sProdFilterPmtTPI) %>;
        var sProdFilterPmtTIOnly = <%= AspxTools.JsGetElementById(sProdFilterPmtTIOnly) %>;
 
        if ( sProdFilterPmtTPI.checked == true 
          || sProdFilterPmtTIOnly.checked == true ) {
              args.IsValid = true;
          } else {
            args.IsValid = false;
          }
    }


function f_validateLckdDays(source, args) {
  var o = <%= AspxTools.JsGetElementById(sProdRLckdDays) %>;
  if (null != o) {
    if (o.value == "")
      args.IsValid = false;
    else {
      try {
        var _v = parseInt(o.value);
        args.IsValid = _v > 0;
      } catch (e) {
        args.IsValid = false;
      }
    }
  }
}

function f_show2ndFinancing()
{
  var no = <%= AspxTools.JsGetElementById(bNo2ndFinancing) %>;
  var yes = <%= AspxTools.JsGetElementById(bYes2ndFinancing) %>;
  var row = document.getElementById('SecondFinancingRow');
  var l2Pct = <%= AspxTools.JsGetElementById(sLtvROtherFinPe) %>;
  
  if (no.checked)
  {
      row.style.display = 'none';
      if (l2Pct.value != '0.000%')
      {
        l2Pct.value = '0.000%';
        Calc.Pct.onchange(l2Pct);
      }      
  }
  else 
  {
    row.style.display = '';
  }
}

function f_validatesLAmtCalcPe( source, args )
{
  var sLAmtCalcPe = <%= AspxTools.JsGetElementById(sLAmtCalcPe) %>.value;
  args.IsValid = ( parseInt(sLAmtCalcPe.replace(/\$/,'').replace(/,/,'') ) >= 1 );
}

function f_validatesProdMIOptionT( source, arguments )
{
  arguments.IsValid = true; return ;
  var mIDdl = document.getElementById(source.controltovalidate);  
  arguments.IsValid = ( mIDdl.disabled == true || mIDdl.value != <%= AspxTools.JsString(E_sProdMIOptionT.LeaveBlank.ToString("D")) %> );
}

function f_validatesProdConvMIOptionT( source, arguments )
{
  arguments.IsValid = true; return ;
  //var mIDdl = document.getElementById(source.controltovalidate);  
  //arguments.IsValid = ( mIDdl.disabled == true || mIDdl.value != <%= AspxTools.JsString(E_sProdConvMIOptionT.NoMI.ToString("D")) %> );
}

function f_setsProdRLckdExpiredDLabel(result)
{
    var label = <%= AspxTools.JsGetElementById(sProdRLckdExpiredDLabel)%>;
    if (label) {
        label.innerHTML = result;
    }
}

function f_setsProdMIOptionT( ltv80TestResult , bInit )
{
  gsLtv80TestResultT = ltv80TestResult;
  if (! <%= AspxTools.JsBool(IsReadOnly) %>)
  {
    var ddl = <%= AspxTools.JsGetElementById(sProdMIOptionT) %>;
    if ( ddl != null ){
      if ( ltv80TestResult != <%= AspxTools.JsNumeric((int) E_sLtv80TestResultT.Over80) %> ){
        ddl.selectedIndex = 0;
        ddl.style.backgroundColor = gReadonlyBackgroundColor;
        ddl.disabled = true;
      } else {
        ddl.style.backgroundColor = "";
        ddl.disabled = false;
      }
    }
    f_onValidateRequiredFields();
  }
}


function f_setsProdConvMIOptionT( ltv80TestResult , bInit )
{
  gsLtv80TestResultT = ltv80TestResult;
  if (! <%= AspxTools.JsBool(IsReadOnly) %>)
  {
    var ddl = <%= AspxTools.JsGetElementById(sProdConvMIOptionT) %>;
    if ( ddl != null ){
      if ( ltv80TestResult != <%= AspxTools.JsNumeric((int) E_sLtv80TestResultT.Over80) %> ){
        ddl.selectedIndex = ddl.length -1;
        ddl.style.backgroundColor = gReadonlyBackgroundColor;
        ddl.disabled = true;
      } else {
        ddl.style.backgroundColor = "";
        ddl.disabled = false;
      }
    }
    f_onValidateRequiredFields();
  }
}



function f_onclick_DuRefiPlus(o){
 if (typeof o == 'undefined' || o == null || typeof o.checked == 'undefined')
    return;
 
 var sProdIncludeNormalProc = <%= AspxTools.JsGetElementById(sProdIncludeNormalProc) %>;
 
 if (o.checked)
    sProdIncludeNormalProc.checked = true;

 sProdIncludeNormalProc.disabled = o.checked;
 f_onValidateRequiredFields();
}


function getMoneyValue(str){
    var val = null;
    try {
       if (!str) return null;
       if (str.length == 0) return null;
       var _v = str.replace(/\$/,'').replace(/,/,'');
       
       if (_v.match(/^\((.+\))$/)){
            _v = _v.substring(1,_v.length-2);
            _v = "-" + _v;
       }
       if (!isNaN(_v)){
           val = parseFloat(_v);
       }
    } catch (e) {}
    return val;
}

function f_validate_sApprValPe( source, args ) {
    var sApprValPe = <%= AspxTools.JQuery(sApprValPe) %>.val();
    var sApprValPeMoneyValue = getMoneyValue( sApprValPe );
    args.IsValid = sApprValPeMoneyValue >= 0;
}

<% if ( m_isAutoPmiOrNewPml ) { %>
function f_showSplitRow()
{
    var convMi = <%= AspxTools.JsGetElementById(sProdConvMIOptionT) %>;
    var splitRow = document.getElementById('SplitPmiRow');
    if ( convMi != null )
    {
        var isSplit = convMi.value == <%= AspxTools.JsString(E_sProdConvMIOptionT.BorrPaidSplitPrem.ToString("D")) %>;
        
        splitRow.style.display = isSplit ? 'inline' : 'none';
        //alert(<%= AspxTools.JsString(E_sProdConvMIOptionT.BorrPaidSplitPrem.ToString("D")) %>);
    
    }
    
    
    f_onValidateRequiredFields();
}

function f_validatesConvSplitMIRT( source, arguments )
{
    var splitRow = document.getElementById('SplitPmiRow');
    if ( splitRow && splitRow.style.display == 'none' )
    {
        arguments.IsValid = true;
        return;
    }

    var mIDdl = document.getElementById(source.controltovalidate);  
    arguments.IsValid = mIDdl.value != <%= AspxTools.JsString(E_sConvSplitMIRT.Blank.ToString("D")) %> ;
}
<%} %>


    <%if(CurrentBroker.IsEnabledLockPeriodDropDown){ %>
    function f_updateProdRLckdDaysFromDDL(ddl)
    {
        var hiddenTB = <%= AspxTools.JsGetElementById(sProdRLckdDays) %>;
        hiddenTB.value = ddl.value;
        hiddenTB.onchange();            // trigger the calculations that were on the rate lock textbox.
        f_onValidateRequiredFields();   // enable the navigation buttons.
    }
    <%} %>
//-->
</script>

<!--[if IE]>
<style type="text/css">
fieldset.QuickPricerFieldSet label
{
     vertical-align: text-top;
}
fieldset.QuickPricerFieldSet span label
{
    position: relative;
    bottom: 2px;
}
#filtersAndSorting table tr td table tr td input
{
    margin: 0px;
}
</style>
<![endif]-->
<div class="QuickPricerPanel">
<fieldset id="personal" class="QuickPricerFieldSet">
    <legend class="LegendSectionHeader">Quick Pricer</legend>
    <br />
    
    <p>Complete all information on this page to view pricing.</p>
    <br />
    
    <asp:PlaceHolder runat="server" ID="sLeadSrcDescPlaceHolder">
        <ml:EncodedLabel runat="server" AssociatedControlID="sLeadSrcDesc">Lead Source</ml:EncodedLabel>
        <asp:DropDownList runat="server" ID="sLeadSrcDesc"></asp:DropDownList>
        <br />
    </asp:PlaceHolder>
    
    <label for=<%= AspxTools.HtmlAttribute(sLPurposeTPe.ClientID) %>>Loan Purpose </label>
    <asp:dropdownlist id="sLPurposeTPe" tabIndex="1" runat="server" onchange="f_onchange_sLPurposeTPe(false);"  sProdCalcEntryT="7"  width="135px"></asp:dropdownlist>
	<br />
	<br />
	
	<div  id="AppraisalValuePanel">
	    <ml:EncodedLabel runat="server" AssociatedControlID="sApprValPe" >Appraised Value</ml:EncodedLabel>	
	    <ml:MoneyTextBox runat="server" ID="sApprValPe" Width="100px" sProdCalcEntryT="7" TabIndex="3"  onchange="Calc.Money.onchange(this);" onkeyup="Calc.Money.onkeyup(this);"></ml:MoneyTextBox>
	    <asp:CustomValidator runat="server" ID="sApprValPeValidator" ClientValidationFunction="f_validate_sApprValPe"  ControlToValidate="sApprValPe"  CssClass="RequiredValidatorMessage"></asp:CustomValidator>
	    <br />
	    <span id="negApprVal" style="display:   none; color: Red;"> Negative Appraised Value is not allowed.</span>
	</div>
	
	<div>
	    <label id="HouseValueLabel" for=<%= AspxTools.HtmlAttribute(sHouseValPe.ClientID) %>>Home Value</label>
        <ml:moneytextbox id="sHouseValPe" onchange="Calc.Money.onchange(this);" onkeyup="Calc.Money.onkeyup(this);" tabIndex="5" runat="server" width="100px" Text="$0.00" preset="money" sProdCalcEntryT="7"></ml:moneytextbox>
        <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="sHouseValPe" InitialValue="$0.00" cssclass="RequireValidatorMessage" forecolor=" "></asp:RequiredFieldValidator>
        <span id="negHouseVal" style="display:none; color:red; " >Negative Sales Price is not allowed.</span>
	</div>
	
	<div>
	    <label id="DownPaymentLabel" for=<%= AspxTools.HtmlAttribute(sDownPmtPcPe.ClientID) %>>Down Payment / Equity</label>
        <ml:percenttextbox id="sDownPmtPcPe" NegVal="true" tabIndex="6" runat="server" onchange="Calc.Pct.onchange(this);" width="100px" preset="percent" sProdCalcEntryT="0"></ml:percenttextbox>&nbsp;
        <ml:moneytextbox id="sEquityPe" NegVal="true" tabIndex="7" runat="server" onchange="Calc.Money.onchange(this);" width="100px" preset="money" sProdCalcEntryT="1"></ml:moneytextbox>
        <br />
        <span id="errDownPmt" style="display:none; color:red; margin-bottom:8px;" >Down Payment cannot be greater than Sales Price.</span>
    </div>
    
    <div>
	    <label for=<%= AspxTools.HtmlAttribute(sLtvRPe.ClientID)%>>LTV</label>
        <ml:percenttextbox id="sLtvRPe" tabIndex="8" runat="server" onchange="Calc.Pct.onchange(this);" width="100px" preset="percent" sProdCalcEntryT="2"></ml:percenttextbox>&nbsp;
        <ml:moneytextbox id="sLAmtCalcPe" tabIndex="9" runat="server" onchange="Calc.Money.onchange(this);" width="100px" preset="money" sProdCalcEntryT="3"></ml:moneytextbox>
        <asp:CustomValidator id="sLAmtCalcPeValidator" runat="server" ClientValidationFunction="f_validatesLAmtCalcPe" Display="Static" EnableClientScript="True" />
        <br />
        <span id="neg1stLien" style="display:none; color:red;" >Negative 1st Lien amount is not allowed.</span>
    </div>
	
	<div>
	    <label>2nd Financing?</label>  
	    <asp:RadioButton id="bNo2ndFinancing" GroupName="secondFinancing" Checked="True" onclick="f_show2ndFinancing();" tabIndex="10" runat="server"></asp:RadioButton>No&nbsp;
        <asp:RadioButton id="bYes2ndFinancing" GroupName="secondFinancing" Checked="False" onclick="f_show2ndFinancing();" tabIndex="11" runat="server"></asp:RadioButton>Yes&nbsp;
        <br />
    
        <span id="SecondFinancingRow">
            <label id=sProOFinBalPe_Label for=<%= AspxTools.HtmlAttribute(sProOFinBalPe.ClientID) %>>2nd Financing</label>
            <ml:percenttextbox id="sLtvROtherFinPe" tabIndex="12" runat="server" onchange="Calc.Pct.onchange(this);" width="100px" preset="percent" sProdCalcEntryT="5"></ml:percenttextbox>&nbsp;
            <ml:moneytextbox id="sProOFinBalPe" tabIndex="13" runat="server" onchange="Calc.Money.onchange(this);" width="100px" preset="money" sProdCalcEntryT="6"></ml:moneytextbox>
            <span id="neg2ndLien" style="display:none; color:red" >Negative 2nd Financing is not allowed.</span>
            <br />
        </span>
    </div>
    
    <div>
	    <label for=<%= AspxTools.HtmlAttribute(sCltvRPe.ClientID) %>>CLTV</label>
        <ml:percenttextbox id="sCltvRPe" NegVal="true" tabIndex="14" runat="server" onchange="Calc.Pct.onchange(this);" width="100px" preset="percent" sProdCalcEntryT="4" ></ml:percenttextbox>    
	</div>
	<div>
	    <% if ( m_isAutoPmiOrNewPml ) { %>
	    <label for=<%= AspxTools.HtmlAttribute(sProdConvMIOptionT.ClientID) %>>Conv Loan PMI Type</label>
        <asp:dropdownlist id="sProdConvMIOptionT" tabIndex="15" runat="server" onchange="f_showSplitRow();" ></asp:dropdownlist>
        <asp:CustomValidator id="sProdConvMIOptionTValidator" runat="server" ControlToValidate="sProdConvMIOptionT" ClientValidationFunction="f_validatesProdConvMIOptionT"></asp:CustomValidator>	    
	    <% } else { %>
	    <label for=<%= AspxTools.HtmlAttribute(sProdMIOptionT.ClientID) %>>Mortgage Insurance</label>
        <asp:dropdownlist id="sProdMIOptionT" tabIndex="15" runat="server" onchange="f_onValidateRequiredFields();" ></asp:dropdownlist>
        <asp:CustomValidator id="sProdMIOptionTValidator" runat="server" ControlToValidate="sProdMIOptionT" ClientValidationFunction="f_validatesProdMIOptionT"></asp:CustomValidator>
        <% } %>
        
	    <br />
	    
	    <% if ( m_isAutoPmiOrNewPml ) { %>
	    <span id="SplitPmiRow" style="display:none">
            <label id=sConvSplitMIRT_Label for=<%= AspxTools.HtmlAttribute(sConvSplitMIRT.ClientID) %>>Split MI Upfront Premium</label>
            <asp:dropdownlist id="sConvSplitMIRT" tabIndex="15" runat="server" onchange="f_onValidateRequiredFields();" ></asp:dropdownlist>
            <asp:CustomValidator id="sConvSplitMIRTValidator" runat="server" ControlToValidate="sConvSplitMIRT" ClientValidationFunction="f_validatesConvSplitMIRT"></asp:CustomValidator>
            <br />
        </span>
	    <% } %>
	    
	</div>

    <div>
        <label for=<%= AspxTools.JsGetClientIdString(sSpZip) %>>Zip Code</label>
        <ml:zipcodetextbox id="sSpZip" name="address" runat="server" onchange="f_onValidateRequiredFields();" width="70px" TabIndex="20" preset="zipcode"></ml:zipcodetextbox>
    </div>
    
    <div>
        <label for=<%= AspxTools.HtmlAttribute(sSpStatePe.ClientID) %>>State</label> 
        <ml:statedropdownlist name="address" id="sSpStatePe" tabIndex="21" Width="135px" runat="server" onchange="f_sSpState_onchange();" IncludeTerritories="false"></ml:statedropdownlist>
        <span class="RequireValidatorMessage" id="sSpStateValidator"><img src=<%= AspxTools.SafeUrl(ResolveUrl("~/images/error_pointer.gif")) %> /></span>
    </div>
    
    <div>
	    <label for=<%= AspxTools.HtmlAttribute(sSpCounty.ClientID) %>>County</label> 
	    <select id="sSpCounty" name="sSpCounty" style="width:127px" tabindex="22" runat="server" onchange="f_onValidateRequiredFields()"></select>
	    <span class="RequireValidatorMessage" id="sSpCountyValidator"><img src=<%= AspxTools.SafeUrl(ResolveUrl("~/images/error_pointer.gif")) %> /></span>
	</div>
	
	<div>
	    <label for=<%= AspxTools.HtmlAttribute(sProdSpT.ClientID) %>>Property Type</label> 
	    <asp:dropdownlist id="sProdSpT" tabIndex="23" runat="server" onchange="f_onValidateRequiredFields();" width="135px"></asp:dropdownlist>
	</div>
	
	<div>
	    <label for=<%= AspxTools.HtmlAttribute(sOccTPe.ClientID) %>>Property Use</label> 
	    <asp:dropdownlist id="sOccTPe" tabindex="24" runat="server"  onchange="f_onValidateRequiredFields();" Width="135px"></asp:dropdownlist>
	</div>
	
	<div>
	    <label for=<%= AspxTools.HtmlAttribute(sProdImpound.ClientID) %>>Impound?</label> 
        <asp:checkbox id=sProdImpound tabIndex="25" runat="server"></asp:checkbox>&nbsp;Yes
	</div>
	
	<div>
	    <label for=<%= AspxTools.HtmlAttribute(sCreditScoreType1.ClientID) %>>Estimated Credit Score</label> 
        <asp:textbox id="sCreditScoreType1" style="TEXT-ALIGN: right" tabIndex="26" runat="server" Width="50px" onchange="f_onCreditScoreChange();" onkeyup="f_onCreditScoreChange();"></asp:textbox>
	    <asp:customvalidator id="sCreditScoreType1Validator" runat="server" ClientValidationFunction="f_validateCScore"></asp:customvalidator>
    </div>
    <asp:PlaceHolder ID="sProd3rdPartyUwResultTPlaceHolder" runat="server">
    <div>
    <ml:EncodedLabel ID="Label1" AssociatedControlID="sProd3rdPartyUwResultT" Text="AUS Response" runat="server"/>
    <asp:DropDownList ID="sProd3rdPartyUwResultT" runat="server" />
    </div>
    </asp:PlaceHolder>    
   
    <div>
        <label for=<%= AspxTools.HtmlAttribute(sProdRLckdDays.ClientID)%>>Rate Lock Period </label> 
        <% if (CurrentBroker.IsEnabledLockPeriodDropDown){ %>
            <asp:DropDownList runat="server" ID="sProdRLckdDaysDDL" onchange="f_updateProdRLckdDaysFromDDL(this)"></asp:DropDownList>
        <% } %>
        <asp:textbox id="sProdRLckdDays" onchange="Calc.RateLockPeriod.onchange(this);" style="TEXT-ALIGN: right" tabIndex="27" runat="server" Width="50px" onkeyup="Calc.validate();"></asp:textbox>&nbsp;days&nbsp;
	    <asp:customvalidator id="sProdRLckdDaysValidator" runat="server" ClientValidationFunction="f_validateLckdDays"></asp:customvalidator>
	</div>
	
	<div>
	    <label for=<%= AspxTools.HtmlAttribute(sProdRLckdExpiredDLabel.ClientID)%>>Rate Lock Expiration Date</label> 
        <ml:EncodedLabel id="sProdRLckdExpiredDLabel" runat="server" Width="250px"></ml:EncodedLabel>
    </div>
	<br />
   </fieldset>
   
   <fieldset id="filtersAndSorting" name="filtersAndSorting" class="QuickPricerFieldSet">
    <legend class="LegendSectionHeader">Filters</legend>
	<table cellpadding="1" cellspacing="0" border="0" width="100%">
	<thead>
	<tr>
	    <th align="left" style="width:8em"><span >Term</span><asp:customvalidator id="sProdFilterDueValidator" runat="server" ClientValidationFunction="f_validatesProdFilterDue"></asp:customvalidator></th>
	    <th align="left" style="width:12.5em"><span >Amortization Type</span><asp:customvalidator id="sProdFilterFinMethValidator" runat="server" ClientValidationFunction="f_validatesProdFilterFinMeth"></asp:customvalidator></th>
	    <th align="left" style="width:9em"><span >Product Type</span><asp:customvalidator id="sProdFilterTypeValidator" runat="server" ClientValidationFunction="f_validatesProdFilterType"></asp:customvalidator></th>
	</tr>
	</thead>
	<tbody>
	<tr>
	    <td valign="top">
	        <table cellpadding="0" cellspacing="0">
	            <tr><td>
	                <asp:CheckBox id="sProdFilterDue10Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="10yr" TabIndex="30" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterDue15Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="15yr" TabIndex="31" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterDue20Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="20yr" TabIndex="32" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterDue25Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="25yr" TabIndex="33" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterDue30Yrs" onclick="f_onValidateRequiredFields();" runat="server" Text="30yr" TabIndex="34" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterDueOther" onclick="f_onValidateRequiredFields();" runat="server" Text="Other" TabIndex="35" CssClass="normal" />
	            </td></tr>
	        </table>
	    </td>
	    <td valign="top">
	        <table cellpadding="0" cellspacing="0">
	            <tr><td>
	                <asp:CheckBox id="sProdFilterFinMethFixed" onclick="f_onValidateRequiredFields();" runat="server" Text="Fixed" TabIndex="36" CssClass="normal"/>
	            </td></tr>	            
	            <tr><td>
	                <asp:CheckBox id="sProdFilterFinMeth3YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="3yr&nbsp;ARM" TabIndex="39"  CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterFinMeth5YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="5yr&nbsp;ARM" TabIndex="40" CssClass="normal"  />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterFinMeth7YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="7yr&nbsp;ARM" TabIndex="41" CssClass="normal"  />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterFinMeth10YrsArm" onclick="f_onValidateRequiredFields();" runat="server" Text="10yr&nbsp;ARM" TabIndex="42" CssClass="normal"  />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterFinMethOther" onclick="f_onValidateRequiredFields();" runat="server" Text="Other" TabIndex="43" CssClass="normal" />
	            </td></tr>
	        </table>
	    </td>
	    <td valign="top">
	        <table cellpadding="0" cellspacing="0">
	            <tr><td>
	                <asp:CheckBox ID="sProdIncludeNormalProc" onclick="f_onValidateRequiredFields();" runat="server" Text="Conventional" TabIndex="47" CssClass="normal" />
	            </td></tr>
	            <tr id="rowDURefiPlus"><td>
	                <asp:CheckBox ID="sProdIsDuRefiPlus" onclick="f_onclick_DuRefiPlus(this);" runat="server" Text="DU Refi Plus" TabIndex="48" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox ID="sProdIncludeMyCommunityProc" onclick="f_onValidateRequiredFields();" runat="server" Text="My Community / HomeReady" TabIndex="49" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox ID="sProdIncludeHomePossibleProc" onclick="f_onValidateRequiredFields();" runat="server" Text="Home Possible" TabIndex="50" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox ID="sProdIncludeFHATotalProc" onclick="f_onValidateRequiredFields();" runat="server" Text="FHA" TabIndex="51" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox ID="sProdIncludeVAProc" onclick="f_onValidateRequiredFields();" runat="server" Text="VA" TabIndex="52" CssClass="normal" />
	            </td></tr>
	            <tr>
	              <td><asp:CheckBox ID="sProdIncludeUSDARuralProc" onclick="f_onValidateRequiredFields();" runat="server" Text="USDA Rural" TabIndex="52" CssClass="normal" /></td>
	            </tr>
	        </table>
	    </td>
	</tr>

	<tr>
	    <th align="left" style="width:9em"><span >Payment Type</span><asp:customvalidator id="sProdFilterPaymentTypeValidator" runat="server" ClientValidationFunction="f_validatesProdFilterPaymentType"></asp:customvalidator></th>
	</tr>
    <tr>
	    <td valign="top">
	        <table cellpadding="0" cellspacing="0">
	            <tr><td>
	                <asp:CheckBox id="sProdFilterPmtTPI" onclick="f_onValidateRequiredFields();" runat="server" Text="P&I" TabIndex="53" CssClass="normal" />
	            </td></tr>
	            <tr><td>
	                <asp:CheckBox id="sProdFilterPmtTIOnly" onclick="f_onValidateRequiredFields();" runat="server" Text="I/O" TabIndex="54" CssClass="normal" />
	            </td></tr>
	        </table>
	    </td>
	</tbody>
	</table>

    <br />
	<br />
	<input type="button" class="ButtonStyle" tabindex="57" style="position:relative; left:140px;" id="PriceBtn" value="Price" NoHighlight onclick="f_submitPrice();"/>

</fieldset>
</div>