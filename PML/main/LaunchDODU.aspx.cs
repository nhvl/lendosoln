/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;
using System.IO;

using LendersOffice.DU;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace PriceMyLoan.main
{
    public partial class LaunchDODU : MinimalPage
    {
        private string m_url;
        private string m_ri;
        private string m_ci;

        protected void PageLoad(object sender, System.EventArgs e)
        {
            string id = RequestHelper.GetSafeQueryString("id");

            string content = AutoExpiredTextCache.GetFromCache(id);
            string[] parts = content.Split('\n');
            // TODO: Handle where parts count does not equal to 4.
            m_url = parts[0];
            string userName = parts[1];
            string password = parts[2];
            string sDuCaseId = parts[3];

            XisRequestGroup requestGroup = new XisRequestGroup();
            requestGroup.Request.LoginAccountIdentifier = userName;
            requestGroup.Request.LoginAccountPassword = password;
            requestGroup.Request.RequestDateTime = DateTime.Now.ToString();
            requestGroup.Request.RequestData = new XisRequestData();
            requestGroup.Request.RequestData.FnmProduct = new XisFnmProduct();
            requestGroup.Request.RequestData.FnmProduct.FunctionName = "Submit"; //"Submit";
            requestGroup.Request.RequestData.FnmProduct.Name = "DWEB";
            requestGroup.Request.RequestData.FnmProduct.VersionNumber = "2.0";
            requestGroup.Request.RequestData.FnmProduct.Connection = null;

            XisDwebSubmitRequest dwebSubmitRequest = new XisDwebSubmitRequest();
            dwebSubmitRequest.MornetPlusCasefileIdentifier = sDuCaseId;
            dwebSubmitRequest.SoftwareProvider = new XisSoftwareProvider();
            dwebSubmitRequest.SoftwareProvider.AccountNumber = ConstAppDavid.FannieMae_SoftwareProviderCode;
            dwebSubmitRequest.Options = new XisOptions();
            dwebSubmitRequest.Options.DownloadEnable = "Yes";
            XisCasefileInstitution casefileInstitution = new XisCasefileInstitution();
            dwebSubmitRequest.AddCasefileInstitution(casefileInstitution);


            m_ri = GenerateFormValue(requestGroup);
            m_ci = GenerateFormValue(dwebSubmitRequest);

            this.Form.Action = m_url;
            this.EnableViewState = false;
            ClientScript.RegisterHiddenField("RI", m_ri);
            ClientScript.RegisterHiddenField("CI", m_ci);
            //this.Form.Enctype = "

        }
        private string GenerateFormValue(AbstractXisXmlNode node)
        {
            StringBuilder sb = new StringBuilder(500);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.ASCII;
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                node.GenerateXml(writer);
                writer.Flush();
            }

            return sb.ToString();

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
