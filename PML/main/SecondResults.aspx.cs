using System;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;
using LendersOfficeApp.los.RatePrice;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using LODataAccess = DataAccess;
using LendersOffice.Security;
using DataAccess;
using DataAccess.LoanComparison;
using LendersOffice.Admin;

namespace PriceMyLoan.main
{

    public partial class SecondResults : PriceMyLoan.UI.BasePage
    {

        protected bool m_hasError = false;
        protected string m_errorMessage = "";
        protected bool IsRerunMode 
        {
            get 
            {
                return this.lpeRunModeT == LODataAccess.E_LpeRunModeT.OriginalProductOnly_Direct;
            }
        }

        protected bool EnablePmlUIResults3
        {
            get
            {
                return false;
            }
        }

        

        private bool? allowRegister = new bool?();
        private bool IsAllowRegisterRateLock
        {
            get
            {
                if (allowRegister.HasValue == false)
                {

                    allowRegister = this.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ||
                        this.CurrentUserCanPerform(WorkflowOperations.RegisterLoan);
                }

                return allowRegister.Value;
            }
        }

        private bool? allowRequestRateLock = null;
        private bool IsAllowRequestingRateLock
        {
            get
            {
                if (allowRequestRateLock.HasValue == false)
                {
                    allowRequestRateLock = this.CurrentUserCanPerform(WorkflowOperations.RequestRateLockRequiresManualLock)
                        || this.CurrentUserCanPerform(WorkflowOperations.RequestRateLockCausesLock);
                }

                return allowRequestRateLock.Value;
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected IEnumerable<string> ReasonsCannotRegister
        {
            get
            {
                if (IsAllowRegisterRateLock)
                {
                    return new List<string>();
                }

                return this.GetReasonsUserCannotPerformOperation(WorkflowOperations.RegisterLoan);
            }
        }

        protected IEnumerable<string> ReasonsCannotLock
        {
            get
            {
                if (IsAllowRequestingRateLock)
                    return new List<string>(); // User is not blocked--no reasons
                return this.GetReasonsUserCannotPerformOperation(WorkflowOperations.RequestRateLockRequiresManualLock, WorkflowOperations.RequestRateLockCausesLock);
            }
        }
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        private Guid BrokerID 
        {
            get { return ((PriceMyLoanPrincipal) User).BrokerId; }
        }
        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new WorkflowOperation[] { 
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RequestRateLockCausesLock
            };
        }

        private bool? hasDuplicate = null;
        protected bool HasDuplicate(CPageData dataLoan)
        {
            if (!hasDuplicate.HasValue)
            {
                hasDuplicate = DuplicateFinder.FindDuplicateLoansPer105723(dataLoan.sLId, dataLoan.sBrokerId, dataLoan.sSpAddr).Any();
            }
            return hasDuplicate.Value;
        }

        public override IEnumerable<WorkflowOperation> GetReasonsForOp()
        {

            List<WorkflowOperation> current = new List<WorkflowOperation>() { 
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock
            };

            return current;
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            ClientScript.GetPostBackEventReference(this, "");

            this.RegisterService("main", "/main/MainService.aspx");
            this.RegisterJsScript("ModelessDlg.js");

            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.css");

            if (this.EnablePmlUIResults3)
            {
                this.RegisterCSS("PricingResults.min.css");
                this.RegisterJsScript("angular-1.4.8.min.js");
                this.RegisterJsScript("PricingResults.v3.js");
                this.RegisterJsScript("Pml_Submit.js");
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            var principal = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;
            var broker = principal.BrokerDB;

            string expectedVersion = RequestHelper.GetSafeQueryString("version");
            string currentVersion = broker.GetLatestReleaseInfo().DataLastModifiedD.ToString();
            GetResults1.Visible = !this.EnablePmlUIResults3;
            PricingResultsContainer.Visible = this.EnablePmlUIResults3;

            if (this.EnablePmlUIResults3)
            {
                CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(LoanID, typeof(SecondResults));
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                this.RegisterJsGlobalVariables("sLId", LoanID);
                PriceMyLoan.webapp.PmlResults3FieldRegisterer.RegisterFields(dataLoan, this, true, this.HasDuplicate(dataLoan), this.ReasonsCannotLock, this.ReasonsCannotRegister);
            }

            if (currentVersion != expectedVersion) 
            {
                m_hasError = true;
                m_errorMessage = ErrorMessages.InvalidRateOptionVersion;
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
    }
}
