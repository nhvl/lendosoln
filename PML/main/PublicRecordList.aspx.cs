using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using PriceMyLoan.DataAccess;
using LendersOffice.CreditReport;
using PriceMyLoan.Security;
using LendersOffice.Common;



namespace PriceMyLoan.main
{
	/// <summary>
	/// Display list of public records on this loan file.
	/// </summary>
	public partial class PublicRecordList : PriceMyLoan.UI.BasePage
	{
        protected Guid m_currentAppId = Guid.Empty;


		// 2/28/07 mf. Per OPM 9023 we now allow users to edit some values
		// of public record entries.

		protected void PageLoad(object sender, System.EventArgs e)
		{
			CPageData dataLoan = new CPublicRecordData( LoanID );
			dataLoan.InitLoad();


            CAppData dataApp = null;

            Guid appId = Guid.Empty;
            string sAppid = RequestHelper.GetSafeQueryString("appid");
            try
            {
                appId = new Guid(sAppid);
            }
            catch (Exception exc)
            {
                if (sAppid == null)
                    sAppid = "null";

                Tools.LogError("PML public records. Error trying to parse 'appid' guid from query string. Found value: '" + sAppid + "'.");
                throw new CBaseException("Cannot load borrower public records without a valid application id.", exc);
            }

            dataApp = dataLoan.GetAppData(appId);
            m_currentAppId = dataApp.aAppId;
            
            IPublicRecordCollection publicRecords = dataApp.aPublicRecordCollection;
			DataTable recordDisplayTable = publicRecords.CloneFilteredTable("");

			// We create a column to show if this record has been modified (has audit history)
			// and for accurate sorting, map enums to their strings.
			recordDisplayTable.Columns.Add("Modified");
			foreach(DataRow publicRecordRow in recordDisplayTable.Rows)
			{
				publicRecordRow["Modified"] = ( publicRecordRow["AuditTrailXmlContent"].ToString().Length > 0 ) ? "Yes" : "No" ;
				publicRecordRow["DispositionT"] = Tools.CreditPublicRecordDispositionType_rep( (E_CreditPublicRecordDispositionType) int.Parse(publicRecordRow["DispositionT"].ToString()) );
				publicRecordRow["Type"] = Tools.CreditPublicRecordType_rep( (E_CreditPublicRecordType) int.Parse(publicRecordRow["Type"].ToString()) );
			}

			m_PublicRecordDG.DataSource = recordDisplayTable.DefaultView;
			m_PublicRecordDG.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
