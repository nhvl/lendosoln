<%@ Register TagPrefix="uc" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserInformation.ascx.cs" Inherits="PriceMyLoan.main.UserInformation" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script>
    function calcNameOnLoanDoc()
    {
        var nameOnLDocCheckbox = document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDocLock) %>);
        if(!nameOnLDocCheckbox.checked)
        {
            var fName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_FirstName) %>).value + " ";
            var mName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_MiddleName) %>).value;
            if(mName != "")
                mName += " ";
            var lName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_LastName) %>).value;
            var suffix = document.getElementById(<%= AspxTools.JsGetClientIdString(m_Suffix) %>).value;
            if(suffix != "")
                lName += ", ";
            var nameOnLDoc = fName+mName+lName+suffix;
            document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDoc) %>).value = nameOnLDoc;
        }
    }

    function lockNameOnLoanDoc(el)
    {
        if(!el) return;

        var nameOnLoanDoc = document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDoc) %>);
        nameOnLoanDoc.readOnly = !el.checked;

        if(!el.checked) calcNameOnLoanDoc();
    }

    $j(function()
    {
        addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);
        lockNameOnLoanDoc(<%= AspxTools.JsGetElementById(m_NameOnLoanDocLock) %>);
        validateUserInfoPage();
        $j(<%=AspxTools.JsGetElementById(RequireCellPhone) %>).on("change", validateUserInfoPage);
    });

    function validateUserInfoPage() {
        var errorMessageLabel = document.getElementById(<%= AspxTools.JsGetClientIdString(RequiredFieldMessage) %>);
        var error = appendErrorMsg("", [
                {id: <%= AspxTools.JsGetClientIdString(m_FirstName) %>, label:"First Name"},
                {id: <%= AspxTools.JsGetClientIdString(m_LastName ) %>, label:"Last Name" },
                {id: <%= AspxTools.JsGetClientIdString(m_Phone    ) %>, label:"Phone"     },
                {id: <%= AspxTools.JsGetClientIdString(m_Email    ) %>, label:"Email"     },
            ]
            .filter(function(x) { return !(document.getElementById(x.id).value.trim()) })
            .map(function(x) { return x.label })
            .join(", ")
        );

        error = copyValidator(error, <%= AspxTools.JsGetClientIdString(RegularExpressionValidator1) %>, 'Email');

        var emailPattern = /^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9_.-]+$/;
        var isValid =
            isNotEmptyTextbox(<%= AspxTools.JsGetClientIdString(m_FirstName) %>) &&
            isNotEmptyTextbox(<%= AspxTools.JsGetClientIdString(m_LastName) %>) &&
            isNotEmptyTextbox(<%= AspxTools.JsGetClientIdString(m_Phone) %>) &&
            emailPattern.test(document.getElementById(<%= AspxTools.JsGetClientIdString(m_Email) %>).value);

        ToggleCellphoneValidator();

        if ($j(<%=AspxTools.JsGetElementById(RequireCellPhone) %>).val() === "True") {
            var hasCell = isNotEmptyTextbox(<%= AspxTools.JsGetClientIdString(m_CellPhone) %>);
            if (!hasCell) {
                error = appendErrorMsg(error, "Cell Phone");
            }

            isValid = isValid && hasCell;
        }

        ValidatePhone();
        ValidateCellPhone();

        console.log("validateUserInfoPage", error);
        jQuery(errorMessageLabel).text(error);
        jQuery(errorMessageLabel).trigger('change');

        return isValid;
    }

    function copyValidator(error, elementId, missingField) {
        if (document.getElementById(elementId).style.display == 'none')
            return error;
        else
            return appendErrorMsg(error, missingField);
    }

    function appendErrorMsg(error, missingField) {
        if (!missingField) return error;

        if (error == '')
            error = 'Please fill out all required fields: ';
        else
            error += ', ';

        error += missingField;
        return error;
    }

    function isNotEmptyTextbox(elementId) {
        return document.getElementById(elementId).value.replace(/^\s+|\s+$/g, '') != '';
    }

    <%-- // 04/14/06 mf - Validation of the phone textbox was not working properly.
         // We force it here. --%>
    function ValidatePhone()
    {
        var phoneValidator = <%= AspxTools.JsGetElementById(PhoneValidator) %>;
        if (phoneValidator && !phoneValidator.isvalid && typeof(ValidatorValidate) == 'function')
            ValidatorValidate(phoneValidator);
    }

    function ValidateCellPhone()
    {
        var phoneValidator = <%= AspxTools.JsGetElementById(CellPhoneValidator) %>;
        if (phoneValidator && !phoneValidator.isvalid && typeof(ValidatorValidate) == 'function')
            ValidatorValidate(phoneValidator);
    }

    function ToggleCellphoneValidator()
    {
        if($j(<%=AspxTools.JsGetElementById(RequireCellPhone) %>).val() === "True")
        {
            ValidatorEnable(<%=AspxTools.JsGetElementById(CellPhoneValidator) %>, true);
            $j('#cellPhoneRedAsterisk').show();
        }
        else
        {
            ValidatorEnable(<%=AspxTools.JsGetElementById(CellPhoneValidator) %>, false);
            $j('#cellPhoneRedAsterisk').hide();
        }
    }
</script>

<div class="content-container">
    <span class="indicates-reqired"><span class="text-danger">*</span> Indicates required fields</span>
    <header class="header">Personal Information</header>
    <div class="profile-padding-left">
        <div class="table">
            <div>
                <div>
                    <label>Name</label>
                    <div class="table">
                        <div>
                            <div>
                                <label>First Name
                                    <asp:RequiredFieldValidator runat="server" EnableViewState="False" ControlToValidate="m_FirstName" Display="Dynamic" ID="FirstNameValidator" CssClass="hidden"/>
                                    <span class="text-danger">*</span></label>
                                <asp:TextBox ID="m_FirstName" runat="server" MaxLength="40" Width="180" onchange="calcNameOnLoanDoc();" onblur="validateUserInfoPage();"/>
                            </div>
                            <div>
                                <label>Middle Name</label>
                                <asp:TextBox onchange="calcNameOnLoanDoc();" ID="m_MiddleName" runat="server" MaxLength="21" Width="180"/>
                            </div>
                            <div>
                                <label>Last Name
                                    <asp:RequiredFieldValidator runat="server" EnableViewState="False" ControlToValidate="m_LastName" Display="Dynamic" ID="LastNameValidator" CssClass="hidden"/>
                                    <span class="text-danger">*</span></label>
                                <asp:TextBox ID="m_LastName" runat="server" MaxLength="40" Width="180" onchange="calcNameOnLoanDoc();" onblur="validateUserInfoPage();"/>
                            </div>
                            <div class="col-suffix">
                                <label>Suffix</label>
                                <ml:ComboBox onchange="calcNameOnLoanDoc();" ID="m_Suffix" runat="server" MaxLength="10" Width="50"/>
                            </div>
                            <div id="NameOnLoanDocs" runat="server">
                                <label>Name on Loan Documents</label>
                                <div>
                                    <asp:TextBox ID="m_NameOnLoanDoc" runat="server" MaxLength="100" class="col-name-loan-documents"/>
                                    <label class="lock-checkbox"><asp:CheckBox onclick="lockNameOnLoanDoc(this);" ID="m_NameOnLoanDocLock" runat="server"/></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <header class="header">Contact Information</header>
    <div class="profile-padding-left">
        <div class="table table-phone-profile">
            <div>
                <div>
                    <div class="table">
                        <div>
                            <div>
                                <label>Phone
                                    <asp:RequiredFieldValidator runat="server" EnableViewState="False" ControlToValidate="m_Phone" Display="Dynamic" ID="PhoneValidator" CssClass="hidden"/>
                                    <span class="text-danger">*</span></label>
                                <uc:phonetextbox id="m_Phone" onblur="ValidatePhone();validateUserInfoPage();" runat="server" preset="phone" Width="180"/>
                            </div>
                            <div>
                                <label>Fax</label>
                                <uc:phonetextbox id="m_Fax" runat="server" preset="phone" Width="180"/>
                            </div>
                            <div>
                                <label>Cell
                                    <asp:RequiredFieldValidator runat="server" EnableViewState="False" ControlToValidate="m_CellPhone" Display="Dynamic" ID="CellPhoneValidator" CssClass="hidden"/>
                                    <span class="text-danger" id="cellPhoneRedAsterisk">*</span></label>
                                <uc:phonetextbox id="m_CellPhone" onblur="ValidateCellPhone(); validateUserInfoPage();" runat="server" preset="phone" Width="180"/>
                            </div>
                            <div class="vertical-align-bottom col-checkbox">
                                <asp:CheckBox runat="server" ID="IsCellphoneForMultiFactorOnly" Text="Private: For multi-factor authentication only" />
                            </div>
                            <div>
                                <asp:HiddenField runat="server" ID="RequireCellPhone" />
                            </div>
                        </div>
                        <div>
                            <div class="pager-profile">
                                <label>Pager</label>
                                <uc:phonetextbox id="m_Pager" runat="server" preset="phone" Width="180"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table">
            <div>
                <div>
                    <label>Email
                        <asp:RequiredFieldValidator runat="server" EnableViewState="False" ControlToValidate="m_Email" Display="Dynamic" ID="EmailValidator" CssClass="hidden"/>
                        <span class="text-danger">*</span></label>
                    <asp:TextBox ID="m_Email" runat="server" MaxLength="80" onblur="validateUserInfoPage();" class="form-control-email"/>
                </div>
                <div class="vertical-align-bottom col-checkbox">
                    <asp:CheckBox ID="chkTaskRelatedEmail" runat="server" Text="Send task-related e-mail" />
                </div>
            </div>
            <div>
                <div><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ControlToValidate="m_Email"/></div>
            </div>
        </div>
    </div>
</div>

<div style="text-align:center">
    <ml:EncodedLabel class="ErrorMessage" ID="RequiredFieldMessage" runat="server" EnableViewState="False"/>
</div>
