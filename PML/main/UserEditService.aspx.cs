using DataAccess;
using LendersOffice.Common;
using PriceMyLoan.Security;
using System;
using System.Data.SqlClient;
using System.Web.UI;

namespace PriceMyLoan.main
{
    /// <summary>
    /// Summary description for UserEditService.
    /// </summary>
    public partial class UserEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
		public PriceMyLoanPrincipal PriceMyLoanUser 
		{
			get 
			{ 
				return (PriceMyLoanPrincipal) Page.User; 
			}
		}
		
		protected override void Process(string methodName) 
		{
			switch (methodName) 
			{
				case "UnlockAccount":
					UnlockAccount();
					break;
			}
		}

		private void UnlockAccount()
		{
			if( PriceMyLoanUser.IsPmlManager == false)
			{
				SetResult("ErrorMessage", "Permission Denied - You lack authorization to unlock pml user accounts");
				return;
			}
			
			string login = "";
			Guid employeeId;
			Guid brokerPmlSiteId = Guid.Empty;

			try
			{
				login = GetString("login");
				
				//OPM 1863
				employeeId = GetGuid("id");


				SqlParameter[] parameters =  {
												new SqlParameter("@LoginNm", login),
												new SqlParameter("@LoginBlockExpirationD", SmallDateTime.MinValue),
												new SqlParameter("@Type", "P"),
												new SqlParameter("@BrokerPmlSiteId", PriceMyLoanUser.BrokerDB.PmlSiteID)
											};

				StoredProcedureHelper.ExecuteNonQuery(PriceMyLoanUser.BrokerId, "ResetLoginFailureCount", 1, parameters);
			}
			catch( CBaseException e )
			{
				Tools.LogError("Unable to unlock account", e);
				SetResult("ErrorMessage", e.UserMessage);
				return;
			}
		}
	}
}