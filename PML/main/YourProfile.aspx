<%@ Page Language="c#" CodeBehind="YourProfile.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.YourProfile" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" TagName="ModelessDlg" Src="../Common/ModalDlg/ModelessDlg.ascx" %>
<%@ Register Src="../PasswordReset/SetUserSecurityQuestions.ascx" TagName="SecurityQuestions" TagPrefix="uc" %>
<%@ Register Src="~/main/UserInformation.ascx" TagName="UserInformation" TagPrefix="uc" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Your Profile</title>
    <style>
        .add-padding {
            padding-left: 32px;
            padding-right: 32px;
        }

        .ButtonStyle {
            width: 8em;
        }

        #RegisterDevicePopup, #ErrorMessageDisplayDiv, #RegisterAuthenticatorPopup {
            display: none;
            padding: 0;
        }

        #DeviceName {
            width: 100%;
        }

        .error-message {
            color:red;
        }

        .popup div {
            margin-bottom: 10px;
        }

        .ui-dialog-titlebar {
            display: none;
        }

        .body {
            padding: 5px;
        }

        .password-container {
            display: none;
        }

        .content-container {
            margin-bottom: 10px;
        }

        #m_clientCertificateGrid td:nth-child(1), #m_registeredIpDataGrid td:nth-child(1), #m_registeredMobileDevicesGrid td:nth-child(1)
        {
            width: 20%;
        }

        #m_clientCertificateGrid td:nth-child(2)
        {
            width: 70%;
        }

        #m_registeredIpDataGrid td:nth-child(2), #m_registeredMobileDevicesGrid td:nth-child(2)
        {
            width: 35%;
        }

        #m_registeredIpDataGrid td:nth-child(3), #m_registeredMobileDevicesGrid td:nth-child(3)
        {
            width: 40%;
        }

        #m_clientCertificateGrid td:nth-child(3), #m_registeredIpDataGrid td:nth-child(4), #m_registeredMobileDevicesGrid td:nth-child(4)
        {
            width: 5%;
            text-align: center;
        }
        .TabContentSection {
	        padding-top: 2em;
        }
        .auth-note {
            margin-bottom: 5px;
        }
    </style>
</head>
<body id="MainBody" ms_positioning="FlowLayout" runat="server">

<script>
    function _init()
    {
        var errorE = document.getElementById('m_errorMessage');
        if( errorE != null )
        {

            window.parent.LQBPopup.ShowElement( errorE.value );
        }

        <%= AspxTools.JsGetElementById(m_previousPassword) %>.focus();

            <%-- // 02/14/08 mf. OPM 19669 --%>
        f_displayButton();
        $j('a.passwordRulesAnchor').click(showPasswordRules);

        tabsHandler($j('#tabSection').val());
    }

    $j(document).ready(function(){
        $j("#RegisterNewDeviceBtn").click(RegisterNewDevice);
        $j("#ResetDevicePopupBtn").click(ResetDevicePopup);
        if (!document.getElementById('ServiceCredentialsJson'))
        {
            document.getElementById('serviceCredentialTabItem').style.display = "none";
        }
        else
        {
            ServiceCredential.Initialize({
                WindowContext: window.parent,
                ServiceCredentialJsonId: 'ServiceCredentialsJson',
                ServiceCredentialTableId: 'ServiceCredentialsTable'
            });
            ServiceCredential.RenderCredentials();
            $j('#AddServiceCredentialBtn').on('click', ServiceCredential.AddServiceCredential);
        }

        TPOStyleUnification.Components();

        $j("#tabs>li a").click(function(e) {
            e.preventDefault();
            tabsHandler($j(this).attr("href"));
        });

        
    });

    function VerifyAuthenticatorConfig(popup) {

        var args = {
            key: popup.find('.AuthKey').val(),
            token: popup.find('.AuthToken').val()
        };

        var result = gService.main.call("CompleteAuthenticatorRegistration", args, false, false, false);
        if (result.error) {
            result.error = false;
            DisplayErrorMessage(result.UserMessage);
        }
        else if (result.value["Success"] == "False") {
            DisplayErrorMessage('The provided token is not valid. Please try again.');
        }
        else {
            window.parent.LQBPopup.Hide();
            var scope = angular.element(document.getElementById("yourProfileController")).scope();
            scope.$apply(function () {
                scope.vm._refresh();
            });
        }
    }
    

    function DisplayErrorMessage(message)
    {
        alert(message);
    }

    var isNewDeviceRegistered = false;
    //This function should call the variables in parent page.
    function RegisterNewDevice(){
        var deviceName = window.parent.$j("#DeviceName").val();

        if (window.parent.$j.trim(deviceName) === "") {
            window.parent.simpleDialog.alert('Device name is required.');
            return;
        }

        var args = {
            DeviceName: deviceName
        };

        var result = gService.main.call("RegisterNewDevice", args);

        if(result.error){
            //display error
            DisplayErrorMessage(result.UserMessage);
            result.error = false;
        }
        else{
            if(result.value["Success"]){
                window.parent.$j(".register-container").toggle();
                window.parent.$j(".password-container").toggle();

                window.parent.$j("#MobilePassword").text(result.value["MobilePassword"]);
                isNewDeviceRegistered = true;
            }
            else{
                DisplayErrorMessage("Failed to register mobile device.  Please try again.");
            }
        }
    }

    function ResetDevicePopup(){
        $j("#DeviceName").val("");
        if(isNewDeviceRegistered)
        {
            window.parent.iFrameScrollPosition = getCurrentScrollPostion();
            location.reload();
        }
        else{
            window.parent.iFrameScrollPosition = 0;
        }
    }

    function getCurrentScrollPostion(){
        var pos=window.parent.$j('body').first().scrollTop();
        if(pos != 0) return pos;
        return window.parent.document.documentElement ? window.parent.document.documentElement.scrollTop : window.parent.document.body.scrollTop;
    }

    function verifyFieldsNotBlank( source, arguments )
    {
        var oldPassword = <%= AspxTools.JsGetElementById(m_previousPassword) %>;
            var newPassword = <%= AspxTools.JsGetElementById(m_newPassword) %>;
            var confirmPassword = <%= AspxTools.JsGetElementById(m_confirmPassword) %>;
            var statusText = <%= AspxTools.JsGetElementById(m_passwordStatus) %>;

            if ( oldPassword.value == '' && newPassword.value == '')
            return;

            if ( oldPassword.value == '' || newPassword.value == '' || confirmPassword.value == '')
            {
                statusText.innerHTML = "Please complete all the fields.";
                oldPassword.focus();
                arguments.IsValid = false;
            }
            else
            {
                arguments.IsValid = true;
            }
        }

        function f_installClientCertificate()
        {
            window.parent.LQBPopup.Show(gVirtualRoot + '/InstallClientCertificate.aspx',
                {
                    hideCloseButton: true,
                    draggable: false,
                    onReturn: function() { window.location.reload(true); }
                });
        }

        function f_displayButton()
        {
            if (frames && frames["ModalFrame"] ) return;
            document.getElementById("BtnRow").style.display = 'block';
        }

        function f_logout() {
            self.location = <%= AspxTools.JsString(DataAccess.Tools.VRoot + "/logout.aspx") %>;
        }

        function showPasswordRules(e)
        {
            var guidelines=TPOStyleUnification.GetPasswordGuidelines('<%= AspxTools.HtmlString(LendersOffice.Constants.ConstApp.PasswordGuidelines) %>');

            // When this page is hosted within a frame and the user must reset their password,
            // we cannot access window.parent. When that happens, display a simple alert with the
            // message. All other instances should use LQBPopup to ensure the backdrop mask
            // covers the entire page.
            try {
                window.parent.LQBPopup.ShowElement($j('<div><div class="modal-show-rules">'+guidelines["content"]+"</div><div>"),{headerText:guidelines["header"],hideXButton:true,draggable:false});
            } catch (e) {
                simpleDialog.alert(guidelines["content"], guidelines["header"]);
            }
        }

    function tabsHandler(p) {
        // Hide all content.
        $j(".TabContentSection").hide();
        var tabName;
        if (typeof(p) === 'string' && p !== '') {
            showTab($j("#tabs>li a").filter('[href="' + p + '"]').first());
        } else {
            showFirstTab();
        }
    }

    function showTab(tab) {

        // Remove any "active" class.
        $j("#tabs>li a").removeClass("selected");
        $j("#tabs>li").removeClass("active");

        // Add "active" class to selected tab.
        tab.addClass("selected");
        tab.parent().addClass("active");

        // Hide all tab content
        $j(".TabContentSection").hide();

        // Find the href attribute value to identify the active tab + content
        var activeTab = tab.attr("href");

        if (navigator.appVersion.indexOf("MSIE 9.") != -1) {
            //IE 9 loses border cell when using fadeIn
            $j(activeTab).show();
        } else {
            // Fade in the active ID content.
            $j(activeTab).fadeIn();
        }

        $j('#tabSection').val(activeTab)
    }

    function showFirstTab() {
        // Activate first tab.
        $j("#tabs>li a:first").addClass("selected").show();
        $j("#tabs>li a:first").parent().addClass("active");

        // Show first tab content.
        $j(".TabContentSection").first().show();
    }
</script>


<form method="post" runat="server">
    <asp:HiddenField runat="server" ID="tabSection" />
    <div name="content-detail">
        <div class="warp-section warp-section-tabs">
            <div class="static-tabs">
                <div class="static-tabs-inner">
                    <header class="page-header table-flex table-flex-app-info">
                        <div>My Profile</div>
                        <div class="btnDiv text-right" id="BtnRow">
                            <button type="button" runat="server" ID="m_changeBtn" OnServerClick="ChangeClick" class="btn btn-default">Save</button>
                            <input type="button" class="btn btn-default" onclick="f_logout();" runat="server" id="LogOffBtn" value="LOG OFF"/>
                        </div>
                    </header>
                    <ul id="tabs" class="nav nav-tabs nav-tabs-panels" style="margin-right: auto">
                        <li class="first-tab"><a href="#secPersonalInformation" class="selected">Personal Information</a></li>
                        <li><a href="#secLoginSettings">Login Settings</a></li>
                        <li><a href="#secSystemAccess">System Access</a></li>
                        <li id="serviceCredentialTabItem"><a href="#secServiceCredentials">Service Credentials</a></li>
                    </ul>
                </div>
            </div>
            <div class="content-tabs">
                <div class="table-service">
                    <div>
                        <div class="table-service-right">
                            <div id="secPersonalInformation" class="TabContentSection">
                                <uc:UserInformation id="UserInformation" runat="server" />
                            </div>
                            <div id="secLoginSettings" class="TabContentSection">
                                <% if (!m_isForcedChange)
                                { %>
                                <div id="SecurityQuestionContainer" class="content-container">
                                    <header class="header">Security Information</header>
                                    <div class="profile-padding-left">
                                        <div>
                                            <uc:SecurityQuestions ID="SecurityQuestions" runat="server" EditMode="Edit" />
                                        </div>
                                        <div style="padding-right: 20px;">
                                            <ml:EncodedLabel ID="m_securityQuestionStatus" ForeColor="red" runat="server" EnableViewState="false"/>
                                        </div>
                                    </div>
                                </div>
                                <%} %>
                                <div id="ChangePasswordContainer" class="content-container">
                                    <header class="header">Change Password</header>
                                    <div class="profile-padding-left">
                                        <div class="table">
                                            <div>
                                                <div>
                                                    <label>Login Name</label>
                                                    <asp:TextBox ReadOnly="true" ID="m_userName" runat="server"/>
                                                </div>
                                                <div>
                                                    <label>Old Password</label>
                                                    <asp:TextBox ID="m_previousPassword" TextMode="password" runat="server"/>
                                                </div>
                                                <div>
                                                    <label>New Password <span id="pwRules">(<a class="passwordRulesAnchor">show rules</a>)
                                                    </span></label>
                                                    <asp:TextBox ID="m_newPassword" TextMode="password" runat="server"/>
                                                </div>
                                                <div>
                                                    <label>Retype Password</label>
                                                    <asp:TextBox ID="m_confirmPassword" TextMode="password" runat="server"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div style="padding-right: 20px;">
                                                <ml:PassthroughLabel ID="m_passwordStatus" ForeColor="red" runat="server" EnableViewState="false"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="secSystemAccess" class="TabContentSection">
                                <div ng-bootstrap="YourProfile" your-profile id="yourProfileController"></div>
                            </div>
                            <div id="secServiceCredentials" class="TabContentSection">
                                <header class="header">Service Credentials</header>
                                <div class="profile-padding-left">
                                    <table id="ServiceCredentialsTable" class="primary-table margin-bottom">
                                        <tbody></tbody>
                                    </table>
                                    <button id="AddServiceCredentialBtn" class="btn btn-default" type="button">Add Credential</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <button type="button" OnServerClick="BackToPipeline_Click" runat="server" ID="BackToPipeline" class="btn btn-default">Back to Pipeline</button>
    </div>
</form>
    
<div id="RegisterDevicePopup" class="hidden">
    <div class="register-container">
        <div class="CertificateHeader modal-header">
            <h4 class="modal-title">Register New Mobile Device</h4>
        </div>
        <div class="register-body body modal-body">
            <div class="table">
                <div>
                    <div>
                        <span class="text-grey">New Device Name </span>
                        <span class="required">*</span>
                    </div>
                    <div>
                        <input type="text" maxlength="100" id="DeviceName" />
                    </div>
                    <div>
                        <span class="supplementary-info">(Please name the device you wish to register)</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-flat" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-flat" id="RegisterNewDeviceBtn">Generate Mobile ID</button>
        </div>
    </div>
    <div class="password-container" style="display:none">
        <div class="modal-header">
            <h4 class="modal-title">Mobile ID</h4>
        </div>
        <div class="password-body body modal-body">
            <div class="table">
                <div>
                    <div>
                        <label class="text-grey">Mobile ID:</label>
                    </div>
                    <div><span id="MobilePassword"></span></div>
                </div>
            </div>
            <div class="margin-bottom">
                <ul>
                    <li>To Register:</li>
                    <li>1.  Download and open the mobile LendingQB app.</li>
                    <li>2.  Enter your credentials and the <span class="text-grey">Mobile ID</span> shown above where indicated.</li>
                </ul>
            </div>
            <div class="text-danger">
                This Mobile ID is active for the next 10 minutes and may be used once on one device only.
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-flat" data-dismiss="modal" id="ResetDevicePopupBtn">Close</button>
        </div>
    </div>
</div>



    <div class="hidden RegisterAuthenticatorPopup">
        <input type="hidden" class="AuthKey" />
        <div class="register-container">
            <div class="CertificateHeader modal-header">
                <h4 class="modal-title">Register Authenticator App</h4>
            </div>
            <div class="register-body body modal-body">
                <div class="ViewerBody">
                    The authenticator app will be the primary source of one time passwords. You may still request one manually if your authenticator app is not available. 
                </div>
                <div class="table">
                    <div>
                        <div>
                            <span class="text-grey">Step 1</span>
                        </div>
                        <div>
                            <div>
                                Scan the following barcode with your favorite authenticator application.
                                <br />
                                <img class="AuthBarcode" alt="barcode" width="200" height="200" />
                                <br />
                                You may also enter the following code manually: 
                            <div class="ManualAuthCode"></div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="text-grey">Step 2 </div>
                        <div>
                            Enter the current token code from your app to validate the setup.
                        </div>
                    </div>
                    <div>
                        <div></div>
                        <div>
                            <input type="text" class="AuthToken" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-flat VerifyAuthenticatorConfig">Configure Authenticator</button>
            </div>
        </div>
    </div>


    <ml:ModelessDlg ID="ModelessDlg" runat="server"/>
</body>
</html>
