﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalMonthlyExpenses.aspx.cs" Inherits="PriceMyLoan.main.AdditionalMonthlyExpenses" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Additional Monthly Expenses</title>
    <style type="text/css">
        #OkBtn, #CancelBtn { font-weight: bold; }
        .content { padding: 10px;}
        .content table
        {
            width: 99%;
        }
        .center { text-align: center; }

        img
        {
            cursor:pointer;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Additional Monthly Expenses</h4>
    <form id="form1" runat="server">
    <div class="content">
        <table>
            <tr>
                <td> 
                    <ml:EncodedLabel AssociatedControlID="sProRealETx" runat="server" >Property Taxes </ml:EncodedLabel> 
                </td>
                <td style="white-space:nowrap;"> 
                    <asp:PlaceHolder runat="server" ID="sProRealETxHolder">
                        <ml:PercentTextBox ID="sProRealETxR" runat="server" CssClass="calcfield"></ml:PercentTextBox>
                        of 
                        <asp:DropDownList runat="server" ID="sProRealETxT" CssClass="calcfield"></asp:DropDownList>
                        + 
                        <ml:MoneyTextBox ID="sProRealETxMb" runat="server" CssClass="calcfield"></ml:MoneyTextBox>
                        =
                    </asp:PlaceHolder>
                </td>
                <td>
                    <ml:MoneyTextBox readonly="true" runat="server" ID="sProRealETx" CssClass="calcfield"></ml:MoneyTextBox> 
                </td>
            </tr>
            <tr>
                <td> 
                    <ml:EncodedLabel AssociatedControlID="sProHazIns" runat="server">Hazard Insurance</ml:EncodedLabel>  
                </td>
                <td style="white-space:nowrap;">
                    <asp:PlaceHolder runat="server" ID="sProHazInsHolder">
                        <ml:PercentTextBox ID="sProHazInsR" runat="server" CssClass="calcfield"></ml:PercentTextBox>
                        of 
                        <asp:DropDownList runat="server" ID="sProHazInsT" CssClass="calcfield"></asp:DropDownList>
                        + 
                        <ml:MoneyTextBox ID="sProHazInsMb" decimalDigits="4" runat="server" CssClass="calcfield"></ml:MoneyTextBox>
                        =
                    </asp:PlaceHolder>
                </td>
                <td>
                     <ml:MoneyTextBox readonly="true" runat="server" id="sProHazIns" CssClass="calcfield"></ml:MoneyTextBox> 
                </td>
            </tr>
            <tr>
                <td> <ml:EncodedLabel AssociatedControlID="sProHoAssocDues" runat="server" class="calcfield">HOA fees</ml:EncodedLabel>  </td>
                <td></td>
                <td> <ml:MoneyTextBox runat="server" id="sProHoAssocDues" CssClass="calcfield"></ml:MoneyTextBox> 
            </tr>
            <tr id="MISection" runat="server">
                <td><ml:EncodedLabel AssociatedControlID="sProMIns" runat="server" CssClass="calcfield">Mortgage Insurance</ml:EncodedLabel> </td>
                <td></td>
                <td> <ml:MoneyTextBox runat="server" ID="sProMIns" CssClass="calcfield"></ml:MoneyTextBox></td>
            </tr>
            <tr>
                <td>  <asp:TextBox runat="server" ID="sProOHExpDesc" CssClass="calcfield"></asp:TextBox> </td>
                <td style="text-align:right;">
                    <input type="checkbox" runat="server" ID="sProOHExpLckd" CssClass="calcfield" />

                </td>
                <td> 
                    <ml:MoneyTextBox runat="server" id="sProOHExp" CssClass="calcfield"></ml:MoneyTextBox>
                </td>
            </tr>
            <tr>
                <td> Total </td>
                <td></td>
                <td> <ml:MoneyTextBox runat="server" id="sMonthlyPmtPe" ReadOnly="true" CssClass="calcfield"></ml:MoneyTextBox> </td>
            </tr>
           
        </table>
        <br />
        <p runat="server" id="MINote">
            Note: Monthly MIP expense is calculated automatically and should not be included here.
        <br />
        </p>
        <div class="center">
            <input type="button" value="  OK  " id="OkBtn" />
            <input type="button" value="Cancel" id="CancelBtn" />
        </div>

    </div>
    </form>
    
    <script type="text/javascript">
        $j(function() {

            $j('#OkBtn').click(function(){

                var args = getAllFormValues();
                args.sLId = <%= AspxTools.JsString(LoanID) %>;

                var result = gService.main.call("SaveMonthlyExpenses", args);

                args = {};
                args.sMonthlyPmtPe = $j("#sMonthlyPmtPe").val();
                parent.LQBPopup.Return(args); 
            });
            $j('#CancelBtn').click(function() {
                parent.LQBPopup.Hide();
            });

            $j("#sProOHExpLckd").change(function(){
                
                locksProOHExp($j(this).prop("checked"));
                $j("#sProOHExp").change();
            });

            function locksProOHExp(locked)
            {
                if(!locked)
                {
                    $j("#sProOHExp").attr("readonly", "");
                    $j("#sProOHExp").css("background-color", "lightgray");
                }
                else
                {
                    $j("#sProOHExp").removeAttr("readonly");
                    $j("#sProOHExp").css("background-color", "");
                }
            }

            locksProOHExp($j("#sProOHExpLckd").prop("checked"));

            $j(document).delegate('input.calcfield, select', 'change', function() {
                var $obj = $j(this);
                if ($obj.val().match("-") || $obj.val().match("[(]")  ) {
                    alert('Negative values are not allowed.');
                    $obj.val("$0.00");
                }
                
                var args = getAllFormValues();
                args.sLId = <%= AspxTools.JsString(LoanID) %>;
                var results = gService.main.call('CalculateMonthlyExpenses', args);
                if (results && results.value ) {
                    $j("#sProRealETx").val(results.value["sProRealETx"]);
                    $j("#sProHazIns").val(results.value["sProHazIns"]);
                    $j("#sMonthlyPmtPe").val(results.value["sMonthlyPmtPe"]);
                    $j("#sProOHExp").val(results.value["sProOHExp"]);
                }
            });
        });
    </script>
</body>
</html>
