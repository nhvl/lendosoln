<%@ Page Language="c#" CodeBehind="UserAdmin.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.UserAdmin" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Manage Users</title>
</head>
<body>
    <script>
        var editorWindow = null;

        function onInit()
        {
            toggleClearSearch();
        }
        <%-- // HACK: For some reason when you give this window focus by closing it,
             // it attemps to highlight the last selected field (which doesn't exist anymore) and
             // causes an error. This hack removes the input onfocus handlers when the window closes --%>
        function onUnload()
        {
            jQuery('input').off('.highlightField');
        }

        function dirtySearch()
        {
            jQuery('#updateGrid').show();
        }
    </script>
    
    <div ng-bootstrap="UserAdmin" user-admin></div>
    <form method="post" runat="server"></form>
</body>
</html>
