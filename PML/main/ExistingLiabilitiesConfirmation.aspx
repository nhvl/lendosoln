<%@ Page language="c#" Codebehind="ExistingLiabilitiesConfirmation.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.ExistingLiabilitiesConfirmation" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>ExistingLiabilitiesConfirmation</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	<script language=javascript>
<!--
function _init() {
  resize(550, 280);
}
function f_choice(choice) {
  var args = { choice: choice };
  onClosePopup(args);
}
//-->
</script>
    <h4 class="page-header">Existing Liabilities Confirmation</h4>
    <form id="ExistingLiabilitiesConfirmation" method="post" runat="server">
      <table width="100%">
        <tr><td style="color:black;font-weight:bold">There are existing liabilities.&nbsp; How should we proceed?</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td  style="color:black;font-weight:bold"><ul><li>Delete the existing liabilities and import new liabilities from the credit report</li><br>- or -
<li>Keep the existing liabilities and <i>attempt</i> to update them with new data from the credit report</li>        
        </ul>
        </td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td align=center>
            <input type=button value="Delete existing and import new" class="buttonstyle" onclick="f_choice(0);" >&nbsp;<input type=button value="Keep existing and update" class="buttonstyle" onclick="f_choice(1);">&nbsp;<input type=button value="Cancel" class="buttonstyle" onclick="f_choice(2);"></td></tr>
      </table>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
