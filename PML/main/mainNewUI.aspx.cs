namespace PriceMyLoan.UI.Main
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using ConfigSystem;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using PriceMyLoan.Common;

    public partial class mainNewUI : PriceMyLoan.UI.BasePage
    {
        private const string ErrorCode_VersionMismatch = "VERSIONMISMATCH";
        private const string ErrorCode_FieldEditPermissionDenied = "FIELD_EDIT_PERMISSION_DENIED";
        protected GetResultsNewUI m_getResults;
        protected Panel ActivityReportingPanel; 

        protected System.Web.UI.WebControls.PlaceHolder content;
        protected int m_currentIndex = 0;
        protected int m_previousIndex = 0;
        protected int m_maxVisitedIndex = 0;

        public string m_creditAction = "";
        public Guid m_AppId = Guid.Empty;

        protected bool m_hasError = false;
        private ArrayList m_list;
        private string[] m_statuses;

        public bool UserOnlyHasRunPMLToStep3
        {
            get
            {
                // User has "only run to step 3" but has neither "run PML registered loans" nor "run PML all loans"
                return
                (true == CurrentUserCanPerform(WorkflowOperations.RunPmlToStep3) &&
                 false == CurrentUserCanPerformAny(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans));
            }
        }

        protected string m_loanIdEncompassRequestType
        {
            get
            {
                return LoanID + "_" + PriceMyLoanRequestHelper.EncompassRequestType.ToString();
            }
        }
        protected int m_maxSteps 
        {
            get { return m_list.Count; }
        }
        protected bool m_allowGoToPipeline 
        {
            get { return PriceMyLoanUser.IsAllowGoToPipeline && !PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            List<WorkflowOperation> current = new List<WorkflowOperation> { 
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans, 
                WorkflowOperations.RunPmlToStep3,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.UseOldSecurityModelForPml,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock
            };

            return current;
        }

        public override IEnumerable<WorkflowOperation> GetReasonsForOp()
        {

            List<WorkflowOperation> current = new List<WorkflowOperation>() { 
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock
            };

            return current;
        }

        private bool OldEngineRedirectToDenial()
        {
            if (this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                return false; // 7/28/2006 dd - Skip permission checking if mode is rerun.

            bool canWrite = CurrentUserCanPerform(WorkflowOperations.WriteLoan);

            if (!canWrite)
            {
                // OPM 5068 Cord -- Take the user to the Access Denial page.
                return true;
            }
            return false;
        }

        private bool NewEgineRedirectToDenial()
        {
            if (false == CurrentUserCanPerform(WorkflowOperations.ReadLoan))
            {
                return true;
            }

            if (false == CurrentUserCanPerformAny(
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3))
            {
                return true;
            }

            return false;
        }

        private void CheckForPermission() 
        {
            bool newRedirectToDenial = NewEgineRedirectToDenial();
            bool oldRedirectToDenial = OldEngineRedirectToDenial();

            bool redirectToDenial = CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ? oldRedirectToDenial : newRedirectToDenial;

            if (redirectToDenial)
            {
                PriceMyLoanRequestHelper.AccessDenial(LoanID);
            }
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            CheckForPermission();
            this.EnableViewState = false;
            ClientScript.GetPostBackEventReference(this, ""); // NEED TO HAVE THIS LINE in order for .NET to register __doPostBack

            m_list = new ArrayList();
            m_list.Add(m_getResults);
            m_statuses = new string[m_list.Count];

            this.RegisterService("main", "/main/mainservice.aspx");
            this.RegisterJsScript("ModelessDlg.js");
            this.RegisterJsScript("utilities.js");
            //this.ActivityReportingPanel.Visible =  this.PriceMyLoanUser.IsRecordPmlActivity;
        }
        private int FindTabByKey(string key) 
        {
            int ret = -1;
            key = key.ToLower(); // 9/8/2004 dd - Perform case-insensitive search.
            for (int i = 0; i < m_list.Count; i++) 
            {
                ITabUserControl tab = (ITabUserControl) m_list[i];
                if (tab.TabID.ToLower() == key) 
                {
                    ret = i;
                    break;
                }
            }
            return ret;
        }



        private void ConfigureStep4Circumstances()
        {
            // Get the reasons why the user cannot perform the following operations
            List<string> reasons = GetReasonsUserCannotPerformOperation(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans).ToList();
            ConditionType failedConditionType = GetOperationFailedConditionType(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans);

            if (failedConditionType == ConditionType.Restraint)
            {
                Step4CircumstancesMessage.Text = "Pricing cannot be run for this loan file for the following circumstances:";
            }
            else
            {
                Step4CircumstancesMessage.Text = "Pricing may be run for this loan file under any of the following circumstances:";
            }

            // Show up to 10 reasons
            Step4CircumstancesList.DataSource = reasons.Take(10);
            Step4CircumstancesList.DataBind();        
        }
        
        private void SetExclusiveVisibleControl(int index)
        {
            for (int i = 0; i < m_list.Count; i++) 
            {
                if (i == index) m_statuses[i] = "'active'";
                else if (i <= m_maxVisitedIndex) m_statuses[i] = "'on'";
                else m_statuses[i] = "'off'";

                Control ctl = (Control) m_list[i];
                ctl.Visible = i == index;
                if (ctl.Visible) 
                {
                    if (ctl == m_getResults)
                    {
                        ConfigureStep4Circumstances();
                    }
                    ClientScript.RegisterHiddenField("_ClientID", ctl.ClientID);
                }
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            ErrorPanel.Visible = false;
            string errorCode = string.Empty;
            string errorMessage = string.Empty;
			// 02/05/08 mf. OPM 20137. For Citi, we cannot have the post movement between
			// steps report a 200 response.  It must return a 302 by using a redirect.
			// We should now redirect after each post, providing the navigation details
			// in the query string.

            PmlMainQueryStringManager mainQueryString = new PmlMainQueryStringManager(Request.QueryString);

			LoadNavigationInfoFromQueryString( mainQueryString );

			// Non-postback: 
			// 1. Load the appropriate step's data to its control
			// 2. Display only this step's control in the UI.
            if (!Page.IsPostBack) 
            {
                if (string.IsNullOrEmpty(mainQueryString.ErrorCode) == false)
                {
                    if (HandleErrorCode(mainQueryString.ErrorCode))
                    {
                        SetExclusiveVisibleControl(-1);
                        ClientScript.RegisterArrayDeclaration("_statuses", string.Join(",", m_statuses));
                        mainQueryString.ErrorCode = string.Empty; // 8/4/2010 dd - Clear out error code.
                        RegisterJsGlobalVariables("ErrorGobackUrl", "main.aspx?" + mainQueryString);
                        return; // 8/4/2010 dd - If error code is valid then we display and stop render further.
                    }                    
                }
				try 
				{
					if (m_currentIndex >= 0 && m_currentIndex < m_list.Count )
					{
						ITabUserControl ctl = (ITabUserControl) m_list[m_currentIndex];
						ctl.LoadData();
					}
				} 
				catch (NonCriticalException)
				{
					// 7/28/2004 dd - Do not switch tab if user error occur.
					m_currentIndex = m_previousIndex;
				}

				SetExclusiveVisibleControl(m_currentIndex);
				ClientScript.RegisterArrayDeclaration("_statuses", string.Join(",", m_statuses));
            }

			// Postback:
			// 1. Call the SaveData method of the posted step's control
			// 2. Redirect to this page with the step requested in the querystring
			else if (Page.IsPostBack) 
			{
                try
                {
                    m_getResults.SetAdditionalQueryStringValues(mainQueryString);
                }
                catch (VersionMismatchException)
                {
                    m_currentIndex = m_previousIndex;
                    errorCode = ErrorCode_VersionMismatch;
                }
                catch (LoanFieldWritePermissionDenied ex)
                {
                    m_currentIndex = m_previousIndex;
                    errorCode = ErrorCode_FieldEditPermissionDenied;                    
                    errorMessage = ex.UserMessage;
                }

                if (m_AppId != Guid.Empty)
                    mainQueryString.AddPair("appId", m_AppId.ToString());

				m_maxVisitedIndex = m_currentIndex < m_maxVisitedIndex ? m_maxVisitedIndex : m_currentIndex;
				// 8/4/2004 dd - Make sure MaxVisitedIndex never include the very last step.
				m_maxVisitedIndex = m_maxVisitedIndex <= m_list.Count - 2 ? m_maxVisitedIndex : m_list.Count - 2;

				// Perform the redirect to appropriate page.
				mainQueryString.Tabkey = ( (ITabUserControl) m_list[ m_currentIndex ] ).TabID;
				mainQueryString.PreviousIndex = m_currentIndex.ToString();
				mainQueryString.MaxVisitedIndex = m_maxVisitedIndex.ToString();
                mainQueryString.ErrorCode = errorCode;
                if (!string.IsNullOrEmpty(errorMessage))
                    mainQueryString.AddPair("errorMessage", errorMessage);                    
				Response.Redirect("mainNewUI.aspx?" + mainQueryString.ToString() + "&isBestPrice=" + RequestHelper.GetInt("isBestPrice"));
			}
        }

        private bool HandleErrorCode(string errorCode)
        {
            if (errorCode.Equals(ErrorCode_VersionMismatch))
            {
                ErrorPanel.Visible = true;
                m_errorMessage.Text = "ERROR: " + ErrorMessages.AnotherUserMadeChangeToLoan;
                m_hasError = true;
                return true;
            }
            else if (errorCode.Equals(ErrorCode_FieldEditPermissionDenied))
            {
                ErrorPanel.Visible = true;
                if (Request["errorMessage"] != null)
                    m_errorMessage.Text = Request["errorMessage"].ToString();
                else
                    m_errorMessage.Text = "Modifying loan field denied";
                m_hasError = true;
                return true;
            }
            return false;
        }

		private void LoadNavigationInfoFromQueryString (PmlMainQueryStringManager qsManager)
		{
			if (IsPostBack)
			{
				try 
				{
                    string eventArgument = Request.Form["__EVENTARGUMENT"];
                    // 3/4/2011 dd - eventArgument will have following format.
                    //  {tabIndex}:{key0}={value0}:{key1}={value1}:...
                    string[] parts = eventArgument.Split(':');

					m_currentIndex = int.Parse(parts[0]);

                    for (int i = 1; i < parts.Length; i++)
                    {
                        string[] data = parts[i].Split('=');
                        if (data.Length == 2)
                        {
                            qsManager.AddPair(data[0], data[1]);
                        }
                        else if (data.Length == 1)
                        {
                            // 3/7/2011 dd - Clear value.
                            qsManager.AddPair(data[0], string.Empty);
                        }
                    }
				} 
				catch 
				{
					if (qsManager.Tabkey != string.Empty) 
					{
						m_currentIndex = FindTabByKey(qsManager.Tabkey);
						m_currentIndex = m_currentIndex < 0 ? 0 : m_currentIndex; // 9/8/2004 dd - If key is not found then default to first tab.
					} 
				}
			}
			else
			{
				if (qsManager.Tabkey != string.Empty) 
				{
					m_currentIndex = FindTabByKey( qsManager.Tabkey );
					m_currentIndex = m_currentIndex < 0 ? 0 : m_currentIndex; // 9/8/2004 dd - If key is not found then default to first tab.
					//m_maxVisitedIndex = m_currentIndex;
				}
			}
				
			if ( qsManager.PreviousIndex != string.Empty ) m_previousIndex = int.Parse( qsManager.PreviousIndex );
			if ( qsManager.MaxVisitedIndex != string.Empty ) m_maxVisitedIndex = int.Parse( qsManager.MaxVisitedIndex );
		}

        protected bool m_isLogin 
        {
            get 
            {
                return m_allowGoToPipeline;
                // Determine if user access this step from login screen or through pmlsiteid
                //return RequestHelper.GetSafeQueryString("islogin") == ConstAppDavid.PmlStrangeKey;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {    
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
    }
}
