using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using LendersOfficeApp.los.RatePrice;
using PriceMyLoan.Security;
using PriceMyLoan.UI.Main;
using LendersOffice.Constants;
using DataAccess.LoanComparison;
namespace PriceMyLoan.main
{
	public partial class CertificateWaitPageService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            Guid loanID = GetGuid("loanID", Guid.Empty);
            if (loanID != Guid.Empty)
            {
                DuplicateFinder.SetSkipLoanSubmissionCheck(loanID, true);
            }
            switch (methodName)
            {
                case "SubmitUnderwriting":
                    SubmitUnderwriting();
                    break;
                case "IsAvailable":
                    IsAvailable();
                    break;
                case "MinuteWarning":
                    MinuteWarning();
                    break;
                case "ReportTimeout":
                    ReportTimeout();
                    break;

            }
            
        }

        private string BuildRequestedRate(string rate, string fee, string id) 
        {
            return string.Format("{0}:{1}:{2}", rate, fee, id);
        }

        private void SubmitUnderwriting() 
        {
            Guid productID = GetGuid("productID");
            string version = GetString("version");
            string uniqueChecksum = GetString("uniquechecksum", "");
            string productRate = GetString("productRate");
            string productPoint = GetString("productPoint");
            string productRawId = GetString("productRawId");
            string requestedRate = this.BuildRequestedRate(productRate, productPoint, productRawId);
            E_RenderResultModeT renderResultModeT = E_RenderResultModeT.Regular;

            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;

            Guid loanID = GetGuid("loanID");

            CPageData dataLoan = new CCheckFor2ndLoan(loanID); // dd 8/5/05 Temporary use this data object for sProdLpePriceGroupId
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            Guid lpePriceGroupId = dataLoan.sProdLpePriceGroupId;
            E_sLienQualifyModeT sLienQualifyModeT = (E_sLienQualifyModeT) GetInt("sLienQualifyModeT");

            Guid firstLienLpId = GetGuid("FirstLienLpId", Guid.Empty);
            decimal firstLienNoteRate = GetRate("FirstLienNoteRate", 0);
            decimal firstLienPoint = GetRate("FirstLienPoint", 0);
            decimal firstLienMPmt = GetMoney("FirstLienMPmt", 0);
            decimal secondLienMPmt = GetMoney("SecondLienMPmt", 0);
            Guid pinStateId = GetGuid("PinStateId", Guid.Empty);
            PricingState state = null;

            if (pinStateId != Guid.Empty)
            {
                var states = dataLoan.GetPricingStates();
                state = states.GetState(pinStateId);

                if (state == null)
                {
                    throw new CBaseException("Pin State has been deleted. Please rerun comparison report.", "Pin State has been deleted. Please rerun comparison report.");
                }
            }

            string sTempLpeTaskMessageXml = "";

            if (dataLoan.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct) 
            {
                // ^ If we are only going to rerun the registered program, we should have the
                //   task message XML on file, so we don't need to construct it for the 2nd.
                sTempLpeTaskMessageXml = dataLoan.sTempLpeTaskMessageXml;
            }
            else if (sLienQualifyModeT == E_sLienQualifyModeT.OtherLoan)
            {
                // ^ Else if that is not the case and we are running the combo 2nd, we need
                //   to dummy up the 1st lien task message XML to re-apply the selected 1st
                //   to base the 2nd lien on.
                string requested1stLienRate = this.BuildRequestedRate(firstLienNoteRate.ToString(), firstLienPoint.ToString(), string.Empty);

                sTempLpeTaskMessageXml = DistributeUnderwritingEngine.GenerateTemporaryAcceptFirstLoanMessage(
                    principal: principal,
                    loanId: loanID,
                    productId: firstLienLpId,
                    requestedRate: requested1stLienRate,
                    lpePriceGroupId: dataLoan.sProdLpePriceGroupId,
                    version: string.Empty,
                    debugResultStartTicks: "0",
                    renderResultModeT: renderResultModeT);
            }

            Guid requestID = DistributeUnderwritingEngine.SubmitToEngineForRenderCertificate(principal, loanID, productID, requestedRate, 
                sLienQualifyModeT, lpePriceGroupId, sTempLpeTaskMessageXml, version, renderResultModeT,
                E_sPricingModeT.RegularPricing, uniqueChecksum, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, state, GetString("parId", ""));

            int polling = DistributeUnderwritingEngine.GetSuggestedPollingInterval(requestID);
            SetResult("RequestID", requestID);
            SetResult("Polling", polling);
            SetResult("InvalidRateOption", "False");

        }
        private void IsAvailable() 
        {
            Guid requestID = GetGuid("requestid");

            bool isAvailable = LpeDistributeResults.IsResultAvailable(requestID);

            SetResult("IsAvailable", isAvailable ? "1" : "0");
        }

        private void ReportTimeout() 
        {
            try 
            {
                Guid loanID = GetGuid("loanID");
                Guid requestID = GetGuid("RequestID");
                Guid productID = GetGuid("ProductID");
                PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;

                StringBuilder sb = new StringBuilder();

                sb.Append("Preview Certificate\n");
                sb.Append("LoanId=" + loanID + ", ProductID=" + productID + ", principal=" + principal);
                sb.Append(Environment.NewLine).Append("LPE_REQUEST").Append(Environment.NewLine).Append(Environment.NewLine);
            
                SqlParameter[] parameters = { new SqlParameter("@LpeRequestBatchId", requestID) };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "CaptureLpeRequestByBatchId", parameters)) 
                {
                    while (reader.Read()) 
                    {
                        sb.AppendFormat("  LpeRequestBatchId={7}, CreatedDate={0}, ExpiredDate={1}, CurrentPartInBatch={2}, NumberOfRequestsInBatch={3}, LpeRequestType={4}{5}",
                            reader["LpeRequestCreatedDate"], reader["LpeRequestExpiredDate"], reader["LpeRequestCurrentPartInBatch"],
                            reader["LpeRequestNumberOfRequestsInBatch"], reader["LpeRequestType"], Environment.NewLine, requestID);
                    }
                }

                CBaseException exc = new LPETimeoutException(sb.ToString());

                Tools.LogError(exc);
            } 
            catch {}
        }
        private void MinuteWarning() 
        {
            Guid loanID = GetGuid("loanID");
            Guid productID = GetGuid("productID", Guid.Empty);
            PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) this.User;

            // 3/13/2006 dd - OPM 4324 - Only log as warning in PB.
            Tools.LogWarning("LPE 1 minute warning. Preview Certificate. LoanId=" + loanID + ", ProductID=" + productID + ", principal=" + principal);
        }
	}
}
