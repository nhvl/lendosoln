/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace PriceMyLoan.main
{
	/// <summary>
	/// Summary description for LoanProspectorMainFrame.
	/// </summary>
	public partial class LoanProspectorMainFrame : PriceMyLoan.UI.BasePage
	{
        protected string m_url;

		protected void PageLoad(object sender, System.EventArgs e)
		{
            string cmd = RequestHelper.GetSafeQueryString("cmd");

            if (string.IsNullOrEmpty(cmd))
            {
                m_url = "LoanProspectorMain.aspx?loanid=" + RequestHelper.LoanID + "&entrypoint=" + Server.HtmlEncode(RequestHelper.GetSafeQueryString("entrypoint"));
            }
            else
            {
                m_url = "LoanProspectorSendToSystem.aspx?cmd=" + Server.HtmlEncode(cmd);
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
