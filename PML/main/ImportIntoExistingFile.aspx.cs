﻿using System;
using System.Collections.Generic;
using LendersOffice.Conversions;
using System.IO;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.ConfigSystem.Operations;

namespace PriceMyLoan.main
{
    public partial class ImportIntoExistingFile : PriceMyLoan.UI.BasePage
    {
        private const string UnexpectedUploadError = "An unexpected error occurred while uploading your file. If this persists, please contact support.";
        private const string InsufficientAccessError = "You do not have access to modify this loan file.";

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new[] { WorkflowOperations.ImportIntoExistingFile };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !this.CurrentUserCanPerform(WorkflowOperations.ImportIntoExistingFile))
            {
                throw new AccessDenied("User does not have permission to import into this file.");
            }

            var dataLoan = CPageData.CreateUsingSmartDependency(
                this.LoanID,
                typeof(ImportIntoExistingFile));
            dataLoan.InitLoad();

            this.sLNm.Text = dataLoan.sLNm;
            this.aBNm.Text = dataLoan.GetAppData(0).aBNm;
            this.sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            this.sSpAddr.Text = dataLoan.sSpAddr_SingleLine;
        }

        protected void OnUploadClick(object sender, EventArgs e)
        {
            if (this.FileUpload.PostedFile.ContentLength == 0)
            {
                UploadError.Text = "Please select a file to upload.";
                return;
            }

            if (!Path.GetExtension(this.FileUpload.PostedFile.FileName).Equals(".fnm", StringComparison.OrdinalIgnoreCase))
            {
                UploadError.Text = "Please select a file with the .fnm extension for upload.";
                return;
            }

            string fileContent;

            using (var streamReader = new StreamReader(this.FileUpload.PostedFile.InputStream))
            {
                try
                {
                    fileContent = streamReader.ReadToEnd();
                }
                catch (OutOfMemoryException)
                {
                    UploadError.Text = UnexpectedUploadError;
                    return;
                }
                catch (IOException)
                {
                    UploadError.Text = UnexpectedUploadError;
                    return;
                }
            }

            try
            {
                var fannieMae32Importer = new FannieMae32Importer(FannieMaeImportSource.PriceMyLoan);
                fannieMae32Importer.ImportIntoExisting(fileContent, this.LoanID);

                this.Close.Value = true.ToString();
            }
            catch (LoanFieldWritePermissionDenied)
            {
                UploadError.Text = InsufficientAccessError;
            }
            catch (PageDataAccessDenied)
            {
                UploadError.Text = InsufficientAccessError;
            }
            catch (CBaseException exc)
            {
                UploadError.Text = exc.UserMessage;
            }
        }
    }
}
