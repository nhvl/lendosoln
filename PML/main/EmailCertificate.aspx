<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="PriceMyLoan.Common"%>
<%@ Register TagPrefix="uc1" TagName="ModelessDlg" Src="../common/ModalDlg/ModelessDlg.ascx" %>
<%@ Page language="c#" Codebehind="EmailCertificate.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.EmailCertificate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head runat="server">
		<title>EmailCertificate</title>
	</head>
	<body>
	    <script type="text/javascript">
	        jQuery(function($){
	            $('#btnCancel').on('click', function(){ window.parent.closeEmail();});
	        });
            function _init() {
                resizeForIE6And7(530, 470);
                var oEmailAddr = <%= AspxTools.JsGetElementById(EmailAddr) %>;
                oEmailAddr.focus();
                validateEmail();
            }
            function trim(stringToTrim) {
	            return stringToTrim.replace(/^\s+|\s+$/g,"");
            }

            function IsEmailValid(email)
            { 
                var isValid = true; 
	            if ( email == null || email == '' ) 
	            { 
	                isValid = false; 
                }
                else 
                {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ 
                    isValid = re.test(email.replace(/^\s+|\s+$/g, ''));
                }
	            return isValid;
            }
            
            function IsEmailListValid(str)
            {
                var valid = true;
                var y =  str.split(';');
                for( var i = 0; i < y.length; i++)
                {
                    if ( trim(y[i]) == '' ) { continue; }
                    valid = valid && IsEmailValid(trim(y[i]));
                }
                return valid;
            }
            
            function IsCustomValid() 
            {
                var text = document.getElementById(<%= AspxTools.JsGetClientIdString(FromEmailAddr)  %>); 
                var error = document.getElementById('customFromWrong');
                var email = trim(text.value);

                if (! document.getElementById(<%= AspxTools.JsGetClientIdString( sendFromCustom ) %>).checked ) 
                {
                    $j(error).text('');
                    return true;
                }
       
                if ( email == '') 
                {
                    $j(error).text('* Required'); 
                    return false;
                }
                else if ( IsEmailValid(trim(text.value)) ) 
                {
                    $j(error).text('');
                    return true; 
                }
                else 
                {
                    $j(error).text('* Invalid');
                    return false;
                }
            }
            
            function IsCCValid() 
            {
                
                var ccField = document.getElementById(<%= AspxTools.JsGetClientIdString( CCAddr ) %>); 
                var ccError = document.getElementById('ccInvalid');
                var emails = trim(ccField.value);
                if ( emails == '' ) { $j(ccError).text(''); return true; }
                if (! IsEmailListValid(emails) ) 
                {
                    $j(ccError).text('* Invalid');
                    return false;
                }
                $j(ccError).text('');
                return true;
            }
            function IsToValid() 
            {
                var toField = document.getElementById(<%= AspxTools.JsGetClientIdString( EmailAddr ) %>); 
                var toError = document.getElementById('toEmailErr');
                var emails = trim(toField.value);
                if ( emails == '' ) { $j(toError).text('* Required'); return false; }
                if ( !IsEmailListValid(emails) ) 
                {
                    $j(toError).text('* Invalid');
                    return false;
                }
                $j(toError).text('');
                return true;
            }
            
        
            function validateEmail() {
                var c = IsCustomValid();
                var d = IsCCValid(); 
                var e = IsToValid();
                var isValid = c && d && e;
                document.getElementById("btnSendEmail").disabled = !isValid;
                return isValid;
            }

            function f_sendEmail() {
                if ( !validateEmail() ) {
                    alert('Fix any errors before proceeding.');
                    return;
                }
              var args = new Object();
              args["LoanID"] =              <%= AspxTools.JsString(RequestHelper.LoanID) %>;
              args["ProductID"] =           <%= AspxTools.JsString(RequestHelper.GetGuid("productid")) %>;
              args["CCAddr"] =              <%= AspxTools.JsGetElementById(CCAddr) %>.value; 
              args["EmailAddr"] =           document.getElementById(<%= AspxTools.JsGetClientIdString( EmailAddr ) %>).value; 
              args["Subject"] =             <%= AspxTools.JsGetElementById(Subject) %>.value;
              args["Note"] =                <%= AspxTools.JsGetElementById(Note) %>.value;
              args["FromEmailAddr"] =       GetFromAddress();
              args["mode"] =                <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("mode")) %>;
              args["version"] =             <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("version")) %>;
              args["sLienQualifyModeT"] =   <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("lienqualifymodet")) %>;
              args["uniquechecksum"] =      <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("uniquechecksum")) %>;
              args["productRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productRate")) %>;
              args["productPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productPoint")) %>;
              args["productRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productRawId")) %>;
              var result = gService.main.call("SendEmail", args, true, true, true);
              if (!result.error) {
                var errorMessage = result.value["ErrMsg"];
                if (errorMessage != null && errorMessage != "") {
                  alert(errorMessage);
                } else {
                  alert('Send Completed');
                }
                window.parent.closeEmail();
              } else {
                alert('Unable to send email please try again.');
              }
            }
            
            function GetFromAddress() 
            {
                var email = '';
                var corporate = document.getElementById(<%= AspxTools.JsGetClientIdString( sendFromCorporate) %>);
                var currentUser = document.getElementById(<%= AspxTools.JsGetClientIdString( sendFromCurrentUser ) %>);
                if ( corporate && corporate.checked ) 
                {   
                    if ( document.getElementById('eventLabel') &&  trim($j('#eventLabel').text()) != '' ) 
                    {
                        email += '"' + trim($j('#eventLabel').text()) + '"'; 
                    }
                    email += '<' + document.getElementById('eventEmail').innerHTML + '>'; 
                }
                else if ( currentUser.checked )  
                {
                    if ( document.getElementById('userLabel') &&  trim($j('#userLabel').text()) != '' ) 
                    {
                        email = '"' + trim(document.getElementById('userLabel').innerHTML) + '"'; 
                    }
                    email += '<' + document.getElementById('userMailTo').innerHTML + '>';
                }
                else 
                {
                    email = document.getElementById(<%= AspxTools.JsGetClientIdString(FromEmailAddr) %>).value;
                }
                return email;
            }
            
            function ToggleCustom(obj) 
            {
                obj.checked = true;
                var custom = document.getElementById(<%= AspxTools.JsGetClientIdString(sendFromCustom) %>);
                $j('#FromEmailAddr').readOnly(!custom.checked);
                window.setTimeout("validateEmail()", 100);
               return true;
            }
            <%-- OPM 4256 Cord, loads a the rolodex for picking an e-mail from contacts. For embedded pml users only. --%>
            <% if ( PriceMyLoanConstants.IsEmbeddedPML ) { %>
            function pickContacts() {
	            showModal('/../los/RolodexList.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID) %>, null, null, null, function(args){
                     if (args.OK)
                    {
                        var emailTxtBox = <%= AspxTools.JsGetElementById(EmailAddr) %>;
                        
                        <%-- // ( emailTxtBox.value.trim ) --%>
                        if ( emailTxtBox.value.replace(/^\s+|\s+$/, '') == "" )
                        {
                            emailTxtBox.value = args.AgentEmail;
                        }
                        else
                        {
                            emailTxtBox.value += ";" + args.AgentEmail;
                        }
                        
                        validateEmail();
                    }
                },{hideCloseButton:true}); <%-- //Rolodex.chooseFromRolodex(); --%>
            }


        <% } %>



//-->
		</script>
		<form id="EmailCertificate" method="post" runat="server">
			<table id="Table1" cellspacing="1" cellpadding="1" style="table-layout:fixed">
				<tr>
					<td class="Subheader" noWrap colSpan="2">Email Preview Certificate</td>
					<td style="text-align: right"></td>
					<td style="width: 20em"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
				    <td class="FieldLabel">From</td>				    
				    <td colspan="2"> 
				        <asp:RadioButton Checked="true" runat="server" onclick="return ToggleCustom(this)" ID="sendFromCurrentUser"  GroupName="From"/> 
				        <span runat="server" ID="userLabel"> </span>
				        &lt;<a runat="server" id="userMailTo"></a>&gt;
				    </td>
				    <td></td>
				</tr>
				<tr runat="server" visible="false" id="fromEventRow">
				    <td></td>
				    <td colspan="2">
				        <asp:RadioButton GroupName="From" runat="server" onclick="return ToggleCustom(this)" id="sendFromCorporate" /> 
				        <span runat="server" ID="eventLabel" ></span>
				        <span runat="server"  visible="false" id="eventStartPattern" >&lt;</span><a runat="server" id="eventEmail"></a><span runat="server"  visible="false" id="eventEndPattern">&gt;</span>
				    </td>
				    <td></td>
				</tr>
				<tr style="padding-bottom: 10px">
					<td class="FieldLabel"></td>
					<td colspan="2">
					    <asp:RadioButton runat="server" onclick="return ToggleCustom(this)" ID="sendFromCustom" GroupName="From" />
					    <input type="text" id="FromEmailAddr"  readonly="readonly" onblur="validateEmail()" onkeyup="validateEmail()" runat="server" style="width:310px "/></td>
					<td >
					    <span style="color:red" id="customFromWrong" ></span>
					</td>
				</tr>
				<tr >				
				    <td>&nbsp;</td>
				    <% if ( PriceMyLoanConstants.IsEmbeddedPML ) { %>
				    <td colspan="2" style="padding:0.2em 0 0.2em 0">
						<a href="#" onclick="pickContacts();">Insert Recipient from Contacts</a>						
					</td>
				    <% } %>
				</tr>
				<tr>
					<td class="FieldLabel">To</td>
					<td  colspan="2">
						<asp:textbox id="EmailAddr" onkeyup="validateEmail()" onblur="validateEmail()" runat="server" Width="344px"></asp:textbox>
					</td>
					<td >
					    <span style="color:red" id="toEmailErr" ></span>
					</td>
				</tr>
				
                <tr > 
					<td class="FieldLabel">CC</td>
					<td nowrap colspan="2"> 
						<asp:TextBox ID="CCAddr" onkeyup="validateEmail();" onblur="validateEmail();" Runat="server" Width="344px" > </asp:TextBox>
					</td>
					<td id="ccInvalid" style="color:red">
					
					</td>
				</tr>
				<tr >
					<td class="FieldLabel">Subject&nbsp;&nbsp;</td>
					<td noWrap colspan="2"><asp:textbox id="Subject" onkeyup="validateEmail();" runat="server" Width="344px" MaxLength="100">Loan Program Details</asp:textbox></td>
					<td></td>
				</tr>
				<tr >
					<td class="FieldLabel" vAlign="top" style="padding-top: 5px" >Body</td>
					<td colspan="2"><asp:textbox id="Note" runat="server" Width="300px" Height="206px" TextMode="MultiLine"></asp:textbox></td>
					<td></td>
				</tr>
				<tr>
					<td noWrap></td>
					<td noWrap width="75">&nbsp;</td>
					<td noWrap>&nbsp;</td>
					<td noWrap>&nbsp;</td>
				</tr>
				<tr>
					<td noWrap></td>
					<td colspan="3" >
					    <input class="ButtonStyle" id="btnSendEmail" onclick="f_sendEmail();" type="button" value="Send Email"/>&nbsp;&nbsp;
						<input class="ButtonStyle" id="btnCancel" type="button" value="Cancel"/>
					</td>
				</tr>
			</table>
			<uc1:ModelessDlg id="ModelessDlg1" runat="server"></uc1:ModelessDlg></form>
	</body>
</html>
