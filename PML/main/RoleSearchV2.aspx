﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoleSearchV2.aspx.cs" Inherits="PriceMyLoan.main.RoleSearchV2" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <style>
        a { cursor: pointer; }

        th.headerSortUp {
            background-image: url(../images/Tri_DESC.gif);
            background-repeat: no-repeat;
            background-position: 80px 50%;
        }

        th.headerSortDown {
            background-image: url(../images/Tri_ASC.gif);
            background-repeat: no-repeat;
            background-position: 80px 50%;
        }

        .warning {
            padding: 5px;
            padding-top: 1.5em;
            color: #EA1C0D;
        }
    </style>
</head>
<body>
<div lqb-popup class="modal-status-agent">
    <div class="modal-header">
        <button type="button" class="close modal-close-btn"><i class="material-icons">&#xE5CD;</i></button>
        <h4 class="modal-title"><ml:EncodedLiteral ID="m_Title" runat="server"/></h4>
    </div>
    <div class="modal-body">
        <div ng-app="RoleSearch" role-search></div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-flat modal-close-btn" type="button">Close</button>
    </div>
</div>
<form runat="server"></form>
</body>
</html>
