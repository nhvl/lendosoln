﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;
using PriceMyLoan.Common;
using PriceMyLoan.Security;

using LendersOffice.Common;
using PriceMyLoan.DataAccess;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using MeridianLink.CommonControls;

namespace PriceMyLoan.main
{
    public partial class ImportLoanDataAudit : PriceMyLoan.UI.BasePage
    {
        private bool showWarning = false;

        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }

        protected bool m_allowGoToPipeline
        {
            get { return PriceMyLoanUser.IsAllowGoToPipeline; }
        }

        protected override bool IncludeMaterialDesignFiles => false;

        private void CheckForPermission()
        {
            if (this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                return; // 7/28/2006 dd - Skip permission checking if mode is rerun.

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(PriceMyLoanUser.ConnectionInfo, PriceMyLoanUser.BrokerId, LoanID, WorkflowOperations.WriteLoanOrTemplate);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(PriceMyLoanUser));

            bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);

            //AccessControlProcessor accessControlProcessor = new AccessControlProcessor();
            //AccessBinding accessBinding = accessControlProcessor.Resolve(PriceMyLoanUser, LoanID);

            if (!canWrite)
            {
                // OPM 5068 Cord -- Take the user to the Access Denial page.
                PriceMyLoanRequestHelper.AccessDenial(LoanID);
            }

        }

        private void DisplayLoanSummary()
        {
            CPageData dataLoan = new CPmlLoanSummaryData(LoanID);
            dataLoan.InitLoad();

            sLNm.Text = dataLoan.sLNm;
            aBNm.Text = dataLoan.GetAppData(0).aBNm;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep.Replace(".00", "");
            // 10/8/2004 dd - Temporary Hack to remove .000;
            sLtvR.Text = dataLoan.sLtvR_rep.Replace(".000", "");
            sCltvR.Text = dataLoan.sCltvR_rep.Replace(".000", "");
            sCreditScoreType1.Text = dataLoan.sCreditScoreType1_rep;
            sCreditScoreType2.Text = dataLoan.sCreditScoreType2_rep;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML)
            {
                string url = string.Format("main.aspx?loanid={0}", LoanID);
                Response.Redirect(url);
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckForPermission();

            if (!Page.IsPostBack)
            {
                TransformData();
                DisplayLoanSummary();
            }

            this.PageID = "ImportedLoanDataAudit";

            CPageData data = new CFHATotalAuditData(LoanID);
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            data.InitLoad();


            List<CAppData> apps = new List<CAppData>(data.nApps);
            decimal totalLiquidAssets = 0;
            int totalMortLiab = 0;
            int totalRepos = 0;

            for (int i = 0; i < data.nApps; i++)
            {
                CAppData currentApp = data.GetAppData(i);
                if (currentApp.aIsPrimary)
                {
                    ls_PrimaryBorrower.Text = currentApp.aBNm;
                }
                totalMortLiab += currentApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Mortgage | E_DebtGroupT.Heloc).Count;
                totalLiquidAssets += currentApp.aAsstLiqTot;
                totalRepos += currentApp.aReCollection.CountRegular + currentApp.aReCollection.CountSpecial;
                apps.Add(currentApp);


            }

            Applications.DataSource = apps;
            Applications.DataBind();

            ls_AppraisedValue.Text = data.sApprVal_rep;
            ls_CashftBorrower.Text = data.sTransNetCash_rep;
            ls_LoanAmount.Text = data.sLAmtCalc_rep;
            ls_LoanPurpose.Text = data.sLPurposeT_rep;
            ls_MortgageLiabilities.Text = totalMortLiab.ToString();
            ls_NumberOfTotalApps.Text = data.nApps.ToString();
            ls_PurchasePrice.Text = data.sPurchPrice_rep;
            ls_ReoPropertiesCount.Text = totalRepos.ToString();
            ls_RepresentativeScore.Text = data.sCreditScoreType2Soft_rep;
            ls_SellerClosingCostPaid.Text = data.sTotCcPboPbs_rep;
            ls_TotalCosts.Text = data.sTotTransC_rep;
            ls_TotalLiquidAssets.Text = totalLiquidAssets.ToString("C");
            ls_TotalLoanAmount.Text = data.sFinalLAmt_rep;
            ls_TotalMonthlyIncome.Text = data.sLTotI_rep;
            ls_TotalNonMortgagePayments.Text = data.sLiaMonLTot_rep;
            ls_UpfrontMip.Text = data.sFfUfmip1003_rep;
            ls_UpfrontMipFinanced.Text = data.sFfUfmipFinanced_rep;
            ls_ltvcltv.Text = string.Format("{0} / {1}", data.sLtvR_rep, data.sCltvR_rep);

            sLTotITotalScorecardWarning.Text = data.sLTotIPmlImportWarning;
            sApprValTotalScorecardWarning.Text = data.sApprValPmlImportWarning;
            sFfUfmip1003TotalScorecardWarning.Text = data.sFfUfmip1003PmlImportWarning;
            sBorrCountTotalScorecardWarning.Text = data.sBorrCountPmlImportWarning;

            sPurchPriceTotalScorecardWarning.Text = data.sPurchPricePmlImportWarning;

            sLAmtCalcTotalScorecardWarning.Text = data.sLAmtCalcPmlImportWarning;

            if (data.sLTotIPmlImportWarning.Length != 0 ||
                data.sApprValPmlImportWarning.Length != 0 ||
                data.sFfUfmip1003PmlImportWarning.Length != 0 ||
                data.sBorrCountPmlImportWarning.Length != 0 ||
                data.sPurchPricePmlImportWarning.Length != 0 ||
                data.sLAmtCalcPmlImportWarning.Length != 0)
            {
                showWarning = true;
            }

            warningMsg.Visible = showWarning;
        }

        protected void Applications_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            CAppData data = (CAppData)args.Item.DataItem;
            var isTargetingUlad2019 = data.LoanData.sIsTargeting2019Ulad;

            EncodedLiteral aBEmplrJobTitleTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBEmplrJobTitleTotalScorecardWarning");
            EncodedLiteral aCEmplrJobTitleTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCEmplrJobTitleTotalScorecardWarning");
            EncodedLiteral aBSsnTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBSsnTotalScorecardWarning");
            EncodedLiteral aCSsnTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCSsnTotalScorecardWarning");
            EncodedLiteral aBDecResidencyTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBDecResidencyTotalScorecardWarning");
            EncodedLiteral aCDecResidencyTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCDecResidencyTotalScorecardWarning");
            EncodedLiteral aBDecOccTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBDecOccTotalScorecardWarning");
            EncodedLiteral aCDecOccTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCDecOccTotalScorecardWarning");




            EncodedLiteral borrowerFullName = (EncodedLiteral)args.Item.FindControl("BorrowerFullName");
            EncodedLiteral borrowerSsn = (EncodedLiteral)args.Item.FindControl("BorrowerSsn");
            EncodedLiteral borrowerCititzenshipStatus = (EncodedLiteral)args.Item.FindControl("BorrowerCititzenshipStatus");
            EncodedLiteral borrowerTitle = (EncodedLiteral)args.Item.FindControl("BorrowerTitle");
            EncodedLiteral borrowerCreditScores = (EncodedLiteral)args.Item.FindControl("BorrowerCreditScores");
            EncodedLiteral borrowerOccupancy = (EncodedLiteral)args.Item.FindControl("BorrowerOccupancy");
            EncodedLiteral coborrowerName = (EncodedLiteral)args.Item.FindControl("CoborrowerName");
            EncodedLiteral coborrowerSsn = (EncodedLiteral)args.Item.FindControl("CoborrowerSsn");
            EncodedLiteral coborrowerCitizenshipStatus = (EncodedLiteral)args.Item.FindControl("CoborrowerCitizenshipStatus");
            EncodedLiteral coborrowerTitle = (EncodedLiteral)args.Item.FindControl("CoborrowerTitle");
            EncodedLiteral coborrowerScores = (EncodedLiteral)args.Item.FindControl("CoborrowerScores");
            EncodedLiteral coborrowerOccupancy = (EncodedLiteral)args.Item.FindControl("CoborrowerOccupancy");
            EncodedLiteral monthlyIncome = (EncodedLiteral)args.Item.FindControl("MonthlyIncome");
            EncodedLiteral liquidAssets = (EncodedLiteral)args.Item.FindControl("LiquidAssets");
            EncodedLiteral nonMortgagePayment = (EncodedLiteral)args.Item.FindControl("NonMortgagePayment");
            EncodedLiteral appNumber = (EncodedLiteral)args.Item.FindControl("AppNumber");
            PlaceHolder placeHolder = (PlaceHolder)args.Item.FindControl("CoborrowerFields");

            appNumber.Text = (args.Item.ItemIndex + 1).ToString();
            borrowerFullName.Text = data.aBNm;
            borrowerSsn.Text = data.aBSsnMasked;
            borrowerCititzenshipStatus.Text = data.aBCitizenDescription;
            borrowerTitle.Text = data.aBEmplrJobTitle;
            borrowerCreditScores.Text = string.Format("XP: {0} TU: {1} EF: {2}", data.aBExperianScore_rep, data.aBTransUnionScore_rep, data.aBEquifaxScore_rep);
            borrowerOccupancy.Text = isTargetingUlad2019 ? data.aBDecIsPrimaryResidenceUlad.ToYN() : data.aBDecOcc;
            aBEmplrJobTitleTotalScorecardWarning.Text = data.aBEmplrJobTitlePmlImportWarning;
            aBSsnTotalScorecardWarning.Text = data.aBSsnPmlImportWarning;
            aBDecResidencyTotalScorecardWarning.Text = data.aBDecResidencyPmlImportWarning;
            aBDecOccTotalScorecardWarning.Text = data.aBDecOccPmlImportWarning;


            if (data.aBEmplrJobTitlePmlImportWarning.Length != 0 ||
                data.aBSsnPmlImportWarning.Length != 0 ||
                data.aBDecResidencyPmlImportWarning.Length != 0 ||
                data.aBDecOccPmlImportWarning.Length != 0)
            {
                showWarning = true;
            }

            placeHolder.Visible = data.aCIsValidNameSsn;

            if (data.aCIsValidNameSsn)
            {
                coborrowerCitizenshipStatus.Text = data.aCCitizenDescription;
                coborrowerName.Text = data.aCNm;
                coborrowerOccupancy.Text = isTargetingUlad2019 ? data.aCDecIsPrimaryResidenceUlad.ToYN() : data.aCDecOcc;
                coborrowerScores.Text = string.Format("XP: {0} TU: {1} EF: {2}", data.aCExperianScore_rep, data.aCTransUnionScore_rep, data.aCEquifaxScore_rep);
                coborrowerSsn.Text = data.aCSsnMasked;
                coborrowerTitle.Text = data.aCEmplrJobTitle;

                aCEmplrJobTitleTotalScorecardWarning.Text = data.aCEmplrJobTitlePmlImportWarning;
                aCSsnTotalScorecardWarning.Text = data.aCSsnPmlImportWarning;
                aCDecResidencyTotalScorecardWarning.Text = data.aCDecResidencyPmlImportWarning;
                aCDecOccTotalScorecardWarning.Text = data.aCDecOccPmlImportWarning;

                if (data.aCEmplrJobTitlePmlImportWarning.Length != 0 ||
                    data.aCSsnPmlImportWarning.Length != 0 ||
                    data.aCDecResidencyPmlImportWarning.Length != 0 ||
                    data.aCDecOccPmlImportWarning.Length != 0)
                {
                    showWarning = true;
                }
            }

            monthlyIncome.Text = data.aBTotI_rep;
            liquidAssets.Text = data.aAsstLiqTot_rep;
            nonMortgagePayment.Text = data.aLiaMonTot_rep;

            warningMsg.Visible = showWarning;
        }

        protected void m_btnNext_Click(object sender, System.EventArgs e)
        {
            string url = string.Format("agents.aspx?loanid={0}", LoanID);
            Response.Redirect(url);
        }
    }
   
}