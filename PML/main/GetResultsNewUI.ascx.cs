namespace PriceMyLoan.UI.Main
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;
    using ConfigSystem;
    using global::DataAccess;
    using global::DataAccess.Core.Construction;
    using global::DataAccess.LoanComparison;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Events;
    using LendersOffice.Migration;
    using LendersOffice.PriceMyLoan;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using PriceMyLoan.Common;
    using PriceMyLoan.UI;

    public partial class GetResultsNewUI : BaseUserControl, ITabUserControl
    {
        protected bool m_showQm = false;
        protected bool m_showRateForIneligible = false;
        protected bool m_has2ndLoan = false;
        protected E_sLienQualifyModeT m_currentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan;
        protected bool m_isWaiting = true;
        protected string m_resultHeader = "Results";
        protected bool m_hasLinkedLoan = false;
        protected bool m_hasError = false;
        protected LosConvert m_losConvert = new LosConvert();
        protected System.Web.UI.WebControls.Repeater m_ineligibleRateRepeater;
        private Hashtable m_lIsArmMarginDisplayedHash = new Hashtable();
        protected List<object> m_gDataLpList = new List<object>();
        protected List<object> m_gDataRateOptionList = new List<object>();
        protected List<object> m_gDataList = new List<object>();
        protected bool m_isSkip2ndLoan = false;
        protected long m_renderResultDuration = 0L;
        protected string m_renderResultDurationDetail = "";
        protected long m_calcServerDuration = 0L;
        protected int m_calcServerCount = 0;
        protected bool m_isLpeManualSubmissionAllowed = true;
        protected string m_batchCreatedTime = "";
        protected string m_sortString = ""; // OPM 9712
        protected bool m_showMaxDtiWarning = false;
        protected string m_waitingMessage = "Processing your request...";
        protected bool m_isLpeDisqualificationEnabled = true;
        protected bool m_isOptionARMUsedInPml = true; // 01/23/08 mf. OPM 19782
        protected bool m_displayPmlFeeIn100Format = false; // OPM 19525 02/04/08 mf 
        private bool m_isBorrowerPaidOriginatorCompAndIncludedInFees = false;
        private bool m_isLenderPaidOriginatorCompAndAdditionToFees = false;
        protected bool m_isStatusLead = false;
        protected bool m_showMonitorRatesLink = false;
        protected bool m_disablePinLinks = false;
        protected bool displayCashToCloseAndReservesCalculation = false;
        protected bool m_hasExpiredRate = false; // 7/29/2009 dd - OPM 22144
        private DateTime m_pageInitTimestamp;
        protected string m_sensitiveData = "";
        protected string m_debugDetail = "";

        public string TabID
        {
            get { return "RESULT"; }
        }
        private string m_cmd
        {
            get
            {
                if (RequestHelper.GetSafeQueryString("cmd") != null && RequestHelper.GetSafeQueryString("cmd") != "")
                {
                    return RequestHelper.GetSafeQueryString("cmd");
                }
                else
                {
                    return Request.Form["__EVENTTARGET"];
                }
            }
        }

        protected string m_confirmationTitle
        {
            // 02/27/08 mf. OPM 20619
            get { return IsPmlSubmissionAllowed ? "Confirmation" : "Summary"; }
        }

        private bool m_unableToSubmitWithoutCredit = false;
        protected bool UnableToSubmitWithoutCredit
        {
            get { return m_unableToSubmitWithoutCredit; }
        }

        protected bool m_isRerunMode = false;
        protected bool m_IsProgramRegistered = false;
        protected bool m_eligibleLoanProgramsExist = false;

        protected bool m_sProdDocT_NoIncome = false;
        protected bool HideQRate { get; set; }
        protected bool IsDisplayReserveMonths = true; // OPM 233679

        private bool? x_hasDuplicate = null;
        protected bool m_HasDuplicate
        {
            get
            {
                if (!x_hasDuplicate.HasValue)
                {
                    var dataLoan = new CPageData(LoanID, new string[] { "sSpAddr" });
                    dataLoan.AllowLoadWhileQP2Sandboxed = true;
                    dataLoan.InitLoad();
                    x_hasDuplicate = DuplicateFinder.FindDuplicateLoansPer105723(dataLoan.sLId, dataLoan.sBrokerId, dataLoan.sSpAddr).Count() > 0;
                }
                return x_hasDuplicate.Value;
            }
        }

        public void LoadData()
        {
            Page.ClientScript.RegisterHiddenField("ActionCmd", "Wait");

            CPageData dataLoan = new CCheckFor2ndLoan(LoanID);
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);
            this.m_displayPmlFeeIn100Format = dataLoan.sPriceGroup.DisplayPmlFeeIn100Format;


            // To properly render this page, we need to know what the doctype of this
            // loan is.  For performance, we piggyback on the payload of this object.

            switch (dataLoan.sProdDocT)
            {
                case E_sProdDocT.NINA:
                case E_sProdDocT.NIVA:
                case E_sProdDocT.NINANE:
                case E_sProdDocT.NIVANE:
                case E_sProdDocT.NISA:
                case E_sProdDocT.DebtServiceCoverage:
                case E_sProdDocT.NoIncome:
                    m_sProdDocT_NoIncome = true;
                    break;
            }

            this.IsDisplayReserveMonths = this.GetIsDisplayReserveMonths(dataLoan);

            bool hideDtiCol = (!CurrentBroker.IsIncomeAssetsRequiredFhaStreamline) //opm 28103 fs 04/08/10
                                && dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance
                                && (!dataLoan.sIsCreditQualifying);

            hideDtiCol |= dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl
                           && (!dataLoan.sIsCreditQualifying);

            m_sProdDocT_NoIncome |= hideDtiCol;

            if (dataLoan.sIsQualifiedForOriginatorCompensation)
            {
                m_isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
                    && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                m_isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
            }

            m_isStatusLead = Tools.IsStatusLead(dataLoan.sStatusT);

            m_IsQP2File = E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT;
           
            if (m_IsQP2File)
            {
                if (PriceMyLoanConstants.IsEmbeddedPML)
                {
                    m_RegisterAndRateLockBlockingMessage = JsMessages.LPE_CannotSubmitInQP2Mode;
                }
                else
                {
                    if (CurrentBroker.IsAllowExternalUserToCreateNewLoan)
                    {
                        m_RegisterAndRateLockBlockingMessage = JsMessages.LPE_CannotSubmitInQP2ModeInPML;
                    }
                    else
                    {
                        m_RegisterAndRateLockBlockingMessage = JsMessages.LPE_CannotSubmitInQP2ModeInPMLAndCantCreateLoan;
                    }
                }
            }

            m_showMonitorRatesLink = BrokerDB.RetrieveById(BrokerID).IsEnableRateMonitorService
                && m_isStatusLead
                && dataLoan.sLienPosT == E_sLienPosT.First
                && (E_sLoanFileT.QuickPricer2Sandboxed != dataLoan.sLoanFileT ||
                    (PriceMyLoanConstants.IsEmbeddedPML && CurrentBroker.AllowRateMonitorForQP2FilesInEmdeddedPml) ||
                    (false == PriceMyLoanConstants.IsEmbeddedPML && CurrentBroker.AllowRateMonitorForQP2FilesInTpoPml));

            // OPM 130137 - Disable "Pin" links for loans with subordinate financing
            m_disablePinLinks = dataLoan.sLtvR != dataLoan.sCltvR || dataLoan.sLienPosT == E_sLienPosT.Second;

            m_unableToSubmitWithoutCredit = !PriceMyLoanUser.HasPermission(Permission.CanSubmitWithoutCreditReport)
                && !dataLoan.sAllAppsHasCreditReportOnFile && !PriceMyLoanRequestHelper.IsEncompassIntegration;
            m_showRateForIneligible = PriceMyLoanUser.HasPermission(Permission.CanApplyForIneligibleLoanPrograms);

            if (dataLoan.sPml1stVisitResultStepD_rep == "")
            {
                if (dataLoan.sEmployeeLenderAccExecId != Guid.Empty)
                {
                    LoanPricingRan ev = new LoanPricingRan();
                    ev.Initialize(dataLoan.sLId, PriceMyLoanUser.DisplayName, dataLoan.sEmployeeLenderAccExecId);
                    ev.Send();
                }

                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.AllowSaveWhileQP2Sandboxed = true;
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitResultStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();
            }

            m_IsProgramRegistered = dataLoan.sLpTemplateId != Guid.Empty;

            //keep track of the last time we ran pml 
            //this is used for opm 115416 
            //av 2013 4/10
            CPageData resultGenerateD = new CFullAccessPageData(LoanID, new string[] { "sPmlLastGenerateResultD" });
            resultGenerateD.AllowSaveWhileQP2Sandboxed = true;
            resultGenerateD.InitSave(ConstAppDavid.SkipVersionCheck);
            resultGenerateD.UpdateLastGeneratedResultD();
            resultGenerateD.Save();

            if ((RequestHelper.GetSafeQueryString("actionCmd") != null && RequestHelper.GetSafeQueryString("actionCmd") == "Result")
                || (m_cmd == "Run2nd" && Request.Form["actionCmd"] == "Result"))
            {
                m_isWaiting = false;


                m_has2ndLoan = dataLoan.sIs8020PricingScenario;
                if (this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                {
                    m_hasLinkedLoan = false; // Always false for rerun mode.
                    m_isSkip2ndLoan = !dataLoan.sLinkedLoanInfo.IsLoanLinked;

                }
                else
                {
                    m_hasLinkedLoan = dataLoan.sLinkedLoanInfo.IsLoanLinked;
                }

                if (m_has2ndLoan)
                    m_resultHeader = "Results for First Loan";

                if (m_cmd == "Run2nd" && m_has2ndLoan)
                {
                    m_currentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan;
                    m_resultHeader = "Results for Second Loan";
                    HideQRate = dataLoan.sSubFinT == E_sSubFinT.Heloc;
                    RenderResult(dataLoan);
                }
                else
                {
                    m_currentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan;
                    RenderResult(dataLoan);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the reserve months column is displayed.
        /// </summary>
        /// <param name="loan">
        /// The loan to display reserve months.
        /// </param>
        /// <returns>
        /// True if the reserve months column is displayed, false otherwise.
        /// </returns>
        private bool GetIsDisplayReserveMonths(CPageData loan)
        {
            var loanPurpose = loan.sLPurposeT;
            if (loanPurpose == E_sLPurposeT.Purchase)
            {
                return true;
            }

            if (loanPurpose == E_sLPurposeT.Construct || loanPurpose == E_sLPurposeT.ConstructPerm)
            {
                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges)
                            && loan.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase)
                {
                    return true;
                }
                else
                {
                    return this.BrokerAllowsDisplayingReserveMonths();
                }
            }

            if (loanPurpose.EqualsOneOf(
                E_sLPurposeT.Refin,
                E_sLPurposeT.RefinCashout, 
                E_sLPurposeT.VaIrrrl, 
                E_sLPurposeT.FhaStreamlinedRefinance, 
                E_sLPurposeT.HomeEquity))
            {
                return this.BrokerAllowsDisplayingReserveMonths();
            }

            throw new UnhandledEnumException(loanPurpose);
        }

        /// <summary>
        /// Gets a value indicating whether the broker allows displaying 
        /// the reserve months in the results.
        /// </summary>
        /// <returns>
        /// True if the broker allows displaying the reserve months in the 
        /// results, false otherwise.
        /// </returns>
        private bool BrokerAllowsDisplayingReserveMonths()
        {
            return this.CurrentBroker.EnableCashToCloseAndReservesDebugInPML &&
                !this.CurrentBroker.DisplayBreakEvenMthsInsteadOfReserveMthsInPML2ForNonPurLoans;
        }

        private void RenderResult(CPageData dataLoan)
        {
            Page.ClientScript.RegisterHiddenField("sIsRenovationLoanSupportEnabled", (dataLoan.BrokerDB.EnableRenovationLoanSupport && dataLoan.sIsRenovationLoan).ToString());
            (Page as LendersOffice.Common.BasePage).RegisterJsScript("qualifyingborrower.js");
            (Page as LendersOffice.Common.BasePage).RegisterJsGlobalVariables("IsUlad2019", dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019);
            List<PricingState> MatchingPricingStates = new List<PricingState>();
            MatchingPricingStates.AddRange(dataLoan.GetLoanMatchingPricingStates());

            m_waitingMessage = "Displaying your results ...";
            long start = DateTime.Now.Ticks;

            List<Guid> requestIds;
            string requestIdString = m_cmd == "Run2nd" ? Request.Form["RequestID"] : RequestHelper.GetSafeQueryString("RequestID");
            Guid id;

            // - Old Format
            if (!Guid.TryParse(requestIdString, out id))
            {
                if (Tools.TryGetGuidFrom(requestIdString, out id))
                {
                    string cacheKey = MainService.GetMergeKeyFrom(PrincipalFactory.CurrentPrincipal.EmployeeId, id);
                    requestIds = SerializationHelper.JsonNetDeserialize<List<Guid>>(AutoExpiredTextCache.GetFromCache(cacheKey));
                }
                else
                {
                    throw new GenericUserErrorMessageException("Cannot load request keys");
                }
            }
            else
            {
                requestIds = new List<Guid>() { id };
            }

            StringBuilder sensitiveData = new StringBuilder();
            StringBuilder debugDetail = new StringBuilder();

            List<UnderwritingResultItem> resultItems = new List<UnderwritingResultItem>();
            long interval0 = DateTime.Now.Ticks;
            foreach (var requestId in requestIds)
            {
                UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(requestId);
         
                if (null == resultItem)
                {
                    // 2/20/2008 dd - One of the probable cause is user click "Refresh" or "Back" from browser. Rerun pricing for them.
                    m_isWaiting = true;
                    return;
                }

                if (resultItem.IsErrorMessage)
                {
                    m_hasError = true;
                    ((PriceMyLoan.UI.BasePage)this.Page).AddInitScriptFunctionWithArgs("f_displayErrorMessage", "'" + Utilities.SafeJsString(resultItem.ErrorMessage) + "'");
                    return;
                }

                if (resultItem.Results == null)
                {
                    throw new CBaseException(PriceMyLoan.UI.Common.PmlErrorMessages.CertificateNullResult, "DistributeUnderwritingEngine.RetrieveResults return null for " + requestId);
                }

                resultItems.Add(resultItem);

                this.m_calcServerCount += resultItem.CalcServerCount;
                this.m_calcServerDuration = Math.Max(resultItem.CalcServerExecutionDurationInMs, m_calcServerDuration);
                sensitiveData.AppendLine(resultItem.DetailTransactionMessage);
                debugDetail.AppendLine(resultItem.DebugDetail);
            }

            this.m_sensitiveData  = EncryptionHelper.Encrypt(sensitiveData.ToString());
            this.m_debugDetail = EncryptionHelper.Encrypt(debugDetail.ToString());
            
            long interval1 = DateTime.Now.Ticks;
            List<CApplicantPriceXml> eligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> ineligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> insufficientList = new List<CApplicantPriceXml>();

            Hashtable m_developerErrorHash = new Hashtable();
            int currentIndex = 0;
            string idposfix = m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "0" : "1";

            m_gDataList = new List<object>();

            foreach (var result in resultItems)
            {
                foreach (object o in result.Results)
                {
                    CApplicantPriceXml product = o as CApplicantPriceXml;

                    if (null == product)
                        continue;

                    switch (product.Status)
                    {
                        case E_EvalStatus.Eval_Eligible:

                            RenderJsData(product, dataLoan);
                            product.TempIndex = currentIndex++;

                            eligibleList.Add(product);
                            break;
                        case E_EvalStatus.Eval_Ineligible:
                            RenderJsData(product, dataLoan);
                            product.TempIndex = currentIndex++;

                            if (product.AreRatesExpired)
                            {
                                m_hasExpiredRate = true;
                            }

                            ineligibleList.Add(product);
                            break;
                        case E_EvalStatus.Eval_InsufficientInfo:
                            insufficientList.Add(product);
                            if (product.Errors.IndexOf(ErrorMessages.LP_DisabledBySAEMessage) < 0)
                            {
                                // 6/21/2007 dd - OPM 10242 - If the LP is disable by SAE then do not send insufficient email to developer.
                                foreach (string errMsg in product.DeveloperErrors)
                                {
                                    if (!m_developerErrorHash.Contains(errMsg))
                                    {
                                        m_developerErrorHash.Add(errMsg, null);
                                    }
                                }
                            }
                            break;
                        case E_EvalStatus.Eval_HideFromResult:
                            // Ignore. OPM 31884
                            break;
                        default:
                            throw new UnhandledEnumException(product.Status);
                    }
                }
            }

            m_eligibleLoanProgramsExist = eligibleList.Count > 0;
            insufficientList.Sort(new CApplicantPriceXmlNameComparer());
            long interval2 = DateTime.Now.Ticks;

            #region Display Best Prices Per Program View

            //PML 2.0 Best Price is enabled by default. Check to see if they have always use best price then check user pemrission. 
            bool includeAllProgramsInResults = !CurrentBroker.IsAlwaysUseBestPrice || PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowBypassAlwaysUseBestPrice);

            // 7/1/2009 dd - OPM 24899 - Pass in option whether to filter out higher rate with worst price.
            RateMergeBucket rateMergeBucket = new RateMergeBucket(CurrentBroker.IsShowRateOptionsWorseThanLowerRate, includeAllProgramsInResults);

            for (int i = 0; i < eligibleList.Count; i++)
            {
                rateMergeBucket.Add(eligibleList[i]);
            }
            m_gDataRateOptionList = new List<object>();

            E_OriginatorCompPointSetting pointSetting;

            // OPM 64253.  If lender has a special option turned on, we want to 
            // display the point value as adjusted for originator comp.
            string pointRep;
            if (CurrentBroker.IsUsePriceIncludingCompensationInPricingResult && m_isLenderPaidOriginatorCompAndAdditionToFees)
            {
                // OPM 64253
                pointSetting = E_OriginatorCompPointSetting.UsePointWithOriginatorComp;
            }
            else
            {
                pointSetting = E_OriginatorCompPointSetting.UsePoint;
            }

            Tuple<RateMergeGroupKey, Decimal> overrideParRate = ApplyLoanWorkerUnit.GetParRateOverride(resultItems, CurrentBroker, includeAllProgramsInResults, pointSetting);

            foreach (RateMergeGroup mergeGroup in rateMergeBucket.RateMergeGroups)
            {
                if (mergeGroup.RateOptions.Count == 0)
                    continue;

                decimal parRate = ApplyLoanWorkerUnit.GetParRate(overrideParRate, mergeGroup, pointSetting);

                string groupMergeName = mergeGroup.Name;
                int representativeRateIndex = mergeGroup.RepresentativeRateIndex;

                List<object> groupMergeItemList = new List<object>();
                m_gDataRateOptionList.Add(groupMergeItemList);

                groupMergeItemList.Add(groupMergeName);
                groupMergeItemList.Add(representativeRateIndex);

                List<object> rateOptionList = new List<object>();
                groupMergeItemList.Add(rateOptionList);

                foreach (CApplicantRateOption rateOption in mergeGroup.RateOptions)
                {
                    if (rateOption.IsRateExpired)
                    {
                        m_hasExpiredRate = true;
                    }

                    // 11/12/10 mf. OPM 32422. Show warning only if there is a violation.
                    if (m_showMaxDtiWarning == false && rateOption.IsViolateMaxDti)
                    {
                        m_showMaxDtiWarning = true;
                    }

                    switch (pointSetting)
                    {
                        case E_OriginatorCompPointSetting.UsePoint:
                            pointRep = rateOption.Point_rep;
                            break;
                        case E_OriginatorCompPointSetting.UsePointLessOriginatorComp:
                            pointRep = rateOption.PointLessOriginatorComp_rep;
                            break;
                        case E_OriginatorCompPointSetting.UsePointWithOriginatorComp:
                            pointRep = rateOption.PointIncludingOriginatorComp_rep;
                            break;
                        default:
                            throw new UnhandledEnumException(pointSetting);
                    }

                    /* If you want to change how the closing cost breakdown is rendered please also see look for the same
                     * comment(s) in GetResultsNewUI.ascx, GetResultsNewUI.ascx.cs, LoanProductResultControlNewUI.cs, LoanComparisonXml.cs.
                     */
                    // This will sort the breakdown for eligible loan programs.
                    // Constructing the link is done in the JS for eligible loan programs.
                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        // Sort by Loan Section then by Hudline
                        rateOption.ClosingCostBreakdown.Sort(delegate(RespaFeeItem a, RespaFeeItem b)
                        {
                            int loanSecCompareVal = a.DisclosureSectionT.CompareTo(b.DisclosureSectionT);
                            if (loanSecCompareVal == 0)
                            {
                                return int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine));
                            }
                            else
                            {
                                return loanSecCompareVal;
                            }
                        });
                    }
                    else
                    {
                        rateOption.ClosingCostBreakdown.Sort((a, b) => int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine))); // sort closing cost breakdown by hudline
                    }

                    string qmStatus = "";
                    string hpmMode = "";
                    EmployeeDB empDB = EmployeeDB.RetrieveById(CurrentBroker.BrokerID, PriceMyLoanUser.EmployeeId);
                    if (rateOption.QMData != null && (dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent || CurrentBroker.DisplayQMResultsForCorrespondentFiles) && (CurrentBroker.ShowQMStatusInPml2 || empDB.ShowQMStatusInPml2))
                    {
                        m_showQm = true;
                        QMStatusCalculator calculator = new QMStatusCalculator(parRate, rateOption.Apr_rep, rateOption.QMData, m_losConvert, rateOption.Rate);
                        qmStatus = calculator.GetQMStatus().ToString("d");
                        hpmMode = calculator.GetHPMLStatus().ToString("d");
                    }

                    List<object> rateOptionItem = new List<object>()
                        {
                            rateOption.Rate_rep // 0
                            , pointRep // 1
                            , rateOption.Apr_rep // 2
                            , rateOption.Margin_rep // 3
                            , rateOption.FirstPmtAmt_rep // 4
                            , "" // 12/1/2009 dd - We are no longer display top ratio in result. rateOption.TopRatio_rep // 5
                            , rateOption.BottomRatio_rep == "-1.000" || rateOption.BottomRatio_rep == "" ? "N/A" : rateOption.BottomRatio_rep // 6
                            , rateOption.RateOptionId // 7
                            , rateOption.TeaserRate_rep // 8
                            , rateOption.Reserves.Replace("months", "").TrimWhitespaceAndBOM() // 9
                            , rateOption.QualPmt_rep // 10
                            , CApplicantRateOption.PointFeeInResult(dataLoan.sPriceGroup.DisplayPmlFeeIn100Format, pointRep) // 11
                            , rateOption.ApplicantPriceTempIndex // 12
                            , !m_sProdDocT_NoIncome && rateOption.IsViolateMaxDti ? 1 : 0 // 13
                            , rateOption.BankAmount_rep // 14
                            , rateOption.IsDisqualified ? 1 : 0 //15,
                            , rateOption.DisqualReason //16
                            , rateOption.BreakEvenPoint.Replace("months", "").TrimWhitespaceAndBOM() //17
                            , rateOption.ClosingCost //18
                            , HasMatchingPricingState(MatchingPricingStates, rateOption.LpTemplateId, rateOption.Rate_rep) // 19
                            , ObsoleteSerializationHelper.JavascriptJsonSerialize(rateOption.ClosingCostBreakdown.Select(item => new {
                                HudLine = item.HudLine,
                                Description = item.Description,
                                Fee = m_losConvert.ToMoneyString(item.Fee, FormatDirection.ToRep),
                                Source = item.DescriptionSource,
                                LoanSec = Tools.GetIntegratedDisclosureString(item.DisclosureSectionT)})) // 20
                            , rateOption.PrepaidCharges // 21
                            , rateOption.NonPrepaidCharges // 22
                            , rateOption.IsRateMergeWinner //23
                            , rateOption.NumberOfRateMergeCompetitors //24
                            , rateOption.QRate_rep //25
                            , qmStatus  //26
                            , parRate   //27
                            , hpmMode   //28
                            , dataLoan.sDisclosureRegulationT.ToString("D") //29
                            , ObsoleteSerializationHelper.JavascriptJsonSerialize(rateOption.LoanData) // 30
                        };
                    rateOptionList.Add(rateOptionItem);

                }
            }
            #endregion



            if (ineligibleList.Count != 0)
            {
                if (this.CurrentBroker.IsPricingByRateMergeGroupEnabled)
                {
                    // OPM 329348. Merged ineligibles might be out-of-order.
                    ineligibleList.Sort((p, q) => p.lLpTemplateNm.CompareTo(q.lLpTemplateNm));
                }

                if (m_showRateForIneligible)
                {
                    for (int i = 0; i < ineligibleList.Count; i++)
                    {
                        /* If you want to change how the closing cost breakdown is rendered please also see look for the same
                         * comment(s) in GetResultsNewUI.ascx, GetResultsNewUI.ascx.cs, LoanProductResultControlNewUI.cs, LoanComparisonXml.cs.
                         */
                        // Closing cost breakdown for the first(View More checkbox unchecked) ineligible loan program handled in LoanProductResultControlNewUI.
                        LoanProductResultControlNewUI c = new LoanProductResultControlNewUI();
                        c.ID = "_i" + idposfix + i;
                        c.IsDisplayReserveMonths = IsDisplayReserveMonths;
                        c.ApplicantPrice = ineligibleList[i];
                        c.IsLoanRegistrationDisabled = !IsAllowRegisterRateLock;
                        c.Has2ndLoan = m_has2ndLoan;
                        c.CurrentLienQualifyModeT = m_currentLienQualifyModeT;
                        c.CssClassName = i % 2 == 1 ? "IneAltBg" : "IneBg";
                        c.IsNoIncome = m_sProdDocT_NoIncome;
                        c.BrokerDB = this.CurrentBroker;
                        c.CanSubmitForLock = IsAllowRequestingRateLock;
                        c.CurrentLpeRunModeT = lpeRunModeT;
                        c.IsProgramRegistered = m_IsProgramRegistered;
                        c.IsBorrowerPaidOriginatorCompAndIncludedInFees = m_isBorrowerPaidOriginatorCompAndIncludedInFees;
                        c.IsLenderPaidOriginatorCompAndAdditionToFees = m_isLenderPaidOriginatorCompAndAdditionToFees;
                        c.DisplayPmlFeeIn100Format = dataLoan.sPriceGroup.DisplayPmlFeeIn100Format;
                        c.UserCanApplyForIneligibleLoanPrograms = PriceMyLoanUser.HasPermission(Permission.CanApplyForIneligibleLoanPrograms);
                        c.sDisclosureRegulationT = dataLoan.sDisclosureRegulationT;
                        m_ineligibleRatePlaceHolder.Controls.Add(c);

                    }
                }
                else
                {
                    m_ineligibleRepeater.DataSource = ineligibleList;
                    m_ineligibleRepeater.DataBind();
                }
            }

            if (insufficientList.Count != 0)
            {
                m_insufficientRepeater.DataSource = insufficientList;
                m_insufficientRepeater.DataBind();
            }

            m_ineligibleNotFoundLiteral.Visible = ineligibleList.Count == 0;
            m_ineligibleRateNotFoundLiteral.Visible = ineligibleList.Count == 0;
            m_insufficientNotFoundLiteral.Visible = insufficientList.Count == 0;

            long interval3 = DateTime.Now.Ticks;

            SaveToTemporaryFile();

            long endTicks = DateTime.Now.Ticks;
            m_renderResultDuration = (endTicks - start) / 10000L;
            m_renderResultDurationDetail = string.Format("0-1:{0}ms, 1-2:{1}ms, 2-3:{2}ms, 3-4:{3}ms, 4-5:{4}ms", (interval0 - start) / 10000L, (interval1 - interval0) / 10000L, (interval2 - interval1) / 100000L, (interval3 - interval2) / 100000L, (endTicks - interval3) / 10000L);

            var firstResultItem = resultItems.FirstOrDefault();
            var firstResultItemCreatedDate = firstResultItem != null ? firstResultItem.BatchCreatedDate.ToLongTimeString() : "N/A (No Results Available)";

            m_batchCreatedTime = firstResultItemCreatedDate + " - (PageInit):" + m_pageInitTimestamp.ToLongTimeString() + " - " + Tools.GetDBCurrentDateTime().ToLongTimeString();
           
            string debugMsg = LOCache.Get("DEBUG_" + id) as string;
            if (null != debugMsg)
            {
                m_batchCreatedTime += debugMsg;
            }

            object startTicks = LOCache.Get("DEBUG_" + id + "_START_TICKS");
            if (startTicks != null)
            {
                int durationMs = (int)(DateTime.Now.Ticks - (long)startTicks) / 10000;
                DistributeUnderwritingEngine.RecordPricingTotalTime(id, "RunPML", durationMs);

            }
        }

        private string HasMatchingPricingState(List<PricingState> list, Guid lLpTemplateId, string rate)
        {
            try
            {
                return list.Find(p => p.lLpTemplateId == lLpTemplateId && p.SelectedNoteRate.Equals(rate)).Id.ToString();
            }
            catch (NullReferenceException) { }

            return "";
        }

        /// <summary>
        /// Return two links title to used in best price pricing mode. The first link will be use for normal rate. The second link will be use when rate is expire.
        /// If RSE feature is not turn on the first link and second will be the same.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetLinkTitles()
        {
            List<string> oldLinks = ResultActionLink.GetLinkTitles_NoWorkflow(m_has2ndLoan, m_currentLienQualifyModeT, IsPmlSubmissionAllowed,
                IsEncompassIntegration, lpeRunModeT, PriceMyLoanUser.IsRateLockedAtSubmission);

            List<string> newLinks = ResultActionLink.GetLinkTitles(m_has2ndLoan, m_currentLienQualifyModeT, IsPmlSubmissionAllowed,
                IsEncompassIntegration, lpeRunModeT, PriceMyLoanUser.IsRateLockedAtSubmission, IsAllowRequestingRateLock, m_IsProgramRegistered, IsAllowRegisterRateLock);

            // Always use old until we get no mismatches
            return UseOldSecurityVersion ? oldLinks : newLinks;
        }

        protected IEnumerable<string> ReasonsCannotRegister
        {
            get
            {
                if (IsAllowRegisterRateLock)
                {
                    return new List<string>();
                }

                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetReasonsUserCannotPerformOperation(WorkflowOperations.RegisterLoan);
            }
        }

        protected IEnumerable<string> ReasonsCannotLock
        {
            get
            {
                if (IsAllowRequestingRateLock)
                    return new List<string>(); // User is not blocked--no reasons
                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetReasonsUserCannotPerformOperation(WorkflowOperations.RequestRateLockRequiresManualLock, WorkflowOperations.RequestRateLockCausesLock);
            }
        }

        protected ConditionType CannotRegisterFailedConditionType
        {
            get
            {
                if (IsAllowRegisterRateLock)
                {
                    return ConditionType.Invalid;
                }

                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetOperationFailedConditionType(WorkflowOperations.RegisterLoan);
            }
        }

        protected ConditionType CannotLockFailedConditionType
        {
            get
            {
                if (IsAllowRequestingRateLock)
                {
                    return ConditionType.Invalid; // User is not blocked--no reasons
                }

                var page = (PriceMyLoan.UI.BasePage)Page;
                return page.GetOperationFailedConditionType(WorkflowOperations.RequestRateLockRequiresManualLock, WorkflowOperations.RequestRateLockCausesLock);
            }
        }
        
        protected List<string> m_versionList = new List<string>(2);
        private int GetVersionIndex(string version)
        {
            for (int index = 0; index < m_versionList.Count; index++)
            {
                if (version == m_versionList[index])
                    return index;
            }
            m_versionList.Add(version);
            return m_versionList.Count - 1;
        }
        private void RenderJsData(CApplicantPriceXml applicantPrice, CPageData dataLoan)
        {
            List<object> list = new List<object>() {
                applicantPrice.lLpTemplateId // 0
                , applicantPrice.lLpTemplateNm // 1
                , GetVersionIndex(applicantPrice.Version) //2
                , applicantPrice.MaxDti_rep == "" ? "N/A" : applicantPrice.MaxDti_rep.Replace("%", "") //3
                , applicantPrice.IsBlockedRateLockSubmission ? 1 : 0 //4
                , applicantPrice.RateLockSubmissionUserWarningMessage //5
                , applicantPrice.AreRatesExpired ? 1 : 0 //6
                , applicantPrice.UniqueChecksum // 7
            };
            m_gDataLpList.Add(list);

            bool isIneligibleAndNoSubmit = applicantPrice.Status == E_EvalStatus.Eval_Ineligible && !m_showRateForIneligible;

            List<object> applicantPriceData = new List<object>();

            applicantPriceData.Add(new List<object>()
            {
                applicantPrice.lLpTemplateId // 0
                , isIneligibleAndNoSubmit ? "" : applicantPrice.lLpTemplateNm // 1
                , GetVersionIndex(applicantPrice.Version) // 2
                , applicantPrice.IsBlockedRateLockSubmission ? 1 : 0 // 3
                , applicantPrice.RateLockSubmissionUserWarningMessage // 4
                , applicantPrice.AreRatesExpired ? 1 : 0 // 5
                , applicantPrice.UniqueChecksum // 6

            });

            List<object> rateOptionList = new List<object>();
            applicantPriceData.Add(rateOptionList);

            m_gDataList.Add(applicantPriceData);

            if (!isIneligibleAndNoSubmit)
            {
                // 6/1/2007 dd - Only render rate option if program is Eligible or (Ineglible and ShowRate)
                CApplicantRateOption[] rateOptions = applicantPrice.ApplicantRateOptions;
                // 5/16/2007 dd - TODO Only need to render when rateOption.Length > 2.
                for (int rateOptionIndex = 0; rateOptionIndex < rateOptions.Length; rateOptionIndex++)
                {
                    CApplicantRateOption rateOption = rateOptions[rateOptionIndex];
                    rateOption.SetApplicantPrice(applicantPrice);

                    // 11/12/10 mf. OPM 32422. Show warning only if there is a violation.
                    if (m_showMaxDtiWarning == false && rateOption.IsViolateMaxDti)
                    {
                        m_showMaxDtiWarning = true;
                    }

                    // OPM 64253.  If lender has a special option turned on, we want to 
                    // display the point value as adjusted for originator comp.
                    // KEEP THIS LOGIC IN SYNC WITH GETQMPArRate in the RageMergeGroup. 
                    // and 
                    string pointRep;
                    if (CurrentBroker.IsUsePriceIncludingCompensationInPricingResult && m_isLenderPaidOriginatorCompAndAdditionToFees)
                    {
                        // OPM 64253
                        pointRep = rateOption.PointIncludingOriginatorComp_rep;
                    }
                    else
                    {
                        pointRep = rateOption.Point_rep;
                    }

                    /* If you want to change how the closing cost breakdown is rendered please also see look for the same
                     * comment(s) in GetResultsNewUI.ascx, GetResultsNewUI.ascx.cs, LoanProductResultControlNewUI.cs, LoanComparisonXml.cs.
                     */
                    // This seems to be used for the ineligible loan programs displayed when the View More checkbox is checked. 
                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        // Sort by Loan Section then by Hudline
                        rateOption.ClosingCostBreakdown.Sort(delegate(RespaFeeItem a, RespaFeeItem b)
                        {
                            int loanSecCompareVal = a.DisclosureSectionT.CompareTo(b.DisclosureSectionT);
                            if (loanSecCompareVal == 0)
                            {
                                return int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine));
                            }
                            else
                            {
                                return loanSecCompareVal;
                            }
                        });
                    }
                    else
                    {
                        rateOption.ClosingCostBreakdown.Sort((a, b) => int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine))); // sort closing cost breakdown by hudline
                    }

                    var closingCostsBreakdown = ObsoleteSerializationHelper.JavascriptJsonSerialize(rateOption.ClosingCostBreakdown.Select(item => new
                    {
                        HudLine = item.HudLine,
                        Description = item.Description,
                        Fee = m_losConvert.ToMoneyString(item.Fee, FormatDirection.ToRep),
                        Source = item.DescriptionSource,
                        LoanSec = Tools.GetIntegratedDisclosureString(item.DisclosureSectionT)
                    }));

                    var closingCostsLink = "<a href='#' class='displayCostsLink' " +
                                           "closingCostBreakdown='" + closingCostsBreakdown + "'" +
                                           "ClosingCost='" + rateOption.ClosingCost + "'" +
                                           "PrepaidCharges='" + rateOption.PrepaidCharges + "'" +
                                           "NonPrepaidCharges='" + rateOption.NonPrepaidCharges + "'" +
                                           "LoanProgram='" + rateOption.LpTemplateNm + "'" +
                                           "NoteRate='" + rateOption.Rate_rep + "'" +
                                           "TRID='" + dataLoan.sDisclosureRegulationT.ToString("D") + "'" +
                                           ">" + rateOption.ClosingCost + "</a>";

                    string cashToCloseLink = string.Empty;
                    string reservesLink = string.Empty;

                    if (CurrentBroker.EnableCashToCloseAndReservesDebugInPML)
                    {
                        var loanData = rateOption.LoanData;

                        // sONewFinCcVal is not actually sONewFinCcVal.  It's actually sONewFinCcAsOCreditAmt_rep.  So we need to reverse it here
                        cashToCloseLink = "<a href='#' class='displayCashToCloseLink' " +
                            "LoanProgram='" + rateOption.LpTemplateNm + "' " +
                            "NoteRate='" + rateOption.Rate_rep + "' " +
                            "sPurchPrice='" + loanData.sPurchPrice + "' " +
                            "sAltCost='" + loanData.sAltCost + "' " +
                            "sLandCost='" + loanData.sLandCost + "' " +
                            "sRefPdOffAmt1003='" + loanData.sRefPdOffAmt1003 + "' " +
                            "sTotEstPp1003='" + loanData.sTotEstPp1003 + "' " +
                            "sTotEstCcNoDiscnt1003='" + loanData.sTotEstCcNoDiscnt1003 + "' " +
                            "sFfUfmip1003='" + loanData.sFfUfmip1003 + "' " +
                            "sLDiscnt1003='" + loanData.sLDiscnt1003 + "' " +
                            "sTotTransC='" + loanData.sTotTransC + "' " +
                            "sONewFinBal='" + loanData.sONewFinBal + "' " +
                            "sTotCcPbs='" + loanData.sTotCcPbs + "' " +
                            "sOCredit1Desc='" + loanData.sOCredit1Desc + "' " +
                            "sOCredit1Amt='" + loanData.sOCredit1Amt + "' " +
                            "sOCredit2Desc='" + loanData.sOCredit2Desc + "' " +
                            "sOCredit2Amt='" + loanData.sOCredit2Amt + "' " +
                            "sOCredit3Desc='" + loanData.sOCredit3Desc + "' " +
                            "sOCredit3Amt='" + loanData.sOCredit3Amt + "' " +
                            "sOCredit4Desc='" + loanData.sOCredit4Desc + "' " +
                            "sOCredit4Amt='" + loanData.sOCredit4Amt + "' " +
                            "sOCredit5Amt='" + loanData.sOCredit5Amt + "' " +
                            "sONewFinCc='" + loanData.sONewFinCc + "' " +
                            "sONewFinCcInverted='" + loanData.sONewFinCcInverted + "' " +
                            "sTotLiquidAssets='" + loanData.sTotLiquidAssets + "' " +
                            "sLAmtCalc='" + loanData.sLAmtCalc + "' " +
                            "sFfUfmipFinanced='" + loanData.sFfUfmipFinanced + "' " +
                            "sFinalLAmt='" + loanData.sFinalLAmt + "' " +
                            "sTransNetCash='" + loanData.sTransNetCash + "' " +
                            "sIsIncludeProrationsIn1003Details='" + loanData.sIsIncludeProrationsIn1003Details + "' " +
                            "sIsIncludeProrationsInTotPp='" + loanData.sIsIncludeProrationsInTotPp + "' " +
                            "sTotEstPp='" + loanData.sTotEstPp + "' " +
                            "sTotalBorrowerPaidProrations='" + loanData.sTotalBorrowerPaidProrations + "' " +
                            "sIsIncludeONewFinCcInTotEstCc='" + loanData.sIsIncludeONewFinCcInTotEstCc + "' " +
                            "sTotEstCcNoDiscnt='" + loanData.sTotEstCcNoDiscnt + "' " +
                            "sAltCostLckd='" + loanData.sAltCostLckd + "' " +
                            "sLandIfAcquiredSeparately1003='" + loanData.sLandIfAcquiredSeparately1003 + "' " +
                            "sLDiscnt1003Lckd='" + loanData.sLDiscnt1003Lckd + "' " +
                            "sPurchasePrice1003='" + loanData.sPurchasePrice1003 + "' " +
                            "sRefNotTransMortgageBalancePayoffAmt='" + loanData.sRefNotTransMortgageBalancePayoffAmt + "' " +
                            "sRefTransMortgageBalancePayoffAmt='" + loanData.sRefTransMortgageBalancePayoffAmt + "' " +
                            "sSellerCreditsUlad='" + loanData.sSellerCreditsUlad + "' " +
                            "sTotCcPbsLocked='" + loanData.sTotCcPbsLocked + "' " +
                            "sTotCreditsUlad='" + loanData.sTotCreditsUlad + "' " +
                            "sTotEstBorrCostUlad='" + loanData.sTotEstBorrCostUlad + "' " +
                            "sTotEstCc1003Lckd='" + loanData.sTotEstCc1003Lckd + "' " +
                            "sTotEstPp1003Lckd='" + loanData.sTotEstPp1003Lckd + "' " +
                            "sTotMortLAmtUlad='" + loanData.sTotMortLAmtUlad + "' " +
                            "sTotMortLTotCreditUlad='" + loanData.sTotMortLTotCreditUlad + "' " +
                            "sTotOtherCreditsUlad='" + loanData.sTotOtherCreditsUlad + "' " +
                            "sTotTransCUlad='" + loanData.sTotTransCUlad + "' " +
                            "sTransNetCashUlad='" + loanData.sTransNetCashUlad + "' " +
                            "sTRIDSellerCredits='" + loanData.sTRIDSellerCredits + "' " +
                            ">" + loanData.sTransNetCash + "</a>";

                        if (IsDisplayReserveMonths)
                        {
                            reservesLink = "<a href='#' class='displayReservesLink' " +
                                "LoanProgram='" + rateOption.LpTemplateNm + "' " +
                                "NoteRate='" + rateOption.Rate_rep + "' " +
                                "Reserves='" + rateOption.Reserves.Replace("months", "") + "' " +
                                "sTotLiquidAssets='" + loanData.sTotLiquidAssets + "' " +
                                "sTransNetCashNonNegative='" + loanData.sTransNetCashNonNegative + "' " +
                                "sMonthlyPmt='" + loanData.sMonthlyPmt + "' " +
                                ">" + rateOption.Reserves.Replace("months", "") + "</a>";
                        }
                    }
                    // This is way out of sync with the other rate option listing for non-merge eligible.
                    rateOptionList.Add(new List<object>()
                    {
                        rateOption.Rate_rep //0
                        , pointRep // 1
                        , rateOption.Apr_rep // 2
                        , applicantPrice.lIsArmMarginDisplayed ? rateOption.Margin_rep : "" // 3
                        , rateOption.FirstPmtAmt_rep // 4
                        , "" // 12/1/2009 dd - We are no longer display top ratio in result. rateOption.TopRatio_rep // 5
                        , rateOption.BottomRatio_rep == "-1.000" || rateOption.BottomRatio_rep == "" ? "N/A" : rateOption.BottomRatio_rep // 6
                        , rateOption.RateOptionId // 7
                        , rateOption.TeaserRate_rep // 8
                        , rateOption.Reserves.Replace("months", "").TrimWhitespaceAndBOM() // 9
                        , rateOption.ClosingCost // 10
                        , CApplicantRateOption.PointFeeInResult(dataLoan.sPriceGroup.DisplayPmlFeeIn100Format, pointRep) // 11
                        , !m_sProdDocT_NoIncome && rateOption.IsViolateMaxDti ? 1 : 0 // 12
                        , rateOption.BankAmount_rep // 13
                        , rateOption.IsDisqualified ? 1 : 0 // 14
                        , rateOption.DisqualReason // 15
                        , rateOption.BreakEvenPoint.Replace("months", "").TrimWhitespaceAndBOM() // 16
                        , closingCostsLink // 17
                        , cashToCloseLink // 18
                        , reservesLink //19

                    });
                }
            }
        }
        public void SaveData()
        {
        }

        public void SetAdditionalQueryStringValues(PmlMainQueryStringManager qsManager)
        {
            // 02/15/08 mf. OPM 20137.  These form values need to go the query string.

            if (Request.Form["ActionCmd"] != null)
            {
                qsManager.AddPair("actionCmd", Request.Form["ActionCmd"]);
            }
            if (Request.Form["RequestID"] != null)
            {
                qsManager.AddPair("requestId", Request.Form["RequestID"]);
            }
            if (Request.Form["JsScriptStats"] != null)
            {
                qsManager.AddPair("jsScriptStats", Request.Form["JsScriptStats"]);
            }
        }


        private bool? x_allowRegister = new bool?();
        private bool IsAllowRegisterRateLock
        {
            get
            {
                if (x_allowRegister.HasValue == false)
                {
                    PriceMyLoan.UI.BasePage parentPage = this.Page as PriceMyLoan.UI.BasePage;
                    if (null == parentPage)
                    {
                        // Safety throw. If getresults control ends up on page that does not
                        // inherit from basepage, we should work in the security checks there.
                        throw new CBaseException(ErrorMessages.Generic, "GetResults is child control on page that does not inherit from Base.");
                    }
                    else
                    {
                        x_allowRegister = parentPage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ||
                            parentPage.CurrentUserCanPerform(WorkflowOperations.RegisterLoan);
                    }
                }

                return x_allowRegister.Value;
            }
        }

        protected bool m_IsQP2File = false;
        protected string m_RegisterAndRateLockBlockingMessage = String.Empty;

        private bool? x_AllowRequestRateLock = null;
        private bool IsAllowRequestingRateLock
        {
            get
            {
                if (x_AllowRequestRateLock.HasValue == false)
                {
                    PriceMyLoan.UI.BasePage parentPage = this.Page as PriceMyLoan.UI.BasePage;
                    if (null == parentPage)
                    {
                        // Safety throw. If getresults control ends up on page that does not
                        // inherit from basepage, we should work in the security checks there.
                        throw new CBaseException(ErrorMessages.Generic, "GetResults is child control on page that does not inherit from Base.");
                    }
                    else
                    {
                        x_AllowRequestRateLock = parentPage.CurrentUserCanPerform(WorkflowOperations.RequestRateLockRequiresManualLock)
                            || parentPage.CurrentUserCanPerform(WorkflowOperations.RequestRateLockCausesLock);
                    }
                }

                return x_AllowRequestRateLock.Value;
            }
        }

        private bool? x_UseOldSecurityVersion = null;
        private bool UseOldSecurityVersion
        {
            get
            {
                if (x_UseOldSecurityVersion.HasValue == false)
                {
                    PriceMyLoan.UI.BasePage parentPage = this.Page as PriceMyLoan.UI.BasePage;
                    if (null == parentPage)
                    {
                        // Safety throw. If getresults control ends up on page that does not
                        // inherit from basepage, we should work in the security checks there.
                        throw new CBaseException(ErrorMessages.Generic, "GetResults is child control on page that does not inherit from Base.");
                    }
                    else
                    {
                        x_UseOldSecurityVersion = parentPage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml);
                    }

                }

                return x_UseOldSecurityVersion.Value;
            }
        }

        protected bool IsPmlSubmissionAllowed
        {
            get
            {
                // 7/17/2009 dd - We do not allow user to submit the loan if it is running inside Encompass EPass
                return this.CurrentBroker.IsPmlSubmissionAllowed && !IsEncompassIntegration;
            }
        }
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected HtmlGenericControl CreateDisqualifiedRules(object o)
        {
            HtmlGenericControl span = new HtmlGenericControl("span");

            List<string> list = (List<string>)o;
            foreach (string s in list)
            {

                HtmlGenericControl div = new HtmlGenericControl("div");
                div.InnerText = s;
                span.Controls.Add(div);
            }
            return span;
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {

            if (RequestHelper.GetSafeQueryString("jsScriptStats") != null)
                Page.ClientScript.RegisterHiddenField("JsScriptStats", RequestHelper.GetSafeQueryString("jsScriptStats"));
            else
                Page.ClientScript.RegisterHiddenField("JsScriptStats", Request.Form["JsScriptStats"]);


            Page.ClientScript.RegisterHiddenField("RequestID", "");
            if (!Page.IsPostBack)
            {
                Page.ClientScript.RegisterHiddenField("ActionCmd", "Wait");
            }
            m_isRerunMode = this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct;

            if (Page.IsPostBack && (RequestHelper.GetSafeQueryString("cmd") == "Run2nd"))
                LoadData();

        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            m_pageInitTimestamp = Tools.GetDBCurrentDateTime();
            ((LendersOffice.Common.BasePage)this.Page).RegisterJsScript("getresult.js");
            ((LendersOffice.Common.BasePage)this.Page).RegisterJsScript("jquery.tmpl.js");

            BrokerDB broker = this.CurrentBroker;

            m_isLpeManualSubmissionAllowed = broker.IsLpeManualSubmissionAllowed && !IsEncompassIntegration;
            m_isLpeDisqualificationEnabled = broker.IsLpeDisqualificationEnabled;
            m_isOptionARMUsedInPml = broker.IsOptionARMUsedInPml;
            displayCashToCloseAndReservesCalculation = broker.EnableCashToCloseAndReservesDebugInPML;
        }

        private void SaveToTemporaryFile()
        {
            string name = string.Format("{0}_{1}", m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "LPEFirstResult" : "LPESecondResult",
                this.LoanID.ToString("N"));

            string tempFile = TempFileUtils.NewTempFilePath();

            using (StreamWriter writer = new StreamWriter(tempFile, false))
            {
                MainResultPanel.RenderControl(new HtmlTextWriter(writer));
            }

            FileDBTools.WriteFile(E_FileDB.Temp, name, tempFile);

            try
            {
                FileOperationHelper.Delete(tempFile);
            }
            catch (IOException)
            {
                // NO-OP
            }
        }

        private void SetLiteralFrom(RepeaterItemEventArgs arg, XElement element, string fieldId, string attrId)
        {
            ITextControl field = arg.Item.FindControl(fieldId) as ITextControl;
            field.Text = SafeAttribute(element, attrId);
        }

        private string SafeAttribute(XElement element, string attrId)
        {
            XAttribute attr = element.Attribute(attrId);
            if (attr != null)
            {
                return attr.Value;
            }
            else
            {
                return "-none-";
            }
        }

        protected void TitleRepeater_DataBound(Object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Footer || args.Item.ItemType == ListItemType.Header)
            {
                return;
            }

            XElement element = args.Item.DataItem as XElement;

            SetLiteralFrom(args, element, "Key", "Key");
            SetLiteralFrom(args, element, "CostType", "CostType");
            SetLiteralFrom(args, element, "CostSubType", "CostSubType");
            SetLiteralFrom(args, element, "Name", "Name");
            SetLiteralFrom(args, element, "GFEName", "GFEName");
            SetLiteralFrom(args, element, "Amount", "Amount");
            SetLiteralFrom(args, element, "AmountBuyer", "Amount_buyer");
            SetLiteralFrom(args, element, "AmountSeller", "Amount_seller");

            SetLiteralFrom(args, element, "GFEAmount", "GFEAmount");
            SetLiteralFrom(args, element, "GFEAmountBuyer", "GFEamount_buyer");
            SetLiteralFrom(args, element, "GFEAmountSeller", "GFEamount_seller");

            SetLiteralFrom(args, element, "PolicyEndoRel", "PolicyEndoRel");
            SetLiteralFrom(args, element, "MappedTo", "MappedTo");
            SetLiteralFrom(args, element, "MappedValue", "MappedValue");

            HtmlAnchor anchor = args.Item.FindControl("keylink") as HtmlAnchor;
            string name = SafeAttribute(element, "Name");

            if (name.Length == 0)
            {
                name = "No Named Received from First American";
            }
            anchor.Attributes.Add("title", name);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

    }
}