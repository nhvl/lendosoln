using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using DataAccess;
using PriceMyLoan.DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.Common;
using LendersOffice.AntiXss;
using LendersOffice.Constants;

namespace PriceMyLoan.main
{
	public partial class EditPublicRecord : PriceMyLoan.UI.BasePage
	{
        private Guid m_currentAppId = Guid.Empty;

		// 2/28/07 mf. Per OPM 9023 we now allow users to edit some values
		// of public record entries.

		protected void PageLoad(object sender, System.EventArgs e)
		{

			Tools.Bind_CreditPublicRecordDispositionType(DispositionT);
			Tools.Bind_CreditPublicRecordType(Type);

            string sAppId = null;
            try
            {
                sAppId = RequestHelper.GetSafeQueryString("appid");
                m_currentAppId = new Guid(sAppId);
            }
            catch (Exception exc)
            {
                if (sAppId == null)
                    sAppId = "null";

                Tools.LogError("PML Edit Public Record. Error trying to parse 'appid' guid from query string. Found value: '" + sAppId + "'.");
                throw new CBaseException("Cannot edit a public record without a valid application id.", exc);
            }

			if ( ! Page.IsPostBack )
				LoadData();
		}

		private void LoadData()
		{
			CPageData dataLoan = new CPublicRecordData( LoanID );
			dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;
            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);
			IPublicRecordCollection publicRecords = dataApp.aPublicRecordCollection;
			var editRecord = publicRecords.GetRegRecordOf( new Guid( RequestHelper.GetSafeQueryString("recordid") ) );

			CourtName.Text = editRecord.CourtName;
			Tools.SetDropDownListValue(Type, editRecord.Type);
			ReportedD.Text = editRecord.ReportedD_rep;
			DispositionD.Text = editRecord.DispositionD_rep;
			BkFileD.Text = editRecord.BkFileD_rep;
			LastEffectiveD.Text = editRecord.LastEffectiveD_rep;
			Tools.SetDropDownListValue(DispositionT, editRecord.DispositionT);
			BankruptcyLiabilitiesAmount.Text = editRecord.BankruptcyLiabilitiesAmount_rep;
			IncludeInPricing.Checked = editRecord.IncludeInPricing;

            m_auditGridView.DataSource = editRecord.AuditTrailItems;
            m_auditGridView.DataBind();

		}

		private CPageData SaveData()
		{
            CPageData dataLoan = new CApplicantInfoData(LoanID);
			dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(m_currentAppId);
			IPublicRecordCollection publicRecords = dataApp.aPublicRecordCollection;
            
			var editRecord = publicRecords.GetRegRecordOf( new Guid( RequestHelper.GetSafeQueryString("recordid") ) );
			editRecord.CourtName = CourtName.Text;
			editRecord.Type = (E_CreditPublicRecordType) Tools.GetDropDownListValue( Type );
			editRecord.ReportedD_rep = ReportedD.Text;
			editRecord.DispositionD_rep = DispositionD.Text;
			editRecord.BkFileD_rep = BkFileD.Text;
			editRecord.LastEffectiveD_rep = LastEffectiveD.Text;
			editRecord.DispositionT = (E_CreditPublicRecordDispositionType) Tools.GetDropDownListValue( DispositionT );
			editRecord.BankruptcyLiabilitiesAmount_rep = BankruptcyLiabilitiesAmount.Text;
			editRecord.IncludeInPricing = IncludeInPricing.Checked;
			editRecord.Update();

			dataLoan.Save();
            return dataLoan;
		}

		// Save and close
		protected void OnOkClick (object sender, System.EventArgs e )
		{
			CPageData dataLoan = SaveData();
            ClientScript.RegisterStartupScript(this.GetType(), "autoclose", "window.parent.LQBPopup.Return();", true);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
