﻿
namespace PriceMyLoan.main
{
    using System;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// This class acts as the service page for the YourProfile page.  It currently only contains methods for a user's mobile devices.
    /// </summary>
    public partial class YourProfileService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Executes a method matching the methodName.
        /// </summary>
        /// <param name="methodName">The name of the method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "RegisterNewDevice":
                    this.RegisterNewDevice();
                    break;
                case "RemoveDevice":
                    this.RemoveDevice();
                    break;
                case "RegisterForAuthenticator":
                    this.RegisterForAuthenticator();
                    break;
                case "CompleteAuthenticatorRegistration":
                    this.CompleteAuthenticatorRegistration();
                    break;
                case "DisableAuthenticator":
                    DisableAuthenticator();
                    break;


            }
        }

        /// <summary>
        /// Deletes a device from the user's list of registered devices.
        /// </summary>
        private void RemoveDevice()
        {
            // Get register info id
            long id = Convert.ToInt64(GetString("DeviceId"));

            bool isSuccessful = MobileDeviceUtilities.RemoveDevice(id);

            SetResult("Success", isSuccessful);
        }

        /// <summary>
        /// Registers a new device for the user's account.
        /// </summary>
        private void RegisterNewDevice()
        {
            string deviceName = GetString("DeviceName");
            string mobilePassword;

            bool isSuccessful = MobileDeviceUtilities.RegisterNewDevice(deviceName, out mobilePassword);

            if (isSuccessful)
            {
                this.SetResult("MobilePassword", mobilePassword);
            }

            this.SetResult("Success", isSuccessful);
        }

        /// <summary>
        /// Creates registration information for authenticator.
        /// </summary>
        private void RegisterForAuthenticator()
        {
            var data = MultiFactorAuthCodeUtilities.GenerateAuthenticatorRegistrationInfo(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.UserId);
            if (data == null)
            {
                SetResult("Success", "False");
                SetResult("ErrorMessage", "OTP Service is not available. Please try again later.");
                return;
            }

            var key = Guid.NewGuid();
            AutoExpiredTextCache.AddToCacheByUser(PrincipalFactory.CurrentPrincipal, data.Seed, TimeSpan.FromMinutes(60), key);

            SetResult("Success", "True");
            SetResult("Key", key);
            SetResult("ManualCode", data.ManualCode);
            SetResult("Url", data.QrCodeUrl);
        }
        private void DisableAuthenticator()
        {
            var e = EmployeeDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);
            e.MfaOtpSeed = string.Empty;
            e.Save();
        }
        /// <summary>
        /// Validates the passed in the token.
        /// </summary>
        private void CompleteAuthenticatorRegistration()
        {
            string seed = AutoExpiredTextCache.GetUserString(PrincipalFactory.CurrentPrincipal, GetString("key"));
            var result = MultiFactorAuthCodeUtilities.ValidateAuthenticatorToken(seed, GetString("token"));

            if (result.HasValue)
            {
                SetResult("Success", result.Value.ToString());
                if (result.Value)
                {
                    var e = EmployeeDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);
                    e.MfaOtpSeed = seed;
                    e.Save();
                    return;
                }
            }

            SetResult("Success", "False");
            SetResult("ErrorMessage", "OTP Service is not available. Please try again later.");
        }
    }
}