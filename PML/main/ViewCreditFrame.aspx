<%@ Page language="c#" Codebehind="ViewCreditFrame.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.ViewCreditFrame" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>Credit Report</title>
    <style>
        /* OPM 471677, Update design for iframe modal*/
        .body-iframe {
            overflow: hidden;
            margin: 0;   
        }
        .body-iframe iframe {
            width: 100%;
            height: 100%;
        }
        .body-iframe form {
            height: 100%;
            margin: 0;
        }
        .body-iframe .top {
            height: 50px;
        }
        .body-iframe .bottom {
            position: absolute;
            top: 50px;
            left: 0;
            bottom: 0;
            right: 0;
        }
    </style>
  </head>
    <h4 class="page-header">Credit Report</h4>
    <body class="body-iframe">
        <form runat="server">
            <div class="top">
                <iframe class="height-50px" id="menu" name="menu" src=<%= AspxTools.SafeUrl(m_menuUrl) %> scrolling=no></iframe>
            </div>
            <div class="bottom">
                <iframe id="MainFrame" name="body" src=<%= AspxTools.SafeUrl(m_bodyUrl) %>></iframe>
            </div>
        </form>
    </body>
</html>
