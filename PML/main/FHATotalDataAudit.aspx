﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHATotalDataAudit.aspx.cs" Inherits="PriceMyLoan.main.FHATotalDataAudit"  EnableEventValidation="false" EnableViewState="false" EnableViewStateMac="false" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>FHA Total Data Audit</title>
    <style type="text/css" >
        body { background-color: #FFF; width:95%;}
        legend, h2 { margin: 0; padding: 0; }
        .Fields { border-collapse: collapse; background-color: White; margin-left: 20px; width: 95%;  table-layout: fixed;}
        .Fields tr td { padding-left: 5px; }
        table.Fields tr { vertical-align:top; }
        .alt { background-color: #DAEEF3; }
        .nav { display: block; width:100%;  text-align: center; margin-top: 10px;}
        .nav input { margin: 0 auto;   }
        .Column1 { width: 180px; }
        .Column2 { width: 180px; }
        .Column3 { color: Red; }
        .Column1W { width: 230px; }
        #mainbody { font-family:verdana,georgia, sans-serif; }
        
        #mainbody fieldset.headerLine{
            border-top : none;
            border-bottom: none;
            border-right: none;
            border-left: none;
        }
        
        legend.MainHeader
        {
        	font-weight: bold;
        	color: #ff9933;
        	font-size:16px; 
        	margin-bottom:10px; 
            padding:3px;
        }
      
        #mainbody fieldset h2, #mainbody div h2 {
            font-size:14px; 
            font-weight:bold; 
            color:#ff9933;  
            margin-bottom:5px; 
            padding:3px;
            padding-left:10px;
        }
        #mainbody a {
            color:#565656;  
            font-weight:bold; 
            text-decoration:none;
            cursor:pointer;
        }
        #mainbody table.Fields { border:solid 1px #BCBCBC; }
        #mainbody table.Fields td.tableHeader        
        {
        	font-weight:bold;
	        color:Black;
	        background-color:#e2e2e2;
	        padding-top: 2px;
	        padding-right: 0px;
	        padding-bottom: 2px;
	        padding-left: 5px;
       }
       #mainbody a.Anchor
       {
       	    font-weight:normal;
       	    color: Blue;
       	    font-family : Verdana, Arial, Helvetica, sans-serif;
	        font-size : 11px;
       }
    </style>
</head>
<body onload="">
    <script type="text/javascript">
        function _init() {
                FHATotalDataAudit.onload();
        }
    </script>
    <h4 class="page-header">FHA Total Data Audit</h4>
    <form id="form1" runat="server">
    <div id="mainbody">
        <fieldset class="headerLine">
        <h2>Borrower Info</h2>
        <asp:Repeater runat="server" ID="Applications" OnItemDataBound="Applications_OnItemDataBound">
            <ItemTemplate>
                <table  class="Fields dataTable">
                    <tr>
                    <td class="tableHeader Column1">Application <ml:EncodedLiteral runat="server" ID="AppNumber" /></td>
                    <td class="tableHeader Column2"></td>
                    <td class="tableHeader Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#">Borrower</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerFullName" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower SSN</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerSsn" /></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aBSsnTotalScorecardWarning" /></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower Citizenship</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerCititzenshipStatus" /></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server" ID="aBDecResidencyTotalScorecardWarning" /></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower Position/Title</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerTitle" /></td>
                        <td  class="Column3"><ml:EncodedLiteral runat="server" ID="aBEmplrJobTitleTotalScorecardWarning" /></td>
                    </tr>
                    
                    <tr>
                        <td class="Column1"><a href="#" >Borrower Credit Scores</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerCreditScores"></ml:EncodedLiteral></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower intends to occupy the property as primary residence</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerOccupancy"></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server" ID="aBDecOccTotalScorecardWarning"></ml:EncodedLiteral></td>
                    </tr>
                    <asp:PlaceHolder runat="server" id="CoborrowerFields" Visible="False">
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerName"></ml:EncodedLiteral> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1" ><a href="#">Co-borrower SSN</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerSsn"></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCSsnTotalScorecardWarning"> </ml:EncodedLiteral></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower Citizenship</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerCitizenshipStatus"></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCDecResidencyTotalScorecardWarning"> </ml:EncodedLiteral></td>   
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower Position/Title</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerTitle"></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server" ID="aCEmplrJobTitleTotalScorecardWarning" ></ml:EncodedLiteral> </td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower Credit Scores</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat=server ID="CoborrowerScores"></ml:EncodedLiteral></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower intends to occupy the property as primary residence?</a></td>
                        <td class="Column2"> <ml:EncodedLiteral runat="server" ID="CoborrowerOccupancy"></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCDecOccTotalScorecardWarning"> </ml:EncodedLiteral></td>   
                    </tr>
    
                    </asp:PlaceHolder>
                    <tr>
                        <td class="Column1"><a href="#" >Monthly Income</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="MonthlyIncome"></ml:EncodedLiteral> </td>
                        <td></td>
                    </tr>
                      <tr>
                        <td class="Column1"><a href="#">Gift Funds</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="GiftFunds"></ml:EncodedLiteral> </td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Liquid Assets</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="LiquidAssets"></ml:EncodedLiteral> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Non-Mortgage Payment</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="NonMortgagePayment"></ml:EncodedLiteral></td>
                        <td></td>
                    </tr>
                </table>
                <br />
                </ItemTemplate>
        </asp:Repeater>
            </fieldset>  
        <div>
        <h2>Loan Summary</h2>
        <table id="LoanSummary" class="Fields dataTable">
            <tr>
                <td class="Column1"><a href="#">Primary Borrower</a></td>
                <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_PrimaryBorrower" /></td>
                <td></td>
            </tr>
            <tr >
                <td class="Column1"><a href="#">Number of Applicants</a></td>
                <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_NumberOfTotalApps" /></td>
                <td class="Column3"><ml:EncodedLiteral runat="server" ID="sBorrCountTotalScorecardWarning" /></td>
            </tr>
            <tr>
                <td  class="Column1"><a href="#" >Representative Score</a></td>
                <td  class="Column2"><ml:EncodedLiteral runat="server" ID="ls_RepresentativeScore" /></td>
                <td></td>
            </tr>
            <tr>
                <td class="Column1"><a href="#" >Total Monthly Income</a></td>
                <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalMonthlyIncome" /></td>
                <td class="Column3"><ml:EncodedLiteral runat="server" ID="sLTotITotalScorecardWarning" /></td>
            </tr>
            <tr>
                    <td class="Column1"><a href="#" >Total Liquid Assets</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalLiquidAssets" /></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Total Non-Mortgage Payment</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalNonMortgagePayments" /></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Mortgage Liabilities</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_MortgageLiabilities" /></td>
                    <td class="Column3"></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >REO Properties</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_ReoPropertiesCount" /></td>
                    <td class="Column3"></td>
                </tr>
                       <tr>
                    <td class="Column1"><a href="#" >Total Cost</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalCosts"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="Column1"><a href="#" >Borrower's closing costs paid by seller</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_SellerClosingCostPaid"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>   
                <tr>
                    <td class="Column1"><a href="#">Borrower paid closing costs</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="sTotalScorecardBorrPaidCc"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>   
                          <tr>
                    <td class="Column1"><a href="#">Total closing costs</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="sTotEstCcNoDiscnt1003"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="Column1"><a href="#" >Cash from/to Borrower</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_CashftBorrower"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>         
            <tr>
                    <td class="Column1"><a href="#" >Loan Purpose</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_LoanPurpose"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
           <tr>
                    <td class="Column1"><a href="#">Property Type</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="sGseSpTFriendlyDisplay"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>  
            <tr>
                    <td class="Column1"><a href="#" >Purchase Price</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_PurchasePrice"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server" ID="sPurchPriceTotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Appraised Value</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_AppraisedValue"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server" ID="sApprValTotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Loan Amount</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_LoanAmount"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1" ><a href="#" >Upfront MIP</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_UpfrontMip"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server" ID="sFfUfmip1003TotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#"  >Upfront MIP Financed</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_UpfrontMipFinanced"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Total Loan Amount</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalLoanAmount"></ml:EncodedLiteral></td>
                    <td class="Column3"> <ml:EncodedLiteral runat="server" ID="sLAmtCalcTotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >LTV/CLTV</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_ltvcltv"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
        </table>
                <br />
              <h2 style="display:inline;">Additional Validation</h2>&nbsp;<a href="javascript:FHATotalDataAudit.ToggleTable(this);" runat="server" class="Anchor" id="TableAnchor">(hide)</a>
              <table id="ExtraValidationHeader" class="Fields" style="margin-top:6px;" > 
                <tr class="alt">
                    <td class="Column1"><a href="#" >Additional Validation Rules</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="AdditionalValidationStatus"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server" id="AdditioanlValidationError"></ml:EncodedLiteral></td>
                </tr>
              </table>
              <table  id="ExtraValidation" class="Fields" style="margin-top:6px;" >
                  <tbody>
                      <asp:Repeater runat="server" id="ExtraEmploymentValidation" OnItemDataBound="ExtraValidation_DataBound" >
                        
                        <ItemTemplate >
                            <tr class="alt">
                                <td class="Column1W">Application <ml:EncodedLiteral runat="server" ID="AppNumber"></ml:EncodedLiteral> 
                                <br />&nbsp;<a id="ValidationFixLink"  runat="server"></a>
                                </td>
                                <td class="Column2"><ml:EncodedLiteral runat="server" ID="ValidationStatus"></ml:EncodedLiteral></td>
                                <td class="Column3"><ml:EncodedLiteral runat="server" ID="ValidationError"></ml:EncodedLiteral></td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr  >
                                <td class="Column1W">Application <ml:EncodedLiteral runat="server" ID="AppNumber"></ml:EncodedLiteral> 
                                <br />&nbsp;<a id="ValidationFixLink"  runat="server"></a>
                                </td>
                                <td class="Column2"><ml:EncodedLiteral runat="server" ID="ValidationStatus"></ml:EncodedLiteral></td>
                                <td class="Column3"><ml:EncodedLiteral runat="server" ID="ValidationError"></ml:EncodedLiteral></td>
                            </tr>
                        </AlternatingItemTemplate>
                      </asp:Repeater>
                  </tbody>
              </table>
        </div>  
        <br />  
        <div class="nav" >
        <asp:Button runat="server" ID="ContinueBtn"  Text="Continue to FHA TOTAL Submission" NoHighlight Width="250" class="ButtonStyle" OnClick="Continue_OnClick"/>&nbsp;&nbsp;
        <input type="button" id="CancelBtn" class="ButtonStyle" value="Cancel" NoHighlight onclick="f_close();" runat="server"/>
        </div>
    </div>
    </form>
    <script type="text/javascript">
      function f_close()
      {
        parent.LQBPopup.Return({OK:false});
      }
        var FHATotalDataAudit = {
            onload : function() {
              $j('.dataTable tr').filter(':odd').addClass('alt');
                if ( <%= AspxTools.JsBool(HideTable) %> ) {
                    $j('#ExtraValidation').hide();
                }
                //resize(780, 600);
            },
            
            ToggleTable : function() {
                var header = $j('#ExtraValidationHeader');
                var table = $j('#ExtraValidation');
                if ( table.is(':visible') === false ) { 
                    table.show(); 
                    header.hide();
                    $j('#TableAnchor').html('(hide)');
                } 
                else { 
                    table.hide();
                    header.show();
                    $j('#TableAnchor').html('(show)');
                }
            }
        };
    </script>

</body>
</html>
