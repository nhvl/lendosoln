﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportLoanDataAudit.aspx.cs" Inherits="PriceMyLoan.main.ImportLoanDataAudit" EnableEventValidation="false" EnableViewState="false" EnableViewStateMac="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title>Imported Loan File</title>
    <style type="text/css" >
        body { background-color: #FFF; margin:0px; padding-top: 0; }
        legend, h2 { margin: 0; padding: 0; }
        .Fields { border-collapse: collapse; background-color: White; margin-left: 20px; width: 95%;  table-layout: fixed;}
        .Fields tr td { padding-left: 5px; }
        table.Fields tr { vertical-align:top; }
        .alt { background-color: #DAEEF3; }
        .Column1 { width: 180px; }
        .Column2 { width: 180px; }
        .Column3 { color: Red; }
        .text-center { text-align: center; }
        .audit-loan-header { margin-top: -14px; }
        .audit-loan-header ul.loan-info-bar { width: 98%; }
        #mainbody { font-family:verdana,georgia, sans-serif; margin: 10px; margin-right:30px; width:800px; }
        
        #mainbody fieldset.headerLine{
            border-top : solid 1px #ff9933;
            border-bottom: none;
            border-right: none;
            border-left: none;
        }
        
        legend.MainHeader
        {
        	font-weight: bold;
        	color: #ff9933;
        	font-size:16px; 
        	margin-bottom:10px; 
            padding:3px;
        }
      
        #mainbody fieldset h2, #mainbody div h2 {
            font-size:14px; 
            font-weight:bold; 
            color:#ff9933;  
            margin-bottom:5px; 
            padding:3px;
            padding-left:10px;
        }
        #mainbody a {
            color:#565656;  
            font-weight:bold; 
            text-decoration:none;
            cursor:pointer;
        }
        #mainbody table.Fields { border:solid 1px #BCBCBC; }
        #mainbody table.Fields td.tableHeader        
        {
        	font-weight:bold;
	        color:Black;
	        background-color:#e2e2e2;
	        padding-top: 2px;
	        padding-right: 0px;
	        padding-bottom: 2px;
	        padding-left: 5px;
        }
        td.warningMsg
        {
        	border: #ffcc99 1px dashed;
        	font-weight: bold;
        	color: red;
        	background-color: #ffffcc;
        }
    </style>
</head>
<body scroll=yes>
    <script type="text/javascript">
        function _init() {
                DataAudit.onload();
        }
    </script>
    <form id="form1" runat="server">
    <div class="audit-loan-header">
        <div id="NavigationHeaderController" class="warp">
            <div>
                <ul class="loan-info-bar">
                    <% if (!IsEncompassIntegration && m_allowGoToPipeline) { %>
                    <li class="back-btn">
                        <a href="#" onclick="f_goToPipeline();">
                            <i class="material-icons pipeline-icon ng-cloak">&#xE879;</i>
                            <span class="pipeline-text">Pipeline</span>
                        </a>
                    </li>
                    <% } %>
                    <li>
                        <p class="title">Loan Number: <span><ml:EncodedLiteral id="sLNm" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">Borrower Name: <span><ml:EncodedLiteral id="aBNm" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">Loan Amount: <span><ml:EncodedLiteral id="sLAmtCalc" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">
                            Primary Credit Score: 
                            <span>
                                <ml:EncodedLiteral id="sCreditScoreType1" runat="server" />
                                <a onclick="f_openHelp('Q00006.html', 400, 150);" title="Help"><i class="material-icons">&#xE887;</i></a>
                            </span>
                        </p>
                    </li>
                    <li>
                        <p class="title">
                            Lowest Credit Score: 
                            <span>
                                <ml:EncodedLiteral id="sCreditScoreType2" runat="server" />
                                <a onclick="f_openHelp('Q00006.html', 400, 150);" title="Help"><i class="material-icons">&#xE887;</i></a>
                            </span>
                        </p>
                    </li>
                    <li>
                        <p class="title">LTV: <span><ml:EncodedLiteral id="sLtvR" runat="server" /></span></p>
                    </li>
                    <li>
                        <p class="title">CLTV: <span><ml:EncodedLiteral id="sCltvR" runat="server" /></span></p>
                    </li>
                    <% if (!IsEncompassIntegration) { %>
                    <li>
                        <p class="title text-center">
                            <span class="log-out">
                                <a id="LogoutButton" onclick="f_logout();" href="#"><i class="material-icons logout-icon">&#xE879;</i><span class="text log-out-text">Log Off</span></a>
                            </span>
                        </p>
                    </li>
                    <% } %>
                </ul>
            </div>
        </div>
    </div>
    <br />
    <asp:PlaceHolder ID="warningMsg" runat="server" Visible="true">
    <table style="margin:10px;">
        <tr>
         <td class="warningMsg" >    
            CAUTION: A review of the 1003 data indicates that some information may be incorrect or incomplete. Please revisit the warnings below, and correct and re-import the file if needed.
         </td>
        </tr>
    </table>
    </asp:PlaceHolder>
    <div id="mainbody">
        <fieldset class="headerLine">
        <legend class="MainHeader">Imported Loan File -- 1003 Data Summary</legend>
        <h2>Borrower Info</h2>
        <asp:Repeater runat="server" ID="Applications" OnItemDataBound="Applications_OnItemDataBound">
            <ItemTemplate>
                <table  class="Fields">
                    <tr>
                    <td class="tableHeader Column1">Application <ml:EncodedLiteral runat="server" ID="AppNumber"></ml:EncodedLiteral></td>
                    <td class="tableHeader Column2"></td>
                    <td class="tableHeader Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#">Borrower</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerFullName"></ml:EncodedLiteral></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower SSN</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerSsn" ></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aBSsnTotalScorecardWarning"> </ml:EncodedLiteral></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower Citizenship</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerCititzenshipStatus" ></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server" ID="aBDecResidencyTotalScorecardWarning" ></ml:EncodedLiteral> </td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower Position/Title</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerTitle" ></ml:EncodedLiteral></td>
                        <td  class="Column3"><ml:EncodedLiteral runat="server" ID="aBEmplrJobTitleTotalScorecardWarning" ></ml:EncodedLiteral> </td>
                    </tr>
                    
                    <tr>
                        <td class="Column1"><a href="#" >Borrower Credit Scores</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerCreditScores"></ml:EncodedLiteral></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Borrower intends to occupy the property as primary residence</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerOccupancy"></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server" ID="aBDecOccTotalScorecardWarning"></ml:EncodedLiteral></td>
                    </tr>
                    <asp:PlaceHolder runat="server" id="CoborrowerFields" Visible="False">
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerName"></ml:EncodedLiteral> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1" ><a href="#">Co-borrower SSN</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerSsn"></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCSsnTotalScorecardWarning"> </ml:EncodedLiteral></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower Citizenship</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerCitizenshipStatus"></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCDecResidencyTotalScorecardWarning"> </ml:EncodedLiteral></td>   
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower Position/Title</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerTitle"></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server" ID="aCEmplrJobTitleTotalScorecardWarning" ></ml:EncodedLiteral> </td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower Credit Scores</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerScores"></ml:EncodedLiteral></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Co-borrower intends to occupy the property as primary residence?</a></td>
                        <td class="Column2"> <ml:EncodedLiteral runat="server" ID="CoborrowerOccupancy"></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCDecOccTotalScorecardWarning"> </ml:EncodedLiteral></td>   
                    </tr>
    
                    </asp:PlaceHolder>
                    <tr>
                        <td class="Column1"><a href="#" >Monthly Income</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="MonthlyIncome"></ml:EncodedLiteral> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Liquid Assets</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="LiquidAssets"></ml:EncodedLiteral> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" >Non-Mortgage Payment</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="NonMortgagePayment"></ml:EncodedLiteral></td>
                        <td></td>
                    </tr>
                </table>
                <br />
                </ItemTemplate>
        </asp:Repeater>
            </fieldset>  
        <div>
        <h2>Loan Summary</h2>
        <table id="LoanSummary" class="Fields">
            <tr>
                <td class="Column1"><a href="#">Primary Borrower</a></td>
                <td class="Column2"> <ml:EncodedLiteral runat="server" ID="ls_PrimaryBorrower" ></ml:EncodedLiteral></td>
                <td></td>
            </tr>
            <tr >
                <td class="Column1"><a href="#">Number of Applicants</a></td>
                <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_NumberOfTotalApps"></ml:EncodedLiteral></td>
                <td class="Column3"><ml:EncodedLiteral runat="server" ID="sBorrCountTotalScorecardWarning"></ml:EncodedLiteral></td>
            </tr>
            <tr>
                <td  class="Column1"><a href="#" >Representative Score</a></td>
                <td  class="Column2"><ml:EncodedLiteral runat="server" ID="ls_RepresentativeScore"></ml:EncodedLiteral></td>
                <td></td>
            </tr>
            <tr>
                <td class="Column1"><a href="#" >Total Monthly Income</a></td>
                <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalMonthlyIncome"></ml:EncodedLiteral></td>
                <td class="Column3"><ml:EncodedLiteral runat="server" ID="sLTotITotalScorecardWarning"></ml:EncodedLiteral></td>
            </tr>
            <tr>
                    <td class="Column1"><a href="#" >Total Liquid Assets</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalLiquidAssets"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Total Non-Mortgage Payment</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalNonMortgagePayments"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Mortgage Liabilities</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_MortgageLiabilities"></ml:EncodedLiteral></td>
                    <td class="Column3"></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Reo Properties</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_ReoPropertiesCount"></ml:EncodedLiteral></td>
                    <td class="Column3"></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Borrower's closing cost paid by seller</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_SellerClosingCostPaid"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>   
                <tr>
                    <td class="Column1"><a href="#" >Total Cost</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalCosts"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Cash from/to Borrower</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_CashftBorrower"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>         
            <tr>
                    <td class="Column1"><a href="#" >Loan Purpose</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_LoanPurpose"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Purchase Price</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_PurchasePrice"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server" ID="sPurchPriceTotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Appraised Value</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_AppraisedValue"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server" ID="sApprValTotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Loan Amount</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_LoanAmount"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1" ><a href="#" >Upfront MIP</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_UpfrontMip"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server" ID="sFfUfmip1003TotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#"  >Upfront MIP Financed</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_UpfrontMipFinanced"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >Total Loan Amount</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_TotalLoanAmount"></ml:EncodedLiteral></td>
                    <td class="Column3"> <ml:EncodedLiteral runat="server" ID="sLAmtCalcTotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" >LTV/CLTV</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server" ID="ls_ltvcltv"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
        </table>
        </div>  
        <br />  
        <asp:Button id=m_btnNext runat="server" Text="Next >" CssClass="ButtonStyle" NoHighlight onclick="m_btnNext_Click"></asp:Button>
        <br />  
        <br />  
    </div>
    </form>
    <script type="text/javascript">
        var DataAudit = {
            onload : function() {
                var tables = document.getElementsByTagName("table");
                for (var x = 0; x < tables.length; x++ ) {
                    if ( tables[x].className !== 'Fields' ) {
                        continue;
                    }  
                    var rows = tables[x].getElementsByTagName("tr");
                    var isAlt = false;
                    for(i = 0; i < rows.length; i++){  
                        rows[i].className = isAlt ? 'alt' : '';
                        isAlt = !isAlt;
                    }
                }
            }
        };
        function f_editLoan(sLId) {
            document.location = "agents.aspx?loanid=" + encodeURIComponent(sLId);
        }
        function f_goToPipeline() {
            self.location = <%= AspxTools.JsString(VirtualRoot + "/Main/Pipeline.aspx") %>;
        }
        function f_openHelp(page, w, h)
        {
            var url = VRoot + "/help/" + page;
            LQBPopup.Show(url, { 'width': w, 'height': h});
        }
        function f_logout() {
         self.location = gVirtualRoot + "/logout.aspx";
        }
    </script>
</body>
</html>
