<%@ Page language="c#" Codebehind="EditPublicRecord.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.EditPublicRecord" %>
<%@ Register TagPrefix="ml" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>Edit Public Record</title>
    <style>
    pre {
    font-size: 11px;
	  line-height: normal;
	  color: #666666;
	  font-family : Verdana, Arial, Helvetica, sans-serif;
	  
    }
    </style>
  </head>
  <body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto;">
  	<script language=javascript>
<!--
  	    jQuery(function($) {
  	        $('#Cancel').click(function() { window.parent.LQBPopup.Return(); });
  	    });
//-->
</script>
  
    <form id="EditPublicRecord" method="post" runat="server">
      <span class="SectionHeader">&nbsp;Edit Public Record</span>
      <table cellpadding="0" cellspacing="2" border="0">
      <tr>
        <td class="FieldLabel">Court Name</td>
        <td><asp:TextBox id="CourtName" runat="server" Width="200px" /></td>
        <td class="FieldLabel">Type</td>
        <td><asp:DropDownList id="Type" runat="server" Width="200px" /></td>
      </tr>
      <tr>
        <td class="FieldLabel">Amount</td>
        <td><ml:moneytextbox id="BankruptcyLiabilitiesAmount" width="90" runat="server" preset="money" /></td>
        <td class="FieldLabel">Disposition</td>
        <td><asp:DropDownList id="DispositionT" runat="server" Width="200px" /></td>
      </tr>
      <tr>
        <td class="FieldLabel">Disposition Date</td>
        <td><ml:DateTextBox id="DispositionD" runat="server" width="75" preset="date" /></td>
        <td class="FieldLabel">Filed Date</td>
        <td><ml:DateTextBox id="BkFileD" runat="server" width="75" preset="date" /></td>
      </tr>
      <tr>
        <td class="FieldLabel">Reported Date</td>
        <td><ml:DateTextBox id="ReportedD" runat="server" width="75" preset="date" /></td>
        <td class="FieldLabel">Last Effective Date</td>
        <td><ml:DateTextBox id="LastEffectiveD" runat="server" width="75" preset="date" /></td>
      </tr>
      <tr>
        <td class="FieldLabel">Include In Pricing</td>
        <td><asp:CheckBox id="IncludeInPricing" runat="server" /></td>
        <td></td>
        <td></td>
      </tr>
      <tr><td colspan="4">&nbsp;</td></tr>
      <tr>
        <td class="FieldLabel" colspan="4">Audit History:</td>
      </tr>
      <tr>
        <td colspan="4">
        <div style="BORDER:1px solid; MARGIN:3px 3px 8px 3px;OVERFLOW:auto;HEIGHT:210px">
          <asp:GridView ID="m_auditGridView" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="DataGrid" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" HeaderStyle-CssClass="GridHeader" EnableViewState="false" RowStyle-VerticalAlign="Top">
            <Columns>
              <asp:BoundField ShowHeader="true" DataField="UserName" HeaderText="User" />
              <asp:BoundField ShowHeader="true" DataField="EventDate" HeaderText="Date" />
              <asp:TemplateField ShowHeader="true" HeaderText="Description">
                <ItemTemplate>
                  <pre><%# AspxTools.HtmlString(Eval("Description").ToString()) %></pre>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>

        </div>
        </td>
      </tr>
      <tr>
        <td colspan="4" align="center"><asp:Button runat="server" Text=" OK " id="OkBtn" OnClick="OnOkClick" class="ButtonStyle" />&nbsp;<input id="Cancel" type="button" value="Cancel" class="ButtonStyle" /></td>
      </tr>
      </table>      
   </form>
	<ml:cModalDlg id=CModalDlg1 runat="server"></ml:cModalDlg>
  </body>
</html>
