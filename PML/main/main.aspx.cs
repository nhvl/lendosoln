namespace PriceMyLoan.UI.Main
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using ConfigSystem;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using PriceMyLoan.Common;

    public partial class main : PriceMyLoan.UI.BasePage
    {
        private const string ErrorCode_VersionMismatch = "VERSIONMISMATCH";
        private const string ErrorCode_FieldEditPermissionDenied = "FIELD_EDIT_PERMISSION_DENIED";
        protected EnterCredit m_enterCredit;
        protected ApplicantInfo m_applicantInfo;
        protected PropertyInfo m_propertyInfo;
        protected GetResults m_getResults;
        protected Panel ActivityReportingPanel; 

        protected EnterCreditMultApp m_enterCreditMultApp;
        protected ApplicantInfoMultApp m_applicantInfoMultApp;

        protected System.Web.UI.WebControls.PlaceHolder content;
        protected int m_currentIndex = 0;
        protected int m_previousIndex = 0;
        protected int m_maxVisitedIndex = 0;

        public string m_creditAction = "";
        public Guid m_AppId = Guid.Empty;

        protected bool m_hasError = false;
        private ArrayList m_list;
        private string[] m_statuses;

        protected string Source
        {
            get
            {
                return RequestHelper.GetSafeQueryString("source");
            }
        }

        public bool UserOnlyHasRunPMLToStep3
        {
            get
            {
                // User has "only run to step 3" but has neither "run PML registered loans" nor "run PML all loans"
                return
                (true == CurrentUserCanPerform(WorkflowOperations.RunPmlToStep3) &&
                 false == CurrentUserCanPerformAny(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans));
            }
        }

        protected string m_loanIdEncompassRequestType
        {
            get
            {
                return LoanID + "_" + PriceMyLoanRequestHelper.EncompassRequestType.ToString();
            }
        }
        protected int m_maxSteps 
        {
            get { return m_list.Count; }
        }
        protected bool m_allowGoToPipeline 
        {
            get { return PriceMyLoanUser.IsAllowGoToPipeline && !PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            List<WorkflowOperation> current = new List<WorkflowOperation> { 
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans, 
                WorkflowOperations.RunPmlToStep3,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.UseOldSecurityModelForPml,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock
            };

            return current;
        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

        public override IEnumerable<WorkflowOperation> GetReasonsForOp()
        {

            List<WorkflowOperation> current = new List<WorkflowOperation>() { 
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.RegisterLoan,
                WorkflowOperations.RequestRateLockCausesLock
            };

            return current;
        }

        private bool OldEngineRedirectToDenial()
        {
            if (this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                return false; // 7/28/2006 dd - Skip permission checking if mode is rerun.

            bool canWrite = CurrentUserCanPerform(WorkflowOperations.WriteLoan);

            if (!canWrite)
            {
                // OPM 5068 Cord -- Take the user to the Access Denial page.
                return true;
            }
            return false;
        }

        private bool NewEgineRedirectToDenial()
        {
            if (false == CurrentUserCanPerform(WorkflowOperations.ReadLoan))
            {
                return true;
            }

            if (false == CurrentUserCanPerformAny(
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3))
            {
                return true;
            }

            return false;
        }

        private void CheckForPermission() 
        {
            bool newRedirectToDenial = NewEgineRedirectToDenial();
            bool oldRedirectToDenial = OldEngineRedirectToDenial();

            bool redirectToDenial = CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ? oldRedirectToDenial : newRedirectToDenial;

            if (redirectToDenial)
            {
                PriceMyLoanRequestHelper.AccessDenial(LoanID);
            }
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            RegisterJsScript("jquery-ui.js");
            RegisterCSS("/webapp/jquery-ui.css");
            CheckForPermission();
            this.EnableViewState = false;
            ClientScript.GetPostBackEventReference(this, ""); // NEED TO HAVE THIS LINE in order for .NET to register __doPostBack

            //opm 25872 fs 04/25/09
            m_enterCreditMultApp.Visible   = PriceMyLoanUser.IsPricingMultipleAppsSupported;
            m_applicantInfoMultApp.Visible = PriceMyLoanUser.IsPricingMultipleAppsSupported;
            m_enterCredit.Visible          = !PriceMyLoanUser.IsPricingMultipleAppsSupported;
            m_applicantInfo.Visible        = !PriceMyLoanUser.IsPricingMultipleAppsSupported;

            m_list = new ArrayList();

            if (PriceMyLoanUser.IsPricingMultipleAppsSupported)
            {
                m_list.Add(m_enterCreditMultApp);
                m_list.Add(m_applicantInfoMultApp);
            }
            else
            {
                m_list.Add(m_enterCredit);
                m_list.Add(m_applicantInfo);
            }
            m_list.Add(m_propertyInfo);
            m_list.Add(m_getResults);
            m_statuses = new string[m_list.Count];

            this.RegisterService("main", "/main/mainservice.aspx");
            this.RegisterJsScript("ModelessDlg.js");
            this.RegisterJsScript("utilities.js");
            //this.ActivityReportingPanel.Visible =  this.PriceMyLoanUser.IsRecordPmlActivity;
        }
        private int FindTabByKey(string key) 
        {
            int ret = -1;
            key = key.ToLower(); // 9/8/2004 dd - Perform case-insensitive search.
            for (int i = 0; i < m_list.Count; i++) 
            {
                ITabUserControl tab = (ITabUserControl) m_list[i];
                if (tab.TabID.ToLower() == key) 
                {
                    ret = i;
                    break;
                }
            }
            return ret;
        }

        private void ConfigureStep4Circumstances()
        {
            // Get the reasons why the user cannot perform the following operations
            List<string> reasons = GetReasonsUserCannotPerformOperation(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans).ToList();
            ConditionType failedConditionType = GetOperationFailedConditionType(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans);

            if (failedConditionType == ConditionType.Restraint)
            {
                Step4CircumstancesMessage.Text = "Pricing cannot be run for this loan file for the following circumstances:";
            }
            else
            {
                Step4CircumstancesMessage.Text = "Pricing may be run for this loan file under any of the following circumstances:";
            }

            // Show up to 10 reasons
            Step4CircumstancesList.DataSource = reasons.Take(10);
            Step4CircumstancesList.DataBind();        
        }
        
        private void SetExclusiveVisibleControl(int index)
        {
            for (int i = 0; i < m_list.Count; i++) 
            {
                if (i == index) m_statuses[i] = "'active'";
                else if (i <= m_maxVisitedIndex) m_statuses[i] = "'on'";
                else m_statuses[i] = "'off'";

                Control ctl = (Control) m_list[i];
                ctl.Visible = i == index;
                if (ctl.Visible) 
                {
                    if (ctl == m_getResults)
                    {
                        ConfigureStep4Circumstances();
                    }
                    ClientScript.RegisterHiddenField("_ClientID", ctl.ClientID);
                }
            }
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_btnLogOff.Visible = m_allowGoToPipeline;
            m_btnStart.Visible = m_allowGoToPipeline;

            ErrorPanel.Visible = false;
            string errorCode = string.Empty;
            string errorMessage = string.Empty;
			// 02/05/08 mf. OPM 20137. For Citi, we cannot have the post movement between
			// steps report a 200 response.  It must return a 302 by using a redirect.
			// We should now redirect after each post, providing the navigation details
			// in the query string.

            PmlMainQueryStringManager mainQueryString = new PmlMainQueryStringManager(Request.QueryString);

			LoadNavigationInfoFromQueryString( mainQueryString );

			// Non-postback: 
			// 1. Load the appropriate step's data to its control
			// 2. Display only this step's control in the UI.
            if (!Page.IsPostBack) 
            {
                if (string.IsNullOrEmpty(mainQueryString.ErrorCode) == false)
                {
                    if (HandleErrorCode(mainQueryString.ErrorCode))
                    {
                        SetExclusiveVisibleControl(-1);
                        ClientScript.RegisterArrayDeclaration("_statuses", string.Join(",", m_statuses));
                        mainQueryString.ErrorCode = string.Empty; // 8/4/2010 dd - Clear out error code.
                        RegisterJsGlobalVariables("ErrorGobackUrl", "main.aspx?" + mainQueryString);

                        btnGetResult.Visible = false;
                        btnContinue.Visible = false;
                        btnNotes.Visible = false;
                        m_tableNavigationSteps.Visible = false;
                        return; // 8/4/2010 dd - If error code is valid then we display and stop render further.
                    }
                    
                }
				try 
				{
					if (m_currentIndex >= 0 && m_currentIndex < m_list.Count )
					{
						ITabUserControl ctl = (ITabUserControl) m_list[m_currentIndex];
						ctl.LoadData();
					}
				} 
				catch (NonCriticalException)
				{
					// 7/28/2004 dd - Do not switch tab if user error occur.
					m_currentIndex = m_previousIndex;
				}

				SetExclusiveVisibleControl(m_currentIndex);
				ClientScript.RegisterArrayDeclaration("_statuses", string.Join(",", m_statuses));
            }

			// Postback:
			// 1. Call the SaveData method of the posted step's control
			// 2. Redirect to this page with the step requested in the querystring
            else if (Page.IsPostBack)
            {
                try
                {
                    if (m_previousIndex != -1)
                    {
                        ITabUserControl ctl = (ITabUserControl)m_list[m_previousIndex];
                        if (!IsReadOnly)
                        {
                            ctl.SaveData();
                        }
                        else
                        {
                            if (ctl.TabID == "CREDIT" && m_enterCreditMultApp.Visible)
                            {
                                m_enterCreditMultApp.setNextAppId();
                            }
                            if (ctl.TabID == "APPINFO" && m_applicantInfoMultApp.Visible)
                            {
                                m_applicantInfoMultApp.setNextAppId();
                            }

                            if (ctl.TabID == "PROPINFO" && m_propertyInfo.Visible)
                            {
                                //so we can save the fields on postback
                                m_propertyInfo.SaveDataReadOnly();
                            }
                        }


                        // 02/15/08 mf. OPM 20137.  The results screen has some custom non-navigational
                        // values it needs across posts.  It might be better to work this into our 
                        // framework (adding this method to the tab interface).
                        if (ctl == m_getResults)
                            m_getResults.SetAdditionalQueryStringValues(mainQueryString);
                        else if (ctl == m_propertyInfo)
                            m_propertyInfo.SetAdditionalQueryStringValues(mainQueryString);
                    }
                }
                catch (NonCriticalException)
                {
                    // 7/28/2004 dd - Do not switch tab if user error occur.
                    m_currentIndex = m_previousIndex;

                    try
                    {
                        if (((ITabUserControl)m_list[m_previousIndex]).TabID == "CREDIT")
                        {
                            if (m_enterCredit.Visible)
                                m_enterCredit.SetAdditionalQueryStringValues(mainQueryString);

                            if (m_enterCreditMultApp.Visible)
                                m_enterCreditMultApp.SetAdditionalQueryStringValues(mainQueryString);
                        }
                    }
                    catch { }
                }
                catch (VersionMismatchException)
                {
                    m_currentIndex = m_previousIndex;
                    errorCode = ErrorCode_VersionMismatch;
                }
                catch (LoanFieldWritePermissionDenied ex)
                {
                    m_currentIndex = m_previousIndex;
                    errorCode = ErrorCode_FieldEditPermissionDenied;
                    errorMessage = ex.UserMessage;
                }

                if (m_AppId != Guid.Empty)
                    mainQueryString.AddPair("appId", m_AppId.ToString());

                m_maxVisitedIndex = m_currentIndex < m_maxVisitedIndex ? m_maxVisitedIndex : m_currentIndex;
                // 8/4/2004 dd - Make sure MaxVisitedIndex never include the very last step.
                m_maxVisitedIndex = m_maxVisitedIndex <= m_list.Count - 2 ? m_maxVisitedIndex : m_list.Count - 2;

                // Perform the redirect to appropriate page.
                mainQueryString.Tabkey = ((ITabUserControl)m_list[m_currentIndex]).TabID;
                mainQueryString.PreviousIndex = m_currentIndex.ToString();
                mainQueryString.MaxVisitedIndex = m_maxVisitedIndex.ToString();
                mainQueryString.ErrorCode = errorCode;
                if (!string.IsNullOrEmpty(errorMessage))
                    mainQueryString.AddPair("errorMessage", errorMessage);
                try
                {
                    Response.Redirect("main.aspx?" + mainQueryString.ToString());
                }
                catch (ArgumentException)
                {
                    Tools.LogErrorWithCriticalTracking("Attempt redirect to main.aspx?" + mainQueryString.ToString());
                }
            }

            BrokerDB brokerDB = BrokerDB.RetrieveById(LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId);
            if ((brokerDB.RemoveMessageToLenderTPOPortal && Source == "PML") || (brokerDB.RemoveMessageToLenderEmbeddedPML && Source != "PML"))
                btnNotes.Visible = false;
        }

        private bool HandleErrorCode(string errorCode)
        {
            if (errorCode.Equals(ErrorCode_VersionMismatch))
            {
                ErrorPanel.Visible = true;
                m_errorMessage.Text = "ERROR: " + ErrorMessages.AnotherUserMadeChangeToLoan;
                m_hasError = true;
                return true;
            }
            else if (errorCode.Equals(ErrorCode_FieldEditPermissionDenied))
            {
                ErrorPanel.Visible = true;
                if (Request["errorMessage"] != null)
                    m_errorMessage.Text = Request["errorMessage"].ToString(); // don't need to use aspxtools here.
                else
                    m_errorMessage.Text = "Modifying loan field denied";
                m_hasError = true;
                return true;
            }
            return false;
        }

		private void LoadNavigationInfoFromQueryString (PmlMainQueryStringManager qsManager)
		{
			if (IsPostBack)
			{
				try 
				{
                    string eventArgument = Request.Form["__EVENTARGUMENT"];
                    // 3/4/2011 dd - eventArgument will have following format.
                    //  {tabIndex}:{key0}={value0}:{key1}={value1}:...
                    string[] parts = eventArgument.Split(':');

					m_currentIndex = int.Parse(parts[0]);

                    for (int i = 1; i < parts.Length; i++)
                    {
                        string[] data = parts[i].Split('=');
                        if (data.Length == 2)
                        {
                            qsManager.AddPair(data[0], data[1]);
                        }
                        else if (data.Length == 1)
                        {
                            // 3/7/2011 dd - Clear value.
                            qsManager.AddPair(data[0], string.Empty);
                        }
                    }
				} 
				catch 
				{
					if (qsManager.Tabkey != string.Empty) 
					{
						m_currentIndex = FindTabByKey(qsManager.Tabkey);
						m_currentIndex = m_currentIndex < 0 ? 0 : m_currentIndex; // 9/8/2004 dd - If key is not found then default to first tab.
					} 
				}
			}
			else
			{
				if (qsManager.Tabkey != string.Empty) 
				{
					m_currentIndex = FindTabByKey( qsManager.Tabkey );
					m_currentIndex = m_currentIndex < 0 ? 0 : m_currentIndex; // 9/8/2004 dd - If key is not found then default to first tab.
					//m_maxVisitedIndex = m_currentIndex;
				}
			}
				
			if ( qsManager.PreviousIndex != string.Empty ) m_previousIndex = int.Parse( qsManager.PreviousIndex );
			if ( qsManager.MaxVisitedIndex != string.Empty ) m_maxVisitedIndex = int.Parse( qsManager.MaxVisitedIndex );
		}

        public void UpdateLoanSummaryDisplay(CPageData dataLoan) 
        {
            sLNm.Text = dataLoan.sLNm;
            aBNm.Text = dataLoan.GetAppData(0).aBNm;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep.Replace(".00", "");
            // 10/8/2004 dd - Temporary Hack to remove .000;
            sLtvR.Text = dataLoan.sLtvR_rep.Replace(".000", "");
            sCltvR.Text = dataLoan.sCltvR_rep.Replace(".000", "");
            sCreditScoreType1.Text = dataLoan.sCreditScoreType1_rep;
            sCreditScoreType2.Text = dataLoan.sCreditScoreType2_rep;

        }


        protected bool m_isLogin 
        {
            get 
            {
                return m_allowGoToPipeline;
                // Determine if user access this step from login screen or through pmlsiteid
                //return RequestHelper.GetSafeQueryString("islogin") == ConstAppDavid.PmlStrangeKey;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {    
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion


    }


	// 02/13/08 mf. OPM 20137 
	// Querystring manager class for Main.aspx.
	public class PmlMainQueryStringManager
	{
        //private string m_loanId; 
        //private string m_isLogin;
        //private string m_tabkey;
        //private string m_previousIndex;
        //private string m_maxVisitedIndex;
        //private string m_additionalValues;
		
		private System.Collections.Specialized.NameValueCollection m_queryString;

		public string LoanId
		{
			get { return GetValue("loanId"); }
			set { SetValue("loanId", value); }
		}
		public string IsLogin
		{
			get { return GetValue("islogin"); }
			set { SetValue("islogin", value); }
		}
		public string Tabkey
		{
			get { return GetValue("tabkey"); }
			set { SetValue("tabkey", value); }
		}
		public string PreviousIndex
		{
			get { return GetValue("previousIndex"); }
			set { SetValue("previousIndex", value); }
		}
		public string MaxVisitedIndex
		{
			get { return GetValue("maxVisitedIndex"); }
			set { SetValue("maxVisitedIndex", value); }
		}

        public string ErrorCode
        {
            get { return GetValue("errorcode"); }
            set { SetValue("errorcode", value); }
        }

        private Dictionary<string, string> m_exportQueryStringList = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

		public PmlMainQueryStringManager( System.Collections.Specialized.NameValueCollection QueryString )
		{
            m_queryString     = QueryString;
            //m_loanId          = GetString( "loanId" );
            //m_isLogin         = GetString( "islogin" );
            //m_tabkey          = GetString( "tabkey" );
            //m_previousIndex   = GetString( "previousIndex" );
            //m_maxVisitedIndex = GetString( "maxVisitedIndex" );
            //ErrorCode = GetString("errorcode");

            string[] requiredFields = { "loanId", "islogin", "tabkey", "previousIndex", "maxVisitedIndex", "errorcode", "SecondLienMPmt", "Source" };
            foreach (var field in requiredFields)
            {
                CopyField(QueryString, field);
            }
		}

        private void CopyField(System.Collections.Specialized.NameValueCollection queryString, string key)
        {
            string value = queryString[key];

            if (string.IsNullOrEmpty(value) == false)
            {
                SetValue(key, value);
            }
        }

		private string GetString( string key )
		{
			if ( m_queryString[ key ] != null )
			{
				return Utilities.SafeHtmlString( m_queryString[ key ] );
			}
			return string.Empty;
		}
        private string GetValue(string key)
        {
            string value;
            if (m_exportQueryStringList.TryGetValue(key, out value) == false)
            {
                value = string.Empty;
            }
            return value;
        }
        private void SetValue(string key, string value)
        {
            m_exportQueryStringList[key] = value;
        }
		public void AddPair( string key, string val )
		{
            m_exportQueryStringList[key] = val;
			//m_additionalValues += "&" + Utilities.SafeHtmlString(key) + "=" + Utilities.SafeHtmlString(val);
		}

		public override string ToString()
		{
            StringBuilder sb = new StringBuilder();

            bool isFirst = true;
            foreach (var kv in m_exportQueryStringList)
            {
                if (string.IsNullOrEmpty(kv.Value))
                {
                    continue;
                }
                if (isFirst == false)
                {
                    sb.Append("&");
                }
                sb.AppendFormat("{0}={1}", kv.Key, System.Web.HttpUtility.UrlEncode(kv.Value));
                isFirst = false;
            }
            return sb.ToString();
            //string str = string.Format("loanId={0}&tabkey={2}&previousIndex={3}&maxVisitedIndex={4}" + m_additionalValues
            //    , m_loanId // 0
            //    , m_isLogin // 1
            //    , m_tabkey // 2
            //    , m_previousIndex // 3
            //    , m_maxVisitedIndex // 4
            //    );

            //if (string.IsNullOrEmpty(ErrorCode) == false)
            //{
            //    str += "&errorcode=" + Utilities.SafeHtmlString(ErrorCode);
            //}
            //return str;
		}
	}

}
