using System;

namespace PriceMyLoan.UI
{
	public interface ITabUserControl
	{
        void LoadData();
        void SaveData();
        string TabID { get; }

	}
}
