using System;
using LendersOffice.CreditReport;
using LendersOffice.Common;
using DataAccess;


namespace PriceMyLoan.UI.Main
{

	public partial class ViewCreditMenu : PriceMyLoan.UI.BasePage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {
            ICreditReportView creditReportView = null;

            Guid dbFileKey = RequestHelper.GetGuid("dbfilekey", Guid.Empty);
            Guid applicationID = RequestHelper.GetGuid("applicationid", Guid.Empty);
            Guid loanID = RequestHelper.GetGuid("loanid", Guid.Empty);
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this, includeSimplePopupDependencies: false);

            if (Guid.Empty != loanID)
            {
                CPageData dataLoan = new CCreditReportViewData(loanID);
                dataLoan.InitLoad();

                CAppData dataApp = dataLoan.GetAppData(applicationID);
                creditReportView = dataApp.CreditReportView.Value;

            }
            else if (dbFileKey != Guid.Empty)
            {
                creditReportView = CAppBase.GetCreditReportViewByFileDbKey(dbFileKey).Value;
            }

            btnViewPdf.Visible = creditReportView != null && creditReportView.HasPdf;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.RegisterJsScript("LQBPrintFix.js");

			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
