<%@ Page language="c#" Codebehind="ViewDUFindingsFrame.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.ViewDUFindingsFrame" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>DU Findings</title>
  </head>
  <frameset rows="50,*" frameborder=yes>
    <frame name=menu src=<%= AspxTools.SafeUrl(m_menuUrl) %> scrolling=no>
    <frame name=body src=<%= AspxTools.SafeUrl(m_bodyUrl) %>>
  </frameset>
</html>
