<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GetResults.ascx.cs" Inherits="PriceMyLoan.UI.Main.GetResults" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import namespace="PriceMyLoan.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<style type="text/css">
a.ineligibleLoanProgramsLink
{
    color:#ff9933;    
}
a.ineligibleLoanProgramsLink:hover
{
    color:blue;    
}
</style>
<script language="javascript" type="text/javascript">
<!--
  $(function()
  {
      function f_msg($anchor, x, y) {   
        var message = 'Rate is disqualified because ' + $anchor.attr('data-msg');
        var links = '';
        
        <% if(m_showRateForIneligible) { %>
            var arg1 = $anchor.attr('data-arg1'),
                arg2 = $anchor.attr('data-arg2'),
                arg3 = $anchor.attr('data-arg3'),
                arg4 = $anchor.attr('data-arg4'),
                func = $anchor.attr('data-func'), 
                container = $('<div>');
                container.append($('<span>').html('&nbsp;'));
                if (arg4 != null && arg4.indexOf('register loan') >= 0 ){ 
                        var isDisabled = arg4.indexOf('register loan?') >= 0;        
                        if (isDisabled){
                            container.append("&nbsp;&nbsp;<a href='#' onclick='return false;' disabled='disabled'>register</a>&nbsp;<a href='#' onclick='return f_SRM();'>?</a>");
                        }
                        else {
                        $('<a>', {
                            'data-func' : func,
                            'data-arg1' : arg1,
                            'data-arg2' : arg2,
                            'data-arg3' : arg3,
                            'href' : '#'
                            }).addClass('disqual_register').text('register loan').appendTo(container);
                    }
                }
                if( arg4 != null && arg4.indexOf('lock rate') >= 0 )
                {
                    container.append($('<span>').html('&nbsp;'));
                    $('<a>', {
                        'data-func' : func,
                        'data-arg1' : arg1,
                        'data-arg2' : arg2,
                        'data-arg3' : arg3,
                        'href' : '#'
                    }).addClass('disqual_lock').text('lock rate').appendTo(container);
                }
                links = container.html();
        <% } %>
        
        
        var oPanel = document.getElementById('RateWarningDiv');
            oPanel.style.display = 'block';
            oPanel.style.width= '250px';
            oPanel.style.top = y +'px';
            oPanel.style.left = (x+50) + 'px';
            oPanel.innerHTML = '<p>' + message + "</p><br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' />" + links +"</span>";
        return false; 
    }
    
    $('a.disqual_lock,a.disqual_register').on('click', function(e){
        var $a = $(this), 
            funcstr = $a.attr('data-func'),
            arg1 = $a.attr('data-arg1'),
            arg2 = $a.attr('data-arg2'),
            arg3 = $a.attr('data-arg3'),
            isRateLock = $a.hasClass('disqual_lock'); 
            
            if (funcstr == 'f01') {
                return f01(arg1,arg2,arg3, isRateLock);
            }
            else {
                return f_submitRateMerge(arg1, arg2, isRateLock);
            }
        
        
    });
    $('a.displayMsgLink').on('click', function(e){
        var anchor = $(this);
        f_displayMsg(anchor, e.pageX, e.pageY, 80);
        return false;
    });
    $('a.unavailable').on('click', function(e){
        var anchor = $(this);
        f_msg(anchor, e.pageX, e.pageY);
        return false;
    });
    // Init
    $(document).keydown(f_quickDebug);
    $('.ineligibleLoanProgramsLink').click(toggleIneligibleLoanPrograms);
        <% if (!m_eligibleLoanProgramsExist){%>         
            $('.ineligibleLoanProgramsDisplayText').hide();        
        <%} else {%>
            $('.ineligibleLoanProgramsHideText').hide(); 
            $('.ineligibleLoanProgramsTD').hide();
        <%} %>
      });
  function toggleIneligibleLoanPrograms()
  {
    $('.ineligibleLoanProgramsHideText').toggle();
    $('.ineligibleLoanProgramsDisplayText').toggle();
    $('.ineligibleLoanProgramsTD').toggle();
  }
  var g_time0 = (new Date()).getTime();
  var gVerHash = <%= AspxTools.JsArray(m_versionList) %>;
  
  var g_iResultTimeout = <%= AspxTools.JsNumeric(ConstAppDavid.LPE_NumberOfMinutesBeforeMessageExpire) %> * 60000 + 10000; <%-- // Plus extra 10 seconds --%>
  var g_sUpdateMessage = <%= AspxTools.JsString(JsMessages.LpeResultsTimeout) %>;
  var g_renderResultDuration = <%= AspxTools.JsNumeric(m_renderResultDuration) %>;
  var g_loanID = <%= AspxTools.JsString(LoanID) %>;
  var g_sensitiveData = <%= AspxTools.JsString(m_sensitiveData) %>;  
  var g_confirmTitle = <%= AspxTools.JsString(m_confirmationTitle) %>;
  var g_renderResultModeT = <%= AspxTools.JsString(RenderResultModeT.ToString("D")) %>;
  var g_debugDetail = <%= AspxTools.JsString(m_debugDetail) %>;


  function f_getCurrentGData() {
    <% if (m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) { %>
    return gData0;
    <% } else { %>
    return gData1;
    <% } %>
  }
  
  function f_displayMsg($anchor, x, y, width) {   
        var oPanel = document.getElementById('RateWarningDiv');
            oPanel.style.display = 'block';
            oPanel.style.width= width +'px';
            oPanel.style.top = y +'px';
            oPanel.style.left = (x+50) + 'px';
            oPanel.innerHTML = '<p>' + $anchor.attr('displayMsg') + "</p><br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' /></span>";
        return false; 
    }
  function f_calcTiming() {
    var o = new Object();
    
    var str = document.forms[0]['JsScriptStats'].value.split(':');
    o.numOfProducts = str[1];
    o.pollingCount = str[3];
    o.calcServerCount = <%= AspxTools.JsNumeric(m_calcServerCount) %>; 
    o.batchCreated = <%= AspxTools.JsString(m_batchCreatedTime) %>;
      <%-- // Server Time --%>
    o.s0 = parseFloat(str[0]) / 1000; <%-- Calculate Pricing Time = Server Processing + Wait --%>
    o.s1 = parseFloat(g_renderResultDuration) / 1000;      
    o.s2 = parseFloat(<%= AspxTools.JsNumeric(m_calcServerDuration) %>) / 1000;
    o.s5 = parseFloat(str[4]) / 1000; <%-- Time spend checkin IsResultAvailable --%>
    o.s4 = parseFloat(str[2]) / 1000; <%-- Time spend retrieve loan products to perform pricing --%>
    o.renderResultDebug = <%= AspxTools.JsString(m_renderResultDurationDetail) %>;
     
    o.s3 = o.s0 - o.s2 - o.s4 -o.s5;
      
      <%-- // Client Time --%>
    o.c0 = (g_endRenderPage - g_startRenderPage) / 1000;
    o.c1 = g_iDownloadTime - o.c0; <%-- // Only download time. --%>
    
    o.debugClient0 = (g_time0 - g_startRenderPage) / 1000;
    o.debugClient1 = (g_time1 - g_time0) / 1000;
    o.debugClient2 = (g_time2 - g_time1) / 1000;
    o.debugClient3 = (g_endRenderPage - g_time2) / 1000;
    o.total = o.s0 + o.s1 + g_iDownloadTime;
      
    o.percent_server = (o.s0 + o.s1) / o.total * 100;
    o.percent_client = g_iDownloadTime / o.total * 100;
    
    return o;
  }
  
  function f_qualify2ndLoan(productID, rate, point, payment, rawId, version, blockSubmit, blockSubmitMsg, uniqueChecksum) {
	if( !f_allowClick() ) return false;
    _popupWindow = window.open( gVirtualRoot + "/main/SecondResults.aspx?loanid=" + g_loanID + "&cmd=Run2nd&1stProductID=" + productID + "&1stRate=" + rate + "&1stPoint=" + point + "&1stMPmt=" + payment + "&rawId=" + rawId + "&version=" + version + '&debugResultStartTicks=' + <%= AspxTools.JsNumeric(DateTime.Now.Ticks) %> + '&1stBlockSubmit=' + blockSubmit + '&1stBlockSubmitMsg=' + escape(blockSubmitMsg) + '&1stUniqueChecksum=' + uniqueChecksum, 
    "Result",
    'width=750px, height=550px,  resizable=yes, scrollbars=yes, status=yes, help=no, centerscreen=yes,');
  }
  function f_submitManually() {
    <% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.OtherLoan) { %>
      f_submitManual2nd();
      return false;
    <% } %>
    if (!f_allowClick()) return false;
      <% if (m_RegisterAndRateLockBlockingMessage != String.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
      <%} else if (UnableToSubmitWithoutCredit) { %>
        alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
        return false;
      <% } else { %>
          
    var args = new Object();
    args["loanid"] = g_loanID;
    args["submitmanually"] = "True";
    args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
    
    var queryString = "";      
    for (var o in args) {
      queryString += o + "=" + escape(args[o]) +"&";
    }
    _popupWindow = window.open(gVirtualRoot + "/Main/ConfirmationPage.aspx?" + queryString,  
        g_confirmTitle.replace(' ', '_') ,
        'width=200px, height=200px, centerscreen=yes, resizable=yes, scrollbars=yes, status=yes, help=no');
    return false; 
    <% } %>
  }  
  function f_submitManual2nd() {
    if (!f_allowClick()) return false;
      <% if (m_RegisterAndRateLockBlockingMessage != String.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
      <% } else if (UnableToSubmitWithoutCredit) { %>
        alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
        return false;
      <% } else { %>
      
      var args = new Object();
      args["loanid"] = g_loanID;
      args["manual2nd"] = "True";
      args["Has2ndLoan"] = "True";
      args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;      
      args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
      args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
      args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;
      args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;
      args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
      args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;      
      args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
      args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
      var queryString = "";      
      for (var o in args) {
        queryString += o + "=" + escape(args[o]) +"&";
      }
      _popupWindow = window.open(gVirtualRoot+ "/Main/ConfirmationPage.aspx?" + queryString, g_confirmTitle.replace(' ', '_'),'' );
      return false;        
      <% } %>
  }

    <%-- This function display the certificate. Shorten the function name to save space. printLoanProduct--%>
    function f00(currentIndex) {
      
	    if( !f_allowClick() ) return false;
	    var gData = f_getCurrentGData();
	    var productData = gData[currentIndex][0];
	    var productid = productData[0];
	    
	    
	    var extraParms = "";
	    
	    if( <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %> == 1 )
	    {
	    
	      extraParms = [ "&FirstLienLpId="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>,     
	                     "FirstLienNoteRate=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>, 
	                     "FirstLienPoint="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>, 
                         "FirstLienMPmt=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>].join("&");
        } 
	    
	    var version = gVerHash[productData[2]];
	    var uniqueChecksum = productData[6];
	    
       _popupWindow = window.open(gVirtualRoot+ '/Main/QualifiedLoanProgramPrintFrame.aspx?loanid=' + g_loanID + '&productid=' + productid + '&lienqualifymodet=' + <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %> + '&version=' + version + '&uniquechecksum=' + uniqueChecksum + extraParms,
      'Preview_Certificate', 'width=880px,height=580px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no' );
      return false;
    }

    <%-- This function perform actual submission. Shorten the function name to save space. Old name is submitRateOption --%>
function f01(rawId, hashId, currentIndex, bIsRateLock) {
  
  if( !f_allowClick() ) return false;
  
  <%if(m_HasDuplicate){ %>
        f_displayHasDuplicate();
        return false;
  <%} %>
  
  var bIsRateLock = !!bIsRateLock;
  var productData = null;
  var gData = null;
  if (hashId.match(/_e0|_i0/)) 
      gData = gData0[currentIndex];
  else
      gData = gData1[currentIndex];
      
  var productData = gData[0];
  var productID = productData[0];
  var productName = productData[1];
  var bIsBlockedRateLockSubmission = productData[3] + "";
  var uniqueChecksum = productData[6];
  var rateOptionList = gData[1];
  var rate = '';
  var point = '';
  var apr = '';
  var margin = '';
  var payment = '';
  var bottom = '';
  var teaserRate = '';
  var qrate = '';
  var qualPmt = '';
  
  for (var i = 0; i < rateOptionList.length; i++) {
    var rateOption = rateOptionList[i];
    if (rateOption[7] == rawId) {
      rate = rateOption[0];
      point = rateOption[1];
      apr = rateOption[2];
      margin = rateOption[3];
      payment = rateOption[4];
      bottom = rateOption[6];
      teaserRate = rateOption[8];
      qrate = rateOption[9];
      qualPmt = rateOption[10];
      break;
    }
  }
  
  <% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) { %>
    f_qualify2ndLoan(productID, rate, point, payment, rawId, gVerHash[productData[2]], bIsBlockedRateLockSubmission, productData[4], uniqueChecksum );
  <% } else if (m_hasLinkedLoan && false ) {  // Allow user pick first loan, but alert when they try to submit both loans.%> 
    alert (<%= AspxTools.JsString(JsMessages.LpeUnableToSubmitLoanProgram) %>);
    return false;
  <% } else if (m_RegisterAndRateLockBlockingMessage != String.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
  <% } else if (UnableToSubmitWithoutCredit) { %>
    alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
    return false;
  <% } else { %>
  
  var args = new Object();
  args["loanid"] = g_loanID;
  args["productid"] = productID;
  args["rate"] = rate;
  args["point"] = point;
  args["apr"] = apr;
  args["margin"] = margin;
  args["payment"] = payment;
  args["bottom"] = bottom;
  args["productName"] = productName;
  args["teaserrate"] = teaserRate;
  args["qrate"] = qrate;
  args["qualPmt"] = qualPmt;
  args["Has2ndLoan"] = <%= AspxTools.JsString(m_has2ndLoan.ToString()) %>;
  args["RawId"] = rawId;
  args["Version"] = gVerHash[productData[2]];
  args["uniqueChecksum"] = uniqueChecksum;
  args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;
  args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
  args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
  args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;
  args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;
  args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
  args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;
  args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
  args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
  args["blocksubmit"] = bIsBlockedRateLockSubmission;
  args["blocksubmitmsg"] = productData[4];
  args["requestratelock"] = bIsRateLock ? "t" : "f";  
  var queryString = "";
  for (var o in args) {
    queryString += o + "=" + escape(args[o]) +"&";
  }
  <% if (!m_isCitiDemo) { %>
    <% if (lpeRunModeT ==  E_LpeRunModeT.OriginalProductOnly_Direct) { %>
      var expiredRateDialog = gVirtualRoot + "/Main/LockUnavailable.aspx";
      var expiredDialogTitle = "Lock Unavailable";
    <% } else { %>
      var expiredRateDialog = gVirtualRoot +"/Main/ConfirmationPage.aspx";
      var expiredDialogTitle = g_confirmTitle;
    <% } %>
    var dialogUrl = bIsBlockedRateLockSubmission == "0" ? gVirtualRoot + "/Main/ConfirmationPage.aspx" : expiredRateDialog ;
    var winTitle = bIsBlockedRateLockSubmission == "0" ? g_confirmTitle : expiredDialogTitle;
    winTitle = winTitle.replace(' ', '_');
  _popupWindow = window.open( dialogUrl+"?" + queryString,winTitle ,'width=500px,height=300px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no');
  <% } else { %> 
  window.open(<%= AspxTools.JsString(VirtualRoot) %> + '/Main/CitiDemo.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>, "Confirmation", "toolbar=no,status=no");
  <% } %>
  return false;
  <% } %>
}

    
function submitSkip2nd() {
  if( !f_allowClick() ) return false;
  
  <%if(m_HasDuplicate){ %>
        f_displayHasDuplicate();
        return false;
  <%} %>
  <% if (m_RegisterAndRateLockBlockingMessage != String.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
  <% } else if (UnableToSubmitWithoutCredit) { %>
    alert( <%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
    return false;
  <% } else { %>

  var args = new Object();
  args["loanid"] = g_loanID;
  args["skip2nd"] = "True";
  args["_1stVersion"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("version")) %>;
  args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;  
  args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
  args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;   
  args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;   
  args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;   
  args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
  args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;  
  args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
  args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
  
  var queryString = "";      
  for (var o in args) {
    queryString += o + "=" + escape(args[o]) +"&";
  }
  _popupWindow = window.open(gVirtualRoot + "/Main/ConfirmationPage.aspx?" + queryString, g_confirmTitle.replace(' ', '_'),'width=200px, height=200px, centerscreen=yes, resizable=yes, scrollbars=yes, status=yes, help=no' );
  return false;      
  <% } %>
}

if (!window.console) { window.console = { log : function() {} }; };

<% if (m_isWaiting) { %>
function f_uc_init() {
  // First page load
  console.log('Init0');
  oEventArg = _currentIndex;
  f_displayWaiting();
  var extraArgs = {
    SecondLienMPmt: <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("SecondLienMPmt")) %>
  };
  f_Underwriting("0", extraArgs); <%-- //sLienQualifyModeT = E_sLienQUalifyModeT.ThisLoan --%>
  console.log('Finished with Init0');
}
<% } else  { %>
  function f_uc_init() {
    console.log('Init1');
    // Processing your request...
    f_hideNavigation();
    setTimeout('f_displayResultImpl()', 200);
    console.log('Finished with Init1');
  }
<% } %>

function f_displayResultImpl() {
    f_captureTime(); // Performance measurement
    f_displayResult(); // Show the rate merge table
    if (typeof(f_displayNavigation) != 'undefined')
      f_displayNavigation();
}
function f_captureTime() {
  console.log('Capture timing results');
  var args = new Object();
  args["InitialTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
  g_iStartCapture = (new Date()).getTime();
  var result = gService.main.call("PingTime", args);
  if (!result.error) {
    g_iDownloadTime = parseFloat(result.value["duration"]) / 1000;
  }
  g_iEndCapture = (new Date()).getTime();
  var o = f_calcTiming();

  if (o.total > <%= AspxTools.JsNumeric(ConstStage.LpeTimingEmailThresholdInSeconds) %>) {
    f_emailTimingImpl('Automate timing result.');
  }
  
  console.log('Finished with timing results');
}
  function f_displayResult() {
      if (typeof(Storage) !== "undefined")
      {
          sessionStorage["ClientPriceTiming_RenderResultStart"] = (new Date()).getTime();;
      }
  <% if (m_isBestPrice) { %>
    console.log('Render rate merge table');
    f_renderRateMergeTable(<%= AspxTools.JsString(m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "0" : "1") %>);
    console.log('Finished with rendering rate merge table');
  <% } %>
  <% if (!m_hasError) { %>
    f_hideWaiting();
    document.getElementById("MainPanel").style.display = "";    
      <% } %>

    if (typeof(Storage) !== "undefined")
    {
        sessionStorage["ClientPriceTiming_RenderResultEnd"] = (new Date()).getTime();
 
        sessionStorage["ClientPriceTiming_End"] = (new Date()).getTime();

            var args = {};
            args.LoanID = g_loanID;
            var propertyList = ["ClientPriceTiming_Start", "ClientPriceTiming_RenderResultStart", "ClientPriceTiming_RenderResultEnd", "ClientPriceTiming_End", "ClientPriceTiming_RequestId"];

            for (var i = 0; i < propertyList.length; i++)
            {
                args[propertyList[i]] = sessionStorage[propertyList[i]];
            }
                    
            gService.utils.call("RecordClientTiming", args);

    }
}
function f_reportTimeout() {
  var args = new Object();
  args["LoanID"] = g_loanID;
  args["ProductCount"] = g_sNumberOfProducts;
  args["RequestID"] = g_sRequestID;
  args["ResultPollingCount"] = g_iResultPollingCount + '';
  args["StartedTime"] = <%= AspxTools.JsString(Tools.GetDBCurrentDateTime().ToString()) %>;
  
  gService.main.call("GetResult_ReportTimeout", args);
}
  var g_time1 = (new Date()).getTime();

function f_displayHasDuplicate()
{
    LQBPopup.ShowElement(
          $('#divForDuplicatePmlLoanSubmissions')
        , {
               'height': 130 // resize the window to match the size of the content
             , 'hideCloseButton': true
          }
        );
}

<% if (m_isBestPrice) { %>
function f_submitRateMerge(groupIndex, rateIndex, bIsRateLock) {

  if( !f_allowClick() ) return false;
  
  <%if(m_HasDuplicate){ %>
        f_displayHasDuplicate();
        return false;
  <%} %>
  
  var bIsRateLock = !!bIsRateLock; // Do this to handle case where bIsRateLock is null.
  var gRateOptionList = f_getRateOptionList(<%= AspxTools.JsNumeric(m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? 0 : 1) %>);
  var gLpList = f_getLpList(<%= AspxTools.JsNumeric(m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? 0 : 1) %>);
  
  var rateOption = gRateOptionList[groupIndex][2][rateIndex]
  	    
	var productIndex = rateOption[12];
	var productData = gLpList[productIndex];
  
  var productID = productData[0];
  var productName = productData[1];
  var uniqueChecksum = productData[7];
  var bIsBlockedRateLockSubmission = productData[4] + "";
  
  var rate = rateOption[0];
  var point = rateOption[1];
  var apr = rateOption[2];
  var margin = rateOption[3];
  var payment = rateOption[4];
  var bottom = rateOption[6];
  var teaserRate = rateOption[8];
  var qrate = rateOption[9];
  var qualPmt = rateOption[10];
  var rawId = rateOption[7];
  
  
  <% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) { %>
    f_qualify2ndLoan(productID, rate, point, payment, rawId, gVerHash[productData[2]], bIsBlockedRateLockSubmission, productData[5], uniqueChecksum );
  <% } else if (m_hasLinkedLoan && false) {  // Allow user pick first loan, but alert when they try to submit both loans.%> 
    alert (<%= AspxTools.JsString(JsMessages.LpeUnableToSubmitLoanProgram) %>);
    return false;
  <% } else if (m_RegisterAndRateLockBlockingMessage != String.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
  <% } else if (UnableToSubmitWithoutCredit) { %>
    alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
    return false;
  <% } else { %>
  
  var args = new Object();
  args["loanid"] = g_loanID;
  args["productid"] = productID;
  args["rate"] = rate;
  args["point"] = point;
  args["apr"] = apr;
  args["margin"] = margin;
  args["payment"] = payment;
  args["bottom"] = bottom;
  args["productName"] = productName;
  args["uniqueChecksum"] = uniqueChecksum;  
  args["teaserrate"] = teaserRate;
  args["qrate"] = qrate;
  args["qualPmt"] = qualPmt;
  args["Has2ndLoan"] = <%= AspxTools.JsString(m_has2ndLoan.ToString()) %>;
  args["RawId"] = rawId;
  args["Version"] = gVerHash[productData[2]];
  args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
  args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
  args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;
  args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;    
  args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;
  args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
  args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;
  args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
  args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
  args["blocksubmit"] = bIsBlockedRateLockSubmission;
  args["blocksubmitmsg"] = productData[5];
  args["requestratelock"] = bIsRateLock ? "t" : "f";
  
  var queryString = "";
  for (var o in args) {
    queryString += o + "=" + escape(args[o]) +"&";
  }
      <% if (lpeRunModeT ==  E_LpeRunModeT.OriginalProductOnly_Direct) { %>
      var expiredRateDialog = gVirtualRoot +"/Main/LockUnavailable.aspx";
      var expiredDialogTitle = "Lock Unavailable";
    <% } else { %>
      var expiredRateDialog = gVirtualRoot +"/Main/ConfirmationPage.aspx";
      var expiredDialogTitle = g_confirmTitle;
    <% } %>
    var dialogUrl = bIsBlockedRateLockSubmission == "0" ? gVirtualRoot +"/Main/ConfirmationPage.aspx" : expiredRateDialog ;
    var winTitle = bIsBlockedRateLockSubmission == "0" ? g_confirmTitle : expiredDialogTitle;
    winTitle = winTitle.replace(' ', '_');
  _popupWindow = window.open( dialogUrl+"?" + queryString, winTitle,'width=500px,height=300px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no');

  return false;
  <% } %>
}

function f_previewRateMerge(groupIndex, rateIndex) {
      
	    if( !f_allowClick() ) return false;
	    
      var gRateOptionList = f_getRateOptionList(<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>);
      var gLpList = f_getLpList(<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>);
  
  	    
	    var productIndex = gRateOptionList[groupIndex][2][rateIndex][12];
	    var productData = gLpList[productIndex];
	    var productid= productData[0];
      var uniqueChecksum = productData[7];	    
	    
	    var extraParms = '';
	    if( <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %> == 1 )
	    {
	    
	      extraParms = [ "&FirstLienLpId="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>,     
	                     "FirstLienNoteRate=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>, 
	                     "FirstLienPoint="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>, 
                         "FirstLienMPmt=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>].join("&");
        } 
	    
	    var version = gVerHash[productData[2]];
      _popupWindow = window.open(gVirtualRoot +'/Main/QualifiedLoanProgramPrintFrame.aspx?loanid=' + g_loanID + '&productid=' + productid + '&lienqualifymodet=' + <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %> + '&version=' + version + '&uniqueChecksum=' + uniqueChecksum + extraParms,
      'Preview_Certificate','width=880px,height=580px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no' );
      return false;
}
<% } %>
//-->
</script>

<div id="divForDuplicatePmlLoanSubmissions" style="display:none">
    <div style="padding-top: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
        <div style="color:Red;text-align:left">
          *A loan file with duplicate borrower and property information has already been received.
          Submission for registration/lock of this loan file is prohibited.
         </div>
         <br />
         <div style="text-align:left">
          Please contact your lock desk if you have any questions regarding this alert message.
         </div>
         <br />
         <div>
            <input type="button" value="Close" onclick='LQBPopup.Hide()'/>
         </div>
     </div>
</div>

<table width="100%" height="400" border="0" id="WaitTable">
	<tr>
		<td align="center" valign="middle">
      <div class="loading-content">
          <img  id="Img1" class="img-loading" src="~/images/loading.gif" alt="Loading..." runat="server"/>
          <h3 class="text-loading"><%= AspxTools.HtmlString(m_waitingMessage) %></h3>
      </div>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" style="PADDING-LEFT: 5px">
<span id=MainPanel style="display: none">
<asp:Panel ID="MainResultPanel" Runat=server EnableViewState=False>
<script type="text/javascript">
<% if (m_isBestPrice) { %>
<%-- This function display the certificate. Shorten the function name to save space. printLoanProduct--%>
function f_getLpList(qualMode) {
  if (qualMode == 0) {
    return gLpList0;
  } else {
    return gLpList1;
  }
}
function f_getRateOptionList(qualMode) {
    if (qualMode == 0) {
      if (typeof(gRateOptionList0) != 'undefined')
        return gRateOptionList0;
    } else {
      if (typeof(gRateOptionList1) != 'undefined')
        return gRateOptionList1;
    }
    return null;
}  

function f_getLinkTitle<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>(bIsBlockedRateLockSubmission) {
  var titles = <%= AspxTools.JsArray(GetLinkTitles()) %>;
  if (bIsBlockedRateLockSubmission) {
    return titles[1];
  } else {
    return titles[0];
  }
}
function f_renderRateMergeTable(qualMode) {
  var $oTable = $('#RateMergeEligibleTable' + qualMode);
  
  var gRateOptionList = f_getRateOptionList(qualMode);
  
  if (null == gRateOptionList) return;
  var gLpList = f_getLpList(qualMode);
  var iLpCount = gRateOptionList.length;
  
  var numOfCols = <%= AspxTools.JsNumeric(13 - ( m_sProdDocT_NoIncome ? 3 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)) %>;
  for (var i = 0; i < iLpCount; i++) {
    var cssName = 'ElAltBg';
    var oLp = gRateOptionList[i];
    var $oTBody = $('<tbody/>').addClass(cssName).appendTo($oTable); //document.createElement("tbody");
    //oTBody.className = cssName;
    
    //oTable.appendChild(oTBody);
    var $oRow = null;
    var $oCell = null;
    
    if (i != 0) {
      $oRow = $('<tr/>').appendTo($oTBody); //.insertRow(-1);
      $oCell = $('<td/>').prop('colSpan', numOfCols).html('<hr />').appendTo($oRow);
      //oCell = oRow.insertCell(-1);
      //oCell.colSpan = numOfCols;
      //oCell.innerHTML = "<hr>";
    }
    $oRow = $('<tr/>').appendTo($oTBody);
    //oRow = oTBody.insertRow(-1);
    $oCell = $('<td/>').addClass('MergeGroupName').html(oLp[0]).appendTo($oRow);
    $oCell.css({'padding-right':'2px'}).prop('noWrap', true).prop('colSpan', 4);
    //oCell = oRow.insertCell(-1);
    //oCell.className='MergeGroupName';
    //oCell.innerHTML = "<span class='MergeGroupName'>" + oLp[0] + "</span>";
    //oCell.innerHTML = oLp[0];
    //oCell.style.paddingRight = '2px';
    //oCell.noWrap = true;
    //oCell.colSpan = 4;
    
    var $oMoreTBody = $('<tbody/>').addClass(cssName).prop('id', 'rmM_' + qualMode + '_' + i).appendTo($oTable);//.createElement("tbody");
    //oMoreTBody.id = 'rmM_' +qualMode + '_' + i;
    //oMoreTBody.style.display = ''; // Default to view more.
    //oMoreTBody.className = cssName;
    //oTable.appendChild(oMoreTBody);

    var oRateOptionList = oLp[2];
    var rowCssName = 'ElBg';
    var previousRate = '';
    for (var j = 0; j < oRateOptionList.length; j++) {
      var oRateOption = oRateOptionList[j];
      if (oRateOption[0] != previousRate) {
        rowCssName = rowCssName == 'ElAltBg' ? 'ElBg' : 'ElAltBg';
      }  
      
      var rateExpiredCssName = 'ri ';
      var bIsBlockedRateLockSubmission = gLpList[oRateOption[12]][4] == 1;
      var bAreRatesExpired = gLpList[oRateOption[12]][6] == 1;
      //if (i == 0) bIsBlockedRateLockSubmission = true;
      if (bAreRatesExpired) {
        rateExpiredCssName = 'rie ';
      }    
      var linkTitle =  qualMode == 0 ? f_getLinkTitle0(bIsBlockedRateLockSubmission) : f_getLinkTitle1(bIsBlockedRateLockSubmission);
      var violateDtiCss = oRateOption[13] == 1 ? ' ViolateDti' : '';
      var violateDtiNotation = oRateOption[13] == 1 ? '** ' : '';           
      previousRate = oRateOption[0];
      //oRow = oMoreTBody.insertRow(-1);
      $oRow = $('<tr/>').appendTo($oMoreTBody);
      f_insertCellSubmit($oRow, linkTitle, i, j, rowCssName, qualMode, oRateOption[15], oRateOption[16]);
      <% if ( m_isOptionARMUsedInPml ) { %>
        f_insertCellRateMerge($oRow, oRateOption[8], rateExpiredCssName + rowCssName); <%-- // TeaserRate; --%>
      <% } %>
      f_insertCellRateMerge($oRow, oRateOption[0], rateExpiredCssName + rowCssName); <%-- // Rate; --%>
      f_insertCellRateMerge($oRow, oRateOption[11], rateExpiredCssName + rowCssName); <%-- // Point display in result; --%>
      f_insertCellRateMerge($oRow, oRateOption[4], rateExpiredCssName + rowCssName); <%-- // FirstPmtAmt; --%>
      <% if (m_sProdDocT_NoIncome) { %>
        f_insertCellRateMerge($oRow, oRateOption[3], rateExpiredCssName + rowCssName); <%-- // Margin; --%>
      <% } else { %>
        <% if ( !HideQRate ) { %>
        var qrate = oRateOption[9] == '' ? oRateOption[0] : oRateOption[9];
        <% } else { %>
        var qrate = oRateOption[9];
        <% }  %>        
        
        f_insertCellRateMerge($oRow, qrate, rateExpiredCssName + rowCssName); <%-- // QRate; --%>
        f_insertCellRateMerge($oRow, oRateOption[3], rateExpiredCssName + rowCssName); <%-- // Margin; --%>
        f_insertCellRateMerge($oRow, violateDtiNotation + oRateOption[6], rateExpiredCssName + rowCssName + violateDtiCss); <%-- // Bottom Ratio; --%>
      f_insertCellRateMerge($oRow, gLpList[oRateOption[12]][3], rateExpiredCssName + rowCssName + violateDtiCss); // MAX DTI
    
      <% } %>

      f_insertCellRateMerge($oRow, gLpList[oRateOption[12]][1], 'ri1 ' + rowCssName); // LP Name
      
    }
    var $oLessTBody = $('<tbody/>').prop('id', 'rML_' + qualMode + '_' + i).addClass(cssName).css({'display':'none'}).appendTo($oTable); //document.createElement("tbody");
    //oLessTBody.id = 'rmL_' + qualMode + '_' + i;
    //oLessTBody.style.display = 'none'; // Hide par rate.
    //oLessTBody.className = cssName;
    //oTable.appendChild(oLessTBody);
    

      var oRateOption = oRateOptionList[oLp[1]];
      
      var rateExpiredCssName = 'ri ';
      var bIsBlockedRateLockSubmission = gLpList[oRateOption[12]][4] == 1;
      var bAreRatesExpired = gLpList[oRateOption[12]][6] == 1;

      if (bAreRatesExpired) {
        rateExpiredCssName = 'rie ';
      }    
      var linkTitle =  qualMode == 0 ? f_getLinkTitle0(bIsBlockedRateLockSubmission) : f_getLinkTitle1(bIsBlockedRateLockSubmission);
      var violateDtiCss = oRateOption[13] == 1 ? ' ViolateDti' : '';      
      var violateDtiNotation = oRateOption[13] == 1 ? '** ' : '';      
      $oRow = $('<tr/>').appendTo($oLessTBody);
      f_insertCellSubmit($oRow, linkTitle, i, oLp[1], '', qualMode, oRateOption[15], oRateOption[16]);

      <% if ( m_isOptionARMUsedInPml ) { %>
        f_insertCellRateMerge($oRow, oRateOption[8], 'ri'); <%-- // TeaserRate; --%>
      <% } %>
      f_insertCellRateMerge($oRow, oRateOption[0], rateExpiredCssName); <%-- // Rate; --%>
      f_insertCellRateMerge($oRow, oRateOption[11], rateExpiredCssName); <%-- // Point display in result; --%>
      f_insertCellRateMerge($oRow, oRateOption[4], 'ri'); <%-- // FirstPmtAmt; --%>
      <% if (m_sProdDocT_NoIncome) { %>
        f_insertCellRateMerge($oRow, oRateOption[3], 'ri'); <%-- // Margin; --%>
      <% } else { %>
        var qrate = oRateOption[9] == '' ? oRateOption[0] : oRateOption[9];

        f_insertCellRateMerge($oRow, qrate, 'ri'); <%-- // QRate; --%>
        f_insertCellRateMerge($oRow, oRateOption[3], 'ri'); <%-- // Margin; --%>
        f_insertCellRateMerge($oRow, violateDtiNotation + oRateOption[6], 'ri' + violateDtiCss); <%-- // Bottom Ratio; --%>
      f_insertCellRateMerge($oRow, gLpList[oRateOption[12]][3], 'ri ' + violateDtiCss); // MAX DTI
    
      <% } %>
      
      f_insertCellRateMerge($oRow, gLpList[oRateOption[12]][1], 'ri1'); // LP Name
      
  }
}

function f_insertCellSubmit(oRow, linkTitle, groupIndex, rateIndex, cssName, qualMode, disqualified, reason) {
  
  // OPM 103212 - This cell was formerly used for Merit Matrix integration. GF
  var $oCell = $('<td />').addClass(cssName).appendTo(oRow);//.insertCell(-1);
  $oCell.css({'text-align':'right','padding-right':'3px'}).prop('noWrap',true);
  
  
  $oCell = $('<td/>').addClass(cssName).appendTo(oRow); //.insertCell(-1);
  $oCell.css({'text-align':'right','padding-right':'3px'}).prop('noWrap',true);
  $oCell.html("<a href='#' onclick='return f_previewRateMerge(" + groupIndex + "," + rateIndex + ");'>preview</a>");
  
  if( disqualified == 1 ) 
  {
        $oCell = $('<td colspan="2" />').addClass(cssName).appendTo(oRow); 
        $oCell.css({'text-align':'right','padding-right':'3px', 'padding-left':'3px', 'border-left':'1px solid #999999'}).prop('noWrap',true);
        var escaped = $('<div/>').text(reason).html(); 
        //$oCell.html("<a href='#'  class='unavailable' data-msg='"+ escaped  +"'>unavailable</a>");
        $oCell.html($('<div>').append($('<a>').prop('href' , '#').addClass('unavailable').attr({
            'data-msg' : escaped,
            'data-func' : 'f_submitRateMerge',
            'data-arg1' : groupIndex,
            'data-msg' : escaped,
            'data-arg4' : linkTitle,
            'data-arg2' : rateIndex }).text('unavailable')).html());
        return;
  }

  $oCell = $('<td />').addClass(cssName).appendTo(oRow); //.insertCell(-1);
  $oCell.css({'text-align':'right','padding-right':'3px','padding-left':'3px', 'border-left':'1px solid #999999'}).prop('noWrap', true);
  
  //oCell.style.textAlign='right';
  //oCell.style.paddingRight = '3px';
  //oCell.noWrap = true;
  //oCell.className = cssName;
  //oCell.style.borderLeft = '1px solid #999999';
  if (linkTitle != null && linkTitle != "") {
  <%-- 7/28/2010 dd - OPM 42047 --%>
    var parts = linkTitle.split('/');
    var firstLink = parts[0];
    if (firstLink == '?')
    {
        $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>Lock</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>");
    }
    else if( parts[0] == "register loan?"){
	  $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' onclick='return false;' disabled='disabled'>register</a>&nbsp;<a href='#' onclick='return f_SRM();'>?</a>");
    }
    else 
    {
      var isRateLock = firstLink == "lock rate";
	  $oCell.html("<a href='#' onclick='return f_submitRateMerge(" + groupIndex + "," + rateIndex + ", " + isRateLock + ");'>"+ firstLink + "</a>");
	}
	
	  $oCell = $('<td />').addClass(cssName).appendTo(oRow);        
      //oCell.className = cssName;
	  if (parts.length > 1) {
	    $oCell.css({'text-align':'right','padding-right':'3px', 'padding-left':'3px', 'border-left':'1px solid #999999'}).prop('noWrap',true);
	    //oCell.style.textAlign='right';
        //oCell.style.paddingRight = '3px';
        //oCell.noWrap = true;
        //oCell.style.borderLeft = '1px solid #999999';
	  // Lock Rate
	    var secondLink = parts[1];
	    if ( secondLink == '?')
	    {
	        $oCell.html("<a href='#' disabled='disabled'>lock rate</a>&nbsp;<a href='#' onclick='f_ShowLockMsg()'>?</a>");
	    }
	    else
	        $oCell.html("<a href='#' onclick='return f_submitRateMerge(" + groupIndex + "," + rateIndex + ", true);'>"+ secondLink + "</a>");
	  }
	}

}

function f_insertCellRateMerge(oRow, content, cssClass) {
  if (content == null || "" == content)
  {
    content = '&nbsp;';
  }
  $('<td />').html(content).addClass(cssClass).prop('noWrap', true).appendTo(oRow);
  /*
  var oCell = oRow.insertCell(-1);
  oCell.className = cssClass;
  oCell.noWrap = true;
  if (null == content || "" == content)
    oCell.innerHTML = '&nbsp;';
  else
    oCell.innerHTML = content;
    */
}


<% } /* if(m_isBestPrice) */%>

<% if (m_isBestPrice) { %>
  var gLpList<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %> = <%= AspxTools.JsArray(m_gDataLpList) %>;
  
  var gRateOptionList<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %> = <%= AspxTools.JsArray(m_gDataRateOptionList) %>;
<% } %>

function f_SRM()
{
    var message = "You do not have permission to register this loan";
    var reasonsCannotRegisterList = <%= AspxTools.JsArray(ReasonsCannotRegister) %>;
    var CannotRegisterFailedConditionType = <%= AspxTools.JsNumeric(CannotRegisterFailedConditionType) %>;
    var reasonList = "";
        
    if (reasonsCannotRegisterList.length == 0){
        message = "Registering is not permitted for this loan file.";
    }
    else{
        if (CannotRegisterFailedConditionType == 2 /* Restraint */) {
            message += ", because of the following reasons:";
        } else {
            message += ".  The loan can be registered under any of the following circumstances:";
        }

        reasonList = "<ul>";
        var alpha = new Array('A','B','C','D','E','F','G','H','I','J');
        for (var i = 0; i < reasonsCannotRegisterList.length && i < 10 ; i++)
        {
            reasonList += '<li>' + alpha[i] + '.&nbsp;' + reasonsCannotRegisterList[i];
        }
        reasonList+= "</ul>";
    }
    
    var oPanel = document.getElementById('RateWarningDiv');
        oPanel.style.display = 'block';
        oPanel.style.width= '650px';
        oPanel.style.height = '150px';
        oPanel.style.top = '100px';
        oPanel.style.left = '100px';
    
     oPanel.innerHTML = '<p>' + message + '</p>' + reasonList  + "<br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' /></span>";

    return false; 
}



function f_ShowLockMsg()
{
    var message = "You do not have permission to lock this loan";
    var reasonsCannotLockList = <%= AspxTools.JsArray(ReasonsCannotLock) %>;
    var CannotLockFailedConditionType = <%= AspxTools.JsNumeric(CannotLockFailedConditionType) %>;
    
    var reasonList = "";
    if (reasonsCannotLockList.length == 0)
    {
        message = "Locking the rate is not permitted for this loan file.";
    }
    else
    {
        if (CannotLockFailedConditionType == 2 /* Restraint */) {
            message += ", because of the following reasons:";
        } else {
            message += ".  The loan can be locked under any of the following circumstances:";
        }

        reasonList = "<ul>";
        var alpha = new Array('A','B','C','D','E','F','G','H','I','J');
        for (var i = 0; i < reasonsCannotLockList.length && i < alpha.length ; i++)
        {
            reasonList += '<li>' + alpha[i] + '.&nbsp;' + reasonsCannotLockList[i];
        }
        reasonList+= "</ul>";
    }
    var oPanel = document.getElementById('RateWarningDiv');
    oPanel.style.display = 'block';
    oPanel.style.width= '650px';
    oPanel.style.height = '150px';
    oPanel.style.top = '100px';
    oPanel.style.left = '100px';
    
    oPanel.innerHTML = '<p>' + message + '</p>' + reasonList  + "<br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' /></span>";
}
function f_closeLockMsg()
{
    document.getElementById('RateWarningDiv').style.display = 'none';
}

  var gData<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %> = <%= AspxTools.JsArray(m_gDataList) %>;
  
function f_buildHiddenRates<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>(oTBody, data, id, currentIndex) {
  if (!!$(oTBody).data('initialized'))
  {
    return;
  }
  //if (oTBody.initialized)
  //  return;
      
  var productInfo = data[0];
  var rateOptions = data[1];

  var bIsBlockedRateLockSubmission = productInfo[3] == 1; 
  var bAreRatesExpired = productInfo[5] == 1;

  var titles = <%= AspxTools.JsArray(GetLinkTitles()) %>;
  var linkTitle = bIsBlockedRateLockSubmission ? titles[1] : titles[0];


  for (var i = 0; i < rateOptions.length; i++) {
  
    var $oRow = $('<tr/>').appendTo(oTBody);
    var $oCell = $('<td/>').appendTo($oRow);
    var rateOption = rateOptions[i];
    var isDisqualified = rateOption[14] == 1;
    if (i == 0) {
      $oCell.html("<a href='#' onclick='return f00(" + currentIndex + ");'>preview</a>").addClass('c0').prop('vAlign','top');  
      
      //oCell.innerHTML = "<a href='#' onclick='return f00(" + currentIndex + ");'>preview</a>";
      //oCell.vAlign = 'top';
      //oCell.className = 'c0';
      
      $('<td/>').html("<label for='M" + id + "'>View More </label><input type=checkbox id='M" + id + "' onclick='f_L(\"" + id + "\", this);' checked>").addClass('c1')
                 .prop('vAlign','top').prop('noWrap',true).appendTo($oRow);
      //oCell = f_insertCell(oRow);
      //oCell.innerHTML = "<label for='M" + id + "'>View More </label><input type=checkbox id='M" + id + "' onclick='f_L(\"" + id + "\", this);' checked>";
      //oCell.vAlign = 'top';
      //oCell.className = 'c1';
      //oCell.noWrap = 'true';
      
    } else {
      $oCell.html("&nbsp;").prop('colSpan', 2);
      //oCell.colSpan = 2;
    }
    
    $oCell = $('<td/>').appendTo($oRow).addClass('c2');
    //oCell = f_insertCell(oRow);
    //oCell.className = 'c2';
    var margin = rateOption[3] == '' ? '0.000' : rateOption[3];
    
    if (bIsBlockedRateLockSubmission) { 
     if ( isDisqualified )
     {
        var escaped = $('<div/>').text(rateOption[15]).html(); 
        //"<a href='#' class='unavailable'  data-msg='" + escaped  +"'>unavailable</a>"; 
        var disqualhtml = $('<div>').append($('<a>').prop('href', '#').addClass('unavailable').attr({
            'data-func' : 'f01',
            'data-arg1' : rateOption[7],
            'data-arg2' : id, 
            'data-msg' : escaped,
            'data-arg3' : currentIndex, 
            'data-arg4' : linkTitle
            }).text('unavailable')).html();
        $oCell.html($oCell.html() + disqualhtml);
     }
     else
        $oCell.html("<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ");\">" + linkTitle + "</a>");
      //oCell.innerHTML = "<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ");\">" + linkTitle + "</a>";
    } 
    else if( isDisqualified ) {
    
        var escaped = $('<div/>').text(rateOption[15]).html(); 
        //"<a href='#' class='unavailable'  data-msg='" + escaped  +"'>unavailable</a>"; 
        var disqualhtml = $('<div>').append($('<a>').prop('href', '#').addClass('unavailable').attr({
            'data-func' : 'f01',
            'data-arg1' : rateOption[7],
            'data-arg2' : id, 
            'data-msg' : escaped,
            'data-arg3' : currentIndex,
            'data-arg4' : linkTitle
            }).text('unavailable')).html();
         
            
        $oCell.html($oCell.html() + '&nbsp;&nbsp;' +  disqualhtml);
    }
    else {
	    if ( linkTitle != '' ) 
	    {
	      <%-- 7/28/2010 dd - OPM 42047 --%>
        var parts = linkTitle.split('/');
        var firstLink = parts[0];
            if (firstLink == '?')
            {
              $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>Lock rate</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>");
                //oCell.innerHTML += "&nbsp;&nbsp;<a href='#' disabled='disabled'>Lock rate</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>";
            }
            else if( firstLink === "register loan?"){
               $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>register loan</a>&nbsp;<a href='#' onclick='return f_SRM();'>?</a>");
            }
            else
            {
              $oCell.html($oCell.html() + "<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ", false);\">" + parts[0] + "</a>");
    	      //oCell.innerHTML = "<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ", false);\">" + parts[0] + "</a>";
    	      }
	      if (parts.length > 1) {
	      
	      	    var secondLink = parts[1];
            if ( secondLink == '?')
            {
              $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>lock rate</a>&nbsp;<a href='#' onclick='f_ShowLockMsg()'>?</a>");
                //oCell.innerHTML += "&nbsp;&nbsp;<a href='#' disabled='disabled'>lock rate</a>&nbsp;<a href='#' onclick='f_ShowLockMsg()'>?</a>";
            }
            else
            {
            $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ", true);\">" + parts[1] + "</a>");
    	        //oCell.innerHTML += "&nbsp;&nbsp;<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ", true);\">" + parts[1] + "</a>";
    	      }
	      }
	    }
    }
    <% if ( m_isOptionARMUsedInPml ) { %>
    f_insertRateOptionCell($oRow, rateOption[8]); <%-- // TeaserRate; --%>
    <% } %>
    f_insertRateOptionCell($oRow, rateOption[0],bAreRatesExpired); <%-- // Rate; --%>
    f_insertRateOptionCell($oRow, rateOption[11],bAreRatesExpired); <%-- // Point display in result; --%>
    f_insertRateOptionCell($oRow, rateOption[4],bAreRatesExpired); <%-- // FirstPmtAmt; --%>
    <% if (m_sProdDocT_NoIncome) { %>
      f_insertRateOptionCell($oRow, rateOption[3],bAreRatesExpired); <%-- // Margin; --%>
    <% } else { %>
      var violateDtiCss = rateOption[12] == 1 ? 'ViolateDti' : '';
      var violateDtiNotation = rateOption[12] == 1 ? '** ' : '';           

      f_insertRateOptionCell($oRow, rateOption[9],bAreRatesExpired); <%-- // QRate; --%>
      f_insertRateOptionCell($oRow, rateOption[3],bAreRatesExpired); <%-- // Margin; --%>
      $oCell = f_insertRateOptionCell($oRow, violateDtiNotation + rateOption[6],bAreRatesExpired); <%-- // Bottom Ratio; --%>
      if (violateDtiCss != '')
      {
        $oCell.addClass(violateDtiCss);
      }
    <% } %>
  
  }
  $(oTBody).data('initialized', true)
  //oTBody.initialized = true;
}

function f_insertRateOptionCell(oRow, str, bAreRatesExpired) {

  var className = !!bAreRatesExpired ? 'rie' : 'ri';
  
  var html = str == '' ? '&nbsp;' : str;
  
  return $('<td />').html(html).addClass(className).appendTo(oRow);
  /*
  var oCell = f_insertCell(oRow);
  if (null != bAreRatesExpired && bAreRatesExpired) {
    oCell.className = 'rie';
  } else {
    oCell.className = 'ri';
  }
  if (str == '')
    oCell.innerHTML = "&nbsp;";
  else
    oCell.innerHTML = str;
  return oCell;
  */
}

function f_insertRow(oTable) {
  alert('Please do not use me f_insertRow. I am no longer love.');
  var oRow = document.createElement("TR");
  oTable.appendChild(oRow);
  return oRow;
}
function f_insertCell(oRow) {
  alert('Please do not use me f_insertCell. I am no longer love.');
  var oCell = document.createElement("TD");
  oRow.appendChild(oCell);
  return oCell;
}
   var g_time2 = (new Date()).getTime();
 
</script>
<table class="LoanResultGroup" cellspacing="0" cellPadding="0" width="780" border="0">
  <tr>
    <TD><SPAN class=ResultHeader><%= AspxTools.HtmlString(m_resultHeader) %></SPAN><% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.OtherLoan) { %>&nbsp;&nbsp;<INPUT class=ButtonStyle onclick=f_rerun1stLoan(); type=button value=Close NoHighlight> 
<% } %><BR>You can expand a product listing to view 
            its rate options by clicking on the "View More" Checkbox. <% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) { %><BR>You must select a rate option for the 
            first lien in order to qualify the second. <% } if ( m_sortString != "" ) { %><br><br><span class="FieldLabel"><%= AspxTools.HtmlString(m_sortString) %></span><% if (!CurrentBroker.IsBestPriceEnabled) { %>&nbsp;You can return to the previous step to change the sort 
            option.<% } } %></TD></tr>
              <tr>
    <td>&nbsp;</td></tr>
    <% if (m_hasExpiredRate) { %>
            <tr><td style="color:red">Rates shown in red are expired</td></tr>
    <% } %>
</table><br>
    
    <% if ( m_isBestPrice) { %>
    <table style="WIDTH:700px" cellspacing="0" cellpadding="0" border=0 id=<%= AspxTools.HtmlAttribute("RateMergeEligibleTable" + (m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "0":"1")) %>>
    <% if (!m_sProdDocT_NoIncome) { %>
    <tr><td colspan=<%= AspxTools.HtmlAttribute(m_isOptionARMUsedInPml ? "10" : "9") %>&nbsp;</td><td colspan=3 style='padding-left:6px'><% if ( m_showMaxDtiWarning) { %>** - exceeds Max DTI<% } %></td></tr>
    <% } %>
      <TR>
    <TD class=ResultProductType noWrap colSpan=4 style="PADDING-RIGHT:20px"><% if (m_isLpeDisqualificationEnabled) { %>                        Eligible Loan Programs<% } %></TD>
    <% if (m_isOptionARMUsedInPml ) { %>
    <TD class="RateItemHeader RateItem_Col10">TEASER RATE (<A onclick="f_openHelp('Q00008.html', 400, 200);" tabIndex=-1 href="#" >?</A>)</TD>
    <% } %>
    <TD class="RateItemHeader RateItem_Col1">NOTE RATE</TD>
    <TD class="RateItemHeader RateItem_Col2"><% if (m_displayPmlFeeIn100Format) { %>                        PRICE<% } else { %>                          POINT<% } %></TD>
    <TD class="RateItemHeader RateItem_Col5">PAYMENT</TD>
    <% if ( ! m_sProdDocT_NoIncome ) { %> 
    <td class="RateItemHeader RateItem_Col1">QUAL RATE</td>
    <td class="RateItemHeader RateItem_Col4">MARGIN</td>
    <td class="RateItemHeader RateItem_Col9">DTI</td>
    <td class="RateItemHeader RateItem_Col9">MAX DTI</td>

    <% } else { %>
    <TD class="RateItemHeader RateItem_Col4">MARGIN</TD>
    <% } %>
    <td class="RateItemHeader" nowrap>&nbsp;</td>
    </TR>
  <TR>
    <TD bgColor=#000000 colSpan=<%= AspxTools.HtmlAttribute((13 - ( m_sProdDocT_NoIncome ? 3 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>'><IMG height=1 src="../images/spacer.gif" 
      width=100></TD></TR>
    </table>
    <% } %>
    <% if (!m_isBestPrice) { %>
<TABLE class=RateItem_TableWidth cellSpacing=0 cellPadding=0 border=0>
    <% if (!m_sProdDocT_NoIncome) { %>
    <tr><td  colspan=<%= AspxTools.HtmlAttribute(m_isOptionARMUsedInPml ? "9" : "8") %>></td><td colspan=2 style='padding-left:6px' nowrap><% if ( m_showMaxDtiWarning) { %>** - exceeds Max DTI<% } %></td></tr>
    <% } %>

  <TR>
    <TD class=ResultProductType noWrap colSpan=2><% if (m_isLpeDisqualificationEnabled) { %>                        Eligible Loan Programs<% } %></TD>
    <TD class="RateItemHeader RateItem_Col0">&nbsp;&nbsp;&nbsp;</TD>
    <% if (m_isOptionARMUsedInPml ) { %>
    <TD class="RateItemHeader RateItem_Col10">TEASER RATE (<A onclick="f_openHelp('Q00008.html', 400, 200);" tabIndex=-1 href="#" >?</A>)</TD>
    <% } %>
    <TD class="RateItemHeader RateItem_Col1">NOTE RATE</TD>
    <TD class="RateItemHeader RateItem_Col2"><% if (m_displayPmlFeeIn100Format) { %>                        PRICE<% } else { %>                          POINT<% } %></TD>
    <TD class="RateItemHeader RateItem_Col5">PAYMENT</TD>
    <% if ( ! m_sProdDocT_NoIncome ) { %> 
    <TD class="RateItemHeader RateItem_Col1">QUAL RATE</TD>
    <TD class="RateItemHeader RateItem_Col4">MARGIN</TD>
    <TD class="RateItemHeader RateItem_Col9">DTI</TD>
    
    <% } else { %>
    <TD class="RateItemHeader RateItem_Col4">MARGIN</TD>
    <% } %>
    <td style="width:80px">&nbsp;</td>
    </TR>
  <TR>
    <TD bgColor=#000000 colSpan=<%= AspxTools.HtmlAttribute((10 - ( m_sProdDocT_NoIncome ? 2 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>>
    <IMG height=1 src="../images/spacer.gif" width=100></TD></TR><asp:PlaceHolder id=m_eligiblePlaceHolder runat="server"></asp:PlaceHolder>
  <TR>
    <TD style="font-weight:bold;" align="center" colSpan=<%= AspxTools.HtmlAttribute((10 - ( m_sProdDocT_NoIncome ? 2 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>>
    <br /><ml:EncodedLiteral id=m_eligibleNotFoundLiteral runat="server">No qualified loan programs found. </ml:EncodedLiteral>
    <% if (m_eligibleNotFoundLiteral.Visible && m_isLpeManualSubmissionAllowed) { %>
      <BR><A onclick=f_submitManually(); href="#">Please click here to submit this loan for an exception or contact us for other loan options.</A>
    <% } %>
      </TD></TR>
      
      </TABLE>
      <% } /* if (!m_isBestPrice) */ %>
      
    <BR>
<HR class=RateItem_TableWidth align=left color=#ff9933 noShade SIZE=1>
<BR>
<TABLE class=RateItem_TableWidth cellSpacing=0 cellPadding=0 border=0>
  <TBODY>
    <tr>
      <td class="ResultProductType">Loan Programs Requiring Additional Borrower Info</td>
    </tr>
    <TR>
    <TD><asp:repeater id=m_insufficientRepeater runat="server" enableviewstate="false">
						<headertemplate>
							<table class="RateItem_TableWidth" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="3" bgcolor="#000000"><img src="../images/spacer.gif" width="100" height="1"></td>
								</tr>
						</headertemplate>
						<itemtemplate>
							<tr class="InsufficientItemBackground">
								<td colspan="3" class="ResultProductName"><%# AspxTools.HtmlString(Eval("lLpTemplateNm").ToString())%></td>
							</tr>
							<tr class="InsufficientItemBackground">
								<td class="RateItem_ColA" valign="top">&nbsp;</td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlString(Eval("Errors").ToString())%></td>
							</tr>
						</itemtemplate>
						<alternatingitemtemplate>
							<tr class="InsufficientAlternatingItemBackground">
								<td colspan="3" class="ResultProductName"><%# AspxTools.HtmlString(Eval("lLpTemplateNm").ToString())%></td>
							</tr>
							<tr class="InsufficientAlternatingItemBackground">
								<td class="RateItem_ColA" valign="top">&nbsp;</td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlString(Eval("Errors").ToString())%></td>
							</tr>
						</alternatingitemtemplate>
						<footertemplate>
	</TABLE>
	<table class="RateItem_TableWidth" border="0" cellpadding="0" cellspacing="0" bgcolor="#000000">
		<tr>
			<td bgcolor="#999999"><img src="../images/spacer.gif" width="100" height="1"></td>
		</tr>
	</table>
	</footertemplate> </asp:repeater></td></tr>
    <tr>
      <td align="center" style="font-weight:bold"><ml:EncodedLiteral ID="m_insufficientNotFoundLiteral" runat="server">None</ml:EncodedLiteral></td>
    </tr>
</table>

      <% if (m_isLpeDisqualificationEnabled) { // OPM 16132 %>
      <!-- Allow User to submit ineligible loan product start here --><% if (m_showRateForIneligible) { %><BR>
<HR class=RateItem_TableWidth align=left color=#ff9933 noShade SIZE=1>
<BR>
<div class="ResultProductType">
    <a href="javascript:void(0)" class="ineligibleLoanProgramsLink">
    <span class="ineligibleLoanProgramsHideText">- Hide</span>
    <span class="ineligibleLoanProgramsDisplayText">+ Display</span> Ineligible Loan Programs
    </a>
</div>
<TABLE class="RateItem_TableWidth ineligibleLoanProgramsTD" cellSpacing=0 cellPadding=0 border=0>
    <% if (!m_sProdDocT_NoIncome) { %>
    <tr><td  colspan=<%= AspxTools.HtmlAttribute(m_isOptionARMUsedInPml ? "9" : "8") %>></td><td colspan=2 style='padding-left:6px' nowrap><% if ( m_showMaxDtiWarning) { %>** - exceeds Max DTI<% } %></td></tr>
    <% } %>
  <TR>
    <%--<TD class="RateItemHeader" noWrap colSpan=2></TD>--%>
    <TD class="RateItemHeader RateItem_Col0" colspan=3>&nbsp;&nbsp;&nbsp;</TD>
    <% if (m_isOptionARMUsedInPml ) { %>
    <TD class="RateItemHeader RateItem_Col10">TEASER RATE</TD>
    <% } %>
    <TD class="RateItemHeader RateItem_Col1">NOTE RATE</TD>
    <TD class="RateItemHeader RateItem_Col2"><% if (m_displayPmlFeeIn100Format) { %>                        PRICE<% } else { %>                          POINT<% } %></TD>
    <TD class="RateItemHeader RateItem_Col5">PAYMENT</TD>
    <% if ( ! m_sProdDocT_NoIncome ) { %> 
    <TD class="RateItemHeader RateItem_Col1">QUAL RATE</TD>
    <TD class="RateItemHeader RateItem_Col4">MARGIN</TD>
    <TD class="RateItemHeader RateItem_Col9">DTI</TD>

    <% } else { %>
    <TD class="RateItemHeader RateItem_Col4">MARGIN</TD>
    <% } %>

    <td style="width:80px">&nbsp;</td>
    
    </TR>
  <TR>
    <TD bgColor=#000000 colSpan=<%= AspxTools.HtmlAttribute((10 - ( m_sProdDocT_NoIncome ? 2 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>>
    <IMG height=1 src="../images/spacer.gif" width=100></TD></TR><asp:PlaceHolder id=m_ineligibleRatePlaceHolder runat="server"></asp:PlaceHolder>
  <TR>
    <TD style="font-weight:bold" align=center colSpan=<%= AspxTools.HtmlAttribute((10 - ( m_sProdDocT_NoIncome ? 2 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>>
    <ml:EncodedLiteral id=m_ineligibleRateNotFoundLiteral runat="server">None</ml:EncodedLiteral></TD>
    </TR>
    </TABLE>
    <% } else { %><!-- Allow User to submit ineligible loan product end here --><BR>
<HR class=RateItem_TableWidth align=left color=#ff9933 noShade SIZE=1>
<BR>
<TABLE class=RateItem_TableWidth cellSpacing=0 cellPadding=0 border=0>
  <TBODY>
  <TR>
    <TD class="ResultProductType">
        <a href="javascript:void(0)" class="ineligibleLoanProgramsLink">
        <span class="ineligibleLoanProgramsHideText">- Hide</span>
        <span class="ineligibleLoanProgramsDisplayText">+ Display</span> Ineligible Loan Programs
        </a>
    </TD></TR>
  <TR>
    <TD class="ineligibleLoanProgramsTD"><asp:repeater id=m_ineligibleRepeater runat="server" enableviewstate="false">
						<headertemplate>
							<table class="RateItem_TableWidth" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="3" bgcolor="#000000"><img src="../images/spacer.gif" width="100" height="1"></td>
								</tr>
						</headertemplate>
						<itemtemplate>
							<tr class="IneBg">
								<td class="ResultProductName" colspan="3"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateNm").ToString()) %>&nbsp;&nbsp;
								</td>
							</tr>
							<tr class="IneBg">
								<td class="RateItem_ColA" valign="top">&nbsp;<a href='#' onclick="return f00(<%# AspxTools.JsString(Eval("TempIndex").ToString()) %>);">preview</a></td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlControl(CreateDisqualifiedRules(Eval("DisqualifiedRuleList")))%>
								</td>
							</tr>
						</itemtemplate>
						<alternatingitemtemplate>
							<tr class="IneAltBg">
								<td class="ResultProductName" colspan="3"><%# AspxTools.HtmlString(Eval("lLpTemplateNm").ToString()) %>&nbsp;&nbsp;
								</td>
							</tr>
							<tr class="IneAltBg">
								<td class="RateItem_ColA" valign="top">&nbsp;<a href='#' onclick="return f00(<%# AspxTools.JsString(Eval("TempIndex").ToString()) %>);">preview</a></td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlControl(CreateDisqualifiedRules(Eval("DisqualifiedRuleList")))%>
								</td>
							</tr>
						</alternatingitemtemplate>
						<footertemplate>
	</TABLE>
	<table class="RateItem_TableWidth" border="0" cellpadding="0" cellspacing="0" bgcolor="#000000">
		<tr>
			<td bgcolor="#999999"><img src="../images/spacer.gif" width="100" height="1"></td>
		</tr>
	</table>
	</footertemplate> </asp:repeater></TD></TR>
    <tr>
      <td align="center" style="font-weight:bold"><ml:EncodedLiteral ID="m_ineligibleNotFoundLiteral" runat="server">None</ml:EncodedLiteral></td>
    </tr>
  </TBODY>
  </TABLE>
    <% } %><% } // if (m_isLpeDisqualificationEnabled ) %>

<% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.OtherLoan) { %>
<TABLE class=RateItem_TableWidth cellSpacing=0 cellPadding=0 border=0>
    <% if (!m_isRerunMode) { %>
    <TR>
        <TD>&nbsp;</TD></TR>
        <% if (m_isLpeManualSubmissionAllowed) { %>    
            <TR><TD valign=middle align=center><A onclick=f_submitManual2nd(); href="#">Please click here to submit 2nd loan for an exception.</A></TD></TR>
        <% } %>
    <% } %>
    
    <% if (!m_isRerunMode || m_isSkip2ndLoan || (m_isRerunMode && !m_eligibleLoanProgramsExist)) { %>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="middle" align="center" height="20"><a onclick="return submitSkip2nd();" href="#">If seller or other lender provides 2nd financing, click here to submit.</a></td>
    </tr>
    <% } %>
    
</TABLE><% } %></span>

<span id="ErrorMessagePanel" style="DISPLAY: none">
  <table height="100%" width="100%">
    <tr>
      <td style="FONT-WEIGHT: bold; COLOR: red" valign="middle" align="center">
        <div id="ErrorMessage"></div>
        <a ID="ErrorReasons" href="#" onclick="return f_step4Circumstance_onclick();">Click here for more information</a>
      </td>
    </tr>
  </table>
</span>

</asp:Panel></td></tr></tbody></table>
<div id="DebugTimingPanel" style="border:solid 2px black;padding: 5px; DISPLAY:none; COLOR:black; POSITION:absolute; BACKGROUND-COLOR:white"></div>
<div id="RateWarningDiv" style="BORDER-RIGHT:black 2px solid; PADDING-RIGHT:5px; BORDER-TOP:black 2px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 2px solid; COLOR:black; PADDING-TOP:5px; BORDER-BOTTOM:black 2px solid; POSITION:absolute; BACKGROUND-COLOR:white;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:x-small"></div>

