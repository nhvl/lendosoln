<%@ Page language="c#" Codebehind="main.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.main" enableViewState="False" ValidateRequest="true"%>
<%@ Register TagPrefix="uc1" TagName="Agents" Src="Agents.ascx" %>
<%@ Register TagPrefix="uc1" TagName="GetResults" Src="GetResults.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EnterCredit" src="EnterCredit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ApplicantInfo" src="ApplicantInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PropertyInfo" src="PropertyInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EnterCreditMultApp" src="EnterCreditMultApp.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ApplicantInfoMultApp" src="ApplicantInfoMultApp.ascx" %>

<%@ Import Namespace="PriceMyLoan.Common"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"> 
<html>
	<head runat="server">
		<title>Price My Loan</title>
        <style type="text/css">
            body { margin: 0; }
            #ErrorPanel
            {
                color: red;
                font-weight: bold;
            }
            .MainWarning
            {
                border-right: #ffcc99 1px dashed;
                border-top: #ffcc99 1px dashed;
                font-weight: bold;
                border-left: #ffcc99 1px dashed;
                color: red;
                border-bottom: #ffcc99 1px dashed;
                background-color: #ffffcc;
            }
            .amodal
            {
                line-height: 1em;
                height: 100px;
                border: 3px inset black;
                top: 150px;
                margin-left: 200px;
                left: 0;
                right: 0;
                width: 600px;
                display: none;
                position: absolute;
                background-color: whitesmoke;
                padding: 5px;
                font-family:Verdana , Arial, Helvetica, sans-serif;
                font-size: 11px;
            }
            
            .amodal ol
            {
                list-style-type: upper-alpha; 
            }
            .amodal ol li 
            {
                padding-bottom: 5px;
            }            
        </style>
	</head>
	<body onunload="f_onunload();">
	<script type="text/javascript">
	    $j(document).ready(function() {
	        // 2/3/2012 dd - Short cut to jump to page directly.
	        $j(document).keydown(function(event) {
	            if (event.ctrlKey && !event.altKey && !event.shiftKey) {
	                var tabKey = '';
	                var previousIndex = '';
	                var maxVisitedIndex = '';
	                if (event.which == 49) {
	                    tabKey = 'CREDIT';
	                    previousIndex = 0;
	                    maxVisitedIndex = 0;
	                } else if (event.which == 50) {
	                    tabKey = 'APPINFO';
	                    previousIndex = 1;
	                    maxVisitedIndex = 1;
	                }
	                else if (event.which == 51) {
	                    tabKey = 'PROPINFO';
	                    previousIndex = 2;
	                    maxVisitedIndex = 2;
	                }
	                else if (event.which == 52) {
	                    tabKey = 'RESULT';
	                    previousIndex = 2;
	                    maxVisitedIndex = 3;
	                }
	                if (tabKey != '') {
	                    event.preventDefault();
	                    self.location = 'main.aspx?loanId=' + g_loanId + '&tabkey=' + tabKey + '&previousIndex=' + previousIndex + '&maxVisitedIndex=' + maxVisitedIndex;
	                }
	            }
	        });
	    }
	);
	</script>
	
		<script  type="text/javascript">
          <!--
          var g_startRenderPage = (new Date()).getTime();
          var _popupWindow = null;
          var _nameOfChild = null;
          var _bSubmitting = false;
          var _currentIndex = <%= AspxTools.JsNumeric(m_currentIndex) %>;
          var _maxVisitedIndex = <%= AspxTools.JsNumeric(m_maxVisitedIndex) %>;
          var MAX_STEPS = <%= AspxTools.JsNumeric(m_maxSteps) %>;
          var g_bIsEmbeddedPml = <%= AspxTools.JsBool(PriceMyLoanConstants.IsEmbeddedPML) %>;
          var g_loanId = <%= AspxTools.JsString(LoanID) %>;
            
          function f_onunload(){
            if (null != _popupWindow && !_popupWindow.closed) _popupWindow.close();
          }
          function f_allowClick(){
	        return _popupWindow == null || _popupWindow.closed == true;
          }
          
          function f_updateChildName( sNameOfChild ){
            if( sNameOfChild != null && sNameOfChild != "" ){
	          _nameOfChild = sNameOfChild;
            }
            else {
              _nameOfChild = null;
            }
          }
          
          function f_smartFocus(){
            if (typeof(f_focus_1stRequired) == 'function'){
              f_focus_1stRequired();
            }
          }  
  
          function _init(event){
            <% if (m_hasError == false) { %>
              var index =  _statuses.length;
              for (var i = 0; i < index; i++){
                var id = "img" + i;
                
                document.getElementById(id).onclick = function() { f_clickStep(event, this); };
                f_swapImg("img" + i, _statuses[i]);
                var allowableIndex = _currentIndex < _maxVisitedIndex ? _maxVisitedIndex : _currentIndex;      
              
                if (i == allowableIndex + 1){
                    var img = document.getElementById("img" + i);      
                    img.onmouseover = function () { f_onMouseOver(img); };
                    img.onmouseout = function() { f_onMouseOut(img); };
                }
              }
              
              
            <% } %>

            f_hideSummary();
            if (typeof(f_uc_init) == 'function'){
              f_uc_init();
            }
                    
            f_enableButtons();
            if (typeof(f_onValidateRequiredFields) == "function"){
              f_onValidateRequiredFields();
            }
              
            f_smartFocus();
        }
        
        function f_setFileVersion(sFileVersion){
            document.getElementById("sFileVersion").value = sFileVersion;  
        }
        
        function f_hideSummary(){
            if(typeof(parent.f_displayLoanSummary) == "function"){
                if(arguments.length == 0){
                    parent.f_displayLoanSummary(false);
                }
                else{
                    parent.f_displayLoanSummary(false, arguments[0]);
                }
            }
        }
        
        
        <%-- // Possible values for d are -1, 1 for back and continue, respectively. --%>
        function f_go(event, d){
  	        if(!f_allowClick()){
  	            return false;
            }
		
		    if (typeof(ML) != 'undefined' && ML.ErrorGobackUrl != '' && ML.ErrorGobackUrl != null){
		        self.location = ML.ErrorGobackUrl;
		        return;
		    }
		
		    if (d == -1 && _currentIndex == 0 && !g_bIsEmbeddedPml){
		        self.location = "agents.aspx?loanid=" + g_loanId;
            }
            else if ((d == 1 && _currentIndex < MAX_STEPS - 2) || (d == -1 && _currentIndex > 0)){
                var bForceValidate = true;
                if (d == -1){
                    bForceValidate = false;
                }
                f_switchTab(_currentIndex + d, bForceValidate, null, event);
            }
        }
        
        
  function f_swapImg(id, status) {
    var img = document.getElementById(id);
    img.src = img.src.replace(/_[a-z]+/, "_" + status);
    img.status = status == "on" ? "c" : "i";
    if (status == "on" || status == "active") {
      img.onmouseover = function () { f_onMouseOver(img); };
      img.onmouseout = function() { f_onMouseOut(img); };
      
    }
  }
  function f_clickStep(event, img) {
    var i = parseInt(img.id.substr(3));
    var allowableIndex = _currentIndex < _maxVisitedIndex ? _maxVisitedIndex : _currentIndex;
	if( !f_allowClick() )
		return false;
		
		var bForceValidate = true;
		if (i < _currentIndex)
		  bForceValidate = false;
		  
    if (f_isTabCompleted(i)) f_switchTab(i, bForceValidate, null, event);
    else if (i == allowableIndex + 1) f_switchTab(i, bForceValidate, null, event);
  }
  
  function f_step4Circumstance_onclick() {
    Modal.ShowPopup2('<%= AspxTools.ClientId(Step4CircumstancesDiv) %>');
    return false;
  }
  function f_onMouseOver(img) {	
	var i = parseInt(img.id.substr(3));
	
	img.prevImg = img.src;
	
	if ( i < _currentIndex )
	{
		if ( !document.getElementById("btnBack").disabled )
		{
			img.className = "ClickableImage";
			img.src = img.src.replace(/_[a-z]+/, "_active");
		}
		else
		{
			img.className = "";
		}
	}
	else
	{	
		if ( !document.getElementById("btnContinue").disabled 
		  || !document.getElementById("btnGetResult").disabled )
		{
			img.className = "ClickableImage";
			img.src = img.src.replace(/_[a-z]+/, "_active");
		}
		else
		{
			img.className = "";
		}
	}
}

 
  function f_onMouseOut(img) {
    if (null != img.prevImg){
      img.src = img.prevImg;
    }
  }
  
  function f_switchTab(i, bForceValidate, extraArgs, event) {
    if (!_bSubmitting) {
      var bValid = true;
      bForceValidate = null; <%-- // force validate all time. --%>
      if (null == bForceValidate || bForceValidate) {
        if (typeof(f_validatePage) == 'function')
          bValid = f_validatePage();
      }
      
      if (bValid) {
        _bSubmitting = true;
        f_enableAllButtons(false);

        var switchTabCallback = function(bPostBack) {
            var eventArg = i + '';
            if (extraArgs != null)
            {
                for (var p in extraArgs)
                {
                    if (extraArgs.hasOwnProperty(p))
                    {
                        var v = extraArgs[p];
                        if (v != null && v !== '') {
                            eventArg += ':' + p + '=' + extraArgs[p];
                        } else {
                            eventArg += ':' + p;
                        }
                    }
                }
            }
            if (bPostBack)  
                __doPostBack('', eventArg);
            else 
                _bSubmitting = false;
        };

        if (typeof(f_prePostBack) != "undefined") {
            f_prePostBack(event, switchTabCallback);
        }
        else {
            switchTabCallback(true);
        }
      }
    }
  }
  
  function f_isTabCompleted(i) {
    var img = document.getElementById("img" + i);
    if (img != null) {
      return img.status == "c";
    } else {
      return false;
    }
  }
  
  function f_setTabCompleted(i, bCompleted) {
    var img = document.getElementById("img" + i);
    if (img != null) {
      img.status = bCompleted ? "c" : "i";
    }
  }
  function f_clearClientPriceTiming()
  {
      // dd 2/10/2017 - this method is duplicate with one in ResultsFilter.ascx of the new PML UI.
      // Reason for duplicate code is lack of time to find the common script that share between both new PML and old PML ui.
      if (typeof (Storage) !== "undefined") {
          // Only support client side timing if DOM Storage is enable.
          sessionStorage.removeItem("ClientPriceTiming_Start");
          sessionStorage.removeItem("ClientPriceTiming_RenderResultStart");
          sessionStorage.removeItem("ClientPriceTiming_RenderResultEnd");
          sessionStorage.removeItem("ClientPriceTiming_End");
          sessionStorage.removeItem("ClientPriceTiming_RequestId");
      }
  }
  function f_getResult(event) {
      if (typeof(Storage) !== "undefined")
      {
          f_clearClientPriceTiming();
          sessionStorage["ClientPriceTiming_Start"] = (new Date()).getTime();
      }
    var allowableIndex = _currentIndex < _maxVisitedIndex ? _maxVisitedIndex : _currentIndex;  
    var i = <%= AspxTools.JsNumeric(m_maxSteps - 1) %>;
	if( !f_allowClick() )
		return false;
    if (i == allowableIndex + 1 || f_isTabCompleted(i)) f_switchTab(i, null, null, event);  
  }
  function f_enableButtons() {
    var allowableIndex = _currentIndex < _maxVisitedIndex ? _maxVisitedIndex : _currentIndex;  
    
    f_enableBtnById("btnBack", _currentIndex != 0 || !g_bIsEmbeddedPml);
    f_enableBtnById("btnContinue", _currentIndex < MAX_STEPS - 2);
    f_enableBtnById("btnGetResult", MAX_STEPS - 1 == allowableIndex + 1 || f_isTabCompleted(MAX_STEPS - 1));
  }
  function f_hideNavigation() {
    document.getElementById("NavigationPanel").style.display= "none";
  }
  function f_displayNavigation() {
    document.getElementById("NavigationPanel").style.display= "";
  }
  function f_enableAllButtons(bEnabled) {
    if (!bEnabled) {
      f_enableBtnById("btnBack", false);
      f_enableBtnById("btnContinue", false);
      f_enableBtnById("btnGetResult", false);
    } else {
      f_enableButtons();
    }
    
  }
  function f_enableBtnById(id, bEnabled) {
    var btn = document.getElementById(id);
    if (null != btn) {
      btn.disabled = !bEnabled;
      if (id == "btnContinue" )
       { if (!bEnabled && _currentIndex == (MAX_STEPS -2))
			btn.style.display = "none";
		 else btn.style.display = "";
       }
    }
  }
//  function f_btnContinue_onkeyup() {
//    if (event.keyCode == 13) f_go(1);
//  }
  function f_goToPipeline( bShowCertificate ) {
    <% if (PriceMyLoanConstants.IsEmbeddedPML) { %>
      parent.f_afterLPESubmitted();
  	<% } else if (!m_allowGoToPipeline) { %>
      self.location = <%= AspxTools.JsString(VirtualRoot + "/MortgageLeagueLogout.aspx?code=submitted") %>;
    <% } else { %>
	  if (bShowCertificate != null && bShowCertificate) {
		  self.location = <%= AspxTools.JsString(VirtualRoot + "/Main/Pipeline.aspx?certificateId=") %> + g_loanId;
	  } else {
		  self.location = <%= AspxTools.JsString(VirtualRoot + "/Main/Pipeline.aspx") %>;
	  }
	  <% } %>
  }
  function f_goToStep3(event, extraArgs){
    <%-- // OPM 6810 mf. We send users to step 3 if pricing times out. --%>
	f_switchTab(2, false, extraArgs, event);
  }
  function f_onResultClose(){
	f_goToPipeline( false );
  }

  function f_logout() {
    self.location = gVirtualRoot + "/logout.aspx";
  }
  function f_openHelp(page, w, h){
	  var url = gVirtualRoot + "/help/" + page;
	  LQBPopup.Show(url, {
	    width : w,
	    height: h
	  });
	  return false;
  }
  function f_openWarningWindow(){
    LQBPopup.Show( gVirtualRoot +  "/main/ListModifiedFields.aspx?loanid=" + g_loanId, {width:450, height:700, hideCloseButton: true} );
  }
  function f_enterNotes(){
	LQBPopup.Show(gVirtualRoot + "/main/BrokerNotes.aspx?loanid=" + g_loanId,{
	    onReturn : f_setFileVersion,
	    closeText : 'Cancel',
	    hideCloseButton: true 
	}); 

  }
  <%--
  // 6/19/07 mf. OPM 14208.  We do not want user to be able to restart
  // a pricing session by clicking back to one of the previous steps.
  // This will disable all image links.  No need to re-enable, because when
  // pricing is running, one of two things will happen: either results will 
  // be produced, or a timeout will occur and the user will sent back to 
  // step 3, which will both re-enable the links appropriately.
  --%>
  function f_disableStepLinks(){
    for (var i = 0; i < _statuses.length; i++) {
      var img = document.getElementById("img" + i);
      img.src = img.src.replace(/_[a-z]+/, (i == _statuses.length - 1 ? "_active" : "_on"));
      img.status = "i";
      img.className = "";
      img.onmouseover = ""; 
      img.onmouseout =  ""; 
    }
  }
  		    <% if (IsEncompassIntegration) { %>
    function f_sendToEncompass(w) {
        var args = new Object();
        var result = gService.main.call("GetResult_GenerateByPassTicket", args);
        var authid='';
        if (!result.error) {
          authid = result.value["Ticket"];
        }
       if(typeof(w) != 'undefined' && w != null && typeof(w.close) != 'undefined')
       {
           w.close();
       }
       setTimeout(function(){cedeControlToEncompass(authid)}, 1000);
    }
    function cedeControlToEncompass(authid)
    {
        location.href = '_EPASS_SIGNATURE;PRICEMYLOAN;;RETRIEVELOAN;' + <%= AspxTools.JsString(m_loanIdEncompassRequestType) %> + ';' + authid;
    }
    <% } %>
  //-->
        </script>
		<form id="main" method="post" runat="server" autocomplete="off" enctype="multipart/form-data">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" class="TopHeaderBackgroundColor">
						<tbody>
							<tr>
								<td width="1%" height="1%">
									<input type="button" value="Pipeline" class="ButtonStyle" onclick="if( !f_allowClick() ) return false; f_goToPipeline();" NoHighlight runat="server" id="m_btnStart">
								</td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tbody>
										<tr class="LoanSummaryBackgroundColor">
											<td class="LoanSummaryItem">Loan Number:&nbsp;<ml:EncodedLiteral id="sLNm" runat="server" /></td>
											<td class="LoanSummaryItem">Borrower Name:&nbsp;<ml:EncodedLiteral id="aBNm" runat="server" /></td>
											<td class="LoanSummaryItem">Loan Amount:&nbsp;<ml:EncodedLiteral id="sLAmtCalc" runat="server" /></td>
										</tr>
										<tr class="LoanSummaryBackgroundColor">
											<td class="LoanSummaryItem">Primary Credit Score: &nbsp;<ml:EncodedLiteral id="sCreditScoreType1" runat="server" />
													&nbsp;&nbsp;<a  id="lnkHelpPrimaryCreditScore" href='#' style="FONT-WEIGHT: bold;COLOR: #ff9933;TEXT-DECORATION: none" title='Help' onclick="f_openHelp('Q00006.html', 400, 250);">(explain)</a></td>
											<td class="LoanSummaryItem">Lowest Credit Score: &nbsp;<ml:EncodedLiteral id="sCreditScoreType2" runat="server" />													&nbsp;&nbsp;<a href='#'  id="lnkHelpLowestCreditScore" style="FONT-WEIGHT: bold;COLOR: #ff9933;TEXT-DECORATION: none" title='Help' onclick="f_openHelp('Q00006.html', 400, 250);">(explain)</a></td>
											<td class="LoanSummaryItem">LTV: <ml:EncodedLiteral id="sLtvR" runat="server" /></td>
											<td class="LoanSummaryItem">CLTV: <ml:EncodedLiteral id="sCltvR" runat="server" /></td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>

						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td background='../images/create_bar_bg.gif'>
						<%-- Header Navigation Start Here --%>
						<table border="0" cellspacing="0" cellpadding="0" id="m_tableNavigationSteps" runat="server">
							<tr>
								<td><img id="img0" src="../images/step1_off.gif" border="0" /></td>
								<td><img id="img1" src="../images/step2_off.gif" border="0" /></td>
								<td><img id="img2" src="../images/step3_off.gif" border="0" /></td>
								<td><img id="img3" src="../images/step4_off.gif" border="0" /></td>
								<td><input type="button" value="LOG OFF" class="ButtonStyle" onclick="f_logout();" NoHighlight id="m_btnLogOff" runat="server"></td>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						</table>
						<%-- Header Navigation End Here --%>
					</td>
				</tr>
				<% if (m_displayWarning && !m_hasError) { %>
				<tr>
					<td style="BORDER-RIGHT: #ffcc99 1px dashed; BORDER-TOP: #ffcc99 1px dashed; FONT-WEIGHT: bold; BORDER-LEFT: #ffcc99 1px dashed; COLOR: red; BORDER-BOTTOM: #ffcc99 1px dashed; BACKGROUND-COLOR: #ffffcc">
						<%= AspxTools.HtmlString(m_warningMsg) %>
						<a href="#" onclick="f_openWarningWindow(); return false;">View updated fields</a></td>
				</tr>
				<% } %>
				
			</table>
			
			<div runat="server" id="Step4CircumstancesDiv" class="amodal" style="z-index:500">
			    
			    <ml:EncodedLiteral ID="Step4CircumstancesMessage" runat="server" />
			    
			    <asp:Repeater runat="server" ID="Step4CircumstancesList" >
			        <HeaderTemplate>
			            <ol>
			        </HeaderTemplate>
			        <ItemTemplate>
			            <li><%# AspxTools.HtmlString((string)Container.DataItem) %></li>
			        </ItemTemplate>
			        <FooterTemplate>
			            </ol>
			        </FooterTemplate>
			    </asp:Repeater>
			    <div style="text-align:center;">
			        
			    <input type="button" value="Close" onclick="Modal.Hide()" />
			    </div>
			</div>
			
			<uc1:EnterCredit id="m_enterCredit" runat="server" />
			<uc1:EnterCreditMultApp id="m_enterCreditMultApp" Visible="false" runat="server" />
			<uc1:ApplicantInfo id="m_applicantInfo" runat="server" />
			<uc1:ApplicantInfoMultApp id="m_applicantInfoMultApp" Visible="false" runat="server" />
			<uc1:PropertyInfo id="m_propertyInfo" runat="server" />
			<uc1:GetResults id="m_getResults" runat="server" />
			<div id="ErrorPanel" runat="server">
			  <ml:EncodedLiteral ID="m_errorMessage" runat="server" />
			</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableNagivationButtons">
				<tbody id="NavigationPanel">
					<tr>
						<td align="left" style="PADDING-LEFT:5px;PADDING-TOP:20px" width="780">
							<%-- Navigation Buttons Start Here --%>
							<input type="button" id="btnBack" onclick="f_go(event, -1);" tabindex="100" value="< Go Back" NoHighlight class="ButtonStyle" style="WIDTH:80px">&nbsp;
							<input type="submit" id="btnContinue" onclick="f_go(event, 1);" tabindex="101" value="Next >" NoHighlight class="ButtonStyle" style="WIDTH:80px" runat="server">&nbsp;
							<input type="submit" id="btnGetResult" onclick="f_getResult(event);" value="Get Results >>>" NoHighlight class="ButtonStyle" tabindex="102" style="WIDTH:120px" runat="server">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" id="btnNotes" onclick="f_enterNotes();" value="Add Message to Lender" NoHighlight class="ButtonStyle" tabindex="103" style="WIDTH:170px" runat="server">
							<%-- Navigation Buttons End Here --%>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
				</tbody>
			</table>
		</form>
		<script language="javascript">
	  
	  var g_endRenderPage = (new Date()).getTime();
		</script>

	</body>
</HTML>
