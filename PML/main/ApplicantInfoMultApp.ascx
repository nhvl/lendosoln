﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ApplicantInfoMultApp.ascx.cs" Inherits="PriceMyLoan.UI.Main.ApplicantInfoMultApp" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<script language=javascript>

function f_focus_1stRequired() {
  var aBFirstNm = <%= AspxTools.JsGetElementById(aBFirstNm) %>;
  var aBLastNm = <%= AspxTools.JsGetElementById(aBLastNm) %>;
  var aBSsn = <%= AspxTools.JsGetElementById(aBSsn) %>;
  var aCFirstNm = <%= AspxTools.JsGetElementById(aCFirstNm) %>;
  var aCLastNm = <%= AspxTools.JsGetElementById(aCLastNm) %>;
  var aCSsn = <%= AspxTools.JsGetElementById(aCSsn) %>;
  var aBHasSpouse = <%= AspxTools.JsGetElementById(aBHasSpouse) %>.checked;
  
  if (!<%= AspxTools.JsBool(IsReadOnly) %>) {
    if (aBFirstNm.value == "") aBFirstNm.focus();
    else if (aBLastNm.value == "") aBLastNm.focus();
    else if (aBSsn.value == "") aBSsn.focus();
    else if (aBHasSpouse && aCFirstNm.value == "") aCFirstNm.focus();
    else if (aBHasSpouse && aCLastNm.value == "") aCLastNm.focus();
    else if (aBHasSpouse && aCSsn.value == "") aCSsn.focus();
    else aBFirstNm.focus();
  }

}
var gDisabledSsn = false;
var gCurrentSsnId = null;
function f_validateSsn(source, args) {
  
  var o = document.getElementById(source.controltovalidate);
  if (gDisabledSsn && gCurrentSsnId == source.controltovalidate)
    source.innerHTML = "";
  else
    source.innerHTML = "Invalid format of SSN";
    
  if (null != o && o.value != "") {
    args.IsValid = o.value.length == 11;
    return;
  }
  args.IsValid = true;
  
}
function f_ssn(o) {

  if (o.value.length < 11) {
    gDisabledSsn = true;
    gCurrentSsnId = o.id;
  }
    
  f_onValidateRequiredFields();
  
  gDisabledSsn = false;
    
}
function f_keydown_ssn() {
  <%--
  //if (event.keyCode == 9) {
  //  f_onValidateRequiredFields();
  //}
  --%>
}
function f_validatePage() {
  var bValid = true;
  if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
    bValid = Page_ClientValidate();
    
  return bValid;
}

function f_onValidateRequiredFields() {
  var appsValidator = <%= AspxTools.JsGetElementById(OtherAppsValidator) %>;
  appsValidator.enabled = false;
  var bValid = f_validatePage();
  appsValidator.enabled = true; 
  var bOtherAppsValid = f_validatePage() && bValid;
    
  var switchAppDdl = <%= AspxTools.JsGetElementById(m_appList) %>;
  switchAppDdl.disabled = !bValid;
  f_enableAllButtons(bOtherAppsValid);
}

<%-- //opm 4382 fs 08/02/08 --%>
function f_coApplicantHasEmptyFields()
{
  if (<%= AspxTools.JsBool(IsReadOnly) %>)
	return true;
	
  var tbox1 = <%= AspxTools.JsGetElementById(aCFirstNm) %>;
  var tbox2 = <%= AspxTools.JsGetElementById(aCMidNm) %>;
  var tbox3 = <%= AspxTools.JsGetElementById(aCLastNm) %>;
  var tbox4 = <%= AspxTools.JsGetElementById(aCSuffix) %>;
  var tbox5 = <%= AspxTools.JsGetElementById(aCSsn) %>;
  var tbox6 = <%= AspxTools.JsGetElementById(aCEmail) %>;
  
  if (tbox1 == null || tbox2 == null || tbox3 == null || tbox4 == null || tbox5 == null || tbox6 == null)
	return false;
	try{
	    if (tbox1.value == "" && tbox2.value == "" && tbox3.value == "" && tbox4.value == "" && tbox5.value == "" && tbox6.value == ""){
	        return true;
	    }
	}catch(e){}
   return false;
}


function f_confirmSpouseClick(cb) {
  if (!cb.checked && !f_coApplicantHasEmptyFields())
  {
   if (confirm ("Are you sure you want to delete the co-applicant information?"))
    f_onHasSpouseClick(cb);
   else cb.checked = "true";
  }
  else
     f_onHasSpouseClick(cb);
}


function f_onHasSpouseClick(cb) {

  f_spouseReadOnlyFields();
    
  if (!cb.checked) {
    <%= AspxTools.JsGetElementById(aProdCCitizenT) %>.options.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(aCSsn) %>.value = "";
    <%= AspxTools.JsGetElementById(sIsCSelfEmployed) %>.checked = false;
  } 
  f_onValidateRequiredFields();    
}


function f_spouseReadOnlyFields()
{
  var cb = <%= AspxTools.JsGetElementById(aBHasSpouse) %>;
  var tbox1 = <%= AspxTools.JsGetElementById(aCFirstNm) %>;
  var tbox2 = <%= AspxTools.JsGetElementById(aCMidNm) %>;
  var tbox3 = <%= AspxTools.JsGetElementById(aCLastNm) %>;
  var tbox4 = <%= AspxTools.JsGetElementById(aCSuffix) %>;
  var tbox5 = <%= AspxTools.JsGetElementById(aCSsn) %>;
  var tbox6 = <%= AspxTools.JsGetElementById(aCEmail) %>;
  var ddl1  = <%= AspxTools.JsGetElementById(aProdCCitizenT) %>;
  var cb2    = <%= AspxTools.JsGetElementById(sIsCSelfEmployed) %>;
    
  if (cb == null || tbox1 == null || tbox2 == null || tbox3 == null || tbox4 == null || tbox5 == null || tbox6 == null)
	return;
 
    var isReadOnly = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> );
    $j([tbox1,tbox2,tbox3,tbox4,tbox5,tbox6, ddl1, cb2]).readOnly( isReadOnly );
    
//  tbox1.readOnly = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? "readonly" : "";
//  tbox2.readOnly = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? "readonly" : "";
//  tbox3.readOnly = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? "readonly" : "";
//  tbox4.readOnly = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? "readonly" : "";
//  tbox5.readOnly = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? "readonly" : "";
//  tbox6.readOnly = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? "readonly" : "";
//  
  
//  ddl1.disabled  = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? true : false;
//  cb2.disabled    = !(cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %> ) ? true : false;
//  
  if (!<%= AspxTools.JsBool(IsReadOnly) %>)
  {
   tbox1.value = cb.checked ? tbox1.value : "";
   tbox2.value = cb.checked ? tbox2.value : "";
   tbox3.value = cb.checked ? tbox3.value : "";
   tbox4.value = cb.checked ? tbox4.value : "";
   tbox5.value = cb.checked ? tbox5.value : "";
   tbox6.value = cb.checked ? tbox6.value : "";
  }
  
  var v1 = <%= AspxTools.JsGetElementById(aCFirstNmValidator) %>;
  var v2 = <%= AspxTools.JsGetElementById(aCLastNmValidator) %>;  
  var v3 = <%= AspxTools.JsGetElementById(aCSsnValidator) %>;  
  var v4 = <%= AspxTools.JsGetElementById(aCSsnFormatValidator) %>;
  var v5 = <%= AspxTools.JsGetElementById(aCEmailValidator) %>;
  v1.enabled = cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %>;
  v2.enabled = cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %>;
  v3.enabled = cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %>;
  v4.enabled = cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %>;
  if (v5 != null){
    v5.enabled = cb.checked && !<%= AspxTools.JsBool(IsReadOnly) %>;
  }
}

function f_uc_init() {
  f_invalidApps_init();

  <%-- opm 4382 fs 07/25/08 --%>
  f_spouseReadOnlyFields();
  $j('#lnkEditLiability').click(f_editLiability);
  
  if (<%= AspxTools.JsBool(IsReadOnly) %>)
  {
	document.getElementById('lnkEditLiability').disabled = true;
	document.getElementById('lnkEditLiability').style.textDecorationNone = true;
	document.getElementById('lnkEditLiability').style.cursor = "default";
  }
}
function f_editLiability() {
  if (<%= AspxTools.JsBool(IsReadOnly) %>)
    return;
    LQBPopup.Show(gVirtualRoot + '/main/LiabilityList.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&appid='+ <%= AspxTools.JsString(m_currentAppId) %>,{
        width: 700,
        height: 500, 
        hideCloseButton: true, 
        onReturn : f_editLiability_callback
    });
}

function f_editLiability_callback(args){

        <%= AspxTools.JsGetElementById(sTransmOMonPmtPe) %>.value = args.aTransmOMonPmtPe;
        f_setFileVersion(args.fileVersion);
}

function f_onEditPublicRecordsClick() {
    var url = 'PublicRecordList.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&appid=' + <%= AspxTools.JsString(m_currentAppId) %>;
    LQBPopup.Show(url, { width: '1000', height: '700' } );

	return false;  
}


<% if (m_displayCreditReportButton) { %>
function f_onViewCreditReportClick() {
	var report_url = ML.VirtualRoot + '/Main/ViewCreditFrame.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&applicationid=' + <%= AspxTools.JsString(m_applicationID) %>;
    LQBPopup.Show(report_url, {width: 700, height: 800, hideCloseButton: true});
	return false;  
}
<% } %>

function f_triggerAction(action)
{
    if (action === null || typeof(action) !== 'string')
        return;
    
    var appsValidator = <%= AspxTools.JsGetElementById(OtherAppsValidator) %>;
    appsValidator.enabled = false;
    f_validatePage();
        
     __doPostBack('', action);
}

function f_emailValidation(src, args){
  var regex = new RegExp(<%=AspxTools.JsString(ConstApp.EmailValidationExpression) %>);
  if (typeof src == undefined || src == null)
    return;
  
  try{
    var sToValidate = (src == <%=AspxTools.JsGetElementById(aBEmailValidator) %>) ? <%=AspxTools.JsGetElementById(aBEmail) %>.value : <%=AspxTools.JsGetElementById(aCEmail) %>.value;
    var isValid = sToValidate.match(regex);
    var isEmpty = (sToValidate.length == 0);
    args.IsValid = <% if (CurrentBroker.IsaBEmailRequiredInPml) { %> isValid <% } else { %> isValid || isEmpty <% } %>;
  }catch(e){}
}

function f_validateOtherApps(src, args){ 
    if (typeof(g_invApps) == 'undefined' || typeof(g_invApps.length) == 'undefined' || g_invApps.length == 0)
    {
        args.IsValid = true;
        return;
    }
    
    var names = "";
    var isFirst = true;
    for (i=0; i < g_invApps.length; i++)
    {
        if (!isFirst)
            names += ", ";
        else
            isFirst = false;
                
        names += "'" + g_invApps[i] + "'";
    }
    src.innerText = "Correct the following application before moving to the next step: " + names;
    args.IsValid = false;
}

function f_invalidApps_init()
{
    var appList = <%= AspxTools.JsGetElementById(m_appList) %>;
    var hash = new Object();
    
    if (typeof(invalidApps) == 'undefined' || invalidApps == null)
        return;
    if (typeof(g_invApps) == 'undefined')
        return;
    
    for (var i=0; i < invalidApps.length; i++)
    {
            hash[invalidApps[i]] = "";
    }
    
    for (var i=0; i < appList.options.length; i++)
    {
        var o = appList.options[i];
        if (typeof(hash[o.value]) != 'undefined')
        {
            if (o.selected)
            {
                hash[o.value] = undefined;
            }
            else
            {
                hash[o.value] = o.text;            
                g_invApps.push(o.text);
            }
        }
    }
}
var g_invApps = new Array();

function FthbClick(type)
{
    var fthb;
    var hist;
    
    if(type=='B')
    {
        fthb = <%=AspxTools.JsGetElementById(aBTotalScoreIsFthb) %>;
        hist = <%=AspxTools.JsGetElementById(aBHasHousingHistContainer) %>;
        }
    else
    {
        fthb =  <%=AspxTools.JsGetElementById(aCTotalScoreIsFthb) %>;
        hist = <%=AspxTools.JsGetElementById(aCHasHousingHistContainer) %>;
        }
    

    if(fthb.checked)
        hist.style.display = "";
    else
        hist.style.display = "none";
}

</script>

<br />
<table>
<tr>
    <td class="SubHeader" style="padding-bottom:1em; padding-left:0.5em;">Application Information</td>
</tr>
<tr><td style="margin:0.5em; padding-left:0.5em;">
  <div>
    <asp:dropdownlist id="m_appList" Width="220" runat="server" onchange="f_triggerAction(event, 'switch');"></asp:dropdownlist>
  </div>
  <fieldset style="border:1px solid #999999; padding:0.5em;">
    <legend style="border-width:0px; margin-left:1em;padding:0.2em 0.8em;" class="FieldLabel">
      <ml:EncodedLabel id="m_appTitle" CssClass="FieldLabel" runat="server" />
    </legend>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top">
 
    
    
<table id="Table1" cellSpacing="0" cellPadding="7" width="700" border="0">
  <TBODY>
  <tr>
    <td colSpan="2">
      <table width="650" border="0" cellSpacing="0" cellPadding="0">
        <tr>
          <td vAlign="top" noWrap>
            <table cellSpacing="0" cellPadding="0">
              <tr>
                <td class="SubHeader" style="padding-bottom:0.5em" colspan="2">Applicant Info</td>
              </tr>
              <tr>
                <td class="FieldLabel">First Name</td>
                <td noWrap><asp:textbox id=aBFirstNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aBFirstNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aBFirstNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Middle Name</td>
                <td noWrap><asp:textbox id=aBMidNm width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Last Name</td>
                <td noWrap><asp:textbox id=aBLastNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aBLastNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aBLastNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Suffix</td>
                <td noWrap><asp:textbox id=aBSuffix width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>SSN</td>
                <td noWrap><ml:ssntextbox id=aBSsn onkeyup="f_ssn(this);" onblur="f_onValidateRequiredFields();" width="130px" runat="server" preset="ssn"></ml:ssntextbox><asp:requiredfieldvalidator id=aBSsnValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aBSsn"></asp:requiredfieldvalidator><asp:CustomValidator id=aBSsnFormatValidator runat="server" ErrorMessage="Invalid format of SSN" ControlToValidate="aBSsn" ClientValidationFunction="f_validateSsn" Display="Dynamic" CssClass="FieldLabel"></asp:CustomValidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>E-mail</td>
                <td noWrap>
                    <asp:textbox id="aBEmail" width="160px" onkeyup="f_onValidateRequiredFields();" runat="server"></asp:textbox>
                    <asp:CustomValidator id="aBEmailValidator" runat="server" forecolor="" ClientValidationFunction="f_emailValidation" ValidateEmptyText="true" cssclass="RequireValidatorMessage" controltovalidate="aBEmail"></asp:CustomValidator>
                </td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Citizenship</td>
                <td class=FieldValue noWrap><asp:dropdownlist id=aProdBCitizenT runat="server"></asp:dropdownlist></td>
              </tr>
              <tr>
                <td class="FieldLabel" noWrap>Self Employed?</td>
                <td class=FieldLabel noWrap><asp:checkbox id=sIsSelfEmployed runat="server" Text="Yes"></asp:checkbox></td>
              </tr>
              <tr>
                <td class="FieldLabel" noWrap>Credit Scores</td>
                <td class=FieldValue noWrap>
                  <span title="Experian">XP:</span><asp:textbox id=aBExperianScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="TransUnion">TU:</span><asp:textbox id=aBTransUnionScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="Equifax">EF:</span><asp:textbox id=aBEquifaxScorePe runat="server" Width="35px"></asp:textbox>
                </td>
              </tr>
                            <tr id="aBTotalScoreIsFthbContainer" runat="server">
                                <td  class="FieldLabel" noWrap>First Time Home Buyer?</td>
                                <td class="FieldLabel">
                                    <asp:CheckBox ID="aBTotalScoreIsFthb" runat="server" onclick="FthbClick('B');" />
                                </td>
                            </tr>
                            <tr id="aBHasHousingHistContainer" runat="server">
                                <td  class="FieldLabel" noWrap>Has Housing History?</td>
                                <td>
                                    <asp:CheckBox ID="aBHasHousingHist" runat="server" /> 
                                </td>
                            </tr>
            </table>
           </td>
          <TD style="WIDTH: 22px" vAlign=top noWrap>&nbsp;&nbsp;&nbsp; </TD>
          <td vAlign=top noWrap style="padding-left:2em">
            <table cellSpacing="0" cellPadding="0">
              <tbody id=SpousePanel>
              <tr>
                <td class="SubHeader" style="padding-bottom:0.5em" colspan="2" valign="top">
                    <asp:checkbox id="aBHasSpouse" onclick="f_confirmSpouseClick(this);" runat="server" />Co-Applicant Info
                </td>
              </tr>
              <tr>
                <td class="FieldLabel" noWrap>First Name</td>
                <td noWrap><asp:textbox id=aCFirstNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aCFirstNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aCFirstNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Middle Name</td>
                <td noWrap><asp:textbox id=aCMidNm width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Last Name</td>
                <td noWrap><asp:textbox id=aCLastNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aCLastNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aCLastNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Suffix</td>
                <td noWrap><asp:textbox id=aCSuffix width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>SSN</td>
                <td noWrap><ml:ssntextbox id=aCSsn  onkeyup="f_ssn(this);" onblur="f_onValidateRequiredFields();" width="130px" runat="server" preset="ssn"></ml:ssntextbox><asp:requiredfieldvalidator id=aCSsnValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aCSsn"></asp:requiredfieldvalidator><asp:CustomValidator id=aCSsnFormatValidator runat="server" ErrorMessage="Invalid format of SSN" ControlToValidate="aCSsn" ClientValidationFunction="f_validateSsn" Display="Dynamic" CssClass="FieldLabel"></asp:CustomValidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>E-mail</td>
                <td noWrap>
                    <asp:textbox id="aCEmail" width="160px" onkeyup="f_onValidateRequiredFields();" runat="server"></asp:textbox>
                    <asp:CustomValidator id=aCEmailValidator runat="server" forecolor="" ClientValidationFunction="f_emailValidation" ValidateEmptyText="true" cssclass="RequireValidatorMessage" controltovalidate="aCEmail"></asp:CustomValidator>
                </td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Citizenship</td>
                <td class=FieldValue noWrap><asp:dropdownlist id="aProdCCitizenT" runat="server"></asp:dropdownlist></td>
              </tr>
              <tr>
                <td class="FieldLabel" noWrap>Self Employed?</td>
                <td class=FieldLabel noWrap><asp:checkbox id="sIsCSelfEmployed" runat="server" Text="Yes"></asp:checkbox></td>
              </tr>
              <tr>
                <td class="FieldLabel" noWrap>Credit Scores</td>
                <td class=FieldValue noWrap><span title="Experian">XP:</span><asp:textbox id=aCExperianScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="TransUnion">TU:</span><asp:textbox id=aCTransUnionScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="Equifax">EF:</span><asp:textbox id=aCEquifaxScorePe runat="server" Width="35px"></asp:textbox></td></tr>
                  
                            <tr id="aCTotalScoreIsFthbContainer" runat="server">
                                <td  class="FieldLabel" noWrap>First Time Home Buyer?</td>
                                <td class="FieldLabel">
                                    <asp:CheckBox ID="aCTotalScoreIsFthb" runat="server" onclick="FthbClick('C');" />
                                </td>
                            </tr>
                            <tr id="aCHasHousingHistContainer" runat="server">
                                <td  class="FieldLabel" noWrap>Has Housing History?</td>
                                <td>
                                    <asp:CheckBox ID="aCHasHousingHist" runat="server" /> 
                                </td>
                            </tr>
             </tbody>
            </table>
           </td>
          </tr>
        </table>
      </td>
  </tr>
  <tr><td><hr /></td></tr>
  <tr>
    <td vAlign="top" colSpan="2">
      <table id="Table2" cellSpacing="0" cellPadding="0" border="0">
        <TR>
          <TD class=FieldLabel noWrap width=105>Total Payment&nbsp;</TD>
          <TD class=FieldLabel noWrap><ml:moneytextbox id=sTransmOMonPmtPe width="90" runat="server" preset="money" ReadOnly="True"></ml:moneytextbox>&nbsp;/
			month&nbsp;<A  href="#" id="lnkEditLiability">Edit Liabilities</A>
			<% if (m_displayPublicRecordsEdit) {
			if ( ! IsReadOnly ) { %>
			  &nbsp;&nbsp;&nbsp;<a id="lnkEditPublicRecord" href="#" onclick="return f_onEditPublicRecordsClick();" >Edit Public Records</a>
			<% } else { %>
			  &nbsp;&nbsp;&nbsp;<a id="lnkEditPublicRecord" href="#" style="CURSOR: default; TEXT-DECORATION: none" disabled="disabled" >Edit Public Records</a>
			<% } 
			 } else if (m_displayCreditReportButton) { %> 
			&nbsp;&nbsp;&nbsp;<span title="No editable public records on credit report">No Public Records</span>
			<% }
     if (m_displayCreditReportButton) { %>
    &nbsp;&nbsp;&nbsp;<a id="lnkViewCreditReport" href="#" onclick="return f_onViewCreditReportClick();" >View Credit Report</a>
    <% } %>            
            </TD></TR>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>Negative 
            Cash Flow from Other Properties&nbsp;<ml:moneytextbox id=sOpNegCfPe width="90px" runat="server" preset="money"></ml:moneytextbox>&nbsp;/ 
            month&nbsp;&nbsp;<a id="lnkHelpNegCf" onclick="f_openHelp('Q00004.html', 730, 600);" tabIndex=-1 href="#" >Explain</a></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2 style="PADDING-TOP: 5px">Total income will be asked at the next step.</td>
        </tr>
     </table>
    </td>
  </tr>
  </TBODY>
  </table>
  </td>
</tr>
</table>
  </fieldset><br/>
  <asp:customvalidator id="OtherAppsValidator" CssClass="FieldLabel" runat="server" ErrorMessage="Invalid application found." ClientValidationFunction="f_validateOtherApps"></asp:customvalidator>
</td></tr></table>