/// Author: David Dao

using System;
using System.Collections.Specialized;
using System.Data;
using System.Linq;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Integration.LoanProductAdvisor;
using LendersOffice.ObjLib.Conversions.Aus;

namespace PriceMyLoan.main
{
	public partial class LoanProspectorExportLoginService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Save":
                    Save();
                    break;
            }
        }

        private void Save() 
        {
            
            Guid sLId = GetGuid("LoanID");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            CPageData dataLoan = new CLoanProspectorLoginData(sLId);
            dataLoan.InitSave(sFileVersion);

            AusCreditOrderingInfo tempCraInfo = new AusCreditOrderingInfo
            {
                CraProviderId = GetString("FreddieCrcDDL", ""),
                CreditReportOption = GetString("CreditChoice", "") == "Merged" ? (GetBool("cbRequestReorderCredit") ? CreditReportType.Reissue : CreditReportType.OrderNew) : CreditReportType.UsePrevious,
                Applications = dataLoan.Apps.Select(app => this.GetAppCreditData(app))
            };

            LoanSubmitRequest.SaveTemporaryCRAInformation(sLId, tempCraInfo);

            dataLoan.sFreddieLoanId = GetString("sFreddieLoanId");
            dataLoan.sFreddieTransactionId = GetString("sFreddieTransactionId");
            dataLoan.sLpAusKey = GetString("sLpAusKey");
            dataLoan.sFreddieSellerNum = GetString("sFreddieSellerNum");
            dataLoan.sFreddieTpoNum = GetString("sFreddieTpoNum");
            dataLoan.sFreddieNotpNum = GetString("sFreddieNotpNum");
            dataLoan.sFredProcPointT = (E_sFredProcPointT) GetInt("sFredProcPointT");

            

            if (GetString("sFreddieLpPassword") != "") 
            {
                dataLoan.sFreddieLpPassword = GetString("sFreddieLpPassword");
            }
            dataLoan.Save();
        }
        
        /// <summary>
        /// Generates a credit requesting data object for an application, based on service arguments.
        /// </summary>
        /// <param name="appData">The application to build a credit data object for.</param>
        /// <returns>The credit reference number to add to credit ordering info.</returns>
        private AusCreditOrderingInfo.AusCreditAppData GetAppCreditData(CAppData appData)
        {
            string key = "B" + appData.aAppId.ToString("N");
            string reference = this.GetString(key, string.Empty);

            string key2 = "ReorderB" + appData.aAppId.ToString("N");
            bool reorder = this.GetBool(key2, false);

            return new AusCreditOrderingInfo.AusCreditAppData(appData.aBNm, appData.aCNm, reference, appData.aAppId, reorder);
        }
    }
}
