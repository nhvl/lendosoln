/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Conversions;
using LendersOffice.Drivers.Gateways;
using LODataAccess = DataAccess;

namespace PriceMyLoan.main
{
	/// <summary>
	/// Summary description for CitiDemo.
	/// </summary>
	public partial class CitiDemo : PriceMyLoan.UI.BasePage
	{

        private string m_productionUrl = "https://correspondent.citimortgage.com/Correspondent/";
        private string m_testUrl = "https://uat2.correspondent.citimortgage.com/Correspondent/";

        protected string m_loginKeyId = "";
        protected string m_editKeyId = "";
    
        protected string m_initJs = "";
        protected bool m_launch = false;

        private string m_loanNumber = "000000099326";
		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (RequestHelper.GetSafeQueryString("action") == "post") 
            {
                PerformPost();
            }
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        private void PerformPost() 
        {

            NameValueCollection nv = (NameValueCollection) Cache[RequestHelper.GetSafeQueryString("keyid")];
            if (null == nv)
                return;

            Response.ClearContent();
            StringBuilder sb = new StringBuilder();

            sb.Append("<html><head></head><body onload=\"init();\"><script language=javascript>");
            sb.Append(Environment.NewLine);
            sb.Append("<!--");
            sb.Append(Environment.NewLine);
            sb.Append("function init() { document.forms[0].submit(); }");
            sb.Append(Environment.NewLine);
            sb.Append("//-->");
            sb.Append(Environment.NewLine);
            sb.Append("</script>");
            sb.Append(Environment.NewLine);
            sb.AppendFormat("<form method=\"post\" action=\"{0}\">", nv["url"]);

            foreach (string key in nv.Keys) 
            {
                if (key == "url") continue;
                sb.AppendFormat("<input type=\"hidden\" name=\"{0}\" value=\"{1}\">{2}", key, nv[key], Environment.NewLine);
            }
            sb.Append(Environment.NewLine);
            sb.Append("</form></body></html>");
            Response.Write(sb.ToString());
            

            Response.Flush();

            Cache.Remove(RequestHelper.GetSafeQueryString("keyid"));
            Response.Close();

            //Response.End();
        }
        private string GetLoginUrl(bool isProduction) 
        {
            return isProduction ? m_productionUrl + "login.do" : m_testUrl + "login.do";
        }
        private string GetEditUrl(bool isProduction) 
        {
            return isProduction ? m_productionUrl + "EditLoan.do" : m_testUrl + "EditLoan.do";

        }
        private string GetImportServlet(bool isProduction) 
        {
            return isProduction ? m_productionUrl + "LOSImportServlet" : m_testUrl + "LOSImportServlet";

        }
        private string GetLogoutUrl(bool isProduction) 
        {
            return isProduction ? m_productionUrl + "logout.jsp" : m_testUrl + "logout.jsp";

        }
        protected void SubmitToCiti(object sender, System.EventArgs e)
        {
            bool isImport = cbIsImport.Checked;

            if (isImport) 
            {

                if (!ExportToCiti()) 
                {
                    return;
                }
            }
            m_loginKeyId = Guid.NewGuid().ToString();

            bool isProduction = rblServers.SelectedItem.Value == "Production";

            NameValueCollection nv = new NameValueCollection();
            nv["url"] = GetLoginUrl(isProduction);
            nv["LOGIN_NAME"] = tbUserName.Text;
            nv["PASSWORD"] = tbPassword.Text;

            Cache[m_loginKeyId] = nv;


            if (isImport) 
            {
                m_editKeyId = Guid.NewGuid().ToString();

                nv = new NameValueCollection();
                nv["url"] = GetEditUrl(isProduction);
                nv["LOAN_NUMBER"] = m_loanNumber;
                nv["From"] = "SUMMARY";
                nv["FLOAT"] = "";
                nv["LoanType"] = "UNREG";
                Cache[m_editKeyId] = nv;
            }
            m_launch = true;

        }

        private bool ExportToCiti() 
        {

            bool ret = true;
            try 
            {

                Guid loanId = RequestHelper.GetGuid("loanid");
                FannieMae32Exporter exporter = new FannieMae32Exporter(loanId);

                byte[] buffer = exporter.Export();

                string fnmaContent = System.Text.Encoding.ASCII.GetString(buffer);
            

                StringBuilder sb = new StringBuilder();
                sb.Append("-----------------------------7d8673912703c2");
                sb.Append(Environment.NewLine);
                sb.Append("Content-Disposition: form-data; name=\"mode\"");
                sb.Append(Environment.NewLine);
                sb.Append(Environment.NewLine);
                sb.Append("single");
                sb.Append(Environment.NewLine);
                sb.Append("-----------------------------7d8673912703c2");
                sb.Append(Environment.NewLine);
                sb.Append("Content-Disposition: form-data; name=\"errorUrl\"");
                sb.Append(Environment.NewLine);
                sb.Append(Environment.NewLine);
                sb.Append("error.jsp");
                sb.Append(Environment.NewLine);
                sb.Append("-----------------------------7d8673912703c2");
                sb.Append(Environment.NewLine);
                sb.Append("Content-Disposition: form-data; name=\"forwardUrl\"");
                sb.Append(Environment.NewLine);
                sb.Append(Environment.NewLine);
                sb.Append("EditLoanFromLOS.do");
                sb.Append(Environment.NewLine);

                sb.Append("-----------------------------7d8673912703c2");
                sb.Append(Environment.NewLine);
                sb.Append("Content-Disposition: form-data; name=\"uploadFile\"; filename=\"C:\\Test.fnm\"");
                sb.Append(Environment.NewLine);
                sb.Append("Content-Type: application/octet-stream");
                sb.Append(Environment.NewLine);
                sb.Append(Environment.NewLine);
                sb.Append(fnmaContent);
                sb.Append(Environment.NewLine);
                sb.Append("-----------------------------7d8673912703c2--");


                AbstractWebBot bot = new AbstractWebBot();
                bool isProduction = rblServers.SelectedItem.Value == "Production";

                NameValueCollection nv = new NameValueCollection();
                nv["LOGIN_NAME"] = tbUserName.Text;
                nv["PASSWORD"] = tbPassword.Text;

                string response;
                response = bot.UploadDataReturnResponse(GetLoginUrl(isProduction), nv);

//                LODataAccess.Tools.LogError("LoginResponse=" + response);
//                Logger.Log("LoginResponse-" + response);

            
                string url = GetImportServlet(isProduction);
            
                string rawData = sb.ToString();
                LODataAccess.Tools.LogError(rawData);

                Regex regex = new Regex("<td colspan=\"12\">&nbsp;&nbsp; (?<id>[0-9]+)</td>");

                response = bot.RawUploadDataAndReturnResponse(url, "multipart/form-data; boundary=---------------------------7d8673912703c2", rawData, null, null);
//                LODataAccess.Tools.LogError("ImportResponse=" + response);

                Match match = regex.Match(response);
                if (match.Success) 
                {
                    ltStatus.Text = "IMPORT SUCCESSFUL: Pre-RegisterID=" + match.Groups["id"].Value;
                    m_loanNumber = match.Groups["id"].Value;
                } 
                else 
                {
                    ltStatus.Text = "WARNING: Unable to find Pre-REgisterID";
                }

                response = bot.DownloadData(GetLogoutUrl(isProduction));
//                LODataAccess.Tools.LogBug("LogoutResponse=" + response);
            } 
            catch (LODataAccess.CBaseException exc) 
            {
                ltStatus.Text = "ERROR WHILE EXPORT TO CITI: " + exc.UserMessage;
                ret = false;
            }
            return ret;

        }
	}


    public  class AbstractWebBot
    {
        private const string BotUserAgentString = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.0.3705)" ;
        private CookieContainer m_cookieContainer = null;
        private bool m_allowAutoRedirect = true;

        private HttpStatusCode m_httpStatusCode = HttpStatusCode.OK;
        private WebHeaderCollection m_webHeaderCollection = null;

        /// <summary>
        /// Get the Http Status Code of the last request.
        /// </summary>
        public HttpStatusCode HttpStatusCode 
        {
            get { return m_httpStatusCode; }
        }

        public WebHeaderCollection WebHeaderCollection 
        {
            get { return m_webHeaderCollection; }
        }

        public bool IsRedirection 
        {
            get 
            {
                if (m_httpStatusCode == HttpStatusCode.Found ||
                    m_httpStatusCode == HttpStatusCode.Redirect ||
                    m_httpStatusCode == HttpStatusCode.Moved ||
                    m_httpStatusCode == HttpStatusCode.MovedPermanently) 
                {
                    return true;
                }
                return false;
            }
        }
        public string RedirectionUrl 
        {
            get 
            {
                if (null != m_webHeaderCollection) 
                {
                    string url = m_webHeaderCollection["location"];
                    if (null != url)
                        return url.TrimWhitespaceAndBOM();
                }
                return "";
            }
        }
        protected bool AllowAutoRedirect 
        {
            get { return m_allowAutoRedirect; }
            set { m_allowAutoRedirect = value; }
        }

        public AbstractWebBot() 
        {
            m_cookieContainer = new CookieContainer();
        }

        public string GetCookieValue(string url, string cookieName) 
        {
            CookieCollection coll = m_cookieContainer.GetCookies(new Uri(url));
            Cookie cookie = coll[cookieName];
            if (null != cookie)
                return cookie.Value;
            return "";
        }
        public void SetCookie(string url, string cookieName, string value) 
        {
            m_cookieContainer.Add(new Cookie(cookieName, value, "/", url));
        }
        public void DebugCookieContainer(Uri url) 
        {
            CookieCollection coll = m_cookieContainer.GetCookies(url);
            LogInfo("Cookies for " + url);
            foreach (Cookie c in coll) 
            {
                LogInfo("Cookie[" + c.Name + "] = [" + c.Value + "]");
            }
        }
        private HttpWebRequest CreateRequest(string url, string method) 
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            //			request.ServicePoint.ConnectionLimit = 50;
            request.UserAgent = BotUserAgentString;
            request.KeepAlive = false;
            request.Method = method;
            request.AllowAutoRedirect = m_allowAutoRedirect;

            if (method == "POST") 
            {
                request.ContentType = "application/x-www-form-urlencoded";
            }
            request.CookieContainer = m_cookieContainer;

            return request;
        }
        private HttpWebResponse GetResponse(HttpWebRequest request) 
        {
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            m_httpStatusCode = response.StatusCode;
            m_webHeaderCollection = response.Headers;

            return response;
        }

        protected void LogInfo(string msg) 
        {
        }
        public string RawUploadDataAndReturnResponse(string url, string contentType, string rawData, NameValueCollection headersCollection, string refererUrl) 
        {
			
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(rawData);
            
            HttpWebRequest request = CreateRequest(url, "POST");
            request.ContentType = contentType;
            request.ContentLength = data.Length;
            request.KeepAlive = false;  // 2/20/08 db - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.

            if (null != refererUrl)
                request.Referer = refererUrl;

            if (null != headersCollection) 
            {
                request.Headers.Add(headersCollection);
            }
            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {

                    return GetString(stream);
                }
            }
        }
        public string RawUploadDataAndReturnResponse(string url, string contentType, string rawData, NameValueCollection headersCollection) 
        {
            return RawUploadDataAndReturnResponse(url, contentType, rawData, null, null);
        }
        public string RawUploadDataAndReturnResponse(string url, string contentType, string rawData) 
        {
            return RawUploadDataAndReturnResponse(url, contentType, rawData, null);
        }
        public void UploadData(string url, NameValueCollection postData) 
        {
            UploadData(url, postData, null);
        }

        public void UploadData(string url, NameValueCollection postData, string refererUrl) 
        {
            StringBuilder sb = new StringBuilder();
            bool bFirst = true;
            foreach (string key in postData.Keys) 
            {
                if (!bFirst) 
                {
                    sb.Append("&");
                }
                sb.AppendFormat("{0}={1}", key, UrlEncode(postData[key]));
                bFirst = false;

            }

            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(sb.ToString());
            
            HttpWebRequest request = CreateRequest(url, "POST");
            request.KeepAlive = false;  // 2/20/08 db - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
			
            request.ContentLength = data.Length;
            if (null != refererUrl)
                request.Referer = refererUrl;
            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                return; // NO-OP
            }


        }


        public string UploadDataReturnResponse(string url, NameValueCollection postData) 
        {
            return UploadDataReturnResponse(url, postData, null);
        }
        public string UploadDataReturnResponse(string url, NameValueCollection postData, string refererUrl) 
        {
            StringBuilder sb = new StringBuilder();
            bool bFirst = true;
            foreach (string key in postData.Keys) 
            {
                if (!bFirst) 
                {
                    sb.Append("&");

                }

                sb.AppendFormat("{0}={1}", key, UrlEncode(postData[key]));
                bFirst = false;
            }

            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(sb.ToString());
            
            HttpWebRequest request = CreateRequest(url, "POST");
            request.KeepAlive = false;  // 2/20/08 db - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            request.ContentLength = data.Length;
            if (null != refererUrl)
                request.Referer =refererUrl;

            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {
                    return GetString(stream);
                }
            }

        }

        public static string UrlEncode(string str) 
        {
            if (null == str)
                return "";

            StringBuilder sb = new StringBuilder();

            foreach (char ch in str.ToCharArray()) 
            {
                if (ch == ' ') 
                    sb.Append('+');
                else if (IsSafeChar(ch))
                    sb.Append(ch);
                else
                    sb.Append(ConvertToHex(ch));
            }
            return sb.ToString();


        }
        private static bool IsSafeChar(char ch) 
        {
            if (char.IsLetterOrDigit(ch))
                return true;
            switch (ch) 
            {
                case '\'':
                case '(':
                case ')':
                case '*':
                case '-':
                case '.':
                case '!':
                case '_':
                    return true;
                default:
                    return false;
            }
        }
        private static string ConvertToHex(char ch) 
        {

            ushort _v = (ushort) ch;
            string s = "%" + IntToHex((_v >> 4) & 15) + IntToHex(_v & 15);
            return s;
            
        }
        private static char IntToHex(int i) 
        {
            if (i < 10)
                return (char) (i + 48);
            else
                return (char) (i + 55);
        }
        /// <summary>
        /// Just go to the website without need to return html.
        /// </summary>
        /// <param name="url"></param>
        public void GoToUrl(string url) 
        {
            GoToUrl(url, null);
        }

        public void GoToUrl(string url, string refererUrl) 
        {
            HttpWebRequest request = CreateRequest(url, "GET");
            if (null != refererUrl)
                request.Referer = refererUrl;

            using (WebResponse response = GetResponse(request)) 
            {
                ; // NO-OP
            }


        }

        public string DownloadData(string url) 
        {
            return DownloadData(url, null);
        }

        /// <summary>
        /// Go to url and return HTML content
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string DownloadData(string url, string refererUrl) 
        {
            HttpWebRequest request = CreateRequest(url, "GET");
            if (null != refererUrl)
                request.Referer = refererUrl;

            using (WebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {
                    return GetString(stream);
                }
            }
        }

        private void DebugResponse(WebResponse response) 
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Url={0}{1}",response.ResponseUri, Environment.NewLine);
            sb.AppendFormat("ContentType={0}{1}", response.ContentType, Environment.NewLine);
            foreach (string key in response.Headers.Keys) 
            {
                sb.AppendFormat("Headers[{0}]=[{1}]{2}", key, response.Headers[key], Environment.NewLine);
            }
//            LogInfo(sb.ToString());
        }

        private void WriteToFile(string sFileName, Stream str)
        {
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream fs)
            {
                str.CopyTo(fs.Stream);
            };

            BinaryFileHelper.OpenNew(sFileName, writeHandler);
        }

        private string GetString(Stream stream) 
        {
            const int BUFFER_SIZE = 50000;
            byte[] buffer = new byte[BUFFER_SIZE];
            int n = 0;

            StringBuilder sb = new StringBuilder();
            while ((n = stream.Read(buffer, 0, BUFFER_SIZE)) > 0) 
            {
                sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, n));
            }
            return sb.ToString();
        }

        protected void WriteToFile(string sFileName, byte[] rgb)
        {
            BinaryFileHelper.WriteAllBytes(sFileName, rgb);
        }
    }
}
