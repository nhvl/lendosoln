using System;
using System.Drawing;
using System.Web.UI;
using DataAccess;
using LendersOffice.Common;
using PriceMyLoan.los.DataAccess;



namespace PriceMyLoan.main
{
	public partial class AddLiability : PriceMyLoan.UI.BasePage
	{
        private Guid m_appId = Guid.Empty;
		protected bool Skip = false;

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

		protected void PageInit(object sender, System.EventArgs e) 
		{
			ComZip.SmartZipcode(ComCity, ComState);
		}


		protected void PageLoad(object sender, System.EventArgs e)
		{
            string sAppId = null;
            try
            {
                sAppId = RequestHelper.GetSafeQueryString("appid");
                m_appId = new Guid(sAppId);
            }
            catch (Exception exc)
            {
                if (sAppId == null)
                    sAppId = "null";

                Tools.LogError("PML Add Liability. Error trying to parse 'appid' guid from query string. Found value: '" + sAppId + "'.");
                throw new CBaseException("Cannot add a liability without a valid application id.", exc);
            }

			if (!Page.IsPostBack) 
			{
				Tools.Bind_DebtT(DebtT);
				m_btnOK.Attributes.Add("onclick", "f_btnClick();");
				Skip = (LoanID == Guid.Empty);
				if (Skip)
					disableWebControls();
			}
			m_error.Visible = false;
		}

		private void disableWebControls()
		{
			m_btnOK.Enabled = false;
			OwnerT.Enabled = false;
			DebtT.Enabled = false;
			ComNm.Enabled = false;
			Bal.Enabled = false;
			Pmt.Enabled = false;
			WillBePdOff.Enabled = false;
			UsedInRatio.Enabled = false;
			ComAddr.Enabled = false;
			ComCity.Enabled = false;
			ComState.Enabled = false;
			ComZip.Enabled = false;
			AccNm.Enabled = false;
			AccNum.Enabled = false;

			ComCity.BackColor = Color.LightGray;
			ComAddr.BackColor = Color.LightGray;
			Pmt.BackColor = Color.LightGray;
			Bal.BackColor = Color.LightGray;
			ComNm.BackColor = Color.LightGray;
			ComZip.BackColor = Color.LightGray;
			AccNm.BackColor = Color.LightGray;
			AccNum.BackColor = Color.LightGray;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Init += new System.EventHandler(this.PageInit);
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

		

		protected void m_btnOK_Click(object sender, System.EventArgs e)
		{
			if (Skip)
			{
				PriceMyLoan.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] {"OK=true"});
				return;
			}

            try
            {
                int version = RequestHelper.GetInt("version");
                PmlAddLiabilityData newLiability = new PmlAddLiabilityData(LoanID, m_appId, PriceMyLoanUser);
                newLiability.Add(version,
                      (E_DebtRegularT)Int32.Parse(DebtT.Items[DebtT.SelectedIndex].Value)
                    , (E_LiaOwnerT)OwnerT.SelectedIndex
                    , Bal.Text
                    , Pmt.Text
                    , ComNm.Text
                    , ComAddr.Text
                    , ComCity.Text
                    , ComState.Value
                    , ComZip.Text
                    , AccNm.Text
                    , AccNum.Text
                    , UsedInRatio.Checked
                    , WillBePdOff.Checked
                    );

                ClientScript.RegisterStartupScript(this.GetType(), "autoclose", "window.parent.LQBPopup.Return()", true);
            }
            catch (VersionMismatchException exc)
            {
                m_error.Visible = true;
                m_error.Text = exc.UserMessage;
                m_btnOK.Enabled = false;
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                m_error.Visible = true;
                m_error.Text = exc.UserMessage;
                m_btnOK.Enabled = false;
            }
            catch (CBaseException)
            {
                m_error.Visible = true;
                m_error.Text = "The liability could not be added.";
                m_btnOK.Enabled = false;
            }
		}
	}
}