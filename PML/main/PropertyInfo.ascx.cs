using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.Security;
using PriceMyLoan.Common;
using PriceMyLoan.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PriceMyLoan.UI.Main
{

    public partial class PropertyInfo : BaseUserControl, ITabUserControl
    {
        protected bool m_isAsk3rdPartyUwResultInPml = false;
        protected bool m_isShowPriceGroupInEmbeddedPml = false;
        protected bool m_isShowsProdEstimatedResidualI = false;
        protected bool m_isOptionARMUsedInPml = false;  // 01/23/08 mf. OPM 19872
        protected E_sLtv80TestResultT sLtv80TestResultT;
        protected bool m_is8020ComboAllowedInPml = false;
        protected bool m_isTotalEnabled = false;
        protected bool m_isAutoPmiOrNewPml = false; // OPM 109393
        protected bool m_usingNewEngine = false;

        protected bool m_showCreditQualifying = false;

        private const string SortCookieName = "ResultSortOrder";

        protected bool m_renderAsSecond;
        protected bool m_missingCounty = true;
        protected bool m_useOriginatorComp = false;
        protected bool m_usePmi = false;

        private string currentCounty;
        protected bool m_EnableAusProcessingClick;
        protected AbstractUserPrincipal CurrentUser
        {
            get
            {

                return Page.User as AbstractUserPrincipal;
            }
        }

        private void AssociateLabel(HtmlGenericControl label, Control control)
        {
            label.Attributes.Add("for", control.ClientID);
        }
        protected void PageInit(object sender, System.EventArgs e)
        {
            #region Associate label with corresponding control
            var controlLabelList = new KeyValuePair<HtmlGenericControl, Control>[] {
                new KeyValuePair<HtmlGenericControl, Control>(sOccTPeLabel, sOccTPe),
                new KeyValuePair<HtmlGenericControl, Control>(sProdSpTLabel, sProdSpT),
                new KeyValuePair<HtmlGenericControl, Control>(sProdSpStructureTLabel, sProdSpStructureT),
                new KeyValuePair<HtmlGenericControl, Control>(sProdCondoStoriesLabel, sProdCondoStories),
                new KeyValuePair<HtmlGenericControl, Control>(sFhaCondoApprovalStatusTLabel, sFhaCondoApprovalStatusT),
                new KeyValuePair<HtmlGenericControl, Control>(sProdIsSpInRuralAreaLabel, sProdIsSpInRuralArea),
                new KeyValuePair<HtmlGenericControl, Control>(sProdIsCondotelLabel, sProdIsCondotel),
                new KeyValuePair<HtmlGenericControl, Control>(sProdIsNonwarrantableProjLabel, sProdIsNonwarrantableProj),
                new KeyValuePair<HtmlGenericControl, Control>(sOccRPeLabel, sOccRPe),
                new KeyValuePair<HtmlGenericControl, Control>(sSpGrossRentPeLabel, sSpGrossRentPe),
                new KeyValuePair<HtmlGenericControl, Control>(aPresOHExpPeLabel, aPresOHExpPe),
                new KeyValuePair<HtmlGenericControl, Control>(sLPurposeTPeLabel, sLPurposeTPe),
                new KeyValuePair<HtmlGenericControl, Control>(sIsCreditQualifyingLabel, sIsCreditQualifying),
                new KeyValuePair<HtmlGenericControl, Control>(sSpLienLabel, sSpLien),
                new KeyValuePair<HtmlGenericControl, Control>(sFHASalesConcessionsLabel, sFHASalesConcessions),
                new KeyValuePair<HtmlGenericControl, Control>(sProdCashoutAmtLabel, sProdCashoutAmt),
                new KeyValuePair<HtmlGenericControl, Control>(sHas1stTimeBuyerPeLabel, sHas1stTimeBuyerPe),
                new KeyValuePair<HtmlGenericControl, Control>(sProdHasHousingHistoryLabel, sProdHasHousingHistory),
                new KeyValuePair<HtmlGenericControl, Control>(sOtherLFinMethTLabel, sOtherLFinMethT),
                new KeyValuePair<HtmlGenericControl, Control>(sLpIsNegAmortOtherLienLabel, sLpIsNegAmortOtherLien),
                new KeyValuePair<HtmlGenericControl, Control>(sProdImpoundLabel, sProdImpound),
                new KeyValuePair<HtmlGenericControl, Control>(sProdDocTLabel, sProdDocT),
                new KeyValuePair<HtmlGenericControl, Control>(sIsPrimaryWageEarnerLabel, sIsPrimaryWageEarner),
                new KeyValuePair<HtmlGenericControl, Control>(sPrimAppTotNonspIPeLabel, sPrimAppTotNonspIPe),
                new KeyValuePair<HtmlGenericControl, Control>(sProdAvailReserveMonthsLabel, sProdAvailReserveMonths),
                new KeyValuePair<HtmlGenericControl, Control>(sProdRLckdDaysLabel, sProdRLckdDays),
                new KeyValuePair<HtmlGenericControl, Control>(sHasAppraisalLabel, sHasAppraisal),
                new KeyValuePair<HtmlGenericControl, Control>(sOriginalAppraisedValueLabel, sOriginalAppraisedValue),
                new KeyValuePair<HtmlGenericControl, Control>(sHouseValPeLabel, sHouseValPe),
                new KeyValuePair<HtmlGenericControl, Control>(sLtvRPeLabel, sLtvRPe),
                new KeyValuePair<HtmlGenericControl, Control>(sProOFinBalPeLabel, sProOFinBalPe),
                new KeyValuePair<HtmlGenericControl, Control>(sCltvRPeLabel, sCltvRPe),
                new KeyValuePair<HtmlGenericControl, Control>(sProdLpePriceGroupNmLabel, sProdLpePriceGroupNm),
                new KeyValuePair<HtmlGenericControl, Control>(sProdMIOptionTLabel, sProdMIOptionT),
                new KeyValuePair<HtmlGenericControl, Control>(sProdConvMIOptionTLabel, sProdConvMIOptionT ),
                new KeyValuePair<HtmlGenericControl, Control>(sProdIsFhaMipFinancedLabel, sProdIsFhaMipFinanced),
                new KeyValuePair<HtmlGenericControl, Control>(sProOFinPmtPeLabel, sProOFinPmtPe),
                new KeyValuePair<HtmlGenericControl, Control>(sProdFilterDue10YrsLabel, sProdFilterDue10Yrs),
                new KeyValuePair<HtmlGenericControl, Control>(sProdFilterFinMethFixedLabel, sProdFilterFinMethFixed),
                new KeyValuePair<HtmlGenericControl, Control>(sConvSplitMIRTLabel, sConvSplitMIRT),
                new KeyValuePair<HtmlGenericControl, Control>(PaymentTypeLabel, sProdFilterPmtTPI)
            };
            foreach (var controlLabel in controlLabelList)
            {
                AssociateLabel(controlLabel.Key, controlLabel.Value);
            }
            #endregion

            Tools.Bind_sProdSpT(sProdSpT);
            Tools.Bind_sLPurposeT(sLPurposeTPe);
            Tools.Bind_aOccT(sOccTPe);
            Tools.Bind_sProdSpStructureT(sProdSpStructureT);
            Tools.Bind_sFhaCondoApprovalStatusT(sFhaCondoApprovalStatusT);
            Tools.Bind_sProdAvailReserveMonths(sProdAvailReserveMonths);
            Tools.Bind_sProdMIOptionT(sProdMIOptionT);
            Tools.Bind_sProdConvMIOptionT(sProdConvMIOptionT, CurrentBroker.EnabledBPMISinglePremium_Case115836, CurrentBroker.EnableBPMISplitPremium_Case115836 );
            Tools.Bind_sFinMethT(sOtherLFinMethT);
            Tools.Bind_PercentBaseLoanAmountsT(sOriginatorCompensationBorrPaidBaseT);
            Tools.Bind_sConvSplitMIRT(sConvSplitMIRT);

            // We must maintain the existing value of the loan's
            // documentation type when binding the dropdown. I
            // do not like having to do a separate InitLoad
            // outside of the LoadData/SaveData methods, but
            // this binding must happen on page init to ensure 
            // the dropdown maintains its values across postbacks. 
            // We're in the process of deprecating PML 1, so I don't 
            // believe this will present a performance issue.
            var loan = new CPageData(this.LoanID, nameof(PropertyInfo), new[] { nameof(CPageData.sProdDocT) });
            loan.InitLoad();

            Tools.Bind_sProdDocT(sProdDocT, this.CurrentBroker, loan.sProdDocT);

            SetRequiredValidatorMessage(sHouseValPeRequireValidator);
            SetRequiredValidatorMessage(sProdCashoutAmtValidator);
            SetRequiredValidatorMessage(sPrimAppTotNonspIPeValidator);
            SetRequiredValidatorMessage(sProdAvailReserveMonthsValidator);
            SetRequiredValidatorMessage(sProdRLckdDaysValidator);
            SetRequiredValidatorMessage(sLPurposeTPeValidator);
            SetRequiredValidatorMessage(sTotalRenovationCostsValidator);
            SetRequiredValidatorMessage(sProdMIOptionTValidator);
            SetRequiredValidatorMessage(sProdConvMIOptionTValidator);
            SetRequiredValidatorMessage(sProdFilterFinMethValidator);
            SetRequiredValidatorMessage(sProdFilterDueValidator);
            SetRequiredValidatorMessage(sLAmtCalcPeValidator);
            SetRequiredValidatorMessage(sProdEstimatedResidualIValidator);
            SetRequiredValidatorMessage(sProOFinPmtPeValidator);
            SetRequiredValidatorMessage(sProOFinBalPeValidator);
            SetRequiredValidatorMessage(sFHASalesConcessionsValidator);
            SetRequiredValidatorMessage(sOriginalAppraisedValueValidator);
            SetRequiredValidatorMessage(sSpAddrValidator);
            SetRequiredValidatorMessage(sSpZipValidator);
            SetRequiredValidatorMessage(sSpCityValidator);
            SetRequiredValidatorMessage(sPriorSalesDValidator);
            SetRequiredValidatorMessage(sPriorSalesSellerValidator);
            SetRequiredValidatorMessage(sPriorSalesPriceValidator);
            SetRequiredValidatorMessage(sApprValPeValidator);
            SetRequiredValidatorMessage(sNumFinancedPropertiesValidator);
            SetRequiredValidatorMessage(sSubFinPeValidator);
            SetRequiredValidatorMessage(sAsIsAppraisedValuePevalNegativeValidator);
            SetRequiredValidatorMessage(sProdFilterTypeValidator);
            SetRequiredValidatorMessage(sHouseValPeValidator);
            SetRequiredValidatorMessage(sConvSplitMIRTValidator);
            sSpZip.SmartZipcode(sSpCity, sSpStatePe, "sSpCounty");


            m_isAsk3rdPartyUwResultInPml = CurrentBroker.IsAsk3rdPartyUwResultInPml;
            m_isShowPriceGroupInEmbeddedPml = CurrentBroker.IsShowPriceGroupInEmbeddedPml;
            m_isShowsProdEstimatedResidualI = CurrentBroker.PmlRequireEstResidualIncome;
            m_isOptionARMUsedInPml = CurrentBroker.IsOptionARMUsedInPml;
            m_is8020ComboAllowedInPml = CurrentBroker.Is8020ComboAllowedInPml;

            m_showCreditQualifying = !CurrentBroker.IsIncomeAssetsRequiredFhaStreamline;

            //opm 34211 fs 10/30/09 - PML FHA TOTAL Scorecard
            if (CurrentBroker.IsTotalScorecardEnabled && CurrentUser.HasPermission(Permission.CanAccessTotalScorecard))
            {
                m_isTotalEnabled = true;
            }

            Tools.Bind_sPriorSalesPropertySeller(sPriorSalesSellerT);

            sProdEstimatedResidualIValidator.Enabled = m_isShowsProdEstimatedResidualI;

            if (m_isAsk3rdPartyUwResultInPml)
            {
                Tools.Bind_sProd3rdPartyUwResultT(sProd3rdPartyUwResultT);
            }

            resultSort.Items.Add("Note Rate");
            resultSort.Items.Add("Loan Program Name");

            resultSort.SelectedIndex = 1;


            main mainpage = Page as main;
            m_EnableAusProcessingClick = !IsReadOnly;

            if (mainpage == null)
            {
                Tools.LogError("NOTIFY ANTONIO: PropertyInfo Page is not main!");
            }
            //opm 11948 fs 07/30/08

            if (mainpage == null || mainpage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml))
            {
                sProdRLckdDays.Attributes["AlwaysEnable"] = "true";
                sProdRLckdDaysValidator.Attributes["AlwaysEnable"] = "true";
                sProdRLckdDaysDDL.Attributes["AlwaysEnable"] = "true";
            }

            if (mainpage != null && mainpage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) == false)
            {
                m_usingNewEngine = true;

                if (mainpage.CurrentUserCanPerform(WorkflowOperations.RunPmlForAllLoans))
                {
                    m_EnableAusProcessingClick = true;

                    sProdIncludeNormalProc.Attributes["AlwaysEnable"] = "true";
                    sProdIncludeMyCommunityProc.Attributes["AlwaysEnable"] = "true";
                    sProdIncludeHomePossibleProc.Attributes["AlwaysEnable"] = "true";
                    sProdIncludeFHATotalProc.Attributes["AlwaysEnable"] = "true";
                    sProdIncludeVAProc.Attributes["AlwaysEnable"] = "true";
                    sProdIncludeUSDARuralProc.Attributes["AlwaysEnable"] = "true";

                    sProdFilterDue10Yrs.Attributes["AlwaysEnable"] = "true";
                    sProdFilterDue15Yrs.Attributes["AlwaysEnable"] = "true";
                    sProdFilterDue20Yrs.Attributes["AlwaysEnable"] = "true";
                    sProdFilterDue25Yrs.Attributes["AlwaysEnable"] = "true";
                    sProdFilterDue30Yrs.Attributes["AlwaysEnable"] = "true";
                    sProdFilterDueOther.Attributes["AlwaysEnable"] = "true";
                    sProdFilterDueValidator.Attributes["AlwaysEnable"] = "true";

                    sProdFilterFinMethFixed.Attributes["AlwaysEnable"] = "true";
                    sProdFilterFinMethValidator.Attributes["AlwaysEnable"] = "true";
                    sProdFilterFinMeth3YrsArm.Attributes["AlwaysEnable"] = "true";
                    sProdFilterFinMeth5YrsArm.Attributes["AlwaysEnable"] = "true";
                    sProdFilterFinMeth7YrsArm.Attributes["AlwaysEnable"] = "true";
                    sProdFilterFinMeth10YrsArm.Attributes["AlwaysEnable"] = "true";
                    sProdFilterFinMethOther.Attributes["AlwaysEnable"] = "true";

                    sProdFilterPmtTPI.Attributes["AlwaysEnable"] = "true";
                    sProdFilterPmtTIOnly.Attributes["AlwaysEnable"] = "true";

                    sProdRLckdDays.Attributes["AlwaysEnable"] = "true";
                    sProdRLckdDaysValidator.Attributes["AlwaysEnable"] = "true";
                    sProdRLckdDaysDDL.Attributes["AlwaysEnable"] = "true";


                }
                else if (mainpage.CurrentUserCanPerform(WorkflowOperations.RunPmlForRegisteredLoans))
                {
                    sProdRLckdDays.Attributes["AlwaysEnable"] = "true";
                    sProdRLckdDaysValidator.Attributes["AlwaysEnable"] = "true";
                    sProdRLckdDaysDDL.Attributes["AlwaysEnable"] = "true";
                }
            }

            BestPricingOptionPanel.Visible = CurrentBroker.IsBestPriceEnabled && !CurrentUser.IsAlwaysUseBestPrice;
            RegularPricingOptionPanel.Visible = !CurrentBroker.IsBestPriceEnabled;
            DOSubmissionPlaceHolder.Visible = IsAllowDoSubmission;
            DUSubmissionPlaceHolder.Visible = IsAllowDuSubmission;
            LPSubmissionPlaceHolder.Visible = IsAllowLpSubmission;
            TotalSubmissionPlaceHolder.Visible = m_isTotalEnabled;
            Ask3rdPartyUwPlaceHolder.Visible = m_isAsk3rdPartyUwResultInPml;

            sProd3rdPartyUwResultTLink.Disabled = IsReadOnly;
            UwResultModify.Visible = IsAllowDoSubmission || IsAllowDuSubmission;

            ProdIncludeModify.Visible = IsAllowDoSubmission || IsAllowDuSubmission;
            sProdIncludeModifyLink.Disabled = !m_EnableAusProcessingClick;

            sProdIsDuRefiPlusLink.Visible = IsAllowDuSubmission || IsAllowDoSubmission;
            sProdIsDuRefiPlusLink.Disabled = IsReadOnly;

            sIsRenovationLoanPanel.Visible = this.CurrentBroker.IsEnableRenovationCheckboxInPML || this.CurrentBroker.EnableRenovationLoanSupport;
        }


        public string TabID
        {
            get { return "PROPINFO"; }
        }
        public void LoadData()
        {
            CPageData dataLoan = new CPropertyInfoData(LoanID);
            dataLoan.InitLoad();
            monthlyexpenses.Visible = !IsReadOnly;
            m_useOriginatorComp = dataLoan.sIsQualifiedForOriginatorCompensation && CurrentBroker.IsDisplayCompensationChoiceInPml;
            // 8/13/07 OPM 4442. We are supporting stand-alone seconds.
            // We will have two versions of this screen based on the lien
            // position of the file.  Seconds have a different data set.

            m_renderAsSecond = dataLoan.sLienPosT == E_sLienPosT.Second || dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sIsLineOfCredit;
            m_usePmi = dataLoan.BrokerDB.HasEnabledPMI;
            (this.Page as BasePage).RegisterJsGlobalVariables("IsPmiEnabled", m_usePmi);
            (this.Page as BasePage).RegisterJsGlobalVariables("IsStandAloneSecond", dataLoan.sIsStandAlone2ndLien);

            m_isAutoPmiOrNewPml = CurrentBroker.HasEnabledPMI || dataLoan.IsNewPMLEnabled;

            if (dataLoan.sPml1stVisitPropLoanStepD_rep == "")
            {
                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitPropLoanStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();

                dataLoan = new CPropertyInfoData(LoanID);
                dataLoan.InitLoad();
            }

            sFileVersion = dataLoan.sFileVersion;
            if (PriceMyLoanUser.IsPricingMultipleAppsSupported)
            {
                setPrimaryBorrowerDropdown(sIsPrimaryWageEarner, dataLoan);
            }
            sIsPrimaryWageEarner.Visible = PriceMyLoanUser.IsPricingMultipleAppsSupported;


            CAppData dataApp = dataLoan.GetAppData(0);

            if (PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML && m_isShowPriceGroupInEmbeddedPml)
            {
                sProdLpePriceGroupNm.Text = dataLoan.sProdLpePriceGroupNm;
            }

            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpZip.Text = dataLoan.sSpZip;

            //01-10-08 av opm 18913 Populate the new dropdownlist with counties in the state and select the one that matches it. 

            currentCounty = dataLoan.sSpCounty;
            counties.DataSource = StateInfo.Instance.GetCountiesFor(dataLoan.sSpStatePe);
            counties.DataBind();

            sSpStatePe.Value = dataLoan.sSpStatePe;

            if (!m_renderAsSecond)
            {
                Tools.SetDropDownListValue(sLPurposeTPe, dataLoan.sLPurposeTPe);
            }
            else
            {
                sPriorSalesDValidator.Enabled = false;
                sPriorSalesPriceValidator.Enabled = false;
                sPriorSalesSellerValidator.Enabled = false;
            }
            //opm 28103 fs 04/06/10
            sSpLien.Text = dataLoan.sSpLien_rep;

            // 1/7/2010 dd - OPM 44203 - sIsCreditQualifying does not depend on "Are Income and Asset Require .." broker permission.
            //if (m_showCreditQualifying)
            //{
            sIsCreditQualifying.Checked = dataLoan.sIsCreditQualifying; //opm 42582 fs 11/18/09
            //}

            //opm 32180 fs 11/28/09
            bool bIsCredQualifying = dataLoan.sIsCreditQualifying;
            dataLoan.sIsCreditQualifying = true;
            sPrimAppTotNonspIPe.Text = dataLoan.sPrimAppTotNonspIPe_rep;
            sProdEstimatedResidualI.Text = dataLoan.sProdEstimatedResidualI_rep;
            Tools.SetDropDownListValue(sProdAvailReserveMonths, dataLoan.sProdAvailReserveMonths_rep);
            dataLoan.sIsCreditQualifying = bIsCredQualifying;
            sProdIsLoanEndorsedBeforeJune09.Checked = dataLoan.sProdIsLoanEndorsedBeforeJune09;
            bHeloc2ndFinancing.Checked = false;
            bCloseEnd2ndFinancing.Checked = false;
            sHCLTVRPe.Text = dataLoan.sHCLTVRPe_rep;
            
            switch (dataLoan.sSubFinT)
            {
                case E_sSubFinT.CloseEnd:
                    bCloseEnd2ndFinancing.Checked = true;
                    break;
                case E_sSubFinT.Heloc:
                    bHeloc2ndFinancing.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sSubFinT);
            }


            sSubFinToPropertyValue.Text = dataLoan.sSubFinToPropertyValue_rep;
            sSubFinPe.Text = dataLoan.sSubFinPe_rep;

            sRequestCommunityOrAffordableSeconds.Checked = dataLoan.sRequestCommunityOrAffordableSeconds;

            sProdFhaUfmip.Text = dataLoan.sProdFhaUfmip_rep;
            sProdIsTexas50a6Loan.Checked = dataLoan.sProdIsTexas50a6Loan;
            sPreviousLoanIsTexas50a6Loan.Checked = dataLoan.sPreviousLoanIsTexas50a6Loan;

            sFHASalesConcessions.Text = dataLoan.sFHASalesConcessions_rep;
            sHasAppraisal.Checked = dataLoan.sHasAppraisal;
            sOriginalAppraisedValue.Text = dataLoan.sOriginalAppraisedValue_rep;

            sPriorSalesPrice.Text = dataLoan.sPriorSalesPrice_rep;
            sPriorSalesD.Text = dataLoan.sPriorSalesD_rep;
            if (string.IsNullOrEmpty(sPriorSalesD.Text))
            {
                sPriorSalesD.Text = "mm/dd/yyyy";
            }
            Tools.SetDropDownListValue(sPriorSalesSellerT, dataLoan.sPriorSalesPropertySellerT);


            #region av 4/07 va funding fee
            sProdIsVaFundingFinanced.Checked = dataLoan.sProdIsVaFundingFinanced;
            sProdVaFundingFee.Text = dataLoan.sProdVaFundingFee_rep;
            #endregion

            sProdIsUsdaRuralHousingFeeFinanced.Checked = dataLoan.sProdIsUsdaRuralHousingFeeFinanced;

            Tools.SetDropDownListValue(sOccTPe, dataLoan.sOccTPe);
            Tools.SetDropDownListValue(sProdSpStructureT, dataLoan.sProdSpStructureT);
            Tools.SetDropDownListValue(sFhaCondoApprovalStatusT, dataLoan.sFhaCondoApprovalStatusT);
            sLAmtCalcPe.Text = dataLoan.sLAmtCalcPe_rep;
            sProOFinBalPe.Text = dataLoan.sProOFinBalPe_rep;
            sProdCashoutAmt.Text = dataLoan.sProdCashoutAmt_rep;
            sProdRLckdDays.Text = dataLoan.sProdRLckdDays_rep;
            LoadLockPeriodOptionsAndSetTextboxValue(dataLoan);
            sProdRLckdExpiredDLabel.Text = dataLoan.sProdRLckdExpiredDLabel;

            sIsRenovationLoan.Checked = dataLoan.sIsRenovationLoan;
            sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep;
            sAsIsAppraisedValuePeval.Text = dataLoan.sAsIsAppraisedValuePeval_rep;

            sHouseValPe.Text = dataLoan.sHouseValPe_rep;
            sDownPmtPcPe.Text = dataLoan.sDownPmtPcPe_rep;
            sEquityPe.Text = dataLoan.sEquityPe_rep;
            sLtvRPe.Text = dataLoan.sLtvRPe_rep;
            sCltvRPe.Text = dataLoan.sCltvRPe_rep;
            sLtvROtherFinPe.Text = dataLoan.sLtvROtherFinPe_rep;
            sApprValPe.Text = dataLoan.sApprValPe_rep;

            aPresOHExpPe.Text = (PriceMyLoanUser.IsPricingMultipleAppsSupported) ? dataLoan.sPresOHExpPe_rep : dataApp.aPresOHExpPe_rep; //opm 25872 fs 05/06/09

            sOccRPe.Text = dataLoan.sOccRPe_rep;
            sSpGrossRentPe.Text = dataLoan.sSpGrossRentPe_rep;
            
            
            sProRealETxPe.Value= dataLoan.sProRealETxPe_rep;
            sProHazInsPe.Value= dataLoan.sProHazInsPe_rep;
            sProHoAssocDuesPe.Value= dataLoan.sProHoAssocDuesPe_rep;
            sProMInsPe.Value= dataLoan.sProMInsPe_rep;
            sProOHExpPe.Value= dataLoan.sProOHExpPe_rep;
            sProOHExpDescPe.Value = dataLoan.sProOHExpDesc;
            sMonthlyPmtPe.Text = dataLoan.sMonthlyPmtPe_rep;

            sProdIsSpInRuralArea.Checked = dataLoan.sProdIsSpInRuralArea;
            if (!m_renderAsSecond)
                sProdImpound.Checked = dataLoan.sProdImpound;
            sProdIsCondotel.Checked = dataLoan.sProdIsCondotel;
            sProdIsNonwarrantableProj.Checked = dataLoan.sProdIsNonwarrantableProj;

            if (!m_renderAsSecond)
                sHas1stTimeBuyerPe.Checked = dataLoan.sHas1stTimeBuyerPe;
            if (!m_renderAsSecond)
            {
                Tools.SetDropDownListValue(sProdMIOptionT, dataLoan.sProdMIOptionT);
                Tools.SetDropDownListValue(sProdConvMIOptionT, dataLoan.sProdConvMIOptionT);
                Tools.SetDropDownListValue(sConvSplitMIRT, dataLoan.sConvSplitMIRT);
            }
            sLtv80TestResultT = dataLoan.sLtv80TestResultT;
            if (!m_renderAsSecond)
            {
                sProdHasHousingHistory.Checked = dataLoan.sProdHasHousingHistory;
                sProdIsFhaMipFinanced.Checked = dataLoan.sProdIsFhaMipFinanced; //opm 28086 fs 03/03/09
            }
            sNumFinancedProperties.Text = dataLoan.sNumFinancedProperties_rep;

            if (m_useOriginatorComp)
            {
                sOriginatorCompensationPaymentSourceT_Borrower.Checked = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid;
                sOriginatorCompensationPaymentSourceT_Lender.Checked = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid;
                sOriginatorCompensationBorrPaidPc.Text = dataLoan.sOriginatorCompensationBorrPaidPc_rep;
                Tools.SetDropDownListValue(sOriginatorCompensationBorrPaidBaseT, dataLoan.sOriginatorCompensationBorrPaidBaseT);
                sOriginatorCompensationBorrPaidMb.Text = dataLoan.sOriginatorCompensationBorrPaidMb_rep;
            }

            sProdFilterDue10Yrs.Checked = dataLoan.sProdFilterDue10Yrs;
            sProdFilterDue15Yrs.Checked = dataLoan.sProdFilterDue15Yrs;
            sProdFilterDue20Yrs.Checked = dataLoan.sProdFilterDue20Yrs;
            sProdFilterDue25Yrs.Checked = dataLoan.sProdFilterDue25Yrs;
            sProdFilterDue30Yrs.Checked = dataLoan.sProdFilterDue30Yrs;
            sProdFilterDueOther.Checked = dataLoan.sProdFilterDueOther;
            sProdFilterFinMethFixed.Checked = dataLoan.sProdFilterFinMethFixed;
            sProdFilterFinMeth3YrsArm.Checked = dataLoan.sProdFilterFinMeth3YrsArm;
            sProdFilterFinMeth5YrsArm.Checked = dataLoan.sProdFilterFinMeth5YrsArm;
            sProdFilterFinMeth7YrsArm.Checked = dataLoan.sProdFilterFinMeth7YrsArm;
            sProdFilterFinMeth10YrsArm.Checked = dataLoan.sProdFilterFinMeth10YrsArm;
            sProdFilterFinMethOther.Checked = dataLoan.sProdFilterFinMethOther;
            sProdFilterPmtTPI.Checked = dataLoan.sProdFilterPmtTPI;
            sProdFilterPmtTIOnly.Checked = dataLoan.sProdFilterPmtTIOnly;

            // We store the filter to cookie by loanid
            // 08/21/07 mf - OPM 17579 We remove the ability to sort by teaser rate
            HttpCookie cookie = Request.Cookies.Get(SortCookieName);
            if (cookie != null)
            {
                switch (cookie.Value)
                {
                    case PriceMyLoanConstants.SortType_ByNoteRate: resultSort.SelectedIndex = 0; break;
                    case PriceMyLoanConstants.SortType_ByName: resultSort.SelectedIndex = 1; break;
                    case PriceMyLoanConstants.SortType_BestPrice:
                        resultSort.SelectedIndex = 1;

                        if (this.CurrentBroker.IsBestPriceEnabled)
                        {
                            BestPricingCb.Checked = true;
                        }
                        break;
                    default:
                        Tools.LogBug("Unhandled sort option. Sort Option=" + cookie.Value);
                        resultSort.SelectedIndex = 1;
                        break;
                }
            }

            if (BrokerID == new Guid("B94B7017-9E70-4DE7-A316-B5CD7D1E34BB"))
            {
                BestPricingCb.Checked = true;
            }

            Page.ClientScript.RegisterHiddenField("sProdCalcEntryT", dataLoan.sProdCalcEntryT.ToString("D"));
            Page.ClientScript.RegisterHiddenField("Launch", "");
            Page.ClientScript.RegisterHiddenField("LpEntry", "");
            Page.ClientScript.RegisterHiddenField("_PreviousScroll", RequestHelper.GetSafeQueryString("_PreviousScroll"));            

            sProdCondoStories.Text = dataLoan.sProdCondoStories_rep;
            if (dataLoan.sProdSpT == E_sProdSpT.Townhouse)
            {
                // 7/8/2005 dd - OPM 2270.
                Tools.SetDropDownListValue(sProdSpT, E_sProdSpT.PUD);
            }
            else
            {
                Tools.SetDropDownListValue(sProdSpT, dataLoan.sProdSpT);
            }
            Tools.SetDropDownListValue(sProdDocT, dataLoan.sProdDocT);
            //            Tools.SetDropDownListValue(sProdFinMethFilterT, dataLoan.sProdFinMethFilterT);

            if (m_isAsk3rdPartyUwResultInPml)
            {
                Tools.SetDropDownListValue(sProd3rdPartyUwResultT, dataLoan.sProd3rdPartyUwResultT);
                sProdIncludeMyCommunityProc.Checked = dataLoan.sProdIncludeMyCommunityProc;
                sProdIncludeHomePossibleProc.Checked = dataLoan.sProdIncludeHomePossibleProc;
                sProdIncludeNormalProc.Checked = dataLoan.sProdIncludeNormalProc;
                sProdIncludeFHATotalProc.Checked = dataLoan.sProdIncludeFHATotalProc;
                sProdIncludeVAProc.Checked = dataLoan.sProdIncludeVAProc;
                sProdIncludeUSDARuralProc.Checked = dataLoan.sProdIncludeUSDARuralProc;
                sProdIsDuRefiPlus.Checked = dataLoan.sProdIsDuRefiPlus;

            }

            if (string.IsNullOrEmpty(dataLoan.sDuFindingsHtml))
            {
                lnkViewDoFindings.Style.Add("display", "none");
                lnkViewDuFindings.Style.Add("display", "none");
            }
            else
            {
                ltrViewDoFindings.Style.Add("display", "none");
                ltrViewDuFindings.Style.Add("display", "none");

            }
            if (string.IsNullOrEmpty(dataLoan.sFreddieFeedbackResponseXml.Value))
            {
                btnViewLp.Style.Add("display", "none");
            }
            else
            {
                ltrViewLp.Style.Add("display", "none");
            }

            if (string.IsNullOrEmpty(dataLoan.sTotalScoreCertificateXmlContent.Value))
            {
                btnViewFindings.Style.Add("display", "none");
            }
            else
            {
                ltrViewFindings.Style.Add("display", "none");
            }
            //ltrViewDoFindings.Style.Add("display", string.IsNullOrEmpty(dataLoan.sDuFindingsHtml) ? "" : "none");
            //lnkViewDoFindings.Style.Add("display", string.IsNullOrEmpty(dataLoan.sDuFindingsHtml) ? "none" : "");
            //ltrViewDuFindings.Style.Add("display", string.IsNullOrEmpty(dataLoan.sDuFindingsHtml) ? "" : "none");
            //lnkViewDuFindings.Style.Add("display", string.IsNullOrEmpty(dataLoan.sDuFindingsHtml) ? "none" : "");
            //ltrViewLp.Style.Add("display", string.IsNullOrEmpty(dataLoan.sFreddieFeedbackResponseXml) ? "" : "none");
            //btnViewLp.Style.Add("display", string.IsNullOrEmpty(dataLoan.sFreddieFeedbackResponseXml) ? "none" : "");
            //ltrViewFindings.Style.Add("display", string.IsNullOrEmpty(dataLoan.sTotalScoreCertificateXmlContent) ? "" : "none");
            //btnViewFindings.Style.Add("display", string.IsNullOrEmpty(dataLoan.sTotalScoreCertificateXmlContent) ? "none" : "");

            // 01/16/06 mf. OPM 8339. On the client, we need to know the status of the 
            // mortgage tradelines on the credit report to properly render the FTHB controls.
            if (IsReadOnly == false)
            {
                GetMortTradelines(dataLoan);     //opm 25872: TODO: tradelines needs to be in dataLoan or computed at loan level.
            }

            if (m_renderAsSecond)
            {
                Tools.SetDropDownListValue(sOtherLFinMethT, dataLoan.sOtherLFinMethT);
                sLpIsNegAmortOtherLien.Checked = dataLoan.sLpIsNegAmortOtherLien;
                sProOFinPmtPe.Text = dataLoan.sProOFinPmtPe_rep;
            }

            string sProd3rdPartyUwResultTEnabled = "1";
            string sProdIncludeEnabled = "1";
            string sProdIsDuRefiPlusEnabled = "1";
            if (IsAllowDoSubmission || IsAllowDuSubmission)
            {
                // 10/26/2007 dd - OPM 17672 - We currently disable the two drop downs for DO/DU integration in PML, to prevent user from randomly modify their data.
                sProd3rdPartyUwResultT.Enabled = false;
                sProdIncludeEnabled = "0";
                sProd3rdPartyUwResultTEnabled = "0";
                sProdIsDuRefiPlusEnabled = "0";
            }
            Page.ClientScript.RegisterHiddenField("sProd3rdPartyUwResultTEnabled", sProd3rdPartyUwResultTEnabled);
            Page.ClientScript.RegisterHiddenField("sProdIncludeEnabled", sProdIncludeEnabled);
            Page.ClientScript.RegisterHiddenField("sProdIsDuRefiPlusEnabled", sProdIsDuRefiPlusEnabled);

            //opm 25227 fs 11/14/08
            if (m_renderAsSecond)
            {
                bYes2ndFinancing.Checked = true;
                bYes2ndFinancing.Enabled = false;
                bNo2ndFinancing.Enabled = false;
            }
            else
            {
                bool has2nd = dataLoan.sHas2ndFinPe || (dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sSubFinT == E_sSubFinT.Heloc && (dataLoan.sSubFin > 0 || dataLoan.sConcurSubFin > 0)); // OPM 145405 Helocs can have $0 Other Balance
                bYes2ndFinancing.Checked = has2nd;
                bNo2ndFinancing.Checked = !has2nd;
            }

            //OPM 80002: Need to let the client side know if there's a coborrower or more than one app
            var hasMultipleBorrowers = dataLoan.nApps > 1 || dataLoan.GetAppData(0).aBHasSpouse;
            Page.ClientScript.RegisterHiddenField("HasMultipleBorrowers", hasMultipleBorrowers.ToString());

            NonOccupantCoborrower.Checked = dataLoan.sHasNonOccupantCoborrowerPe;

            main _parentPage = this.Page as main;
            if (null != _parentPage)
                _parentPage.UpdateLoanSummaryDisplay(dataLoan);
        }

        private void GetMortTradelines(CPageData dataLoan)
        {
            // We store if the credit report has mortgage tradelines in
            // past 12 months or ever in hidden input on client.

            if (m_trades.Value == string.Empty || m_trades12.Value == string.Empty)
            {
                bool hasCreditReportOnFile = false;
                bool hasAtLeast1stTimeHomeBuyerWithin100 = false;
                bool hasAtLeast1stTimeHomeBuyerWithin12 = false;
                int nApps = (PriceMyLoanUser.IsPricingMultipleAppsSupported) ? dataLoan.nApps : 1;

                for (int i = 0; i < nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    if (dataApp.aIsCreditReportOnFile)
                    {
                        LendersOffice.CreditReport.ICreditReport report = dataApp.CreditReportData.Value;

                        if (!hasAtLeast1stTimeHomeBuyerWithin100 && report.Is1stTimeHomeBuyerWithin(100))
                        {
                            hasAtLeast1stTimeHomeBuyerWithin100 = true;
                        }
                        if (!hasAtLeast1stTimeHomeBuyerWithin12 && report.Is1stTimeHomeBuyerWithin(12))
                        {
                            hasAtLeast1stTimeHomeBuyerWithin12 = true;
                        }
                        //m_trades.Value = report.Is1stTimeHomeBuyerWithin(100) ? "1" : "0";
                        //m_trades12.Value = report.Is1stTimeHomeBuyerWithin(12) ? "1" : "0";

                        // 07/31/07 mf. OPM 4442.  We need to load up the mortgage tradelines
                        // for easy choosing in the UI
                        foreach (LendersOffice.CreditReport.AbstractCreditLiability o in report.AllLiabilities)
                        {
                            if (o.IsTypeMortgage && o.IsActive)
                            {
                                sProOFinBalPeDdl.Items.Add("$" + o.UnpaidBalanceAmount.ToString("N") + " : " + o.CreditorName);
                                sProOFinPmtPeDdl.Items.Add("$" + o.MonthlyPaymentAmount.ToString("N") + " : " + o.CreditorName);
                            }
                        }
                        hasCreditReportOnFile = true;
                    }
                }
                // Per 4442, we add a blank entry as the first, so we do
                // not confuse the user with a default mortgage trade.
                if (sProOFinBalPeDdl.Items.Count > 0)
                {
                    sProOFinBalPeDdl.Items.Insert(0, "");
                    sProOFinPmtPeDdl.Items.Insert(0, "");
                }
                if (hasCreditReportOnFile)
                {
                    m_trades.Value = hasAtLeast1stTimeHomeBuyerWithin100 ? "1" : "0";
                    m_trades12.Value = hasAtLeast1stTimeHomeBuyerWithin12 ? "1" : "0";

                }
                else
                {
                    // No credit report on file
                    m_trades.Value = "x";
                    m_trades12.Value = "x";

                }
            }
        }

        protected void CountyDataBound(object sender, RepeaterItemEventArgs e)
        {
            HtmlGenericControl county = e.Item.FindControl("countyOption") as HtmlGenericControl;
            if (e.Item.DataItem.ToString().ToLower().Equals(currentCounty.ToLower()))
            {
                this.m_missingCounty = false;
                county.Attributes.Add("selected", "selected");
            }
            county.Attributes.Add("value", AspxTools.HtmlString(e.Item.DataItem.ToString()));
            county.InnerText = AspxTools.HtmlString(e.Item.DataItem.ToString());
        }
        public void SaveDataReadOnly()
        {
            Tools.LogInfo("PropertyInfo::SaveDataReadOnly");

            main mainpage = Page as main;

            //if using old engine just bail because old logic doesnt validate the rate lock on postback. -- future improve
            if (mainpage == null || mainpage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml))
            {
                return;
            }

            //if you only have   run to step 3 then you can edit any fields
            if (mainpage.CurrentUserCanPerform(WorkflowOperations.RunPmlToStep3) && mainpage.CurrentUserCanPerformAny(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans) == false)
            {
                return;
            }

            CPageData dataLoan = new CPropertyInfoData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            if (mainpage.CurrentUserCanPerformAny(WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans))
            {
                dataLoan.sProdRLckdDays_rep = sProdRLckdDays.Text;
            }

            if (mainpage.CurrentUserCanPerform(WorkflowOperations.RunPmlForAllLoans))
            {
                if (CurrentBroker.IsAsk3rdPartyUwResultInPml && Request.Form["sProdIncludeEnabled"] != "0")
                {
                    if (dataLoan.sProdIsDuRefiPlus == false)
                    {
                        dataLoan.sProdIncludeNormalProc = sProdIncludeNormalProc.Checked;
                    }
                    dataLoan.sProdIncludeMyCommunityProc = sProdIncludeMyCommunityProc.Checked;
                    dataLoan.sProdIncludeHomePossibleProc = sProdIncludeHomePossibleProc.Checked;
                    dataLoan.sProdIncludeFHATotalProc = sProdIncludeFHATotalProc.Checked;
                    dataLoan.sProdIncludeVAProc = sProdIncludeVAProc.Checked;
                    dataLoan.sProdIncludeUSDARuralProc = sProdIncludeUSDARuralProc.Checked;
                }

                dataLoan.sProdFilterDue10Yrs = sProdFilterDue10Yrs.Checked;
                dataLoan.sProdFilterDue15Yrs = sProdFilterDue15Yrs.Checked;
                dataLoan.sProdFilterDue20Yrs = sProdFilterDue20Yrs.Checked;
                dataLoan.sProdFilterDue25Yrs = sProdFilterDue25Yrs.Checked;
                dataLoan.sProdFilterDue30Yrs = sProdFilterDue30Yrs.Checked;
                dataLoan.sProdFilterDueOther = sProdFilterDueOther.Checked;

                dataLoan.sProdFilterFinMethFixed = sProdFilterFinMethFixed.Checked;
                dataLoan.sProdFilterFinMeth3YrsArm = sProdFilterFinMeth3YrsArm.Checked;
                dataLoan.sProdFilterFinMeth5YrsArm = sProdFilterFinMeth5YrsArm.Checked;
                dataLoan.sProdFilterFinMeth7YrsArm = sProdFilterFinMeth7YrsArm.Checked;
                dataLoan.sProdFilterFinMeth10YrsArm = sProdFilterFinMeth10YrsArm.Checked;
                dataLoan.sProdFilterFinMethOther = sProdFilterFinMethOther.Checked;

                dataLoan.sProdFilterPmtTPI = sProdFilterPmtTPI.Checked;
                dataLoan.sProdFilterPmtTIOnly = sProdFilterPmtTIOnly.Checked;
            }
            // We store filter to cookie based on loan ID
            // Note that currently Teaser Rate is not an option
            string sortValue;
            if (CurrentBroker.IsBestPriceEnabled)
            {
                if (CurrentUser.IsAlwaysUseBestPrice || BestPricingCb.Checked)
                {
                    sortValue = PriceMyLoanConstants.SortType_BestPrice;
                }
                else
                {
                    sortValue = PriceMyLoanConstants.SortType_ByName;
                }
            }
            else
            {
                switch (resultSort.SelectedIndex)
                {
                    case 0: sortValue = PriceMyLoanConstants.SortType_ByNoteRate; break;
                    case 1: sortValue = PriceMyLoanConstants.SortType_ByName; break;
                    default:
                        Tools.LogBug("Unhandled sort option.");
                        sortValue = PriceMyLoanConstants.SortType_ByName;
                        break;
                }
            }

            // 10/10/07 mf. OPM 17339. We are extending the life of this crumb.
            // The sort order will live until user chooses to clear our cookie.
            LendersOffice.Common.RequestHelper.StoreToCookie(SortCookieName, sortValue, DateTime.MaxValue);


            dataLoan.Save();
        }

        public void SaveData()
        {

            CPageData dataLoan = new CPropertyInfoData(LoanID);
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            #region opm 58376
            decimal sLtvRPe_old = dataLoan.sLtvRPe;
            int sTermInYr_old = dataLoan.sTermInYr;
            #endregion

            CAppData dataApp = dataLoan.GetAppData(0);

            m_useOriginatorComp = dataLoan.sIsQualifiedForOriginatorCompensation && CurrentBroker.IsDisplayCompensationChoiceInPml;
            m_renderAsSecond = dataLoan.sLienPosT == E_sLienPosT.Second;
            m_isAutoPmiOrNewPml = CurrentBroker.HasEnabledPMI || dataLoan.IsNewPMLEnabled; 
            SaveCreditQualInfo(dataLoan);

            dataLoan.sSpAddr = sSpAddr.Text;
            dataLoan.sSpCity = sSpCity.Text;
            dataLoan.sSpZip = sSpZip.Text;

            string county = Request.Form.Get("sSpCounty");

            if (county == null)
            {
                ErrorUtilities.DisplayErrorPage(
                    new CBaseException(ErrorMessages.Generic, "County is null "),
                    false, //send email
                    PriceMyLoanUser.BrokerId,
                    PriceMyLoanUser.EmployeeId);
            }

            dataLoan.sSpCounty = county;
            dataLoan.sSpStatePe = sSpStatePe.Value;

            dataLoan.sProdCondoStories_rep = sProdCondoStories.Text;
            dataLoan.sProdSpT = (E_sProdSpT)Tools.GetDropDownListValue(sProdSpT);
            dataLoan.sProdCalcEntryT = (E_sProdCalcEntryT)int.Parse(Request.Form["sProdCalcEntryT"]);
            dataLoan.sLAmtCalcPe_rep = sLAmtCalcPe.Text;
            if (!m_renderAsSecond)
                dataLoan.sLPurposeTPe = (E_sLPurposeT)Tools.GetDropDownListValue(sLPurposeTPe);
            dataLoan.sProdCashoutAmt_rep = sProdCashoutAmt.Text;
            if (!m_renderAsSecond)
                dataLoan.sProdRLckdDays_rep = sProdRLckdDays.Text;
            dataLoan.sHouseValPe_rep = sHouseValPe.Text;

            dataLoan.sProOFinBalPe_rep = sProOFinBalPe.Text;
            dataLoan.sDownPmtPcPe_rep = sDownPmtPcPe.Text;
            dataLoan.sEquityPe_rep = sEquityPe.Text;
            dataLoan.sLtvRPe_rep = sLtvRPe.Text;
            dataLoan.sCltvRPe_rep = sCltvRPe.Text;
            if (bCloseEnd2ndFinancing.Checked)
            {
                dataLoan.sSubFinT = E_sSubFinT.CloseEnd;
            }
            else 
            {
                dataLoan.sSubFinT = E_sSubFinT.Heloc;
            }
            if (bYes2ndFinancing.Checked)
            {
                if (bHeloc2ndFinancing.Checked)
                {
                    dataLoan.sSubFinPe_rep = sSubFinPe.Text;
                }
            }

            dataLoan.sRequestCommunityOrAffordableSeconds = sRequestCommunityOrAffordableSeconds.Checked;

            if (dataLoan.sLPurposeTPe == E_sLPurposeT.Purchase)
            {
                dataLoan.sApprValPe_rep = sApprValPe.Text;
            }

            if (dataLoan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                dataLoan.sProdIsLoanEndorsedBeforeJune09 = sProdIsLoanEndorsedBeforeJune09.Checked;
            }
            //opm 28103 fs 04/06/10
            if (!m_renderAsSecond)
                dataLoan.sSpLien_rep = sSpLien.Text;

            //opm 25872 fs 05/06/09
            if (PriceMyLoanUser.IsPricingMultipleAppsSupported)
            {
                if (dataLoan.sOccT != E_sOccT.PrimaryResidence)
                {
                    dataLoan.sPresOHExpPe_rep = aPresOHExpPe.Text;
                }
            }
            else
            {
                dataApp.aPresOHExpPe_rep = aPresOHExpPe.Text;
            }

            if (!m_renderAsSecond)
            {
                dataLoan.sFHASalesConcessions_rep = sFHASalesConcessions.Text;
                dataLoan.sHasAppraisal = sHasAppraisal.Checked;
                dataLoan.sOriginalAppraisedValue_rep = sOriginalAppraisedValue.Text;
                if (dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
                {
                    if (dataLoan.sHasAppraisal == false)
                    {
                        dataLoan.sHouseValPe = 0; // OPM 58758 sHouseValPe not displayed in this case, default to 0.

                        // OPM 59499 - Update the streamline refi appraisal setting
                        dataLoan.sFHAPurposeIsStreamlineRefiWithAppr = false;
                        dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr = true;
                    }
                    else
                    {
                        // OPM 59499 - Update the streamline refi appraisal setting
                        dataLoan.sFHAPurposeIsStreamlineRefiWithAppr = true;
                        dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr = false;
                    }
                }
            }

            if (this.CurrentBroker.IsEnableRenovationCheckboxInPML ||
                this.CurrentBroker.EnableRenovationLoanSupport)
            {
                bool isRenovationLoan = sIsRenovationLoan.Checked;
                dataLoan.sIsRenovationLoan = isRenovationLoan;
                if (isRenovationLoan)
                {
                    dataLoan.sTotalRenovationCosts_rep = sTotalRenovationCosts.Text;
                    dataLoan.sAsIsAppraisedValuePeval_rep = sAsIsAppraisedValuePeval.Text;
                }
            }

            dataLoan.sOccTPe = (E_sOccT)Tools.GetDropDownListValue(sOccTPe);
            if (dataLoan.nApps == 1)
            {
                dataLoan.GetAppData(0).aOccT = (E_aOccT)Tools.GetDropDownListValue(sOccTPe);
            }
            dataLoan.sOccRPe_rep = sOccRPe.Text;
            dataLoan.sSpGrossRentPe_rep = sSpGrossRentPe.Text;

            dataLoan.sProRealETxPe_rep = sProRealETxPe.Value;
            dataLoan.sProHazInsPe_rep = sProHazInsPe.Value;
            dataLoan.sProHoAssocDuesPe_rep = sProHoAssocDuesPe.Value;
            dataLoan.sProOHExpDesc = sProOHExpDescPe.Value;
            dataLoan.sProOHExpPe_rep = sProOHExpPe.Value;
            dataLoan.sProMInsPe_rep = sProMInsPe.Value;


            dataLoan.sProdIsSpInRuralArea = sProdIsSpInRuralArea.Checked;
            dataLoan.sProdIsCondotel = sProdIsCondotel.Checked;
            dataLoan.sProdIsNonwarrantableProj = sProdIsNonwarrantableProj.Checked;
            // 1/7/2010 dd - OPM 44203 - sIsCreditQualifying does not depend on "Are Income and Asset Require .." broker permission.
            //if (m_showCreditQualifying)
            //{
            dataLoan.sIsCreditQualifying = sIsCreditQualifying.Checked; //opm 42582 fs 11/18/09
            //}

            //opm 28525
            //We cannot rely on 'sProdIsTexas50a6Loan.Checked' value if the checkbox is disabled (even if checked in the UI).
            //In that case, 'checked' always returns false!
            bool isTexasRequired = false;
            if (!sProdIsTexas50a6Loan.Checked)
            {
                isTexasRequired = dataLoan.sSpState == "TX" && dataLoan.sOccTPe == E_sOccT.PrimaryResidence;
                isTexasRequired &= (!m_renderAsSecond) ? (dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout) : (dataLoan.sProdCashoutAmt_rep != "$0.00");
            }
            dataLoan.sProdIsTexas50a6Loan = sProdIsTexas50a6Loan.Checked || isTexasRequired;

            dataLoan.sPreviousLoanIsTexas50a6Loan = sPreviousLoanIsTexas50a6Loan.Checked;

            dataLoan.sLtvROtherFinPe_rep = sLtvROtherFinPe.Text;
            if (!m_renderAsSecond)
                dataLoan.sProdImpound = sProdImpound.Checked;

            dataLoan.sProdDocT = (E_sProdDocT)Tools.GetDropDownListValue(sProdDocT);
            //dataLoan.sFannieDocT = CommonFunctions.ProductTToFannieDocT(dataLoan.sProdDocT, dataLoan.sFannieDocT); // OPM 58372
            //            dataLoan.sProdFinMethFilterT     = (E_sProdFinMethFilterT) Tools.GetDropDownListValue(sProdFinMethFilterT);
            dataLoan.sProdSpStructureT = (E_sProdSpStructureT)Tools.GetDropDownListValue(sProdSpStructureT);
            dataLoan.sFhaCondoApprovalStatusT = (E_sFhaCondoApprovalStatusT)Tools.GetDropDownListValue(sFhaCondoApprovalStatusT);
            dataLoan.sProdAvailReserveMonths_rep = sProdAvailReserveMonths.SelectedItem.Value;

            if (m_isShowsProdEstimatedResidualI)
                dataLoan.sProdEstimatedResidualI_rep = sProdEstimatedResidualI.Text;

            // sHas1stTimeBuyerPe is not always posted. It is disabled and true when
            // the following conditions are met:
            // 1. Loan is not refi
            // 2. Credit Report is on file
            // 3. No mortgage tradelines within 12 months
            // 4. No mortgage tradelines ever


            if (!m_renderAsSecond)
            {
                if ((E_sLPurposeT)Tools.GetDropDownListValue(sLPurposeTPe) != E_sLPurposeT.Refin
                    && (E_sLPurposeT)Tools.GetDropDownListValue(sLPurposeTPe) != E_sLPurposeT.RefinCashout
                    && m_trades12.Value == "0"
                    && m_trades.Value == "0")
                {
                    dataLoan.sHas1stTimeBuyerPe = true;
                }
                else
                {
                    dataLoan.sHas1stTimeBuyerPe = sHas1stTimeBuyerPe.Checked;
                }
            }

            dataLoan.sPrimAppTotNonspIPe_rep = sPrimAppTotNonspIPe.Text;
            dataLoan.sNumFinancedProperties_rep = sNumFinancedProperties.Text;
            if (!m_renderAsSecond)
            {
                dataLoan.sProdIsFhaMipFinanced = sProdIsFhaMipFinanced.Checked; //opm 28086 fs 03/03/09
                dataLoan.sProdFhaUfmip_rep = sProdFhaUfmip.Text;
                dataLoan.sProdIsUsdaRuralHousingFeeFinanced = sProdIsUsdaRuralHousingFeeFinanced.Checked;

                //opm 43371 av 4/7/10 va funding fee 
                if (dataLoan.sLPurposeTPe != E_sLPurposeT.FhaStreamlinedRefinance)
                {
                    dataLoan.sProdIsVaFundingFinanced = sProdIsVaFundingFinanced.Checked;
                    dataLoan.sProdVaFundingFee_rep = sProdVaFundingFee.Text;
                }
            }

            if (m_useOriginatorComp)
            {
                dataLoan.sOriginatorCompensationPaymentSourceT = sOriginatorCompensationPaymentSourceT_Borrower.Checked ? E_sOriginatorCompensationPaymentSourceT.BorrowerPaid : E_sOriginatorCompensationPaymentSourceT.LenderPaid;

                if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                {
                    dataLoan.sOriginatorCompensationBorrPaidPc_rep = sOriginatorCompensationBorrPaidPc.Text;
                    dataLoan.sOriginatorCompensationBorrPaidBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(sOriginatorCompensationBorrPaidBaseT);
                    dataLoan.sOriginatorCompensationBorrPaidMb_rep = sOriginatorCompensationBorrPaidMb.Text;
                }
            }

            dataLoan.sProdFilterDue10Yrs = sProdFilterDue10Yrs.Checked;
            dataLoan.sProdFilterDue15Yrs = sProdFilterDue15Yrs.Checked;
            dataLoan.sProdFilterDue20Yrs = sProdFilterDue20Yrs.Checked;
            dataLoan.sProdFilterDue25Yrs = sProdFilterDue25Yrs.Checked;
            dataLoan.sProdFilterDue30Yrs = sProdFilterDue30Yrs.Checked;
            dataLoan.sProdFilterDueOther = sProdFilterDueOther.Checked;
            dataLoan.sProdFilterFinMethFixed = sProdFilterFinMethFixed.Checked;
            dataLoan.sProdFilterFinMeth3YrsArm = sProdFilterFinMeth3YrsArm.Checked;
            dataLoan.sProdFilterFinMeth5YrsArm = sProdFilterFinMeth5YrsArm.Checked;
            dataLoan.sProdFilterFinMeth7YrsArm = sProdFilterFinMeth7YrsArm.Checked;
            dataLoan.sProdFilterFinMeth10YrsArm = sProdFilterFinMeth10YrsArm.Checked;
            dataLoan.sProdFilterFinMethOther = sProdFilterFinMethOther.Checked;

            dataLoan.sProdFilterPmtTPI = sProdFilterPmtTPI.Checked;
            dataLoan.sProdFilterPmtTIOnly = sProdFilterPmtTIOnly.Checked;

            if (PriceMyLoanUser.IsPricingMultipleAppsSupported)
            {
                savePrimaryBorrower(dataLoan, Request.Form[sIsPrimaryWageEarner.UniqueID]);
            }

            // We store filter to cookie based on loan ID
            // Note that currently Teaser Rate is not an option
            string sortValue;
            if (CurrentBroker.IsBestPriceEnabled)
            {
                if (CurrentUser.IsAlwaysUseBestPrice || BestPricingCb.Checked)
                {
                    sortValue = PriceMyLoanConstants.SortType_BestPrice;
                }
                else
                {
                    sortValue = PriceMyLoanConstants.SortType_ByName;
                }
            }
            else
            {
                switch (resultSort.SelectedIndex)
                {
                    case 0: sortValue = PriceMyLoanConstants.SortType_ByNoteRate; break;
                    case 1: sortValue = PriceMyLoanConstants.SortType_ByName; break;
                    default:
                        Tools.LogBug("Unhandled sort option.");
                        sortValue = PriceMyLoanConstants.SortType_ByName;
                        break;
                }
            }

            // 10/10/07 mf. OPM 17339. We are extending the life of this crumb.
            // The sort order will live until user chooses to clear our cookie.
            LendersOffice.Common.RequestHelper.StoreToCookie(SortCookieName, sortValue, DateTime.MaxValue);

            // OPM 109393. Auto PMI lender needs the Auto PMI MI choices.
            if (m_isAutoPmiOrNewPml)
            {
                dataLoan.sProdConvMIOptionT = dataLoan.sLtv80TestResultT == E_sLtv80TestResultT.Over80 ? 
                    (E_sProdConvMIOptionT)Tools.GetDropDownListValue(sProdConvMIOptionT) : 
                    E_sProdConvMIOptionT.Blank;
                dataLoan.sConvSplitMIRT = (E_sConvSplitMIRT)Tools.GetDropDownListValue(sConvSplitMIRT);
            }
            else
            {
                // 10/23/06 mf - OPM 7285.  We use Leave Blank when LTV <= 80 (client ddl is disabled and set to blank)
                dataLoan.sProdMIOptionT = dataLoan.sLtv80TestResultT == E_sLtv80TestResultT.Over80 ? (E_sProdMIOptionT)Tools.GetDropDownListValue(sProdMIOptionT) : E_sProdMIOptionT.LeaveBlank;
            }
            if (m_isAsk3rdPartyUwResultInPml)
            {

                if (Request.Form["sProd3rdPartyUwResultTEnabled"] != "0")
                {
                    // 11/6/2007 dd - Only update value if drop down list is enabled.
                    dataLoan.sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT)Tools.GetDropDownListValue(sProd3rdPartyUwResultT);
                }
                if (Request.Form["sProdIncludeEnabled"] != "0")
                {

                    dataLoan.sProdIncludeMyCommunityProc = sProdIncludeMyCommunityProc.Checked;
                    dataLoan.sProdIncludeHomePossibleProc = sProdIncludeHomePossibleProc.Checked;
                    dataLoan.sProdIncludeFHATotalProc = sProdIncludeFHATotalProc.Checked;
                    dataLoan.sProdIncludeVAProc = sProdIncludeVAProc.Checked;
                    dataLoan.sProdIncludeUSDARuralProc = sProdIncludeUSDARuralProc.Checked;

                    bool isDuRefiPlus = (Request.Form["sProdIsDuRefiPlusEnabled"] == "0" && dataLoan.sProdIsDuRefiPlus);
                    dataLoan.sProdIncludeNormalProc = isDuRefiPlus ? true : sProdIncludeNormalProc.Checked;
                }
                else
                {
                    // If the checkboxes are disabled, but the user selected a VaIrrrl, then include VA
                    E_sLPurposeT loanPurpose = (E_sLPurposeT)Tools.GetDropDownListValue(sLPurposeTPe);
                    if (loanPurpose == E_sLPurposeT.VaIrrrl)
                    {
                        dataLoan.sProdIncludeVAProc = true;
                    }
                }
                if (Request.Form["sProdIsDuRefiPlusEnabled"] != "0")
                {
                    dataLoan.sProdIsDuRefiPlus = sProdIsDuRefiPlus.Checked;
                    if (sProdIsDuRefiPlus.Checked)
                    {
                        dataLoan.sProdIncludeNormalProc = true; //opm 32493 fs 09/08/09
                    }
                }
            }

            if (m_renderAsSecond)
            {
                dataLoan.sLpIsNegAmortOtherLien = sLpIsNegAmortOtherLien.Checked;
                dataLoan.sOtherLFinMethT = (E_sFinMethT)Tools.GetDropDownListValue(sOtherLFinMethT);
                dataLoan.sProOFinPmtPe_rep = sProOFinPmtPe.Text;
            }


            if (dataLoan.sLT == E_sLT.FHA)
            {
                dataLoan.Update_sProMinsR(sLtvRPe_old, sTermInYr_old);
                //OPM 92651: When saving PropertyInfo in PML, copy the value of sProdIsFhaMipFinanced to sFfUfMipIsBeingFinanced
                //in an FHA loan. Since some stuff happens when you set sProdIsFhaMipFinanced, don't do it unless you need to.
                if (dataLoan.sProdIsFhaMipFinanced != dataLoan.sFfUfMipIsBeingFinanced)
                {
                    dataLoan.sFfUfMipIsBeingFinanced = dataLoan.sProdIsFhaMipFinanced;
                }
            }

            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase && sPriorSalesD.Text.Length > 0 && false == sPriorSalesD.Text.TrimWhitespaceAndBOM().Equals("mm/dd/yyyy", StringComparison.OrdinalIgnoreCase))
            {
                dataLoan.sPriorSalesD_rep = sPriorSalesD.Text;
                dataLoan.sPriorSalesPrice_rep = sPriorSalesPrice.Text;
                dataLoan.sPriorSalesPropertySellerT = (E_sPriorSalesPropertySellerT)Tools.GetDropDownListValue(sPriorSalesSellerT);
            }
            else
            {
                dataLoan.sPriorSalesD_rep = "";
            }

            if (bYes2ndFinancing.Checked)
            {
                if (bHeloc2ndFinancing.Checked)
                {
                    dataLoan.sHCLTVRPe_rep = sHCLTVRPe.Text;
                    dataLoan.sSubFinToPropertyValue_rep = sSubFinToPropertyValue.Text;
                }
            }

            dataLoan.sHasNonOccupantCoborrowerPe = NonOccupantCoborrower.Checked;

            dataLoan.Update_sProMInsFieldsPer110559();
            
            dataLoan.Save();
        }
        public void SetAdditionalQueryStringValues(PmlMainQueryStringManager qsManager)
        {
            // 02/20/08 mf. OPM 20137.  These form values need to go the query string.

            if (Request.Form["Launch"] != null && Request.Form["Launch"] != string.Empty)
            {
                qsManager.AddPair("launch", Request.Form["Launch"]);
            }
            if (Request.Form["LpEntry"] != null && Request.Form["LpEntry"] != string.Empty)
            {
                qsManager.AddPair("LpEntry", Request.Form["LpEntry"]);
            }
            if (Request.Form["_PreviousScroll"] != null && Request.Form["_PreviousScroll"] != string.Empty)
            {
                qsManager.AddPair("_PreviousScroll", Request.Form["_PreviousScroll"]);
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }


        private void setPrimaryBorrowerDropdown(DropDownList ddl, CPageData dataLoan)
        {
            if (ddl == null || dataLoan == null)
                throw new ArgumentNullException();

            //Populate dropdown box accordingly
            ddl.Items.Clear();
            ListItem li = null;
            ListItem selectedItem = null;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dApp = dataLoan.GetAppData(i);

                string name = null;

                name = dApp.aBLastFirstNm;
                if (name.Length == 0)
                {
                    name = "App " + (i + 1) + " - Borrower";
                }

                li = new ListItem(name, dApp.aAppId.ToString() + "-b");
                if (dApp.aBIsPrimaryWageEarner)
                {
                    selectedItem = li;
                }
                ddl.Items.Add(li);

                if (dApp.aBHasSpouse)
                {
                    name = dApp.aCLastFirstNm;
                    if (name.Length == 0)
                    {
                        name = "App " + (i + 1) + " - Co-borrower";
                    }
                    li = new ListItem(name, dApp.aAppId.ToString() + "-c");
                    if (dApp.aCIsPrimaryWageEarner)
                    {
                        selectedItem = li;
                    }
                    ddl.Items.Add(li);
                }
            }

            if (selectedItem != null)
            {
                int index = ddl.Items.IndexOf(selectedItem);
                if (index >= 0 && index < ddl.Items.Count)
                    ddl.Items[index].Selected = true;
                else
                    ddl.Items[0].Selected = true;
            }
            else
            {
                ddl.Items[0].Selected = true;
            }
        }

        private void savePrimaryBorrower(CPageData dataLoan, string postedValue)
        {
            if (string.IsNullOrEmpty(postedValue))
            {
                if (postedValue == null)
                    Tools.LogWarning("[PML step 3] sIsPrimaryWageEarner dropdown value not posted back.");
                dataLoan.GetAppData(0).aBIsPrimaryWageEarner = true; //assume primary wage earner is "Borr from App 0"
                return;
            }

            Guid appId = Guid.Empty;
            bool isBorrower = true;

            try
            {
                appId = new Guid(postedValue.Substring(0, 36));
                isBorrower = postedValue.Substring(37, 1).ToLower() == "b";
            }
            catch
            {
                dataLoan.GetAppData(0).aBIsPrimaryWageEarner = true; //assume primary wage earner is "Borr from App 0"
                return;
            }

            CAppData dApp = dataLoan.GetAppData(appId);
            if (dApp != null)
            {
                if (isBorrower)
                    dApp.aBIsPrimaryWageEarner = true;
                else
                    dApp.aCIsPrimaryWageEarner = true;
            }
            else
            {
                dataLoan.GetAppData(0).aBIsPrimaryWageEarner = true; //assume primary wage earner is "Borr from App 0"
            }
        }

        private void SaveCreditQualInfo(CPageData dataLoan)
        {
            E_sLPurposeT loanPurpose = (E_sLPurposeT)Tools.GetDropDownListValue(sLPurposeTPe);

            if (m_showCreditQualifying || (!m_renderAsSecond && loanPurpose == E_sLPurposeT.VaIrrrl))
            {
                bool IsCredQual = sIsCreditQualifying.Checked;

                //opm 32180 fs 11/25/09
                //Set to TRUE temporarily to force the Data-layer to save sPrimAppTotNonspIPe, sProdAvailReserveMonths and sProdEstimatedResidualI
                dataLoan.sIsCreditQualifying = true;

                dataLoan.sPrimAppTotNonspIPe_rep = sPrimAppTotNonspIPe.Text;
                dataLoan.sProdAvailReserveMonths_rep = sProdAvailReserveMonths.SelectedItem.Value;
                if (m_isShowsProdEstimatedResidualI)
                    dataLoan.sProdEstimatedResidualI_rep = sProdEstimatedResidualI.Text;

                //At this point the real value is set.
                dataLoan.sIsCreditQualifying = IsCredQual;
            }
        }

        private void LoadLockPeriodOptionsAndSetTextboxValue(CPageData pageData)
        {
            //! fix this.
            // get the rate lock option periods
            // adds them to the drop down
            // and "migrates" the old value to one of the drop down values, giving it the smallest larger value.
            if (CurrentBroker.IsEnabledLockPeriodDropDown)
            {
                string[] options = CurrentBroker.AvailableLockPeriodOptionsCSV.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var numericOptions = from s in options
                                     select UInt32.Parse(s);
                var sortedOptions = from n in numericOptions
                                    orderby n
                                    select n;

                uint sProdRLckdDaysValue = (uint)pageData.sProdRLckdDays;
                sProdRLckdDaysDDL.Items.Clear();
                sProdRLckdDaysDDL.Items.Add(new ListItem("None", "0"));
                int index = 1;
                sProdRLckdDaysDDL.SelectedIndex = 0;
                sProdRLckdDays.Text = "0";
                bool isSelected = (sProdRLckdDaysValue == 0);
                foreach (uint option in sortedOptions)
                {
                    sProdRLckdDaysDDL.Items.Add(new ListItem(option.ToString(), option.ToString()));
                    if (!isSelected && option >= sProdRLckdDaysValue)
                    {
                        sProdRLckdDaysDDL.SelectedIndex = index;
                        sProdRLckdDays.Text = sProdRLckdDaysDDL.SelectedValue;
                        isSelected = true;
                    }
                    index++;
                }
                sProdRLckdDays.CssClass += " hidden";
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);


        }
        #endregion
    }
}
