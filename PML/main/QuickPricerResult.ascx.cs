﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using DataAccess;
using PriceMyLoan.Security;

namespace PriceMyLoan.main
{
    public partial class QuickPricerResult : System.Web.UI.UserControl
    {
        protected string m_quickPricerDisclaimerAtResult = "";
        protected override void OnInit(EventArgs e)
        {
            this.Load += new EventHandler(this.PageLoad);
            base.OnInit(e);
        }

        private PriceMyLoanPrincipal GetPrincipal()
        {
            return ((PriceMyLoan.UI.BasePage)this.Page).CustomPrincipal;
        }

        public bool IsForAnonymousQp
        {
            get;
            set;
        }

        private void PageLoad(object sender, EventArgs e)
        {
            AbstractUserPrincipal principal = GetPrincipal();

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
            m_quickPricerDisclaimerAtResult = broker.QuickPricerDisclaimerAtResult;

            CPageData dataLoan = new CQuickPricerLoanData(broker.QuickPricerTemplateId);
            dataLoan.InitLoad();
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            List<KeyValuePair<string, string>> replacementKeywords = new List<KeyValuePair<string, string>>();
            replacementKeywords.Add(new KeyValuePair<string, string>("[LockPeriod]", dataLoan.sProdRLckdDays_rep));
            replacementKeywords.Add(new KeyValuePair<string, string>("[Impound]", dataLoan.sProdImpound ? "impound" : "no impound"));

            ProcessDisclaimerMessage(m_quickPricerDisclaimerAtResult, replacementKeywords);

        }

        private void ProcessDisclaimerMessage(string msg, List<KeyValuePair<string, string>> replacementKeywords)
        {
            if (string.IsNullOrEmpty(msg))
                return;

            foreach (KeyValuePair<string, string> o in replacementKeywords)
            {
                msg = msg.Replace(o.Key, o.Value);
            }

            string[] lines = msg.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            foreach (string s in lines)
            {
                HtmlGenericControl div = new HtmlGenericControl("div");

                if (!string.IsNullOrEmpty(s))
                {
                    div.InnerText = s;
                }
                else
                {
                    div.InnerHtml = "&nbsp;";
                }
                m_disclaimerPlaceHolder.Controls.Add(div);

            }

        }
    }
}