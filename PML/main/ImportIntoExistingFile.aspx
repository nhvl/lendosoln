﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportIntoExistingFile.aspx.cs" Inherits="PriceMyLoan.main.ImportIntoExistingFile" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Import Into Existing File</title>
    <style>
        #FileUpload
        {
            width: 90%;
            height: 20px;
        }
        body
        {
            padding: 15px;
        }
        table
        {
            width: 100%;
        }
        table td.left-col-label
        {
            width: 17%;
            color: black;
        }
        table td.left-col-data
        {
            width: 17%;
        }
        table td.right-col-label
        {
            width: 20%;
            color: black;
        }
        table td.right-col-data
        {
            width: 43%;
        }
        .warning
        {
            padding-bottom: 10px;
            color: #EA1C0D; // red color class text-danger
        }
        .buttons
        {
            padding-top: 10px;
        }
        .hidden
        {
            display: none;
        }
    </style>
</head>
<body>
<form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="Close" />

    <div lqb-popup>
        <div class="modal-header">
            <button type="button" class="close" id="btnX"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title">Import FNM 3.2 File</h4>
        </div>
        <div class="modal-body">
            <loading-icon id='ProgressIndicator' style="display: none;">Importing file…</loading-icon>
            <div id="Content">
                <div class="table table-modal-padding-bottom margin-bottom">
                    <div><div><label class="text-grey align-right">Loan Number:     </label></div><div><ml:EncodedLiteral runat="server" ID="sLNm"     /></div></div>
                    <div><div><label class="text-grey align-right">Borrower Name:   </label></div><div><ml:EncodedLiteral runat="server" ID="aBNm"     /></div></div>
                    <div><div><label class="text-grey align-right">Loan Amount:     </label></div><div><ml:EncodedLiteral runat="server" ID="sLAmtCalc"/></div></div>
                    <div><div><label class="text-grey align-right">Property Address:</label></div><div><ml:EncodedLiteral runat="server" ID="sSpAddr"  /></div></div>
                </div>

                <div class="warning warning-app-info">
                    <p>
                        Warning: Importing a FNM 3.2 file will replace the current application
                        data on this loan file with application data from the imported file.
                    </p>
                    <p>
                        <ml:EncodedLiteral runat="server" ID="UploadError"></ml:EncodedLiteral>
                    </p>
                </div>

                <div>
                    <asp:FileUpload runat="server" ID="FileUpload" accept=".fnm" required/>
                    <span class="required">*</span>
                    <asp:RequiredFieldValidator ID="FileUploadValidator" runat="server" Display="None" ErrorMessage="*" ControlToValidate="FileUpload" />
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-flat" id="Cancel" type="button">Cancel</button>

            <ml:NoDoubleClickButton runat="server" ID="Upload" Text="Upload"
                OnClick="OnUploadClick"
                CausesValidation="true"
                CssClass="btn btn-flat"/>
        </div>
    </div>
</form>

<script>
(function($){
    $(function() {
        TPOStyleUnification.UnifyLoadingIcons($("#ProgressIndicator"));
        const popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
        const iframeSelf = new IframeSelf();
        iframeSelf.popup = popup;



        $('#Cancel').on('click', function() {
            iframeSelf.resolve(null);
        });
        $('#btnX').on('click', function() {
            iframeSelf.resolve(null);
        });

        if ($('#Close').val() === 'True') {
            iframeSelf.resolve(true);
            $('#ProgressIndicator').show();
        }
    });
    TPOStyleUnification.Components();

    $("#Upload").on("click", function displayProgressBar() {
        if (Page_ClientValidate()) {
            $j('#ProgressIndicator').show();
        }
    });
})(jQuery);
</script>
</body>
</html>
