<%@ Import Namespace="PriceMyLoan.Common"%>
<%@ Register TagPrefix="uc1" TagName="GetResults" Src="GetResults.ascx" %>
<%@ Page language="c#" Codebehind="RerunResult.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.RerunResult" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>RerunResult</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout"  leftmargin="0" topmargin="0"  onunload="f_onUnload();">
	<script language=javascript>
<!--
		var _popupWindow = null;
		var _nameOfChild = null;

		function _init()
		{

			<% if (!Page.IsPostBack) { %>
			f_displayWaiting();
			f_Underwriting("0");
			<% } else { %>
			  f_displayResult();
			<% } %>
		}
		function f_goToPipeline( bShowCertificate )
		{
    <% if (PriceMyLoanConstants.IsEmbeddedPML) { %>
      parent.f_afterLPESubmitted();
  	<% } else if (!m_allowGoToPipeline) { %>
      self.location = <%= AspxTools.JsString(VirtualRoot + "/MortgageLeagueLogout.aspx?code=submitted") %>;
    <% } else { %>
	  if (bShowCertificate != null && bShowCertificate) {
		  self.location = <%= AspxTools.JsString(VirtualRoot + "/Main/Pipeline.aspx?certificateId=" + LoanID) %>;
	  } else {
		  self.location = <%= AspxTools.JsString(VirtualRoot + "/Main/Pipeline.aspx?") %>;
	  }
	  <% } %>

		}
		function f_onResultClose()
		{
			f_goToPipeline( false );
		}
		function f_allowClick()
		{
			return _popupWindow == null || _popupWindow.closed == true;
		}

		function f_updateChildName( sNameOfChild )
		{
			if( sNameOfChild != null && sNameOfChild != "" )
			{
				_nameOfChild = sNameOfChild;
			}
			else
			{
				_nameOfChild = null;
			}
		}
		
		function f_hideNavigation()
		{
		}		
		function f_onUnload()
		{
		      if (null != _popupWindow && !_popupWindow.closed) _popupWindow.close();
		}
		
//-->
</script>

    <form id="RerunResult" method="post" runat="server">
				    <% if (!PriceMyLoanConstants.IsEmbeddedPML) { %>
							<table width="100%" border="0" cellpadding="5" cellspacing="0" class="TopHeaderBackgroundColor">
								<tr>
									<td width="1%" height="1%">
										<% if (m_allowGoToPipeline) { %>
										<input type="button" value="Pipeline" class="ButtonStyle" onclick="if( !f_allowClick() ) return false; f_goToPipeline();" NoHighlight>
										<% } %>
									</td>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr class="LoanSummaryBackgroundColor">
												<td class="LoanSummaryItem">Loan #:
													<ml:EncodedLiteral id="sLNm" runat="server"></ml:EncodedLiteral></td>
												<td class="LoanSummaryItem">
													<ml:EncodedLiteral id="aBNm" runat="server"></ml:EncodedLiteral></td>
												<td class="LoanSummaryItem">Amt:
													<ml:EncodedLiteral id="sLAmtCalc" runat="server"></ml:EncodedLiteral></td>
												<td class="LoanSummaryItem">LTV:
													<ml:EncodedLiteral id="sLtvR" runat="server"></ml:EncodedLiteral></td>
												<td class="LoanSummaryItem">CLTV:
													<ml:EncodedLiteral id="sCltvR" runat="server"></ml:EncodedLiteral></td>
												<td class="LoanSummaryItem">Primary <a href='#' style="FONT-WEIGHT: bold;COLOR: #ff9933;TEXT-DECORATION: none" title='Help' onclick="f_openHelp('Q00006.html', 400, 250);">
														(?)</a> :
													<ml:EncodedLiteral id="sCreditScoreType1" runat="server"></ml:EncodedLiteral></td>
												<td class="LoanSummaryItem">Lowest <a href='#' style="FONT-WEIGHT: bold;COLOR: #ff9933;TEXT-DECORATION: none" title='Help' onclick="f_openHelp('Q00006.html', 400, 250);">
														(?)</a> :
													<ml:EncodedLiteral id="sCreditScoreType2" runat="server"></ml:EncodedLiteral></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
					<%  } %>    
					<table width="100%">
					  <tr><td style="padding-left:10px">
    <uc1:GetResults id=GetResults1 runat="server"></uc1:GetResults>
    </td></tr>
					</table>
    <uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

     </form>
	
  </body>
</HTML>
