﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LendingLicensesPanel.ascx.cs" Inherits="PriceMyLoan.main.LendingLicensesPanel" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Placeholder id="pnlLsIdentifier" runat="server">
<div class="content-container">
    <%--OPM 55321: changed the label to its current value, added mask.js compat to the text field to will make it harder to submit non-numbers. --%>
    <header class="header">Nationwide Mortgage Licensing System and Registry</header>

    <div class="profile-padding-left">
        <div class="table">
            <div><div><label id="lblLOID" class="indentLeft" runat="server">Loan Originator NMLS ID</label></div><div>
                <asp:textbox ID="tbNmlsIdentifier" runat="server" Width="20em" CssClass="mask" preset="nmlsID"/>
            </div></div>
        </div>
    </div>
</div>

<header class="header">State Licenses</header>
</asp:Placeholder>

<div class="profile-padding-left">
    <div class="indentLeft margin-bottom">
        <button id='<%= AspxTools.HtmlString(m_prefix + "AddBtn") %>' type="button" onclick="LICENSES.fn.addLendingLicense(<%= AspxTools.JsString(m_prefix) %>);" class="btn btn-default">Add License</button>
        &nbsp;&nbsp;
        <span id='<%= AspxTools.HtmlString(m_prefix + "licErrMsg") %>' class="text-danger"></span>
    </div>
    <div class="indentLeft">
        <table class="DataGrid modal-table-scrollable wide none-hover-table table-license">
            <thead><tr class="GridHeader">
                <th>License #</th>
                <th>State</th>
                <th>Expiration Date</th>
                <th></th>
            </tr></thead>
            <tbody id='<%= AspxTools.HtmlString(m_prefix + "LendingLicensesRows") %>'></tbody>
        </table>
        <div class="no-license margin-bottom" id='<%= AspxTools.HtmlString(m_prefix + "emptyPanel") %>' style="display: none;">
            No licenses to display.
        </div>
    </div>
</div>
<asp:CustomValidator id="licValidator" ClientValidationFunction="__customLicensesValidator" runat="server" />

<script>
    try { if (this.LICENSES) { LICENSES.fn.attachLoadListener(); } } catch (e) { }
</script>
