﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OriginatingCompanyServiceCredentials.aspx.cs" Inherits="PriceMyLoan.main.OriginatingCompanyServiceCredentials" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Service Credentials</title>
    <script type="text/javascript">
        function _init() {
            ServiceCredential.Initialize({
                WindowContext: window.parent,
                IsOriginatingCompany: true,
                ServiceCredentialJsonId: 'ServiceCredentialsJson',
                ServiceCredentialTableId: 'ServiceCredentialsTable'
            });
            ServiceCredential.RenderCredentials();
            $('#AddServiceCredentialBtn').on('click', ServiceCredential.AddServiceCredential);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <header class="page-header">Service Credentials</header>
        <div class="padding-left-p16">
            <div class="margin-bottom">Configure company-level credentials here. Any credentials added will auto-fill for users in this portal.</div>
            <table id="ServiceCredentialsTable" class="primary-table margin-bottom">
                <tbody></tbody>
            </table>
            <button id="AddServiceCredentialBtn" class="btn btn-default" type="button">Add Credential</button>
        </div>
    </div>
    </form>
</body>
</html>
