using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Constants;
using LendersOffice.Common;
using PriceMyLoan.Security;
using LendersOffice.Admin;

using PriceMyLoan.Common;
using LendersOffice.AntiXss;
using System.Text.RegularExpressions;

namespace PriceMyLoan.main
{

	public partial class EmailCertificate : PriceMyLoan.UI.BasePage
	{
        private static Regex nameAndEmail = new Regex(@"(^""(?<name>.*?)""\s<(?<email>" + ConstApp.EmailValidationExpression + ")>$)|^(?<email>" + ConstApp.EmailValidationExpression + ")$", RegexOptions.ExplicitCapture);

        private static Regex emailRegex = new Regex(ConstApp.EmailValidationExpression);

		public override bool IsReadOnly
		{
			get
			{
				// 10/06/06 - mf OPM 7662.  When this page is read-only, people sometimes click the send
				// button, but they cannot enter recipients.  The user should be able to email the cert
				// even if in read-only mode.  If we need a readonly (disabled) version of this page,
				// we can disable the send button.  For now, we can always send.
				return false;
			}
		}

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

		protected void PageInit(object sender, System.EventArgs e)
		{
            sendFromCustom.Attributes.Add("onblur", "validateEmail()");
            sendFromCorporate.Attributes.Add("onblur", "validateEmail()");
            sendFromCurrentUser.Attributes.Add("onblur", "validateEmail()");
			this.RegisterService("main", "/main/EmailCertificateService.aspx");
            //RegularExpressionValidator1.Text = ErrorMessages.InvalidEmailAddress + " (no spaces)";
			EmailAddr.ToolTip = "(Use semi-colons to separate multiple addresses)";
            CCAddr.ToolTip = EmailAddr.ToolTip;
			PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) User;
            userLabel.InnerText = principal.FirstName + " " + principal.LastName;
            userMailTo.HRef = AspxTools.HtmlAttribute("mailto:" + principal.Email).Replace("\"", string.Empty);
            userMailTo.InnerText = principal.Email;
            CCAddr.Text = principal.Email;
		    // 12/20/06 mf - OPM 8833. We can now get the "From" address based on
			// the rule settings of the broker and the assignments of the loan.
			EventNotificationRulesView rView = new EventNotificationRulesView( ((PriceMyLoanPrincipal) this.User).BrokerId );
            string fullEmailAddress = rView.GetFromAddressString(LoanID);
            string name; 
            string email; 
            if (IsValidEmail( fullEmailAddress, out name, out email) &&  email !=  principal.Email) 
            {
                if (!string.IsNullOrEmpty(name))
                {
                    eventStartPattern.Visible = true;
                    eventEndPattern.Visible = true;
                }
 
                eventLabel.InnerText = name;
                eventEmail.HRef = AspxTools.HtmlAttribute("mailto:" + email).Replace("\n", string.Empty).Replace("\"", string.Empty);
                eventEmail.InnerText = email;
                fromEventRow.Visible = true; 
            }
		


		}

        private static bool IsValidEmail(string input, out string name, out string email)
        {
            name = "";
            email = "";

            Match match = nameAndEmail.Match(input);
            if (!match.Success)
            {
                return false;
            }

            email = match.Groups["email"].Value;
            if (match.Groups["name"].Success)
            {
                name = match.Groups["name"].Value;
            }
            return true;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }

	}
}
