using System;
using System.Collections.Generic;
using System.Web.UI;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Common;
using PriceMyLoan.Common;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.PriceMyLoan;
using System.Text;
using LendersOffice.Constants;
using System.Linq;
using LendersOffice.ConfigSystem.Operations;

namespace PriceMyLoan.UI
{

    public class LoanProductResultControlNewUI : System.Web.UI.Control
    {
        private CApplicantPriceXml m_applicantPrice;
        private string m_cssClassName;
        private bool m_has2ndLoan;
        private E_sLienQualifyModeT m_currentLienQualifyModeT;
        private bool m_isNoIncome;
        private BrokerDB m_brokerDB = null;
        private E_LpeRunModeT m_LpeRunModeT;
        private bool m_canSubmitForLock;
        private bool m_isProgramRegistered;

        public bool UserCanApplyForIneligibleLoanPrograms { get; set; }

        public bool DisplayPmlFeeIn100Format { get; set; }

        public bool IsBorrowerPaidOriginatorCompAndIncludedInFees { get; set; }

        public bool IsLenderPaidOriginatorCompAndAdditionToFees { get; set; }

        public CApplicantPriceXml ApplicantPrice
        {
            get { return m_applicantPrice; }
            set { m_applicantPrice = value; }
        }
        public string CssClassName
        {
            get { return m_cssClassName; }
            set { m_cssClassName = value; }
        }

        public bool Has2ndLoan
        {
            get { return m_has2ndLoan; }
            set { m_has2ndLoan = value; }
        }

        public E_sLienQualifyModeT CurrentLienQualifyModeT
        {
            get { return m_currentLienQualifyModeT; }
            set { m_currentLienQualifyModeT = value; }
        }

        // OPM 8952.  We have to hide QRate and DTI when this is set
        public bool IsNoIncome
        {
            get { return m_isNoIncome; }
            set { m_isNoIncome = value; }
        }

        public BrokerDB BrokerDB
        {
            get { return m_brokerDB; }
            set { m_brokerDB = value; }
        }

        private bool IsPmlSubmissionAllowed
        {
            get
            {
                return BrokerDB.IsPmlSubmissionAllowed;
            }
        }
        public E_LpeRunModeT CurrentLpeRunModeT
        {
            get { return m_LpeRunModeT; }
            set { m_LpeRunModeT = value; }
        }

        public bool CanSubmitForLock
        {
            get { return m_canSubmitForLock; }
            set { m_canSubmitForLock = value; }
        }

        public bool IsProgramRegistered
        {
            get { return m_isProgramRegistered; }
            set { m_isProgramRegistered = value; }
        }
        
        public bool IsLoanRegistrationDisabled { get; set; }
        public bool IsDisplayReserveMonths { get; set; }

        public E_sDisclosureRegulationT sDisclosureRegulationT { get; set; }

        private LosConvert m_losConvert = new LosConvert();

        protected override void Render(HtmlTextWriter output)
        {
            // 01/21/08 mf OPM 19782. We need to hide Teaser Rate column depending on a broker
            // condition. Refactored a little here to make conditional column rendering easier.
            // Basically, we hide the Teaser rate column if this lender has set the corporate
            // option IsOptionARMUsedInPml (OPM 19782). If this scenario has a no-income
            // doc type we hide the DTI and Qualify Rate columns (OPM 8952).

            int rateColumns = 7 - (m_isNoIncome ? 1 : 0) - (BrokerDB.IsOptionARMUsedInPml ? 0 : 1);

            string eligible = m_applicantPrice.Status == E_EvalStatus.Eval_Eligible ? "1" : "0";

            var isShowCashToClose = BrokerDB.EnableCashToCloseAndReservesDebugInPML;

            var endColumn = "<td colspan=2>&nbsp;</td>";

            if (isShowCashToClose)
            {
                endColumn += "<td></td>";
            }

            // Render LP Name.
            output.Write("<tr class='{1}'><td colspan={2} class='PrdNm'>{0}</td>{3}<td colspan='3'></td></tr>",
                AspxTools.HtmlString(m_applicantPrice.lLpTemplateNm), // 0
                m_cssClassName, // 1
                rateColumns - 1, // 2
                endColumn);
            if (m_applicantPrice.Status == E_EvalStatus.Eval_Ineligible)
            {
                // 10/5/2005 dd - OPM 2804 - Display disqualifying reason for users who can apply for disqualified loans.
                output.Write("<tr class='{1}'><td colspan={2} style='padding-left:20px'>{0}</td></tr>",
                    Page.Server.HtmlDecode(m_applicantPrice.DisqualifiedRules), // 0
                    m_cssClassName, // 1
                    rateColumns + 4 + (isShowCashToClose ? 1 : 0)
                    );
            }

            output.Write("<tbody id='L_{0}'><tr class='{1}'><td valign=top class='c0'>"
                + (m_applicantPrice.ApplicantRateOptions.Length == 0 ? "&nbsp;" : "<a href='#' onclick='return f00({2});'>preview</a>")
                + "</td>", this.ID, m_cssClassName, m_applicantPrice.TempIndex);

            if (m_applicantPrice.ApplicantRateOptions.Length == 1)
            {
                // 10/4/2005 dd - Do not display view more checkbox if there is only 1 rate option.
                output.Write("<td class='c1'>&nbsp;</td>");
            }
            else if (m_applicantPrice.ApplicantRateOptions.Length == 0)
            {
                // 1/16/2013 OPM 105703 - moved down to rate column logic

                // 06/26/09 OPM 2950.  There can now be disqualified programs with no valid rate options.
                //output.Write("<td colspan='"
                //+ (rateColumns + 2).ToString()
                //+ "' class='c1'>&nbsp;</td>");
                //+ "' class='c1'>&nbsp;</td><td class='c1' >&nbsp;</td>");
            }
            else
            {
                output.Write("<td valign=top class='c1' nowrap><label for='L{0}'>View More </label><input id='L{0}' type=checkbox onclick='f_M(\"{0}\", this, {1});updateSize();'></td>", this.ID, m_applicantPrice.TempIndex);
            }


            CApplicantRateOption rateOption = null;

            if (m_applicantPrice.RepresentativeRateOption.Length > 0 && m_applicantPrice.RepresentativeRateOption[0] != null)
            {
                rateOption = m_applicantPrice.RepresentativeRateOption[0];
            }
            else if(m_applicantPrice.ApplicantRateOptions.Length > 0)
            {
                rateOption = m_applicantPrice.ApplicantRateOptions[0];
            }

            if(rateOption != null)
            {
                rateOption.SetApplicantPrice(m_applicantPrice);
                string bottomRatio = rateOption.BottomRatio_rep;

                if (bottomRatio == "-1.000" || bottomRatio == "")
                {
                    // 9/11/2006 dd - OPM 7345
                    bottomRatio = "N/A";
                }
                else
                {
                    if (rateOption.IsViolateMaxDti)
                    {
                        bottomRatio = "** " + bottomRatio;
                    }
                }
                output.Write("<td class=c2>{0}</td>", RenderSubmitLink(rateOption, m_applicantPrice.IsBlockedRateLockSubmission));

                string cssClass = m_applicantPrice.AreRatesExpired ? "rie" : "ri";

                if (BrokerDB.IsOptionARMUsedInPml)
                {
                    output.Write(RenderTableCell(rateOption.TeaserRate_rep, "ri"));
                }
                output.Write(RenderTableCell(rateOption.Rate_rep, cssClass));

                // OPM 64253.  If lender has a special option turned on, we want to 
                // display the point value as adjusted for originator comp.
                string pointRep;
                if (BrokerDB.IsUsePriceIncludingCompensationInPricingResult && IsLenderPaidOriginatorCompAndAdditionToFees)
                {
                    // OPM 64253
                    pointRep = rateOption.PointIncludingOriginatorComp_rep;
                }
                else
                {
                    pointRep = rateOption.Point_rep;
                }



                output.Write(RenderTableCell(CApplicantRateOption.PointFeeInResult(DisplayPmlFeeIn100Format, pointRep), cssClass));

                output.Write(RenderTableCell(rateOption.FirstPmtAmt_rep, cssClass));

                if (!m_isNoIncome)
                {
                    var maxDti = m_applicantPrice.MaxDti_rep == "" ? "N/A" : m_applicantPrice.MaxDti_rep.Replace("%", "");
                    var maxDtiMessage = "<a href='#' class='displayMsgLink' displayMsg='Max DTI: " + maxDti + "'/>" + bottomRatio + "</a>";
                    output.Write(RenderTableCell(maxDtiMessage, cssClass + (rateOption.IsViolateMaxDti ? " ViolateDti" : "")));
                }
                
                output.Write(RenderTableCell(rateOption.Apr_rep, cssClass));

                /* If you want to change how the closing cost breakdown is rendered please also see look for the same
                 * comment(s) in GetResultsNewUI.ascx, GetResultsNewUI.ascx.cs, LoanProductResultControlNewUI.cs, LoanComparisonXml.cs.
                 */
                // This sorts
                if (this.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    // Sort by Loan Section then by Hudline
                    rateOption.ClosingCostBreakdown.Sort(delegate(RespaFeeItem a, RespaFeeItem b)
                    {
                        int loanSecCompareVal = a.DisclosureSectionT.CompareTo(b.DisclosureSectionT);
                        if (loanSecCompareVal == 0)
                        {
                            return int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine));
                        }
                        else
                        {
                            return loanSecCompareVal;
                        }
                    });
                }
                else
                {
                    rateOption.ClosingCostBreakdown.Sort((a, b) => int.Parse(a.HudLine).CompareTo(int.Parse(b.HudLine))); // sort closing cost breakdown by hudline
                }

                var closingCostsBreakdown = ObsoleteSerializationHelper.JavascriptJsonSerialize(rateOption.ClosingCostBreakdown.Select(item => new
                {
                    HudLine = AspxTools.JsStringUnquoted(item.HudLine),
                    Description = AspxTools.JsStringUnquoted(item.Description),
                    Fee = m_losConvert.ToMoneyString(item.Fee, FormatDirection.ToRep),
                    Source = item.DescriptionSource,
                    LoanSec = Tools.GetIntegratedDisclosureString(item.DisclosureSectionT)
                }));

                // This renders the link to display the breakdown.
                var closingCostsLink = "<a href='#' class='displayCostsLink' " +
                                       "closingCostBreakdown='" + closingCostsBreakdown + "'" +
                                       "ClosingCost='" + AspxTools.HtmlString(rateOption.ClosingCost) + "'" +
                                       "PrepaidCharges='" + AspxTools.HtmlString(rateOption.PrepaidCharges) + "'" +
                                       "NonPrepaidCharges='" + AspxTools.HtmlString(rateOption.NonPrepaidCharges) + "'" +
                                       "LoanProgram='" + AspxTools.HtmlString(rateOption.LpTemplateNm) + "'" +
                                       "NoteRate='" + rateOption.Rate_rep + "'" +
                                       "TRID='" + this.sDisclosureRegulationT.ToString("D") + "'" +
                                       ">" + AspxTools.HtmlString(rateOption.ClosingCost) + "</a>";
                output.Write(RenderTableCell(closingCostsLink, cssClass));

                if (BrokerDB.EnableCashToCloseAndReservesDebugInPML)
                {
                    var loanData = rateOption.LoanData;

                    var cashToCloseLink = "<a href='#' class='displayCashToCloseLink' " +
                        "LoanProgram='" + AspxTools.HtmlString(rateOption.LpTemplateNm) + "' " +
                        "NoteRate='" + rateOption.Rate_rep + "' " +
                        "sPurchPrice='" + AspxTools.HtmlString(loanData.sPurchPrice) + "' " +
                        "sAltCost='" + AspxTools.HtmlString(loanData.sAltCost) + "' " +
                        "sLandCost='" + AspxTools.HtmlString(loanData.sLandCost) + "' " +
                        "sRefPdOffAmt1003='" + AspxTools.HtmlString(loanData.sRefPdOffAmt1003) + "' " +
                        "sTotEstPp1003='" + AspxTools.HtmlString(loanData.sTotEstPp1003) + "' " +
                        "sTotEstCcNoDiscnt1003='" + AspxTools.HtmlString(loanData.sTotEstCcNoDiscnt1003) + "' " +
                        "sFfUfmip1003='" + AspxTools.HtmlString(loanData.sFfUfmip1003) + "' " +
                        "sLDiscnt1003='" + AspxTools.HtmlString(loanData.sLDiscnt1003) + "' " +
                        "sTotTransC='" + AspxTools.HtmlString(loanData.sTotTransC) + "' " +
                        "sONewFinBal='" + AspxTools.HtmlString(loanData.sONewFinBal) + "' " +
                        "sTotCcPbs='" + AspxTools.HtmlString(loanData.sTotCcPbs) + "' " +
                        "sOCredit1Desc='" + AspxTools.HtmlString(loanData.sOCredit1Desc) + "' " +
                        "sOCredit1Amt='" + AspxTools.HtmlString(loanData.sOCredit1Amt) + "' " +
                        "sOCredit2Desc='" + AspxTools.HtmlString(loanData.sOCredit2Desc) + "' " +
                        "sOCredit2Amt='" + AspxTools.HtmlString(loanData.sOCredit2Amt) + "' " +
                        "sOCredit3Desc='" + AspxTools.HtmlString(loanData.sOCredit3Desc) + "' " +
                        "sOCredit3Amt='" + AspxTools.HtmlString(loanData.sOCredit3Amt) + "' " +
                        "sOCredit4Desc='" + AspxTools.HtmlString(loanData.sOCredit4Desc) + "' " +
                        "sOCredit4Amt='" + AspxTools.HtmlString(loanData.sOCredit4Amt) + "' " +
                        "sOCredit5Amt='" + AspxTools.HtmlString(loanData.sOCredit5Amt) + "' " +
                        "sONewFinCc='" + AspxTools.HtmlString(loanData.sONewFinCc) + "' " +
                        "sTotLiquidAssets='" + AspxTools.HtmlString(loanData.sTotLiquidAssets) + "' " +
                        "sLAmtCalc='" + AspxTools.HtmlString(loanData.sLAmtCalc) + "' " +
                        "sFfUfmipFinanced='" + AspxTools.HtmlString(loanData.sFfUfmipFinanced) + "' " +
                        "sFinalLAmt='" + AspxTools.HtmlString(loanData.sFinalLAmt) + "' " +
                        "sTransNetCash='" + AspxTools.HtmlString(loanData.sTransNetCash) + "' " +
                        "sIsIncludeProrationsIn1003Details='" + loanData.sIsIncludeProrationsIn1003Details + "' " +
                        "sIsIncludeProrationsInTotPp='" + loanData.sIsIncludeProrationsInTotPp + "' " +
                        "sTotEstPp='" + AspxTools.HtmlString(loanData.sTotEstPp) + "' " +
                        "sTotalBorrowerPaidProrations='" + AspxTools.HtmlString(loanData.sTotalBorrowerPaidProrations) + "' " +
                        "sIsIncludeONewFinCcInTotEstCc='" + loanData.sIsIncludeONewFinCcInTotEstCc + "' " +
                        "sTotEstCcNoDiscnt='" + AspxTools.HtmlString(loanData.sTotEstCcNoDiscnt) + "' " +
                        "sAltCostLckd='" + AspxTools.HtmlString(loanData.sAltCostLckd) + "' " +
                        "sLandIfAcquiredSeparately1003='" + AspxTools.HtmlString(loanData.sLandIfAcquiredSeparately1003) + "' " +
                        "sLDiscnt1003Lckd='" + AspxTools.HtmlString(loanData.sLDiscnt1003Lckd) + "' " +
                        "sPurchasePrice1003='" + AspxTools.HtmlString(loanData.sPurchasePrice1003) + "' " +
                        "sRefNotTransMortgageBalancePayoffAmt='" + AspxTools.HtmlString(loanData.sRefNotTransMortgageBalancePayoffAmt) + "' " +
                        "sRefTransMortgageBalancePayoffAmt='" + AspxTools.HtmlString(loanData.sRefTransMortgageBalancePayoffAmt) + "' " +
                        "sSellerCreditsUlad='" + AspxTools.HtmlString(loanData.sSellerCreditsUlad) + "' " +
                        "sTotCcPbsLocked='" + AspxTools.HtmlString(loanData.sTotCcPbsLocked) + "' " +
                        "sTotCreditsUlad='" + AspxTools.HtmlString(loanData.sTotCreditsUlad) + "' " +
                        "sTotEstBorrCostUlad='" + AspxTools.HtmlString(loanData.sTotEstBorrCostUlad) + "' " +
                        "sTotEstCc1003Lckd='" + AspxTools.HtmlString(loanData.sTotEstCc1003Lckd) + "' " +
                        "sTotEstPp1003Lckd='" + AspxTools.HtmlString(loanData.sTotEstPp1003Lckd) + "' " +
                        "sTotMortLAmtUlad='" + AspxTools.HtmlString(loanData.sTotMortLAmtUlad) + "' " +
                        "sTotMortLTotCreditUlad='" + AspxTools.HtmlString(loanData.sTotMortLTotCreditUlad) + "' " +
                        "sTotOtherCreditsUlad='" + AspxTools.HtmlString(loanData.sTotOtherCreditsUlad) + "' " +
                        "sTotTransCUlad='" + AspxTools.HtmlString(loanData.sTotTransCUlad) + "' " +
                        "sTransNetCashUlad='" + AspxTools.HtmlString(loanData.sTransNetCashUlad) + "' " +
                        "sTRIDSellerCredits='" + AspxTools.HtmlString(loanData.sTRIDSellerCredits) + "' " +
                        ">" + AspxTools.HtmlString(loanData.sTransNetCash) + "</a>";

                    output.Write(RenderTableCell(cashToCloseLink, cssClass));
                }


                if (IsDisplayReserveMonths)
                {
                    if (BrokerDB.EnableCashToCloseAndReservesDebugInPML)
                    {
                        // Link
                        var loanData = rateOption.LoanData;
                        var reservesLink = "<a href='#' class='displayReservesLink' " +
                            "LoanProgram='" + AspxTools.HtmlString(rateOption.LpTemplateNm) + "' " +
                            "NoteRate='" + rateOption.Rate_rep + "' " +
                            "Reserves='" + AspxTools.HtmlString(rateOption.Reserves.Replace("months", "")) + "' " +
                            "sTotLiquidAssets='" + AspxTools.HtmlString(loanData.sTotLiquidAssets) + "' " +
                            "sTransNetCashNonNegative='" + AspxTools.HtmlString(loanData.sTransNetCashNonNegative) + "' " +
                            "sMonthlyPmt='" + AspxTools.HtmlString(loanData.sMonthlyPmt) + "' " +
                            ">" + AspxTools.HtmlString(rateOption.Reserves.Replace("months", "")) + "</a>";

                        output.Write(RenderTableCell(reservesLink, cssClass));
                    }
                    else
                    {
                        // No Link.
                        output.Write(RenderTableCell(rateOption.Reserves.Replace("months", "").TrimWhitespaceAndBOM(), cssClass));
                    }
                }
                else
                {
                    output.Write(RenderTableCell(rateOption.BreakEvenPoint.Replace("months", "").TrimWhitespaceAndBOM(), cssClass));
                }
            }
            else
            {
                output.Write("<td class='c1' colspan='{0}'>&nbsp;</td>", (rateColumns + 3).ToString());
            }
            output.Write("</tr></tbody>");

            if (m_applicantPrice.ApplicantRateOptions.Length > 1)
            {
                output.Write("<tbody id='M_{0}'></tbody>", this.ID);
            }

        }
        private string RenderTableCell(string content, string cssClass)
        {
            return "<td nowrap class='" + cssClass + "'>" + (content == String.Empty ? "&nbsp;" : content) + "</td>";
        }

        private string RenderSubmitLink(CApplicantRateOption rateOption, bool isBlockedRateLockSubmission)
        {

            string margin = rateOption.Margin_rep;

            List<string> newLinks = ResultActionLink.GetLinkTitles(
                m_has2ndLoan
                , m_currentLienQualifyModeT
                , IsPmlSubmissionAllowed
                , false
                , m_LpeRunModeT
                , BrokerDB.IsRateLockedAtSubmission
                , m_canSubmitForLock
                , m_isProgramRegistered
                , !IsLoanRegistrationDisabled
                );

            List<string> oldLinks = ResultActionLink.GetLinkTitles_NoWorkflow(
                m_has2ndLoan
                , m_currentLienQualifyModeT
                , IsPmlSubmissionAllowed
                , false
                , m_LpeRunModeT
                , BrokerDB.IsRateLockedAtSubmission
                );

            bool useOldModel = true;
            BasePage resultsPage = this.Page as BasePage;
            if (resultsPage != null)
            {
                useOldModel = resultsPage.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml);
            }
            else
            {
                // Safety throw. If getresults control ends up on page that does not
                // inherit from basepage, we should work in the security checks there.
                throw new CBaseException(ErrorMessages.Generic, "LoanProductResultControl is child control on page that does not inherit from Base.");
            }

            // Always use old until we get no mismatches
            List<string> titles = useOldModel ? oldLinks : newLinks;

            string linkTitle = null;

            if (isBlockedRateLockSubmission)
            {
                linkTitle = titles[1];
            }
            else
            {
                linkTitle = titles[0];
            }

            if (BrokerDB.IsRateLockedAtSubmission == false && m_LpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                return "&nbsp;"; // 06/11/08 mf - OPM 18996 Re-run float submission without ability to lock means no submit link


            if (!m_applicantPrice.lIsArmMarginDisplayed)
            {
                margin = "0.000";
            }
            string[] parts = linkTitle.Split('/');

            string html = string.Empty;
            if (parts[0] != "?") // No access.
            {
                if (rateOption.IsDisqualified)
                {
                    html += String.Format("&nbsp;<a href='#' class='unavailable' data-func='f01' data-msg={0} data-arg1='{1}' data-arg2='{2}' data-arg3='{3}' data-arg4='{4}'>unavailable</a>", 
                        AspxTools.HtmlAttribute(rateOption.DisqualReason),
                        AspxTools.HtmlString(rateOption.RateOptionId), 
                        this.ID,
                        m_applicantPrice.TempIndex,
                        (UserCanApplyForIneligibleLoanPrograms ? AspxTools.HtmlString(linkTitle) : string.Empty)
                        );
                }
                else if (parts[0] == "register loan?")
                {
                    html = @"<a href='#' onclick='return false;' disabled='disabled' >register</a>";
                    html += "&nbsp;<a href='#' onclick='return f_SRM();'>?</a>";
                }
                else
                {
                    html = string.Format(@"<a href='#' onclick=""return f01('{1}','{2}', {3});"">{0}</a>",
                        AspxTools.HtmlString(parts[0].Replace("register loan", "register")), // 0
                        AspxTools.HtmlString(rateOption.RateOptionId), // 1
                        this.ID, // 2
                        m_applicantPrice.TempIndex // 3

                        );
                }
            }
            else 
            {
                html = @"<a href='#' disabled='disabled'>request lock</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>";
            }

            if (parts.Length > 1 && rateOption.IsDisqualified == false)
            {
                if (parts[1] != "?")
                {
                    html += string.Format(@"&nbsp;&nbsp;<a href='#' onclick=""return f01('{1}','{2}', {3}, true);"">{0}</a>",
                    AspxTools.HtmlString(parts[1].Replace("lock rate", "request lock")), // 0
                    AspxTools.HtmlString(rateOption.RateOptionId), // 1
                    this.ID, // 2
                    m_applicantPrice.TempIndex // 3

                    );
                }
                else
                {
                    html += @"&nbsp;&nbsp;<a href='#' disabled='disabled'>request lock</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>";
                }
            }
            return html;
        }
    }
}
