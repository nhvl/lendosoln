﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Xml;
using System.Text;
using System.IO;
using LendersOffice.Security;
using PriceMyLoan.Common;
using LendersOffice.Admin;
using PriceMyLoan.Security;
using LendersOffice.ObjLib.Resource;

namespace PriceMyLoan.main
{
    public partial class FHATotalFindings : PriceMyLoan.UI.BasePage
    {
        private const string NO_FINDINGS_MESSAGE = @"<div style=""width:100%; color:#565656; font-weight:bold; font-family:verdana,georgia, sans-serif; padding-top:200px; text-align:center;"">There are no FHA TOTAL Scorecard Findings on this loan file.</div>";

#if LQB_NET45
        protected global::System.Web.UI.HtmlControls.HtmlIframe Certificate;
#else
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Certificate;
#endif

        private string XsltFileLocation
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.FindingsXslt, CurrentBroker.BrokerID); }
        }

        private BrokerDB x_brokerDB;
        private BrokerDB CurrentBroker
        {
            get
            {
                if (null == x_brokerDB)
                {
                    x_brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
                }
                return x_brokerDB;
            }
        }


        private bool isAccessDenied
        {
            get
            {
                return !(CurrentBroker.IsTotalScorecardEnabled) || (PriceMyLoanUser.HasPermission(LendersOffice.Security.Permission.CanAccessTotalScorecard) == false);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            //opm 34211 fs 10/30/09 - PML FHA TOTAL Scorecard only enabled for Thinh's broker for now
            if (isAccessDenied)
                PriceMyLoanRequestHelper.AccessDenial(Guid.Empty);

            this.RegisterJsScript("LQBPrintFix.js");

            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHATotalFindings));
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            data.InitLoad();
            
            if (RequestHelper.GetInt("viewcert", -1) != -1)
            {
   
                Response.Clear();

                if (string.IsNullOrEmpty(data.sTotalScoreCertificateXmlContent.Value))
                {
                    Response.Output.WriteLine(NO_FINDINGS_MESSAGE);
                }
                else
                {
                    System.Xml.Xsl.XsltArgumentList arg = new System.Xml.Xsl.XsltArgumentList();
                    arg.AddParam("currentYear", "", DateTime.Now.Year);
                    XslTransformHelper.Transform(XsltFileLocation, data.sTotalScoreCertificateXmlContent.Value, Response.OutputStream, arg);
                }
                Response.End();
            }

            if (string.IsNullOrEmpty(data.sTotalScoreCertificateXmlContent.Value))
            {
                conditionImport.Visible = false;
            }
            if (PriceMyLoanUser.IsSpecialRemnEncompassTotalAccount)
            {
                // 6/14/2012 dd - REMN integration with ENcompass do not need to display close and condition import button.

                conditionImport.Visible = false;
                m_btnClose.Visible = false;
                Response.Redirect("FHATotalFindings.aspx?viewcert=1&loanid=" + LoanID, true);
                return;
            }
            this.RegisterService("FHATotal", "/main/FHATotalSubmitService.aspx");

            Certificate.Attributes.Add("src", "FHATotalFindings.aspx?viewcert=1&loanid=" + LoanID);
            //Certificate.Attributes.Add("onload", "f_resizeIframe('Certificate',20);");
        }
    }
}
