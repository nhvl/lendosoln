<%@ Page language="c#" Codebehind="CreditScoreGlossary.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.CreditScoreGlossary" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>CreditScoreGlossary</title>
  </head>
  <body >
	
    <form id="CreditScoreGlossary" method="post" runat="server">
<h2>Credit Score Type Glossary</h2>
<input type=button value="Print" onclick="window.print();" >
<input type=button value="Close" onclick="parent.close();" >
<ul>
  <li><b>Credit Score Type 1&nbsp;:</b>
    <p>Middle of 3 or lower of 2 scores for the primary wage earner. At least 2 scores are required for primary wage earner.</p>
  <li><b>Credit Score Type 2&nbsp;:</b>
    <p>Lowest borrower using middle of 3 or lower of 2 for each borrower. At least 2 scores are required for each borrower.</p>
  <li><b>Credit Score Type 3&nbsp;: </b>
    <p>Average score of the primary borrower.</p>
  </li>
  <li><b>MAX&nbsp;:</b>
    <p>Highest credit score of all borrowers.</p>
  </li>
</ul>
     </form>
	
  </body>
</html>
