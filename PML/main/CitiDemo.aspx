<%@ Page language="c#" Codebehind="CitiDemo.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.CitiDemo" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <head runat="server">
    <title>CitiDemo</title>
  </head>
  <body MS_POSITIONING="FlowLayout" onload="init();">
	<script language=javascript>
<!--
function init() {
  <% if (m_launch) { %>
    f_launchLogin();
    <% if (cbIsImport.Checked) { %>
    setTimeout('f_edit();', <%= AspxTools.JsNumeric(int.Parse(tbWait.Text) * 1000) %>);
    <% } %>
  <% } %>
}
function f_launchLogin() {
  var w = window.open('CitiDemo.aspx?action=post&keyid=' + <%= AspxTools.JsString(m_loginKeyId) %>, 'citi');
  w.focus();
}
function f_edit() {
  window.open('CitiDemo.aspx?action=post&keyid=' + <%= AspxTools.JsString(m_editKeyId) %>, 'citi');

}
//-->
</script>

    <form id="CitiDemo" method="post" runat="server">
<TABLE id=Table1 cellSpacing=1 cellPadding=1 width=300 border=1>
  <TR>
    <TD colSpan=2>EXPERIMENTAL FEATURE - PML &amp; CITI INTEGRATION</TD></TR>
  <TR>
    <TD></TD>
    <TD><asp:RadioButtonList id=rblServers runat="server">
<asp:ListItem Value="Production" Selected="True">Production</asp:ListItem>
<asp:ListItem Value="Integration">Integration</asp:ListItem>
</asp:RadioButtonList></TD></TR>
  <TR>
    <TD>Login Name</TD>
    <TD><asp:TextBox id=tbUserName runat="server"></asp:TextBox></TD></TR>
  <TR>
    <TD>Password</TD>
    <TD><asp:TextBox id=tbPassword runat="server" TextMode="Password"></asp:TextBox></TD></TR>
  <TR>
    <TD nowrap >Wait between Login and Edit Page (seconds)</TD>
    <TD><asp:TextBox id=tbWait runat="server">10</asp:TextBox></TD></TR>
  <TR>
    <TD noWrap>Is Import FNMA File</TD>
    <TD><asp:CheckBox id=cbIsImport runat="server" Text="Yes" Checked="True"></asp:CheckBox></TD></TR>
  <TR>
    <TD></TD>
    <TD><asp:Button id=submitToCiti runat="server" Text="Submit to Citi" onclick="SubmitToCiti"></asp:Button></TD></TR></TABLE><ml:EncodedLiteral id=ltStatus runat="server"></ml:EncodedLiteral>

     </form>
	
  </body>
</HTML>
