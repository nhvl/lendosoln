<%@ Page language="c#" Codebehind="ViewCredit.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.ViewCredit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
  <title>Credit Report</title>
</head>
<body ms_positioning="FlowLayout" class="RightBackground">
  <script type="text/javascript">
    function f_close() {
      self.close();
    }
  </script>
  <form id="ViewCredit" method="post" runat="server">

  <asp:PlaceHolder runat="server" ID="MessageTable" Visible="true">
  <table id="Table1" cellspacing="0" cellpadding="0" width="100%" height="90%" align="center" border="0">
    <tr>
      <td style="font-weight: bold; font-size: 12pt;font-family: Arial, Helvetica, sans-serif;" valign="center" align="middle">
        <ml:EncodedLiteral ID="m_message" runat="server" />
        <br /><br />
        <div id="m_panelButtons" runat="server">
        <asp:Button ID="m_btnContinue" runat="server" Text="Accept" />
        <input type="button" value="Cancel"  runat="server" id="cancelbtn" onclick="window.parent.parent.LQBPopup.Hide()" />
        </div>
      </td>
    </tr>
  </table>
  </asp:PlaceHolder>
  <asp:PlaceHolder runat="server" ID="PdfHolder" Visible="false">
    <frameset id="PdfFrameSet">
        <iframe id="PdfFrame" runat="server"></iframe>
    </frameset>
  </asp:PlaceHolder>
  </form>
</body>
</html>
