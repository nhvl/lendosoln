using System;
using DataAccess;
using LendersOffice.Common;
namespace PriceMyLoan.main
{

	public partial class CertificateWaitPage : PriceMyLoan.UI.BasePage
	{

        protected string ProductID 
        {
            get { return RequestHelper.GetSafeQueryString("productid"); }
        }
        protected E_sLienQualifyModeT sLienQualifyModeT 
        {
            get { return (E_sLienQualifyModeT) RequestHelper.GetInt("lienqualifymodet", 0); }
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("main", "/main/CertificateWaitPageService.aspx");
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

            this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
