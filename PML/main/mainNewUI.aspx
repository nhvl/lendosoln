<%@ Page language="c#" Codebehind="mainNewUI.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.mainNewUI" enableViewState="False" ValidateRequest="true"%>
<%@ Register TagPrefix="uc1" TagName="GetResultsNewUI" Src="GetResultsNewUI.ascx" %>
<%@ Import Namespace="PriceMyLoan.Common"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"> 
<html>
	<head runat="server">
		<title>Price My Loan</title>
        <style type="text/css">
            body { margin: 0; }
            #ErrorPanel
            {
                color: red;
                font-weight: bold;
            }
            .MainWarning
            {
                border-right: #ffcc99 1px dashed;
                border-top: #ffcc99 1px dashed;
                font-weight: bold;
                border-left: #ffcc99 1px dashed;
                color: red;
                border-bottom: #ffcc99 1px dashed;
                background-color: #ffffcc;
            }
            .amodal
            {
                line-height: 1em;
                height: 100px;
                border: 3px inset black;
                top: 150px;
                left: 0;
                right: 0;
                width: 600px;
                display: none;
                position: absolute;
                background-color: whitesmoke;
                padding: 5px;
                font-family:Verdana , Arial, Helvetica, sans-serif;
                font-size: 11px;
            }
            
            .amodal ol
            {
                list-style-type: upper-alpha; 
            }
            .amodal ol li 
            {
                padding-bottom: 5px;
            }            
        </style>
	</head>
	<body onunload="f_onunload();">	
		<script  type="text/javascript">
          <!--
          var g_startRenderPage = (new Date()).getTime();
          var _popupWindow = null;
          var _nameOfChild = null;
          var _bSubmitting = false;
          var _currentIndex = <%= AspxTools.JsNumeric(m_currentIndex) %>;
          var _maxVisitedIndex = <%= AspxTools.JsNumeric(m_maxVisitedIndex) %>;
          var MAX_STEPS = <%= AspxTools.JsNumeric(m_maxSteps) %>;
          var g_bIsEmbeddedPml = false;
          var g_loanId = <%= AspxTools.JsString(LoanID) %>;
            
          function f_hideNavigation() { /* to keep getresult.js happy */ }
          function f_onunload(){
            if (null != _popupWindow && !_popupWindow.closed) _popupWindow.close();
          }
          function f_allowClick(){
	        return _popupWindow == null || _popupWindow.closed == true;
          }
          
          function f_updateChildName( sNameOfChild ){
            if( sNameOfChild != null && sNameOfChild != "" ){
	          _nameOfChild = sNameOfChild;
            }
            else {
              _nameOfChild = null;
            }
          }
          
          function f_smartFocus(){
            if (typeof(f_focus_1stRequired) == 'function'){
              f_focus_1stRequired();
            }
          }  
  
          function _init(){
            <% if (m_hasError == false) { %>
              var index =  _statuses.length;
            <% } %>
            f_hideSummary();
            if (typeof(f_uc_init) == 'function'){
              f_uc_init();
            }
            if (typeof(f_onValidateRequiredFields) == "function"){
              f_onValidateRequiredFields();
            }
            f_smartFocus();
        }
        
        function f_setFileVersion(sFileVersion){
            document.getElementById("sFileVersion").value = sFileVersion;  
        }
        
        function f_hideSummary(){
            if(typeof(parent.f_displayLoanSummary) == "function"){
                if(arguments.length == 0){
                    parent.f_displayLoanSummary(false);
                }
                else{
                    parent.f_displayLoanSummary(false, arguments[0]);
                }
            }
        }
        
        
        <%-- // Possible values for d are -1, 1 for back and continue, respectively. --%>
        function f_go(event, d){
  	        if(!f_allowClick()){
  	            return false;
            }
		
		    if (typeof(ML) != 'undefined' && ML.ErrorGobackUrl != '' && ML.ErrorGobackUrl != null){
		        self.location = ML.ErrorGobackUrl;
		        return;
		    }
		
		    if (d == -1 && _currentIndex == 0 && !g_bIsEmbeddedPml){
		        self.location = "agents.aspx?loanid=" + g_loanId;
            }
            else if ((d == 1 && _currentIndex < MAX_STEPS - 2) || (d == -1 && _currentIndex > 0)){
                var bForceValidate = true;
                if (d == -1){
                    bForceValidate = false;
                }
                f_switchTab(_currentIndex + d, bForceValidate, null, event);
            }
        }
        
  function f_clickStep(event, img) {
    var i = parseInt(img.id.substr(3));
    var allowableIndex = _currentIndex < _maxVisitedIndex ? _maxVisitedIndex : _currentIndex;
	if( !f_allowClick() )
		return false;
		
		var bForceValidate = true;
		if (i < _currentIndex)
		  bForceValidate = false;
		  
    if (f_isTabCompleted(i)) f_switchTab(i, bForceValidate, null, event);
    else if (i == allowableIndex + 1) f_switchTab(i, bForceValidate, null, event);
  }
  
  function f_step4Circumstance_onclick() {
    Modal.ShowPopupNewUI('<%= AspxTools.ClientId(Step4CircumstancesDiv) %>');
    return false;
  }
  
  function f_isTabCompleted(i) {
    var img = document.getElementById("img" + i);
    if (img != null) {
      return img.status == "c";
    } else {
      return false;
    }
  }
  
  function f_setTabCompleted(i, bCompleted) {
    var img = document.getElementById("img" + i);
    if (img != null) {
      img.status = bCompleted ? "c" : "i";
    }
  }
  
  function f_getResult(event) {

    var allowableIndex = _currentIndex < _maxVisitedIndex ? _maxVisitedIndex : _currentIndex;  
    var i = <%= AspxTools.JsNumeric(m_maxSteps - 1) %>;
	if( !f_allowClick() )
		return false;
    if (i == allowableIndex + 1 || f_isTabCompleted(i)) f_switchTab(i, null, null, event);  
  }
  function f_onResultClose(){
	parent.PML.goBack();
  }
  function f_openWarningWindow(){
    parent.LQBPopup.Show( gVirtualRoot +  "/main/ListModifiedFields.aspx?loanid=" + g_loanId, {width:450, height:700, hideCloseButton: true} );
  }
  		    <% if (IsEncompassIntegration) { %>
    function f_sendToEncompass(w) {
        var args = new Object();
        var result = gService.main.call("GetResult_GenerateByPassTicket", args);
        var authid='';
        if (!result.error) {
          authid = result.value["Ticket"];
        }
       if(typeof(w) != 'undefined' && w != null && typeof(w.close) != 'undefined')
       {
           w.close();
       }
       setTimeout(function(){cedeControlToEncompass(authid)}, 1000);
    }
    function cedeControlToEncompass(authid)
    {
        location.href = '_EPASS_SIGNATURE;PRICEMYLOAN;;RETRIEVELOAN;' + <%= AspxTools.JsString(m_loanIdEncompassRequestType) %> + ';' + authid;
    }
    <% } %>
	var g_endRenderPage = (new Date()).getTime();
    
  //-->
		</script>
		<form id="main" method="post" runat="server" autocomplete="off" enctype="multipart/form-data">			
			<div runat="server" id="Step4CircumstancesDiv" class="amodal" style="z-index:500">
			    
                <ml:EncodedLiteral ID="Step4CircumstancesMessage" runat="server" />

                <ol>
                    <asp:Repeater runat="server" ID="Step4CircumstancesList" >
                        <ItemTemplate>
                            <li><%# AspxTools.HtmlString((string)Container.DataItem) %></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ol>

			    <div style="text-align:center;">
			        
			    <input type="button" value="Close" onclick="Modal.Hide()" />
			    </div>
			</div>
			
			<uc1:GetResultsNewUI id="m_getResults" runat="server" />
			<div id="ErrorPanel" runat="server">
			  <ml:EncodedLiteral ID="m_errorMessage" runat="server" />
			</div>
		</form>
	</body>
</HTML>