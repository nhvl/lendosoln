﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using DataAccess;
using LendersOffice.Security;
using PriceMyLoan.Common;
using LendersOffice.ObjLib.QuickPricer;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace PriceMyLoan.UI.main
{
    public partial class RateMonitorDialog : PriceMyLoan.UI.BasePage
    {
        private Guid m_loanId;
        private string m_bpgroup;
        private long? m_monitorToUpdate;
        private CPageData m_dataLoan;

        protected void PageInit(object sender, System.EventArgs e)
        {
            m_bpgroup = Request["bpgroupname"];
            m_loanId = new Guid(Request["loanid"]);

            m_dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(m_loanId, typeof(RateMonitorDialog));
            m_dataLoan.AllowLoadWhileQP2Sandboxed = true;
            m_dataLoan.InitLoad();
            if(E_sLoanFileT.QuickPricer2Sandboxed == m_dataLoan.sLoanFileT)
            {
                var usrMsg = "May not run monitors on quickpricer sandboxed files.";
                if (PriceMyLoanConstants.IsEmbeddedPML && false == m_dataLoan.BrokerDB.AllowRateMonitorForQP2FilesInEmdeddedPml)
                {
                    throw new CBaseException(usrMsg, usrMsg);
                }
                else if(false == PriceMyLoanConstants.IsEmbeddedPML && false == m_dataLoan.BrokerDB.AllowRateMonitorForQP2FilesInTpoPml)
                {
                    throw new CBaseException(usrMsg, usrMsg);
                }
            }

            //See if there is already a monitor for this Loan & Pricing Group
            //Initialize fields accordingly
            object[] existing = CheckExistingMonitor(m_dataLoan.sBrokerId);
            if (existing == null)
            {
                m_DialogMessage.Text = "Enter a target rate & points to receive notification when the rate is available.";
                m_email.Text = PriceMyLoanUser.Email;

                m_monitorToUpdate = null;
            }
            else
            {
                m_DialogMessage.Text = "A rate monitor already exists. Enter a new target rate & point to update the existing monitor.";
                m_rate.Text = existing[1].ToString()+"%";
                m_point.Text = existing[2].ToString();
                m_email.Text = existing[3].ToString();

                m_monitorToUpdate = Convert.ToInt64(existing[0]);
            }
            m_programName.Text = m_bpgroup;

            var isQP2File = E_sLoanFileT.QuickPricer2Sandboxed == m_dataLoan.sLoanFileT;;
            if(isQP2File)
            {
                m_nameRowHolder.Visible = true;
                if (false == Page.IsPostBack)
                {
                    m_name.Text = QuickPricer2LoanPoolManager.GetRateMonitorNameByLoanID(m_dataLoan.sBrokerId, m_loanId, PriceMyLoanUser.UserId);
                }
            }            
        }

        protected void ValidateRateMonitor(object sender, System.EventArgs e)
        {
            //Initialize to no errors, assign them as needed
            m_ErrorMessageText.InnerText = "";
            m_rateErrorIcon.Style["visibility"] = "hidden";
            m_pointErrorIcon.Style["visibility"] = "hidden";
            m_emailErrorIcon.Style["visibility"] = "hidden";
            m_nameErrorIcon.Style["visibility"] = "hidden";

            float rate = 0, point = 0;
            string emailText = "";
            string name = null;

            //Ensure required fields are defined
            //- Store them in local fields in the process
            bool undefError = false;

            try
            {
                rate = float.Parse(m_rate.Text.Replace("%", ""));
            }
            catch (FormatException)
            {
                undefError = true;
                m_rateErrorIcon.Style["visibility"] = "visible";
            }
            try
            {
                point = float.Parse(m_point.Text);
            }
            catch (FormatException)
            {
                undefError = true;
                m_pointErrorIcon.Style["visibility"] = "visible";
            }

            // Check name field (only need for quickpricer 2.0 sandboxed files.)
            if (E_sLoanFileT.QuickPricer2Sandboxed == m_dataLoan.sLoanFileT)
            {
                name = m_name.Text.TrimWhitespaceAndBOM();
                if (string.IsNullOrEmpty(name))
                {
                    undefError = true;
                    m_nameErrorIcon.Style["visibility"] = "visible";
                }
            }

            emailText = m_email.Text;

            if (emailText.Equals(""))
            {
                undefError = true;
                m_emailErrorIcon.Style["visibility"] = "visible";
            }

            if (undefError)
            {
                m_ErrorMessageText.InnerText = "Please fill out all of the required fields above.";
                //Don't continue until error resolved
                return;
            }

            //Everything between semi-colons or commas must be a valid email address
            string[] emails = emailText.Split(';', ',');
            string pattern = ConstApp.EmailValidationExpression;
            bool invalidEmail=false;

            //Check 'email' field
            foreach (string email in emails)
            {
                if (!Regex.IsMatch(email.TrimWhitespaceAndBOM(), pattern))
                {
                    invalidEmail = true;
                    m_emailErrorIcon.Style["visibility"] = "visible";
                    break;
                }
            }

            if (invalidEmail)
            {
                //Don't continue until error resolved
                return;
            }

            //Make sure 'rate' is positive
            if (rate <= 0)
            {
                m_rateErrorIcon.Style["visibility"] = "visible";
                m_ErrorMessageText.InnerText = "Rate must be a positive number.";
                //Don't continue until error resolved
                return;
            }            

            //No Errors Found -> Submit Monitor and close window
            var isForQP2File = E_sLoanFileT.QuickPricer2Sandboxed == m_dataLoan.sLoanFileT;
            E_SqlExceptionReason? reasonFailed;
            if (false == SubmitRateMonitor(rate, point, emailText, name, isForQP2File, m_dataLoan.sBrokerId, m_dataLoan.sLId, out reasonFailed))
            {
                string msg = null;
                switch(reasonFailed.Value)
                {
                    case E_SqlExceptionReason.RateMonitorNameExists:
                        msg = "Rate monitor name already exists.";
                        break;
                    case E_SqlExceptionReason.Unknown:
                        msg = "An unexpected error occurred.  Please contact us if the problem continues.";
                        break;
                    default:
                        throw new UnhandledEnumException(reasonFailed.Value);
                }
                m_ErrorMessageText.InnerText = msg;
                return;
            }
            ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true); 
        }

        // If this monitor exists, return [monitorId, rate, point, email, cc]
        // Otherwise, null
        protected object[] CheckExistingMonitor(Guid brokerId)
        {
            object[] existing = null;
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@sLId", m_loanId),
                                                new SqlParameter("@bpdesc", m_bpgroup)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RateMonitorLoad", parameters))
                {
                    if (reader.Read())
                    {
                        existing = new object[5];
                        reader.GetValues(existing);
                    }
                }
            }
            catch (SqlException e)
            {
                Tools.LogError("RateMonitorDialog.CheckExistingMonitor() threw SqlException");
                Tools.LogError(e);
            }
            return existing;
        }

        public enum E_SqlExceptionReason
        {
            Unknown = 0,
            RateMonitorNameExists = 1
        }
           
        /// <summary>
        /// Create a new monitor or update if an existing one was found on Load
        /// </summary>
        /// <param name="name">Only need this to be non-null when the loan is qp2.0 sandboxed.</param>
        protected bool SubmitRateMonitor(float rate, float point, string email, string name, bool isForQP2File, Guid brokerID, Guid loanID, out E_SqlExceptionReason? reasonFailed)
        {
            reasonFailed = null;
            try
            {
                if (m_monitorToUpdate == null)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>()
                    {
                        GetTypedSqlParameter("@sLId", SqlDbType.UniqueIdentifier, m_loanId),
                        GetTypedSqlParameter("@rate", SqlDbType.Decimal, rate),
                        GetTypedSqlParameter("@point", SqlDbType.Decimal, point),
                        GetTypedSqlParameter("@email", SqlDbType.VarChar, email),
                        GetTypedSqlParameter("@bpdesc", SqlDbType.VarChar, m_bpgroup),
                        GetTypedSqlParameter("@currDate", SqlDbType.DateTime, DateTime.Now),
                        GetTypedSqlParameter("@createdBy", SqlDbType.UniqueIdentifier, PriceMyLoanUser.UserId),
                        GetTypedSqlParameter("@createdByType", SqlDbType.Char, PriceMyLoanUser.Type),
                        GetTypedSqlParameter("@doQP2FileUpdate", SqlDbType.Bit, isForQP2File),
                        GetTypedSqlParameter("@rateMonitorScenarioName", SqlDbType.VarChar, name),
                        GetTypedSqlParameter("@brokerID", SqlDbType.UniqueIdentifier, brokerID)     
                    };

                    StoredProcedureHelper.ExecuteNonQuery(brokerID, "RateMonitorNew", 0, parameters);
                }
                else
                {
                    List<SqlParameter> parameters = new List<SqlParameter>()
                    {
                        GetTypedSqlParameter("@monitorid", SqlDbType.BigInt, m_monitorToUpdate),
                        GetTypedSqlParameter("@rate", SqlDbType.Decimal, rate),
                        GetTypedSqlParameter("@point", SqlDbType.Decimal, point),
                        GetTypedSqlParameter("@email", SqlDbType.VarChar, email),
                        GetTypedSqlParameter("@currDate", SqlDbType.DateTime, DateTime.Now),
                        GetTypedSqlParameter("@sLId", SqlDbType.UniqueIdentifier, m_loanId),
                        GetTypedSqlParameter("@doQP2FileUpdate", SqlDbType.Bit, isForQP2File),
                        GetTypedSqlParameter("@rateMonitorScenarioName", SqlDbType.VarChar, name),
                        GetTypedSqlParameter("@brokerID", SqlDbType.UniqueIdentifier, brokerID),
                        GetTypedSqlParameter("@createdBy", SqlDbType.UniqueIdentifier, PriceMyLoanUser.UserId),
                    };

                    StoredProcedureHelper.ExecuteNonQuery(brokerID, "RateMonitorUpdate", 0, parameters);
                }
                return true;
            }
            catch (SqlException e)
            {
                if (e.Message.Contains("rate monitor name already exists for that user."))
                {
                    reasonFailed = E_SqlExceptionReason.RateMonitorNameExists;
                }
                else
                {
                    reasonFailed = E_SqlExceptionReason.Unknown;
                    Tools.LogError("RateMonitorDialog.SubmitRateMonitor() threw SqlException", e);
                }
                return false;
            }
        }
        private static SqlParameter GetTypedSqlParameter(string field, SqlDbType type, object value)
        {
            SqlParameter p = new SqlParameter(field, type);
            if (value != null)
            {
                p.Value = value;
            }
            else
            {
                p.Value = DBNull.Value;
            }
            return p;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
