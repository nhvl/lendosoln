<%@ Page language="c#" Codebehind="pipeline.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.UI.Main.pipeline" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="Nav" Src="~/main/AccordionNav.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>PriceMyLoan</title>
    <!-- This application uses open source software. For more information please see https://secure.pricemyloan.com/inc/thirdpartysoftware.txt. -->
</head>
<body class="app header-fixed hidden">

<script>
    function showEditUser(url, settings) {
        return IframePopup.show(url, settings).then(function (data) {
            var error = data[0];
            var shouldReload = data[1];
            if (error != null) {
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (shouldReload) reloadUsersManager();
            }
        });

        function reloadUsersManager() {
            window.frames[0].postback();
        }
    }

    $j(function () {
        // -------------------------------------------------
        var animationDelay = 0;

        function collapseNonSelectedPanels(curr) {
            var $aParentUl = $j('#nav'), selectedAnchor = $aParentUl.find('a.selected');

            //close any open folders that are not parents of selected anchor.
            $aParentUl.find('li').each(function () {
                var $this = $j(this);
                if ($this.find('a').index(selectedAnchor) < 0) {
                    if ($this.find('a').index(curr) < 0) {
                        $this.children('ul').slideUp(animationDelay);
                        $this.children('a').removeClass('subdrop');
                        $j(this).find("i").toggleClass("arrow-rotate", false);
                        $this.toggleClass("selected-folder", false);
                    }
                }
                else {
                    $this.toggleClass("selected-folder", true);
                }
            });
        }

        function callPipelineMethod(name, args, success, error) {
            callWebMethodAsync({
                url: 'Pipeline.aspx/' + name,
                type: 'POST',
                async: true,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(args),
                success: success,
                error: error
            });
        }

        function pickPipelineReport() {
            var url = VRoot + "/webapp/PipelineReportPicker.aspx?";
            var settings = {
                width: 825,
                height: 365,
                onReturn: onPipelineReportPickerReturn
            };

            LQBPopup.Show(url, settings);
        }

        function onPipelineReportPickerReturn(result) {
            if (!result.OK) {
                return;
            }

            var args = {
                selectedReportsJson: result.SelectedReportsJson
            };

            callPipelineMethod(
                'UpdatePipelineReportView', 
                args, 
                function onSuccess() { window.location.href = VRoot + '/main/pipeline.aspx?pg=0'; },
                function onError() { simpleDialog.alert('Unable to save settings. Please try again.'); });
        }

        $j(".wrap-sidebar .collapse-arrow").click(function () {
            var $this = $j(this);
            var $parent = $this.parent();
            var isExpanded = $parent.hasClass('toggled-wide');

            $this.find("i").toggleClass("arrow-rotate", !isExpanded);
            $parent.toggleClass("toggled-wide", !isExpanded);
            $j(".app-body").toggleClass("toggled", !isExpanded);
        });

        $j(".sidebar #nav a").on('click', function (e) {
            var $a = $j(this);
            var hasSub = $a.parent().hasClass("has_sub");
            var selectedAnchor = $j('.sidebar a.selected');

            if (hasSub) {
                e.preventDefault();
            }

            if ($a.hasClass("clicksOnce")) {
                $a.attr('disabled', 'disabled');
            }

            //expand a node
            if (!$a.hasClass("subdrop") && hasSub) {
                collapseNonSelectedPanels($a);
                // open our new menu and add the open class
                $j(this).next("ul").slideDown(animationDelay);
                $j(this).find("i").toggleClass("arrow-rotate", true);
                $j(this).addClass("subdrop");
            }
            //handle closing an open menu
            else if ($j(this).hasClass("subdrop")) {
                $j(this).removeClass("subdrop");
                $j(this).next("ul").slideUp(animationDelay);
                $j(this).find("i").toggleClass("arrow-rotate", false);
            }
            else {
                $j(".main-slot").addClass("padding-right-32");
                $j("main").removeClass("main-quick-pricer");
                if ($a.hasClass('within')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    if ($a.hasClass('full-expand')) {
                        $j("html").removeClass("padding-right-32");
                        $j("main").addClass("main-quick-pricer");
                    }

                    var iframePromise = pipelineCtrl.showDashboard($a.data("url"))
                        .then(function (frame) {
                            if ($a.data('sendExternalEndpoints') && !$a.hasClass('non-qm')) {
                                if (frame != null) {
                                    frame.contentWindow.postMessage({ endpoints: [window.location.origin + VRoot + '/ExternalMethods.aspx'] }, $a.data("url"));
                                }
                            }
                        });

                    return false;
                }
                else if ($a.hasClass('pipeline-report-picker')) {
                    pickPipelineReport();
                    return false;
                }
                else if ($a.hasClass('ImportLeadLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.showImport(true);
                }
                else if ($a.hasClass('createPLeadLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("PURCHASE_LEAD", $a.text());
                }
                else if ($a.hasClass('createRLeadLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("REFINANCE_LEAD", $a.text());
                }
                else if ($a.hasClass('createH1LeadLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("HELOC_1STLIEN_LEAD", $a.text());
                }
                else if ($a.hasClass('createH2LeadLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("HELOC_2NDLIEN_LEAD", $a.text());
                }
                else if ($a.hasClass('createSLeadLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("SECONDLIEN_LEAD", $a.text());
                }
                else if ($a.hasClass('ImportLoanLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.showImport(false);
                }
                else if ($a.hasClass('createPLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("PURCHASE", $a.text());
                }
                else if ($a.hasClass('createRLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("REFINANCE", $a.text());
                }
                else if ($a.hasClass('createH1Link')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("HELOC_1STLIEN", $a.text());
                }
                else if ($a.hasClass('createH2Link')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("HELOC_2NDLIEN", $a.text());
                }
                else if ($a.hasClass('createSLink')) {
                    selectedAnchor.removeClass('selected');
                    $a.addClass('selected');
                    collapseNonSelectedPanels($a);

                    pipelineCtrl.beginCreateLoan("SECONDLIEN", $a.text());
                }
                else if ($a.attr('data-outside') === 'true') {
                    if ($a.hasClass('system')) {
                        selectedAnchor.removeClass('selected');
                        $a.addClass('selected');
                        collapseNonSelectedPanels($a);

                        var $a_href = $a[0].href;
                        var pgMatch = $a_href.match(/pipeline\.aspx\?pg=(\d+)$/);
                        var pg = pgMatch == null ? 0 : Number(pgMatch[1]);
                        if (!Number.isNaN(pg)) {
                            pipelineCtrl.navTo(pg);
                            return false;
                        }
                    }

                    if ($a.data('sendExternalEndpoints')) {
                        var opened = window.open($a[0].href, '');
                        // Take advantage of LQBNestedFrameSupport.js to tell when the child is loaded.
                        var postToChild = function (messageEvent) {
                            if ($a[0].href.startsWith(messageEvent.origin) && messageEvent.data === 'loaded') {
                                opened.postMessage({ endpoints: [window.location.origin + VRoot + '/ExternalMethods.aspx'] }, $a[0].href);
                                window.removeEventListener('message', postToChild, false);
                            }
                        };
                        window.addEventListener('message', postToChild, false);
                        return false;
                    }
                    else return true;
                }
                return false;
            }
        });

        $j("li.has_sub > a.open").click();
        $j("li.has_sub > a.open").removeClass('open');

        animationDelay = 350;

        $j(".sidebar-dropdown a").on('click', function (e) {
            e.preventDefault();
            if (!$j(this).hasClass("open")) {
                // hide any open menus and remove all other classes
                $j(".sidebar #nav").slideUp(350);
                $j(".sidebar-dropdown a").removeClass("open");
                $j(this).find("i").toggleClass("arrow-rotate", false);

                // open our new menu and add the open class
                $j(".sidebar #nav").slideDown(350);
                $j(this).find("i").toggleClass("arrow-rotate", true);
                $j(this).addClass("open");
            }

            else if ($j(this).hasClass("open")) {
                $j(this).removeClass("open");
                $j(".sidebar #nav").slideUp(350);
                $j(this).find("i").toggleClass("arrow-rotate", false);
            }
        });

        $j(window).on('beforeunload', function () {
            var expandedNodes = [];
            $j('#Navigation a.subdrop').each(function () {
                expandedNodes.push($j(this).attr('data-fid'));
            });

            document.cookie = 'NavState=' + JSON.stringify(expandedNodes); + "; path=/";
        });

        var TPOMode = $j(<%= AspxTools.JsGetElementById(TPOMode) %>);
        $j(".PortalModeContainer").append(TPOMode);
        if (TPOMode.attr("readonly") == "true") {
            TPOMode.click(function () { TPOMode.blur(); });
        }
        //create event for TPOMode after appending
        $j(".sidebar #nav select").change(function (e) {
            __doPostBack("ModeChange", $j(this).val());
        });

        if (typeof (CoBorrowerPointImportHelper) === "object") {
            CoBorrowerPointImportHelper.init();
        }

        $j('#ImportCancel').click(function () {
            ShowImportPanel(false);
            hideLoans(false);
        });

        if (!<%=AspxTools.JsBool(HasAccess) %>)
        {
            $j("#Navigation, #SpanNotFrame, #TPOMode").css("display", "none");
        }
        TPOStyleUnification.Components();
        //hide body until it is loaded completely.
        $j("body").removeClass("hidden");
    });
</script>

<form class="form-pipeline" method="post" autocomplete="off" runat="server">
    <header class="app-header navbar">
        <div class="container-fluid">
            <table id="AdminTable" class="loan-info-header" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td class="FieldLabel info-header">
                        <span class="info"><ml:EncodedLiteral id="m_welcomeLabel" runat="server" EnableViewState="False"/></span>
                        <span class="log-out"><a id="LogoutButton"><i class="material-icons logout-icon">&#xE879;</i><span class="text">Log Off</span></a></span>
                    </td>
                </tr>
                <tr><td>
                    <span runat="server" id="AccessDeniedMessage" visible="false"></span>
                </td></tr>
            </table>
        </div>
    </header>

    <div class="app-body">
        <div class="wrap-sidebar no-top-padding navigation-container">
            <div id="Navigation" class="sidebar sidebar-arrow sidebar-nav sidebar-wide">
                <uc:Nav runat="server" ID="Nav"/>
            </div>
            <a class="collapse-arrow"><i class="material-icons collapsing">&#xE5CB;</i></a>
        </div>

        <main class="main">
            <div class="container-fluid padding-container-right-none full-iframe-no-padding">
                <div class="main-slot padding-right-32" ng-bootstrap="PipelineLoan" pml-pipeline></div>
            </div>
        </main>
    </div>

    <asp:DropDownList ID="TPOMode" runat="server"/>
</form>
</body>
</html>
