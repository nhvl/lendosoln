﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;

namespace PriceMyLoan.main
{
    public partial class LockRateService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private Guid LoanID
        {
            get
            {
                Guid loanId = GetGuid("loanid", Guid.Empty);

                if (loanId == Guid.Empty)
                    throw new CBaseException(ErrorMessages.Generic, "No LoanId provided.");
                return loanId;
            }
        }

        private E_AutoRateLockAction Mode
        {
            get
            {
                return (E_AutoRateLockAction)GetInt("mode");
            }
        }

        private string RateLockDays
        {
            get
            {
                return GetString("lockdays", "");

            }
        }

        private AutoRateLock GetAutoRateLock()
        {

            if (RateLockDays == string.Empty)
            {
                return AutoRateLock.Create(LoanID, Mode);
            }
            else
            {
                return AutoRateLock.Create(LoanID, Mode, RateLockDays);
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CreatePriceRequest":
                    CreatePriceRequest();
                    break;
                case "GetResultIfReady":
                    GetResultIfReady();
                    break;
            }
        }

        private void CreatePriceRequest()
        {
            AutoRateLock locker = GetAutoRateLock();

            string requestId = string.Empty;
            try
            {
                requestId = locker.CreatePricingRequest();
            }
            catch (AutoRateLock.NonCriticalAutoRateLockException exc)
            {
                requestId = string.Empty;
                SetResult("displayerror", exc.UserMessage);
            }
            catch (CLoanTemplateNotFoundException exc)
            {
                requestId = string.Empty;
                SetResult("displayerror", exc.UserMessage);
            }

            if (string.IsNullOrEmpty(requestId) == false)
            {
                SetResult("requestid", requestId);
            }
        }

        private void GetResultIfReady()
        {
            AutoRateLock locker = GetAutoRateLock();

            string requestId = GetString("requestid");
            string lockAction = GetString("lockAction", "");
            string lockArguments = GetString("lockArguments", "");

            bool isReady = false;
            try
            {
                isReady = locker.IsResultReady(requestId);
            }
            catch (AutoRateLock.NonCriticalAutoRateLockException exc)
            {
                isReady = false;
                SetResult("displayerror", exc.UserMessage);
            }


            if (isReady)
            {
                SetResult("isdone", true);

                if (lockAction == string.Empty)
                {
                    // We are not doing any action with this result.
                    // Only return the table result to UI.
                    List<string[]> resultRows = locker.ResultTable;
                    SetResult("TableJSON", ObsoleteSerializationHelper.JavascriptJsonSerialize(resultRows));
                }
                else
                {
                    // We got a result.  Now modify the loan file and notify UI.
                    AutoRateLock.AutoRateLockResult result = null;

                    try
                    {
                        result = locker.ExecuteAction(lockAction, lockArguments);
                    }
                    catch (AutoRateLock.NonCriticalAutoRateLockException exc)
                    {
                        SetResult("displayerror", exc.UserMessage);
                    }

                    if (result != null)
                    {
                        SetResult("newexpiration", result.ExpirationDate);
                        SetResult("newrate", result.Rate);
                        SetResult("newprice", result.Price);
                        SetResult("totalevents", result.TotalEvents);
                    }
                }
            }
            else
            {
                SetResult("isdone", false);
            }
        }

    }
}
