using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using DataAccess;
using LendersOffice.Common;
using PriceMyLoan.DataAccess;
using LendersOffice.Reminders;
using PriceMyLoan.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Resource;

namespace PriceMyLoan.main
{
	public partial class LoanSummary : LendersOffice.Common.BaseXsltPage
	{
		private string m_pmlLenderSiteId;

		private Guid BrokerID 
		{
			get { return ((PriceMyLoan.Security.PriceMyLoanPrincipal) Page.User).BrokerId; }
		}
		protected override XsltArgumentList XsltParams 
		{
			get 
			{ 
				XsltArgumentList args = new XsltArgumentList();
				args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
				args.AddParam("VirtualRoot", "", Tools.VRoot);
				// 10/19/06 mf - OPM 7610.  We want to mask SSN in PML
				if ( ! PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML )
					args.AddParam("MaskSsn","", "True");
				return args;

			}
		}

        protected override string XsltFileLocation 
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.PmlLoanViewXslt, this.BrokerID);  ; }
        }
		protected override void InitXslt() 
		{

			SqlParameter[] parameters = { new SqlParameter("@BrokerID", BrokerID) };
            Guid brokerPmlSiteId = Guid.Empty;
			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "RetrieveLenderSiteIDByBrokerID", parameters)) 
			{
				if (reader.Read()) 
				{
					brokerPmlSiteId = (Guid) reader["BrokerPmlSiteID"];
				}
			}
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, LoanID);
		}
        protected override void GenerateXmlData(XmlWriter writer) 
        {
            try
            {
                LendersOfficeApp.newlos.Underwriting.PmlLoanSummaryXmlData.Generate(writer, LoanID, false, false);
            }
            catch (PageDataLoadDenied)
            {
                writer.WriteElementString("NoPermission", null);
            }
        }
        protected Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }


	}
}
