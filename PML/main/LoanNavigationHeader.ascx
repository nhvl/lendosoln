﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoanNavigationHeader.ascx.cs" Inherits="PriceMyLoan.main.LoanNavigationHeader" %>
<div id="LoanHeaderPopup">
    <div id="LoanHeaderPopupText">&nbsp;</div>
</div>

<% if (!ManualPlaceElements) { %>
<div ng-bootstrap="LoanHeader" loan-info-bar></div>
<div ng-bootstrap="LoanHeader" loan-left-navigation></div>
<% } %>
