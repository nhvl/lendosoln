<%@ Register TagPrefix="uc1" TagName="ModelessDlg" Src="../common/ModalDlg/ModelessDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>

<%@ Page Language="c#" CodeBehind="QualifiedLoanProgramPrintMenu.aspx.cs" AutoEventWireup="false"
    Inherits="PriceMyLoan.UI.Main.QualifiedLoanProgramPrintMenu" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>QualifiedLoanProgramPrintMenu</title>
</head>
<body onunload="f_onunload();">

    <script language="javascript">
    <!--
        var _popupWindow = null;

        jQuery(function($) {
            $('#btnPrint').on('click', f_printMe);
            $('#btnEmail').on('click', f_email);
            $('#btnClose').on('click', close);

            <%if(  RequestHelper.GetBool("detailCert") ) {%>
            $('#btnNormalCert').on('click', f_showNormalCert);
            <%}%>

            window.scroll(0, 0);
            
        });

        function f_printMe() {
            try {
                parent.frames['frmBody'].printMe();
            } catch (e) {
                logJSException(e, 'attempted to run f_printMe, no go.'); // may want to rethrow e
            }
        }
        function f_email() {
            window.parent.openEmail();
        }
        function f_disableEmailButton() {
            document.getElementById("btnEmail").disabled = true;
        }
        function f_disablePrintButton() {
            document.getElementById("btnPrint").disabled = true;
        }
        function f_onunload() {
            if (null != _popupWindow && !_popupWindow.closed) _popupWindow.close();
        }
        function f_onInvalidRateOption() {
            var btnClose = document.getElementById('btnClose');
            btnClose.value = "Back";
            btnClose.onclick = f_gotoStep3;
        }
        function f_gotoStep3() {
            if (typeof (window.parent.parent.opener.f_goToStep3) != 'undefined')
                window.parent.parent.opener.f_goToStep3();
            self.close();
        }
        function close() {
            window.parent.close();
        }
        function f_showNormalCert() {
            window.parent.showNormalCert();
        }
    //-->
    </script>

    <form runat="server">
    <asp:Button runat="server"  UseSubmitBehavior="false" ID="BackButton" CssClass="ButtonStyle" Text="Back" Visible="false" OnClientClick="javascript:history.go(-2)" />
    <input type="button" id="btnPrint" value="Print ..." class="ButtonStyle"
        nohighlight>&nbsp;&nbsp;
    <input type="button" id="btnEmail" value="Email ..." class="ButtonStyle"
        nohighlight>&nbsp;&nbsp;
    <input type="button" id="btnClose" value="Close" class="ButtonStyle"
        nohighlight>&nbsp;&nbsp;

    <%if(  RequestHelper.GetBool("detailCert") ) {%>
    <input type="button" id="btnNormalCert" value="Show Normal Certificate" class="ButtonStyle"
        nohighlight>&nbsp;&nbsp;
    <%}%>

    <label style="font-family: sans-serif; color: Red; font-weight: bold;">
        *** NOTE: This is NOT a valid PRE-APPROVAL CERTIFICATE.</label>
    <uc1:ModelessDlg ID="ModelessDlg1" runat="server"></uc1:ModelessDlg>
    </form>
</body>
</html>
