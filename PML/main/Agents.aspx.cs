using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using DataAccess;
using LendersOffice.Constants;
using PriceMyLoan.Common;
using PriceMyLoan.DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;

namespace PriceMyLoan.UI.Main
{

	public partial class AgentsPage : PriceMyLoan.UI.BasePage
	{

        protected Agents m_agents;

        private bool m_hasError = false;

        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }
        protected bool IsNewLoan
        {
            get
            {
                string isnew = RequestHelper.GetSafeQueryString("isnew");
                if (!string.IsNullOrEmpty(isnew) && isnew.ToLower() == "t")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected bool m_allowGoToPipeline 
        {
            get { return PriceMyLoanUser.IsAllowGoToPipeline && !IsEncompassIntegration; }
        }

        protected string Source
        {
            get
            {
                return RequestHelper.GetSafeQueryString("source");
            }
        }

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new WorkflowOperation[] {
                WorkflowOperations.ReadLoan,
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3,
                WorkflowOperations.UseOldSecurityModelForPml
            };
        }

        private bool OldEngineRedirectToDenial()
        {
            if (this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                return false; // 7/28/2006 dd - Skip permission checking if mode is rerun.

            bool canWrite = CurrentUserCanPerform(WorkflowOperations.WriteLoan);

            if (!canWrite)
            {
                // OPM 5068 Cord -- Take the user to the Access Denial page.
                return true;
            }
            return false;
        }

        private bool NewEgineRedirectToDenial()
        {
            if (false == CurrentUserCanPerform(WorkflowOperations.ReadLoan))
            {
                return true;
            }

            if (false == CurrentUserCanPerformAny(
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3))
            {
                return true;
            }

            return false;
        }

        public void CheckForPermission()
        {
            bool newRedirectToDenial = NewEgineRedirectToDenial();
            bool oldRedirectToDenial = OldEngineRedirectToDenial();

            bool redirectToDenial = CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ? oldRedirectToDenial : newRedirectToDenial;

            if (redirectToDenial)
            {
                PriceMyLoanRequestHelper.AccessDenial(LoanID);
            }
        }

        private void DisplayLoanSummary()
        {
            CPageData dataloan = new CPmlLoanSummaryData(LoanID);
            dataloan.InitLoad();

            sLNm.Text = dataloan.sLNm;
            aBNm.Text = dataloan.GetAppData(0).aBNm;
            sLAmtCalc.Text = dataloan.sLAmtCalc_rep.Replace(".00", "");
            // 10/8/2004 dd - Temporary Hack to remove .000;
            sLtvR.Text = dataloan.sLtvR_rep.Replace(".000", "");
            sCltvR.Text = dataloan.sCltvR_rep.Replace(".000", "");
            sCreditScoreType1.Text = dataloan.sCreditScoreType1_rep;
            sCreditScoreType2.Text = dataloan.sCreditScoreType2_rep;
        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        
		protected void PageLoad(object sender, System.EventArgs e)
		{
            this.GoToPipelineLink.Visible = !this.IsEncompassIntegration && this.m_allowGoToPipeline;
            this.LogOutLink.Visible = !this.IsEncompassIntegration;

            //if (PriceMyLoanConstants.IsEmbeddedPML)
            //{
            //    RegisterJsScript("ModelessDlg.js");
            //}
            CheckForPermission();

         //   m_btnStart.Visible = m_allowGoToPipeline;
//            m_btnLogOff.Visible = !PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML;

            if (!Page.IsPostBack) 
            {
                TransformData();
                DisplayLoanSummary();
                m_agents.LoadData();
            }
		}

        private void PagePreRender(object sender, System.EventArgs e)
        {
            if (m_hasError == false)
            {
                // 10/18/2010 dd - Only display warning if there is no error.
                tbodyWarning.Visible = m_displayWarning;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);
        }
		#endregion

        protected void m_btnNext_Click(object sender, System.EventArgs e)
        {
            if (!IsReadOnly) 
            {
                try
                {
                    m_agents.SaveData();
                }
                catch (VersionMismatchException exc)
                {
                    DisplayError(exc.UserMessage);
                    m_hasError = true;
                }
                catch (LoanFieldWritePermissionDenied ex)
                {
                    m_errorMessage.Text = ex.UserMessage;
                    m_hasError = true;
                    return;
                }
            }

            if (m_hasError == false)
            {
                string url = string.Format("main.aspx?loanid={0}&source=" + Source, LoanID);

                bool isNewPmlUIEnabled = false;
                BrokerDB broker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
                if (broker.IsNewPmlUIEnabled)
                {
                    isNewPmlUIEnabled = true;
                }
                else
                {
                    EmployeeDB employee = EmployeeDB.RetrieveById(PriceMyLoanUser.BrokerId, PriceMyLoanUser.EmployeeId);
                    if (employee.IsNewPmlUIEnabled)
                    {
                        isNewPmlUIEnabled = true;
                    }
                }

                if (isNewPmlUIEnabled)
                {
                    url = string.Format("../webapp/pml.aspx?loanid={0}&source={1}", LoanID, "PML");
                    if (IsNewLoan)
                    {
                        url += "&isnew=t";
                    }
                }

                Response.Redirect(url);
            }
        }

        private void DisplayError(string errorMessage)
        {
            m_errorMessage.Text = "ERROR: " + errorMessage;
            tbodyError.Visible = true;
            tbodyMain.Visible = false;
            tbodyWarning.Visible = false;
        }
	}
}
