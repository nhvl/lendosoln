/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Conversions;
using LendersOffice.Common;

namespace PriceMyLoan.main
{
	public partial class FannieExport : MinimalPage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
			ExportFannieMae32();
		}
        private void ExportFannieMae32() 
        {
            FannieMae32Exporter exporter = new FannieMae32Exporter(LoanID);
            Response.Clear();
            Response.ContentType = "application/text";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + exporter.OutputFileName);

            byte[] buffer = exporter.Export();

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.End();
        }

        protected Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
