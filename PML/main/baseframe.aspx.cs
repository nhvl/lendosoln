using System;
using System.Linq;
using LendersOffice.Common;

namespace PriceMyLoan.main
{
	public partial class baseframe : PriceMyLoan.UI.BasePage
	{
        protected string m_url = "";

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
             return E_XUAComaptibleValue.Edge;
        }

        private void PageInit(object sender, EventArgs args) 
        {
            m_url = "pipeline.aspx?" +
                string.Join("&",
                    Request.QueryString.Keys.Cast<string>()
                        .Where(key => key != null && key.ToLower() != "authid")
                        .Select(key => $"{key}={Server.UrlEncode(RequestHelper.GetSafeQueryString(key))}")
                );
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new EventHandler(this.PageInit);
		}
		#endregion

	}

}
