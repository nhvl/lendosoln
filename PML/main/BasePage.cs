namespace PriceMyLoan.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using ConfigSystem;
    using ConfigSystem.Engine;
    using global::DataAccess;
    using global::DataAccess.LoanValidation;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.PmlSnapshot;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using PriceMyLoan.DataAccess;
    using PriceMyLoan.Security;
    using System.Web.UI.HtmlControls;

    public class BasePage : LendersOffice.Common.BaseServicePage
    {
        private enum StyleContentType
        {
            Master = 0,
            Navigation = 1,
            Pricing = 2
        }

        protected override bool EnableIEOnlyCheck
        {
            get
            {
                return false;
            }
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }


        /// <summary>
        /// Returns null if the principal does not exist.
        /// </summary>
        public PriceMyLoanPrincipal PriceMyLoanUser
        {
            get
            {
                if (null == Context || null == Context.User)
                {
                    return null;
                }
                if (Context.User is PriceMyLoanPrincipal)
                {
                    return (PriceMyLoanPrincipal)Context.User;
                }
                if (Context.User is System.Security.Principal.GenericPrincipal)
                {
                    return null;
                }

                Tools.LogBug("Unexpected principal type: " + Context.User.GetType().ToString());
                return null;

            }
        }

        public virtual PriceMyLoanPrincipal CustomPrincipal
        {
            get
            {
                return this.PriceMyLoanUser;
            }
        }

        /// <summary>
        /// Gets a value indicating whether CSS and JS files
        /// material design files are included on page initialization.
        /// </summary>
        /// <remarks>
        /// This property is intended to maintain legacy styling on certain
        /// pages such as the pricing page (pml.aspx) and can be removed once
        /// pages overriding this property have been updated to use material design.
        /// </remarks>
        protected virtual bool IncludeMaterialDesignFiles
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether the page will include style content
        /// that is specific to a user, i.e. the selected theme for the user's OC
        /// and lender.
        /// </summary>
        /// <remarks>
        /// Certain pages will not have a reference to a user principal, such as the 
        /// login page. For those pages, we will use the lender's default styling 
        /// instead of pulling data from the principal.
        /// </remarks>
        protected virtual bool IncludePrincipalSpecificStyling => true;

        #region New Executing Engine  -- PML Checks
        private Dictionary<WorkflowOperation, bool> m_OperationChecks;
        private Dictionary<WorkflowOperation, ExecutionEngineDebugMessage> m_Reasons;

        /// <summary>
        /// Children can override this if they want to include more than the basic  read/write loan permission
        /// The set returned by this call will be union with read/write/UseOldSecurityModelForPml
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new WorkflowOperation[] { };
        }

        public bool CurrentUserCanPerformAny(params WorkflowOperation[] operations)
        {
            foreach (WorkflowOperation operation in operations)
            {
                if (true == CurrentUserCanPerform(operation))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns whether the user can perform an operation. If for some reason we could not determine if the user can run the operations
        /// this will return FALSE.
        /// </summary>
        /// <param name="operation">Name of operation (Use ConstApp.OPERATION_*)</param>
        /// <returns>Whether the operation is available or not.</returns>
        public bool CurrentUserCanPerform(WorkflowOperation operation)
        {
            if (m_OperationChecks == null)
            {
                LoadOperationChecks();
            }

            if (!m_OperationChecks.ContainsKey(operation))
            {
                throw new CBaseException(ErrorMessages.Generic, "Trying to check for an operation that was not part of GetAdditionalOperationsThatNeedChecks");
            }

            return m_OperationChecks[operation];
        }

        public virtual IEnumerable<WorkflowOperation> GetReasonsForOp()
        {
            return new WorkflowOperation[] { };
        }

        /// <summary>
        /// Gathers list of operations we are interested in and populates a dictionary with the results.
        /// Under two cases the results will always be false if LoanId is not present or the principal is null
        /// except for OPERATION_USE_OLD_SECURITY_MODEL_PML since we want to default to this.
        /// </summary>
        private void LoadOperationChecks()
        {
            var operations = new List<WorkflowOperation>(8)
            {
                WorkflowOperations.UseOldSecurityModelForPml,
                WorkflowOperations.WriteLoan,
                WorkflowOperations.ReadLoan,
                WorkflowOperations.WriteField,
                WorkflowOperations.ProtectField
            };

            if (this.PriceMyLoanUser?.EnableNewTpoLoanNavigation ?? false)
            {
                // Pages that use the new loan navigation header will display
                // a link to the pricing page depending on whether the user has
                // pricing permission.
                operations.AddRange(new[]
                {
                    WorkflowOperations.RunPmlForAllLoans,
                    WorkflowOperations.RunPmlForRegisteredLoans,
                    WorkflowOperations.RunPmlToStep3
                });
            }

            //possibly unneeded null check.
            IEnumerable<WorkflowOperation> childOperations = GetAdditionalOperationsThatNeedChecks() ?? new WorkflowOperation[] { };
            IEnumerable<WorkflowOperation> fullOperationList = operations.Union(childOperations);
            IEnumerable<WorkflowOperation> reasonOperationList = GetReasonsForOp();

            m_OperationChecks = new Dictionary<WorkflowOperation, bool>();
            m_Reasons = new Dictionary<WorkflowOperation, ExecutionEngineDebugMessage>();

            //if we dont have a principal or there is no loan id return false.
            if (PriceMyLoanUser == null || LoanID == Guid.Empty)
            {
                foreach (WorkflowOperation operation in fullOperationList)
                {
                    m_OperationChecks.Add(operation, false);
                }

                foreach (WorkflowOperation operation in reasonOperationList)
                {
                    m_Reasons.Add(operation, null);
                }

                return;
            }

            LoanValueEvaluator evaluator = new LoanValueEvaluator(PriceMyLoanUser.ConnectionInfo, PriceMyLoanUser.BrokerId, LoanID, fullOperationList.ToArray());
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(PriceMyLoanUser));
            foreach (WorkflowOperation operation in fullOperationList)
            {
                m_OperationChecks.Add(operation, LendingQBExecutingEngine.CanPerform(operation, evaluator));

            }

            foreach (WorkflowOperation operation in reasonOperationList)
            {
                if (m_OperationChecks.ContainsKey(operation) == false)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Bug, requesting messages for " + operation.ToString() + " but operation not in GetAdditionalOperationsThatNeedChecks" );
                }

                m_Reasons.Add(operation, LendingQBExecutingEngine.GetExecutionEngineDebugMessage(operation, evaluator));
            }

        }

        public IEnumerable<string> GetReasonsUserCannotPerformOperation(params WorkflowOperation[] operations)
        {
            IEnumerable<ExecutionEngineDebugMessage> debugMessages = GetOperationDebugMessages(operations);
            IEnumerable<string> reasons = debugMessages.Where(d => d != null && d.FailedConditionType == ConditionType.Restraint).SelectMany(d => d.UserFriendlyMessages);
            if (!reasons.Any())
            {
                reasons = debugMessages.Where(d => d != null && d.FailedConditionType == ConditionType.Privilege).SelectMany(d => d.UserFriendlyMessages);
            }

            return reasons;
        }

        public ConditionType GetOperationFailedConditionType(params WorkflowOperation[] operations)
        {
            IEnumerable<ExecutionEngineDebugMessage> debugMessages = GetOperationDebugMessages(operations);

            if (debugMessages.Any(d => d != null && d.FailedConditionType == ConditionType.Restraint))
            {
                return ConditionType.Restraint;
            }

            if (debugMessages.Any(d => d != null && d.FailedConditionType == ConditionType.Privilege))
            {
                return ConditionType.Privilege;
            }

            return ConditionType.Invalid;
        }

        private IEnumerable<ExecutionEngineDebugMessage> GetOperationDebugMessages(params WorkflowOperation[] operations)
        {
            if (m_OperationChecks == null || m_Reasons == null)
            {
                LoadOperationChecks();
            }

            List<ExecutionEngineDebugMessage> debugMessages = new List<ExecutionEngineDebugMessage>();

            foreach (WorkflowOperation operation in operations)
            {
                if (m_OperationChecks.ContainsKey(operation) == false || m_Reasons.ContainsKey(operation) == false)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Bug, requesting debug data for " + operation.ToString() + " but operation not in GetAdditionalOperationsThatNeedChecks or its not in GetOpsReasons");
                }

                debugMessages.Add(m_Reasons[operation]);
            }

            return debugMessages;
        }

        public bool CurrentUserCanPerformPricing()
        {
            if (this.CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml))
            {
                if (this.lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                    return true; // 7/28/2006 dd - Skip permission checking if mode is rerun.

                return this.CurrentUserCanPerform(WorkflowOperations.WriteLoan);
            }
            else
            {
                if (!this.CurrentUserCanPerform(WorkflowOperations.ReadLoan))
                {
                    return false;
                }

                return this.CurrentUserCanPerformAny(
                    WorkflowOperations.RunPmlForAllLoans,
                    WorkflowOperations.RunPmlForRegisteredLoans,
                    WorkflowOperations.RunPmlToStep3);
            }
        }

        public void SetButtonDisplayFromWorkflow(
            System.Web.UI.HtmlControls.HtmlButton button,
            HtmlGenericControl div,
            params WorkflowOperation[] operations)
        {
            if (!operations.Any())
            {
                return;
            }

            // This configuration and engine will be cached after the first retrieval. We need to
            // check for null since the broker may not have a released workflow configuration.
            var brokerWorkflowRepo = ConfigHandler.GetRepository(this.PriceMyLoanUser.BrokerId);
            var executingEngine = ExecutingEngine.GetEngineByBrokerId(brokerWorkflowRepo, this.PriceMyLoanUser.BrokerId);

            if (executingEngine == null || operations.All(operation => !executingEngine.HasOperation(operation)))
            {
                button.Visible = false;
            }
            else if (!this.CurrentUserCanPerformAny(operations))
            {
                button.Disabled = true;

                var tooltipDiv = new HtmlGenericControl("div");
                tooltipDiv.Attributes.Add("title", "You do not currently have permission to perform this operation on this file.");
                tooltipDiv.Attributes.Add("class", "disabled-tooltip-container");
                tooltipDiv.Attributes.Add("data-toggle", "tooltip");
                tooltipDiv.Attributes.Add("data-placement", "bottom");                
                tooltipDiv.Controls.Add(button);

                div.Controls.Add(tooltipDiv);
                div.Attributes.Add("class", div.Attributes["class"] += " disabled-div");
            }
        }

        #endregion

        #region Special area to determine the pricing running mode
        private bool x_isPriceModeInitialize = false;
        private E_LpeRunModeT x_lpeRunModeT = E_LpeRunModeT.NotAllowed;
        public E_LpeRunModeT lpeRunModeT
        {
            get
            {
                if (!x_isPriceModeInitialize)
                {
                    Guid sLId = LendersOffice.Common.RequestHelper.GetGuid("loanid", Guid.Empty);
                    if (sLId == Guid.Empty)
                    {
                        x_lpeRunModeT = E_LpeRunModeT.NotAllowed;
                    }
                    else
                    {
                        try
                        {
                            CPageData dataLoan = new CCheckLpeRunModeData(sLId);
                            dataLoan.AllowLoadWhileQP2Sandboxed = true;
                            dataLoan.InitLoad();

                            x_lpeRunModeT = dataLoan.lpeRunModeT;
                        }
                        catch (PageDataLoadDenied)
                        {
                            x_lpeRunModeT = E_LpeRunModeT.NotAllowed;
                        }
                    }
                    x_isPriceModeInitialize = true;

                }

                return x_lpeRunModeT;
            }
        }
        #endregion
        public int sFileVersion
        {
            get;
            set;
        }

        public virtual Guid LoanID
        {
            get
            {
                try
                {
                    if (Context.Items["LoanID"] != null)
                        return (Guid) Context.Items["LoanID"];

                    string str = Request["LoanID"];
                    if (string.IsNullOrEmpty(str))
                    {
                        return Guid.Empty;
                    }
                    else
                    {
                        return new Guid(Request["LoanID"]);  // 08/25/04-Binh-Took out ".Form" so that it won't break on a GET method
                    }
                }
                catch (FormatException)
                {
                    return Guid.Empty;
                }
            }
        }

        #region Special area to determine if warning about modified fields.
        private bool x_isDisplayWarningInitialize = false;
        private bool x_isDisplayWarning = false;
        private string x_warningMsg = "";

        protected string m_warningMsg
        {
            get { return x_warningMsg; }
        }

        protected bool m_displayWarning
        {
            get
            {
                if (!x_isDisplayWarningInitialize)
                {
                    CPageData dataLoan = new CListModifyPmlFieldsData(LoanID);
                    dataLoan.InitLoad();

                    //ArrayList list = dataLoan.ListModifyPmlData();
                    PmlSnapshotDiffResult result = dataLoan.GetModifyPmlData();
                    x_isDisplayWarning = result.Count != 0;

                    if (result.Count > 0)
                    {
                        string user = result.OriginalUserName;
                        DateTime ts = result.OriginalTimestamp;
                        if (string.IsNullOrEmpty(user))
                        {
                            x_warningMsg = string.Format("CAUTION: Your data has been updated since the last time pricing was run. Please check each field for accuracy.");
                        }
                        else
                        {
                            if (ts != DateTime.MinValue)
                            {
                                x_warningMsg = string.Format("CAUTION: Your data has been updated since pricing was run by {0} on {1}. Please check each field for accuracy.", user, Tools.GetDateTimeDescription(ts));

                            }
                            else
                            {
                                x_warningMsg = string.Format("CAUTION: Your data has been updated since the last time pricing was run by {0}. Please check each field for accuracy.", user);
                            }
                        }
                    }
                }
                return x_isDisplayWarning;
            }

        }
        #endregion

        protected virtual bool RunValidationOverride
        {
            get { return true; }
        }

        protected virtual bool IsCrossSiteForgeryCheck
        {
            get { return true; }
        }
        protected int TransformData()
        {
            return TransformData(false);
        }
        protected int TransformData(bool initNewLoanForPml)
        {
            var sFileVersion = Tools.TransformData(LoanID, initNewLoanForPml, Page.IsPostBack, this.lpeRunModeT, false);

            // If you are going to change the polarity of this bit, or otherwise change the behavior so
            // that PML 2.0 may call the validation methods prior to running pricing, then make sure to
            // update the code in DistributeUnderwritingEngine.GetPricingResultInXml() to have similar behavior.
            // gf 9/29/16
            if (RunValidationOverride)
            {
                return sFileVersion;
            }

            // 12/4/2006 dd - Validation Loan Data for PML.

            // 12/4/2006 dd - As precaution, only run validation in rerun mode.
            // Since in rerun mode, user will not have ability to edit loan field. Therefore
            // redirect to an error page if loan does not pass validation.

            //10/14/10 with the workflow changes based on above comment it seems that this really should be checking readonly.
            //since we need to redirect if the user cant fix mistakes because the loan is readonly
            //with the workflow changes lperunmode wont determine read only anymore so we need to check the write.
            //granted im not quite sure what the effects will be here though by these changes when using the new engine.
            //I may still need to check readonly instead of write loan.

            bool oldRunValidation = lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct;
            bool newRunValidation = !CurrentUserCanPerform(WorkflowOperations.WriteLoan);

            bool runValidaton = CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ? oldRunValidation : newRunValidation;

            if (runValidaton)
            {
                var dataLoan = new CValidateForRunningPricingData(LoanID);
                dataLoan.InitLoad();

                var resultList = Tools.RunInternalPricingValidation(dataLoan, this.PriceMyLoanUser);
                if (resultList.ErrorCount != 0)
                {
                    Response.Redirect("~/main/ValidationErrorPage.aspx?loanid=" + LoanID);
                }
            }
            return sFileVersion;
        }
        public override string StyleSheet
        {
            get { return VirtualRoot + (this.IncludeMaterialDesignFiles ? "/css/style.css" : "/css/PricingStyle.css"); }
        }
        public virtual bool IsReadOnly
        {
            get
            {
                if (Context == null || Context.Request == null)
                    return false;

                if (CurrentUserCanPerform(WorkflowOperations.WriteField)) return false;

                bool newIsLoanReadOnly = CurrentUserCanPerform(WorkflowOperations.ReadLoan) && !CurrentUserCanPerform(WorkflowOperations.WriteLoan);
                bool oldIsLoanReadOnly = lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct;

                //dont log Embedded PML is readonly mismatches
                if (newIsLoanReadOnly == false && oldIsLoanReadOnly == true && PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML )
                {
                    return CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ? oldIsLoanReadOnly : newIsLoanReadOnly;
                }


                return CurrentUserCanPerform(WorkflowOperations.UseOldSecurityModelForPml) ? oldIsLoanReadOnly : newIsLoanReadOnly;
            }
        }
        public string IsReadOnlyJs
        {
            get
            {
                return IsReadOnly ? "true" : "false";
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            RegisterJsScript("JSErrorHandling.js");
            base.OnPreInit(e);
        }

        protected override E_JqueryVersion GetJQueryVersion()
        {
            return EnableJquery && EnableJqueryMigrate ? E_JqueryVersion._1_11_2 : E_JqueryVersion._1_12_4; 
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (PriceMyLoanConstants.IsEmbeddedPML)
            {
                return;
            }

            if (this.IncludePrincipalSpecificStyling)
            {
                this.AddUserStyleContent(StyleContentType.Navigation);

                if (this.IncludeMaterialDesignFiles)
                {
                    this.AddUserStyleContent(StyleContentType.Master);
                }
                else
                {
                    this.AddUserStyleContent(StyleContentType.Pricing);
                }
            }
            else
            {
                this.AddAnonymousStyleProviderReference();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.RegisterCSS("MasterFiles/MasterFont.css");

            if(!PriceMyLoanConstants.IsEmbeddedPML)
            {
                this.RegisterJsScript("IEPolyfill.js");
                this.RegisterJsScript("app.js");
            }

            if (this.IncludeMaterialDesignFiles)
            {
                this.RegisterJsScript("jquery-ui.js");
                this.RegisterJsScript("LQBPopupV2.js");
                this.RegisterJsScript("bootstrap.min.js");
                this.RegisterJsScript("material-components-web.min.js");

                this.RegisterCSS("webapp/jquery-ui.css");
                this.RegisterCSS("mdc-theme-variables.css");
                this.RegisterCSS("bootstrap.min.css");
                this.RegisterCSS("material-components-web.min.css");
            }
            else
            {
                this.RegisterJsScript("LQBPopup.js");
            }

            EnableJquery = true;

            this.RegisterJsScript("toastr-2.1.2.min.js");
            this.RegisterCSS("toastr-2.1.2.css");
            this.RegisterJsGlobalVariables("isSaveButtonEnabled", Tools.IsSaveButtonEnabled(this.PriceMyLoanUser));

            // 6/17/2010 dd - Perform Cross Site Request Forgery Check for POST request.
            if (IsCrossSiteForgeryCheck)
            {
                if (Request.HttpMethod == "POST")
                {
                    string data = Request.Form[SecretFormKeyName];
                    if (IsValidRandomFormKey(data) == false)
                    {
                        Response.Redirect("~/SessionExpired.aspx");
                        return;
                    }
                }
            }
            string _sFileVersion = Request.Form["sFileVersion"];
            if (string.IsNullOrEmpty(_sFileVersion) == false)
            {
                int v = 0;
                int.TryParse(_sFileVersion, out v);
                sFileVersion = v;
            }
			// OPM 5068 Cord -- Take users to access denial page
			// during PageDataLoadDenied
			try
			{
				InitializeUserControls();
			}
			catch ( PageDataLoadDenied )
			{
                PriceMyLoanRequestHelper.AccessDenial(RequestHelper.GetGuid("loanid", Guid.Empty));
			}

#if DEBUG
			// 01/15/08 - OPM 19604. Add debug script if app is in debug mode.
			ClientScript.RegisterClientScriptBlock(this.GetType(), "pml debug script", LendersOffice.Constants.ConstAppMatthew.PmlLocalDebugScript );
#endif

            //opm 28879 Make sure not to check the denial page for read only because it will trigger the app error page with a data load denied error.
            if ( !(this is PriceMyLoan.UI.Main.AccessDenial))
            {
                if (IsReadOnly)
                {
                    DisableAllValidators();
                }
            }

            this.PreRender += new EventHandler(BasePage_PreRender);
            base.OnInit(e);
        }

        /// <summary>
        /// At some point we should disable the event handlers if this is set to true.<para></para>
        /// Until then, we have to catch ThreadAbortException on redirection. :-(.
        /// </summary>
        public bool IsDoingRedirect
        {
            get;
            set;
        }

        private string SecretFormKeyName
        {
            get { return "__KEY__"; }
        }
        void BasePage_PreRender(object sender, EventArgs e)
        {
            // 6/17/2010 dd - Add form key to all of our PML pages.
            // This is result from Citi regressing test. They want us to have Cross SIte Request Forgery (CSRF) protection build in.
            this.ClientScript.RegisterHiddenField(SecretFormKeyName, BuildRandomFormKey());

            this.ClientScript.RegisterHiddenField("sFileVersion", sFileVersion.ToString());
        }

        private string BuildRandomFormKey()
        {
            string data = Guid.NewGuid() + "|" + Request.Url.AbsolutePath + "|" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            return EncryptionHelper.Encrypt(data);
        }

        /// <summary>
        /// Adds a link to the style content for the page 
        /// based on the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">
        /// The type of the style content.
        /// </param>
        private void AddUserStyleContent(StyleContentType type)
        {
            var href = $"{this.VirtualRoot}/StyleProvider.aspx?f={type}&v={IncludeVersion}";
            this.AddStyleLink(href, type.ToString());
        }

        /// <summary>
        /// Adds a link to the anonymous style content provider.
        /// </summary>
        private void AddAnonymousStyleProviderReference()
        {
            var lenderPmlSiteId = RequestHelper.GetGuid("lenderpmlsiteid", Guid.Empty);
            var href = $"{this.VirtualRoot}/AnonymousStyleProvider.aspx?lenderpmlsiteid={lenderPmlSiteId}&v={IncludeVersion}";
            this.AddStyleLink(href, lenderPmlSiteId.ToString());
        }

        /// <summary>
        /// Adds a style link to the page.
        /// </summary>
        /// <param name="href">
        /// The link to the style content.
        /// </param>
        /// <param name="scriptBlockKey">
        /// The key for the link's script block in the event
        /// that the header is not set to run at the server.
        /// </param>
        private void AddStyleLink(string href, string scriptBlockKey)
        {
            if (this.Header == null)
            {
                var link = $"<link href='{href}' type='text/css' rel='stylesheet' />";
                this.ClientScript.RegisterClientScriptBlock(typeof(BasePage), "StyleProvider" + scriptBlockKey, link);
            }
            else
            {
                var element = new System.Web.UI.HtmlControls.HtmlGenericControl("link");
                element.Attributes.Add("href", href);
                element.Attributes.Add("type", "text/css");
                element.Attributes.Add("rel", "stylesheet");

                this.Header.Controls.Add(element);
            }
        }

        private bool IsValidRandomFormKey(string key)
        {
            string[] parts = EncryptionHelper.Decrypt(key).Split('|');


            if (Request.Url.AbsolutePath.Equals(parts[1], StringComparison.OrdinalIgnoreCase))
            {
                DateTime dt = DateTime.Parse(parts[2]);

                if (dt.AddMinutes(PriceMyLoanConstants.CookieExpireMinutes).CompareTo(DateTime.Now) > 0)
                {
                    return true;
                }
            }

            return false;
        }
        private void InitializeUserControls()
        {
            foreach (Control c in Controls)
            {
                InitializeIndividualUserControl(c);
            }
        }
        private void DisableAllValidators()
        {
            foreach (BaseValidator v in this.Validators)
            {
                v.Enabled = false;
				if (v.Attributes["AlwaysEnable"] == "true")
				{	//opm 11948 fs 07/30/08
					v.Enabled = true;
				}
            }
        }
        private void InitializeIndividualUserControl(Control c)
        {

            TextBox tb = c as TextBox;

            if (null != tb)
            {
                InitializeTextBox(tb);
                return;
            }

            DropDownList ddl = c as DropDownList;
            if (null != ddl)
            {
                InitializeDropDownList(ddl);
                return;
            }

            CheckBox cb = c as CheckBox;
            if (null != cb)
            {
                InitializeCheckBox(cb);
                return;
            }

            MeridianLink.CommonControls.StateDropDownList sddl = c as MeridianLink.CommonControls.StateDropDownList;
            if (null != sddl)
            {
                InitializeStateDropDownList(sddl);
                return;
            }
            System.Web.UI.HtmlControls.HtmlInputCheckBox hicb = c as System.Web.UI.HtmlControls.HtmlInputCheckBox;
            if (null != hicb)
            {
                InitializeHtmlInputCheckBox(hicb);
                return;
            }

            System.Web.UI.WebControls.BaseValidator v = c as System.Web.UI.WebControls.BaseValidator;
            if (null != v)
            {
                InitializeValidatorControl(v);
                return;
            }

            foreach (Control c1 in c.Controls)
            {
                InitializeIndividualUserControl(c1);
            }

        }
        private void InitializeValidatorControl(BaseValidator c)
        {
            c.Enabled = c.Enabled && !IsReadOnly;
            if (c.Attributes["AlwaysEnable"] != null)
            {
                c.Enabled = true;
            }
        }
        private void InitializeTextBox(TextBox c)
        {
            c.EnableViewState = false;
            c.ReadOnly = c.ReadOnly || IsReadOnly;

			if (c.Attributes["AlwaysEnable"] != null)
			{	//opm 11948 fs 07/30/08
				c.ReadOnly = false;
			}
        }
        private void InitializeDropDownList(DropDownList c)
        {
            c.Enabled = c.Enabled && !IsReadOnly;
            if (c.Attributes["AlwaysEnable"] != null)
            {	//opm 25872 fs 04/20/09
                c.Enabled = true;
            }
        }
        private void InitializeCheckBox(CheckBox c)
        {
            c.Enabled = c.Enabled && !IsReadOnly;
            if (c.Attributes["AlwaysEnable"] != null)
            {
                c.Enabled = true;
            }
        }
        private void InitializeStateDropDownList(MeridianLink.CommonControls.StateDropDownList c)
        {
            c.Enabled = c.Enabled && !IsReadOnly;
        }
        private void InitializeHtmlInputCheckBox(System.Web.UI.HtmlControls.HtmlInputCheckBox c)
        {
            c.Disabled = c.Disabled || IsReadOnly;

            if (c.Attributes["AlwaysEnable"] != null)
            {
                c.Disabled = false;
            }
        }



    }

}
