<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GetResultsNewUI.ascx.cs" Inherits="PriceMyLoan.UI.Main.GetResultsNewUI" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc1" TagName="QualifyingBorrower" Src="./QualifyingBorrower.ascx" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import namespace="PriceMyLoan.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<style type="text/css">
a.ineligibleLoanProgramsLink
{
    color:#ff9933;    
}
a.ineligibleLoanProgramsLink:hover
{
    color:blue;    
}
tr.looser {
    display: none;
}

a.showlooser, a.showlooser:active {
    text-decoration: none;
    outline: none;
    border: 0;
}

div.pn {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
</style>
<script language="javascript" type="text/javascript">
<!--
  $j(function()
  {
      if (ML.IsUlad2019) {
          initializeQualifyingBorrower(true, null, true);
          var $trueContainer = $('.true-container');
          $trueContainer.attr('class', $trueContainer.attr('class') + ' wrap');
      }
      else {
          $('.qualifying-borrower-container').remove();
      }

        $j(document).delegate('a.showlooser', 'click', function(){
            var $this = $j(this), rateId = $this.attr('data-rateid');
            
            if (this.innerHTML == '+') {
                this.innerHTML = '-';
                $this.closest('tbody').find('tr[data-rateid="' + rateId + '"]').show();
            }
            else {
                this.innerHTML = '+';
                $this.closest('tbody').find('tr[data-rateid="' + rateId + '"]').hide();
            }
            updateSize();
            return false;
        });
      function f_msg($anchor, x, y) {   
        var message = 'Rate is disqualified because ' + $anchor.attr('data-msg');
        var links = '';
        
        <% if(m_showRateForIneligible) { %>
            var arg1 = $anchor.attr('data-arg1'),
                arg2 = $anchor.attr('data-arg2'),
                arg3 = $anchor.attr('data-arg3'),
                arg4 = $anchor.attr('data-arg4'),
                func = $anchor.attr('data-func'), 
                container = $j('<div>');
            container.append($j('<span>').html('&nbsp;'));
                
                if (arg4 != null && arg4.indexOf('register loan') >= 0 ){ 
                    var isDisabled = arg4.indexOf('register loan?') >= 0;        
                    if (isDisabled){
                        container.append("&nbsp;&nbsp;<a href='#' onclick='return false;' disabled='disabled'>register</a>&nbsp;<a href='#' onclick='return f_SRM();'>?</a>");
                    }else {
                        $j('<a>', {
                            'data-func' : func,
                            'data-arg1' : arg1,
                            'data-arg2' : arg2,
                            'data-arg3' : arg3,
                            'href' : '#'
                        }).addClass('disqual_register').text('register').appendTo(container);
                    }
                }
                if( arg4 != null && arg4.indexOf('lock rate') >= 0 )
                {
                    container.append($j('<span>').html('&nbsp;'));
                    $j('<a>', {
                        'data-func' : func,
                        'data-arg1' : arg1,
                        'data-arg2' : arg2,
                        'data-arg3' : arg3,
                        'href' : '#'
                    }).addClass('disqual_lock').text('request lock').appendTo(container);
                }
                
                if (arg4 != null && arg4.indexOf('go to 2nd lien') >= 0)
                {
                  
                    container.append($j('<span>').html('&nbsp;'));
                    $j('<a>', {
                        'data-func' : func,
                        'data-arg1' : arg1,
                        'data-arg2' : arg2,
                        'data-arg3' : arg3,
                        'href' : '#'
                    }).addClass('disqual_lock').text('go to 2nd lien').appendTo(container);
                }
                
                links = container.html();
        <% } %>
        
        
          var oPanel = document.getElementById('RateWarningDiv');
            oPanel.style.display = 'block';
            oPanel.style.width= '250px';
            oPanel.style.top = y +'px';
            oPanel.style.left = (x+50) + 'px';
            oPanel.innerHTML = '<p>' + message + "</p><br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' />" + links +"</span>";
        return false; 
    }
    
    function f_displayMsg($anchor, x, y, width) {   
        var oPanel = document.getElementById('RateWarningDiv');
            oPanel.style.display = 'block';
            oPanel.style.width= width +'px';
            oPanel.style.top = y +'px';
            oPanel.style.left = (x+50) + 'px';
            oPanel.innerHTML = '<p>' + $anchor.attr('displayMsg') + "</p><br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' /></span>";
        return false; 
    }
    
    function f_displayCosts($anchor, x, y) {
        var closingCostBreakdownTemplate = $j('#closingCostBreakdownTemplate').template();
    
        var closingCostBreakdown = $anchor.attr('closingCostBreakdown');
        closingCostBreakdown = $j.parseJSON(closingCostBreakdown);
        
        var prepaidCharges = $anchor.attr('PrepaidCharges');
        var nonPrepaidCharges = $anchor.attr('NonPrepaidCharges');
        var closingCostFee = $anchor.attr('ClosingCost');
        
        
        var loanProgram = $anchor.attr('LoanProgram');
        var noteRate = $anchor.attr('NoteRate');
        var TRID = $anchor.attr('TRID');
        
        var $container = $j.tmpl(closingCostBreakdownTemplate, {
            'NoteRate': noteRate,
            'LoanProgram': loanProgram,
            'closingCostBreakdown': closingCostBreakdown,
            'PrepaidCharges': prepaidCharges,
            'NonPrepaidCharges': nonPrepaidCharges,
            'ClosingCostFee': closingCostFee,
            'TRID' : TRID
        });
        $container.find('#breakdownTable tr:odd').addClass('ElAltBg'); // stripe the table
        $container.find('#breakdownTable tr:even').addClass('ElBg');
        
        $container.find('#titleDisclaimer').hide();

        var disclaimerHeight = 0;
        $j(closingCostBreakdown).each(function() {
              if ( this.Source && this.Source == "First American Quote" )
              {
                $container.find('#titleDisclaimer').show();
                disclaimerHeight = 200;
                return false; // aka. break
              }
        });
        var rowHeight = 22;
        var gutterHeight = 190 + 5;
        parent.LQBPopup.ShowElement($j('<div></div>').append($container), {
            'popupClasses': 'ClosingCostBreakdown',
            'width': 600,
            'height': closingCostBreakdown.length * rowHeight + gutterHeight + disclaimerHeight // resize the window to match the size of the content
        });
        return false; 
    }

    <% if ( displayCashToCloseAndReservesCalculation ) { %>
    function f_displayCashToClose($anchor, x, y) {
        var cashToCloseTemplate = $j('#CashToCloseTemplate').template();

           var LoanProgram = $anchor.attr('LoanProgram');
           var NoteRate = $anchor.attr('NoteRate');
           var sPurchPrice = $anchor.attr('sPurchPrice');
           var sAltCost = $anchor.attr('sAltCost');
           var sLandCost = $anchor.attr('sLandCost');
           var sRefPdOffAmt1003 = $anchor.attr('sRefPdOffAmt1003');
           var sTotEstPp1003 = $anchor.attr('sTotEstPp1003');
           var sTotEstCcNoDiscnt1003 = $anchor.attr('sTotEstCcNoDiscnt1003');
           var sFfUfmip1003 = $anchor.attr('sFfUfmip1003');
           var sLDiscnt1003 = $anchor.attr('sLDiscnt1003');
           var sTotTransC = $anchor.attr('sTotTransC');
           var sONewFinBal = $anchor.attr('sONewFinBal');
           var sTotCcPbs = $anchor.attr('sTotCcPbs');
           var sOCredit1Desc = $anchor.attr('sOCredit1Desc');
           var sOCredit1Amt = $anchor.attr('sOCredit1Amt');
           var sOCredit2Desc = $anchor.attr('sOCredit2Desc');
           var sOCredit2Amt = $anchor.attr('sOCredit2Amt');
           var sOCredit3Desc = $anchor.attr('sOCredit3Desc');
           var sOCredit3Amt = $anchor.attr('sOCredit3Amt');
           var sOCredit4Desc = $anchor.attr('sOCredit4Desc');
           var sOCredit4Amt = $anchor.attr('sOCredit4Amt');
           var sOCredit5Amt = $anchor.attr('sOCredit5Amt');
           var sONewFinCc = $anchor.attr('sONewFinCc');
           var sONewFinCcInverted = $anchor.attr('sONewFinCcInverted');
           var sTotLiquidAssets = $anchor.attr('sTotLiquidAssets');
           var sLAmtCalc = $anchor.attr('sLAmtCalc');
           var sFfUfmipFinanced = $anchor.attr('sFfUfmipFinanced');
           var sFinalLAmt = $anchor.attr('sFinalLAmt');
           var sTransNetCash = $anchor.attr('sTransNetCash');
           var sIsIncludeProrationsIn1003Details = $anchor.attr('sIsIncludeProrationsIn1003Details') === 'True';
           var sIsIncludeProrationsInTotPp = $anchor.attr('sIsIncludeProrationsInTotPp') === 'True';
           var sTotEstPp = $anchor.attr('sTotEstPp');
           var sTotalBorrowerPaidProrations = $anchor.attr('sTotalBorrowerPaidProrations');
           var sIsIncludeONewFinCcInTotEstCc = $anchor.attr('sIsIncludeONewFinCcInTotEstCc') === 'True';
           var sTotEstCcNoDiscnt = $anchor.attr('sTotEstCcNoDiscnt');

        var model = {
            'LoanProgram': LoanProgram,
            'NoteRate': NoteRate,
            'sPurchPrice': sPurchPrice,
            'sAltCost': sAltCost,
            'sLandCost': sLandCost,
            'sRefPdOffAmt1003': sRefPdOffAmt1003,
            'sTotEstPp1003': sTotEstPp1003,
            'sTotEstCcNoDiscnt1003': sTotEstCcNoDiscnt1003,
            'sFfUfmip1003': sFfUfmip1003,
            'sLDiscnt1003': sLDiscnt1003,
            'sTotTransC': sTotTransC,
            'sONewFinBal': sONewFinBal,
            'sTotCcPbs': sTotCcPbs,
            'sOCredit1Desc': sOCredit1Desc,
            'sOCredit1Amt': sOCredit1Amt,
            'sOCredit2Desc': sOCredit2Desc,
            'sOCredit2Amt': sOCredit2Amt,
            'sOCredit3Desc': sOCredit3Desc,
            'sOCredit3Amt': sOCredit3Amt,
            'sOCredit4Desc': sOCredit4Desc,
            'sOCredit4Amt': sOCredit4Amt,
            'sOCredit5Amt': sOCredit5Amt,
            'sONewFinCc': sONewFinCc,
            'sONewFinCcInverted': sONewFinCcInverted,
            'sTotLiquidAssets': sTotLiquidAssets,
            'sLAmtCalc': sLAmtCalc,
            'sFfUfmipFinanced': sFfUfmipFinanced,
            'sFinalLAmt': sFinalLAmt,
            'sTransNetCash': sTransNetCash,
            'sIsIncludeProrationsIn1003Details': sIsIncludeProrationsIn1003Details,
            'sIsIncludeProrationsInTotPp': sIsIncludeProrationsInTotPp,
            'sTotEstPp': sTotEstPp,
            'sTotalBorrowerPaidProrations': sTotalBorrowerPaidProrations,
            'sIsIncludeONewFinCcInTotEstCc': sIsIncludeONewFinCcInTotEstCc,
            'sTotEstCcNoDiscnt': sTotEstCcNoDiscnt,
            'sAltCostLckd': $anchor.attr('sAltCostLckd'),
            'sLandIfAcquiredSeparately1003': $anchor.attr('sLandIfAcquiredSeparately1003'),
            'sLDiscnt1003Lckd': $anchor.attr('sLDiscnt1003Lckd'),
            'sPurchasePrice1003': $anchor.attr('sPurchasePrice1003'),
            'sRefNotTransMortgageBalancePayoffAmt': $anchor.attr('sRefNotTransMortgageBalancePayoffAmt'),
            'sRefTransMortgageBalancePayoffAmt': $anchor.attr('sRefTransMortgageBalancePayoffAmt'),
            'sSellerCreditsUlad': $anchor.attr('sSellerCreditsUlad'),
            'sTotCcPbsLocked': $anchor.attr('sTotCcPbsLocked'),
            'sTotCreditsUlad': $anchor.attr('sTotCreditsUlad'),
            'sTotEstBorrCostUlad': $anchor.attr('sTotEstBorrCostUlad'),
            'sTotEstCc1003Lckd': $anchor.attr('sTotEstCc1003Lckd'),
            'sTotEstPp1003Lckd': $anchor.attr('sTotEstPp1003Lckd'),
            'sTotMortLAmtUlad': $anchor.attr('sTotMortLAmtUlad'),
            'sTotMortLTotCreditUlad': $anchor.attr('sTotMortLTotCreditUlad'),
            'sTotOtherCreditsUlad': $anchor.attr('sTotOtherCreditsUlad'),
            'sTotTransCUlad': $anchor.attr('sTotTransCUlad'),
            'sTransNetCashUlad': $anchor.attr('sTransNetCashUlad'),
            'sTRIDSellerCredits': $anchor.attr('sTRIDSellerCredits')
        };

        var $trueContainer = $('.true-container');
        if (ML.IsUlad2019) {
            populateQualifyingBorrower(model);
            parent.LQBPopup.ShowElement($trueContainer, {
                'width': 1100,
                'height': 625,
                isCopyHTML: false
            });
        }
        else {
            var $container = $j.tmpl(cashToCloseTemplate, model);

            parent.LQBPopup.ShowElement($j('<div></div>').append($container), {
                'width': 855,
                'height': 325
            });
        }

        return false; 
    }

        function f_displayReserves($anchor, x, y) {
        var reservesTemplate = $j('#ReservesTemplate').template();

           var LoanProgram = $anchor.attr('LoanProgram');
           var NoteRate = $anchor.attr('NoteRate');
           var Reserves = $anchor.attr('Reserves');
           var sTotLiquidAssets = $anchor.attr('sTotLiquidAssets');
           var sTransNetCashNonNegative = $anchor.attr('sTransNetCashNonNegative');
           var sMonthlyPmt = $anchor.attr('sMonthlyPmt');

        var $container = $j.tmpl(reservesTemplate, {
            'LoanProgram': LoanProgram,
            'NoteRate': NoteRate,
            'Reserves': Reserves,
            'sTotLiquidAssets': sTotLiquidAssets,
            'sTransNetCashNonNegative': sTransNetCashNonNegative,
            'sMonthlyPmt': sMonthlyPmt
        });
        
        parent.LQBPopup.ShowElement($j('<div></div>').append($container), {
            'width': 635,
            'height': 140
        });
        return false; 
    }
    
      <% } %>
    $j(document).delegate('a.disqual_lock,a.disqual_register', 'click', function(e){
        var $a = $j(this), 
            funcstr = $a.attr('data-func'),
            arg1 = $a.attr('data-arg1'),
            arg2 = $a.attr('data-arg2'),
            arg3 = $a.attr('data-arg3'),
            isRateLock = $a.hasClass('disqual_lock'); 
            
            if (funcstr == 'f01') {
                return f01(arg1,arg2,arg3, isRateLock);
            }
            else {
                return f_submitRateMerge(arg1, arg2, isRateLock);
            }
        
        
    });
    $j(document).delegate('a.unavailable', 'click', function(e){
        var anchor = $j(this);
        f_msg(anchor, e.pageX, e.pageY);
        return false;
    });
    $j(document).delegate('a.displayMsgLink', 'click', function(e){
        var anchor = $j(this);
        f_displayMsg(anchor, e.pageX, e.pageY, 80);
        return false;
    });
    $j(document).delegate('a.displayWideMsgLink', 'click', function(e){
        var anchor = $j(this);
        f_displayMsg(anchor, e.pageX, e.pageY, 300);
        return false;
    });
    $j(document).delegate('a.displayCostsLink', 'click', function(e){
        var anchor = $j(this);
        var $body = $j('body');
        f_displayCosts(anchor, e.pageX, 20);
        return false;
    });
      <% if (displayCashToCloseAndReservesCalculation) { %>
    $j(document).delegate('a.displayCashToCloseLink', 'click', function(e){
        var anchor = $j(this);
        var $body = $j('body');
        f_displayCashToClose(anchor, e.pageX, 20);
        return false;
    });
    $j(document).delegate('a.displayReservesLink', 'click', function(e){
        var anchor = $j(this);
        var $body = $j('body');
        f_displayReserves(anchor, e.pageX, 20);
        return false;
    });
      <% } %>
    // Init
    $j(document).keydown(f_quickDebug);

    $j('.ineligibleLoanProgramsLink').click(toggleIneligibleLoanPrograms);
    <% if (!m_eligibleLoanProgramsExist){%>         
        $j('.ineligibleLoanProgramsDisplayText').hide();        
    <%} else {%>
        $j('.ineligibleLoanProgramsHideText').hide(); 
        $j('.ineligibleLoanProgramsTD').hide();
    <%} %>
  });
  function toggleIneligibleLoanPrograms()
  {
    $j('.ineligibleLoanProgramsHideText').toggle();
    $j('.ineligibleLoanProgramsDisplayText').toggle();
    $j('.ineligibleLoanProgramsTD').toggle();
    updateSize();
  }
  var g_time0 = (new Date()).getTime();
  var gVerHash = <%= AspxTools.JsArray(m_versionList) %>;
  
  var g_iResultTimeout = <%= AspxTools.JsNumeric(ConstAppDavid.LPE_NumberOfMinutesBeforeMessageExpire) %> * 60000 + 10000; <%-- // Plus extra 10 seconds --%>
  var g_sUpdateMessage = <%= AspxTools.JsString(JsMessages.PMLTimeoutCheckFilter) %>;
  var g_renderResultDuration = <%= AspxTools.JsNumeric(m_renderResultDuration) %>;
  var g_loanID = <%= AspxTools.JsString(LoanID) %>;
  var g_sensitiveData = <%= AspxTools.JsString(m_sensitiveData) %>;  
  var g_confirmTitle = <%= AspxTools.JsString(m_confirmationTitle) %>;
  var g_renderResultModeT = parent.getRenderResultModeT();
  var g_debugDetail = <%= AspxTools.JsString(m_debugDetail) %>;


  function f_getCurrentGData() {
    <% if (m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) { %>
    return gData0;
    <% } else { %>
    return gData1;
    <% } %>
  }
  
  function f_calcTiming() {
    var o = new Object();
    
    var str = document.forms[0]['JsScriptStats'].value.split(':');
    o.numOfProducts = str[1];
    o.pollingCount = str[3];
    o.calcServerCount = <%= AspxTools.JsNumeric(m_calcServerCount) %>; 
    o.batchCreated = <%= AspxTools.JsString(m_batchCreatedTime) %>;
      <%-- // Server Time --%>
    o.s0 = parseFloat(str[0]) / 1000; <%-- Calculate Pricing Time = Server Processing + Wait --%>
    o.s1 = parseFloat(g_renderResultDuration) / 1000;      
    o.s2 = parseFloat(<%= AspxTools.JsNumeric(m_calcServerDuration) %>) / 1000;
    o.s5 = parseFloat(str[4]) / 1000; <%-- Time spend checkin IsResultAvailable --%>
    o.s4 = parseFloat(str[2]) / 1000; <%-- Time spend retrieve loan products to perform pricing --%>
    o.renderResultDebug = <%= AspxTools.JsString(m_renderResultDurationDetail) %>;
     
    o.s3 = o.s0 - o.s2 - o.s4 -o.s5;
      
      <%-- // Client Time --%>
    o.c0 = (g_endRenderPage - g_startRenderPage) / 1000;
    o.c1 = g_iDownloadTime - o.c0; <%-- // Only download time. --%>
    
    o.debugClient0 = (g_time0 - g_startRenderPage) / 1000;
    o.debugClient1 = (g_time1 - g_time0) / 1000;
    o.debugClient2 = (g_time2 - g_time1) / 1000;
    o.debugClient3 = (g_endRenderPage - g_time2) / 1000;
    o.total = o.s0 + o.s1 + g_iDownloadTime;
      
    o.percent_server = (o.s0 + o.s1) / o.total * 100;
    o.percent_client = g_iDownloadTime / o.total * 100;
    
    return o;
  }
  
  function f_qualify2ndLoan(productID, rate, point, payment, rawId, version, blockSubmit, blockSubmitMsg, uniqueChecksum) {
	if( !f_allowClick() ) return false;
    _popupWindow = window.open( gVirtualRoot + "/main/SecondResults.aspx?loanid=" + g_loanID + "&cmd=Run2nd&1stProductID=" + productID + "&1stRate=" + rate + "&1stPoint=" + point + "&1stMPmt=" + payment + "&rawId=" + rawId + "&version=" + version + '&debugResultStartTicks=' + <%= AspxTools.JsNumeric(DateTime.Now.Ticks) %> + '&1stBlockSubmit=' + blockSubmit + '&1stBlockSubmitMsg=' + escape(blockSubmitMsg) + '&1stUniqueChecksum=' + uniqueChecksum, 
    "Result",
    'width=750px, height=550px,  resizable=yes, scrollbars=yes, status=yes, help=no, centerscreen=yes,');
  }
  function f_submitManually() {
    <% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.OtherLoan) { %>
      f_submitManual2nd();
      return false;
    <% } %>
    if (!f_allowClick()) return false;
      <% if (m_RegisterAndRateLockBlockingMessage != string.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
      <%} else if (UnableToSubmitWithoutCredit) { %>
        alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
        return false;
      <% } else { %>
          
    var args = new Object();
    args["loanid"] = g_loanID;
    args["submitmanually"] = "True";
    args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
    
    var queryString = "";      
    for (var o in args) {
      queryString += o + "=" + escape(args[o]) +"&";
    }
    _popupWindow = window.open(gVirtualRoot + "/Main/ConfirmationPage.aspx?" + queryString,  
        g_confirmTitle.replace(' ', '_') ,
        'width=200px, height=200px, centerscreen=yes, resizable=yes, scrollbars=yes, status=yes, help=no');
    return false; 
    <% } %>
  }  
  function f_submitManual2nd() {
    if (!f_allowClick()) return false;
      <% if (m_RegisterAndRateLockBlockingMessage != string.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
      <%} else if (UnableToSubmitWithoutCredit) { %>
        alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
        return false;
      <% } else { %>
      
      var args = new Object();
      args["loanid"] = g_loanID;
      args["manual2nd"] = "True";
      args["Has2ndLoan"] = "True";
      args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;      
      args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
      args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
      args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;
      args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;
      args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
      args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;      
      args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
      args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
      var queryString = "";      
      for (var o in args) {
        queryString += o + "=" + escape(args[o]) +"&";
      }
      _popupWindow = window.open(gVirtualRoot+ "/Main/ConfirmationPage.aspx?" + queryString, g_confirmTitle.replace(' ', '_'),'' );
      return false;        
      <% } %>
  }

    <%-- This function display the certificate. Shorten the function name to save space. printLoanProduct--%>
    function f00(currentIndex) {
      
	    if( !f_allowClick() ) return false;
	    var gData = f_getCurrentGData();
	    var productData = gData[currentIndex][0];
	    var productid = productData[0];
	    
	    
	    var extraParms = "";
	    
	    if( <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %> == 1 )
	    {
	    
	      extraParms = [ "&FirstLienLpId="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>,     
	                     "FirstLienNoteRate=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>, 
	                     "FirstLienPoint="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>, 
                         "FirstLienMPmt=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>].join("&");
        } 
	    
	    var version = gVerHash[productData[2]];
	    var uniqueChecksum = productData[6];
	    
       _popupWindow = window.open(gVirtualRoot+ '/Main/QualifiedLoanProgramPrintFrame.aspx?loanid=' + g_loanID + '&productid=' + productid + '&lienqualifymodet=' + <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %> + '&version=' + version + '&uniquechecksum=' + uniqueChecksum + extraParms,
      'Preview_Certificate', 'width=880px,height=580px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no' );
      return false;
    }

    <%-- This function perform actual submission. Shorten the function name to save space. Old name is submitRateOption --%>
function f01(rawId, hashId, currentIndex, bIsRateLock) {
  
  if( !f_allowClick() ) return false;
  
  <%if(m_HasDuplicate){ %>
        f_displayHasDuplicate();
        return false;
  <%} %>
  
  var bIsRateLock = !!bIsRateLock;
  var productData = null;
  var gData = null;
  if (hashId.match(/_e0|_i0/)) 
      gData = gData0[currentIndex];
  else
      gData = gData1[currentIndex];
      
  var productData = gData[0];
  var productID = productData[0];
  var productName = productData[1];
  var bIsBlockedRateLockSubmission = productData[3] + "";
  var uniqueChecksum = productData[6];
  var rateOptionList = gData[1];
  var rate = '';
  var point = '';
  var apr = '';
  var margin = '';
  var payment = '';
  var bottom = '';
  var teaserRate = '';
  var qrate = '';
  var qualPmt = '';
  var parid = '';
  
  for (var i = 0; i < rateOptionList.length; i++) {
    var rateOption = rateOptionList[i];
    if (rateOption[7] == rawId) {
      rate = rateOption[0];
      point = rateOption[1];
      apr = rateOption[2];
      margin = rateOption[3];
      payment = rateOption[4];
      bottom = rateOption[6];
      teaserRate = rateOption[8];
      qrate = rateOption[25] == '' ? rateOption[0] : rateOption[25];
      qualPmt = rateOption[10];
      parid = rateOption[27];
      break;
    }
  }
  <% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) { %>
    f_qualify2ndLoan(productID, rate, point, payment, rawId, gVerHash[productData[2]], bIsBlockedRateLockSubmission, productData[4], uniqueChecksum );
  <% } else if (m_hasLinkedLoan && false ) {  // Allow user pick first loan, but alert when they try to submit both loans.%> 
    alert (<%= AspxTools.JsString(JsMessages.LpeUnableToSubmitLoanProgram) %>);
    return false;
  <% } else if (m_RegisterAndRateLockBlockingMessage != string.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
  <% } else if (UnableToSubmitWithoutCredit) { %>
    alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
    return false;
  <% } else { %>
  var args = new Object();
  args["loanid"] = g_loanID;
  args["productid"] = productID;
  args["rate"] = rate;
  args["point"] = point;
  args["apr"] = apr;
  args["margin"] = margin;
  args["payment"] = payment;
  args["bottom"] = bottom;
  args["productName"] = productName;
  args["teaserrate"] = teaserRate;
  args["qrate"] = qrate;
  args["qualPmt"] = qualPmt;
  args["Has2ndLoan"] = <%= AspxTools.JsString(m_has2ndLoan.ToString()) %>;
  args["RawId"] = rawId;
  args["Version"] = gVerHash[productData[2]];
  args["uniqueChecksum"] = uniqueChecksum;
  args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;
  args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
  args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
  args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;
  args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;
  args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
  args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;
  args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
  args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
  args["blocksubmit"] = bIsBlockedRateLockSubmission;
  args["blocksubmitmsg"] = productData[4];
  args["requestratelock"] = bIsRateLock ? "t" : "f";  
  args["parId"] = parid;
  var queryString = "";
  for (var o in args) {
    queryString += o + "=" + escape(args[o]) +"&";
  }

    <% if (lpeRunModeT ==  E_LpeRunModeT.OriginalProductOnly_Direct) { %>
      var expiredRateDialog = gVirtualRoot + "/Main/LockUnavailable.aspx";
      var expiredDialogTitle = "Lock Unavailable";
    <% } else { %>
      var expiredRateDialog = gVirtualRoot +"/Main/ConfirmationPage.aspx";
      var expiredDialogTitle = g_confirmTitle;
    <% } %>
    var dialogUrl = bIsBlockedRateLockSubmission == "0" ? gVirtualRoot + "/Main/ConfirmationPage.aspx" : expiredRateDialog ;
    var winTitle = bIsBlockedRateLockSubmission == "0" ? g_confirmTitle : expiredDialogTitle;
    winTitle = winTitle.replace(' ', '_');
  _popupWindow = window.open( dialogUrl+"?" + queryString,winTitle ,'width=500px,height=300px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no');

  return false;
  <% } %>
}

    
function submitSkip2nd() {
  if( !f_allowClick() ) return false;
  
  <%if(m_HasDuplicate){ %>
        f_displayHasDuplicate();
        return false;
  <%} %>
  <% if (m_RegisterAndRateLockBlockingMessage != string.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
  <% } else if (UnableToSubmitWithoutCredit) { %>
    alert( <%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
    return false;
  <% } else { %>

  var args = new Object();
  args["loanid"] = g_loanID;
  args["skip2nd"] = "True";
  args["_1stVersion"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("version")) %>;
  args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;  
  args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
  args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;   
  args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;   
  args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;   
  args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
  args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;  
  args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
  args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
  
  var queryString = "";      
  for (var o in args) {
    queryString += o + "=" + escape(args[o]) +"&";
  }
  _popupWindow = window.open(gVirtualRoot + "/Main/ConfirmationPage.aspx?" + queryString, g_confirmTitle.replace(' ', '_'),'width=200px, height=200px, centerscreen=yes, resizable=yes, scrollbars=yes, status=yes, help=no' );
  return false;      
  <% } %>
}
    
<% if (m_isWaiting) { %>
function f_uc_init() {

  oEventArg = _currentIndex;

  var extraArgs = {
  SecondLienMPmt: <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("SecondLienMPmt")) %>
  };
  

  f_displayWaiting();
  f_Underwriting("0", extraArgs); <%-- //sLienQualifyModeT = E_sLienQUalifyModeT.ThisLoan --%>
}
<% } else  { %>
  function f_uc_init() {
    f_hideNavigation();
    setTimeout('f_displayResultImpl()', 200);
  }
<% } %>
function updateSize() {
parent.document.getElementById('resultsIframe').height = document.body.scrollHeight + 110;
parent.document.getElementById('resultsIframe').width = document.body.scrollWidth;
}
function f_displayResultImpl() {
    f_captureTime();
    f_displayResult();
    if (typeof(f_displayNavigation) != 'undefined')
      f_displayNavigation();
    updateSize();
    setDisclaimerData();
}
function setDisclaimerData() {
    parent.Results.AreDisplayed = true;
    parent.PML.triggerPricingDone();
}
function f_captureTime() {
  var args = new Object();
  args["InitialTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
  g_iStartCapture = (new Date()).getTime();
  var result = gService.main.call("PingTime", args);
  if (!result.error) {
    g_iDownloadTime = parseFloat(result.value["duration"]) / 1000;
  }
  g_iEndCapture = (new Date()).getTime();
  var o = f_calcTiming();
  if (o.total > <%= AspxTools.JsNumeric(ConstStage.LpeTimingEmailThresholdInSeconds) %>)
    f_emailTimingImpl('Automate timing result.');
}
function f_displayResult() {
    var renderResultStart = (new Date()).getTime();
  f_renderRateMergeTable(<%= AspxTools.JsString(m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "0" : "1") %>);
  
  <% if (!m_hasError) { %>
    f_hideWaiting();
    document.getElementById("MainPanel").style.display = ""; 
    var renderResultEnd = (new Date()).getTime();
    parent.PML.setSessionStorage("ClientPriceTiming_RenderResultStart", renderResultStart);
    parent.PML.setSessionStorage("ClientPriceTiming_RenderResultEnd", renderResultEnd);
    parent.PML.setSessionStorage("ClientPriceTiming_NavTiming", JSON.stringify(performance.timing));

  <% } %>
}
function f_reportTimeout() {
  var args = new Object();
  args["LoanID"] = g_loanID;
  args["ProductCount"] = g_sNumberOfProducts;
  args["RequestID"] = g_sRequestID;
  args["ResultPollingCount"] = g_iResultPollingCount + '';
  args["StartedTime"] = <%= AspxTools.JsString(Tools.GetDBCurrentDateTime().ToString()) %>;
  
  gService.main.call("GetResult_ReportTimeout", args);
}
  var g_time1 = (new Date()).getTime();

function f_displayRateMonitorByName(name) {
    var heightToPopup = 275 + <%if(m_IsQP2File) {%> 40 <%}else { %> 0 <%} %>;
    _popupWindow = window.open(gVirtualRoot+ '/Main/RateMonitorDialog.aspx?loanid=' + g_loanID + '&bpgroupname=' + name,
      'Monitor_Rates', 'width=300px,height='+heightToPopup +'px' );
}
function f_displayHasDuplicate()
{
    parent.LQBPopup.ShowElement(
          $j('#divForDuplicatePmlLoanSubmissions')
        , {
               'height': 130 // resize the window to match the size of the content
             , 'hideCloseButton': true
          }
        );
}


function f_submitRateMerge(groupIndex, rateIndex, bIsRateLock) {
    
    if( !f_allowClick() ) return false;
    
    <%if(m_HasDuplicate){ %>
        f_displayHasDuplicate();
        return false;
    <%} %>
    
    var bIsRateLock = !!bIsRateLock; // Do this to handle case where bIsRateLock is null.
    var gRateOptionList = f_getRateOptionList(<%= AspxTools.JsNumeric(m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? 0 : 1) %>);
    var gLpList = f_getLpList(<%= AspxTools.JsNumeric(m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? 0 : 1) %>);
  
    var rateOption = gRateOptionList[groupIndex][2][rateIndex]
  	    
	var productIndex = rateOption[12];
	var productData = gLpList[productIndex];
  
    var productID = productData[0];
    var productName = productData[1];
    var uniqueChecksum = productData[7];
    var bIsBlockedRateLockSubmission = productData[4] + "";
  
    var rate = rateOption[0];
    var point = rateOption[1];
    var apr = rateOption[2];
    var margin = rateOption[3];
    var payment = rateOption[4];
    var bottom = rateOption[6];
    var teaserRate = rateOption[8];
    var qrate = rateOption[25] == '' ? rateOption[0] : rateOption[25];
    var qualPmt = rateOption[10];
    var rawId = rateOption[7];
    var parId = rateOption[27];
  
    <% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan) { %>
        f_qualify2ndLoan(productID, rate, point, payment, rawId, gVerHash[productData[2]], bIsBlockedRateLockSubmission, productData[5], uniqueChecksum );
    <% } else if (m_hasLinkedLoan  && false ) {  // Allow user pick first loan, but alert when they try to submit both loans.%> 
        alert (<%= AspxTools.JsString(JsMessages.LpeUnableToSubmitLoanProgram) %>);
        return false;
    <% } else if (m_RegisterAndRateLockBlockingMessage != string.Empty){ %>
        alert(<%= AspxTools.JsString(m_RegisterAndRateLockBlockingMessage) %>);
        return false;
    <% } else if (UnableToSubmitWithoutCredit) { %>
        alert(<%= AspxTools.JsString(JsMessages.LPE_CannotSubmitWithoutCredit) %>);
        return false;
    <% } else { %>
  
        var args = new Object();
        args["loanid"] = g_loanID;
        args["productid"] = productID;
        args["rate"] = rate;
        args["point"] = point;
        args["apr"] = apr;
        args["margin"] = margin;
        args["payment"] = payment;
        args["bottom"] = bottom;
        args["productName"] = productName;
        args["uniqueChecksum"] = uniqueChecksum;  
        args["teaserrate"] = teaserRate;
        args["qrate"] = qrate;
        args["qualPmt"] = qualPmt;
        args["Has2ndLoan"] = <%= AspxTools.JsString(m_has2ndLoan.ToString()) %>;
        args["RawId"] = rawId;
        args["Version"] = gVerHash[productData[2]];
        args["1stRate"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>;
        args["1stPoint"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>;
        args["1stMPmt"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>;
        args["1stProductId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>;    
        args["1stRawId"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rawId")) %>;
        args["1stBlockSubmit"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmit")) %>;
        args["1stBlockSubmitMsg"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stBlockSubmitMsg")) %>;
        args["1stUniqueChecksum"] = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stUniqueChecksum")) %>;
        args["debugResultStartTicks"] = <%= AspxTools.JsString(DateTime.Now.Ticks.ToString()) %>;
        args["blocksubmit"] = bIsBlockedRateLockSubmission;
        args["blocksubmitmsg"] = productData[5];
        args["requestratelock"] = bIsRateLock ? "t" : "f";
        args["parId"] = parId;
  
        var queryString = "";
        for (var o in args) {
            queryString += o + "=" + escape(args[o]) +"&";
        }
        
        <% if (lpeRunModeT ==  E_LpeRunModeT.OriginalProductOnly_Direct) { %>
            var expiredRateDialog = gVirtualRoot +"/Main/LockUnavailable.aspx";
            var expiredDialogTitle = "Lock Unavailable";
        <% } else { %>
            var expiredRateDialog = gVirtualRoot +"/Main/ConfirmationPage.aspx";
            var expiredDialogTitle = g_confirmTitle;
        <% } %>
        var dialogUrl = bIsBlockedRateLockSubmission == "0" ? gVirtualRoot +"/Main/ConfirmationPage.aspx" : expiredRateDialog ;
        var winTitle = bIsBlockedRateLockSubmission == "0" ? g_confirmTitle : expiredDialogTitle;
        winTitle = winTitle.replace(' ', '_');
        popupWindow = window.open(dialogUrl + "?" + queryString, winTitle,'width=500px,height=300px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no');

        return false;
    <% } %>
}

function f_previewRateMerge(groupIndex, rateIndex) {
    if( !f_allowClick() ) return false;
    
    <%if(m_HasDuplicate){ %>
        f_displayHasDuplicate();
        return false;
    <%} %>
    
    var gRateOptionList = f_getRateOptionList(<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>);
    var gLpList = f_getLpList(<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>);

    
    var productIndex = gRateOptionList[groupIndex][2][rateIndex][12];
    var productData = gLpList[productIndex];
    var productid= productData[0];
    var uniqueChecksum = productData[7];	   
    
    
      var productRate = gRateOptionList[groupIndex][2][rateIndex][0];
      var productDTI = gRateOptionList[groupIndex][2][rateIndex][6]; 
      var productPoint = gRateOptionList[groupIndex][2][rateIndex][1];
      var productRawId = gRateOptionList[groupIndex][2][rateIndex][7];
    
    var extraParms = '';
    if( <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %> == 1 )
    {
    
      extraParms = [ "&FirstLienLpId="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stProductID")) %>,     
                     "FirstLienNoteRate=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stRate")) %>, 
                     "FirstLienPoint="+ <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stPoint")) %>, 
                     "FirstLienMPmt=" + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("1stMPmt")) %>].join("&");
    } 
    
    var version = gVerHash[productData[2]];
    _popupWindow = window.open(gVirtualRoot +'/Main/QualifiedLoanProgramPrintFrame.aspx?loanid=' + g_loanID
        + '&productid=' + productid
        + '&lienqualifymodet=' + <%= AspxTools.JsString(m_currentLienQualifyModeT.ToString("D")) %>
        + '&version=' + version
        + '&uniqueChecksum=' + uniqueChecksum
        + extraParms
        + '&productRate=' + productRate 
        + '&productDTI=' + productDTI
        + '&productPoint=' + productPoint
        + '&productRawId=' + productRawId,
        'Preview_Certificate',
        'width=880px,height=580px,centerscreen=yes,resizable=yes,scrollbars=yes,status=yes,help=no' );
    return false;
}


//-->
</script>

<script id="closingCostBreakdownTemplate" type="text/x-jquery-tmpl">
    <div id="ClosingCostBreakdownContainer">
        <h3 id="ClosingCostBreakdownHeader">Estimated Closing Cost Breakdown</h3>

        <br />

        <div><span class='FieldLabel'>Loan Program:</span> ${LoanProgram}</div>
        <div><span class='FieldLabel'>Note Rate:</span> ${NoteRate}%</div>

        <br />

        <table id="breakdownTable">
            <tr class="RateItemHeader">
                {{if TRID == "2"}}
                    <td class="first">Loan Est Sec</td>
                {{else}}
                    <td class="first">HUD Line #</td>
                {{/if}}
                <td>Description</td>
                <td>Amount</td>
                <td class="SourceColumn">Source</td>
            </tr>
            {{each(i, cost) closingCostBreakdown}}
                <tr>
                    {{if TRID == "2"}}
                        <td>${cost.LoanSec}</td>
                    {{else}}
                        <td>${cost.HudLine}</td>
                    {{/if}}
                    <td>${cost.Description}</td>
                    <td class='closingCostFee'>${cost.Fee}</td>
                    <td class='SourceColumn' style='text-align: right' >${cost.Source}</td>
                </tr>
            {{/each}}
            
            <tr style="background-color:white;border: 0; ">
                <td style="border: 0; padding-top: 1em;"> &nbsp; </td> 
                <td style="border: 0; padding-top: 1em;" class="closingCostLabel">Prepaid Charges</td>
                <td style="border: 0; padding-top: 1em;" class="closingCostFee">${PrepaidCharges}</td>
            </tr>
            <tr style="background-color:white; ">
                    <td style="border: 0; border-bottom: 2px solid black;"> &nbsp; </td> 
                    <td style="border: 0; border-bottom: 2px solid black;" class="closingCostLabel">Non-Prepaid Charges</td>
                    <td style="border: 0; border-bottom: 2px solid black;" class="closingCostFee">${NonPrepaidCharges}</td>
                </tr>
                <tr id="ClosingCostRow" style="background-color:white;border: 0;">
                    <td style="border: 0;"> &nbsp; </td> 
                    <td  style="border: 0;" class="closingCostLabel">Cost for Borrower to Close:</td>
                    <td style="border: 0;" class="closingCostFee">${ClosingCostFee}</td>
                </tr>
        </table>
        <div id="titleDisclaimer">
            <br />
            <span id="faccDisclaimer">
            <span class='FieldLabel'>First American Quote Disclaimer:</span>
            <p style="font-size:80%" >Answer(s) to one or more parameter question(s) First American Title Insurance Company utilizes to gather data required to
            determine the rate to be charged or validity of a particular product for a particular use has/have been preselected.  As a
            result, such parameter question(s) has/have been eliminated from the Site User�s view on <%= AspxTools.HtmlString(CurrentBroker.Name) %>'s
            site.  Such pre-selections may, in some instances, result in the fee quoted by <%= AspxTools.HtmlString(CurrentBroker.Name) %>
            differing from the fee returned by First American�s First American Comprehensive Calculator ("FACC").</p>
            <br />
            <p style="font-size:80%" >There are many variables that need to be considered in determining the final rate to be charged, including geographic and
            transaction specific items which are beyond the functionality provided by First American's FACC.  All estimates obtained
            through the use of the FACC are dependent upon the accuracy of the information entered into the FACC, including, but not
            limited to, the answers to the parameter questions referred to above.  Please contact your local First American office or
            agent to confirm the accuracy of the rates quoted.</p>
            </span>
        </div>

    </div>
</script>
<% if (displayCashToCloseAndReservesCalculation) { %>
<script id="CashToCloseTemplate" type="text/x-jquery-tmpl">
    <div id="CashToCloseContainer">
        <h3 id="CashToCloseHeader">Cash to Close Calculation</h3>

        <br />

        <table style="width: 100%">
            <tr>
                <td style="text-align:left"><span class='FieldLabel'>Loan Program:</span> ${LoanProgram}</td>
                <td style="text-align:right"><span class='FieldLabel'>Note Rate:</span> ${NoteRate}%</td>
            </tr>
        </table>
        <br />

        <table id="cashToCloseTable" >
            <tr class="ElBg">
                <td class="cashLabel">a. Purchase Price</td>
                <td></td>
                <td class="cashValue">${sPurchPrice}</td>
                <td class="cashLabel">j. Subordinate financing</td>
                <td class="cashValue">${sONewFinBal}</td>
            </tr>
            <tr class="ElAltBg">
                <td class="cashLabel">b. Alterations, improvements, repairs</td>
                <td></td>
                <td class="cashValue">${sAltCost}</td>
                <td class="cashLabel">k. Borrower's closing costs paid by Seller</td>
                <td class="cashValue">${sTotCcPbs}</td>
            </tr>
            <tr class="ElBg">
                <td class="cashLabel">c. Land (if acquired separately)</td>
                <td></td>
                <td class="cashValue">${sLandCost}</td>
                <td class="cashLabel">l. ${sOCredit1Desc} </td>
                <td class="cashValue">${sOCredit1Amt}</td>
            </tr>
            <tr class="ElAltBg">
                <td class="cashLabel">d. Refi (incl. debts to be paid off)</td>
                <td></td>
                <td class="cashValue">${sRefPdOffAmt1003}</td>
                <td class="cashLabel">${sOCredit2Desc}</td>
                <td class="cashValue">${sOCredit2Amt}</td>
            </tr>
            <tr class="ElBg">
                <td class="cashLabel">e. Estimated prepaid items</td>
                <td></td>
                <td class="cashValue">${sTotEstPp1003}</td>
                <td class="cashLabel">${sOCredit3Desc}</td>
                <td class="cashValue">${sOCredit3Amt}</td>
            </tr>
            <tr class="ElAltBg">
                <td class="cashLabel indented">
                    {{if sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp }}
                    Prepaids and Reserves
                    {{/if}}
                </td>
                <td class="cashValue">
                    {{if sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp }}
                    ${sTotEstPp}
                    {{/if}}
                </td>
                <td></td>
                <td class="cashLabel">${sOCredit4Desc}</td>
                <td class="cashValue">${sOCredit4Amt}</td>
            </tr>
            <tr class="ElBg">
                <td class="cashLabel indented">
                    {{if sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp }}
                    Prorations paid by borrower
                    {{/if}}
                </td>
                <td class="cashValue">
                    {{if sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp }}
                    ${sTotalBorrowerPaidProrations}
                    {{/if}}
                </td>
                <td></td>
                <td class="cashLabel">Lender Credit</td>
                <td class="cashValue">${sOCredit5Amt}</td>
            </tr>
            {{if !sIsIncludeONewFinCcInTotEstCc}}
            <tr class="ElAltBg">
                <td></td>
                <td></td>
                <td></td>
                <td class="cashLabel">Other financing closing costs</td>
                <td class="cashValue">${sONewFinCc}</td>
            </tr>
            {{/if}}
            <tr class="{{if sIsIncludeONewFinCcInTotEstCc}}ElAltBg{{else}}ElBg{{/if}}">
                <td class="cashLabel">f. Estimated closing costs</td>
                <td></td>
                <td class="cashValue">${sTotEstCcNoDiscnt1003}</td>
                <td class="cashLabel">{{if !(sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp)}}m. Loan amount (exclude PMI, MIP, FF financed){{/if}}</td>
                <td class="cashValue">{{if !(sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp)}}${sLAmtCalc}{{/if}}</td>
            </tr>
            {{if sIsIncludeONewFinCcInTotEstCc}}
            <tr class="ElBg">
                <td class="cashLabel indented">This loan closing costs</td>
                <td class="cashValue">${sTotEstCcNoDiscnt}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="ElAltBg">
                <td class="cashLabel indented">Other financing closing costs</td>
                <td class="cashValue">${sONewFinCcInverted}</td>
                <td></td>
                <td class="cashLabel">{{if (sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp)}}m. Loan amount (exclude PMI, MIP, FF financed){{/if}}</td>
                <td class="cashValue">{{if (sIsIncludeProrationsIn1003Details && sIsIncludeProrationsInTotPp)}}${sLAmtCalc}{{/if}}</td>
            </tr>
            {{/if}}
            <tr class="{{if sIsIncludeONewFinCcInTotEstCc}}ElBg{{else}}ElAltBg{{/if}}">
                <td class="cashLabel">g. PMI, MIP, Funding Fee</td>
                <td></td>
                <td class="cashValue">${sFfUfmip1003}</td>
                <td class="cashLabel">n. PMI, MIP, Funding Fee financed</td>
                <td class="cashValue">${sFfUfmipFinanced}</td>
            </tr>
            <tr class="{{if sIsIncludeONewFinCcInTotEstCc}}ElAltBg{{else}}ElBg{{/if}}">
                <td class="cashLabel">h. Discount (if Borrower will pay)</td>
                <td></td>
                <td class="cashValue">${sLDiscnt1003}</td>
                <td class="cashLabel">o. Loan amount (add m &amp; n)</td>
                <td class="cashValue">${sFinalLAmt}</td>
            </tr>
            <tr class="{{if sIsIncludeONewFinCcInTotEstCc}}ElBg{{else}}ElAltBg{{/if}}">
                <td class="cashLabel">i. Total costs (add items a to h)</td>
                <td></td>
                <td class="cashValue">${sTotTransC}</td>
                <td class="cashLabel" style="font-weight:bold">p. Cash from / to Borr (subtract j, k, l &amp; o from i)</td>
                <td class="cashValue" style="font-weight:bold">${sTransNetCash}</td>
            </tr>
        </table>
    </div>
</script>

<script id="ReservesTemplate" type="text/x-jquery-tmpl">
    <div id="ReservesContainer">
        <h3 id="ReservesHeader">Reserve Months Calculation</h3>

        <br />

        <table style="width: 100%">
            <tr>
                <td style="text-align:left"><span class='FieldLabel'>Loan Program:</span> ${LoanProgram}</td>
                <td style="text-align:right"><span class='FieldLabel'>Note Rate:</span> ${NoteRate}%</td>
            </tr>
        </table>
        <br />

        <table id="reservesTable" >
            <tr>
                <td></td>
                <td></td>
                <td>Total Liquid Assets - Cash from Borrower</td>
                <td></td>
                <td>${sTotLiquidAssets} - ${sTransNetCashNonNegative}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Reserve Months</td>
                <td>=</td>
                <td><hr /></td>
                <td>=</td>
                <td><hr /></td>
                <td>=</td>
                <td>${Reserves} months</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Qualifying PITIA</td>
                <td></td>
                <td>${sMonthlyPmt} / month</td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</script>
<% } %>

<div id="divForDuplicatePmlLoanSubmissions" style="display:none">
    <div style="padding-top: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
        <div style="color:Red;text-align:left">
          *A loan file with duplicate borrower and property information has already been received.
          Submission for registration/lock of this loan file is prohibited.
         </div>
         <br />
         <div style="text-align:left">
          Please contact your lock desk if you have any questions regarding this alert message.
         </div>
         <br />
         <div>
            <input type="button" value="Close" onclick='LQBPopup.Hide()'/>
         </div>
     </div>
</div>

<table width="100%" height="400" border="0" id="WaitTable">
	<tr>
		<td align="center" valign="middle">
      <div class="loading-content">
          <img  id="Img1" class="img-loading" src="~/images/loading.gif" alt="Loading..." runat="server"/>
          <h3 class="text-loading"><%= AspxTools.HtmlString(m_waitingMessage) %></h3>
      </div>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" style="PADDING-LEFT: 5px">
<SPAN id=MainPanel style="display: none"><asp:Panel ID="MainResultPanel" width="99%" Runat=server EnableViewState=False>
<script type="text/javascript">


<%-- This function display the certificate. Shorten the function name to save space. printLoanProduct--%>
function f_getLpList(qualMode) {
  if (qualMode == 0) {
    return gLpList0;
  } else {
    return gLpList1;
  }
}
function f_getRateOptionList(qualMode) {
    if (qualMode == 0) {
      if (typeof(gRateOptionList0) != 'undefined')
        return gRateOptionList0;
    } else {
      if (typeof(gRateOptionList1) != 'undefined')
        return gRateOptionList1;
    }
    return null;
}  

function f_getLinkTitle<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>(bIsBlockedRateLockSubmission) {
    var titles = <%= AspxTools.JsArray(GetLinkTitles()) %>;
    var title = bIsBlockedRateLockSubmission ? titles[1] : titles[0];
    return title == "" ? "&zwnj;" : title;
}
function f_renderRateMergeTable(qualMode) {
  var $oTable = $j('#RateMergeEligibleTable' + qualMode);
  var isRateMonitorEnabled =  <% if ( m_showMonitorRatesLink ) { %> true <% } else { %> false <% } %>;
  var gRateOptionList = f_getRateOptionList(qualMode);
  
  if (null == gRateOptionList) return;
  var gLpList = f_getLpList(qualMode);
  var iLpCount = gRateOptionList.length;
  
  var numOfCols = <%= AspxTools.JsNumeric(14 - ( m_sProdDocT_NoIncome ? 3 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1) - (m_showQm ? 0 : 1)) %>;
  for (var i = 0; i < iLpCount; i++) {
    var cssName = 'ElAltBg';
    var oLp = gRateOptionList[i];
    var $oTBody = $j('<tbody/>').addClass(cssName).appendTo($oTable); //document.createElement("tbody");
    var $oRow = null;
    var $oCell = null;
    
    if (i != 0) {
      $oRow = $j('<tr/>').appendTo($oTBody); //.insertRow(-1);
      $oCell = $j('<td/>').prop('colSpan', numOfCols).html('<hr />').appendTo($oRow);
    }
    $oRow = $j('<tr/>').appendTo($oTBody);
    $oCell = $j('<td/>').addClass('MergeGroupName').html(oLp[0]).appendTo($oRow);
    
    
    var mergeGroupCols = isRateMonitorEnabled ? numOfCols - 1 : numOfCols; 
    
    $oCell.css({'padding-right':'2px'}).prop('noWrap', true).prop('colSpan', mergeGroupCols);
    
    if (isRateMonitorEnabled) {
        $oCell = $j('<td/>').addClass('MergeGroupName').html("<a href='javascript:void(0)' class='RateMonitorButton' onclick='f_displayRateMonitorByName(\"" + oLp[0] + "\");'>monitor rates</a>").appendTo($oRow);
        $oCell.css({'padding-right':'2px'}).prop('noWrap', true);
    }
    
    var $oMoreTBody = $j('<tbody/>').addClass(cssName).prop('id', 'rmM_' + qualMode + '_' + i).appendTo($oTable);//.createElement("tbody");

    var oRateOptionList = oLp[2];
    var rowCssName = 'ElBg';
    var previousRate = '';
    for (var j = 0; j < oRateOptionList.length; j++) {
      var oRateOption = oRateOptionList[j];
      if (oRateOption[0] != previousRate) {
        rowCssName = rowCssName == 'ElAltBg' ? 'ElBg' : 'ElAltBg';
      }  
      
      var rateExpiredCssName = 'ri ';
      var bIsBlockedRateLockSubmission = gLpList[oRateOption[12]][4] == 1;
      var bAreRatesExpired = gLpList[oRateOption[12]][6] == 1;
      if (bAreRatesExpired) {
        rateExpiredCssName = 'rie ';
      }    
      var linkTitle =  qualMode == 0 ? f_getLinkTitle0(bIsBlockedRateLockSubmission) : f_getLinkTitle1(bIsBlockedRateLockSubmission);
      var violateDtiCss = oRateOption[13] == 1 ? ' ViolateDti' : '';
      var violateDtiNotation = oRateOption[13] == 1 ? '** ' : '';           
      previousRate = oRateOption[0];
      $oRow = $j('<tr/>').appendTo($oMoreTBody);
      if (oRateOption[23] == 'True') {
        $oRow.addClass('winner');
      }
      else {
        $oRow.addClass('looser').attr('data-rateid', oRateOption[0].replace(/\./g,'_'));
      }
      f_insertCellSubmit($oRow, linkTitle, i, j, rowCssName, qualMode, oRateOption[15], oRateOption[16], oRateOption[19]);
      <% if ( m_isOptionARMUsedInPml ) { %>
        f_insertCellRateMerge($oRow, oRateOption[8], rateExpiredCssName + rowCssName); <%-- // TeaserRate; --%>
      <% } %>
      f_insertCellRateMerge($oRow, oRateOption[0], rateExpiredCssName + rowCssName); <%-- // Rate; --%>
      f_insertCellRateMerge($oRow, oRateOption[11], rateExpiredCssName + rowCssName); <%-- // Point display in result; --%>
      f_insertCellRateMerge($oRow, oRateOption[4], rateExpiredCssName + rowCssName); <%-- // FirstPmtAmt; --%>
      <% if (!m_sProdDocT_NoIncome) { %>
        f_insertCellRateMerge($oRow, "<a href='#' class='displayMsgLink' displayMsg='Max DTI: " + gLpList[oRateOption[12]][3] + "'>" + violateDtiNotation + oRateOption[6] + "</a>", rateExpiredCssName + rowCssName + violateDtiCss); <%-- // Bottom Ratio; --%>
      <% } %>
      f_insertCellRateMerge($oRow, oRateOption[2], rateExpiredCssName + rowCssName); // APR
      <% if (m_showQm) { %>
      f_insertQMCell($oRow,  rateExpiredCssName + rowCssName, oRateOption[26], oRateOption[28]); 
      <% } %>
      
      //<%-- 
          /* If you want to change how the closing cost breakdown is rendered please also see look for the same
           * comment(s) in GetResultsNewUI.ascx, GetResultsNewUI.ascx.cs, LoanProductResultControlNewUI.cs, LoanComparisonXml.cs.
           */
           // This constructs the link to display the closing cost breakdown.
           // Sorting is done in the code behind for eligible loan programs. 
      //--%>
      
      var closingCostsLink = "<a href='#' class='displayCostsLink' " + 
                             "closingCostBreakdown='" + oRateOption[20] + "'" +
                             "ClosingCost='" + oRateOption[18] + "'" +
                             "PrepaidCharges='" + oRateOption[21] + "'" +
                             "NonPrepaidCharges='" + oRateOption[22] + "'" +
                             "LoanProgram='" + gLpList[oRateOption[12]][1] + "'" +
                             "NoteRate='" + oRateOption[0] + "'" +
                             "TRID='" + oRateOption[29] + "'" +
                             ">" + oRateOption[18] + "</a>";
      f_insertCellRateMerge($oRow, closingCostsLink, rateExpiredCssName + rowCssName); // Closing costs

      <% if ( displayCashToCloseAndReservesCalculation ) { %>
      var cashToCloseLink = f_getCashToCloseLink( oRateOption[30], gLpList[oRateOption[12]][1], oRateOption[0] );
      f_insertCellRateMerge($oRow, cashToCloseLink, rateExpiredCssName + rowCssName); // Cash to close
    <% } %>

      <% if (IsDisplayReserveMonths) { %>
        <% if ( displayCashToCloseAndReservesCalculation ) { %>
        var reservesLink = f_getReservesLink( oRateOption[30], gLpList[oRateOption[12]][1], oRateOption[0], oRateOption[9] );
        f_insertCellRateMerge($oRow, reservesLink, rateExpiredCssName + rowCssName); // Reserves Calc
        <% } else { %>
        f_insertCellRateMerge($oRow, oRateOption[9], rateExpiredCssName + rowCssName); // Reserves
        <% } %>
      <% } else { %>
      f_insertCellRateMerge($oRow, oRateOption[17], rateExpiredCssName + rowCssName); // Break Even Point
      <% } %>
      
      var size = 35;
      var templateName = gLpList[oRateOption[12]][1]; 
      if (templateName && templateName.length >= size)
      {
        templateName = templateName.substring(0,size) + '...';
      }
      f_insertCellRateMerge($oRow, "<a href='#' onclick='return f_previewRateMerge(" + i + "," + j + ");' title='"+ gLpList[oRateOption[12]][1]  + "'>" + templateName + "</a>", 'ri1 ' + rowCssName); // LP Name
      
    }
    var $oLessTBody = $j('<tbody/>').prop('id', 'rML_' + qualMode + '_' + i).addClass(cssName).css({'display':'none'}).appendTo($oTable); //document.createElement("tbody");

      var oRateOption = oRateOptionList[oLp[1]];
      
      var rateExpiredCssName = 'ri ';
      var bIsBlockedRateLockSubmission = gLpList[oRateOption[12]][4] == 1;
      var bAreRatesExpired = gLpList[oRateOption[12]][6] == 1;

      if (bAreRatesExpired) {
        rateExpiredCssName = 'rie ';
      }    
      var linkTitle =  qualMode == 0 ? f_getLinkTitle0(bIsBlockedRateLockSubmission) : f_getLinkTitle1(bIsBlockedRateLockSubmission);
      var violateDtiCss = oRateOption[13] == 1 ? ' ViolateDti' : '';      
      var violateDtiNotation = oRateOption[13] == 1 ? '** ' : '';      
      $oRow = $j('<tr/>').appendTo($oLessTBody);
      f_insertCellSubmit($oRow, linkTitle, i, oLp[1], '', qualMode, oRateOption[15], oRateOption[16], oRateOption[19]);

      <% if ( m_isOptionARMUsedInPml ) { %>
        f_insertCellRateMerge($oRow, oRateOption[8], 'ri'); <%-- // TeaserRate; --%>
      <% } %>
      f_insertCellRateMerge($oRow, oRateOption[0], rateExpiredCssName); <%-- // Rate; --%>
      f_insertCellRateMerge($oRow, oRateOption[11], rateExpiredCssName); <%-- // Point display in result; --%>
      f_insertCellRateMerge($oRow, oRateOption[4], 'ri'); <%-- // FirstPmtAmt; --%>
      <% if (m_sProdDocT_NoIncome) { %>
        f_insertCellRateMerge($oRow, oRateOption[3], 'ri'); <%-- // Margin; --%>
      <% } else { %>
        var qrate = oRateOption[25] == '' ? oRateOption[0] : oRateOption[25];

        f_insertCellRateMerge($oRow, qrate, 'ri'); <%-- // QRate; --%>
        f_insertCellRateMerge($oRow, oRateOption[3], 'ri'); <%-- // Margin; --%>
        f_insertCellRateMerge($oRow, violateDtiNotation + oRateOption[6], 'ri' + violateDtiCss); <%-- // Bottom Ratio; --%>
      f_insertCellRateMerge($oRow, gLpList[oRateOption[12]][3], 'ri ' + violateDtiCss); // MAX DTI
    
      <% } %>
      
      f_insertCellRateMerge($oRow, gLpList[oRateOption[12]][1], 'ri1'); // LP Name
      
  }
}

function f_insertCellSubmit(oRow, linkTitle, groupIndex, rateIndex, cssName, qualMode, disqualified, reason, pinId) {
  var gRateOptionList = f_getRateOptionList(qualMode);
  var rateOption = gRateOptionList[groupIndex][2][rateIndex];  
  var productIndex = gRateOptionList[groupIndex][2][rateIndex][12];
  var gLpList = f_getLpList(qualMode);
  var productData = gLpList[productIndex];
  var productid= productData[0];
  var rate = rateOption[0];
  var $oCell = $j('<td />').addClass(cssName).css('width', '50px').appendTo(oRow);
    if (rateOption[23] == 'True' && rateOption[24] > 0){
    var rateId = rateOption[0].replace(/\./g,'_'); 
    $oCell.html('<a href="#" class="showlooser" data-rateid="' + rateId + '">+</a>&nbsp;');
  }
  else {
  $oCell.html("&nbsp;");
  }
  $oCell.css({'text-align':'right'});
  
  $oCell = $j('<td>').addClass(cssName).html('&nbsp;').appendTo(oRow);  //.insertCell(-1);
  
  if (disqualified != 1) {
      <% if ( m_disablePinLinks ) { %>
      $oCell.html("<a href='#' style='color: #ccc' "
      + "class='displayWideMsgLink' displayMsg='Loan Comparison Reports "
      + "are not currently available for scenarios with subordinate financing.'>"
      + "pin</a>");
      <% } else { %>
      var linkText = pinId == '' ? 'pin' : 'unpin';

      if ($j("#sIsRenovationLoanSupportEnabled").val() === "True") {
          $oCell.html("<a href='#' disabled='true'>" + linkText + "</a>");
      }
      else {
          $oCell.html("<a href='#' pinId='" + pinId + "' onclick='parent.editPinState($j(this), \""
            + productid // product id
            + "\",\"" + rate // note rate
            + "\");return false;'>" + linkText + "</a>");      
      }
      <% } %>
        
            $oCell.css({'border-right':'1px solid #999999'});
  }
  
  $oCell.css({'text-align':'right','padding-right':'3px'}).prop('noWrap',true);
  if( disqualified == 1 ) 
  {
        $oCell = $j('<td colspan="2" />').addClass(cssName).appendTo(oRow); 
        $oCell.css({'text-align':'right','padding-right':'3px', 'padding-left':'3px'}).prop('noWrap',true);
        var escaped = $j('<div/>').text(reason).html(); 
        $oCell.html($j('<div>').append($j('<a>').prop('href' , '#').addClass('unavailable').attr({
            'data-msg' : escaped,
            'data-func' : 'f_submitRateMerge',
            'data-arg1' : groupIndex,
            'data-msg' : escaped,
            'data-arg4' : linkTitle,
            'data-arg2' : rateIndex }).text('unavailable')).html());
        return;
  }

  $oCell = $j('<td />').addClass(cssName).appendTo(oRow); //.insertCell(-1);

  if (linkTitle != null && linkTitle != "" ) { //&& !parent.PML.options.IsLead) {
  <%-- 7/28/2010 dd - OPM 42047 --%>
    $oCell.css({'text-align':'right','padding-right':'3px','padding-left':'3px'}).prop('noWrap', true);
    var parts = linkTitle.split('/');
    var firstLink = parts[0];
    if (firstLink == '?')
    {
        $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>Lock</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>");
    }
    else if( parts[0] == "register loan?"){
	  $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' onclick='return false;' disabled='disabled'>register</a>&nbsp;<a href='#' onclick='return f_SRM();'>?</a>");
    }
    else 
    {
      var isRateLock = firstLink == "lock rate";
      firstLink = firstLink == "register loan" ? "register" : firstLink;
      firstLink = firstLink == "lock rate" ? "request lock" : firstLink;
	  $oCell.html("<a href='#' onclick='return f_submitRateMerge(" + groupIndex + "," + rateIndex + ", " + isRateLock + ");'>"+ firstLink + "</a>");
	}
	
	  $oCell = $j('<td />').addClass(cssName).appendTo(oRow);        
	  if (parts.length > 1) {
	    $oCell.css({'text-align':'right','padding-right':'3px', 'padding-left':'3px', 'border-left':'1px solid #999999'}).prop('noWrap',true);
	  // Lock Rate
	    var secondLink = parts[1];
        secondLink = secondLink == "lock rate" ? "request lock" : secondLink;
	    if ( secondLink == '?')
	    {
	        $oCell.html("<a href='#' disabled='disabled'>request lock</a>&nbsp;<a href='#' onclick='f_ShowLockMsg()'>?</a>");
	    }
	    else
	        $oCell.html("<a href='#' onclick='return f_submitRateMerge(" + groupIndex + "," + rateIndex + ", true);'>"+ secondLink + "</a>");
	  }
	}

}

function f_insertQMCell(oRow, cssClass, qmMode, hpmMode) {
  var content = $j('<div style="text-align:center">');
  
  if (cssClass  && cssClass.indexOf('ri1') > -1) {
    content.addClass('pn');
  }
  if (typeof qmMode != 'undefined' && qmMode != 0){ 
    var img = $j('<img width=10 />'), imgUrl, tooltip = ''; 
    if (qmMode == 2 || qmMode == 3){
        tooltip = 'QM Status - ';
        if ( qmMode == 2){
            tooltip += 'Provisionally Eligible'; 
        }else{
            tooltip += 'Eligible';
        }

        if( hpmMode == 2){
            imgUrl = '../images/warn.png';
            tooltip += "\nHigher-priced QM"
        } else {
            imgUrl = '../images/pass.png';
        }
    }
    else if (qmMode == 1){
        imgUrl = '../images/fail.png';
        tooltip = 'QM Status - Fail';
    }
    img.prop('src', imgUrl);
    img.prop('alt', tooltip).appendTo(content);
  } 
  else {
    content.html('&nbsp;')
  }
  $j('<td align=center />').append(content).addClass(cssClass).prop('noWrap', true).appendTo(oRow);
}

    <% if ( displayCashToCloseAndReservesCalculation ) { %>
function f_getCashToCloseLink(loanDataJson, loanProgram, noteRate)
{
    var loanData = $j.parseJSON(loanDataJson);

    return "<a href='#' class='displayCashToCloseLink' " +
        "LoanProgram='" + loanProgram + "' " +
        "NoteRate='" + noteRate + "' " +
            "sPurchPrice='" + loanData.sPurchPrice + "' " +
            "sAltCost='" + loanData.sAltCost + "' " +
            "sLandCost='" + loanData.sLandCost + "' " +
            "sRefPdOffAmt1003='" + loanData.sRefPdOffAmt1003 + "' " +
            "sTotEstPp1003='" + loanData.sTotEstPp1003 + "' " +
            "sTotEstCcNoDiscnt1003='" + loanData.sTotEstCcNoDiscnt1003 + "' " +
            "sFfUfmip1003='" + loanData.sFfUfmip1003 + "' " +
            "sLDiscnt1003='" + loanData.sLDiscnt1003 + "' " +
            "sTotTransC='" + loanData.sTotTransC + "' " +
            "sONewFinBal='" + loanData.sONewFinBal + "' " +
            "sTotCcPbs='" + loanData.sTotCcPbs + "' " +
            "sOCredit1Desc='" + loanData.sOCredit1Desc + "' " +
            "sOCredit1Amt='" + loanData.sOCredit1Amt + "' " +
            "sOCredit2Desc='" + loanData.sOCredit2Desc + "' " +
            "sOCredit2Amt='" + loanData.sOCredit2Amt + "' " +
            "sOCredit3Desc='" + loanData.sOCredit3Desc + "' " +
            "sOCredit3Amt='" + loanData.sOCredit3Amt + "' " +
            "sOCredit4Desc='" + loanData.sOCredit4Desc + "' " +
            "sOCredit4Amt='" + loanData.sOCredit4Amt + "' " +
            "sOCredit5Amt='" + loanData.sOCredit5Amt + "' " +
            "sONewFinCc='" + loanData.sONewFinCc + "' " +
            "sONewFinCcInverted='" + loanData.sONewFinCcInverted + "' " +
            "sTotLiquidAssets='" + loanData.sTotLiquidAssets + "' " +
            "sLAmtCalc='" + loanData.sLAmtCalc + "' " +
            "sFfUfmipFinanced='" + loanData.sFfUfmipFinanced + "' " +
            "sFinalLAmt='" + loanData.sFinalLAmt + "' " +
            "sTransNetCash='" + loanData.sTransNetCash + "' " +
            "sIsIncludeProrationsIn1003Details='" + (loanData.sIsIncludeProrationsIn1003Details ? 'True' : 'False') + "' " +
            "sIsIncludeProrationsInTotPp='" + (loanData.sIsIncludeProrationsInTotPp ? 'True' : 'False') + "' " +
            "sTotEstPp='" + loanData.sTotEstPp + "' " +
            "sTotalBorrowerPaidProrations='" + loanData.sTotalBorrowerPaidProrations + "' " +
            "sIsIncludeONewFinCcInTotEstCc='" + (loanData.sIsIncludeONewFinCcInTotEstCc ? 'True' : 'False') + "' " +
            "sTotEstCcNoDiscnt='" + loanData.sTotEstCcNoDiscnt + "' " +
            "sAltCostLckd='" + loanData.sAltCostLckd + "' " +
            "sLandIfAcquiredSeparately1003='" + loanData.sLandIfAcquiredSeparately1003 + "' " +
            "sLDiscnt1003Lckd='" + loanData.sLDiscnt1003Lckd + "' " +
            "sPurchasePrice1003='" + loanData.sPurchasePrice1003 + "' " +
            "sRefNotTransMortgageBalancePayoffAmt='" + loanData.sRefNotTransMortgageBalancePayoffAmt + "' " +
            "sRefTransMortgageBalancePayoffAmt='" + loanData.sRefTransMortgageBalancePayoffAmt + "' " +
            "sSellerCreditsUlad='" + loanData.sSellerCreditsUlad + "' " +
            "sTotCcPbsLocked='" + loanData.sTotCcPbsLocked + "' " +
            "sTotCreditsUlad='" + loanData.sTotCreditsUlad + "' " +
            "sTotEstBorrCostUlad='" + loanData.sTotEstBorrCostUlad + "' " +
            "sTotEstCc1003Lckd='" + loanData.sTotEstCc1003Lckd + "' " +
            "sTotEstPp1003Lckd='" + loanData.sTotEstPp1003Lckd + "' " +
            "sTotMortLAmtUlad='" + loanData.sTotMortLAmtUlad + "' " +
            "sTotMortLTotCreditUlad='" + loanData.sTotMortLTotCreditUlad + "' " +
            "sTotOtherCreditsUlad='" + loanData.sTotOtherCreditsUlad + "' " +
            "sTotTransCUlad='" + loanData.sTotTransCUlad + "' " +
            "sTransNetCashUlad='" + loanData.sTransNetCashUlad + "' " +
            "sTRIDSellerCredits='" + loanData.sTRIDSellerCredits + "' " +
            ">" + loanData.sTransNetCash + "</a>";
    }

    function f_getReservesLink(loanDataJson, loanProgram, noteRate, reserves)
{
        var loanData = $j.parseJSON(loanDataJson);

    return "<a href='#' class='displayReservesLink' " +
        "LoanProgram='" + loanProgram + "' " +
        "NoteRate='" + noteRate + "' " +
        "Reserves='" + reserves + "' " +
         "sTotLiquidAssets='" + loanData.sTotLiquidAssets + "' " +
         "sTransNetCashNonNegative='" + loanData.sTransNetCashNonNegative + "' " +
         "sMonthlyPmt='" + loanData.sMonthlyPmt + "' " +
            ">" + reserves + "</a>";
    }
    <% } %>
function f_insertCellRateMerge(oRow, content, cssClass) {
  if (content == null || "" == content)
  {
    content = '&nbsp;';
  }
  if ( cssClass  && cssClass.indexOf('ri1') > -1) {
    content = $j('<div>').addClass('pn').html(content)[0].outerHTML;
  }

  var td = $j('<td />').html(content).addClass(cssClass).prop('noWrap', true); 
  td.appendTo(oRow);
}



  var gLpList<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %> = <%= AspxTools.JsArray(m_gDataLpList) %>;
  
  var gRateOptionList<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %> = <%= AspxTools.JsArray(m_gDataRateOptionList) %>;


function f_SRM()
{
    var message = "You do not have permission to register this loan";
    var reasonsCannotRegisterList = <%= AspxTools.JsArray(ReasonsCannotRegister) %>;
    var CannotRegisterFailedConditionType = <%= AspxTools.JsNumeric(CannotRegisterFailedConditionType) %>;

    var reasonList = "";
        
    if (reasonsCannotRegisterList.length == 0){
        message = "Registering is not permitted for this loan file.";
    }
    else{
        if (CannotRegisterFailedConditionType == 2 /* Restraint */) {
            message += ", because of the following reasons:";
        } else {
            message += ".  The loan can be registered under any of the following circumstances:";
        }

        reasonList = "<ul>";
        var alpha = new Array('A','B','C','D','E','F','G','H','I','J');
        for (var i = 0; i < reasonsCannotRegisterList.length && i < 10 ; i++)
        {
            reasonList += '<li>' + alpha[i] + '.&nbsp;' + reasonsCannotRegisterList[i];
        }
        reasonList+= "</ul>";
        

    }
    
    var oPanel = document.getElementById('RateWarningDiv');
        oPanel.style.display = 'block';
        oPanel.style.width= '650px';
        oPanel.style.height = '150px';
        oPanel.style.top = '100px';
        oPanel.style.left = '100px';
    
     oPanel.innerHTML = '<p>' + message + '</p>' + reasonList  + "<br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' /></span>";

    return false; 
}



function f_ShowLockMsg()
{
    var message = "You do not have permission to lock this loan";
    var reasonsCannotLockList = <%= AspxTools.JsArray(ReasonsCannotLock) %>;
    var CannotLockFailedConditionType = <%= AspxTools.JsNumeric(CannotLockFailedConditionType) %>;
    
    var reasonList = "";
    if (reasonsCannotLockList.length == 0)
    {
        message = "Locking the rate is not permitted for this loan file.";
    }
    else
    {
        if (CannotLockFailedConditionType == 2 /* Restraint */) {
            message += ", because of the following reasons:";
        } else {
            message += ".  The loan can be locked under any of the following circumstances:";
        }

        reasonList = "<ul>";
        var alpha = new Array('A','B','C','D','E','F','G','H','I','J');
        for (var i = 0; i < reasonsCannotLockList.length && i < alpha.length ; i++)
        {
            reasonList += '<li>' + alpha[i] + '.&nbsp;' + reasonsCannotLockList[i];
        }
        reasonList+= "</ul>";
    }
    var oPanel = document.getElementById('RateWarningDiv');
    oPanel.style.display = 'block';
    oPanel.style.width= '650px';
    oPanel.style.height = '150px';
    oPanel.style.top = '100px';
    oPanel.style.left = '100px';
    
    oPanel.innerHTML = '<p>' + message + '</p>' + reasonList  + "<br /><span style='text-align=center;width:100%'><input type='button' value=' Close ' onclick='f_closeLockMsg();' /></span>";
}
function f_closeLockMsg()
{
    document.getElementById('RateWarningDiv').style.display = 'none';
}

  var gData<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %> = <%= AspxTools.JsArray(m_gDataList) %>;
  
function f_buildHiddenRates<%= AspxTools.JsNumeric((int)m_currentLienQualifyModeT) %>(oTBody, data, id, currentIndex) {
  if (!!$j(oTBody).data('initialized'))
  {
    return;
  }
  var productInfo = data[0];
  var rateOptions = data[1];

  var bIsBlockedRateLockSubmission = productInfo[3] == 1; 
  var bAreRatesExpired = productInfo[5] == 1;

  var titles = <%= AspxTools.JsArray(GetLinkTitles()) %>;
    var linkTitle = bIsBlockedRateLockSubmission ? titles[1] : titles[0];
    linkTitle = linkTitle == "" ? "&zwnj;" : linkTitle;
  var m_isLead = <%= AspxTools.JsBool(m_isStatusLead) %>;

  for (var i = 0; i < rateOptions.length; i++) {
  
    var $oRow = $j('<tr/>').appendTo(oTBody);
    var $oCell = $j('<td/>').appendTo($oRow);
    var rateOption = rateOptions[i];
    var isDisqualified = rateOption[14] == 1;
    if (i == 0) {
      $oCell.html("<a href='#' onclick='return f00(" + currentIndex + ");'>preview</a>").addClass('c0').prop('vAlign','top');  
      $j('<td/>').html("<label for='M" + id + "'>View More </label><input type=checkbox id='M" + id + "' onclick='f_L(\"" + id + "\", this);updateSize();' checked>").addClass('c1')
                 .prop('vAlign','top').prop('noWrap',true).appendTo($oRow);
      
    } else {
      $oCell.html("&nbsp;").prop('colSpan', 2);
    }
    
    $oCell = $j('<td/>').appendTo($oRow).addClass('c2');
    var margin = rateOption[3] == '' ? '0.000' : rateOption[3];
    if (bIsBlockedRateLockSubmission) { 
     if ( isDisqualified )
     {
        var escaped = $j('<div/>').text(rateOption[15]).html(); 
        var disqualhtml = $j('<div>').append($j('<a>').prop('href', '#').addClass('unavailable').attr({
            'data-func' : 'f01',
            'data-arg1' : rateOption[7],
            'data-arg2' : id, 
            'data-msg' : escaped,
            'data-arg3' : currentIndex, 
            'data-arg4' : linkTitle
            }).text('unavailable')).html();
        $oCell.html($oCell.html() + disqualhtml);
     }
     else
        $oCell.html("<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ");\">" + linkTitle + "</a>");
    } 
    else if( isDisqualified ) {
        var escaped = $j('<div/>').text(rateOption[15]).html();   
        var disqualhtml = $j('<div>').append($j('<a>').prop('href', '#').addClass('unavailable').attr({
            'data-func' : 'f01',
            'data-arg1' : rateOption[7],
            'data-arg2' : id, 
            'data-msg' : escaped,
            'data-arg3' : currentIndex,
            'data-arg4' : linkTitle
            }).text('unavailable')).html();
         
            
        $oCell.html($oCell.html() + disqualhtml);
    }
    else {
	    if ( linkTitle != '' ) 
	    {
	        <%-- 7/28/2010 dd - OPM 42047 --%>
        var parts = linkTitle.split('/');
        var firstLink = parts[0];
            if (firstLink == '?')
            {
              $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>request lock</a>&nbsp;<a href='#' onclick='return f_ShowLockMsg();'>?</a>");
            }
            else if( firstLink === "register loan?"){
               $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>register</a>&nbsp;<a href='#' onclick='return f_SRM();'>?</a>");
            }
            else
            {
              firstLink = firstLink == "register loan" ? "register" : firstLink;
              firstLink = firstLink == "lock rate" ? "request lock" : firstLink;
              $oCell.html($oCell.html() + "<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ", false);\">" + firstLink + "</a>");
    	      }
	      if (parts.length > 1) {
	      
	      	    var secondLink = parts[1];
            if ( secondLink == '?')
            {
              $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' disabled='disabled'>request lock</a>&nbsp;<a href='#' onclick='f_ShowLockMsg()'>?</a>");
            }
            else
            {
            secondLink = secondLink == "lock rate" ? "request lock" : secondLink;
            $oCell.html($oCell.html() + "&nbsp;&nbsp;<a href='#' onclick=\"return f01('" + rateOption[7] + "','" + id + "'," + currentIndex + ", true);\">" + secondLink + "</a>");
    	      }
	      }
	    }
    }
    <% if ( m_isOptionARMUsedInPml ) { %>
    f_insertRateOptionCell($oRow, rateOption[8]); <%-- // TeaserRate; --%>
    <% } %>
    f_insertRateOptionCell($oRow, rateOption[0],bAreRatesExpired); <%-- // Rate; --%>
    f_insertRateOptionCell($oRow, rateOption[11],bAreRatesExpired); <%-- // Point display in result; --%>
    f_insertRateOptionCell($oRow, rateOption[4],bAreRatesExpired); <%-- // FirstPmtAmt; --%>
    <% if (!m_sProdDocT_NoIncome) { %>
      var violateDtiCss = rateOption[12] == 1 ? 'ViolateDti' : '';
      var violateDtiNotation = rateOption[12] == 1 ? '** ' : '';
      $oCell = f_insertRateOptionCell($oRow, violateDtiNotation + rateOption[6],bAreRatesExpired); <%-- // Bottom Ratio; --%>
      if (violateDtiCss != '')
      {
        $oCell.addClass(violateDtiCss);
      }
    <% } %>
    f_insertRateOptionCell($oRow, rateOption[2], bAreRatesExpired); <%-- //APR  --%>
    
      f_insertRateOptionCell($oRow, rateOption[17], bAreRatesExpired); <%-- // Closing costs --%>
      <% if ( displayCashToCloseAndReservesCalculation ) { %>
      f_insertRateOptionCell($oRow, rateOption[18], bAreRatesExpired); <%-- // Cash to close --%>
      <% } %>

   
      <% if (IsDisplayReserveMonths) { %>
            <% if ( displayCashToCloseAndReservesCalculation ) { %>
        f_insertRateOptionCell($oRow, rateOption[19], bAreRatesExpired); <%-- // Reserves Link --%>
        <% } else { %>
        f_insertRateOptionCell($oRow, rateOption[9], bAreRatesExpired); <%-- // Reserves --%>
      <% } %>
    <% } else { %>
        f_insertRateOptionCell($oRow, rateOption[16], bAreRatesExpired); <%-- // Break Even Point --%>
    <% } %>   
  }
  $j(oTBody).data('initialized', true)
  //oTBody.initialized = true;
}

function f_insertRateOptionCell(oRow, str, bAreRatesExpired) {
  var className = !!bAreRatesExpired ? 'rie' : 'ri';  
  var html = str == '' ? '&nbsp;' : str;  
  return $j('<td />').html(html).prop('noWrap', true).addClass(className).appendTo(oRow);
}

function f_insertRow(oTable) {
  alert('Please do not use me f_insertRow. I am no longer love.');
  var oRow = document.createElement("TR");
  oTable.appendChild(oRow);
  return oRow;
}
function f_insertCell(oRow) {
  alert('Please do not use me f_insertCell. I am no longer love.');
  var oCell = document.createElement("TD");
  oRow.appendChild(oCell);
  return oCell;
}
   var g_time2 = (new Date()).getTime();
 
</script>
    <div style="font-size: 1.2em;font-weight:bold;color:#ff9933;margin-bottom:5px;padding:3px;">
        Results
    </div>   
 
    <table width="100%" cellspacing="0" cellpadding="0"  border=0 id=<%= AspxTools.HtmlAttribute("RateMergeEligibleTable" + (m_currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan ? "0":"1")) %>>
    <tr><td colspan=<%= AspxTools.HtmlAttribute((7 + (m_isOptionARMUsedInPml ? 0 : 1 ) +  (displayCashToCloseAndReservesCalculation ? 0 : 1) ).ToString()) %>>&nbsp;</td><td colspan=<%= AspxTools.HtmlAttribute(m_showQm ? "6" : "5") %> style='padding-left:6px; color: Red; text-align:right;'>Rates shown in red are expired.</td></tr>
    <tr><td colspan=<%= AspxTools.HtmlAttribute((6 + (m_isOptionARMUsedInPml ? 0 : 1 ) +  (displayCashToCloseAndReservesCalculation ? 0 : 1) ).ToString()) %>>&nbsp;</td><td colspan=<%= AspxTools.HtmlAttribute(m_showQm ? "7" : "6") %> style='padding-left:6px; text-align:right;'>* - The costs displayed are the borrower's non-financed settlement charges.</td></tr>
    <% if (!m_sProdDocT_NoIncome) { %>
    <tr><td colspan=<%= AspxTools.HtmlAttribute((7 + (m_isOptionARMUsedInPml ? 0 : 1 ) +  (displayCashToCloseAndReservesCalculation ? 0 : 1) ).ToString()) %>>&nbsp;</td><td colspan=<%= AspxTools.HtmlAttribute(m_showQm ? "6" : "5") %> style='padding-left:6px; text-align:right;'><% if ( true) { %>** - exceeds Max DTI<% } %></td></tr>
    <% } %>
      <TR>
    <TD class=ResultProductType noWrap colSpan=4 style="PADDING-RIGHT:20px"><% if (m_isLpeDisqualificationEnabled) { %>                        Eligible Loan Programs<% } %></TD>
    <% if (m_isOptionARMUsedInPml ) { %>
    <TD class="RateItemHeader">TEASER RATE (<A onclick="f_openHelp('Q00008.html', 400, 200);" tabIndex=-1 href="#" >?</A>)</TD>
    <% } %>
    <TD class="RateItemHeader">RATE</TD>
    <TD class="RateItemHeader"><% if (m_displayPmlFeeIn100Format) { %>                        PRICE<% } else { %>                          POINT<% } %></TD>
    <TD class="RateItemHeader">PAYMENT</TD>
    <% if ( ! m_sProdDocT_NoIncome ) { %> 
    <td class="RateItemHeader">DTI</td>
    <% } %>
    <td class="RateItemHeader">APR</td>
    
    <% if (m_showQm)
       { %>
     <td class="RateItemHeader">QM</td>
    <% } %>
    <td class="RateItemHeader">CLOSING<br/>COSTS</td>
    <% if (displayCashToCloseAndReservesCalculation) { %>
    <td class="RateItemHeader">CASH TO<br />CLOSE</td>
    <% }%>
    <% if (IsDisplayReserveMonths) { %>
    <td class="RateItemHeader">RESERVE</br>MONTHS</td>
    <% } else { %>
    <td class="RateItemHeader">BREAK EVEN<br/> MONTHS</td>
    <% } %>
    <td class="RateItemHeader" nowrap></td>
    </TR>
  <TR>
    <TD bgColor=#000000 colSpan=<%= AspxTools.HtmlAttribute((13 + ( displayCashToCloseAndReservesCalculation ? 1 : 0 ) + ( m_showQm ? 1 : 0 ) - ( m_sProdDocT_NoIncome ? 1 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>'><IMG height=1 src="../images/spacer.gif" 
      width=100></TD></TR>
 </table>
   

    <BR>
<HR align=left color=#ff9933 noShade SIZE=1>
<BR>
<TABLE cellSpacing=0 cellPadding=0 border=0>
  <TBODY>
    <tr>
      <td class="ResultProductType">Loan Programs Requiring Additional Borrower Info</td>
    </tr>
    <TR>
    <TD><asp:repeater id=m_insufficientRepeater runat="server" enableviewstate="false">
						<headertemplate>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="3" bgcolor="#000000"><img src="../images/spacer.gif" width="100" height="1"></td>
								</tr>
						</headertemplate>
						<itemtemplate>
							<tr class="InsufficientItemBackground">
								<td colspan="3" class="ResultProductName"><%# AspxTools.HtmlString(Eval("lLpTemplateNm").ToString())%></td>
							</tr>
							<tr class="InsufficientItemBackground">
								<td class="RateItem_ColA" valign="top">&nbsp;</td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlString(Eval("Errors").ToString())%></td>
							</tr>
						</itemtemplate>
						<alternatingitemtemplate>
							<tr class="InsufficientAlternatingItemBackground">
								<td colspan="3" class="ResultProductName"><%# AspxTools.HtmlString(Eval("lLpTemplateNm").ToString())%></td>
							</tr>
							<tr class="InsufficientAlternatingItemBackground">
								<td class="RateItem_ColA" valign="top">&nbsp;</td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlString(Eval("Errors").ToString())%></td>
							</tr>
						</alternatingitemtemplate>
						<footertemplate>
	</TABLE>
	<table border="0" cellpadding="0" cellspacing="0" bgcolor="#000000" width="100%">
		<tr>
			<td bgcolor="#999999"><img src="../images/spacer.gif" width="100%" height="1"></td>
		</tr>
	</table>
	</footertemplate> </asp:repeater></td></tr>
    <tr>
      <td align="center" style="font-weight:bold"><ml:EncodedLiteral ID="m_insufficientNotFoundLiteral" runat="server">None</ml:EncodedLiteral></td>
    </tr>
</table>

      <% if (m_isLpeDisqualificationEnabled) { // OPM 16132 %>
      <!-- Allow User to submit ineligible loan product start here --><% if (m_showRateForIneligible) { %><BR>
<HR align=left color=#ff9933 noShade SIZE=1>
<BR>
<div class="ResultProductType">
    <a href="javascript:void(0)" class="ineligibleLoanProgramsLink">
    <span class="ineligibleLoanProgramsHideText">- Hide</span>
    <span class="ineligibleLoanProgramsDisplayText">+ Display</span> Ineligible Loan Programs
    </a>
</div>
<TABLE width="100%" class="ineligibleLoanProgramsTD" cellSpacing=0 cellPadding=0 border=0>
    <tr><td colspan=<%= AspxTools.HtmlAttribute(m_isOptionARMUsedInPml ? "3" : "2") %>>&nbsp;</td><td colspan=9 style='padding-left:6px; color: Red; text-align:right;'>Rates shown in red are expired.</td></tr>
    <tr><td colspan=<%= AspxTools.HtmlAttribute(m_isOptionARMUsedInPml ? "3" : "2") %>>&nbsp;</td><td colspan=9 style='padding-left:6px; text-align:right;'>* - The costs displayed are the borrower's non-financed settlement charges.</td></tr>
    <% if (!m_sProdDocT_NoIncome) { %>
    <tr><td colspan=<%= AspxTools.HtmlAttribute(m_isOptionARMUsedInPml ? "3" : "2") %>>&nbsp;</td><td colspan=9 style='padding-left:6px; text-align:right;' nowrap><% if ( m_showMaxDtiWarning) { %>** - exceeds Max DTI<% } %></td></tr>
    <% } %>
  <TR>
    <%--<TD class="RateItemHeader" noWrap colSpan=2></TD>--%>
    <TD class="RateItemHeader" colspan=3>&nbsp;&nbsp;&nbsp;</TD>
    <% if (m_isOptionARMUsedInPml ) { %>
    <TD class="RateItemHeader">TEASER RATE</TD>
    <% } %>
    <TD class="RateItemHeader">RATE</TD>
    <TD class="RateItemHeader"><% if (m_displayPmlFeeIn100Format) { %>                        PRICE<% } else { %>                          POINT<% } %></TD>
    <TD class="RateItemHeader">PAYMENT</TD>
    <% if ( ! m_sProdDocT_NoIncome ) { %> 
    <TD class="RateItemHeader">DTI</TD>
    <% } %>
    <TD class="RateItemHeader">APR</TD>
    <TD class="RateItemHeader">CLOSING<br/>COSTS</TD>
    <% if (displayCashToCloseAndReservesCalculation) { %>
    <td class="RateItemHeader">CASH TO<br />CLOSE</td>
    <% }%>
    <% if (IsDisplayReserveMonths) { %>
    <TD class="RateItemHeader">RESERVE<br/>MONTHS</TD>
    <% } else { %>
    <TD class="RateItemHeader">BREAK EVEN<br/> MONTHS</TD>
    <% } %>

    <td></td>
    
    </TR>
  <TR>
    <TD bgColor=#000000 colSpan=<%= AspxTools.HtmlAttribute((12 - (displayCashToCloseAndReservesCalculation ? 0 : 1) - ( m_sProdDocT_NoIncome ? 1 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>>
    <IMG height=1 src="../images/spacer.gif" width=100></TD></TR><asp:PlaceHolder id=m_ineligibleRatePlaceHolder runat="server"></asp:PlaceHolder>
  <TR>
    <TD style="font-weight:bold" align=center colSpan=<%= AspxTools.HtmlAttribute((12 - (displayCashToCloseAndReservesCalculation ? 0 : 1) - ( m_sProdDocT_NoIncome ? 1 : 0 ) - (m_isOptionARMUsedInPml ? 0 : 1)).ToString()) %>>
    <ml:EncodedLiteral id=m_ineligibleRateNotFoundLiteral runat="server">None</ml:EncodedLiteral></TD>
    </TR>
    </TABLE>
    <% } else { %><!-- Allow User to submit ineligible loan product end here --><BR>
<HR style="width:100%" align=left color=#ff9933 noShade SIZE=1>
<BR>
<TABLE style="width:100%" cellSpacing=0 cellPadding=0 border=0>
  <TBODY>
  <TR>
    <TD class="ResultProductType">
        <a href="javascript:void(0)" class="ineligibleLoanProgramsLink">
        <span class="ineligibleLoanProgramsHideText">- Hide</span>
        <span class="ineligibleLoanProgramsDisplayText">+ Display</span> Ineligible Loan Programs
        </a>
    </TD></TR>
  <TR>
    <TD class="ineligibleLoanProgramsTD"><asp:repeater id=m_ineligibleRepeater runat="server" enableviewstate="false">
						<headertemplate>
							<table style="width:100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="3" bgcolor="#000000"><img src="../images/spacer.gif" width="100" height="1"></td>
								</tr>
						</headertemplate>
						<itemtemplate>
							<tr class="IneBg">
								<td class="ResultProductName" colspan="3"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateNm").ToString()) %>&nbsp;&nbsp;
								</td>
							</tr>
							<tr class="IneBg">
								<td class="RateItem_ColA" valign="top">&nbsp;<a href='#' onclick="return f00(<%# AspxTools.JsString(Eval("TempIndex").ToString()) %>);">preview</a></td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlControl(CreateDisqualifiedRules(Eval("DisqualifiedRuleList")))%>
								</td>
							</tr>
						</itemtemplate>
						<alternatingitemtemplate>
							<tr class="IneAltBg">
								<td class="ResultProductName" colspan="3"><%# AspxTools.HtmlString(Eval("lLpTemplateNm").ToString()) %>&nbsp;&nbsp;
								</td>
							</tr>
							<tr class="IneAltBg">
								<td class="RateItem_ColA" valign="top">&nbsp;<a href='#' onclick="return f00(<%# AspxTools.JsString(Eval("TempIndex").ToString()) %>);">preview</a></td>
								<td width="11px" class="LeftBorder">&nbsp;</td>
								<td height="20px"><%# AspxTools.HtmlControl(CreateDisqualifiedRules(Eval("DisqualifiedRuleList")))%>
								</td>
							</tr>
						</alternatingitemtemplate>
						<footertemplate>
	</TABLE>
	<table style="width:100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#000000">
		<tr>
			<td bgcolor="#999999"><img src="../images/spacer.gif" width="100%" height="1"></td>
		</tr>
	</table>
	</footertemplate> </asp:repeater></TD></TR>
    <tr>
      <td align="center" style="font-weight:bold"><ml:EncodedLiteral ID="m_ineligibleNotFoundLiteral" runat="server">None</ml:EncodedLiteral></td>
    </tr>
  </TBODY>
  </TABLE>
    <% } %><% } // if (m_isLpeDisqualificationEnabled ) %>

<% if (m_has2ndLoan && m_currentLienQualifyModeT == E_sLienQualifyModeT.OtherLoan) { %>
<TABLE style="width:700" cellSpacing=0 cellPadding=0 border=0>
    <% if (!m_isRerunMode) { %>
    <TR>
        <TD>&nbsp;</TD></TR>
        <% if (m_isLpeManualSubmissionAllowed) { %>    
            <TR><TD valign=middle align=center><A onclick=f_submitManual2nd(); href="#">Please click here to submit 2nd loan for an exception.</A></TD></TR>
        <% } %>
    <% } %>
    
    <% if (!m_isRerunMode || m_isSkip2ndLoan || (m_isRerunMode && !m_eligibleLoanProgramsExist)) { %>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="middle" align="center" height="20"><a onclick="return submitSkip2nd();" href="#">If seller or other lender provides 2nd financing, click here to submit.</a></td>
    </tr>
    <% } %>
    
</TABLE><% } %>
    </asp:Panel>
</span>

<span id="ErrorMessagePanel" style="DISPLAY: none">
  <table height="100%" width="100%">
    <tr>
      <td style="FONT-WEIGHT: bold; COLOR: red" valign="middle" align="center">
        <div id="ErrorMessage"></div>
        <a ID="ErrorReasons" href="#" onclick="return f_step4Circumstance_onclick();">Click here for more information</a>
      </td>
    </tr>
  </table>
</span>

    </td></tr></tbody></table>
<div id="DebugTimingPanel" style="border:solid 2px black;padding: 5px; DISPLAY:none; COLOR:black; POSITION:absolute; BACKGROUND-COLOR:white"></div>
<div id="RateWarningDiv" style="BORDER-RIGHT:black 2px solid; PADDING-RIGHT:5px; BORDER-TOP:black 2px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 2px solid; COLOR:black; PADDING-TOP:5px; BORDER-BOTTOM:black 2px solid; POSITION:absolute; BACKGROUND-COLOR:white;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:x-small"></div>

<div class="qualifying-borrower-container">
    <uc1:QualifyingBorrower runat="server" ID="QualifyingBorrower" ></uc1:QualifyingBorrower>
</div>