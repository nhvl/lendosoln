﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FriendlyAccessDenial.aspx.cs" Inherits="PriceMyLoan.main.FriendlyAccessDenial" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Access Denied</title>
</head>
<body>
    <link href="../css/style.css"  type="text/css" rel="Stylesheet"/>
    <script type="text/javascript">
            
        $(document).ready(function(){
            var dontUnload = false;
            function closeWindow(){
                window.setTimeout("window.close()", 1000);
            }
            $('#logout').click(function(){
                dontUnload = true;
                if( parent.f_logoutParent ) {
                    f_logoutParent();
                }
                var arg = window.dialogArguments;
                if(arg){
                    args.Logout = '1';
                    closeWindow();
                }
                else if( window.opener && window.opener.f_logout ) {
                    window.opener.f_logout();
                    closeWindow();
                }
                
                
            });
            $(window).on(".unload", function(){
                debugger;
                if( dontUnload ) {
                    return;
                }
                var arg = window.dialogArguments;
                if(arg) { 
                    arg.gotopipeline = true;
                    var opener = arg.opener;
                    if( opener && opener.f_goToPipeline){
                        opener.f_goToPipeline();
                        closeWindow();
                        return;
                    }   
                }
    
                if( !!window.opener && !!window.opener.f_goToPipeline) {
                    window.opener.f_goToPipeline(false);
                    closeWindow();
                    return;
                }
                
                if( top && top.opener && top.opener.f_goToPipeline ) {
                    top.opener.f_goToPipeline();
                }
                
            });
     
            var showLogout = frames && top.frames["ModalFrame"];
            $('#BtnRow').toggle(showLogout);
            
        });
    </script>

    <form id="form1" runat="server">
    <div>
        <table width="100%" height="100%">
            <tr id="BtnRow" runat="server" >
                <td align="right">
                    <input type="button" class="ButtonStyle" id="logout" value="LOG OFF">
                </td>
            </tr>
            <tr>
                <td valign="center" align="center" style="font-weight: bold; font-size: 18px">
                    <ml:EncodedLiteral runat="server" ID="ErrorMessage" Text="You no longer have access to this loan."></ml:EncodedLiteral>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
