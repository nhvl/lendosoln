﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LockRate.aspx.cs" Inherits="PriceMyLoan.main.LockRate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        #PricingTableErrorDiv, #FloatDownMsgSpan { display: none; }

        .submitted .current {
            display: none;
        }

        .initial-table:not(.submitted) .prior, .initial-table:not(.submitted) .new-container {
            display: none;
        }

        #ConfirmationTable .text-right, #ConfirmationTable text-left {
            width: 50%;
        }

        #PricingTable th.caption {
            padding-top: 3px;
            padding-bottom: 3px;
        }
    </style>
</head>
<body style="OVERFLOW-Y: auto;" >
    <div id="paddingBox">
        
        <form id="LockRate" runat="server">
        <div class="modal-header">
            
            <h4 class="modal-title"><ml:EncodedLiteral id="m_Title" runat="server" /></h4>
        </div>
        <div class="modal-body">
            <div class="table center-div initial-table">
                <div>
                    <div class="text-right text-grey">Loan Number</div>
                    <div><ml:EncodedLiteral id="m_LoanNumber" runat="server" /></div>
                </div>
                <div>
                    <div class="text-right text-grey">Primary Applicant</div>
                    <div><ml:EncodedLiteral id="m_Applicant" runat="server" /></div>
                </div>
                <asp:PlaceHolder id="m_relockRow" runat="server">
                    <div>
                        <div class="text-right text-grey">Lock Expiration Date</div>
                        <div><ml:EncodedLiteral id="m_lockExpirationDate" runat="server" /></div>
                    </div>
                    <div>
                        <div class="text-right text-grey">Re-Lock Period</div>
                        <div>
                            <select id="m_lockDays" runat="server" onchange="onChangeLockPeriod()" />&nbsp;days
                            <button type="button" class="btn btn-default left-margin" id="priceBtn" onclick="priceLoan()" >Price</button>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder id="m_lockDatesRow" runat="server">
                    <div>
                        <div class="text-right text-grey">Original Lock Expiration Date</div>
                        <div><ml:EncodedLiteral id="m_OriginalLockExpirationDate" runat="server" /></div>
                    </div>
                    <div>
                        <div class="text-right text-grey"><span class="current">Current</span><span class="prior">Prior</span> Lock Expiration Date</div>
                        <div><ml:EncodedLiteral id="m_CurrentLockExpirationDate" runat="server" /></div>
                    </div>
                    <div class="new-container">
                        <div class="text-right text-grey">New Lock Expiration Date</div>
                        <div id="NewLockExpirationDate"></div>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder id="m_lockedRatesRow" runat="server">
                    <div>
                        <div class="text-right text-grey">Currently Locked Rate</div>
                        <div><ml:EncodedLiteral id="m_rowCurrentlyLockedRate" runat="server" /></div>
                    </div>
                    <div>
                        <div class="text-right text-grey"><span class="current">Currently</span><span class="prior">Prior</span> Locked <%= AspxTools.HtmlString( m_pointsPrice ) %></div>
                        <div><ml:EncodedLiteral id="m_rowCurrentlyLockedPoints" runat="server" /></div>
                    </div>
                </asp:PlaceHolder>
                <div>
                    <div class="text-right text-grey">Number of Times This File Has<br /><ml:EncodedLiteral id="m_timesEventDesc" runat="server" /></div>
                    <div id="NumTimesActionPerformed"><ml:EncodedLiteral id="m_timesEventOccured" runat="server" /></div>
                </div>
                <asp:PlaceHolder id="m_extentionDaysRow" runat="server">
                <div>
                    <div class="text-right text-grey">Total Extension Previously Applied To This Loan File &nbsp;<ml:EncodedLiteral id="Literal1" runat="server" /></div>
                    <div><ml:EncodedLiteral id="m_extensionDays" runat="server" /></div>
                </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder id="m_reasonRow" runat="server">
                    <div>
                        <div class="text-right text-grey">Reason For&nbsp;<ml:EncodedLiteral id="m_reasonDesc" runat="server" /> <span class="text-danger">*</span></div>
                        <div class="width-half"><input class="width-full" id="reason" type="text" /></div>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder id="m_PricingTableDiv" runat="server">
                    <div>
                        <div class="text-right text-grey"><span class="current">Currently</span> Locked Rate</div>
                        <div><ml:EncodedLiteral id="m_currentlyLockedRate" runat="server" /></div>
                    </div>
                    <div>
                        <div class="text-right text-grey"><span class="current">Currently</span><span class="prior">Prior</span> Locked <%= AspxTools.HtmlString( m_pointsPrice ) %></div>
                        <div><ml:EncodedLiteral id="m_currentlyLockedPoints" runat="server" /></div>
                    </div>
                    <div class="new-container">
                        <div class="text-right text-grey">New Locked <%= AspxTools.HtmlString( m_pointsPrice ) %></div>
                        <div id="NewLockedPoints"></div>
                    </div>
                </asp:PlaceHolder>
            </div>
            <div class="text-center" id="PricingTableErrorDiv">
                <div id='WaitImg' style="display: none"><span id='PricingTableMsg'></span></div>
                <span class="text-danger " id='PricingTableError'></span>
            </div>
            <div id="PricingTable"></div>

            <asp:PlaceHolder id="m_floatDownMsg" runat="server">
                <span id="FloatDownMsgSpan" class="text-danger padding-bottom-1em">Note: A rate lock float down will not change the current lock expiration date for this loan.</span>
            </asp:PlaceHolder>
            <asp:PlaceHolder id="m_expirationDateSpan" runat="server">
                <div class="padding-top-h1em">
                    <div class="table">
                        <div>
                            <div class="text-grey">Expiration Date</div>
                            <div><ml:EncodedLiteral id="m_expirationDate" runat="server"></ml:EncodedLiteral></div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <span id="ExpirationDateRelockLabel" class="text-grey"></span><span id="ExpirationDateRelock"  ></span>
            <div id="ConfirmationTable" class="none-display table center-div"></div>
        </div>
        </form>
        <div class="modal-footer">
            <button type="button" class="btn btn-flat" onclick="parent.LQBPopup.Hide();">Cancel</button>
        </div>
            <!-- very similar code exists in los/RateLock/LockRate.aspx -->
            <asp:PlaceHolder runat="server" id="m_extendScript">
                <script type="text/javascript">
                    var gRunPricingOnInit = true;
                    var gWaitMsg = "Please wait while your lock extension options are generated.";
                    
                    function getActionNode(tableRow, showCalendarDays)
                    {
                        var actionStr = showCalendarDays ? tableRow[5] : tableRow[4];
                        var node = document.createElement('a');
                        if (showCalendarDays && tableRow[4] === "0 days") {
                            setDisabledAttr(node, true);
                            node.title = "This option does not extend the Lock Expiration Date";
                        }
                        else if (showCalendarDays && actionStr == "disabled") {
                            setDisabledAttr(node, true);
                            node.title = "This option is unavailable because there is a better option which results in the same Lock Expiration Date.";
                        }
                        else {
                            node.href = "javascript:extend('" + actionStr + "')";
                        }
                        node.appendChild(document.createTextNode('extend'));

                        return node;
                    }

                    function extend(extention) {
                    
                        if ( document.getElementById('reason').value == '' )
                        {
                            alert('Please enter an explanation for why a lock extension is needed.' );
                            return;
                        }
                        
                        var arguments = extention.split(":");
                        
                        var args = new Object();
                        args['lockAction'] = arguments[0];
                        args['lockArguments'] = extention.replace(arguments[0] + ":", "") + ":" + document.getElementById('reason').value;
                        
                        priceLoan( args );
                        
                    }
                    function buildConfirmationTable( result )
                    {
                        var confirmationTable = $('.initial-table');
                        confirmationTable.removeClass("none-display");
                        confirmationTable.addClass("submitted");

                        var confirmationCell = $('<div></div>')
                            .append($('<div></div>').text('Your rate lock extension has been approved.').addClass('text-success'));

                        confirmationTable.prepend(confirmationCell);


                        $("#NewLockExpirationDate").text(result['newexpiration']);
                        $("#NewLockedPoints").text(result['newprice']);
                        $("#NumTimesActionPerformed").text(result['totalevents']);
                        $("#reason").prop("readonly", true);

                        $("#LockRate").next(".modal-footer").find("button").text("Close");

                        refreshPipeline( <%= AspxTools.JsString( m_loanId ) %>, result['newexpiration']);
                    }
                    function tableBuiltCallback( bIsVisible )
                    {
                    }
                </script>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" id="m_floatDownScript">
                <script type="text/javascript">
                    var gRunPricingOnInit = true;
                    var gWaitMsg = "Please wait while your Float Down options are generated.";

                    function getActionNode(tableRow, showCalendarDays) {
                        var actionStr = tableRow[6];
                        var node = document.createElement('a');
                        node.href = "javascript:floatDown('" + actionStr + "')";
                        node.appendChild(document.createTextNode(actionStr.split(":")[0]));

                        return node;
                    }

                    function floatDown(args) {
                        var arguments = args.split(":");
                        if (arguments[0] == 'unavailable') {
                            var reasons = '';
                            for (var i = 1; i < arguments.length; i++) {
                                reasons += " " + arguments[i];
                            }
                            alert(reasons);
                        }
                        else {
                            floatDownImpl( args );
                        }
                    }

                    function floatDownImpl( arguments ) {
                    
                        var floatDownArgs = arguments.split(":");

                        if (floatDownArgs.length == 0) {
                            return;
                        }
                        
                        var args = new Object();
                        args['lockAction'] = floatDownArgs[0];
                        args['lockArguments'] = arguments.replace(floatDownArgs[0] + ":", "");

                        priceLoan(args);
                    }
                    
                    function addRow(table, label, value)
                    {
                        var row = $("<div></div>").append($("<div class='text-right text-grey'></div>").text(label)).append($("<div class='text-left'></div>").text(value));
                        table.append(row);
                    }

                    function buildConfirmationTable( result )
                    {
                        var confirmationTable = $('#ConfirmationTable');
                        confirmationTable.removeClass('none-display');

                        var confirmationCell = $('<div></div>')
                            .append($('<div></div>').text('Your rate lock float down for this loan has been approved.').addClass('text-success'));

                        confirmationTable.append(confirmationCell);

                        addRow(confirmationTable, 'Loan Number ', <%= AspxTools.JsString( m_LoanNumber.Text ) %>);
                        addRow(confirmationTable, 'Primary Applicant ', <%= AspxTools.JsString( m_Applicant.Text ) %>);

                        addRow(confirmationTable, 'Prior Locked Rate ', <%= AspxTools.JsString(  m_rowCurrentlyLockedRate.Text ) %>);
                        addRow(confirmationTable, 'Prior Locked <%= AspxTools.HtmlString( m_pointsPrice ) %> ', <%= AspxTools.JsString(  m_rowCurrentlyLockedPoints.Text ) %>);

                        addRow(confirmationTable, 'New Locked Rate ', result['newrate']);
                        addRow(confirmationTable, 'New Locked <%= AspxTools.HtmlString( m_pointsPrice ) %> ', result['newprice']);

                        confirmationTable.append($("<div></div>").append($("<div class='text-right text-grey'>Number of Times This File Has <br/> Had A Rate Lock Float Down </div>")).append($("<div class='text-left'></div>").text(result['totalevents'])));
                        addRow(confirmationTable, 'Lock Expiration Date ', result['newexpiration']);
                        parent.LQBPopup.AutoResizeIframeModal();
                        $("#LockRate").next(".modal-footer").find("button").text("Close");
                    }

                    function tableBuiltCallback( bIsVisible )
                    {
                        var tableMsg = document.getElementById('FloatDownMsgSpan');
                        if (tableMsg != null)
                            tableMsg.style.display = bIsVisible ? 'inline': 'none';
                    }
                </script>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" id="m_relockScript">
                <script type="text/javascript">
                    var gRunPricingOnInit = false;
                    var gWaitMsg = "Please wait while your re-lock options are generated.";

                    function getActionNode(tableRow, showCalendarDays) {
                        var actionStr = tableRow[5];
                        var node = document.createElement('a');
                        node.href = "javascript:relock('" + actionStr + "')";
                        node.appendChild(document.createTextNode('re-lock'));

                        return node;
                    }

                    function relock(argStr) {
                    
                        if ( document.getElementById('PricingTable').disabled ) return;

                        if (document.getElementById('reason').value == '') {
                            alert('Please enter an explanation for why a relock is needed.');
                            return;
                        }

                        parent.LQBPopup.showWaitTable(true);
                        
                        setTimeout(function(){
                            var arguments = argStr.split(":");

                            var args = new Object();
                            args['lockAction'] = arguments[0];
                            args['lockArguments'] = argStr.replace(arguments[0] + ":", "") + ":" + document.getElementById('reason').value;

                            priceLoan(args);
                        }, 0);
                    }
                    
                    
                    function buildConfirmationTable( result )
                    {
                        var confirmationTable = $('#ConfirmationTable');
                        confirmationTable.parent("")
                        confirmationTable.addClass("text-grey");
                        confirmationTable.removeClass('none-display');
                        confirmationTable.before($('<div></div>').text('Your relock for this loan has been approved.').addClass('text-success'));

                        var loanInformationRow = $('<div></div>')
                            .append($('<div class="text-right text-grey"></div>').text('Loan Number'))
                            .append($('<div class="text-left"></div>').text(<%= AspxTools.JsString( m_LoanNumber.Text ) %>));
                        var applicantRow = $("<div></div>")
                            .append($('<div class="text-right text-grey"></div>').text('Primary Applicant'))
                            .append($('<div class="text-left"></div>').text(<%= AspxTools.JsString( m_Applicant.Text ) %>));

                        confirmationTable.append(loanInformationRow);
                        confirmationTable.append(applicantRow);

                        var lockInformationRow = $('<div></div>')
                            .append($('<div class="text-right text-grey"></div>').text('Re-Locked Rate '))
                            .append($('<div class="text-left"></div>').text(result['newrate']));
                        var relockPriceRow = $("<div></div>")
                            .append($('<div class="text-right text-grey"></div>').text('Re-Locked <%= AspxTools.HtmlString( m_pointsPrice ) %> '))
                            .append($('<div class="text-left"></div>').text(result['newprice']));

                        confirmationTable.append(lockInformationRow);
                        confirmationTable.append(relockPriceRow);

                        var lockExpirationRow = $('<div></div>')
                            .append($('<div class="text-right text-grey"></div>').text('Re-Locked Expiration Date'))
                            .append($('<div class="text-left"></div>').text(result['newexpiration']));

                        confirmationTable.append(lockExpirationRow);

                        var closeButton = $('<button></button>')
                            .addClass('btn btn-flat')
                            .text('Close')
                            .click(function() { closeLockRatePopup(false); });


                        var numberOfRelocksRow = $('<div></div>')
                            .append($('<div class="text-right text-grey"></div>').text('Number of Times This File Has Been Re-Locked '))
                            .append($('<div class="text-left"></div>').text(result['totalevents']));


                        confirmationTable.append(numberOfRelocksRow);
                        $("#LockRate").next(".modal-footer").find("button").text("Close");
                        refreshPipeline( <%= AspxTools.JsString( m_loanId ) %>, result['newexpiration']);
                        parent.LQBPopup.AutoResizeIframeModal();
                    }
                    
                    function tableBuiltCallback( bIsVisible )
                    {
                        var lockDates = <%=AspxTools.JsArray(m_newExpirationDates) %>;
                        if ( document.getElementById('m_lockDays') != null )
                        {
                            document.getElementById('ExpirationDateRelockLabel').innerText = bIsVisible ?  'New Lock Expiration Date: ' : '';
                            document.getElementById('ExpirationDateRelock').innerText = bIsVisible ? lockDates[document.getElementById('m_lockDays').selectedIndex] : '';
                        }
                        
                        disableTable( bIsVisible );
                        
                        var priceBtn = document.getElementById('priceBtn');
                        if ( priceBtn != null )
                        {
                            priceBtn.disabled =  bIsVisible ? '' : 'disabled';
                        }
                        
                    }
                    
                    function onChangeLockPeriod()
                    {
                        disableTable( false );
                    }

                </script>
            </asp:PlaceHolder>
        
        
            <script type="text/javascript">
            function _init() {
                TPOStyleUnification.Components();
                TPOStyleUnification.wrapByLoadingContent($("#WaitImg"));     
                if (!$("#paddingBox").parent(".modal-content").length && window.self == window.top){
                    $("#paddingBox").addClass("padding-32");
                }

                if( gRunPricingOnInit )
                {
                    parent.LQBPopup.showWaitTable(true);
                    setWaitMsg(gWaitMsg);
                    setTimeout(function() { priceLoan(null); }, 800);
                }

                if (typeof window.parent !== 'undefined' &&
                    typeof window.parent.ratePopupAlert !== 'undefined') {
                    window.alert = window.parent.ratePopupAlert;
                }
            }
            
            var oldWindowAlert = window.alert;
            var pollInterval;
            var pollCount = 0;

            function priceLoan( actionArgs ) {
                var isForAction = actionArgs != null;
                var days = null;
                var args = new Object();
                if ( document.getElementById('m_lockDays') != null )
                {
                    days = document.getElementById('m_lockDays').value;
                    if ( days == '')
                    {
                        alert('Please choose a lock days value.');
                        return;
                    }
                    
                    args['lockdays'] = days;
                }

                parent.LQBPopup.showWaitTable(true);

                setTimeout(function(){                
                setWaitMsg(isForAction ? 'Please Wait.' : gWaitMsg);

                document.getElementById('PricingTable').innerHTML = '';
                tableBuiltCallback( false );
                
                if ( isForAction )
                    document.getElementById('PricingTable').style.display = 'none';
            
                
                var result = executeService("CreatePriceRequest", args);
                if (isError) return;            
                
                pollCount = 0; 
                if ( result && result.value && result.value['requestid'])
                {
                    var requestId = result.value['requestid'];
                    
                    args = (actionArgs == null ) ? new Object(): actionArgs;
                    
                    args['requestid'] = requestId;
                    if ( days != null)
                        args['lockdays'] = days;
                    
                    pollInterval = setInterval(function() { getResult( args, isForAction ); }, 3000);
                    
                }
                
                }, 0);
            }
            
            
            function getResult( args, bIsForAction )
            {
                pollCount++;
                
                var result = executeService( 'GetResultIfReady', args );
                if (isError) return;
                            
                if ( result && result.value && result.value['isdone'] === 'True')
                {
                    clearInterval(pollInterval);
                    setWaitMsg('');
                   
                    if ( bIsForAction )
                    {
                        $(".table").addClass('none-display');
                        buildConfirmationTable( result.value );
                        parent.LQBPopup.showWaitTable(false);
                    } 
                    else
                    {
                        buildTable(result.value['TableJSON']);
                        $("#PricingTable").show();
                        $("#PricingTableErrorDiv").hide();
                        TPOStyleUnification.fixedHeaderTable( $("#PricingTable")[0]);
                        tableBuiltCallback( true );
                    }

                    parent.LQBPopup.AutoResizeIframeModal();
                    parent.LQBPopup.showWaitTable(false);
                }
                
            
                if ( pollCount >=  5)
                {
                    clearInterval(pollInterval);
                    parent.LQBPopup.showWaitTable(false);
                    alert ('Unable to get results');
                }
                
            }
            
            
            function executeService( service, args )
            {
                args['loanid'] = <%= AspxTools.JsString(m_loanId.ToString()) %>;
                args['mode'] = <%= AspxTools.JsString( m_mode.ToString("D")) %>;
                
                var result = gService.lockrate.call(service, args, false, false, true);
                if ( !result.error )
                {
                    if (result.value['displayerror'])
                    {
                        parent.LQBPopup.showWaitTable(false);
                        displayError( result.value['displayerror']);
                    }
                    
                    return result;
                }            
                else
                {
                    parent.LQBPopup.showWaitTable(false);
                    if ( result.UserMessage != null )
                        displayError(result.UserMessage);
                    else
                        if ( result.value['UserMessage'] )
                            displayError(result.value['UserMessage']);
                    return null;
                }
                
            }
            
            var isError = false;
            function displayError( error )
            {
                isError = true;
                setWaitMsg('');
                document.getElementById('PricingTableError').innerText = error;
                $("#PricingTableErrorDiv").show();
                $("#PricingTable").hide();
                tableBuiltCallback( false );
                parent.LQBPopup.AutoResizeIframeModal();
                
                if (pollInterval)
                    clearInterval(pollInterval);
            }
            
            const maxInlineHeaderLength = 24;
            function buildTable(jsonContent) {
                var tableContent = $.parseJSON(jsonContent);
                var parentNode = document.getElementById('PricingTable');
                var table = document.createElement('table');
                $(table).attr("width", "500px");
                table.className = "DataGrid modal-table-scrollable wide table-lock-rate flex-table minimum";


                var header;
                if (tableContent.length > 0){
                    header = document.createElement('thead');
                    tr = document.createElement('tr');
                    tr.className = "header";
                    
                    for (var j = 0; j < tableContent[0].length; j++) {
                        th = document.createElement('th');

                        if(j == tableContent[0].length -1)
                        {
                            var flexPadding = $("<th class='flex-padding'></th>");
                            tr.appendChild(flexPadding[0]);
                            th.className +=  ' text-left flex-static'; 
                        }
                        else {
                            th.className +=  ' text-right';
                        }

                        var thText = tableContent[0][j].toString();

                        if(thText.indexOf("<br/>") != -1)
                        {
                            th.className += " caption";
                        }

                        th.className += " middle-align-inline-content";

                        $(th).css("width", "auto");

                        var thElement = document.createElement("span");
                
                        thElement.innerHTML = thText;
                        th.appendChild(thElement);
                        tr.appendChild(th);
                    }


                    header.appendChild(tr);
                }
                
                table.appendChild(header);

                var body = document.createElement('tbody');
                var distinctCalendarDays = {};
                
                var tr, td;
                for (var i = 1; i < tableContent.length; i++) {
                    tr = document.createElement('tr');
                    
                    tr.className = 'GridAutoItem';
                    
                    for (var j = 0; j < tableContent[i].length; j++) {
                        td = document.createElement('td');
                        if ( j == tableContent[i].length -1 && i != 0 )
                        {
                            var flexPadding = $("<td class='flex-padding'></td>");
                            tr.appendChild(flexPadding[0]);
                            td.appendChild(getActionNode(tableContent[i], ML.showCalendarDays));
                            td.className += ' text-left flex-static';
                        }
                        else
                        {
                            td.appendChild(document.createTextNode(tableContent[i][j]));
                            td.className += ' text-right';
                        }

                        tr.appendChild(td);
                    }
                    body.appendChild(tr);
                }

                table.appendChild(body);
                parentNode.appendChild(table);

                if(body.scrollHeight > $(body).innerHeight())
                {
                    $(table).addClass("has-scroll");
                }

                setFlexBasis("#PricingTable table", [], 16, 14);
                $(table).removeClass("minimum");
            }

            function setFlexBasis(tableSelector, currentBasis, fontSizeTd, fontSizeTh)
            {
                var table = $(tableSelector);
                var rows = table.find("tr")

                for(i = 0; i < rows.length; i++)
                {
                    var cells = $(rows[i]).find("td, th");
                    for(j = 0; j < cells.length; j++)
                    {
                        var $cell = $(cells[j]);

                        if(currentBasis[j] == undefined || currentBasis[j] < $cell.innerWidth())
                        {
                            currentBasis[j] = $cell.innerWidth();
                        }
                    }
                }

                // Remove/Re-Create the old style rule 
                $("head .pricing-col-basis").remove();
                var styleText = "<style class='.pricing-col-basis'>";
                for(i = 0; i < currentBasis.length; i++)
                {
                    styleText += tableSelector + " td:nth-child(" + (i+1) + "), "+ tableSelector +"  th:nth-child(" + (i+1) + ") {flex-basis: " + currentBasis[i] + "px; -ms-flex: 1 0 " + currentBasis[i] + "px;} ";
                }
                styleText += "</style>";

                $("head").append($(styleText));
            }
            
            function buildCell(label, content, cssClass)
            {
                var td = document.createElement('td');
                var labelSpan = document.createElement('span');
                labelSpan.className = 'FieldLabel';
                labelSpan.style.paddingRight = '3px';
                labelSpan.appendChild(document.createTextNode(label));
                td.appendChild(labelSpan);
                td.appendChild(document.createTextNode(content));
                td.className += cssClass;
                return td;

            }
            function disableTable( bToggle )
            {
                document.getElementById('PricingTable').disabled = bToggle ? '' : 'disabled';
            }
            
            function setWaitMsg ( msg )
            {
                console.log('msg',msg);
                bShowWait = msg != '';
                document.getElementById('PricingTableMsg').innerText = msg;
                
                document.getElementById('WaitImg').style.display = bShowWait ? 'block' : 'none';
            }
            
            function refreshPipeline( id, exp)
            {
                if ( window && window.opener )
                {
                    if ( typeof(window.opener.SetExpDate) != 'undefined' )
                    {
                        window.opener.SetExpDate(id,exp);
                    }
                }
            }

            function closeLockRatePopup(isCancel) {
                window.alert = oldWindowAlert;

                if (typeof window.parent !== 'undefined' &&
                    typeof window.parent.ratePopupCloseCallback !== 'undefined') {
                    window.parent.ratePopupCloseCallback(isCancel);
                }
                else {
                    self.close();
                }
            }
            </script>
    </div>
</body>
</html>
