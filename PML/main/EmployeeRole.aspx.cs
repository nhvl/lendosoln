using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Roles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PriceMyLoan.main
{
    public partial class EmployeeRole : PriceMyLoan.UI.BasePage
	{
		protected System.Web.UI.WebControls.Button m_ClearAssignment;

        private Guid m_sPmlBrokerIdFilter = Guid.Empty;

		private string ShowMode
		{
			get
			{
				if( RequestHelper.GetSafeQueryString( "show" ) != null )
				{
					return RequestHelper.GetSafeQueryString( "show" );
				}

				return String.Empty;
			}
		}

		private string RoleDesc
		{
			get
			{
				if( RequestHelper.GetSafeQueryString( "role" ) != null )
				{
					return RequestHelper.GetSafeQueryString( "role" );
				}

				return String.Empty;
			}
		}

		private string SearchFilter
		{
			get
			{
                if (m_SearchFilter.Text.TrimWhitespaceAndBOM() != string.Empty)
				{
					return m_SearchFilter.Text.TrimWhitespaceAndBOM();
				}

				return null;
			}
		}

		private string ErrorMessage
		{
			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		private ArrayList ErrorList
		{
			set
			{
                String eList = string.Empty;

				foreach( String sE in value )
				{
					if( eList.Length > 0 )
					{
						eList += "\r\n";
					}

					eList += sE;
				}

				ErrorMessage = eList;
			}
		}

		private string ArgsToWrite
		{
			set { ClientScript.RegisterHiddenField( "m_argsToWrite" , value.TrimEnd( ',' ) ); }
		}

		private string CommandToDo
		{
			set { ClientScript.RegisterHiddenField( "m_commandToDo" , value.TrimWhitespaceAndBOM() ); }
		}

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// We now check if the user has restricted assignment turned on.
			// With assignment restriction, the user can only assign an
			// employee that belongs to their branch.  The initial requirement
			// is to simplify processing of loans originated outside a central
			// processing center (hub) and assigned to a processor within
			// the central pool.  To maintain consistency in the pairing up
			// of shared processor with remote loan officers, we create
			// branches for each loan officer and add the processors that
			// they will use as internal employees of the officer's branch.
			// This way, remote officers won't get confused about who to
			// add to their loan.
			//
			// If you want to expose broker level employees as well, then
			// add that logic to the sproc (that's the central point for
			// retrieval and still generic enough -- passing the branch
			// as optional allows us to limit the result w/o implying too
			// much app-level logic).
			//
			// 8/2/2005 kb - Per case 2533, we will allow for searching
			// for employees by name matching.  To prevent large payloads,
			// we show the too much message when the number of employees
			// is great.  For this patch, we will limit this check to
			// only officer viewing.
            bool isPmlRole = RoleDesc == ConstApp.ROLE_LOAN_OFFICER ||
                RoleDesc == ConstApp.ROLE_BROKERPROCESSOR || 
                RoleDesc == ConstApp.ROLE_EXTERNAL_SECONDARY ||
                RoleDesc == ConstApp.ROLE_EXTERNAL_POST_CLOSER;

            if (RequestHelper.GetGuid("origid", Guid.Empty) != Guid.Empty && 
               isPmlRole)
            {
                m_sPmlBrokerIdFilter = RequestHelper.GetGuid("origid");
            }

			BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();

            if (RoleDesc.ToLower() == "agent")
			{
				// 8/4/2005 kb - We previously counted the number of employees
				// with a given role to decide whether or not to hide them
				// at start (see case 2541).  However, it's easier to just
				// check a broker bit.
                BrokerDB brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);

                if (m_SearchFilter.Text.TrimWhitespaceAndBOM() == string.Empty && brokerDB.HasManyUsers == true)
				{
					// Too many employees to show.  Go to the help box.

					m_TooMany.Visible = true;

					m_Grid.Visible  = false;
					m_Empty.Visible = false;

					return;
				}
			}

			try
			{
                E_EmployeeStatusFilterT status = E_EmployeeStatusFilterT.All;
                if (m_activeRB.Checked) 
                {
                    status = E_EmployeeStatusFilterT.ActiveOnly;
                } 
                else if (m_inactiveRB.Checked) 
                {
                    status = E_EmployeeStatusFilterT.InactiveOnly;
                }

                if (PriceMyLoanUser.HasPermission(LendersOffice.Security.Permission.AllowLoanAssignmentsToAnyBranch) == false)
				{
                    E_BranchChannelT? branchChannelFilter = null;
                    E_sCorrespondentProcessT? correspondentProcessFilter = null;

                    // PML users have a broker branch, mini-correspondent branch,
                    // and correspondent branch. We need to filter based on the 
                    // correct branch, which requires looking at the channel and
                    // correspondent process type of the file.
                    if (this.LoanID != Guid.Empty && isPmlRole)
                    {
                        // Bypass security to avoid any conflicts with workflow.
                        // We need to filter based on the correct branch and corr.
                        // process type regardless of user access.
                        var dataLoan = new CEmployeeData(this.LoanID);
                        dataLoan.InitLoad();

                        branchChannelFilter = dataLoan.sBranchChannelT;
                        correspondentProcessFilter = dataLoan.sCorrespondentProcessT;
                    }

                    roleTable.Retrieve(
                        PriceMyLoanUser.BrokerId,
                        PriceMyLoanUser.BranchId,
                        SearchFilter,
                        RoleDesc,
                        status,
                        branchChannelFilter,
                        correspondentProcessFilter);
				}
				else
				{
                    roleTable.Retrieve(
                        PriceMyLoanUser.BrokerId,
                        Guid.Empty,
                        SearchFilter,
                        RoleDesc,
                        status,
                        null,
                        null);
				}

                if (RequestHelper.GetGuid("loanid", Guid.Empty) != Guid.Empty && isPmlRole)
                {
                    LoanAccessInfo info = new LoanAccessInfo(RequestHelper.LoanID);
                    m_sPmlBrokerIdFilter = info.sPmlBrokerId;
                    m_Grid.Columns[1].Visible = false;
                }
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to load employees in role." , e );
			}

            if (PriceMyLoanUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false || 
                RoleDesc.ToLower() != "agent" || 
                RoleDesc.ToLower() != "externalsecondary" || 
                ShowMode.ToLower() == "simple")
			{
				// 11/1/2004 kb - We only show the external broker name
				// when the broker is a lender user with price my loan
				// enabled.  We expect (especially with loan officers)
				// long lists that underwriters and lender account execs
				// must sift to find the desired outside user.
				//
				// 8/2/2005 kb - We now only show the external broker
				// column with agents in a non-simple mode.

				m_Grid.Columns[ 1 ].Visible = false;
			}
			else
			{
				// 11/3/2004 kb - Per tn's request, we suspect that lenders
				// will have many 'P' users in their list when they show
				// the full user list display.

				m_Grid.Columns[ 0 ].HeaderText = "User";
			}

            if (roleTable[RoleDesc] != null)
			{

                List<BrokerLoanAssignmentTable.Spec> ls = new List<BrokerLoanAssignmentTable.Spec>();
                if (m_sPmlBrokerIdFilter != Guid.Empty)
                {
                    foreach (BrokerLoanAssignmentTable.Spec spec in roleTable[RoleDesc])
                    {
                        if (spec.PmlBrokerId == m_sPmlBrokerIdFilter)
                        {
                            ls.Add(spec);
                        }
                    }
                    m_Grid.DataSource = ViewGenerator.Generate(ls);
                }
                else
                {
                    m_Grid.DataSource = ViewGenerator.GenerateGeneric(roleTable[RoleDesc]);
                }
                m_Grid.DataBind();
			}

			if( m_Grid.Items.Count == 0 )
			{
				m_TooMany.Visible = false;
				m_Empty.Visible   = true;
			}
			else
			{
				m_TooMany.Visible = false;
				m_Empty.Visible   = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new EventHandler(this.PageInit);

        }

		#endregion

        private void PageInit(object sender, EventArgs e)
        {
            m_Grid.ItemCreated += new DataGridItemEventHandler(m_Grid_ItemCreated);
        }


        private void m_Grid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == System.Web.UI.WebControls.ListItemType.Header || e.Item.ItemType == System.Web.UI.WebControls.ListItemType.Footer)
                return;

            LinkButton linkButton = new LinkButton();
            linkButton.CommandName = "Assign";
            linkButton.CommandArgument = GetEmployeeInfoArg(e.Item.DataItem);
            linkButton.Text = DataBinder.Eval(e.Item.DataItem, "FullNameWithIsActiveStatus").ToString();

            e.Item.Cells[0].Controls.Add(linkButton);
        }


		protected string GetEmployeeInfoArg( object oRow )
		{
			// Build command arg for data grid click of employee.

			DataRowView dRow = oRow as DataRowView;

			if( dRow != null )
			{
				return String.Format
                    ("{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}:{8}:{9}:{10}:{11}"
					, dRow[ "EmployeeId" ]
					, dRow[ "FullName"   ]
					, dRow[ "Email"      ]
                    , dRow[ "Addr"]
                    , dRow["City"]
                    , dRow["State"]
                    , dRow["Zip"]
                    , dRow["Phone"]
                    , dRow["Fax"]
                    , dRow["CompanyName"]
                    , dRow["EmployeeLicenseXml"]
                    , dRow["CompanyLicenseXml"]
					);
			}

            return string.Empty;
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void EmployeeAssignmentClick( object source , System.Web.UI.WebControls.DataGridCommandEventArgs a )
        {
			// Assign clicked employee to the given role.  If any
			// error occurs, just popup a simple message and skip.

			try
			{
				String[] sArgs = a.CommandArgument.ToString().Split( ':' );

				if( a.CommandName.ToLower() == "assign" )
				{
					if( sArgs.Length < 8 )
					{
						throw new GenericUserErrorMessageException( "Invalid employee argument format -- not enough items." );
					}

                    if (ShowMode.ToLower() == "search")
                    {
                        ArgsToWrite = "Email=" + AspxTools.JsString(sArgs[2]) + "|EmployeeName=" + AspxTools.JsString(sArgs[1]) + "|EmployeeId=" + AspxTools.JsString(sArgs[0]) + "|OK=true";

                        CommandToDo = "Close";
                    }
                    else
                    {
                        CPageData dataLoan = new CEmployeeData(RequestHelper.LoanID);
                        dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                        dataLoan.InitLoad();

                        LicenseInfoList agentLicenseInfoList = LicenseInfoList.ToObject(sArgs[10]);
                        string agentStateLicense = CAgentFields.GetLendingLicenseByState(dataLoan.sSpStatePe, agentLicenseInfoList);

                        LicenseInfoList companyLicenseInfoList = LicenseInfoList.ToObject(sArgs[11]);
                        string companyStateLicense = CAgentFields.GetLendingLicenseByState(dataLoan.sSpStatePe, companyLicenseInfoList);

                        //The problem is that for non-pml employee, company license should populate according to the new logic
                                                
                        if (ShowMode.ToLower() == "full" || ShowMode.ToLower() == "fullsearch")
                        {
                            Guid employeeId = new Guid(sArgs[0]);                            
                            CAgentFields agent = dataLoan.GetAgentOfRole( E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

                            E_AgentRoleT e_ExistingRoleWithCommission = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(employeeId, RequestHelper.LoanID);
                            bool bCopyCommissionToAgent = (e_ExistingRoleWithCommission == E_AgentRoleT.Other || e_ExistingRoleWithCommission == E_AgentRoleT.LoanOfficer);
                            CommonFunctions.CopyEmployeeInfoToAgent(PriceMyLoanUser.BrokerId, agent, employeeId, bCopyCommissionToAgent);

                            ArgsToWrite = string.Format("Email={0}|EmployeeName={1}|EmployeeId='{2}'|Addr={3}|City={4}|State={5}|Zip={6}|Phone={7}|Fax={8}|sActualBrokerNm={9}|OK=true|AgentLicenseNum={10}|CompanyLicenseNum={11}|AgentLicenseXml={12}|CompanyLicenseXml={13}|LoanOfficer_PhoneOfCompany={14}|LoanOfficer_FaxOfCompany={15}|CommissionPointOfLoanAmount={16}|CommissionPointOfGrossProfit={17}|CommissionMinBase={18}|LoanOriginatorIdentifier={19}|CompanyLoanOriginatorIdentifier={20}|sFileVersion={21}",
                                AspxTools.JsString(sArgs[2]),
                                AspxTools.JsString(sArgs[1]),
                                new Guid(sArgs[0]),
                                AspxTools.JsString(sArgs[3]),
                                AspxTools.JsString(sArgs[4]),
                                AspxTools.JsString(sArgs[5]),
                                AspxTools.JsString(sArgs[6]),
                                AspxTools.JsString(sArgs[7]),
                                AspxTools.JsString(sArgs[8]),
                                AspxTools.JsString(agent.CompanyName),
                                AspxTools.JsString(Server.HtmlEncode(agent.LicenseNumOfAgent)),
                                AspxTools.JsString(Server.HtmlEncode(agent.LicenseNumOfCompany)),
                                AspxTools.JsString(Server.HtmlEncode(sArgs[10])),
                                AspxTools.JsString(Server.HtmlEncode(sArgs[11])),
                                AspxTools.JsString(Server.HtmlEncode(agent.PhoneOfCompany)),
                                AspxTools.JsString(Server.HtmlEncode(agent.FaxOfCompany)),
                                AspxTools.JsString(Server.HtmlEncode(agent.CommissionPointOfLoanAmount_rep)),
                                AspxTools.JsString(Server.HtmlEncode(agent.CommissionPointOfGrossProfit_rep)),
                                AspxTools.JsString(Server.HtmlEncode(agent.CommissionMinBase_rep)),
                                AspxTools.JsString(Server.HtmlEncode(agent.LoanOriginatorIdentifier)),
                                AspxTools.JsString(Server.HtmlEncode(agent.CompanyLoanOriginatorIdentifier)),
                                dataLoan.sFileVersion
                                );
                            CommandToDo = "Close";
                        }
                        else
                        {
                            AssignEmployee(new Guid(sArgs[0]), sArgs[2], sArgs[1], sArgs[3], sArgs[4], sArgs[5], sArgs[6], sArgs[7], sArgs[8], sArgs[9], agentStateLicense, companyStateLicense, sArgs[10], sArgs[11]);
                        }
                    }
				}
			}
			catch( PageDataAccessDenied )
			{
				ErrorMessage = "Access denied. You do not have permission to assign employee to role.";
			}
			catch
			{
				ErrorMessage = "Failed to assign employee to role.";
			}
        }

		/// <summary>
		/// Handle UI click.
		/// </summary>
		
		protected void ClearAssignmentClick( object sender , System.EventArgs a )
		{
			// Assign the no-employee employee to clear the role.

			try
			{
                AssignEmployee(Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
			}
			catch( PageDataAccessDenied )
			{
				ErrorMessage = "Access denied. You do not have permission to clear role.";
			}
			catch
			{
				ErrorMessage = "Failed to clear role.";
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

        private void AssignEmployee(Guid employeeID, string email, string employeeName, string addr, string city, string state, string zip, string phone, string fax, string sActualBrokerNm, string sAgentStateLicense, string sCompanyStateLicense, string sAgentStateLicenseXml, string sCompanyStateLicenseXml)
        {
            // Calculate loan assignment delta and send out email
            // notifications to every affected user who currently
            // wants to receive emails.
			//
			// 8/16/2004 kb - We now process role change notification
			// in the background.  All we do here is track the delta
			// based on who is being changed.  Note: We use the set
			// id property after it's set to retrieve the previous.
			// This works because the previous remains after setting.

            CPageData       dataLoan = new CEmployeeData( RequestHelper.LoanID );

			ArrayList      errorList = new ArrayList();
			Guid              roleId = Guid.Empty;
			Guid              prevId = Guid.Empty;
			Boolean   hasErrorToShow = false;

			dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            //sFileVersion = dataLoan.sFileVersion;

			switch( RequestHelper.GetSafeQueryString( "role" ) )
			{
                case "Agent":
					dataLoan.sEmployeeLoanRepId = employeeID;
					roleId = CEmployeeFields.s_LoanRepRoleId;
					prevId = dataLoan.sEmployeeLoanRepId;

                    if (employeeID != Guid.Empty && 
                        dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent)
                    {
                        var newLoanOfficer = new EmployeeDB(employeeID, PriceMyLoanUser.BrokerId);
                        newLoanOfficer.Retrieve();

                        if (newLoanOfficer.BranchID != Guid.Empty)
                        {
                            dataLoan.AssignBranch(newLoanOfficer.BranchID);
                        }
                    }
					break;
				case "Processor":
                    dataLoan.sEmployeeProcessorId = employeeID;
					roleId = CEmployeeFields.s_ProcessorRoleId;
					prevId = dataLoan.sEmployeeProcessorId;
					break;
                case "Manager":
                    dataLoan.sEmployeeManagerId = employeeID;
					roleId = CEmployeeFields.s_ManagerRoleId;
					prevId = dataLoan.sEmployeeManagerId;
					break;
                case "RealEstateAgent":
                    dataLoan.sEmployeeRealEstateAgentId = employeeID;
					roleId = CEmployeeFields.s_RealEstateAgentRoleId;
					prevId = dataLoan.sEmployeeRealEstateAgentId;
					break;
                case "Telemarketer":
                    dataLoan.sEmployeeCallCenterAgentId = employeeID;
					roleId = CEmployeeFields.s_CallCenterAgentRoleId;
					prevId = dataLoan.sEmployeeCallCenterAgentId;
					break;
				case "LenderAccountExec":
					dataLoan.sEmployeeLenderAccExecId = employeeID;
					roleId = CEmployeeFields.s_LenderAccountExecRoleId;
					prevId = dataLoan.sEmployeeLenderAccExecId;
					break;
				case "LockDesk":
					dataLoan.sEmployeeLockDeskId = employeeID;
					roleId = CEmployeeFields.s_LockDeskRoleId;
					prevId = dataLoan.sEmployeeLockDeskId;
					break;
				case "Underwriter":
					dataLoan.sEmployeeUnderwriterId = employeeID;
					roleId = CEmployeeFields.s_UnderwriterRoleId;
					prevId = dataLoan.sEmployeeUnderwriterId;
					break;
				case "LoanOpener":
                    dataLoan.sEmployeeLoanOpenerId = employeeID;
					roleId = CEmployeeFields.s_LoanOpenerId;
					prevId = dataLoan.sEmployeeLoanOpenerId;
					break;
                case "Shipper":
                    dataLoan.sEmployeeShipperId = employeeID;
                    roleId = CEmployeeFields.s_ShipperId;
                    prevId = dataLoan.sEmployeeShipperId;
                    break;
                case "Funder":
                    dataLoan.sEmployeeFunderId = employeeID;
                    roleId = CEmployeeFields.s_FunderRoleId;
                    prevId = dataLoan.sEmployeeFunderId;
                    break;
                case "PostCloser":
                    dataLoan.sEmployeePostCloserId = employeeID;
                    roleId = CEmployeeFields.s_PostCloserId;
                    prevId = dataLoan.sEmployeePostCloserId;
                    break;
                case "Insuring":
                    dataLoan.sEmployeeInsuringId = employeeID;
                    roleId = CEmployeeFields.s_InsuringId;
                    prevId = dataLoan.sEmployeeInsuringId;
                    break;
                case "CollateralAgent":
                    dataLoan.sEmployeeCollateralAgentId = employeeID;
                    roleId = CEmployeeFields.s_CollateralAgentId;
                    prevId = dataLoan.sEmployeeCollateralAgentId;
                    break;
                case "DocDrawer":
                    dataLoan.sEmployeeDocDrawerId = employeeID;
                    roleId = CEmployeeFields.s_DocDrawerId;
                    prevId = dataLoan.sEmployeeDocDrawerId;
                    break;
                case "CreditAuditor":
                    dataLoan.sEmployeeCreditAuditorId = employeeID;
                    roleId = CEmployeeFields.s_CreditAuditorId;
                    prevId = dataLoan.sEmployeeCreditAuditorId;
                    break;
                case "DisclosureDesk":
                    dataLoan.sEmployeeDisclosureDeskId = employeeID;
                    roleId = CEmployeeFields.s_DisclosureDeskId;
                    prevId = dataLoan.sEmployeeDisclosureDeskId;
                    break;
                case "JuniorProcessor":
                    dataLoan.sEmployeeJuniorProcessorId = employeeID;
                    roleId = CEmployeeFields.s_JuniorProcessorId;
                    prevId = dataLoan.sEmployeeJuniorProcessorId;
                    break;
                case "JuniorUnderwriter":
                    dataLoan.sEmployeeJuniorUnderwriterId = employeeID;
                    roleId = CEmployeeFields.s_JuniorUnderwriterId;
                    prevId = dataLoan.sEmployeeJuniorUnderwriterId;
                    break;
                case "LegalAuditor":
                    dataLoan.sEmployeeLegalAuditorId = employeeID;
                    roleId = CEmployeeFields.s_LegalAuditorId;
                    prevId = dataLoan.sEmployeeLegalAuditorId;
                    break;
                case "LoanOfficerAssistant":
                    dataLoan.sEmployeeLoanOfficerAssistantId = employeeID;
                    roleId = CEmployeeFields.s_LoanOfficerAssistantId;
                    prevId = dataLoan.sEmployeeLoanOfficerAssistantId;
                    break;
                case "Purchaser":
                    dataLoan.sEmployeePurchaserId = employeeID;
                    roleId = CEmployeeFields.s_PurchaserId;
                    prevId = dataLoan.sEmployeePurchaserId;
                    break;
                case "QCCompliance":
                    dataLoan.sEmployeeQCComplianceId = employeeID;
                    roleId = CEmployeeFields.s_QCComplianceId;
                    prevId = dataLoan.sEmployeeQCComplianceId;
                    break;
                case "Secondary":
                    dataLoan.sEmployeeSecondaryId = employeeID;
                    roleId = CEmployeeFields.s_SecondaryId;
                    prevId = dataLoan.sEmployeeSecondaryId;
                    break;
                case "Servicing":
                    dataLoan.sEmployeeServicingId = employeeID;
                    roleId = CEmployeeFields.s_ServicingId;
                    prevId = dataLoan.sEmployeeServicingId;
                    break;
                case "ExternalSecondary":
                    dataLoan.sEmployeeExternalSecondaryId = employeeID;
                    roleId = CEmployeeFields.s_ExternalSecondaryId;
                    prevId = dataLoan.sEmployeeExternalSecondaryId;

                    if (employeeID != Guid.Empty && dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
                    {
                        var newExternalSecondary = new EmployeeDB(employeeID, PriceMyLoanUser.BrokerId);
                        newExternalSecondary.Retrieve();

                        Guid branchID;

                        if (dataLoan.sCorrespondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                        {
                            branchID = newExternalSecondary.MiniCorrespondentBranchID;
                        }
                        else
                        {
                            branchID = newExternalSecondary.CorrespondentBranchID;
                        }

                        if (branchID != Guid.Empty)
                        {
                            dataLoan.AssignBranch(branchID);
                        }
                    }
                    break;
                case "ExternalPostCloser":
                    dataLoan.sEmployeeExternalPostCloserId = employeeID;
                    roleId = CEmployeeFields.s_ExternalPostCloserId;
                    prevId = dataLoan.sEmployeeExternalPostCloserId;
                    break;
                default:
                    throw new GenericUserErrorMessageException( "Invalid role '" + RequestHelper.GetSafeQueryString( "role" ) + "'." );
            }

			if( hasErrorToShow == false )
			{
				// Save the changes to the loan.  We update the cache table
				// under the hood.

				try
				{
					dataLoan.Save();
                    sFileVersion = TransformData(); // OPM 12680 - Recalculate price group if user change loan officer.
					RoleChange.Mark( RequestHelper.LoanID , PriceMyLoanUser.BrokerId , dataLoan
						, prevId
						, employeeID
						, roleId
						, E_RoleChangeT.Update
						);
				}
				catch( LendersOffice.Security.AccessDenied e )
				{
					errorList.Add
						( String.Format( "{0}: Access denied." , dataLoan.sLNm )
						);

					hasErrorToShow = true;

					Tools.LogError( "Employee role change: Failed to save and mark role change.  Access denied.", e );
				}
                catch (CBaseException e)
                {
                    errorList.Add(e.UserMessage);
                    hasErrorToShow = true;
                }
                catch ( Exception e )
				{
					Tools.LogError( "Employee role change: Failed to save and mark role change.", e );
				}

				// 7/22/2005 kb - Hide the grid because we don't want to re-
				// render and pipe out a whole bunch of content when the window
				// is set to close anyway.

				m_Grid.DataSource = new ArrayList();
				m_Grid.DataBind();

				m_Grid.Visible = false;

				// Close the window by leaving a command for the page's init
				// scripting post-load.

				CommandToDo = "Close";
			}

			if( hasErrorToShow == true )
			{
				errorList.Add( String.Format( "Changes to file {0} are disregarded." , dataLoan.sLNm ) );
			}

            // Close the popup list and move on.

			if( errorList.Count == 0 )
			{
                ArgsToWrite = string.Format("Email={0}|EmployeeName={1}|EmployeeId='{2}'|Addr={3}|City={4}|State={5}|Zip={6}|Phone={7}|Fax={8}|sActualBrokerNm={9}|OK=true|AgentLicenseNum={10}|CompanyLicenseNum={11}|AgentLicenseXml={12}|CompanyLicenseXml={13}|sFileVersion={14}",
                    AspxTools.JsString(email), AspxTools.JsString(employeeName), employeeID, AspxTools.JsString(addr), AspxTools.JsString(city),
                    AspxTools.JsString(state), AspxTools.JsString(zip), AspxTools.JsString(phone), AspxTools.JsString(fax), AspxTools.JsString(sActualBrokerNm),
                    AspxTools.JsString(sAgentStateLicense), AspxTools.JsString(sCompanyStateLicense), AspxTools.JsString(Server.HtmlEncode(sAgentStateLicenseXml)), AspxTools.JsString(Server.HtmlEncode(sCompanyStateLicenseXml)), sFileVersion);
                Tools.LogError(string.Format("Email={0}|EmployeeName={1}|EmployeeId='{2}'|Addr={3}|City={4}|State={5}|Zip={6}|Phone={7}|Fax={8}|sActualBrokerNm={9}|OK=true|AgentLicenseNum={10}|CompanyLicenseNum={11}|AgentLicenseXml={12}|CompanyLicenseXml={13}|sFileVersion={14}",
                    AspxTools.JsString(email), AspxTools.JsString(employeeName), employeeID, AspxTools.JsString(addr), AspxTools.JsString(city),
                    AspxTools.JsString(state), AspxTools.JsString(zip), AspxTools.JsString(phone), AspxTools.JsString(fax), AspxTools.JsString(sActualBrokerNm),
                    AspxTools.JsString(sAgentStateLicense), AspxTools.JsString(sCompanyStateLicense), AspxTools.JsString(Server.HtmlEncode(sAgentStateLicenseXml)), AspxTools.JsString(Server.HtmlEncode(sCompanyStateLicenseXml)), sFileVersion)
);
                Tools.LogError("sFileVersion=" + sFileVersion);
			}
			else
			{
				ArgsToWrite = "Email=''|EmployeeName=''|EmployeeId=''|OK=false";

				ErrorList = errorList;
			}
        }

	}

}
