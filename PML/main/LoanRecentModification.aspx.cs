﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace PriceMyLoan.main
{
    public partial class LoanRecentModification : PriceMyLoan.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AbstractUserPrincipal principal = Page.User as AbstractUserPrincipal;
            if (null != principal)
            {
                List<string> list = CPageData.GetRecentModificationUserName(LoanID, principal);
                m_repeater.DataSource = list;
                m_repeater.DataBind();
            }
        }
    }
}
