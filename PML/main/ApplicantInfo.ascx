<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ApplicantInfo.ascx.cs" Inherits="PriceMyLoan.UI.Main.ApplicantInfo" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>


<script type="text/javascript">
    var g_isPageReadOnly = <%= AspxTools.JsBool(IsReadOnly) %>;
    
    function f_focus_1stRequired() 
    {
      var aBFirstNm = <%= AspxTools.JsGetElementById(aBFirstNm) %>;
      var aBLastNm = <%= AspxTools.JsGetElementById(aBLastNm) %>;
      var aBSsn = <%= AspxTools.JsGetElementById(aBSsn) %>;
      var aCFirstNm = <%= AspxTools.JsGetElementById(aCFirstNm) %>;
      var aCLastNm = <%= AspxTools.JsGetElementById(aCLastNm) %>;
      var aCSsn = <%= AspxTools.JsGetElementById(aCSsn) %>;
      var aBHasSpouse = <%= AspxTools.JsGetElementById(aBHasSpouse) %>.checked;
      
      if (!g_isPageReadOnly) 
      {
        if (aBFirstNm.value == "") aBFirstNm.focus();
        else if (aBLastNm.value == "") aBLastNm.focus();
        else if (aBSsn.value == "") aBSsn.focus();
        else if (aBHasSpouse && aCFirstNm.value == "") aCFirstNm.focus();
        else if (aBHasSpouse && aCLastNm.value == "") aCLastNm.focus();
        else if (aBHasSpouse && aCSsn.value == "") aCSsn.focus();
        else aBFirstNm.focus();
      }
    }
    
    var gDisabledSsn = false, gCurrentSsnId = null;
    
    function f_validateSsn(source, args) 
    {
        var o = document.getElementById(source.controltovalidate);
        var $source = $j(source);
        if (gDisabledSsn && gCurrentSsnId == source.controltovalidate)
        {
            $source.html("");
        }
        else
        {
            $source.html("Invalid format of SSN");
        }
        
        if (null != o && o.value != "")
        {
            args.IsValid = o.value.length == 11;
            return;
        }
        args.IsValid = true;
    }
    
    
    function f_ssn(o)
    {
        var $o = $j(o);
        if ($o.val().length < 11)
        {
            gDisabledSsn = true;
            gCurrentSsnId = $o.prop('id');
        }
    
        f_onValidateRequiredFields();
  
        gDisabledSsn = false;
    }
    

    function f_validatePage() 
    {
      var bValid = true;
      if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
        bValid = Page_ClientValidate();
      return bValid;
    }
    
    function f_onValidateRequiredFields() 
    {
        var bValid = true;
        if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
        bValid = Page_ClientValidate();
    
        f_enableAllButtons(bValid);
    }
    
    <%-- //opm 4382 fs 08/02/08 --%>
    function f_coApplicantHasEmptyFields()
    {
        if (g_isPageReadOnly)
        {
	        return true;
	    }
	    
	    var cbFields = $j('<%= AspxTools.JSelector(aCFirstNm,aCMidNm,aCLastNm,aCSuffix,aCSsn,aCEmail) %>');
    	var isEmpty = true;
    	
    	cbFields.each(function()
    	{
            var $this = $j(this);
            if( $this.val() != '' && $this.val() != '000-00-0000')
            {
                isEmpty = false;
                return false;
            }    	    
    	});
  
        return isEmpty;
    }
    
    function f_confirmSpouseClick(cb)
    {
        var $cb = $j(cb);
        if ($cb.prop('checked') == false && !f_coApplicantHasEmptyFields())
        {
            if (confirm ("Are you sure you want to delete the co-applicant information?"))
            {
                f_onHasSpouseClick(cb);
            }
            else 
            {
                $cb.prop('checked', true) 
            }
        }
        else
        {
            f_onHasSpouseClick(cb);
        }
    }

    function f_onHasSpouseClick(cb)
    {
        f_spouseReadOnlyFields();
        
        var aIsBorrSpousePrimaryWageEarner = $j('<%= AspxTools.JSelector(aIsBorrSpousePrimaryWageEarner) %>'), $cb = $j(cb);
        if (!$cb.prop('checked')) 
        {
            var aCSsn = $j('<%= AspxTools.JSelector(aCSsn) %>');
            aIsBorrSpousePrimaryWageEarner.prop('checked',false);
            aCSsn.val('');
        } 
    
        f_onValidateRequiredFields();    
    }
    
    function f_spouseReadOnlyFields()
    {
    
        var aBHasSpouse = $j('<%= AspxTools.JSelector(aBHasSpouse) %>'),  hasSpouse = aBHasSpouse.prop('checked');
        var cbFields = $j('<%= AspxTools.JSelector(aCFirstNm,aCMidNm,aCLastNm,aCSuffix,aCSsn,aCEmail) %>'); // coborrower fields
        var aIsBorrSpousePrimaryWageEarner = $j('<%= AspxTools.JSelector(aIsBorrSpousePrimaryWageEarner) %>');
        var isReadOnly = !(hasSpouse && !g_isPageReadOnly );
        
        cbFields.readOnly(isReadOnly);
        aIsBorrSpousePrimaryWageEarner.readOnly(isReadOnly);
  
        if (!g_isPageReadOnly && false == hasSpouse)
        {
            cbFields.val('');
            aIsBorrSpousePrimaryWageEarner.prop('checked', false);
        }
        
//        if( !g_isPageReadOnly && hasSpouse)
//        {
//            $j("<%= AspxTools.JSelector(aCSsn) %>").val("000-00-0000");
//        }
//        
        var validators = $j('<%= AspxTools.JSelector(aCFirstNmValidator,aCLastNmValidator,aCSsnValidator,aCSsnFormatValidator,aCEmailValidator) %>');
        validators.each(function()
        {
            this.enabled = hasSpouse && !g_isPageReadOnly;
        });
    }

    function f_uc_init() 
    {
        <%-- opm 4382 fs 07/25/08 --%>
        f_spouseReadOnlyFields();
  
        if (g_isPageReadOnly)
        {
            $j('#lnkEditLiability').prop('disabled', true).css({'text-decoration' : 'none', 'cursor' : 'default'});
        }
    }
    
    function f_editLiability() 
    {
        if(g_isPageReadOnly)
        {
            return;
        }
        
        var urlFragments = [ML.VirtualRoot , '/main/LiabilityList.aspx?loanid=', <%= AspxTools.JsString(LoanID) %> ,  '&appid=',<%= AspxTools.JsString(m_applicationID) %>];
        LQBPopup.Show(urlFragments.join(''), 
            {
                hideCloseButton:true, 
                width: 700,
                height: 500, 
                onReturn : function(args)
                    {
                               <%= AspxTools.JsGetElementById(sTransmOMonPmtPe) %>.value = args.aTransmOMonPmtPe;
                                f_setFileVersion(args.fileVersion);
                    }
            }
        );
    }
    
    

    function f_onEditPublicRecordsClick() 
    {
        var fragments = [ML.VirtualRoot , '/Main/PublicRecordList.aspx?loanid=', <%= AspxTools.JsString(LoanID) %>,  '&appid=', <%= AspxTools.JsString(m_applicationID) %>];
        LQBPopup.Show(fragments.join(''), 
            {
                hideCloseButton:true
            });
	    return false;  
    }



    function f_emailValidation(src, args){
        var regex = new RegExp(<%=AspxTools.JsString(ConstApp.EmailValidationExpression) %>);
        if (typeof src == undefined || src == null)
            return;
  
        try
        {
            var sToValidate = (src == <%=AspxTools.JsGetElementById(aBEmailValidator) %>) ? <%=AspxTools.JsGetElementById(aBEmail) %>.value : <%=AspxTools.JsGetElementById(aCEmail) %>.value;
            var isValid = sToValidate.match(regex);
            var isEmpty = (sToValidate.length == 0);
            args.IsValid = <% if (CurrentBroker.IsaBEmailRequiredInPml) { %> isValid <% } else { %> isValid || isEmpty <% } %>;
        }catch(e){}
    }
</script>

<asp:PlaceHolder runat="server" ID="ShowCreditScripts" Visible="false">
    <script type="text/javascript">
        function f_onViewCreditReportClick() {
            var fragments = [ML.VirtualRoot , '/Main/ViewCreditFrame.aspx?loanid=', <%= AspxTools.JsString(LoanID) %>,  '&applicationid=', <%= AspxTools.JsString(m_applicationID) %>];
            LQBPopup.Show(fragments.join(''), {width: 700, height: 800, hideCloseButton: true});
	        return false;  
        }
    </script>
</asp:PlaceHolder>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" style="PADDING-LEFT: 5px">
<table id=Table1 cellSpacing=0 cellPadding=5 width=700 border=0>
  <TBODY>
  <%-- 2/20/08 OPM 20270. We do not display this message for Citi. --%>
  <% if (! PriceMyLoan.Common.PriceMyLoanConstants.IsEmbeddedPML && PriceMyLoanUser.BrokerId != new Guid("c748e65c-82f0-409a-8c0a-4b8ed017d45b" ) ) { %>
  <tr>
    <td colspan=2 style="FONT-WEIGHT: bold; COLOR: dimgray;" 
      >For scenarios with <span style="FONT-STYLE: italic">non-married</span> co-applicants, please contact your account executive for assistance.</td></tr>
  <% } %>
  <tr>
    <td colSpan=2>
      <table width=600 border=0 cellSpacing=0 cellPadding=0>
        <tr>
          <td vAlign=top noWrap>
            <table cellSpacing=0 cellPadding=0>
              <tr>
                <td noWrap colSpan=2><IMG src="../images/st_applicantinfo.gif" ></td></tr>
              <tr>
                <td class=FieldLabel noWrap>First Name</td>
                <td noWrap><asp:textbox id=aBFirstNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aBFirstNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aBFirstNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Middle 
                  Name&nbsp;&nbsp;</td>
                <td noWrap><asp:textbox id=aBMidNm width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Last Name</td>
                <td noWrap><asp:textbox id=aBLastNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aBLastNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aBLastNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Suffix</td>
                <td noWrap><asp:textbox id=aBSuffix width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>SSN</td>
                <td noWrap><ml:ssntextbox id=aBSsn onkeyup="f_ssn(this);" onblur="f_onValidateRequiredFields();" width="130px" runat="server" preset="ssn"></ml:ssntextbox><asp:requiredfieldvalidator id=aBSsnValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aBSsn"></asp:requiredfieldvalidator><asp:CustomValidator id=aBSsnFormatValidator runat="server" ErrorMessage="Invalid format of SSN" ControlToValidate="aBSsn" ClientValidationFunction="f_validateSsn" Display="Dynamic" CssClass="FieldLabel"></asp:CustomValidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>E-mail</td>
                <td noWrap>
                    <asp:textbox id="aBEmail" width="160px" onkeyup="f_onValidateRequiredFields();" runat="server"></asp:textbox>
                    <asp:CustomValidator id="aBEmailValidator" runat="server" forecolor="" ClientValidationFunction="f_emailValidation" ValidateEmptyText="true" cssclass="RequireValidatorMessage" controltovalidate="aBEmail"></asp:CustomValidator>
                </td></tr>
              <tr>
                <td class=FieldLabel noWrap width=105>Credit 
                  Scores&nbsp;&nbsp;</td>
                <td class=FieldValue noWrap><span title="Experian">XP:</span><asp:textbox id=aBExperianScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="TransUnion">TU:</span><asp:textbox id=aBTransUnionScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="Equifax">EF:</span><asp:textbox id=aBEquifaxScorePe runat="server" Width="35px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Citizenship</td>
                <td class=FieldValue noWrap><asp:dropdownlist id=aProdBCitizenT runat="server"></asp:dropdownlist></td></tr>
              <tr>
                <td class=FieldLabel noWrap colSpan=2 
                  >&nbsp;&nbsp;&nbsp;&nbsp;Has Co-Applicant?&nbsp;&nbsp;<asp:checkbox id=aBHasSpouse onclick=f_confirmSpouseClick(this); runat="server" text="Yes"></asp:checkbox></td></tr></table></td>
          <TD style="WIDTH: 22px" vAlign=top noWrap>&nbsp;&nbsp;&nbsp; </TD>
          <td vAlign=top noWrap>
            <table cellSpacing=0 cellPadding=0>
              <tbody id=SpousePanel>
              <tr>
                <td class=FieldLabel noWrap colSpan=2 
                  ><IMG src="../images/st_coaapplicantinfo.gif" ></td></tr>
              <tr>
                <td class=FieldLabel noWrap>First Name</td>
                <td noWrap><asp:textbox id=aCFirstNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aCFirstNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aCFirstNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Middle 
                  Name&nbsp;&nbsp;</td>
                <td noWrap><asp:textbox id=aCMidNm width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Last Name</td>
                <td noWrap><asp:textbox id=aCLastNm onkeyup=f_onValidateRequiredFields(); width="160px" runat="server"></asp:textbox><asp:requiredfieldvalidator id=aCLastNmValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aCLastNm"></asp:requiredfieldvalidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Suffix</td>
                <td noWrap><asp:textbox id=aCSuffix width="160px" runat="server"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>SSN</td>
                <td noWrap><ml:ssntextbox id=aCSsn  onkeyup="f_ssn(this);" onblur="f_onValidateRequiredFields();" width="130px" runat="server" preset="ssn"></ml:ssntextbox><asp:requiredfieldvalidator id=aCSsnValidator runat="server" forecolor=" " cssclass="RequireValidatorMessage" controltovalidate="aCSsn"></asp:requiredfieldvalidator><asp:CustomValidator id=aCSsnFormatValidator runat="server" ErrorMessage="Invalid format of SSN" ControlToValidate="aCSsn" ClientValidationFunction="f_validateSsn" Display="Dynamic" CssClass="FieldLabel"></asp:CustomValidator></td></tr>
              <tr>
                <td class=FieldLabel noWrap>E-mail</td>
                <td noWrap>
                    <asp:textbox id="aCEmail" width="160px" onkeyup="f_onValidateRequiredFields();" runat="server"></asp:textbox>
                    <asp:CustomValidator id="aCEmailValidator" runat="server" forecolor="" ClientValidationFunction="f_emailValidation" ValidateEmptyText="true" cssclass="RequireValidatorMessage" controltovalidate="aCEmail"></asp:CustomValidator>
                </td></tr>
              <tr>
                <td class=FieldLabel noWrap>Credit 
                  Scores&nbsp;&nbsp;&nbsp;</td>
                <td class=FieldValue noWrap><span title="Experian">XP:</span><asp:textbox id=aCExperianScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="TransUnion">TU:</span><asp:textbox id=aCTransUnionScorePe runat="server" Width="35px"></asp:textbox> 
                  <span title="Equifax">EF:</span><asp:textbox id=aCEquifaxScorePe runat="server" Width="35px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap colSpan=2 
                  >Co-Applicant is Primary Wage Earner?&nbsp;
<asp:checkbox id=aIsBorrSpousePrimaryWageEarner runat="server" Text="Yes"></asp:checkbox></td></tr></tbody></table></td></tr></table></td></tr>
<tr><td><hr></td></tr>
  <tr>
    <td vAlign=top colSpan=2>
      <table id=Table2 cellSpacing=0 cellPadding=0 border=0>
					<tr>
          <td class=FieldLabel noWrap width=105>Self Employed?</td>
          <td class=FieldLabel noWrap width="100%"><asp:checkbox id=sIsSelfEmployed runat="server" Text="Yes"></asp:checkbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2 
        >&nbsp;</td></tr>
        <TR>
          <TD class=FieldLabel noWrap width=105>Total Payment 
          &nbsp;</TD>
          <TD class=FieldLabel noWrap><ml:moneytextbox id=sTransmOMonPmtPe width="90" runat="server" preset="money" ReadOnly="True"></ml:moneytextbox>&nbsp;/
			month&nbsp;<A onclick=f_editLiability(); href="#" id="lnkEditLiability">Edit Liabilities</A>
			<% if (m_displayPublicRecordsEdit) {
			if ( ! IsReadOnly ) { %>
			  &nbsp;&nbsp;&nbsp;<a id="lnkEditPublicRecord" href="#" onclick="return f_onEditPublicRecordsClick();" >Edit Public Records</a>
			<% } else { %>
			  &nbsp;&nbsp;&nbsp;<a id="lnkEditPublicRecord" href="#" style="CURSOR: default; TEXT-DECORATION: none" disabled="disabled" >Edit Public Records</a>
			<% } 
			 } else if (m_displayCreditReportButton) { %> 
			&nbsp;&nbsp;&nbsp;<span title="No editable public records on credit report">No Public Records</span>
			<% }
     if (m_displayCreditReportButton) { %>
    &nbsp;&nbsp;&nbsp;<a id="lnkViewCreditReport" href="#" onclick="return f_onViewCreditReportClick();" >View Credit Report</a>
    <% } %>            
            </TD></TR>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>Negative 
            Cash Flow from Other Properties&nbsp;<ml:moneytextbox id=sOpNegCfPe width="90px" runat="server" preset="money"></ml:moneytextbox>&nbsp;/ 
            month&nbsp;&nbsp;<a id="lnkHelpNegCf" onclick="f_openHelp('Q00004.html', 730, 600);" tabIndex=-1 href="#" >Explain</a></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2 style="PADDING-TOP: 5px">Total income will be asked at the next step.</td></tr></table></td></tr>
  <tr>
    <td noWrap colSpan=2></td></tr></TBODY></table>
</td>
					</tr>
</table>
