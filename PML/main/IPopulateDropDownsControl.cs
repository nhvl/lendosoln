﻿namespace PriceMyLoan.UI
{
    using System.Collections.Generic;
    using global::DataAccess;

    /// <summary>
    /// This interface defines a control that can return a map
    /// of dropdownlists and their options.
    /// The data should then be used by the page containing these controls
    /// to populate the actual dropdownlists.
    /// </summary>
    public interface IPopulateDropDownsControl
    {
        Dictionary<string, object> PopulateDDLs(CPageData loan);
        Dictionary<string, object> GetHiddenOptions();
    }
}
