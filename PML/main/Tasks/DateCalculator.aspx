﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DateCalculator.aspx.cs" Inherits="PriceMyLoan.main.Tasks.DateCalculator" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Date Calculator</title>
</head>
<body>
<script>
    function _init() {
        resizeForIE6And7(550, 400);
    }
</script>
<div lqb-popup class="modal-date-calculator">
    <div date-calculator></div>
</div>
<form runat="server"></form>
</body>
</html>
