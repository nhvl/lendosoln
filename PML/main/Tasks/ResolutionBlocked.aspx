﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResolutionBlocked.aspx.cs" Inherits="PriceMyLoan.main.Tasks.ResolutionBlocked" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Task Resolution Blocked</title>
</head>
<style type="text/css">
    #divButton { text-align: right; height: 15%; }
    .FieldLabel { height: 85%; overflow: scroll; }
</style>
<body>
    <form id="form1" runat="server">
    <div class="FieldLabel">
        <ml:EncodedLabel ID="ResolutionDenialMessage" runat="server"></ml:EncodedLabel>
    </div>
    <div id="divButton">
        <input type="button" value=" OK "  id="OkBtn"/> 
    </div>
    </form>
    <script type="text/javascript">
        $j(function(){
            var resolutionDenialMessage = <%=AspxTools.JsGetElementById(ResolutionDenialMessage)%>;
            var dialogArgs = parent.LQBPopup.GetArguments();
            
            resolutionDenialMessage.innerText = dialogArgs.resolutionDenialMessage;
            
            $j('#OkBtn').click(function(){
                parent.LQBPopup.Hide();
            });
        });
    </script>
</body>
</html>
