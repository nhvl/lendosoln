﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoleAndUserPicker.aspx.cs" Inherits="PriceMyLoan.main.Tasks.RoleAndUserPicker" %>
<!DOCTYPE html>
<html>
<head runat="server">
<style>
    th.header
    {
        cursor: pointer;
    }
    thead th.sortable { cursor: pointer; font-weight: bold!important;  }
</style>
</head>
<body class="modal-iframe-body" onload="resizeForIE6And7(500, 400);" >
<div lqb-popup class="modal-pipeline-tasks">
    <div ng-bootstrap="RoleAndUserPicker" role-and-user-picker></div>
</div>
<form runat="server"></form>
</body>
</html>
