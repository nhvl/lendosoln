﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Common;
using DataAccess;
using System.Data;
using LendersOffice.Security;
using System.Threading;
using System.Data.SqlClient;
using System.Web.Services;
using LendersOffice.ConfigSystem;
using LendersOffice.Admin;
using LendersOffice.ObjLib.Task;
using LendersOffice.ConfigSystem.Operations;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.main.Tasks
{
    public partial class RoleAndUserPicker : PriceMyLoan.UI.BasePage
    {
        protected int PermissionLevelID
        {
            get { return RequestHelper.GetInt("PermLevel", -1); }
        }

        protected bool SkipSecurityCheck
        {
            get { return RequestHelper.GetBool("IsBatch"); }
        }

        protected bool IsPipelineQuery
        {
            get { return RequestHelper.GetBool("IsPipeline"); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable m_dataTable = new DataTable();
            m_dataTable.Columns.Add("UserId", typeof(string));
            m_dataTable.Columns.Add("FullName", typeof(string));
            m_dataTable.Columns.Add("Roles", typeof(string));
            RegisterJsObjectWithJsonNetAnonymousSerializer("RoleAndUserPickerViewModel", new
            {
                LoanID,
                IsPipeline = IsPipelineQuery,
                PermissionLevelID,
                SkipSecurityCheck,
                // roleListOptions = Role.LendingQBRoles.OrderBy(p => p.ModifiableDesc).Select(r => new {value = r.Id, label = r.ModifiableDesc}),
            });
        }

        public class UserForAssign
        {
            public Guid   UserId { get; set; }
            public string FullName { get; set; }
            public string Roles { get; set; }
        }

        private static List<UserForAssign> SearchNonPipelineEmptyText(Guid LoanID)
        {
            DataTable m_dataTable = new DataTable();
            m_dataTable.Columns.Add("UserId", typeof(string));
            m_dataTable.Columns.Add("FullName", typeof(string));
            m_dataTable.Columns.Add("Roles", typeof(string));

            Guid brokerId = Guid.Empty;
            DbConnectionInfo.GetConnectionInfoByLoanId(LoanID, out brokerId);
            var lTable = new LoanAssignmentContactTable(brokerId, LoanID);

            return lTable.Items
                .Where(lA => lA.IsPmlUser)
                //.Select(lA => new { lA.UserId, lA.FullName, lA.RoleModifiableDesc })
                .GroupBy(lA => lA.UserId)
                .Where(g => PrincipalFactory.RetrievePrincipalForUser(brokerId, g.Key, "P") != null)
                .Select(g => new UserForAssign
                {
                    UserId = g.Key,
                    FullName = g.First().FullName,
                    Roles = string.Join(", ", g.Select(lA => lA.RoleModifiableDesc).OrderBy(s => s))
                })
                .ToList()
                ;

        }

        [WebMethod]
        public static List<UserForAssign> Search(string text, bool IsPipeline, Guid LoanID, bool SkipSecurityCheck, int PermissionLevelID)
        {
            text = text.Trim();
            if (!IsPipeline && string.IsNullOrEmpty(text)) return SearchNonPipelineEmptyText(LoanID);

            string NameFilter = text.IndexOf(' ') < 0 ? text : text.Substring(0, text.IndexOf(' ')).TrimWhitespaceAndBOM();
            string LastFilter = text.IndexOf(' ') < 0 ? string.Empty : text.Substring(text.IndexOf(' ')).TrimWhitespaceAndBOM();

            AbstractUserPrincipal user = Thread.CurrentPrincipal as AbstractUserPrincipal;
            CPageData loanData = null;
            if (!IsPipeline)
            {
                loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(RoleAndUserPicker));
                loanData.InitLoad();
            }
            Guid brokerId = user.BrokerId;
            LoanValueEvaluator valueEvaluator = null;
            if (!IsPipeline)
            {
                valueEvaluator = new LoanValueEvaluator(brokerId, LoanID, WorkflowOperations.ReadLoanOrTemplate, WorkflowOperations.WriteLoanOrTemplate);
            }

            var pA = new []
            {
                loanData == null ? null : new SqlParameter("@LoanId", loanData.sLId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@NameFilter", NameFilter),
                new SqlParameter("@NoLO", 1),
                new SqlParameter("@PmlBrokerId", PrincipalFactory.CurrentPrincipal.PmlBrokerId),
                String.IsNullOrEmpty(LastFilter) ? null : new SqlParameter("@LastFilter", LastFilter),
            }.Where(p => p != null);

            var users = new List<UserForAssign>();
            using (IDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_GetUsersForAssignment", pA))
            {
                while (dr.Read())
                {
                    var employeeId      = new Guid(dr["EmployeeId"].ToString());
                    var userId          = new Guid(dr["UserId"].ToString());
                    var userName        = dr["FullName"].ToString();
                    var branchId        = (Guid)dr["BranchId"];
                    var pmlBrokerId     = (Guid)dr["PmlBrokerId"];
                    var pmlLevelAccess  = (E_PmlLoanLevelAccess)dr["PmlLevelAccess"];
                    var permissions     = (string)dr["Permissions"];
                    var applicationType = E_ApplicationT.PriceMyLoan;

                    ExecutingEnginePrincipal principal = ExecutingEnginePrincipal.CreateFrom(brokerId, branchId, pmlBrokerId, employeeId, userId, pmlLevelAccess, applicationType, permissions, user.Type);
                    //AbstractUserPrincipal principal = PrincipalFactory.RetrievePrincipalForUser(userId, type) as AbstractUserPrincipal;

                    if (!SkipSecurityCheck)
                    {
                        if (principal == null)
                        {
                            continue;
                        }

                        // Workflow config check
                        valueEvaluator.SetEvaluatingPrincipal(principal);
                        if (!LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator))
                        {
                            continue;
                        }

                        // Permission level check
                        if (PermissionLevelID != -1)
                        {
                            PermissionLevel selectedPermission = PermissionLevel.Retrieve(user.BrokerId, PermissionLevelID);
                            List<Guid> UserGroups = GroupDB.ListInclusiveGroupForEmployee(principal.BrokerId, principal.EmployeeId)
                                .Select(ugroup => ugroup.Id)
                                .ToList();
                            List<E_RoleT> Roles = principal.GetRoles().ToList();
                            if (!selectedPermission.IsRoleOrGroupInLevel(E_PermissionLevel.WorkOn, Roles, UserGroups))
                            {
                                continue;
                            }
                        }
                    }

                    users.Add(new UserForAssign
                    {
                        UserId = userId,
                        FullName = userName,
                        Roles = loanData == null ? string.Empty : string.Join(",", new[]
                            {
                                (employeeId == loanData.sEmployeeProcessorId           ) ? "Processor"                : null,
                                (employeeId == loanData.sEmployeeBrokerProcessorId     ) ? "Processor (External)"     : null,
                                (employeeId == loanData.sEmployeeCallCenterAgentId     ) ? "Call Center Agent"        : null,
                                (employeeId == loanData.sEmployeeCloserId              ) ? "Closer"                   : null,
                                (employeeId == loanData.sEmployeeLenderAccExecId       ) ? "Lender Account Executive" : null,
                                (employeeId == loanData.sEmployeeLoanOpenerId          ) ? "Loan Opener"              : null,
                                (employeeId == loanData.sEmployeeLoanRepId             ) ? "Loan Officer"             : null,
                                (employeeId == loanData.sEmployeeLockDeskId            ) ? "Lock Desk"                : null,
                                (employeeId == loanData.sEmployeeManagerId             ) ? "Manager"                  : null,
                                (employeeId == loanData.sEmployeeRealEstateAgentId     ) ? "Real Estate Agent"        : null,
                                (employeeId == loanData.sEmployeeUnderwriterId         ) ? "Underwriter"              : null,
                                (employeeId == loanData.sEmployeeFunderId              ) ? "Funder"                   : null,
                                (employeeId == loanData.sEmployeeShipperId             ) ? "Shipper"                  : null,
                                (employeeId == loanData.sEmployeePostCloserId          ) ? "Post-Closer"              : null,
                                (employeeId == loanData.sEmployeeInsuringId            ) ? "Insuring"                 : null,
                                (employeeId == loanData.sEmployeeCollateralAgentId     ) ? "Collateral Agent"         : null,
                                (employeeId == loanData.sEmployeeDocDrawerId           ) ? "Doc Drawer"               : null,
                                (employeeId == loanData.sEmployeeCreditAuditorId       ) ? "Credit Auditor"           : null,
                                (employeeId == loanData.sEmployeeDisclosureDeskId      ) ? "Disclosure Desk"          : null,
                                (employeeId == loanData.sEmployeeJuniorProcessorId     ) ? "Junior Processor"         : null,
                                (employeeId == loanData.sEmployeeJuniorUnderwriterId   ) ? "Junior Underwriter"       : null,
                                (employeeId == loanData.sEmployeeLegalAuditorId        ) ? "Legal Auditor"            : null,
                                (employeeId == loanData.sEmployeeLoanOfficerAssistantId) ? "Loan Officer Assistant"   : null,
                                (employeeId == loanData.sEmployeePurchaserId           ) ? "Purchaser"                : null,
                                (employeeId == loanData.sEmployeeQCComplianceId        ) ? "QC Compliance"            : null,
                                (employeeId == loanData.sEmployeeSecondaryId           ) ? "Secondary"                : null,
                                (employeeId == loanData.sEmployeeServicingId           ) ? "Servicing"                : null,
                                (employeeId == loanData.sEmployeeExternalSecondaryId   ) ? "Secondary (External)"     : null,
                                (employeeId == loanData.sEmployeeExternalPostCloserId  ) ? "Post-Closer (External)"   : null,
                            }
                            .Where(r => r != null)
                            .OrderBy(s => s)
                            ),
                    });
                }
            }
            return users;
        }

    }
}
