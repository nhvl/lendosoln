<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskPermissionPicker.aspx.cs" Inherits="PriceMyLoan.main.Tasks.TaskPermissionPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Task Permission Picker</title>
</head>
<body>
<link href="<%= AspxTools.HtmlString(StyleSheet) %>" type="text/css" rel="stylesheet" />
<form id="form1" runat="server">
    <div lqb-popup>
        <div class="modal-header">
            <h4 class="modal-title">Select task permission level</h4>
        </div>
        <div class="modal-body">
            <table class="DataGrid modal-table-scrollbar">
                <thead><tr class="GridHeader">
                    <th>Permission</th>
                    <th>Description</th>
                    <th>Can be Condition</th>
                </tr></thead>
                <tbody><%--<tr>
                    <td><a onclick="selectPermission(${i})">${d.Name}</a></td>
                    <td>${d.Description}</td>
                    <td>${d.IsAppliesToConditions ? "yes" : "no"}</td>
                </tr>--%></tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" id="m_Cancel" class="btn btn-flat modal-close">Cancel</button>
        </div>
    </div>
</form>
<script>
function _init() {
    resizeForIE6And7(600, 400);
}
(function($){
    var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    var iframeSelf = new IframeSelf();
    iframeSelf.popup = popup;

    document.querySelector("table tbody").innerHTML = TaskPermissionPickerViewModel.data.map(function(d, i) {
        return ("<tr>\n                    <td><a data-index=\"" + i + "\">" + d.Name + "</a></td>\n                    <td>" + d.Description + "</td>\n                    <td>" + (d.IsAppliesToConditions ? "yes" : "no") + "</td>\n                </tr>");
    }).join("");
    TPOStyleUnification.fixedHeaderTable(document.querySelector("table"));

    $("table tbody").on("click", "a[data-index]", function(event) {
        var index = Number(event.currentTarget.dataset["index"]);
        var d = TaskPermissionPickerViewModel.data[index];
        iframeSelf.resolve({
            id                    : d.Id,
            name                  : d.Name,
            permissionDescription : d.Description,
            conditionsAllowed     : d.IsAppliesToConditions,
            OK                    : true,
        });
    });

    $("button.modal-close").click(function(){ iframeSelf.resolve(null) });
})(jQuery);
</script>
</body>
</html>
