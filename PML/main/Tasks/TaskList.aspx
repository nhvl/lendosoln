﻿<%@ Page
    Language="C#"
    AutoEventWireup="false"
    CodeBehind="TaskList.aspx.cs"
    Inherits="PriceMyLoan.main.Tasks.TaskList"
    MaintainScrollPositionOnPostback="true" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Task List</title>

    <style>
        .TasksTitle
        {
            padding: 5px;
            margin: 5px 0 5px 0;
            font-size: 1.1em;
        }

        #TopButtons
        {
            width: 100%;
            position: relative;
            padding: 5px;
            height: 2.5em;
        }

        #TopButtonsLeftSide
        {
            white-space: nowrap;
        }

        #TopButtonsRightSide
        {
            white-space: nowrap;
            text-align: right;
        }

        #BatchButtons
        {
            padding-top: 16px;
        }

        .AlignLeft
        {
            text-align: left;
        }

        .WarnOnResolveColumn
        {
            display: none;
        }

        #ResolveErrorTable
        {
            border-collapse: collapse;
            color: black;
            margin-top: 10px;
            width: 100%;
            background-color: white;
        }

            #ResolveErrorTable td
            {
                border: solid 1px black;
                padding: 2px;
            }

        .ui-widget-content
        {
            background-color: white !important;
        }

        .ui-widget-header
        {
            border-color: #2E4160 !important;
            background: #495B79 !important;
        }

        .Task .DocColumn
        {
            display: none;
        }

        .DocColumn table
        {
            width: 100%;
            border: none;
        }

            .DocColumn table td
            {
                border: none !important;
            }

        .DocBtnsContainer + .AssociatedDocsContainer
        {
            /*border-top: 1px solid #c5c5c5;
            margin-top: .5em;
            padding-top: .5em;*/
        }

        .associatedDoc
        {
            color: green;
        }

        .requiredDoc
        {
            color: #EA1C0D; // red color class text-danger
        }

        .requiredDocType
        {
            /*font-weight: bold;*/
            margin-right: 15px;
        }

        table.FullWidth {
            width: 100%;
        }

        div.CenterAlign {
            margin: 0 auto;
            width: 90%;
        }

        table#TaskTable {
            clear: right;
        }

        .btnDiv {
            margin-bottom: 0;
            display: flex;
            flex-direction: row-reverse;
        }
        .DocBtnsContainer>div:first-child {
            margin-bottom: 8px;
        }
    </style>
</head>
<body class="NotPML NoLinkUnderline LinkHoverUnderline ViewerBody">
<form runat="server">
<asp:HiddenField ID="m_isTemplate" runat="server" />
<uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"/>

<script>
    <!--

    var allCB = "selectAllCB";
    var CBName = "taskCB_name";
    var CBInput = "input[name='"+CBName+"']";
    var CBSelected = CBInput+":checked";
    var rootUrl = <%= AspxTools.JsString(Tools.VRoot) %>;
    var subUrl = "/main/Tasks/";
    var taskSearchTextbox = "taskSearchTextbox";
    var taskViewerLink = subUrl + "TaskViewer.aspx";
    var taskEditorLink = subUrl + "TaskEditorV2.aspx";
    var batchButtonsDisabled = true;
    var taskDialogOptions = 'width=750,height=400,center=yes,resizable=yes,scrollbars=yes,status=no,help=no';

    var batchButtonsDisabled = true;

    // Mark every checkbox if the top checkbox is clicked
    function selectAllCB_onClick() {
        if ($("#"+allCB).prop('checked')) {
            $(CBInput).prop('checked', true);
        } else {
            $(CBInput).prop('checked', false);
        }
        if ($(CBSelected).length <= 0) {
            $("#BatchButtons button").prop('disabled', true);
            batchButtonsDisabled = true;
        } else {
            $("#BatchButtons button").prop('disabled', false);
            batchButtonsDisabled = false;
        }
        // highlight the rows
        $(CBInput).each( function() {
            $(this).change();
            highlightRowByCheckbox(this);
        });
    }

    // Action to take when marking a single checkbox
    function taskCB_onClick(checkbox) {
        highlightRowByCheckbox(checkbox);
        $("#"+allCB).prop('checked', false);
        if ($(CBSelected).length == 0) {
            $("#BatchButtons button").prop('disabled', true);
            batchButtonsDisabled = true;
        } else {
            $("#BatchButtons button").prop('disabled', false);
            batchButtonsDisabled = false;
        }
    }

    // ##### Top operations! ####################

    $(document).on("click", ".associateBtn", AssociateDocs);

    function UploadDocs(dropzone)
    {
        if (getFilesCount(dropzone.id) === 0) {
            // No files selected by the user, e.g.
            // the file types selected were invalid.
            return;
        }

        var taskId = $(dropzone).attr("taskId");
        ConditionDocumentUploadHelper.UploadDocs(dropzone, ML.LoanId, taskId, true/*isPml*/);
    }

    function documentUploadPostUploadCallback(dropzoneId, taskId, newDocuments) {
        var docsJSON = $('#' + dropzoneId).parent().parent().attr("associatedDocs") || '[]';
        var docs = JSON.parse(docsJSON);

        var newDocumentIds = newDocuments.map(function (newDoc) { return newDoc.DocumentId; });
        SaveDocAssociations(docs.concat(newDocumentIds), taskId);
    }

    function AssociateDocs()
    {
        var taskId = $(this).attr("taskId");
        var url = '../EDocs/EDocPicker.aspx?loanid=' + <%= AspxTools.JsString(LoanID)%> + '&taskId=' + taskId;
        var args = {
            docs: $(this).parent().parent().attr("associatedDocs")
        };

        IframePopup.show(url, { args: args }).then(function(pair) {
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result == null) return;
                var docIds = result.docIds;
                var docTypeIds = result.docTypeIds;

                SaveDocAssociations(docIds, taskId);
            }
        });
    }

    function SaveDocAssociations(DocsToBeAssociated, TaskId)
    {
        var args = {
            DocsToBeAssociated: JSON.stringify(DocsToBeAssociated),
            TaskId: TaskId
        }
        gService.taskEditor.call("AssociateDocs", args);
        taskList_refresh();
    }

    // This should show the task list's new state if it's been updated outside
    function taskList_refresh() {
        //  This has to be refresh, or we'll run into double Post Submissions.
        self.location.href = self.location.href;
    }

    function showTaskViewer(taskID) {
        showTaskDialog(taskID, false, false);
    }

    function showTaskCreator() {
        showTaskDialog('', true, false);
    }

    function showTaskDialog(taskID, isCreator, isTp) {
        var link;
        var windowName;
        var queryString;
        if (isCreator) { // creating a new task does not require a taskID
            link = taskEditorLink;
            queryString =
                '?loanid=' + <%= AspxTools.JsString(LoanID) %> +
                    '&IsTemplate=' + $('#m_isTemplate').val();
            windowName = '_blank';
        } else {
            link = taskViewerLink;
            queryString =
                '?loanid=' + <%= AspxTools.JsString(LoanID) %> +
                    '&IsTemplate=' + $('#m_isTemplate').val();
            if (taskID !== null) {
                queryString += '&taskid=' + taskID;
            }
            if (isTp) {
                queryString += "&tp=1";
            }
            windowName = 'PMLtaskWindow__' + taskID;
        }
        var url = VRoot + link + queryString;

        IframePopup.show(url, { }).then(function(pair) {
            var error = pair[0];
            var isEdit = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (isEdit == null) return;
                if (isCreator || isEdit) {
                    window.location.href = window.location.href;
                }
            }
        });
    }

    function addNewTask_onClick() {
        showTaskCreator();
    }

    function taskSearchBtn_onClick() {
        var loanId = <%= AspxTools.JsString(LoanID) %>;
        var taskId = document.getElementById("taskSearchTextbox").value.trim();

        // OPM 69246 We have removed this behavior for now
        //var listurl = "TaskList.aspx?taskId=" + taskId + "&fromLoanId="+loanId;
        //var popupWin = window.open(listurl);
        //popupWin.focus();

        var url = "TaskViewer.aspx?taskId=" + taskId;
        IframePopup.show(url, {}).then(function(pair) {
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result == null) return;
            }
        });

        $(this).blur();
    }

    function refresh_onClick() {
        taskList_refresh();
    }
    function getCheckList() {
        var $CB = $(CBSelected);
        var taskIDs = [];
        $CB.each(function() {
            taskIDs.push(this.value.split(':')[0]);
        });
        return taskIDs;
    }

    function getWarningMessage(CBSelector)
    {
        var warning = "";
        $(CBSelector).each(function(){
            if($(this).closest('tr').find(".WarnOnResolveColumn").text() == "True")
            {
                warning += this.value.split(':')[0] + "<br/>";
            }
        });

        return warning;
    }

    function submitToConditionReview() {
        if (ML.HideStatusSubmissionButtonsInOriginatorPortal) {
            return;
        }

        simpleDialog.confirm('Are you sure you would like to <strong>Submit to Condition Review</strong>?').then(function(isConfirm){
            if (isConfirm) {
                performConditionReviewSubmission();
            }
        });
    }

    function performConditionReviewSubmission()
    {
        var params = {
            sLId: <%=AspxTools.JsString(this.LoanID)%>,
            status: <%=AspxTools.JsNumeric(DataAccess.E_sStatusT.Loan_ConditionReview)%>
        };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VRoot + '/main/PipelineService.aspx/ChangeLoanStatus',
            data: JSON.stringify(params),
            dataType: 'json',
            async: false,
            success: function () { postConditionReviewCallback(true); },
            error: function ()  { postConditionReviewCallback(false); }
        });
    }

    function postConditionReviewCallback(successful) {
        var message = 'Submission to "Condition Review" status ' + (successful ? 'successful.' : 'failed.');
        simpleDialog.alert(message).then(function () {
            if (!successful) {
                return;
            }

            if (ML.HideStatusAndAgentsPageInOriginatorPortal) {
                $('#SubmitToConditionReviewButton').hide();
            }
            else {
                window.location.href = VRoot + '/webapp/StatusAndAgents.aspx?loanid=' + <%=AspxTools.JsString(this.LoanID)%>;
            }
        });
    }

    // ##### Batch operations! ##################

    function PerformOperation(opName, data) {
        if (data == null) {
            data = { 'taskIds': getCheckList().join(',') };
        }

        var resolutionBlockTriggerNames = [];
        var dateResolutionFieldIds = [];

        var $CB = $(CBSelected);
        $CB.each(function() {
            values = this.value.split(':');
            resolutionBlockTriggerNames.push(values[2]);
            dateResolutionFieldIds.push(values[3]);
        });

        data['resolutionBlockTriggerNamesRawString'] = resolutionBlockTriggerNames.join(',');
        data['resolutionDateFieldIdsRawString'] =  dateResolutionFieldIds.join(',');
        data['loanId'] = <%= AspxTools.JsString(LoanID)%>;

            var strParams = JSON.stringify(data);
        var resp = '';
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../PipelineService.aspx/' + opName,
            data: strParams,
            dataType: 'json',
            async: false,
            success: function(response) { resp = response.d;},
            failure: function(response) { alert("error - " + response); },
            error: function(xhr, textStatus, errorThrown)  {
                var err = JSON.parse(xhr.responseText);
                alert(err.Message);
            }
        });

        return resp;
    }

    function assign_onClick(loanId) {
        url = VRoot + subUrl + 'RoleAndUserPicker.aspx?IsBatch=' + true
            + '&IsTemplate=' + false
            + '&loanid='+ loanId;
        IframePopup.show(url, { }).then(function(pair) {
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result == null) return;
                if (result.OK) {
                    var data = {
                        taskIds: getCheckList().join(','),
                        id     : result.id,
                        type   : result.type
                    };
                    var msg = PerformOperation('Assign', data);
                    if(msg.length > 0)
                    {
                        alert(msg);
                        return;
                    }
                    taskList_refresh();
                }
            }
        });
    }

    function reactivate(e)
    {
        var el = $(this);
        var url = "TaskEditorV2.aspx?loanId=<%=AspxTools.HtmlString(LoanID.ToString())%>&taskId="+el.attr("taskId")+"&mode=activate";

        IframePopup.show(url, { }).then(function(pair){
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result == null) return;
                window.location.reload();
            }
        });
    }

    function resolve_Batch()
    {
        var taskIds = [];
        var taskVersions = [];

        var $CB = $(CBSelected);
        $CB.each(function() {
            values = this.value.split(':');
            taskIds.push(values[0]);
            taskVersions.push(values[1]);
        });

        var message = getWarningMessage(CBSelected);

        resolve_Confirm(message, taskIds, taskVersions);

    }

    function resolve_Single(e)
    {
        var el = $(this);
        var taskIds = [el.attr("taskId")];
        var taskVersions = [el.attr("taskVersion")];

        var message = getWarningMessage("#" + el.attr("taskId"));

        resolve_Confirm(message, taskIds, taskVersions);
    }

    function resolve_Confirm(message, taskIds, taskVersions) {
        var data = {
            'taskIds': taskIds.join(','),
            'taskVersions': taskVersions.join(',')
        };

        if(message != '' )
        {
            $(".TaskListWithDocs").html(message);
            LQBPopup.ShowElementWithWrapper($("#ConditionDocReqDialog"),{
                width:400,
                backdrop:'static',
                closeText:'Cancel',
                buttons:[{'OK' :function() { performResolve(data);}
                }]
            });
        }
        else
        {
            performResolve(data);
        }
    }

    function performResolve(data)
    {
        var msg = PerformOperation('Resolve', data);
        if(msg.length > 0)
        {
            var errorList = $.parseJSON(msg);
            var errorTable = $("#ResolveErrorTable");
            for(i = 0; i < errorList.length; i++)
            {
                var errorProp = errorList[i];
                var row = $("<tr></tr>");

                for(j = 0; j < 3; j++)
                {
                    var td = $("<td></td>");
                    td.text(errorProp[j]);
                    row.append(td);
                }

                errorTable.append(row);
            }
            LQBPopup.ShowElement($("#ResolveErrorDiv"),{
                backdrop:'static',
                width:600,
                height:400,
                hideCloseButton:true,
                onClose:function(){taskList_refresh();},
                buttons:[{"Ok": function(){taskList_refresh();}
                }],
            });
            return;
        }
        else
        {
            taskList_refresh();
        }
    }

    function setFollowupDate_onClick() {
        url = VRoot + subUrl + 'DateSelector.aspx';

        IframePopup.show(url, {}).then(function(pair) {
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result.OK) {
                    var data = {
                        'taskIds': getCheckList().join(','),
                        'date': result.date
                    };
                    PerformOperation('SetFollowupDate', data);
                    taskList_refresh();
                }
            }
        });


    }

    (function($){
        var EnableConditionMode = <%= AspxTools.JsBool(EnableConditionMode) %>;

        $(function(){
            var $batchButtons = $("#BatchButtons");
                $batchButtons.toggleClass("task-batch-buttons", $batchButtons.find("button").length >= 7);

            $('#SubmitToConditionReviewButton').toggle(!ML.HideStatusSubmissionButtonsInOriginatorPortal);

            var btnExportToCSV = document.getElementById("btnExportToCSV");
            $(btnExportToCSV).on("click", f_ExportToCSV);

            function f_ExportToCSV() {
                btnExportToCSV.blur();
                var data = {
                    taskIds       : getCheckList().join(','),
                    conditionmode : EnableConditionMode,
                    all           : false
                };
                var strParams = JSON.stringify(data);
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../PipelineService.aspx/' + 'ExportAllTasksToCSV',
                    data: strParams,
                    dataType: 'json',
                    success: function(response) {
                        download(EnableConditionMode ? "ConditionList.csv" : "TaskList.csv", response.d, "text/csv");
                        btnExportToCSV.disabled = false;
                    },
                    failure: function(response) {
                        alert("error - " + response);
                        btnExportToCSV.disabled = false;
                    },
                    error: function(xhr, textStatus, errorThrown)  {
                        var err = JSON.parse(xhr.responseText);
                        alert(err.Message);
                        btnExportToCSV.disabled = false;
                    }
                });

                btnExportToCSV.disabled = true;
            }

            // C:\LendOSoln\LendingQB.Website.LOS\ts-src\utils\download.ts
            function download(filename, text, mime) {
                if (mime === void 0) { mime = "text/plain"; }
                if (navigator.msSaveBlob != null) {
                    navigator.msSaveBlob(new Blob([text]), filename);
                    return;
                }
                var uriContent = "data:" + mime + ";charset=utf-8," + encodeURIComponent(text);
                var element = document.createElement("a");
                element.setAttribute("href", uriContent);
                element.setAttribute("download", filename);
                element.style.display = "none";
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            }
        });
    })(jQuery);

    function exportToPDF_onClick(loanId) {
        var data = {
            loanId        : loanId,
            conditionmode : false,
            taskIDs       : getCheckList().join(',')
        };

            <% if (EnableConditionMode)
               { %>
        data.conditionmode = true;
            <% } %>
        var filePath = PerformOperation('ExportTasksToPDF', data);
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(filePath);        
    }

    function subscribe_onClick() {
        var label = <%= AspxTools.JsString(EnableConditionMode ? "conditions" : "tasks") %>;
        simpleDialog.confirm('Are you sure you want to subscribe to the selected '+ label+'?', "", "Yes", "No").then(function(isConfirm){
            if (!isConfirm) return;
            PerformOperation('SubscribeToTasks');
        });
    }
    function unsubscribe_onClick() {
        var label = <%= AspxTools.JsString(EnableConditionMode ? "conditions" : "tasks") %>;
        simpleDialog.confirm('Are you sure you want to un-subscribe from the selected '+ label+'?', "", "Yes", "No").then(function(isConfirm){
            if (!isConfirm) return;
            PerformOperation('UnsubscribeTasks');
        });
    }
        function renderSupportingDocuments(){
            $('div[support-document]').each(function(){
                var $requiredLabel=$(this).find('#RequiredLabel');
                var $requiredNone=$(this).find('#RequiredNone');
                var $associatedLabel=$(this).find('#AssociatedLabel');
                var $associatedNone=$(this).find('#AssociatedNone');
                var $requiredDocTypeContainer=$(this).find('[id$=RequiredDocTypeContainer]');
                var $associatedDocsContainer=$(this).find('[id$=AssociatedDocsContainer]');
                var $DocButtons=$(this).find('[id$=DocButtons]');
                var $statusImage=$(this).find('[id$=reqStatusImage]');

                var isDocuButtonsExists = ($DocButtons.length !== 0);
                var isRequiredDocTypeExists=($requiredDocTypeContainer.length !== 0);
                var isAssociatedDocsExists= ($associatedDocsContainer.length !== 0);

                $(this).find('hr').toggle(isDocuButtonsExists);
                $statusImage.toggle(isDocuButtonsExists && isRequiredDocTypeExists);
                $requiredLabel.toggle(isDocuButtonsExists);
                $requiredNone.toggle(isDocuButtonsExists && !isRequiredDocTypeExists);
                $associatedLabel.toggle(isDocuButtonsExists);
                $associatedNone.toggle(isDocuButtonsExists && !isAssociatedDocsExists);
            });
        }

        // ##### Init! ##############################

        // just pretend it's the same as $(document).ready
        function _init() {
            TPOStyleUnification.Components();
            $(document).on("click",".btn-resolve",  resolve_Single);
            $(document).on("click", ".btn-reactivate", reactivate);

            $('.drag-drop-container').each(function (index, dropzone) {
                var dragDropSettings = {
                    compact: true,
                    isPmlParam: true,
                    onProcessAllFilesCallback: function () { UploadDocs(dropzone); },
                    blockUpload: !ML.CanUploadEdocs,
                    blockUploadMsg: ML.UploadEdocsFailureMessage,
                    hideFilesInFileListParam: true
                };

                registerDragDropUpload(dropzone, dragDropSettings);
            });

            // Stuff that should only be done when everything's loaded
            $("input[type='button']").prop("NoHighlight", true); // turn off button highlighting

            // Set the add new task href
            queryString =
                '?loanid=' + <%= AspxTools.JsString(LoanID) %> +
                '&IsTemplate=' + $('#m_isTemplate').val();


            var $taskSearchTextbox = $("#"+taskSearchTextbox);
            var $taskSearchBtn = $("#taskSearchBtn");
            $taskSearchBtn.on("click", taskSearchBtn_onClick);
            function updateSearchBtn(len) {
                $taskSearchBtn.prop('disabled', len <= 0);
            }
            $taskSearchTextbox.keypress(function(event){
                // Prevent users from entering invalid characters (we should still validate)
                // Case 64938 has a list of invalid characters
                var alphanumericRegex = /^[34679ACDEFGHJKLMNPRTWXY]$/i;
                if (!alphanumericRegex.test(String.fromCharCode(event.which))) {
                    return false;
                }
                return true;
            });
            $taskSearchTextbox.keyup(function(event) {
                // Click the search button when the user presses enter in the search textbox
                if (event.which === 13) {
                    $taskSearchBtn.click();
                }
                updateSearchBtn($taskSearchTextbox.val().length);
            });

            // If no checkboxes are selected, then disable all the batch operations
            $("#BatchButtons button").prop('disabled', $(CBSelected).length <= 0);

            updateSearchBtn($taskSearchTextbox.val().length);

            // Select the row of urgent items and bold the whole row
            // .DiscDue is given to a DueDate or FollowUpDate in the codebehind:GetDateWarningCss
            $(".DiscDue").closest(".GridItem, .GridAlternatingItem").css('font-weight', 'bold');
            //OPM 69201: And make due dates themselves red
            $(".DiscDue").css('color', 'Red');

            //455192
            renderSupportingDocuments();
        }

        $(window).resize(function(){
            $(".warp").css("min-width", ($("table.DataGrid").width()) + "px");
            var closeDiv = $(".td1-header");
        });
        // -->
</script>

<tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />
<div class="content-detail" name="content-detail">
    <div class="warp-section section-tasks-conditions">
        <div>
            <header class="page-header table-flex table-flex-app-info">
                <div><ml:EncodedLiteral runat="server" ID="HeaderTop2"/>s</div>
                <div class="btnDiv btn-sticky">
                    <button type="button" id="SubmitToConditionReviewButton" class="btn-btn-default" runat="server" style="display: none;" onclick="submitToConditionReview();">Submit to Condition Review</button>
                </div>
            </header>
            <div ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>
        </div>
        <fieldset class="content-tasklist">
            <table id="TopButtons" class="margin-bottom">
                <tr class="none-border none-hover-tr">
                    <td id="TopButtonsLeftSide" class="none-padding">
                        <button type="button" runat="server" id="AddTaskBtn" onclick="addNewTask_onClick();" class="btn btn-default">Add new task</button>
                        <button type="button" onclick="taskList_refresh();" class="btn btn-default" notforedit>Refresh</button>
                        &nbsp;
                        <span class="label-pipeline">
                            Display
                            <ml:EncodedLiteral runat="server" ID="HeadingOne">Task</ml:EncodedLiteral>
                            assigned to:
                        </span>
                        <asp:DropDownList runat="server" ID="m_assignmentFilter" AutoPostBack="true" OnSelectedIndexChanged="m_openResolvedTasksGV_SelectedFilterChanged"/>
                    </td>
                    <td id="TopButtonsRightSide" visible="false" class="none-padding">
                        <span class="label-pipeline"><ml:EncodedLiteral runat="server" ID="HeaderThree">Task</ml:EncodedLiteral>
                        #</span>
                        <asp:TextBox name="taskSearch" runat="server" EnableViewState="true" ID="taskSearchTextbox" NotForEdit/>
                        <span class="btn-search-tasks"><button type="button" name="taskSearch" id="taskSearchBtn" class="btn btn-default" notforedit>Search <i class="material-icons">&#xE8B6;</i></button></span>
                    </td>
                </tr>
            </table>

            <div id="openResolvedTasksDiv">
                <div class="header header-tasklist" runat="server" id="OpenTaskLabel">
                    Open Tasks
                </div>

                <asp:GridView
                    ID="m_openResolvedTasksGV"
                    runat="server"
                    OnRowDataBound="m_openResolvedTasksGV_OnDataBound"
                    AutoGenerateColumns="false"
                    EnableViewState="false"
                    CssClass='DataGrid width-full'>
                    <HeaderStyle CssClass="GridHeader AlignLeft" />
                    <AlternatingRowStyle CssClass="GridAlternatingItem HoverHighlight" />
                    <RowStyle CssClass="GridItem HoverHighlight" />

                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="CheckboxColumn" />
                            <HeaderTemplate>
                                <input type="checkbox" name="selectAllCB_name" id="selectAllCB" onclick="selectAllCB_onClick();" notforedit />
                            </HeaderTemplate>
                            <ItemStyle CssClass="CheckboxColumn" />
                            <ItemTemplate>
                                <input NoLabel type="checkbox" id="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>" name="taskCB_name" onclick="taskCB_onClick(this)"
                                    value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskRowVersion").ToString()) %>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ResolutionBlockTriggerName").ToString())%>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ResolutionDateSetterFieldId").ToString())%>'
                                    notforedit />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="StatusColumn" />
                            <HeaderTemplate>
                                <asp:LinkButton ID="LinkButton2"
                                    Text="Status"
                                    CommandName="Sort"
                                    CommandArgument="TaskStatus openResolvedTasksGV_StatusHeader"
                                    OnCommand="m_openResolvedTasksGV_Sorting"
                                    runat="server">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_StatusHeader" />
                            </HeaderTemplate>
                            <ItemStyle CssClass="StatusColumn" />
                            <ItemTemplate><span class="label-active"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CalcTaskStatus").ToString()) %></span>
                                <div><a runat="server" id="statusBtn" type="button"></a></div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderStyle-CssClass="CondIdCondCategoryIdColumn stacked-sort-cell">
                            <HeaderTemplate>
                                <div class="max-height-container">
                                    <asp:LinkButton
                                        runat="server"
                                        CommandName="Sort"
                                        CommandArgument="TaskId openResolvedTasksGV_TaskHeader"
                                        OnCommand="m_openResolvedTasksGV_Sorting"
                                        Text="Task" ID="TaskLink">
                                    </asp:LinkButton>
                                    <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_TaskHeader" data-associated-link="TaskLink" /> /<br />
                                    <asp:LinkButton
                                        ID="CondCategoryIdLink"
                                        runat="server"
                                        CommandName="Sort"
                                        CommandArgument="CondCategoryId openResolvedTasksGV_CondCategoryIdHeader"
                                        OnCommand="m_openResolvedTasksGV_Sorting"
                                        Text="Category">
                                    </asp:LinkButton>
                                    <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_CondCategoryIdHeader" data-associated-link="CondCategoryIdLink" />
                                </div>
                            </HeaderTemplate>
                            <ItemStyle CssClass="TaskIDColumn" />
                            <ItemTemplate>
                                <a href="javascript:showTaskViewer('<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>')" class="OpenTaskIDLink TaskViewerLink" title="Open the task viewer for this item.">

                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>
                                </a> /<br />
                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondCategoryId").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="WarnOnResolveColumn" />
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemStyle CssClass="WarnOnResolveColumn" />
                            <ItemTemplate><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "WarnOnResolve").ToString())%></ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="SubjectColumn" />
                            <HeaderTemplate>
                                <asp:LinkButton
                                    Text="Subject"
                                    CommandName="Sort"
                                    CommandArgument="TaskSubject openResolvedTasksGV_SubjectHeader"
                                    OnCommand="m_openResolvedTasksGV_Sorting"
                                    runat="server">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_SubjectHeader" />
                            </HeaderTemplate>
                            <ItemStyle CssClass="SubjectColumn" />
                            <ItemTemplate>
                                <a data="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString()) %>" href="javascript:showTaskViewer('<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>')" class="OpenTaskIDLink TaskViewerLink" title="Open the task viewer for this item.">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString()) %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="DocColumn" />
                            <HeaderTemplate>
                                <span>Supporting Documents</span>
                            </HeaderTemplate>
                            <ItemStyle CssClass="DocColumn" />
                            <ItemTemplate>
                                <div support-document>
                                    <div class="table none-padding-bottom none-padding-right">
                                        <div>
                                            <div>
                                                <div class="table table-font-default">
                                                    <div>
                                                        <div>
                                                            <label id="RequiredLabel"><b>Requirement:</b></label>
                                                        </div>
                                                        <div>
                                                            <span id="RequiredNone">None</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div >
                                                <i class="material-icons" runat="server" id="reqStatusImage">&#xE86C;</i>
                                                <span runat="server" id="reqStatusText"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="RequiredDocTypeContainer" runat="server">
                                        <span class="requiredDocType"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondRequiredDocType_rep").ToString()) %></span>
                                    </div>
                                    <hr/>
                                    <div class="table table-association">
                                        <div>
                                            <div><label id="AssociatedLabel"><b>Association:</b></label></div>
                                            <div><span id="AssociatedNone">None</span></div>
                                        </div>
                                    </div>
                                    <div class="AssociatedDocsContainer" id="AssociatedDocsContainer" runat="server">
                                        <asp:GridView ID="AssociatedDocsGrid" runat="server" AutoGenerateColumns="false" EnableViewState="false" OnRowDataBound="AssociatedDocsGrid_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemStyle CssClass="Unlink none-padding-left" />
                                                    <ItemTemplate>
                                                        <span runat="server" id="UnlinkContainer">
                                                            <a href="#" onclick="__doPostBack('unlink', '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString() + ":" + DataBinder.Eval(Container.DataItem, "DocId").ToString()) %>')">
                                                                <span class="span-unlink">
                                                                    <svg width="20" height="17" viewBox="0 0 20 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                        <title>Unlink</title>
                                                                        <g id="Canvas" transform="translate(-3123 -14214)">
                                                                            <g id="Group 2">
                                                                                <g id="Vector">
                                                                                    <use xlink:href="#unlink_path0_fill" transform="translate(3123 14218)" class="unlink-use" />
                                                                                </g>
                                                                                <g id="Line 2">
                                                                                    <use xlink:href="#unlink_path1_stroke" transform="matrix(0.707107 0.707107 -0.707107 0.707107 3124.8 14216.2)" class="unlink-use" />
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <defs>
                                                                            <path id="unlink_path0_fill" d="M 1.9 5C 1.9 3.29 3.29 1.9 5 1.9L 9 1.9L 9 0L 5 0C 2.24 0 0 2.24 0 5C 0 7.76 2.24 10 5 10L 9 10L 9 8.1L 5 8.1C 3.29 8.1 1.9 6.71 1.9 5ZM 6 6L 14 6L 14 4L 6 4L 6 6ZM 15 0L 11 0L 11 1.9L 15 1.9C 16.71 1.9 18.1 3.29 18.1 5C 18.1 6.71 16.71 8.1 15 8.1L 11 8.1L 11 10L 15 10C 17.76 10 20 7.76 20 5C 20 2.24 17.76 0 15 0Z" />
                                                                            <path id="unlink_path1_stroke" d="M 0 0L 20.5183 0L 20.5183 -2L 0 -2L 0 0Z" />
                                                                        </defs>
                                                                    </svg>
                                                                </span>unlink
                                                            </a>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocFolderType").ToString()) %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="DocBtnsContainer" id="DocButtons" runat="server">
                                        <div>
                                            <a class="associateBtn" taskid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%>">
                                            <i class="material-icons large-material-icons link">&#xE157;</i>associate previously uploaded</a>
                                        </div>
                                        <div class="InsetBorder full-width">
                                            <div id="DragDropZone" class="drag-drop-container" runat="server"></div>
                                        </div>
                                        <div class="drag-drop-error-div"></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="DateColumn" />
                            <HeaderTemplate>
                                <asp:LinkButton
                                    Text="Due Date"
                                    CommandName="Sort"
                                    CommandArgument="TaskDueDateComparison openResolvedTasksGV_DueDateHeader"
                                    OnCommand="m_openResolvedTasksGV_Sorting"
                                    runat="server">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_DueDateHeader" />
                            </HeaderTemplate>
                            <ItemStyle CssClass="DateColumn" />
                            <ItemTemplate>
                                <span class="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CalcTaskDueDateWarningCss").ToString()) %>">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskDueDate", "{0:MM/dd/yyyy}"))%>
                                </span>
                                <a
                                    style="display: <%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalcDisplay").ToString() ) %>"
                                    title="<%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalc").ToString() ) %>">CALC
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="AssignedToColumn" />
                            <HeaderTemplate>
                                <asp:LinkButton
                                    Text="Assigned To"
                                    CommandName="Sort"
                                    CommandArgument="AssignedUserFullName openResolvedTasksGV_AssignedToHeader"
                                    OnCommand="m_openResolvedTasksGV_Sorting"
                                    runat="server">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_AssignedToHeader" />
                            </HeaderTemplate>
                            <ItemStyle CssClass="AssignedToColumn" />
                            <ItemTemplate>
                                <div style="width: 8em; overflow: hidden;">
                                    <!-- IE appeasement -->
                                </div>
                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssignedUserFullName").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderStyle-CssClass="DueDateAssignedToColumn stacked-sort-cell">
                            <HeaderTemplate>
                                <div class="max-height-container">
                                    <asp:LinkButton
                                        ID="ConditionDueDateSortLink"
                                        Text="Due Date"
                                        CommandName="Sort"
                                        CommandArgument="TaskDueDateComparison openResolvedConditionsGV_DueDateHeader"
                                        OnCommand="m_openResolvedTasksGV_Sorting"
                                        runat="server">
                                    </asp:LinkButton>
                                    <ml:EncodedLabel runat="server" ID="openResolvedConditionsGV_DueDateHeader" data-associated-link="ConditionDueDateSortLink" /> /<br />
                                    <asp:LinkButton
                                        ID="ConditionAssignedToSortLink"
                                        Text="Assigned To"
                                        CommandName="Sort"
                                        CommandArgument="AssignedUserFullName openResolvedConditionsGV_AssignedToHeader"
                                        OnCommand="m_openResolvedTasksGV_Sorting"
                                        runat="server">
                                    </asp:LinkButton>
                                    <ml:EncodedLabel runat="server" ID="openResolvedConditionsGV_AssignedToHeader" data-associated-link="ConditionAssignedToSortLink" />
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CalcTaskDueDateWarningCss").ToString()) %>">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskDueDate", "{0:MM/dd/yyyy}"))%>
                                </span>
                                <a
                                    style="display: <%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalcDisplay").ToString() ) %>"
                                    title="<%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalc").ToString() ) %>">CALC
                                </a><br />

                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssignedUserFullName").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <div id="BatchButtons">
                <button type="button" id="assignBtn" onclick="assign_onClick(<%= AspxTools.JsString(LoanID)%>)" class="btn btn-default">Assign</button>
                <button type="button" id="resolveBtn" onclick="resolve_Batch()" class="btn btn-default">Resolve</button>
                <button type="button" id="setFollowupDateBtn" onclick="setFollowupDate_onClick()" class="btn btn-default">Set Follow-up Date</button>
                <button type="button" id="btnExportToCSV" class="btn btn-default">Export to CSV</button>
                <button type="button" id="exportToPDFBtn" onclick="exportToPDF_onClick(<%= AspxTools.JsString(LoanID)%>);$(this).blur();" class="btn btn-default">Export to PDF</button>

                <%-- Hide subscribe links if the user has opted out of e-mail notifications --%>
                <button runat="server" type="button" id="subscribeBtn" onclick="subscribe_onClick();$(this).blur();" class="btn btn-default">Subscribe</button>
                <button runat="server" type="button" id="unsubscribeBtn" onclick="unsubscribe_onClick();$(this).blur();" class="btn btn-default">Un-subscribe</button>
            </div>

            <div id="closedTasksDiv" class="padding-top-3em">
                <div class="header header-tasklist" runat="server" id="HeadingFour">Closed Tasks</div>
                <asp:GridView ID="m_closedTasksGV" runat="server"
                    OnRowDataBound="m_openResolvedTasksGV_OnDataBound" EnableViewState="false" AutoGenerateColumns="false"
                    CssClass="DataGrid none-selected-table width-full">
                    <HeaderStyle CssClass="GridHeader AlignLeft" />
                    <AlternatingRowStyle CssClass="GridAlternatingItem HoverHighlight" />
                    <RowStyle CssClass="GridItem HoverHighlight" />

                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="TaskIDColumn" />
                            <HeaderTemplate>
                                <asp:LinkButton
                                    Text="Task"
                                    CommandName="Sort"
                                    CommandArgument="TaskId closedTasksGV_TaskHeader"
                                    OnCommand="m_closedTasksGV_Sorting"
                                    runat="server" ID="TaskLink">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskHeader" />
                            </HeaderTemplate>
                            <ItemStyle CssClass="TaskIDColumn" />
                            <ItemTemplate>
                                <a href="javascript:showTaskViewer('<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>')" class="OpenTaskIDLink TaskViewerLink" title="Open the task viewer for this item.">

                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="CondCategoryIdColumn" />
                            <HeaderTemplate>
                                <asp:LinkButton ID="LinkButton1"
                                    runat="server"
                                    CommandName="Sort"
                                    CommandArgument="CondCategoryId closedTasksGV_CondCategoryIdHeader"
                                    OnCommand="m_closedTasksGV_Sorting"
                                    Text="Category">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="closedTasksGV_CondCategoryIdHeader" />
                            </HeaderTemplate>
                            <ItemStyle CssClass="CondCategoryIdColumn" />
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondCategoryId").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="TaskSubject">
                            <HeaderStyle CssClass="SubjectColumn" />
                            <HeaderTemplate>
                                <asp:LinkButton
                                    Text="Subject"
                                    CommandName="Sort"
                                    CommandArgument="TaskSubject closedTasksGV_TaskSubject"
                                    OnCommand="m_closedTasksGV_Sorting"
                                    runat="server">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskSubject" />
                            </HeaderTemplate>
                            <ItemStyle CssClass="SubjectColumn" />
                            <ItemTemplate>
                                <a data='  <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString()) %>' href="javascript:showTaskViewer('<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>')" class="OpenTaskIDLink TaskViewerLink" title="Open the task viewer for this item.">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString()) %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle CssClass="DocColumn" />
                            <HeaderTemplate>
                                <span>Supporting Documents</span>
                            </HeaderTemplate>
                            <ItemStyle CssClass="DocColumn" />
                            <ItemTemplate>
                                <div id="RequiredDocTypeContainer" runat="server">
                                    <img runat="server" id="reqStatusImage" />
                                    <span class="requiredDocType"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondRequiredDocType_rep").ToString()) %></span>
                                    <span runat="server" id="reqStatusText"></span>
                                </div>
                                <div class="DocBtnsContainer" id="DocButtons" runat="server">
                                    <button type="button" class="associateBtn" taskid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%>">Associate Previously Uploaded</button>
                                    <div class="InsetBorder full-width">
                                        <div id="DragDropZone" class="drag-drop-container" runat="server"></div>
                                    </div>
                                    <div class="drag-drop-error-div"></div>
                                </div>
                                <div class="AssociatedDocsContainer" id="AssociatedDocsContainer" runat="server">
                                    <asp:GridView ID="AssociatedDocsGrid" runat="server"
                                        AutoGenerateColumns="false" EnableViewState="false" OnRowDataBound="AssociatedDocsGrid_RowDataBound"
                                        CssClass="DataGrid">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemStyle CssClass="Unlink" />
                                                <ItemTemplate>
                                                    <span runat="server" id="UnlinkContainer">
                                                        <a onclick="__doPostBack('unlink', '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId") + ":" + DataBinder.Eval(Container.DataItem, "DocId")) %>')">unlink</a>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <span><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocFolderType").ToString()) %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:LinkButton
                                    Text="Last Updated"
                                    CommandName="Sort"
                                    CommandArgument="TaskLastModifiedDateComparison closedTasksGV_TaskLastModifiedDate"
                                    OnCommand="m_closedTasksGV_Sorting"
                                    runat="server">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskLastModifiedDate" Text=""/>
                            </HeaderTemplate>
                                        <HeaderStyle CssClass="DateColumn" />
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskLastModifiedDate", "{0:MM/dd/yyyy}"))%>
                            </ItemTemplate>
                                        <ItemStyle CssClass="DateColumn" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:LinkButton
                                    Text="Owner"
                                    CommandName="Sort"
                                    CommandArgument="OwnerFullName closedTasksGV_TaskOwnerFullName"
                                    OnCommand="m_closedTasksGV_Sorting"
                                    runat="server">
                                </asp:LinkButton>
                                <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskOwnerFullName" />
                            </HeaderTemplate>
                                        <HeaderStyle CssClass="AssignedToColumn" />
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OwnerFullName").ToString())%>
                            </ItemTemplate>
                                        <ItemStyle CssClass="AssignedToColumn" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>

        <div style="display: none;" id="ConditionDocReqDialog">
            The following conditions are missing an association with a required document type.  Are you sure you want to resolve?
            <br />
            <br />
            <div class="TaskListWithDocs" style="white-space: pre;"></div>
        </div>
        <div style="display: none;" id="ResolveErrorDiv">
            The selected operation failed for the following tasks:

            <table id="ResolveErrorTable">
                <tr class="GridHeader">
                    <td>Task</td>
                    <td>Subject</td>
                    <td>Reason</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</form>
</body>
</html>

