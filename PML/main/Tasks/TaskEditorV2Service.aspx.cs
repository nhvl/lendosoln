﻿namespace PriceMyLoan.main.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using PriceMyLoan.Security;

    public partial class TaskEditorV2Service : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CheckOwnerPermission":
                    CheckOwnerPermission();
                    break;
                case "SetDocRequestSatisfied":
                    SetDocRequestSatisfied();
                    break;
                case "AssociateDocs":
                    AssociateDocs();
                    break;
                case "UnlinkDoc":
                    this.UnlinkDoc();
                    return;
            }
        }

        private void CheckOwnerPermission()
        {
            Guid ownerID = GetGuid("ownerID");
            int permissionLevelID = GetInt("permissionLevelID");
            Guid brokerID = (HttpContext.Current.User as PriceMyLoanPrincipal).BrokerId;

            AbstractUserPrincipal principal = LendersOffice.Security.PrincipalFactory.RetrievePrincipalForUser(brokerID, ownerID, "?");
            if (principal == null)
            {
                SetResult("hideError", true);
                return;
            }

            // Permission level check
            List<Guid> UserGroups = new List<Guid>();
            foreach (Group ugroup in GroupDB.ListInclusiveGroupForEmployee(principal.BrokerId, principal.EmployeeId))
            {
                UserGroups.Add(ugroup.Id);
            }
            List<E_RoleT> Roles = new List<E_RoleT>();
            foreach (E_RoleT role in principal.GetRoles())
            {
                Roles.Add(role);
            }
            PermissionLevel selectedPermission = PermissionLevel.Retrieve(principal.BrokerId, permissionLevelID);
            SetResult("hideError", selectedPermission.IsRoleOrGroupInLevel(E_PermissionLevel.Manage, Roles, UserGroups));
        }

        private void AssociateDocs()
        {
            List<Guid> docIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(this.GetString("DocsToBeAssociated"));
            string taskId = this.GetString("TaskID");
            AbstractUserPrincipal principal = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;

            if (null == principal)
            {
                this.SetResult("error", "true");
                return;
            }

            Task task = Task.Retrieve(principal.BrokerId, taskId);
            task.UpdateDocumentAssociations(docIds);
            task.Save(false);
        }

        private void SetDocRequestSatisfied()
        {
            Guid loanId = GetGuid("LoanId");
            string taskId = GetString("TaskID");
            int RequiredDocumentTypeId = GetInt("RequiredDocumentTypeId");
            string DocsToBeAssociated = GetString("DocsToBeAssociated");
            string DocsToBeAssociatedTypeIds = GetString("DocsToBeAssociatedTypeIds");
            bool SaveAssociations = GetBool("SaveAssociations");

            AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;
            if (null == principal)
            {
                SetResult("error", "true");
                return;
            }

            if (SaveAssociations)
            {
                List<Guid> docIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(DocsToBeAssociated);
                Task task = Task.Retrieve(principal.BrokerId, taskId);
                task.UpdateDocumentAssociations(docIds);
                task.Save(false);
            }

            // Required doc type is "NONE", so return N/A
            if (RequiredDocumentTypeId == -1)
            {
                SetResult("DocRequestSatisfied", "N/A");
                SetResult("ResolveAction", "");
                return;
            }

            // Check if associated docs satisfy doc type requirements
            List<int> docTypeIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<int>>(DocsToBeAssociatedTypeIds);
            if (docTypeIds != null && docTypeIds.Contains(RequiredDocumentTypeId))
            {
                SetResult("DocRequestSatisfied", "Yes");
                SetResult("ResolveAction", "");
            }
            else
            {
                SetResult("DocRequestSatisfied", "No");
                
                Task task;
                if (string.IsNullOrEmpty(taskId)) // new task
                    task = Task.Create(loanId, principal.BrokerId);
                else
                    task = Task.Retrieve(principal.BrokerId, taskId);

                E_UserTaskPermissionLevel permissionLevel = task.GetPermissionLevelFor(principal);
                if (permissionLevel == E_UserTaskPermissionLevel.Manage)
                {
                    SetResult("ResolveAction", "Confirm");
                }
                else
                {
                    SetResult("ResolveAction", "Block");
                }
            }
        }

        private void UnlinkDoc()
        {
            var docId = this.GetGuid("DocumentId");
            var taskId = this.GetString("TaskId");

            var task = Task.Retrieve(LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId, taskId);

            task.UpdateDocumentAssociations(task.AssociatedDocs.Where(doc => doc.DocumentId != docId).Select(doc => doc.DocumentId));
            task.Save(false);
        }
    }
}
