﻿namespace PriceMyLoan.main.Tasks
{
    using System;
    using System.Collections.Generic;

    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LendersOffice.AntiXss;

    public partial class TaskViewer : PriceMyLoan.UI.BasePage
    {
        
        protected string TaskID
        {
            get { return RequestHelper.GetSafeQueryString("taskId"); }
        }

        protected Guid LoanID_Override
        {
            get;
            set;
        }

        protected bool IsTemplate
        {
            get { return RequestHelper.GetBool("IsTemplate"); }
        }

        protected AbstractUserPrincipal BrokerUser
        {
            get
            {
                AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;
                if (null == principal)
                    RequestHelper.Signout();

                return principal;
            }
        }

        protected EmployeeDB CurrentEmployee
        {
            get
            {
                var principal = BrokerUser;
                EmployeeDB currentUserEmployee = new EmployeeDB(principal.EmployeeId, principal.BrokerId);
                currentUserEmployee.Retrieve();
                return currentUserEmployee;
            }
        }

        protected bool IsCondition { get; private set; }

        protected bool ShowResolutionSetting { get; private set; }

        protected string ErrorMessage { get; private set; }


        private void PageInit(object sender, System.EventArgs e)
        {
          
            this.PageTitle = "Viewer"; 
            LoanID_Override = LoanID;

            Task task = null;
            try
            {
                task = Task.Retrieve(BrokerUser.BrokerId, TaskID);
            }
            catch (TaskNotFoundException)
            {
                LoanID_Override = Guid.Empty;
                DisplayError("The selected task does not exist.");
                return;
            }
            catch (TaskPermissionException)
            {
                LoanID_Override = Guid.Empty;
                DisplayError("You do not have access to the selected task.");
                return;
            }
            if (task == null) return;

            this.RegisterJsGlobalVariables("EnableNewTpoLoanNavigation", this.PriceMyLoanUser.EnableNewTpoLoanNavigation);
            this.RegisterJsGlobalVariables("NeedRefresh", RequestHelper.GetBool("NeedRefresh"));

            if (task.TaskIsCondition)
            {
                m_Header.Text = "Condition ";
                TaskWording1.Text = "condition";
                TaskWording2.Text = "condition";
                TaskOwnerLabel.InnerText = "Condition Owner";
                TaskPermissionLabel.InnerText = "Condition Permissions";
            }
            else
            {
                m_Header.Text = "Task ";
            }
            m_Header.Text += task.TaskId; 
            Subject.Text = task.TaskSubject;
            BorrowerName.Text = task.BorrowerNmCached;
            TemplateOwner.Text = TaskOwner.Text = task.OwnerFullName;
            TemplateAssigned.Text = Assigned.Text = task.AssignedUserFullName;
            sLNm.Text = task.LoanNumCached;
            Status.Text = task.TaskStatus_rep;
            DueDateCalcDescription.Text = task.DueDateDescription(true);
            History.Text = task.TaskHistoryHtml;
            DueDate.Text = task.TaskDueDate_rep;

            if (RequestHelper.GetBool("tp"))
            {
                if (!task.TaskIsCondition)
                {
                    GoBackLink.InnerText = "Back to Task List";
                }
                GoBackLink.Visible = true;
                GoBackLink.HRef = string.Format("TaskList.aspx?LoanId={0}&cv={1}", AspxTools.HtmlString(task.LoanId), AspxTools.HtmlString(task.TaskIsCondition));
            }

            // Make the dates more visible if they are urgent
            if (task.TaskDueDate.HasValue && task.TaskDueDate.Value <= DateTime.Today && task.TaskStatus == E_TaskStatus.Active)
            {
                DueDate.CssClass = "text-danger";
            }
            FollowupDate.Text = task.TaskFollowUpDate_rep;
            if (task.TaskFollowUpDate.HasValue && task.TaskFollowUpDate.Value <= DateTime.Today && task.TaskStatus == E_TaskStatus.Active)
            {
                FollowupDate.CssClass = "text-danger";
            }

            ConditionCategoryChoice.Text = task.CondCategoryId_rep;
            TaskPermission.Text = task.PermissionLevelName;
            PermissionDescription.Title = task.PermissionLevelDescription;
            RequiredDocTypeSection.Visible = BrokerDB.RetrieveById(BrokerUser.BrokerId).IsEDocsEnabled && task.CondRequiredDocTypeId.HasValue;
            RequiredDocumentTypeName.Text = task.CondRequiredDocType_rep; 

            if (TaskSubscription.IsSubscribed(BrokerUser.BrokerId, task.TaskId, BrokerUser.UserId))
            {
                IsNotSubscribed.Style.Add("display", "none");
            }
            else
            {
                IsSubscribed.Style.Add("display", "none");
            }

            bool isTaskActive = task.TaskStatus == E_TaskStatus.Active;
            bool isTaskResolved = task.TaskStatus == E_TaskStatus.Resolved;
            bool isTaskClosed = task.TaskStatus == E_TaskStatus.Closed;
            E_UserTaskPermissionLevel permissionLevel = task.GetPermissionLevelFor(BrokerUser);

            m_activateLink.Visible = !IsTemplate &&
                                     (isTaskClosed || isTaskResolved);

            m_assignLink.Visible = !IsTemplate &&
                                   isTaskActive;

            m_closeLink.Visible = !IsTemplate &&
                                  isTaskResolved &&
                                  (permissionLevel == E_UserTaskPermissionLevel.Manage || permissionLevel == E_UserTaskPermissionLevel.Close);

            m_editLink.Visible = isTaskActive ||
                                 isTaskResolved;

            m_emailLink.Visible = !IsTemplate &&
                                  isTaskActive &&
                                  !task.HasEmailEntry;

            m_replyLink.Visible = !IsTemplate &&
                                  isTaskActive &&
                                  task.HasEmailEntry;

            m_forwardLink.Visible = !IsTemplate &&
                                    isTaskActive &&
                                    task.HasEmailEntry;

            m_resolveLink.Visible = !IsTemplate &&
                                    isTaskActive;

            // Subscription link visibility
            if (CurrentEmployee.TaskRelatedEmailOptionT != E_TaskRelatedEmailOptionT.ReceiveEmail)
            {
                subscriptionArea.Visible = false;
            }

            LoanID_Override = task.LoanId;

            ClientScript.GetPostBackEventReference(this, "");

            ShowResolutionSetting = !string.IsNullOrEmpty(task.ResolutionBlockTriggerName) || !string.IsNullOrEmpty(task.ResolutionDateSetterFieldId);
            if (string.IsNullOrEmpty(task.ResolutionBlockTriggerName))
            {
                ResolutionBlockTriggerNameLabel.Text = "None";
                ResolutionBlockTriggerExplainLink.Visible = false;
            }
            else
            {
                ResolutionBlockTriggerNameLabel.Text = task.ResolutionBlockTriggerName;
            }

            ResolutionBlockTriggerExplainLink.Title = task.ResolutionDenialMessage;

            ResolutionDateSetterFieldDescription.Text = task.GetResolutionDateSetterFieldDescription();

            IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> triggers = TaskTriggerAssociation.GetTaskTriggerAssociations(task);

            this.RegisterJsGlobalVariables("HasAutoTriggers", triggers.ContainsKey(TaskTriggerType.AutoResolve) || triggers.ContainsKey(TaskTriggerType.AutoClose));
            AutoResolveTriggerLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoResolve) ? triggers[TaskTriggerType.AutoResolve].TriggerName : "None";
            AutoCloseTriggerLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoClose) ? triggers[TaskTriggerType.AutoClose].TriggerName : "None";
        }

        protected override void OnInit(EventArgs e)
        {
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
            RegisterService("loanedit", url);
            this.Init += this.PageInit;
            base.OnInit(e);
        }

        private void DisplayError(string msg)
        {
            pnlTaskViewer.Visible = false;
            ErrorMessage = msg;
        }
    }
}
