﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using LendersOffice.Admin;

namespace PriceMyLoan.main.Tasks
{
    public partial class TaskPermissionPicker : PriceMyLoan.UI.BasePage
    {

        private string TaskID
        {
            get { return RequestHelper.GetSafeQueryString("TaskID"); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsObjectWithJsonNetAnonymousSerializer("TaskPermissionPickerViewModel", new
            {
                data = Search(TaskID)
                    .Select(task => new { task.Id, task.Description, task.Name, task.IsAppliesToConditions })
                    .ToArray(),
            });
        }

        static IEnumerable<PermissionLevel> Search(string TaskID)
        {
            var CurrentPrincipal = PrincipalFactory.CurrentPrincipal;
            var BrokerId = CurrentPrincipal.BrokerId;
            if (CurrentPrincipal == null) return null;


            var UserGroups = GroupDB.ListInclusiveGroupForEmployee(BrokerId, CurrentPrincipal.EmployeeId).Select(ugroup => ugroup.Id).ToList();
            var Roles = CurrentPrincipal.GetRoles().ToList();

            var permissionLevels = PermissionLevel.RetrieveManageLevelsForUser(BrokerId, Roles, UserGroups);
            if (!string.IsNullOrEmpty(TaskID))
            {
                var task = Task.Retrieve(BrokerId, TaskID);
                if (task.TaskIsCondition)
                {
                    permissionLevels = permissionLevels.Where(p => p.IsAppliesToConditions);
                }
            }
            return permissionLevels.Where(p => p.IsActive);
        }
    }
}
