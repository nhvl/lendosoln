﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.ObjLib.Task;

namespace PriceMyLoan.main.Tasks
{
    public partial class DateCalculator : PriceMyLoan.UI.BasePage
    {
        public string duration
        {
            get
            {
                int d = Math.Abs(RequestHelper.GetInt("duration", 0));
                return d.ToString();
            }
        }

        public bool positiveDuration
        {
            get
            {
                return RequestHelper.GetInt("duration", 0) > -1;                
            }
        }

        public string fieldId
        {
            get
            {
                return RequestHelper.GetSafeQueryString("fieldId");
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("angular-1.4.8.min.js");
            this.RegisterJsScript("webapp/Tasks/DateCalculator.js");

            var parameters = Task.ListValidDateFieldParameters();

            SecurityParameter.Parameter param = null;
            if (!string.IsNullOrEmpty(fieldId))
            {
                try
                {
                    param = parameters.First(t => t.Id == fieldId);
                }
                catch (InvalidOperationException) { }
            }
            if (param == null)
            {
                param = parameters[0];
            }
            

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("DateCalculatorViewModel", new
            {
                test = string.Join("\n", parameters.Select(p => $"{p.FriendlyName} {p.Id}")),
                parameters = parameters.Select(p => new { p.Id, p.FriendlyName, p.CategoryName }).ToArray(),
                duration = duration,
                positiveDuration = positiveDuration,
                fieldId = param.Id,
                LoanID = LoanID,
            });
        }

        protected override void OnInit(EventArgs e)
        {
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
            RegisterService("main", url);
            base.OnInit(e);
        }
    }
}
