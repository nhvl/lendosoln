﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;

namespace PriceMyLoan.main.Tasks
{
    public partial class TaskViewerServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TaskViewerServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ChangeSubscription":
                    ChangeSubscription();
                    break;
                case "CloseTask":
                    CloseTask();
                    break;
            }
        }

        private void ChangeSubscription()
        {
            AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;
            Guid UserId = GetGuid("UserId");
            string TaskId = GetString("TaskId");
            bool ToSubscribe = GetBool("ToSubscribe");

            if (ToSubscribe)
            {
                TaskSubscription.Subscribe(principal.BrokerId, TaskId, UserId);
            }
            else
            {
                TaskSubscription.Unsubscribe(principal.BrokerId, TaskId, UserId);
            }
        }

        private void CloseTask()
        {
            AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;

            string TaskId = GetString("TaskId");
            Task task = Task.Retrieve(principal.BrokerId, TaskId);
            task.Close();
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
    public partial class TaskViewerService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new TaskViewerServiceItem());
        }
    }
}
