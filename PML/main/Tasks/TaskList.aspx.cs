﻿namespace PriceMyLoan.main.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using global::EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.ObjLib.DocumentConditionAssociation;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using PriceMyLoan.Security;

    public partial class TaskList : UI.BasePage
    {
        private enum OpenTaskConditionGridColumns
        {
            Checkbox = 0,
            Status = 1,
            ConditionIdCategoryId = 2,
            WarnOnResolve = 3,
            Subject = 4,
            SupportingDocs = 5,
            TaskDueDate = 6,
            TaskAssignedTo = 7,
            ConditionDueDateAssignedToCombined = 8
        }

        protected bool EnableConditionMode
        {
            get
            {
                string target = Request.Form["__EVENTTARGET"];
                if (target == "changeTab")
                {
                    ViewState["cv"] = 1 == Int32.Parse(Request.Form["__EVENTARGUMENT"]);
                    return 1 == Int32.Parse(Request.Form["__EVENTARGUMENT"]);
                }

                return (bool?) ViewState["cv"] ?? RequestHelper.GetBool("cv");
            }
        }

        private readonly string OPENRESOLVED_GRIDVIEWNAME = "OpenResolved";
        private readonly string OPENRESOLVED_DEFAULTSORT = "TaskDueDateComparison ASC, TaskFollowupDate ASC, TaskSubject ASC";
        private readonly string OPENRESOLVED_TASKFILTER = "TaskStatus <> " + (int)E_TaskStatus.Closed;
        private readonly string CLOSED_GRIDVIEWNAME = "Closed";
        private readonly string CLOSED_DEFAULTSORT = "TaskLastModifiedDate DESC, TaskSubject ASC";
        private readonly string CLOSED_TASKFILTER = "TaskStatus = " + (int)E_TaskStatus.Closed;
        private const string CONDFILTER_ALL = "ANYONE";
        private const string CONDFILTER_COND = "CONDITIONS";
        private const string CONDFILTER_NONCOND = "NONCONDITIONS";
        private const string FILTER_ANYBODY = "ANYBODY";
        private const string FILTER_ME = "ME";
        private const string FILTER_ROLE = "ROLE";
        private const string FILTER_USERID = "USERID";
        private const string FILTER_NOBODY = "NOBODY";
        private const int FILTER_DROPDOWN_LENGTH = 40;

        private bool _initError = false;

        /// <summary>
        /// One cookie is stored per loan for each of the dropdown filters
        /// </summary>
        private const string FilterCookie = "SelectedFilter";

        private Guid UserID
        {
            get
            {
                return ((PriceMyLoanPrincipal)Context.User).UserId;
            }
        }
        private Guid EmployeeID
        {
            get
            {
                return ((PriceMyLoanPrincipal)Context.User).EmployeeId;
            }
        }

        private Guid BrokerID
        {
            get
            {
                return ((PriceMyLoanPrincipal)Context.User).BrokerId;
            }
        }

        private int FileUploadLimit => 12;

        private string AcceptedFileExtensions
        {
            get
            {
                if (this.PriceMyLoanUser.BrokerDB.AllowExcelFilesInEDocs)
                {
                    return ".xml, .pdf, .xls, .xlsx";
                }

                return ".xml, .pdf";
            }
        }

        protected EmployeeDB CurrentEmployee
        {
            get
            {
                EmployeeDB currentUserEmployee = new EmployeeDB(EmployeeID, BrokerID);
                currentUserEmployee.Retrieve();
                return currentUserEmployee;
            }
        }

        private Dictionary<string, List<DocumentConditionAssociation>> associatedDocs = new Dictionary<string, List<DocumentConditionAssociation>>();

        private IEnumerable<EDocument> loanDocs;
        private IEnumerable<EDocument> LoanDocs
        {
            get
            {
                return loanDocs ??
                       (loanDocs = EDocumentRepository.GetUserRepository().GetDocumentsByLoanId(this.LoanID));
            }
        }

        protected DataTable m_data;

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this, includeSimplePopupDependencies: false);

            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");
            this.RegisterJsScript("ConditionDocumentUploadHelper.js");

            this.RegisterCSS("jquery-ui-1.11.css");
            this.RegisterCSS("DragDropUpload.css");

            this.RegisterJsGlobalVariables("LoanId", this.LoanID);
            this.RegisterJsGlobalVariables("HideStatusSubmissionButtonsInOriginatorPortal", this.PriceMyLoanUser.BrokerDB.HideStatusSubmissionButtonsInOriginatorPortal);
            this.RegisterJsGlobalVariables("HideStatusAndAgentsPageInOriginatorPortal", this.PriceMyLoanUser.BrokerDB.HideStatusAndAgentsPageInOriginatorPortal);

            var newLoanNavigationEnabled = this.PriceMyLoanUser.EnableNewTpoLoanNavigation;

            this.RegisterJsGlobalVariables("EnableNewTpoLoanNavigation", newLoanNavigationEnabled);
            //Check loan id
            if (LoanID == Guid.Empty)
            {
                _initError = true;
                //invalid task id. This happens when searching for a task. Just close the task list
                // MPTODO: this ain't gonna work
                string script = @"
<script type='text/javascript'>
<!--
    window.top.close();
-->
</script>
";
                Page.ClientScript.RegisterStartupScript(typeof(Page), "Close", script);
            }

            // Populate assignment dropdown
            m_assignmentFilter.Items.Add(new ListItem("Me", FILTER_ME));
            m_assignmentFilter.Items.Add(new ListItem("Anyone", FILTER_ANYBODY));

            // Create DataTable
            m_data = new DataTable();

            m_data.Columns.Add(new DataColumn("TaskId"));
            m_data.Columns.Add(new DataColumn("TaskRowVersion"));
            m_data.Columns.Add(new DataColumn("CondCategoryId"));
            m_data.Columns.Add(new DataColumn("TaskIsCondition"));
            m_data.Columns.Add(new DataColumn("TaskSubject"));
            m_data.Columns.Add(new DataColumn("TaskStatus", typeof(E_TaskStatus)));
            m_data.Columns.Add(new DataColumn("TaskDueDate", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskDueDateCalcDisplay"));
            m_data.Columns.Add(new DataColumn("TaskDueDateCalc"));
            m_data.Columns.Add(new DataColumn("TaskFollowUpDate", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskLastModifiedDate", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskAssignedUserId", typeof(Guid)));
            m_data.Columns.Add(new DataColumn("AssignedUserFullName"));
            m_data.Columns.Add(new DataColumn("OwnerFullName"));
            m_data.Columns.Add(new DataColumn("TaskDueDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskFollowUpDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskLastModifiedDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("CalcTaskStatus"));
            m_data.Columns.Add(new DataColumn("WarnOnResolve"));
            m_data.Columns.Add(new DataColumn("CalcTaskDueDateWarningCss"));
            m_data.Columns.Add(new DataColumn("CalcTaskFollowupDateWarningCss"));
            m_data.Columns.Add(new DataColumn("ResolutionBlockTriggerName"));
            m_data.Columns.Add(new DataColumn("ResolutionDateSetterFieldId"));
            m_data.Columns.Add(new DataColumn("CondRequiredDocTypeId"));
            m_data.Columns.Add(new DataColumn("CondRequiredDocType_rep"));
            m_data.Columns.Add(new DataColumn("ConditionFulfilsAssociatedDocs"));
            m_data.Columns.Add(new DataColumn("AssociatedDocIds"));

            // Hide subscription links
            subscribeBtn.Visible = CurrentEmployee.TaskRelatedEmailOptionT == E_TaskRelatedEmailOptionT.ReceiveEmail;
            unsubscribeBtn.Visible = CurrentEmployee.TaskRelatedEmailOptionT == E_TaskRelatedEmailOptionT.ReceiveEmail;

            this.PageID = "NewTaskList";
            this.PageTitle = "New Task List";

            this.RegisterService("taskEditor", "/main/Tasks/TaskEditorV2Service.aspx");
        }

        #region Page Load
        protected void PageLoad(object sender, EventArgs e)
        {
            string target = (string)Request.Form["__EVENTTARGET"];
            if (target == "unlink")
            {
                string argsString = Request.Form["__EVENTARGUMENT"];
                string[] args = argsString.Split(':');


                Task t = Task.Retrieve(BrokerID, args[0]);
                var id = new Guid(args[1]);

                t.UpdateDocumentAssociations(t.AssociatedDocs.Where(doc => doc.DocumentId != id).Select(doc => doc.DocumentId));
                t.Save(false);
            }

            m_openResolvedTasksGV.CssClass += " " + (EnableConditionMode ? "Condition LoanEditorTaskConditionGrid" : "Task");
            m_closedTasksGV.CssClass += " " + (EnableConditionMode ? "Condition" : "Task");

            associatedDocs = DocumentConditionAssociation.GetAssociationsByLoanHeavy(BrokerID, LoanID, PriceMyLoanUser)
                .GroupBy(association => association.TaskId)
                .ToDictionary(g => g.Key, g => g.ToList());

            if (_initError) return;

            if (!EnableConditionMode)
            {
                this.m_openResolvedTasksGV.Columns[(int)OpenTaskConditionGridColumns.ConditionIdCategoryId].Visible = false;
                this.m_openResolvedTasksGV.Columns[(int)OpenTaskConditionGridColumns.ConditionDueDateAssignedToCombined].Visible = false;
                this.m_closedTasksGV.Columns[2].Visible = false;
            }
            else
            {
                this.m_openResolvedTasksGV.Columns[(int)OpenTaskConditionGridColumns.TaskDueDate].Visible = false;
                this.m_openResolvedTasksGV.Columns[(int)OpenTaskConditionGridColumns.TaskAssignedTo].Visible = false;
                this.AddTaskBtn.Visible = false;
            }

            //DateTime starttime = DateTime.Now;
            if (!this.Page.IsPostBack)  // Only the first load of this page
            {
                // I don't use Tools.SetDropDownListValue here because that doesn't work with the current mix
                //   of Guids and sentinel values

                if (RequestHelper.GetSafeQueryString("src") == "pipeline")
                {
                    m_assignmentFilter.SelectedValue = FILTER_ANYBODY; //if called from pipeline, set the index to "Anybody"
                }
                else
                {

                    try
                    {
                        // Matches the selected value to the cookie value
                        m_assignmentFilter.SelectedValue = Request.Cookies[FilterCookie].Value;
                    }
                    catch (IndexOutOfRangeException) // This value is not in the list : possibly the user got unassigned
                    {
                        m_assignmentFilter.SelectedValue = FILTER_ANYBODY; // So just set it to anybody
                    }
                    catch (NullReferenceException) // The cookie doesn't exist
                    {
                        m_assignmentFilter.SelectedValue = FILTER_ANYBODY; // So just set it to anybody
                    }
                }
            }
            // Every single load
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TaskList));
            dataLoan.InitLoad();
            m_isTemplate.Value = dataLoan.IsTemplate.ToString();

            var enableLoanNavigationUi = this.PriceMyLoanUser.EnableNewTpoLoanNavigation;

            var location = this.EnableConditionMode ?
                    LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationConditions :
                    LendersOffice.Integration.GenericFramework.LinkLocation.TpoLoanNavigationTasks;

            this.LoanNavHeader.CloseOnReturn = !enableLoanNavigationUi;

            this.LoanNavHeader.SetDataFromLoan(dataLoan, location);
            this.SetSubmitToConditionReviewButtonDisplay(dataLoan, enableLoanNavigationUi);

            // Populate gridviews
            var ds = GetPopulatedDataSet();

            m_openResolvedTasksGV_Load(ds);
            m_closedTasksGV_Load(ds);

            if (!EnableConditionMode)
            {
                AddTaskBtn.Visible = true;
                m_openResolvedTasksGV.Columns[(int)OpenTaskConditionGridColumns.ConditionIdCategoryId].Visible = false;
                m_closedTasksGV.Columns[1].Visible = false;
                m_closedTasksGV.Columns[2].Visible = true;


                HeadingOne.Text = HeaderThree.Text = HeaderTop2.Text = "Task";
                HeadingFour.InnerText = "Closed Tasks";
                Header.Title = "Task List";
                OpenTaskLabel.InnerText = "Open Tasks";
            }
            else
            {
                AddTaskBtn.Visible = false;

                HeadingOne.Text = HeaderThree.Text = HeaderTop2.Text = "Condition";
                HeadingFour.InnerText = "Cleared Conditions";
                Header.Title = "Condition List";
                OpenTaskLabel.InnerText = "Open Conditions";
            }

            string isDownloadCSV = RequestHelper.GetSafeQueryString("csv");
            string condString = RequestHelper.GetSafeQueryString("conditionmode");
            if (!string.IsNullOrEmpty(isDownloadCSV) && !string.IsNullOrEmpty(condString))
            {
                bool conditionmode = !condString.Equals("false");
                ExportToCSV(isDownloadCSV, conditionmode, dataLoan.sLNm);
                return;
            }

            Tuple<bool, string> results = Tools.IsWorkflowOperationAuthorized(this.PriceMyLoanUser, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.UploadEDocs);
            this.RegisterJsGlobalVariables("CanUploadEdocs", results.Item1);
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", results.Item2);
        }

        /// <summary>
        /// Sets the display of the "Submit to Condition Review" button.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to submit into the "Condition Review" status.
        /// </param>
        /// <param name="enableLoanNavigationUi">
        /// True if the TPO loan navigation UI is enabled, false otherwise.
        /// </param>
        private void SetSubmitToConditionReviewButtonDisplay(
            CPageData dataloan,
            bool enableLoanNavigationUi)
        {
            if (!enableLoanNavigationUi || !this.EnableConditionMode)
            {
                this.SubmitToConditionReviewButton.Visible = false;
                return;
            }

            var enabledBrokerStatuses = this.PriceMyLoanUser.BrokerDB.GetEnabledStatusesByChannelAndProcessType(
                dataloan.sBranchChannelT,
                dataloan.sCorrespondentProcessT);

            if (!enabledBrokerStatuses.Contains(E_sStatusT.Loan_ConditionReview))
            {
                this.SubmitToConditionReviewButton.Visible = false;
                return;
            }

            if (!dataloan.LoanFileWorkflowRules.CanChangeLoanStatus(E_sStatusT.Loan_ConditionReview))
            {
                this.SubmitToConditionReviewButton.Disabled = true;
                this.SubmitToConditionReviewButton.Attributes.Add("title", "You do not currently have permission to perform this operation on this file.");
            }
        }

        private void ExportToCSV(string filepath, bool conditionmode, string loanNum)
        {
            var filename = conditionmode ? $"ConditionList_{loanNum}.csv" : $"TaskList_{loanNum}.csv";
            RequestHelper.SendFileToClient(Context, filepath, "text/csv", filename);
            Response.End();
        }

        protected void m_openResolvedTasksGV_Load(DataSet ds)
        {
            // For refactoring considerations, here's an alternate design:
            // Create a subclass of GridView
            // Create custom fields instead of using templatefields

            // And here's the design we went with

            DataView openResolvedTasksView = new DataView(ds.Tables[0])
            {
                RowFilter = PopulateRowFilter(OPENRESOLVED_TASKFILTER) // Filtering
            };

            // Sorting
            try
            {
                openResolvedTasksView.Sort = PopulateSortUpdateViewState(OPENRESOLVED_GRIDVIEWNAME, OPENRESOLVED_DEFAULTSORT);
            }
            catch (IndexOutOfRangeException)
            {
                openResolvedTasksView.Sort = OPENRESOLVED_DEFAULTSORT;
            }

            GV_Load(
                m_openResolvedTasksGV,
                OPENRESOLVED_GRIDVIEWNAME,
                openResolvedTasksView,
                isOpenTasks: true);
        }

        protected void m_closedTasksGV_Load(DataSet ds)
        {
            DataView closedTasksView = new DataView(ds.Tables[0])
            {
                // Filtering
                RowFilter = PopulateRowFilter(CLOSED_TASKFILTER)
            };

            // Sorting
            try
            {
                closedTasksView.Sort = PopulateSortUpdateViewState(CLOSED_GRIDVIEWNAME, CLOSED_DEFAULTSORT);
            }
            catch (IndexOutOfRangeException)
            {
                closedTasksView.Sort = CLOSED_DEFAULTSORT;
            }

            GV_Load(
                m_closedTasksGV,
                CLOSED_GRIDVIEWNAME,
                closedTasksView,
                isOpenTasks: false);
        }

        private string PopulateRowFilter(string defaultFilter)
        {
            try
            {
                string condFilter = EnableConditionMode ? CONDFILTER_COND : CONDFILTER_NONCOND;
                return BuildRowFilter(defaultFilter, m_assignmentFilter.SelectedValue, condFilter);
            }
            catch (FormatException) // It's not a Guid
            {
                return defaultFilter;
            }
            catch (IndexOutOfRangeException)
            {
                return defaultFilter;
            }
            catch (NullReferenceException)
            {
                return defaultFilter;
            }
        }

        private string PopulateSortUpdateViewState(string gvName, string defaultSort)
        {
            string sort;
            try
            {
                sort = string.Format(
                    "{0} {1}",
                    Request.Cookies[gvName + "SortExpression"].Value,
                    Request.Cookies[gvName + "SortDirection"].Value
                );

                ViewState[gvName + "SortExpression"] = Request.Cookies[gvName + "SortExpression"].Value;
                ViewState[gvName + "SortDirection"] = Request.Cookies[gvName + "SortDirection"].Value;
            }
            catch (NullReferenceException) // The cookies DO NOT EXIST! WHERE DID THE COOKIES GO?! Were they ever here in the first place?
            {
                sort = defaultSort;
            }
            return sort;
        }

        private void GV_Load(GridView gv, string gvName, DataView dv, bool isOpenTasks)
        {

            gv.DataSource = dv;
            gv.DataBind();

            // Can only do this after DataBind
            if (Request.Cookies[gvName + "SortedColumn"] != null && Request.Cookies[gvName + "SortDirection"] != null)
            {
                AddArrowToColumn(
                    gv,
                    Request.Cookies[gvName + "SortedColumn"].Value,
                    Request.Cookies[gvName + "SortDirection"].Value
                );
            }
            else if (isOpenTasks)
            {
                // Default to due date header.
                var labelName = this.EnableConditionMode ? "openResolvedConditionsGV_DueDateHeader" : "openResolvedTasksGV_DueDateHeader";
                AddArrowToColumn(gv, labelName, "ASC");
            }
            else
            {
                // Default to last updated header.
                AddArrowToColumn(gv, "closedTasksGV_TaskLastModifiedDate", "DESC");
            }
        }
        #endregion

        private DataSet GetPopulatedDataSet()
        {
            List<Task> tasks = Task.GetTasksByEmployeeAccess(BrokerID, LoanID, UserID);
            //Tools.LogInfo("SP time: " + (DateTime.Now - spstarttime).TotalMilliseconds + "ms");

            //DateTime rowcreatestarttime = DateTime.Now;

            //m_data.Clear();

            var taskPermissionProcessor = new TaskPermissionProcessor(LendersOffice.Security.PrincipalFactory.CurrentPrincipal);

            foreach (Task task in tasks)
            {
                var row = m_data.NewRow();

                row["TaskId"] = task.TaskId;
                row["TaskRowVersion"] = task.TaskRowVersion;
                row["TaskSubject"] = task.TaskSubject;
                row["TaskStatus"] = task.TaskStatus;
                row["CondCategoryId"] = task.CondCategoryId_rep == string.Empty ? "" : task.CondCategoryId_rep;
                row["TaskIsCondition"] = task.TaskIsCondition;

                // We use a hidden comparison column to sort by date and ensure that blank dates go last
                if (task.TaskDueDate == null)
                {
                    row["TaskDueDate"] = DBNull.Value;
                    row["TaskDueDateComparison"] = DateTime.MaxValue;
                    if (!string.IsNullOrEmpty(task.TaskDueDateCalcField))
                    {
                        row["TaskDueDateCalcDisplay"] = "inherit";
                        row["TaskDueDateCalc"] = task.TaskSingleDueDate;
                    }
                    else
                    {
                        row["TaskDueDateCalcDisplay"] = "none";
                        row["TaskDueDateCalc"] = "";
                    }
                }
                else
                {
                    row["TaskDueDate"] = task.TaskDueDate.Value.Date;
                    row["TaskDueDateComparison"] = task.TaskDueDate.Value.Date;
                    row["TaskDueDateCalcDisplay"] = "none";
                    row["TaskDueDateCalc"] = "";
                }

                if (task.TaskFollowUpDate == null)
                {
                    row["TaskFollowUpDate"] = DBNull.Value;
                    row["TaskFollowUpDateComparison"] = DateTime.MaxValue;
                }
                else
                {
                    row["TaskFollowUpDate"] = task.TaskFollowUpDate.Value.Date;
                    row["TaskFollowUpDateComparison"] = task.TaskFollowUpDate.Value.Date;
                }

                if (task.TaskLastModifiedDate == null)
                {
                    row["TaskLastModifiedDate"] = DBNull.Value;
                    row["TaskLastModifiedDateComparison"] = DateTime.MaxValue;
                }
                else
                {
                    row["TaskLastModifiedDate"] = task.TaskLastModifiedDate.Date;
                    row["TaskLastModifiedDateComparison"] = task.TaskLastModifiedDate.Date;
                }

                row["TaskAssignedUserId"] = task.TaskAssignedUserId;
                row["AssignedUserFullName"] = task.AssignedUserFullName;
                row["OwnerFullName"] = task.OwnerFullName;

                row["WarnOnResolve"] = task.TaskIsCondition
                    && task.CondRequiredDocTypeId.HasValue
                    && taskPermissionProcessor.Resolve(task) == E_UserTaskPermissionLevel.Manage
                    && !task.ConditionFulfilsAssociatedDocs();

                row["CalcTaskStatus"] = task.TaskStatus_rep; // Not exactly calculated, but we need to differentiate it from the int TaskStatus
                row["CalcTaskDueDateWarningCss"] = GetDateWarningCss(task.TaskDueDate);
                row["CalcTaskFollowupDateWarningCss"] = GetDateWarningCss(task.TaskFollowUpDate);

                row["ResolutionBlockTriggerName"] = task.ResolutionBlockTriggerName;
                row["ResolutionDateSetterFieldId"] = task.ResolutionDateSetterFieldId;

                row["CondRequiredDocType_rep"] = task.CondRequiredDocType_rep;
                row["CondRequiredDocTypeId"] = task.CondRequiredDocTypeId;
                row["ConditionFulfilsAssociatedDocs"] = task.ConditionFulfilsAssociatedDocs();

                m_data.Rows.Add(row);
            }
            var ds = new DataSet();
            ds.Tables.Add(m_data);

            //Tools.LogInfo("Row creation time: " + (DateTime.Now - rowcreatestarttime).TotalMilliseconds + "ms");

            return ds;
        }

        #region Filtering
        protected void m_openResolvedTasksGV_SelectedFilterChanged(object sender, EventArgs e)
        {
            RequestHelper.StoreToCookie(FilterCookie, m_assignmentFilter.SelectedItem.Value, SmallDateTime.MaxValue);
        }

        protected string BuildRowFilter(string defaultFilter, string selectedValue, string selectedCondValue)
        {
            List<string> filterList = new List<string>(4);
            if ((string.IsNullOrEmpty(selectedValue) || selectedValue == FILTER_ANYBODY)
                && (string.IsNullOrEmpty(selectedCondValue) || selectedCondValue == CONDFILTER_ALL))
            {
                return defaultFilter; // Don't need to filter
            }

            if (!string.IsNullOrEmpty(defaultFilter))
            {
                filterList.Add(defaultFilter);
            }

            // determine assignment filter
            if (!string.IsNullOrEmpty(selectedValue) && selectedValue == FILTER_ME)
            {
                filterList.Add($"TaskAssignedUserId = \'{UserID}\'");
            }
            // determine condition filter
            if (!string.IsNullOrEmpty(selectedCondValue) && selectedCondValue != CONDFILTER_ALL)
            {
                string filterType = selectedCondValue;
                string selectedConditionStatus;
                switch (filterType)
                {
                    case CONDFILTER_COND:
                        selectedConditionStatus = "True";
                        break;
                    case CONDFILTER_NONCOND:
                        selectedConditionStatus = "False";
                        break;
                    default:
                        selectedConditionStatus = "False";
                        Tools.LogError("Task List: unhandled condition filter type!");
                        break;
                }
                filterList.Add($"TaskIsCondition = \'{selectedConditionStatus}\'");
            }

            return string.Join(" AND ", filterList.ToArray());
        }
        #endregion

        #region Sorting
        protected void m_openResolvedTasksGV_Sorting(object sender, CommandEventArgs e)
        {
            SortOperation(sender, e, m_openResolvedTasksGV, "OpenResolved");
        }

        protected void m_closedTasksGV_Sorting(object sender, CommandEventArgs e)
        {
            SortOperation(sender, e, m_closedTasksGV, "Closed");
        }

        private void SortOperation(object sender, CommandEventArgs e, GridView gv, string gvName)
        {
            string[] commandArguments = ((string)e.CommandArgument).Split(' ');
            string field = commandArguments[0];
            string headerID = commandArguments[1];

            string sortDirection = GetSortDirection(
                field,
                ViewState[gvName + "SortExpression"] as string,
                ViewState[gvName + "SortDirection"] as string
            );
            ViewState[gvName + "SortExpression"] = field;
            ViewState[gvName + "SortDirection"] = sortDirection;
            string sortExpression = field + " " + sortDirection;

            ((DataView)gv.DataSource).Sort = sortExpression;
            gv.DataBind();

            // Get rid of the arrow from the last sorted column
            string lastSortedColumn = ViewState[gvName + "SortedColumn"] as string;
            if (lastSortedColumn != null)
            {
                RemoveArrowFromColumn(gv, lastSortedColumn);
            }

            AddArrowToColumn(gv, headerID, sortDirection);
            ViewState[gvName + "SortedColumn"] = headerID;

            // Now we can store the latest sort in a cookie
            RequestHelper.StoreToCookie(gvName + "SortedColumn", headerID, SmallDateTime.MaxValue);
            RequestHelper.StoreToCookie(gvName + "SortExpression", field, SmallDateTime.MaxValue);
            RequestHelper.StoreToCookie(gvName + "SortDirection", sortDirection, SmallDateTime.MaxValue);
        }

        protected void m_openResolvedTasksGV_OnDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton sort = e.Row.FindControl("TaskLink") as LinkButton;
                sort.Text = EnableConditionMode ? "Condition" : "Task";
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRow dataRow = ((DataRowView)e.Row.DataItem).Row;

                string taskId = (string)dataRow["TaskId"];
                bool isActive = ((string)dataRow["CalcTaskStatus"]).Equals("active", StringComparison.OrdinalIgnoreCase);
                var reqDocTypeId = dataRow[ "CondRequiredDocTypeId"];

                HtmlAnchor statusBtn = (HtmlAnchor)e.Row.FindControl("statusBtn");
                HtmlGenericControl docButtonsContainer = ((HtmlGenericControl)e.Row.FindControl("DocButtons"));
                HtmlGenericControl requiredDocsContainer = (HtmlGenericControl)e.Row.FindControl("RequiredDocTypeContainer");
                var dropzone = (HtmlGenericControl)e.Row.FindControl("DragDropZone");

                dropzone.Attributes.Add("taskid", taskId);
                dropzone.Attributes.Add("limit", this.FileUploadLimit.ToString());
                dropzone.Attributes.Add("extensions", this.AcceptedFileExtensions);

                GridView associatedDocsGrid = (GridView)e.Row.FindControl("AssociatedDocsGrid");

                DataTable associatedDocsSource = new DataTable();
                List<Guid> associatedDocIds = new List<Guid>();
                List<DocumentConditionAssociation> taskDocs;

                // Only show the upload and associate docs buttons if the task is active.
                docButtonsContainer.Visible = isActive;

                // Check if the Status button exists.  If it does, determine whether it should resolve or close the task on click.
                if (statusBtn != null)
                {
                    if (isActive)
                    {
                        statusBtn.InnerText = "Resolve";
                        statusBtn.Attributes.Add("class", "btn-resolve statusBtn btn btn-default");
                    }
                    else
                    {
                        statusBtn.InnerText = "re-activate";
                        statusBtn.Attributes.Add("class", "btn-reactivate statusBtn");
                    }
                    statusBtn.Attributes.Add("taskId", taskId);
                    statusBtn.Attributes.Add("taskVersion", (string)DataBinder.Eval(e.Row.DataItem, "TaskRowVersion"));
                }

                // If the task is active, and it has a required doc, display what doc is required, and whether the requirement is satisfied.
                if (reqDocTypeId != DBNull.Value && isActive)
                {
                    string conditionFulfilsAssociatedDocs = (string)DataBinder.Eval(e.Row.DataItem, "ConditionFulfilsAssociatedDocs");

                    HtmlGenericControl reqStatusImage = (HtmlGenericControl)e.Row.FindControl("reqStatusImage");
                    HtmlGenericControl reqStatusText = (HtmlGenericControl)e.Row.FindControl("reqStatusText");

                    if (conditionFulfilsAssociatedDocs.ToLower().Equals("true"))
                    {
                        reqStatusImage.InnerHtml = "&#xE86C;";
                        reqStatusImage.Attributes.Add("class", "material-icons green");
                        reqStatusText.InnerText = "Satisfied";
                        reqStatusText.Attributes.Add("class", "associatedDoc");
                    }
                    else
                    {
                        reqStatusImage.InnerHtml = "&#xE147;";
                        reqStatusImage.Attributes.Add("class", "material-icons red");
                        reqStatusText.InnerText = "Not Satisfied";
                        reqStatusText.Attributes.Add("class", "requiredDoc");
                    }
                }
                else
                {
                    requiredDocsContainer.Visible = false;
                }

                // Check if the task has any associated docs.  If it does, display a list of them, along with a link that allows users to unlink them.
                if (associatedDocs.TryGetValue(taskId, out taskDocs))
                {
                    associatedDocsSource.Columns.Add("DocId");
                    associatedDocsSource.Columns.Add("DocFolderType");
                    associatedDocsSource.Columns.Add("TaskId");
                    associatedDocsSource.Columns.Add("HasAccess");

                    var roleIds = LendersOffice.Security.PrincipalFactory.CurrentPrincipal.Roles.Select(roleT => Role.Get(roleT).Id);
                    // Fill the data table for the grid view.
                    foreach (DocumentConditionAssociation assoc in taskDocs)
                    {
                        associatedDocIds.Add(assoc.DocumentId);
                        DataRow row = associatedDocsSource.NewRow();
                        row["DocId"] = assoc.DocumentId;
                        row["TaskId"] = assoc.TaskId;
                        row["DocFolderType"] = assoc.DocumentFolderName + " : " + assoc.DocumentTypeName;
                        row["HasAccess"] = this.LoanDocs.Any(doc => doc.DocumentId == assoc.DocumentId);

                        associatedDocsSource.Rows.Add(row);
                    }

                    docButtonsContainer.Attributes.Add("associatedDocs", ObsoleteSerializationHelper.JavascriptJsonSerialize(associatedDocIds));

                    associatedDocsGrid.DataSource = associatedDocsSource;
                    associatedDocsGrid.DataBind();
                    associatedDocsGrid.HeaderRow.Visible = false;

                    if (!isActive)
                    {
                        // Hide the unlink link if the loan is not visible.
                        associatedDocsGrid.Columns[0].Visible = false;
                    }
                }

                // Hide the associated docs contianer if there are no associated docs.
                ((HtmlGenericControl)e.Row.FindControl("AssociatedDocsContainer")).Visible = associatedDocIds.Count > 0;
            }
        }

        private void RemoveArrowFromColumn(GridView gridview, string headerID)
        {
            // This might happen if there are no tasks, or if we called RemoveArrowFromColumn at the wrong time
            Label header = gridview.HeaderRow?.FindControl(headerID) as Label;
            if (header != null)
            {
                header.Text = "";
            }
        }
        private void AddArrowToColumn(GridView gridview, string headerID, string sortDirection)
        {
            // Add the arrow to the sorted column
            Label header = gridview.HeaderRow?.FindControl(headerID) as Label;

            if (header != null)
            {
                var sortLbl = new System.Web.UI.HtmlControls.HtmlGenericControl("i");
                sortLbl.Attributes.Add("class", "sort material-icons");
                sortLbl.InnerHtml = (sortDirection == "DESC") ? "&#xE5CF;" : "&#xE5CE;";
                header.Controls.Add(sortLbl);

                var linkButton = (LinkButton)header.Parent.Controls[1];
                if (!string.IsNullOrEmpty(header.Attributes["data-associated-link"]))
                {
                    linkButton = header.Parent.FindControl(header.Attributes["data-associated-link"]) as LinkButton;
                }
                linkButton.Style["font-weight"] = "Bold";
            }
        }

        /// <summary>
        /// Get the sort direction based on the example from
        /// http://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.gridview.sorting.aspx
        /// </summary>
        /// <param name="column"></param>
        /// <param name="lastSortExpression"></param>
        /// <param name="lastDirection"></param>
        /// <returns></returns>
        private string GetSortDirection(string column, string lastSortExpression, string lastDirection)
        {
            string sortDirection = "ASC";

            if (lastSortExpression != null)
            {
                if (lastSortExpression == column)
                {
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }
            return sortDirection;
        }
        #endregion

        #region Datagrid Display

        /// <summary>
        /// Check if the date in question is today or before today, then return
        /// the necessary css.
        /// </summary>
        /// <returns></returns>
        private string GetDateWarningCss(object queryDate)
        {
            if (queryDate is DateTime)
            {
                DateTime date = ((DateTime)queryDate).Date;
                // If the date is today or before today, style it differently
                return date.CompareTo(DateTime.Now.Date) <= 0 ? "DiscDue" : "DiscDueNot";
                // We take care of the row using jQuery, in _init
            }
            return string.Empty;
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

        protected void AssociatedDocsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var row = e.Row;
            var rowView = row.DataItem as DataRowView;
            if (e.Row.DataItem != null)
            {
                var unlinkLink = row.FindControl("UnlinkContainer") as HtmlGenericControl;
                unlinkLink.Visible = bool.Parse(rowView["HasAccess"].ToString());
            }
        }
    }
}
