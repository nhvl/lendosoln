﻿using System;

namespace PriceMyLoan.main.Tasks
{
    public partial class TaskListSetFollowupDateModal : PriceMyLoan.UI.BasePage
    {

        public string Description = string.Empty;
        protected void PageLoad(object sender, EventArgs e)
        {
            DateField.Text = "";
            this.RegisterJsScript("jquery-ui.js");
            this.RegisterCSS("/webapp/jquery-ui.css");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
