<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskViewer.aspx.cs" Inherits="PriceMyLoan.main.Tasks.TaskViewer" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Task Viewer</title>
    <style>
        .ErrorMessage
        {
            font-weight: bold;
            color:#EA1C0D;
            padding:0.5em;
        }
    </style>
</head>
<body>
<style>
    .table>div.hideWhenTemplate      { <%= AspxTools.HtmlString(!IsTemplate           ? "" : "display: none") %>; }
    .table>div.TemplateRow           { <%= AspxTools.HtmlString(IsTemplate            ? "" : "display: none") %>; }
    .table>div.showWhenCondition     { <%= AspxTools.HtmlString(IsCondition           ? "" : "display: none") %>; }
    .table>div.ResolutionSettingsRow { <%= AspxTools.HtmlString(ShowResolutionSetting ? "" : "display: none") %>; }
</style>
<form class="pu-window" runat="server" id="form1">
    <asp:Panel ID="pnlTaskViewer" runat="server">
        <asp:HiddenField ID="ResolutionDenialMessage" runat="server" />
        <div lqb-popup class="modal-pipeline-section">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="material-icons">&#xE5CD;</i></span></button>
                <h4 class="modal-title">
                    <ml:EncodedLabel id="m_Header" runat="server"/>
                    <span class="wrap-links text-right pull-right">
                        <a id="m_editLink"     runat="server" href="#" onclick="openEditor('edit');"    >Edit</a>
                        <a id="m_assignLink"   runat="server" href="#" onclick="openEditor('edit');"    >Assign</a>
                        <a id="m_emailLink"    runat="server" href="#" onclick="openEditor('email');"   >E-mail</a>
                        <a id="m_replyLink"    runat="server" href="#" onclick="openEditor('reply');"   >Reply</a>
                        <a id="m_forwardLink"  runat="server" href="#" onclick="openEditor('forward');" >Forward</a>
                        <a id="m_resolveLink"  runat="server" href="#" onclick="openEditor('resolve');" >Resolve</a>
                        <a id="m_activateLink" runat="server" href="#" onclick="openEditor('activate');">Re-activate</a>
                        <a id="m_closeLink"    runat="server" href="#" onclick="closeTask();"           >Close</a>
                        <a href="#" runat="server" id="GoBackLink" visible="false">Back to Condition List</a>
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="table"><div>
                    <div class="subject-edit text-grey text-right"><label>Subject</label></div>
                    <div><ml:EncodedLabel ID="Subject" runat="server"/></div>
                </div></div>
                <div class="table-flex">
                    <div>
                        <div class="table table-modal-padding-bottom">
                            <div class="hideWhenTemplate"><div class="text-grey text-right nowrap col-edit-left"><label>Assigned to       </label></div><div><ml:EncodedLabel ID="Assigned"         runat="server"/></div></div>
                            <div class="hideWhenTemplate"><div class="text-grey text-right nowrap col-edit-left"><label>Status            </label></div><div><ml:EncodedLabel ID="Status"           runat="server"/></div></div>
                            <div class=""                ><div class="text-grey text-right nowrap col-edit-left"><label>Due Date          </label></div><div><ml:EncodedLabel ID="DueDate"          runat="server"/><ml:EncodedLabel ID="DueDateCalcDescription" runat="server"/></div></div>
                            <div class="TemplateRow"     ><div class="text-grey text-right nowrap col-edit-left"><label>To Be Assigned To </label></div><div><ml:EncodedLabel ID="TemplateAssigned" runat="server"/></div></div>
                            <div class="ResolutionSettingsRow"><div class="text-grey text-right nowrap col-edit-left"><label>Resolution Block Trigger</label></div><div><ml:EncodedLabel ID="ResolutionBlockTriggerNameLabel" runat="server"/>
                                <a class="tooltips" data-html="true" data-toggle="tooltip" data-placement="bottom"
                                    id="ResolutionBlockTriggerExplainLink" runat="server"><i class="material-icons">&#xE887;</i></a>
                            </div></div>

                            <div class="AutoTriggersRow">
                                <div class="text-grey text-right nowrap col-edit-left">
                                    <label>Auto-Resolve Trigger</label>
                                </div>
                                <div>
                                    <ml:EncodedLabel ID="AutoResolveTriggerLabel" runat="server"/>
                                </div>
                            </div>

                            <div class=""                ><div class="text-grey text-right nowrap col-edit-left"><label>Follow-up Date    </label></div><div><ml:EncodedLabel ID="FollowupDate"     runat="server"/></div></div>
                            <div class="showWhenCondition"><div class="text-grey text-right nowrap col-edit-left"><label>Category         </label></div><div><ml:EncodedLabel ID="ConditionCategoryChoice"     runat="server"/></div></div>
                        </div>
                    </div>
                    <div>
                        <div class="table table-modal-padding-bottom">
                            <div class="hideWhenTemplate"><div class="text-grey text-right nowrap"><label>Borrower          </label></div><div><ml:EncodedLabel ID="BorrowerName"     runat="server"/></div></div>
                            <div class="hideWhenTemplate"><div class="text-grey text-right nowrap"><label>Loan Number       </label></div><div><ml:EncodedLabel ID="sLNm"             runat="server"/></div></div>
                            <div class="hideWhenTemplate"><div class="text-grey text-right nowrap"><label id="TaskOwnerLabel" runat="server">Task Owner        </label></div><div><ml:EncodedLabel ID="TaskOwner"        runat="server"/></div></div>
                            <div class="TemplateRow"     ><div class="text-grey text-right nowrap"><label>To Be Owned By    </label></div><div><ml:EncodedLabel ID="TemplateOwner"    runat="server"/></div></div>
                            <div class="ResolutionSettingsRow"><div class="text-grey text-right nowrap"><label>Resolution Date Setter</label></div><div><ml:EncodedLabel ID="ResolutionDateSetterFieldDescription" runat="server"/></div></div>

                            <div class="AutoTriggersRow">
                                <div class="text-grey text-right nowrap col-edit-left">
                                    <label>Auto-Close Trigger</label>
                                </div>
                                <div>
                                    <ml:EncodedLabel ID="AutoCloseTriggerLabel" runat="server"/>
                                </div>
                            </div>

                            <div id="tooltips-permissions-div" class="tooltips-permissions"><div class="text-grey text-right nowrap"><label id="TaskPermissionLabel" runat="server">Task Permission</label></div><div><ml:EncodedLabel ID="TaskPermission" runat="server"/>
                                <a class="tooltips" data-html="true" data-toggle="tooltip" data-placement="bottom"
                                    id="PermissionDescription" runat="server"><i class="material-icons">&#xE887;</i></a>
                            </div></div>
                            <div class="showWhenCondition" id="RequiredDocTypeSection" runat="server"><div class="text-grey text-right nowrap"><label>Required Document Type</label></div><div><ml:EncodedLabel ID="RequiredDocumentTypeName" runat="server"/></div></div>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="history-section">
                    <header class="text-grey">History</header>
                    <div class="history">
                        <ml:PassthroughLabel ID="History" runat="server"/>
                    </div>
                </div>

                <%-- Hide when they don't have notifications turned on --%>
                <asp:Placeholder id="subscriptionArea" runat="server">
                    <div id="IsNotSubscribed" runat="server">
                        <a id="subscribeLink" href="#">Subscribe</a> to receive e-mail notification when this <ml:EncodedLiteral runat="server" ID="TaskWording1">task</ml:EncodedLiteral> is updated
                    </div>
                    <div id="IsSubscribed" runat="server">
                        <a id="unsubscribeLink" href="#">Unsubscribe</a> to be removed from e-mail notifications related to <ml:EncodedLiteral runat="server" ID="TaskWording2">task</ml:EncodedLiteral> task
                    </div>
                </asp:Placeholder>
            </div>
        </div>
    </asp:Panel>
</form>
<script>(function($) {
    var ErrorMessage = <%= AspxTools.JsString(ErrorMessage) %>;

    var iframeSelf = new IframeSelf();
    if (!!ErrorMessage) {
        iframeSelf.onShow = function(){
            simpleDialog.alert(ErrorMessage, "Error", "Close").then(function() {
                iframeSelf.resolve(null);
            });
        }
        return;
    }

    var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    iframeSelf.popup = popup;

    $('.AutoTriggersRow').toggle(ML.HasAutoTriggers);

    $(".modal-header button.close").click(function() {
        iframeSelf.resolve(ML.NeedRefresh);
    });

    $("#GoBackLink").on("click", function(event){
        iframeSelf.resolve(true);
        return false;
    });

    $("#subscribeLink"  ).click(function(){ subscribe(true ); return false });
    $("#unsubscribeLink").click(function(){ subscribe(false); return false });

    function subscribe(toSubscribe) {
        var input = {
            UserId      : <%= AspxTools.JsString(BrokerUser.UserId.ToString()) %>,
            TaskId      : <%= AspxTools.JsString(TaskID) %>,
            ToSubscribe : toSubscribe,
        };
        var result = gService.loanedit.call("ChangeSubscription", input);
        document.getElementById('IsNotSubscribed').style.display = toSubscribe ? 'none' : '';
        document.getElementById('IsSubscribed'   ).style.display = toSubscribe ? '' : 'none';
    }

    $("#m_closeLink").click(closeTask);
    function closeTask() {
        var input = {
            BrokerId : <%= AspxTools.JsString(BrokerUser.BrokerId.ToString()) %>,
            TaskId   : <%= AspxTools.JsString(TaskID) %>,
        };
        var result = gService.loanedit.call("CloseTask", input);

        // Refresh the parent
        try {
            if (window.opener != null && typeof window.opener.taskList_refresh !== 'undefined') {
                window.opener.taskList_refresh();
            } else {
                iframeSelf.resolve(true);
            }
        } catch(e) {
            iframeSelf.resolve(true);
        }
    }

    setTimeout(function () {
        $('[data-toggle="tooltip"]').tooltip({
            position: {
                my: "center top+10",
                at: "center bottom"
            },
            // content: function () {
            //     return $(this).attr('title');
            // }
        });
        $('#tooltips-permissions-div').toggleClass("tooltips-permissions",$("#PermissionDescription").attr('data-original-title').length > 10);
    });

    TPOStyleUnification.refactorTaskHistory(document.getElementById("History"));

    // TPOStyleUnification.Components();
})(jQuery);

function openEditor(op) {
    var url = "TaskEditorV2.aspx?loanId=<%=AspxTools.HtmlString(LoanID_Override.ToString())%>&IsTemplate=<%=AspxTools.HtmlString(IsTemplate.ToString())%>&taskId=<%=AspxTools.HtmlString(TaskID.ToString())%>&mode=" + encodeURIComponent(op) + "&tp=" + <%= AspxTools.JsString(LendersOffice.Common.RequestHelper.GetBool("tp").ToString()) %>;
    self.location = url; // Need to preserve window.opener
}

function closePage() {
    // Close the page
    self.close(); // If this page was not opened by javascript, this will not work in Firefox
}
</script>
<script src="/common/ModalDlg/CModalDlg.js"></script>
</body>
</html>
