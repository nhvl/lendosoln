﻿using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;

namespace PriceMyLoan.main.Tasks
{
    public partial class DateCalculatorServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DateCalculatorServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "generateOutput":
                    GenerateOutput();
                    break;
            }
        }

        private void GenerateOutput()
        {
            string field = GetString("field");
            bool addOffset = GetBool("addOffset");
            int offset = (addOffset ? 1 : -1) * GetInt("offset");
            
            string dateString = Task.GetCalculatedDueDate(sLId, offset, field);
            bool pastDate = !string.IsNullOrEmpty(dateString) && Convert.ToDateTime(dateString) < DateTime.Today;

            SetResult("dateDescription", Tools.GetTaskDueDateDescription(sLId, offset, field, true));
            SetResult("date", dateString);
            SetResult("pastDate", pastDate);
            SetResult("offset", offset);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
    public partial class DateCalculatorService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new DateCalculatorServiceItem());
        }
    }
}
