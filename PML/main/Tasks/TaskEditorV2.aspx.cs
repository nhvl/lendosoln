﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using EDocs;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Email;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;

namespace PriceMyLoan.main.Tasks
{
    public partial class TaskEditorV2 : PriceMyLoan.UI.BasePage
    {
        protected const string EMAIL_REGEX_PATTERN = ConstApp.EmailValidationExpression;

        public override string StyleSheet
        {
            get { return VirtualRoot + "/css/style.css"; }
        }

        protected string TaskID
        {
            get { return RequestHelper.GetSafeQueryString("taskId"); }
        }

        protected bool NewTask
        {
            get { return string.IsNullOrEmpty(TaskID); }
        }

        protected string Mode
        {
            get { return RequestHelper.GetSafeQueryString("mode"); }
        }

        protected Guid LoanID_Override
        {
            get;
            set;
        }

        protected AbstractUserPrincipal BrokerUser
        {
            get
            {
                AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;
                if (null == principal)
                    RequestHelper.Signout();

                return principal;
            }
        }

        protected Guid BrokerID
        {
            get
            {
                return BrokerUser.BrokerId;
            }
        }

        protected bool IsCondition { get; set; }

        protected BrokerDB brokerDB
        {
            get { return BrokerDB.RetrieveById(BrokerID); }
        }

        private int FileUploadLimit => 12;

        private string AcceptedFileExtensions
        {
            get
            {
                if (this.PriceMyLoanUser.BrokerDB.AllowExcelFilesInEDocs)
                {
                    return ".xml, .pdf, .xls, .xlsx";
                }

                return ".xml, .pdf";
            }
        }

        private string m_missingFields;

        protected bool ResolutionSettingsRow { get; set; }
        protected bool EmailRow { get; set; }
        protected bool RequiredDocTypeSection { get; set; }
        protected bool ConditionRow { get; set; }

        protected void LoadData()
        {
            this.RegisterJsScript("Jquery-ui.js");
            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");
            this.RegisterJsScript("ConditionDocumentUploadHelper.js");

            this.RegisterCSS("/webapp/jquery-ui.css");
            this.RegisterCSS("DragDropUpload.css");

            this.DragDropZone.Attributes.Add("limit", this.FileUploadLimit.ToString());
            this.DragDropZone.Attributes.Add("extensions", this.AcceptedFileExtensions);

            Task task;
            E_UserTaskPermissionLevel permissionLevel;

            var taskIsCondition = false;
            if (!NewTask)
            {
                task = Task.Retrieve(BrokerID, TaskID);
                taskIsCondition = task.TaskIsCondition;
                LoanID_Override = task.LoanId;

                switch (Mode)
                {
                    case "email":
                    case "reply":
                    case "forward":
                        EmailRow = true;
                        CommentsRow.Visible = false;
                        if (Mode != "email")
                        {
                            EmailBody.Text = task.MostRecentEmailContent;
                        }
                        m_Save.Text = "Send & Save";
                        m_Header.Text = "E-mail via Task " + task.TaskId;
                        break;
                    case "resolve":
                        permissionLevel = task.GetPermissionLevelFor(BrokerUser);
                        m_Close.Visible = permissionLevel == E_UserTaskPermissionLevel.Manage ||
                                          permissionLevel == E_UserTaskPermissionLevel.Close;
                        m_Resolve.Visible = true;
                        m_Save.Visible = false;
                        StatusNext.Text = " -> Resolved";
                        changeTaskAssignmentLink.Visible = false;
                        AssignedPointer.Text = " -> ";
                        AssignedOwner.Text = task.OwnerFullName;
                        m_Header.Text = "Resolve Task " + task.TaskId;
                        break;
                    case "activate":
                        if (task.TaskDueDate.HasValue && task.TaskDueDate.Value < DateTime.Today)
                        {
                            task.TaskDueDate = DateTime.Today;
                            task.TaskDueDateCalcDays = 0;
                            task.TaskDueDateCalcField = "";
                            task.TaskDueDateLocked = true;
                        }
                        if (task.TaskFollowUpDate.HasValue && task.TaskFollowUpDate.Value < DateTime.Today)
                        {
                            task.TaskFollowUpDate = DateTime.Today;
                        }
                        if (task.AssignToOnReactivationUserId.HasValue)
                        {
                            task.AssignToOnReactivationUserId = task.AssignToOnReactivationUserId.Value;
                        }
                        else if (task.TaskResolvedUserId.HasValue)
                        {
                            task.TaskAssignedUserId = task.TaskResolvedUserId.Value;
                        }
                        StatusNext.Text = " -> Active";
                        m_Header.Text = "Re-activate Task " + task.TaskId;
                        break;
                    case "edit":
                    default:
                        m_Header.Text = "Edit Task " + task.TaskId;
                        break;


                }
                if (task.TaskIsCondition)
                {
                    m_Header.Text = m_Header.Text.Replace("Task", "Condition");
                    TaskOwnerLabel.Text = "Condition Owner";
                    TaskPermissionLabel.Text = "Condition Permission";
                }
                StatusResolved.Value = (task.TaskStatus == E_TaskStatus.Resolved).ToString();
                OwnerID.Value = task.TaskOwnerUserId_rep;
                AssignedID.Value = task.TaskAssignedUserId_rep;
                SavedVersion.Value = task.TaskRowVersion;
                DueDate.Text = task.TaskDueDate_rep;
                DueDateCalcDescription.Text = task.DueDateDescription(true);

                if (task.TaskStatus == E_TaskStatus.Resolved)
                {
                    changeTaskAssignmentLink.Visible = false;
                }
                if (task.TaskStatus == E_TaskStatus.Closed
                    || task.TaskOwnerUserId == BrokerUser.UserId)
                {
                    takeOwnershipLink.Visible = false;
                }
            }
            else
            {
                // Creating a new task
                LoanID_Override = RequestHelper.GetGuid("loanid");
                task = Task.Create(LoanID, BrokerID);
                m_Header.Text = "New Task";
                HistoryDiv.Visible = false;
                
                DueDate.Text = DateTime.Today.ToShortDateString();
            }

            this.RegisterJsGlobalVariables("TaskIsCondition", taskIsCondition);

            BorrowerName.Text = task.BorrowerNmCached;
            sLNm.Text = task.LoanNumCached;
            TaskOwner.Text = task.OwnerFullName;
            Assigned.Text = task.AssignedUserFullName;
            FollowupDate.Text = task.TaskFollowUpDate_rep;

            if (Task.CanViewField(BrokerUser, task.TaskDueDateCalcField))
            {
                DueDateCalcField.Value = task.TaskDueDateCalcField;
            }
            PermissionLevelID.Value = task.TaskPermissionLevelId.ToString();
            PermissionDescription.Title = task.PermissionLevelDescription;
            TaskPermission.Text = task.PermissionLevelName;
            DueDateOffset.Value = task.TaskDueDateCalcDays.ToString();
            AssignedID.Value = task.TaskAssignedUserId.ToString();
            OwnerRole.Value = task.TaskToBeOwnerRoleId_rep;
            AssignedRole.Value = task.TaskToBeAssignedRoleId_rep;
            Status.Text = task.TaskStatus_rep;
            History.Text = task.TaskHistoryHtml;
            DueDateLocked.Checked = task.TaskDueDateLocked;
            Subject.Text = task.TaskSubject;
            FromAddress.Text = task.TaskFromAddress;

            RequiredDocumentTypeName.Text = task.CondRequiredDocType_rep;
            RequiredDocumentTypeId.Value = (task.CondRequiredDocTypeId ?? -1).ToString();

            DocRequestSatisfied.Text = task.ConditionFulfilsAssociatedDocs_rep();
            if (DocRequestSatisfied.Text.Equals("No", StringComparison.CurrentCultureIgnoreCase))
            {
                DocRequestSatisfied.ForeColor = System.Drawing.Color.Red;
            }

            DocsToBeAssociated.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(task.AssociatedDocs.Select(p => p.DocumentId).ToList());

            List<int> docTypeIds = new List<int>();
            foreach (DocumentConditionAssociation dc in task.AssociatedDocs)
            {
                if (!docTypeIds.Contains(dc.DocTypeId))
                    docTypeIds.Add(dc.DocTypeId);
            }
            DocsToBeAssociatedTypeIds.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(docTypeIds);

            if (!BrokerUser.HasPermission(Permission.AllowReadingFromRolodex))
            {
                m_addForCCAddress.Visible = false;
                m_addForToAddress.Visible = false;
            }

            permissionLevel = task.GetPermissionLevelFor(BrokerUser);

            ConditionRow = task.TaskIsCondition;
            if (task.TaskIsCondition)
            {
                ConditionRow = true;
                if (task.CondCategoryId.HasValue)
                {
                    ConditionCategories.SelectedValue = task.CondCategoryId.Value.ToString();
                }


                // Default DocType fields to hidden
                RequiredDocTypeSection = false;
                DocSatisfiedRow.Style.Add("display", "none");
                this.DocumentSectionRow.Style.Add("display", "none");

                // Show DocType fields if conditions are met
                if (brokerDB.IsEDocsEnabled)
                {
                    if (permissionLevel == E_UserTaskPermissionLevel.Manage || task.CondRequiredDocTypeId.HasValue)
                    {
                        RequiredDocTypeSection = true;
                        phEdit.Visible = permissionLevel == E_UserTaskPermissionLevel.Manage;
                    }

                    if(!NewTask)
                    {
                        DocSatisfiedRow.Style.Add("display", "");

                        var associatedDocs = task.AssociatedDocs;
                        if (associatedDocs.Count != 0)
                        {
                            this.DocumentSectionRow.Style.Add("display", string.Empty);
                            this.RenderAssociatedDocsTable(associatedDocs, task.TaskStatus == E_TaskStatus.Active);
                        }
                    }
                }
            }
            else
            {
                ConditionRow = false;  // OPM 170314 - ConditionRow includes RequiredDocTypeSection & tdRequiredDocumentTypeName
                DocSatisfiedRow.Style.Add("display", "none");
                this.DocumentSectionRow.Style.Add("display", "none");
            }

            if (permissionLevel == E_UserTaskPermissionLevel.WorkOn ||
                permissionLevel == E_UserTaskPermissionLevel.Close)
            {
                Subject.ReadOnly = true;
                DueDate.ReadOnly = true;

                //Using Enable=false will render span tag. It affects TPO Unification style.
                DueDateLocked.InputAttributes.Add("disabled", "disabled");
                DueDateLockedLabel.Attributes.Add("class", "lock-checkbox disabled");

                takeOwnershipLink.Visible = false;
                dueDateCalculation.Visible = false;
                changePermissionLevel.Visible = false;
                ConditionCategories.Enabled = false;
            }

            if (!task.ConditionFulfilsAssociatedDocs())
            {
                ResolveAction.Value = permissionLevel == E_UserTaskPermissionLevel.Manage ? "Confirm" : "Block";
            }

            ResolutionSettingsRow = !string.IsNullOrEmpty(task.ResolutionBlockTriggerName) || !string.IsNullOrEmpty(task.ResolutionDateSetterFieldId);

            IsResolutionBlocked.Value = task.IsResolutionBlocked().ToString();
            DisplayResolutionDenialMessageLink.Title = task.ResolutionDenialMessage;

            ResolutionBlockTrigger.Text = task.ResolutionBlockTriggerName;
            ResolutionBlockTriggerName.Value = task.ResolutionBlockTriggerName;
            if (string.IsNullOrEmpty(task.ResolutionBlockTriggerName) || permissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                ClearResolutionBlockTrigger.Visible = false;
            }

            ResolutionDateSetterFieldIdLabel.Text = task.GetResolutionDateSetterFieldDescription();
            ResolutionDateSetterFieldId.Value = task.ResolutionDateSetterFieldId;
            if (string.IsNullOrEmpty(task.ResolutionDateSetterFieldId) || permissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                ClearResolutionDateSetter.Visible = false;
            }

            IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> triggers = TaskTriggerAssociation.GetTaskTriggerAssociations(task);

            this.RegisterJsGlobalVariables("HasAutoTriggers", triggers.ContainsKey(TaskTriggerType.AutoResolve) || triggers.ContainsKey(TaskTriggerType.AutoClose));
            AutoResolveTriggerLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoResolve) ? triggers[TaskTriggerType.AutoResolve].TriggerName : "None";
            AutoCloseTriggerLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoClose) ? triggers[TaskTriggerType.AutoClose].TriggerName : "None";

            Tuple<bool, string> results = Tools.IsWorkflowOperationAuthorized(this.PriceMyLoanUser, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.UploadEDocs);
            this.RegisterJsGlobalVariables("CanUploadEdocs", results.Item1);
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", results.Item2);
        }

        private void RenderAssociatedDocsTable(IEnumerable<DocumentConditionAssociation> associatedDocs, bool active)
        {
            var userDocRepo = EDocumentRepository.GetUserRepository();
            var listLoanDocs = new HashSet<Guid>(userDocRepo.GetDocumentsByLoanId(this.LoanID).Select(doc => doc.DocumentId));

            var associatedDocsSource = new DataTable();
            associatedDocsSource.Columns.Add("DocId");
            associatedDocsSource.Columns.Add("DocFolderType");
            associatedDocsSource.Columns.Add("HasAccess");

            foreach (var association in associatedDocs)
            {
                DataRow row = associatedDocsSource.NewRow();
                row["DocId"] = association.DocumentId;
                row["DocFolderType"] = association.DocumentFolderName + " : " + association.DocumentTypeName;
                row["HasAccess"] = listLoanDocs.Contains(association.DocumentId);

                associatedDocsSource.Rows.Add(row);
            }

            this.AssociatedDocsGrid.DataSource = associatedDocsSource;
            this.AssociatedDocsGrid.DataBind();
            this.AssociatedDocsGrid.HeaderRow.Visible = false;

            if (!active)
            {
                // Hide the unlink link if the loan is not visible.
                this.AssociatedDocsGrid.Columns[0].Visible = false;
            }
        }

        protected void AssociatedDocsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var row = e.Row;
            var rowView = row.DataItem as DataRowView;
            if (e.Row.DataItem != null)
            {
                var unlinkLink = row.FindControl("UnlinkContainer") as HtmlGenericControl;
                unlinkLink.Visible = bool.Parse(rowView["HasAccess"].ToString());
            }
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            ConditionCategories.DataSource = ConditionCategory.GetCategories(BrokerID);
            ConditionCategories.DataTextField = "Category";
            ConditionCategories.DataValueField = "Id";
            ConditionCategories.DataBind();

            this.PageTitle = "Task Editor";

            EmailBody.Attributes.Add("onfocus", "emailBodySetCursorPosition(this);");
            EmailBody.Attributes.Add("onblur", "emailBodyGetCursorPosition(this);");

            ClientScript.RegisterHiddenField("ResolveWarningUrl", Page.ResolveClientUrl("~/main/EDocs/ReqDocTypeResolveWarning.aspx"));
            ClientScript.RegisterHiddenField("ResolutionBlockedUrl", Page.ResolveClientUrl("~/main/Tasks/ResolutionBlocked.aspx"));
            ClientScript.RegisterHiddenField("DocTypePickerUrl", Page.ResolveClientUrl("~/main/EDocs/DocTypePicker.aspx"));
            ClientScript.RegisterHiddenField("EDocPickerUrl", Page.ResolveClientUrl("~/main/EDocs/EDocPicker.aspx"));

            LoadData();
        }

        protected void OnSaveClicked(Object sender, CommandEventArgs e)
        {
            Task task;
            if (NewTask)
            {
                LoanID_Override = RequestHelper.GetGuid("loanid");
                task = Task.Create(LoanID, BrokerID);
            }
            else
            {
                task = Task.Retrieve(BrokerID, TaskID);
                LoanID_Override = task.LoanId;
            }

            m_missingFields = "";

            if (string.IsNullOrEmpty(Subject.Text))
            {
                appendMissingField("Subject");
                SubjectR.Visible = true;
            }
            else
            {
                SubjectR.Visible = false;
            }

            if ((DueDateLocked.Checked && string.IsNullOrEmpty(DueDate.Text))
                || (!DueDateLocked.Checked && string.IsNullOrEmpty(DueDateCalcField.Value) && string.IsNullOrEmpty(task.TaskDueDateCalcField)))
            {
                appendMissingField("Due Date");
                DueDateR.Visible = true;
            }
            else
            {
                DueDateR.Visible = false;
            }

            bool AddressValid = true;
            if ((Mode == "email" || Mode == "reply" || Mode == "forward"))
            {
                if (string.IsNullOrEmpty(ToAddress.Text.TrimWhitespaceAndBOM()))
                {
                    appendMissingField("To");
                    ToAddressR.Visible = true;
                }
                else
                {
                    ToAddressR.Visible = false;
                    Regex reEmail = new Regex(EMAIL_REGEX_PATTERN);

                    string[] toAddresses = ToAddress.Text.TrimWhitespaceAndBOM().Replace(',', ';').Split(';');
                    foreach (string s in toAddresses)
                    {
                        AddressValid &= reEmail.IsMatch(s);
                    }
                    if (!string.IsNullOrEmpty(CCAddress.Text.TrimWhitespaceAndBOM()))
                    {
                        string[] CCAddresses = CCAddress.Text.TrimWhitespaceAndBOM().Replace(',', ';').Split(';');
                        foreach (string s in CCAddresses)
                        {
                            AddressValid &= reEmail.IsMatch(s);
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(m_missingFields) || !AddressValid)
            {
                int offset = 0;
                try
                {
                    offset = Convert.ToInt32(DueDateOffset.Value);
                }
                catch (FormatException) { }
                string fieldId = DueDateCalcField.Value;
                if (string.IsNullOrEmpty(fieldId))
                {
                    fieldId = task.TaskDueDateCalcField;
                }
                DueDateCalcDescription.Text = Tools.GetTaskDueDateDescription(LoanID, offset, fieldId, true);
                if (!string.IsNullOrEmpty(m_missingFields))
                {
                    m_errorMessage.Text = "The following fields are required: " + m_missingFields;
                }
                if (!AddressValid)
                {
                    m_errorMessage.Text += Environment.NewLine + ErrorMessages.InvalidEmailAddresses;
                }
                return;
            }

            if (!string.IsNullOrEmpty(m_missingFields))
            {
                m_errorMessage.Text = "The following fields are required: " + m_missingFields;
                return;
            }

            task.TaskPermissionLevelId = Convert.ToInt32(PermissionLevelID.Value);
            task.TaskDueDateLocked = DueDateLocked.Checked;
            if (DueDateLocked.Checked)
            {
                task.TaskDueDate_rep = DueDate.Text;
                task.TaskDueDateCalcField = "";
                task.TaskDueDateCalcDays = 0;
            }
            else
            {
                if (!string.IsNullOrEmpty(DueDateCalcField.Value.TrimWhitespaceAndBOM()))
                {
                	task.TaskDueDateCalcField = DueDateCalcField.Value;
                	task.TaskDueDateCalcDays = Convert.ToInt32(DueDateOffset.Value);
                	if (string.IsNullOrEmpty(DueDate.Text))
                	{
                	    task.TaskDueDate = null;
                	}
                	else
                	{
                	    task.TaskDueDate = DateTime.Parse(DueDate.Text);
                	}
				}
            }

            if (NewTask)
            {
                task.TaskCreatedByUserId = BrokerUser.UserId;
                OwnerID.Value = BrokerUser.UserId.ToString();
            }

            task.TaskSubject = Subject.Text;
            task.TaskAssignedUserId = new Guid(AssignedID.Value);
            task.TaskOwnerUserId = new Guid(OwnerID.Value);
            task.TaskFollowUpDate_rep = FollowupDate.Text;
            task.Comments = m_Comments.Text;


            task.CondRequiredDocTypeId = null; // OPM 170314 - default required doc id to null
            if (task.TaskIsCondition)
            {
                task.CondCategoryId = Convert.ToInt32(ConditionCategories.SelectedValue);

                if (brokerDB.IsEDocsEnabled && RequiredDocumentTypeId.Value != "-1")
                {
                    task.CondRequiredDocTypeId = int.Parse(RequiredDocumentTypeId.Value);
                }

                List<Guid> docIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(DocsToBeAssociated.Value);
                task.UpdateDocumentAssociations(docIds);
            }

            var permissionLevel = task.GetPermissionLevelFor(BrokerUser);
            if (task.GetPermissionLevelFor(BrokerUser) == E_UserTaskPermissionLevel.Manage)
            {
                task.ResolutionBlockTriggerName = ResolutionBlockTriggerName.Value;
                task.ResolutionDateSetterFieldId = ResolutionDateSetterFieldId.Value;
            }

            switch (Mode)
            {
                case "resolve":
                    if (e.CommandArgument.ToString() == "CloseTask")
                    {
                        task.ResolveAndClose();
                    }
                    else
                    {
                        task.Resolve();
                    }
                    IsCondition = task.TaskIsCondition;
                    ClosePage(TaskID, needListRefresh: true);
                    break;
                case "activate":
                    if (e.CommandArgument.ToString() == "RemoveAndReactivate")
                    {
                        RemoveAndReactivate(task);
                    }
                    else
                    {
                        Reactivate(task);
                    }

                    break;
                case "email":
                case "reply":
                case "forward":
                    CBaseEmail email = new CBaseEmail(BrokerID);
                    email.From = task.TaskFromAddress;
                    email.To = ToAddress.Text;
                    email.CCRecipient = CCAddress.Text;
                    email.Subject = EmailSubject.Value;
                    email.Message = EmailBody.Text;
                    try
                    {
                        task.SaveAndSendEmail(SavedVersion.Value, email);
                    }
                    catch (TaskSaveAndSendWhenNotActiveException)
                    {
                        m_errorMessage.Text = "Another user has resolved or closed this task.";
                        Tools.LogError(string.Format("TaskSaveAndSendWhenNotActiveException see 114783.  loan {0}, task {1}", LoanID, task.TaskId));
                        return;
                    }
                    IsCondition = task.TaskIsCondition;
                    ClosePage(TaskID, needListRefresh: false);
                    break;
                case "edit":
                default:
                    if (task.Save(true))
                    {
                        IsCondition = task.TaskIsCondition;
                        ClosePage(task.TaskId, needListRefresh: true);
                    }
                    else
                    {
                        LoadData();
                    }
                    break;
            }
        }

        private void Reactivate(Task task)
        {
            bool isAutoResolveTriggerTrue = task.IsAutoResolveTriggerTrue();
            bool isAutoCloseTriggerTrue = task.IsAutoCloseTriggerTrue();

            if (!isAutoResolveTriggerTrue && !isAutoCloseTriggerTrue)
            {
                // triggers not hit, reactivate and close.
                task.Reactivate();
                IsCondition = task.TaskIsCondition;
                ClosePage(TaskID, needListRefresh: true);
            }

            // Triggers hit. Open pop-up.
            string headerString = null;
            string warningString = null;
            if (isAutoResolveTriggerTrue && isAutoCloseTriggerTrue)
            {
                headerString = "Auto-Resolve and Auto-Close";
                warningString = "auto-resolve and auto-close";
            }
            else if (isAutoResolveTriggerTrue)
            {
                headerString = "Auto-Resolve";
                warningString = "auto-resolve";
            }
            else if (isAutoCloseTriggerTrue)
            {
                headerString = "Auto-Close";
                warningString = "auto-close";
            }

            this.IsAutoResolveTriggerTrue.Value = isAutoResolveTriggerTrue.ToYN();
            this.IsAutoCloseTriggerTrue.Value = isAutoCloseTriggerTrue.ToYN();

            this.warningLiteral1.Text = warningString;
            this.warningLiteral2.Text = warningString;
            this.warningLiteral3.Text = warningString;

            headerString += " Trigger Still True";

            this.RegisterJsGlobalVariables("ReactivateWarningHeader", headerString);
            this.RegisterJsGlobalVariables("ShowReactivateWarning", true);
        }

        private void RemoveAndReactivate(Task task)
        {
            //remove triggers.
            if (this.IsAutoResolveTriggerTrue.Value == "Y")
            {
                TaskTriggerAssociation.DeleteTaskTriggerAssociations(task, TaskTriggerType.AutoResolve);
            }

            if (this.IsAutoCloseTriggerTrue.Value == "Y")
            {
                TaskTriggerAssociation.DeleteTaskTriggerAssociations(task, TaskTriggerType.AutoClose);
            }

            this.IsAutoResolveTriggerTrue.Value = null;
            this.IsAutoCloseTriggerTrue.Value = null;

            task.Reactivate();
            IsCondition = task.TaskIsCondition;
            ClosePage(TaskID, needListRefresh: true);
        }

        private void appendMissingField(string field)
        {
            if (string.IsNullOrEmpty(m_missingFields))
            {
                m_missingFields += field;
            }
            else
            {
                m_missingFields += ", " + field;
            }
        }

        private void ClosePage(string taskID, bool needListRefresh)
        {
            Response.Redirect(String.Format("TaskViewer.aspx?LoanId={0}&cv={1}&tp={2}&TaskId={3}&NeedRefresh={4}", LoanID, IsCondition, RequestHelper.GetBool("tp"), taskID, needListRefresh));
        }

        protected override void OnInit(EventArgs e)
        {
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
            RegisterService("loanedit", url);
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
