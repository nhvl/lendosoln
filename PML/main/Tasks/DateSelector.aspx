﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="DateSelector.aspx.cs" Inherits="PriceMyLoan.main.Tasks.TaskListSetFollowupDateModal" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../Common/ModalDlg/ModelessDlg.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Set Follow-up Date</title>
</head>
<body>
<form runat="server">
    <div lqb-popup>
        <div class="modal-header">
            <h4 class="modal-title">Set Follow-up Date</h4>
        </div>
        <div class="modal-body">
            <div class="table">
                <div>
                    <div class="text-grey">
                        <label>Follow-up Date</label>
                    </div>
                    <div>
                        <ml:DateTextBox id="DateField" runat="server" IsDisplayCalendarHelper="false"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-flat" id="cancelBtn">Cancel</button>
            <button type="button" class="btn btn-flat" id="OKBtn">Save</button>
        </div>
    </div>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"/>
</form>
<script>(function($) {
    var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    var iframeSelf = new IframeSelf();
    iframeSelf.popup = popup;

    $("#OKBtn").click(function() {
        iframeSelf.resolve({OK:true, date:$("#DateField").val()});
    });

    $("#cancelBtn").click(function() {
        iframeSelf.resolve({OK:false});
    });
    TPOStyleUnification.Components();
})(jQuery);</script>
</body>
</html>
