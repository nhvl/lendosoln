<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="TaskEditorV2.aspx.cs" Inherits="PriceMyLoan.main.Tasks.TaskEditorV2" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Task Editor</title>
    <style>
        textbox { resize: vertical; }
        .FieldLabel
        {
            padding: 3px;
            text-align: right;
            font-weight: bold;
        }

        #LQBPopupUDiv0 {
            z-index: 9999 !important;
        }
    </style>
</head>
<body class="modal-iframe-body">
<style>
    .table > .ResolutionSettingsRow { <%= AspxTools.HtmlString(ResolutionSettingsRow  ? "" : "display: none") %> }
    .table > .EmailRow              { <%= AspxTools.HtmlString(EmailRow               ? "" : "display: none") %> }
    .table > .ExistingTaskRow       { <%= AspxTools.HtmlString(!NewTask               ? "" : "display: none") %> }
    .table > .ConditionRow          { <%= AspxTools.HtmlString(ConditionRow           ? "" : "display: none") %> }
    .table > .RequiredDocTypeSection{ <%= AspxTools.HtmlString(RequiredDocTypeSection ? "" : "display: none") %> }
    .table-flex > div:last-child { margin-left: 10px; }
    .table-flex > div { max-width: 335px; }
    #reactivateWarning { display: none; }
    #AssociatedDocsGrid, .doc-section-cell, .doc-section-cell td { border: none; }
</style>
<script>
<!--
    var taskIsDirty = false;

    var subUrl = "/main/Tasks/";

    function setCommentRowLocation() {
        var $commentRow = $('#CommentsRow');
        if ($commentRow.length === 0) {
            // Removed by codebehind
            return;
        }

        $commentRow.detach();

        var $commentCell = $commentRow.find('> div');
        $commentCell.find('.comments-edit').removeClass('comments-edit').addClass('col-edit-left');

        $('#CategoryRow').after($commentCell);
    }

    function _init() {
        if (<%= AspxTools.HtmlString(Page.IsPostBack.ToString()).ToLower() %>) {
            setTaskDirty();
        }
        checkDueDateStatus();
        setOwnerPermissionWarningStatus();

        document.getElementById('Subject').focus();

        if (ML.TaskIsCondition) {
            setCommentRowLocation();
        }

        TPOStyleUnification.Components();

        $j('.AutoTriggersRow').toggle(ML.HasAutoTriggers);

        if (ML.ShowReactivateWarning) {
            LQBPopup.ShowElement($j('#reactivateWarning'), {'width': 400 , 'height':150 , 'hideCloseButton': true, 'headerText': ML.ReactivateWarningHeader });
        }

        var dragDropSettings = {
            compact: true,
            isPmlParam: true,
            onProcessAllFilesCallback: uploadEdoc,
            blockUpload: !ML.CanUploadEdocs,
            blockUploadMsg: ML.UploadEdocsFailureMessage,
            hideFilesInFileListParam: true
        };
        registerDragDropUpload(document.getElementById('DragDropZone'), dragDropSettings);
    }

    function unlinkDoc(event, docId) {
        var args = {
            TaskId: <%=AspxTools.JsString(this.TaskID)%>,
            DocumentId: docId
        };

        gService.loanedit.callAsyncSimple('UnlinkDoc', args, function (result) {
            if (result.error) {
                simpleDialog.alert(result.UserMessage);
                return;
            }

            window.location.href = window.location.href;
        });
    }

    function attemptTaskListRefresh() {
        if (window.opener != null && !window.opener.closed)
        {
            if (typeof window.opener.taskList_refresh !== 'undefined') {
                window.opener.taskList_refresh();
            }
        }
    }

    function emailBodyGetCursorPosition(ctrl) { // this is bound to the control by the codebehind
        // TODO: see case 68089
    }

    function emailBodySetCursorPosition(ctrl) { // this is bound to the control by the codebehind
        // Move the cursor to the end of the textbox
        ctrl.focus();
        ctrl.value = ctrl.value;

        return true;
    }

    function calculationPopup() {
        var offset = document.getElementById("DueDateOffset");
        var field = document.getElementById("DueDateCalcField");
        url = VRoot + subUrl + 'DateCalculator.aspx'
            + "?loanid=" + <%=AspxTools.JsString(LoanID_Override)%>
            + "&duration=" + offset.value
            + "&fieldId=" + field.value;

        loadDummyModal();
        IframePopup.show(url, {}).then(function(pair){
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result == null) return;
                if (result.OK) {
                    offset.value = result.offset;
                    field.value = result.id;
                    $j("#DueDateCalcDescription").text(result.dateDescription);
                    document.getElementById("DueDate").value = result.date;
                    document.getElementById("DueDateLocked").checked = false;
                    checkDueDateStatus();
                    setTaskDirty();
                }
            }
        });
    }

    function permissionPopup() {
        var url = VRoot + subUrl + "TaskPermissionPicker.aspx?TaskID=" + <%=AspxTools.JsString(TaskID)%>;

        IframePopup.show(url, {}).then(function(pair){
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result == null) return;
                if (result.OK) {
                    document.getElementById("PermissionDescription").title = result.permissionDescription;
                    document.getElementById("PermissionLevelID").value = result.id;
                    $j("#TaskPermission").text(result.name);

                    setOwnerPermissionWarningStatus();
                    setTaskDirty();
                }
            }
        });
    }

    function setOwnerPermissionWarningStatus() {
        if(document.getElementById('OwnerID').value != <%= AspxTools.JsString(BrokerUser.UserId) %>
          && document.getElementById('OwnerID').value != '00000000-0000-0000-0000-000000000000') {
            var input = new Object();
            input["ownerID"] = document.getElementById('OwnerID').value;
            input["permissionLevelID"] = document.getElementById('PermissionLevelID').value;
            var result = gService.loanedit.call("CheckOwnerPermission", input);

            if (!result.error && result.value["hideError"] == <%=AspxTools.JsString(false.ToString())%>) {
                var owner = $j('#TaskOwner').text();
                $j('#ownerPermissionWarning').text('Note: ' +
                    owner + ' can manage this task because he/she owns it. ' +
                    owner + ' would not otherwise be able to manage tasks in this task permission level.');
                return false;
            }
        }

        $j('#ownerPermissionWarning').text('');
    }

    function takeOwnership() {
        var owner = <%= AspxTools.JsString(BrokerUser.FirstName + ' ' + BrokerUser.LastName) %>;
        var ownerId = <%= AspxTools.JsString(BrokerUser.UserId) %>;
        document.getElementById('OwnerID').value = ownerId;
        $j('#TaskOwner').text(owner);
        $j('#takeOwnershipLink').hide();
        if (<%=AspxTools.JsString(Mode)%> == 'resolve') {
            $j('#AssignedOwner').text(owner);
        }
        if (document.getElementById('StatusResolved').value == 'True') {
            $j('#Assigned').text(owner);
            document.getElementById('AssignedID').value = ownerId;
        }
        setOwnerPermissionWarningStatus();
        setTaskDirty();
        return false;
    }

    function addAddressFromContact(tbName) {
        var rolodex = new Rolodex2();
        rolodex.chooseFromRolodex(19, <%=AspxTools.JsString(LoanID_Override)%>).then(function(pair){
            var error = pair[0];
            var args = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (args == null) return;
                if (!args.OK) return;

                var addressTextbox = document.getElementById(tbName);
                addressTextbox.value = (!addressTextbox.value ? "" : (addressTextbox.value + ", ")) + args.AgentEmail;
                setTaskDirty();
            }
        });
        return false;
    }

    function checkDueDateStatus() {
        var dueDate = document.getElementById("DueDate");
        var $dueDate = $j(dueDate);
        if (document.getElementById("DueDateLocked").checked) { // No date if it's locked
            $j("#DueDateCalcDescription").text('');
            document.getElementById('DueDateOffset').value = '';
            document.getElementById('DueDateCalcField').value = '';
            $dueDate.readOnly(document.getElementById("DueDateLocked").disabled);
        } else {
            $dueDate.readOnly(true);
        }
        // $.fn.readOnly has the side effect of setting the element's readOnly attribute
        $j(dueDate.nextSibling).toggle(!$dueDate.readOnly());
        updateEmailSubject();
    }

    function clearManualDate() {
        if (!document.getElementById("DueDateLocked").checked) {
            document.getElementById("DueDate").value = '';
        }
    }

    function loadDummyModal(){
        //8/6/2017 huyn a dirty trick to solve flash radio button issue (459184#BugEvent.2401630), will take deep look into this when all high priroity cases are solved.
        
        LQBPopup.ShowElement($j('#dummyDiv'), { popupClasses:"hidden", backdrop: false });
        setTimeout(function () {LQBPopup.Hide();},1000);
    }

    function changeAssigned() {
        var permLevel = document.getElementById('PermissionLevelID').value;
        var id = document.getElementById("AssignedID");
        var role = document.getElementById("AssignedRole");
        var label = document.getElementById("Assigned");

        loadDummyModal();

        var url = VRoot + subUrl + "RoleAndUserPicker.aspx?loanid=" + <%=AspxTools.JsString(LoanID_Override)%> + "&PermLevel=" + permLevel + '&IsTemplate=false'
        IframePopup.show(url, {}).then(function(pair) {
            var error = pair[0];
            var result = pair[1];
            if (error != null) {
                console.error(error);
                simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
            } else {
                if (result == null) return;
                if (result.OK) {
                    if (result.type == 'user') {
                        id.value = result.id;
                    } else {
                        role.value = result.id;
                    }
                    $j(label).text(result.name);
                    setTaskDirty();
                }
            }
        });
    }

    function updateEmailSubject() {
        var subjectLabel = document.getElementById('TaskEmailSubject');
        if (document.getElementById('TaskEmailSubject') == null) {
            return;
        }

        var prefix = '';
        if (<%=AspxTools.JsString(Mode)%> == 'reply') {
            prefix = 'RE: ';
        }
        if (<%=AspxTools.JsString(Mode)%> == 'forward') {
            prefix = 'FWD: ';
        }
        var taskId = <%=AspxTools.JsString(TaskID)%>;
        var ccode = <%=AspxTools.JsString(brokerDB.CustomerCode)%>;
        var sId = $j('#sLNm').text();
        var brNm = $j('#BorrowerName').text();
        var dueDate = document.getElementById("DueDate").value != '' ? document.getElementById('DueDate').value : $j('#DueDateCalcDescription').text();
        var subject = document.getElementById('Subject').value;
        var emailSubject = prefix + 'Task ' + ccode + '_' + taskId + ' Due: ' + dueDate + ' - ' + sId + ' - ' + brNm + ' - ' + subject;
        $j(subjectLabel).text(emailSubject);
        document.getElementById('EmailSubject').value = emailSubject;
    }

    function disabledSubmitButtons(act) {
        if (act === 0 || act === 1) {
            var resolveAction = $j('#ResolveAction').val();
            if( resolveAction != "" )
            {
                openResolveWarning(act, resolveAction);
                return false;
            }

            var isResolutionBlocked = $j('#IsResolutionBlocked').val() === 'True';
            var resolutionDenialMessage = $j('#ResolutionDenialMessage').val();
            var args = {
                resolutionDenialMessage: resolutionDenialMessage
            };

            if (isResolutionBlocked) {
                LQBPopup.Show(resolutionBlockedUrl, {
                    'width': 400,
                    'height': 90,
                    hideCloseButton: true
                }, args);
                return false;
            }
        }

        disableButton('m_Close');
        disableButton('m_Resolve');
        disableButton('m_Save');
        disableButton('m_Cancel');
        return true;
    }

    function disableButton(id) {
        if (document.getElementById(id) != null) {
            document.getElementById(id).disabled = true;
        }
    }

    function setTaskDirty() {
        taskIsDirty = true;
    }

    function confirmClose() {
        if (!taskIsDirty){
            closeTaskEditor();
        }
        else{
            simpleDialog.confirm("Close task editor? Any unsaved changes will be lost.").then(function(result) {
                if (result === true) {
                    closeTaskEditor();
                }
            });
        }
    }



    function closePage(taskId) { // This is called by the codebehind in a ClientScriptBlock
        history.go(-1);
    }

    function clearResolutionBlockTrigger() {
        LQBPopup.ShowElement('Are you sure you want to clear the resolution block trigger from this task?',{
            closeText:'No',
            buttons:[{"Yes":function(){
                    setTaskDirty();
                    $j('#ResolutionBlockTrigger').text('None');
                    $j('#ResolutionBlockTriggerName').val('');
                    $j('.HideOnResolutionBlockTriggerClear').hide();
                }
            }]
        });
    }

    function clearResolutionDateSetter() {
        LQBPopup.ShowElement('Are you sure you want to clear the resolution date setter from this task?',{
            closeText:'No',
            buttons:[{"Yes":function(){
                    setTaskDirty();
                    $j('#ResolutionDateSetterFieldIdLabel').text('None');
                    $j('#ResolutionDateSetterFieldId').val('');
                    $j('.HideOnResolutionDateSetterClear').hide();
                }
            }]
        });
    }
-->
</script>


<script src="/inc/mask.js?v=6852097d-94aa-4bb7-a556-93003ef2d853" type="text/javascript"></script>
<script type="text/javascript" src="/common/ModalDlg/CModalDlg.js"></script>
<form runat="server" class="pu-window" name="form1" id="form1">
    <asp:HiddenField id="DocsToBeAssociated" runat="server" />
    <asp:HiddenField id="DocsToBeAssociatedTypeIds" runat="server" />
    <asp:HiddenField id="AssignedRole" value="00000000-0000-0000-0000-000000000000" runat="server" />
    <asp:HiddenField id="AssignedID" value="00000000-0000-0000-0000-000000000000" runat="server" />
    <asp:HiddenField id="OwnerRole" value="00000000-0000-0000-0000-000000000000" runat="server" />
    <asp:HiddenField id="OwnerID" value="00000000-0000-0000-0000-000000000000" runat="server" />
    <asp:HiddenField id="DueDateOffset" runat="server" />
    <asp:HiddenField id="DueDateCalcField" runat="server" />
    <asp:HiddenField id="PermissionLevelID" runat="server" />
    <asp:HiddenField id="EmailSubject" runat="server" />
    <asp:HiddenField id="StatusResolved" runat="server" />
    <asp:HiddenField id="SavedVersion" runat="server" />
    <asp:HiddenField id="ResolveAction" runat="server" />
    <asp:HiddenField id="RequiredDocumentTypeId" value="-1" runat="server" />
    <asp:HiddenField ID="IsResolutionBlocked" runat="server" />
    <asp:HiddenField ID="ResolutionDenialMessage" runat="server" />
    <asp:HiddenField ID="ResolutionBlockTriggerName" runat="server" />
    <asp:HiddenField ID="ResolutionDateSetterFieldId" runat="server" />
    <asp:HiddenField ID="IsAutoResolveTriggerTrue" runat="server" />
    <asp:HiddenField ID="IsAutoCloseTriggerTrue" runat="server" />

    <div lqb-popup hidden class="modal-pipeline-section edit-section">
        <div class="modal-header">
            <h4 class="modal-title"><ml:EncodedLabel ID="m_Header" runat="server"/></h4>
        </div>
        <div class="modal-body">
            <div class="table wide"><div>
                <div class="subject-edit text-grey text-right nowrap"><label>Subject
                    <img id="SubjectR" src="~/images/error_icon.gif" alt="Required" visible="false" runat="server" /></label></div>
                <div><asp:TextBox ID="Subject" runat="server" style="width:100%;" TextMode="MultiLine" Rows="3" onchange="updateEmailSubject();setTaskDirty();"/></div>
            </div></div>

            <div class="table-flex">
                <div>
                    <div class="table table-modal-padding-bottom none-bottom">
                        <div><div class="text-grey text-right nowrap col-edit-left"><label>Assigned to</label></div><div>
                            <ml:EncodedLabel ID="Assigned" runat="server"/>
                            <ml:EncodedLiteral ID="AssignedPointer" runat="server"/>
                            <ml:EncodedLabel ID="AssignedOwner" runat="server"/>
                            <a id="changeTaskAssignmentLink" runat="server" href="#" onclick="changeAssigned('taskAssign');">change</a>
                        </div></div>
                        <div class="ExistingTaskRow"><div class="text-grey text-right nowrap col-edit-left"><label>Status</label></div><div>
                            <ml:EncodedLiteral ID="Status" runat="server"/>
                            <ml:EncodedLiteral ID="StatusNext" runat="server"/>
                        </div></div>
                        <div><div class="text-grey text-right nowrap col-edit-left"><label>Due Date <img id="DueDateR" src="~/images/error_icon.gif" alt="Required" visible="false" runat="server" /></label></div><div>
                            <ml:datetextbox ID="DueDate" runat="server" preset="date" CssClass="mask" Width="60" onchange="updateEmailSubject();setTaskDirty();" IsDisplayCalendarHelper="false"></ml:datetextbox>
                            <label class="lock-checkbox" runat="server" id="DueDateLockedLabel">
                                <asp:CheckBox ID="DueDateLocked" runat="server" onclick="checkDueDateStatus();setTaskDirty();clearManualDate();" />
                            </label>
                            <a id="dueDateCalculation" runat="server" href="#" onclick="calculationPopup();">calculate</a>
                            <ml:EncodedLabel ID="DueDateCalcDescription" runat="server"></ml:EncodedLabel>

                        </div></div>
                        <div class="ResolutionSettingsRow"><div class="text-grey text-right nowrap col-edit-left"><label>Resolution Block Trigger</label></div><div>
                            <ml:EncodedLabel ID="ResolutionBlockTrigger" runat="server"/>
                            <a class="tooltips HideOnResolutionBlockTriggerClear" data-html="true" data-toggle="tooltip" data-placement="bottom"
                                    id="DisplayResolutionDenialMessageLink" runat="server"><i class="material-icons">&#xE887;</i></a>
                            <a href="#" id="ClearResolutionBlockTrigger" runat="server" onclick="clearResolutionBlockTrigger();" class="HideOnResolutionBlockTriggerClear">clear</a>
                        </div></div>

                        <div class="AutoTriggersRow">
                            <div class="text-grey text-right nowrap col-edit-left">
                                <label>Auto-Resolve Trigger</label>
                            </div>
                            <div>
                                <ml:EncodedLabel ID="AutoResolveTriggerLabel" runat="server"/>
                            </div>
                        </div>

                        <div class="FollowUpDateRow"><div class="text-grey text-right nowrap col-edit-left"><label>Follow-up Date</label></div><div><ml:datetextbox ID="FollowupDate" onchange="setTaskDirty();" runat="server" preset="date" CssClass="mask" Width="60" IsDisplayCalendarHelper="false"/></div></div>
                        <div id="CategoryRow" class="ConditionRow"><div class="text-grey text-right nowrap"><label>Category</label></div><div><asp:DropDownList ID="ConditionCategories" onchange="setTaskDirty();" runat="server"/></div></div>
                    </div>
                </div>


                <div>
                    <div class="table table-modal-padding-bottom none-bottom">
                        <div><div class="text-grey text-right nowrap"><label>Borrower</label></div><div><ml:EncodedLabel ID="BorrowerName" runat="server"/></div></div>
                        <div><div class="text-grey text-right nowrap"><label>Loan Number</label></div><div><ml:EncodedLabel ID="sLNm" runat="server"/></div></div>
                        <div class="ExistingTaskRow"><div class="text-grey text-right nowrap padding-bottom-condition-edit"><label><ml:EncodedLabel ID="TaskOwnerLabel" runat="server" Text="Task Owner"/></label></div><div>
                            <ml:EncodedLabel ID="TaskOwner" runat="server"/>
                            <a id="takeOwnershipLink" href="#" onclick="takeOwnership();" runat="server">take ownership</a></div></div>
                        <div id="tooltips-permissions-div" class="tooltips-permissions"><div class="text-grey text-right nowrap padding-bottom-condition-edit"><label><ml:EncodedLabel ID="TaskPermissionLabel" runat="server" Text="Task Permission"/></label></div><div>
                            <ml:EncodedLabel ID="TaskPermission" runat="server"/>
                            <a class="tooltips" data-html="true" data-toggle="tooltip" data-placement="bottom"
                                id="PermissionDescription" runat="server"><i class="material-icons">&#xE887;</i></a>
                            <a id="changePermissionLevel" runat="server" href="#" onclick="permissionPopup();">change</a>
                        </div></div>
                        <div class="ResolutionSettingsRow"><div class="text-grey text-right nowrap"><label>Resolution Date Setter</label></div><div>
                            <ml:EncodedLabel ID="ResolutionDateSetterFieldIdLabel" runat="server"/>
                            <a href="#" id="ClearResolutionDateSetter" runat="server" onclick="clearResolutionDateSetter();" class="HideOnResolutionDateSetterClear">clear</a>
                        </div></div>

                        <div class="AutoTriggersRow">
                            <div class="text-grey text-right nowrap col-edit-left">
                                <label>Auto-Close Trigger</label>
                            </div>
                            <div>
                                <ml:EncodedLabel ID="AutoCloseTriggerLabel" runat="server"/>
                            </div>
                        </div>

                        <div class="ConditionRow RequiredDocTypeSection"><div class="text-grey text-right nowrap"><label>Required Document Type</label></div><div>
                            <ml:EncodedLabel runat="server" ID="RequiredDocumentTypeName"/>
                            <asp:PlaceHolder ID="phEdit" runat="server">
                                <a href="#" runat="server" id="clear"         onclick="clearDocType();">clear</a>
                                <a href="#" runat="server" id="selectDocType" onclick="openDocTypePicker();">select Doc Type</a>
                            </asp:PlaceHolder>
                        </div></div>
                        <div id="DocSatisfiedRow" runat="server"><div class="text-grey text-right nowrap"><label>Document requirement satisfied?</label></div><div>
                            <ml:EncodedLabel runat="server" ID="DocRequestSatisfied"/>
                            <a href="#" onclick="return pickEdoc()">associate doc</a>
                            <div class="InsetBorder full-width">
                                <div id="DragDropZone" class="drag-drop-container" runat="server"></div>
                            </div>
                            <div class="drag-drop-error-div"></div>
                        </div></div>
                        <div id="DocumentSectionRow" runat="server">
                            <div class="text-grey text-right nowrap">&nbsp;</div>
                            <div class="doc-section-cell">
                                <asp:GridView ID="AssociatedDocsGrid" runat="server" AutoGenerateColumns="false" EnableViewState="false" OnRowDataBound="AssociatedDocsGrid_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemStyle CssClass="Unlink none-padding-left" />
                                            <ItemTemplate>
                                                <span runat="server" id="UnlinkContainer">
                                                    <a onclick="unlinkDoc(event, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DocId").ToString()) %>);">
                                                        <span class="span-unlink">
                                                            <svg width="20" height="17" viewBox="0 0 20 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                <title>Unlink</title>
                                                                <g id="Canvas" transform="translate(-3123 -14214)">
                                                                    <g id="Group 2">
                                                                        <g id="Vector">
                                                                            <use xlink:href="#unlink_path0_fill" transform="translate(3123 14218)" class="unlink-use" />
                                                                        </g>
                                                                        <g id="Line 2">
                                                                            <use xlink:href="#unlink_path1_stroke" transform="matrix(0.707107 0.707107 -0.707107 0.707107 3124.8 14216.2)" class="unlink-use" />
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                                <defs>
                                                                    <path id="unlink_path0_fill" d="M 1.9 5C 1.9 3.29 3.29 1.9 5 1.9L 9 1.9L 9 0L 5 0C 2.24 0 0 2.24 0 5C 0 7.76 2.24 10 5 10L 9 10L 9 8.1L 5 8.1C 3.29 8.1 1.9 6.71 1.9 5ZM 6 6L 14 6L 14 4L 6 4L 6 6ZM 15 0L 11 0L 11 1.9L 15 1.9C 16.71 1.9 18.1 3.29 18.1 5C 18.1 6.71 16.71 8.1 15 8.1L 11 8.1L 11 10L 15 10C 17.76 10 20 7.76 20 5C 20 2.24 17.76 0 15 0Z" />
                                                                    <path id="unlink_path1_stroke" d="M 0 0L 20.5183 0L 20.5183 -2L 0 -2L 0 0Z" />
                                                                </defs>
                                                            </svg>
                                                        </span>unlink
                                                    </a>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <span><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocFolderType").ToString()) %></span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table table-modal-padding-bottom wide margin-top none-bottom">
                <div class="EmailRow"><div class="text-grey text-right email-subject-edit nowrap"><label>From</label></div><div><ml:EncodedLiteral ID="FromAddress" runat="server"/></div></div>
                <div class="EmailRow"><div class="text-grey text-right email-subject-edit nowrap"><label>To <img id="ToAddressR" src="~/images/error_icon.gif" alt="Required" visible="false" runat="server" /></label></div><div>
                    <asp:TextBox ID="ToAddress" runat="server" onchange="setTaskDirty();" class="input-w320" />
                    <!-- <a id="m_addForToAddress" runat="server" onclick="addAddressFromContact('ToAddress');">add</a> 9/2/2017 huyn hide the add link because it may expose user sensitive information-->
                </div></div>
                <div class="EmailRow"><div class="text-grey text-right email-subject-edit nowrap"><label>CC</label></div><div>
                    <asp:TextBox ID="CCAddress" runat="server" onchange="setTaskDirty();" class="input-w320" />
                    <!-- <a id="m_addForCCAddress" runat="server" onclick="addAddressFromContact('CCAddress');">add</a> -->
                </div></div>
                <div class="EmailRow"><div class="text-grey text-right email-subject-edit nowrap"><label>Subject</label></div><div><ml:EncodedLabel ID="TaskEmailSubject" runat="server" onchange="setTaskDirty();"/></div></div>
                <div class="EmailRow"><div class="text-grey text-right email-subject-edit nowrap align-top"><label>Body</label></div><div><asp:TextBox ID="EmailBody" TextMode="MultiLine" Width="100%" Rows="6" runat="server" onchange="setTaskDirty();"/></div></div>
            </div>

            <div class="table wide" id="CommentsRow" runat="server">
                <div>
                    <div class="comments-edit text-grey text-right nowrap align-top">
                        <label>Comments</label>
                    </div>
                    <div>
                        <asp:TextBox id="m_Comments" textmode="MultiLine" width="100%" rows="6" runat="server" onchange="setTaskDirty();"/>
                    </div>
                </div>
            </div>

            <p><ml:EncodedLabel ID="m_errorMessage" runat="server" ForeColor="Red"/></p>
            <p id="ownerPermissionWarning">Note: The current task owner would not normally be allowed to manage this task under the current permission level.</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-flat" id="m_Cancel" type="button" onClick="confirmClose();">Cancel</button>
            <asp:Button ID="m_Close"   CssClass="btn btn-flat" Text="Resolve & Close" runat="server" Visible="false" OnCommand="OnSaveClicked" CommandArgument="CloseTask" UseSubmitBehavior="false" OnClientClick="if(!disabledSubmitButtons(0))return false;" />
            <asp:Button ID="m_Resolve" CssClass="btn btn-flat" Text="Resolve"         runat="server" Visible="false" OnCommand="OnSaveClicked" CommandArgument="SaveTask"  UseSubmitBehavior="false" OnClientClick="if(!disabledSubmitButtons(1))return false;" />
            <asp:Button ID="m_Save"    CssClass="btn btn-flat" Text="OK"              runat="server"                 OnCommand="OnSaveClicked" CommandArgument="SaveTask"  UseSubmitBehavior="false" OnClientClick="disabledSubmitButtons();" />
        </div>

        <div class="history-section" id="HistoryDiv" runat="server">
            <hr>
            <header class="text-grey">History</header>
            <div class="history">
                <ml:PassthroughLiteral ID="History" runat="server"/>
            </div>
        </div>
    </div>

    <div id="reactivateWarning">
        <div class="align-left">
            The <ml:EncodedLiteral id="warningLiteral1" runat="server" /> trigger for this task is still true.
            Reactivating it now will cause it to <ml:EncodedLiteral id="warningLiteral2" runat="server" /> again.
        </div>

        <div class="align-left PaddingTop">
            Do you want to remove the <ml:EncodedLiteral id="warningLiteral3" runat="server" /> trigger to prevent this from happening?
        </div>

        <div class="center PaddingTop">
            <asp:Button ID="btnRemoveAndReactivate" Text="Remove trigger and re-activate the task." runat="server" OnCommand="OnSaveClicked" CommandArgument="RemoveAndReactivate" UseSubmitBehavior="false" />
        </div>

        <div class="center PaddingTop">
            <input id="btnWarningCancel" type="button" value="Cancel" onclick="LQBPopup.Hide();" />
        </div>
    </div>
</form>
<div id="dummyDiv"></div>
<script>
(function($) {
    var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    var iframeSelf = new IframeSelf();
    iframeSelf.popup = popup;

    window.closeTaskEditor = function closeTaskEditor(){
        <% if(NewTask) { %>
            if (typeof window.parent.LQBPopup !== "undefined") {
                iframeSelf.resolve(true);
            }
            else {
                window.close();
            }
        <% } else { %>
            var url = "TaskViewer.aspx?taskId=<%=AspxTools.HtmlString(TaskID.ToString())%>&tp=" + <%= AspxTools.JsString(LendersOffice.Common.RequestHelper.GetBool("tp").ToString()) %>;
            self.location = url;
        <% } %>
    };

    TPOStyleUnification.refactorTaskHistory(document.querySelector("#HistoryDiv .history"));
    
    setTimeout(function () {
        $('#tooltips-permissions-div').toggleClass("tooltips-permissions",$("#PermissionDescription").attr('data-original-title').length > 10);   
    },100);
        
})(jQuery);

        var RequiredDocumentTypeId = <%=AspxTools.JsGetElementById(RequiredDocumentTypeId)%>;
        var RequiredDocumentTypeName = <%=AspxTools.JsGetElementById(RequiredDocumentTypeName)%>;
        var DocsToBeAssociated = <%=AspxTools.JsGetElementById(DocsToBeAssociated)%>;
        var DocsToBeAssociatedTypeIds = <%=AspxTools.JsGetElementById(DocsToBeAssociatedTypeIds)%>;
        var DocRequestSatisfied = <%=AspxTools.JsGetElementById(DocRequestSatisfied)%>;
        var ResolveAction = <%=AspxTools.JsGetElementById(ResolveAction)%>;

        var resolveWarningUrl = $j('#ResolveWarningUrl').val();
        var resolutionBlockedUrl = $j('#ResolutionBlockedUrl').val();

        function openResolveWarning(act, resolveAction) {
            var args = {
                'act' : act,
                'resolveAction' : resolveAction
            };

            LQBPopup.Show(resolveWarningUrl,
                {
                    'width': 400,
                    'height': 90,
                    'hideCloseButton': true,
                    'onReturn': function(act) {
                        ResolveAction.value = "";

                        var $button = (
                            (act === 0) ? $j(<%=AspxTools.JsGetElementById(m_Close  )%>) : (
                            (act === 1) ? $j(<%=AspxTools.JsGetElementById(m_Resolve)%>) :
                            undefined));

                        $button.click();
                    }
                }, args);
        }

        var docTypePickerUrl = $j('#DocTypePickerUrl').val();
        function openDocTypePicker() {
            LQBPopup.Show(docTypePickerUrl, { 'onReturn': function(id, name) {
                    $j(RequiredDocumentTypeName).text(name);
                    RequiredDocumentTypeId.value = id;
                    SetDocRequestSatisfied(false);
                }
            });
        }

        function clearDocType() {
            $j(RequiredDocumentTypeName).text("None");
            RequiredDocumentTypeId.value = "-1";
            SetDocRequestSatisfied(false);
        }

        var EDocPickerUrl = $j('#EDocPickerUrl').val();
        function pickEdoc() {
            var args = {
                'docs' : DocsToBeAssociated.value
            };

            var Url = EDocPickerUrl + '?loanid=' + <%=AspxTools.JsString(LoanID_Override)%> + '&taskid=' + <%=AspxTools.JsString(TaskID)%>;


            IframePopup.show(Url, { args: args }).then(function(pair) {
                    var error = pair[0];
                    var result = pair[1];
                    if (error != null) {
                        console.error(error);
                        simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
                    } else {
                        if (result == null) return;
                        var docIds = result.docIds;
                        var docTypeIds = result.docTypeIds;
                        DocsToBeAssociated.value = JSON.stringify(docIds);
                        DocsToBeAssociatedTypeIds.value = JSON.stringify(docTypeIds);
                        SetDocRequestSatisfied(true);
                    }
                });
        }

        function uploadEdoc() {
            if (getFilesCount('DragDropZone') === 0) {
                // No files selected by the user, e.g.
                // the file types selected were invalid.
                return;
            }

            var dropzone = document.getElementById('DragDropZone');
            var taskId = <%=AspxTools.JsString(this.TaskID)%>;
            var loanId = <%=AspxTools.JsString(this.LoanID_Override)%>;
            ConditionDocumentUploadHelper.UploadDocs(dropzone, loanId, taskId, true/*isPml*/);
        }

        function documentUploadPostAssociationCallback() {
            window.location.href = window.location.href;
        }

        function SetDocRequestSatisfied(saveAssociations)
        {
            var input = new Object();
            input["LoanId"] = <%=AspxTools.JsString(LoanID_Override)%>;
            input["TaskID"] = <%=AspxTools.JsString(TaskID)%>;
            input["RequiredDocumentTypeId"] = RequiredDocumentTypeId.value;
            input["DocsToBeAssociated"] = DocsToBeAssociated.value;
            input["DocsToBeAssociatedTypeIds"] = DocsToBeAssociatedTypeIds.value;
            input["SaveAssociations"] = saveAssociations.toString();

            var result = gService.loanedit.call("SetDocRequestSatisfied", input);

            if (!result.error) {
                ResolveAction.value = result.value.ResolveAction;

                $j(DocRequestSatisfied).text(result.value.DocRequestSatisfied);
                if (result.value.DocRequestSatisfied == "No")
                {
                    DocRequestSatisfied.style.color = "red";
                }
                else
                {
                    DocRequestSatisfied.style.color = "";
                }
            }
        }
</script>
</body>
</html>
