<%@ Page language="c#" Codebehind="DisplayWarningMessage.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.DisplayWarningMessage" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>DisplayWarningMessage</title>
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor=gainsboro>
	<script language=javascript>
<!--
function _init() {
  resize(500, 290);
}
function f_choice(choice) {
  var args = { choice: choice };
  onClosePopup(args);
}
//-->
	</script>
	
    <form id="DisplayWarningMessage" method="post" runat="server">
      <table width="100%">
        <tr><td style="FONT-WEIGHT:bold;COLOR:black">Warning message regarding this credit provider posted on&nbsp;<%= AspxTools.HtmlString(RequestHelper.GetSafeQueryString("WarningStartD"))%>:</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td style="FONT-WEIGHT:bold;COLOR:black"><%= AspxTools.HtmlString(RequestHelper.GetSafeQueryString("WarningMsg"))%></td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td align=center><input type=button value="Try it anyway" class="buttonstyle" onclick="f_choice(0);">&nbsp;<input type=button value="Cancel" class="buttonstyle" onclick="f_choice(1);"></td></tr>
      </table>
    </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</html>
