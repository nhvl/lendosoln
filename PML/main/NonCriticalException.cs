using System;
using DataAccess;
using LendersOffice.Common;

namespace PriceMyLoan
{
	/// <summary>
	/// Summary description for NonCriticalException.
	/// </summary>
	public class NonCriticalException : CBaseException
	{
		public NonCriticalException(string msg)
            : base (ErrorMessages.Generic, msg)
		{
		}
	}
}
