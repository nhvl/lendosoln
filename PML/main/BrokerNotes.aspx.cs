using System;
using System.Web.UI;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;

namespace PriceMyLoan.main
{
	public partial class BrokerNotes : PriceMyLoan.UI.BasePage
	{

        public override bool IsReadOnly 
        {
            get 
            {
                // 8/2/2006 dd - Always allow user to enter broker note.
                return false;
            }
        }


        protected int m_noteLength = 700;
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (!Page.IsPostBack) 
            {
                if (!CanRead)
                {
                    throw new AccessDenied();
                }
                CPageData dataLoan = new CLpeBrokerNotesData(LoanID);
                dataLoan.InitLoad();
                sFileVersion = dataLoan.sFileVersion;
                m_noteLength = Math.Min(700, 1001 - dataLoan.sLpeNotesFromBrokerToUnderwriterHistory.Length);

                sLpeNotesFromBrokerToUnderwriterNew.Text = dataLoan.sLpeNotesFromBrokerToUnderwriterNew;

            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

		protected void m_btnOK_Click(object sender, System.EventArgs e)
		{
            if (!CanRead)
            {
                throw new AccessDenied();
            }

            CPageData dataLoan = new CLpeBrokerNotesData(LoanID);
            dataLoan.InitSave(sFileVersion);

            dataLoan.sLpeNotesFromBrokerToUnderwriterNew = sLpeNotesFromBrokerToUnderwriterNew.Text;
            dataLoan.Save();
            ClientScript.RegisterStartupScript(typeof(BrokerNotes), "close", "parent.LQBPopup.Return('" + dataLoan.sFileVersion + "');",true);
          // PriceMyLoan.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] { "OK=true", "sFileVersion='" + dataLoan.sFileVersion + "'" });
		}

        private bool CanRead {
            get
            {
                AbstractUserPrincipal principal = User as AbstractUserPrincipal;
                // 6/1/2010 dd - Since we allow broker to enter notes even he/she no longer has access to write to the loan. We
                // still need to perform a check that broker can at least read a file.
                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.ConnectionInfo, principal.BrokerId, LoanID, WorkflowOperations.ReadLoan);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoan, valueEvaluator);


                return canRead;
            }
        }
	}
}
