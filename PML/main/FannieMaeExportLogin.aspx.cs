using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.ServiceCredential;
using LendersOffice.Security;
using System;
using System.Linq;

namespace PriceMyLoan.main
{

    public partial class FannieMaeExportLogin : PriceMyLoan.UI.BasePage
	{
        protected string m_doDuHeader
        {
            get { return m_isDo ? "Desktop Originator" : "Desktop Underwriter"; }
        }
        protected bool m_isDo 
        {
            get { return RequestHelper.GetSafeQueryString("isdo") == "t"; }
        }

        protected bool m_hasAutoLoginInformation 
        {
            get;
            set;
        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        
		protected void PageLoad(object sender, System.EventArgs e)
		{
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FannieMaeExportLogin));
            dataLoan.InitLoad();

            if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
            {
                var usrMsg = "You do not have permission to run do/du in quickpricer mode.";
                var devMsg = "Programming error.  Should not reach FannieMaeExportLogin if E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT";
                throw new CBaseException(usrMsg, devMsg);
            }

            sDuCaseId.Text = dataLoan.sDuCaseId;
            sDuLenderInstitutionId.Text = dataLoan.sDuLenderInstitutionId;

            AbstractUserPrincipal principal = (AbstractUserPrincipal) this.User;
            if (m_isDo)
            {
                this.m_hasAutoLoginInformation = principal.HasDoAutoLogin;
                FannieMaeMORNETUserID.Text = principal.HasDoAutoLogin ? principal.DoAutoLoginNm : principal.DoLastLoginNm;
            }
            else
            {
                if (principal.BrokerDB.UsingLegacyDUCredentials)
                {
                    this.m_hasAutoLoginInformation = principal.HasDuAutoLogin;
                    FannieMaeMORNETUserID.Text = principal.HasDuAutoLogin ? principal.DuAutoLoginNm : principal.DuLastLoginNm;
                }
                else
                {
                    var savedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                            .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter && credential.IsEnabledForNonSeamlessDu);
                    if (savedCredential != null)
                    {
                        this.m_hasAutoLoginInformation = true;
                        FannieMaeMORNETUserID.Text = savedCredential.UserName;
                    }
                }
            }

            m_rememberLoginCB.Checked = FannieMaeMORNETUserID.Text != "";
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("loanedit", "/main/fanniemaeexportloginservice.aspx");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
