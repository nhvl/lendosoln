/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Conversions.LoanProspector;
using LendersOffice.Common;
using LendersOffice.Security;

namespace PriceMyLoan.main
{
	public partial class LoanProspectorExportLogin : PriceMyLoan.UI.BasePage
	{
        class TmpBorrower 
        {
            private string m_borrowerID = "";
            public string BorrowerID 
            {
                get { return m_borrowerID; }
                set { m_borrowerID = value; }
            }
            private string m_borrowerName = "";
            private string m_reference = "";
            public string BorrowerName 
            {
                get { return m_borrowerName; }
                set { m_borrowerName = value; }
            }
            public string Reference 
            {
                get { return m_reference; }
                set { m_reference = value; }
            }

            public TmpBorrower(string id, string name, string reference) 
            {
                this.BorrowerID = id;
                this.BorrowerName = name;
                this.Reference = reference;
            }
        }

        protected bool m_hasLpPassword = false;
        protected bool m_sHasFreddieInfileCredit = false;
        protected List<string> m_mcrnList = null;

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        
		protected void PageLoad(object sender, System.EventArgs e)
		{
            CPageData dataLoan = new CLoanProspectorLoginData(LoanID);
            dataLoan.InitLoad();

            if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
            {
                var msg = "May not use LoanProspector when loan is quickpricer sandboxed";
                throw new CBaseException(msg, msg);
            }

            m_sHasFreddieInfileCredit = dataLoan.sHasFreddieInfileCredit;
            int nApps = dataLoan.nApps;

            ArrayList borrowerList = new ArrayList();
            string lastCra = null;

            m_mcrnList = new List<string>();
            for (int i = 0; i < nApps; i++) 
            {
                CAppData app = dataLoan.GetAppData(i);
                string borrowerName = app.aBNm;
                if (app.aCNm != "") 
                {
                    borrowerName += " & " + app.aCNm;
                }
                string borrowerId = "B" + app.aAppId.ToString("N"); //"B" + app.aBSsn.Replace("-", "");
                m_mcrnList.Add(borrowerId);


                SqlParameter[] parameters =  { new SqlParameter("@ApplicationID", app.aAppId) };

                string referenceNumber = "";
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(dataLoan.sBrokerId, "RetrieveCreditReport", parameters))
                {
                    if (reader.Read()) 
                    {
                        referenceNumber = (string) reader["ExternalFileID"];
                        if (reader["CrAccProxyId"] is DBNull)
                            lastCra = reader["ComId"].ToString();
                        else
                            lastCra = reader["CrAccProxyId"].ToString();

                    }
                }

                borrowerList.Add(new TmpBorrower(borrowerId, borrowerName, referenceNumber));
            }
            // 4/22/2008 dd - Try to map our CRA with Freddie Credit Reporting company
            string cra = LoanProspectorCreditReportingCompanyMapping.GetLpCrcCode(lastCra);
            this.RegisterJsGlobalVariables("cra", cra);

            Tools.SetDropDownListValue(sFredProcPointT, dataLoan.sFredProcPointT);
            sFreddieLoanId.Text = dataLoan.sFreddieLoanId;
            sFreddieTransactionId.Text = dataLoan.sFreddieTransactionId;
            sLpAusKey.Text = dataLoan.sLpAusKey;
            sFreddieSellerNum.Text = dataLoan.sFreddieSellerNum;
            sFreddieTpoNum.Text = dataLoan.sFreddieTpoNum;
            sFreddieLpPassword.Text = dataLoan.sFreddieLpPassword.Value;
            sFreddieNotpNum.Text = dataLoan.sFreddieNotpNum;
            m_hasLpPassword = dataLoan.sFreddieLpPassword.Value != "";

            m_borrowerListRepeater.DataSource = borrowerList;
            m_borrowerListRepeater.DataBind();


            m_borrowerListReorderRepeater.DataSource = borrowerList;
            m_borrowerListReorderRepeater.DataBind();
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("loanedit", "/main/loanprospectorexportloginservice.aspx");
            Tools.Bind_IPair(sFredProcPointT, Tools.sFredProcPointT_pairs);
            MainCreditReportingCompanies.DataSource = Tools.FreddieMacCreditReportingCompanyMapping;
            MainCreditReportingCompanies.DataBind();
            TechnicalAffiliates.DataSource = Tools.FreddieMacTechnicalAffiliateMapping;
            TechnicalAffiliates.DataBind();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
