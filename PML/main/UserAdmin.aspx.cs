using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using LqbGrammar;
using PriceMyLoan.Security;

namespace PriceMyLoan.main
{
    /// <summary>
    /// Summary description for UserAdmin.
    /// </summary>
    public partial class UserAdmin : PriceMyLoan.UI.BasePage
    {
        [WebMethod]
        public static object[] GetUsers(string firstName, string lastName)
        {
            try
            {
                string error = null;
                Dictionary<string, object>[] result = null;

                Tools.LogAndThrowIfErrors(() =>
                {
                    var PriceMyLoanUser = LendersOffice.Security.PrincipalFactory.CurrentPrincipal as PriceMyLoanPrincipal;
                    if (PriceMyLoanUser == null || !PriceMyLoanUser.IsPmlManager)
                    {
                        error = "Non-supervisor attempts to access supervised user list.";
                        return;
                    }

                    var parameters = new[]
                    {
                        new SqlParameter("@BrokerId", PriceMyLoanUser.BrokerId),
                        string.IsNullOrEmpty(firstName) ? null : new SqlParameter("@NameFilter", firstName),
                        string.IsNullOrEmpty(lastName) ? null : new SqlParameter("@LastFilter", lastName),
                        new SqlParameter("@PmlExternalManagerEmployeeId", PriceMyLoanUser.EmployeeId),
                    }.Where(p => p != null);

                    var ds = new DataSet();
                    DataSetHelper.Fill(ds, PriceMyLoanUser.BrokerId, "ListPMLUsersByBrokerId", parameters);

                    result = DataTableToRowDictionaries(ds.Tables[0])
                        .Select(user =>
                        {
                            var EmployeeId = (Guid)user["EmployeeId"];
                            var PmlBrokerId = (Guid)user["PmlBrokerId"];
                            var UserId = (Guid)user["UserId"];
                            var LoginNm = (string)user["LoginNm"];
                            var UserName = (string)user["UserName"];
                            var IsActive = (bool)user["IsActive"];
                            var Permissions = GetApplicableLoanCreationString(EmployeeId, PmlBrokerId, PriceMyLoanUser);
                            var EmployeeRoles = GetEmployeeRolesString(EmployeeId, PriceMyLoanUser);

                            return new Dictionary<string, object>
                            {
                                { "UserId"       , UserId        },
                                { "LoginNm"      , LoginNm       },
                                { "UserName"     , UserName      },
                                { "Permissions"  , Permissions   },
                                { "IsActive"     , IsActive      },
                                { "EmployeeRoles", EmployeeRoles },
                                { "EmployeeId"   , EmployeeId    },
                            };
                        })
                        .ToArray();
                });

                return new[] { error, (object) result };
            }
            catch (CBaseException e)
            {
                return new[] { e.Message, null };
            }
        }

        private static string GetApplicableLoanCreationString(Guid employeeId, Guid pmlBrokerId, IUserPrincipal userPrincipal)
        {
            var buP = new BrokerUserPermissions(userPrincipal.BrokerId, employeeId);

            var pB = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, userPrincipal.BrokerId);

            var rs = new[]
                {
                    pB.Roles.Contains(E_OCRoles.Correspondent    ) && buP.HasPermission(Permission.AllowCreatingCorrChannelLoans     ) ? "Correspondent"     : null,
                    pB.Roles.Contains(E_OCRoles.MiniCorrespondent) && buP.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans ) ? "Mini-Correspondent": null,
                    pB.Roles.Contains(E_OCRoles.Broker           ) && buP.HasPermission(Permission.AllowCreatingWholesaleChannelLoans) ? "Wholesale"         : null,
                }.Where(s => !string.IsNullOrEmpty(s))
                .ToArray();

            return (rs.Length < 1) ? "No" : "Yes - " + string.Join(", ", rs);
        }

        private static string GetEmployeeRolesString(Guid employeeId, IUserPrincipal userPrincipal)
        {
            var roles = new EmployeeRoles(userPrincipal.BrokerId, employeeId);

            return string.Join(", ", new[]
                {
                    roles.IsInRole(CEmployeeFields.s_LoanRepRoleId       ) ? "Loan Officer" : null,
                    roles.IsInRole(CEmployeeFields.s_BrokerProcessorId   ) ? "Processor"    : null,
                    roles.IsInRole(CEmployeeFields.s_ExternalSecondaryId ) ? "Secondary"    : null,
                    roles.IsInRole(CEmployeeFields.s_ExternalPostCloserId) ? "Post-Closer"  : null,
                    roles.IsInRole(CEmployeeFields.s_AdministratorRoleId ) ? "Supervisor"   : null,
                }.Where(r => r != null)
            );
        }

        private static IEnumerable<Dictionary<string, object>> DataTableToRowDictionaries(DataTable table)
        {
            return table.Rows.Cast<DataRow>().Select(row => table.Columns.Cast<DataColumn>()
                .ToDictionary(col => col.ColumnName, col => row[col.ColumnName]))
                //.ToArray()
                ;
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // 02/12/08 mf. OPM 19614 - User must have pml supervisor status to view.
            if (!PriceMyLoanUser.IsPmlManager)
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Non-supervisor attempts to access supervised user list.");

            RegisterJsScript("LQBNestedFrameSupport.js");
        }

        protected void PagePreRender(object sender, EventArgs e)
        {

        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);
        }
        #endregion
    }
}
