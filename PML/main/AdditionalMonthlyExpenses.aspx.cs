﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.UI;
using DataAccess;
using LendersOffice.Common;

namespace PriceMyLoan.main
{
    public partial class AdditionalMonthlyExpenses : PriceMyLoan.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //load from js 
            CPageData page = CPageData.CreatePmlPageDataUsingSmartDependency(LoanID, typeof(AdditionalMonthlyExpenses));
            page.AllowLoadWhileQP2Sandboxed = true;
            page.InitLoad();
            //sProRealETxPe.Text = page.sProRealETxPe_rep;
            //sProHazInsPe.Text = page.sProHazInsPe_rep;
            //sProHoAssocDuesPe.Text = page.sProHoAssocDuesPe_rep;
            //sProOHExpDescPe.Text = page.sProOHExpDesc;
            //sProOHExpPe.Text = page.sProOHExpPe_rep;
            //sMonthlyPmtPe.Text = page.sMonthlyPmtPe_rep;
            //sProMInsPe.Text = page.sProMInsPe_rep;

            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_PercentBaseT(sProRealETxT);

            Tools.SetDropDownListValue(sProHazInsT, page.sProHazInsT);
            Tools.SetDropDownListValue(sProRealETxT, page.sProRealETxT);
            page.sApprValPe_rep = RequestHelper.GetSafeQueryString("sApprValPe");
            page.sHouseValPe_rep = RequestHelper.GetSafeQueryString("sHouseValPe");
            page.sDownPmtPcPe_rep = RequestHelper.GetSafeQueryString("sDownPmtPcPe");
            page.sEquityPe_rep = RequestHelper.GetSafeQueryString("sEquityPe");
            page.sLtvRPe_rep = RequestHelper.GetSafeQueryString("sLtvRPe");
            page.sLAmtCalcPe_rep = RequestHelper.GetSafeQueryString("sLAmtCalcPe");

            ClientScript.RegisterHiddenField("sApprValPe", page.sApprValPe_rep);
            ClientScript.RegisterHiddenField("sHouseValPe", page.sHouseValPe_rep);
            ClientScript.RegisterHiddenField("sDownPmtPcPe", page.sDownPmtPcPe_rep);
            ClientScript.RegisterHiddenField("sEquityPe", page.sEquityPe_rep);
            ClientScript.RegisterHiddenField("sLtvRPe", page.sLtvRPe_rep);
            ClientScript.RegisterHiddenField("sLAmtCalcPe", page.sLAmtCalcPe_rep);

            sProHazInsR.Text = page.sProHazInsR_rep;
            sProHazInsMb.Text = page.sProHazInsMb_rep;
            sProHazInsHolder.Visible = !page.sHazardExpenseInDisbursementMode;
            sProRealETxR.Text = page.sProRealETxR_rep;
            sProRealETxMb.Text = page.sProRealETxMb_rep;
            sProRealETx.Text = page.sProRealETx_rep;
            sProRealETxHolder.Visible = !page.sRealEstateTaxExpenseInDisbMode;
            sProHazIns.Text = page.sProHazIns_rep;
            sProHoAssocDues.Text = page.sProHoAssocDues_rep;
            sProOHExpDesc.Text = page.sProOHExpDesc;
            sProOHExp.Text = page.sProOHExp_rep;
            sProOHExpLckd.Checked = page.sProOHExpLckd;
            sMonthlyPmtPe.Text = page.sMonthlyPmtPe_rep;

            MINote.Visible = page.BrokerDB.HasEnabledPMI; // OPM 26013
            MISection.Visible = !page.BrokerDB.HasEnabledPMI; // OPM 26013

            RegisterService("main", "/main/mainservice.aspx");
        }
        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
    }
}
