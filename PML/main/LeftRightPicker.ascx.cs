﻿#region Generated Code
namespace PriceMyLoan.main
#endregion
{
    using System;
    using PriceMyLoan.common;

    /// <summary>
    /// Provides a picker for selecting a set of options
    /// from a group of options.
    /// </summary>
    public partial class LeftRightPicker : UI.BaseUserControl
    {
        /// <summary>
        /// The settings for the picker.
        /// </summary>
        private LeftRightPickerSettings pickerSettings;

        /// <summary>
        /// Sets the sources for the picker.
        /// </summary>
        /// <param name="availableSource">
        /// The data source for the list of available items.
        /// </param>
        /// <param name="assignedSource">
        /// The data source for the list of assigned items.
        /// </param>
        public void SetPickerSettings(LeftRightPickerSettings settings)
        {
            this.pickerSettings = settings;
        }

        /// <summary>
        /// Initializes the picker.
        /// </summary>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            var basePage = this.Page as UI.BasePage;
            if (basePage == null)
            {
                var message = LendersOffice.Common.ErrorMessages.Generic;
                throw new global::DataAccess.CBaseException(message, message);
            }

            basePage.RegisterCSS("LeftRightPicker.css");
            basePage.RegisterJsScript("LeftRightPicker.js");

            base.OnInit(e);
        }

        /// <summary>
        /// Binds data for the picker.
        /// </summary>
        public override void DataBind()
        {
            if (this.pickerSettings == null)
            {
                var message = LendersOffice.Common.ErrorMessages.Generic;
                throw new global::DataAccess.CBaseException(message, message);
            }

            this.AvailableLabel.InnerText = this.pickerSettings.AvailableLabel;
            this.AssignedLabel.InnerText = this.pickerSettings.AssignedLabel;

            this.Available.DataSource = this.pickerSettings.AvailableSource;
            this.Available.DataTextField = this.pickerSettings.DataTextField;
            this.Available.DataValueField = this.pickerSettings.DataValueField;
            this.Available.DataBind();

            this.Assigned.DataSource = this.pickerSettings.AssignedSource;
            this.Assigned.DataTextField = this.pickerSettings.DataTextField;
            this.Assigned.DataValueField = this.pickerSettings.DataValueField;
            this.Assigned.DataBind();

            base.DataBind();
        }
    }
}