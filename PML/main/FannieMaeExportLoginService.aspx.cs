using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.DU;
using LendersOffice.Security;
using PriceMyLoan.DataAccess;
using PriceMyLoan.Security;
using LendersOffice.Constants;
using LendersOffice.ObjLib.ServiceCredential;
using System.Linq;

namespace PriceMyLoan.main
{
	public partial class FannieMaeExportLoginService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "ExportToDuDo":
                    ExportToDuDo();
                    break;
            }
        }
        private AbstractFnmaXisRequest CreateRequest() 
        {
            Guid sLId = GetGuid("LoanID");

            bool isAutoLogin = GetBool("AutoLogin", false);
            bool isDo = GetBool("IsDo");

            string fannieMaeMornetUserId = "";
            string fannieMaeMornetPassword = "";
            string sDuCaseId = "";
            string sDuLenderInstitutionId = "";

            if (isAutoLogin) 
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeExportLoginService));
                dataLoan.InitLoad();
                sDuCaseId = dataLoan.sDuCaseId;

                // 11/29/2007 dd - Use information in user settings.
                PriceMyLoanPrincipal principal = this.User as PriceMyLoanPrincipal;
                if (null != principal) 
                {
                    if (isDo)
                    {
                        fannieMaeMornetUserId = principal.DoAutoLoginNm;
                        fannieMaeMornetPassword = principal.DoAutoPassword;
                    }
                    else
                    {
                        if (principal.BrokerDB.UsingLegacyDUCredentials)
                        {
                            fannieMaeMornetUserId = principal.DuAutoLoginNm;
                            fannieMaeMornetPassword = principal.DuAutoPassword;
                        }
                        else
                        {
                            var savedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                                    .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter && credential.IsEnabledForNonSeamlessDu);
                            if (savedCredential != null)
                            {
                                fannieMaeMornetUserId = savedCredential.UserName;
                                fannieMaeMornetPassword = savedCredential.UserPassword.Value;
                            }
                        }
                    }
                } 
                else 
                {
                    Tools.LogBug("Authentication is not PriceMyLoanPrincipal. Unable to set auto login for DO/DU.");
                }
            } 
            else 
            {
                // 11/29/2007 dd - Use information provide by user.
                fannieMaeMornetUserId = GetString("FannieMaeMORNETUserID");
                fannieMaeMornetPassword = GetString("FannieMaeMORNETPassword");
                sDuLenderInstitutionId = GetString("sDuLenderInstitutionId", "");

                sDuCaseId = GetString("sDuCaseId", "");
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeExportLoginService));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sDuCaseId = sDuCaseId;
                dataLoan.sDuLenderInstitutionId = sDuLenderInstitutionId;
                dataLoan.Save();

                bool rememberLastLogin = GetBool("RememberLastLogin");
                string rememberLoginNm = rememberLastLogin ? GetString("FannieMaeMORNETUserID") : "";
                string parameterName = isDo ? "@DOLastLoginNm" : "@DULastLoginNm";

                AbstractUserPrincipal principal = (AbstractUserPrincipal) this.User;
                SqlParameter[] parameters = {
                                                new SqlParameter("@UserId", principal.UserId),
                                                new SqlParameter(parameterName, rememberLoginNm) 
                                            };
                StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "UpdateLastDoDuLpLoginNm", 3, parameters);
            }

            FnmaDwebCasefileImportRequest request = new FnmaDwebCasefileImportRequest(isDo, sLId);
            request.FannieMaeMORNETUserID = fannieMaeMornetUserId;
            request.FannieMaeMORNETPassword = fannieMaeMornetPassword;
            request.MornetPlusCasefileIdentifier = sDuCaseId;

            return request;

        }
        private void ExportToDuDo() 
        {
            string status = "ERROR";
            string errorMessage = "";

            AbstractFnmaXisRequest request = CreateRequest();

            try
            {
                AbstractFnmaXisResponse response = DUServer.Submit(request);

                if (response.IsReady)
                {
                    // DWEB Casefile Import only support synchronous connection.
                    if (response.HasError)
                    {
                        status = "ERROR";
                        errorMessage = response.ErrorMessage;
                    }
                    else if (response.HasBusinessError)
                    {
                        status = "ERROR";
                        errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                    }
                    else
                    {
                        AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
                        bool isDo = GetBool("IsDo");
                        Guid sLId = GetGuid("LoanID");
                        SubmitToFnmaAuditItem audit = new SubmitToFnmaAuditItem(principal, isDo, request.FannieMaeMORNETUserID);
                        AuditManager.RecordAudit(sLId, audit);

                        string cacheAuth = string.Format("{0}\n{1}\n{2}\n{3}", request.Url, request.FannieMaeMORNETUserID, request.FannieMaeMORNETPassword, response.MornetPlusCasefileIdentifier);
                        string id = AutoExpiredTextCache.AddToCache(cacheAuth, new TimeSpan(0, 1, 0)); // Cache for 1 minute.

                        status = "DONE";
                        SaveResponse((FnmaDwebCasefileImportResponse)response);
                        SetResult("sDuCaseId", response.MornetPlusCasefileIdentifier);
                        SetResult("id", id);
                    }
                }
            }
            catch (PageDataAccessDenied exc)
            {
                status = "ERROR";
                errorMessage = exc.UserMessage;
            }
            catch (CBaseException exc)
            {
                status = "ERROR";
                errorMessage = exc.UserMessage;
                Tools.LogErrorWithCriticalTracking("PML: Unable to export file to DU", exc);
            }
            catch (Exception exc)
            {
                status = "ERROR";
                errorMessage = ErrorMessages.Generic;
                Tools.LogErrorWithCriticalTracking("PML: Unable to export file to DU", exc);
            }

            SetResult("Status", status);
            SetResult("ErrorMessage", errorMessage);
        }

        private void SaveResponse(FnmaDwebCasefileImportResponse response) 
        {
            Guid sLId = GetGuid("LoanID");
            response.SaveToFileDB(sLId);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeExportLoginService));

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDuCaseId = response.MornetPlusCasefileIdentifier;
            dataLoan.Save();
        }


	}
}
