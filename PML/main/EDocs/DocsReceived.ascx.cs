﻿namespace PriceMyLoan.main.EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using global::DataAccess;
    using global::EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;

    public partial class DocsReceived : PriceMyLoan.UI.BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var basePage = this.Page as BasePage;
            if (basePage == null)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "This user control is meant to be used on a BasePage.");
            }

            var repo = EDocumentRepository.GetUserRepository();
            var docs = repo.GetDocumentsByLoanId(LoanID).OrderByDescending(p => p.LastModifiedDate).ToArray();
            var xmlDocs = repo.GetGenericDocumentsByLoanId(LoanID).ToArray();
            var xmlDocDict = new HashSet<Guid>(xmlDocs.Select(doc => doc.DocumentId));

            if (docs.Any(doc => !doc.LoanId.HasValue))
            {
                throw new CBaseException(ErrorMessages.Generic, "DocsReceive-Encountered EDocument without loan id. Should not be possible.");
            }

            var brokerDB = BrokerDB.RetrieveById(LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId);

            bool isBarcodeScannerEnabled = brokerDB.IsDocMagicBarcodeScannerEnabled ||
                brokerDB.IsDocuTechBarcodeScannerEnabled ||
                brokerDB.IsIDSBarcodeScannerEnabled;

            this.DocMagicStatusBar.Visible = isBarcodeScannerEnabled;

            basePage.RegisterJsObjectWithJsonNetAnonymousSerializer("docsReceivedVm", new
            {
                docs = docs.Select(currentDoc => new
                {
                    currentDoc.DocumentId,
                    DocStatus = EDocument.StatusText(currentDoc.DocStatus),
                    IsObsoleteOrRejected = currentDoc.DocStatus == E_EDocStatus.Obsolete || currentDoc.DocStatus == E_EDocStatus.Rejected,
                    currentDoc.Folder.FolderNm,
                    currentDoc.DocTypeName,
                    currentDoc.AppName,
                    Description = currentDoc.PublicDescription,
                    currentDoc.LastModifiedDate,
                    currentDoc.PageCount,
                    LinkedGenericDoc = xmlDocDict.Contains(currentDoc.DocumentId),
                }),
                genericDocs = xmlDocs.Where(g => !g.IsLinkedToEDoc).Select(xmlDoc => new
                {
                    xmlDoc.DocumentId,
                    xmlDoc.FolderNm,
                    xmlDoc.DocTypeName,
                    xmlDoc.AppName,
                    xmlDoc.Description,
                    LastModifiedDate = xmlDoc.CreatedD,
                }),
                isBarcodeScannerEnabled,
                brokerDB.HideStatusColumnInTpoEdocs,
            });

            basePage.RegisterJsGlobalVariables("isBarcodeScannerEnabled", isBarcodeScannerEnabled);
            basePage.RegisterJsGlobalVariables("HideStatusColumn", brokerDB.HideStatusColumnInTpoEdocs);

        }
    }
}
