﻿namespace PriceMyLoan.main.EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Services;
    using System.Web.UI;
    using Common;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.Integration.GenericFramework;

    public partial class EDocs : UI.BasePage
    {
        private const int DocumentListTabIndex = 3;

        public override bool IsReadOnly
        {
            get { return false; }
        }

        protected override IEnumerable<WorkflowOperation> GetAdditionalOperationsThatNeedChecks()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UseDocumentCapture
            };
        }

        public override IEnumerable<WorkflowOperation> GetReasonsForOp()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UseDocumentCapture
            };
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                return E_XUAComaptibleValue.Edge;
            }

            return base.GetForcedCompatibilityMode();
        }

        protected override bool RunValidationOverride
        {
            get { return false; }
        }

        public bool showUploadPopup
        {
            get { return RequestHelper.GetBool("redirect") && tabIndex == 1 && !IsPostBack; }
        }


        public Guid sLId
        {
            get { return RequestHelper.GetGuid("loanid", Guid.Empty); }
        }

        protected string pipelineUrl
        {
            get { return VirtualRoot + "/Main/Pipeline.aspx"; }
        }

        public string tabsBG
        {
            get { return "PML/images/tabs-bg.gif"; }
        }

        public int tabIndex;

        protected void Page_Init(object sender, EventArgs e)
        {

            m_loadDefaultStylesheet = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.css");

            tabIndex = 1;

            this.RegisterJsGlobalVariables("HideStatusSubmissionButtonsInOriginatorPortal", this.PriceMyLoanUser.BrokerDB.HideStatusSubmissionButtonsInOriginatorPortal);
            this.RegisterJsGlobalVariables("HideStatusAndAgentsPageInOriginatorPortal", this.PriceMyLoanUser.BrokerDB.HideStatusAndAgentsPageInOriginatorPortal);

            var hideUploadAndFaxLinks = this.PriceMyLoanUser.EnableNewTpoLoanNavigation && this.PriceMyLoanUser.BrokerDB.HideUploadAndFaxLinksInTpoLoanNavigationEdocs;
            this.RegisterJsGlobalVariables("HideUploadAndFaxLinksInTpoEdocs", hideUploadAndFaxLinks);
            if (hideUploadAndFaxLinks)
            {
                tabIndex = DocumentListTabIndex;
            }
            else if (Request.Form["__EVENTTARGET"] == "tabIndex")
            {
                tabIndex = Int32.Parse(Request.Form["__EVENTARGUMENT"]);
            }
            else if (RequestHelper.GetBool("redirect"))
            {
                tabIndex = 1;
            }

            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            RegisterJsScript("jquery.idTabs.min.js");
            RegisterJsScript("/utilities.js");
            this.RegisterService("edoc", "/main/EDocs/EdocsService.aspx");

            if (!this.PriceMyLoanUser.BrokerDB.EnableKtaIntegration)
            {
                this.tab4.Visible = false;
                this.DocumentCaptureTabLink.Visible = false;
            }

            ClientScript.RegisterHiddenField("DocTypePickerUrl", Page.ResolveClientUrl("~/main/EDocs/DocTypePicker.aspx"));

            if (false == CurrentUserCanPerform(WorkflowOperations.ReadLoan))
            {
                PriceMyLoanRequestHelper.AccessDenial(RequestHelper.GetGuid("loanid", Guid.Empty));
            }

            SetEnabledPerformancePageRenderTimeWarning(false);

            CPageData dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(EDocs));
            dataloan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataloan.InitLoad();

            this.LoanNavHeader.SetDataFromLoan(dataloan, LinkLocation.TpoLoanNavigationEdocs);

            if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                this.FaxCoversheetsButton.Visible = this.PriceMyLoanUser.BrokerDB.IsEnableBrokerFaxPackages;
                this.SetSubmitToConditionReviewButtonDisplay(dataloan);
            }
            else
            {
                this.LoanNavEdocsDiv.Visible = false;
            }
        }

        protected void FaxCoversheetsButton_Click(object sender, EventArgs e)
        {
            PriceMyLoanRequestHelper.DownloadCoverSheetList(this.LoanID);
        }

        /// <summary>
        /// Sets the display of the "Submit to Condition Review" button.
        /// </summary>
        /// <param name="dataloan">
        /// The loan file to submit into the "Condition Review" status.
        /// </param>
        private void SetSubmitToConditionReviewButtonDisplay(CPageData dataloan)
        {
            var enabledBrokerStatuses = this.PriceMyLoanUser.BrokerDB.GetEnabledStatusesByChannelAndProcessType(
                                dataloan.sBranchChannelT,
                                dataloan.sCorrespondentProcessT);

            if (!enabledBrokerStatuses.Contains(E_sStatusT.Loan_DocumentCheck))
            {
                this.SubmitToDocumentCheckButton.Visible = false;
                return;
            }

            if (!dataloan.LoanFileWorkflowRules.CanChangeLoanStatus(E_sStatusT.Loan_DocumentCheck))
            {
                this.SubmitToDocumentCheckButton.Disabled = true;
                this.SubmitToDocumentCheckButton.Attributes.Add("title", "You do not currently have permission to perform this operation on this file.");
            }
        }

        [WebMethod]
        public static void DeleteDocMagicError(string id)
        {
            DocMagicPDFManager.RemoveDocMagicError(LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId, new Guid(id));
        }

        [WebMethod]
        public static object GetDocMagicStatus(string slid)
        {
            IList<DocMagicPDFStatusData> statuses = DocMagicPDFManager.GetUploadsForLoan(LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId, new Guid(slid));

            List<DocMagicPDFStatusData> inQStatus = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> prepStatus = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> readBarStaturs = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> splitStatus = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> errorStatus = new List<DocMagicPDFStatusData>();

            foreach (DocMagicPDFStatusData status in statuses)
            {
                List<DocMagicPDFStatusData> list;
                switch (status.Status)
                {
                    case E_DocMagicPDFStatus.IN_Q:
                        list = inQStatus;
                        break;
                    case E_DocMagicPDFStatus.CONVERT_TIFF:
                        list = prepStatus;
                        break;
                    case E_DocMagicPDFStatus.SCAN_BARCODES:
                        list = readBarStaturs;
                        break;
                    case E_DocMagicPDFStatus.CREATING_EDOCS:
                        list = splitStatus;
                        break;
                    case E_DocMagicPDFStatus.ERROR:
                        list = errorStatus;
                        break;
                    default:
                        throw new UnhandledEnumException(status.Status);
                }

                list.Add(status);
            }


            return new
            {
                InQStatus = inQStatus,
                ConvertStatus = prepStatus,
                ScanStatus = readBarStaturs,
                SplitStatus = splitStatus,
                ErrorStatus = errorStatus
            };
        }
    }
}
