﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using EDocs;
using LendersOffice.Security;
using PriceMyLoan.Security;

namespace PriceMyLoan.main.EDocs
{
    public partial class DocTypePickerControl : System.Web.UI.UserControl
    {
        // These fields double as html tag attributes
        // So, you can do something like
        //     <uc:DocTypePicker id="..." runat="..." Width="250px" OnChange="alert('hello');">
        public string Width = "";
        public string OnChange = "";
        public string Value { get { return m_selectedDocType.Text; } }
        public bool Enabled
        {
            get
            {
                return m_selectedDocType.Enabled && m_selectedDocTypeDescription.Enabled;
            }
            set
            {
                m_selectedDocType.Enabled = value;
                m_dialogLink.Style["display"] = value ? "" : "none";
            }
        }
        public TextBox ValueCtrl { get { return m_selectedDocType; } }
        public Label DescriptionCtrl { get { return m_selectedDocTypeDescription; } }

        private Guid BrokerID { get { return ((PriceMyLoanPrincipal)Page.User).BrokerId; } }

        public void Clear()
        {
            m_selectedDocType.Text = "-1";
            m_selectedDocTypeDescription.Text = "No Doc Type Selected";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            m_selectedDocType.Style.Add("display", "none");
            m_selectedDocTypeDescription.Text = "No Doc Type Selected";

            // This should be put in the ascx file, but we can't put it there since the control is runat="server"
            m_dialogLink.Attributes["onclick"] = this.ClientID + ".showDialog()";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            m_DocTypePickerDiv.Style.Add("width", Width);
            m_selectedDocType.Attributes.Add("onchange", OnChange);
            if (this.Value != "-1")
            {
                var docTypeList = EDocumentDocType.GetDocTypesByBroker(BrokerID).ToList();
                var selectedDocType = docTypeList.Find((DocType d) => d.DocTypeId == this.Value);
                if (selectedDocType == null)
                {
                    return;
                }
                m_selectedDocTypeDescription.Text = selectedDocType.FolderAndDocTypeName;
            }
        }
    }
}