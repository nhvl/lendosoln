﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReqDocTypeResolveWarning.aspx.cs" Inherits="PriceMyLoan.main.EDocs.ReqDocTypeResolveWarning" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        html, body
        {
            overflow: hidden;
        }
        
        #divButtons
        {
            text-align: right;
            position: absolute;
            bottom: 15px;
            right: 15px;
        }
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
        <div class="FieldLabel">
            <ml:EncodedLabel ID="lblWarning" runat="server" />
            <div id="divButtons">
                <input type="button" value=" OK "  id="OkBtn"/> 
                <input type="button" value="Cancel" id="CancelBtn" />
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $j(function(){
            var LblWarning = <%=AspxTools.JsGetElementById(lblWarning)%>;
            var dialogArgs = parent.LQBPopup.GetArguments();
            
            if (dialogArgs && dialogArgs.resolveAction && dialogArgs.resolveAction === 'Confirm') {
                $j(LblWarning).text('This condition is missing an association with a required document type. Are you sure you want to resolve?');
            } else {
                // NOTE: DEFAULTS TO Block ACTION
                $j('#OkBtn').hide();
                $j('#CancelBtn').prop('value',' OK ');
                $j(LblWarning).text('This condition is missing an association with a required document type. Please associate with the required document type in order to resolve.');
            }
            
            $j('#CancelBtn').click(function(){
                parent.LQBPopup.Hide();
            });
            
            $j('#OkBtn').click(function(){
                parent.LQBPopup.Return(dialogArgs.act);
            });
        });
    </script>
</body>
</html>
