﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="UploadDocToTask.aspx.cs" Inherits="PriceMyLoan.main.EDocs.UploadDocToTask" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Condition-Document Upload</title>
    <style type="text/css">
        body { padding: 10px;  }
        table.DocDetails {
             table-layout: fixed;
        }
       table.DocDetails{width: 650px;  }
       label.desc { vertical-align: top ; }
       span.DocTypePicker { display: block; margin-top: 4px; }
       td.Error { color: #EA1C0D; }
       .max-width-cell { 
           max-width: 300px;
           word-wrap: break-word;
       }
    </style>
</head>
<body>
<form id="form1" runat="server">
    <div lqb-popup class="modal-pipeline-section upload-docs-section">
        <div class="modal-header">
            <button type="button" class="close" id="CloseBtn"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title">Upload Docs</h4>
        </div>
        <div class="modal-body">
            <table runat="server" id="TaskTable" border="1" cellpadding="0" cellspacing="0" class="DataGrid none-hover-table wide margin-bottom">
                <thead>
                    <tr class="GridHeader">
                        <th>Condition</th>
                        <th>Category</th>
                        <th>Subject</th>
                        <th>Required DocType</th>
                    </tr>
                </thead>
                <tr class="GridItem">
                <td><ml:EncodedLiteral runat="server" ID="TaskID"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="CategoryRep"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="Subject"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="RequiredDocType"></ml:EncodedLiteral></td>
                </tr>
            </table>

            <p class="text-grey label-doc">
                <ml:EncodedLiteral runat="server" ID="ModalDescription">Please select document(s) to upload and associate with this condition.</ml:EncodedLiteral>
            </p>

            <table class="primary-table none-hover-table wide">
                <tr>
                    <th>File Name</th>
                    <th>Doc Type</th>
                    <th>Description</th>
                    <th>Application</th>
                    <th>&nbsp;</th>
                </tr>
                <asp:Repeater runat="server" ID="FileListingTable" OnItemDataBound="FileListingTable_ItemDataBound">
                    <ItemTemplate>
                        <tr class="file-row">
                            <td class="max-width-cell">
                                <ml:EncodedLabel runat="server" ID="Filename" CssClass="Filename"></ml:EncodedLabel>
                            </td>
                            <td class="max-width-cell">
                                <ml:EncodedLabel runat="server" ID="RequiredDocumentTypeName" CssClass="required-doc-type-name" />
                                <input type="hidden" runat="server" id="RequiredDocumentTypeId" value="-1" class="SelectedDocTypeId" />
                                <a runat="server" id="selectDocType" class="select-doc-type">select Doc Type</a>
                                <i id="validatorIcon" runat="server" class="material-icons validator-icon"></i>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="Description" TextMode="MultiLine" CssClass="Description" maxcharactercount="200"></asp:TextBox>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ApplicationDdl" CssClass="ApplicationDdl"></asp:DropDownList>
                            </td>
                            <td>
                                <a class="remove-link">remove</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
           
        </div>
        <div class="modal-footer">
            <button type="button" id="CancelBtn" class="btn btn-flat">Cancel</button>
            <button type="button" id="UploadButton" class="btn btn-flat">Upload Document</button>
        </div>
    </div>
</form>

<script>
    var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    var iframeSelf = new IframeSelf();
    iframeSelf.popup = popup;

    function closeTaskDocumentUploadPopup() {
        iframeSelf.resolve(null);
    }

    (function ($) {
        $(function () {
            checkUploadButtonState();

            $('#CancelBtn, #CloseBtn').click(function () {
                iframeSelf.resolve(null);
            });

            $('.remove-link').click(removeDocument);
            $('#UploadButton').click(function () {
                uploadDocuments(iframeSelf);
            });

            TPOStyleUnification.Components();
        });

        $(".select-doc-type").click(function (event) {
            LQBPopup.Show(ML.DocTypePickerUrl, {
                onReturn: function (id, name) {
                    var $link = $(retrieveEventTarget(event));
                    $link.text('change Doc Type');

                    var $parent = $link.parent();
                    $parent.find('.SelectedDocTypeId').val(id);
                    $parent.find('.required-doc-type-name').text(name);
                    $parent.find('.validator-icon').html("&#xE86C;").removeClass("red rotate-45").addClass("green");

                    checkUploadButtonState();
                }
            });
        });
    })(jQuery);
</script>
</body>
</html>
