﻿<%@ Page
    Language="C#"
    AutoEventWireup="True"
    CodeBehind="DocTypePicker.aspx.cs"
    Inherits="PriceMyLoan.main.EDocs.DocTypePicker"
    EnableViewState="false"
%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Document Type Picker</title>
    <style>
        .AlignLeft
        {
        	text-align: left;
        }
        .Highlight
        {
        	background-color:Yellow;
        }

        #SearchDiv
        {

        }

        #ExplorerDiv
        {
        	width: 100%;
        	overflow-x: hidden;
        }

        #m_DocTypeExplorer
        {

        }

        #Buttons
        {
            text-align: center;
        }
    </style>
    <script src="../../inc/jquery.tablesorter.min.js"></script>
</head>
<body class="scroll-vertical-none">
<script>
<!--
    $j(document).ready(function() {
        TPOStyleUnification.TableSorterStyle($j("#m_SearchResults"));
    });

    function _init() {
        f_searchBoxKeyUp();
        $j("#m_SearchResults").tablesorter();
        $j("#m_SearchResults").bind("sortEnd", function() {
            setClass($j('#m_SearchResults tbody')[0]);
            TPOStyleUnification.fixedHeaderTable(document.getElementById("m_SearchResults"));
        });
        TPOStyleUnification.fixedHeaderTable(document.getElementById("m_SearchResults"));
    }

    // Retain alternating row style
    function setClass(tBody) {
        for (var i = 0; i < tBody.rows.length; i++) {
            var row = tBody.getElementsByTagName("tr")[i];
            if (i % 2 < 1)
                row.className = 'GridItem';
            else
                row.className = 'GridAlternatingItem';
        }
    }

    // ##### Codebehind! ####################
    // These functions will be called by the codebehind

    function highlight(word, docTypeId) {
        var regex = new RegExp(word, 'g');
        var replacement = "<span class='Highlight'>" + word + "</span>";
        var $elements = $j('.'+docTypeId);
        $elements.each(function (i, element) {
            var text = $j(element).html();
            text = text.replace(regex, replacement);
            $j(element).html(text);
        });
    }

    function f_searchBoxKeyUp() { // this function is connected to the textbox in the codebehind
        var searchBox = document.getElementById('SearchBox');
        var searchBtn = document.getElementById('SearchBtn');
        var len = searchBox.value.length;

        searchBtn.disabled = len < 1;
    }
    // ######################################



    function f_openFolder(folderId) {
        window.location = 'DocTypePicker.aspx' + '?folderid=' + encodeURIComponent(folderId);
    }

    function f_toFolders() {
        window.location = 'DocTypePicker.aspx';
    }

    function f_close(){
        parent.LQBPopup.Hide();
    }

    function f_selectDocType(docTypeId, docTypeName) {
        parent.LQBPopup.Return(docTypeId, docTypeName);
    }

-->
</script>
<form runat="server">
    <div>
        <div class="modal-header">
            <button type="button" class="close" onclick="f_close()"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title"><span><ml:EncodedLiteral ID="m_HeaderMessage" runat="server"/></span></h4>
        </div>

        <div class="modal-body container-fluid">
            <div id="BreadcrumbsDiv" class="breadcrumbs-dialogs" runat="server" >
                <a runat="server" href='#' onclick="f_toFolders()">Folders</a> >
                <ml:EncodedLabel runat="server" ID="m_selectedFolder" Text="No Folder Selected"></ml:EncodedLabel>
            </div>
            <div id="SearchDiv">
                <div class="table">
                    <div>
                        <div class="text-grey text-bold">
                            Search for:
                        </div>
                        <div>
                            <asp:TextBox runat="server" id="SearchBox" name="q"/>
                        </div>
                        <div>
                            <span><button ID="SearchBtn" OnServerClick="SearchBtn_Click" runat="server" class="btn btn-default">Search <i class="material-icons">&#xE8B6;</i></button></span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ExplorerDiv">
                <div id="DocFolderDiv" runat="server">
                    <div class="choose-doc-folder text-grey text-bold"><label><ml:EncodedLiteral ID="m_FolderMessage" runat="server" Text="Choose a Doc Folder:"/></label></div>
                    <asp:GridView ID="m_DocFolderExplorer" runat="server" AutoGenerateColumns="false" EnableViewState="false" CssClass="DataGrid primary-table wide  modal-table-scrollable modal-table-doc-type" AlternatingRowStyle-CssClass="GridAlternatingItem HoverHighlight" RowStyle-CssClass="GridItem HoverHighlight" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAlternatingItem" />
                        <RowStyle CssClass="GridItem"/>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="AlignLeft" />
                                <HeaderTemplate>Folder</HeaderTemplate>
                                <ItemTemplate>
                                    <a href='#' onclick="f_openFolder(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderId").ToString()) %>)">
                                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FolderNm").ToString()) %>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="DocTypeDiv" runat="server">
                    <div class="choose-doc-folder text-grey text-bold"><label>Choose a Doc Type:</label></div>
                    <asp:GridView ID="m_DocTypeExplorer" runat="server" AutoGenerateColumns="false" EnableViewState="false" CssClass="DataGrid primary-table wide  modal-table-scrollable modal-table-doc-type" AlternatingRowStyle-CssClass="GridAlternatingItem HoverHighlight" RowStyle-CssClass="GridItem HoverHighlight" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader AlignLeft" />
                        <AlternatingRowStyle CssClass="GridAlternatingItem" />
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="AlignLeft" />
                                <HeaderTemplate>Doc Type</HeaderTemplate>
                                <ItemTemplate>
                                    <a href='#' onclick="f_selectDocType(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderAndDocTypeName").ToString())%>)">
                                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeName").ToString())%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div id="SearchResultDiv" runat="server">
                    <div class="text-grey text-bold"><ml:EncodedLabel ID="m_SearchMessage" runat="server" Text="Matching Doc Types:"></ml:EncodedLabel></div>
                    <asp:GridView ID="m_SearchResults" runat="server" AutoGenerateColumns="false" EnableViewState="false" CssClass="DataGrid modal-table-scrollable modal-table-doc-type" AlternatingRowStyle-CssClass="GridAlternatingItem HoverHighlight" RowStyle-CssClass="GridItem HoverHighlight" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader AlignLeft" />
                        <AlternatingRowStyle CssClass="GridAlternatingItem" />
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="AlignLeft" />
                                <HeaderTemplate><a>Doc Type</a></HeaderTemplate>
                                <ItemTemplate>
                                    <a
                                        data="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeName").ToString())%>"
                                        href='#'
                                        onclick="f_selectDocType(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderAndDocTypeName").ToString())%>)"
                                    >
                                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeName").ToString())%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="AlignLeft" />
                                <HeaderTemplate><a>Folder</a></HeaderTemplate>
                                <ItemTemplate>
                                    <a
                                        data="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FolderNm").ToString()) %>"
                                        href='#'
                                        onclick="f_openFolder(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderId").ToString()) %>)"
                                    >
                                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FolderNm").ToString()) %>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</form>
</body>
</html>
