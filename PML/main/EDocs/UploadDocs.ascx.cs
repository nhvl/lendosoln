﻿namespace PriceMyLoan.main.EDocs
{
    using System;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    public partial class UploadDocs : PriceMyLoan.UI.BaseUserControl
    {
        private string PipelineUrl
        {
            get { return VirtualRoot + "/Main/Pipeline.aspx"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(UploadDocs));
            dataloan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataloan.InitLoad();

            var basePage = (BasePage)this.Page;

            basePage.RegisterJsScript("utilities.js");
            basePage.RegisterJsScript("DragDropUpload.js");
            basePage.RegisterJsScript("DocumentUploadHelper.js");
            basePage.RegisterJsScript("UploadDocs.js");

            basePage.RegisterCSS("UploadDocs.css");
            basePage.RegisterCSS("DragDropUpload.css");

            basePage.RegisterJsGlobalVariables("sLId", this.LoanID);
            basePage.RegisterJsGlobalVariables("PipelineUrl", this.PipelineUrl);
            basePage.RegisterJsGlobalVariables("UploadToOcr", this.PriceMyLoanUser.BrokerDB.OCRHasConfiguredAccount && this.PriceMyLoanUser.BrokerDB.IsOCREnabled);
            basePage.RegisterJsGlobalVariables("ScanDocMagicBarcodesDocTypeId", ConstApp.ScanDocMagicBarcodesDocTypeId);

            basePage.RegisterJsObjectWithJsonNetAnonymousSerializer("Apps", dataloan.GetAppNameAndIds());

            Tuple<bool, string> results = Tools.IsWorkflowOperationAuthorized(this.PriceMyLoanUser, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.UploadEDocs);
            basePage.RegisterJsGlobalVariables("CanUploadEdocs", results.Item1);
            basePage.RegisterJsGlobalVariables("UploadEdocsFailureMessage", results.Item2);
        }
    }
}
