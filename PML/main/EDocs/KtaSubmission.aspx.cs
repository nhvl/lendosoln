﻿#region Generated code
namespace PriceMyLoan.main.EDocs
#endregion
{
    using System;
    using LendersOffice.Constants;

    /// <summary>
    /// Provides a means for submitting to KTA.
    /// </summary>
    public partial class KtaSubmission : UI.BasePage
    {
        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Action = ConstStage.KtaApiUrl;
        }
    }
}