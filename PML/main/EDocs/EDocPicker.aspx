﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EDocPicker.aspx.cs" Inherits="PriceMyLoan.main.EDocs.EDocPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>EDocument Selector</title>
    <style>

        div.buttonPanel  {
            text-align: center;
            margin-top: 10px;
        }
        .highlight {
            background-color: #ffff00 !important;
        }
    </style>
</head>
<body class="EditBackground">
<form id="form1" runat="server">
    <div lqb-popup class="modal-pipeline-section associate-doc-section">
        <div class="modal-header">
            <button type="button" class="close" id="CloseBtn"><i class="material-icons">&#xE5CD;</i></button>
            <h4 class="modal-title"><ml:EncodedLiteral ID="ModalTitle" runat="server">Select additional documents to add to this condition</ml:EncodedLiteral></h4>
        </div>
        <div class="modal-body scrollable">
            <p class="text-grey label-doc" id="Description" runat="server">Please select which documents to associate with this condition. Documents of the required type are highlighted.</p>
            <table class="DataGrid wide margin-bottom none-hover-table" id="ConditionTable" runat="server">
                <tr class="GridHeader">
                    <th>Condition</th>
                    <th>Category</th>
                    <th>Subject</th>
                    <th>Required Doc Type</th>
                    <th>Due Date</th>
                </tr>
                <tr class="GridItem" style="font-weight:normal;">
                <td><ml:EncodedLiteral runat="server" ID="ConditionId"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="CondCategory"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="TaskSubject"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="CondRequiredDoctype"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="TaskDueDate"></ml:EncodedLiteral></td>
                </tr>
            </table><br/>

            <div class="table">
                <div>
                    <div class="text-grey"><label>Search By Description:</label></div>
                    <div><input type="text" name="Query" id="Query" /></div>
                </div>
            </div>

            <div class="scrollable">
                <asp:Repeater runat="server" ID="DocListing">
                    <HeaderTemplate>
                        <table id="DocTable" class="DataGrid modal-table-scrollbar table-associate-doc wide none-hover-table">
                            <thead>
                                <tr class="GridHeader">
                                    <th class="none"><input NoLabel type="checkbox" class="<%# AspxTools.HtmlString(GetSelectAllClass()) %>" name="selectAll" id="selectAll" /></th>
                                    <th><a>Folder</a></th>
                                    <th><a>DocType</a></th>
                                    <th><a>Description</a></th>
                                    <th><a>Last Modified</a></th>
                                    <th>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class='docRow <%# AspxTools.HtmlString(GetEDocClass(  ((EDocument)Container.DataItem ))) %>' style="font-weight:normal;">
                            <td><input NoLabel type="checkbox" data-docid='<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString())  %>'
                                    data-doctypeid='<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentTypeId.ToString())  %>' class="picked EdocCb">
                            </td>
                            <td><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).Folder.FolderNm ) %></td>
                            <td><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocTypeName ) %></td>
                            <td class="PublicDescription"><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).PublicDescription ) %></td>
                            <td><%# AspxTools.HtmlString( ((EDocument)Container.DataItem).LastModifiedDate.ToString("M/d/yy h:mm tt")) %></td>
                            <td class="td-short-width"><a href="#" data-docid='<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString() )%>' class="view"  >view</a></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="CancelBtn" class="btn btn-flat">Cancel</button>
            <button type="button" id="OkBtn"     class="btn btn-flat">OK    </button>
        </div>
    </div>
</form>
<script>(function($){
    var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
    var iframeSelf = new IframeSelf();
    iframeSelf.popup = popup;

    iframeSelf.onShow = function(settings){
        var rows = $('#DocTable tbody tr'), data = [], $query = $('#Query'), timeoutID = null,
            str = null, $o, $tds, isAlt = false, cbx = $('input.picked'), docs = [];

        var dialogArgs = settings.args;
        if (dialogArgs && dialogArgs.docs) {
            docs = JSON.parse(dialogArgs.docs);
        }

        resizeForIE6And7(700,500);
        var filter = [];
        if (window.dialogArguments && window.dialogArguments.FilterDocsIds){
            filter = window.dialogArguments.FilterDocsIds;
        }
        rows.each( function(i,o){
            $o = $(o);
            $tds = $o.children('td');
            if ($.inArray($tds.eq(0).find('input.picked').data('docid'), filter)> -1) {
                $o.remove();
                return;
            }
            if ($.inArray($tds.eq(0).find('input.picked').data('docid'), docs) > -1)  {
                $tds.eq(0).find('input').prop('checked', true);
            }
            str = $tds.eq(2).text().toLowerCase() + '~' + $tds.eq(3).text().toLowerCase() + '~' + $tds.eq(4).text().toLowerCase();
            data.push(str);
            $o.addClass(isAlt ? 'GridAlternatingItem' : 'GridItem');
            isAlt = !isAlt;
        });

        rows = $('#DocTable tbody tr');

        $('#DocTable').on( 'click', 'a.view', function(){
            var docid= $(this).data('docid');
            window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid , '_parent');
            return false;
        }).tablesorter({
            headers : { 0: { sorter: false}, 2: { sorter: "text"}, 5: {sorter: false} }
        }).on("sortEnd", function(){
            var alt = false;
            $('#DocTable tbody tr').each(function(i,o){
                if ( alt ) { $(o).removeClass('GridItem').addClass('GridAlternatingItem');}
                else { $(o).removeClass('GridAlternatingItem').addClass('GridItem'); }
                alt = !alt;
            });
        });
        TPOStyleUnification.TableSorterStyle($('#DocTable'));

        function searchTable() {
            var value = $query.val(), i = 0, rowCount = data.length, row;
            if (value.length == 0) {
                rows.show();
                return;
            }

            value = value.toLowerCase();

            for (i; i < rowCount; i++) {
                row = data[i];
                if (row.indexOf(value) == -1) {
                    rows.eq(i).hide();
                }
                else {
                    rows.eq(i).show();
                }
            }
        }

        $query.keyup (function(){
            if (timeoutID) {
                clearTimeout(timeoutID);
            }

            timeoutID = setTimeout(searchTable, 500);
        });

        $('#selectAll').change(function(){
            var $o = $(this);

            if ($o.is(':checked')) {
                cbx.filter(':visible').prop('checked', true);
            }
            else {
                cbx.prop('checked', false);
            }
            cbx.change();
        });

        if (ML.IsSinglePickOnly) {
            $('.EdocCb').change(function () {
                if ($(this).prop('checked')) {
                    $('.EdocCb').not($(this)).prop('checked', false);
                }
            });
        }

        $('#CancelBtn, #CloseBtn').click(function () {
            iframeSelf.resolve(null);
        });

        $('#OkBtn').click(function(){
            var docIds = [];
            var docDescriptions = {};
            var docTypeIds = [];
            cbx.filter(':checked').each(function(){
                var docId = $(this).attr('data-docid')
                var description = $(this).closest('.docRow').find('.PublicDescription').text();

                docIds.push(docId);
                docDescriptions[docId] = description;

                var docTypeId = $(this).attr('data-doctypeid');
                if ( $.inArray( docTypeId, docTypeIds) == -1) {
                    docTypeIds.push(docTypeId);
                }
            });

            iframeSelf.resolve({docIds:docIds, docTypeIds:docTypeIds, docDescriptions:docDescriptions});
        });

        $('form').submit(function(){
            return false;
        });
        TPOStyleUnification.Components();
        TPOStyleUnification.TableSorterStyle($('#DocTable'));


        TPOStyleUnification.fixedHeaderTable(document.querySelector("table.modal-table-scrollbar"));
    }
})(jQuery);
</script>
</body>
</html>
