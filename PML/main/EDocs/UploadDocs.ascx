<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="UploadDocs.ascx.cs" Inherits="PriceMyLoan.main.EDocs.UploadDocs" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<div id="DragDropZoneParent" ng-controller="dragDropAdditionalInfoController">
    <p class="note-tab1 margin-bottom">You may upload up to 12 documents at a time with a total upload size of 100MB.</p>

    <div id="DragDropZoneContainer" class="InsetBorder full-width">
        <div id="DragDropZone" class="drag-drop-container" limit="12" extensions=".pdf, .xml"></div>
    </div>

    <br />

    <div drag-drop-additional-info-dir></div>

    <br />

    <div id="ErrorDiv"></div>
    <div id="UploadCountDiv"></div>

    <p class="wrap-btn-upload-docs">
        <span>
            <input id="UploadButton" type="button" value="Upload Docs" class="uploadBtn btn btn-default" />
        </span>
    </p>
</div>
<div id="UploadDialog" style="display: none;">

    <div class="text-center">
        <p class="padding-bottom-1em" id="uploadDialogText"></p>
        <div id="LoadingImg"></div>
    </div>
    <a id="PipelineLink" runat="server" target="_blank" style="display: none;" autofocus></a>
</div>

<script type="text/javascript">
    var dropzoneId = 'DragDropZone';
    
    function backToPipeline() {
        window.open(ML.PipelineUrl);
    }
    
    function toDocumentList() {
        $("#link3").click();
    }

    function getUploadLocation() {
        var uploadLocation = 'uploaded';
        if (ML.UploadToOcr) {
            uploadLocation += ' to OCR';
        }

        return uploadLocation;
    }

    function uploadInit() {
        $('#ErrorDiv, #UploadCountDiv').hide();
        $('#UploadButton').click(uploadFiles);

        setUploadButtonDisabled(true);

        var dragDropSettings = {
            appNamesParam: Apps,
            hasAdditionalInfoParam: true,
            isPmlParam: true,
            showBarcodesParam: true,
            blockUpload: !ML.CanUploadEdocs,
            blockUploadMsg: ML.UploadEdocsFailureMessage,
            onDocTypeSelectCallback: onDocTypeSelect,
            onProcessAllFilesCallback: function () {
                window.setTimeout(TextareaUtilities.LengthCheckInit, 0);
            }
        };

        registerDragDropUpload(document.getElementById(dropzoneId), dragDropSettings);
    }

    function updateDirtyBit_callback() {
        clearDirty();
    }
</script>
