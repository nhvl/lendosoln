﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LendersOffice.Common;
using EDocs;
using LendersOffice.AntiXss;
using System.Text.RegularExpressions;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace PriceMyLoan.main.EDocs
{
    /// <summary>
    /// This is the dialog that pops up when you try to select a document type for an edoc
    /// </summary>
    public partial class DocTypePicker : PriceMyLoan.UI.BasePage
    {

        private Guid BrokerID
        {
            get { return PriceMyLoanUser.BrokerId; }
        }

        protected string FolderID
        {
            get { return RequestHelper.GetSafeQueryString("folderId"); }
        }

        protected string SearchQuery
        {
            get { return RequestHelper.GetSafeQueryString("q"); }
        }

        protected void PageInit(object sender, EventArgs e)
        {
            SearchBox.Attributes.Add("onkeyup", "f_searchBoxKeyUp()");
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            var docTypeList = EDocumentDocType.GetDocTypesByBroker(BrokerID, true /* enforcefolderpermissions */).ToList();

            // folderList: We need the folders that have docs, so we can't use EDocumentFolder.GetFoldersInBroker
            var docFolders = from doc in docTypeList select doc.Folder;
            var folderList = docFolders.Distinct().ToList();
            DocTypeDiv.Visible = false;
            DocFolderDiv.Visible = false;
            SearchResultDiv.Visible = false;
            BreadcrumbsDiv.Visible = false;
            m_HeaderMessage.Text = "Select a Doc Type";
            if (!string.IsNullOrEmpty(SearchQuery))
            {
                SearchResultDiv.Visible = true;
                var foundDocuments = Search(SearchQuery, docTypeList);
                var searchResults = from doc in foundDocuments
                                    select new {
                                        FolderAndDocTypeName = doc.FolderAndDocTypeName,
                                        DocTypeName = doc.DocTypeName,
                                        DocTypeId = doc.DocTypeId,
                                        FolderId = doc.Folder.FolderId,
                                        FolderNm = doc.Folder.FolderNm
                                    };

                m_SearchResults.DataSource = searchResults;
                m_SearchResults.DataBind();
                
                // Allow sorting by either column
                if (m_SearchResults.Rows.Count > 0)
                {
                    //This replaces <td> with <th> and adds the scope attribute
                    m_SearchResults.UseAccessibleHeader = true;
                    //This will add the <thead> and <tbody> elements
                    m_SearchResults.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    m_SearchMessage.Text = "No doc types matched the search value entered.";
                }
                return;
            }
            else if (!string.IsNullOrEmpty(FolderID)) // A folder has been selected
            {
                DocTypeDiv.Visible = true;
                BreadcrumbsDiv.Visible = true;
                var selectedFolder = folderList.Find(f => f.FolderId == int.Parse(FolderID));
                m_selectedFolder.Text = selectedFolder.FolderNm;
                m_DocTypeExplorer.DataSource = docTypeList.Where(d => d.Folder.FolderId == selectedFolder.FolderId);
                m_DocTypeExplorer.DataBind();

                if (m_DocTypeExplorer.Rows.Count > 0)
                {
                    //This replaces <td> with <th> and adds the scope attribute
                    m_DocTypeExplorer.UseAccessibleHeader = true;
                    //This will add the <thead> and <tbody> elements
                    m_DocTypeExplorer.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                return;
            }
            else // No folder selected and no search query, so show folders
            {
                DocFolderDiv.Visible = true;
                m_HeaderMessage.Text = "Select a Doc Folder";
                folderList.Sort();
                var folders = from f in folderList
                              select new { FolderId = f.FolderId, FolderNm = f.FolderNm };
                m_DocFolderExplorer.DataSource = folders;
                m_DocFolderExplorer.DataBind();

                if (m_DocFolderExplorer.Rows.Count > 0)
                {
                    //This replaces <td> with <th> and adds the scope attribute
                    m_DocFolderExplorer.UseAccessibleHeader = true;
                    //This will add the <thead> and <tbody> elements
                    m_DocFolderExplorer.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    m_FolderMessage.Text = "No doc types have been set up. Please contact your administrator.";
                }
                return;
            }

        }

        private IEnumerable<DocType> Search(string searchQuery, List<DocType> docTypeList)
        {
            // OR search terms entered without quotes
            // AND search terms entered with quotes
            // Searches are case-insensitive
            searchQuery = HttpUtility.HtmlDecode(searchQuery);
            Regex searchSplitRegex = new Regex(ConstApp.SearchMatchQuotesExpression);
            var searchSplit = from Match m in searchSplitRegex.Matches(searchQuery)
                              where m.Groups["match"].Success
                              select m.Groups["match"].Value.ToUpper();

            // Sort the docs by doc type name
            docTypeList.Sort((DocType x, DocType y) => x.DocTypeName.CompareTo(y.DocTypeName));
            // Use a set so that duplicates do not show up
            var foundDocuments = new HashSet<DocType>();

            foreach (var searchTerm in searchSplit)
            {
                // Adding all relevant docs takes two passes through the docTypeList
                // It could be done in one pass, but let's just keep it simple for now

                // Add all the matching doc types that the user has access to
                var matchingDocTypes = docTypeList.Where((DocType d) => d.DocTypeName.ToUpper().Contains(searchTerm));
                foreach (var doc in matchingDocTypes)
                {
                    foundDocuments.Add(doc);
                }

                // Add all the doc types belonging to matching folders that the user has access to
                var matchingFolders = docTypeList.Where((DocType d) => d.Folder.FolderNm.ToUpper().Contains(searchTerm));
                foreach (var doc in matchingFolders)
                {
                    foundDocuments.Add(doc);
                }
            }
            return foundDocuments;
        }

        protected void SearchBtn_Click(object sender, EventArgs e)
        {
            var searchTerm = SearchBox.Text;
            Response.Redirect("DocTypePicker.aspx" + "?q=" + searchTerm);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
