<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="FaxDocs.ascx.cs" Inherits="PriceMyLoan.main.EDocs.FaxDocs" %>
<%@Import Namespace="LendersOffice.AntiXss"%>
<%@ Register  TagPrefix="uc" TagName="LoanHeader" Src="~/common/LoanHeader.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<div>
      <table class="primary-table wide none-hover-table table-fax-docs" cellspacing="0" cellpadding="0" id="m_Grid">
        <thead class="header">
          <tr>
            <th> Doc Type </th>
            <th> Application </th>
            <th> Description</th>
            <th class="text-center">Fax Cover</th>
            <th class="text-right table-fax-check"><input type="checkbox" id="batchSelectAllCb" onclick="onBatchSelectClick()" NoHover />
              <label for="batchSelectAllCb">All</label></th>
          </tr>
        </thead>
        <tbody>
          <tr class="grey-bg">
            <td class="DocTypeCol"><input name="DocTypeId_0" runat="server" type="hidden" id="DocTypeId_0" class="doctypeid" value="-1">
              <span class="Name">No Doc Type Selected</span><span> <a href="#" class="PickerLink">select Doc Type </a></span></td>
            <td><asp:DropDownList ID="AppDdl_0" runat="server" Width="280px" /></td>
            <td><asp:TextBox id="DescTb_0" runat="server" Width="375px" MaxLength="200" readonly/></td>
            <td><span id="downloadLink_0" class="none-display"><a class="icon-file-download" onclick='window.location = $(this).next().attr("href")'><i class="material-icons">&#xE2C4;</i></a><asp:LinkButton ID="LinkButton2" runat="server" Text="download" CommandName="download" CommandArgument="0" OnCommand="OnDownloadCommand"/></span></td>
            <td><span class="batch_cb" preset="NoHoverSelect">
              <asp:CheckBox id="batchSelectCb_0" CssClass="batch_cb" runat="server"/>
              </span></td>
          </tr>
          <tr>
            <td class="DocTypeCol"><input name="DocTypeId_1" runat="server" type="hidden" id="DocTypeId_1" class="doctypeid" value="-1">
              <span class="Name">No Doc Type Selected</span><span> <a href="#" class="PickerLink">select Doc Type </a></span></td>
            <td><asp:DropDownList ID="AppDdl_1" runat="server" Width="280px"  /></td>
            <td><asp:TextBox id="DescTb_1" runat="server" Width="375px" MaxLength="200" readonly/></td>
            <td><span id="downloadLink_1" class="none-display"><a class="icon-file-download" onclick='window.location = $(this).next().attr("href")'><i class="material-icons">&#xE2C4;</i></a><asp:LinkButton id="download_1" runat="server" Text="download" CommandName="download" CommandArgument="1" OnCommand="OnDownloadCommand" /></span></td>
            <td><span class="batch_cb" preset="NoHoverSelect">
              <asp:CheckBox id="batchSelectCb_1" CssClass="batch_cb" runat="server"/>
              </span></td>
          </tr>
          <tr class="grey-bg">
            <td class="DocTypeCol"><input name="DocTypeId_2" runat="server" type="hidden" id="DocTypeId_2" class="doctypeid" value="-1">
              <span class="Name">No Doc Type Selected</span><span> <a href="#" class="PickerLink">select Doc Type </a></span></td>
            <td><asp:DropDownList ID="AppDdl_2" runat="server" Width="280px"  /></td>
            <td><asp:TextBox id="DescTb_2" runat="server" Width="375px" MaxLength="200" readonly/></td>
            <td><span id="downloadLink_2" class="none-display"><a class="icon-file-download" onclick='window.location = $(this).next().attr("href")'><i class="material-icons">&#xE2C4;</i></a><asp:LinkButton id="download_2" runat="server" Text="download" CommandName="download" CommandArgument="2" OnCommand="OnDownloadCommand" /></span></td>
            <td><span class="batch_cb" preset="NoHoverSelect">
              <asp:CheckBox id="batchSelectCb_2" CssClass="batch_cb" runat="server"/>
              </span></td>
          </tr>
          <tr>
            <td class="DocTypeCol"><input name="DocTypeId_3" runat="server" type="hidden" id="DocTypeId_3" class="doctypeid" value="-1">
              <span class="Name">No Doc Type Selected</span><span> <a href="#" class="PickerLink">select Doc Type </a></span></td>
            <td><asp:DropDownList ID="AppDdl_3" runat="server" Width="280px"  /></td>
            <td><asp:TextBox id="DescTb_3" runat="server" Width="375px" MaxLength="200" readonly/></td>
            <td><span id="downloadLink_3" class="none-display"><a class="icon-file-download" onclick='window.location = $(this).next().attr("href")'><i class="material-icons">&#xE2C4;</i></a><asp:LinkButton id="download_3" runat="server" Text="download" CommandName="download" CommandArgument="3" OnCommand="OnDownloadCommand" /></span></td>
            <td><span class="batch_cb" preset="NoHoverSelect">
              <asp:CheckBox id="batchSelectCb_3" CssClass="batch_cb" runat="server"/>
              </span></td>
          </tr>
          <tr class="grey-bg">
            <td class="DocTypeCol"><input name="DocTypeId_4" runat="server" type="hidden" id="DocTypeId_4" class="doctypeid" value="-1">
              <span class="Name">No Doc Type Selected</span><span> <a href="#" class="PickerLink">select Doc Type </a></span></td>
            <td><asp:DropDownList ID="AppDdl_4" runat="server" Width="280px"  /></td>
            <td><asp:TextBox id="DescTb_4" runat="server" Width="375px" MaxLength="200" readonly/></td>
            <td><span id="downloadLink_4" class="none-display"><a class="icon-file-download" onclick='window.location = $(this).next().attr("href")'><i class="material-icons">&#xE2C4;</i></a><asp:LinkButton id="download_4" runat="server" Text="download" CommandName="download" CommandArgument="4" OnCommand="OnDownloadCommand" /></span></td>
            <td><span class="batch_cb" preset="NoHoverSelect">
              <asp:CheckBox id="batchSelectCb_4" CssClass="batch_cb" runat="server"/>
              </span></td>
          </tr>
          <tr>
            <td class="DocTypeCol"><input name="DocTypeId_5" runat="server" type="hidden" id="DocTypeId_5" class="doctypeid" value="-1">
              <span class="Name">No Doc Type Selected</span><span> <a href="#" class="PickerLink">select Doc Type </a></span></td>
            <td><asp:DropDownList ID="AppDdl_5" runat="server" Width="280px"  /></td>
            <td><asp:TextBox id="DescTb_5" runat="server" Width="375px" MaxLength="200" readonly/></td>
            <td><span id="downloadLink_5" class="none-display"><a class="icon-file-download" onclick='window.location = $(this).next().attr("href")'><i class="material-icons">&#xE2C4;</i></a><asp:LinkButton id="download_5" runat="server" Text="download" CommandName="download" CommandArgument="5" OnCommand="OnDownloadCommand" /></span></td>
            <td><span class="batch_cb" preset="NoHoverSelect">
              <asp:CheckBox id="batchSelectCb_5" CssClass="batch_cb" runat="server"/>
              </span></td>
          </tr>
          <tr class="none-border">
            <td align="left" colspan="5" class="padding-top-h1em">
              <button type="button" class="btn btn-default" onclick="onClearSelectClick()">Clear list</button>
              <asp:LinkButton ID="batchDownloadBtn" runat="server" Text="download selected"  CommandName="download" CommandArgument="selected" OnCommand="OnDownloadCommand" CssClass="btn btn-default"/>
            </td>
          </tr>
        </tbody>
      </table>
      </div>
    
<script type="text/javascript" language="javascript">


    function f_validatePage(){
        if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
            return Page_ClientValidate();  
        return true;
    }
    
 
    function faxInit(){
        onClearSelectClick();
        var docTypePickerUrl = $('#DocTypePickerUrl').val();
        
        onDocTypeChange();
        $('a.PickerLink').on("click", function(e){
            var that = this; 
            e.preventDefault();
            e.stopPropagation();
            LQBPopup.Show(docTypePickerUrl,{
            'onReturn' : function(id,name){
                var a = $(that);
                a.parents('td').find('span.Name').text(name);
                a.parents('td').find('input.doctypeid').val(id);
                onDocTypeChange();
            }});
            
        });
           
        $(<%=AspxTools.JsGetElementById(batchDownloadBtn) %>).on("click", 
        function(e){
            if( !checkStuff() ){
                e.preventDefault();
            }
        });

        $('input[type=checkbox]').change(function(){
          toogleDownloadBtn();
        });
    }

    function toogleDownloadBtn(){
      $(<%=AspxTools.JsGetElementById(batchDownloadBtn) %>).toggleClass("disabled",!checkStuff());
    }

    function checkStuff(){
        var bx = $('.batch_cb input:checked');
        return bx.length > 0;
    }

  function performChecboxChange(){
    $(<%= AspxTools.JsGetElementById(batchSelectCb_0)%>).change();
    $(<%= AspxTools.JsGetElementById(batchSelectCb_1)%>).change();
    $(<%= AspxTools.JsGetElementById(batchSelectCb_2)%>).change();
    $(<%= AspxTools.JsGetElementById(batchSelectCb_3)%>).change();
    $(<%= AspxTools.JsGetElementById(batchSelectCb_4)%>).change();
    $(<%= AspxTools.JsGetElementById(batchSelectCb_5)%>).change();
  }
  function onBatchSelectClick()
  {
    var bSelect = document.getElementById("batchSelectAllCb").checked;
    //If checkbox is hidden (has no value), keep it unchecked.
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = (bSelect && <%= AspxTools.JsGetElementById(DocTypeId_0)%>.value != -1);
    <%= AspxTools.JsGetElementById(batchSelectCb_1)%>.checked = (bSelect && <%= AspxTools.JsGetElementById(DocTypeId_1)%>.value != -1);
    <%= AspxTools.JsGetElementById(batchSelectCb_2)%>.checked = (bSelect && <%= AspxTools.JsGetElementById(DocTypeId_2)%>.value != -1);
    <%= AspxTools.JsGetElementById(batchSelectCb_3)%>.checked = (bSelect && <%= AspxTools.JsGetElementById(DocTypeId_3)%>.value != -1);
    <%= AspxTools.JsGetElementById(batchSelectCb_4)%>.checked = (bSelect && <%= AspxTools.JsGetElementById(DocTypeId_4)%>.value != -1);
    <%= AspxTools.JsGetElementById(batchSelectCb_5)%>.checked = (bSelect && <%= AspxTools.JsGetElementById(DocTypeId_5)%>.value != -1);
    performChecboxChange();
  }
  
  function onClearSelectClick()
  {
    <%= AspxTools.JsGetElementById(DocTypeId_0) %>.value = '-1';
    <%= AspxTools.JsGetElementById(DocTypeId_1) %>.value = '-1';
    <%= AspxTools.JsGetElementById(DocTypeId_2) %>.value = '-1';
    <%= AspxTools.JsGetElementById(DocTypeId_3) %>.value = '-1';
    <%= AspxTools.JsGetElementById(DocTypeId_4) %>.value = '-1';
    <%= AspxTools.JsGetElementById(DocTypeId_5) %>.value = '-1';
    
    document.getElementById("batchSelectAllCb").checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_1)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_2)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_3)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_4)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_5)%>.checked = false;
    performChecboxChange();

    
    
    $('span.Name').text("No Doc Type Selected");
    
    <%= AspxTools.JsGetElementById(AppDdl_0)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_1)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_2)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_3)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_4)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_5)%>.selectedIndex = 0;
    
    <%= AspxTools.JsGetElementById(DescTb_0)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_1)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_2)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_3)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_4)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_5)%>.value = '';
    
    onDocTypeChange();

    toogleDownloadBtn();
  }
  

  function onDocTypeChange() 
  {
    var bEnable = <%= AspxTools.JsGetElementById(DocTypeId_0)%>.value != -1;
    document.getElementById('downloadLink_0').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.style.display = bEnable ? 'inline' : 'none';
    TPOStyleUnification.DisplayComponent(<%= AspxTools.JsGetElementById(batchSelectCb_0)%>);
    $(<%= AspxTools.JsGetElementById(DescTb_0)%>).readOnly(!bEnable);
    
    bEnable = <%= AspxTools.JsGetElementById(DocTypeId_1)%>.value != -1;
    document.getElementById('downloadLink_1').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_1)%>.style.display = bEnable ? 'inline' : 'none';
    TPOStyleUnification.DisplayComponent(<%= AspxTools.JsGetElementById(batchSelectCb_1)%>);
    $(<%= AspxTools.JsGetElementById(DescTb_1)%>).readOnly(!bEnable);

    
    bEnable = <%= AspxTools.JsGetElementById(DocTypeId_2)%>.value != -1;
    document.getElementById('downloadLink_2').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_2)%>.style.display = bEnable ? 'inline' : 'none';
    TPOStyleUnification.DisplayComponent(<%= AspxTools.JsGetElementById(batchSelectCb_2)%>);
    $(<%= AspxTools.JsGetElementById(DescTb_2)%>).readOnly(!bEnable);

    bEnable = <%= AspxTools.JsGetElementById(DocTypeId_3)%>.value != -1;
    document.getElementById('downloadLink_3').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_3)%>.style.display = bEnable ? 'inline' : 'none';
    TPOStyleUnification.DisplayComponent(<%= AspxTools.JsGetElementById(batchSelectCb_3)%>);
    $(<%= AspxTools.JsGetElementById(DescTb_3)%>).readOnly(!bEnable);
    
    bEnable = <%= AspxTools.JsGetElementById(DocTypeId_4)%>.value != -1;
    document.getElementById('downloadLink_4').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_4)%>.style.display = bEnable ? 'inline' : 'none';
    TPOStyleUnification.DisplayComponent(<%= AspxTools.JsGetElementById(batchSelectCb_4)%>);
    $(<%= AspxTools.JsGetElementById(DescTb_4)%>).readOnly(!bEnable);

    bEnable = <%= AspxTools.JsGetElementById(DocTypeId_5)%>.value != -1;
    document.getElementById('downloadLink_5').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_5)%>.style.display = bEnable ? 'inline' : 'none';
    TPOStyleUnification.DisplayComponent(<%= AspxTools.JsGetElementById(batchSelectCb_5)%>);
    $(<%= AspxTools.JsGetElementById(DescTb_5)%>).readOnly(!bEnable);

    
  }
  
    function setDocType(id, name)
    {
        $('#doctypepicker').hide();    
        f_displayBlockMask(false);    
        if( _lastAnchor ) {
            var a = $(_lastAnchor); 
            a.parents('td').find('span.Name').text(name);
            a.parents('td').find('input.doctypeid').val(id);
            onDocTypeChange();
        }
            
    }
    
    function cancelDocTypePick(){
        f_displayBlockMask(false);
        $('#doctypepicker').hide();
    }

//-->
</script>


        <script type="text/javascript" src="../../inc/utilities.js"></script>
