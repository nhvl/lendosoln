﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using DataAccess;
using LendersOffice.Security;
using PriceMyLoan.DataAccess;
using EDocs;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;

namespace PriceMyLoan.main.EDocs
{
    public partial class FaxDocs : PriceMyLoan.UI.BaseUserControl
    {
        protected bool IsEncompassIntegration
        {
            get { return PriceMyLoanRequestHelper.IsEncompassIntegration; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                BindApps();
            }

        }




        private void SendFile(byte[] file)
        {
            Response.Clear();
            Response.ClearContent();
            Response.OutputStream.Write(file, 0, file.Length);
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment;filename=FaxCover.pdf");
            Response.Flush();
            Response.End();
        }

        protected void OnDownloadCommand(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "download")
            {
                string argument = e.CommandArgument.ToString();

                if (argument != "selected")
                    CreatePdf(argument);
                else
                    CreateBatchPdf();
            }
        }

        private string GetLoanNumber()
        {
            CPageData dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FaxDocs));
            dataloan.InitLoad();
            return dataloan.sLNm;
        }

        private List<DocType> x_DocTypes = null;
        private List<DocType> DocTypeList
        {
            get
            {
                if (x_DocTypes == null)
                {
                    x_DocTypes = EDocumentDocType.GetDocTypesByBroker(PriceMyLoanUser.BrokerId, E_EnforceFolderPermissions.True).ToList();
                }

                return x_DocTypes;
            }
        }

        private string GetDocTypeName(string docTypeId)
        {
            foreach (DocType docType in DocTypeList)
            {
                if (docType.DocTypeId == docTypeId)
                    return docType.DocTypeName; // Do not want folder name.
            }

            Tools.LogBug("Could not find doc type: " + docTypeId);
            return string.Empty;
        }


        private void CreatePdf(string index)
        {
            string docType = string.Empty;
            string docTypeDescription = string.Empty;
            string description = string.Empty;
            Guid appId = Guid.Empty;
            string appDescription = string.Empty;

            switch (index)
            {
                case "0":
                    docType = DocTypeId_0.Value;
                    docTypeDescription = GetDocTypeName(DocTypeId_0.Value);
                    description = DescTb_0.Text;
                    appId = new Guid(AppDdl_0.SelectedValue);
                    appDescription = AppDdl_0.SelectedItem.Text;
                    break;
                case "1":
                    docType = DocTypeId_1.Value;
                    docTypeDescription = GetDocTypeName(DocTypeId_1.Value);
                    description = DescTb_1.Text;
                    appId = new Guid(AppDdl_1.SelectedValue);
                    appDescription = AppDdl_1.SelectedItem.Text;
                    break;
                case "2":
                    docType = DocTypeId_2.Value;
                    docTypeDescription = GetDocTypeName(DocTypeId_2.Value);
                    description = DescTb_2.Text;
                    appId = new Guid(AppDdl_2.SelectedValue);
                    appDescription = AppDdl_2.SelectedItem.Text;
                    break;
                case "3":
                    docType = DocTypeId_3.Value;
                    docTypeDescription = GetDocTypeName(DocTypeId_3.Value);
                    description = DescTb_3.Text;
                    appId = new Guid(AppDdl_3.SelectedValue);
                    appDescription = AppDdl_3.SelectedItem.Text;
                    break;
                case "4":
                    docType = DocTypeId_4.Value;
                    docTypeDescription = GetDocTypeName(DocTypeId_4.Value);
                    description = DescTb_4.Text;
                    appId = new Guid(AppDdl_4.SelectedValue);
                    appDescription = AppDdl_4.SelectedItem.Text;
                    break;
                case "5":
                    docType = DocTypeId_5.Value;
                    docTypeDescription = GetDocTypeName(DocTypeId_5.Value);
                    description = DescTb_5.Text;
                    appId = new Guid(AppDdl_5.SelectedValue);
                    appDescription = AppDdl_5.SelectedItem.Text;
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Bad fax cover row id: " + index);
            }

            EDocsFaxNumber number = EDocsFaxNumber.Retrieve(PriceMyLoanUser.BrokerId);
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.InitLoad();

            EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
            {
                Type = E_FaxCoverType.EDocs,
                LoanId = LoanID,
                AppId = appId,
                LenderName = GetCompanyName(),
                Description = description,
                DocTypeId = int.Parse(docType),
                DocTypeDescription = docTypeDescription,
                FaxNumber = number.FaxNumber,
                ApplicationDescription = appDescription,
                LoanNumber = GetLoanNumber(),
            };

            byte[] file = EdocFaxCover.GenerateBarcodePackagePdf(new EdocFaxCover.FaxCoverData[] { coverData });

            SendFile(file);
        }

        private void CreateBatchPdf()
        {
            // Create a multi-page pdf based on the selected docs.

            List<EdocFaxCover.FaxCoverData> coverDataList = new List<EdocFaxCover.FaxCoverData>(6);

            string companyName = GetCompanyName();
            EDocsFaxNumber number = EDocsFaxNumber.Retrieve(PriceMyLoanUser.BrokerId);
            string loanNumber = GetLoanNumber();


            if (batchSelectCb_0.Checked && DocTypeId_0.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.EDocs,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_0.SelectedValue),
                    LenderName = companyName,
                    Description = DescTb_0.Text,
                    DocTypeId = int.Parse(DocTypeId_0.Value),
                    DocTypeDescription = GetDocTypeName(DocTypeId_0.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_0.SelectedItem.Text,
                    LoanNumber =  loanNumber
                };

                coverDataList.Add(coverData);
            }
            if (batchSelectCb_1.Checked && DocTypeId_1.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.EDocs,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_1.SelectedValue),
                    LenderName = companyName,
                    Description = DescTb_1.Text,
                    DocTypeId = int.Parse(DocTypeId_1.Value),
                    DocTypeDescription = GetDocTypeName(DocTypeId_1.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_1.SelectedItem.Text,
                    LoanNumber = loanNumber
                };

                coverDataList.Add(coverData);
            }
            if (batchSelectCb_2.Checked && DocTypeId_2.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.EDocs,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_2.SelectedValue),
                    LenderName = companyName,
                    Description = DescTb_2.Text,
                    DocTypeId = int.Parse(DocTypeId_2.Value),
                    DocTypeDescription = GetDocTypeName(DocTypeId_2.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_2.SelectedItem.Text,
                    LoanNumber = loanNumber
                };

                coverDataList.Add(coverData);
            }
            if (batchSelectCb_3.Checked && DocTypeId_3.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.EDocs,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_3.SelectedValue),
                    LenderName = companyName,
                    Description = DescTb_3.Text,
                    DocTypeId = int.Parse(DocTypeId_3.Value),
                    DocTypeDescription = GetDocTypeName(DocTypeId_3.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_3.SelectedItem.Text,
                    LoanNumber = loanNumber
                };

                coverDataList.Add(coverData);
            }
            if (batchSelectCb_4.Checked && DocTypeId_4.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.EDocs,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_4.SelectedValue),
                    LenderName = companyName,
                    Description = DescTb_4.Text,
                    DocTypeId = int.Parse(DocTypeId_4.Value),
                    DocTypeDescription = GetDocTypeName(DocTypeId_4.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_4.SelectedItem.Text,
                    LoanNumber = loanNumber
                };

                coverDataList.Add(coverData);
            }
            if (batchSelectCb_5.Checked && DocTypeId_5.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.EDocs,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_5.SelectedValue),
                    LenderName = companyName,
                    Description = DescTb_5.Text,
                    DocTypeId = int.Parse(DocTypeId_5.Value),
                    DocTypeDescription = GetDocTypeName(DocTypeId_5.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_5.SelectedItem.Text,
                    LoanNumber = loanNumber
                };

                coverDataList.Add(coverData);
            }

            if (coverDataList.Count == 0) return;

            byte[] file = EdocFaxCover.GenerateBarcodePackagePdf(coverDataList.ToArray());

            SendFile(file);
        }

        private string GetCompanyName()
        {
            // This may need to change.
            BrokerDB broker = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
            return broker.Name;

        }

        private void BindApps()
        {

            Dictionary<Guid, string> apps = new Dictionary<Guid, string>();

            CPageData dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FaxDocs));
            dataloan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataloan.InitLoad();

            var count = dataloan.nApps;

            for (var i = 0; i < count; i++)
            {
                var appData = dataloan.GetAppData(i);

                string name = appData.aBNm;
                if (!string.IsNullOrEmpty(appData.aCNm))
                {
                    name += " & " + appData.aCNm;
                }

                apps.Add(appData.aAppId, name);
            }

            AppDdl_0.DataSource = apps;
            AppDdl_1.DataSource = apps;
            AppDdl_2.DataSource = apps;
            AppDdl_3.DataSource = apps;
            AppDdl_4.DataSource = apps;
            AppDdl_5.DataSource = apps;

            AppDdl_0.DataTextField = "Value";
            AppDdl_1.DataTextField = "Value";
            AppDdl_2.DataTextField = "Value";
            AppDdl_3.DataTextField = "Value";
            AppDdl_4.DataTextField = "Value";
            AppDdl_5.DataTextField = "Value";

            AppDdl_0.DataValueField = "Key";
            AppDdl_1.DataValueField = "Key";
            AppDdl_2.DataValueField = "Key";
            AppDdl_3.DataValueField = "Key";
            AppDdl_4.DataValueField = "Key";
            AppDdl_5.DataValueField = "Key";

            AppDdl_0.DataBind();
            AppDdl_1.DataBind();
            AppDdl_2.DataBind();
            AppDdl_3.DataBind();
            AppDdl_4.DataBind();
            AppDdl_5.DataBind();

        }
    }
}
