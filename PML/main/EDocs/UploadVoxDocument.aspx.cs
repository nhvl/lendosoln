﻿namespace PriceMyLoan.main.EDocs
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Allows users to upload documents to the VOX framework.
    /// </summary>
    public partial class UploadVoxDocument : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");

            this.RegisterCSS("DragDropUpload.css");

            var loan = CPageData.CreateUsingSmartDependency(RequestHelper.GetGuid("loanid"), typeof(UploadDocToTask));
            loan.InitLoad();

            var selectedAppId = RequestHelper.GetGuid("appid", Guid.Empty);

            IEnumerable<AppNameAndId> apps = null;
            if (selectedAppId != Guid.Empty)
            {
                var selectedApp = loan.GetAppData(selectedAppId);
                apps = new[]
                {
                    new AppNameAndId(selectedApp.aBNm_aCNm, selectedAppId)
                };
            }
            else
            {
                apps = loan.GetAppNameAndIds();
            }

            this.RegisterJsGlobalVariables("LoanId", loan.sLId);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("Apps", apps);

            Tuple<bool, string> results = Tools.IsWorkflowOperationAuthorized(this.PriceMyLoanUser, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.UploadEDocs);
            this.RegisterJsGlobalVariables("CanUploadEdocs", results.Item1);
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", results.Item2);
        }
    }
}