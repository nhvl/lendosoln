﻿namespace PriceMyLoan.main.EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using global::EDocs;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial class UploadDocToTask : PriceMyLoan.UI.BasePage
    {
        private Dictionary<Guid, string> applicationNamesById;
        private DocType condRequiredDocType;

        private void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsGlobalVariables("DocTypePickerUrl", this.Page.ResolveClientUrl("~/main/EDocs/DocTypePicker.aspx"));

            this.RegisterJsScript("Jquery-ui.js");
            this.RegisterJsScript("utilities.js");
            this.RegisterJsScript("UploadDocToTask.js");

            this.RegisterCSS("/webapp/jquery-ui.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var taskId = RequestHelper.GetSafeQueryString("TaskId");

            Task t = Task.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, taskId);
            TaskID.Text = t.TaskId;
            CategoryRep.Text = t.CondCategoryId_rep;
            RequiredDocType.Text = t.CondRequiredDocType_rep;
            Subject.Text = t.TaskSubject;

            if (t.CondRequiredDocTypeId.HasValue)
            {
                var reqDocType = EDocumentDocType.GetDocTypeById(this.PriceMyLoanUser.BrokerId, t.CondRequiredDocTypeId.Value);
                if (reqDocType.Folder.CanRead())
                {
                    this.condRequiredDocType = reqDocType;
                }
            }

            CPageData loan = CPageData.CreateUsingSmartDependency(RequestHelper.GetGuid("LoanId"), typeof(UploadDocToTask));
            loan.InitLoad();

            this.applicationNamesById = loan.Apps.ToDictionary(app => app.aAppId, app => app.aBNm_aCNm);

            var cacheKey = RequestHelper.GetSafeQueryString("CacheKey");
            var filenameJson = AutoExpiredTextCache.GetFromCacheByUser(PrincipalFactory.CurrentPrincipal, cacheKey);
            var filenames = SerializationHelper.JsonNetDeserialize<List<string>>(filenameJson);

            this.FileListingTable.DataSource = filenames;
            this.FileListingTable.DataBind();
        }

        protected void FileListingTable_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var filename = e.Item.DataItem.ToString();
            var label = e.Item.FindControl("Filename") as EncodedLabel;
            label.Text = filename;

            var applicationDropdown = e.Item.FindControl("ApplicationDdl") as DropDownList;
            foreach (var appNameById in this.applicationNamesById)
            {
                applicationDropdown.Items.Add(new ListItem(appNameById.Value, appNameById.Key.ToString("N")));
            }

            var validatorIcon = e.Item.FindControl("validatorIcon") as HtmlGenericControl;
            if (this.condRequiredDocType != null)
            {
                var docTypeLabel = e.Item.FindControl("RequiredDocumentTypeName") as EncodedLabel;
                docTypeLabel.Text = this.condRequiredDocType.FolderAndDocTypeName;

                var docTypeId = e.Item.FindControl("RequiredDocumentTypeId") as HtmlInputHidden;
                docTypeId.Value = this.condRequiredDocType.Id.ToString();

                var docTypeSelectLink = e.Item.FindControl("selectDocType") as HtmlAnchor;
                docTypeSelectLink.InnerText = "change Doc Type";

                validatorIcon.InnerHtml = "&#xE86C;";
                validatorIcon.Attributes["class"] += " green";
            }
            else
            {
                validatorIcon.InnerHtml = "&#xE147;";
                validatorIcon.Attributes["class"] += " red rotate-45";
            }
        }
    }
}
