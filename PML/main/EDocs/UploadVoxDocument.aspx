﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadVoxDocument.aspx.cs" Inherits="PriceMyLoan.main.EDocs.UploadVoxDocument" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Document</title>
    <script type="text/javascript">
        function setUploadButtonDisabled(disabled) {
            $('#UploadButton').prop('disabled', disabled);
        }

        function onDocTypeSelect() {
            var files = retrieveFiles('DragDropZone');
            var filesWithoutDocType = files.filter(function (file) { return !fileHasDocType(file); });
            setUploadButtonDisabled(filesWithoutDocType.length !== 0);
        }

        $(function () {
            setUploadButtonDisabled(true);

            var dragDropSettings = {
                appNamesParam: Apps,
                isPmlParam: true,
                hasAdditionalInfoParam: true,
                onDocTypeSelectCallback: onDocTypeSelect,
                blockUpload: !ML.CanUploadEdocs,
                blockUploadMsg: ML.UploadEdocsFailureMessage
            };

            registerDragDropUpload(document.getElementById('DragDropZone'), dragDropSettings);

            var popup = new LqbPopup(document.querySelector("div[lqb-popup]"));
            var iframeSelf = new IframeSelf();
            iframeSelf.popup = popup;

            $('#CloseButton, #CancelButton').click(function () {
                iframeSelf.resolve(null);
            });

            $('#UploadButton').click(function () {
                var files = retrieveFiles('DragDropZone');
                if (files.length !== 1) {
                    LQBPopup.ShowString('Please select a file to upload.');
                    return;
                }

                if (!fileHasDocType(files[0])) {
                    LQBPopup.ShowString('Please select a doc type for the file to upload.');
                    return;
                }

                triggerOverlayImmediately();
                applyCallbackFileObjects('DragDropZone', function (file) {
                    var args = {
                        loanid: ML.LoanId,
                        aAppId: file.appId,
                        DocTypeId: file.docTypeId,
                        InternalDescription: file.internalComments,
                        PublicDescription: file.desc
                    };

                    DocumentUploadHelper.Upload('UploadDocument', file, args, function (result) {
                        removeOverlay();

                        if (result.error) {
                            LQBPopup.ShowString(result.UserMessage);
                            return;
                        }

                        window.setTimeout(function () {
                            iframeSelf.resolve({
                                docId: result.value['DocumentId'],
                                description: file.desc
                            });
                        });
                    });
                });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div lqb-popup class="modal-date-calculator">
            <div class="modal-header">
                <button type="button" class="close" id="CloseButton"><i class="material-icons">&#xE5CD;</i></button>
                <h4 class="modal-title">Please select a document to upload for association.</h4>
            </div>
            <div id="DragDropZoneParent" ng-controller="dragDropAdditionalInfoController" class="modal-body">
                <div class="InsetBorder full-width">
                    <div id="DragDropZone" class="drag-drop-container" limit="1" extensions=".pdf"></div>
                </div>

                <br />

                <div drag-drop-additional-info-dir></div>

                <br />

                <div class="drag-drop-error-div"></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="CancelButton" class="btn btn-flat modal-close">Cancel</button>
                <button type="button" id="UploadButton" class="btn btn-flat modal-close">Upload</button>
            </div>
        </div>
    </form>
</body>
</html>
