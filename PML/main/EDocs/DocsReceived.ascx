﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocsReceived.ascx.cs" Inherits="PriceMyLoan.main.EDocs.DocsReceived" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style>
    .status-column {
        color: #4caf50;
    }
    .status-column.obsolete {
        color: #ea1cdd;
    }
    #ReceivedDocsTable tbody tr:hover {
        background-color: antiquewhite;
    }
</style>

<asp:Panel runat="server" ID="DocMagicStatusBar">
    <div class="DocMagicBar text-center div-warning">
        <div id="step1li"><span id="Count1"></span>in Scanning Queue <a href="#"  dataid="section1"id="step1details" class="DocMagicDetail">details </a></div>
        <div id="step2li">Preparing <span id="Count2"></span>for Scanning <a href="#"  dataid="section2"id="step2details" class="DocMagicDetail">details </a></div>
        <div id="step3li">Scanning Barcodes for <span id="Count3"></span><a href="#"  dataid="section3"id="step3details" class="DocMagicDetail">details </a></div>
        <div id="step4li">Splitting Documents <a href="#"  dataid="section4"id="step4details" class="DocMagicDetail">details </a></div>
        <div id="step5li">Error Processing <span id="Count5" ></span><a id="step5details" href="#"  dataid="section5"class="DocMagicDetail">details </a></div>
    </div>

    <div id="section1" style="display: none">
        <table id="table1" class="primary-table wide none-hover-table">
            <thead class="header">
                <tr class="GridHeader">
                    <td>Application</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div id="section2" style="display: none">
        <table id="table2" class="primary-table wide none-hover-table">
            <thead class="header">
                <tr class="GridHeader">
                    <td>Application</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div id="section3" style="display: none">
        <table id="table3" class="primary-table wide none-hover-table">
            <thead class="header">
                <tr>
                    <td>Application</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div id="section4"  style="display: none">
            <table id="table4" class="primary-table wide none-hover-table" >
            <thead class="header">
                <tr class="GridHeader">
                    <td>Application</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div id="section5" style="display: none">
            <table id="table5" class="primary-table wide none-hover-table">
            <thead class="header">
                <tr class="">
                    <td>Application</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                    <td>Error Details</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</asp:Panel>

<div ng-bootstrap="Edocs">
    <docs-received></docs-received>
</div>

<script type="text/javascript">
    function f_validatePage() {
        if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
            return Page_ClientValidate();
        return true;
    }

    var currentTimeout = null;
    function deleteDocMagicError(errorId){
        $.ajax({
            url: 'EDocs.aspx/DeleteDocMagicError',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id : errorId }),
            success : function(){
                $(this).closest('tr').remove();
                window.clearTimeout(currentTimeout);
                runforever();
            }
        });
    }

    var pdfIds = [];
    function UpdateDocMagicBar() {
        var currentIds = [];
        var data = {
            slid : <%= AspxTools.JsString(LoanID) %>
        };
        $.ajax({
            url: 'EDocs.aspx/GetDocMagicStatus',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            success : function(msg){

                var step1Data = msg.d.InQStatus;
                var step2Data = msg.d.ConvertStatus;
                var step3Data = msg.d.ScanStatus;
                var step4Data = msg.d.SplitStatus;
                var step5Data = msg.d.ErrorStatus;

                var steps = [
                    { data : step1Data, id : "#step1details", tableid : "#table1", countid : "#Count1", liid : "#step1li", titleSuffix:" in Scanning Queue"} ,
                    { data : step2Data, id : "#step2details", tableid : "#table2", countid : "#Count2", liid : "#step2li", titleSuffix:" Preparing for Scanning" } ,
                    { data : step3Data, id : "#step3details", tableid : "#table3", countid : "#Count3", liid : "#step3li", titleSuffix:" in Barcode Scanning Queue" } ,
                    { data : step4Data, id : "#step4details", tableid : "#table4", countid : "#Count4", liid : "#step4li", titleSuffix:" in Splitting Queue" } ,
                    { data : step5Data, id : "#step5details", tableid : "#table5", countid : "#Count5", liid : "#step5li", titleSuffix:" Processing Error" }
                ];


                var showBar = false;
                for( var i = 0; i < steps.length; i++) {
                    var item = steps[i];
                    var showLink =  item.data.length != 0;
                    $(item.id).toggle(showLink);

                    if( showLink ) {
                        showBar = true;
                    }

                    var documentString = item.data.length != 1 ? "Documents" : "Document";
                    var msg = item.data.length + " " + documentString;
                    var title = documentString + '' + item.titleSuffix;
                    msg += " ";

                    $(item.id).attr("modalTitle", title);
                    $(item.countid).html(msg);
                    $(item.liid).toggle(showLink);
                    var tbody = $(item.tableid).children('tbody');
                    tbody.html('');
                    var data = item.data;
                    var rowHtml = [];
                    var isAlt = false;
                    for( var x = 0; x < data.length; x++ ) {
                        var row = data[x];
                        currentIds.push(row.Id);
                        var cssClass = isAlt ? '' : 'grey-bg';
                        var html = ["<tr class='", cssClass, "'><td>", row.Application, '</td><td>', row.Description,
                            '</td><td>', row.InternalComments, '</td><td>', row.Pages, '</td><td>',row.UploadedOn];
                        if( i === 4 ) {
                            html.push('</td><td>');
                            html.push(row.StatusNotes);
                            html.push('</td><td>');
                            html.push("<a href='#' class='deleteStatus' onclick='deleteDocMagicError(\"" + row.Id + "\");'>clear</a>");
                        }

                        html.push('</td></tr>');

                        rowHtml.push(html.join(''));
                        isAlt = !isAlt;
                    }
                    $(item.tableid).children('tbody').html('').append(rowHtml.join(''));
                }

                var someCompleted = false;
                for( var i = 0; i < pdfIds.length; i++ ) {
                    if( $.inArray(pdfIds[i], currentIds) === -1 ) {
                        someCompleted = true;
                        break;
                    }
                }

                pdfIds = currentIds;
                $('div.DocMagicBar').toggle(showBar);
                if( someCompleted ) {
                    reload();
                }
            }
        });

    }

    function runforever() {
        if (!ML.isBarcodeScannerEnabled) return;

        UpdateDocMagicBar();
        currentTimeout = window.setTimeout(runforever,30000);
    }

    function reload() {
        window.location.reload();
    }

    $(function () {
        $('a.DocMagicDetail').on("click", function(e){
            e.preventDefault();

            var $item = $('#' + $(this).attr('dataid'));
            /*
            var windowH = $(window).height();
            var windowW = $(window).width();

            $item.css({
                position:"fixed",
                left: ((windowW - $item.outerWidth())/2 + $(document).scrollLeft()),
                top: ((windowH - $item.outerHeight())/2 + $(document).scrollTop())
            }).show();
            */

            var title = $(this).attr("modalTitle");

            LQBPopup.ShowElementWithWrapper($item,{
                draggable:false,
                headerText: title,
                hideXButton: true,
                popupClasses: "modal-document-detail"
            });
            // $item.find('a').focus();
            return false;
        });

        runforever();
    });
</script>
