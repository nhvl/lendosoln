﻿#region Generated code
namespace PriceMyLoan.main.EDocs
#endregion
{
    using System;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentCapture;

    /// <summary>
    /// Provides an interface for uploading documents to a capture vendor.
    /// </summary>
    public partial class DocumentCaptureUpload : UI.BaseUserControl
    {
        /// <summary>
        /// Loads the control.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.PriceMyLoanUser.BrokerDB.EnableKtaIntegration)
            {
                return;
            }

            if (string.IsNullOrEmpty(ConstStage.KtaApiUrl))
            {
                ErrorUtilities.DisplayErrorPage(
                    "KTA API url is null or empty.",
                    isSendEmail: false,
                    brokerID: this.PriceMyLoanUser.BrokerId,
                    employeeID: this.PriceMyLoanUser.EmployeeId);

                return;
            }

            var serviceCredential = KtaUtilities.GetServiceCredential(this.PriceMyLoanUser);
            var hasServiceCredential = serviceCredential != null;

            this.CredentialTable.Visible = !hasServiceCredential;

            var parent = (UI.BasePage)this.Page;

            parent.RegisterJsGlobalVariables("HasKtaServiceCredential", hasServiceCredential);
            parent.RegisterJsGlobalVariables("PollInterval", ConstStage.KtaUploadPollInterval);
            parent.RegisterJsGlobalVariables("LoanId", this.LoanID);

            parent.RegisterJsGlobalVariables("CanSubmitToKta", parent.CurrentUserCanPerform(WorkflowOperations.UseDocumentCapture));
            parent.RegisterJsGlobalVariables("KtaSubmissionDenialReason", string.Join(",", parent.GetReasonsUserCannotPerformOperation(WorkflowOperations.UseDocumentCapture)));

            var dataloan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(DocumentCaptureUpload));
            dataloan.InitLoad();

            parent.RegisterJsGlobalVariables("AppId", dataloan.GetAppData(0).aAppId);
        }
    }
}