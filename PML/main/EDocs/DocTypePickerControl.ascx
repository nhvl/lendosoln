﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocTypePickerControl.ascx.cs" Inherits="PriceMyLoan.main.EDocs.DocTypePickerControl" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<%--
    Case 65393.
    EDocument Type Picker. This replaces the asp:DropDownList previously used to pick out EDocument types.
    In the codebehind, the Value attribute will contain the user's selected value. Use that to save.
    
    Custom Attributes:
        Width       - The width of the control
        OnChange    - The javascript to call once a doctype has been selected
        Value       - The number of the doctype selected (readonly)
        ValueCtrl   - The control holding the Value. This is useful for getting the value in javascript,
                        since Value will bind at the page's compile time.
                        Example: <%= AspxTools.JsGetElementById(myDocPicker.ValueCtrl)%>.value
    TODO: add readonly attribute
--%>

<script type="text/javascript">
<!--
var docTypePickerLink = "/main/EDocs/DocTypePicker.aspx";

var <%= AspxTools.HtmlString(this.ClientID) %> = {
    showDialog : function(currentselection) {
        var queryString = "";
        var result = showModal(docTypePickerLink + queryString);
        if (result && result.docTypeId && result.docTypeName) { // New doc type selected
            <%= AspxTools.JsGetElementById(m_selectedDocType)%>.value = result.docTypeId;
            <%= AspxTools.JsGetElementById(m_selectedDocTypeDescription)%>.innerHTML = result.docTypeName;
            
            // Provide (imperfect) onchange functionality for the user control
            // Imperfections: 'this' will refer to the m_selectedDocType textbox
            var ctrl = <%= AspxTools.JsGetElementById(m_selectedDocType)%>;
            if (document.createEvent && ctrl.dispatchEvent) {
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", true, true);
                ctrl.dispatchEvent(evt);
            } else if (ctrl.fireEvent) {
                ctrl.fireEvent("onchange");
            }
        }
        return;
    },
    clear : function() {
        <%= AspxTools.JsGetElementById(m_selectedDocTypeDescription) %>.innerHTML = "No Doc Type Selected";
        <%= AspxTools.JsGetElementById(m_selectedDocType) %>.value = "-1";
    }
}

-->
</script>

<span id="m_DocTypePickerDiv" runat="server">
    <asp:Label runat="server" ID="m_selectedDocTypeDescription"></asp:Label>
    <span runat="server" id="m_dialogSpan">
        [ <a href="#" runat="server" id="m_dialogLink">select Doc Type</a> ]
    </span>
    <asp:TextBox runat="server" ID="m_selectedDocType" Text="-1"/>
</span>