﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="EDocs.aspx.cs" Inherits="PriceMyLoan.main.EDocs.EDocs" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="tpo" TagName="LoanNavHeader" Src="~/main/LoanNavigationHeader.ascx" %>
<%@ Register TagName="FaxDocs" TagPrefix="uc" Src="~/main/EDocs/FaxDocs.ascx" %>
<%@ Register TagName="DocsReceived" TagPrefix="uc" Src="~/main/EDocs/DocsReceived.ascx" %>
<%@ Register TagName="UploadDocs" TagPrefix="uc" Src="~/main/EDocs/UploadDocs.ascx" %>
<%@ Register TagName="DocumentCapture" TagPrefix="uc" Src="~/main/EDocs/DocumentCaptureUpload.ascx" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>EDocs</title>
</head>
<body>
<form runat="server">
<div class="warp">
    <tpo:LoanNavHeader ID="LoanNavHeader" runat="server" />
    <div class="content-detail" name="content-detail">
        <div class="warp-section warp-section-tabs edocs-section" loan-static-tab>
            <div class="static-tabs"><div class="static-tabs-inner">
                <header class="page-header table-flex table-flex-app-info">
                    <div>E-docs</div>
                </header>
                <div id="GenericFrameworkLinksEdocs" ng-bootstrap="LoanHeader" generic-framework-vendor-links></div>
                <div id="LoanNavEdocsDiv" class="text-right" runat="server">
                    <button type="button" id="FaxCoversheetsButton" runat="server" class="btn btn-default" OnServerClick="FaxCoversheetsButton_Click">Fax Coversheets</button>
                    <button type="button" id="SubmitToDocumentCheckButton" runat="server" class="btn btn-default" style="display: none;" onclick="submitToDocumentCheck();">Submit to Document Check</button>
                </div>

                <ul id="LinkRow" nowrap class="nav nav-tabs nav-tabs-panels">
                    <li><a id="link1" data-index="1" href="#tab1" class="tabLink">Upload Docs</a></li>
                    <li><a data-index="2" id="link2" class="tabLink" href="#tab2">Fax Docs</a></li>
                    <li><a data-index="3" id="link3" class="tabLink" href="#tab3">Document List</a></li>
                    <li id="DocumentCaptureTabLink" runat="server"><a data-index="4" id="link4" class="tabLink" href="#tab4">Document Capture</a></li>
                </ul>
            </div></div>
            <div class="content-tabs">
                <div id="tab1" class="content-tabs-edocs" data-link-id="link1">
                    <uc:UploadDocs runat="server" />
                </div>
                <div id="tab2" class="content-tabs-edocs" data-link-id="link2">
                    <uc:FaxDocs runat="server" />
                </div>
                <div id="tab3" class="content-tabs-edocs" data-link-id="link3">
                    <uc:DocsReceived runat="server" />
                </div>
                <div id="tab4" runat="server" class="content-tabs-edocs" data-link-id="link4">
                    <uc:DocumentCapture runat="server" />
                </div>
            </div>
        </div>
      <asp:CheckBox runat="server" AutoPostBack="true" style="display:none;" />
    </div>
</div>
</form>
<script>
    var unloadMessage = 'You have documents that are still pending upload. Are you sure you want to leave this screen?';

    function getPreNavigationUnloadMessage() {
        if (shouldShowPreTabChangeUnloadMessage()) {
            return unloadMessage;
        }
    }

    function shouldShowPreTabChangeUnloadMessage() {
        return getFilesCount('DragDropZone') !== 0 || getFilesCount('DocumentCaptureUploadDragDropZone') !== 0;
    }

    function edocsTabClick(newTabId) {
        var linkId = $(newTabId).attr('data-link-id');

        if (shouldShowPreTabChangeUnloadMessage()) {
            simpleDialog
                .confirm(unloadMessage, "Leave This Page?", "YES", "NO")
                .then(function (isConfirmed) {
                    if (isConfirmed) {
                        resetDragDrop('DragDropZone');
                        resetDragDrop('DocumentCaptureUploadDragDropZone');
                        document.getElementById(linkId).click();
                    }
                });

            return false;
        }

        TPOStyleUnification.TabClick(document.getElementById(linkId));
        return true;
    }

    var tabSettings = {
        start: <%=AspxTools.JsNumeric(tabIndex) %> - 1,
        click: edocsTabClick
    };
    $("#LinkRow").idTabs(tabSettings);

    $("#link3").click(function () {
        if (shouldShowPreTabChangeUnloadMessage()) {
            // Do not refresh the page until the user responds
            // to the prompt to save their documents.
            return;
        }

        $("#tab3").hide();
        Page_BlockSubmit = false;
        __doPostBack("tabIndex", "3");
    });

    $(".btn").mouseup(function(){this.blur();});

    var minwidth = 0;

    $(".span1-header").each(function(){
        minwidth = $(this).width();
    });

    $(".span2-header").each(function(){
        minwidth = $(this).width();
    });

    $(".span3-header").each(function(){
        minwidth = $(this).width();
    });

    $(".warp.header").css("min-width", minwidth);

    function setIframeHeight(iframe) {
        if (iframe) {
            var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
            if (iframeWin.document.body) {
                var height = iframeWin.document.documentElement.scrollHeight || iframeWin.document.body.scrollHeight;
                $(iframe).css("height", height + 4);
            }
        }
    };

    function submitToDocumentCheck() {
        if (ML.HideStatusSubmissionButtonsInOriginatorPortal) {
            return;
        }

        simpleDialog.confirm('Are you sure you would like to <strong>Submit to Document Check</strong>?').
        then(function(isConfirm){
            if (isConfirm) performDocumentCheckSubmission();
        });
    }

    function performDocumentCheckSubmission() {
        var params = {
            sLId: <%=AspxTools.JsString(this.LoanID)%>,
            status: <%=AspxTools.JsNumeric(DataAccess.E_sStatusT.Loan_DocumentCheck)%>
        };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VRoot + '/main/PipelineService.aspx/ChangeLoanStatus',
            data: JSON.stringify(params),
            dataType: 'json',
            async: false,
            success: function () { postDocumentCheckCallback(true); },
            error: function ()  { postDocumentCheckCallback(false); }
        });
    }

    function postDocumentCheckCallback(successful) {
        var message = 'Submission to "Document Check" status ' + (successful ? 'successful.' : 'failed.');

        simpleDialog.alert(message).
            then(function () {
                if (!successful) {
                    return;
                }

                if (ML.HideStatusAndAgentsPageInOriginatorPortal) {
                    $('#SubmitToDocumentCheckButton').hide();
                }
                else {
                    window.location.href = VRoot + '/webapp/StatusAndAgents.aspx?loanid=' + <%=AspxTools.JsString(this.LoanID)%>;
                }
        });
    }

    // Move the "Submit to Document Check" button up to the same row
    // as the generic framework links.
    function updateDocumentCheckButton() {
        var frameworkDiv = $('div.generic-framework');
        if (frameworkDiv.length === 0) {
            // No generic framework vendors specific by the lender.
            return;
        }

        var frameworkLink = frameworkDiv.find('.generic-framework-vendor');

        var documentCheckDiv = $('#LoanNavEdocsDiv');
        documentCheckDiv.detach();
        documentCheckDiv.addClass(frameworkLink.attr('class'));

        frameworkDiv.append(documentCheckDiv);
    }

    function _init()
    {
        if (ML.HideUploadAndFaxLinksInTpoEdocs) {
            $('#LinkRow, #tab1, #tab2, #tab4').hide();
            $('#tab3').addClass('less-top-padding');
        }
        else {
            faxInit();
            uploadInit();
        }

        $('#SubmitToDocumentCheckButton').toggle(!ML.HideStatusSubmissionButtonsInOriginatorPortal);
        if (!ML.HideStatusSubmissionButtonsInOriginatorPortal) {
            window.setTimeout(updateDocumentCheckButton);
        }

        if(<%= AspxTools.JsBool(showUploadPopup) %>)
        {
            showUploadDialog('Upload complete!', {
                shouldShowWaiting: false,
                shouldShowXButton: true,
                shouldShowPipeline: true,
                width: 420,
            });
        }

        TPOStyleUnification.Components(document,true);
        TPOStyleUnification.TabPages();
    }
</script>
</body>
</html>
