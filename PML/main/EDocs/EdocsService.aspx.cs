﻿#region Generated code
namespace PriceMyLoan.main.EDocs
#endregion
{
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Provides a set of utilities for the e-docs page.
    /// </summary>
    public partial class EdocsService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Calls the method specified by name.
        /// </summary>
        /// <param name="methodName">
        /// The name of the method.
        /// </param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(GetKtaPostbackData):
                    this.GetKtaPostbackData();
                    break;
                case nameof(SetUploadCookie):
                    this.SetUploadCookie();
                    break;
                case nameof(this.PollForOcrStatus):
                    this.PollForOcrStatus();
                    break;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Gets the data necessary for posting documents to KTA.
        /// </summary>
        private void GetKtaPostbackData()
        {
            KtaUtilities.GenerateKtaPostbackData(this, PrincipalFactory.CurrentPrincipal, E_EDocOrigin.PML);
        }

        /// <summary>
        /// Sets the upload cookie for a document upload.
        /// </summary>
        private void SetUploadCookie()
        {
            KtaUtilities.SetUploadCookie(this);
        }

        /// <summary>
        /// Polls for the status of a KTA upload request.
        /// </summary>
        private void PollForOcrStatus()
        {
            KtaUtilities.PollForOcrStatus(this, PrincipalFactory.CurrentPrincipal);
        }
    }
}