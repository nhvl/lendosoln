﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentCaptureUpload.ascx.cs" Inherits="PriceMyLoan.main.EDocs.DocumentCaptureUpload" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
    $(document).ready(function () {
        TPOStyleUnification.Components();
        validateWorkflowPermissions();
    });

    function validateWorkflowPermissions() {
        if (!ML.CanSubmitToKta) {
            $('#ClientSubmit').prop('disabled', true).attr('title', ML.KtaSubmissionDenialReason);
        }
    }

    function validate() {
        var errors = "";
        if (!ML.HasKtaServiceCredential) {
            if ($.trim($('#' + <%= AspxTools.JsString(KtaUsername.ClientID) %>).val()) === "") {
                errors += "Please enter a valid username.<br />";
            }

            if ($.trim($('#' + <%= AspxTools.JsString(KtaPassword.ClientID) %>).val()) === "") {
                errors += "Please enter a valid password.<br />";
            }
        }

        var failedValidation = false;
        var hasFileSelected = false;
        for (var i = 0; i < window.frames.length; ++i) {
            if (!window.frames[i].validate()) {
                failedValidation = true;
                break;
            }
            hasFileSelected = hasFileSelected || window.frames[i].hasFile();
        }

        if (failedValidation) {
            errors += 'Please correct the file errors before continuing.';
        }
        else if (!hasFileSelected) {
            errors += 'Please select at least one file to upload.';
        }

        if (errors !== "") {
            simpleDialog.alert(errors);
            return false;
        }

        return true;
    }

    function clientSubmit() {
        if (!validate()) {
            return;
        }

        var args = {
            LoanId: ML.LoanId,
            AppId: ML.AppId
        };

        if (!ML.HasKtaServiceCredential) {
            args.Username = $('#' + <%= AspxTools.JsString(KtaUsername.ClientID) %>).val();
            args.Password = $('#' + <%= AspxTools.JsString(KtaPassword.ClientID) %>).val();
        }

        var frameDataSet = [];
        for (var i = 0; i < window.frames.length; ++i) {
            var frame = window.frames[i];
            if (typeof frame.hasFile === 'function' && frame.hasFile()) {
                var frameData = {};
                frameData.frame = frame;
                frameData.fileName = frame.getFileName();
                frameDataSet.push(frameData);
            }
        }

        args.RequestCount = frameDataSet.length;

        gService.edoc.callAsyncSimple('GetKtaPostbackData', args, function (result) {
            if (result.error) {
                simpleDialog.alert('Unable to submit. Please try again.');
                return;
            }

            var cookies = JSON.parse(result.value.Cookies);
            var payloads = JSON.parse(result.value.KtaPayloads);
            var requestIds = JSON.parse(result.value.RequestIds);

            for (var i = 0; i < frameDataSet.length; i++) {
                var frameData = frameDataSet[i];
                frameData.cookie = cookies[i];
                frameData.payload = payloads[i];
                frameData.requestId = requestIds[i];
            }

            submitDocuments(frameDataSet);
        });
    }

    function submitDocuments(frameDataSet) {
        $('#FileUploadTable, #ClientSubmit').hide();
        setLoadingPopup(true);

        var uploadCounter = 0;
        var completedRequests = [];
        submitSingleDocument(uploadCounter, frameDataSet, completedRequests);
    }

    function submitSingleDocument(uploadCounter, frameDataSet, completedRequests) {
        if (uploadCounter >= frameDataSet.length) {
            summarizeUpload(completedRequests);
            return;
        }

        var frameData = frameDataSet[uploadCounter];
        frameData.frame.submitForm(frameData.cookie, frameData.payload, function (isSuccess) {
            if (isSuccess) {
                setupPolling(uploadCounter, frameDataSet, completedRequests);
            } else {
                // Nothing to poll for, queue up the next file
                recordCompletedRequest(completedRequests, frameData, /* success */ false);
                submitSingleDocument(++uploadCounter, frameDataSet, completedRequests);
            }
        });
    }

    function setupPolling(uploadCounter, frameDataSet, completedRequests) {
        var pollingId = window.setInterval(function () {
            pollForOcrStatus(uploadCounter, frameDataSet, completedRequests);
        }, ML.PollInterval);

        frameDataSet[uploadCounter].pollingId = pollingId;
    }

    function pollForOcrStatus(uploadCounter, frameDataSet, completedRequests) {
        var frameData = frameDataSet[uploadCounter];
        var args = { RequestId: frameData.requestId };
        gService.edoc.callAsyncSimple('PollForOcrStatus', args, function (result) {
            if (result.error) {
                window.clearTimeout(frameDataSet[uploadCounter].pollingId);
                recordCompletedRequest(completedRequests, frameData, /* success */ false);
                submitSingleDocument(++uploadCounter, frameDataSet, completedRequests);
                return;
            }

            if (result.value.Ready === "True") {
                window.clearTimeout(frameDataSet[uploadCounter].pollingId);
                recordCompletedRequest(completedRequests, frameData, /* success */ true);
                submitSingleDocument(++uploadCounter, frameDataSet, completedRequests);
            }
        });
    }

    function recordCompletedRequest(completedRequests, frameData, success) {
        for (var i = 0; i < completedRequests.length; i++) {
            // The async behavior can cause entries to be added more than once.
            // Verify that the requestId has not yet been added to the array.
            if (frameData.requestId === completedRequests[i].requestId) {
                return;
            }
        }

        completedRequests.push({
            fileName: frameData.fileName,
            requestId: frameData.requestId,
            success: success
        });
    }

    function summarizeUpload(completedRequests) {
        var successfulUploadMessage = 'Successfully uploaded documents: \n';
        var failedUploadMessage = 'Failed documents:\n';

        var hasSuccessfulFiles = false;
        var hasFailedFiles = false;

        for (var i = 0; i < completedRequests.length; i++) {
            var request = completedRequests[i];
            if (request.success) {
                successfulUploadMessage += request.fileName + '\n';
                hasSuccessfulFiles = true;
            } else {
                failedUploadMessage += request.fileName + '\n';
                hasFailedFiles = true;
            }
        }

        var result = '';
        if (hasSuccessfulFiles) {
            result += successfulUploadMessage + '\n\n';
        }

        if (hasFailedFiles) {
            result += failedUploadMessage;
        }

        setLoadingPopup(false);
        simpleDialog.alert(result);
    }

    function setLoadingPopup(show) {
        if (show) {
            LQBPopup.ShowElementWithWrapper($("#CaptureLoading"), {
                draggable: false,
                hideXButton: true,
                hideCloseButton: true,
                backdrop: "static",
            });
        }
        else {
            LQBPopup.Hide();
        }
    }
</script>
<style type="text/css">
    .submit-frame {
        border: none;
        height: 38px;
        width: 100%;
    }

    #FileUploadTable {
        width: 50%;
    }

    #FileUploadTable input[type=file] {
        width: 100%;
    }
</style>

<table class="primary-table wide none-hover-table table-upload-docs" id="CredentialTable" runat="server">
    <tr class="GridHeader">
        <th colspan="2">
            Capture Credentials
        </th>
    </tr>
    <tr>
        <td class="FieldLabel">
            Username
        </td>
        <td>
            <asp:TextBox ID="KtaUsername" runat="server" doesntdirty="true" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            Password
        </td>
        <td>
            <asp:TextBox ID="KtaPassword" runat="server" TextMode="Password" doesntdirty="true" />
        </td>
    </tr>
</table>

<br />

<table id="FileUploadTable" class="primary-table wide none-hover-table table-upload-docs">
    <tr class="GridHeader">
        <th>File Upload</th>
    </tr>
    <tr>
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr>
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr>
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr>
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr>
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr>
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
</table>

<br />

<input type="button" id="ClientSubmit" onclick="clientSubmit();" value="Upload" class="btn btn-default" />

<div id="CaptureLoading" class="text-center none-display">
    Uploading, please wait.
    <br />
    <img src="../../images/loading.gif" alt="Loading" />
</div>
