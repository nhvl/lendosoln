﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;

namespace PriceMyLoan.main.EDocs
{
    public partial class EDocPicker : PriceMyLoan.UI.BasePage
    {
        private Guid BrokerID
        {
            get { return PriceMyLoanUser.BrokerId; }
        }

        protected int? RequiredDocTypeId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //need to load associations also and filter those out 
            //need to allow associations from opener and filter those out .
            string taskid = RequestHelper.GetSafeQueryString("taskid");
            if (!string.IsNullOrEmpty(taskid))
            {
                Task t = Task.Retrieve(BrokerID, taskid);
                ConditionId.Text = t.TaskId;
                CondCategory.Text = t.CondCategoryId_rep;
                CondRequiredDoctype.Text = t.CondRequiredDocType_rep;
                TaskDueDate.Text = t.TaskDueDate_rep;
                TaskSubject.Text = t.TaskSubject;
                RequiredDocTypeId = t.CondRequiredDocTypeId;
            }
            else
            {
                this.ConditionTable.Visible = false;
                this.Description.Visible = false;
                this.ModalTitle.Text = "Select document(s) for association";
            }

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            IList<EDocument> docs = repo.GetDocumentsByLoanId(LoanID);
            List<DocType> p = EDocumentDocType.GetStackOrderedDocTypesByBroker(BrokerDB.RetrieveById(BrokerID)).ToList();
            Dictionary<string, int> docTypeIndeces = new Dictionary<string,int>();
            
            for(int i =0; i<p.Count; i++)
            {
                docTypeIndeces.Add(p[i].DocTypeId, i);
            }
            DocListing.DataSource = docs.OrderBy(y=>GetIndex(y.DocumentTypeId.ToString(), docTypeIndeces));
            DocListing.DataBind();
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
            RegisterJsScript("jquery.tablesorter.min.js");
            this.RegisterJsGlobalVariables("IsSinglePickOnly", RequestHelper.GetBool("single"));

        } 

        protected string GetEDocClass(EDocument edoc)
        {
            if (RequiredDocTypeId.HasValue && edoc.DocumentTypeId == RequiredDocTypeId.Value)
            {
                return "highlight";
            }
            return "";
        }

        protected string GetSelectAllClass()
        {
            if (RequestHelper.GetBool("single"))
            {
                return "none-display";
            }

            return string.Empty;
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        public int GetIndex(string id, Dictionary<string,int> indeces)
        {
           int index;
           if (indeces.TryGetValue(id, out index))
           {
               return index;
           }

           return int.MaxValue;
        }
    }
}
