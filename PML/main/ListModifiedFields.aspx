<%@ Page language="c#" Codebehind="ListModifiedFields.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.ListModifiedFields" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>ListModifiedFields</title>
		<style>
.SectionHeaderSmall 
{
	font-size: 14px;
	color: #FF9933;
	font-weight: bolder;
	padding-top: 10px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;
}		
.WarningMsgBorder 
{
	font-family : Verdana, Arial, Helvetica, sans-serif;
	font-size : 11px;
  border:#ffcc99 1px dashed;
  font-weight:bold;
  color:#EA1C0D;
  background-color:#ffffcc;
}
pre {
    font-size: 11px;
	color: #666666;
    font-family: Arial, Helvetica, sans-serif;
}
		</style>  
  </HEAD>
  <body MS_POSITIONING="FlowLayout"  leftmargin="0" topmargin="0"  scroll=yes>
  <script type="text/javascript" language="javascript">
  <!--
      function f_close() {
          if (parent && parent.closeFrame) {
              parent.closeFrame();
          }
          else if (typeof (parent.LQBPopup) != 'undefined'){
              parent.LQBPopup.Hide();
          }
          else if (parent.window) {
            parent.window.close();
          }
          else {
            self.close();
            }
      }
  
  // -->
  </script>
    <h4 class="page-header">View updated fields</h4>
    <form id="ListModifiedFields" method="post" runat="server">
    <div class="WarningMsgBorder">
      <%= AspxTools.HtmlString(m_warningMessage) %>
    </div>
    
    <div style="margin:5px">
    <div class="SectionHeaderSmall">Step 1 - Credit</div>
    <asp:GridView ID="m_gvStep1" runat="server" AutoGenerateColumns="false" EnableViewState="false" CssClass="DataGrid" Width="95%" RowStyle-VerticalAlign="Top"  RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" HeaderStyle-CssClass="GridHeader" HeaderStyle-HorizontalAlign="Left">
      <EmptyDataTemplate>
        <div class="GridItem" style="width:95%; text-align:center; font-weight:bold">No Changes</div>
      </EmptyDataTemplate>
      <Columns>
        <asp:BoundField HeaderText="Name" DataField="Description" />
        <asp:TemplateField HeaderText="Old Value">
          <ItemTemplate>
            <pre><%# AspxTools.HtmlString(Eval("OldValue").ToString()) %></pre>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="New Value">
        <ItemTemplate>
          <pre><%# AspxTools.HtmlString(Eval("NewValue").ToString()) %></pre>
          </ItemTemplate> 
        </asp:TemplateField>
      </Columns>
    </asp:GridView>
    
      <div class="SectionHeaderSmall">Step 2 - Applicant</div>
      <asp:GridView ID="m_gvStep2" runat="server" AutoGenerateColumns="false" EnableViewState="false"  RowStyle-VerticalAlign="Top" CssClass="DataGrid" Width="95%" RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" HeaderStyle-CssClass="GridHeader" HeaderStyle-HorizontalAlign="Left">
        <EmptyDataTemplate>
          <div class="GridItem" style="width: 95%; text-align: center; font-weight: bold">No Changes</div>
        </EmptyDataTemplate>
        <Columns>
          <asp:BoundField HeaderText="Name" DataField="Description" />
          <asp:TemplateField HeaderText="Old Value">
            <ItemTemplate>
              <pre><%# AspxTools.HtmlString(Eval("OldValue").ToString()) %></pre>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="New Value">
            <ItemTemplate>
              <pre><%# AspxTools.HtmlString(Eval("NewValue").ToString()) %></pre>
            </ItemTemplate>
          </asp:TemplateField>
        </Columns>
      </asp:GridView>
      
      <div class="SectionHeaderSmall">Step 3 - Property & Loan</div>
      <asp:GridView ID="m_gvStep3" runat="server" AutoGenerateColumns="false" EnableViewState="false"  RowStyle-VerticalAlign="Top" CssClass="DataGrid" Width="95%" RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" HeaderStyle-CssClass="GridHeader" HeaderStyle-HorizontalAlign="Left">
        <EmptyDataTemplate>
          <div class="GridItem" style="width: 95%; text-align: center; font-weight: bold">No Changes</div>
        </EmptyDataTemplate>
        <Columns>
          <asp:BoundField HeaderText="Name" DataField="Description" />
          <asp:TemplateField HeaderText="Old Value">
            <ItemTemplate>
              <pre><%# AspxTools.HtmlString(Eval("OldValue").ToString()) %></pre>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="New Value">
            <ItemTemplate>
              <pre><%# AspxTools.HtmlString(Eval("NewValue").ToString()) %></pre>
            </ItemTemplate>
          </asp:TemplateField>
        </Columns>
      </asp:GridView>
    </div>
    <br><br>
    <div style="width: 100%; text-align: center">
      <input type="button" value="Close" onclick="f_close();"></div>
    </form>
	
  </body>
</HTML>
