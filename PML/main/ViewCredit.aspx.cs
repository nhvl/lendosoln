using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using LendersOffice.Constants;
using LendersOffice.Security;
using System.Threading;
using PriceMyLoan.Common;


namespace PriceMyLoan.UI.Main
{

    public partial class ViewCredit : PriceMyLoan.UI.BasePage
    {
#if LQB_NET45
        protected global::System.Web.UI.HtmlControls.HtmlIframe PdfFrame;
#else
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl PdfFrame;
#endif

        // 3/20/2009 dd - The credit report warning text is provided to us by Citi. DO NOT MODIFY unless notify Thinh.
        private const string CreditReportWarning = "This file contains Social Security Number, sensitive information, in clear text. If it is misused it can potentially be used for identity theft. Please avoid opening or printing this file on a public computer as the content of this file may be cached on the computer. Delete local cache if you open this file on a public computer. ";

        private const string NoCreditReportText = "There is no credit report on this application (or no viewable credit data is available).";

        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_message.Text = CreditReportWarning;
        }
        private void m_btnContinue_Click(object sender, EventArgs e)
        {
            LoadCreditReport();
        }

        private void LoadCreditReport() 
        {

            Guid sLId = RequestHelper.GetGuid("loanid");
            Guid applicationID = RequestHelper.GetGuid("applicationid", Guid.Empty);
            CPageData dataLoan = new CCreditReportViewData(sLId);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(applicationID);

            ICreditReportView creditReportView = dataApp.CreditReportView.Value;

            if (null != creditReportView)
            {
                if (creditReportView.HasHtml)
                {
                    Response.Clear();
                    byte[] buffer;

                    //Since we cleared the response up there, we need to activate the print button like this
                    string showPrint = @"<script>parent.menu.f_showPrintButton();</script>";
                    buffer = System.Text.UTF8Encoding.UTF8.GetBytes(showPrint);
                    Response.OutputStream.Write(buffer, 0, buffer.Length);

                    buffer = System.Text.UTF8Encoding.UTF8.GetBytes(creditReportView.HtmlContent);
                    Response.OutputStream.Write(buffer, 0, buffer.Length);
                    Response.End();
                }
                
                else if (creditReportView.HasPdf)
                {
                    //If there's only pdf data, transmit the pdf to them inline. This will
                    //make it show up on their in-browser pdf viewer if they have one, or 
                    //ask them if they want to download the file if they don't.
                    //The in-browser pdf viewer will have its own save option.
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    byte[] buffer = System.Convert.FromBase64CharArray(creditReportView.PdfContent.ToCharArray(), 0, creditReportView.PdfContent.Length);
                    Response.OutputStream.Write(buffer, 0, buffer.Length);
                    Response.End();
                }
                    
                else
                {
                    m_message.Text = ErrorMessages.CreditReportViewableDataMissing;
                    m_btnContinue.Visible = false;
                    cancelbtn.Visible = false;
                }
            }
            else
            {
                m_message.Text = NoCreditReportText;
            }

        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }
		
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.m_btnContinue.Click += new EventHandler(m_btnContinue_Click);

        }

		#endregion
    }
}
