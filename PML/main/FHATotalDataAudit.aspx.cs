﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using MeridianLink.CommonControls;
using PriceMyLoan.Common;

namespace PriceMyLoan.main
{
    public partial class FHATotalDataAudit : PriceMyLoan.UI.BasePage
    {
        class ExtraValidationHelper
        {
            public string LinkName { get; set; }
            public int AppNumber { get; set; }
            public Guid AppId { get; set; }
            public string PageUrl { get; set; }
            public string PageUrlArgs { get; set; }
            public string Error { get; set; }
        }

        public bool HideTable { get; set; }

        private BrokerDB x_brokerDB;
        private BrokerDB CurrentBroker
        {
            get
            {
                if (null == x_brokerDB)
                {
                    x_brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
                }
                return x_brokerDB;
            }
        }


        private bool isAccessDenied
        {
            get
            {
                return !(CurrentBroker.IsTotalScorecardEnabled) || (PriceMyLoanUser.HasPermission(LendersOffice.Security.Permission.CanAccessTotalScorecard) == false);
            }
        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            // 6/14/2012 dd - REMN integration with ENcompass do not need to display cancel button.
            CancelBtn.Visible = PriceMyLoanUser.IsSpecialRemnEncompassTotalAccount == false;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //opm 34211 fs 10/30/09 - PML FHA TOTAL Scorecard only enabled for Thinh's broker for now
            if (isAccessDenied)
                PriceMyLoanRequestHelper.AccessDenial(Guid.Empty);

            HideTable = true;
            this.PageID = "FHATotalAudit";
                                                                                  
            CPageData data = new CFHATotalAuditData(LoanID);
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            data.InitLoad();

            if (E_sLoanFileT.QuickPricer2Sandboxed == data.sLoanFileT)
            {
                var msg = "You may not access FHA Total Data Audit when loan is quickpricer sandboxed.";
                throw new CBaseException(msg, msg);
            }


            List<CAppData> apps = new List<CAppData>(data.nApps );
            List<ExtraValidationHelper> otherItems = new List<ExtraValidationHelper>();

            for( int i = 0; i < data.nApps; i++ ) 
            {
                CAppData currentApp = data.GetAppData(i);
                
                apps.Add( currentApp );

                otherItems.Add(new ExtraValidationHelper()
                {
                    LinkName = "Borrower Employment History",
                    AppNumber = i + 1,
                    AppId = currentApp.aAppId,
                    Error = currentApp.aBEmpCollectionValid ? string.Empty : "Employment dates are not valid",
                });

                if (currentApp.aCIsValidNameSsn)
                {
                    otherItems.Add(new ExtraValidationHelper()
                    {
                        LinkName = "Co-borrower Employment History",
                        AppNumber = i + 1,
                        AppId = currentApp.aAppId,
                        Error = currentApp.aCEmpCollectionValid ? string.Empty : "Employment dates are not valid",
                    });
                }

                if (!currentApp.aBEmpCollectionValid || !currentApp.aCEmpCollectionValid)
                {
                    HideTable = false;
                }
            }

            Applications.DataSource = apps;
            Applications.DataBind();
            
            ls_PrimaryBorrower.Text = data.sPrimaryNm;
            ls_AppraisedValue.Text = data.sApprVal_rep;
            ls_CashftBorrower.Text = data.sTransNetCash_rep;
            ls_LoanAmount.Text = data.sLAmtCalc_rep;
            ls_LoanPurpose.Text = data.sLPurposeT_rep;
            ls_MortgageLiabilities.Text = data.sTotalMortgageTradelineCount_rep;

            ls_NumberOfTotalApps.Text = data.sBorrCount_rep;
            ls_PurchasePrice.Text = data.sPurchPrice_rep;
            ls_ReoPropertiesCount.Text = data.sReCollectionCount_rep;
            ls_RepresentativeScore.Text = data.sCreditScoreType2Soft_rep;
            ls_SellerClosingCostPaid.Text = data.sTotCcPboPbs_rep;
            ls_TotalCosts.Text = data.sTotTransC_rep;
            ls_TotalLiquidAssets.Text = data.sAsstLiqTotal_rep;
            ls_TotalLoanAmount.Text = data.sFinalLAmt_rep;
            ls_TotalMonthlyIncome.Text = data.sLTotI_rep;
            ls_TotalNonMortgagePayments.Text = data.sLiaMonLTot_rep;
            ls_UpfrontMip.Text = data.sFfUfmip1003_rep;
            ls_UpfrontMipFinanced.Text = data.sFfUfmipFinanced_rep;
            ls_ltvcltv.Text = string.Format("{0} / {1}", data.sLtvR_rep, data.sCltvR_rep);

            sLTotITotalScorecardWarning.Text = data.sLTotIPmlImportWarning;
            sApprValTotalScorecardWarning.Text = data.sApprValPmlImportWarning;
            sFfUfmip1003TotalScorecardWarning.Text = data.sFfUfmip1003PmlImportWarning;
            sBorrCountTotalScorecardWarning.Text = data.sBorrCountTotalScorecardWarning;

            ExtraEmploymentValidation.DataSource = otherItems;
            ExtraEmploymentValidation.DataBind(); 

            sPurchPriceTotalScorecardWarning.Text = data.sPurchPricePmlImportWarning;
            sGseSpTFriendlyDisplay.Text = data.sGseSpTFriendlyDisplay;
            sTotEstCcNoDiscnt1003.Text = data.sTotEstCcNoDiscnt1003_rep;
            sTotalScorecardBorrPaidCc.Text = data.sTotalScorecardBorrPaidCc_rep;


            ContinueBtn.Enabled = !data.sHasTotalScorecardWarning;
            sLAmtCalcTotalScorecardWarning.Text = data.sLAmtCalcPmlImportWarning;

            if (HideTable)
            {
                AdditioanlValidationError.Text = "";
                AdditionalValidationStatus.Text = "All passed";
                TableAnchor.InnerText = "(show)";

            }
            else
            {

                AdditionalValidationStatus.Text = "";
                AdditioanlValidationError.Text = "Errors detected";
                TableAnchor.InnerText = "(hide)";
            }
        }

        protected void Continue_OnClick(object sender, EventArgs args)
        {
            Response.Redirect("FHATotalSubmit.aspx?loanid=" + LoanID);
        }

        protected void ExtraValidation_DataBound(object sender, RepeaterItemEventArgs args)
        {
            ExtraValidationHelper currentItem = args.Item.DataItem as ExtraValidationHelper;
            EncodedLiteral appNumber = (EncodedLiteral)args.Item.FindControl("AppNumber");
            HtmlAnchor anchor = (HtmlAnchor)args.Item.FindControl("ValidationFixLink");
            EncodedLiteral error = (EncodedLiteral)args.Item.FindControl("ValidationError");
            EncodedLiteral status = (EncodedLiteral)args.Item.FindControl("ValidationStatus");

            appNumber.Text = currentItem.AppNumber.ToString();
            anchor.InnerText = currentItem.LinkName;
            anchor.HRef = "#";

            if (string.IsNullOrEmpty(currentItem.Error))
            {
                status.Text = "Passed";
            }
            else
            {
                error.Text = currentItem.Error;
            }

        }

        protected void Applications_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            CAppData data = (CAppData)args.Item.DataItem;
            var isTargetingUlad2019 = data.LoanData.sIsTargeting2019Ulad;

            EncodedLiteral aBEmplrJobTitleTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBEmplrJobTitleTotalScorecardWarning");
            EncodedLiteral aCEmplrJobTitleTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCEmplrJobTitleTotalScorecardWarning");
            EncodedLiteral aBSsnTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBSsnTotalScorecardWarning");
            EncodedLiteral aCSsnTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCSsnTotalScorecardWarning");
            EncodedLiteral aBDecResidencyTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBDecResidencyTotalScorecardWarning");
            EncodedLiteral aCDecResidencyTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCDecResidencyTotalScorecardWarning");
            EncodedLiteral aBDecOccTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aBDecOccTotalScorecardWarning");
            EncodedLiteral aCDecOccTotalScorecardWarning = (EncodedLiteral)args.Item.FindControl("aCDecOccTotalScorecardWarning");

            EncodedLiteral giftFunds = (EncodedLiteral)args.Item.FindControl("GiftFunds");
        


            EncodedLiteral borrowerFullName = (EncodedLiteral)args.Item.FindControl("BorrowerFullName");
            EncodedLiteral borrowerSsn = (EncodedLiteral)args.Item.FindControl("BorrowerSsn");
            EncodedLiteral borrowerCititzenshipStatus = (EncodedLiteral)args.Item.FindControl("BorrowerCititzenshipStatus");
            EncodedLiteral borrowerTitle = (EncodedLiteral)args.Item.FindControl("BorrowerTitle");
            EncodedLiteral borrowerCreditScores = (EncodedLiteral)args.Item.FindControl("BorrowerCreditScores");
            EncodedLiteral borrowerOccupancy = (EncodedLiteral)args.Item.FindControl("BorrowerOccupancy");
            EncodedLiteral coborrowerName = (EncodedLiteral)args.Item.FindControl("CoborrowerName");
            EncodedLiteral coborrowerSsn = (EncodedLiteral)args.Item.FindControl("CoborrowerSsn");
            EncodedLiteral coborrowerCitizenshipStatus = (EncodedLiteral)args.Item.FindControl("CoborrowerCitizenshipStatus");
            EncodedLiteral coborrowerTitle = (EncodedLiteral)args.Item.FindControl("CoborrowerTitle");
            EncodedLiteral coborrowerScores = (EncodedLiteral)args.Item.FindControl("CoborrowerScores");
            EncodedLiteral coborrowerOccupancy = (EncodedLiteral)args.Item.FindControl("CoborrowerOccupancy");
            EncodedLiteral monthlyIncome = (EncodedLiteral)args.Item.FindControl("MonthlyIncome");
            EncodedLiteral liquidAssets = (EncodedLiteral)args.Item.FindControl("LiquidAssets");
            EncodedLiteral nonMortgagePayment = (EncodedLiteral)args.Item.FindControl("NonMortgagePayment");
            EncodedLiteral appNumber = (EncodedLiteral)args.Item.FindControl("AppNumber");
            PlaceHolder placeHolder = (PlaceHolder)args.Item.FindControl("CoborrowerFields");

            appNumber.Text = (args.Item.ItemIndex + 1).ToString();
            borrowerFullName.Text = data.aBNm;
            borrowerSsn.Text = data.aBSsnMasked;
            borrowerCititzenshipStatus.Text = data.aBCitizenDescription;
            borrowerTitle.Text = data.aBEmplrJobTitle;
            borrowerCreditScores.Text = string.Format("XP: {0} TU: {1} EF: {2}", data.aBExperianScore_rep, data.aBTransUnionScore_rep, data.aBEquifaxScore_rep);
            borrowerOccupancy.Text = isTargetingUlad2019 ? data.aBDecIsPrimaryResidenceUlad.ToYN() : data.aBDecOcc;
            aBEmplrJobTitleTotalScorecardWarning.Text = data.aBEmplrJobTitlePmlImportWarning;
            aBSsnTotalScorecardWarning.Text = data.aBSsnPmlImportWarning;
            aBDecResidencyTotalScorecardWarning.Text = data.aBDecResidencyPmlImportWarning;
            aBDecOccTotalScorecardWarning.Text = data.aBDecOccPmlImportWarning;

            placeHolder.Visible = data.aCIsValidNameSsn;

            if (data.aCIsValidNameSsn)
            {
                coborrowerCitizenshipStatus.Text = data.aCCitizenDescription;
                coborrowerName.Text = data.aCNm;
                coborrowerOccupancy.Text = isTargetingUlad2019 ? data.aCDecIsPrimaryResidenceUlad.ToYN() : data.aCDecOcc;
                coborrowerScores.Text = string.Format("XP: {0} TU: {1} EF: {2}", data.aCExperianScore_rep, data.aCTransUnionScore_rep, data.aCEquifaxScore_rep);
                coborrowerSsn.Text = data.aCSsnMasked;
                coborrowerTitle.Text = data.aCEmplrJobTitle;

                aCEmplrJobTitleTotalScorecardWarning.Text = data.aCEmplrJobTitlePmlImportWarning;
                aCSsnTotalScorecardWarning.Text = data.aCSsnPmlImportWarning;
                aCDecResidencyTotalScorecardWarning.Text = data.aCDecResidencyPmlImportWarning;
                aCDecOccTotalScorecardWarning.Text = data.aCDecOccPmlImportWarning;
            }

            monthlyIncome.Text = data.aTotI_rep;
            liquidAssets.Text = data.aAsstLiqTot_rep;
            nonMortgagePayment.Text = data.aLiaMonTot_rep;
            giftFunds.Text = data.aGiftFundAssets_rep;
      
        }
    }

}


