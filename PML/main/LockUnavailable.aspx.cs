﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;
using PriceMyLoan.Security;
using DataAccess;

namespace PriceMyLoan.main
{
    public partial class LockUnavailable : PriceMyLoan.UI.BasePage
    {

        private Guid BrokerID
        {
            get { return PriceMyLoanUser.BrokerId; }
        }
        private CPageData x_LoanWithPriceGroupData = null;
        /// <summary>
        /// a loan with price group data, and it is initloaded;
        /// </summary>
        private CPageData LoanWithPriceGroupData
        {
            get
            {
                if (x_LoanWithPriceGroupData == null)
                {
                    var loan = new CPageData(LoanID, new string[] { "sPriceGroup" });
                    loan.InitLoad();
                    x_LoanWithPriceGroupData = loan;
                }
                return x_LoanWithPriceGroupData;
            }
        }
        protected string m_rateLockSubmissionUserWarningMessage
        {
            get
            {
                // 12/13/2007 dd - Display warning message from 1st product first. If 1st product has no warning then look at warning for 2nd product.
                string str = RequestHelper.GetSafeQueryString("blocksubmitmsg");
                if (str == "" || str == null)
                    str = RequestHelper.GetSafeQueryString("1stblocksubmitmsg");

                if (null == str)
                    return "";
                
                return RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(LoanWithPriceGroupData.sPriceGroup.LockPolicyID.Value, PriceMyLoanUser.LpeRsExpirationByPassPermission, str, BrokerID);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
