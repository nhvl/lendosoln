﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LendersOffice.CreditReport;

namespace PriceMyLoan.UI.Main
{
    class TemporaryCRAInfo
    {
        public string VendorName;
        public string ProtocolUrl;
        public Guid ProtocolID;
        public CreditReportProtocol ProtocolType;
        public string UserName;
        public string Password;
        public string AccountID;
        public bool IsExperianPulled;
        public bool IsEquifaxPulled;
        public bool IsTransUnionPulled;
        public bool IsFannieMaePulled;
        public string CreditMornetPlusUserId;
        public string CreditMornetPlusPassword;
        public string ProviderId;

        public TemporaryCRAInfo(string vendorName, string url, Guid protocolID, CreditReportProtocol protocolType, string providerId /*MCL Code*/)
        {
            this.VendorName = vendorName;
            this.ProtocolUrl = url;
            this.ProtocolID = protocolID;
            this.ProtocolType = protocolType;
            this.ProviderId = providerId;
        }
    }
}
