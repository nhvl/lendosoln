using System;
using System.Data;
using System.Data.SqlClient;
using LendersOffice.Admin;
using LendersOffice.Common;
using PriceMyLoan.Security;

using LODataAccess = DataAccess;
using PriceMyLoan.Common;
using System.Web.UI.HtmlControls;
using LendersOffice.Constants;
using LendersOffice.AntiXss;
using System.Web.UI;
namespace PriceMyLoan.UI
{

    public class BaseUserControl : System.Web.UI.UserControl
    {
        protected int sFileVersion
        {
            get
            {
                return ((BasePage)this.Page).sFileVersion;
            }
            set
            {
                ((BasePage)this.Page).sFileVersion = value;
            }
        }
        private const string ContextItem_BrokerDbKey="PmlCurrentBrokerDb";
        private BrokerDB x_brokerDB;

        private BrokerDB m_brokerDB 
        {
            get 
            {
                if (null == x_brokerDB) 
                {
                    // 3/26/2008 dd - Add BrokerDB object to Context.Items cache.
                    x_brokerDB = Context.Items[ContextItem_BrokerDbKey] as BrokerDB;
                    if (null == x_brokerDB) 
                    {
                        x_brokerDB = BrokerDB.RetrieveById(PriceMyLoanUser.BrokerId);
                        Context.Items[ContextItem_BrokerDbKey] = x_brokerDB;

                    }
                }
                return x_brokerDB;
            }
        }
        protected BrokerDB CurrentBroker 
        {
            get { return m_brokerDB; }
        }

        protected bool IsAllowDoSubmission 
        {
            get 
            {
                return (((PriceMyLoanConstants.IsEmbeddedPML && m_brokerDB.IsEmbeddedPmlDoEnabled) || (!PriceMyLoanConstants.IsEmbeddedPML && m_brokerDB.IsPmlDoEnabled))
                    );
            }
        }
        protected bool IsAllowDuSubmission 
        {
            get 
            {
                return (((PriceMyLoanConstants.IsEmbeddedPML && m_brokerDB.IsEmbeddedPmlDuEnabled) || (!PriceMyLoanConstants.IsEmbeddedPML && m_brokerDB.IsPmlDuEnabled))
                    );
            }
        }
        protected bool IsAllowLpSubmission 
        {
            get { return (((PriceMyLoanConstants.IsEmbeddedPML && m_brokerDB.IsEmbeddedPmlLpEnabled) || (!PriceMyLoanConstants.IsEmbeddedPML && m_brokerDB.IsPmlLpEnabled))
                );
            }
        }
        public string VirtualRoot 
        {
            get { return LODataAccess.Tools.VRoot; }
        }


        public Guid LoanID 
        {
            get 
            {
                if (Page is PriceMyLoan.webapp.pml)
                {
                    return ((PriceMyLoan.webapp.pml)Page).LoanID;
                }
                try 
                {
                    if (Context.Items["LoanID"] != null)
                        return (Guid) Context.Items["LoanID"];

                    string str = Request["LoanID"];
                    if (string.IsNullOrEmpty(str))
                    {
                        return Guid.Empty;
                    }
                    else
                    {
                        return new Guid(Request["LoanID"]);  // 08/25/04-Binh-Took out ".Form" so that it won't break on a GET method
                    }
                } 
                catch (FormatException)
                {
                    return Guid.Empty;
                }
            }            
        }
        public PriceMyLoanPrincipal PriceMyLoanUser 
        {
            get 
            { 
                if (null == Context) return null;
                return (PriceMyLoanPrincipal) Context.User; 
            }
        }
        public Guid BrokerID 
        {
            get 
            { 
                if (null == Context) return Guid.Empty;
                return PriceMyLoanUser.BrokerId; 
            }
        }
        public Guid EmployeeID 
        {
            get 
            { 
                if (null == Context) return Guid.Empty;
                return PriceMyLoanUser.EmployeeId; 
            }
        }
        public Guid UserID 
        {
            get 
            {
                if (null == Context) return Guid.Empty;
                return PriceMyLoanUser.UserId;
            }
        }

        public bool IsReadOnly 
        {
            get 
            {
                BasePage p = this.Page as BasePage;

                if (null != p)
                    return p.IsReadOnly;

                return false;
            }
        }

        public LODataAccess.E_LpeRunModeT lpeRunModeT 
        {
            get 
            {
                BasePage p = this.Page as BasePage;
                
                if (null != p)
                    return p.lpeRunModeT;
                return LODataAccess.E_LpeRunModeT.NotAllowed;
            }

        }

        public string IsReadOnlyJs 
        {
            get 
            {
                return IsReadOnly ? "true" : "false";
            }
        }

        //opm 27866 fs 03/02/09
        public bool IsCitiBroker
        {
            get
            {
                return false;
            }
        }

        public bool IsDoingRedirect
        {
            get
            {
                if (Page is BasePage)
                {
                    return ((BasePage)Page).IsDoingRedirect;
                }
                else
                {
                    return false;
                }
            }
        }

        protected void SetRequiredValidatorMessage(System.Web.UI.WebControls.BaseValidator v) 
        {
            HtmlImage img = new HtmlImage();
            img.Src = ResolveUrl("~/images/error_pointer.gif");
            v.Controls.Add(img);
        }

        protected string GetRequiredImageUrl() 
        {
            return ResolveUrl("~/images/error_pointer.gif");   
        }

        protected string JQuery(params Control[] ctrlList) 
        {
            return AspxTools.JQuery(ctrlList);
        }
        protected string JsString(string input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsString(Enum input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsString(Guid input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsString(int input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsArray(params Enum[] list)
        {
            return AspxTools.JsArray(list);
        }
    }
}
