namespace PriceMyLoan.main
{
    using System;
    using global::DataAccess;
    using LendersOffice.Conversions.LoanProspector;
    using PriceMyLoan.UI;
    using LendersOffice.Security;

    public partial class LoanProspectorSendToSystem : BasePage
    {
        private const string LpFormKey = "responseData";
        protected override LendersOffice.Common.E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return LendersOffice.Common.E_XUAComaptibleValue.Edge;
        }
        protected override bool IsCrossSiteForgeryCheck
        {
            get { return false; }
        }

        [System.Web.Services.WebMethod]
        public static object Import(bool import1003Data, bool importLpFeedback, bool importCreditReport, string responseData)
        {
            try
            {
                // 2017-01-09 If the large request size here becomes an issue, we could easily switch responseData to load from the
                // auto-expire text cache.  This will also have the side effect that there will be a time limit on how long the page
                // can be used to import the data from LP, so Brian and I decided to hold off on this decision for now.
                var parsedServiceOrder = LoanProspectorResponseParser.Parse(responseData); // assume successful parsing, since the page load already checked that. If user manually changes it, in their face.
                LoanProspectorImporter importer = LoanProspectorImporter.CreateImporterForNonseamless(parsedServiceOrder, isImport1003: import1003Data, isImportFeedback: importLpFeedback, isImportCreditReport: importCreditReport, loanId: null);
                importer.Import();
                return new { IsSuccessful = true };
            }
            catch (Exception exc)
            {
                if (exc is PageDataAccessDenied || exc is LoanFieldWritePermissionDenied)
                {
                    Tools.LogWarning(exc);
                }
                else
                {
                    Tools.LogError(exc);
                }

                return new { IsSuccessful = false, UserMessage = (exc as CBaseException)?.UserMessage ?? "Import failed. Please contact your LendingQB system administrator." };
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (this.Request.HttpMethod != "POST")
            {
                this.RegisterJsGlobalVariables("AutoClose", true);
                return;
            }

            string xml = this.Request.Form[LpFormKey];
            var loadedData = LoanProspectorHelper.LoadAssessmentData(xml);
            if (loadedData.IsSuccessful)
            {
                this.AssessmentSummaryData.DataSource = loadedData.AssessmentSummaryData;
                this.AssessmentSummaryData.DataBind();
                this.LpResponseData.Value = xml;// the footprint of a hidden field holding xml is less than the JS variable due to all the escaped characters
            }
            else
            {
                this.RegisterJsGlobalVariables("GeneralErrorMessage", loadedData.UserErrorMessage);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
