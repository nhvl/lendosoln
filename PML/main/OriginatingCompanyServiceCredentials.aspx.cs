﻿using System;
using System.Web.UI;
using LendersOffice.Common;
using LendersOffice.ObjLib.ServiceCredential;

namespace PriceMyLoan.main
{
    public partial class OriginatingCompanyServiceCredentials : UI.BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("ServiceCredential.js");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.PriceMyLoanUser.IsPmlManager)
            {
                throw new LendersOffice.Security.AccessDenied();
            }

            ServiceCredentialContext context = ServiceCredentialContext.CreateEditOriginatingCompanyContextForUser(this.PriceMyLoanUser);
            var serviceCredentials = ServiceCredential.GetTpoEditableOriginatingCompanyServiceCredentials(context);
            var serviceCredentialsJson = SerializationHelper.JsonNetAnonymousSerialize(serviceCredentials);
            Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
        }
    }
}