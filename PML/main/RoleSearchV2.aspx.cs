﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace PriceMyLoan.main
{
    public partial class RoleSearchV2 : PriceMyLoan.UI.BasePage
    {
        private static PmlEmployeeRoleControlValues LoadControlValues(string key, PriceMyLoanPrincipal PriceMyLoanUser)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new CBaseException(ErrorMessages.Generic, "key is missing");
            }

            var controlValues = PmlEmployeeRoleControlValues.RetrieveFromCache(key);

            if (controlValues == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Could not load pml control values");
            }

            if (controlValues.OwnerId != PriceMyLoanUser.UserId)
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "User id does not match the stored id.");
            }

            return controlValues;

        }

        protected void Page_Load(object sender, System.EventArgs a)
        {
            var key = RequestHelper.GetSafeQueryString("k");
            var controlValue = LoadControlValues(key, PriceMyLoanUser);

            m_Title.Text = (RequestHelper.GetBool("isassigned") ? "Re-Assign " : "Assign ") + controlValue.RoleDesc;

            if (IsPostBack)
            {
                return;
            }

            var searchResults = Search(controlValue, controlValue.SearchFilter, controlValue.EmployeeStatus, PriceMyLoanUser);

            RegisterJsObjectWithJsonNetAnonymousSerializer("RoleSearchViewModel", new
            {
                // m_controlValues,
                key,
                controlValue.RoleDesc,
                searchFilter = controlValue.SearchFilter,
                employeeStatus = controlValue.EmployeeStatus,
                m_noLOAssignedWarning = RequestHelper.GetBool("showlowarning"),
                LoDesc = controlValue.RoleId == CEmployeeFields.s_LoanRepRoleId ? "Loan Officer" : (
                    (PriceMyLoanConstants.IsEmbeddedPML ? "(External) " : string.Empty) + "Secondary"
                ),
                searchResults,
            });
        }

        [WebMethod]
        public static IEnumerable<object> Search(string key, string searchFilter, E_EmployeeStatusFilterT employeeStatus)
        {
            var PriceMyLoanUser = HttpContext.Current.User as PriceMyLoanPrincipal;
            if (PriceMyLoanUser == null)
            {
                throw new CBaseException("Session expired.", "Unexpected principal");
            }

            var controlValues = LoadControlValues(key, PriceMyLoanUser);

            return Search(controlValues, searchFilter, employeeStatus, PriceMyLoanUser);
        }

        private static IEnumerable<object> Search(PmlEmployeeRoleControlValues controlValues, string searchFilter, E_EmployeeStatusFilterT employeeStatus, PriceMyLoanPrincipal PriceMyLoanUser)
        {
            var Role = controlValues.Role;
            var RoleID = controlValues.RoleId;
            var EmployeeId = controlValues.EmployeeId;
            var LoanId = controlValues.LoanId;

            bool isPmlRole =
                RoleID == CEmployeeFields.s_LoanRepRoleId ||
                RoleID == CEmployeeFields.s_BrokerProcessorId ||
                RoleID == CEmployeeFields.s_ExternalSecondaryId ||
                RoleID == CEmployeeFields.s_ExternalPostCloserId;

            //if theres an employee id load the pml broker id from it
            Guid m_sPmlBrokerIdFilter = Guid.Empty;
            if (EmployeeId != Guid.Empty && isPmlRole)
            {
                EmployeeDB db = new EmployeeDB(EmployeeId, PriceMyLoanUser.BrokerId);
                db.Retrieve();
                m_sPmlBrokerIdFilter = db.PmlBrokerId;
            }

            E_BranchChannelT? branchChannelFilter = null;
            E_sCorrespondentProcessT? correspondentProcessFilter = null;
            string m_sState = null;

            if (LoanId != Guid.Empty && isPmlRole)
            {
                LoanAccessInfo info = new LoanAccessInfo(LoanId);
                m_sPmlBrokerIdFilter = info.sPmlBrokerId;

                // load the state from dataloan
                CPageData data = CPageData.CreatePmlPageDataUsingSmartDependency(LoanId, typeof(RoleSearchV2));
                data.InitLoad();

                m_sState = data.sSpStatePe;

                branchChannelFilter = data.sBranchChannelT;
                correspondentProcessFilter = data.sCorrespondentProcessT;
            }

            //check to see if the ui has to tell the user to search for words before results.
            BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();

            if (PriceMyLoanUser.HasPermission(LendersOffice.Security.Permission.AllowLoanAssignmentsToAnyBranch))
            {
                roleTable.Retrieve(
                    PriceMyLoanUser.BrokerId,
                    Guid.Empty,
                    searchFilter,
                    Role,
                    employeeStatus,
                    null,
                    null);
            }
            else
            {
                // PML users have a broker branch, mini-correspondent branch,
                // and correspondent branch. We need to filter based on the
                // correct branch, which requires looking at the channel and
                // correspondent process type of the file.
                roleTable.Retrieve(
                    PriceMyLoanUser.BrokerId,
                    PriceMyLoanUser.BranchId,
                    searchFilter,
                    Role,
                    employeeStatus,
                    branchChannelFilter,
                    correspondentProcessFilter);
            }

            if (roleTable[Role] != null)
            {
                return roleTable[Role]
                    .Where(spec => (
                        // The current user is an LO user, so let them pick from any PML broker
                        (PriceMyLoanUser.ApplicationType == E_ApplicationT.LendersOffice)
                        // The current user is a PML user, so only let them pick from users of the same PML broker
                        || (m_sPmlBrokerIdFilter != Guid.Empty && spec.PmlBrokerId == m_sPmlBrokerIdFilter)
                    ))
                    .Select(role => new
                    {
                        employeeId = role.EmployeeId,
                        fullName = role.FullName,
                        email = role.Email,
                        addr = role.Addr,
                        city = role.City,
                        state = role.State,
                        zip = role.Zip,
                        phone = role.Phone,
                        fax = role.Fax,
                        companyName = role.CompanyName,
                        licenses = !string.IsNullOrEmpty(m_sState) ? CAgentFields.GetLendingLicenseByState(m_sState, role.Licenses) : "",
                        pager = role.Pager,
                        cellphone = role.CellPhone,
                        role.FullNameWithIsActiveStatus,
                    })
                    .ToArray();
            }
            return new object[0];
        }
    }
}
