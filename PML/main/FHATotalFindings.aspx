﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHATotalFindings.aspx.cs" Inherits="PriceMyLoan.main.FHATotalFindings" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>FHA TOTAL Scorecard Findings</title>
</head>
<body bgcolor="white" onBeforeUnload="return ConditionCheckUnload();">
    <h4 class="page-header">FHA TOTAL Scorecard Findings</h4>
    <form id="form1" runat="server">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="padding-bottom:5px; padding-right:3px;">
        <tr>
            <td>
                <input type="button" class="ButtonStyle" NoHighlight onclick="ConditionCheck(false);" id="conditionImport" runat="server" value="Import Conditions" />
            </td>
            <td align="right" valign="top">
                <input type="button" class="ButtonStyle" NoHighlight style="width:80px;" onclick="f_print();" value="Print" />&nbsp;&nbsp;
                <input id="m_btnClose" type="button" class="ButtonStyle" NoHighlight style="width:80px;" onclick="f_close();" value="Close" runat="server"/>
            </td>
        </tr>
    </table>
    <iframe runat="server" id="Certificate" width="100%" ></iframe>
    </form>
    <script type="text/javascript">
      $j(function() {
        resize();
        
        $j(window).resize(resize);
      });
         var importConditions = false;

       function f_print() {
           lqbPrintByFrame(window.frames['Certificate']);
       }
       function f_close()
       {
        var iswinopen = <%= AspxTools.JsBool(RequestHelper.GetBool("winopen")) %>;
        if (iswinopen)
        {
          self.close();
        }
        else
        {
          parent.LQBPopup.Return({OK:false});
        }
       }
       function resize()
       {
          var height = $j(document).height() - 20 - $j('#Certificate').offset().top;
          $j('#Certificate').height(height);
       }
       
       function ConditionCheckUnload()
       {
           <% if (PriceMyLoanUser.IsSpecialRemnEncompassTotalAccount)
          { %>
           return;
          <% } %>
           if( importConditions || (document.location.search.indexOf('fv=1') === -1) ) { return; }
           var condImport = document.getElementById('conditionImport');
           condImport.disabled = true;

           setTimeout(function(){
               condImport.disabled = false;
           }, 0);
           return UnloadMessage;
       }
      
        function ConditionCheck(ask) {
          <% if (PriceMyLoanUser.IsSpecialRemnEncompassTotalAccount) { %>
            return;
          <% } %>
            if( importConditions || ( ask && document.location.search.indexOf('fv=1') === -1) ) { return; }
            var condImport = document.getElementById('conditionImport');
            condImport.disabled = true;
            if ( ask ) {
                var answer =   confirm('Import conditions from FHA TOTAL Scorecard findings?');
                if ( !answer ) { return; }
            }
            var obj = {
                LoanID : <%= AspxTools.JsString(LoanID) %>
            };
                
            var y = gService.FHATotal.call("ImportConditions", obj );
            try{
                if ( y.error) 
                {
                    alert( y.UserMessage );
                    condImport.disabled = false;
                }
                else {
                    importConditions = true;
                    condImport.disabled = true;
                }
            }catch(e){
                logJSException(e, 'problem with condition check.'); alert('System Error. Please contact us if this happens again.');
            }
            
        }
    </script>

</body>
</html>
