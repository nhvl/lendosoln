﻿#region Generated Code
namespace PriceMyLoan.main
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.GenericFramework;
    using LendersOffice.ObjLib.PMLNavigation;

    /// <summary>
    /// Provides a means for navigating across loan pages.
    /// </summary>
    public partial class LoanNavigationHeader : UI.BaseUserControl
    {
        /// <summary>
        /// The URL for the legacy closing cost fee editor page.
        /// </summary>
        private const string LegacyClosingCostUrl = "/webapp/TPOFeeEditor.aspx";

        /// <summary>
        /// The URL for the task listing page.
        /// </summary>
        private const string TaskListUrl = "TaskList.aspx?cv=0";

        /// <summary>
        /// The name of the tab for the legacy closing cost editor.
        /// </summary>
        private const string LegacyClosingCostTabName = "Closing Costs";

        /// <summary>
        /// Gets a value indicating whether the header has generic
        /// framework vendors to display on the page.
        /// </summary>
        /// <value>
        /// True if the header has generic framework vendor links,
        /// false otherwise.
        /// </value>
        /// <remarks>
        /// Certain pages may need to render spacers or other UI based
        /// on whether this header is set to display the list of generic
        /// framework vendors. This property is provided as a means to 
        /// avoid the performance hit of loading the broker's list of 
        /// vendors here and on the header's parent page.
        /// </remarks>
        public bool HasGenericFrameworkVendorLinks { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the window is closed when
        /// clicking the "Pipeline" link.
        /// </summary>
        /// <value>
        /// True if the window is closed on return, false otherwise.
        /// </value>
        public bool CloseOnReturn { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the elements in this user control will be render as default 
        /// or manually injected to user HTML.
        /// </summary>
        /// <value>
        /// True if one want to injected the element manually.
        /// </value>
        public bool ManualPlaceElements { get; set; }

        /// <summary>
        /// Sets header data from the specified <paramref name="dataloan"/>.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to load data from.
        /// </param>
        /// <param name="location">
        /// The location of the page to render generic framework links or null if the
        /// header should not render generic framework links.
        /// </param>
        public void SetDataFromLoan(CPageData dataloan, LinkLocation? location = null)
        {
            TaskConditionCount taskConditionCount = null;
            if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                taskConditionCount = new TaskConditionCount(dataloan.sBrokerId, dataloan.sLId, this.PriceMyLoanUser.UserId);
                taskConditionCount.Retrieve();
            }

            var navigationConfigItems = this.GetConfigItems();
            var hasLegacyClosingCostUrl = this.IsItemSelected(LegacyClosingCostUrl);

            // Keep this list in sync with common.js
            string url = Request.Url.ToString();
            bool disableExplicitSavingPage = this.IsItemSelected("/webapp/pml.aspx?source=PML") || this.IsItemSelected("/main/EDocs/Edocs.aspx") || this.IsItemSelected("/webapp/StatusAndAgents.aspx") ||
                this.IsItemSelected("/main/Tasks/TaskList.aspx") || this.IsItemSelected("/webapp/RateLock.aspx") || this.IsItemSelected("/webapp/OrderServices.aspx");

            var parent = (UI.BasePage)this.Page;

            var viewModel = new
            {
                sLId = dataloan.sLId,
                sLNm = dataloan.sLNm,
                aBNm = dataloan.GetAppData(0).aBNm,
                sLAmt = dataloan.sLAmtCalc_rep,
                sSpAddr = dataloan.sSpAddr_SingleLine,
                sLT = dataloan.sLT_rep,
                sCreditScoreType2 = dataloan.sCreditScoreType2,
                sClosingCostFeeVersionT = dataloan.sClosingCostFeeVersionT,
                sRateLockStatusT = dataloan.sRateLockStatusT,
                sIsRateLockExpired = dataloan.sIsRateLockExpired,
                sQMStatusT = dataloan.sQMStatusT,
                sHighPricedMortgageT = dataloan.sHighPricedMortgageT,
                hasLegacyClosingCostUrl = hasLegacyClosingCostUrl,
                hasDisplayQmQueryString = RequestHelper.GetBool("DisplayQM"),
                hasConditionQueryString = RequestHelper.GetBool("cv"),
                taskCount = taskConditionCount?.TaskCount ?? 0,
                conditionCount = taskConditionCount?.ConditionCount ?? 0,
                navigationItems = this.GenerateNavigationItems(navigationConfigItems, taskConditionCount, dataloan),
                alignmentModel = this.GetLinkAlignmentModel(navigationConfigItems, hasLegacyClosingCostUrl),
                genericFrameworkVendorLinks = this.GenerateGenericFrameworkVendorLinks(dataloan.sBrokerId, dataloan.sLId, location),
                currentLocation = location,
                closeOnReturn = this.CloseOnReturn,
                isSaveButtonEnabled = Tools.IsSaveButtonEnabled(this.PriceMyLoanUser),
                disableExplicitSavingPage = disableExplicitSavingPage,
                isOnPricing = this.IsItemSelected("/webapp/pml.aspx?source=PML"),
                isOnClosingCosts = this.IsItemSelected("/webapp/TPOPortalClosingCosts.aspx"),
                isLead = Tools.IsStatusLead(dataloan.sStatusT)
            };

            parent.RegisterJsObjectWithJsonNetAnonymousSerializer("LoanNavigationHeaderViewModel", viewModel);
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the initialization.
        /// </param>
        /// <param name="e">
        /// The arguments for the initialization.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            var parent = (UI.BasePage)this.Page;

            parent.RegisterCSS("LoanNavigationHeader.css");
            parent.RegisterCSS("font-awesome.css");
            parent.RegisterCSS("/webapp/jquery-ui.css");

            parent.RegisterJsScript("Jquery-ui.js");
            parent.RegisterJsScript("genericframework.js");
        }

        /// <summary>
        /// Gets the configuration items for the header.
        /// </summary>
        /// <returns>
        /// The configuration items as page name - page URL pairs.
        /// </returns>
        private IEnumerable<KeyValuePair<string, string>> GetConfigItems()
        {
            var configFileLocation = Tools.GetServerMapPath(PriceMyLoanConstants.TpoPortalNavigationFilename);
            var xmlDoc = XDocument.Load(configFileLocation);

            return xmlDoc.Descendants("item").Select(element => new KeyValuePair<string, string>(
                element.Attribute("name").Value, element.Attribute("url").Value));
        }

        /// <summary>
        /// Generates a list of navigation items for the header.
        /// </summary>
        /// <param name="configItems">
        /// The list of navigation config item key-value pairs.
        /// </param>
        /// <param name="taskConditionCount">
        /// The number of tasks and conditions on the loan file.
        /// </param>
        /// <returns>
        /// A list of navigation items to display in the header.
        /// </returns>
        private IEnumerable<LoanNavigationItem> GenerateNavigationItems(
            IEnumerable<KeyValuePair<string, string>> configItems,
            TaskConditionCount taskConditionCount,
            CPageData dataloan)
        {
            return !this.PriceMyLoanUser.EnableNewTpoLoanNavigation
                    ? new List<LoanNavigationItem>()
                    : configItems
                        .Where(item => !this.IsHiddenForUser(item.Key, taskConditionCount, dataloan))
                        .Select(item => new LoanNavigationItem
                        {
                            Name = item.Key,
                            Url = this.VirtualRoot + item.Value,
                            Selected = this.IsItemSelected(item.Value)
                        })
                        .ToList()
                ;
        }

        /// <summary>
        /// Gets a value indicating whether a header item is hidden for a user.
        /// </summary>
        /// <param name="pageId">
        /// The id of the page to check.
        /// </param>
        /// <param name="taskConditionCount">
        /// The number of tasks and conditions on the loan file.
        /// </param>
        /// <param name="dataloan">
        /// The Loan data.
        /// </param>
        /// <returns>
        /// True if the page is hidden for a user, false otherwise.
        /// </returns>
        private bool IsHiddenForUser(string pageId, TaskConditionCount taskConditionCount, CPageData dataloan)
        {
            if (pageId == "Status and Agents")
            {
                return this.PriceMyLoanUser.BrokerDB.HideStatusAndAgentsPageInOriginatorPortal;
            }

            if (pageId == "Closing Costs")
            {
                return this.PriceMyLoanUser.BrokerDB.DisableFeeEditorInTPOPortalCase183419;
            }

            if (pageId == "Pricing")
            {
                var basePage = (UI.BasePage)this.Page;
                return !basePage.CurrentUserCanPerformPricing();
            }

            if (pageId == "QM")
            {
                return !this.PriceMyLoanUser.BrokerDB.AllowQMEditTPO;
            }

            if (pageId == "Disclosures")
            {
                return !this.PriceMyLoanUser.EnableDisclosurePageInTpoLoanNavigation || Tools.IsStatusLead(dataloan.sStatusT);
            }

            if (pageId == "Order Services")
            {
                return this.PriceMyLoanUser.BrokerDB.HideServicesDashboardInTPOPortal;
            }

            if (pageId == "Tasks")
            {
                if (this.PriceMyLoanUser.BrokerDB.IsUseTasksInTpoPortal)
                {
                    return false;
                }

                return !this.IsItemSelected(TaskListUrl) && taskConditionCount.TaskCount == 0;
            }

            if (pageId == "Rate Lock")
            {
                return Tools.IsStatusLead(dataloan.sStatusT);
            }

            return false;
        }

        /// <summary>
        /// Gets a value indicating whether a navigation item is currently selected,
        /// i.e. the user is on the page that the item links to.
        /// </summary>
        /// <param name="itemUrl">
        /// The url for the navigation item.
        /// </param>
        /// <returns>
        /// True if the navigation item is selected, false otherwise.
        /// </returns>
        private bool IsItemSelected(string itemUrl)
        {
            return this.Request.RawUrl.Contains(itemUrl, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Generates the model for the alignment of generic framework vendor links
        /// for the header.
        /// </summary>
        /// <param name="navigationConfigItems">
        /// The list of navigation configuration items used to determine the current page.
        /// </param>
        /// <param name="hasLegacyClosingCostUrl">
        /// True if the loan is using the legacy closing costs page, false otherwise.
        /// </param>
        /// <returns>
        /// A complex type containing the model.
        /// </returns>
        private object GetLinkAlignmentModel(IEnumerable<KeyValuePair<string, string>> navigationConfigItems, bool hasLegacyClosingCostUrl)
        {
            if (!this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
            {
                return null;
            }

            var alignmentSettings = this.PriceMyLoanUser.BrokerDB.TpoLoanNavigationHeaderLinkAlignmentSettings;

            string selectedPageName = null;
            if (hasLegacyClosingCostUrl)
            {
                selectedPageName = LegacyClosingCostTabName;
            }
            else
            {
                selectedPageName = navigationConfigItems.FirstOrDefault(kvp => this.IsItemSelected(kvp.Value)).Key;
            }

            var selectedAlignment = alignmentSettings[selectedPageName];

            return new
            {
                LeftAligned = selectedAlignment == TpoNavigationLinkAlignment.Left,
                CenterAligned = selectedAlignment == TpoNavigationLinkAlignment.Center,
                RightAligned = selectedAlignment == TpoNavigationLinkAlignment.Right
            };
        }

        /// <summary>
        /// Generates a list of generic framework vendor links to display on the page.
        /// </summary>
        /// <param name="brokerId">
        /// The broker ID for the loan.
        /// </param>
        /// <param name="loanId">
        /// The ID of the loan.
        /// </param>
        /// <param name="location">
        /// The location of the page to render generic framework links or null if the
        /// header should not render generic framework links.
        /// </param>
        /// <returns>
        /// A list of complex types containing information for the generic framework vendor links.
        /// </returns>
        private IEnumerable<object> GenerateGenericFrameworkVendorLinks(Guid brokerId, Guid loanId, LinkLocation? location)
        {
            if (!this.PriceMyLoanUser.EnableNewTpoLoanNavigation || !location.HasValue)
            {
                return Enumerable.Empty<object>();
            }

            var vendorLinks = from vendor in GenericFrameworkVendor.LoadVendors(brokerId)
                              where vendor.LaunchLinkConfig.IsDisplayLinkForLoanAndUser(loanId, location.Value)
                              select new
                              {
                                  DisplayAsButton = vendor.LaunchLinkConfig.IsLinkDisplayAsButton(location.Value),
                                  DisplayName = vendor.LaunchLinkConfig.LinkDisplayName(location.Value),
                                  ProviderID = vendor.ProviderID
                              };

            this.HasGenericFrameworkVendorLinks = vendorLinks.Any();
            return vendorLinks;
        }
    }
}
