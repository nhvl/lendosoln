namespace PriceMyLoan.UI.Main
{
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using MeridianLink.CommonControls;
    using PriceMyLoan.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Web.UI;

    public partial  class Agents : BaseUserControl, ITabUserControl
	{
        private Guid sPmlBrokerId = Guid.Empty;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            AgentsPage parentPage = Page as AgentsPage;
            if (parentPage != null)
            {
                parentPage.CheckForPermission();
            }

            SetRequiredValidatorMessage(LoanOfficer_AgentNameValidator);
            SetRequiredValidatorMessage(LoanOfficer_PhoneValidator);
            SetRequiredValidatorMessage(LoanOfficer_EmailAddrValidator);
            SetRequiredValidatorMessage(BrokerProcessor_EmailValidator);
            SetRequiredValidatorMessage(BrokerProcessor_PhoneValidator);

            MissingLOImage.ImageUrl = GetRequiredImageUrl();

            LoanOfficer_Zip.SmartZipcode(LoanOfficer_City, LoanOfficer_State);
            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;
            RegularExpressionValidator1.Text = ErrorMessages.InvalidEmailAddress;
            BrokerProcessor_EmailValidator2.ValidationExpression = ConstApp.EmailValidationExpression;
            BrokerProcessor_EmailValidator2.Text = ErrorMessages.InvalidEmailAddress;

            Page.ClientScript.RegisterHiddenField("_ClientID", this.ClientID);

            LoanOfficer_LicenseNumOfAgent.Attributes.Add("readonly", "readonly");
            LoanOfficer_LicenseNumOfCompany.Attributes.Add("readonly", "readonly");

            LoanOfficer_LicenseNumOfCompany.BackColor = Color.LightGray;
            LoanOfficer_LicenseNumOfAgent.BackColor = Color.LightGray;

            BrokerProcessor_LicenseNumOfAgent.Attributes.Add("readonly", "readonly");
            BrokerProcessor_LicenseNumOfAgent.BackColor = Color.LightGray;

            BPControlLinks.Visible = false == IsReadOnly;
            LOControlLinks.Visible = false == IsReadOnly;
            if (IsReadOnly)
            {
                MissingLOImage.Visible = false;
            }
        }

        protected bool m_hasRecentModification;

        private string embeddedLORole = "";
        private string embeddedLORoleDesc = "";
        private string m_sLOPickerUrl;
        private string m_sBpPickerUrl;

        protected string EmbeddedLORole
        {
            get { return this.embeddedLORole; }
        }

        protected string EmbeddedLORoleDesc
        {
            get { return this.embeddedLORoleDesc; }
        }

        protected string LoPickerUrl
        {
            get { return m_sLOPickerUrl; }
        }

        protected string BpPickerUrl
        {
            get { return m_sBpPickerUrl; }
        }

        protected bool IsEmbeddedPML 
        {
            get { return PriceMyLoanConstants.IsEmbeddedPML; }
        }
        public string TabID 
        { 
            get { return "AGENTS"; }
        }

        private void InitRolePickers(CPageData dataLoan)
        {
            if (!this.IsReadOnly)
            {
                string loRoleDesc;
                Guid loRoleID;
                string loRole;

                string processorRoleDesc;
                Guid processorRoleID;
                string processorRole;

                if (dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent)
                {
                    loRoleDesc = "Loan Officer";
                    loRoleID = CEmployeeFields.s_LoanRepRoleId;
                    loRole = ConstApp.ROLE_LOAN_OFFICER;

                    processorRoleDesc = "Processor";
                    processorRoleID = CEmployeeFields.s_BrokerProcessorId;
                    processorRole = ConstApp.ROLE_BROKERPROCESSOR;
                }
                else
                {
                    loRoleDesc = "Secondary";
                    loRoleID = CEmployeeFields.s_ExternalSecondaryId;
                    loRole = ConstApp.ROLE_EXTERNAL_SECONDARY;

                    processorRoleDesc = "Post-Closer";
                    processorRoleID = CEmployeeFields.s_ExternalPostCloserId;
                    processorRole = ConstApp.ROLE_EXTERNAL_POST_CLOSER;
                }

                PmlEmployeeRoleControlValues loValues = new PmlEmployeeRoleControlValues()
                {
                    LoanId = RequestHelper.LoanID,
                    OwnerId = PriceMyLoanUser.UserId,
                    RoleDesc = loRoleDesc,
                    RoleId = loRoleID,
                    ShowMode = "fullsearch",
                    Role = loRole,
                    SearchFilter = "",
                    EmployeeId = Guid.Empty
                };

                if (IsEmbeddedPML)
                {
                    loValues.ShowMode = "full";
                }

                m_sLOPickerUrl = Page.ResolveClientUrl("~/main/RoleSearch.aspx?k=" + PmlEmployeeRoleControlValues.Save(loValues, PriceMyLoanConstants.CookieExpireMinutes));

                if (false == IsEmbeddedPML)
                {
                    loValues.RoleDesc = processorRoleDesc;
                    loValues.Role = processorRole;
                    loValues.RoleId = processorRoleID;
                    m_sBpPickerUrl = Page.ResolveClientUrl("~/main/RoleSearch.aspx?k=" + PmlEmployeeRoleControlValues.Save(loValues, PriceMyLoanConstants.CookieExpireMinutes));

                }
            }
        }

        public void LoadData() 
        {
            StandalonePMLScripts.Visible = !IsEmbeddedPML && !IsReadOnly;
            EmbeddedPmlScripts.Visible = IsEmbeddedPML;

            // 8/2/2010 dd - Detect if any loan modification in past X minutes and display warning to user.
            List<string> userNames = CPageData.GetRecentModificationUserName(LoanID, PriceMyLoanUser);
            m_hasRecentModification = userNames.Count > 0;

            // 5/16/2005 dd - If user has AE role then we don't make Name, Phone & Email fields required.
            // 5/9/2006 dd - OPM 4832 - We are allow "B" user to see this steps.

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Agents));
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;

            bool isCorrespondentLoan = dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent;

            CEmployeeFields lo = !isCorrespondentLoan ?
                dataLoan.sEmployeeLoanRep : dataLoan.sEmployeeExternalSecondary;

            if (lo.IsValid)
            {
                LoId.Value = lo.EmployeeId.ToString();
            }
            else
            {
                LoId.Value = Guid.Empty.ToString();
            }

            this.InitRolePickers(dataLoan);

            if (isCorrespondentLoan)
            {
                var loLabels = new List<EncodedLiteral>()
                {
                    this.LoDesc,
                    this.LoDescAssign,
                    this.LoDescCellPhone,
                    this.LoDescEmail,
                    this.LoDescFax,
                    this.LoDescPager,
                    this.LoDescPhone
                };

                string loDesc = "Secondary";

                if (PriceMyLoanConstants.IsEmbeddedPML)
                {
                    loDesc += " (External)";
                }

                loLabels.ForEach(l => l.Text = loDesc);

                var procLabels = new List<EncodedLiteral>()
                {
                    this.ProcDesc,
                    this.ProcDescAssign,
                    this.ProcDescCellPhone,
                    this.ProcDescEmail,
                    this.ProcDescFax,
                    this.ProcDescPager,
                    this.ProcDescPhone
                };

                string procDesc = "Post-Closer";

                if (PriceMyLoanConstants.IsEmbeddedPML)
                {
                    procDesc += " (External)";
                }

                procLabels.ForEach(l => l.Text = procDesc);
            }

            if (IsEmbeddedPML)
            {
                this.embeddedLORole = isCorrespondentLoan ? "ExternalSecondary" : "Agent";
                this.embeddedLORoleDesc = isCorrespondentLoan ? "Secondary+(External)" : "Loan+Officer";

                if (isCorrespondentLoan)
                {
                    this.btnAssign.Value = "Assign to Secondary (External)...";
                }

                LoanOfficer_AgentNameValidator.Enabled = false;
                LoanOfficer_PhoneValidator.Enabled = false;
                LoanOfficer_EmailAddrValidator.Enabled = false;
                BrokerProcessor_EmailValidator.Enabled = false;
                BrokerProcessor_PhoneValidator.Enabled = false;    
                BPFieldSection.Visible = false;

                var loAgentT = isCorrespondentLoan ? E_AgentRoleT.ExternalSecondary : E_AgentRoleT.LoanOfficer;
                CAgentFields agent = dataLoan.GetAgentOfRole(loAgentT, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                LoanOfficer_AgentName.Text = agent.AgentName;
                AgentLicenseInfoList = agent.LicenseInfoList;
                LoanOfficer_LicenseNumOfAgent.Text = CAgentFields.GetLendingLicenseByState(dataLoan.sSpStatePe, AgentLicenseInfoList);
                LoanOfficer_Phone.Text = agent.Phone;
                LoanOfficer_FaxNum.Text = agent.FaxNum;
                LoanOfficer_CellPhone.Text = agent.CellPhone;
                LoanOfficer_PagerNum.Text = agent.PagerNum;
                LoanOfficer_EmailAddr.Text = agent.EmailAddr;

                LoanOfficer_Notes.Text = agent.Notes;
                LoanOfficer_CompanyName.Text = agent.CompanyName;
                CompanyLicenseInfoList = agent.CompanyLicenseInfoList;
                LoanOfficer_LicenseNumOfCompany.Text = CAgentFields.GetLendingLicenseByState(dataLoan.sSpStatePe, CompanyLicenseInfoList);
                LoanOfficer_StreetAddr.Text = agent.StreetAddr;
                LoanOfficer_City.Text = agent.City;
                LoanOfficer_State.Value = agent.State;
                LoanOfficer_Zip.Text = agent.Zip;
                LoanOfficer_PhoneOfCompany.Text = agent.PhoneOfCompany;
                LoanOfficer_FaxOfCompany.Text = agent.FaxOfCompany;

                CommissionPointOfLoanAmount.Value = agent.CommissionPointOfLoanAmount_rep;
                CommissionMinBase.Value = agent.CommissionMinBase_rep;
                CommissionPointOfGrossProfit.Value = agent.CommissionPointOfGrossProfit_rep;
                CompanyLoanOriginatorIdentifier.Value = agent.CompanyLoanOriginatorIdentifier;
                LoanOriginatorIdentifier.Value = agent.LoanOriginatorIdentifier;
            }
            else
            {
                if (lo.IsValid)
                {
                    var loAgentT = isCorrespondentLoan ? E_AgentRoleT.ExternalSecondary : E_AgentRoleT.LoanOfficer;
                    CAgentFields agent = dataLoan.GetAgentOfRole(loAgentT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    
                    LoanOfficer_AgentName2.Text = lo.FullName + "&nbsp;&nbsp;&nbsp;";
                    LoanOfficer_LicenseNumOfAgent.Text = agent.LicenseNumOfAgent;
                    LoanOfficer_Phone.Text = agent.Phone;
                    LoanOfficer_FaxNum.Text = agent.FaxNum;
                    LoanOfficer_CellPhone.Text = agent.CellPhone;
                    LoanOfficer_PagerNum.Text = agent.PagerNum;
                    LoanOfficer_EmailAddr.Text = agent.EmailAddr;

                    CommissionPointOfLoanAmount.Value = agent.CommissionPointOfLoanAmount_rep;
                    CommissionMinBase.Value = agent.CommissionMinBase_rep;
                    CommissionPointOfGrossProfit.Value = agent.CommissionPointOfGrossProfit_rep;
                }              

                CEmployeeFields processor = !isCorrespondentLoan ? 
                    dataLoan.sEmployeeBrokerProcessor : dataLoan.sEmployeeExternalPostCloser;

                if (processor.IsValid)
                {
                    var procAgentT = isCorrespondentLoan ? E_AgentRoleT.ExternalPostCloser : E_AgentRoleT.BrokerProcessor;
                    CAgentFields agent = dataLoan.GetAgentOfRole(procAgentT, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                    BpId.Value = processor.EmployeeId.ToString();
                    BrokerProcessor_FullName.Text = processor.FullName + "&nbsp;&nbsp;&nbsp";
                    BrokerProcessor_CellPhone.Text = agent.CellPhone;
                    BrokerProcessor_EmailAddr.Text = agent.EmailAddr;
                    BrokerProcessor_FaxNum.Text = agent.FaxNum;
                    BrokerProcessor_LicenseNumOfAgent.Text = agent.LicenseNumOfAgent;
                    BrokerProcessor_PagerNum.Text = agent.PagerNum;
                    BrokerProcessor_Phone.Text = agent.Phone;
                }
                else
                {
                    BpId.Value = Guid.Empty.ToString();
                }

                ViewState["loid"] = LoId.Value;
                ViewState["bpid"] = BpId.Value;
            }
        }

        public void SaveData() 
        {
            LoanAccessInfo info = new LoanAccessInfo(RequestHelper.LoanID);
            sPmlBrokerId = info.sPmlBrokerId; 

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Agents));
            dataLoan.TemporaryDidUserMakeChanges = Request["IsUpdate"] == "1";
            dataLoan.InitSave(sFileVersion);

            bool isCorrespondentLoan = dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent;

            if (IsEmbeddedPML)
            {
                var loAgentT = isCorrespondentLoan ? E_AgentRoleT.ExternalSecondary : E_AgentRoleT.LoanOfficer;
                CAgentFields agent = dataLoan.GetAgentOfRole(loAgentT, E_ReturnOptionIfNotExist.CreateNew);

                agent.AgentName = LoanOfficer_AgentName.Text;
                agent.LicenseInfoList = AgentLicenseInfoList;               
                agent.Phone = LoanOfficer_Phone.Text;
                agent.FaxNum = LoanOfficer_FaxNum.Text;
                agent.CellPhone = LoanOfficer_CellPhone.Text;
                agent.PagerNum = LoanOfficer_PagerNum.Text;
                agent.EmailAddr = LoanOfficer_EmailAddr.Text;

                agent.CompanyName = LoanOfficer_CompanyName.Text;
                agent.CompanyLicenseInfoList = CompanyLicenseInfoList;
                agent.StreetAddr = LoanOfficer_StreetAddr.Text;
                agent.City = LoanOfficer_City.Text;
                agent.State = LoanOfficer_State.Value;
                agent.Zip = LoanOfficer_Zip.Text;
                agent.PhoneOfCompany = LoanOfficer_PhoneOfCompany.Text;
                agent.FaxOfCompany = LoanOfficer_FaxOfCompany.Text;
                agent.Notes = LoanOfficer_Notes.Text;               

                agent.EmployeeId =  CommonFunctions.ToGuid_Safe(LoId.Value);

                if (!isCorrespondentLoan && dataLoan.sEmployeeLoanRepId != agent.EmployeeId)
                {
                    dataLoan.sEmployeeLoanRepId = agent.EmployeeId;
                    this.AssignBranch(agent.EmployeeId, dataLoan);
                }
                else if (isCorrespondentLoan && dataLoan.sEmployeeExternalSecondaryId != agent.EmployeeId)
                {
                    dataLoan.sEmployeeExternalSecondaryId = agent.EmployeeId;
                    this.AssignBranch(agent.EmployeeId, dataLoan);
                }

                E_AgentRoleT e_ExistingRoleWithCommission = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(agent.EmployeeId, LoanID);
                bool bCopyCommissionToAgent = (e_ExistingRoleWithCommission == E_AgentRoleT.Other || e_ExistingRoleWithCommission == E_AgentRoleT.LoanOfficer);
                if (bCopyCommissionToAgent)
                {
                    agent.CommissionPointOfLoanAmount_rep = CommissionPointOfLoanAmount.Value;
                    agent.CommissionMinBase_rep = CommissionMinBase.Value;
                    agent.CommissionPointOfGrossProfit_rep = CommissionPointOfGrossProfit.Value;
                }
                else
                {
                    //set to 0. else it retains the old values
                    agent.CommissionMinBase = 0;
                    agent.CommissionPointOfLoanAmount = 0;
                    agent.CommissionPointOfGrossProfit = 0;
                }
                agent.LoanOriginatorIdentifier = LoanOriginatorIdentifier.Value;
                agent.CompanyLoanOriginatorIdentifier = CompanyLoanOriginatorIdentifier.Value;
                agent.Update();
                dataLoan.Save();
            }
            else
            {
                Guid existingLoId = new Guid(ViewState["loid"].ToString());                
                Guid existingBpId = new Guid(ViewState["bpid"].ToString());
                Guid newLoId = new Guid(LoId.Value);
                Guid newBpId = new Guid(BpId.Value);

                var loAgentT = isCorrespondentLoan ? E_AgentRoleT.ExternalSecondary : E_AgentRoleT.LoanOfficer;
                CAgentFields agent = dataLoan.GetAgentOfRole(loAgentT, E_ReturnOptionIfNotExist.CreateNew);
                var procAgentT = isCorrespondentLoan ? E_AgentRoleT.ExternalPostCloser : E_AgentRoleT.BrokerProcessor;
                CAgentFields bp = dataLoan.GetAgentOfRole(procAgentT, E_ReturnOptionIfNotExist.CreateNew);

                if (!isCorrespondentLoan)
                {
                    if (existingBpId != newBpId)
                    {
                        dataLoan.sEmployeeBrokerProcessorId = newBpId;
                        SetAgentData(bp, newBpId);
                    }

                    if (existingLoId != newLoId)
                    {
                        dataLoan.sEmployeeLoanRepId = newLoId;
                        SetAgentData(agent, newLoId);
                        this.AssignBranch(newLoId, dataLoan);
                    }
                }
                else
                {
                    if (existingBpId != newBpId)
                    {
                        dataLoan.sEmployeeExternalPostCloserId = newBpId;
                        SetAgentData(bp, newBpId);
                    }

                    if (existingLoId != newLoId)
                    {
                        dataLoan.sEmployeeExternalSecondaryId = newLoId;
                        SetAgentData(agent, newLoId);
                        this.AssignBranch(newLoId, dataLoan);
                    }
                }

                //only update existing agents if they are not being cleared 
                //fields should be empty but no harm in ignoring them
                if (newLoId != Guid.Empty)
                {
                    agent.Phone = LoanOfficer_Phone.Text;
                    agent.PagerNum = LoanOfficer_PagerNum.Text;
                    agent.FaxNum = LoanOfficer_FaxNum.Text;
                    agent.EmailAddr = LoanOfficer_EmailAddr.Text;
                    agent.CellPhone = LoanOfficer_CellPhone.Text;
                }
                if (newBpId != Guid.Empty)
                {
                    bp.Phone = BrokerProcessor_Phone.Text;
                    bp.PagerNum = BrokerProcessor_PagerNum.Text;
                    bp.FaxNum = BrokerProcessor_FaxNum.Text;
                    bp.EmailAddr = BrokerProcessor_EmailAddr.Text;
                    bp.CellPhone = BrokerProcessor_CellPhone.Text;
                }

                if (newBpId == Guid.Empty)
                {
                    //redoing this, as im not aware of another way to get this to trigger workflow. The delete agent method bypasses workflow. 
                    if (!bp.IsNewRecord)
                    {
                        DataSet ds = dataLoan.sAgentDataSet;
                        DataTable table = ds.Tables["AgentXmlContent"];
                        for (var i = 0; i < table.Rows.Count; i++)
                        {
                            DataRow row = table.Rows[i];
                            if (row["RecordId"].ToString() == bp.RecordId.ToString())
                            {
                                dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(row, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                                table.Rows.Remove(row);
                                dataLoan.sAgentDataSet = ds;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    bp.Update();
                }
                agent.Update();
                dataLoan.Save();
            }
        }

        private void AssignBranch(Guid employeeID, CPageData dataLoan)
        {
            if (employeeID == Guid.Empty)
            {
                return;
            }

            Guid branchToAssign;

            var emp = new EmployeeDB(employeeID, this.BrokerID);
            emp.Retrieve();

            if (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                if (dataLoan.sCorrespondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                {
                    branchToAssign = emp.MiniCorrespondentBranchID;
                }
                else
                {
                    branchToAssign = emp.CorrespondentBranchID;
                }
            }
            else
            {
                branchToAssign = emp.BranchID;
            }

            if (branchToAssign != Guid.Empty)
            {
                dataLoan.AssignBranch(branchToAssign);
            }
        }

        private void SetAgentData(CAgentFields agent, Guid employeeId)
        {
            agent.ClearExistingData();
            
            if (employeeId == Guid.Empty)
            {
                //no need to look up
                return;
            }
            CommonFunctions.CopyEmployeeInfoToAgent(CurrentBroker.BrokerID, agent, employeeId);
            return;
        }
        LicenseInfoList AgentLicenseInfoList
        {
            get 
            {
                LicenseInfoList agentLicenseInfoList = LicenseInfoList.ToObject(Server.HtmlDecode(fieldAgentLicenseXml.Value));
                return agentLicenseInfoList;
            }
            set 
            {
                fieldAgentLicenseXml.Value = Server.HtmlEncode(value.ToString());
            }
        }
        LicenseInfoList CompanyLicenseInfoList
        {
            get
            {
                LicenseInfoList agentLicenseInfoList = LicenseInfoList.ToObject(Server.HtmlDecode(fieldCompanyLicenseXml.Value));                
                return agentLicenseInfoList;
            }
            set
            {
                fieldCompanyLicenseXml.Value = Server.HtmlEncode(value.ToString());
            }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            /*
            if (IsPostBack)
            {
                ViewState["m_sAgentLicenseInfoList"] = m_sAgentLicenseInfoList.ToString();
                ViewState["m_sCompanyLicenseInfoList"] = m_sCompanyLicenseInfoList.ToString();
            }
            else
            {
                m_sAgentLicenseInfoList = new LicenseInfoList();
                m_sAgentLicenseInfoList.Retrieve(SafeConvert.ToString(ViewState["m_sAgentLicenseInfoList"]));

                m_sCompanyLicenseInfoList = new LicenseInfoList();
                m_sAgentLicenseInfoList.Retrieve(SafeConvert.ToString(ViewState["m_sCompanyLicenseInfoList"]));
            }*/
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
