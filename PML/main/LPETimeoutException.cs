/// Author: David Dao

using System;
using LendersOffice.Common;
using DataAccess;

namespace PriceMyLoan.UI.Main
{
	public class LPETimeoutException : CBaseException
	{
        public override string EmailSubjectCode 
        {
            get { return "LPE_TIMEOUT"; }
        }
		public LPETimeoutException(string devMessages) : base(JsMessages.LpeResultsTimeout, devMessages)
		{
		}
	}
}
