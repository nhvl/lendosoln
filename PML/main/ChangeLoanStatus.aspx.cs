﻿// <copyright file="ChangeLoanStatus.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   03/18/2016
// </summary>
namespace PriceMyLoan.main
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    using global::DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Code Back for TPO portal Change Loan Status dialog.
    /// </summary>
    public partial class ChangeLoanStatus : PriceMyLoan.UI.BasePage
    {
        /// <summary>
        /// Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls Page_Init.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.EnableJquery = false;
        }

        /// <summary>
        /// Page_Loan function.
        /// </summary>
        /// <param name="sender">Control that calls Page_Load.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(ChangeLoanStatus));
            dataLoan.InitLoad();
            
            this.sStatusT.Text = dataLoan.sStatusT_rep;

            if (RequestHelper.GetBool("hideHeader"))
            {
                this.HeaderInfo.Visible = false;
            }
            else
            {
                this.sLNm.Text = dataLoan.sLNm;
                this.aBNm.Text = dataLoan.GetAppData(0).aBNm;
                this.sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
                this.sSpAddr.Text = dataLoan.sSpAddr_SingleLine;
            }

            this.BindStatusList(dataLoan);
        }

        /// <summary>
        /// Sets the value and visibility of the items in the "New Loan Status" radio button list.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan file to submit into a new status.
        /// </param>
        private void BindStatusList(CPageData dataLoan)
        {
            IEnumerable<ListItem> orderedStatuses;
            if (Tools.IsStatusLead(dataLoan.sStatusT))
            {
                orderedStatuses = new[] { E_sStatusT.Lead_New, E_sStatusT.Lead_Canceled, E_sStatusT.Lead_Declined, E_sStatusT.Lead_Other }
                    .Where(status => status != dataLoan.sStatusT) // remove current status as option.
                    .Select(status => new ListItem(CPageBase.sStatusT_map_rep(status), status.ToString("D")));
            }
            else
            {
                HashSet<E_sStatusT> enabledStatuses = this.PriceMyLoanUser.BrokerDB.GetEnabledStatusesByChannelAndProcessType(
                dataLoan.sBranchChannelT,
                dataLoan.sCorrespondentProcessT);

                orderedStatuses = from status in enabledStatuses
                                  where dataLoan.LoanFileWorkflowRules.CanChangeLoanStatus(status) &&
                                    CPageBase.s_OrderByStatusT[status] > CPageBase.s_OrderByStatusT[dataLoan.sStatusT]
                                  orderby CPageBase.s_OrderByStatusT[status]
                                  select new ListItem(CPageBase.sStatusT_map_rep(status), status.ToString("D"));
            }

            if (orderedStatuses.Any())
            {
                this.RadioList.Items.AddRange(orderedStatuses.ToArray());
            }
            else
            {
                var contact = this.PriceMyLoanUser.PortalMode == E_PortalMode.Retail ? "an administrator" : "your account executive";
                this.statusErrMsg.InnerText = $"There are no loan statuses available. Please contact {contact} for more information.";
            }
        }
    }
}