using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace PriceMyLoan.UI.Main
{
    public partial class QualifiedLoanProgramPrintFrame : PriceMyLoan.UI.BasePage
	{
        protected string m_frmMenuUrl = "";
        protected string m_frmBodyUrl = "";

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }
        
        protected bool IsDisplayDetailCert { get; private set; } = false; // opm 458165

		protected void PageLoad(object sender, System.EventArgs e)
		{
            this.RegisterJsScript("utilities.js");
            IsDisplayDetailCert = (Page.User as PriceMyLoan.Security.PriceMyLoanPrincipal)?.CanViewDualCertificate == true;

			// Put user code to initialize the page here
            Guid sLId = RequestHelper.LoanID;
            string productId = Server.UrlEncode(RequestHelper.GetSafeQueryString("productid"));
            string lienQualifyModeT = RequestHelper.GetSafeQueryString("lienqualifymodet");
            string version = RequestHelper.GetSafeQueryString("version");
            string uniqueChecksum = RequestHelper.GetSafeQueryString("uniquechecksum");

            string FirstLienLpId = RequestHelper.GetSafeQueryString("FirstLienLpId"); 
            string FirstLienNoteRate = RequestHelper.GetSafeQueryString("FirstLienNoteRate"); 
            string FirstLienPoint = RequestHelper.GetSafeQueryString("FirstLienPoint"); 
            string FirstLienMPmt = RequestHelper.GetSafeQueryString("FirstLienMPmt");
            Guid PinStateId = RequestHelper.GetGuid("PinStateId", Guid.Empty);

            string productDTI = RequestHelper.GetSafeQueryString("productDTI");
            string productRate = RequestHelper.GetSafeQueryString("productRate");
            string productPoint = RequestHelper.GetSafeQueryString("productPoint");
            string productRawId = RequestHelper.GetSafeQueryString("productRawId");

            m_frmMenuUrl = "QualifiedLoanProgramPrintMenu.aspx?loanid=" + sLId + "&productid=" + productId + "&lienqualifymodet="
                + lienQualifyModeT + "&version=" + version + "&uniquechecksum=" + uniqueChecksum + "&FirstLienLpId=" + FirstLienLpId
                + "&FirstLienNoteRate=" + FirstLienNoteRate + "&FirstLienPoint=" + FirstLienPoint + "&FirstLienMPmt=" + FirstLienMPmt + "&PinStateId=" + PinStateId + "&B=1"
                + (IsDisplayDetailCert ? "&detailCert=1" : string.Empty);

            m_frmBodyUrl = "CertificateWaitPage.aspx?loanid=" + sLId + "&productid=" + productId + "&lienqualifymodet="
                + lienQualifyModeT + "&version=" + version + "&uniquechecksum=" + uniqueChecksum + "&FirstLienLpId=" + FirstLienLpId
                + "&FirstLienNoteRate=" + FirstLienNoteRate + "&FirstLienPoint=" + FirstLienPoint + "&FirstLienMPmt=" + FirstLienMPmt + "&PinStateId=" + PinStateId
                + "&productRate=" + productRate + "&productDTI=" + productDTI + "&productPoint=" + productPoint + "&productRawId=" + productRawId; 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
