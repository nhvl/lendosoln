﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="QuickPricerResult.ascx.cs" Inherits="PriceMyLoan.main.QuickPricerResult" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<div class="QuickPricerResultPanel" style="margin:0px">
<fieldset class="QuickPricerResultFieldSet">
  <legend class="LegendSectionHeader">Result</legend>
  <br />
  <asp:PlaceHolder ID="m_disclaimerPlaceHolder" runat="server"></asp:PlaceHolder>
  <br />
  <br />
  <div id="ResultHtml" style="padding-left:45px"></div>
</fieldset>
</div>
