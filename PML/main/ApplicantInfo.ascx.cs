using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using PriceMyLoan.DataAccess;
using PriceMyLoan.UI;
using PriceMyLoan.Common;
using PriceMyLoan.Security;
using PriceMyLoan.main;
using MeridianLink.CommonControls;
using LendersOffice.Constants;
namespace PriceMyLoan.UI.Main
{
    public partial  class ApplicantInfo : BaseUserControl, ITabUserControl
    {

        protected bool aIsCreditReportOnFile;
        protected Guid m_applicationID = Guid.Empty;
        protected bool m_displayCreditReportButton = false;
        protected bool m_displayPublicRecordsEdit = false;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            //opm 27866 fs 03/02/09
            // 3/20/2009 dd - Enable SSN masking for all PML Lender.
            aBSsn.IsMaskEnabled = true;
            aCSsn.IsMaskEnabled = true;
            
            SetRequiredValidatorMessage(aBFirstNmValidator);
            SetRequiredValidatorMessage(aBLastNmValidator);
            SetRequiredValidatorMessage(aBSsnValidator);
            SetRequiredValidatorMessage(aCFirstNmValidator);
            SetRequiredValidatorMessage(aCLastNmValidator);
            SetRequiredValidatorMessage(aCSsnValidator);
            SetRequiredValidatorMessage(aBEmailValidator);
            SetRequiredValidatorMessage(aCEmailValidator);

            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);

            var borrCitizenshipValues = dataApp.GetApplicableCitizenshipValues(E_BorrowerModeT.Borrower);
            Tools.Bind_aProdCitizenT(aProdBCitizenT, borrCitizenshipValues);

            aBExperianScorePe.Attributes.Add("readonly", "readonly");
            aBTransUnionScorePe.Attributes.Add("readonly", "readonly");
            aBEquifaxScorePe.Attributes.Add("readonly", "readonly");

            aCExperianScorePe.Attributes.Add("readonly", "readonly");
            aCTransUnionScorePe.Attributes.Add("readonly", "readonly");
            aCEquifaxScorePe.Attributes.Add("readonly", "readonly");          
        }

        private void CheckExistingReport(Guid applicationID) 
        {

            SqlParameter[] parameters = { new SqlParameter("@ApplicationID", applicationID) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "RetrieveCreditReportWithAccessLevel", parameters)) 
            {
                if (reader.Read()) 
                {
                    // 8/12/2005 dd - OPM #2382 Allow user to view credit report from PML.
                    // 2/12/2007 mf - OPM 10017. We now have a setting for the connection that determines if
                    // the reports from lender-defined CRA can be viewed in PML.  Setting is null when
                    // non-lender-defined CRA was used.
                    m_displayCreditReportButton = true;

                    if (!(reader["AllowPmlUserToViewReports"] is DBNull))
                    {
                        m_displayCreditReportButton = (bool) reader["AllowPmlUserToViewReports"];
                    }

                    ShowCreditScripts.Visible = m_displayCreditReportButton;
                }
            }
        }
        public string TabID 
        { 
            get { return "APPINFO"; }
        }

        public void LoadData() 
        {
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.InitLoad();
            
            if (dataLoan.sPml1stVisitApplicantStepD_rep == "") 
            {
                CPageData trackData = new CPmlUsagePatternData(LoanID);
                trackData.InitSave(ConstAppDavid.SkipVersionCheck);
                trackData.sPml1stVisitApplicantStepD = CDateTime.Create(DateTime.Now);
                trackData.Save();

                dataLoan = new CApplicantInfoData(LoanID); // 7/29/2010 dd - Need to load again to get the latest file version.
                dataLoan.InitLoad();

            }

            sFileVersion = dataLoan.sFileVersion;

            CAppData dataApp = dataLoan.GetAppData(0);
            m_applicationID = dataApp.aAppId;
            CheckExistingReport(dataApp.aAppId);

            aBExperianScorePe.Text                 = dataApp.aBExperianScorePe_rep;
            aBTransUnionScorePe.Text               = dataApp.aBTransUnionScorePe_rep;
            aBEquifaxScorePe.Text                  = dataApp.aBEquifaxScorePe_rep;
            aCExperianScorePe.Text                 = dataApp.aCExperianScorePe_rep;
            aCTransUnionScorePe.Text               = dataApp.aCTransUnionScorePe_rep;
            aCEquifaxScorePe.Text                  = dataApp.aCEquifaxScorePe_rep;
            aBFirstNm.Text                         = dataApp.aBFirstNm;
            aBMidNm.Text                           = dataApp.aBMidNm;
            aBLastNm.Text                          = dataApp.aBLastNm;
            aBSuffix.Text                          = dataApp.aBSuffix;
            aBSsn.Text                             = dataApp.aBSsn;
            aCFirstNm.Text                         = dataApp.aCFirstNm;
            aCMidNm.Text                           = dataApp.aCMidNm;
            aCLastNm.Text                          = dataApp.aCLastNm;
            aCSuffix.Text                          = dataApp.aCSuffix;
            aCSsn.Text                             = dataApp.aCSsn;
            aBHasSpouse.Checked                    = dataApp.aBHasSpouse;
            sIsSelfEmployed.Checked                = dataLoan.sIsSelfEmployed;
            aIsBorrSpousePrimaryWageEarner.Checked = dataApp.aIsBorrSpousePrimaryWageEarner;

            aBEmail.Text                           = dataApp.aBEmail;   //opm 34190 fs 08/17/09
            aCEmail.Text                           = dataApp.aCEmail;   //opm 34190 fs 08/17/09

            sOpNegCfPe.Text                        = dataLoan.sOpNegCfPe_rep;
            sTransmOMonPmtPe.Text                  = dataLoan.sTransmOMonPmtPe_rep;

            aIsCreditReportOnFile = dataApp.aIsCreditReportOnFile;
//            sHas1stTimeBuyerPe.Checked             = dataLoan.sHas1stTimeBuyerPe;

            // 02/27/07 OPM 9023 - Show edit link if there is public record on file
            m_displayPublicRecordsEdit = dataApp.aPublicRecordCollection.CountRegular > 0;

            Tools.SetDropDownListValue(aProdBCitizenT, dataApp.aProdBCitizenT);

            main _parentPage = this.Page as main;
            if (null != _parentPage)
                _parentPage.UpdateLoanSummaryDisplay(dataLoan);
        }

        public void SaveData() 
        {
            CPageData dataLoan = new CApplicantInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBExperianScorePe_rep          = aBExperianScorePe.Text;
            dataApp.aBTransUnionScorePe_rep        = aBTransUnionScorePe.Text;
            dataApp.aBEquifaxScorePe_rep           = aBEquifaxScorePe.Text;
            dataApp.aCExperianScorePe_rep          = aCExperianScorePe.Text;
            dataApp.aCTransUnionScorePe_rep        = aCTransUnionScorePe.Text;
            dataApp.aCEquifaxScorePe_rep           = aCEquifaxScorePe.Text;

            dataApp.aBFirstNm                      = aBFirstNm.Text;
            dataApp.aBMidNm                        = aBMidNm.Text;
            dataApp.aBLastNm                       = aBLastNm.Text;
            dataApp.aBSuffix                       = aBSuffix.Text;
            if (aBSsn.IsUpdated)
            {
                dataApp.aBSsn                      = aBSsn.ClearText;
            }
            dataApp.aCFirstNm                      = aCFirstNm.Text;
            dataApp.aCMidNm                        = aCMidNm.Text;
            dataApp.aCLastNm                       = aCLastNm.Text;
            dataApp.aCSuffix                       = aCSuffix.Text;
            if (aCSsn.IsUpdated)
            {
                dataApp.aCSsn                      = aCSsn.ClearText;
            }

            dataLoan.sIsSelfEmployed               = sIsSelfEmployed.Checked;

            dataApp.aIsBorrSpousePrimaryWageEarner = aIsBorrSpousePrimaryWageEarner.Checked;
            if (aIsBorrSpousePrimaryWageEarner.Checked) //opm 32807 fs 06/24/09
            {
                dataApp.aCIsPrimaryWageEarner      = true;
            }
            else
            {
                dataApp.aBIsPrimaryWageEarner      = true;
            }

            dataLoan.sOpNegCfPe_rep                = sOpNegCfPe.Text;

            dataApp.aProdBCitizenT                 = (E_aProdCitizenT) Tools.GetDropDownListValue(aProdBCitizenT);

            dataApp.aBEmail                        = aBEmail.Text;   //opm 34190 fs 08/17/09
            dataApp.aCEmail                        = aCEmail.Text;   //opm 34190 fs 08/17/09

//            dataLoan.sHas1stTimeBuyerPe            = sHas1stTimeBuyerPe.Checked;

            dataLoan.InitNewLoanForPml();
            dataLoan.Save();
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
