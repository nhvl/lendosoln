<%@ Page language="c#" Codebehind="FannieDownloadOptions.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.FannieDownloadOptions" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Fannie Mae Download Options</title>
  </HEAD>
  <body style="margin-left:0px" scroll="auto">
	<script type="text/javascript">
<!--
var g_globallyUniqueIdentifier;
var g_pollingInterval = 10000;
var g_isDo = false;
var g_lenderInstitutionIdentifier = "";

function _init() {
  resize(500,450);
  f_getCasefileStatus();
  g_isDo = parent.LQBPopup.GetArguments().bIsDo;
  
  $j('#StatementLabel').text("Update this loan with the following information from " + (g_isDo ? "DO" : "DU") + ":");
}


function f_onImportClick() {
  var bIs1003 = <%= AspxTools.JQuery(Is1003) %>.prop('checked');
  var bIsDuFindings = <%= AspxTools.JQuery(IsDuFindings) %>.prop('checked');
  var bIsCreditReport = <%= AspxTools.JQuery(IsCreditReport) %>.prop('checked');
  if (!bIs1003 && !bIsDuFindings && !bIsCreditReport) {
    alert(<%= AspxTools.JsString(JsMessages.SelectAtLeastOneOptionFnmaDownload) %>);
    return;
  }
  
  g_globallyUniqueIdentifier = "";
  f_displayWait(true);
  f_importToLOImpl();
}
function f_onCancelClick() {

  var dialogArgs = parent.LQBPopup.GetArguments();
  var args = new Object();
  args["IsDo"] = dialogArgs.bIsDo ? "True" : "False";
  args["AutoLogin"] = dialogArgs.bHasAutoLogin ? "True" : "False";
  args["FannieMaeMORNETUserID"] = dialogArgs.sUserName;
  args["FannieMaeMORNETPassword"] = dialogArgs.sPassword;
    args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
  var result = gService.loanedit.call("LogoutDoDu", args);   

  parent.LQBPopup.Return({OK:false});      
}
function f_displayWait(bDisplay) {
  parent.LQBPopup.Resize(300, 220);
  $j('#MainPanel').toggle(bDisplay == false);
  $j('#ErrorPanel').toggle(bDisplay == false);
  $j('#WaitPanel').toggle(bDisplay);
  
  $j('#RetrieveDataLabel').text( "Retrieving data from " + (g_isDo ? "DO" : "DU"));
}
function f_displayErrorMessage(errMsg) {
  parent.LQBPopup.Resize(500, 500);
  $j('#MainPanel').show();  
  $j('#WaitPanel').hide();  
  $j('#ErrorPanel').show();  
  $j('#ErrorMessage').html(errMsg);
}

function f_displayComplete() {
  parent.LQBPopup.Resize(500, 150);

  $j('#MainPanel').hide();
  $j('#WaitPanel').hide();
  $j('#ErrorPanel').hide();  
  $j('#CompletePanel').show();

}
function f_close(bOk) {
  parent.LQBPopup.Return({OK:bOk});
  /*
  var dialogArgs = window.dialogArguments;
  dialogArgs.OK = bOk;
  self.close();
  */
}
function f_getCasefileStatus() {
  var dialogArgs = parent.LQBPopup.GetArguments();

  var args = {};
  args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
  args["IsDo"] = dialogArgs.bIsDo ? "True" : "False";
  args["AutoLogin"] = dialogArgs.bHasAutoLogin ? "True" : "False";  
  args["FannieMaeMORNETUserID"] = dialogArgs.sUserName;
  args["FannieMaeMORNETPassword"] = dialogArgs.sPassword;
  args["sDuCaseId"] = dialogArgs.sDuCaseId;  
  var result = gService.loanedit.call("GetAndSaveCasefileStatus", args);
  
  if (!result.error) {
    $j('#sDuCaseId').text( dialogArgs.sDuCaseId);
    $j('#LastUnderwritingDate').text( result.value["LastUnderwritingDate"]);
    $j('#UnderwritingRecommendationDescription').text(result.value["UnderwritingRecommendationDescription"]);
    $j('#UnderwritingStatusDescription').text(result.value["UnderwritingStatusDescription"]);
    $j('#UnderwritingSubmissionType').text(result.value["UnderwritingSubmissionType"]);
    $j('#DesktopOriginatorSubmissionStatus').text( result.value["DesktopOriginatorSubmissionStatus"]);
    $j('#ProductName').text( result.value["ProductName"]);
    $j('#UpdateDate').text( result.value["UpdateDate"]);
    $j('#AllCreditStatusDescription').text( result.value["AllCreditStatusDescription"]);
    g_lenderInstitutionIdentifier = result.value["LenderInstitutionIdentifier"];

    
  }
}
function f_importToLOImpl() {
  var dialogArgs = parent.LQBPopup.GetArguments();
  
  var args = {};
  args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
  args["IsDo"] = dialogArgs.bIsDo ? "True" : "False";
  args["AutoLogin"] = dialogArgs.bHasAutoLogin ? "True" : "False";  
  args["FannieMaeMORNETUserID"] = dialogArgs.sUserName;
  args["FannieMaeMORNETPassword"] = dialogArgs.sPassword;
  args["GloballyUniqueIdentifier"] = g_globallyUniqueIdentifier;
  args["sDuCaseId"] = dialogArgs.sDuCaseId;  
  args["IsImport1003"] = <%= AspxTools.JsGetElementById(Is1003) %>.checked ? 'True' : 'False';
  args["IsImportDuFindings"] = <%= AspxTools.JsGetElementById(IsDuFindings) %>.checked ? 'True' : 'False';
  args["IsImportCreditReport"] = <%= AspxTools.JsGetElementById(IsCreditReport) %>.checked ? 'True' : 'False';
  args["LenderInstitutionIdentifier"] = g_lenderInstitutionIdentifier;

  var result = gService.loanedit.call("ImportToLO", args);
  if (!result.error) {
    switch (result.value["Status"]) {
      case "ERROR":
        f_displayErrorMessage(result.value["ErrorMessage"]);
        break;
      case "PROCESSING":
        g_globallyUniqueIdentifier = result.value["GloballyUniqueIdentifier"];
        setTimeout(f_importToLOImpl, g_pollingInterval);
        break;
      case "DONE":
        f_displayComplete();
        break;
    } 
  } else {
      var errMsg = 'Error.';
      if (null != result.UserMessage)
        errMsg = result.UserMessage;
      f_displayErrorMessage(errMsg);
      return false;
  }
}

//-->
</script>
    <h4 class="page-header">Casefile Status Summary</h4>
    <form id="FannieDownloadOptions" method="post" runat="server">
    <table cellpadding="0" border="0" width="100%">
      <tbody id="MainPanel">
        <tr>
          <td>
            <table cellpadding="3" cellspacing="0" border="0">
              <tr>
                <td style="padding-left: 5px">Case ID</td>
                <td class="fieldLabel"><div id="sDuCaseId"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Last Underwriting Date</td>
                <td class="fieldLabel"><div id="LastUnderwritingDate"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Underwriting Recommendation</td>
                <td class="fieldLabel"><div id="UnderwritingRecommendationDescription"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Underwriting Status</td>
                <td class="fieldLabel"><div id="UnderwritingStatusDescription"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Underwriting Submission Type</td>
                <td class="fieldLabel"><div id="UnderwritingSubmissionType"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Desktop Originator Submission Status</td>
                <td class="fieldLabel"><div id="DesktopOriginatorSubmissionStatus"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Product Name</td>
                <td class="fieldLabel"><div id="ProductName"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Date of Last Change to the Loan</td>
                <td class="fieldLabel"><div id="UpdateDate"></div></td>
              </tr>
              <tr>
                <td style="padding-left: 5px">Status of Credit Reports</td>
                <td class="fieldLabel"><div id="AllCreditStatusDescription"></div></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><hr></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="fieldLabel" style="padding-left: 10px"><div id="StatementLabel"></div></td>
        </tr>
        <tr>
          <td class="fieldLabel" style="padding-left: 10px"><asp:CheckBox ID="Is1003" runat="server" Text="1003 Data" /></td>
        </tr>
        <tr>
          <td class="fieldLabel" style="padding-left: 10px"><asp:CheckBox ID="IsDuFindings" runat="server" Text="DU Findings" /></td>
        </tr>
        <tr>
          <td class="fieldLabel" style="padding-left: 10px"><asp:CheckBox ID="IsCreditReport" runat="server" Text="Credit Report" /></td>
        </tr>
        <tr>
          <td align="center"><input type="button" value="Update" onclick="f_onImportClick();" class="ButtonStyle">&nbsp;&nbsp; <input type="button" value="Do not update" onclick="f_onCancelClick();" class="ButtonStyle" style="width: 112px"></td>
        </tr>
      </tbody>
      <tbody id="WaitPanel" style="display: none">
        <tr>
          <td valign="middle" align="center" height="100" class="FieldLabel"><img src="../images/status.gif"><br>
            <div id='RetrieveDataLabel'></div>
          </td>
        </tr>
      </tbody>
      <tbody id="ErrorPanel" style="display: none">
        <tr><td><hr></td></tr>
        <tr><td style="color: red"><div id="ErrorMessage"></div></td></tr>
      </tbody>
      <tbody id="CompletePanel" style="display: none">
        <tr><td class="FieldLabel" align="center">Updated data successfully.</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <td align="center"><input type="button" value="Close" onclick="f_close(true);" class="ButtonStyle"> </td>
        </tr>
      </tbody>
    </table>
    </form>
	
  </body>
</HTML>
