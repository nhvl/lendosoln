<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="EmployeeRole.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.main.EmployeeRole" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>EmployeeRole</title>
  </HEAD>
    <body MS_POSITIONING="FlowLayout" scroll="no" bgcolor="gainsboro" style="margin:0px">
    <script language=javascript>

		<!--

		function _init()
		{
			if( document.getElementById("m_errorMessage") != null )
			{
				alert( document.getElementById("m_errorMessage").value );
			}

			if( document.getElementById("m_argsToWrite") != null )
			{
				var opts = document.getElementById("m_argsToWrite").value.split( "|" );
				var arg = {};

				for( var i = 0 ; i < opts.length; i++)
				{
				    var kvp = opts[i].split('=');
				    arg[kvp[0]] = eval(kvp[1]);
				}				
			}
			
			
			if( document.getElementById("m_commandToDo") != null )
			{
				if( document.getElementById("m_commandToDo").value == "Close" )
				{
					parent.LQBPopup.Return(arg);
				}
			}

      <%= AspxTools.JsGetElementById(m_SearchFilter) %>.focus();

			<% if( IsPostBack == false ) { %>
			
			resize( 570 , 450 );
			
			<% } %>
		}

		//-->

    </script>    
    <h4 class="page-header">Employees to select for role <%= AspxTools.HtmlString(RequestHelper.GetSafeQueryString("roledesc").ToLower()) %>:</h4>
    <form id="EmployeeRole" method="post" runat="server">
		<table cellspacing="2" cellpadding="0" width="100%" height="100%">
		<tr>
		<td height="0" style="padding: 0px;">
			<div style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 8px; BORDER-TOP: 2px groove; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: 2px groove; WIDTH: 100%; PADDING-TOP: 8px; BORDER-BOTTOM: 2px groove">
				<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td nowrap style="PADDING-RIGHT: 4px;color:black;">
					Search for:
				</td>
				<td nowrap>
					<asp:TextBox id="m_SearchFilter" runat="server" style="PADDING-LEFT: 4px" Width="169px" />
				</td>
				<td style="FONT: 11px arial; COLOR: dimgray">
					("s" for John Smith or Sam Cash, "b s" for Bob Smith)
				</td>
				</tr>
          <tr>
            <td style="padding-right: 4px; color: black;" nowrap>Employee Status:</td>
            <td nowrap colspan="2" style="color: black;">
              <asp:RadioButton ID="m_activeRB" runat="server" Text="Active" GroupName="status" Checked="True" />
              <asp:RadioButton ID="m_inactiveRB" runat="server" Text="Inactive" GroupName="status" />
              <asp:RadioButton ID="m_anyRB" runat="server" Text="Any Status" GroupName="status" />
            </td>
          </tr>
          <TR>
          <TD style="PADDING-RIGHT: 4px" noWrap></TD>
          <TD noWrap colSpan=2>
					<asp:Button id="m_Search" runat="server" Text="Search">
					</asp:Button></TD></TR>
				</table>
			</div>
		</td>
		</tr>
		<tr valign="top">
		<td height="100%">
			<div style="PADDING-RIGHT: 4px; OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%">
				<ml:CommonDataGrid id="m_Grid" runat="server" CellPadding="2" OnItemCommand="EmployeeAssignmentClick">
					<Columns>
						<asp:TemplateColumn SortExpression="FullName" HeaderText="Employee" />
						<asp:BoundColumn DataField="CompanyName" SortExpression="CompanyName" HeaderText="External Broker" />
					</Columns>
				</ml:CommonDataGrid>
				<asp:Panel id="m_TooMany" runat="server" style="PADDING-RIGHT: 50px; PADDING-LEFT: 50px; PADDING-BOTTOM: 50px; FONT: 11px arial; COLOR: tomato; PADDING-TOP: 50px; TEXT-ALIGN: justify" Visible="False">
					Too many employees 
      with this role. Please refine your search using the above filters. 
      <DIV style="MARGIN-TOP: 12px; WIDTH: 100%">We match against employee's 
      full name, e.g. "Jo", will find all the employees with a first or last 
      name that begins with "Jo". To search for employees by first *and* last 
      name, use two patterns separated by a space. For example: use "bo sm" to 
      return "Bob Smith", "Boaz Smalls", etc. </DIV>
				</asp:Panel>
				<asp:Panel id="m_Empty" runat="server" style="PADDING-RIGHT: 80px; PADDING-LEFT: 80px; PADDING-BOTTOM: 80px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 80px; TEXT-ALIGN: justify" Visible="False">
					No employees matching the specified criteria.  Nothing to show.
				</asp:Panel>
			</div>
		</td>
		</tr>
		<tr>
		<td height="0" align="center" style="PADDING-RIGHT: 8px;PADDING-LEFT: 8px;PADDING-BOTTOM: 8px;PADDING-TOP: 8px">
			<input type="button" value="Cancel" onclick="parent.LQBPopup.Hide();">
		</td>
		</tr>
		</table>
		<uc1:cModalDlg runat="server" id=CModalDlg1>
		</uc1:cModalDlg>
    </form>
  </body>
</HTML>
