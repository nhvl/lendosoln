﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InstallClientCertificate.aspx.cs" Inherits="PriceMyLoan.InstallClientCertificate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" style="width:100%; height:100%;" >
<head runat="server">
    <title>Install Client Certificate</title>
</head>
<body class="NoMargin">
<script type="text/javascript">
function _init() {
    resize(400, 200);
}
function f_close() {
    self.parent.LQBPopup.Return();
}

function f_getCertificate() {
    window.location = ('InstallClientCertificate.aspx?cmd=download&arg=' + ML.EncryptedString);
    window.setTimeout(f_close, 2000);
    return false;
}
function f_isValid() {
    if (<%= AspxTools.JsGetElementById(m_description) %>.value == '')
    {
        self.parent.simpleDialog.alert('Device name is required.');
        return false;
    }
    <%= AspxTools.JsGetElementById(m_continueBtn) %>.click();
    return true;
}
</script>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" onclick="f_close();"><i class="material-icons">&#xE5CD;</i></button>
        <h4 class="modal-title">Install Client Certificate</h4>
    </div>
    
        <div id="Panel0" runat="server">
            <div class="modal-body install-client-certificate-modal-body">
                <div class="margin-bottom"><label>Please specify a name to identify your device. (i.e: Work's laptop)</label></div>
                <div class="table">
                    <div>
                        <div>
                            <label class="text-grey">Name:</label>
                        </div>
                        <div>
                            <asp:TextBox ID="m_description" runat="server" width="150px"/> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="f_close();" class="btn btn-flat">Cancel</button>
                <button ID="m_continueBtn" runat="server" class="hidden">Continue</button>
                <button onclick="return f_isValid();" class="btn btn-flat">Continue</button>
            </div>
        </div>
        
        <div id="Panel1" runat="server" Visible="false">
            <div class="modal-body">
                <div class="margin-bottom"><label>The client certificate can be installed to authenticate a user and bypass the authentication code process. This should only be installed on a trusted machine. You will be required to enter the password below during installation.</label></div>
                <div class="table">
                    <div>
                        <div>
                            <label class="text-grey">Certificate Password:</label>
                        </div>
                        <div>
                            <ml:EncodedLiteral ID="m_certificatePassword" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="return f_getCertificate();" class="btn btn-flat">Get Certificate</button>
            </div>
        </div>
    
    </form>
</body>
</html>
