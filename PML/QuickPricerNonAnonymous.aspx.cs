﻿namespace PriceMyLoan
{
    using System;
    using System.Web.UI.HtmlControls;
    using LendersOffice.Common;
    using PriceMyLoan.Common;
    using PriceMyLoan.Security;

    // This quick pricer is meant for non anonymous qp applications only.
    public partial class QuickPricerNonAnonymous : PriceMyLoan.UI.BasePage
    {
        public override PriceMyLoanPrincipal CustomPrincipal
        {
            get
            {
                return this.User as PriceMyLoanPrincipal;
            }
        }

        protected bool m_allowGoToPipeline
        {
            get
            {
                return CustomPrincipal.IsAllowGoToPipeline;
            }
        }

        protected bool IsEmbeddedPML
        {
            get
            {
                return PriceMyLoanConstants.IsEmbeddedPML;
            }
        }

        protected string PmlUrl
        {
            get
            {
                if (this.PriceMyLoanUser.EnableNewTpoLoanNavigation)
                {
                    return this.VirtualRoot + "/webapp/Loan1003.aspx";
                }
                else if (PriceMyLoanUser.BrokerDB.IsNewPmlUIEnabled)
                {
                    return VirtualRoot + "/webapp/pml.aspx";
                }
                else
                {
                    return VirtualRoot + "/main/agents.aspx";
                }

            }
        }

        protected override bool IncludeMaterialDesignFiles
        {
            get { return false; }
        }

        protected override void OnInit(EventArgs e)
        {
            this.Init += new EventHandler(this.PageInit);
            base.OnInit(e);
        }
        void PageInit(object sender, EventArgs e)
        {
            this.RegisterService("main", "/QuickPricerNonAnonymousService.aspx");
            IncludeStyleSheet("~/css/quickpricer.css");
            RegisterJsScript("quickpricerresult.js");
            RegisterJsScript("LQBNestedFrameSupport.js");

            m_topLinkPanel.Visible = !IsEmbeddedPML && m_allowGoToPipeline;

            if (RequestHelper.GetBool("if"))
            {
                m_topLinkPanel.Visible = false;
            }

            SetRequiredValidatorMessage(CreateLoan_BorrFirstNameValidator);
            SetRequiredValidatorMessage(CreateLoan_BorrLastNameValidator);
            SetRequiredValidatorMessage(CreateLoan_BorrSsnValidator);
            CreateLoanTable.Visible = !IsEmbeddedPML;

            this.QuickPricer1.IsForAnonymousQp = false;
            this.QuickPricerResult1.IsForAnonymousQp = false;
        }

        protected void SetRequiredValidatorMessage(System.Web.UI.WebControls.BaseValidator v)
        {
            HtmlImage img = new HtmlImage();
            img.Src = ResolveUrl("~/images/error_pointer.gif");
            v.Controls.Add(img);
        }
    }
}
