namespace PriceMyLoan.UI
{
    using System;
    using System.Web.UI;
    using global::DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.UI.Themes;
    using PriceMyLoan.Common;
    using PriceMyLoan.Security;

    public partial class index : BasePage
	{
        protected override bool IncludePrincipalSpecificStyling => false;

        private Guid LenderPmlSiteID 
		{
			get { return RequestHelper.GetGuid("LenderPmlSiteID", Guid.Empty); }
		}

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("common.js");
            this.RegisterJsScript("login.js");

            var compatibilityNotification = ConstApp.GetPmlBrowserIncompatibleNotification(
                    Request.Browser.Browser,
                    Request.Browser.Version,
                    Request.UserAgent);

            this.RegisterJsGlobalVariables("CompatNotification", compatibilityNotification);
            this.RegisterJsGlobalVariables("LenderPmlSiteID", this.LenderPmlSiteID);
        }

        protected void PageLoad(object sender, EventArgs e)
		{
            Response.Headers.Add("IsLogin", "true");
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }

            // 02/13/08 mf. OPM 19614. Display error message if there is one.
            if (!IsPostBack && RequestHelper.GetSafeQueryString("errorcode") != null)
            {
                PriceMyLoanRequestHelper.SetRequestErrorMessage(RequestHelper.GetSafeQueryString("errorcode"), this.m_ErrMsg, this.m_ErrMsg2);
            }
            else
            {
                if (LenderPmlSiteID == Guid.Empty)
                {
                    Guid cx = RequestHelper.GetGuid("cx", Guid.Empty);
                    if (cx == new Guid("AC9640C6-0325-4683-9DB0-708EE103064F"))
                    {
                        Tools.LogInfo("Precompiling assemblies started.");
                        System.Web.HttpContext.Current.Server.ScriptTimeout = 600;
                        System.Diagnostics.Stopwatch el = System.Diagnostics.Stopwatch.StartNew();
                        var assemblies = new System.Reflection.Assembly[] { 
                            System.Reflection.Assembly.GetAssembly(typeof(CPageData)),
                            System.Reflection.Assembly.GetExecutingAssembly(), 
                        };
                        Tools.RunJITOn(assemblies);
                        Tools.LogInfo("Precompile assemblies took " + el.ElapsedMilliseconds + "ms.");
                        el.Stop();
                    }

                    Response.Redirect("~/SessionExpired.aspx");
                }
                else
                {
                    // 10/27/2009 dd - Verify the LenderPmlSiteId is valid.
                    if (BrokerDB.GetBrokerIdByBrokerPmlSiteId(LenderPmlSiteID) == Guid.Empty)
                    {
                        Response.Redirect("~/SessionExpired.aspx");
                    }

                }
            }
		}

		protected void OnLoginClick( object sender , System.EventArgs a )
		{
            if (m_ValidBrowser.Value.ToLower().Equals("false"))
            {
                return;
            }

            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }

            PriceMyLoanRequestHelper.IsEncompassIntegration = false; // If user can log in then it is not an Encompass integration
            // 6/2/2010 dd - Base on Citi request (M7). We only accept POST request for authentication page.
            if (Page.Request.HttpMethod != "POST")
            {
                return;
            }

            Guid lenderPmlSiteID = this.LenderPmlSiteID;
			if (Guid.Empty == lenderPmlSiteID) 
			{
				Tools.LogError("Why does someone access generic PML login page without LenderPmlSiteID? Redirect them to SessionExpire page.");
				Response.Redirect("~/SessionExpired.aspx");
			}

			if (Page.IsPostBack) 
			{ 
				PrincipalFactory.E_LoginProblem loginProblem;
				PriceMyLoanPrincipal principal = (PriceMyLoanPrincipal) PrincipalFactory.CreateWithFailureType(LoginName.Text, Password.Text, lenderPmlSiteID, out loginProblem, true /* isStoreToCookies*/);
				
				if (null != principal)
				{
					if(!principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
					{
						this.m_ErrMsg.Text = "Permission denied.";
						this.m_ErrMsg2.Text = string.Empty;
						return;
					}
                    
                    RequestHelper.StoreToCookie("LenderPmlSiteId", lenderPmlSiteID.ToString(), false);

                    bool didLog = RequestHelper.EnforceMultiFactorAuthentication();

                    // Only insert an audit record if we don't need MFA.  The code below the above line won't run if MFA is being enforced.
                    if (!didLog)
                    {
                        RequestHelper.InsertAuditRecord(principal, lenderPmlSiteID);
                    }

                    if (!BrokerUserDB.AreSecurityAnswersSetForTheUser(principal.BrokerId, principal.UserId))
                    {
                        Response.Redirect(PriceMyLoanConstants.SetSecurityQuestionsUrl);
                    }

                    Response.Redirect(PriceMyLoanConstants.PipelineUrl + "?isLoadingDashboard=true");
                    
				} 
				else
				{
					// 02/13/08 mf. OPM 19614 We need to redirect even on a failed login.
					var errorCode = PriceMyLoanRequestHelper.GetErrorCodeFromLoginProblem(loginProblem);
					Response.Redirect(Request.Url.AbsolutePath + "?LenderPmlSiteId=" + LenderPmlSiteID + "&errorcode=" + errorCode);
				}
			}
		}	

        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_11_2;
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new EventHandler(this.PageLoad);
            this.Init += new EventHandler(this.PageInit);
		}
		#endregion


	}
}
