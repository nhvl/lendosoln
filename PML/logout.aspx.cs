namespace PriceMyLoan.UI
{
    using System;
    using System.Web;
    using System.Web.Security;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.AntiXss;
    using LendersOffice.Security;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public partial class logout : BasePage
    {
        /// <summary>
        /// Processes the forms cookie for the logout.
        /// </summary>
        /// <returns>
        /// The type of the user that logged out.
        /// </returns>
        private AbstractUserPrincipal ProcessFormsCookie()
        {
            // 10/28/2009 dd - I have to get the CookieSessionId manually is because at this page I do not have PriceMyLoanPrincipal object.
            HttpCookie cookie = Request.Cookies.Get(FormsAuthentication.FormsCookieName);
            if (null != cookie) 
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                if (null != ticket) 
                {
                    string userData = ticket.UserData;
                    string[] parts = userData.Split('|');
                    string cookieSessionId = parts[5];
                    if (!string.IsNullOrEmpty(cookieSessionId)) 
                    {
                        // Add cookie to blacklist to prevent malicious users from re-sending
                        // the original forms cookie of a user that logged out.
                        LOCache.Set("COOKIE_SESSION_ID_" + cookieSessionId, "", DateTime.Now.AddHours(4));
                    }

                    Guid userId = new Guid(parts[1]);

                    InitialPrincipalTypeT initialPrincipalType;
                    Enum.TryParse(parts[7], out initialPrincipalType);

                    Guid initialUserId = Guid.Empty;
                    Guid.TryParse(parts[8], out initialUserId);

                    var principal = Security.PrincipalFactory.CreateByPmlUserID_SLOW(userId, initialPrincipalType, initialUserId);

                    return (AbstractUserPrincipal)principal;
                }
            }

            return null;
        }
		protected void PageLoad(object sender, EventArgs e)
		{
            var hasInternalTpoCookie = this.Request.Cookies[ConstApp.InternalTpoCookieName]?.Value == "T";
            var errorCode = RequestHelper.GetSafeQueryString("errorcode"); // OPM 24860

            var principal = ProcessFormsCookie();

            RequestHelper.ClearAuthenticationCookies();

            var isRetailUser = false;
            if (principal != null)
            {
                isRetailUser = string.Equals("B", principal.Type, StringComparison.OrdinalIgnoreCase);

                SecurityEventLogHelper.CreateLogoutLog(principal);
            }

            Guid lenderPmlSiteId = Guid.Empty;
            if (!isRetailUser)
            {
                lenderPmlSiteId = this.GetLenderPmlSiteId();
            }

            string url;
            Tools.LogInfo("Logout, errorCode=" + errorCode);
            if (!hasInternalTpoCookie && isRetailUser)
            {
                url = "RetailLogin.aspx";
            }
            else if (hasInternalTpoCookie || lenderPmlSiteId == Guid.Empty)
            {
                url = "SessionExpired.aspx";
            }
            else
            {
                url = "simple_login.aspx?lenderpmlsiteid=" + lenderPmlSiteId;
            }

			if (!string.IsNullOrWhiteSpace(errorCode))
            {
                url += "&errorcode=" + AspxTools.JsStringUnquoted(errorCode);
            }

            ClientScript.RegisterStartupScript(GetType(), "Load", $"<script type='text/javascript'>redirect('{url}');</script>");
            returnLink.HRef = url;            
        }

        private Guid GetLenderPmlSiteId()
        {
            var lenderPmlSiteId = Guid.Empty;
            try
            {
                HttpCookie cookie = Request.Cookies["LenderPmlSiteId"];
                if (null != cookie)
                {
                    string str = cookie.Value;
                    if (string.IsNullOrEmpty(str) == false)
                    {
                        lenderPmlSiteId = new Guid(str);
                    }
                }
            }
            catch (FormatException)
            {
                lenderPmlSiteId = Guid.Empty;
            }

            return lenderPmlSiteId;
        }
        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}
}
