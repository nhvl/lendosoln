using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.IO;
using System.Text.RegularExpressions;
using LendersOffice.Common;
using System.Text;

using LendersOffice.DistributeUnderwriting;

using LODataAccess = DataAccess;
using PriceMyLoan.Security;
using PriceMyLoan.Common;
using LendersOffice.Admin;
using LendersOffice.Constants;
using DataAccess;
using CommonProjectLib.Logging;
using LqbGrammar.Exceptions;
using LqbGrammar;
using LendersOffice.ObjLib.Resource;

namespace PriceMyLoan 
{
	public class Global : System.Web.HttpApplication
	{
        class FirstRequestInitialize
        {
            private static bool s_isInitialized = false;
            private static object s_lock = new object();

            public static void Initialize(HttpContext context)
            {
                if (s_isInitialized)
                {
                    return;
                }

                lock (s_lock)
                {
                    if (s_isInitialized)
                    {
                        return;
                    }

                    if (WebsiteUtilities.IsDatabaseOffline)
                    {
                        // 2/29/2012 dd - Do not initialize if database is offline. Need to perform iisreset when database
                        // is back online.
                        return;
                    }
                    Utilities.DetectTimeDifferentWithDB();
                    Tools.SetupServicePointManager();
                    Tools.SetupServicePointCallback();
                    BrokerDB.InitializeBrokerIdLock();
                    Tools.LogInfo("PriceMyLoan_Global", "PriceMyLoan_Global::Initialize");

                    if (!ResourceManager.Instance.AreGenericResourcesAvailable())
                    {
                        EmailUtilities.SendToCritical($"{ConstAppDavid.ServerName} PML Is Not Configured Correctly", "PmlShared or PmlCommon cannot be resolved.");
                    }

                    s_isInitialized = true;
                }
            }
        }

        public Global()
		{
			InitializeComponent();
		}	
		protected void Application_Start(Object sender, EventArgs e)
		{
            InitializeApplicationFramework();
        }

        private static void InitializeApplicationFramework()
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = "PML";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }
        }

        protected void Session_Start(Object sender, EventArgs e)
		{

		}
        private int m_maxRequestSize = 300000;
		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
            FirstRequestInitialize.Initialize(((HttpApplication)sender).Context);

            if (WebsiteUtilities.IsDatabaseOffline)
            {
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    if (Request.Url.AbsolutePath.EndsWith("simple_login.aspx", StringComparison.OrdinalIgnoreCase) == false)
                    {
                        Response.Redirect("~/simple_login.aspx?lenderpmlsiteid=" + Request.QueryString["lenderpmlsiteid"]);
                    }
                }
                return;
            }

            if (ConstSite.EnableRedirectToCustomFrame && Request.Url.PathAndQuery.Equals("/"))
            {
                // 9/8/2015 - dd - This check may not need since it is already handle in PriceMyLoanRequestHelper.AuthenticateRequest
                Response.Redirect("~/custom_frame/index.htm");
                return;
            }

            HttpContext.Current.Items["SanitizeOutput"] = "Yes";
            HttpRequest request = HttpContext.Current.Request;
            bool isPost = string.Compare(request.HttpMethod, "POST", true) == 0;

            string[] ignorePostPages = { 
                                       "/main/edocs/edocs.aspx",
                                       "/main/edocs/uploaddoctotask.aspx"
                                   };
            string url = request.Url.AbsolutePath.ToLower();
            if (isPost)
            {
                foreach (string s in ignorePostPages)
                {
                    if (url.EndsWith(s))
                        return;
                }
            }
            if (request.ContentLength > m_maxRequestSize) 
            {
                string str = string.Format("LARGE REQUEST PAGE - Url={0}, ContentLength={1}, TotalBytes={2}", request.RawUrl, request.ContentLength, request.TotalBytes);
                LODataAccess.Tools.LogWarning(str);
                EmailUtilities.UpdateExceptionTracking(str);

                m_maxRequestSize = request.ContentLength;
            }

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }

            // 8/13/2007 dd - OPM 17446 - The code below will allow our 3rd party website to embedded our login page on their domain.
            HttpContext context = HttpContext.Current;
            if (null != HttpContext.Current) 
            {
                if (HttpContext.Current.Request.Path.IndexOf("/CitiDemo.aspx") > 0) 
                {
                    // 8/8/2008 dd - THIS IS A TEMPORARY HACK SO THAT CITI DEMO WILL WORK.
                    return;
                }
                if (Response.ContentType != "application/pdf" && Response.ContentType != "text/csv")
                {
                    try
                    {
                        HttpContext.Current.Response.AppendHeader("P3P", "CP=\"CAO PSA OUR\"");
                    }
                    catch (HttpException)
                    {
                        // NO-OP. The append could fail if HTTP headers have been sent. No need to handle this exception.
                    }
                }
				if (Response.ContentType == "text/html")
				{
					// 01/30/08 mf. OPM 19673 - We do not want these headers when response
					// is pdf or fannie file.  This code sets the headers:
					// Cache-control: No-cache, no-store
					// Pragma: No-cache

					Response.Cache.SetCacheability(HttpCacheability.NoCache);
					Response.Cache.SetNoStore();
				}
                bool performCitiPost200Check = false; // 6/14/2011 dd - Turn off temporary
                if (context.Items.Contains(ConstAppDavid.HttpContextItem_IsServicePageKey))
                {
                    if ((bool)context.Items[ConstAppDavid.HttpContextItem_IsServicePageKey])
                    {
                        performCitiPost200Check = false;
                    }
                }
                // 5/10/2011 dd - Based from Citi security requirement. We cannot return status 200 OK on the post request.
                if (performCitiPost200Check)
                {
                    if (context.Request.RequestType == "POST" && context.Response.StatusCode == 200)
                    {
                        Tools.LogError("[CITI_VIOLATION] " + context.Request.RawUrl + " return POST 200");
                    }
                }

            }

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }
			PriceMyLoan.Common.PriceMyLoanRequestHelper.AuthenticateRequest();     
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                Server.ClearError();
                return;
            }
            Exception exc = Server.GetLastError();

            if (exc is System.OutOfMemoryException)
            {
                // A desparate call, assuming that we can get out-of-memory exception before garbage collection is invoked to do a full (time-consuming) job to collect all collectable memory.
                GC.GetTotalMemory( true );
            }
            if (exc is HttpException)
            {
                if (((HttpException)exc).GetHttpCode() == 404)
                {
                    // 6/18/2013 dd - Ignore 404 page not found.
                    return;
                }
            }

            // If the user entered a filename that failed the HTTPRequestValidation
            // because &# was in it, display a custom error message
            bool isInvalidFilename = false;
            if (exc is System.Reflection.TargetInvocationException)
            {
                HttpRequestValidationException innerException = exc.InnerException as HttpRequestValidationException;
                string invalidPattern = ".*filename=\\\".*&#.*\\\"";
                if (innerException != null && Regex.IsMatch(exc.InnerException.Message, invalidPattern, RegexOptions.Singleline))
                {
                    isInvalidFilename = true;                    
                }
            }

            PriceMyLoanPrincipal principal = HttpContext.Current.User as PriceMyLoanPrincipal;
            bool autofillErrorForm = (principal != null);
            if (autofillErrorForm && !isInvalidFilename)
            {
                ErrorUtilities.DisplayErrorPage(exc, true, principal.BrokerId, principal.EmployeeId);
            }
            else if (autofillErrorForm && isInvalidFilename)
            {
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.EDocs.InvalidUploadFilename, exc.ToString()),
                        false, principal.BrokerId, principal.EmployeeId);
            }
            else if (!autofillErrorForm && !isInvalidFilename)
            {
                ErrorUtilities.DisplayErrorPage(exc, true, Guid.Empty, Guid.Empty);
            }
            else // !autofillErrorForm && isInvalidFilename
            {
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.EDocs.InvalidUploadFilename, exc.ToString()),
                        false, Guid.Empty, Guid.Empty);
            }
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{
            Tools.LogInfo("PriceMyLoan_Global", "PriceMyLoan_Global::Application_End");
		}

        protected void Application_PostMapRequestHandler(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (context.Handler is System.Web.UI.Page && !string.IsNullOrEmpty(context.Request.PathInfo))
            {
                string contentType = context.Request.ContentType.Split(';')[0];
                if (contentType.Equals("application/json", StringComparison.OrdinalIgnoreCase))
                {
                    context.Response.Filter = new LendersOffice.HttpModule.WebMethodExceptionLogger(context.Response);
                }
            }
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
		{    
		}
		#endregion
	}
}

