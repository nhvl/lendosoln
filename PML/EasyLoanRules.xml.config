<?xml version="1.0" encoding="utf-8" ?>
<RuleEngine>
	<history>
		<version id="1.0" date="2005/11/14">initial version</version>
		<version id="1.1" date="2005/11/2x">Allow managers to edit closed loans, others can read</version>
		<version id="1.1.5" date="2005/12/05">Transit from 1.1 to 1.2
           - Add CanWriteNonAssignedLoan variable
           - Add RestrictivelyEditLoanRule
        <!-- 2005/12/12 add more test cases -->
        </version>
		<version id="1.2" date="2005/12/28">Finish testing</version>
		<version id="1.25" date="2006/01/05">Add ActAsRateLocked</version>
		<version id="1.32" date="2006/01/24">Finish testing for ActAsRateLocked variable. Add "all" testcases for this version</version>
		<version id="1.32bis" date="2006/02/27">Divide Active state of LoanStatus into 2 parts : ActiveButBeforeApprove and Active </version>
		<version id="1.36" date="2006/02/28">Testing 3 new rules for opm 3385 </version>
		<version id="1.40" date="2006/03/01">Finish to add 3 new rules for opm 3385 </version>
		<version id="1.45" date="2006/03/15">Add security for "group of fields"</version>
		<version id="1.46" date="2006/04/12">Change the way to get "group of fields"</version>
		<version id="1.50" date="2006/05/24">Finish testing, rename version 1.46 to 1.50</version>
	</history>
	<FileVersion>2006-05-24</FileVersion>
	<vars>
		<var name="Application" type="enum">
			<value>LenderOffice</value>
			<value>PriceMyLoan</value>
		</var>
		<var name="UserType" type="enum">
			<value>Broker</value>
			<value>PriceMyLoan</value>
			<value>Internal</value>
		</var>
		<var name="Scope" type="enum">
			<value>CrossCorporate</value>
			<value>InScope</value>
			<value>Nonduty</value>
		</var>
		<var name="IsAssigned" type="bool" />
		<var name="HaveUnderwriter" type="enum">
			<value shortValue="U" note="Underwrite">true</value>
			<value shortValue="u" note="NoUnderwrite">false</value>
		</var>
		<var name="EditAssignedUnderwriter" type="bool" />
		<var name="HaveProcessor" type="enum">
			<value shortValue="P">true</value>
			<value shortValue="p">false</value>
		</var>
		<var name="EditAssignedProcessor" type="bool" />
		<var name="AccessClosedLoan" type="bool" />
		<var name="IsValid" type="enum">
			<value shortValue="V" note="Valid">true</value>
			<value shortValue="D" note="Delete">false</value>
		</var>
		<var name="IsLoan" type="enum">
			<value shortValue="L" note="Loan">true</value>
			<value shortValue="T" note="Template">false</value>
		</var>
		<var name="LimitTemplateToBranch" type="enum">
			<value shortValue="L">true</value>
			<value shortValue="l">false</value>
		</var>
		<var name="PriceMyLoan" type="bool" />
		<!-- var name="LoanIsManagedByUser" type="bool" -->
		<!-- IsExtAssigned = LoanIsManagedByUser | IsAssigned  " -->
		<var name="IsExtAssigned" type="bool" />
		<var name="Lien" type="enum">
			<value>First</value>
			<value>Second</value>
			<value>Other</value>
		</var>
		<var name="Role" type="enum">
			<value shortValue="C">Accountant</value>
			<value shortValue="A">Administrator</value>
			<value shortValue="X">LenderAccountExec</value>
			<value shortValue="L">LoanOfficer</value>
			<value shortValue="O">LoanOpener</value>
			<value shortValue="D">LockDesk</value>
			<value shortValue="M">Manager</value>
			<value shortValue="P">Processor</value>
			<value shortValue="R">RealEstateAgent</value>
			<value shortValue="T">Telemarketer</value>
			<value shortValue="U">Underwriter</value>
      <value shortValue="S">Closer</value>
			<value shortValue="?">Other</value>
		</var>
		<var name="LoanStatus" type="enum">
			<value shortValue="L">Lead</value>
			<value shortValue="O">Open</value>
			<value shortValue="B">ActiveButBeforeApprove</value>
			<value shortValue="A">Active</value>
			<value shortValue="I">Inactive</value>
			<value shortValue="C">Close</value>
		</var>
		<var name="CanWriteNonAssignedLoan" type="enum">
			<value shortValue="W">true</value>
			<value shortValue="w">false</value>
		</var>
		<var name="ActAsRateLocked" type="enum">
			<value shortValue="N">NotAffect</value>
			<value shortValue="R">AllowRead</value>
			<value shortValue="W">AllowWrite</value>
		</var>
		<var name="IsOnlyAccountantCanModifyTrustAccount" type="enum">
			<value shortValue="Y">true</value>
			<value shortValue="N">false</value>
		</var>
		<var name="CanModifyLoanName" type="enum">
			<value shortValue="M">true</value>
			<value shortValue="m">false</value>
		</var>
		<var name="IsRateLocked" type="enum">
			<value shortValue="L">true</value>
			<value shortValue="U">false</value>
		</var>
		<var name="IsAccountExecutiveOnly" type="enum">
			<value shortValue="O">true</value>
			<value shortValue="o">false</value>
		</var>
		<var name="CanCreateLoanTemplate" type="enum">
			<value shortValue="C">true</value>
			<value shortValue="c">false</value>
		</var>
		<var name="HasLenderDefaultFeatures" type="enum">
			<value shortValue="H">true</value>
			<value shortValue="h">false</value>
		</var>
	</vars>
	<groups>
		<group>DefaultGroup</group>
		<group>LoanNameGroup</group>
		<group>RateLockedGroup</group>
		<group>LiabilityCollectionGroup</group>
		<group>UnderwriterGroup</group>
		<group>TrustAccountGroup</group>
		<group>AuditHistoryGroup</group>
		<group>CreateTemplateLinkGroup</group>
		<group>LinkForLenderDefaultFeatureGroup</group>
		<group>LinkForPMLGroup</group>
		<group>OldConditionGroup</group>
		<group>ModifyConditionsPageGroup</group>
		<group>BreakRateLockGroup</group>
		<group>Page_PMLDefaultValuesGroup</group>
		<group>TestingNewRuleGroup</group>
	</groups>
	<rules>
		<rule id="R001" name="CrossCorporateRule" result="NeverRead">
			<note>Cross-broker access is not allowed</note>
			<condition varname="Scope">CrossCorporate</condition>
		</rule>
		<rule id="R002" name="DeletedRule" result="NeverRead">
			<note>nobody (include managers) can read deleted loan/template</note>
			<condition varname="IsValid">false</condition>
		</rule>
		<rule id="R003" name="AdminRole" result="NeverRead">
			<note>Administrator is not allowed to access loans, templates</note>
			<condition varname="Role">Administrator</condition>
		</rule>
		<!--  ==================== General Loan Rules =========================== -->
		<blockRule>
			<note>The loan rules </note>
			<condition varname="IsLoan">true</condition>
			<rule  id="R004" name="ClosedLoan_NormalRule" result="NeverRead">
				<note>if AccessClosedLoan = off, nobody can read closed loans#Cannot read closed loans.#</note>
				<condition varname="LoanStatus">Close</condition>
				<condition varname="AccessClosedLoan">false</condition>
			</rule>
			<rule id="R005" name="ClosedLoan_SpecRule" result="NeverWrite">
				<note>if AccessClosedLoan = on, non-managers can not edit closed loans#Non-managers cannot edit closed loans.#</note>
				<condition varname="LoanStatus">Close</condition>
				<condition varname="AccessClosedLoan">true</condition>
				<!-- <condition varname="Role">LoanOfficer,LenderAccountExec</condition> -->
				<condition varname="Role">~Manager</condition>
			</rule>
			<rule  id="R006" name="AssignedUnderwriterRule" result="NeverWrite">
				<note>#You cannot edit this loan because there is currently an Underwriter assigned to it.#</note>
				<condition varname="Role">~LockDesk,Manager</condition>
				<condition varname="HaveUnderwriter">true</condition>
				<condition varname="EditAssignedUnderwriter">false</condition>
			</rule>
			<rule id="R007" name="AssignedProcessorRule" result="NeverWrite">
				<note>#You cannot edit this loan because there is currently a Processor assigned to it.#</note>
				<condition varname="Role">LoanOfficer,LenderAccountExec,RealEstateAgent</condition>
				<condition varname="HaveProcessor">true</condition>
				<condition varname="EditAssignedProcessor">false</condition>
			</rule>
			<rule  id="R008" name="RestrictivelyEditLoanRule" result="NeverWrite">
				<note>Can not write non-assigned loan#You cannot edit this loan because it is not assigned to you.#</note>
				<condition varname="CanWriteNonAssignedLoan">false</condition>
				<condition varname="IsAssigned">false</condition>
			</rule>
			<rule id="R009" name="PML_Engine_and_LoanOfficer_Role" result="DenyWrite">
				<note>LoanOfficer can not edit loans that are past 'open' status#Loan Officers cannot edit loans that are not in the 'open' status (for example, 'inactive' or 'closed' loans).#</note>
				<condition varname="Role">LoanOfficer</condition>
				<condition varname="PriceMyLoan">true</condition>
				<condition varname="LoanStatus">Inactive,ActiveButBeforeApprove,Active,Close</condition>
			</rule>
		</blockRule>
		<rule id="R010" name="LimitTemplateToBranchRule" result="DenyRead">
			<note>Nonduty can not read template if LimitTemplateToBranch = true#You cannot view this loan because you are not in the same branch as this loan.#</note>
			<condition varname="IsLoan">false</condition>
			<condition varname="LimitTemplateToBranch">true</condition>
			<condition varname="Scope">Nonduty</condition>
		</rule>
		<!--  ==================== Loan Rules For LenderOffice =========================== -->
		<blockRule>
			<note>The rules relate with loan for Lender Office Application</note>
			<condition varname="Application">LenderOffice</condition>
			<condition varname="UserType">Broker</condition>
			<condition varname="IsLoan">true</condition>
			<condition varname="Scope">InScope</condition>
			<rule  id="R011" name="Accountant_RealEstateAgentRole">
				<note>Accountant, RealEstateAgent can read valid loan if loan status is open or active, otherwise can not read.#Accountants and Real Estate Agents are only allowed to read a valid loan if the loan's status is open or active.#</note>
				<condition varname="Role">Accountant,RealEstateAgent</condition>
				<switch varname="LoanStatus" defaultResult="DenyRead">
					<case result="AllowRead">Open,ActiveButBeforeApprove,Active</case>
				</switch>
			</rule>
			<rule  id="R012" name="PML_Engine_and_AE_Role" result="DenyWrite">
				<note>LenderAccountExec can not edit loans that are past 'open' status#Lender Account Executives can only edit open loans, not inactive, active, or closed loans.#</note>
				<condition varname="Role">LenderAccountExec</condition>
				<condition varname="PriceMyLoan">true</condition>
				<condition varname="ActAsRateLocked">NotAffect</condition>
				<condition varname="LoanStatus">Inactive,ActiveButBeforeApprove,Active,Close</condition>
			</rule>
			<rule  id="R013" name="AE_and_ActAsRateLocked">
				<note>#Lender Account Executives cannot edit the loan if the rate is locked.#</note>
				<condition varname="Role">LenderAccountExec</condition>
				<condition varname="PriceMyLoan">true</condition>
				<switch varname="ActAsRateLocked">
					<case result="DenyWrite">AllowRead</case>
					<case result="AllowWrite">AllowWrite</case>
				</switch>
			</rule>
			<rule  id="R014" name="LOX_Role">
				<note>LoanOfficer, LenderAccountExec can read inactive loan, other cases they can edit loan.#Loan Officers and Lender Account Executives can read inactive loans, but not edit them.#</note>
				<condition varname="Role">LoanOfficer, LenderAccountExec</condition>
				<switch varname="LoanStatus" defaultResult="AllowWrite">
					<case result="AllowRead">Inactive</case>
				</switch>
			</rule>
			<rule id="R015" name="Telemarketer_Role" result="AllowWrite">
				<note>Telemarketer role only edits valid loan if loan status is lead.#Telemarketers can only edit a loan if the loan status is lead.#</note>
				<condition varname="Role">Telemarketer</condition>
				<condition varname="LoanStatus">Lead</condition>
			</rule>
			<rule id="R016" name="Managers_Role" result="AllowWrite">
				<note>LockDesk,Processor,Underwriter,Manager can edit loan.#Only Lock Desks, Loan Openers, Processors, Underwriters and Managers can edit the loan.#</note>
				<condition varname="Role">LockDesk,Processor,Underwriter,Manager</condition>
			</rule>
			<rule id="R017" name="LoanOpener_Role">
				<note>LoanOpener can edit loans for status up to loan open but not beyond that#Loan openers can only edit open loans and leads.#</note>
				<condition varname="Role">LoanOpener</condition>
				<switch varname="LoanStatus" defaultResult="AllowRead">
					<case result="AllowWrite">Lead,Open</case>
				</switch>
			</rule>
			<rule id="R018" name="StewartRule" result="NeverWrite">
				<note>if PriceMyLoan = true then  nobody, except LockDesk,Underwriter,Manager, can edit loans with status Active or beyond#Only Lock Desks, Underwriters and Managers can edit Active, Inactive and Closed loans.#</note>
				<condition varname="Role">~LockDesk,Underwriter,Manager</condition>
				<condition varname="LoanStatus">Active, Inactive, Close</condition>
				<condition varname="PriceMyLoan">true</condition>
			</rule>
      <rule id="R040" name="Closer_Role" result="AllowRead">
        <note>Closer role can only write certain loan statuses. However that check is perform in code. This rule allow Closer to have read permisison on all statuses.</note>
        <condition varname="Role">Closer</condition>
        <condition varname="LoanStatus">Lead,Open,ActiveButBeforeApprove,Active,Inactive,Close</condition>
      </rule>
		</blockRule>
		<!--  ==================== Template Rules For LenderOffice =========================== -->
		<blockRule>
			<note>Template rules for Lender Office Application</note>
			<condition varname="Application">LenderOffice</condition>
			<condition varname="UserType">Broker</condition>
			<condition varname="IsLoan">false</condition>
			<rule id="R019" name="TemplateRule" result="AllowWrite">
				<condition varname="Role">LockDesk,LoanOpener,Processor,Underwriter,Manager,LoanOfficer,LenderAccountExec</condition>
			</rule>
			<rule id="R020" name="LOX_CannotEdit_UnassignedTemplate" result="DenyWrite">
				<condition varname="Role">LoanOfficer,LenderAccountExec</condition>
				<condition varname="IsAssigned">false</condition>
			</rule>
			<rule id="R021" name="NondutyCannotEditTemplateRule" result="DenyWrite">
				<condition varname="Scope">Nonduty</condition>
			</rule>
			<!--
            <rule name="LimitTemplateToBranchRule" result="DenyRead">
                <note>Nonduty can not read template if LimitTemplateToBranch = true</note>
                <condition varname="LimitTemplateToBranch">true</condition>
                <condition varname="Scope">Nonduty</condition>
            </rule>
            -->
		</blockRule>
		<!--  ==================== Security groups for LendersOffice =========================== -->
		<blockRule>
			<note>Security groups for LendersOffice</note>
			<condition varname="Application">LenderOffice</condition>
			<rule id="R022" name="EditLoanNameGroupRule" result="DenyWrite">
				<note>Only user with CanModifyLoanName can edit loan number.</note>
				<applyToGroups>LoanNameGroup</applyToGroups>
				<condition varname="CanModifyLoanName">false</condition>
			</rule>
			<rule id="R023" name="RateLockedGroupRule" result="DenyWrite">
				<note>If rate lock mode = on, do not allow user to edit certain fields</note>
				<applyToGroups>RateLockedGroup</applyToGroups>
				<condition varname="IsRateLocked">true</condition>
			</rule>
			<rule id="R024" name="LiabilityColRule" result="DenyWrite">
				<note>Account Executive Only Role will not able to edit liabilities</note>
				<condition varname="IsAccountExecutiveOnly">true</condition>
				<condition varname="Role">LenderAccountExec</condition>
				<applyToGroups>LiabilityCollectionGroup</applyToGroups>
			</rule>
			<rule id="R025" name="UnderwriterGroupRule" result="DenyWrite">
				<note>Only Underwriter can edit 'Exclude From Underwriting' in liability record</note>
				<condition varname="Role">~Underwriter</condition>
				<applyToGroups>UnderwriterGroup</applyToGroups>
			</rule>
			<rule id="R026" name="TrustAccountGroupRule" result="DenyWrite">
				<note>Only Account can edit this group</note>
				<applyToGroups>TrustAccountGroup</applyToGroups>
				<condition varname="Role">~Accountant</condition>
				<condition varname="IsOnlyAccountantCanModifyTrustAccount">true</condition>
			</rule>
			<rule id="R027" name="AuditHistoryGroupRule">
				<note>Only Admin,Manager,Underwriter,LockDesk can read 'AuditHistory group'</note>
				<applyToGroups>AuditHistoryGroup</applyToGroups>
				<switch varname="Role" defaultResult="NeverRead">
					<case result="NeverWrite">Administrator,Manager,Underwriter,LockDesk</case>
				</switch>
			</rule>
			<rule id="R028" name="CreateTemplateLinkGroupRule">
				<applyToGroups>CreateTemplateLinkGroup</applyToGroups>
				<switch varname="CanCreateLoanTemplate" defaultResult="NeverRead">
					<case result="NeverWrite">true</case>
				</switch>
			</rule>
			<rule id="R029" name="LinkForLenderDefaultFeatureGroupRule">
				<applyToGroups>LinkForLenderDefaultFeatureGroup</applyToGroups>
				<switch varname="HasLenderDefaultFeatures" defaultResult="NeverRead">
					<case result="NeverWrite">true</case>
				</switch>
			</rule>
			<rule id="R030" name="LinkForPMLGroupRule" result="NeverRead">
				<applyToGroups>LinkForPMLGroup</applyToGroups>
				<switch varname="PriceMyLoan" defaultResult="NeverRead">
					<case result="NeverWrite">true</case>
				</switch>
			</rule>
			<rule id="R031" name="OldConditionGroupRule" result="NeverRead">
				<applyToGroups>OldConditionGroup</applyToGroups>
				<switch varname="HasLenderDefaultFeatures" defaultResult="NeverRead">
					<case result="NeverWrite">false</case>
				</switch>
			</rule>
			<rule id="R032" name="ModifyConditionsPageGroupRule" result="NeverWrite">
				<applyToGroups>ModifyConditionsPageGroup</applyToGroups>
				<condition varname="Role">~Underwriter,Manager,LockDesk</condition>
			</rule>
			<rule id="R033" name="BreakRateLockGroupRule" result="NeverWrite">
				<applyToGroups>BreakRateLockGroup</applyToGroups>
				<condition varname="Role">~Underwriter,Manager,LockDesk</condition>
			</rule>
			<rule id="R034" name="Page_PMLDefaultValuesGroupRule">
				<note>Only "Feature.PriceMyLoan on and Loan = template" can view PML Default Values page</note>
				<applyToGroups>Page_PMLDefaultValuesGroup</applyToGroups>
				<condition varname="IsLoan">false</condition>
				<switch varname="PriceMyLoan" defaultResult="NeverRead">
					<case result="NeverWrite">true</case>
				</switch>
			</rule>
			<!--  will remove this rule after change source code -->
			<rule id="R035" name="Page_PMLDefaultValuesGroupRule" result="NeverRead">
				<note>Only "Feature.PriceMyLoan on and Loan = template" can view PML Default Values page</note>
				<applyToGroups>Page_PMLDefaultValuesGroup</applyToGroups>
				<condition varname="IsLoan">true</condition>
			</rule>
			<!--  Only Administrator can view this page -->
			<rule id="R036" name="TestingNewRuleGroupRule" result="NeverWrite">
				<applyToGroups>TestingNewRuleGroup</applyToGroups>
				<switch varname="Role" defaultResult="NeverRead">
					<case result="NeverWrite">Administrator</case>
				</switch>
			</rule>
		</blockRule>
		<!--  ==================== PriceMyLoan =========================== -->
		<blockRule>
			<note>The loan rules for PriceMyLoan</note>
			<condition varname="Application">PriceMyLoan</condition>
			<condition varname="UserType">PriceMyLoan</condition>
			<condition varname="Role">LoanOfficer</condition>
			<condition varname="PriceMyLoan">true</condition>
			<condition varname="IsLoan">true</condition>
			<rule  id="R037" name="PriceMyLoan_LienPosition">
				<note>if LienPositon = First, LoanOfficer can edit loan. if LienPositon = Second, he can read loan </note>
				<switch varname="Lien">
					<case result="AllowWrite">First</case>
					<case result="AllowRead">Second</case>
				</switch>
			</rule>
			<rule id="R038" name="PriceMyLoan_AssignRule" result="DenyRead">
				<note>if LoanOfficer is not assigned to current loan and he is not ExternalManagerOfLoan, then he is not allowed to read</note>
				<condition varname="IsExtAssigned">false</condition>
				<condition varname="IsAssigned">false</condition>
			</rule>
			<!--
                <rule name="PriceMyLoan_Active_InactiveRule" result="NeverWrite">
                    <note>With , nobody edit loan if LoanStatus = Active, Inactive</note>
                    <condition varname="LoanStatus">Inactive,Active</condition>
                </rule>
            -->
		</blockRule>
		<blockRule>
			<note>The template rules for PriceMyLoan</note>
			<condition varname="Application">PriceMyLoan</condition>
			<condition varname="UserType">PriceMyLoan</condition>
			<condition varname="Role">LoanOfficer</condition>
			<condition varname="PriceMyLoan">true</condition>
			<condition varname="IsLoan">false</condition>
			<rule id="R039" name="PriceMyLoan_TemplateRule" result="AllowRead">
				<condition varname="Role">LoanOfficer</condition>
			</rule>
			<!--
            <rule name="LimitTemplateToBranchRule" result="DenyRead">
                <note>Nonduty can not read template if LimitTemplateToBranch = true</note>
                <condition varname="LimitTemplateToBranch">true</condition>
                <condition varname="Scope">Nonduty</condition>
            </rule>
            -->
		</blockRule>
	</rules>
	<!--  ==================== Mapping section ============================ -->
	<mapping>
		<group name="LoanNameGroup">
			<field name="sLNm" />
		</group>
		<group name="RateLockedGroup">
			<field name="sNoteIR" />
			<field name="sTerm" />
			<field name="sDue" />
			<field name="sBrokComp1Pc" />
			<field name="sRAdjMarginR" />
			<field name="sRLckdExpiredD" />
			<field name="sRLckdD" />
		</group>
		<group name="LiabilityCollectionGroup">
			<field name="aLiaCollection" />
		</group>
		<group name="UnderwriterGroup">
			<field name="aLiaExcFromUnderwriting" />
		</group>
		<group name="TrustAccountGroup">
			<field name="sTrust1PayableChkNum" />
			<field name="sTrust1ReceivableChkNum" />
			<field name="sTrust1ReceivableN" />
			<field name="sTrust1Desc" />
			<field name="sTrust1D" />
			<field name="sTrust1PayableAmt" />
			<field name="sTrust1ReceivableAmt" />
			<field name="sTrust2PayableChkNum" />
			<field name="sTrust2ReceivableChkNum" />
			<field name="sTrust2ReceivableN" />
			<field name="sTrust2Desc" />
			<field name="sTrust2D" />
			<field name="sTrust2PayableAmt" />
			<field name="sTrust2ReceivableAmt" />
			<field name="sTrust3PayableChkNum" />
			<field name="sTrust3ReceivableChkNum" />
			<field name="sTrust3ReceivableN" />
			<field name="sTrust3Desc" />
			<field name="sTrust3D" />
			<field name="sTrust3PayableAmt" />
			<field name="sTrust3ReceivableAmt" />
			<field name="sTrust4PayableChkNum" />
			<field name="sTrust4ReceivableChkNum" />
			<field name="sTrust4ReceivableN" />
			<field name="sTrust4Desc" />
			<field name="sTrust4D" />
			<field name="sTrust4PayableAmt" />
			<field name="sTrust4ReceivableAmt" />
			<field name="sTrust5PayableChkNum" />
			<field name="sTrust5ReceivableChkNum" />
			<field name="sTrust5ReceivableN" />
			<field name="sTrust5Desc" />
			<field name="sTrust5D" />
			<field name="sTrust5PayableAmt" />
			<field name="sTrust5ReceivableAmt" />
			<field name="sTrust6PayableChkNum" />
			<field name="sTrust6ReceivableChkNum" />
			<field name="sTrust6ReceivableN" />
			<field name="sTrust6Desc" />
			<field name="sTrust6D" />
			<field name="sTrust6PayableAmt" />
			<field name="sTrust6ReceivableAmt" />
			<field name="sTrust7PayableChkNum" />
			<field name="sTrust7ReceivableChkNum" />
			<field name="sTrust7ReceivableN" />
			<field name="sTrust7Desc" />
			<field name="sTrust7D" />
			<field name="sTrust7PayableAmt" />
			<field name="sTrust7ReceivableAmt" />
			<field name="sTrust8PayableChkNum" />
			<field name="sTrust8ReceivableChkNum" />
			<field name="sTrust8ReceivableN" />
			<field name="sTrust8Desc" />
			<field name="sTrust8D" />
			<field name="sTrust8PayableAmt" />
			<field name="sTrust8ReceivableAmt" />
			<field name="sTrust9PayableChkNum" />
			<field name="sTrust9ReceivableChkNum" />
			<field name="sTrust9ReceivableN" />
			<field name="sTrust9Desc" />
			<field name="sTrust9D" />
			<field name="sTrust9PayableAmt" />
			<field name="sTrust9ReceivableAmt" />
			<field name="sTrust10PayableChkNum" />
			<field name="sTrust10ReceivableChkNum" />
			<field name="sTrust10ReceivableN" />
			<field name="sTrust10Desc" />
			<field name="sTrust10D" />
			<field name="sTrust10PayableAmt" />
			<field name="sTrust10ReceivableAmt" />
			<field name="sTrust11PayableChkNum" />
			<field name="sTrust11ReceivableChkNum" />
			<field name="sTrust11ReceivableN" />
			<field name="sTrust11Desc" />
			<field name="sTrust11D" />
			<field name="sTrust11PayableAmt" />
			<field name="sTrust11ReceivableAmt" />
			<field name="sTrust12PayableChkNum" />
			<field name="sTrust12ReceivableChkNum" />
			<field name="sTrust12ReceivableN" />
			<field name="sTrust12Desc" />
			<field name="sTrust12D" />
			<field name="sTrust12PayableAmt" />
			<field name="sTrust12ReceivableAmt" />
			<field name="sTrust13PayableChkNum" />
			<field name="sTrust13ReceivableChkNum" />
			<field name="sTrust13ReceivableN" />
			<field name="sTrust13Desc" />
			<field name="sTrust13D" />
			<field name="sTrust13PayableAmt" />
			<field name="sTrust13ReceivableAmt" />
			<field name="sTrust14PayableChkNum" />
			<field name="sTrust14ReceivableChkNum" />
			<field name="sTrust14ReceivableN" />
			<field name="sTrust14Desc" />
			<field name="sTrust14D" />
			<field name="sTrust14PayableAmt" />
			<field name="sTrust14ReceivableAmt" />
			<field name="sTrust15PayableChkNum" />
			<field name="sTrust15ReceivableChkNum" />
			<field name="sTrust15ReceivableN" />
			<field name="sTrust15Desc" />
			<field name="sTrust15D" />
			<field name="sTrust15PayableAmt" />
			<field name="sTrust15ReceivableAmt" />
			<field name="sTrust16PayableChkNum" />
			<field name="sTrust16ReceivableChkNum" />
			<field name="sTrust16ReceivableN" />
			<field name="sTrust16Desc" />
			<field name="sTrust16D" />
			<field name="sTrust16PayableAmt" />
			<field name="sTrust16ReceivableAmt" />
		</group>
		<group name="AuditHistoryGroup">
			<field name="Page_AuditHistory" />
		</group>
		<group name="CreateTemplateLinkGroup">
			<field name="Page_CreateTemplate" />
		</group>
		<group name="LinkForLenderDefaultFeatureGroup">
			<field name="Folder_Underwriting" />
			<field name="Folder_LockDesk" />
			<field name="Page_Funding" />
		</group>
		<group name="LinkForPMLGroup">
			<field name="Folder_PriceMyLoan" />
			<field name="Page_Certificate" />
			<field name="Page_PmlReviewInfo" />
			<field name="Page_ViewBrokerNotes" />
			<field name="Page_QualifiedLoanProgramSearch" />
		</group>
		<group name="OldConditionGroup">
			<field name="Page_StatusConditions" />
		</group>
		<group name="Page_PMLDefaultValuesGroup">
			<field name="Page_PMLDefaultValues" />
		</group>
	</mapping>
	<!--  ==================== Test cases section ============================
         This file doesn't contain test cases section because we want the rule file is load as fast as possible.
         Please, look at "LendersOfficeApp\Rules.arc\EasyLoanRules-version 1.50.xml" to see the full content.
    -->
	<SomeTestcases></SomeTestcases>
	<AllTestcases></AllTestcases>
</RuleEngine>
