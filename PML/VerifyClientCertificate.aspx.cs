﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using PriceMyLoan.Common;

namespace PriceMyLoan
{
    public partial class VerifyClientCertificate : MinimalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cacheId = RequestHelper.GetMFACacheKey(RequestHelper.GetSafeQueryString("id"));

            string str = AutoExpiredTextCache.GetFromCache(cacheId);

            string[] parts = str.Split('|');

            if (parts.Length != 3)
            {
                throw CBaseException.GenericException("Unexpected format for ClientCertificate string.");
            }
            // This page only verify if user has proper client certificate.
            // If user certificate is valid then redirect to successful url.
            Guid brokerId = new Guid(parts[0]);
            Guid userId = new Guid(parts[1]);
            bool redirectToAuthenticationCodePageOnInvalid = parts[2] == "1";

            string url = string.Empty;
            if (ClientCertificateUtilities.IsValidClientCertificate(Request.ClientCertificate, PrincipalFactory.CurrentPrincipal))
            {
                // This way of accessing the pml site id is similar to what is
                // done in AuthenticationCodePage.aspx.cs. gf opm 457995
                // PML site ID cookie will be null for retail users.
                HttpCookie siteIdCookie = Request.Cookies.Get("LenderPmlSiteId");
                var lenderPmlSiteId = Guid.Empty;
                if (siteIdCookie != null)
                {
                    lenderPmlSiteId = new Guid(siteIdCookie.Value);
                }

                RequestHelper.InsertAuditRecord(PrincipalFactory.CurrentPrincipal, lenderPmlSiteId);
                url = PriceMyLoanConstants.PipelineUrl;
            }
            else
            {
                if (redirectToAuthenticationCodePageOnInvalid)
                {
                    url = ConstApp.LendingQBAuthenticationCodePage;
                    RequestHelper.GenerateMultiFactorAuthenticationCode(brokerId, userId);
                }
                else
                {
                    url = ConstApp.LendingQBErrorPage;

                    // 5/11/2014 dd - Assume when get redirect to error page, it is because of IP restriction.
                    string devMessage = "Access failed due to IP restriction. Current IP :" + RequestHelper.ClientIP + " UserId:" + userId;
                    ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.IPRestrictionAccessDenied, devMessage), false, Guid.Empty, Guid.Empty);
                }
            }

            Response.Redirect(url);
        }
    }
}
