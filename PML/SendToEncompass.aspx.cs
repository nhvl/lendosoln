﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using Integration.Encompass;
using LendersOffice.Common;
using PriceMyLoan.Common;

namespace PriceMyLoan
{
    public partial class SendToEncompass : MinimalPage
    {

        protected override void OnInit(EventArgs e)
        {
            this.Init += new EventHandler(SendToEncompass_Init);
            base.OnInit(e);
        }

        private string GenerateXmlResponse()
        {
            E_Encompass360RequestT eRequestType = PriceMyLoanRequestHelper.EncompassRequestType;
            // The request type is added to the url in main.aspx
            string sLoanID = RequestHelper.GetSafeQueryString("loanid");
            string[] sLoanIDAndEncompassRequestType = sLoanID.Split('_');
            //string xml = @"<pricemyloan_envelope source=""Ellie Mae"" action=""EllieMaeQuoteResponse""><body>MDTI BackEnd=""20"" /><LOAN MISMOVersionIdentifier=""2.3.1""></LOAN><product name=""Test"" rate=""3.3""/></body></pricemyloan_envelope>";
            EncompassResponse encompassResponse = new EncompassResponse();
            if (sLoanIDAndEncompassRequestType.Length > 1) 
            {
                sLoanID = sLoanIDAndEncompassRequestType[0];
                eRequestType = PriceMyLoanRequestHelper.ConvertEncompassRequestType(sLoanIDAndEncompassRequestType[1]);
            }
            return encompassResponse.Export(new Guid(sLoanID), eRequestType); 
        }

        private void SendToEncompass_Init(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "text/xml";

            string xml = GenerateXmlResponse();

            Tools.LogInfo("SendToEncompass.aspx:: " + xml);

            Response.Write(xml);
            Response.End();
        }
    }
}
