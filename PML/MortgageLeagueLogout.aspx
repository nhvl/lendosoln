<%@ Page language="c#" Codebehind="MortgageLeagueLogout.aspx.cs" AutoEventWireup="false" Inherits="PriceMyLoan.MortgageLeagueLogout" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>MortgageLeagueLogout</title>
    <style type="text/css">
        .bold {
            font: bold;
        }
    </style>
  </head>
  <body MS_POSITIONING="FlowLayout">
	<script language=javascript>
<!--
	    function f_close() {
	        if (typeof(parent.LQBPopup) != "undefined") {
	            parent.LQBPopup.Hide();
	        } else {
	            top.close();
	        }
	    }
//-->
</script>

<form id="MortgageLeagueLogout" method="post" runat="server">
<table id=Table1 height="100%" cellspacing=0 cellpadding=0 width="100%" border=0>
  <tr>
    <td valign="middle" align="center">
        <asp:PlaceHolder ID="m_msg" runat="server" Visible="false"><span class="bold">Thank you for submitting your loan. 
            The next available Lender Account Executive will contact you shortly.<br /><br /></span></asp:PlaceHolder>
        <br />
        <input id=btnClose type=button value="Close Window" onclick="f_close();">
    </td>
  </tr>
</table>
</form>

</body>
</html>
