﻿namespace PriceMyLoan
{
    using System;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI.HtmlControls;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using PriceMyLoan.Common;
    using PriceMyLoan.Security;

    // This version of quick pricer is meant for anonymous quick pricer only!!! So it will use the anon qp cookie to create the principals.
    public partial class QuickPricer : PriceMyLoan.UI.BasePage
    {
        private AbstractUserPrincipal GetPrincipal()
        {
            AbstractUserPrincipal principal = null;
            try
            {
                Guid pmlLenderSiteId = RequestHelper.GetGuid("lenderpmlsiteid", Guid.Empty);
                if (Guid.Empty != pmlLenderSiteId)
                {
                    Guid anonymousId = RequestHelper.GetGuid("anonymousid", Guid.Empty);

                    if (anonymousId == Guid.Empty)
                    {
                        PriceMyLoan.Security.PrincipalFactory.E_LoginProblem loginProblem;

                        principal = (AbstractUserPrincipal)PriceMyLoan.Security.PrincipalFactory.CreateWithFailureType(ConstAppDavid.PmlQuickPricerAnonymousLogin,
                            ConstAppDavid.PmlQuickPricerAnonymousPassword, pmlLenderSiteId, out loginProblem, true, isAnonymousQP: true);

                        if (PriceMyLoan.Security.PrincipalFactory.E_LoginProblem.None != loginProblem)
                        {
                            throw new AccessDenied();
                        }
                    }
                    else
                    {
                        principal = (AbstractUserPrincipal)PriceMyLoan.Security.PrincipalFactory.CreateUsingAnonymousQuickPricerUserId(pmlLenderSiteId, anonymousId);
                    }
                }
                else
                {
                    // Quick Pricer access by authenticate users. Pull information from cookie.
                    HttpCookie cookie = Request.Cookies.Get(ConstAppDavid.AnonymousQpCookieName);
                    if (null != cookie)
                    {
                        string cookieValue = cookie.Value;
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookieValue);
                        if (ticket.Expired == false)
                        {
                            principal = PriceMyLoan.Security.PrincipalFactory.Create(new FormsIdentity(ticket), isAnonymousQP: true) as AbstractUserPrincipal;
                        }
                    }
                }
            }
            catch (AccessDenied)
            {
                throw;
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw new CSessionExpiredException();
            }

            if (null == principal)
            {
                throw new CSessionExpiredException();
            }

            return principal;
        }

        private PriceMyLoanPrincipal anonQpPrincipal = null;
        public override PriceMyLoanPrincipal CustomPrincipal
        {
            get
            {
                if (anonQpPrincipal == null)
                {
                    anonQpPrincipal = GetPrincipal() as PriceMyLoanPrincipal;
                }

                return anonQpPrincipal;
            }
        }

        protected bool m_allowGoToPipeline
        {
            get { return CustomPrincipal.IsAllowGoToPipeline; }
        }

        protected bool IsEmbeddedPML
        {
            get
            {
                return PriceMyLoanConstants.IsEmbeddedPML;
            }
        }

        protected string PmlUrl
        {
            get
            {
                if (CustomPrincipal.BrokerDB.IsNewPmlUIEnabled)
                {
                    return VirtualRoot + "/webapp/pml.aspx";
                }
                else
                {
                    return VirtualRoot + "/main/agents.aspx";
                }

            }
        }

        protected override bool IncludeMaterialDesignFiles => false;

        protected override string UtilitiesServiceUrl
        {
            get
            {
                return "/quickpricerservice.aspx";
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.Init += new EventHandler(this.PageInit);
            base.OnInit(e);
        }

        void PageInit(object sender, EventArgs e)
        {
            this.RegisterService("main", "/quickpricerservice.aspx");
            IncludeStyleSheet("~/css/quickpricer.css");
            RegisterJsScript("quickpricerresult.js");
            RegisterJsScript("LQBNestedFrameSupport.js");

            m_topLinkPanel.Visible = !IsEmbeddedPML && m_allowGoToPipeline;

            if (RequestHelper.GetBool("if"))
            {
                m_topLinkPanel.Visible = false;
            }

            SetRequiredValidatorMessage(CreateLoan_BorrFirstNameValidator);
            SetRequiredValidatorMessage(CreateLoan_BorrLastNameValidator);
            SetRequiredValidatorMessage(CreateLoan_BorrSsnValidator);
            CreateLoanTable.Visible = !IsEmbeddedPML;

            this.QuickPricer1.IsForAnonymousQp = true;
            this.QuickPricerResult1.IsForAnonymousQp = true;
        }

        protected void SetRequiredValidatorMessage(System.Web.UI.WebControls.BaseValidator v)
        {
            HtmlImage img = new HtmlImage();
            img.Src = ResolveUrl("~/images/error_pointer.gif");
            v.Controls.Add(img);
        }

    }
}
