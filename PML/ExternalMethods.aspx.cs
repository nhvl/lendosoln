﻿namespace PriceMyLoan
{
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using PrincipalFactory = LendersOffice.Security.PrincipalFactory;
    using PriceMyLoan.ObjLib;
    using PriceMyLoan.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using global::DataAccess;

    /// <summary>
    /// This is a service page for external pages to call methods from the user's browser, using their cookie to impersonate them.
    /// The page enables CORS "simple requests", which means only GET/POST requests with application/x-www-form-urlencoded Content-Type. (Web Forms makes non-simple requests difficult)
    /// </summary>
    public partial class ExternalMethods : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets a dictionary of methods that this page provides.
        /// </summary>
        private Dictionary<string, Action> Methods { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalMethods"/> class.
        /// </summary>
        public ExternalMethods()
        {
            this.Methods = new Dictionary<string, Action>(StringComparer.OrdinalIgnoreCase)
            {
                { nameof(this.GetMethods), this.GetMethods },
                { nameof(this.GetUserInfo), this.GetUserInfo },
                { nameof(this.CreateLoan), this.CreateLoan }
            };
        }

        /// <summary>
        /// Processes the method request.
        /// </summary>
        /// <param name="methodName">The name of the method being invoked.</param>
        protected override void Process(string methodName)
        {
            if (this.Methods.ContainsKey(methodName))
            {
                this.Methods[methodName].Invoke();
            }
            else
            {
                throw new CBaseException($"The requested method does not exist: {methodName}.", $"Origin: {HttpContext.Current.Request.Headers["Origin"]}; Method: {methodName}; ");
            }
        }

        /// <summary>
        /// Event handler which contains page load logic to handle cross-origin requests.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var request = HttpContext.Current.Request;
            if (request.HttpMethod != "POST" && request.HttpMethod != "OPTIONS")
            {
                throw new Exception("This page is only available through POST requests.");
            }

            var requestOrigin = new Uri(request.Headers["Origin"]);
            var user = PrincipalFactory.CurrentPrincipal;
            var pmlUser = user as PriceMyLoanPrincipal;
            if (pmlUser != null)
            {
                var navigationManager = new PMLNavigationManager(pmlUser, this.ResolveClientUrl);
                if (!navigationManager.PagesWithAccessRecursive().Any(node => 
                    !node.isFolder 
                    && node.ConfigHref != null 
                    && node.ProvideSystemAccess
                    && Uri.Compare(new Uri(node.ConfigHref), requestOrigin, UriComponents.Scheme | UriComponents.HostAndPort, UriFormat.SafeUnescaped, StringComparison.OrdinalIgnoreCase) == 0))
                {
                    return;
                }
            }

            if (!ConstStage.CrossOriginUrls.Any(url => Uri.Compare(requestOrigin, url, UriComponents.Scheme | UriComponents.HostAndPort, UriFormat.SafeUnescaped, StringComparison.OrdinalIgnoreCase) == 0))
            {
                // The browser should reject the request without the Access-Control headers.
                PaulBunyanHelper.Log(LendersOffice.Logging.LoggingLevel.Error, null, $"Cross-Origin request from an origin not in ConstStage: {requestOrigin}", null);
                return;
            }

            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", request.Headers["Origin"]);
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Credentials", "true");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Methods", "POST");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Headers", "Content-Type, Accept");

            PaulBunyanHelper.Log(LendersOffice.Logging.LoggingLevel.Info, "Cross-Origin Request", 
                $"Cross-Origin request from Origin: {request.Headers["Origin"]} to page: {request.Url} on behalf of user {user.DisplayName} ({PrincipalFactory.CurrentPrincipal.LoginNm}).{Environment.NewLine}" +
                $"Method: {RequestHelper.GetSafeQueryString("method")}",
                null);
        }

        /// <summary>
        /// Gets a list of the available methods from this endpoint.
        /// </summary>
        protected void GetMethods()
        {
            SetResult("MethodNames", SerializationHelper.JsonNetAnonymousSerialize(this.Methods.Keys));
        }

        /// <summary>
        /// Gets information about the current user.
        /// </summary>
        protected void GetUserInfo()
        {
            var user = PrincipalFactory.CurrentPrincipal;
            SetResult("FullName", user.DisplayName);
            SetResult("UserName", user.LoginNm);
        }

        /// <summary>
        /// A fake example method which takes input and processes it to produce the output.
        /// </summary>
        protected void CreateLoan()
        {
            SetResult("success", true);
            SetResult("FullName", $"{GetString("FirstName")} {GetString("LastName")}");
            SetResult("sLRefNum", Guid.NewGuid());
        }
    }
}