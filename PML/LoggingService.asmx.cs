﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DataAccess;
using LendersOffice.Email;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace PriceMyLoan
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LoggingService : System.Web.Services.WebService
    {
        [WebMethod]
        public void LogJSError(string message, string url, int lineNumber, string[] lines, string windowLocation, string referrer, string opener)
        {
            LogAndSendEmailToSupport(message, url, lineNumber, lines, null, null, windowLocation, referrer, opener);
        }
        [WebMethod]
        public void LogJSException(string message, string url, int lineNumber, string[] lines, string[] stack, string msgFromDeveloper, string windowLocation, string referrer, string opener)
        {
            LogAndSendEmailToSupport(message, url, lineNumber, lines, stack, msgFromDeveloper, windowLocation, referrer, opener);
        }

        [WebMethod]
        public void LogJSDeveloperError(string message, string url, string[] stack, string windowLocation, string referrer, string opener)
        {
            string msg = "JSDeveloperError:" + Environment.NewLine +
                "Message: " + message + Environment.NewLine +
                "JS URL: " + url + Environment.NewLine +
                "JS Callstack: " + string.Join(Environment.NewLine + "\t", stack) + Environment.NewLine +
                "Window Location: " + windowLocation + Environment.NewLine +
                "Referrer: " + referrer + Environment.NewLine +
                "Opener: " + opener;

            Tools.LogErrorWithCriticalTracking(msg);
        }

        private void LogAndSendEmailToSupport(string message, string url, int lineNumber, string[] lines, string[] stack, string msgFromDeveloper, string windowLocation, string referrer, string opener)
        {
            HttpContext httpContext = HttpContext.Current;

            string context = "not detected from httpContext.";
            string userAgent = "not detected from httpContext.";
            if (httpContext != null) 
            {
                try
                {
                    context = httpContext.Request.Url.Host;
                    userAgent = httpContext.Request.UserAgent;
                }
                catch (HttpException)
                {
                }
            }


            string body =
                "Date: " + DateTime.Now.ToString() + Environment.NewLine +
                "JSERROR: " + Environment.NewLine;


            var p = PrincipalFactory.CurrentPrincipal; 
            if (p != null)
            {
                body +=
                    "LoginName: " + p.LoginNm + Environment.NewLine;
            }

            body +=
                "JS Message: " + message + Environment.NewLine +
                "Line Number: " + lineNumber + Environment.NewLine;

            if(!string.IsNullOrEmpty(msgFromDeveloper))
            {
                body += 
                    "Msg from developer: " + msgFromDeveloper + Environment.NewLine;
            }
            body +=
                "Lines (guessing):" + Environment.NewLine + string.Join("\t" + Environment.NewLine, lines) + Environment.NewLine +
                "JS URL: " + url + Environment.NewLine +
                "Host: " + context + Environment.NewLine +
                "UserAgent: " + userAgent + Environment.NewLine +
                "Window Location: " + windowLocation + Environment.NewLine +
                "Referrer: " + referrer + Environment.NewLine +
                "Opener: " + opener + Environment.NewLine
                ;
                

            if (stack != null)
            {
               body +=  
                   "Callstack (from exception): " + Environment.NewLine + 
                   "\t" + string.Join(Environment.NewLine + "\t", stack);
            }

            Tools.LogBug(body);

            // Don't send email out if it's not on production.  (Don't use Const
            if (ConstStage.FileDBSiteCode == "LOBETA" || ConstStage.FileDBSiteCode == "LODEMO")
            {
                return;
            }
            
            //! add back when ready to.
            /*LendersOffice.Common.EmailUtilities.SmtpSend(new CBaseEmail()
            {
                From = "scottk@meridianlink.com",
                To = "support@lendingqb.com",
                Subject = "[PML Javascript Error] - " + message,
                Message = body + Environment.NewLine +
                    "Instructions: Please flag, mark as subcase, or duplicate, depending on the login, host and JS message."
               ,
            });*/
            
        }
    }

}
