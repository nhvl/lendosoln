function radialProgress(parent) {
	var _data = null, _duration = 1, _selection, _margin = {
		top : 0,
		right :10,
		bottom : 10,
		left : 0
	}, __width = 150, __height = 150, _diameter = 150, _label = "", _fontSize = 26;

	var _mouseClick;

	var _value = 0, _minValue = 0, _maxValue = 100;

	var _currentArc = 0, _currentArc2 = 0, _currentValue = 0;

	var _arc = d3.svg.arc().startAngle(0 * (Math.PI / 180)); //just radians

	var _arc2 = d3.svg.arc().startAngle(0 * (Math.PI / 180)).endAngle(0); //just radians

	_selection = d3.select(parent);

	function component() {

		_selection.each(function(data) {

			// Select the svg element, if it exists.
			var svg = d3.select(this).selectAll("svg").data([ data ]);

			var enter = svg.enter().append("svg").attr("class", "radial-svg")
					.append("g");

			measure();

			svg.attr("width", __width).attr("height", __height);

			var background = enter.append("g").attr("class", "component").attr(
					"cursor", "pointer").on("click", onMouseClick);

			_arc.endAngle(360 * (Math.PI / 180))

			background.append("rect").attr("class", "background").attr("width",
					_width).attr("height", _height);

			background.append("path").attr("transform",
					"translate(" + _width / 2 + "," + _width / 2 + ")").attr(
					"d", _arc);
			//            if (_value <= 100){
			//            background.append("text")
			//                .attr("class", "label")
			//                .attr("transform", "translate(" + _width / 2 + "," + (_width + _fontSize) + ")")
			//                .text(_label);
			//            }
			//            else{
			//            	background.append("text")
			//                .attr("class", "label1")
			//                .attr("transform", "translate(" + _width / 2 + "," + (_width + _fontSize) + ")")
			//                .text(_label1);
			//            }
			background.append("text").attr("class", "label1").attr(
					"transform",
					"translate(" + _width / 2 + "," + (_width + _fontSize)
							+ ")").text(_label);

			var g = svg.select("g").attr("transform",
					"translate(" + _margin.left + "," + _margin.top + ")");

			_arc.endAngle(_currentArc);
			enter.append("g").attr("class", "arcs");
			var path = svg.select(".arcs").selectAll(".arc").data(data);
			path.enter().append("path").attr("class", "arc").attr("transform",
					"translate(" + _width / 2 + "," + _width / 2 + ")").attr(
					"d", _arc);

			//Another path in case we exceed 100%
			var path2 = svg.select(".arcs").selectAll(".arc2").data(data);
			path2.enter().append("path").attr("class", "arc2").attr(
					"transform",
					"translate(" + _width / 2 + "," + _width / 2 + ")").attr(
					"d", _arc2);

			enter.append("g").attr("class", "labels");
			var label = svg.select(".labels").selectAll(".label").data(data);
			if (_value <= 100) {
				label.enter().append("text").attr("class", "label").attr("y",
						_width / 2 + _fontSize / 3).attr("x", _width / 2).attr(
						"cursor", "pointer").attr("width", _width)
				// .attr("x",(3*_fontSize/2))
				.text(
						function(d) {
							//alert(_value + "%");
							return (_value) + "%"
						}).style("font-size", _fontSize + "px").on("click",
						onMouseClick);
			} else {
				label.enter().append("text").attr("class", "label1").attr("y",
						_width / 2 + _fontSize / 3).attr("x", _width / 2).attr(
						"cursor", "pointer").attr("width", _width)
				// .attr("x",(3*_fontSize/2))
				.text(
						function(d) {
							//alert(_value + "%");

							return (_value) + "%"
						}).style("font-size", _fontSize + "px").on("click",
						onMouseClick);
			}

			path.exit().transition().duration(1).attr("x", 1000).remove();

			layout(svg);

			function layout(svg) {

				var ratio = (_value - _minValue) / (_maxValue - _minValue);
				var endAngle = Math.min(360 * ratio, 360);
				endAngle = endAngle * Math.PI / 180;

				path.datum(endAngle);
				path.transition().duration(_duration).attrTween("d", arcTween);

				if (ratio > 1) {
					path2.datum(Math.min(360 * (ratio - 1), 360) * Math.PI
							/ 180);
					path2.transition().delay(_duration).duration(_duration)
							.attrTween("d", arcTween2);
				}

				label.datum(ratio * 100);
				label.transition().duration(_duration)
						.tween("text", labelTween);

			}

		});

		function onMouseClick(d) {
			if (typeof _mouseClick == "function") {
				_mouseClick.call();
			}
		}
	}

	function labelTween(a) {
		var i = d3.interpolate(_currentValue, a);
		_currentValue = i(0);

		return function(t) {
			_currentValue = i(t);

			this.textContent = (i(t).toFixed(2)) + "%";
		}
	}

	function arcTween(a) {
		var i = d3.interpolate(_currentArc, a);

		return function(t) {
			_currentArc = i(t);
			return _arc.endAngle(i(t))();
		};
	}

	function arcTween2(a) {
		var i = d3.interpolate(_currentArc2, a);

		return function(t) {
			return _arc2.endAngle(i(t))();
		};
	}

	function measure() {
		_width = _diameter - _margin.right - _margin.left - _margin.top
				- _margin.bottom;
		_height = _width;
		_fontSize = 26;
		_arc.outerRadius(55);
		_arc.innerRadius(50);
		_arc2.outerRadius(55);
		_arc2.innerRadius(50);

	}

	component.render = function() {
		measure();
		component();
		return component;
	}

	component.value = function(_) {
		if (!arguments.length)
			return _value;
		_value = [ _ ];
		_selection.datum([ _value ]);
		return component;
	}

	component.margin = function(_) {
		if (!arguments.length)
			return _margin;
		_margin = _;
		return component;
	};

	component.diameter = function(_) {
		if (!arguments.length)
			return _diameter
		_diameter = _;
		return component;
	};

	component.minValue = function(_) {
		if (!arguments.length)
			return _minValue;
		_minValue = _;
		return component;
	};

	component.maxValue = function(_) {
		if (!arguments.length)
			return _maxValue;
		_maxValue = _;
		return component;
	};

	component.label = function(_) {
		if (!arguments.length)
			return _label;
		_label = _;
		return component;
	};

	component._duration = function(_) {
		if (!arguments.length)
			return _duration;
		_duration = _;
		return component;
	};

	return component;

}

function start(divname, percent) {
	var rp1 = radialProgress(document.getElementById(divname)).diameter(200)
			.value(percent).render();

}