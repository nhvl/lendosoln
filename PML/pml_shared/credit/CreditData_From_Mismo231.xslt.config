﻿<?xml version="1.0" encoding="UTF-8"?>
<!-- This stylesheet is used to transform MISMO 2.3.1 into intermediate XML data -->
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:util="urn:XsltUtil"
				exclude-result-prefixes="util">

	<xsl:output method="xml"
				version="1.0"
				encoding="UTF-8" />

	<!-- These parameters should be provided when calling this stylesheet. -->
	<!-- The FNMA Agency ID from the request. -->
	<xsl:param name="AgencyID" />
	<!-- The FNMA Agency Name corresponding to the above Agency ID -->
	<xsl:param name="AgencyName" />

	<xsl:variable name="RESPONSE"
				  select="/RESPONSE_GROUP/RESPONSE" />
	<xsl:variable name="CREDIT_RESPONSE"
				  select="$RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE" />
	<xsl:variable name="BORROWER"
				  select="$CREDIT_RESPONSE/BORROWER[@_PrintPositionType='Borrower']" />
	<xsl:variable name="COBORROWER"
				  select="$CREDIT_RESPONSE/BORROWER[@_PrintPositionType='CoBorrower']" />
	<xsl:variable name="g_sBorrowerCode">
		<xsl:value-of select="$BORROWER/@BorrowerID"/>
	</xsl:variable>
	<xsl:variable name="g_sCoBorrowerCode">
		<xsl:if test="$COBORROWER">
			<xsl:value-of select="$COBORROWER/@BorrowerID"/>
		</xsl:if>
	</xsl:variable>

	<xsl:template match="/">
		<CREDITDATA>
			<HEADER>
				<xsl:variable name="HasEQF"
							  select="count($CREDIT_RESPONSE/CREDIT_REPOSITORY_INCLUDED[@_EquifaxIndicator='Y']) &gt; 0" />
				<xsl:variable name="HasEXP"
							  select="count($CREDIT_RESPONSE/CREDIT_REPOSITORY_INCLUDED[@_ExperianIndicator='Y']) &gt; 0" />
				<xsl:variable name="HasTUC"
							  select="count($CREDIT_RESPONSE/CREDIT_REPOSITORY_INCLUDED[@_TransUnionIndicator='Y']) &gt; 0" />
				<xsl:variable name="HasFICO"
							  select="count($CREDIT_RESPONSE/CREDIT_SCORE[@CreditRepositorySourceType='Other' and @_ModelNameType='FICOExpansionScore']) &gt; 0" />
				<xsl:attribute name="report_type">
					<xsl:choose>
						<xsl:when test="$HasEQF">
							<xsl:choose>
								<xsl:when test="$HasEXP">
									<xsl:choose>
										<xsl:when test="$HasTUC">
											<xsl:choose>
												<xsl:when test="$HasFICO">16</xsl:when>
												<xsl:otherwise>01</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>05</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="$HasTUC">06</xsl:when>
								<xsl:otherwise>02</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$HasEXP">
							<xsl:choose>
								<xsl:when test="$HasTUC">07</xsl:when>
								<xsl:otherwise>03</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$HasTUC">04</xsl:when>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="agency_id">
					<xsl:value-of select="$AgencyID" />
				</xsl:attribute>
				<xsl:attribute name="agency_name">
					<xsl:value-of select="$AgencyName" />
				</xsl:attribute>
				<xsl:attribute name="joint_indicator">
					<xsl:choose>
						<xsl:when test="$COBORROWER">J</xsl:when>
						<xsl:otherwise>I</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="report_number">
					<xsl:value-of select="$CREDIT_RESPONSE/@CreditReportIdentifier" />
				</xsl:attribute>
			</HEADER>

			<SUBJECT>
				<xsl:attribute name="report_date">
					<xsl:value-of select="util:InquiryDate($CREDIT_RESPONSE/@CreditReportFirstIssuedDate)"/>
				</xsl:attribute>

				<xsl:attribute name="lastname">
					<xsl:value-of select="$BORROWER/@_LastName"/>
				</xsl:attribute>
				<xsl:attribute name="firstname">
					<xsl:value-of select="$BORROWER/@_FirstName"/>
				</xsl:attribute>
				<xsl:attribute name="middlename">
					<xsl:value-of select="$BORROWER/@_MiddleName"/>
				</xsl:attribute>
				<xsl:attribute name="suffix">
					<xsl:value-of select="$BORROWER/@_NameSuffix"/>
				</xsl:attribute>
				<xsl:attribute name="ssn">
					<xsl:value-of select="util:Ssn($BORROWER/@_SSN)"/>
				</xsl:attribute>
				<xsl:if test="$COBORROWER">
					<xsl:attribute name="colastname">
						<xsl:value-of select="$COBORROWER/@_LastName"/>
					</xsl:attribute>
					<xsl:attribute name="cofirstname">
						<xsl:value-of select="$COBORROWER/@_FirstName"/>
					</xsl:attribute>
					<xsl:attribute name="comiddlename">
						<xsl:value-of select="$COBORROWER/@_MiddleName"/>
					</xsl:attribute>
					<xsl:attribute name="cosuffix">
						<xsl:value-of select="$COBORROWER/@_NameSuffix"/>
					</xsl:attribute>
					<xsl:attribute name="cossn">
						<xsl:value-of select="util:Ssn($COBORROWER/@_SSN)"/>
					</xsl:attribute>
				</xsl:if>
			</SUBJECT>

			<xsl:if test="$BORROWER/_RESIDENCE[@BorrowerResidencyType='Current']">
				<xsl:call-template name="ADDRESS">
					<xsl:with-param name="a_xmlAddress"
									select="$BORROWER/_RESIDENCE[@BorrowerResidencyType='Current']"/>
					<xsl:with-param name="a_BorrowerType">B</xsl:with-param>
					<xsl:with-param name="a_AddressType">1</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$BORROWER/_RESIDENCE[@BorrowerResidencyType='Prior']">
				<xsl:call-template name="ADDRESS">
					<xsl:with-param name="a_xmlAddress"
									select="$BORROWER/_RESIDENCE[@BorrowerResidencyType='Prior']"/>
					<xsl:with-param name="a_BorrowerType">
						<xsl:choose>
							<xsl:when test="count($CREDIT_RESPONSE/BORROWER) &gt; 1">C</xsl:when>
							<xsl:otherwise>B</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="a_AddressType">2</xsl:with-param>
				</xsl:call-template>
			</xsl:if>

			<xsl:call-template name="PUBLIC_RECORD">
				<xsl:with-param name="a_xmlPublicRecords"
								select="$CREDIT_RESPONSE/CREDIT_PUBLIC_RECORD"/>
			</xsl:call-template>

			<xsl:call-template name="INQUIRY">
				<xsl:with-param name="a_xmlInquiries"
								select="$CREDIT_RESPONSE/CREDIT_INQUIRY"/>
			</xsl:call-template>

			<xsl:call-template name="TRADELINE">
				<xsl:with-param name="a_xmlTradelines"
								select="$CREDIT_RESPONSE/CREDIT_LIABILITY"/>
			</xsl:call-template>

			<xsl:call-template name="RISK_MODEL">
				<xsl:with-param name="a_xmlScores"
								select="$CREDIT_RESPONSE/CREDIT_SCORE"/>
			</xsl:call-template>

			<xsl:call-template name="FILE_VARIATIONS">
				<xsl:with-param name="a_xmlVariations"
								select="$CREDIT_RESPONSE/CREDIT_FILE"/>
			</xsl:call-template>

			<TRAILER></TRAILER>
		</CREDITDATA>
	</xsl:template>

	<xsl:template name="ADDRESS">
		<xsl:param name="a_xmlAddress"/>
		<xsl:param name="a_BorrowerType"/>
		<xsl:param name="a_AddressType"/>
		<ADDRESS>
			<xsl:attribute name="borrower_indicator">
				<xsl:value-of select="$a_BorrowerType"/>
			</xsl:attribute>
			<xsl:attribute name="present_indicator">
				<xsl:value-of select="$a_AddressType"/>
			</xsl:attribute>
			<xsl:attribute name="streetaddress">
				<xsl:value-of select="$a_xmlAddress/@_StreetAddress"/>
			</xsl:attribute>
			<xsl:attribute name="city">
				<xsl:value-of select="$a_xmlAddress/@_City"/>
			</xsl:attribute>
			<xsl:attribute name="state">
				<xsl:value-of select="$a_xmlAddress/@_State"/>
			</xsl:attribute>
			<xsl:attribute name="zip">
				<xsl:value-of select="$a_xmlAddress/@_PostalCode"/>
			</xsl:attribute>
			<xsl:attribute name="source_bureau">8</xsl:attribute>
		</ADDRESS>
	</xsl:template>

	<xsl:template name="PUBLIC_RECORD">
		<xsl:param name="a_xmlPublicRecords"/>

		<xsl:for-each select="$a_xmlPublicRecords">
			<PUBLIC_RECORD>
				<xsl:variable name="TypeCode"
							  select="util:PublicRecordTypeCode(@_Type)" ></xsl:variable>

				<xsl:attribute name="pubrec_class_code">
					<xsl:value-of select="util:PublicRecordClass($TypeCode)" />
				</xsl:attribute>
				<xsl:attribute name="pubrec_type_code">
					<xsl:value-of select="$TypeCode" />
				</xsl:attribute>
				<xsl:attribute name="filed_date">
					<xsl:value-of select="util:FormatMonth(@_FiledDate)" />
				</xsl:attribute>
				<xsl:attribute name="satisfied_date">
					<xsl:value-of select="util:FormatMonth(@_DispositionDate)" />
				</xsl:attribute>
				<xsl:attribute name="status_code">
					<xsl:value-of select="util:PublicRecordStatusCode(@_DispositionType)" />
				</xsl:attribute>
				<xsl:attribute name="ecoa">
					<xsl:value-of select="util:ECOA(@_AccountOwnershipType)" />
				</xsl:attribute>
				<xsl:attribute name="action_amount">
					<xsl:value-of select="@_LegalObligationAmount"/>
				</xsl:attribute>
				<xsl:attribute name="asset_amount"></xsl:attribute>
				<xsl:attribute name="liability_amount"></xsl:attribute>
				<xsl:attribute name="exempt_amount"></xsl:attribute>

				<xsl:variable name="CREDIT_COMMENT"
							  select="CREDIT_COMMENT[string-length(@_Code) &gt; 0]" />

				<xsl:attribute name="remark_code_1">
					<xsl:if test="count($CREDIT_COMMENT) &gt; 0">
						<xsl:value-of select="util:RemarkCode($CREDIT_COMMENT[1]/@_SourceType, $CREDIT_COMMENT[1]/@_Code)" />
					</xsl:if>
				</xsl:attribute>

				<xsl:attribute name="remark_code_2">
					<xsl:if test="count($CREDIT_COMMENT) &gt; 1">
						<xsl:value-of select="util:RemarkCode($CREDIT_COMMENT[2]/@_SourceType, $CREDIT_COMMENT[2]/@_Code)" />
					</xsl:if>
				</xsl:attribute>

				<xsl:attribute name="source_bureau">
					<xsl:call-template name="CalculateSourceBureau">
						<xsl:with-param name="a_xmlNode"
										select="."/>
					</xsl:call-template>
				</xsl:attribute>
			</PUBLIC_RECORD>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="INQUIRY">
		<xsl:param name="a_xmlInquiries"/>
		<xsl:for-each select="$a_xmlInquiries">
			<INQUIRY>
				<xsl:attribute name="inquiry_name">
					<xsl:value-of select="@_Name" />
				</xsl:attribute>
				<xsl:attribute name="inquiry_date">
					<xsl:value-of select="util:InquiryDate(@_Date)" />
				</xsl:attribute>
				<xsl:attribute name="source_bureau">
					<xsl:call-template name="CalculateSourceBureau">
						<xsl:with-param name="a_xmlNode"
										select="."/>
					</xsl:call-template>
				</xsl:attribute>
			</INQUIRY>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="TRADELINE">
		<xsl:param name="a_xmlTradelines"/>
		<xsl:for-each select="$a_xmlTradelines">
			<TRADELINE>
				<xsl:variable name="PAYMENT_PATTERN"
							  select="_PAYMENT_PATTERN"/>
				<xsl:variable name="PaymentHistory">
					<xsl:if test="$PAYMENT_PATTERN">
						<xsl:value-of select="util:HistoryMOP($PAYMENT_PATTERN/@_Data)"/>
					</xsl:if>
				</xsl:variable>

				<xsl:attribute name="ecoa">
					<xsl:value-of select="util:ECOA(@_AccountOwnershipType)" />
				</xsl:attribute>

				<xsl:attribute name="subscriber_number">
					<xsl:variable name="CreditTradeReferenceID"
								  select="@CreditTradeReferenceID"/>
					<xsl:variable name="CREDIT_TRADE_REFERENCE"
								  select="$CREDIT_RESPONSE/CREDIT_TRADE_REFERENCE[@CreditTradeReferenceID=$CreditTradeReferenceID]"/>
					<xsl:if test="$CREDIT_TRADE_REFERENCE">
						<xsl:if test="$CREDIT_TRADE_REFERENCE/CREDIT_REPOSITORY">
							<xsl:value-of select="$CREDIT_TRADE_REFERENCE/CREDIT_REPOSITORY/@_SubscriberCode"/>
						</xsl:if>
					</xsl:if>
				</xsl:attribute>

				<xsl:attribute name="creditor_name">
					<xsl:if test="_CREDITOR and _CREDITOR/@_Name">
						<xsl:value-of select="_CREDITOR/@_Name"/>
					</xsl:if>
				</xsl:attribute>
				<xsl:attribute name="account_number">
					<xsl:value-of select="@_AccountIdentifier" />
				</xsl:attribute>
				<xsl:attribute name="account_owner">
					<xsl:choose>
						<xsl:when test="@BorrowerID=$g_sBorrowerCode">1</xsl:when>
						<xsl:when test="@BorrowerID=$g_sCoBorrowerCode">2</xsl:when>
						<xsl:otherwise>
							<xsl:if test="@_AccountOwnershipType='Joint'">0</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="mop">
					<xsl:if test="_CURRENT_RATING">
						<xsl:value-of select="util:MOPFromRating(_CURRENT_RATING/@_Type)" />
					</xsl:if>
				</xsl:attribute>

				<xsl:variable name="AccountTypeCode"
							  select="util:AccountTypeCode(@CreditLoanType)" />

				<xsl:attribute name="credit_type_code">
					<xsl:value-of select="util:CreditTypeCode(@_AccountType, $AccountTypeCode)" />
				</xsl:attribute>
				<xsl:attribute name="account_type_code">
					<xsl:value-of select="$AccountTypeCode" />
				</xsl:attribute>
				<xsl:attribute name="months_reviewed">
					<xsl:value-of select="@_MonthsReviewedCount" />
				</xsl:attribute>
				<xsl:attribute name="date_opened">
					<xsl:value-of select="util:FormatMonth(@_AccountOpenedDate)" />
				</xsl:attribute>
				<xsl:attribute name="date_reported">
					<xsl:value-of select="util:FormatMonth(@_AccountReportedDate)" />
				</xsl:attribute>
				<xsl:attribute name="high_credit">
					<xsl:value-of select="@_HighCreditAmount" />
				</xsl:attribute>
				<xsl:attribute name="payment_type">
					<xsl:value-of select="util:AccountTermsTypeCode(@_TermsSourceType)" />
				</xsl:attribute>
				<xsl:attribute name="payment">
					<xsl:value-of select="@_MonthlyPaymentAmount" />
				</xsl:attribute>
				<xsl:attribute name="term">
					<xsl:value-of select="@_TermsMonthsCount" />
				</xsl:attribute>
				<xsl:attribute name="balance">
					<xsl:value-of select="@_UnpaidBalanceAmount" />
				</xsl:attribute>
				<xsl:attribute name="status_code">
					<xsl:value-of select="util:AccountStatusCode(@_AccountStatusType)" />
				</xsl:attribute>
				<xsl:attribute name="status_date">
					<xsl:value-of select="util:FormatMonth(@_AccountStatusDate)" />
				</xsl:attribute>
				<xsl:attribute name="last_paid_date">
					<xsl:if test="$PAYMENT_PATTERN">
						<xsl:value-of select="util:FormatMonth($PAYMENT_PATTERN/@_StartDate)" />
					</xsl:if>
				</xsl:attribute>
				<xsl:attribute name="payment_profile">
					<xsl:value-of select="$PaymentHistory" />
				</xsl:attribute>

				<xsl:variable name="HIGHEST_ADVERSE_RATING"
							  select="_HIGHEST_ADVERSE_RATING"/>
				<xsl:choose>
					<xsl:when test="HIGHEST_ADVERSE_RATING">
						<xsl:attribute name="max_mop">
							<xsl:value-of select="util:MOPFromRating($HIGHEST_ADVERSE_RATING/@_Type)" />
						</xsl:attribute>
						<xsl:attribute name="max_delinquency_date">
							<xsl:value-of select="util:FormatMonth($HIGHEST_ADVERSE_RATING/@_Date)" />
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="max_mop"/>
						<xsl:attribute name="max_delinquency_date"/>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:attribute name="past_due">
					<xsl:value-of select="@_PastDueAmount" />
				</xsl:attribute>

				<xsl:variable name="LATE_COUNT"
							  select="_LATE_COUNT"/>
				<xsl:choose>
					<xsl:when test="$LATE_COUNT">
						<xsl:attribute name="day_30">
							<xsl:value-of select="$LATE_COUNT/@_30Days" />
						</xsl:attribute>
						<xsl:attribute name="day_60">
							<xsl:value-of select="$LATE_COUNT/@_60Days" />
						</xsl:attribute>
						<xsl:attribute name="day_90">
							<xsl:value-of select="$LATE_COUNT/@_90Days" />
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="day_30"/>
						<xsl:attribute name="day_60"/>
						<xsl:attribute name="day_90"/>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:variable name="xmlLastYear"
							  select="util:LateCount(substring($PaymentHistory, 1, 12))" />
							  <!-- dd 11/30/2012 - The calculation of these late counts are incorrected. It should use "reported date" instead of payment start date-->
				<xsl:attribute name="day_30_12">
				<!--
					<xsl:value-of select="$xmlLastYear/@_30Days" />
				-->
				</xsl:attribute>
				<xsl:attribute name="day_60_12">
				<!--
					<xsl:value-of select="$xmlLastYear/@_60Days" />
					-->
				</xsl:attribute>
				<xsl:attribute name="day_90_12">
				<!--
					<xsl:value-of select="$xmlLastYear/@_90Days" />
					-->
				</xsl:attribute>
				<xsl:variable name="xmlSecondYear"
							  select="util:LateCount(substring($PaymentHistory, 13, 12))" />
				<xsl:attribute name="day_30_24">
				<!--
					<xsl:value-of select="$xmlSecondYear/@_30Days" />
					-->
				</xsl:attribute>
				<xsl:attribute name="day_60_24">
				<!--
					<xsl:value-of select="$xmlSecondYear/@_60Days" />
					-->
				</xsl:attribute>
				<xsl:attribute name="day_90_24">
				<!--
					<xsl:value-of select="$xmlSecondYear/@_90Days" />
					-->
				</xsl:attribute>
				<xsl:variable name="xmlThirdYear"
							  select="util:LateCount(substring($PaymentHistory, 25, 12))" />
				<xsl:attribute name="day_30_36">
				<!--
					<xsl:value-of select="$xmlThirdYear/@_30Days" />
					-->
				</xsl:attribute>
				<xsl:attribute name="day_60_36">
				<!--
					<xsl:value-of select="$xmlThirdYear/@_60Days" />
					-->
				</xsl:attribute>
				<xsl:attribute name="day_90_36">
				<!--
					<xsl:value-of select="$xmlThirdYear/@_90Days" />
					-->
				</xsl:attribute>

				<xsl:variable name="CREDIT_COMMENT"
							  select="CREDIT_COMMENT[string-length(@_Code) &gt; 0]" />

				<xsl:attribute name="remark_code_1">
					<xsl:if test="count($CREDIT_COMMENT) &gt; 0">
						<xsl:value-of select="util:RemarkCode($CREDIT_COMMENT[1]/@_SourceType, $CREDIT_COMMENT[1]/@_Code)" />
					</xsl:if>
				</xsl:attribute>

				<xsl:attribute name="remark_code_2">
					<xsl:if test="count($CREDIT_COMMENT) &gt; 1">
						<xsl:value-of select="util:RemarkCode($CREDIT_COMMENT[2]/@_SourceType, $CREDIT_COMMENT[2]/@_Code)" />
					</xsl:if>
				</xsl:attribute>

				<xsl:attribute name="remark_code_3">
					<xsl:if test="count($CREDIT_COMMENT) &gt; 2">
						<xsl:value-of select="util:RemarkCode($CREDIT_COMMENT[3]/@_SourceType, $CREDIT_COMMENT[3]/@_Code)" />
					</xsl:if>
				</xsl:attribute>

				<xsl:attribute name="remark_code_4">
					<xsl:if test="count($CREDIT_COMMENT) &gt; 3">
						<xsl:value-of select="util:RemarkCode($CREDIT_COMMENT[4]/@_SourceType, $CREDIT_COMMENT[4]/@_Code)" />
					</xsl:if>
				</xsl:attribute>

				<xsl:attribute name="source_bureau">
					<xsl:call-template name="CalculateSourceBureau">
						<xsl:with-param name="a_xmlNode"
										select="."/>
					</xsl:call-template>
				</xsl:attribute>
			</TRADELINE>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="RISK_MODEL">
		<xsl:param name="a_xmlScores"/>
		<xsl:for-each select="$a_xmlScores">
			<RISK_MODEL>
				<xsl:variable name="ScoringModelID"
							  select="util:ScoringModelID(@CreditRepositorySourceType, @_ModelNameType, @_ModelNameTypeOtherDescription)" />

				<xsl:attribute name="rescore_code">O</xsl:attribute>

				<xsl:attribute name="source_bureau">
					<xsl:choose>
						<xsl:when test="@CreditRepositorySourceType='Equifax'">1</xsl:when>
						<xsl:when test="@CreditRepositorySourceType='Experian'">2</xsl:when>
						<xsl:when test="@CreditRepositorySourceType='TransUnion'">3</xsl:when>
						<xsl:when test="@CreditRepositorySourceType='Other'">
							<xsl:choose>
								<xsl:when test="@_ModelNameType='FICOExpansionScore'">6</xsl:when>
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>

				<xsl:attribute name="model_id">
					<xsl:value-of select="$ScoringModelID" />
				</xsl:attribute>

				<xsl:attribute name="owner_code">
					<xsl:choose>
						<xsl:when test="@BorrowerID=$g_sCoBorrowerCode or @BorrowerID='CoBorrower'">2</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>

				<xsl:choose>
					<xsl:when test="not(@_Value) or string-length(@_Value)=0">
						<xsl:variable name="ExclusionCode"
									  select="_FACTOR[@_Code and string-length(@_Code) &gt; 0]/@_Code" />
						<xsl:attribute name="score">
							<xsl:choose>
								<xsl:when test="number($ExclusionCode) &gt;= 9000 and number($ExclusionCode) &lt;= 9999">
									<xsl:value-of select="number($ExclusionCode)" />
								</xsl:when>
								<xsl:otherwise>999</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="exception_code">
							<xsl:value-of select="util:ExceptionCode($ScoringModelID, $ExclusionCode)" />
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="score">
							<xsl:choose>
								<xsl:when test="number(@_Value) &gt; 0">
									+<xsl:value-of select="format-number(@_Value, '00000')" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@_Value" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="factor_1">
							<xsl:value-of select="util:RiskFactorCode($ScoringModelID, _FACTOR[1]/@_Code)" />
						</xsl:attribute>
						<xsl:attribute name="factor_2">
							<xsl:value-of select="util:RiskFactorCode($ScoringModelID, _FACTOR[2]/@_Code)" />
						</xsl:attribute>
						<xsl:attribute name="factor_3">
							<xsl:value-of select="util:RiskFactorCode($ScoringModelID, _FACTOR[3]/@_Code)" />
						</xsl:attribute>
						<xsl:attribute name="factor_4">
							<xsl:value-of select="util:RiskFactorCode($ScoringModelID, _FACTOR[4]/@_Code)" />
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:variable name="BureauName">
					<xsl:value-of select="@CreditRepositorySourceType"/>
				</xsl:variable>
				<xsl:variable name="BorrowerID">
					<xsl:value-of select="@BorrowerID"/>
				</xsl:variable>
				<xsl:variable name="CreditFileID">
					<xsl:value-of select="@CreditFileID"/>
				</xsl:variable>

				<xsl:variable name="CREDIT_FILE"
							  select="$CREDIT_RESPONSE/CREDIT_FILE[@BorrowerID=$BorrowerID and @CreditRepositorySourceType=$BureauName]" />
        <xsl:if test="$CREDIT_FILE">
				<xsl:attribute name="hit_level">
					<xsl:apply-templates select="$CREDIT_FILE"
										 mode="GetPosition">
						<xsl:with-param name="a_sCreditFileID"
										select="$CreditFileID" />
					</xsl:apply-templates>
				</xsl:attribute>
        </xsl:if>
        <xsl:if test="not($CREDIT_FILE)">
          <!-- 2011-06-03 - dd - I added the default hit level when we could not credit report missing CREDIT_FILE. Hit level is required for TOTAL ScoreCard-->
          <xsl:attribute name="hit_level">
            1
          </xsl:attribute>
        </xsl:if>
			</RISK_MODEL>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="FILE_VARIATIONS">
		<xsl:param name="a_xmlVariations"/>

		<xsl:for-each select="$a_xmlVariations">
			<xsl:if test="@_ResultStatusType='NoFileReturnedError'">
				<ERROR>
					<xsl:attribute name="error_code">
						<xsl:choose>
							<xsl:when test="@BorrowerID=$g_sCoBorrowerCode">
								<xsl:choose>
									<xsl:when test="@CreditRepositorySourceType='Equifax'">2</xsl:when>
									<xsl:when test="@CreditRepositorySourceType='Experian'">5</xsl:when>
									<xsl:when test="@CreditRepositorySourceType='TransUnion'">8</xsl:when>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="@CreditRepositorySourceType='Equifax'">1</xsl:when>
									<xsl:when test="@CreditRepositorySourceType='Experian'">4</xsl:when>
									<xsl:when test="@CreditRepositorySourceType='TransUnion'">7</xsl:when>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="source_bureau">
						<xsl:call-template name="CalculateSourceBureau">
							<xsl:with-param name="a_xmlNode"
											select="."/>
						</xsl:call-template>
					</xsl:attribute>
				</ERROR>
			</xsl:if>
			<xsl:if test="@_ResultStatusType='NoFileReturnedCreditFreeze'">
				<ERROR>
					<xsl:attribute name="error_code">
						<xsl:choose>
							<xsl:when test="@CreditRepositorySourceType='Equifax'">10</xsl:when>
							<xsl:when test="@CreditRepositorySourceType='Experian'">11</xsl:when>
							<xsl:when test="@CreditRepositorySourceType='TransUnion'">12</xsl:when>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="source_bureau">
						<xsl:call-template name="CalculateSourceBureau">
							<xsl:with-param name="a_xmlNode"
											select="."/>
						</xsl:call-template>
					</xsl:attribute>
				</ERROR>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>


	<xsl:template name="CalculateSourceBureau">
		<xsl:param name="a_xmlNode"/>

		<xsl:choose>
			<!-- 
			NOTE: For PRBC and FICO, I think @_SourceType is set to 'Other' and @_SourceTypeOtherDescription is used
			-->
			<xsl:when test="count(CREDIT_REPOSITORY[@_SourceType='MergedData']) &gt; 0">7</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="HasEQF"
							  select="count(CREDIT_REPOSITORY[@_SourceType='Equifax']) &gt; 0" />
				<xsl:variable name="HasEXP"
							  select="count(CREDIT_REPOSITORY[@_SourceType='Experian']) &gt; 0" />
				<xsl:variable name="HasTUC"
							  select="count(CREDIT_REPOSITORY[@_SourceType='TransUnion']) &gt; 0" />
				<xsl:choose>
					<xsl:when test="$HasEQF">
						<xsl:choose>
							<xsl:when test="$HasTUC">
								<xsl:choose>
									<xsl:when test="$HasEXP">7</xsl:when>
									<xsl:otherwise>5</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="$HasEXP">4</xsl:when>
							<xsl:otherwise>1</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$HasTUC">
						<xsl:choose>
							<xsl:when test="$HasEXP">6</xsl:when>
							<xsl:otherwise>3</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$HasEXP">2</xsl:when>
					<xsl:otherwise>8</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="CREDIT_FILE"
				  mode="GetPosition">
		<xsl:param name="a_sCreditFileID" />
		<xsl:if test="@CreditFileID=$a_sCreditFileID">
			<xsl:value-of select="position()" />
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
