/**
 * Custom JQuery plugin for toggling between business and technical message.
 * 
 * Usage:
 * Add the .messageToggle class to the business and technical message elements.
 * Make sure that any messages that should not be displayed initially also have
 * the .messageToggleHidden class.
 * 
 * Enable the message toggling plugin with the javascript:
 * $('#element-containing-all-messages').find('individual-message-selector').messagetoggle(); 
 * 
 * For example:
 * <div id="allMessages">
 *  <div class="message" id="message1">
 *    <span id="businessMessage1" class="messageToggle">Business Message</span>
 *    <span id="technicalMessage1" class="messageToggle messageToggleHidden">Technical Message</span>
 *  </div>
 *  <div class="message" id="message2">
 *    <span id="businessMessage2" class="messageToggle">Business Message</span>
 *    <span id="technicalMessage2" class="messageToggle messageToggleHidden">Technical Message</span>
 *    <a class="messageToggleSwitch" href="javascript:void(0)">Switch Message</a>
 *  </div>
 * </div>
 * 
 * $('#allMessages').find('.messagesContainer').messagetoggle();
 * 
 * In the example above ('.messagesContainer').messagetoggle() could also be used, but it would have worse
 * performance.
 * 
 * Actions:
 * There are three actions that can be performed with the message toggle plugin
 * 
 * Toggle - $('#message1').messagetoggle('toggle') - will hide the currently shown message and 
 * display the currently hidden message
 * 
 * Show Business Message - $('#message1').messagetoggle('showBusinessMessage') - will show the
 * business message and hide the technical message
 * 
 * Show Technical Message - $('#message1').messagetoggle('showTechnicalMessage') - will show
 * the technical message and hide the business message
 * 
 * 
 * Properties:
 * 
 * 
 * 
 * Events:
 * There are two events callbacks can be attached to: onBusinessMessage and onTechnicalMessage.  The
 * callbacks will be triggered when the business and technical messages are shown respectively.
 * 
 * Example Usage:
 * $('#allMessages').find('.messageContainer').messagetoggle({
 *   onTechnicalMessage: function(){ alert('Technical Message!'); }
 *   onBusinessMessage: function(){ alert('Business Message!'); }
 * });
 */
(function ($) {
	'use strict';
	
	var defaults = {
		messageSelector: '.messageToggle',
		messageHiddenClass: 'messageToggleHidden',
		onBusinessMessage: function (){},
		onTechnicalMessage: function (){}
	};
	
	var dataMessageToggle = 'plugin-messagetoggle';
	
	function showMessage(messageToShow, messageToHide, settings) {
		messageToHide.addClass(settings.messageHiddenClass);
		messageToShow.removeClass(settings.messageHiddenClass);
	}
	
	var MessageToggler = function(element, settings) {
		this.element = element;
		this.settings = settings;
		this.init();
	}
	
	MessageToggler.prototype = {
		constructor: MessageToggler,
		
		init: function() {
			var messages = this.element.find(this.settings.messageSelector);
			this.valid = messages.length > 1;
			
			if (this.valid) {
				messages.addClass(this.settings.messageHiddenClass);
				this.businessMessage = messages.first();
				this.technicalMessage = messages.last();
				this.showBusinessMessage();
			}
		},
		
		toggle: function() {
			this.businessMessage.hasClass(this.settings.messageHiddenClass) ? this.showBusinessMessage() : this.showTechnicalMessage();
		},
		
		showBusinessMessage: function() {
			showMessage(this.businessMessage, this.technicalMessage, this.settings);
			this.settings.onBusinessMessage.call(this.element);
		},
		
		showTechnicalMessage: function() {
			showMessage(this.technicalMessage, this.businessMessage, this.settings);
			this.settings.onTechnicalMessage.call(this.element);
		}
	};
	
	function Plugin(option) {
		return this.each(function() {
			var $this = $(this);
			var data = $this.data(dataMessageToggle);
			var newSettings = typeof option === 'object' && option;
			
			if (!data) {
				var settings = $.extend({}, defaults, newSettings);
				data = new MessageToggler($this, settings);
				$this.data(dataMessageToggle, data);
			} else if (newSettings) {
				for (var setting in newSettings) {
					if (newSettings.hasOwnProperty(setting)) {
						data.settings[setting] = newSettings[setting];
					}
				}
			}
			
			if (data.valid && typeof option === 'string' && data[option] instanceof Function) {
				data[option].apply(data);
			}
		});
	}
	
	$.fn.messagetoggle = Plugin;
})(jQuery);