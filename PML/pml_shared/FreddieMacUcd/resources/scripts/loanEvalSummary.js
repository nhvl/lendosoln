function switchTabs(tabId,targetTab) {
	var tabId = tabId;
	 var target_tab_selector = $(targetTab).attr('id')+'Tab';
	 $(tabId +' ul.tabs-list > li a').each(function () {
		 	var tabAnchorLink = this;
		 		var tab = $(tabAnchorLink).attr('id')+'Tab';
			 	if(tab != target_tab_selector)
			 	{
			 		$(tab).removeClass('active').hide();
			 		$(tabAnchorLink).parent().removeClass('active');
			 	}
		});
		$(target_tab_selector).addClass('active').show(); 
		$(targetTab).parent().addClass('active').show();
}	

function showCollapseSections(link) {
	if (link == 'show') {
		$('#dataQualityMsgsTab .table').show();
		$('#dataQualityMsgsTab .table tr').show();
		if($('#dataQualityMsgsTab').find('i').hasClass("ic-dropdown")){
			$('#dataQualityMsgsTab').find('i').addClass("ic-dd-rotate-180");
			$( ".ic-error" ).removeClass("ic-dd-rotate-180");
			$( ".ic-pass" ).removeClass("ic-dd-rotate-180");
			$( ".ic-critical" ).removeClass("ic-dd-rotate-180");
		}
		$("#dataQualityMsgsTab .section-sub-headerDiv").show();
		$("#dataQualityMsgsTab .section-sub-header").show();
		$("#dataQualityMsgsTab .msgTablepadding").show();
		 $( ".showAllInActive" ).switchClass( "showAllInActive", "showAllActive",300,  "easeInQuint" );
	     $( ".hideAllActive" ).switchClass( "hideAllActive", "hideAllInActive", 300, "easeInQuint" );


	}else{
		$("#dataQualityMsgsTab .section-sub-header").hide()
		$("#dataQualityMsgsTab .section-sub-headerDiv").hide();
		$('#dataQualityMsgsTab .table').hide();
		$('#dataQualityMsgsTab .table tr').hide();
		$("#dataQualityMsgsTab .msgTablepadding").hide();
		if($('#dataQualityMsgsTab').find('i').hasClass("ic-dd-rotate-180")){
				$('#dataQualityMsgsTab').find('i').removeClass("ic-dd-rotate-180");
				$( ".ic-error" ).removeClass("ic-dd-rotate-180");
				$( ".ic-pass" ).removeClass("ic-dd-rotate-180");
				$( ".ic-critical" ).removeClass("ic-dd-rotate-180");
		}
		$('#showSections').switchClass("showAllActive","showAllInActive",300,"easeInQuint");
		$('#hideSections').switchClass("hideAllInActive","hideAllActive",300,"easeInQuint");
	}
		
}

function toggleSection(event,sectionValue){
	var target = event.currentTarget;
	var rowClick = false;
	var rowId = '#'+$(target).attr('id');
	if(target.nodeName =='DIV'){
		rowClick = true;
		target = $(target).find('a');
	}

	 var secId = "#sec"+sectionValue;
	 var parentSectionId = "#sec_" +sectionValue;

	if(rowClick == true){
		
		if($(target).find('i').hasClass("ic-dd-rotate-180")){
			hideDD(parentSectionId);
			hideTable(parentSectionId)
		}else  if($(target).find('i').hasClass("ic-dropdown")){
			showDD(parentSectionId);
			showTable(parentSectionId)
		}
	}else if($(target).find('i').hasClass("ic-dd-rotate-180")){
		showDD(parentSectionId);	
		showTable(parentSectionId)
	}else{
		 hideDD(parentSectionId);
		hideTable(parentSectionId)
	}

}

function showTable(parentSectionId){
	
	$(parentSectionId).find('.section-sub-headerDiv').show();
	$(parentSectionId).find('.section-sub-header').show();
	$(parentSectionId).find('.msgTablepadding').show();
	$(parentSectionId).find('.table').show();
	$(parentSectionId).find('.table tbody tr').show();
	$(parentSectionId).find('.table thead tr').show();
	$( ".ic-error" ).removeClass("ic-dd-rotate-180");
			$( ".ic-pass" ).removeClass("ic-dd-rotate-180");
			$( ".ic-critical" ).removeClass("ic-dd-rotate-180");
}

function hideTable(parentSectionId){
	
	
	
	$(parentSectionId).find('.section-sub-header').hide();
	$(parentSectionId).find('.section-sub-headerDiv').hide();
	$(parentSectionId).find('.msgTablepadding').hide();
		$(parentSectionId).find('.table tbody tr').hide();
		$(parentSectionId).find('.table').hide();
		$( ".ic-error" ).removeClass("ic-dd-rotate-180");
			$( ".ic-pass" ).removeClass("ic-dd-rotate-180");
			$( ".ic-critical" ).removeClass("ic-dd-rotate-180");
}
function hide(secId){
	//$(secId).hide(100);
	hideDD(secId);
//	$(secId +' tr').hide(100);
}

function hideDD(secId){
	
	if($(secId).find('i').hasClass("ic-dd-rotate-180")){
		$(secId).find('i').removeClass("ic-dd-rotate-180");
		$( ".ic-error" ).removeClass("ic-dd-rotate-180");
			$( ".ic-pass" ).removeClass("ic-dd-rotate-180");
			$( ".ic-critical" ).removeClass("ic-dd-rotate-180");
	}
}

function showDD(secId){
	if($(secId).find('i').hasClass("ic-dropdown")){
		
			$(secId).find('i').addClass("ic-dd-rotate-180");
			$( ".ic-error" ).removeClass("ic-dd-rotate-180");
			$( ".ic-pass" ).removeClass("ic-dd-rotate-180");
			$( ".ic-critical" ).removeClass("ic-dd-rotate-180");

	}
	
	
}

function toggleDD(target){
	
	 if($(target).hasClass("ic-dd-rotate-180")){
			 $(target).removeClass("ic-dd-rotate-180");
		 }else{
			 $(target).addClass("ic-dd-rotate-180");
		 }
		 $( ".ic-error" ).removeClass("ic-dd-rotate-180");
			$( ".ic-pass" ).removeClass("ic-dd-rotate-180");
			$( ".ic-critical" ).removeClass("ic-dd-rotate-180");

}

function show(secId){
	//$(secId).show(100);
	showDD(secId);		
//	$(secId +" tr").show(100);
}

function gotoTab(tabId){
	 $('html,body').animate({
	        scrollTop: $(tabId).offset().top},
	        'slow');
}

function checkForSystemError(){
	
	$(".ic-print").hide();

	$('#evalRsltsSec').hide();
	$('#evalMsgsCount').hide();
	$('#infoTab').hide();
	$('#sec_giTable').hide();
	$('#msgsTab ul.tabs-list').css("border-bottom","0px");
	$('#msgsTab ul.tabs-list li').slice(1,3).addClass('btn-disabled'); 
	$('#msgsTab ul.tabs-list li').slice(1,3).each(function(){
		$(this).children().css({
			   'cursor' : 'default',
			   'color' : '#006693'
			});
	})
	$('#msgsTab ul.tabs-list li:last-child').hide();
	$('#msgsTab').css('paddingTop','40px');
	hideLeftNavLinksSystemError();
}

function hideLeftNavLinksSystemError(){
	$('#closingDisclosureType').hide();
	$('#loanId').hide();
	$('#borrowerName').hide();
	$('#evalResultsSubList').hide();
	$('#closingInfoSubList').hide();
	$('#transInfoResultsSubList').hide();
	$('#loanInfoSubList').hide();	
}
function checkForHardStop(){
//	$(".ic-print").hide();

	$('#dqLink').addClass('btn-disabled not-active');
		$('#eligLink').addClass('btn-disabled not-active');
		$('#msgsTab ul.tabs-list').css("border-bottom","0px");
		$('#msgsTab ul.tabs-list li').slice(1,3).addClass('btn-disabled'); 
		$('#msgsTab ul.tabs-list li').slice(1,3).each(function(){
			$(this).children().css({
				   'cursor' : 'default',
				   'color' : '#FFF'
				});
		})
}

function leftNavLinks(id){
	var  targetTab;
	if(id == 'parentNavLink'){
		
		$("html, body").animate({ scrollTop: 0 }, "slow");
		
	} else if(id == 'evalResultsSubLink') {
		
		$('.sidebar-sublist li a.active').removeClass('active');
		$('#evalResultsSubLink').addClass('active');
		gotoTab(evalRsltsSec);
		
	} else if(id == 'closingInfoSubLink') {
		$('.sidebar-sublist li a.active').removeClass('active');
		$('#closingInfoSubLink').addClass('active');
			targetTab =$('a[id=#closingInfo]');
		 switchTabs('#infoTab',targetTab);
	     gotoTab(infoTabAnchor);

	} else if(id == 'transInfoResultsSubLink'){
		$('.sidebar-sublist li a.active').removeClass('active');
		$('#transInfoResultsSubLink').addClass('active');
		targetTab =$('a[id=#transInfo]');
		 switchTabs('#infoTab',targetTab);
		 gotoTab(infoTabAnchor);
	    
	} else if(id == 'loanInfoSubLink'){
		$('.sidebar-sublist li a.active').removeClass('active');
		$('#loanInfoSubLink').addClass('active');			
		targetTab =$('a[id=#loanInfo]');
		switchTabs('#infoTab',targetTab);
		gotoTab(infoTabAnchor);
	 	
	} else if(id == 'evalMessagesSubLink'){
		$('.sidebar-sublist li a.active').removeClass('active');
		$('#evalMessagesSubLink').addClass('active');
		gotoTab(evalMsgsCount);
	}
		
	
					
}



$(document).ready(function () {	

var dqCount =document.getElementById('dqCount').innerHTML;
var egCount= document.getElementById('egCount').innerHTML;

if(dqCount ==0 && egCount==0){
		
		checkForHardStop();
		
	}
showCollapseSections("hide");	
		//Results link functionality + right click menu
		$('a.button').click(function(){
			if(dqCount ==0 && egCount==0 && $(this).attr('id') != 'giLink'){
					return;
			}
			var TargetTab = $('a[id=#generalMsgs]');
			if($(this).attr('id') == 'giLink'){
				 targetTab =$('a[id=#generalMsgs]');
				 switchTabs('#msgsTab',targetTab);
			 	 gotoTab(msgsTabAnchor);
			}else if($(this).attr('id') == 'dqLink'){
				targetTab =$('a[id=#dataQualityMsgs]');
				switchTabs('#msgsTab',targetTab);
				 gotoTab(msgsTabAnchor);
			}else if($(this).attr('id') == 'eligLink'){
				 targetTab =$('a[id=#eligibilityMsgs]');
				 switchTabs('#msgsTab',targetTab);
				 gotoTab(msgsTabAnchor);
			}
		});
		
	 //Default Action

		 $('#infoTab ul.tabs-list > li a').click(function(){
			 var targetTab = this;
			 var tabId="#infoTab";
			 switchTabs(tabId,targetTab);
		 } );
		 $('#msgsTab ul.tabs-list > li a').click(function(){ 
			if(dqCount ==0 && egCount==0 && $(this).attr('id') != 'giLink'){
					return;
			}
				 var targetTab = this;
			 	switchTabs('#msgsTab',targetTab);
		 } );
		 
		 //Results link functionality + right click menu
		$('a.button').click(function(){
			
			var TargetTab = $('a[id=#generalMsgs]');
			if($(this).attr('id') == 'giLink'){
				 targetTab =$('a[id=#generalMsgs]');
				 switchTabs('#msgsTab',targetTab);
			 	 gotoTab(msgsTabAnchor);
			}else if($(this).attr('id') == 'dqLink'){
				targetTab =$('a[id=#dataQualityMsgs]');
				switchTabs('#msgsTab',targetTab);
				 gotoTab(msgsTabAnchor);
			}else if($(this).attr('id') == 'eligLink'){
				 targetTab =$('a[id=#eligibilityMsgs]');
				 switchTabs('#msgsTab',targetTab);
				 gotoTab(msgsTabAnchor);
			}
		});
		 
	$('.section-sub-header').click(function(){
			 var target = $(this).find('i');
			 var secTableId = "#sec"+$(this).attr("id").split('_')[2];
			 $(this).next(secTableId).toggle();
			 var table =  $(this).parent().next().toggle();
			 toggleDD(target);
		 });

	    var rotateClass = 'rotate180';
			
		var messages = $('#msgsTab').find('.messageToggleContainer').closest('tr');
		messages.messagetoggle({
			onBusinessMessage: function() {
				this.find('a').attr('title', 'View Technical Message').find('img').removeClass(rotateClass);
			},
			
			onTechnicalMessage: function() {
				this.find('a').attr('title', 'View Business Message').find('img').addClass(rotateClass);
			}
		});
		
		messages.find('a').click(function(){
			$(this).closest('tr').messagetoggle('toggle');
		});

		$('.msgToggleBtn').click(function(){
				
			if ($(this).attr('id') == 'technicalMsgs') {
				$('#businessMsgs').switchClass( "msgToggleActive", "msgToggleBtn",100,  "easeInExpo" );
				messages.messagetoggle('showTechnicalMessage');
			} else {
				$('#technicalMsgs').switchClass( "msgToggleActive", "msgToggleBtn",100,  "easeInExpo" );
				messages.messagetoggle('showBusinessMessage');
			}
			$(this).addClass('msgToggleActive');
		});
		
});		