function roboHelp(link){
	// location intentionally not given initially, as IE has issues when the location contains an anchor (#)
	var win = window.open("", "Help","width=800,height=600,scrollbars=yes,menubar=yes,resizable=yes,location=yes");
	win.location.href = link;
    win.focus();
}

function showFooterLink(link, title) {
	window.open(link, title, "height=500,width=1000,location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,status=no");
}

function numericValue(selector, property) {
	return parseInt(selector.css(property).replace(/[^\d\.]+/, ''));
}

// Set right hand menu items to be on the same vertical level as the first content block
	$(document).ready(function() {
	var utilityList = $('.utility-list');
	var helpIcon = utilityList.find('.ic-help');
	var firstContentBlock = $('.first').first();
	if (utilityList.length === 1 && helpIcon.length === 1 && firstContentBlock.length === 1) {
		var firstContentTopSpace = numericValue(firstContentBlock, 'margin-top');
		var helpIconTopSpace =  numericValue(helpIcon, 'margin-top') + numericValue(helpIcon, 'padding-top');
		var utilityMarginTop = firstContentBlock.position().top - firstContentTopSpace + helpIconTopSpace;
		utilityList.css('margin-top', utilityMarginTop + 'px');
	}
});

function get_browser_info(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE',version:(tem[1]||'')};
        }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
 }

var dateformat={
		 format :function(){
			
	    	var date,
	        format,
	        args = [].slice.call(arguments),
	        returnStr = '',
	        curChar = '',
	        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	        methods =
	        {
	            // Day
	            d: function() { return (date.getDate() < 10 ? '0' : '') + date.getDate(); },
	            D: function() { return days[date.getDay()].substring(0, 3); },
	            j: function() { return date.getDate(); },
	            l: function() { return days[date.getDay()]; },
	            N: function() { return date.getDay() + 1; },
	            S: function() { return (date.getDate() % 10 == 1 && date.getDate() != 11 ? 'st' : (date.getDate() % 10 == 2 && date.getDate() != 12 ? 'nd' : (date.getDate() % 10 == 3 && date.getDate() != 13 ? 'rd' : 'th'))); },
	            w: function() { return date.getDay(); },

	            // Month
	            F: function() { return months[date.getMonth()]; },
	            m: function() { return (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1); },
	            M: function() { return months[date.getMonth()].substring(0, 3); },
	            n: function() { return date.getMonth() + 1; },
	            Y: function() { return date.getFullYear(); },
	            y: function() { return ('' + date.getFullYear()).substr(2); },

	            // Time
	            a: function() { return date.getHours() < 12 ? 'am' : 'pm'; },
	            A: function() { return date.getHours() < 12 ? 'AM' : 'PM'; },
	            g: function() { return date.getHours() % 12 || 12; },
	            G: function() { return date.getHours(); },
	            h: function() { return ((date.getHours() % 12 || 12) < 10 ? '0' : '') + (date.getHours() % 12 || 12); },
	            H: function() { return (date.getHours() < 10 ? '0' : '') + date.getHours(); },
	            i: function() { return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes(); },
	            s: function() { return (date.getSeconds() < 10 ? '0' : '') + date.getSeconds(); },

	            // Timezone
	            O: function() { return (-date.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(date.getTimezoneOffset() / 60)) + '00'; },
	            P: function() { return (-date.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(date.getTimezoneOffset() / 60)) + ':' + (Math.abs(date.getTimezoneOffset() % 60) < 10 ? '0' : '') + (Math.abs(date.getTimezoneOffset() % 60)); },
	            T: function() { var m = date.getMonth(); date.setMonth(0);  var result = date.toTimeString().replace('/^.+ (?([^)]+))?$/', args[1]); date.setMonth(m); return result;},
	            Z: function() { return -date.getTimezoneOffset() * 60; },

	            // Full Date/Time
	            c: function() { return date.format("Y-m-d") + "T" + date.format("H:i:sP"); },
	            r: function() { return date.toString(); },
	            U: function() { return date.getTime() / 1000; },
	            z: function() { 
	            	
	            	var browser=get_browser_info();
                    
	            	if(browser.name =='MSIE' && browser.version =='9'){
                    	return (date.toTimeString().split(' '))[1];
                    }else{
		            	var timeZoneStr=((date.toTimeString().split('('))[1]).split(')')[0];
		            	
		            	var strArry =timeZoneStr.split(' ');
		            	
		            	var tzStr='';
		            	for(var  i=0 ;i<strArry.length;i++){
		            		 tzStr=tzStr+(strArry[i].charAt(0));
		            	}
		            	
		            	return tzStr;
	                    }
	            	},
	            
	        }

	    if(typeof this.getMonth == 'function')
	    {
	        date = this;
	        format = args[0];
	    }
	    else
	    {
	        date = args[0];
	        format = args[1];
	    }

	    for(var i = 0; i < format.length; i++)
	    {
	        var curChar = format.charAt(i);
	        if(methods[curChar])
	        {
	            returnStr += methods[curChar].call();
	        }
	        else
	        {
	            returnStr += curChar;
	        }
	    }
	    return returnStr;
	}
};


