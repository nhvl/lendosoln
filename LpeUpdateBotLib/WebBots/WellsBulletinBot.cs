﻿//Kelvin Young
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class WellsBulletinBot : AbstractWebBot
    {
        private string initialUrl = "https://wffnet.wellsfargo.com/ilonline/funding/wff_login.jsp";
        private static string LogoutUrl = "https://wffnet.wellsfargo.com/ilonline/Logoff.wf";
        string[] userId = new string[2];
        char[] splitter = { '/' };
        private static Regex sessionExpired_regex = new Regex(@"Your session had expired. Please login again", RegexOptions.IgnoreCase);
        private static Regex channel_regex = new Regex(@"name=""CHANNEL"" value=""(?<channel>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex loginUrl_regex = new Regex(@"name=""userLoginForm""\s+action='(?<loginUrl>[^']+)'", RegexOptions.IgnoreCase);
        string temp = null;
        string LoginUrl = "https://wffnet.wellsfargo.com";
        private string homeUrl = "https://wffnet.wellsfargo.com/ilonline/mainStartAction.do";

        /********** UPDATE REGEX BULLETIN NUMBER HERE **********/
        private static string bulletinNumber = "C13-077"; //Bulletin number one higher than current one
        /********** UPDATE REGEX BULLETIN NUMBER HERE **********/

        private static Regex bulletin_regex = new Regex(bulletinNumber, RegexOptions.IgnoreCase); //Regex checks bulletin number
        
        public WellsBulletinBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "WELLSBULLETIN"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            bool loggedIn = false;
            int counter = 0;

            userId = dlinfo.Login.Split(splitter);
            NameValueCollection postData = new NameValueCollection();

            //Login
            while ((loggedIn == false) && (counter < 2))
            {
                temp = DownloadData(initialUrl);
                Match match = loginUrl_regex.Match(temp);
                if (match.Success)
                {                    
                    LoginUrl = LoginUrl + match.Groups["loginUrl"].Value;
                    match = channel_regex.Match(temp);
                    {
                        postData.Add("origin", "FUNDING");
                        postData.Add("MEMBER_ID", userId[0]);
                        postData.Add("USER_ID", userId[1]);
                        postData.Add("PASSWORD", dlinfo.Password);
                        postData.Add("goButton", "Go");
                        postData.Add("CHANNEL", match.Groups["channel"].Value);
                        temp = UploadDataReturnResponse(LoginUrl, postData);
                    }

                    Match loginMatch = sessionExpired_regex.Match(temp);
                    if (!loginMatch.Success)
                        loggedIn = true;
                }
                counter++;
            }

            //Disable if login fails
            if (!loggedIn)
            {
                LogErrorAndSendEmailToAdmin("WellsFargo Bulletin bot failed to login. Login has been disabled. Please check and re-enabled login. Content: " + temp);
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            string pageInfo = null;

            pageInfo = DownloadData(homeUrl);

            //Check for new bulletin
            Match matchBulletin = bulletin_regex.Match(pageInfo);

            if (matchBulletin.Success)
            {
                LogErrorAndSendEmailToAdmin(string.Format("A new Wells bulletin (" + bulletinNumber + ") has been posted. Please verify, update and reenable the bot.", pageInfo));
                dlinfo.Deactivate();
            }

            //Logout
            GoToUrl(LogoutUrl);

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
    }
}