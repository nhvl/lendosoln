﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Threading;
using System.Collections.Generic;

namespace LpeUpdateBotLib.WebBots
{
    public class CaliberWSBot : AbstractWebBot
    {

        public CaliberWSBot(IDownloadInfo info) : base(info)
        {
        }

        public override string BotName
        {
            get { return "CALIBERWS"; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = DownloadFile(dlinfo.Url);

            /* this section adds an extra row in between each rate stack so that we can use DynamicListBottom */

            String path = fileName;
            List<String> lines = new List<String>();

            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    String line;
                    String[] temp_split = new String[9];

                    // loop thru all lines in the rate sheet file we downloaded
                    while ((line = reader.ReadLine()) != null)
                    {
                        // if this line contains a comma
                        if (line.Contains(","))
                        {
                            // split the line into an array of strings
                            String[] split = line.Split(',');
                                        
                            // if the 2nd value of the current line does not match the 2nd value of the previous line
                            if (!(split[1].Equals(temp_split[1])))
                            {
                                // add a blank row
                                line = "";
                                lines.Add(line);
                            }

                            // set the previous line equal to the current line for next iteration
                            temp_split = split;
                            // convert the string array into one csv line
                            line = String.Join(",", temp_split);
                        }
                        // add the line that we have read in
                        lines.Add(line);
                    }
                }

                // overwrite the rate sheet file so that it includes that newly added blank rows
                using (StreamWriter writer = new StreamWriter(path, false))
                {
                    foreach (String line in lines)
                        writer.WriteLine(line);
                }
            }
            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}