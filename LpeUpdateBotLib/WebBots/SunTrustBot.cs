/// Author: David Dao

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

using LpeUpdateBotLib.Common;
namespace LpeUpdateBotLib.WebBots
{
    public class SunTrustBot : AbstractWebBot
    {
        private static Regex s_inputRegex = new Regex(@"name=""(?<name>[^""]+)""[ ]+id=""(?<id>[^""]+)""[ ]+value=""(?<value>[^""]+)""[^>]+>");
        private static Regex s_fileIdRegex = new Regex(@"PopUpFileViewer\((?<fileId>[0-9]+)\);");
        private static Regex s_RateSheetExpRegex = new Regex(@"Current\s+Pricing\s+has\s+Expired");
		private static Regex s_NoRateSheetRegex = new Regex(@"No\s+rate\s+sheets\s+found");
		private static Regex s_PasswordExpiredRegex = new Regex (@"Your password will expire in 2 days");
        private static Regex s_RatesheetIdRegex = new Regex(@"Corr\s+Rate\s+Sheet\s+-\s+csv</td><td style=""[^""]+"">+([0-9/]+)\s+([0-9:]+)\s+((AM|PM)?)</td><td align=""[^""]+""\s+style=""[^""]+"">([0-9\sA-Z]+)</td><td align=""[^""]+""\s+style=""[^>]+>([A-Za-z]+)</td><td align=""[^>]+>\s+<a id=""[^""]+""\s+CommandName=""[^""]+""\s+href=""JavaScript:PopUpFileViewer\((?<RId>[^\)]+)\);""");
        private static Regex s_AdjustmentIdRegex = new Regex(@"onClick=""PopUpFileViewer\((?<AdjId>[^\)]+)\);""><b>&rsaquo;</b> \*\*\*New\*\*\*Correspondent Rate Adjustment Schedule");
		private static Regex s_ChangePasswordRegex = new Regex(@"/Admin/Password/Default.aspx\?Alert=3");
        private static Regex s_TerminatedRegex = new Regex(@"The partner company[^>]*has been terminated", RegexOptions.IgnoreCase);
        private static Regex s_invalidLoginRegex = new Regex(@"Invalid\s+user\s+name\s+or\s+password", RegexOptions.IgnoreCase);
        
        public SunTrustBot(IDownloadInfo info) : base(info)
        {
        }
        public override string BotName 
        {
            get { return "SUNTRUST"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
			string initial = null;
            string content = null;
			string passExpired = null;
			string RId1 = null;
            string AdjId1 = null;

			initial = DownloadData ("https://www.stmpartners.com/public/");

            NameValueCollection postData = new NameValueCollection();
			Match match = s_inputRegex.Match(initial);
            
            bool hasViewState = false;
			while (match.Success) 
			{
                if (match.Groups["name"].Value == "__VIEWSTATE") 
                {
                    postData.Add("__VIEWSTATE", match.Groups["value"].Value);
                    hasViewState = true;
                    //break;
                } 
                else if (match.Groups["name"].Value == "__EVENTVALIDATION") 
                {
                    postData.Add("__EVENTVALIDATION", match.Groups["value"].Value);
                }
                match = match.NextMatch(); // 11/10/2008 dd - This will get all hidden input validation and put to POST data.
			}
            if (!hasViewState) 
            {
                LogErrorAndSendEmailToAdmin ("Suntrust Bot can't find Viewstate value. Content: "+initial);
                return E_BotDownloadStatusT.Error;
            }
            postData.Add("ctl00$LoginPlaceholder$LoginBox$UserID", dlinfo.Login);
            postData.Add("ctl00$LoginPlaceholder$LoginBox$Password", dlinfo.Password);
            postData.Add("ctl00$LoginPlaceholder$LoginBox$SignOnButton.x", "17");
            postData.Add("ctl00$LoginPlaceholder$LoginBox$SignOnButton.y", "13");
            postData.Add("wd_user_id", "");
            postData.Add("wd_password", "");

            passExpired = UploadDataReturnResponse("https://www.stmpartners.com/public/", postData);
			
            // 11/10/2008 dd - After login detect if user id is correct or not.
            match = s_ChangePasswordRegex.Match(passExpired);
            if (match.Success)
            {
                ReportDeactivation("Suntrust password has expired for login: " + dlinfo.Login + " with bot description: " + dlinfo.Description +". Please update it and re-enable any dl using this login.");
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            match = s_invalidLoginRegex.Match(passExpired);
            if (match.Success)
            {
                ReportDeactivation("Suntrust login is no longer working for login: " + dlinfo.Login + " with bot description: " + dlinfo.Description + ". Please update it and re-enable any dl using this login.");
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            Match mTerminated = s_TerminatedRegex.Match(passExpired);
            if (mTerminated.Success)
            {
                ReportDeactivation(string.Format("Suntrust login failed for {0}, {1}. Please contact the client to determine their status with Suntrust, and re-enable any dl's using this login if possible. If we're using their login for adjustment files or any globally used data, those files should be pulled via another client's credentials for the time being.", dlinfo.MainFileName, dlinfo.Login));
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            if (dlinfo.Description == "SUNTRUST_ADJ_01")
            {
                content = DownloadData("https://www.stmpartners.com/private/Home/Homepage.aspx");
                Match AdjustmentIdTest = s_AdjustmentIdRegex.Match(content);
                //Adjustment ratesheet
                if (AdjustmentIdTest.Success)
                {
                    AdjId1 = AdjustmentIdTest.Groups["AdjId"].Value;
                    RatesheetFileName = DownloadFile(@"https://stmpartners.com/Private/ResourceCenter/FileViewer.asp?FileID=" + AdjId1);

                    //Logout
                    GoToUrl(@"https://www.stmpartners.com/Public/Logout.asp?IgnoreSessionTimeout=true");
                    GoToUrl(@"https://www.stmpartners.com/public/");

                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("Invalid Suntrust Adjustment ID. Please update bot. Content: " + content);
                    return E_BotDownloadStatusT.Error;
                }
            }
            else if (dlinfo.Description == "SUNTRUST_03")
            {

                content = DownloadData("https://www.stmpartners.com/Admin/ResourceCenter/RateSheetViewer.aspx");
                
                //get csv ratesheet ID and adjustment ID
                Match RatesheetIdTest = s_RatesheetIdRegex.Match(content);
                Match match1 = s_NoRateSheetRegex.Match(content);

                content = DownloadData("https://www.stmpartners.com/private/Home/Homepage.aspx");
                Match match2 = s_RateSheetExpRegex.Match(content);

                if (match1.Success || match2.Success)
                {
                    LogInfo(dlinfo.MainFileName + " not available: Suntrust bot has detected reprice notification");
                    return E_BotDownloadStatusT.Error;
                }
                //for correspondent
                else if (RatesheetIdTest.Success)
                {
                    //CSV Ratesheet
                    RId1 = RatesheetIdTest.Groups["RId"].Value;
                    RatesheetFileName = DownloadFile(@"https://www.stmpartners.com/Private/ResourceCenter/FileViewer.aspx?FileID=" + RId1);

                    //Logout
                    GoToUrl(@"https://www.stmpartners.com/Public/Logout.asp?IgnoreSessionTimeout=true");
                    GoToUrl(@"https://www.stmpartners.com/public/");

                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;


                }
                else
                {
                    LogErrorAndSendEmailToAdmin("Invalid Suntrust CSV Ratesheet ID. Please verify for username: " + dlinfo.Login + ". Content: " + content);
                    return E_BotDownloadStatusT.Error;
                }
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Invalid SunTrust Bot Description. Description=" + dlinfo.Description + ". Valid values are 'SUNTRUST_03', 'SUNTRUST_ADJ_01'");
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}
