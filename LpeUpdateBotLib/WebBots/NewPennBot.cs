﻿//Kelvin Young
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class NewPennBot : AbstractWebBot
    {
        public NewPennBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "NEWPENN"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = DownloadFile(@"http://r20.rs6.net/tn.jsp?llr=vsenw9cab&et=1108994407497&s=34048&e=001pHdgfvtbw21TpTiCr6ULAZNgB0jP2BxOBVzPz0H0_Zl6aDipSM2AjGe7C5suB9tbW1D-WTsn_c9heV2XbYv2M_DNBUa98DPobWUd3NghOJlgTa9jSpX3FxKrPT8zpzGrbXXRCKUwnL8-gYkJh8KZNecvaQoZlZ0PkFbtomcpHaqTVkjRTTwg51WIPSsoDjdq"); //the RS that will be DLed

            //Check for error
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("NewPennBot failed to download ratesheet. Please check URL of the ratesheet.");
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}