// Author: Brian Beery
// Date: 3-11-08
// Purpose: Download the Countrywide Price Query rate sheet

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Diagnostics;

using LpeUpdateBotLib;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    /// <summary>
    /// Countrywide Price Query bot
    /// </summary>
    public class CWPriceQBot : AbstractWebBot
    {
        public const string LoanProgs = "1&LP=2&LP=3&LP=4&LP=5&LP=9&LP=19&LP=21&LP=23&LP=80&LP=81&LP=141&LP=241&LP=300&LP=392&LP=393&LP=394&LP=514&LP=516&LP=524&LP=525&LP=551&LP=552&LP=555&LP=558&LP=561&LP=564&LP=568&LP=572&LP=575&LP=585&LP=590&LP=591&LP=592&LP=635&LP=656&LP=659&LP=686&LP=765&LP=803&LP=804&LP=1060&LP=1061&LP=1062&LP=1063&LP=1076&LP=1077&LP=1078&LP=1103&LP=1116&LP=1117&LP=1118&LP=1183&LP=1184&LP=1185&LP=1186&LP=1187&LP=1188&LP=1228&LP=1229&LP=1233&LP=1235&LP=1236&LP=1303&LP=1308&LP=1334&LP=1335&LP=1336&LP=1337&LP=1338&LP=1339&LP=6006&&LP=6007&LP=6008&LP=6009&LP=6022";
		private const string SITE_URL = "https://cld.countrywide.com/";
        private const string LOGIN_URL = "https://cldlogin.countrywide.com/cld/login";
        private static string LOGOUT_URL = string.Format("{0}login/loggedout.asp", SITE_URL);
		private static string RS_URL = string.Format("{0}cld/pricequery/pricequeryresult.asp", SITE_URL);
		private const string DL_URL = "cld/Downloads/PriceQuery/";
		private const string NOREQID = "Redirection does not include request id";
		private string BOTERR = "(CWPriceQ) BotError-";
		private static Regex AuthFailedRegex = new Regex(@"password(\s+)has(\s+)expired");
		private static Regex InactiveAcctRegex = new Regex(@"account(\s+)is(\s+)currently(\s+)inactive");
        private static Regex IncorrectIdPwRegex = new Regex(@"incorrect(\s+)User(\s+)ID(\s+)or(\s+)password");
        private static Regex LockedAcctRegex = new Regex(@"account(\s+)has(\s+)been(\s+)locked");
		
        public CWPriceQBot(IDownloadInfo info) : base(info)
        {
        }
        public override string BotName 
        {
            get { return "CW_PQ"; }
        }

		protected override string DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            // PQ Steps
            //    - Login
            //    - Execute the price query
            //    - Download the xls file
            //    - Logout
            string sFileName = null;

            NameValueCollection nvcPostData = new NameValueCollection();
            nvcPostData.Add("usr_name", dlinfo.Login);
            nvcPostData.Add("usr_password", dlinfo.Password);

            string sHtml = UploadDataReturnResponse(LOGIN_URL, nvcPostData);
			Match mFindProblem = AuthFailedRegex.Match(sHtml) ;
            if(mFindProblem.Success )
			{
				dlinfo.TurnOffCWDL();
				string sErrorMsg = string.Format("{0}Login failed for user id {1} because the password has expired. Please verify login, update pw if necessary, and re-enable any dl using this login.", BOTERR, dlinfo.Login);
				ReportDeactivation(sErrorMsg);
				return null;
			}
			else
            {
                mFindProblem = IncorrectIdPwRegex.Match(sHtml);
                if (mFindProblem.Success)
                {
                    dlinfo.TurnOffCWDL();
                    string sErrorMsg = string.Format("{0}Login failed for user id {1} because of an incorrect password or login. Please verify login, update pw if necessary, and re-enable any dl using this login.", BOTERR, dlinfo.Login);
                    ReportDeactivation(sErrorMsg);
                    return null;
                }
                else
                {
                    mFindProblem = InactiveAcctRegex.Match(sHtml);
                    if (mFindProblem.Success)
                    {
                        dlinfo.TurnOffCWDL();
                        string sErrorMsg = string.Format("{0}User id {1} is inactive. Please ask the client to re-activate, and re-enable any dl using this login.", BOTERR, dlinfo.Login);
                        ReportDeactivation(sErrorMsg);
                        return null;
                    }
                    else
                    {
                        mFindProblem = LockedAcctRegex.Match(sHtml);
                        if (mFindProblem.Success)
                        {
                            dlinfo.TurnOffCWDL();
                            string sErrorMsg = string.Format("{0}User id {1} is locked. Please ask the client to update the pw, and re-enable any dl using this login.", BOTERR, dlinfo.Login);
                            ReportDeactivation(sErrorMsg);
                            return null;
                        }
                    }
                }
			}

			// call PriceQuery
			sHtml = PriceQuery();
			if(isValidResult(sHtml))
			{
				sFileName = DownloadFile(string.Format("{0}{1}", SITE_URL, sHtml));
			}
			else
            {
				string sErrorMsg="";
				if(sHtml.IndexOf(BOTERR)>=0)
					sErrorMsg = sHtml;
                else 
					sErrorMsg = string.Format("{0}Unable to locate xls rate sheet.", BOTERR);

                LogErrorAndSendEmailToAdmin(string.Format("UserID = {0}; {1}", dlinfo.Login, sErrorMsg));
                return null;
            }

            GoToUrl(LOGOUT_URL);

            return sFileName;
        }

		private bool isValidResult(string sRes)
		{
			return sRes.StartsWith("cld/");
		}
		// Executes CW's Price Query
		private string PriceQuery()
		{
			//DataAccess.TimerHelper timer = new DataAccess.TimerHelper();
			//timer.start();
			
			AllowAutoRedirect = false;
			string sHtml = DownloadData(ConstructUrl());
			string sPathOrProgress = "";
			int iPollCount = 0;
			int iMaxPollCount = 25;

			// Follow the price query progress redirection until we time-out or hit an xls 
			if(IsRedirection)
			{
				string sRedrctn = RedirectionUrl;
				string sTmpDirctn = "";
				
				while((sRedrctn != "") && (iPollCount < iMaxPollCount))
				{
					Thread.Sleep(Config.CWPollingInterval);
					iPollCount++;
					if(sRedrctn.StartsWith("/cld/pricequery"))
						sRedrctn = sRedrctn.Remove(0,1);
					else 
						sRedrctn = string.Format("cld/pricequery/{0}", sRedrctn);

					sHtml = DownloadData(string.Format("{0}{1}", SITE_URL, sRedrctn)); 
					
					sRedrctn = RedirectionUrl;
					if(sRedrctn != "")  // store the redirection
						sTmpDirctn = sRedrctn;
				
					sPathOrProgress = ParseForXLS(sHtml);
					
					if(sPathOrProgress.StartsWith("PriceQueryProgress"))
						sTmpDirctn = sRedrctn = sPathOrProgress;
					else if(sPathOrProgress.IndexOf(NOREQID) >= 0)  // if the html response did not contain a request id, use the previous response
						sRedrctn = sTmpDirctn;
				}
			}

			string sResults = "";

			// Download the xls, or return any error
			if (iPollCount >= iMaxPollCount) 
			{
				sResults = string.Format("{0}Polling exceeded limit. Polling Count={1}", BOTERR, iPollCount);
			}
			else 
				sResults = sPathOrProgress;

			//timer.stop();
			//ReportingTools.Log("[" + System.Threading.Thread.CurrentThread.Name + "] - CLDPriceQuery in " + timer.DurationString + ". Result=" + results + ". PollingCount=" + pollingCount);
			
			return sResults;
		}

		// Constructs the price query url
		private string ConstructUrl()
		{
			string featureID = "|4000|4100|4980|4200|4320|4350|4400|4500|4550|4570|4600|4700|4800|4900|4990|4995|4985|";
			string sType = "Price";
			string sBRC = "N";

			// INACTIVES REMOVED: &LP=385&LP=386&LP=387
			string sLPGrp = "All";
			// Format the datetime for inclusion in the query, and include the AM or PM suffix; ex. BE%7C01-13-2005++1%3A05PM;
			//string sPPO = "0&PPO=1&PPO=2&PPO=3&PPO=4";
			string sPPO = "0";
			string sCommitType = "BE|";
			string sMinNoteRate = "";
			string sMaxNoteRate = "";
			string sNoteRateInterval = "0.125";
			string sMinCommitPeriod = "10";
			string sMaxCommitPeriod = "120";

			return RS_URL + "?FeatureId=" + featureID + FormatURL(new string[,]{{"Type", sType}, {"BRC", sBRC}, {"LP", LoanProgs},
																	{"LPGrp", sLPGrp}, {"PPO", sPPO}, {"CommitType", sCommitType}, {"MinNoteRate", sMinNoteRate},
																	{"MaxNoteRate", sMaxNoteRate}, {"NoteRateInterval", sNoteRateInterval}, 
																	{"MinCommitPeriod", sMinCommitPeriod}, {"MaxCommitPeriod", sMaxCommitPeriod}});
					
		}

		// Builds and returns the url for the query from the given string array,
		//  which contains the query tags and parameters
		private string FormatURL(string[,] paramsAndVals)
		{
			string completeUrl = null;

			for(int i=0; i<paramsAndVals.GetLength(0); i++)
			{
				completeUrl = string.Format("{0}&{1}={2}", completeUrl, paramsAndVals[i,0], paramsAndVals[i,1]);
			}
			return completeUrl;
		}
	
		// Parses the given string for EITHER an xls file path OR a query progress string
		//  (e.g. "PriceQueryProgress.asp?PriceQueryRequestId=146453&Flag=y&BRC=")
		// Returns the xls path, query progress string, or an error if neither are found
		private string ParseForXLS(string sHtml)
		{
			int index = 0;
			bool bFound = false;

			if(sHtml != null)
			{
				if(sHtml.IndexOf("Script timed out") > -1)
					sHtml = string.Format("{0}CW returned a script time out.", BOTERR);
				else
				{
					// Look for the xls download path
					index = sHtml.IndexOf(DL_URL);
					if(index < 0)  // no xls
					{
						string sError = "";
						if(!isRedirect(ref sError, ref sHtml, ref index))
							sHtml = sError;
						else
							bFound = true; //found redirection
					}
					else  // xls found, grab the path
					{
						sHtml = sHtml.Substring(index);
						index = sHtml.IndexOf(".xls");
						if(index < 0) // shouldn't happen unless they change the html response
							sHtml = string.Format("{0}Rate sheet (xls) was not included in the response from CW. The response:\n{1}", BOTERR, sHtml);
						else
							bFound = true; //found xls
					}
					if(bFound)
					{
						index+=4;
						sHtml = sHtml.Substring(0, index);			
					}
				}
			}
			return sHtml;
		}
		private bool isRedirect(ref string sErr, ref string sHtml, ref int iHtmlIndex)
		{
			bool bIsRdct = true;

			iHtmlIndex = sHtml.IndexOf("PriceQueryProgress.asp?");
			// no redirection
			if(iHtmlIndex < 0)
			{
				sErr = string.Format("{0}No redirection found.", BOTERR);
				bIsRdct = false;
			}
			else
			{
				// grab the redirection
				sHtml = sHtml.Substring(iHtmlIndex);
				iHtmlIndex = sHtml.IndexOf("BRC=");
			
				// redirection, but no request id
				if(iHtmlIndex < 0)
				{
					sErr = string.Format("{0}{1}", BOTERR, NOREQID);
					bIsRdct = false;
				}
			}
			return bIsRdct;
		}
	}
}
