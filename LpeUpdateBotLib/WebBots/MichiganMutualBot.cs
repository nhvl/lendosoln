﻿//Yin Seo
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class MichiganMutualBot : AbstractWebBot
    {
        //list of URLs that will later be needed to login, download RS, logout, ect
        private static string hostUrl = @"http://home.michiganmutual.com/";
        private static string loginUrl = hostUrl + @"Default.aspx";
        private static string rsUrl = hostUrl + @"Rates.aspx?rsType=3";
        Regex viewState_regex = new Regex(@"<input type=""hidden"" name=""__VIEWSTATE"" id=""__VIEWSTATE"" value=""(?<viewStateValue>[^""]+)"" />", RegexOptions.IgnoreCase);
        Regex eventValidation_regex = new Regex(@"<input type=""hidden"" name=""__EVENTVALIDATION"" id=""__EVENTVALIDATION"" value=""(?<eventValidationValue>[^""]+)"" />", RegexOptions.IgnoreCase);
        Regex logoutValidation_regex = new Regex(@"<input type=""hidden"" name=""__EVENTVALIDATION"" id=""__EVENTVALIDATION"" value=""(?<logoutValidationValue>[^""]+)"" />", RegexOptions.IgnoreCase);
        private static Regex pwChange_regex = new Regex(@"Invalid username and/or password.");

        public MichiganMutualBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "MICHIGANMUTUAL"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null; //the RS that will be DLed

            //Go to website and download content to look for VIEWSTATE
            GoToUrl(loginUrl);
            string vsTemp = DownloadData(loginUrl);

            //find viewstate to use in login and logout
            Match vs_match = viewState_regex.Match(vsTemp);
            Match ev_match = eventValidation_regex.Match(vsTemp);
            if (vs_match.Success && ev_match.Success)
            {
                string viewState = vs_match.Groups["viewStateValue"].Value;
                string eventValidation = ev_match.Groups["eventValidationValue"].Value;

                //Login
                NameValueCollection loginInfo = new NameValueCollection();
                loginInfo.Add("ctl00_scriptMgr1_HiddenField", ";;AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e:en-US:5c09f731-4796-4c62-944b-da90522e2541:de1feab2:f2c8e708:720a52bf:f9cec9bc:589eaa30:a67c2700:ab09e3fe:87104b7c:8613aea7:3202a5a2:be6fb298");
                loginInfo.Add("ctl00$txtUserID", dlinfo.Login);
                loginInfo.Add("ctl00$txtPassword", dlinfo.Password);
                loginInfo.Add("ctl00$txtEmail", "");
                loginInfo.Add("__EVENTTARGET", "ctl00$btnLogin");
                loginInfo.Add("__EVENTARGUMENT", "");
                loginInfo.Add("__VIEWSTATE", viewState);
                loginInfo.Add("__EVENTVALIDATION", eventValidation);

                string loginResponse = UploadDataReturnResponse(loginUrl, loginInfo);

                //Check to see if login has expired
                Match match_pwChange = pwChange_regex.Match(loginResponse);
                if (match_pwChange.Success) //detects pw failure
                {
                    ReportDeactivation("Michigan Mutual password has expired for login: " + dlinfo.Login + " with bot description: " + dlinfo.Description + ". Please verify and contact the client to reset the password.");
                    dlinfo.Deactivate();
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    //Download webpage, html2excel will convert into excel
                    fileName = DownloadFile(rsUrl);
                    string temp = DownloadData(hostUrl + "Rates.aspx");
                    Match logout_match = logoutValidation_regex.Match(temp);
                    Match logoutViewState_match = viewState_regex.Match(temp);
                    if ((logout_match.Success) && (logoutViewState_match.Success))
                    {
                        string logoutValidation = logout_match.Groups["logoutValidationValue"].Value;
                        string logoutViewState = logoutViewState_match.Groups["viewStateValue"].Value;
                        //Logout after
                        NameValueCollection logoutInfo = new NameValueCollection();
                        logoutInfo.Add("ctl00_scriptMgr1_HiddenField", ";;AjaxControlToolkit, Version=4.1.60501.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e:en-US:5c09f731-4796-4c62-944b-da90522e2541:de1feab2:f2c8e708:720a52bf:f9cec9bc:589eaa30:a67c2700:ab09e3fe:87104b7c:8613aea7:3202a5a2:be6fb298");
                        logoutInfo.Add("__EVENTTARGET", "ctl00$lnkBtnLogout");
                        logoutInfo.Add("__EVENTARGUMENT", "");
                        logoutInfo.Add("__LASTFOCUS", "");
                        logoutInfo.Add("__VIEWSTATE", logoutViewState);
                        logoutInfo.Add("__EVENTVALIDATION", logoutValidation);
                        logoutInfo.Add("ctl00$txtEmail", "");
                        logoutInfo.Add("ctl00$MainBodyContent$ddlStates", "");
                        
                        UploadData(hostUrl + "Rates.aspx", logoutInfo);
                    }
                    else
                    {
                        ReportDeactivation("Michigan Mutual bot fail to logout, please update Bot and reactivate.");
                        dlinfo.Deactivate();
                        return E_BotDownloadStatusT.Error;
                    }
                }
            }

            //Check for error
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}