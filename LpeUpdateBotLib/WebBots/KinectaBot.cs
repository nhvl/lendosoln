﻿//Author: Mark Hsu
//Testing.

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class KinectaBot : AbstractWebBot
    {
        private Regex ratesheet_regex = new Regex(@"<div class=""media_block"">\s+<div class=""media_block_image""><a href=""(?<ratesheet>[^""]+xlsx)", RegexOptions.IgnoreCase);
        
        public KinectaBot(IDownloadInfo info) : base(info)
        {
        }

        public override string BotName
        {
            get { return "KINECTA"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string temp = DownloadData(dlinfo.Url);
            Match match = ratesheet_regex.Match(temp);
            string fileName = null;
            if (match.Success)
            {
                fileName = DownloadFile(dlinfo.Url + match.Groups["ratesheet"].Value);
                LogInfo(match.Groups["ratesheet"].Value);
            }
            else
                LogErrorAndSendEmailToAdmin("KinectaBot failed to find today ratesheet. Content: " + temp);

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}