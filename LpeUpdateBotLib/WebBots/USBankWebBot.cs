//Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

using LpeUpdateBotLib.Common;
namespace LpeUpdateBotLib.WebBots
{
	public class USBankWebBot : AbstractWebBot
	{
        private static Regex ratesheetRegex_1 = new Regex(@"<A\s+HREF=""(?<rsURL2>[^""]+)""\s+target=""[^""]+"">T\s+1\s+Govt\s+XLS", RegexOptions.IgnoreCase);
        private static Regex ratesheetRegex_2 = new Regex(@"<A\s+HREF=""(?<rsURL3>[^""]+)""\s+target=""[^""]+"">Fully\s+Delegated\s+XLS", RegexOptions.IgnoreCase);
        private static Regex ratesheetRegex_3 = new Regex(@"<A\s+HREF=""(?<rsURL4>[^""]+)""\s+target=""[^""]+"">Purchase\s+Fund\s+XLS", RegexOptions.IgnoreCase);
        private static Regex ratesheetRegex_4 = new Regex(@"<A\s+HREF=""(?<rsURL5>[^""]+)""\s+target=""[^""]+"">T\s+1\s+Govt/Conv\s+XLS", RegexOptions.IgnoreCase);
		
		public USBankWebBot(IDownloadInfo info) : base(info)
		{
		}
		public override string BotName 
		{
			get { return "USBANK_WEB"; }
		}
		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
			GoToUrl ("https://sellus.usbank.com/");

			NameValueCollection postData = new NameValueCollection();
			postData.Add("BrowserType", "msie");
			postData.Add("BrowserVersion", "11.0");
			postData.Add("ScreenX", "1280");
			postData.Add("ScreenY", "1024");
			postData.Add("Username", dlinfo.Login);
			postData.Add("Password", dlinfo.Password);
            postData.Add("NewPassword", "");
            postData.Add("confirm", "");

			string temp = null;
			UploadData ("https://sellus.usbank.com/Scripts/VerifyLogin.asp", postData);
			temp = DownloadData ("https://sellus.usbank.com/PipelineManager/LefthandLinks.aspx");

            Match match_1 = ratesheetRegex_1.Match(temp);
            Match match_2 = ratesheetRegex_2.Match(temp);
            Match match_3 = ratesheetRegex_3.Match(temp);
            Match match_4 = ratesheetRegex_4.Match(temp);

            if (match_1.Success)
            {
                RatesheetFileName = DownloadFile("https://sellus.usbank.com" + match_1.Groups["rsURL2"].Value);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (match_2.Success)
            {
                RatesheetFileName = DownloadFile("https://sellus.usbank.com" + match_2.Groups["rsURL3"].Value);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (match_3.Success)
            {
                RatesheetFileName = DownloadFile("https://sellus.usbank.com" + match_3.Groups["rsURL4"].Value);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (match_4.Success)
            {
                RatesheetFileName = DownloadFile("https://sellus.usbank.com" + match_4.Groups["rsURL5"].Value);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("USBank Bloomington Bot can't find the ratesheet. Please check for bot Username: "+ dlinfo.Login + ": " + temp);
                return E_BotDownloadStatusT.Error;
            }
		}
	}
}
