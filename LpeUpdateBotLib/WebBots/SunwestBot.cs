﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;
using System.Net;

namespace LpeUpdateBotLib.WebBots
{
    public class SunwestBot : AbstractWebBot
    {
        private static Regex rsUrl_regex = new Regex(@"method=""post""\s+action=""(?<rsUrl>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex rs_regex = new Regex(@"Click here to view the document\</h1\> <a href=""(?<rslink>[^""]+)""");

        public SunwestBot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "SUNWEST"; }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }

        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }

        protected override string DefaultAcceptLanguage
        {
            get
            {
                return @"en-US";
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //YS: Commenting these Security protocol settings out on purpose, as it's actually causing other bots to fail.
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            string temp = DownloadData(@"https://ratesheet.reversesoftonline.com/ReverseMortgage/jsp/extCompensation.jsf?userType=correspondent");

            Match match = rsUrl_regex.Match(temp);
            if (match.Success)
            {
                string ratesheetUrl = @"https://ratesheet.reversesoftonline.com" + match.Groups["rsUrl"].Value;
                NameValueCollection postData = new NameValueCollection();
                postData.Add("body:c:userType", "correspondent");
                postData.Add("body:c:userid", dlinfo.Login);
                postData.Add("body:c:userpwd", dlinfo.Password);
                postData.Add("body:c:fileType", "EXCEL");
                postData.Add("body:c:arsBtn", "Access Rate Sheet");
                postData.Add("body:c_SUBMIT", "1");
                postData.Add("jsf_sequence", "1");
                postData.Add("body:c:_link_hidden_", "");

                string fileName = UploadDataDownloadFile(ratesheetUrl, postData);

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

                if (fileName != null)
                {
                    RatesheetFileName = fileName;
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("Sunwest Bot failed to to login and download the ratesheet.");
                    return E_BotDownloadStatusT.Error;
                }
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Sunwest Bot failed to find Corr ratesheet link. Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}