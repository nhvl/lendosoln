﻿// Author Andrew Giang
// Date: 12-18-08
// Purpose: Download the Sovereign Gov rate sheet

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots
{
    public class SovereignGov : AbstractWebBot
    {

        public SovereignGov(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "SOVEREIGNGOV"; }
        }

        protected override string DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            NameValueCollection nvcPostData = new NameValueCollection();
            nvcPostData = new NameValueCollection();
            nvcPostData.Add("form_submit", "true");
            nvcPostData.Add("PASSWORD", "sovloans");

            UploadData("https://www.sovereignbank.com/corporate/credit/rate_sheets.asp", nvcPostData);

            return DownloadFile("https://www.sovereignbank.com/corporate/credit/download.aspx?id=205");
        }
    }
}