/// Author: David Dao

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    /// <summary>
    /// CountryWide Equity Bot
    /// </summary>
    public class CWEQBot : AbstractWebBot
    {
        private static Regex s_regEx = new Regex(@"cld/Downloads/HelocRatesheet/[A-Za-z0-9_]+\.xls", RegexOptions.Compiled);

        private const string LOGIN_URL = "https://cldlogin.countrywide.com/cld/login";
        private const string LOGOUT_URL = "https://cld.countrywide.com/login/loggedout.asp";
        private const string RS_URL = "https://cld.countrywide.com/cld/PriceGuide/helocratesheet.aspx?nav=y";

        public CWEQBot(IDownloadInfo info) : base(info)
        {
            // 8/17/2007 dd - External OPM 46326 Split 2nd ratesheet into two separate ratesheets.
            if (RateSheetType != "" && RateSheetType != "CES" && RateSheetType != "HELOC") 
            {
                LogErrorAndSendEmailToAdmin("Unsupport HelocType=" + RateSheetType + " in CWEQBot. Download 1 rate sheet.");
            }
        }
        public override string BotName 
        {
            get { return "CW2ND"; }
        }

        protected override string DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            // Steps to download ratesheet
            //    - Login
            //    - Hit the rs_url.
            //    - Search the content for xls link.
            //    - Download the xls file
            //    - Logout
            string sFileName = null;

            NameValueCollection postData = new NameValueCollection();
            postData.Add("usr_name", dlinfo.Login);
            postData.Add("usr_password", dlinfo.Password);

            string html = UploadDataReturnResponse(LOGIN_URL, postData);
            if (html.IndexOf("/cld/login") > 0) 
            {
                dlinfo.TurnOffCWDL();
                string sErrorMsg = "(CWEQBot) Account login is invalid. UserName=" + dlinfo.Login;
                ReportDeactivation(sErrorMsg);
                return null;
            }

            postData = new NameValueCollection();
            postData.Add("SellerId", dlinfo.Login.Split('-')[0]); // CountryWide login has following format  {sellerid}-{xx}
            postData.Add("UserId", dlinfo.Login);

            switch (RateSheetType) 
            {
                case "CES":
                    postData.Add("LoanProgGrpId", "4");
                    break;
                case "HELOC":
                    postData.Add("LoanProgGrpId", "1");
                    break;

            }
            html = UploadDataReturnResponse(RS_URL, postData);
            //            html = DownloadData(RS_URL);

            Match match = s_regEx.Match(html);
            if (match.Success) 
            {
                string url = dlinfo.Url + match.Value;
                sFileName = DownloadFile(url);
            } 
            else 
            {
                string sErrorMsg = "(CWEQBot) Unable to locate xls rate sheet for " + dlinfo.Login + ". HTML=" + html;
                LogErrorAndSendEmailToAdmin(sErrorMsg);
				return null;

            }

            GoToUrl(LOGOUT_URL);

            return sFileName;



        }

    }

}
