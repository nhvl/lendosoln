﻿/// Author: Budi Sulayman, Britton Barmeyer

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Threading;

namespace LpeUpdateBotLib.WebBots
{
    public class FreddieMacBot : AbstractWebBot
    {
        private static string ssoUrl = "https://sso.freddiemac.com/";
        private static string homePage = ssoUrl + @"eai-jct/com.ibm.tivoli.sms.cda.app/cda.jsp?referer=https://las.freddiemac.com";
        private static string loginUrl = ssoUrl + @"pkmslogin.form?token=";
        private static string baseUrl = "https://las.freddiemac.com/";
        private static string commonUrl = baseUrl + "loansellingadvisor/dispatch";

        private static Regex token_regex = new Regex(@"form\s+action=""/pkmslogin.form\?token=(?<token>[^""]+)""\s+method=""POST", RegexOptions.IgnoreCase);
        private static Regex failedLogin_regex = new Regex(@"Authentication failed", RegexOptions.IgnoreCase);
        private static Regex requestFMPriceSheet_regex = new Regex(@"href=""/(?<requestFMPriceSheet>[^""]+)""\s+class=""nav"">Request\s+Freddie\s+Mac\s+Cash\s+Price\s+Sheet", RegexOptions.IgnoreCase);
        private static Regex servicingFee_regex = new Regex(@"<option\s+value=""0.25""\s+selected="""">0.250</option>\s+<option value=""0.375"">0.375</option>\s+<option value=""0.5"">0.5</option>\s+</select>", RegexOptions.IgnoreCase);
        private static Regex loanLevelRequest_regex = new Regex(@"""loan_level_request""\s+value=""(?<llrResult>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex formattedRSResult_regex = new Regex(@"""formattedRateSheetResultsForExport""\s+value=""(?<formattedRSResult>[^""]+)"">", RegexOptions.IgnoreCase);

        public FreddieMacBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "FREDDIEMAC"; }
        }

        /// <summary>
        /// BEGIN new headers from 2/8/16
        /// </summary>
        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }
        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }

        protected override string DefaultAcceptLanguage
        {
            get
            {
                return @"en-US";
            }
        }

        protected override string BotUserAgentString
        {
            get
            {
                return @"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";
            }
        }

        protected override bool NeedNetworkCredentials
        {
            get
            {
                return false;
            }
        }
        

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override bool CopyCookiesFromResponse
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// END new headers from 2/8/16
        /// </summary>
        /// 
        protected string AddLoan(string client, NameValueCollection postData, string product, string llr)
        {
            postData = new NameValueCollection();
            if (client.Equals("PML0304"))
            {
                postData.Add("targetPage", "/pe/pricing/jsp/RateSheetLoanInputFormReleased.jsp");
                postData.Add("RateSheetEditLoanEvent", "");
                postData.Add("pricing_option", "Best Efforts");
                postData.Add("servicing_option", "Released");
                postData.Add("servicing_fee", "0.25");
                postData.Add("remittance_option", "Gold");
                postData.Add("all_interest_rates", "all_interest_rates");
                postData.Add("contract_period", "45");
                postData.Add("loan_amount", "366,500.00");
                postData.Add("mortgage_products", product);
                postData.Add("payment_frequency_type", "1");
                postData.Add("section_of_act_type", "");
                postData.Add("loanprogram_identifier", "");
                postData.Add("cross_sell_rights", "Retained By Seller");
                postData.Add("escrow_type", "escrow_type");
                postData.Add("property_type", "1");
                postData.Add("construction_method_type", "1");
                postData.Add("state", "12");
                postData.Add("project_leagl_structure_type", "");
                postData.Add("lp_level", "");
                postData.Add("property_usage_type", "");
                postData.Add("credit_score", "748");
                postData.Add("purpose", "");
                postData.Add("ltv", "64");
                postData.Add("refinance_cashout_dettype", "");
                postData.Add("units", "1");
                postData.Add("edit_action_type", "Add Loan");
                postData.Add("deleted_loan_row", "-1");
                postData.Add("loan_level_request", llr);
                postData.Add("ref", "/pe/pricing/jsp/RateSheetLoanInputFormReleased.jsp");
                postData.Add("formName", "RateSheetEditLoanReleasedForm");
            }
            return UploadDataReturnResponse(commonUrl, postData);
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null;
            string temp = null;
            string token = null;
            NameValueCollection postData;

            /* Login stuff */
            GoToUrl(baseUrl);
            temp = DownloadData(homePage);
           
            // get login token from home page HTML
            Match match = token_regex.Match(temp);
            if (!match.Success) {
                LogErrorAndSendEmailToAdmin("FreddieMacBot cannot find login token for this login: " + dlinfo.Login + ".Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }
            
            token = match.Groups["token"].Value;

            postData = new NameValueCollection();
            postData.Add("username", dlinfo.Login);
            postData.Add("password", dlinfo.Password);
            postData.Add("login-form-type", "pwd");

            temp = UploadDataReturnResponse(loginUrl + token, postData, homePage);

            Match failedLogin_match = failedLogin_regex.Match(temp);

            // failed to login
            if (failedLogin_match.Success)
            {
                LogErrorAndSendEmailToAdmin("DEACTIVATED. FreddieMacBot - Invalid Login. Please update Freddie Mac password and re-enable any dl entry using this login: " + dlinfo.Login + ". Content: " + temp);
                GoToUrl(baseUrl + "pkmslogout");
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            // need to go to this URL in order for requestFMPriceSheet HTML info to get populated
            GoToUrl(baseUrl + "loansellingadvisor//dispatch?formName=CustSetupForm&UserRSACheckEvent=");

            //PML0270
            if ( dlinfo.Description.Contains("MIMUTUAL") )
            {
                postData = new NameValueCollection();
                postData.Add("RoleCategoryTypeCode", "2");
                postData.Add("RoleCategoryTypes", "2");
                postData.Add("OrganizationBranchId", "52005");
                postData.Add("LegacyNumber", "135083");
                postData.Add("NormalUserEventHandler", "Continue");
                postData.Add("isExternalUser", "TRUE");
                postData.Add("IndividualId", "497659");
                postData.Add("OrganizationBranchAlias", "");
                postData.Add("UsersBranchId", "52005");
                postData.Add("targetPage", "");
                postData.Add("targetEvent", "X");
                postData.Add("ref", "/pe/custsetup/jsp/NormalUserSelectBranch.jsp");
                postData.Add("formName", "CustSetupForm");

                UploadData(commonUrl,postData);
                
                postData = new NameValueCollection();
                postData.Add("SellerDisclaimerYesEvent", "I Agree");
                postData.Add("ref", "/pe/custsetup/jsp/OIMSellerDisclaimer.jsp");
                postData.Add("formName", "CustSetupForm");
                temp = UploadDataReturnResponse(commonUrl, postData);
            }
            
            //everyone else
            else
            {
                postData = new NameValueCollection();
                postData.Add("SellerDisclaimerYesEvent", "I Agree");
                postData.Add("ref", "/pe/custsetup/jsp/OIMSellerDisclaimer.jsp");
                postData.Add("formName", "CustSetupForm");
                temp = UploadDataReturnResponse(commonUrl, postData);
            }

            /* Request FM Price Sheeet */
            match = requestFMPriceSheet_regex.Match(temp);
            if (match.Success)
            {
                GoToUrl(baseUrl + match.Groups["requestFMPriceSheet"].Value, commonUrl);

                postData = new NameValueCollection();
                postData.Add("targetPage", "/pe/pricing/jsp/RateSheetContractInputFormRetained.jsp");
                postData.Add("RateSheetInputEvent", "");
                postData.Add("arm_elligible", "true");

                //BE for Best Effort and M for Mandatory
                if (dlinfo.Description.Contains("BE"))
                    postData.Add("pricing_option", "Best Efforts");
                else
                    postData.Add("pricing_option", "Mandatory");

                //REL for Serivicing Release, nothing for Retained
                if (dlinfo.Description.Contains("REL"))
                    postData.Add("servicing_option", "Released");
                else
                    postData.Add("servicing_option", "Retained");

                //LLP for Loan Level Pricing, nothing for Contract Level Pricing
                if (dlinfo.Description.Contains("LLP"))
                    postData.Add("pricing_level", "LOAN_LEVEL_PRICING");
                else
                    postData.Add("pricing_level", "CONTRACT_LEVEL_PRICING");

                postData.Add("ref", "/pe/pricing/jsp/RateSheetAccess.jsp");
                postData.Add("formName", "RateSheetChooseParametersForm");

                temp = UploadDataReturnResponse(commonUrl, postData);

                // If Loan Level Pricing, find Loan Level Request string. If we can't find it, throw error.
                if (dlinfo.Description.Contains("LLP"))
                    match = loanLevelRequest_regex.Match(temp);
                // If Contract Level Pricing, check if there are any changes to servicing fee options (.25; .375; .5). If there are, throw error.
                else
                    match = servicingFee_regex.Match(temp);
                
                if (!match.Success)
                {
                    if (dlinfo.Description.Contains("LLP"))
                        LogErrorAndSendEmailToAdmin("FreddieMacBot can't find match for Loan Level Request string. Please check investor website and update bot if necessary. Login: " + dlinfo.Login + ". Contents: " + temp);
                    else
                        LogErrorAndSendEmailToAdmin("FreddieMacBot can't find match for servicing fee options. Please check investor website and update bot if necessary. Login: "+dlinfo.Login+". Contents: " + temp);
                    GoToUrl(baseUrl + "pkmslogout");
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    /* Loan Level Pricing */
                    if (dlinfo.Description.Contains("LLP"))
                    {
                        string llr = match.Groups["llrResult"].Value.ToString();

                        // create loan level scenarios for PML0304
                        if (dlinfo.Description.Contains("PML0304"))
                        {
                            // add loan scenarios to price sheet, get new Loan Level Request string after adding each loan scenario
                            temp = AddLoan("PML0304", postData, "3001", llr);
                            match = loanLevelRequest_regex.Match(temp);
                            llr = match.Groups["llrResult"].Value.ToString();
                            temp = AddLoan("PML0304", postData, "3002", llr);
                            match = loanLevelRequest_regex.Match(temp);
                            llr = match.Groups["llrResult"].Value.ToString();
                            temp = AddLoan("PML0304", postData, "3003", llr);
                            match = loanLevelRequest_regex.Match(temp);
                            llr = match.Groups["llrResult"].Value.ToString();
                        }
                        // if DL List entry's description doesn't match any of the clients that are set up to do Loan Level Pricing, throw an error
                        else
                        {
                            LogErrorAndSendEmailToAdmin("FreddieMacBot doesn't recognize client for Loan Level Pricing scenario. Please update Download List entry for login: " + dlinfo.Login + ". Description: " + dlinfo.Description);
                            GoToUrl(baseUrl + "pkmslogout");
                            return E_BotDownloadStatusT.Error;
                        }

                        /* create price sheet */
                        postData = new NameValueCollection();
                        postData.Add("targetPage", "/pe/pricing/jsp/RateSheetProcessing.jsp");
                        postData.Add("RateSheetSubmitLoanEvent", "");
                        postData.Add("loan_level_request", llr);
                        postData.Add("pricing_level", "LOAN_LEVEL_PRICING");

                        //BE for Best Effort and M for Mandatory
                        if (dlinfo.Description.Contains("BE"))
                            postData.Add("pricing_option", "Best Efforts");
                        else
                            postData.Add("pricing_option", "Mandatory");

                        //REL for Serivicing Release, nothing for Retained
                        if (dlinfo.Description.Contains("REL"))
                            postData.Add("servicing_option", "Released");
                        else
                            postData.Add("servicing_option", "Retained");

                        postData.Add("servicing_fee", "0.25");

                        //remittance option
                        if (dlinfo.Description.Contains("GOLD"))
                            postData.Add("remittance_option", "Gold");
                        else
                            postData.Add("remittance_option", "First Tuesday");

                        postData.Add("ref", "/pe/pricing/jsp/RateSheetLoanInputFormReleased.jsp");
                        postData.Add("formName", "RateSheetLoanForm");
                    }
                    /* Contract Level Pricing */
                    else
                    {
                        /* create price sheet */
                        postData = new NameValueCollection();
                        postData.Add("targetPage", "/pe/pricing/jsp/RateSheetProcessing.jsp");
                        postData.Add("RateSheetSubmitContractEvent", "");

                        //BE for Best Effort and M for Mandatory
                        if (dlinfo.Description.Contains("BE"))
                            postData.Add("pricing_option", "Best Efforts");
                        else
                            postData.Add("pricing_option", "Mandatory");

                        //REL for Serivicing Release, nothing for Retained
                        if (dlinfo.Description.Contains("REL"))
                            postData.Add("servicing_option", "Released");
                        else
                            postData.Add("servicing_option", "Retained");

                        postData.Add("pricing_level", "CONTRACT_LEVEL_PRICING");
                        postData.Add("arm_elligible", "true");

                        //servicing fee options
                        if (dlinfo.Description.Contains("1"))
                            postData.Add("servicing_fee", "0.25");
                        else if (dlinfo.Description.Contains("2"))
                            postData.Add("servicing_fee", "0.375");
                        else if (dlinfo.Description.Contains("3"))
                            postData.Add("servicing_fee", "0.5");
                        else
                        {
                            LogErrorAndSendEmailToAdmin("FreddieMacBot doesn't recognize servicing fee option. Please update Download List entry for login: " + dlinfo.Login);
                            GoToUrl(baseUrl + "pkmslogout");
                            return E_BotDownloadStatusT.Error;
                        }

                        postData.Add("mortgage_products", "all_mortgage_products");
                        postData.Add("contract_period", "all_contract_periods");
                        postData.Add("all_interest_rates", "all_interest_rates");

                        //remittance option
                        if ((dlinfo.Description.Contains("GOLD")))
                            postData.Add("remittance_option", "Gold");
                        else
                            postData.Add("remittance_option", "First Tuesday");

                        postData.Add("ref", "/pe/pricing/jsp/RateSheetContractInputFormRetained.jsp");
                        postData.Add("formName", "RateSheetContractRetainedForm");
                    }

                    UploadData(commonUrl, postData);

                    /* download formatted rate sheet */
                    temp = DownloadData(commonUrl + "?targetPage=%2Fpe%2Fpricing%2Fjsp%2FRateSheetResults.jsp&RateSheetProcessingEvent=&ref=%2Fpe%2Fpricing%2Fjsp%2FRateSheetProcessing.jsp&formName=RateSheetProcessingForm");
                    match = formattedRSResult_regex.Match(temp);

                    if (match.Success)
                    {
                        string formattedRS = match.Groups["formattedRSResult"].Value.ToString();
                        postData = new NameValueCollection();
                        postData.Add("targetPage", "/pe/pricing/jsp/ExportRateSheetResults.jsp");
                        postData.Add("RateSheetResultsEvent", "");
                        postData.Add("formattedRateSheetResultsForExport", formattedRS);
                        postData.Add("ref", "/pe/pricing/jsp/RateSheetResults.jsp");
                        postData.Add("formName", "RateSheetResultsForm");

                        fileName = UploadDataDownloadFile(commonUrl, postData);
                    }
                    else
                    {
                        LogErrorAndSendEmailToAdmin("FreddieMacBot failed to get formatted RateSheet result for export. Login: " + dlinfo.Login + ". Content: " + temp);
                        GoToUrl(baseUrl + "pkmslogout");
                        return E_BotDownloadStatusT.Error;
                    }
                }
            }
            else
            {
                LogErrorAndSendEmailToAdmin("FreddieMacBot failed to get Request FM Price Sheet link. Login: " + dlinfo.Login + ". Content: " + temp);
                GoToUrl(baseUrl + "pkmslogout");
                return E_BotDownloadStatusT.Error;
            }

            //logout
            GoToUrl(baseUrl + "pkmslogout");

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}