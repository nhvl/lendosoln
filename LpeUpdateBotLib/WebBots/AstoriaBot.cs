﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots
{
    public class AstoriaBot : AbstractWebBot
    {
        private static string homeUrl = @"https://www.astoriamortgage.com";
        private static string loginUrl = @"https://www.astoriamortgage.com/servlet/asfc.mortgage.login.LoginHandler";
        private static Regex rsUrl_regex = new Regex(@"<a\s+href=""(?<rsUrl>[^""]+)""\s+target=""_blank"">Correspondent\s+Rates\(XLS\)", RegexOptions.IgnoreCase);  

        public AstoriaBot(IDownloadInfo info) : base(info)
        {
        }

        public override string BotName
        {
            get { return "ASTORIA"; }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            GoToUrl(homeUrl);
            
            NameValueCollection postData = new NameValueCollection();
            postData.Add("account", dlinfo.Login);
            postData.Add("Submit.x", "-369");
            postData.Add("Submit.y", "-329");

            UploadData(loginUrl, postData, homeUrl);

            string temp = DownloadData(homeUrl + @"/rates/index.jsp");

            Match match = rsUrl_regex.Match(temp);
            string fileName = null;

            if (match.Success)
                fileName = DownloadFile(homeUrl + match.Groups["rsUrl"].Value);

            GoToUrl(homeUrl + @"/servlet/asfc.mortgage.login.doLogOut");

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}
