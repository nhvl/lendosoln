﻿//Kelvin Young
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class FirstMortgageBot : AbstractWebBot
    {
        public FirstMortgageBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "FIRSTMORTGAGE"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //string fileName = DownloadFile(@"https://firstmortgage.lpx.com/v40/ContentBuilders/LogOff.asp?Dest=https://fmcretail.lpx.com/v40/ShowFile.aspx?id=6d9cf105a870444aa65666f6bc58f59e"); //the RS that will be DLed
            string fileName = DownloadFile(@"https://fmcretail.lpx.com/v40/ShowFile.aspx?id=556f295f6fd5454885bcf61e615cf4fe");
            //Check for error
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}