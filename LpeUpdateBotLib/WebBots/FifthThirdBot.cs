﻿//Yin Seo
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class FifthThirdBot : AbstractWebBot
    {
        //list of URLs that will later be needed to login, download RS, logout, ect
        private static string hostUrl = @"https://53.dorado.com";
        private static string loginUrl = hostUrl + @"/login/";
        private static Regex rs_regex = new Regex(@"<li><a class=""taskCenterLink"" href=""(?<name>[^\"" >]+)"" target=""_new"">Correspondent Ratesheet</a></li>");
        private static Regex pwChange_regex = new Regex(@"The security policies require you to change your password.");
        private static string logoutUrl = hostUrl + @"/login/Logout";

        public FifthThirdBot(IDownloadInfo info): base(info)
        {
        }

        public override string BotName
        {
            get { return "FIFTHTHIRD"; }
        }
        
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null; //the RS that will be DLed

            GoToUrl(hostUrl);

            //Login
            NameValueCollection loginInfo = new NameValueCollection();
            loginInfo.Add("User:Username", dlinfo.Login);
            loginInfo.Add("User:Password", dlinfo.Password);
            string rsUrl = UploadDataReturnResponse(string.Format(loginUrl, hostUrl), loginInfo);

            //Download RS
            ///Search for the URL to the ratesheet
            Match match_rs = rs_regex.Match(rsUrl);
            string rs_name = null;
            Match match_pwChange = pwChange_regex.Match(rsUrl);
            if (match_rs.Success)
            {
                rs_name = match_rs.Groups["name"].Value; //extracts the date out from the RS name
            }
            else if (match_pwChange.Success)
            {
                ReportDeactivation("Fifth Third password has expired for login: " + dlinfo.Login + " with bot description: " + dlinfo.Description + ". Please contact the client to reset the password.");
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }
            else
            {
                //Send error msg to support
                LogErrorAndSendEmailToAdmin("Fifth Third Bot: ratesheet was not found. The following was returned by server: \n{0}", rsUrl);
            }
            ///Append the current date
            rsUrl = string.Format("{0}{1}", hostUrl, rs_name);
            fileName = DownloadFile(rsUrl);

            //Logout after
            GoToUrl(logoutUrl);
            NameValueCollection logoutInfo = new NameValueCollection();
            logoutInfo.Add("CS", "Logout | Util.SetProperty Page:Secure FALSE | Util.SetProperty Page:Name Logout | GenerateByURL");
            logoutInfo.Add("SA", "1300487106951-6");
            UploadData(string.Format(logoutUrl, hostUrl), logoutInfo);
            GoToUrl(loginUrl);

            //Check for error
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
        //when pw expire, deactivate dl
    }
}