﻿//Author: Britton Barmeyer


using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class CalHFABot : AbstractWebBot
    {
        public CalHFABot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "CALHFA"; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // DL HTML from website
            string temp = DownloadData(dlinfo.Url);
            // Find effective DateTime of ratesheet in HTML
            string rsRegexString = @"Data below is effective as of <span[^>]+>(?<rsDT>[^\(]+)\(";
            Regex ratesheet_regex = new Regex(rsRegexString, RegexOptions.IgnoreCase);

            // see if our search matches the HTML file we downloaded
            Match match = ratesheet_regex.Match(temp);

            // if there is a match
            if (match.Success)
            {
                // convert the string of the DateTime to a DateTime object
                DateTime rsDateTime = Convert.ToDateTime(match.Groups["rsDT"].Value);
                // if this ratesheet is newer than the last one
                if (rsDateTime.CompareTo(dlinfo.LastRatesheetTimestamp) > 0)
                {
                    // record the DateTime of the new ratesheet to be compared next time, and download the RS
                    dlinfo.RecordLastRatesheetTimestamp(rsDateTime);
                    RatesheetFileName = DownloadFile(dlinfo.Url);
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                {
                    // don't want to DL the RS because it's not updated
                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                }
            }
            else
            {
                // no match - the HTML probably changed
                LogErrorAndSendEmailToAdmin("CalHFABot can't find the ratesheet Timestamp. Please check and verify. Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}