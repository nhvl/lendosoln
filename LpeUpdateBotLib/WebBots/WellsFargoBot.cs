/// Author: David Dao

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
	public class WellsFargoBot : AbstractWebBot
	{
        private string initialUrl = "https://wffnet.wellsfargo.com/ilonline/funding/wff_index.html";
		private string initialUrl1 = "https://wffnet.wellsfargo.com/ilonline/funding/wff_login.jsp";
        private string initialUrl2 = "https://wffnet.wellsfargo.com/ilonline/feature/wff/authorization/login.start.go";
        private static string LogoutUrl = "https://wffnet.wellsfargo.com/ilonline/Logoff.wf";
        private static string ratesheetUrl = "https://wffnet.wellsfargo.com/ilonline/feature/wff/ratesheet/search.start.go?rateSheetTypeParam=correspondent";
        string[] userId = new string [2];
		char[] splitter = {'/'};
        private static Regex sessionExpired_regex = new Regex(@"Your session had expired. Please login again", RegexOptions.IgnoreCase);
        private static Regex csrfToken_regex = new Regex(@"csrfToken:""(?<csrfToken>[^""]+)""", RegexOptions.IgnoreCase);
        string fileName = null;
        string temp = null;
        string LoginUrl = "https://wffnet.wellsfargo.com/ilonline/feature/wff/authorization/login.submit.go?ACTION=userLoginGoButton";
        
        public WellsFargoBot(IDownloadInfo info) : base(info)
		{
		}
        public override string BotName
		{
			get { return "WELLSFARGO"; }
		}
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            GoToUrl(initialUrl);
            GoToUrl(initialUrl1);
            bool loggedIn = login(dlinfo);
            if (loggedIn)
            {
                //try to access ratesheet page. If it hits session expired page, it will retry to login and download ratesheet page again (2 times).
                string content = DownloadData(ratesheetUrl);
                int counter = 0;
                Match sessionExpiredMatch = sessionExpired_regex.Match(content);
                while ((sessionExpiredMatch.Success) && (counter < 2))
                {
                    loggedIn = login(dlinfo);
                    if (loggedIn)
                    {
                        content = DownloadData(ratesheetUrl);
                        sessionExpiredMatch = sessionExpired_regex.Match(content);
                    }
                    counter++;
                }

                if (!loggedIn)
                {
                    LogErrorAndSendEmailToAdmin("WellsFargo bot failed to login. Login has been disabled. Please check and re-enabled login. Content: " + temp);
                    dlinfo.Deactivate();
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    string rsTimePattern = @"<tr\s+class=""searchResultsOddRow"">\s+<td title=""" + DateTime.Now.Date.ToString("d") + @"\s+(?<rstime>[0-9:a-z\s]+)\s+CST";
                    Regex rsTime_regex = new Regex(rsTimePattern, RegexOptions.IgnoreCase);

                    Match rsTimeMatch = rsTime_regex.Match(content);          
                    TimeSpan benchmark = new TimeSpan(8, 0, 0);
                    
                    if (rsTimeMatch.Success) 
                    {
                        DateTime firstRSTime = DateTime.Parse(rsTimeMatch.Groups["rstime"].Value);

                        //check to see if there is more than 1 ratesheet published today
                        rsTimeMatch = rsTimeMatch.NextMatch();
                        while (rsTimeMatch.Success)
                        {
                            DateTime secondRSTime = DateTime.Parse(rsTimeMatch.Groups["rstime"].Value);
                            if (firstRSTime.CompareTo(secondRSTime) < 0)
                                firstRSTime = secondRSTime;
                            rsTimeMatch = rsTimeMatch.NextMatch();
                        }

                        string ratesheetLinkPattern = @"<td title=""" + DateTime.Now.Date.ToString("d") + @"\s+" + firstRSTime.ToString("t") + @"\s+CST""\s+align=""center"">" + DateTime.Now.Date.ToString("d") + @"\s+" + firstRSTime.ToString("t") + @"\s+CST</td>\s+<td title=""([0-9]+)""\s+align=""center"">([0-9]+)</td>\s+<td title=""[^""]+""\s+align=""center""><a href=""[^""]+""\s+title=""[^""]+"">PDF</a></td>\s+<td title=""[^""]+""\s+align=""center""><a href=""(?<rslink>[^""]+)""\s+title=""[^""]+"">Excel";

                        Regex ratesheetLink = new Regex(ratesheetLinkPattern, RegexOptions.IgnoreCase);
                        Match match = ratesheetLink.Match(content);

                        if (match.Success)
                        {
                            fileName = DownloadFile("https://wffnet.wellsfargo.com" + match.Groups["rslink"].Value);
                        }
                        else
                        {
                            LogErrorAndSendEmailToAdmin("Contents for WellsFargo bot change. Content=" + content);
                        }
                    }
                    else if (!rsTimeMatch.Success && (DateTime.Now.TimeOfDay > benchmark))
                    {
                        LogErrorAndSendEmailToAdmin("WellsFargo bot doesn't find today ratesheet. Please check. Content= " + content);
                    }
                }
                //Logout
                GoToUrl(LogoutUrl);
            }
            else
                LogErrorAndSendEmailToAdmin("WellsFargo bot failed to login. Please check. Content: " + temp);

            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }
        }

        private bool login(IDownloadInfo dlinfo)
        {
            bool loggedIn = false;
            int counter = 0;
            
            userId = dlinfo.Login.Split(splitter);
            NameValueCollection postData = new NameValueCollection();

            while ((loggedIn == false) && (counter < 2))
            {
                temp = DownloadData(initialUrl2);
                Match match = csrfToken_regex.Match(temp);
                if (match.Success)
                {
                    string csrfTokenString = match.Groups["csrfToken"].Value;
                    string temp1 = csrfTokenString.Replace("2D", "-");
                    int i = temp1.IndexOf('\\');
                    while (i > -1)
                    {
                        csrfTokenString = temp1.Remove(i, 2);
                        temp1 = csrfTokenString;
                        i = temp1.IndexOf('\\');
                    }

                    postData.Add("wffAdminIndicator", "");
                    postData.Add("userAccountUpdatedIndicator", "");
                    postData.Add("userAccountLockedIndicator", "");
                    postData.Add("userLoginForgotPasswordSubmitIndicator", "");
                    postData.Add("csrfToken", csrfTokenString);
                    postData.Add("formId", "userLogin");
                    postData.Add("companyId", userId[0]);
                    postData.Add("userId", userId[1]);
                    postData.Add("userLoginPassword", dlinfo.Password);

                    temp = UploadDataReturnResponse(LoginUrl, postData);

                    Match loginMatch = sessionExpired_regex.Match(temp);
                    if (!loginMatch.Success)
                        loggedIn = true;
                }
                counter++;
            }

            return loggedIn;
        }
    }

}
