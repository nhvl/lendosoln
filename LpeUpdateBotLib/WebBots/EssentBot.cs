﻿/// Author: Yin Seo

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class EssentBot : AbstractWebBot
    {
        private static string stdUrl = @"http://essent.us/index.php/home/lenders/rates-and-guidelines/";
        private static string cuUrl = @"http://essent.us/index.php/home/lenders/credit_union-rates-guidelines/";

        //private static Regex stdRatesDate_regex = new Regex(@"Rates\s+\?\?\?\s+Effective\?\?(?<date>[^<]+)</strong>");
        private static Regex stdRatesDate_regex = new Regex(@"<strong>Current Rate Availability Chart as of October 3, 2014</strong>");
        private static Regex stdGuidesDate_regex = new Regex(@"Underwriting\s+Guidelines\s+\?\?\?\s+Effective\s+(?<date>[^<]+)");
        //private static Regex cuRatesDate_regex = new Regex(@"Credit Union Rates</span></div>\n<div class=""[^""]+""></div>\n<p><a href=""[^""]+""><strong>Current Rate Availability Chart as of September 15, 2014</strong></a></p>\n<p><span\s+style=\""color:\s+#([0-9:a-zA-Z\s]*);\""><strong>Rates\s+\?\?\?\s+Effective\?\?(?<date>[^<]+)", RegexOptions.IgnoreCase);
        private static Regex cuRatesDate_regex = new Regex(@"Credit Union Rates</span></div>\n<div class=""[^""]+""></div>\n<p><a href=""[^""]+""><strong>Current Rate Availability Chart as of October 3, 2014</strong>", RegexOptions.IgnoreCase);
        private static Regex cuGuidesDate_regex = new Regex(@"Standard Guidelines<sup>2</sup></span></div>\n<p><span\s+style=\""color:\s+#([0-9:a-zA-Z\s]*);\""><strong>Underwriting\s+Guidelines\s+\?\?\?\s+Effective\s+(?<date>[^<]+)");
        private static Regex cuGuides_regex = new Regex(@"For Credit Union loans, Essent\?\?\?s Standard Guidelines apply.");

        List<string> listOfChanges = new List<string>();
        List<string> listOfRegexError = new List<string>();
        
        //*********************CHANGE DATES HERE***************************
        //private static string stdRatesDate = @"May 5, 2014";
        private static string stdGuidesDate = @"May 5, 2014";
        //private static string cuRatesDate = @"July 14, 2014";
        private static string cuGuidesDate = @"May 5, 2014";

        public EssentBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "ESSENT"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string data = null;
            
            //***STANDARD***
            data = DownloadData(stdUrl);
            //Check standard rates date
            Match stdRatesMatch = stdRatesDate_regex.Match(data);
            if (stdRatesMatch.Success)
            {
                //if (!stdRatesMatch.Groups["date"].Value.Equals(stdRatesDate))
                //    listOfChanges.Add("Standard Rates Date");
            }
            else
                listOfRegexError.Add("Standard Rates Regex");
            //Check guideline date
            Match stdGuidesMatch = stdGuidesDate_regex.Match(data);
            if (stdGuidesMatch.Success)
            {
                if (!stdGuidesMatch.Groups["date"].Value.Equals(stdGuidesDate))
                    listOfChanges.Add("Standard Guidelines Date");
            }
            else
                listOfRegexError.Add("Standard Guidelines Regex");

            //***CREDIT UNION***
            data = DownloadData(cuUrl);
            //Check cu rates date
            Match cuRatesMatch = cuRatesDate_regex.Match(data);
            if (cuRatesMatch.Success)
            {
               // string temp = cuRatesMatch.Groups["date"].Value;
               // if (!cuRatesMatch.Groups["date"].Value.Equals(cuRatesDate))
               //     listOfChanges.Add("Credit Union Rates Date");
            }
            else
                listOfRegexError.Add("Credit Union Rates Regex");
            //Check guideline date
            Match cuGuidesMatch = cuGuidesDate_regex.Match(data);
            if (cuGuidesMatch.Success)
            {
                string temp1 = cuGuidesMatch.Groups["date"].Value;
                if (!cuGuidesMatch.Groups["date"].Value.Equals(cuGuidesDate))
                    listOfChanges.Add("Credit Union Guidelines Date");
            }
            else
                listOfRegexError.Add("Credit Union Guidelines Regex");
            //Check to see if CU guides still follows std
            Match cuGuides = cuGuides_regex.Match(data);
            if (!cuGuides.Success)
            {
                listOfRegexError.Add("Credit Union Guidelines may not be following Standard Guidelines");
            }

            //if there are changes, then send to PML
            if ((listOfChanges.Count != 0) || (listOfRegexError.Count != 0))
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine("Essent Bot detects the following changes. Please check the changes and update the bot and/or template: ");
                foreach (string change in listOfChanges)
                    errorMessage.AppendLine(change);
                foreach (string regexError in listOfRegexError)
                    errorMessage.AppendLine(regexError);
                LogErrorAndSendEmailToAdmin(errorMessage.ToString());
                return E_BotDownloadStatusT.Error;
            }

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
    }
}


