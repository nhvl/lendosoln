﻿//Kelvin Young
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class GNMABot : AbstractWebBot
    {
        public GNMABot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "GNMA"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = DownloadFile(@"http://www.mbsquoteline.com/csv_gnma.php"); //the RS that will be DLed

            //Check for error
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("GNMABot failed to download ratesheet. Please check URL of the ratesheet.");
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}