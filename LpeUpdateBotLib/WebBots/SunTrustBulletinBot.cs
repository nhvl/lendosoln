﻿//Kelvin Young
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class SunTrustBulletinBot : AbstractWebBot
    {
        const string bulletinUrl = @"https://old.stmpartners.com/manual/common/GHR_bulletins_cor.asp?doctype=bulletin";

        public SunTrustBulletinBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "SUNTRUSTBULLETIN"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string pageInfo = null;
            
            pageInfo = DownloadData(bulletinUrl);

            //Bulletin number in description e.g. "COR 15-006"
            //so that we don't need to update the bot every time.
            Regex bulletin_regex = new Regex(dlinfo.Description, RegexOptions.IgnoreCase);

            Match matchBulletin = bulletin_regex.Match(pageInfo);
            if (matchBulletin.Success)
            {
                LogErrorAndSendEmailToAdmin(string.Format("A new SunTrust bulletin (" + dlinfo.Description + ") has been posted. It can be found at: https://old.stmpartners.com/manual/common/GHR_bulletins_cor.asp?doctype=bulletin Please verify and update the description on Download List.", pageInfo));
            }
            else if(pageInfo.Contains("COR 17-001"))//check for new year.
            {
                LogErrorAndSendEmailToAdmin(string.Format("A new SunTrust bulletin COR 17-001 has been posted. It can be found at: https://old.stmpartners.com/manual/common/GHR_bulletins_cor.asp?doctype=bulletin Please verify and update the bot and description with the new year", pageInfo));
            }

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
    }
}