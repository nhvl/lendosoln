/// Author: Budi Sulayman

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class FannieMaeBot : AbstractWebBot
    {
        private static string loginUrl = "https://committing.efanniemae.com/eCommitting/eCommitting";
        private static Regex sessionId_regex = new Regex(@"<input\s+type=hidden\s+name=originalSessionId\s+value=""(?<sessionId>[^""]+)""");
        private static Regex browsePrice_regex = new Regex(@"<select\s+name=""filterCriteria_selectedProduct_value""\s+(?<prodValueTemp>[\w+\s+\W+]+)</select>");
        private static Regex productValue_regex = new Regex(@"option\s+value=""(?<prodValue>[^""]+)""");
        private static Regex loginPage_regex = new Regex(@"Enter\s+your\s+user\s+ID\s+and\s+password\s+to\s+login", RegexOptions.IgnoreCase);
        

        public FannieMaeBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "FANNIEMAE"; }
        }

        public override long TimeoutInMs
        {
            get
            {
                return 480000;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //split dlinfo.login to 2 userId with splitter '/'. First one is user id for login and the second one is branch number to access ratesheet
            string[] userId = new string[2];
            char[] splitter = { '/' };
            userId = dlinfo.Login.Split(splitter);

            bool loggedIn = Login(userId, dlinfo);
            if (loggedIn)
            {
                //Go to Browse Price page to grab option value for each product
                NameValueCollection postData = new NameValueCollection();
                postData.Add("actionResource", "GotoPrices");
                postData.Add("submittedPage", "Main_Menu");

                UploadData(loginUrl, postData);

                postData.Clear();
                postData.Add("actionResource", "GotoPricesProductSearch");
                postData.Add("submittedPage", "Browse_Prices");

                string temp = UploadDataReturnResponse(loginUrl, postData);

                //Create an array of all the programs offered (not include convertible one, but add 11 FHA programs)
                int[] LPList = new int[23] { 331, 4, 337, 3, 883, 2, 333, 879, 854, 1, 334, 336, 914, 888, 923, 924, 928, 934, 919, 913, 918, 384, 884 };

                StringBuilder rsPage = new StringBuilder();
                Match match = browsePrice_regex.Match(temp);
                if (match.Success)
                {
                    if (dlinfo.Description.EndsWith("AA"))
                        rsPage = AAPostRsData(LPList, rsPage, temp, userId, dlinfo);
                    else if (dlinfo.Description.EndsWith("SS"))
                        rsPage = SSPostRsData(LPList, rsPage, temp, userId, dlinfo);
                    else
                    {
                        LogErrorAndSendEmailToAdmin("FannieMae bot encounters error description for id: " + userId[0] + ". Please update it");
                        return E_BotDownloadStatusT.Error;
                    }
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("FannieMae bot can't access pricing for user id: " + userId[0] + ". Content: " + temp);
                    return E_BotDownloadStatusT.Error;
                }

                //Check if the pricing string builder contains error or not
                if (rsPage.ToString().Contains("Error in pulling pricing"))
                    return E_BotDownloadStatusT.Error;
                else
                {
                    RatesheetFileName = WriteToTempFile(rsPage.ToString());
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
            }
            else
                return E_BotDownloadStatusT.Error;
        }

        private Boolean Login(string[] userId, IDownloadInfo dlinfo)
        {
            string temp = DownloadData(loginUrl);
            string originalSessionId = null;

            //Grab session Id for login
            Match match = sessionId_regex.Match(temp);
            if (match.Success)
                originalSessionId = match.Groups["sessionId"].Value;
            else
            {
                LogErrorAndSendEmailToAdmin("FannieMae bot can't find original sessionId to login for user id: " + userId[0] + ". Content: " + temp);
                return false;
            }

            NameValueCollection postData = new NameValueCollection();
            postData.Add("actionResource", "LoginSubmit");
            postData.Add("originalSessionId", originalSessionId);
            postData.Add("submittedPage", "Login");
            postData.Add("userID", userId[0]);
            postData.Add("password", dlinfo.Password);

            //login
            temp = UploadDataReturnResponse(loginUrl, postData);

            //Grab session id after login
            match = sessionId_regex.Match(temp);
            if (match.Success)
                originalSessionId = match.Groups["sessionId"].Value;
            else
            {
                match = loginPage_regex.Match(temp);
                if (match.Success)
                {
                    LogErrorAndSendEmailToAdmin("FannieMae bot can't login. Please check login and re-enable login for user id: " + userId[0]);
                    dlinfo.Deactivate();
                    return false;
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("FannieMae bot can't find original sessionId to access pricing for user id: " + userId[0] + ". Content: " + temp);
                    return false;
                }
            }

            postData = new NameValueCollection();
            postData.Add("actionResource", "SelectSeller");
            postData.Add("originalSessionId", originalSessionId);
            postData.Add("submittedPage", "Select_Seller+Service_Number");
            postData.Add("selectedBranchNumber", userId[1]);

            UploadData(loginUrl, postData);

            return true;
        }
        //Getting Actual/Actual pricing and concatenate it to a string
        private StringBuilder AAPostRsData(int[] LPList, StringBuilder rsPage, string temp, string[] userId, IDownloadInfo dlinfo)
        {
            foreach (int i in LPList)
            {
                //Check if the product has AA pricing or not before request pricing
                string AARegexString = @"\(thisForm\[""options2\W+" + i + @"""\]\)\[[0-9]\]\s+=\s+new\s+Option\(""Actual/Actual"",""1""";
                Regex AA_regex = new Regex(AARegexString, RegexOptions.IgnoreCase);
                Match match = AA_regex.Match(temp);
                if (match.Success)
                {
                    NameValueCollection postRsData = new NameValueCollection();
                    postRsData.Add("actionResource", "GetPricesProductSearch");
                    postRsData.Add("submittedPage", "Browse_Prices___");
                    postRsData.Add("filterCriteria_selectedFamily_value", "All");
                    postRsData.Add("filterCriteria_selectedProduct_value", i.ToString());
                    postRsData.Add("filterCriteria_remittanceType_value", "1");

                    UploadData(loginUrl, postRsData);

                    postRsData.Clear();
                    postRsData.Add("actionResource", "ExportSelectPricePage");
                    postRsData.Add("submittedPage", "Select_Price");

                    string rsPricing = UploadDataReturnResponse(loginUrl, postRsData);
                    match = loginPage_regex.Match(rsPricing);

                    //If being logged out while pulling price, then try to login again. It will try twice
                    int counter = 0;
                    while ((match.Success) && (counter < 2 ))
                    {
                        bool loggedIn2 = Login(userId, dlinfo);
                        if (loggedIn2)
                        {
                            //Go to Browse Price page to grab option value for each product
                            NameValueCollection postData = new NameValueCollection();
                            postData.Add("actionResource", "GotoPrices");
                            postData.Add("submittedPage", "Main_Menu");

                            UploadData(loginUrl, postData);

                            postData.Clear();
                            postData.Add("actionResource", "GotoPricesProductSearch");
                            postData.Add("submittedPage", "Browse_Prices");

                            string temp2 = UploadDataReturnResponse(loginUrl, postData);

                            match = AA_regex.Match(temp2);
                            if (match.Success)
                            {
                                postRsData = new NameValueCollection();
                                postRsData.Add("actionResource", "GetPricesProductSearch");
                                postRsData.Add("submittedPage", "Browse_Prices___");
                                postRsData.Add("filterCriteria_selectedFamily_value", "All");
                                postRsData.Add("filterCriteria_selectedProduct_value", i.ToString());
                                postRsData.Add("filterCriteria_remittanceType_value", "1");

                                UploadData(loginUrl, postRsData);

                                postRsData.Clear();
                                postRsData.Add("actionResource", "ExportSelectPricePage");
                                postRsData.Add("submittedPage", "Select_Price");

                                rsPricing = UploadDataReturnResponse(loginUrl, postRsData);
                                match = loginPage_regex.Match(rsPricing);
                            }
                        }
                        counter++;
                    }
                    if (match.Success)
                    {
                        rsPage.AppendLine("Failed to pull AA pricing for product id: " + i.ToString());
                        break;
                    }
                    else
                        rsPage.AppendLine(rsPricing);
                }
                else
                    rsPage.AppendLine("Product code: " + i + " doesn't have AA pricing.\n");
            }
            return rsPage;
        }

        //Getting Scheduled/Scheduled pricing and concatenate it to a string
        private StringBuilder SSPostRsData(int[] LPList, StringBuilder rsPage, string temp, string[] userId, IDownloadInfo dlinfo)
        {
            foreach (int i in LPList)
            {
                //Check if the product has SS pricing or not before request pricing
                string SSRegexString = @"\(thisForm\[""options2\W+" + i + @"""\]\)\[[0-9]\]\s+=\s+new\s+Option\(""Scheduled/Scheduled"",""3""";
                Regex SS_regex = new Regex(SSRegexString, RegexOptions.IgnoreCase);
                Match match = SS_regex.Match(temp);
                if (match.Success)
                {
                    NameValueCollection postRsData = new NameValueCollection();
                    postRsData.Add("actionResource", "GetPricesProductSearch");
                    postRsData.Add("submittedPage", "Browse_Prices___");
                    postRsData.Add("filterCriteria_selectedFamily_value", "All");
                    postRsData.Add("filterCriteria_selectedProduct_value", i.ToString());
                    postRsData.Add("filterCriteria_remittanceType_value", "3");

                    UploadData(loginUrl, postRsData);

                    postRsData.Clear();
                    postRsData.Add("actionResource", "ExportSelectPricePage");
                    postRsData.Add("submittedPage", "Select_Price");

                    string rsPricing = UploadDataReturnResponse(loginUrl, postRsData);
                    match = loginPage_regex.Match(rsPricing);

                    //If being logged out while pulling price, then try to login again. It will try twice
                    int counter = 0;
                    while ((match.Success) && (counter < 2))
                    {
                        bool loggedIn2 = Login(userId, dlinfo);
                        if (loggedIn2)
                        {
                            //Go to Browse Price page to grab option value for each product
                            NameValueCollection postData = new NameValueCollection();
                            postData.Add("actionResource", "GotoPrices");
                            postData.Add("submittedPage", "Main_Menu");

                            UploadData(loginUrl, postData);

                            postData.Clear();
                            postData.Add("actionResource", "GotoPricesProductSearch");
                            postData.Add("submittedPage", "Browse_Prices");

                            string temp2 = UploadDataReturnResponse(loginUrl, postData);

                            match = SS_regex.Match(temp2);
                            if (match.Success)
                            {
                                postRsData = new NameValueCollection();
                                postRsData.Add("actionResource", "GetPricesProductSearch");
                                postRsData.Add("submittedPage", "Browse_Prices___");
                                postRsData.Add("filterCriteria_selectedFamily_value", "All");
                                postRsData.Add("filterCriteria_selectedProduct_value", i.ToString());
                                postRsData.Add("filterCriteria_remittanceType_value", "3");

                                UploadData(loginUrl, postRsData);

                                postRsData.Clear();
                                postRsData.Add("actionResource", "ExportSelectPricePage");
                                postRsData.Add("submittedPage", "Select_Price");

                                rsPricing = UploadDataReturnResponse(loginUrl, postRsData);
                                match = loginPage_regex.Match(rsPricing);
                            }
                        }
                        counter++;
                    }
                    if (match.Success)
                    {
                        rsPage.AppendLine("Failed to pull SS pricing for product id: " + i.ToString());
                        break;
                    }
                    else
                        rsPage.AppendLine(rsPricing);
                }
                else
                    rsPage.AppendLine("Product code: " + i + " doesn't have SS pricing.\n");
            }
            return rsPage;
        }

        //Getting Actual/Scheduled pricing and concatenate it to a string
        private StringBuilder ASPostRsData(int[] LPList, StringBuilder rsPage, string temp)
        {
            foreach (int i in LPList)
            {
                //Check if the product has AS pricing or not before request pricing
                string ASRegexString = @"\(thisForm\[""options2\W+" + i + @"""\]\)\[[0-9]\]\s+=\s+new\s+Option\(""Actual/Scheduled"",""4""";
                Regex AS_regex = new Regex(ASRegexString, RegexOptions.IgnoreCase);
                Match match = AS_regex.Match(temp);
                if (match.Success)
                {
                    NameValueCollection postRsData = new NameValueCollection();
                    postRsData.Add("actionResource", "GetPricesProductSearch");
                    postRsData.Add("submittedPage", "Browse_Prices___");
                    postRsData.Add("filterCriteria_selectedFamily_value", "All");
                    postRsData.Add("filterCriteria_selectedProduct_value", i.ToString());
                    postRsData.Add("filterCriteria_remittanceType_value", "4");

                    UploadData(loginUrl, postRsData);

                    postRsData.Clear();
                    postRsData.Add("actionResource", "ExportSelectPricePage");
                    postRsData.Add("submittedPage", "Select_Price");

                    string rsPricing = UploadDataReturnResponse(loginUrl, postRsData);
                    match = loginPage_regex.Match(rsPricing);
                    if (match.Success)
                    {
                        rsPage.AppendLine("Error in pulling pricing");
                        break;
                    }
                    else
                        rsPage.AppendLine(rsPricing);
                }
                else
                    rsPage.AppendLine("Product code: " + i + " doesn't have AS pricing.\n");
            }
            return rsPage;
        }

        //Getting Scheduled/Actual pricing and concatenate it to a string
        private StringBuilder SAPostRsData(int[] LPList, StringBuilder rsPage, string temp)
        {
            foreach (int i in LPList)
            {
                //Check if the product has SA pricing or not before request pricing
                string SARegexString = @"\(thisForm\[""options2\W+" + i + @"""\]\)\[[0-9]\]\s+=\s+new\s+Option\(""Scheduled/Actual"",""2""";
                Regex SA_regex = new Regex(SARegexString, RegexOptions.IgnoreCase);
                Match match = SA_regex.Match(temp);
                if (match.Success)
                {
                    NameValueCollection postRsData = new NameValueCollection();
                    postRsData.Add("actionResource", "GetPricesProductSearch");
                    postRsData.Add("submittedPage", "Browse_Prices___");
                    postRsData.Add("filterCriteria_selectedFamily_value", "All");
                    postRsData.Add("filterCriteria_selectedProduct_value", i.ToString());
                    postRsData.Add("filterCriteria_remittanceType_value", "2");

                    UploadData(loginUrl, postRsData);

                    postRsData.Clear();
                    postRsData.Add("actionResource", "ExportSelectPricePage");
                    postRsData.Add("submittedPage", "Select_Price");

                    string rsPricing = UploadDataReturnResponse(loginUrl, postRsData);
                    match = loginPage_regex.Match(rsPricing);
                    if (match.Success)
                    {
                        rsPage.AppendLine("Error in pulling pricing");
                        break;
                    }
                    else
                        rsPage.AppendLine(rsPricing);
                }
                else
                    rsPage.AppendLine("Product code: " + i + " doesn't have SA pricing.\n");
            }
            return rsPage;
        }
    }

}

