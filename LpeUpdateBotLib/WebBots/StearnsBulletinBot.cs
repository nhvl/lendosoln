﻿//Yin Seo
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class StearnsBulletinBot : AbstractWebBot
    {
        private string bulletinUrl = "http://stearnscorrespondent.com/bulletins.php";

        /********** UPDATE REGEX BULLETIN NUMBER HERE **********/
        private static string bulletinNumber_1 = "1014-35"; //current bulletin # +1 (same month)
        private static string bulletinNumber_2 = "1114-35"; //current bulletin # +1 (next month)
        /********** UPDATE REGEX BULLETIN NUMBER HERE **********/

        private static Regex bulletin_regex_1 = new Regex(bulletinNumber_1, RegexOptions.IgnoreCase); //Regex checks bulletin number
        private static Regex bulletin_regex_2 = new Regex(bulletinNumber_2, RegexOptions.IgnoreCase); //Regex checks bulletin number

        public StearnsBulletinBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "STEARNSBULLETIN"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string pageInfo = DownloadData(bulletinUrl);

            //Check for new bulletin
            Match matchBulletin_1 = bulletin_regex_1.Match(pageInfo);
            Match matchBulletin_2 = bulletin_regex_2.Match(pageInfo);

            if (matchBulletin_1.Success) //checks for the first one with the same month
            {
                LogErrorAndSendEmailToAdmin(string.Format("A new Stearns bulletin (" + bulletinNumber_1 + ") has been posted. Please verify at " + bulletinUrl + ", then update and reenable the bot.", pageInfo));
                dlinfo.Deactivate();
            }

            if (matchBulletin_2.Success) //checks for the 2nd one with the next month
            {
                LogErrorAndSendEmailToAdmin(string.Format("A new Stearns bulletin (" + bulletinNumber_2 + ") has been posted. Please verify at " + bulletinUrl + ", then update and reenable the bot.", pageInfo));
                dlinfo.Deactivate();
            }

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
    }
}