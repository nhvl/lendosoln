﻿// Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots
{
    public class IHDABot : AbstractWebBot
    {
        private static string baseUrl = @"https://ilrss.ihda.org/ilrss";
        private static string loginUrl = baseUrl + @"/wb002.cbx";

        private static Regex session_id_regex = new Regex(@"NAME=""session_id""\s+value=""(?<session_id>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex invalid_login_regex = new Regex(@"<title>Login Error</title>", RegexOptions.IgnoreCase);

        public IHDABot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "IHDA"; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }

        protected override string DefaultAcceptLanguage
        {
            get
            {
                return "en-US";
            }
        }

        protected override string BotUserAgentString
        {
            get
            {
                return "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";
            }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected void Logout(NameValueCollection postData, string customer, string[] userId, string session_id)
        {
            postData = new NameValueCollection();
            postData.Add("customer", "00000" + customer);
            postData.Add("web_profile", "0001");
            postData.Add("menu_seq", "0000");
            postData.Add("menu", "PROGRAMS");
            postData.Add("menu_tab", "0001");
            postData.Add("menu_name", "Available Programs");
            postData.Add("menu_type", "0005");
            postData.Add("screen_painter", "MLW220.CBX");
            postData.Add("screen_processor", "MLW220.CBX");
            postData.Add("screen_no", "0000");
            postData.Add("next_menu", "ALLOCATION");
            postData.Add("next_menu_seq", "0000");
            postData.Add("sub_customer", "0" + userId[0]);
            postData.Add("sub_sub_customer", "000" + userId[1]);
            postData.Add("username", userId[2].ToUpper());
            postData.Add("key_status", "0000");
            postData.Add("form_no", "0000");
            postData.Add("session_id", session_id);
            postData.Add("mlw220_program_type", "0000");
            postData.Add("mlw220_sub_prg_type", "0000");
            postData.Add("dummy", "0000");

            UploadData(loginUrl, postData);
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // if it's before 8am or we've already downloaded a rate sheet today, don't download
            if ((DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(8)) < 0) || dlinfo.LastRatesheetTimestamp.Date == DateTime.Today)
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            //split dlinfo.login to 2 userId with splitter '/'. First one is user id for login and the second one is branch number to access ratesheet
            string[] userId = new string[3];
            char[] splitter = { '/' };
            userId = dlinfo.Login.Split(splitter);

            string fileName, temp, customer = null;

            // set the value of the "customer" field that we will be POSTing to login
            if (userId[2].Equals("PMLSupport"))
            {
                customer = "9169";
            }
            else
            {
                customer = null;
            }

            // add all data we will be POSTing to login
            NameValueCollection postData = new NameValueCollection();
            postData.Add("customer", customer);
            postData.Add("menu_type", "0002");
            postData.Add("next_menu_seq", "0000");
            postData.Add("key_status", "0001");
            postData.Add("login_screen", "1");
            postData.Add("login_type", "1");
            postData.Add("web_profile", "1");
            postData.Add("sub_customer", userId[0]);
            postData.Add("sub_sub_customer", userId[1]);
            postData.Add("username", userId[2]);
            postData.Add("password", dlinfo.Password);

            // login
            temp = UploadDataReturnResponse(loginUrl, postData);

            // check to make sure we actually logged in
            Match invalid_login_match = invalid_login_regex.Match(temp);

            // if we didn't, throw an error
            if (invalid_login_match.Success)
            {
                LogErrorAndSendEmailToAdmin("DEACTIVATED. IHDA Bot failed to login for: " + userId[2] + ". Content: " + temp);
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            // find the session ID
            Match session_id_match = session_id_regex.Match(temp);
            if (!session_id_match.Success)
            {
                LogErrorAndSendEmailToAdmin("IHDA Bot could not match Session ID for login: " + userId[2] + ". Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }

            // add all data we will be POSTing to navigate to New Loan Registration page
            postData = new NameValueCollection();
            postData.Add("customer", "00000" + customer);
            postData.Add("web_profile", "0001");
            postData.Add("menu_seq", "0000");
            postData.Add("menu_tab", "0001");
            postData.Add("menu_name", "New Loan Registration");
            postData.Add("menu_type", "0001");
            postData.Add("screen_no", "0000");
            postData.Add("next_menu", "PROGRAMS");
            postData.Add("next_menu_seq", "0000");
            postData.Add("sub_customer", "0" + userId[0]);
            postData.Add("sub_sub_customer", "000" + userId[1]);
            postData.Add("username", userId[2].ToUpper());
            postData.Add("key_status", "0001");
            postData.Add("form_no", "0000");
            postData.Add("session_id", session_id_match.Groups["session_id"].Value);

            // navigate to New Loan Registration page, download HTML
            fileName = UploadDataDownloadFile(loginUrl, postData);

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                Logout(postData, customer, userId, session_id_match.Groups["session_id"].Value);
                dlinfo.RecordLastRatesheetTimestamp(DateTime.Now);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                Logout(postData, customer, userId, session_id_match.Groups["session_id"].Value);
                LogErrorAndSendEmailToAdmin("IHDA Bot failed to download RS for login: " + userId[2] + ". Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}