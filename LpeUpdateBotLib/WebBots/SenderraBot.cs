// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
namespace LpeUpdateBotLib.WebBots
{
	public class SenderraBot : AbstractWebBot
	{
		private static string loginURL = "https://www.senderra.com/default.aspx";
		private static string productURL = "https://www.senderra.com/Pages/products.aspx";
		private static Regex s_viewStateRegex = new Regex (@"<input type=""[^""]+""\s+name=""__VIEWSTATE""\s+id=""__VIEWSTATE""\s+value=""(?<viewstateId>[^""]+)"" />");
        private static Regex s_NoRateSheetRegex = new Regex(@"No rates have been uploaded today");
		private static Regex s_eventValidRegex = new Regex (@"<input type=""[^""]+""\s+name=""__EVENTVALIDATION""\s+id=""__EVENTVALIDATION""\s+value=""(?<eventValidId>[^""]+)"" />");
		
		public SenderraBot(IDownloadInfo info) : base(info)
		{
		}
		public override string BotName 
		{
			get { return "SENDERRA"; }
		}
		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{	
			string temp = null;
			string ratesheetURL = null;
			string viewStateValue = null;
			string eventValidValue = null;
			temp = DownloadData (loginURL);
			Match match = s_viewStateRegex.Match(temp);
			if (match.Success)
			{
				viewStateValue = match.Groups["viewstateId"].Value;
				match = s_eventValidRegex.Match(temp);
				if (match.Success)
					eventValidValue = match.Groups["eventValidId"].Value;
				else
				{
					LogErrorAndSendEmailToAdmin ("Senderra Bot can't find EventValidation value. Content: "+temp);
					return E_BotDownloadStatusT.Error;
				}
			}
			else
			{
				LogErrorAndSendEmailToAdmin ("Senderra Bot can't find Viewstate value. Content: "+temp);
				return E_BotDownloadStatusT.Error;
			}

			NameValueCollection postData = new NameValueCollection();
			postData.Add ("__EVENTTARGET", "");
			postData.Add ("__EVENTARGUMENT", "");
			postData.Add ("__VIEWSTATE", viewStateValue);
			postData.Add ("ctl00$cphSubNavigation$MyAccordion_AccordionExtender_ClientState","-1");
			postData.Add ("ctl00$cphLogin$lgn$loginview$lg$UserName", dlinfo.Login);
			postData.Add ("ctl00$cphLogin$lgn$loginview$lg$Password", dlinfo.Password);
			postData.Add ("ctl00$cphLogin$lgn$loginview$lg$LoginButton", "Log In");
			postData.Add ("__EVENTVALIDATION", eventValidValue);

			UploadData (loginURL, postData);

			temp = DownloadData (productURL);
            string todayDate = DateTime.Now.Date.ToString("yyMMdd");
            string rsRegexString = @"<a\s+href=""(?<ratesheetId>[^""]+)""\s+target=""[^""]+"">SNDR_" + todayDate + @"E.xls";
            string rsRegexString2 = @"<a\s+href=""(?<ratesheetId>[^""]+)""\s+target=""[^""]+"">SNDR_" + todayDate + @"D.xls";
            string rsRegexString3 = @"<a\s+href=""(?<ratesheetId>[^""]+)""\s+target=""[^""]+"">SNDR_" + todayDate + @"C.xls";
            string rsRegexString4 = @"<a\s+href=""(?<ratesheetId>[^""]+)""\s+target=""[^""]+"">SNDR_" + todayDate + @"B.xls";
            string rsRegexString5 = @"<a\s+href=""(?<ratesheetId>[^""]+)""\s+target=""[^""]+"">SNDR_" + todayDate + @"A.xls";
            Regex s_ratesheetRegex = new Regex (rsRegexString);
            Regex s_ratesheetRegex2 = new Regex (rsRegexString2);
            Regex s_ratesheetRegex3 = new Regex (rsRegexString3);
            Regex s_ratesheetRegex4 = new Regex(rsRegexString4);
            Regex s_ratesheetRegex5 = new Regex(rsRegexString5);
            match = s_NoRateSheetRegex.Match(temp);
            if (match.Success)
                return E_BotDownloadStatusT.Error;
            else
            {
                match = s_ratesheetRegex.Match(temp);
                TimeSpan benchmark = new TimeSpan(6, 30, 0);

                if (match.Success)
                {
                    ratesheetURL = match.Groups["ratesheetId"].Value;
                    ratesheetURL = ratesheetURL.Replace("../", "/");
                    ratesheetURL = "https://www.senderra.com" + ratesheetURL;
                }
                else
                {
                    match = s_ratesheetRegex2.Match(temp);
                    if (match.Success)
                    {
                        ratesheetURL = match.Groups["ratesheetId"].Value;
                        ratesheetURL = ratesheetURL.Replace("../", "/");
                        ratesheetURL = "https://www.senderra.com" + ratesheetURL;
                    }
                    else
                    {
                        match = s_ratesheetRegex3.Match(temp);
                        if (match.Success)
                        {
                            ratesheetURL = match.Groups["ratesheetId"].Value;
                            ratesheetURL = ratesheetURL.Replace("../", "/");
                            ratesheetURL = "https://www.senderra.com" + ratesheetURL;
                        }
                        else
                        {
                            match = s_ratesheetRegex4.Match(temp);
                            if (match.Success)
                            {
                                ratesheetURL = match.Groups["ratesheetId"].Value;
                                ratesheetURL = ratesheetURL.Replace("../", "/");
                                ratesheetURL = "https://www.senderra.com" + ratesheetURL;
                            }
                            else
                            {
                                match = s_ratesheetRegex5.Match(temp);
                                if (match.Success)
                                {
                                    ratesheetURL = match.Groups["ratesheetId"].Value;
                                    ratesheetURL = ratesheetURL.Replace("../", "/");
                                    ratesheetURL = "https://www.senderra.com" + ratesheetURL;
                                }
                                else if ((match.Success == false) && (DateTime.Now.TimeOfDay > benchmark))
                                {
                                    LogErrorAndSendEmailToAdmin("Senderra Bot can't find ratesheet URL. Content: " + temp);
                                    return E_BotDownloadStatusT.Error;
                                }
                            }
                        }
                    }
                }

                RatesheetFileName = DownloadFile(ratesheetURL);

                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
		}
	}
}
