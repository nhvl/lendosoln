﻿//Budi Sulayman
namespace LpeUpdateBotLib.WebBots
{
    using LpeUpdateBotLib.Common;
    using System;
    using System.IO;
    using WinSCP;

    public class PennyMacBot : AbstractWebBot
    {
        public PennyMacBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "PENNYMAC"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // if its after 6pm, we're not gonna download the RS. 
            if (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(18)) > 0)
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            // if its Saturday or Sunday, we're not gonna download the RS. 
            if ((DateTime.Now.DayOfWeek == DayOfWeek.Saturday) || (DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            try
            {
                string lastDownloadFolder = Path.Combine(Config.FILES_PATH, "PennyMac_lastDownload");
                string currentDownloadFolder = Path.Combine(Config.FILES_PATH, "PennyMac_currentDownload");

                //7/20/2015 BS - check if folder already exists already or not. If not, create folder then copy all old ratesheets to LastDownload folder
                if (!Directory.Exists(lastDownloadFolder))
                {
                    Directory.CreateDirectory(lastDownloadFolder);
                }

                if (!Directory.Exists(currentDownloadFolder))
                {
                    Directory.CreateDirectory(currentDownloadFolder);
                }

                if (Directory.GetFiles(currentDownloadFolder).Length != 0)
                {
                    foreach (var file in Directory.GetFiles(currentDownloadFolder))
                    {
                        File.Copy(file, Path.Combine(lastDownloadFolder, Path.GetFileName(file)), true);
                    }
                }

                //Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = "66.150.189.57",
                    UserName = "pricemyloan",
                    Password = "",
                    PortNumber = Convert.ToInt32("22000"),
                    SshHostKeyFingerprint = "ssh-rsa 2048 c7:84:60:24:20:2c:e2:1f:15:81:6a:4f:af:cd:40:8e",
                    SshPrivateKeyPath = @"pricemyloan_private_putty_unsecured.ppk"
                };
                
                using (Session session = new Session())
                {
                    //List of files path to be downloaded
                    string fileName = @"BE.XLSX";
                    string parentFolder = @"/pricemyloan_storage";


                    string[,] clientFilesPath = new string[,]
                    {
                    {parentFolder + @"/best capital funding/" + fileName, "PENNYMAC_PML0202.XLS"},
                    {parentFolder + @"/ffc mortgage corp/" + fileName, "PENNYMAC_PML0127.XLS"},
                    {parentFolder + @"/sfmc, lp/" + fileName, "PENNYMAC_PML0233.XLS"},
                    {parentFolder + @"/hancock mortgage partners, llc/" + fileName, "PENNYMAC_PML0252.XLS"},
                    {parentFolder + @"/capstone direct, inc/" + fileName, "PENNYMAC_PML0255.XLS"},
                    {parentFolder + @"/nexera holding llc/" + fileName, "PENNYMAC_PML0262.XLS"},
                    {parentFolder + @"/sunstreet mortgage, llc/" + fileName, "PENNYMAC_PML0253.XLS"},
                    {parentFolder + @"/aurora financial/" + fileName, "PENNYMAC_PML0126.XLS"},
                    {parentFolder + @"/oaktree funding corp/" + fileName, "PENNYMAC_PML0034.XLS"},
                    {parentFolder + @"/newcastle home loans, llc/" + fileName, "PENNYMAC_PML0265.XLS"},
                    {parentFolder + @"/iapprove lending/" + fileName, "PENNYMAC_PML0197.XLS"},
                    {parentFolder + @"/clm mortgage/" + fileName, "PENNYMAC_PML0218.XLS"},
                    {parentFolder + @"/national bank of commerce/" + fileName, "PENNYMAC_PML0248.XLS"},
                    {parentFolder + @"/keypoint credit union/" + fileName, "PENNYMAC_PML0241.XLS"},
                    {parentFolder + @"/inlanta mortgage inc/" + fileName, "PENNYMAC_PML0277.XLS"},
                    {parentFolder + @"/michigan mutual, inc/" + fileName, "PENNYMAC_PML0270.XLS"},
                    {parentFolder + @"/aspire financial inc/" + fileName, "PENNYMAC_PML0249.XLS"},
                    {parentFolder + @"/home mortgage alliance corporation/" + fileName, "PENNYMAC_PML0260.XLS"},
                    {parentFolder + @"/global bancorp/" + fileName, "PENNYMAC_PML0273.XLS"},
                    {parentFolder + @"/primus lending corp/" + fileName, "PENNYMAC_PML0151.XLS"},
                    {parentFolder + @"/mortgage management consultants, inc/" + fileName, "PENNYMAC_PML0114.XLS"},
                    {parentFolder + @"/open mortgage, llc/" + fileName, "PENNYMAC_PML0272.XLS"},
                    {parentFolder + @"/bridgelock capital/" + fileName, "PENNYMAC_PML0236.XLS"},
                    //{parentFolder + @"/trust mortgage lending corporation/" + fileName, "PENNYMAC_PML0210.XLS"},
                    {parentFolder + @"/wall financial, inc/" + fileName, "PENNYMAC_PML0224.XLS"},
                    //{parentFolder + @"/resolute bank/" + fileName, "PENNYMAC_PML0302.XLS"},
                    {parentFolder + @"/banc one mortgage corporation/" + fileName, "PENNYMAC_PML0305.XLS"},
                    {parentFolder + @"/jersey mortgage company of new jersey, inc/" + fileName, "PENNYMAC_PML0303.XLS"},
                    {parentFolder + @"/alterra group, llc/" + fileName, "PENNYMAC_PML0159.XLS"},
                    {parentFolder + @"/iserve residential lending, llc/" + fileName, "PENNYMAC_PML0170.XLS"},
                    {parentFolder + @"/mortgage financial services, llc/" + fileName, "PENNYMAC_PML0307.XLS"},
                    {parentFolder + @"/city lights financial/" + fileName, "PENNYMAC_PML0310.XLS"},
                    {parentFolder + @"/mountain west financial inc/" + fileName, "PML0263_CUSTOM_PENNYMAC.XLS"},
                    {parentFolder + @"/campos financial corp/" + fileName, "PENNYMAC_PML0314.XLS"},
                    {parentFolder + @"/first state bank - ok/" + fileName, "PENNYMAC_PML0296.XLS"},
                    {parentFolder + @"/resource lenders, inc/" + fileName, "PENNYMAC_PML0322.XLS"},
                    {parentFolder + @"/american mortgage service company/" + fileName, "PENNYMAC_PML0324.XLS"},
                    {parentFolder + @"/nexera holding llc/" + fileName, "PML0262_CUSTOM_PENNYMAC.XLS"},
                    {parentFolder + @"/home approvals direct, inc/" + fileName, "PENNYMAC_PML0274.XLS"},
                    {parentFolder + @"/siwell, inc/" + fileName, "PENNYMAC_PML0185.XLS"}};

                    //Connect
                    session.Open(sessionOptions);

                    //Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Automatic;


                    for (int i = 0; i < clientFilesPath.GetLength(0); i++)
                    {
                        try
                        {
                            TransferOperationResult transferResult;
                            string localFilePath = Path.Combine(Path.GetFullPath(currentDownloadFolder), clientFilesPath[i, 1]);
                            transferResult = session.GetFiles(clientFilesPath[i, 0], localFilePath, false, transferOptions);

                            // Throw on any error
                            transferResult.Check();
                        }
                        catch (Exception e)
                        {
                            LogErrorAndSendEmailToAdmin("PennyMac bot failed to download ratesheet for: " + clientFilesPath[i, 1] + "Exception: " + e);
                        }
                    }
                }

                // 7/21/2015 BS
                if (Directory.GetFiles(lastDownloadFolder).Length == 0)
                {
                    foreach (var file in Directory.GetFiles(currentDownloadFolder))
                    {
                        File.Copy(file, PathCfg.BOT_DOWNLOAD_PATH + Path.GetFileName(file), true);
                    }
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }

                bool isNewRatesheet = IsNewRatesheet(lastDownloadFolder, currentDownloadFolder);
                if (isNewRatesheet)
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                else
                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }
            catch (Exception e)
            {
                LogErrorAndSendEmailToAdmin("PennyMac bot failed to download ratesheet for: " + dlinfo.Login + ". Content: " + e);
                return E_BotDownloadStatusT.Error;
            }
        }

        /// <summary>
        /// 7/21/2015 BS - Do a binary comparison between 2 files
        /// </summary>
        /// <returns>True if the files are identical</returns>
        protected static bool IsFileIdentical(string sFile1, string sFile2)
        {
            if (!File.Exists(sFile1) || !File.Exists(sFile2)) return false;

            const int BUFSIZE = 1024;
            byte[] buf1 = new byte[BUFSIZE];
            byte[] buf2 = new byte[BUFSIZE];

            using (FileStream fs1 = new FileStream(sFile1, FileMode.Open))
            {
                using (FileStream fs2 = new FileStream(sFile2, FileMode.Open))
                {
                    while (true)
                    {
                        int n1 = fs1.Read(buf1, 0, BUFSIZE);
                        int n2 = fs2.Read(buf2, 0, BUFSIZE);

                        if (n1 != n2) return false;
                        if (n1 == 0) return true;

                        for (int i = 0; i < n1; i++)
                            if (buf1[i] != buf2[i])
                                return false;
                    }
                }
            }
        }

        /// <summary>
        /// 7/21/2015 BS - Compare each ratesheet between lastDownload and currentDownload
        /// </summary>
        /// <returns>If it's new, then it will copy new ratesheet to DownloadPath</returns>
        protected static bool IsNewRatesheet(string PennyMac_lastDownload, string PennyMac_currentDownload)
        {
            //Compare new and old ratesheet to see if current ratesheet is new
            var lastDownloadFiles = Directory.GetFiles(PennyMac_lastDownload);
            var currentDownloadFiles = Directory.GetFiles(PennyMac_currentDownload);
            bool newRatesheet = false;

            foreach (string lastDownloadFile in lastDownloadFiles)
            {
                string fileName = lastDownloadFile.Substring(PennyMac_lastDownload.Length + 1);
                if (File.Exists(Path.Combine(PennyMac_currentDownload, fileName)))
                {
                    bool isIdenticalRatesheet = IsFileIdentical(lastDownloadFile, Path.Combine(PennyMac_currentDownload, fileName));
                    if (!isIdenticalRatesheet)
                    {
                        File.Copy(Path.Combine(PennyMac_currentDownload, fileName), PathCfg.BOT_DOWNLOAD_PATH + "/" + fileName, true);
                        newRatesheet = true;
                    }
                }
            }

            //if the ratesheet is new and without any previous history about it, then we copy it directly to DownloadPath
            foreach (string currentDownloadFile in currentDownloadFiles)
            {
                string tempName = currentDownloadFile.Substring(PennyMac_currentDownload.Length + 1);
                if (!File.Exists(Path.Combine(PennyMac_lastDownload, tempName)))
                {
                    File.Copy(tempName, PathCfg.BOT_DOWNLOAD_PATH + @"\" + tempName, true);
                    newRatesheet = true;
                }
            }

            return newRatesheet;
        }
    }
}