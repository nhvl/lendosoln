﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Threading;

namespace LpeUpdateBotLib.WebBots
{
    class LenderLiveBot : AbstractWebBot
    {
        private static string hostUrl = @"https://www.lenderlive.com";
        private Regex ratesheet_agency_regex = new Regex(@"<div class='RateSheetDate'>LenderLive STD Agency<br />[^<]+</div><div class='RateSheetPDF'><a href='[^']+' target='_blank'><strong>PDF</strong></a></div><div class='RateSheetExcel'><a href='(?<ratesheetUrl>[^']+)", RegexOptions.IgnoreCase);
        private Regex ratesheet_nonagency_regex = new Regex(@"<div class='RateSheetDate'>LenderLive STD Non-Agency<br />[^<]+</div><div class='RateSheetPDF'><a href='[^']+' target='_blank'><strong>PDF</strong></a></div><div class='RateSheetExcel'><a href='(?<ratesheetUrl>[^']+)", RegexOptions.IgnoreCase);

        public LenderLiveBot(IDownloadInfo info) : base(info)
		{
		}
        public override string BotName
        {
            get 
            { 
                return "LENDERLIVE"; 
            }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string temp = DownloadData(dlinfo.Url);
            string fileName = null;
            if (dlinfo.Description.Contains("_AGENCY"))
            {
                Match match = ratesheet_agency_regex.Match(temp);
                if (match.Success)
                {
                    fileName = DownloadFile(hostUrl + match.Groups["ratesheetUrl"].Value);
                    LogInfo(match.Groups["ratesheetUrl"].Value);
                }
                else
                    LogErrorAndSendEmailToAdmin("LenderLiveBot failed to find today ratesheet. Content: " + temp);

                if (fileName != null)
                {
                    RatesheetFileName = fileName;
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                    return E_BotDownloadStatusT.Error;
            }
            else if (dlinfo.Description.Contains("_NONAGENCY"))
            {
                Match match = ratesheet_nonagency_regex.Match(temp);
                if (match.Success)
                {
                    fileName = DownloadFile(hostUrl + match.Groups["ratesheetUrl"].Value);
                    LogInfo(match.Groups["ratesheetUrl"].Value);
                }
                else
                    LogErrorAndSendEmailToAdmin("LenderLiveBot failed to find today ratesheet. Content: " + temp);

                if (fileName != null)
                {
                    RatesheetFileName = fileName;
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                    return E_BotDownloadStatusT.Error;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }

            
        }
          
    }
}