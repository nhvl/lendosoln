﻿//Kelvin Young & Yin Seo
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class FreedomCorrBot : AbstractWebBot
    {



        public FreedomCorrBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "FREEDOMCORR"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //corr
            string loginUrl_corr = @"https://www.freedomconnect.com/wps/portal/correspondent/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3jPsJBQJydDRwP3UCM3A09Dd0PvkEA_YwN3E_1wkA6zeA8DZ183fxMzM3MTA8tQ55BAdwMogMgb4ACOBvp-Hvm5qfoF2dlpjo6KigDlGqk_/dl3/d3/L0lDU0lKSkpDZ3BSQ1FvS1VRa2chL29Gb2dBRUlRaGpGRUlBQWpPTTRSa0JTVkpRcEdnZ0d0QUEhIS80QzFiOVdfTnIwUkprQWxJTVJKa3ZGRUlpUi1BLzdfSVZUVUJCMUEwR1UyRjBJMUcxS1RRTjMwRzcvaWJtLmludi8xODE1NzYwMTMyODAvd3BzLnBvcnRsZXRzLmxvZ2lu";
            string rsUrl_corr = @"https://www.freedomconnect.com/wps/PA_C_AccessDailyRates/DocumentDownloadRequestor?sessionKey=PC_5_GQ8NPI420GEJE0IUAUNJKS3000020851_documentsList&channel=correspondent&";
            
            //renovation
            string loginUrl_ren = @"https://www.freedomrenovates.com/wps/portal/correspondent/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3jPsJBQJydDRwP3UCM3A09Dd0PvkEA_YwN3E_1wkA6zeA8DZ183fxMzM3MTA8tQ55BAdwMogMgb4ACOBvp-Hvm5qfoF2dlpjo6KigDlGqk_/dl3/d3/L0lDU0lKSkpDZ3BSQ1FvS1VRa2chL29Gb2dBRUlRaGpGRUlBQWpPTTRSa0JTVkpRcEdnZ0d0QUEhIS80QzFiOVdfTnIwUkprQWxJTVJKa3ZGRUlpUi1BLzdfSVZUVUJCMUEwR1UyRjBJMUcxS1RRTjMwRzcvaWJtLmludi8yMTUxODg1NDAyODQvd3BzLnBvcnRsZXRzLmxvZ2lu";
            string renRsURL = @"<td align=""center"" valign=""middle"" width=""40"">\s+<a href=(?<rsLink>[^']+) target=""_blank"">\s+<img alt=""Excel Document";
            Regex renRs_regex = new Regex(renRsURL);

            string fileName = null; //the RS that will be DLed
            string loginResponse = null;

            //Login
            NameValueCollection loginInfo = new NameValueCollection();
            loginInfo.Add("wps.portlets.userid", dlinfo.Login);
            loginInfo.Add("password", dlinfo.Password);
            loginInfo.Add("wps.portlets.resumeSession", "true");

            switch (dlinfo.Description.ToUpper())
            {
                //regular correspondent
                case "FREEDOM_CORRESPONDENT":
                    loginResponse = UploadDataReturnResponse(loginUrl_corr, loginInfo);
                    String mytime = DateTime.Now.Date.ToString(@"MM\\.dd\\.yy");
                    Regex index_Reg = new Regex(mytime + @"(?<let>[a-z]?)[\S\s]*?index=(?<num>\d)");
                    Match m = index_Reg.Match(loginResponse);
                    String s = "";
                    String index = "0";
                    while (m.Success)
                    {
                        if (string.Compare(s,m.Groups["let"].Value) == -1 )
                        {
                            s = m.Groups["let"].Value;
                            index = m.Groups["num"].Value;
                        }
                        LogInfo("digit: " + m.Groups["num"].Value);
                        m = m.NextMatch();
                    }
                    fileName = DownloadFile(rsUrl_corr + "index=" + index);
                    LogInfo("downloaded " + index);
                    /*-------------------------------------------------------
                    Regex indexReg1 = new Regex(@"index=1");
                    Regex indexReg2 = new Regex(@"index=2");
                    Regex indexReg3 = new Regex(@"index=3");
                    Regex indexReg4 = new Regex(@"index=4");

                    Match indexMatch1 = indexReg1.Match(loginResponse);
                    Match indexMatch2 = indexReg2.Match(loginResponse);
                    Match indexMatch3 = indexReg3.Match(loginResponse);
                    Match indexMatch4 = indexReg4.Match(loginResponse);
                    
                    //for when new RS is posted on the bottom of the list
                    //catches new reprices and download the most recent ones
                    if (indexMatch4.Success)
                        fileName = DownloadFile(rsUrl_corr + "index=4");
                    else if (indexMatch3.Success)
                        fileName = DownloadFile(rsUrl_corr + "index=3");
                    else if (indexMatch2.Success)
                        fileName = DownloadFile(rsUrl_corr + "index=2");
                    else if (indexMatch1.Success)
                        fileName = DownloadFile(rsUrl_corr + "index=1");
                    else
                        fileName = DownloadFile(rsUrl_corr + "index=0");
                   -----------------------------------------------------------*/ 

                    //for when the new RS is posted on top of hte list
                    //fileName = DownloadFile(rsUrl_corr + "index=0");

                    //Logout after
                    GoToUrl(loginUrl_corr);
                    break;
                //renonvation correspondent
                default:
                    loginResponse = UploadDataReturnResponse(loginUrl_ren, loginInfo);
                    
                    Match match = renRs_regex.Match(loginResponse);
                    if (match.Success)
                    {
                        string rsLink = "https://www.freedomrenovates.com" + match.Groups["rsLink"].Value;
                        fileName = DownloadFile(rsLink);
                    }
                    //Logout after
                    GoToUrl(loginUrl_ren);

                    break;
            }
            //Check for error
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}