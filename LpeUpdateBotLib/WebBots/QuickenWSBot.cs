// Author: Ian Merna

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Threading;

namespace LpeUpdateBotLib.WebBots
{
    public class QuickenWSBot : AbstractWebBot
    {
        private static string baseUrl = "https://portal.qlmortgageservices.com";
        private static string loginUrl = baseUrl + "/sign-in";
        private static string baseRsUrl = baseUrl + "/pricing/rate-sheet?channel=correspondent";
        private static Regex failedLogin_regex = new Regex(@"You have been signed out", RegexOptions.IgnoreCase);
        //private static Regex WS_rs_regex = new Regex(@"href=""(?<rsUrl>[^""]+)"">[^=]+=""td_three"">" + DateTime.Today.Date.ToString("MMM dd, yyyy"), RegexOptions.IgnoreCase);
        private static Regex Corr_rs_regex = new Regex(@"href=""(?<rsUrl>[^""]+)"">[^=]+=""td_two"">XLS<", RegexOptions.IgnoreCase);


        public QuickenWSBot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "QUICKENBOT"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
                
                NameValueCollection postData = new NameValueCollection();
                postData.Add("signinname", dlinfo.Login);
                postData.Add("password", dlinfo.Password);
                postData.Add("submit", @"Sign In");

                UploadData(loginUrl, postData);

                string temp = DownloadData(baseUrl + "/v2#/v2/p");
                Match failedLogin_match = failedLogin_regex.Match(temp);
                if (failedLogin_match.Success)
                {
                    dlinfo.Deactivate();
                    ReportDeactivation("Quicken Bot failed to login. Please check login/password for user name: " + dlinfo.Login + " and re-activate bot. Content: " + temp);
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    temp = DownloadData(baseRsUrl);
                    Match rs_match = Corr_rs_regex.Match(temp);

                    if (rs_match.Success)
                    {
                        RatesheetFileName = DownloadFile(baseUrl + rs_match.Groups[1].Value);
                        GoToUrl(baseUrl + "/sign-out");
                        return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                    }
                    else
                    {
                        LogErrorAndSendEmailToAdmin("Quicken WS Bot did not find ratesheet. Content; " + temp);
                        return E_BotDownloadStatusT.Error;
                    }

                }

        }
    }
}