﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots
{
    public class SovereignBot : AbstractWebBot 
    {
        private static string base_url = "https://www.santanderwebmortgage.com";
        private static string main_url = @"/defaultframe.ashx";
        private static string login_url = @"/login.ashx";
        private static string rs_url = @"/linkout/responseDocuments.asp?DocType=PDF&PDF=True&DOC_ID=335";

        public SovereignBot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "SOVEREIGN"; }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }

        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }

        protected override string DefaultAcceptLanguage
        {
            get
            {
                return @"en-US";
            }
        }

        protected override int MaxAutoRedirects
        {
            get
            {
                return 2;
            }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string login_result;

            // navigate to the main login page
            GoToUrl(base_url + main_url);

            // fill out the usernmae, password, and click login
            NameValueCollection postData = new NameValueCollection();
            postData.Add("sUser", dlinfo.Login);
            postData.Add("sPassword", dlinfo.Password);
            postData.Add("submit", "Login");

            // store the html of the page we get redirected to, so we can find out if we successfully logged in or not
            login_result = UploadDataReturnResponse(base_url + login_url, postData);

            // if we couldn't log in, we have the wrong credentials
            if (login_result.Contains("Invalid User Name or Password"))
            {
                ReportDeactivation("Santander password incorrect for login: " + dlinfo.Login + ". Please update it and re-enable any bot entry using this login.");
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            // if we get to here, we know we successfully logged in, so we can download the RS
            RatesheetFileName = DownloadFile(base_url + rs_url);

            if (RatesheetFileName != null)
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            else
            {
                LogErrorAndSendEmailToAdmin("Santander Bot failed to download rate sheet. URL: " + base_url + rs_url);
                return E_BotDownloadStatusT.Error;
            }

        }
    }
}