﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots
{
    public class AssociatedBot : AbstractWebBot
    {
        private static string baseUrl = @"https://www.associatedtpo.com";
        private static string loginUrl = @"/Home/LoginCheck";
        private static Regex rsUrl_regex = new Regex(@"Correspondent-Rates.xls", RegexOptions.IgnoreCase);

        public AssociatedBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "ASSOCIATED"; }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            GoToUrl(baseUrl);

            NameValueCollection postData = new NameValueCollection();
            postData.Add("userid", dlinfo.Login);
            postData.Add("password", dlinfo.Password);

            UploadData(baseUrl+loginUrl, postData, baseUrl);

            string temp = DownloadData(baseUrl);

            Match match = rsUrl_regex.Match(temp);
            string fileName = null;

            if (match.Success)
                fileName = DownloadFile(baseUrl + @"/pdf/rates/Correspondent-Rates.xls");

            GoToUrl(baseUrl + @"/Home/SignOut");

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}
