﻿//Budi Sulayman
namespace LpeUpdateBotLib.WebBots
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    //using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;
    using WinSCP;

    public class AmeriHomeBot : AbstractWebBot
    {
        public AmeriHomeBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "AMERIHOME"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            try
            {
                string lastDownloadFolder = Path.Combine(Config.FILES_PATH, "AmeriHome_lastDownload");
                string currentDownloadFolder = Path.Combine(Config.FILES_PATH, "AmeriHome_currentDownload");

                //9/14/2015 BS - check if folder already exists already or not. If not, create folder then copy all old ratesheets to LastDownload folder
                if (!Directory.Exists(lastDownloadFolder))
                    Directory.CreateDirectory(lastDownloadFolder);

                if (!Directory.Exists(currentDownloadFolder))
                    Directory.CreateDirectory(currentDownloadFolder);

                if (Directory.GetFiles(currentDownloadFolder).Length != 0)
                {
                    foreach (var file in Directory.GetFiles(currentDownloadFolder))
                        File.Copy(file, Path.Combine(lastDownloadFolder, Path.GetFileName(file)), true);
                }

                //Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = "sftp.amerihome.com",
                    UserName = dlinfo.Login,
                    Password = dlinfo.Password,
                    PortNumber = Convert.ToInt32("2222"),
                    SshHostKeyFingerprint = "ssh-rsa 2048 bf:e0:66:3c:59:39:ba:ab:65:88:12:e0:fc:a1:4e:97"
                };

                using (Session session = new Session())
                {
                    //List of files path to be downloaded
                    string fileName = @"RateSheet.xlsx";
                    string parentFolder = @"/PriceMyLoan/Exports/RateSheets";
                    string[,] clientFilesPath = new string[,]
                    {{parentFolder + @"/AspireFinancialInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0249.XLS"},
                    {parentFolder + @"/AspireFinancialInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_CONF.XLS"},
                    {parentFolder + @"/AspireFinancialInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_GOV.XLS"},
                    //{parentFolder + @"/AspireFinancialInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_EXPANDED.XLS"},
                    {parentFolder + @"/AspireFinancialInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_JUMBO.XLS"},
                    //{parentFolder + @"/AtlanticPacificMortgageCorporation/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0168.XLS"},
                    {parentFolder + @"/GreenboxLoansInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0220.XLS"},
                    {parentFolder + @"/IapproveLending/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0197.XLS"},
                    {parentFolder + @"/IserveResidentialLendingLlc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0170.XLS"},
                    //{parentFolder + @"/MegaCapitalFundingInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0245.XLS"},
                    {parentFolder + @"/NexeraHoldingLlc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0262.XLS"},
                    //{parentFolder + @"/ExpediteFinancial/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0266.XLS"},
                    {parentFolder + @"/TheLendingPartnersLlc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0243.XLS"},
                    {parentFolder + @"/FfcMortgage/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0127.XLS"},
                    //{parentFolder + @"/PrivatePlusMortgage/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0248.XLS"},
                    {parentFolder + @"/OpenMortgage/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0272.XLS"},
                    //{parentFolder + @"/NewCastleHomeLoansLlc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0265.XLS"},
                    {parentFolder + @"/VentaFinancialGroupInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0159.XLS"},
                    {parentFolder + @"/ClmMortgage/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0218.XLS"},
                    {parentFolder + @"/HomeMortgageAllianceCorporation/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0260.XLS"},
                    {parentFolder + @"/HancockMortgagePartnersLlc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0252.XLS"},
                    {parentFolder + @"/MichiganMutualInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0270.XLS"},
                    {parentFolder + @"/KeypointCreditUnion/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0241.XLS"},
                    {parentFolder + @"/EustisMortgageCorporation/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0295.XLS"},
                    {parentFolder + @"/BestCapitalFunding/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0202.XLS"},
                    //{parentFolder + @"/AmericanLendingSolutions/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0238.XLS"},
                    {parentFolder + @"/JerseyMortgageCompanyOfNewJerseyInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0303.XLS"},
                    {parentFolder + @"/BancOneMortgage/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0305.XLS"},
                    {parentFolder + @"/SfmcLp/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0233.XLS"},
                    {parentFolder + @"/MortgageFinancial/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0307.XLS"},
                    {parentFolder + @"/InlantaMortgage/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0277.XLS"},
                    {parentFolder + @"/MountainWestFinancial/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0263.XLS"},
                    {parentFolder + @"/SiMortgageCompany/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0165.XLS"},
                    {parentFolder + @"/CreditUnionMortgageAssociation/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0321.XLS"},
                    {parentFolder + @"/ResourceLendersInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0322.XLS"},
                    {parentFolder + @"/CapitalMortgageServices/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0185.XLS"},
                    {parentFolder + @"/AmericanMortgageServiceCompany/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0324.XLS"},
                    {parentFolder + @"/LiveWellFinancialInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0328.XLS"},
                    {parentFolder + @"/ResmacInc/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0310.XLS"},
                    {parentFolder + @"/MlMortgageCorp/" + fileName, Path.GetFullPath(currentDownloadFolder) + @"\AMERIHOME_PML0297.XLS"}};

                    //Connect
                    session.Open(sessionOptions);

                    //Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Automatic;


                    for (int i = 0; i < clientFilesPath.GetLength(0); i++)
                    {
                        int j = 0;
                        try
                        {
                            TransferOperationResult transferResult;
                            transferResult = session.GetFiles(clientFilesPath[i, j], clientFilesPath[i, j + 1], false, transferOptions);

                            // Throw on any error
                            transferResult.Check();
                        }
                        catch (Exception e)
                        {
                            LogErrorAndSendEmailToAdmin("AmeriHome bot failed to download ratesheet for: " + clientFilesPath[i, j + 1] + "Exception: " + e);
                        }
                    }
                }

                // 9/14/2015 BS
                if (Directory.GetFiles(lastDownloadFolder).Length == 0)
                {
                    foreach (var file in Directory.GetFiles(currentDownloadFolder))
                        File.Copy(file, PathCfg.BOT_DOWNLOAD_PATH + Path.GetFileName(file), true);

                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }

                bool isNewRatesheet = IsNewRatesheet(lastDownloadFolder, currentDownloadFolder);
                if (isNewRatesheet)
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                else
                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }
            catch (Exception e)
            {
                LogErrorAndSendEmailToAdmin("AmeriHome bot failed to download ratesheet for: " + dlinfo.Login + ". Content: " + e);
                return E_BotDownloadStatusT.Error;
            }
        }

        /// <summary>
        /// 9/14/2015 BS - Do a binary comparison between 2 files
        /// </summary>
        /// <returns>True if the files are identical</returns>
        protected static bool IsFileIdentical(string sFile1, string sFile2)
        {
            if (!File.Exists(sFile1) || !File.Exists(sFile2)) return false;
/*
            if (!FileOperationHelper.Exists(sFile1) || !FileOperationHelper.Exists(sFile2)) return false;
*/

            const int BUFSIZE = 1024;
            byte[] buf1 = new byte[BUFSIZE];
            byte[] buf2 = new byte[BUFSIZE];

            using (FileStream fs1 = new FileStream(sFile1, FileMode.Open))
            {
                using (FileStream fs2 = new FileStream(sFile2, FileMode.Open))
                {
                    while (true)
                    {
                        int n1 = fs1.Read(buf1, 0, BUFSIZE);
                        int n2 = fs2.Read(buf2, 0, BUFSIZE);

                        if (n1 != n2) return false;
                        if (n1 == 0) return true;

                        for (int i = 0; i < n1; i++)
                            if (buf1[i] != buf2[i])
                                return false;
                    }
                }
            }
        }

        /// <summary>
        /// 9/14/2015 BS - Compare each ratesheet between lastDownload and currentDownload
        /// </summary>
        /// <returns>If it's new, then it will copy new ratesheet to DownloadPath</returns>
        protected static bool IsNewRatesheet(string lastDownload, string currentDownload)
        {
            //Compare new and old ratesheet to see if current ratesheet is new
            var lastDownloadFiles = Directory.GetFiles(lastDownload);
            var currentDownloadFiles = Directory.GetFiles(currentDownload);
            bool newRatesheet = false;

            foreach (string lastDownloadFile in lastDownloadFiles)
            {
                string fileName = lastDownloadFile.Substring(lastDownload.Length + 1);
                if (File.Exists(Path.Combine(currentDownload, fileName)))
/*
                if (FileOperationHelper.Exists(Path.Combine(currentDownload, fileName)))
*/
                 {
                    bool isIdenticalRatesheet = IsFileIdentical(lastDownloadFile, Path.Combine(currentDownload, fileName));
                    if (!isIdenticalRatesheet)
                    {
                        File.Copy(Path.Combine(currentDownload, fileName), PathCfg.BOT_DOWNLOAD_PATH + "/" + fileName, true);
/*
                        FileOperationHelper.Copy(Path.Combine(currentDownload, fileName), PathCfg.BOT_DOWNLOAD_PATH + "/" + fileName, true);
*/
                        newRatesheet = true;
                    }
                }
            }


            //if the ratesheet is new and without any previous history about it, then we copy it directly to DownloadPath
            foreach (string currentDownloadFile in currentDownloadFiles)
            {
                string tempName = currentDownloadFile.Substring(currentDownload.Length + 1);
                if (!File.Exists(Path.Combine(lastDownload, tempName)))
                {
                    File.Copy(tempName, PathCfg.BOT_DOWNLOAD_PATH + @"\" + tempName, true);
                    newRatesheet = true;
                }
/*
                if (!FileOperationHelper.Exists(Path.Combine(lastDownload, tempName)))
                {
                    FileOperationHelper.Copy(tempName, PathCfg.BOT_DOWNLOAD_PATH + @"\" + tempName, true);
                    newRatesheet = true;
                }
*/
            }

            return newRatesheet;
        }
    }
}