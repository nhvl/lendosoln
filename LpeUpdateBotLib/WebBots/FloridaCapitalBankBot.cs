﻿//Author: Mark Hsu
//This bot is hardcoded with secret questions and answers.

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class FloridaCapitalBankBot : AbstractWebBot
    {

        public FloridaCapitalBankBot(IDownloadInfo info): base(info)
        {
        }
        public override string BotName
        {
            get { return "FLORIDACAPITAL"; }
        }
        
        //SETTING HEADERS
        protected override string BotUserAgentString
        {
            get
            {
                return "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
            }
        }
        protected override bool WebRequestKeepAlive
        {

            get { return true; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }
        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }
        protected override string DefaultAcceptLanguage
        {
            get
            {
                return @"en-US";
            }
        }
        //END SETTING HEADERS
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string baseUrl = dlinfo.Url;
            string loginUrl = baseUrl + @"Login?ReturnUrl=%2f";
            Regex invalidLogin_regex = new Regex(@"<a href=""/Login/Invalid"">", RegexOptions.IgnoreCase);
            Regex question_regex = new Regex(@"name=""SecurityAnswer""", RegexOptions.IgnoreCase);
            Regex verToken_regex = new Regex(@"name=""__RequestVerificationToken"" type=""hidden"" value=""(?<verToken>[^""]+)""", RegexOptions.IgnoreCase);
            string fileName = null;
            Regex phrase_regex = new Regex(@"default", RegexOptions.IgnoreCase);
            Regex[] secretquestion_regex = new Regex[3];
            String[] secretanswers = new String[3];

            // if its before 7am or after 7pm, we're not gonna download the RS. 
            if ((DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(7)) < 0) || (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(19)) > 0))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            // if its Saturday or Sunday, we're not gonna download the RS. 
            if ((DateTime.Now.DayOfWeek == DayOfWeek.Saturday) || (DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            if (dlinfo.Description.ToUpper() == "FC_PML0201")
            {
                secretquestion_regex[0] = new Regex(@"maiden name", RegexOptions.IgnoreCase);
                secretquestion_regex[1] = new Regex(@"What is the name of your favorite pet", RegexOptions.IgnoreCase);
                secretquestion_regex[2] = new Regex(@"What year was your mother born", RegexOptions.IgnoreCase);
                secretanswers[0] = "Hennessey";
                secretanswers[1] = "Chloe";
                secretanswers[2] = "1942";
                phrase_regex = new Regex(@"Success!", RegexOptions.IgnoreCase);
            }
            else if (dlinfo.Description.ToUpper() == "FC_PML0244")
            {
                secretquestion_regex[0] = new Regex(@"What was the name of your childhood hometown newspaper", RegexOptions.IgnoreCase);
                secretquestion_regex[1] = new Regex(@"What is your oldest cousin's first name", RegexOptions.IgnoreCase);
                secretquestion_regex[2] = new Regex(@"In what town was your first job", RegexOptions.IgnoreCase);
                secretanswers[0] = "pml1";
                secretanswers[1] = "pml2";
                secretanswers[2] = "pml3";
                phrase_regex = new Regex(@"how wolves change rivers", RegexOptions.IgnoreCase);
            }
            else if (dlinfo.Description.ToUpper() == "FC_PML0255")
            {
                secretquestion_regex[0] = new Regex(@"What year was your mother born", RegexOptions.IgnoreCase);
                secretquestion_regex[1] = new Regex(@"What is the name of your favorite pet", RegexOptions.IgnoreCase);
                secretquestion_regex[2] = new Regex(@"What is your oldest cousin's first name", RegexOptions.IgnoreCase);
                secretanswers[0] = "1936";
                secretanswers[1] = "Cooper";
                secretanswers[2] = "Irit";
                phrase_regex = new Regex(@"Open Road", RegexOptions.IgnoreCase);
            }
            else if (dlinfo.Description.ToUpper() == "FC_PML0262")
            {
                secretquestion_regex[0] = new Regex(@"What is the name of your favorite pet", RegexOptions.IgnoreCase);
                secretquestion_regex[1] = new Regex(@"What school did you attend for sixth grade", RegexOptions.IgnoreCase);
                secretquestion_regex[2] = new Regex(@"What was your high school mascot", RegexOptions.IgnoreCase);
                secretanswers[0] = "keno";
                secretanswers[1] = "williams parkway";
                secretanswers[2] = "trojan";
                phrase_regex = new Regex(@"Bridge", RegexOptions.IgnoreCase);
            }
            else if (dlinfo.Description.ToUpper() == "FC_PML0298")
            {
                secretquestion_regex[0] = new Regex(@"What is the make and model of your first car", RegexOptions.IgnoreCase);
                secretquestion_regex[1] = new Regex(@"maiden name", RegexOptions.IgnoreCase);
                secretquestion_regex[2] = new Regex(@"In what city was your high school", RegexOptions.IgnoreCase);
                secretanswers[0] = "Dodge Dart";
                secretanswers[1] = "Krupnik";
                secretanswers[2] = "New Berry Park";
                phrase_regex = new Regex(@"CLF", RegexOptions.IgnoreCase);
            }
            else if (dlinfo.Description.ToUpper() == "FC_PML0309")
            {
                secretquestion_regex[0] = new Regex(@"What was the name of your childhood hometown newspaper", RegexOptions.IgnoreCase);
                secretquestion_regex[1] = new Regex(@"What is your oldest cousin's first name", RegexOptions.IgnoreCase);
                secretquestion_regex[2] = new Regex(@"In what town was your first job", RegexOptions.IgnoreCase);
                secretanswers[0] = "pml";
                secretanswers[1] = "pml";
                secretanswers[2] = "pml";
                phrase_regex = new Regex(@"Topgear", RegexOptions.IgnoreCase);
            }
            else if (dlinfo.Description.ToUpper() == "FC_PML0311")
            {
                secretquestion_regex[0] = new Regex(@"What school did you attend for sixth grade", RegexOptions.IgnoreCase);
                secretquestion_regex[1] = new Regex(@"What is the name of your favorite pet", RegexOptions.IgnoreCase);
                secretquestion_regex[2] = new Regex(@"What was your high school mascot", RegexOptions.IgnoreCase);
                secretanswers[0] = "LINK";
                secretanswers[1] = "BAILEY";
                secretanswers[2] = "COUGAR";
                phrase_regex = new Regex(@"elephante", RegexOptions.IgnoreCase);
            }
            else
            {
                ReportDeactivation("Error, bot description is wrong. Please update and try again.");
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            // Primary Login Info
            GoToUrl(loginUrl);
            string loginPage = DownloadData(loginUrl);
            Match vT_match1 = verToken_regex.Match(loginPage);



            NameValueCollection postData = new NameValueCollection();
            postData.Add("__RequestVerificationToken", vT_match1.Groups["verToken"].Value);
            postData.Add("UserName", dlinfo.Login);
            loginPage = UploadDataReturnResponse(loginUrl, postData, loginUrl);
            
            //Secret Questions
            Match question_match = question_regex.Match(loginPage);
            
            if (question_match.Success)
            {
                postData = new NameValueCollection();
                Match vT_match2 = verToken_regex.Match(loginPage);
                postData.Add("__RequestVerificationToken", vT_match2.Groups["verToken"].Value);

                for (int i = 0; i < 3; i++)
                {
                    Match secretQuestion_match = secretquestion_regex[i].Match(loginPage);
                    if (secretQuestion_match.Success)
                    {
                        postData.Add("SecurityAnswer", secretanswers[i]);
                        break;
                    }
                    else if (i == 2)
                    {
                        ReportDeactivation("Florida Capital Bot can't find matched secret questions for description "+dlinfo.Description+". Please update the bot and re-enable login: " + dlinfo.Login+".\n"+loginPage);
                        dlinfo.Deactivate();
                        return E_BotDownloadStatusT.Error;
                    }
                }
                postData.Add("RememberMe", "false");

                loginPage = UploadDataReturnResponse(loginUrl, postData, loginUrl);
            }
            //Password
            Match phrase_match = phrase_regex.Match(loginPage);
            if(!phrase_match.Success)
            {
                ReportDeactivation("Florida Capital Bot can't find secret phrase: " + phrase_regex.ToString() +", "+dlinfo.Description+". Please update the bot and re-enable login: " + dlinfo.Login + ".\n" + loginPage);
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            postData = new NameValueCollection();
            Match vT_match3 = verToken_regex.Match(loginPage);
            postData.Add("__RequestVerificationToken", vT_match3.Groups["verToken"].Value);
            postData.Add("Password", dlinfo.Password);
            postData.Add("ResetSecurityQuestions", "false");

            string invalidlogin = UploadDataReturnResponse(loginUrl, postData, loginUrl);
            Match invalidLoginMatch = invalidLogin_regex.Match(invalidlogin);
            if (invalidLoginMatch.Success)
            {
                dlinfo.Deactivate();
                ReportDeactivation("Please update Florida Capital Bank password and re-enable any dl using this login: " + dlinfo.Login);
                fileName = null;
                return E_BotDownloadStatusT.Error;
            }
            else
            {
                string temp = DownloadData(baseUrl + @"legacy/services/rates/rates.aspx");
                
                string rsPattern = @"Rate Sheets</font></td></tr>\s+<tr><td>" + DateTime.Now.Date.ToString("MM/d/yyyy") + @"\s*([0-9:.a-zA-Z\s]*)</td><td><a\s+href=""[^""]+""\s+onclick=""javascipt:\s+viewFile\('(?<rsLink>[^']+)'\)""";
                string rsPattern1 = @"Rate Sheets</font></td></tr>\s+<tr><td>" + DateTime.Now.Date.ToString("MM/dd/yyyy") + @"\s*([0-9:.a-zA-Z\s]*)</td><td><a\s+href=""[^""]+""\s+onclick=""javascipt:\s+viewFile\('(?<rsLink>[^']+)'\)""";
                string rsPattern2 = @"Rate Sheets</font></td></tr>\s+<tr><td>" + DateTime.Now.Date.ToString("MM/dd/yy") + @"\s*([0-9:.a-zA-Z\s]*)</td><td><a\s+href=""[^""]+""\s+onclick=""javascipt:\s+viewFile\('(?<rsLink>[^']+)'\)""";
                string rsPattern3 = @"Rate Sheets</font></td></tr>\s+<tr><td>" + DateTime.Now.Date.ToString("M/dd/yy") + @"\s*([0-9:.a-zA-Z\s]*)</td><td><a\s+href=""[^""]+""\s+onclick=""javascipt:\s+viewFile\('(?<rsLink>[^']+)'\)""";
                string rsPattern4 = @"Rate Sheets</font></td></tr>\s+<tr><td>" + DateTime.Now.Date.ToString("M/dd/yyyy") + @"\s*([0-9:.a-zA-Z\s]*)</td><td><a\s+href=""[^""]+""\s+onclick=""javascipt:\s+viewFile\('(?<rsLink>[^']+)'\)""";
                string rsPattern5 = @"Rate Sheets</font></td></tr>\s+<tr><td>" + DateTime.Now.Date.ToString("M/d/yyyy") + @"\s*([0-9:.a-zA-Z\s]*)</td><td><a\s+href=""[^""]+""\s+onclick=""javascipt:\s+viewFile\('(?<rsLink>[^']+)'\)""";

                Regex rs_regex = new Regex(rsPattern);
                Regex rs1_regex = new Regex(rsPattern1);
                Regex rs2_regex = new Regex(rsPattern2);
                Regex rs3_regex = new Regex(rsPattern3);
                Regex rs4_regex = new Regex(rsPattern4);
                Regex rs5_regex = new Regex(rsPattern5);

                Match match = rs_regex.Match(temp);
                Match match1 = rs1_regex.Match(temp);
                Match match2 = rs2_regex.Match(temp);
                Match match3 = rs3_regex.Match(temp);
                Match match4 = rs4_regex.Match(temp);
                Match match5 = rs5_regex.Match(temp);

                if (match.Success)
                {
                    string rsLinkTemp = baseUrl + @"legacy/services/rates/rateFiles.aspx?f=" + match.Groups["rsLink"].Value;
                    fileName = DownloadFile(rsLinkTemp);
                }
                else if (match1.Success)
                {
                    string rsLinkTemp = baseUrl + @"legacy/services/rates/rateFiles.aspx?f=" + match1.Groups["rsLink"].Value;
                    fileName = DownloadFile(rsLinkTemp);
                }
                else if (match2.Success)
                {
                    string rsLinkTemp = baseUrl + @"legacy/services/rates/rateFiles.aspx?f=" + match2.Groups["rsLink"].Value;
                    fileName = DownloadFile(rsLinkTemp);
                }
                else if (match3.Success)
                {
                    string rsLinkTemp = baseUrl + @"legacy/services/rates/rateFiles.aspx?f=" + match3.Groups["rsLink"].Value;
                    fileName = DownloadFile(rsLinkTemp);
                }
                else if (match4.Success)
                {
                    string rsLinkTemp = baseUrl + @"legacy/services/rates/rateFiles.aspx?f=" + match4.Groups["rsLink"].Value;
                    fileName = DownloadFile(rsLinkTemp);
                }
                else if (match5.Success)
                {
                    string rsLinkTemp = baseUrl + @"legacy/services/rates/rateFiles.aspx?f=" + match5.Groups["rsLink"].Value;
                    fileName = DownloadFile(rsLinkTemp);
                }
                else
                    LogErrorAndSendEmailToAdmin("FloridaCapitalBankBot failed to find today ratesheet link. Content: " + temp);

                //Logout
                GoToUrl(baseUrl + @"Login/SignOut");

                if (null != fileName)
                {
                    RatesheetFileName = fileName;
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                {
                    return E_BotDownloadStatusT.Error;
                }
            }
        }
    }
}