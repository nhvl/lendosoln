﻿/// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class GenworthBot : AbstractWebBot
    {
        private static string ratesUrl = @"http://mortgageinsurance.genworth.com/RatesAndGuidelines/RateCards.aspx";
        private static string cuRatesUrl = @"http://mortgageinsurance.genworth.com/CU/RatesGuidelinesAndAnnouncements/Default.aspx";
        private static string guidesUrl = @"http://mortgageinsurance.genworth.com/RatesAndGuidelines/Guidelines.aspx";
        private static string tempD = @"<td class=""date"">\s+(?<date>([0-9/]+))\s+</td>\s+<td\s+class=""[^""]+"">\s+<table\s+style=""[^""]+"">\s+<tr\s+valign=""top"">\s+<td\s+style=""[^""]+"">\s+<img\s+id=""[^""]+""\s+src=""[^""]+""\s+alt=""PDF""\s+align=""middle""\s+style=""[^""]+""\s+/>\s+</td>\s+<td\s+style=""[^""]+"">\s+<a\s+id=""[^""]+""\s+href=""[^""]+""\s+target=""_blank"">";
        private static string tempD_CU = @"<td class=""date"">\s+(?<date>([0-9/]+))\s+</td>\s+<td\s+class=""[^""]+"">\s+<table\s+style=""[^""]+"">\s+<tr\s+valign=""top"">\s+<td\s+style=""[^""]+"">\s+<img\s+id=""[^""]+""\s+src=""[^""]+""\s+alt=""PDF""\s+align=""middle""\s+style=""[^""]+""\s+/>\s+</td>\s+<td\s+style=""[^""]+"">\s+<a\s+id=""[^""]+""\s+href=""[^""]+""\s+target=""_blank"">";
        
        //GENWORTH_STD Rates Regex
        private static Regex bpmiMonthly_regex = new Regex(tempD + "Monthly Premium MI", RegexOptions.IgnoreCase);
        private static Regex bpmiSingle_regex = new Regex(tempD + @"Single Premium MI \(Refundable and Non-Refundable\)", RegexOptions.IgnoreCase);
        private static Regex bpmiSplits_regex = new Regex(tempD + @"Split Premium MI \(All states except Washington\)", RegexOptions.IgnoreCase);
        private static Regex bpmiSplitsWA_regex = new Regex(tempD + @"Split Premium MI \(Washington only\)", RegexOptions.IgnoreCase);
        private static Regex lpmiMonthly_regex = new Regex(tempD + "Lender Paid Monthly Premium MI", RegexOptions.IgnoreCase);
        private static Regex lpmiSingle_regex = new Regex(tempD + "Lender Paid Single Premium MI", RegexOptions.IgnoreCase);
        private static Regex streamlineRefi_regex = new Regex(tempD + "Genworth-Insured Streamlined Refinance Program", RegexOptions.IgnoreCase);
        
        //GENWORTH_CU Rates Regex
        private static Regex cuBpmiMoSiAllStates_regex = new Regex(tempD_CU + @"Credit Union Rates: Monthly Premium MI and Single Premium MI \(for all states unless listed below\)", RegexOptions.IgnoreCase);
        private static Regex cuBpmiSplitAllStates_regex = new Regex(tempD_CU + @"Credit Union Rates: Split Premium MI \(for all states except Washington\)", RegexOptions.IgnoreCase);
        private static Regex cuBpmiSplitWA_regex = new Regex(tempD_CU + @"Credit Union Rates: Split Premium MI \(for Washington only\)", RegexOptions.IgnoreCase);
        private static Regex cuBpmiNY_regex = new Regex(tempD_CU + @"Credit Union Rates: New York", RegexOptions.IgnoreCase);
        private static Regex cuBpmiRI_regex = new Regex(tempD_CU + @"Credit Union Rates: Rhode Island", RegexOptions.IgnoreCase);
        private static Regex cuBpmiWA_regex = new Regex(tempD_CU + @"Credit Union Rates: Washington", RegexOptions.IgnoreCase);
        private static Regex cuLpmiMonthly_regex = new Regex(tempD_CU + "Lender Paid MI Monthly Premium Rates", RegexOptions.IgnoreCase);
        private static Regex cuLpmiSingle_regex = new Regex(tempD_CU + "Lender Paid MI Single Premium Rates", RegexOptions.IgnoreCase);
        private static Regex cuStreamlineRefi_regex = new Regex(tempD_CU + @"Credit Union Rates: Genworth-Insured Streamlined Refinance Program \(for all states unless listed below\)", RegexOptions.IgnoreCase);
        private static Regex cuStreamlineRefiRI_regex = new Regex(tempD_CU + @"Credit Union Rates: Genworth-Insured Streamlined Refinance Program: Rhode Island", RegexOptions.IgnoreCase);
        private static Regex cuStreamlineRefiNY_regex = new Regex(tempD_CU + @"Credit Union Rates: Genworth-Insured Streamlined Refinance Program: New York", RegexOptions.IgnoreCase);
        private static Regex cuStreamlineRefiWA_regex = new Regex(tempD_CU + @"Credit Union Rates: Genworth-Insured Streamlined Refinance Program: Washington", RegexOptions.IgnoreCase);

        //GENWORTH_STD GUIDE Regex
        private static Regex guideSummary_regex = new Regex(tempD + @"Underwriting Guidelines Summary", RegexOptions.IgnoreCase);
        private static Regex ltv_regex = new Regex(tempD + @"Eligibility Recap", RegexOptions.IgnoreCase);
        private static Regex uwGuide_regex = new Regex(tempD + @"Underwriting Guidelines", RegexOptions.IgnoreCase);

        //GENWORTH_CU GUIDE Regex
        private static Regex cuGuideSummary_regex = new Regex(tempD_CU + @"Underwriting Guidelines Summary for Credit Unions", RegexOptions.IgnoreCase);
        private static Regex cuLtv_regex = new Regex(tempD_CU + @"Eligibility Recap for Credit Unions", RegexOptions.IgnoreCase);
        private static Regex cuHFAUw_regex = new Regex(tempD_CU + "HFA Underwriting Guideline Summary", RegexOptions.IgnoreCase);
        private static Regex cuUwGuide_regex = new Regex(tempD_CU + @"Underwriting Guidelines", RegexOptions.IgnoreCase);

        List<string> listOfChanges = new List<string>();
        List<string> listOfRegexError = new List<string>();

        public GenworthBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "GENWORTH"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string temp = null;

            switch (dlinfo.Description)
            {
                #region GENWORTH_STD
                case "GENWORTH_STD":
                    //Rates Date
                    string bpmiMonthlyDate = @"5/14/12";
                    string bpmiSingleDate = @"5/14/12";
                    string bpmiSplitDate = @"5/14/12";
                    string bpmiSplitWADate = @"1/10/11";
                    string lpmiMonthlyDate = @"5/14/12";
                    string lpmiSingleDate = @"5/14/12";
                    string streamlineRefiDate = @"5/14/12";

                    //Guides Date
                    string guideSummaryDate = @"01/27/13";
                    string ltvDate = @"01/27/13";
                    string uwGuideDate = @"01/27/13";

                    /*..............................
                     * Check Rates Date
                     * .............................
                     */

                    temp = DownloadData(ratesUrl);

                    //Check BPMI Monthly Premium
                    Match bpmiMonthlyMatch = bpmiMonthly_regex.Match(temp);
                    if (bpmiMonthlyMatch.Success)
                    {
                        if (!bpmiMonthlyMatch.Groups["date"].Value.Equals(bpmiMonthlyDate))
                            listOfChanges.Add("BPMI Monthly Premiums");
                    }
                    else
                        listOfRegexError.Add("BPMI Monthly Premiums");

                    //Check BPMI Single Premium
                    Match bpmiRefSingleMatch = bpmiSingle_regex.Match(temp);
                    if (bpmiRefSingleMatch.Success)
                    {
                        if (!bpmiRefSingleMatch.Groups["date"].Value.Equals(bpmiSingleDate))
                            listOfChanges.Add("BPMI Single Premium");
                    }
                    else
                        listOfRegexError.Add("BPMI Single Premium");

                    //Check BPMI Split Premium All states except Washington
                    Match bpmiSplitMatch = bpmiSplits_regex.Match(temp);
                    if (bpmiSplitMatch.Success)
                    {
                        if (!bpmiSplitMatch.Groups["date"].Value.Equals(bpmiSplitDate))
                            listOfChanges.Add("BPMI Split Premium (All States except Washington)");
                    }
                    else
                        listOfRegexError.Add("BPMI Split Premium (All States except Washington)");

                    //Check BPMI Split Premium WA only
                    Match bpmiSplitWAMatch = bpmiSplitsWA_regex.Match(temp);
                    if (bpmiSplitWAMatch.Success)
                    {
                        if (!bpmiSplitWAMatch.Groups["date"].Value.Equals(bpmiSplitWADate))
                            listOfChanges.Add("BPMI Split Premium (WA only)");
                    }
                    else
                        listOfRegexError.Add("BPMI Split Premium (WA only)");

                    //Check LPMI Monthly Premium
                    Match lpmiMonthlyMatch = lpmiMonthly_regex.Match(temp);
                    if (lpmiMonthlyMatch.Success)
                    {
                        if (!lpmiMonthlyMatch.Groups["date"].Value.Equals(lpmiMonthlyDate))
                            listOfChanges.Add("LPMI Monthly Premium");
                    }
                    else
                        listOfRegexError.Add("LPMI Monthly Premium");

                    
                    //Check LPMI Single Premium
                    Match lpmiSingleMatch = lpmiSingle_regex.Match(temp);
                    if (lpmiSingleMatch.Success)
                    {
                        if (!lpmiSingleMatch.Groups["date"].Value.Equals(lpmiSingleDate))
                            listOfChanges.Add("LPMI Single Premium");
                    }
                    else
                        listOfRegexError.Add("LPMI Single Premium");

                    //Check Genworth-Insured Streamlined Refi
                    Match streamRefiMatch = streamlineRefi_regex.Match(temp);
                    if (streamRefiMatch.Success)
                    {
                        if (!streamRefiMatch.Groups["date"].Value.Equals(streamlineRefiDate))
                            listOfChanges.Add("Genworth-Insured Streamline Refi");
                    }
                    else
                        listOfRegexError.Add("Genworth-Insured Streamline Refi");

                    /*.......................
                     * Check Guidelines Date
                     * ......................
                     */

                    temp = DownloadData(guidesUrl);

                    //Check UW Guideline Summary
                    Match guideSummaryMatch = guideSummary_regex.Match(temp);
                    if (guideSummaryMatch.Success)
                    {
                        if (!guideSummaryMatch.Groups["date"].Value.Equals(guideSummaryDate))
                            listOfChanges.Add("UW Guidelines Summary");
                    }
                    else
                        listOfRegexError.Add("UW Guidelines Summary");

                    //Check LTV Eligibility
                    Match ltvMatch = ltv_regex.Match(temp);
                    if (ltvMatch.Success)
                    {
                        if (!ltvMatch.Groups["date"].Value.Equals(ltvDate))
                            listOfChanges.Add("LTV Eligibility");
                    }
                    else
                        listOfRegexError.Add("LTV Eligibility");

                    //Check Underwriting Guidelines
                    Match uwGuideMatch = uwGuide_regex.Match(temp);
                    if (uwGuideMatch.Success)
                    {
                        if (!uwGuideMatch.Groups["date"].Value.Equals(uwGuideDate))
                            listOfChanges.Add("Underwriting Guidelines");
                    }
                    else
                        listOfRegexError.Add("Underwriting Guidelines");
                    break;
                #endregion

                #region GENWORTH_CU
                case "GENWORTH_CU":
                    //Rates Date
                    string cuBPMIMoSiAllStates = @"3/26/12";
                    string cuBPMISplitAllStates = @"8/1/11";
                    string cuBPMISplitWA = @"1/10/11";
                    string cuBPMINY = @"5/14/12";
                    string cuBPMIRI = @"3/26/12";
                    string cuBPMIWA = @"3/26/12";
                    string cuLPMIMo = @"5/14/12";
                    string cuLPMISi = @"5/14/12";
                    string cuStreamRefi = @"6/6/11";
                    string cuStreamRefiRI = @"6/28/10";
                    string cuStreamRefiNY = @"8/16/10";
                    string cuStreamRefiWA = @"10/26/09";

                    //Guides Date
                    string cuGuideSummaryDate = @"01/27/13";
                    string cuLtvDate = @"01/27/13";
                    string cuHFAUwDate = @"01/27/13";
                    string cuUwGuideDate = @"01/27/13";

                    /*............................
                     * Check Rates Date
                     * ...........................
                     */

                    temp = DownloadData(cuRatesUrl);

                    //Check Credit Union BPMI Monthly and Single (All States)
                    Match cuBPMIMonthlyMatch = cuBpmiMoSiAllStates_regex.Match(temp);
                    if (cuBPMIMonthlyMatch.Success)
                    {
                        if (!cuBPMIMonthlyMatch.Groups["date"].Value.Equals(cuBPMIMoSiAllStates))
                            listOfChanges.Add("CU BPMI Monthly and Single Premium MI (All States)");
                    }
                    else
                        listOfRegexError.Add("CU BPMI Monthly and Single Premium MI (All States)");

                    //Check Credit Union BPMI Split (All States)
                    Match cuBPMISplitMatch = cuBpmiSplitAllStates_regex.Match(temp);
                    if (cuBPMISplitMatch.Success)
                    {
                        if (!cuBPMISplitMatch.Groups["date"].Value.Equals(cuBPMISplitAllStates))
                            listOfChanges.Add("CU BPMI Split Premium MI (All States)");
                    }
                    else
                        listOfRegexError.Add("CU BPMI Split Premium MI (All States)");

                    //Check Credit Union BPMI Split (Washington)
                    Match cuBPMISplitWAMatch = cuBpmiSplitWA_regex.Match(temp);
                    if (cuBPMISplitWAMatch.Success)
                    {
                        if (!cuBPMISplitWAMatch.Groups["date"].Value.Equals(cuBPMISplitWA))
                            listOfChanges.Add("CU BPMI Split Premium MI (Washington only)");
                    }
                    else
                        listOfRegexError.Add("CU BPMI Split Premium MI (Washington only)");

                    //Check Credit Union BPMI NY
                    Match cuBPMINYMatch = cuBpmiNY_regex.Match(temp);
                    if (cuBPMINYMatch.Success)
                    {
                        if (!cuBPMINYMatch.Groups["date"].Value.Equals(cuBPMINY))
                            listOfChanges.Add("CU BPMI: NY");
                    }
                    else
                        listOfRegexError.Add("CU BPMI: NY");

                    //Check Credit Union BPMI RI
                    Match cuBPMIRIMatch = cuBpmiRI_regex.Match(temp);
                    if (cuBPMIRIMatch.Success)
                    {
                        if (!cuBPMIRIMatch.Groups["date"].Value.Equals(cuBPMIRI))
                            listOfChanges.Add("CU BPMI: RI");
                    }
                    else
                        listOfRegexError.Add("CU BPMI: RI");

                    //Check Credit Union BPMI WA
                    Match cuBPMIWAMatch = cuBpmiWA_regex.Match(temp);
                    if (cuBPMIWAMatch.Success)
                    {
                        if (!cuBPMIWAMatch.Groups["date"].Value.Equals(cuBPMIWA))
                            listOfChanges.Add("CU BPMI: WA");
                    }
                    else
                        listOfRegexError.Add("CU BPMI: WA");

                    //Check Credit Union LPMI Monthly
                    Match cuLPMIMonthlyMatch = cuLpmiMonthly_regex.Match(temp);
                    if (cuLPMIMonthlyMatch.Success)
                    {
                        if (!cuLPMIMonthlyMatch.Groups["date"].Value.Equals(cuLPMIMo))
                            listOfChanges.Add("CU LPMI Monthly Premium");
                    }
                    else
                        listOfRegexError.Add("CU LPMI Monthly Premium");

                    //Check Credit Union LPMI Single
                    Match cuLPMISingleMatch = cuLpmiSingle_regex.Match(temp);
                    if (cuLPMISingleMatch.Success)
                    {
                        if (!cuLPMISingleMatch.Groups["date"].Value.Equals(cuLPMISi))
                            listOfChanges.Add("CU LPMI Single Premium");
                    }
                    else
                        listOfRegexError.Add("CU LPMI Single Premium");

                    //Check Credit Union Streamlined Refi (All States)
                    Match cuStreamlineRefiMatch = cuStreamlineRefi_regex.Match(temp);
                    if (cuStreamlineRefiMatch.Success)
                    {
                        if (!cuStreamlineRefiMatch.Groups["date"].Value.Equals(cuStreamRefi))
                            listOfChanges.Add("CU Streamlined Refi (All States)");
                    }
                    else
                        listOfRegexError.Add("CU Streamlined Refi (All States)");

                    //Check Credit Union Streamlined Refi RI
                    Match cuStreamlineRefiRIMatch = cuStreamlineRefiRI_regex.Match(temp);
                    if (cuStreamlineRefiRIMatch.Success)
                    {
                        if (!cuStreamlineRefiRIMatch.Groups["date"].Value.Equals(cuStreamRefiRI))
                            listOfChanges.Add("CU Streamlined Refi: RI");
                    }
                    else
                        listOfRegexError.Add("CU Streamlined Refi: RI");

                    //Check Credit Union Streamlined Refi NY
                    Match cuStreamlineRefiNYMatch = cuStreamlineRefiNY_regex.Match(temp);
                    if (cuStreamlineRefiNYMatch.Success)
                    {
                        if (!cuStreamlineRefiNYMatch.Groups["date"].Value.Equals(cuStreamRefiNY))
                            listOfChanges.Add("CU Streamlined Refi: NY");
                    }
                    else
                        listOfRegexError.Add("CU Streamlined Refi: NY");

                    //Check Credit Union Streamlined Refi WA
                    Match cuStreamlineRefiWAMatch = cuStreamlineRefiWA_regex.Match(temp);
                    if (cuStreamlineRefiWAMatch.Success)
                    {
                        if (!cuStreamlineRefiWAMatch.Groups["date"].Value.Equals(cuStreamRefiWA))
                            listOfChanges.Add("CU Streamlined Refi: WA");
                    }
                    else
                        listOfRegexError.Add("CU Streamlined Refi: WA");

                    /*.....................................
                     * Check Guidelines Date
                     * ....................................
                     */

                    //Check CU UW Guideline Summary
                    Match cuGuideSummaryMatch = cuGuideSummary_regex.Match(temp);
                    if (cuGuideSummaryMatch.Success)
                    {
                        if (!cuGuideSummaryMatch.Groups["date"].Value.Equals(cuGuideSummaryDate))
                            listOfChanges.Add("CU UW Guidelines Summary");
                    }
                    else
                        listOfRegexError.Add("CU UW Guidelines Summary");


                    //Check CU LTV Eligibility
                    Match cuLtvMatch = cuLtv_regex.Match(temp);
                    if (cuLtvMatch.Success)
                    {
                        if (!cuLtvMatch.Groups["date"].Value.Equals(cuLtvDate))
                            listOfChanges.Add("CU LTV Eligibility");
                    }
                    else
                        listOfRegexError.Add("CU LTV Eligibility");

                    //Check CU Underwriting Guidelines Addendum
                    Match cuHFAUwMatch = cuHFAUw_regex.Match(temp);
                    if (cuHFAUwMatch.Success)
                    {
                        if (!cuHFAUwMatch.Groups["date"].Value.Equals(cuHFAUwDate))
                            listOfChanges.Add("CU HFA Underwriting Guidelines Summary");
                    }
                    else
                        listOfRegexError.Add("CU HFA Underwriting Guidelines Summary");

                    //Check CU Underwriting Guidelines
                    Match cuUwGuideMatch = cuUwGuide_regex.Match(temp);
                    if (cuUwGuideMatch.Success)
                    {
                        if (!cuUwGuideMatch.Groups["date"].Value.Equals(cuUwGuideDate))
                            listOfChanges.Add("CU Underwriting Guidelines");
                    }
                    else
                        listOfRegexError.Add("CU Underwriting Guidelines");
                    break;
                #endregion

                default:
                    ReportDeactivation("Incorrect Genworth Bot description. Please set the correct description. It's either Genworth_STD or Genworth_CU");
                    dlinfo.Deactivate();
                    break;
            }

            if (listOfChanges.Count != 0)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine("Genworth Bot detect changes on the following guidelines/ rates date. Please check the changes and update the bot, templates, and/or map. And enable the download: ");
                foreach (string change in listOfChanges)
                    errorMessage.AppendLine(change);
                ReportDeactivation(errorMessage.ToString());
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }
            else if (listOfRegexError.Count != 0)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine("Genworth Bot detect changes on the following guidelines/rates regex. Please check the changes and update the bot. And enable the download: ");
                foreach (string regexError in listOfRegexError)
                    errorMessage.AppendLine(regexError);
                ReportDeactivation(errorMessage.ToString());
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
    }
}


