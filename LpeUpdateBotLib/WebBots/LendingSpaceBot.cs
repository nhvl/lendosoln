﻿//Author: Ian Merna

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

using LpeUpdateBotLib.Common;
namespace LpeUpdateBotLib.WebBots
{
    public class LendingSpaceBot : AbstractWebBot
    {
        private string loginUrl = @"/SRVLPSPORTAL/LsAuth/Application/LsLogin/Login/Login.jsp";
        private static string logOutUrl = "/corr_portal/LsAuth/includefiles/SignOut.jsp?hidToken=";

        
        private Regex radSessionMgmtAppSessionId_regex = new Regex(@"name=""radSessionMgmtAppSessionId"" value=""(?<radSessionMgmtAppSessionId>[^""]+)", RegexOptions.IgnoreCase);
        private Regex hidSessionId_regex = new Regex(@"name=""hidSessionId"" value=""(?<hidSessionId>[^""]+)", RegexOptions.IgnoreCase);

        private Regex userId_regex = new Regex(@"name=""userId""\s*type=""radio""\s*value='(?<userId>[^']+)'>\s*<a\s*style=""cursor.hand.""\s*onClick=""document.frmRole.userId.(?<userNumRoles>[1234567890])..checked=true.frmRole.imgOK.click..."">\s*SECONDARY\s*MARKETING", RegexOptions.IgnoreCase);
        private Regex hidToken_regex = new Regex(@"name=""hidToken"" value=""(?<hidToken>[^""]+)", RegexOptions.IgnoreCase);

        private Regex userRoles_regex = new Regex(@"UserRoles.jsp.hidToken=(?<userRoles>[^']+)", RegexOptions.IgnoreCase);
        private Regex hidTokenLeadsCatalog_regex = new Regex(@"ListMyLeadsCatg.jsp.hidToken=(?<hidTokenLeadsCatalog>[^""]+)", RegexOptions.IgnoreCase);
        private Regex hidTokenInsertSession_regex = new Regex(@"insertSession.jsp.hidToken=(?<hidTokenInsertSession>[^'""]+)", RegexOptions.IgnoreCase);
        private Regex hidTokenRemoveLock_regex = new Regex(@"RemoveLock.jsp.hidToken=(?<hidTokenRemoveLock>[^'\s]+)", RegexOptions.IgnoreCase);
        private Regex svToken_regex = new Regex(@"<SV_Token>(?<svToken>[^<]+)", RegexOptions.IgnoreCase);
        private Regex logoutToken_regex = new Regex(@"<SV_Token><\!\[CDATA\[(?<logoutToken>[^\]]+)", RegexOptions.IgnoreCase);
        private Regex compID_regex = new Regex(@"<SV_CompId><\!\[CDATA\[(?<compID>[^\]]+)", RegexOptions.IgnoreCase);
        private Regex ratesheetID_regex = new Regex(@"<RateSheetId>(?<ratesheetID>[^<]+)", RegexOptions.IgnoreCase);
        private Regex effectDtm_regex = new Regex(@"<EffectDtm>(?<effectDtm>[^<]+)", RegexOptions.IgnoreCase);
        

        public LendingSpaceBot(IDownloadInfo info)
            : base(info)
        {
        }
        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text /html, application/xhtml+xml, */*";
            }
        }
        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }
        protected override string BotUserAgentString
        {
            get
            {
                return @"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
            }
        }
        public override string BotName
        {
            get { return "LENDINGSPACE"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            
            
            //////Set up the differences for each investor///////
            string orgId, baseUrl, rsFormat, genericErrorMessage, stage;

            if( dlinfo.Description.Contains("SUNTRUST"))
            {
                orgId = @"stm";
                baseUrl = @"https://loanspherelendingspace.bkiconnect.com/stm";
                rsFormat = @"PDF";
                stage = @"/SRVLPSPORTAL";
                genericErrorMessage = @"LendingSpace bot failed for SUNTRUST, login; " + dlinfo.Login + " response HTML; ";
            }
            else if(dlinfo.Description.Contains("FLAGSTAR_LENDINGSPACE"))
            {
                orgId = @"Flagstar";
                baseUrl = @"https://correspondent.flagstar.com";
                rsFormat = @"PDF";
                stage = @"/corr_portal";
                genericErrorMessage = @"LendingSpace bot failed for FLAGSTAR LENDING SPACE, login; " + dlinfo.Login + " response HTML; ";
                loginUrl = @"/corr_portal/LsAuth/Application/LsLogin/Login/Login.jsp";
            }
            else if (dlinfo.Description.Contains("FIFTH_THIRD"))
            {
                orgId = @"53";
                baseUrl = @"https://loanspherelendingspace.bkiconnect.com/53";
                rsFormat = @"PDF";
                stage = @"/corr_portal";
                genericErrorMessage = @"LendingSpace bot failed for FIFTH THIRD, login; " + dlinfo.Login + " response HTML; ";
                loginUrl = @"/corr_portal/LsAuth/Application/LsLogin/Login/Login.jsp";
            }
            else
            {
                LogErrorAndSendEmailToAdmin("invalid bot description");
                return E_BotDownloadStatusT.Error;
            }
            //////*************************************////////

            NameValueCollection postData = new NameValueCollection();
            postData.Add("txtLoginNm", dlinfo.Login);
            postData.Add("passwordtext", "PASSWORD");
            postData.Add("txtPassword", dlinfo.Password);
            postData.Add("OrgIdtxt", "ORG. ID");
            postData.Add("txtOrgId", orgId);
            postData.Add("Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)");
            postData.Add("Version", "4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)");
            postData.Add("Code", "Mozilla");
            postData.Add("Name", "Microsoft Internet Explorer");
            postData.Add("txtSub", "");
            postData.Add("BtnSub", "");


            string dlUrl = null;
            string responseHTML = UploadDataReturnResponse(baseUrl + loginUrl, postData);

            Match hidTokenLeadsCatalog_match = hidTokenLeadsCatalog_regex.Match(responseHTML);
            Match hidTokenRemoveLock_match = hidTokenRemoveLock_regex.Match(responseHTML);
            Match hidTokenInsertSession_match = hidTokenInsertSession_regex.Match(responseHTML);
            Match userRoles_match = userRoles_regex.Match(responseHTML);

            ////
            //in this case user has logged in with no issue, taken directly to main pipeline
            ////
            if (hidTokenLeadsCatalog_match.Success)
            {
                responseHTML = DownloadData(baseUrl + "/corr_portal/LeadMgmt/Application/Lead/ListMyLeadsCatg.jsp?hidToken=" + hidTokenLeadsCatalog_match.Groups[1].Value);
                Match svToken_match = svToken_regex.Match(responseHTML);

                if (svToken_match.Success)
                {
                    NameValueCollection postData2 = new NameValueCollection();
                    postData2.Add("hidToken", svToken_match.Groups[1].Value);
                    postData2.Add("hidDirectory", "");
                    postData2.Add("hidMenuItem", "");
                    postData2.Add("hidMenuLevel", "0");
                    postData2.Add("hidMenuPageName", "../../../LeadMgmt/Application/RateSheet/EditRateSheet.jsp?");
                    postData2.Add("hidUnlockInd", "");
                    postData2.Add("hidNumRoles", "1");
                    responseHTML = UploadDataReturnResponse(baseUrl + "/corr_portal/LeadMgmt/Application/RateSheet/EditRateSheet.jsp?", postData2);
                    Match logoutToken_match = logoutToken_regex.Match(responseHTML);
                    Match ratesheetID_match = ratesheetID_regex.Match(responseHTML);
                    Match effectDtm_match = effectDtm_regex.Match(responseHTML);
                    Match compID_match = compID_regex.Match(responseHTML);

                    if (ratesheetID_match.Success && effectDtm_match.Success && compID_match.Success)
                    {
                        //ratesheet timestamp is blank or ratesheet has been updated
                        if(dlinfo.LastRatesheetTimestamp < Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value))
                        {
                            dlUrl = baseUrl + "/corr_portal/servlet/DownloadFile?RateSheetId=" + ratesheetID_match.Groups[1].Value + "&format=" + rsFormat + "&filetype=EXCEL&effectDtm=" + effectDtm_match.Groups[1].Value + "&expireDtm=&compid=" + compID_match.Groups[1].Value + "&reporttype=BPRICE";
                            RatesheetFileName = DownloadFile(dlUrl);
                            GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);

                            //updating timestamp should be last action before returning
                            dlinfo.RecordLastRatesheetTimestamp(Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value));
                            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                        }
                        else
                        {
                            //last effective time has not changed
                            GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                        }

                            
                    }
                    else
                    {
                        LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                        GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                        return E_BotDownloadStatusT.Error;
                    }
                }
                else
                {
                    LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                    GoToUrl(baseUrl + logOutUrl);
                    return E_BotDownloadStatusT.Error;
                }
            }

            ////
            //In this case there is an existing session in place, must be removed
            ////
            else if (hidTokenRemoveLock_match.Success)
            {
                responseHTML = DownloadData(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/RemoveLock.jsp?hidToken=" + hidTokenRemoveLock_match.Groups[1].Value);
                Match hidSessionId_match = hidSessionId_regex.Match(responseHTML);
                Match radSessionMgmtAppSessionId_match = radSessionMgmtAppSessionId_regex.Match(responseHTML);
                hidTokenInsertSession_match = hidTokenInsertSession_regex.Match(responseHTML);

                if (hidSessionId_match.Success && hidTokenInsertSession_match.Success && radSessionMgmtAppSessionId_match.Success)
                {
                    NameValueCollection postData3 = new NameValueCollection();
                    postData3.Add("hidSessionMgmtSaveFlag", "FALSE");
                    postData3.Add("radSessionMgmtAppSessionId", radSessionMgmtAppSessionId_match.Groups[1].Value);
                    postData3.Add("hidSessionId", hidSessionId_match.Groups[1].Value);
                    postData3.Add("txtSessionMgmtDisplayRows", "10");
                    postData3.Add("hidSessionMgmtNextIndex", "1");
                    postData3.Add("hidSessionMgmtNextPrevVal", "");
                    postData3.Add("hidSessionMgmtPreviousIndex", "0");

                    
                    responseHTML = UploadDataReturnResponse(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/RemoveLock.jsp?acn=del&sid=" + hidSessionId_match.Groups[1].Value, postData3);
                    hidTokenLeadsCatalog_match = hidTokenLeadsCatalog_regex.Match(responseHTML);


                    if (hidTokenLeadsCatalog_match.Success)
                    {
                        responseHTML = DownloadData(baseUrl + "/corr_portal/LeadMgmt/Application/Lead/ListMyLeadsCatg.jsp?hidToken=" + hidTokenLeadsCatalog_match.Groups[1].Value);
                        Match svToken_match = svToken_regex.Match(responseHTML);

                        if (svToken_match.Success)
                        {
                            NameValueCollection postData4 = new NameValueCollection();
                            postData4.Add("hidToken", svToken_match.Groups[1].Value);
                            postData4.Add("hidDirectory", "");
                            postData4.Add("hidMenuItem", "");
                            postData4.Add("hidMenuLevel", "0");
                            postData4.Add("hidMenuPageName", "../../../LeadMgmt/Application/RateSheet/EditRateSheet.jsp?");
                            postData4.Add("hidUnlockInd", "");
                            postData4.Add("hidNumRoles", "1");
                            responseHTML = UploadDataReturnResponse(baseUrl + "/corr_portal/LeadMgmt/Application/RateSheet/EditRateSheet.jsp?", postData4);
                            Match logoutToken_match = logoutToken_regex.Match(responseHTML);
                            Match ratesheetID_match = ratesheetID_regex.Match(responseHTML);
                            Match effectDtm_match = effectDtm_regex.Match(responseHTML);
                            Match compID_match = compID_regex.Match(responseHTML);

                            if (ratesheetID_match.Success && effectDtm_match.Success && compID_match.Success)
                            {
                                //ratesheet timestamp is blank or ratesheet has been updated
                                if (dlinfo.LastRatesheetTimestamp < Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value))
                                {
                                    dlUrl = baseUrl + "/corr_portal/servlet/DownloadFile?RateSheetId=" + ratesheetID_match.Groups[1].Value + "&format=" + rsFormat + "&filetype=EXCEL&effectDtm=" + effectDtm_match.Groups[1].Value + "&expireDtm=&compid=" + compID_match.Groups[1].Value + "&reporttype=BPRICE";
                                    RatesheetFileName = DownloadFile(dlUrl);
                                    GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);

                                    //updating timestamp should be last action before returning
                                    dlinfo.RecordLastRatesheetTimestamp(Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value));
                                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                                }
                                else
                                {
                                    //last effective time has not changed
                                    GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                                }
                            }
                            else
                            {
                                LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                                GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                                return E_BotDownloadStatusT.Error;
                            }
                        }
                        else
                        {
                            LogErrorAndSendEmailToAdmin("LendingSpace Bot failed to find svToken for login " + dlinfo.Login);
                            GoToUrl(baseUrl + logOutUrl);
                            return E_BotDownloadStatusT.Error;
                        }
                    }
                    else
                    {
                        LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                        GoToUrl(baseUrl + logOutUrl);
                        return E_BotDownloadStatusT.Error;
                    }
                }

                else
                {
                    LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                    GoToUrl(baseUrl + logOutUrl);
                    return E_BotDownloadStatusT.Error;
                }


            }

            ////
            //theres some initial pop-up but the login was successful (e.g. "password will expire in 8 days")
            ////
            else if (hidTokenInsertSession_match.Success)
            {
                
                responseHTML = DownloadData(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/insertSession.jsp?hidToken=" + hidTokenInsertSession_match.Groups[1].Value);
                hidTokenLeadsCatalog_match = hidTokenLeadsCatalog_regex.Match(responseHTML);

                if (hidTokenLeadsCatalog_match.Success)
                {


                    responseHTML = DownloadData(baseUrl + "/corr_portal/LeadMgmt/Application/Lead/ListMyLeadsCatg.jsp?hidToken=" + hidTokenLeadsCatalog_match.Groups[1].Value);
                    Match svToken_match = svToken_regex.Match(responseHTML);

                    if (svToken_match.Success)
                    {
                        NameValueCollection postData4 = new NameValueCollection();
                        postData4.Add("hidToken", svToken_match.Groups[1].Value);
                        postData4.Add("hidDirectory", "");
                        postData4.Add("hidMenuItem", "");
                        postData4.Add("hidMenuLevel", "0");
                        postData4.Add("hidMenuPageName", "../../../LeadMgmt/Application/RateSheet/EditRateSheet.jsp?");
                        postData4.Add("hidUnlockInd", "");
                        postData4.Add("hidNumRoles", "1");
                        responseHTML = UploadDataReturnResponse(baseUrl + "/corr_portal/LeadMgmt/Application/RateSheet/EditRateSheet.jsp?", postData4);
                        Match logoutToken_match = logoutToken_regex.Match(responseHTML);
                        Match ratesheetID_match = ratesheetID_regex.Match(responseHTML);
                        Match effectDtm_match = effectDtm_regex.Match(responseHTML);
                        Match compID_match = compID_regex.Match(responseHTML);

                        if (ratesheetID_match.Success && effectDtm_match.Success && compID_match.Success)
                        {
                            //ratesheet timestamp is blank or ratesheet has been updated
                            if (dlinfo.LastRatesheetTimestamp < Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value))
                            {
                                dlUrl = baseUrl + "/corr_portal/servlet/DownloadFile?RateSheetId=" + ratesheetID_match.Groups[1].Value + "&format=" + rsFormat + "&filetype=EXCEL&effectDtm=" + effectDtm_match.Groups[1].Value + "&expireDtm=&compid=" + compID_match.Groups[1].Value + "&reporttype=BPRICE";
                                RatesheetFileName = DownloadFile(dlUrl);
                                GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);

                                //updating timestamp should be last action before returning
                                dlinfo.RecordLastRatesheetTimestamp(Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value));
                                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                            }
                            else
                            {
                                //last effective time has not changed
                                GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                            }
                        }
                        else
                        {
                            LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                            GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                            return E_BotDownloadStatusT.Error;
                        }
                    }
                    else
                    {
                        LogErrorAndSendEmailToAdmin("LendingSpace Bot failed to find svToken for login " + dlinfo.Login);
                        GoToUrl(baseUrl + logOutUrl);
                        return E_BotDownloadStatusT.Error;
                    }
                }
                else
                {
                    LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                    GoToUrl(baseUrl + logOutUrl);
                    return E_BotDownloadStatusT.Error;
                }
            }

            ////
            //the user's login must first select a Role
            ////
            else 
            
            if (userRoles_match.Success)
            {
                responseHTML = DownloadData(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/UserRoles.jsp?hidToken=" + userRoles_match.Groups[1].Value);
                Match hidToken_match = hidToken_regex.Match(responseHTML);
                Match userId_match = userId_regex.Match(responseHTML);

                if (hidToken_match.Success && userId_match.Success)
                {
                    NameValueCollection postData4 = new NameValueCollection();
                    postData4.Add("userId", userId_match.Groups[1].Value);
                    postData4.Add("hidToken", hidToken_match.Groups[1].Value);
                    postData4.Add("Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)");
                    postData4.Add("Version", "4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)");
                    postData4.Add("Code", "Mozilla");
                    postData4.Add("Name", "Microsoft Internet Explorer");


                    responseHTML = UploadDataReturnResponse(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/UserRoles.jsp", postData4);

                    //this part basically just copies the original plan, but for remove lock and insert session
                    hidTokenRemoveLock_match = hidTokenRemoveLock_regex.Match(responseHTML);
                    hidTokenInsertSession_match = hidTokenInsertSession_regex.Match(responseHTML);


                    ////
                    //In this case there is an existing session in place, must be removed
                    ////
                    if (hidTokenRemoveLock_match.Success)
                    {
                        responseHTML = DownloadData(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/RemoveLock.jsp?hidToken=" + hidTokenRemoveLock_match.Groups[1].Value);
                        Match hidSessionId_match = hidSessionId_regex.Match(responseHTML);
                        Match radSessionMgmtAppSessionId_match = radSessionMgmtAppSessionId_regex.Match(responseHTML);
                        hidTokenInsertSession_match = hidTokenInsertSession_regex.Match(responseHTML);

                        if (hidSessionId_match.Success && hidTokenInsertSession_match.Success && radSessionMgmtAppSessionId_match.Success)
                        {
                            NameValueCollection postData3 = new NameValueCollection();
                            postData3.Add("hidSessionMgmtSaveFlag", "FALSE");
                            postData3.Add("radSessionMgmtAppSessionId", radSessionMgmtAppSessionId_match.Groups[1].Value);
                            postData3.Add("hidSessionId", hidSessionId_match.Groups[1].Value);
                            postData3.Add("txtSessionMgmtDisplayRows", "10");
                            postData3.Add("hidSessionMgmtNextIndex", "1");
                            postData3.Add("hidSessionMgmtNextPrevVal", "");
                            postData3.Add("hidSessionMgmtPreviousIndex", "0");


                            responseHTML = UploadDataReturnResponse(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/RemoveLock.jsp?acn=del&sid=" + hidSessionId_match.Groups[1].Value, postData3);
                            
                            
                            hidTokenLeadsCatalog_match = hidTokenLeadsCatalog_regex.Match(responseHTML);

                            if (hidTokenLeadsCatalog_match.Success)
                            {
                                responseHTML = DownloadData(baseUrl + "/corr_portal/LeadMgmt/Application/Lead/ListMyLeadsCatg.jsp?hidToken=" + hidTokenLeadsCatalog_match.Groups[1].Value);
                                Match svToken_match = svToken_regex.Match(responseHTML);

                                if (svToken_match.Success)
                                {
                                    NameValueCollection postData5 = new NameValueCollection();
                                    postData5.Add("hidToken", svToken_match.Groups[1].Value);
                                    postData5.Add("hidDirectory", "");
                                    postData5.Add("hidMenuItem", "");
                                    postData5.Add("hidMenuLevel", "0");
                                    postData5.Add("hidMenuPageName", "../../../LeadMgmt/Application/RateSheet/EditRateSheet.jsp?");
                                    postData5.Add("hidUnlockInd", "");
                                    postData5.Add("hidNumRoles", Convert.ToString(Convert.ToInt32(userId_match.Groups["userNumRoles"].Value) + 1));
                                    responseHTML = UploadDataReturnResponse(baseUrl + "/corr_portal/LeadMgmt/Application/RateSheet/EditRateSheet.jsp?", postData5);
                                    Match logoutToken_match = logoutToken_regex.Match(responseHTML);
                                    Match ratesheetID_match = ratesheetID_regex.Match(responseHTML);
                                    Match effectDtm_match = effectDtm_regex.Match(responseHTML);
                                    Match compID_match = compID_regex.Match(responseHTML);

                                    if (ratesheetID_match.Success && effectDtm_match.Success && compID_match.Success)
                                    {
                                        //ratesheet timestamp is blank or ratesheet has been updated
                                        if (dlinfo.LastRatesheetTimestamp < Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value))
                                        {
                                            dlUrl = baseUrl + "/corr_portal/servlet/DownloadFile?RateSheetId=" + ratesheetID_match.Groups[1].Value + "&format=" + rsFormat + "&filetype=EXCEL&effectDtm=" + effectDtm_match.Groups[1].Value + "&expireDtm=&compid=" + compID_match.Groups[1].Value + "&reporttype=BPRICE";
                                            RatesheetFileName = DownloadFile(dlUrl);
                                            GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);

                                            //updating timestamp should be last action before returning
                                            dlinfo.RecordLastRatesheetTimestamp(Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value));
                                            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                                        }
                                        else
                                        {
                                            //last effective time has not changed
                                            GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                                            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                                        }
                                    }
                                    else
                                    {
                                        LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                                        GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                                        return E_BotDownloadStatusT.Error;
                                    }
                                }
                                else
                                {
                                    LogErrorAndSendEmailToAdmin("LendingSpace Bot failed to find svToken for login " + dlinfo.Login);
                                    GoToUrl(baseUrl + logOutUrl);
                                    return E_BotDownloadStatusT.Error;
                                }
                            }
                            else
                            {
                                LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                                GoToUrl(baseUrl + logOutUrl);
                                return E_BotDownloadStatusT.Error;
                            }
                        }

                        else
                        {
                            LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                            GoToUrl(baseUrl + logOutUrl);
                            return E_BotDownloadStatusT.Error;
                        }


                    }

                    ////
                    //theres some initial pop-up but the login was successful (e.g. "password will expire in 8 days")
                    ////
                    else if (hidTokenInsertSession_match.Success)
                    {

                        responseHTML = DownloadData(baseUrl + stage + "/LsAuth/Application/LsLogin/Login/insertSession.jsp?hidToken=" + hidTokenInsertSession_match.Groups[1].Value);
                        hidTokenLeadsCatalog_match = hidTokenLeadsCatalog_regex.Match(responseHTML);

                        if (hidTokenLeadsCatalog_match.Success)
                        {


                            responseHTML = DownloadData(baseUrl + "/corr_portal/LeadMgmt/Application/Lead/ListMyLeadsCatg.jsp?hidToken=" + hidTokenLeadsCatalog_match.Groups[1].Value);
                            Match svToken_match = svToken_regex.Match(responseHTML);

                            if (svToken_match.Success)
                            {
                                NameValueCollection postData5 = new NameValueCollection();
                                postData5.Add("hidToken", svToken_match.Groups[1].Value);
                                postData5.Add("hidDirectory", "");
                                postData5.Add("hidMenuItem", "");
                                postData5.Add("hidMenuLevel", "0");
                                postData5.Add("hidMenuPageName", "../../../LeadMgmt/Application/RateSheet/EditRateSheet.jsp?");
                                postData5.Add("hidUnlockInd", "");
                                postData5.Add("hidNumRoles", Convert.ToString(Convert.ToInt32(userId_match.Groups["userNumRoles"].Value) + 1));

                                responseHTML = UploadDataReturnResponse(baseUrl + "/corr_portal/LeadMgmt/Application/RateSheet/EditRateSheet.jsp?", postData5);
                                Match logoutToken_match = logoutToken_regex.Match(responseHTML);
                                Match ratesheetID_match = ratesheetID_regex.Match(responseHTML);
                                Match effectDtm_match = effectDtm_regex.Match(responseHTML);
                                Match compID_match = compID_regex.Match(responseHTML);

                                if (ratesheetID_match.Success && effectDtm_match.Success && compID_match.Success)
                                {
                                    //ratesheet timestamp is blank or ratesheet has been updated
                                    if (dlinfo.LastRatesheetTimestamp < Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value))
                                    {
                                        dlUrl = baseUrl + "/corr_portal/servlet/DownloadFile?RateSheetId=" + ratesheetID_match.Groups[1].Value + "&format=" + rsFormat + "&filetype=EXCEL&effectDtm=" + effectDtm_match.Groups[1].Value + "&expireDtm=&compid=" + compID_match.Groups[1].Value + "&reporttype=BPRICE";
                                        RatesheetFileName = DownloadFile(dlUrl);
                                        GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);

                                        //updating timestamp should be last action before returning
                                        dlinfo.RecordLastRatesheetTimestamp(Convert.ToDateTime(effectDtm_match.Groups["effectDtm"].Value));
                                        return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                                    }
                                    else
                                    {
                                        //last effective time has not changed
                                        GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                                        return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                                    }
                                }
                                else
                                {
                                    LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                                    GoToUrl(baseUrl + logOutUrl + logoutToken_match.Groups[1].Value);
                                    return E_BotDownloadStatusT.Error;
                                }
                            }
                            else
                            {
                                LogErrorAndSendEmailToAdmin("LendingSpace Bot failed to find svToken for login " + dlinfo.Login);
                                GoToUrl(baseUrl + logOutUrl);
                                return E_BotDownloadStatusT.Error;
                            }
                        }
                        else
                        {
                            LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                            GoToUrl(baseUrl + logOutUrl);
                            return E_BotDownloadStatusT.Error;
                        }
                    }
                    else
                    {
                        LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                        GoToUrl(baseUrl + logOutUrl);
                        return E_BotDownloadStatusT.Error;
                    }
                }

                else
                {
                    LogErrorAndSendEmailToAdmin(genericErrorMessage + responseHTML);
                    GoToUrl(baseUrl + logOutUrl);
                    return E_BotDownloadStatusT.Error;
                }

            }

            else
            {
                LogErrorAndSendEmailToAdmin("LendingSpace bot failed to find a Token after initial login attempt for login " + dlinfo.Login + ". Login may have expired, please verify. HTML response; " + responseHTML);
                GoToUrl(baseUrl + logOutUrl);
                return E_BotDownloadStatusT.Error;
            }
      
     
        }
    }
}