﻿/// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class MGICBot : AbstractWebBot
    {
        private static string baseUrl = @"http://www.mgic.com/";
        private static string guidesUrl = baseUrl + @"underwriting/index.html";
        private static string ratesUrl = baseUrl + @"rates/rate-cards.html";
        private static string cuRatesUrl = @"http://www.cu.mgic.com/rates/rate_cards.html";

        private static Regex underwriting_regex = new Regex(@"Underwriting Guide \((?<uwDate>[^\)]+)\)", RegexOptions.IgnoreCase);
        private static Regex bpmiMonthly_regex = new Regex(@"href=""/pdfs/(?<bpmiMo>[^_]+)_bpmi_monthly.pdf"">", RegexOptions.IgnoreCase);
        private static Regex bpmiAnnual_regex = new Regex(@"hremf=""/pdfs/(?<bpmiAn>[^_]+)_bpmi_annuals.pdf"">", RegexOptions.IgnoreCase);
        private static Regex bpmiRefundSingle_regex = new Regex(@"href=""/pdfs/(?<bpmiRS>[^_]+)_bpmi_refnd_singles.pdf"">", RegexOptions.IgnoreCase);
        private static Regex bpmiNoRefundSingle_regex = new Regex(@"href=""/pdfs/(?<bpmiNoRS>[^_]+)_bpmi_noref_singles.pdf"">", RegexOptions.IgnoreCase);
        private static Regex bpmiSplits_regex = new Regex(@"href=""/pdfs/(?<bpmiSp>[^_]+)_natl_bpmi_splits.pdf"">", RegexOptions.IgnoreCase);
        private static Regex lpmiSingle_regex = new Regex(@"href=""/pdfs/(?<lpmiSi>[^_]+)_lpmi_singles.pdf"">", RegexOptions.IgnoreCase);
        private static Regex lpmiMonthly_regex = new Regex(@"href=""/pdfs/(?<lpmiMo>[^_]+)_lpmi_monthly.pdf"">", RegexOptions.IgnoreCase);
        private static Regex cuBpmiMonthly_regex = new Regex(@"href=""\.\./pdfs/(?<cubpmiMo>[^_]+)_cu_bpmi_monthly.pdf"" >", RegexOptions.IgnoreCase);
        private static Regex cuBpmiSingle_regex = new Regex(@"href=""\.\./pdfs/(?<cubpmiSi>[^_]+)_cu_bpmi_singles.pdf"" >", RegexOptions.IgnoreCase);
        private static Regex cuSplit_regex = new Regex(@"href=""http://www\.mgic\.com/pdfs/(?<cusplitP>[^_]+)_natl_bpmi_splits.pdf"" >", RegexOptions.IgnoreCase);
        private static Regex cuLPMISingle_regex = new Regex(@"href=""\.\./pdfs/(?<culpmiSi>[^_]+)_cu_lpmi_singles.pdf"" >", RegexOptions.IgnoreCase);
        private static Regex cuLPMIMonthly_regex = new Regex(@"href=""\.\./pdfs/(?<culpmiMo>[^_]+)_cu_lpmi_monthly.pdf"" >", RegexOptions.IgnoreCase);

        List<string> listOfChanges = new List<string>();
        List<string> listOfRegexError = new List<string>();

        public MGICBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "MGIC"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string temp = null;

            switch (dlinfo.Description)
            {
                case "MGIC_STD":
                    string guidesDate = @"05/01/13";
                    string bpmiMonthly = @"71-61210";
                    //string bpmiAnnuals = @"71-61211";
                    string bpmiRefundSingles = @"71-61212";
                    string bpmiNoRefundSingles = @"71-6768";
                    string bpmiSplits = @"71-61126";
                    string lpmiSingles = @"71-6799";
                    string lpmiMonthly = @"71-61213";

                    //Check Underwriting Guide date
                    temp = DownloadData(guidesUrl);
                    Match guidesMatch = underwriting_regex.Match(temp);
                    if (guidesMatch.Success)
                    {
                        if (!guidesMatch.Groups["uwDate"].Value.Equals(guidesDate))
                            listOfChanges.Add("Underwriting Guide");
                    }
                    else
                        listOfRegexError.Add("Underwriting Guide");

                    temp = DownloadData(ratesUrl);

                    //Check BPMI Monthly Premium
                    Match bpmiMonthlyMatch = bpmiMonthly_regex.Match(temp);
                    if (bpmiMonthlyMatch.Success)
                    {
                        if (!bpmiMonthlyMatch.Groups["bpmiMo"].Value.Equals(bpmiMonthly))
                            listOfChanges.Add("BPMI Monthly Premiums");
                    }
                    else
                        listOfRegexError.Add("BPMI Monthly Premiums");

                    //Check BPMI Annual Premium. Removed base eopm 419516
                    /*Match bpmiAnnualMatch = bpmiAnnual_regex.Match(temp);
                    if (bpmiAnnualMatch.Success)
                    {
                        if (!bpmiAnnualMatch.Groups["bpmiAn"].Value.Equals(bpmiAnnuals))
                            listOfChanges.Add("BPMI Annual Premiums");
                    }
                    else
                        listOfRegexError.Add("BPMI Annual Premiums");*/

                    //Check BPMI Refundable Single Premium
                    Match bpmiRefSingleMatch = bpmiRefundSingle_regex.Match(temp);
                    if (bpmiRefSingleMatch.Success)
                    {
                        if (!bpmiRefSingleMatch.Groups["bpmiRS"].Value.Equals(bpmiRefundSingles))
                            listOfChanges.Add("BPMI Refundable Single Premium");
                    }
                    else
                        listOfRegexError.Add("BPMI Refundable Single Premium");

                    //Check BPMI Non-Refundable Single Premium
                    Match bpmiNonRefundSingleMatch = bpmiNoRefundSingle_regex.Match(temp);
                    if (bpmiNonRefundSingleMatch.Success)
                    {
                        if (!bpmiNonRefundSingleMatch.Groups["bpmiNoRS"].Value.Equals(bpmiNoRefundSingles))
                            listOfChanges.Add("BPMI Non-Refundable Single Premium");
                    }
                    else
                        listOfRegexError.Add("BPMI Non-Refundable Single Premium");

                    //Check BPMI Split Premium
                    Match bpmiSplitMatch = bpmiSplits_regex.Match(temp);
                    if (bpmiSplitMatch.Success)
                    {
                        if (!bpmiSplitMatch.Groups["bpmiSp"].Value.Equals(bpmiSplits))
                            listOfChanges.Add("BPMI Split Premium");
                    }
                    else
                        listOfRegexError.Add("BPMI Split Premium");

                    //Check LPMI Single Premium
                    Match lpmiSingleMatch = lpmiSingle_regex.Match(temp);
                    if (lpmiSingleMatch.Success)
                    {
                        if (!lpmiSingleMatch.Groups["lpmiSi"].Value.Equals(lpmiSingles))
                            listOfChanges.Add("LPMI Single Premium");
                    }
                    else
                        listOfRegexError.Add("LPMI Single Premium");

                    //Check LPMI Monthly Premium
                    Match lpmiMonthlyMatch = lpmiMonthly_regex.Match(temp);
                    if (lpmiMonthlyMatch.Success)
                    {
                        if (!lpmiMonthlyMatch.Groups["lpmiMo"].Value.Equals(lpmiMonthly))
                            listOfChanges.Add("LPMI Monthly Premium");
                    }
                    else
                        listOfRegexError.Add("LPMI Monthly Premium");
                    break;

                case "MGIC_CU":
                    string cuBPMIMo = @"71-61201";
                    string cuBPMISi = @"71-61202";
                    string cuSplit = @"71-61126";
                    string cuLPMISi = @"71-61193";
                    string cuLPMIMo = @"71-61215";

                    temp = DownloadData(cuRatesUrl);

                    //Check Credit Union BPMI Monthly
                    Match cuBPMIMonthlyMatch = cuBpmiMonthly_regex.Match(temp);
                    if (cuBPMIMonthlyMatch.Success)
                    {
                        if (!cuBPMIMonthlyMatch.Groups["cubpmiMo"].Value.Equals(cuBPMIMo))
                            listOfChanges.Add("CU BPMI Monthlies");
                    }
                    else
                        listOfRegexError.Add("CU BPMI Monthlies");

                    //Check Credit Union BPMI Singles
                    Match cuBPMISingleMatch = cuBpmiSingle_regex.Match(temp);
                    if (cuBPMISingleMatch.Success)
                    {
                        if (!cuBPMISingleMatch.Groups["cubpmiSi"].Value.Equals(cuBPMISi))
                            listOfChanges.Add("CU BPMI Singles");
                    }
                    else
                        listOfRegexError.Add("CU BPMI Singles");

                    //Check Credit Union Split Premium
                    Match cuSplitMatch = cuSplit_regex.Match(temp);
                    if (cuSplitMatch.Success)
                    {
                        if (!cuSplitMatch.Groups["cusplitP"].Value.Equals(cuSplit))
                            listOfChanges.Add("CU Split Premiums");
                    }
                    else
                        listOfRegexError.Add("CU Split Premiums");

                    //Check Credit Union LPMI Singles
                    Match cuLPMISingleMatch = cuLPMISingle_regex.Match(temp);
                    if (cuLPMISingleMatch.Success)
                    {
                        if (!cuLPMISingleMatch.Groups["culpmiSi"].Value.Equals(cuLPMISi))
                            listOfChanges.Add("CU LPMI Singles");
                    }
                    else
                        listOfRegexError.Add("CU LPMI Singles");

                    //Check Credit Union LPMI Monthly
                    Match cuLPMIMonthlyMatch = cuLPMIMonthly_regex.Match(temp);
                    if (cuLPMIMonthlyMatch.Success)
                    {
                        if (!cuLPMIMonthlyMatch.Groups["culpmiMo"].Value.Equals(cuLPMIMo))
                            listOfChanges.Add("CU LPMI Monthlies");
                    }
                    else
                        listOfRegexError.Add("CU LPMI Monthlies");
                    break;

                default:
                    LogErrorAndSendEmailToAdmin("Incorrect MGIC Bot description. Please set the correct description. It's either MGIC_STD or MGIC_CU");
                    break;
            }

            if (listOfChanges.Count != 0)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine("MGIC Bot detect changes on the following guidelines date/ rates version number. Please check the changes and update the bot, templates, and/or map: ");
                foreach (string change in listOfChanges)
                    errorMessage.AppendLine(change);
                LogErrorAndSendEmailToAdmin(errorMessage.ToString());
                return E_BotDownloadStatusT.Error;
            }
            else if (listOfRegexError.Count != 0)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine("MGIC Bot detect changes on the following guidelines/rates regex. Please check the changes and update the bot: ");
                foreach (string regexError in listOfRegexError)
                    errorMessage.AppendLine(regexError);
                LogErrorAndSendEmailToAdmin(errorMessage.ToString());
                return E_BotDownloadStatusT.Error;
            }

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
    }
}


