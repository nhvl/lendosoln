﻿// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Threading;
using System.Collections.Generic;

namespace LpeUpdateBotLib.WebBots
{
    public class CaliberBot : AbstractWebBot
    {
        private static string baseUrl = @"https://calibercl.com";
        private static string loginUrl = baseUrl + @"/default/login";
        private static string rsUrl = baseUrl + @"/RateSheet";

        public CaliberBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "CALIBER"; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }
        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }
        protected override string DefaultAcceptLanguage 
        { 
            get 
            { 
                return "en-US"; 
            }
        }
        protected override string BotUserAgentString
        {
            get
            {
                return "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0)";
            }
        }
        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // if its before 6:30am or after 7pm, we're not gonna download the RS. 
            if ((DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(6).AddMinutes(30)) < 0) || (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(19)) > 0))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            // if its Saturday or Sunday, we're not gonna download the RS. 
            if ((DateTime.Now.DayOfWeek == DayOfWeek.Saturday) || (DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            // this exception handler was inserted to catch timeout errors upon navigating to the login portal URL, so that we don't expire pricing. eOPM 1278565 [BB]
            try
            {
                GoToUrl(baseUrl);
            }
            catch (WebException)
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            string fileName = null;

            NameValueCollection postData = new NameValueCollection();
            postData.Add("UserName", dlinfo.Login);
            postData.Add("Password", dlinfo.Password);

            // this exception handler was inserted to catch timeout errors upon logging in, so that we don't expire pricing. eOPM 1278565 [BB]
            try
            {
                UploadData(loginUrl, postData);
            }
            catch (WebException)
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            string temp = DownloadData(rsUrl);

            //Check if Caliber has posted today's ratesheet or not
            string todayRs = @"Rate\s+Sheet\s+Publications\s+for\s+&nbsp;\s+" + DateTime.Today.ToString("dddd, MMMM d, yyyy");
            Regex todayRs_regex = new Regex(todayRs, RegexOptions.IgnoreCase);
            Match todayRs_match = todayRs_regex.Match(temp);
            if (todayRs_match.Success)
            {
                if (dlinfo.Description.StartsWith("CALIBER_PDF"))
                {
                    Regex pdfRs_regex = new Regex(@"\<a\s+class=""PDFImage""\s+href=""(?<pdfRS>[^""]+)""\s+target=""_blank""\>View\s+PDF\s+Document", RegexOptions.IgnoreCase);
                    Match pdfRs_match = pdfRs_regex.Match(temp);
                    if (pdfRs_match.Success)
                        fileName = DownloadFile(baseUrl + pdfRs_match.Groups["pdfRS"].Value);
                    else
                        LogErrorAndSendEmailToAdmin("Caliber Bot failed to find PDF RS for login: " + dlinfo.Login + ". Content: " + temp);
                }
                else if (dlinfo.Description.StartsWith("CALIBER_CSV"))
                {
                    Regex csvRs_regex = new Regex(@"\<a\s+class=""ExcelImage""\s+href=""(?<csvRS>[^""]+)""\s+target=""_blank""\>View\s+CSV\s+Document", RegexOptions.IgnoreCase);
                    Match csvRs_match = csvRs_regex.Match(temp);
                    if (csvRs_match.Success)
                    {
                        fileName = DownloadFile(baseUrl + csvRs_match.Groups["csvRS"].Value);

                        /* this section adds an extra row in between each rate stack so that we can use DynamicListBottom */

                        String path = fileName;
                        List<String> lines = new List<String>();

                        if (File.Exists(path))
                        {
                            using (StreamReader reader = new StreamReader(path))
                            {
                                String line;
                                String[] temp_split = new String[9];

                                // loop thru all lines in the rate sheet file we downloaded
                                while ((line = reader.ReadLine()) != null)
                                {
                                    // if this line contains a comma
                                    if (line.Contains(","))
                                    {
                                        // split the line into an array of strings
                                        String[] split = line.Split(',');
                                        
                                        // if the 2nd value of the current line does not match the 2nd value of the previous line
                                        if (!(split[1].Equals(temp_split[1])))
                                        {
                                            // add a blank row
                                            line = "";
                                            lines.Add(line);
                                        }

                                        // set the previous line equal to the current line for next iteration
                                        temp_split = split;
                                        // convert the string array into one csv line
                                        line = String.Join(",", temp_split);
                                    }
                                    // add the line that we have read in
                                    lines.Add(line);
                                }
                            }

                            // overwrite the rate sheet file so that it includes that newly added blank rows
                            using (StreamWriter writer = new StreamWriter(path, false))
                            {
                                foreach (String line in lines)
                                    writer.WriteLine(line);
                            }
                        }
                    }
                    else
                        LogErrorAndSendEmailToAdmin("Caliber Bot failed to find CSV RS for login: " + dlinfo.Login + ". Content: " + temp);
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("DEACTIVATED. Download List entry's Description is invalid for Caliber Bot. Login: " + dlinfo.Login);
                    dlinfo.Deactivate();
                }
            }
            else
                LogErrorAndSendEmailToAdmin("Caliber Bot can't find today's ratesheet for login " + dlinfo.Login + ". Please check login. Content: " + temp);

            if (fileName != null)
            {
                RatesheetFileName = fileName; // text0
                GoToUrl(baseUrl + @"/Default/Logout");
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}