// Author Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class CitiBot : AbstractWebBot
    {
        private static string baseUrl = "https://correspondent.citimortgage.com/";
        private static string loginUrl = baseUrl + "Correspondent/login.do";
        private static string rsUrl = baseUrl + "Correspondent/ViewFirstRates.zo?latest=true&mime=XLS";
        private static Regex deviceId_regex = new Regex(@"name=""device_id""\s+value=""(?<deviceId>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex failedLogin_regex = new Regex(@"Sign In to Your Account", RegexOptions.IgnoreCase);

        public CitiBot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "CITIBOT"; }
        }
        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                //return @"text/html, application/xhtml+xml, */*";
                return @"text/html; charset=ISO-8859-1";
            }
        }

        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }

        protected override string DefaultAcceptLanguage
        {
            get
            {
                return @"en-US";
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            try
            {
                //YS: Commenting these Security protocol settings out on purpose, as it's actually causing other bots to fail.
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
                string temp = DownloadData(baseUrl + "Correspondent/login.jsp");
                Match deviceId_match = deviceId_regex.Match(temp);
                if (deviceId_match.Success)
                {
                    NameValueCollection postData = new NameValueCollection();
                    postData.Add("device_id", deviceId_match.Groups["deviceId"].Value);
                    postData.Add("fp_browser", @"mozilla/5.0 (compatible; msie 9.0; windows nt 6.1; trident/5.0; slcc2; .net clr 2.0.50727; .net clr 3.5.30729; .net clr 3.0.30729; .net4.0c; .net4.0e; .net clr 1.1.4322)|5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E; .NET CLR 1.1.4322)|Win32|0|x86|en-us|16717");
                    postData.Add("fp_display", @"24|1600|900|860|1600");
                    postData.Add("fp_lang", @"lang=en-us|syslang=en-us|userlang=en-us");
                    postData.Add("fp_software", @"abk=6,1,7601,17514|wnt=6,1,7601,18952|dht=9,0,8112,16421|ie5=9,0,8112,16421|ieh=9,0,8112,16421|iee=6,1,7601,16978|wmp=12,0,7601,18840|obp=9,0,8112,16421|oex=6,1,7601,17514");
                    postData.Add("fp_syslang", "en-us");
                    postData.Add("fp_timezone", "-8");
                    postData.Add("fp_userlang", "en-us");
                    postData.Add("fp_fingerPrint", @"version=6.5&pm_fpua=mozilla/5.0 (compatible; msie 9.0; windows nt 6.1; trident/5.0; slcc2; .net clr 2.0.50727; .net clr 3.5.30729; .net clr 3.0.30729; .net4.0c; .net4.0e; .net clr 1.1.4322)|5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E; .NET CLR 1.1.4322)|Win32|0|x86|en-us|16717&pm_fpsc=24|1600|900|860|1600&pm_fpsw=abk=6,1,7601,17514|wnt=6,1,7601,18952|dht=9,0,8112,16421|ie5=9,0,8112,16421|ieh=9,0,8112,16421|iee=6,1,7601,16978|wmp=12,0,7601,18840|obp=9,0,8112,16421|oex=6,1,7601,17514&pm_fptz=-8&pm_fpln=lang=en-us|syslang=en-us|userlang=en-us");
                    postData.Add("LOGIN_NAME", dlinfo.Login);
                    postData.Add("PASSWORD", dlinfo.Password);

                    UploadData(loginUrl, postData);

                    temp = DownloadData(@"https://correspondent.citimortgage.com/Correspondent/home.jsp");
                    Match failedLogin_match = failedLogin_regex.Match(temp);

                    if (failedLogin_match.Success)
                    {
                        //Citi bot should not deactive at this point
                        LogErrorAndSendEmailToAdmin("CitiBot failed to login. Please check login/password for user name: " + dlinfo.Login + ". Content: " + temp);
                        GoToUrl(baseUrl + "Correspondent/logout.do");
                        return E_BotDownloadStatusT.Error;

                    }
                    else
                    {
                        GoToUrl(baseUrl + "Correspondent/rates/control.jsp");
                        RatesheetFileName = DownloadFile(rsUrl);
                        GoToUrl(baseUrl + "Correspondent/logout.do");
                        return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                    }
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("CitiBot failed to find deviceId for login: " + dlinfo.Login + ". Content: " + temp);
                    return E_BotDownloadStatusT.Error;
                }
            }
            finally
            {
                GoToUrl(baseUrl + "Correspondent/logout.do");
            }
        }
    }
}