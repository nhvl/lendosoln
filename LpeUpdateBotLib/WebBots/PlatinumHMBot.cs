﻿//Author: Budi Sulayman

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class PlatinumHMBot : AbstractWebBot
    {
        private static string baseUrl = @"http://www.platinumcorrespondent.com";
        private static string loginUrl = baseUrl + @"/dotnetprotect/Admin/login.aspx?page=http://www.platinumcorrespondent.com/rates.aspx";
        private static string loginUrl1 = baseUrl + @"/dotnetprotect/Admin/login.aspx?page=http%3a%2f%2fwww.platinumcorrespondent.com%2frates.aspx";
        private static string ratesUrl = baseUrl + @"/rates.aspx";
        private static string thisMonth = DateTime.Now.ToString("MMMM");
        private static string prevMonth = DateTime.Now.AddMonths(-1).ToString("MMMM");
        private Regex thisMonthRsUrl_regex = new Regex(thisMonth + @"\s+" + DateTime.Today.Year + @"</span></strong></span></span></td>\s+</tr>\s+</tbody>\s+</table>\s+<p\s+style=""[^""]+"">\s+<a href=""(?<thisMonthRsUrl>[^""]+)""\s+target=""_blank"">Platinum_Home_Mortgage_Correspondent_Rates", RegexOptions.IgnoreCase);
        private Regex thisMonthRepriceUrl_regex = new Regex(thisMonth + @"\s+" + DateTime.Today.Year + @"</span></strong></span></span></td>\s+</tr>\s+</tbody>\s+</table>\s+<h3\s+style=""[^""]+"">\s+<span\s+style=""[^""]+"">Reprice\s+Effective\s+([0-9:]+)\s+(AM|PM)\s+([A-Z]+)</span></h3>\s+<p\s+style=""[^""]+"">\s+<a href=""(?<thisMonthRepriceRsUrl>[^""]+)""\s+target=""_blank"">Platinum_Home_Mortgage_Correspondent_Rates", RegexOptions.IgnoreCase);
        private Regex prevMonthRsUrl_regex = new Regex(prevMonth + @"\s+" + DateTime.Today.Year + @"</span></strong></span></span></td>\s+</tr>\s+</tbody>\s+</table>\s+<p\s+style=""[^""]+"">\s+<a href=""(?<prevMonthRsUrl>[^""]+)""\s+target=""_blank"">Platinum_Home_Mortgage_Correspondent_Rates", RegexOptions.IgnoreCase);
        private Regex prevMonthRepriceUrl_regex = new Regex(prevMonth + @"\s+" + DateTime.Today.Year + @"</span></strong></span></span></td>\s+</tr>\s+</tbody>\s+</table>\s+<h3\s+style=""[^""]+"">\s+<span\s+style=""[^""]+"">Reprice\s+Effective\s+([0-9:]+)\s+(AM|PM)\s+([A-Z]+)</span></h3>\s+<p\s+style=""[^""]+"">\s+<a href=""(?<prevMonthRepriceRsUrl>[^""]+)""\s+target=""_blank"">Platinum_Home_Mortgage_Correspondent_Rates", RegexOptions.IgnoreCase);
        private static Regex viewState_regex = new Regex(@"<input type=""hidden"" name=""__VIEWSTATE"" id=""__VIEWSTATE"" value=""(?<viewState>[^""]+)""", RegexOptions.IgnoreCase);
        //DateTime.Today.Month.ToString()

        public PlatinumHMBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "PLATINUMHM"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null;
            string temp = DownloadData(loginUrl);
            Match viewState_match = viewState_regex.Match(temp);

            if (viewState_match.Success)
            {
                NameValueCollection postData = new NameValueCollection();
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", viewState_match.Groups["viewState"].Value);
                postData.Add("ctl00$ContentPlaceHolder1$tbUserName", dlinfo.Login);
                postData.Add("ctl00$ContentPlaceHolder1$tbPassword", dlinfo.Password);
                postData.Add("ctl00$ContentPlaceHolder1$btnLogin", "Login");

                temp = UploadDataReturnResponse(loginUrl1, postData);

                //Looking for today ratesheet
                Match thisMonthRsUrl_match = thisMonthRsUrl_regex.Match(temp);
                Match thisMonthRepriceUrl_match = thisMonthRepriceUrl_regex.Match(temp);

                //The reason to check previous month because sometimes at the beginning of the week, Platinum will post their new ratesheet under previous month category
                Match prevMonthRsUrl_match = prevMonthRsUrl_regex.Match(temp);
                Match prevMonthRepriceUrl_match = prevMonthRepriceUrl_regex.Match(temp);

                if (thisMonthRsUrl_match.Success)
                    fileName = DownloadFile(baseUrl + thisMonthRsUrl_match.Groups["thisMonthRsUrl"].Value);
                else if (thisMonthRepriceUrl_match.Success)
                    fileName = DownloadFile(baseUrl + thisMonthRepriceUrl_match.Groups["thisMonthRepriceRsUrl"].Value);
                else if (prevMonthRsUrl_match.Success)
                    fileName = DownloadFile(baseUrl + prevMonthRsUrl_match.Groups["prevMonthRsUrl"].Value);
                else if (prevMonthRepriceUrl_match.Success)
                    fileName = DownloadFile(baseUrl + prevMonthRepriceUrl_match.Groups["prevMonthRepriceRsUrl"].Value);
                else
                    LogErrorAndSendEmailToAdmin("PlatinumHMBot failed to find today ratesheet for login: " + dlinfo.Login + ". Content: " + temp);

                postData = new NameValueCollection();
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", "");
                postData.Add("ctl00$LogoutButton", "Logout");

                UploadData(ratesUrl, postData);
            }
            else
                LogErrorAndSendEmailToAdmin("PlatinumHMBot failed to get viewState value for login: " + dlinfo.Login + ". Content: " + temp);

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}