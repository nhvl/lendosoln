﻿//Kelvin Young
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class EverBankBulletinBot : AbstractWebBot
    {
        const string baseUrl = @"https://www.everbankwholesale.com/EverBankLOS/";
        const string loginUrl = baseUrl + @"ProcessHomeLogon.do";
        const string bulletinUrl = baseUrl + @"FileInternalList.do?grpType=Announcements";
        const string logoutUrl = baseUrl + @"Logout.do";

        /********** UPDATE REGEX BULLETIN NUMBER HERE **********/
        private static string bulletinNumber = "2012-28"; //Bulletin number one higher than current one
        /********** UPDATE REGEX BULLETIN NUMBER HERE **********/

        private static Regex bulletin_regex = new Regex(bulletinNumber, RegexOptions.IgnoreCase); //Regex checks bulletin number

        public EverBankBulletinBot(IDownloadInfo info)
            : base(info)
        { 
        }

        public override string BotName
        {
            get { return "EVERBANKBULLETIN"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string loginResponse = null;
            string pageInfo = null;

            GoToUrl(baseUrl);

            //Login into website
            NameValueCollection loginInfo = new NameValueCollection();
            loginInfo.Add("username", dlinfo.Login);
            loginInfo.Add("password", dlinfo.Password);
            loginInfo.Add("submitButton", "");
            loginInfo.Add("SUBMIT.x", "38");
            loginInfo.Add("SUBMIT.y", "16");
            loginInfo.Add("SUBMIT", "Submit");

            loginResponse = UploadDataReturnResponse(loginUrl, loginInfo);

            //Go to website and store page info into string
            pageInfo = DownloadData(bulletinUrl);

            //Check for new bulletin
            Match match = bulletin_regex.Match(pageInfo);
            if (match.Success)
            {
                LogErrorAndSendEmailToAdmin(string.Format("A new EverBank bulletin (" + bulletinNumber + ") has been posted. Please verify and update bot.", pageInfo));
            }

            //Logout
            GoToUrl(logoutUrl);

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
    }
}