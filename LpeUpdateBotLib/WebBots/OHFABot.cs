//Author: Britton Barmeyer


using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class OHFABot : AbstractWebBot
    {

        public OHFABot(IDownloadInfo info) : base(info)
        {
        }

        public override string BotName
        {
            get { return "OHFA"; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }

        private string baseUrl = @"https://www.ok.gov";
        
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {

            string temp = DownloadData(baseUrl + "/ohfa/Lenders/Lender_Resources/index.html");

            string rsRegexString = @"href=""(?<rsUrl>[^""]+)""><[^>]+><[^>]+>Current Interest Rates";
            Regex ratesheet_regex = new Regex(rsRegexString, RegexOptions.IgnoreCase);

            Match match = ratesheet_regex.Match(temp);

            if (match.Success)
            {
                RatesheetFileName = DownloadFile(baseUrl + match.Groups["rsUrl"].Value);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("OHFABot can't find today's ratesheet. Please check and verify. Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }

        }
    }
}