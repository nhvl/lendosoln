// Author: Budi Sulayman
namespace LpeUpdateBotLib.WebBots
{
    using System;
    using System.Collections.Specialized;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    //using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;

    public class ChaseBot : AbstractWebBot
    {
        const string MAIN_URL = "https://chaseloanmanager.chase.com/ChaselockRobot/Service.aspx?Type=RATE";
        const string TIME_URL = "https://www.chaseb2b.com/content/portal/javascript/soap.js";
    

        //this regex is only to look at the script Chase uses to populate the "last updated" time on their website
        private static Regex RateCodeStartStr_regex = new Regex(@"this.RateCodeStartStr='(?<ratecodestartstr>[^']+)'", RegexOptions.IgnoreCase);

        private static Regex VIEWSTATE_regex = new Regex(@"id=""__VIEWSTATE""\s+value=""(?<viewstate>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex EVENTVALIDATION_regex = new Regex(@"id=""__EVENTVALIDATION""\s+value=""(?<eventvalidation>[^""]+)""", RegexOptions.IgnoreCase);

        public ChaseBot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "CHASE"; }
        }

        protected override bool WebRequestKeepAlive
        {
            get { return true; }
        }

        protected override string BotUserAgentString
        {
            get
            {
                return "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko/20100101 Firefox/22.0";
            }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }

        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //check time
            string lastUpdateTimeScript = DownloadData(TIME_URL);
            Match RateCodeStartStr_match = RateCodeStartStr_regex.Match(lastUpdateTimeScript);

            bool bypassTimeCheck = dlinfo.Description.Contains("BYPASS_TIME_CHECK");

            if (RateCodeStartStr_match.Success || bypassTimeCheck)
            {

                //if time>last time or is blank 
                if ( (dlinfo.LastRatesheetTimestamp < Convert.ToDateTime(RateCodeStartStr_match.Groups["ratecodestartstr"].Value)) || bypassTimeCheck)
                {
                    //BEGIN DOWNLOAD
                    //updated for new "robot" site

                    string temp = DownloadData(MAIN_URL);
                    Match viewstate_match = VIEWSTATE_regex.Match(temp);
                    Match eventvalidation_match = EVENTVALIDATION_regex.Match(temp);

                    NameValueCollection postData = new NameValueCollection();
                    postData.Add("__EVENTTARGET", "");
                    postData.Add("__EVENTARGUMENT", "");
                    postData.Add("__VIEWSTATE", viewstate_match.Groups["viewstate"].Value);
                    postData.Add("__EVENTVALIDATION", eventvalidation_match.Groups["eventvalidation"].Value);
                    postData.Add("txtUser", dlinfo.Login);
                    postData.Add("txtPassword", dlinfo.Password);
                    postData.Add("buttonLogOn", "Submit");

                    string fileName = UploadDataDownloadFile(MAIN_URL, postData);
                    bool bypassFileCheck = dlinfo.Description.Contains("BYPASS_FILE_CHECK");

                    if (fileName != null)
                    {
                        //Occasionally Chase will spit back an error instead of a ratesheet
                        //we dont want to continue at this point if that happens, so check if file header is 0xD0CF11E0A1B11AE1
                        //use bypassFileCheck if Chase updates file header

                        if (IsExcelFile(fileName) || bypassFileCheck)
                        {
                            RatesheetFileName = fileName;
                            //AFTER saving ratesheet update the timestamp
                            dlinfo.RecordLastRatesheetTimestamp(Convert.ToDateTime(RateCodeStartStr_match.Groups["ratecodestartstr"].Value));
                            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                        }

                        else
                        {
                            LogErrorAndSendEmailToAdmin("Chase bot pulled a file which had an incorrect type, please verify if action is needed. Login: " + dlinfo.Login + ". FileName: " + fileName);
                            return E_BotDownloadStatusT.Error;
                        }


                }
                    else
                    {
                        LogErrorAndSendEmailToAdmin("Chase bot failed to login and download the ratesheet. Login: " + dlinfo.Login);
                        return E_BotDownloadStatusT.Error;
                    }
                    //END DOWNLOAD

                }
                //else posted time has not changed
                else
                {
                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                }

            }

            //else error in finding ratesheet time
            else
            {
                LogErrorAndSendEmailToAdmin("Chase bot failed to find Last Updated Time. Login: " + dlinfo.Login);
                return E_BotDownloadStatusT.Error;
            }

        }


        /// <summary>
        /// below will check if the downloaded file is Excel, (chase uses older header)
        /// </summary>

        private bool IsExcelFile(string sFileName)
        {
            byte[] buf = new byte[m_ExcelBinaryPrefix.Length];
            bool isExcel = true;
            using (System.IO.FileStream fs = System.IO.File.Open(sFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                fs.Read(buf, 0, buf.Length);

                for (int i = 0; i < buf.Length; i++)
                {
                    if (m_ExcelBinaryPrefix[i] != buf[i])
                    {
                        isExcel = false;
                        break;
                    }
                }
            }

            return isExcel;
/*
            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream fs)
            {
                fs.Stream.Read(buf, 0, buf.Length);

                for (int i = 0; i < buf.Length; i++)
                {
                    if (m_ExcelBinaryPrefix[i] != buf[i])
                    {
                        isExcel = false;
                        break;
                    }
                }
            };

            BinaryFileHelper.OpenRead(sFileName, readHandler);
            return isExcel;
*/
        }

        private byte[] m_ExcelBinaryPrefix = { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 };
    }
}