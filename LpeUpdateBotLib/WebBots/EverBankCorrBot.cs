﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    class EverBankCorrBot : AbstractWebBot
    {
        public EverBankCorrBot(IDownloadInfo info) : base(info)
        { 
        }
        public override string BotName
        {
            get { return "EVERBANKCORR"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
            string fileName = null;
            fileName = DownloadFile(dlinfo.Url);
            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}
