/// Author: David Dao

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    /// <summary>
    /// CountryWide SubPrime bot
    /// </summary>
    public class CWSPBot : AbstractWebBot
    {
        private static Regex s_regEx = new Regex(@"cld/Downloads/Subprime/[A-Za-z0-9_]+\.xls", RegexOptions.Compiled);

        private const string LOGIN_URL = "https://cldlogin.countrywide.com/cld/login";
        private const string LOGOUT_URL = "https://cld.countrywide.com/login/loggedout.asp";
        private const string RS_URL = "https://cld.countrywide.com/cld/priceguide/SubprimeRateSheet.asp?nav=y";

        public CWSPBot(IDownloadInfo info) : base(info)
        {
        }
        public override string BotName 
        {
            get { return "CWSP"; }
        }

        protected override string DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            // Steps to download ratesheet
            //    - Login
            //    - Hit the rs_url.
            //    - Search the content for xls link.
            //    - Download the xls file
            //    - Logout
            string sFileName = null;

            NameValueCollection postData = new NameValueCollection();
            postData.Add("usr_name", dlinfo.Login);
            postData.Add("usr_password", dlinfo.Password);

            string html = UploadDataReturnResponse(LOGIN_URL, postData);
            if (html.IndexOf("/cld/login") > 0) 
            {
                dlinfo.TurnOffCWDL();
                string sErrorMsg = "(CWSPBot) Account login is invalid. UserName=" + dlinfo.Login;
                ReportDeactivation(sErrorMsg);
                return null;
            }

            html = DownloadData(RS_URL);

            Match match = s_regEx.Match(html);
            if (match.Success) 
            {
                string url = dlinfo.Url + match.Value;
                sFileName = DownloadFile(url);
            } 
            else 
            {
                string sErrorMsg = "(CWSPBot) Unable to locate xls rate sheet. HTML=" + html;
                LogErrorAndSendEmailToAdmin(sErrorMsg);
                return null;

            }

            GoToUrl(LOGOUT_URL);

            return sFileName;



        }
    }
}
