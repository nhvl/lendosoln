﻿//Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

using LpeUpdateBotLib.Common;
namespace LpeUpdateBotLib.WebBots
{
    public class QuickenBot : AbstractWebBot
    {
        private static string baseUrl = @"https://portal.qlmortgageservices.com";
        private static string loginUrl = baseUrl + "/sign-in";
        private static string logOutUrl = baseUrl + "/sign-out";

        public QuickenBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "QUICKEN"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            NameValueCollection postData = new NameValueCollection();
            postData.Add("signinname", dlinfo.Login);
            postData.Add("password", dlinfo.Password);
            postData.Add("submit.x", "52");
            postData.Add("submit.y", "21");

            UploadData(loginUrl, postData);

            GoToUrl(baseUrl + "/rate-table");
            int flag = 0;
            string temp = DownloadData(baseUrl + "/pricing/rate-sheet?channel=correspondent");

            string rsRegexString = @"href=""(?<rsUrl>[^""]+)"">[^=]+=""td_two"">XLS</td>\s+<td class=""td_three"">" +DateTime.Today.Date.ToString("MMM dd, yyyy");
            Regex ratesheet_regex = new Regex(rsRegexString, RegexOptions.IgnoreCase);

            Match match = ratesheet_regex.Match(temp);
            while ((!match.Success) && (flag < 3))
            {
                Thread.Sleep(5000);
                temp = DownloadData(baseUrl + "/pricing/rate-sheet?channel=correspondent");
                match = ratesheet_regex.Match(temp);
                flag++;
            }

            if (match.Success)
            {
                RatesheetFileName = DownloadFile(baseUrl + match.Groups["rsUrl"].Value);
                GoToUrl(logOutUrl);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("QuickenBot can't find today ratesheet. Please check and verify. Content: " + temp);
                GoToUrl(logOutUrl);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}