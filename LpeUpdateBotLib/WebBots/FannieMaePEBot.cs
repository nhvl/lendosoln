﻿/// Author: Budi Sulayman

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class FannieMaePEBot : AbstractWebBot
    {
        private string pageInfo, loginUrl, findcredentials, response, dlURL = null;

        private static string peUrl = "https://pe.fanniemae.com/pe";
        //private static string peUrl = "https://pe-demo.fanniemae.com/pe"; //Demo Environment URL
        private static string baseUrl = "https://fmsso.fanniemae.com";
        //private static string baseUrl = "https://fmsso-acpt.fanniemae.com"; //Demo Environment URL
        private static string ssoUrl = "https://fmsso.fanniemae.com/idp/startSSO.ping?PartnerSpId=sso-p2p-BH0-p1-saml-tc-PEWholeLoan&type=formloginonly&TargetResource=https://pe.fanniemae.com/pe";
        //private static string ssoUrl = "https://fmsso-acpt.fanniemae.com/idp/startSSO.ping?PartnerSpId=sso-p2x-BH0-a9-saml-tc-PEWholeLoan&type=formloginonly&TargetResource=https://pe-demo.fanniemae.com/pe"; //Demo Environment URL
        private static string essoPostUrl = "https://pe.fanniemae.com/pe/essoPost";
        //private static string essoPostUrl = "https://pe-demo.fanniemae.com/pe/essoPost"; //Demo Environment URL
        private static string institutionUrl = "https://pe.fanniemae.com/pe/select-institution/";
        //private static string institutionUrl = "https://pe-demo.fanniemae.com/pe/select-institution/"; //Demo Environment URL
        private static string logoutUrl = "https://pe.fanniemae.com/pe/logout";
        //private static string logoutUrl = "https://pe-demo.fanniemae.com/pe/logout"; //Demo Environment URL

        private static Regex SAMLResponse_regex = new Regex(@"NAME=""SAMLResponse"" VALUE=""(?<SAMLResponse>[^\"" >]+)""", RegexOptions.IgnoreCase);
        private static Regex RelayState_regex = new Regex(@"NAME=""RelayState"" VALUE=""(?<RelayState>[^\"" >]+)""", RegexOptions.IgnoreCase);
        private static Regex LoginUrl_regex = new Regex(@"form method=""POST"" action=""(?<LoginUrl>[^\"" >]+)""", RegexOptions.IgnoreCase);

        public FannieMaePEBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "FANNIEMAEPE"; }
        }

        public override long TimeoutInMs
        {
            get
            {
                return 1000000;
            }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            }
        }

        protected override string DefaultAcceptLanguage
        {
            get
            {
                return "en-US,en;q=0.9";
            }
        }

        protected override string BotUserAgentString
        {
            get
            {
                return "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36";
            }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //split dlinfo.login to 2 userId with splitter '/'. First one is user id for login and the second one is branch number to access ratesheet
            string[] userId = new string[2];
            char[] splitter = { '/' };
            userId = dlinfo.Login.Split(splitter);

            // download login portal page HTML to find login URL
            pageInfo = DownloadData(ssoUrl);
            Match LoginUrl_Match = LoginUrl_regex.Match(pageInfo);

            // if we can't find the login URL, throw an error
            if (!LoginUrl_Match.Success)
            {
                LogErrorAndSendEmailToAdmin("Fannie Mae PE Bot cannot find login URL for login: " + userId[0] + ". Content: " + pageInfo);
                return E_BotDownloadStatusT.Error;
            }

            loginUrl = baseUrl + LoginUrl_Match.Groups["LoginUrl"].Value;

            //login
            NameValueCollection postData = new NameValueCollection();
            postData.Add("pf.username", userId[0]);
            postData.Add("pf.pass", dlinfo.Password);
            postData.Add("pf.ok", "clicked");
            postData.Add("pf.cancel", "");
            postData.Add("pf.adapterId", "ExternalUserLoginADAdapter");

            // POST login info, download HTML of response to find credentials we need to finish logging in
            findcredentials = UploadDataReturnResponse(loginUrl, postData);
            Match SAMLResponse_Match = SAMLResponse_regex.Match(findcredentials);
            Match RelayState_Match = RelayState_regex.Match(findcredentials);

            if (SAMLResponse_Match.Success & RelayState_Match.Success)
            {
                /////////////////////ALL OF THIS IS IN IF(SAMLResponse_Match.Success && RelayState_Match.Success)
                postData = new NameValueCollection();
                postData.Add("SAMLResponse", SAMLResponse_Match.Groups["SAMLResponse"].Value);
                postData.Add("RelayState", RelayState_Match.Groups["RelayState"].Value);

                response = UploadDataReturnResponse(essoPostUrl, postData);

                //check for institution (if multiple)
                if (response.Contains("Please select an Institution"))
                {
                    if (response.Contains(userId[1]))
                        response = DownloadData(institutionUrl + userId[1]);
                    else
                    {
                        GoToUrl(logoutUrl);
                        LogErrorAndSendEmailToAdmin("DEACTIVATED. Fannie Mae PE Bot could not find institution " + userId[1] + " for login: " + userId[0] + ". Please update and re-enable download list entry.");
                        dlinfo.Deactivate();
                        return E_BotDownloadStatusT.Error;
                    }
                }
                if (response.Contains("Upcoming Expirations"))
                {
                    long unixTimestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds);

                    if (dlinfo.Description.EndsWith("MDAA"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ActualActual&products=102710&products=160958&products=0020FR&products=0010FR&products=101304&products=132021&products=100645&products=0015FR&products=130829&products=133212&products=MCP30A&products=102301&products=125331&products=0005FR&products=A228AR&products=094447&products=A224AR&products=A222AR&products=A230AR&products=A218AR&products=144854&products=151217&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("MDSS"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ScheduledScheduled&products=102710&products=160958&products=0005FR&products=125331&products=102301&products=MCP30A&products=133212&products=130829&products=0015FR&products=100645&products=132021&products=101304&products=0010FR&products=0020FR&products=A228AR&products=094447&products=A224AR&products=A222AR&products=A230AR&products=A218AR&products=144854&products=151217&underwrittenWithDu=false&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("BEAA"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=BestEfforts&levelType=LoanApplication&remittanceType=ActualActual&products=102710&products=160958&products=0020FR&products=0010FR&products=101304&products=132021&products=100645&products=0015FR&products=130829&products=133212&products=MCP30A&products=102301&products=125331&products=0005FR&products=A228AR&products=094447&products=A224AR&products=A222AR&products=A230AR&products=A218AR&products=144854&products=151217&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("PML0185"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=BestEfforts&levelType=LoanApplication&remittanceType=ActualActual&products=102710&products=160958&products=0020FR&products=0010FR&products=101304&products=132021&products=100645&products=0015FR&products=130829&products=133212&products=MCP30A&products=102301&products=125331&products=0005FR&products=A228AR&products=094447&products=A224AR&products=A222AR&products=A230AR&products=A218AR&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=30&windows=45&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("BESS"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=BestEfforts&levelType=LoanApplication&remittanceType=ScheduleSchedule&products=102710&products=160958&products=0020FR&products=0010FR&products=101304&products=132021&products=100645&products=0015FR&products=130829&products=133212&products=MCP30A&products=102301&products=125331&products=0005FR&products=A228AR&products=094447&products=A224AR&products=A222AR&products=A230AR&products=A218AR&products=144854&products=151217&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("PML0158"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ActualActual&" +
                            "products=160958&products=0005FR&products=125331&products=102710&products=102301&products=MCP30A&products=133212&products=130829&products=0015FR&products=100645&products=132021&products=101304&products=0010FR&products=0020FR&products=A228AR&products=094447&products=A222AR&products=A230AR&products=154552"
                            + "&underwrittenWithDu=false&servicingReleased=false&displayMarketFrozenMessage=false&windows=27&windows=44&windows=65&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("PML0158_MDAA_RS2"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ActualActual&" +
                            "products=160639&products=160058&products=155509&products=154232&products=143846&products=163631&products=163247&products=162738&products=162232&products=151435&products=081521&products=081007&products=080411&products=135931&products=133459"
                            + "&underwrittenWithDu=false&servicingReleased=false&displayMarketFrozenMessage=false&windows=27&windows=44&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("PML0158_MDAA_RS3"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ActualActual&" +
                            "products=114921&products=115446&products=144854&products=130120&products=131839&products=151217"
                            + "&underwrittenWithDu=false&servicingReleased=false&displayMarketFrozenMessage=false&windows=27&windows=44&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("PML0214_HFA_BE"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=BestEfforts&levelType=LoanApplication&remittanceType=ActualActual&products=104315&products=111202&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("PML0214_HFA_MD"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ActualActual&products=104315&products=111202&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    //new additions for LAMT specific pricing eOPM case 840024
                    else if (dlinfo.Description.EndsWith("MDSS_RS2"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ScheduledScheduled&products=163053&products=160639&products=160058&products=155509&products=154232&products=143846&products=163631&products=163247&products=162738&products=162232&products=151435&products=115446&products=131839&products=114921&products=081521&products=081007&products=080411&products=135931&products=133459&products=130120&underwrittenWithDu=false&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("MDAA_RS2"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=Mandatory&levelType=Product&remittanceType=ActualActual&products=163053&products=160639&products=160058&products=155509&products=154232&products=143846&products=163631&products=163247&products=162738&products=162232&products=151435&products=115446&products=131839&products=114921&products=081521&products=081007&products=080411&products=135931&products=133459&products=130120&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;
                    else if (dlinfo.Description.EndsWith("BESS_RS2"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=BestEfforts&levelType=LoanApplication&remittanceType=ScheduleSchedule&products=163053&products=160639&products=160058&products=155509&products=154232&products=143846&products=163631&products=163247&products=162738&products=162232&products=151435&products=115446&products=131839&products=114921&products=081521&products=081007&products=080411&products=135931&products=133459&products=130120&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp; 
                    else if (dlinfo.Description.EndsWith("BEAA_RS2"))
                        dlURL = peUrl + "/pricing/results/csv?priceIncrement=1-8&priceView=total&executionType=BestEfforts&levelType=LoanApplication&remittanceType=ActualActual&products=163053&products=160639&products=160058&products=155509&products=154232&products=143846&products=163631&products=163247&products=162738&products=162232&products=151435&products=115446&products=131839&products=114921&products=081521&products=081007&products=080411&products=135931&products=133459&products=130120&underwrittenWithDu=true&servicingReleased=false&displayMarketFrozenMessage=false&windows=10&windows=30&windows=60&windows=90&requestTimeMs=" + unixTimestamp;

                    else
                    {
                        LogErrorAndSendEmailToAdmin("FannieMaePE bot encountered error in bot description for login: " + userId[0] + ". Please update it");
                        return E_BotDownloadStatusT.Error;
                    }
                    RatesheetFileName = DownloadFile(dlURL);
                    GoToUrl(logoutUrl);
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;

                }
                else
                {
                    LogErrorAndSendEmailToAdmin("Fannie Mae PE Bot could not match the SAML Response and/or the Relay State for login: " + userId[0] + ". Response; " + response);
                    return E_BotDownloadStatusT.Error;
                }
            }

                /////////////////////ALL OF THAT IS IN IF(SAMLResponse_Match.Success && RelayState_Match.Success)
            else
            {
                LogErrorAndSendEmailToAdmin("DEACTIVATED. Fannie Mae PE Bot could not find credentials for login: " + userId[0] + ". Please update and re-enable download list entry. FindCredentials; " + findcredentials);
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}

