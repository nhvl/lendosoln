// Author: Brian Beery
// Date: 4-25-08
// Purpose: Download the M&T rate sheet

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots 
{
	public class MandT : AbstractWebBot 
	{
        private const string sHostUrl = "https://newmortgageportal.mtb.com/";
        private static Regex s_Expired = new Regex(@"The password you used has expired and must be changed before proceeding. Please be aware that passwords are case sensitive");
		private static Regex s_InfoCenter = new Regex(@"<a[ ]+class=""[^""]+""[ ]+href=""/(?<url>[^""]+)"">Rates</a>") ;
		private static Regex s_RatesPage = new Regex(@"<iframe[ ]+src=""(?<url>[^""]+)""[ ]+name=""tree") ;
		private const string sRS = "linkout/responseDocuments.asp?DocType=PDF&PDF=True&DOC_ID=" ;
		private static Regex s_DocID = new Regex(@"Rates[^<]*</td><td><div[ ]+id=""[A-Za-z]+(?<docid>[^""]+)""") ;
		private static Regex s_DocID_Broker = new Regex(@"M&T\sRates\s-\s[A-Za-z\s\(\)]+</td><td><div[ ]+id=""[A-Za-z]+(?<docid>[^""]+)""") ;

		public MandT(IDownloadInfo info) : base(info) { }

		public override string BotName 
		{
			get { return "MandT" ; }
		}

		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
			AllowAutoRedirect = false;
			// Cookies
			GoToUrl(sHostUrl) ;
			GoToUrl(string.Format("{0}prepare.asp", sHostUrl)) ;
			GoToUrl(string.Format("{0}defaultframe.asp", sHostUrl)) ;

			// Login
			NameValueCollection nvcPostData = new NameValueCollection() ;
			nvcPostData.Add("sUser", dlinfo.Login) ;
			nvcPostData.Add("sPassword", dlinfo.Password) ;
            string checkExp = UploadDataReturnResponse(string.Format("{0}login.asp", sHostUrl), nvcPostData);
            //check password expiration
            Match expiredMatch = s_Expired.Match(checkExp);
            if (expiredMatch.Success)
            {
                ReportDeactivation(string.Format("ERROR - MandT Bot password has expired. Please update the password and re-enable bot. Login:{0}",dlinfo.Login));
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

			GoToUrl(string.Format("{0}LoginRedirect.asp", sHostUrl)) ;
			
			// Find rates section
			string sRSListing = DownloadData(string.Format("{0}design/securehomepage.asp", sHostUrl)) ;
			Match match = s_InfoCenter.Match(sRSListing) ;
			
			string sFileName = null ;
			if(match.Success) 
			{
				GoToUrl(string.Format("{0}messaging/message.asp", sHostUrl)) ;
				sRSListing = match.Groups["url"].Value ;
				
				string sRPSubDir = "" ;
				int index = sRSListing.LastIndexOf('/') ;
				if(index > 0)
					sRPSubDir = sRSListing.Substring(0, index+1) ;

				sRSListing = DownloadData(string.Format("{0}{1}", sHostUrl, sRSListing)) ;
				GoToUrl(string.Format("{0}messaging/message.asp", sHostUrl)) ;

				// Find rates listing
				match = s_RatesPage.Match(sRSListing) ;
				if(match.Success)
				{
					sRSListing = match.Groups["url"].Value ;
					sRSListing = DownloadData(string.Format("{0}{1}{2}", sHostUrl, sRPSubDir, sRSListing)) ;

					// DL rate sheet
					if (RateSheetType == "MandT") 
					{
						match = s_DocID.Match(sRSListing) ;
					}
					else 
					{
						match = s_DocID_Broker.Match(sRSListing);
					}
					if(match.Success)
					{
						sFileName = DownloadFile(string.Format("{0}{1}{2}", sHostUrl, sRS, match.Groups["docid"].Value)) ;
					}
					else
					{
						LogErrorAndSendEmailToAdmin(string.Format("ERROR - M&T's rate sheet was not found.\nThe following was returned by their server:\n{0}", sRSListing)) ;
					}
				}			
				else
				{
					LogErrorAndSendEmailToAdmin(string.Format("ERROR - M&T's rates listing was not found.\nThe following was returned by their server:\n{0}", sRSListing)) ;
				}
			}
			else
			{
				LogErrorAndSendEmailToAdmin(string.Format("ERROR - M&T's rates section was not found.\nPossible login failure for user ID {0}.\nThe following was returned by their server:\n{1}", dlinfo.Login, sRSListing)) ;
			}

			GoToUrl(string.Format("{0}logout.asp", sHostUrl)) ;

            if (null != sFileName)
            {
                RatesheetFileName = sFileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }
		}
	}
}