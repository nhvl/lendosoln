﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class BBandTBot : AbstractWebBot
    {
        private static string baseUrl = @"https://correspondentlending.bbt.com/CLWeb/";
        private static Regex viewState_regex = new Regex (@"id=""__VIEWSTATE""\s+value=""(?<viewStateValue>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex clRates_regex = new Regex(@"href=\'(?<clRatesValue>[^\']+)\'\s+target=\'_blank\'>CLRates", RegexOptions.IgnoreCase);
        //private static Regex caRates_regex = new Regex(@"href=\'(?<caRatesValue>[^\']+)\'\s+target=\'_blank\'>CARates", RegexOptions.IgnoreCase);
        private static Regex caRates_regex = new Regex(@"href=\'(?<caRatesValue>[^\']+)\'\s+target=\'_blank\'>CorrAdvantage Rates", RegexOptions.IgnoreCase); 
        //private static Regex validLogin_regex = new Regex(@"CLWeb/pipeline.aspx", RegexOptions.IgnoreCase);
        private static Regex validLogin_regex = new Regex(@"pipeline.aspx", RegexOptions.IgnoreCase);
        private string[] userId = new string[2];
        private char[] splitter = { '/' };

        public BBandTBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "BBT"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null;
            string temp = DownloadData(baseUrl + @"Login.aspx");
            Match viewState_match = viewState_regex.Match(temp);
            if (viewState_match.Success)
            {
                userId = dlinfo.Login.Split(splitter);

                NameValueCollection postData = new NameValueCollection();
                postData.Add("ctl00$ContentPlaceHolder1$ToolkitScriptManager1", "ctl00$ContentPlaceHolder1$UpdatePanel1|ctl00$ContentPlaceHolder1$imgLogon");
                postData.Add("__LASTFOCUS", "");
                postData.Add("ctl00_ContentPlaceHolder1_ToolkitScriptManager1_HiddenField", "");
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", viewState_match.Groups["viewStateValue"].Value);
                postData.Add("ctl00$ContentPlaceHolder1$tbUserName", userId[1]);
                postData.Add("ctl00$ContentPlaceHolder1$tbPassword", dlinfo.Password);
                postData.Add("ctl00$ContentPlaceHolder1$tbCC", userId[0]);
                postData.Add("__ASYNCPOST", "true");
                postData.Add("ctl00$ContentPlaceHolder1$imgLogon.x", "18");
                postData.Add("ctl00$ContentPlaceHolder1$imgLogon.y", "6");

                temp = UploadDataReturnResponse(baseUrl + @"Login.aspx", postData);
                Match validLogin_match = validLogin_regex.Match(temp);
                if (validLogin_match.Success)
                {
                    GoToUrl(baseUrl + @"pipeline.aspx");
                    temp = DownloadData(baseUrl + @"ratesheets.aspx");
                  
                    switch (dlinfo.Description)
                    {
                        case "BBT_CL":
                            Match clRates_match = clRates_regex.Match(temp);
                            if (clRates_match.Success)
                                fileName = DownloadFile(baseUrl + clRates_match.Groups["clRatesValue"].Value);
                            else
                                LogErrorAndSendEmailToAdmin("BBandTBot can't find CL Ratesheet for login: " + userId[1] + ". Contents: " + temp);
                            break;
                        case "BBT_CA":
                            Match caRates_match = caRates_regex.Match(temp);
                            if (caRates_match.Success)
                                fileName = DownloadFile(baseUrl + caRates_match.Groups["caRatesValue"].Value);
                            else
                                LogErrorAndSendEmailToAdmin("BBandTBot can't find CA Ratesheet for login: " + userId[1] + ". Contents: " + temp);
                            break;
                        default:
                            LogErrorAndSendEmailToAdmin("BBandTBot can't recognize download description. Please verify the description for login: " + userId[1]);
                            break;
                    }
                    GoToUrl(baseUrl + @"logout.aspx");
                }
                else
                    LogErrorAndSendEmailToAdmin("BBandTBot failed to login. Please check login/password for user: " + userId[1]);
            }
            else
                LogErrorAndSendEmailToAdmin("BBandTBot can't find viewState value for login: " + userId[1] + ". Contents: " + temp);

            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}