// Author: Brian Beery
// Date: 10-30-07
// Purpose: Download the MIT Wholesale rate sheet

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots 
{
	public class MIT : AbstractWebBot 
	{
		private const string sHostUrl = "https://wld.mortgageit.com/" ;
		private static Regex s_inputRegEx = new Regex(@"<input[ ]+type=""hidden""[ ]+name=""(?<name>[^""]+)""[ ]+id=""[^""]+""[ ]+value=""(?<value>[^""]+)""[^>]+>");
		private static Regex s_optionRegEx = new Regex(@"<option[ ]+selected=""selected""[ ]+value=""(?<name>[^""]+)"">Today[^>]+>");
		private static string sRSMIA = "Rate sheets unavailable for today" ;
		
		public MIT(IDownloadInfo info) : base(info) { }

		public override string BotName 
		{
			get { return "MIT" ; }
		}

		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
			string sLoginUrl = string.Format("{0}default.aspx", sHostUrl) ;
            string sRSListing = string.Format("{0}Members/Rates.aspx", sHostUrl);
			const string sRSBtn = "ctl00$Main$dlRatesheetList$ctl01$LinkButton1" ;
            string sLogOutUrl = string.Format("{0}LogOff.aspx", sHostUrl);
			NameValueCollection nvcPostData = new NameValueCollection() ;

			// Get the login page, collect necessary hidden inputs, pass back along with login info
			string sHtml = DownloadData(sLoginUrl) ;
			nvcPostData.Add(FindHiddenInputs(sHtml)) ;

			nvcPostData.Add("ctl00$Loginmin1$txtUsername", dlinfo.Login) ;
			nvcPostData.Add("ctl00$Loginmin1$txtPassword", dlinfo.Password) ;
			nvcPostData.Add("ctl00$Loginmin1$btnOK", "OK") ;

			UploadData(sLoginUrl, nvcPostData) ;

			// Get rate sheet listing, collect necessary hidden inputs, get rate sheet
			sHtml = DownloadData(sRSListing) ;
			nvcPostData = new NameValueCollection() ;
			
			string sFileName = null ;
			Match match = s_optionRegEx.Match(sHtml) ;
			if(match.Success) 
			{
				if(RSIsAvailable(sHtml))
				{
					nvcPostData.Add("ctl00$Main$ddlHistoricalDate", match.Groups["value"].Value) ;
			
					nvcPostData.Add(FindHiddenInputs(sHtml)) ;
					nvcPostData.Add("__EVENTTARGET", sRSBtn) ;

					sFileName = UploadDataDownloadFile(sRSListing, nvcPostData) ;
				}
			}
			else
			{
				LogErrorAndSendEmailToAdmin(string.Format("ERROR - MIT's wholesale rate sheet was not found.\nThe following was returned by their server:\n{0}", sHtml)) ;
			}

			GoToUrl(sLogOutUrl) ;

            if (null != sFileName)
            {
                RatesheetFileName = sFileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }
		}

		// Parses the given text/aspx for certain hidden inputs
		// Returns an NVC with the name/value pairs
		private NameValueCollection FindHiddenInputs(string sToBeParsed)
		{
			NameValueCollection nvc = new NameValueCollection() ;

			Match match = s_inputRegEx.Match(sToBeParsed) ;
			while(match.Success) 
			{
				if(match.Groups["name"].Value == "__VIEWSTATE") 
				{
					nvc.Add("__VIEWSTATE", match.Groups["value"].Value) ;
				} 
				else if(match.Groups["name"].Value == "__EVENTVALIDATION") 
				{
					nvc.Add("__EVENTVALIDATION", match.Groups["value"].Value) ;
				}
				match = match.NextMatch() ;
			}
			return nvc ;
		}

		// Parses the given text/aspx for a 'rate sheet unavailable' message
		// Returns true if the rate sheet is available (i.e. the message is not found)
		private bool RSIsAvailable(string sToBeParsed)
		{
			return (sToBeParsed.IndexOf(sRSMIA) >= 0) ? false : true ;
		}
	}
}