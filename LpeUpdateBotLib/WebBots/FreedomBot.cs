﻿//Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
namespace LpeUpdateBotLib.WebBots
{
    public class FreedomBot : AbstractWebBot
    {
        private static string baseUrl = "https://www.freedomconnect.com";

        private static Regex loginUrl_regex = new Regex (@"<form method=""POST""\s+action=""(?<loginUrl>[^""]+)""\s+name=""LoginForm", RegexOptions.IgnoreCase);
        private static Regex loginToken_regex = new Regex(@"name=""loginClientHashedToken""\s+value=""(?<loginToken>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex invalidLogin_regex = new Regex(@"User\s+authentication\s+failed", RegexOptions.IgnoreCase);
        private static Regex rsUrl_regex = new Regex(@"href=(?<rsUrl>[^\s]+)\s+target=""_blank""", RegexOptions.IgnoreCase);
        private static Regex logoutUrl_regex = new Regex(@"<a class=""[^""]+""\s+href=\'(?<logoutUrl>[^\']+)\'>Logout", RegexOptions.IgnoreCase);

        public FreedomBot(IDownloadInfo info): base(info)
        {
        }
        public override string BotName
        {
            get { return "FREEDOM"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string temp = null;
            string fileName = null;

            temp = DownloadData(dlinfo.Url);

            // find login token
            Match loginToken_match = loginToken_regex.Match(temp);

            // failed to find login token
            if (!loginToken_match.Success)
            {
                LogErrorAndSendEmailToAdmin("Freedom Bot failed to find login token. Login: " + dlinfo.Login + ". Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }

            // find login URL
            Match loginUrl_match = loginUrl_regex.Match(temp);

            // failed to find login URL
            if (!loginUrl_match.Success)
            {
                LogErrorAndSendEmailToAdmin("Freedom Bot failed to find login URL. Login: " + dlinfo.Login + ". Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }

            string loginToken = loginToken_match.Groups["loginToken"].Value.ToString();
            string loginUrl = loginUrl_match.Groups["loginUrl"].Value.ToString();

            // login
            NameValueCollection postData = new NameValueCollection();
            postData.Add("loginClientHashedToken", loginToken);
            postData.Add("wps.portlets.userid", dlinfo.Login);
            postData.Add("password", dlinfo.Password);
            postData.Add("wps.portlets.resumeSession", "true");
            temp = UploadDataReturnResponse(baseUrl + loginUrl, postData);

            // check if the login credentials are invalid
            Match invalidLogin_match = invalidLogin_regex.Match(temp);

            // failed to login
            if (invalidLogin_match.Success)
            {
                dlinfo.Deactivate();
                ReportDeactivation("Freedom Bot - Invalid login. Please update password and reactivate Download List entry for login: " + dlinfo.Login + ". Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }

            // find ratesheet URL
            Match rsUrl_match = rsUrl_regex.Match(temp);
            string rsUrl = rsUrl_match.Groups["rsUrl"].Value.ToString();

            // find logout URL
            Match logoutUrl_match = logoutUrl_regex.Match(temp);
            string logoutUrl = logoutUrl_match.Groups["logoutUrl"].Value.ToString();

            // failed to find ratesheet URL
            if (!rsUrl_match.Success)
            {
                LogErrorAndSendEmailToAdmin("Freedom Bot failed to find ratesheet URL. Login: " + dlinfo.Login + ". Content: " + temp);
                GoToUrl(baseUrl + logoutUrl);
                return E_BotDownloadStatusT.Error;
            }

            // download ratesheet and logout
            fileName = DownloadFile(baseUrl + rsUrl);
            GoToUrl(baseUrl + logoutUrl);

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}