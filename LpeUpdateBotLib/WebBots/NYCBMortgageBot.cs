/// Author: Budi Sulayman
/// 
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Threading;

namespace LpeUpdateBotLib.WebBots
{
    public class NYCBMortgageBot : AbstractWebBot
    {
        private const string LoginUrl = "https://www.nycbmortgage.com/Login.aspx";
        private const string DownloadUrl = "https://www.nycbmortgage.com/rates/streamxls.asp?";
        private static Regex s_inputRegEx = new Regex(@"<input[ ]+type=""hidden""[ ]+name=""(?<name>[^""]+)""[ ]+id=""[^""]+""[ ]+value=""(?<value>[^""]+)""[^>]+>");
        private static Regex viewStateRegex = new Regex(@"<input\s+type=""[^""]+""\s+name=""__VIEWSTATE""\s+id=""__VIEWSTATE""\s+value=""(?<viewState_value>[^""]+)""");
        private static Regex s_badLoginRegEx = new Regex(@"<span[^>]*>Login ID:");
        private static Regex clientIdRegex = new Regex(@"onKeyPress=""Impers_txtCBID_onKeyPress\(\)""\s+onChange=""Impers_SaveCBID\(this.value\)""\s+value=""(?<clientId>[^""]+)""");

        public NYCBMortgageBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "NYCBMORTGAGE"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string html = DownloadData(LoginUrl);
            NameValueCollection postData = new NameValueCollection();
            string csrfkey = null;
            string viewState = null;

            Match match = s_inputRegEx.Match(html);
            while (match.Success)
            {
                if (match.Groups["name"].Value == "csrfkey")
                    csrfkey = match.Groups["value"].Value;
                else if (match.Groups["name"].Value == "__VIEWSTATE")
                {
                    viewState = match.Groups["value"].Value;
                    break;
                }
                match = match.NextMatch();
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("csrfkey=" + UrlEncode(csrfkey) + "&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=" + UrlEncode(viewState) + "&__EVENTTARGET=" + UrlEncode("ctlLogin:loginControl:btnSubmit") + "&__VIEWSTATEENCRYPTED=&ctlLogin:loginControl:hdnMachineValues=" + UrlEncode("Win32,Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 1.0.3705; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729),983,1229") + "&ctlLogin:loginControl:txtLoginID=" + UrlEncode(dlinfo.Login) + "&ctlLogin:loginControl:txtPassword=" + UrlEncode(dlinfo.Password) + "&ctlLogin:loginControl:btnSubmit=" + UrlEncode("Login"));
               
            html = UploadDataReturnResponse(LoginUrl, sb);

            if ((Regex.Match(html, @"New password does not match the confirmed password").Success) ||
                (s_badLoginRegEx.Match(html).Success))
            {
                string sErrorMsg = string.Format("Login to NYCBMortgage website failed for user id {0}. Please verify login, contact the client and/or update pw if necessary, and re-enable any dl using this login.", dlinfo.Login);
                ReportDeactivation(sErrorMsg);
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }

            match = viewStateRegex.Match(html);
            if (match.Success)
                viewState = match.Groups["viewState_value"].Value;
            else
            {
                LogErrorAndSendEmailToAdmin("NYCBMortgage Bot failed to find viewstate value for login: " + dlinfo.Login + ". Content:" + html);
                return E_BotDownloadStatusT.Error;
            }

            sb = new StringBuilder();
            sb.AppendFormat("csrfkey=" + UrlEncode(csrfkey) + "&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=" + UrlEncode(viewState) + "&__EVENTTARGET=" + UrlEncode("ctlLogin:loginControl:btnSubmit") + "&__VIEWSTATEENCRYPTED=&ctlLogin:loginControl:hdnMachineValues=" + UrlEncode("Win32,Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 1.0.3705; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729),983,1229") + "&ctlLogin:loginControl:txtAnswer0=" + UrlEncode("Blue") + "&ctlLogin:loginControl:txtAnswer1=" + UrlEncode("Blue") + "&ctlLogin:loginControl:btnSubmit=" + UrlEncode("Submit"));

            UploadData(LoginUrl, sb);

            string temp = DownloadData("https://www.nycbmortgage.com/LegacyNavigation.aspx");
            match = clientIdRegex.Match(temp);
            if (match.Success)
            {
                string rawData = @"<docroot><loannumber></loannumber><cbid>" + match.Groups["clientId"].Value + @"</cbid><cbidfromloan>N</cbidfromloan></docroot>";

                RawUploadDataAndReturnResponse("https://www.nycbmortgage.com/navigator/impersonation/server/impers_setcookie.asp", null, rawData, null, "https://www.nycbmortgage.com/MtgMktg/rates/");
                GoToUrl("https://www.nycbmortgage.com/rates/ratesdl.asp");

                DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0);
                DateTime rsTimeInUTC = DateTime.UtcNow;
                long newTime = (rsTimeInUTC - baseTime).Ticks / TimeSpan.TicksPerMillisecond;

                RatesheetFileName = DownloadFile(DownloadUrl + newTime);

                GoToUrl("https://www.nycbmortgage.com/Logout.aspx");

                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("NYCBMortgage bot failed to get client ID for login: " + dlinfo.Login + ". Content: " + temp);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}
