///Author: Budi 

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
	public class NewSouthBot : AbstractWebBot
	{
		/// declare private
		private static string BaseUrl = "https://www.newsouthfederal.com";
        private static string LoginUrl = BaseUrl + "/wholesale/login.php";
        private static Regex rsPage_regex = new Regex (@"a\s+href=""(?<rsPage>[^""]+)"">Rate\s+Sheets</a>");
        private static Regex rsLink_regex = new Regex (@"Portfolio\s+Correspondent\s+Rates</a>\s+\(<a\s+href=""(?<ratesheetLink>[^""]+)"">xls");

		public NewSouthBot(IDownloadInfo info) : base(info)
		{}

		public override string BotName 
		{
			get { return "NEWSOUTH"; }
		}

        protected override string DefaultAcceptHeader
        {
            get
            {
                string acceptValue = "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, application/x-shockwave-flash, application/x-ms-application, application/x-ms-xbap, application/vnd.ms-xpsdocument, application/xaml+xml, application/x-silverlight, */*";
                return acceptValue;
            }
        }

		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
			//If this is the weekend, do nothing
			DayOfWeek day = DateTime.Now.DayOfWeek ;
			if(day == DayOfWeek.Saturday || day == DayOfWeek.Sunday)
				return E_BotDownloadStatusT.Error ;

			NameValueCollection loginData = new NameValueCollection();
			loginData.Add("username", dlinfo.Login);
			loginData.Add("password", dlinfo.Password);
			loginData.Add("Submit", "Submit");


            string temp = UploadDataReturnResponse(LoginUrl, loginData);

            string fileName = null;

            Match match = rsPage_regex.Match(temp);
            if (match.Success)
            {
                string rslink = DownloadData(BaseUrl + match.Groups["rsPage"].Value);
                match = rsLink_regex.Match(rslink);
                if (match.Success)
                    fileName = DownloadFile(BaseUrl + match.Groups["ratesheetLink"].Value);
                else
                    LogErrorAndSendEmailToAdmin("NewSouthBot can't find ratesheet link. Contents: " + rslink);
            }
            else
                LogErrorAndSendEmailToAdmin("NewSouthBot can't find index page after login. Contents: " + temp);

            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }

		}
	}
}
