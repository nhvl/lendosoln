﻿//Author: Ian Merna


using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class WebsiteDownloadBot : AbstractWebBot
    {
        
        public WebsiteDownloadBot(IDownloadInfo info) : base(info)
        {
        }

        public override string BotName
        {
            get { return "WEBSITE_DOWNLOAD"; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            }
        }
        protected override string DefaultAcceptLanguage
        {
            get
            {
                return @"en-US,en;q=0.9";
            }
        }

        protected override string BotUserAgentString
        {
            get
            {
                return @"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
            }
        }


        private Regex startTimeRegex = new Regex(@"AFTER_(?<startTime>[^_]+)");
        private bool hasStartTime = false;
        private int startTimeHour;

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {

            if (dlinfo.Description.Contains("AFTER"))
            {
                Match startTimeMatch = startTimeRegex.Match(dlinfo.Description);
                if (Int32.TryParse(startTimeMatch.Groups[1].Value, out startTimeHour))
                    hasStartTime = true;                
            }

            if (dlinfo.Description.Contains("DAILY") && hasStartTime && dlinfo.LastRatesheetTimestamp.Date != DateTime.Today && (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(startTimeHour).AddMinutes(10)) > 0))
            {
                GoToUrl(dlinfo.Url);
                RatesheetFileName = DownloadFile(dlinfo.Url);
                dlinfo.RecordLastRatesheetTimestamp(DateTime.Now);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (dlinfo.Description.Contains("DAILY") && !hasStartTime && dlinfo.LastRatesheetTimestamp.Date != DateTime.Today)
            {
                GoToUrl(dlinfo.Url);
                RatesheetFileName = DownloadFile(dlinfo.Url);
                dlinfo.RecordLastRatesheetTimestamp(DateTime.Now);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (!dlinfo.Description.Contains("DAILY"))
            {
                GoToUrl(dlinfo.Url);
                RatesheetFileName = DownloadFile(dlinfo.Url);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }

            else
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet; 


        }
    }
}