/// Author: Budi Sulayman
namespace LpeUpdateBotLib.WebBots
{
    using System;
    using System.Collections.Specialized;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    //using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;

    public class LOBot : AbstractWebBot
	{
		private static string loginUrl = "http://loauth/lo/website/index.aspx";
		private static Regex requestId_regex = new Regex (@"<data RequestID=""(?<requestId>[^""]+)""");
		private static Regex isAvailable_regex = new Regex (@"<data Duration=""[^""]+""\s+IsAvailable=""1""\s+/>");
		private static Regex jscriptStats_regex = new Regex (@"jsScriptStats=(?<jscriptStats>[^']+)'>");

		public LOBot(IDownloadInfo info) : base(info)
		{
		}
		public override string BotName 
		{
			get { return "LendersOffice"; }
		}
		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
			NameValueCollection postData = new NameValueCollection();
			postData.Add("__VIEWSTATE", "");
			postData.Add("LoginUserControl1:m_UserName","pmlbuditest");
			postData.Add("LoginUserControl1:m_Password","hoohoo12");
			postData.Add("LoginUserControl1:m_login", "Login");

			string temp = null;
			string temp1 = null;
			string reqId = null;
			string[] loanId = new String[2];

			//loan number to test
			loanId[0] = "b2690290-9c6b-4529-8402-675d9ccc4e08";
			loanId[1] = "d3a4d172-38e6-49bc-a59b-f26f7601a4fa";

			UploadData(loginUrl,postData);

			for (int i = 0; i < loanId.Length; i++)
			{
				string tempPath = @"c:\test"+i+".txt";
                using (System.IO.StreamWriter sw = System.IO.File.CreateText(tempPath))
                {
                    GoToUrl("http://loauth/lo/embeddedpml/main/agents.aspx?userid=fd087830-b1ec-4636-8d0b-356b465e6c6f&loanid=" + loanId[i]);
                    GoToUrl("http://loauth/lo/embeddedpml/main/main.aspx?loanId=" + loanId[i] + "&islogin=&indexurl=&tabkey=PROPINFO&previousIndex=3&maxVisitedIndex=2");
                    temp1 = "<request><data loanid=\"" + loanId[i] + "\" sLienQualifyModeT=\"0\" /></request>";
                    temp = RawUploadDataAndReturnResponse("http://loauth/lo/EmbeddedPml/main/mainservice.aspx?method=GetResult_SubmitUnderwriting", "text/xml", temp1);
                    Match match = requestId_regex.Match(temp);
                    if (match.Success)
                        reqId = match.Groups["requestId"].Value;
                    else
                        LogErrorAndSendEmailToAdmin("LOBot. Can't find request id. " + temp);
                    RawUploadDataAndReturnResponse("http://loauth/lo/EmbeddedPml/main/mainservice.aspx?method=GetResult_IsAvailable", "text/xml", "<request><data requestid=\"" + reqId + "\" /></request>");
                    temp = DownloadData("http://loauth/lo/embeddedpml/main/main.aspx?loanId=" + loanId[i] + "&islogin=&indexurl=&tabkey=RESULT&previousIndex=3&maxVisitedIndex=2&actionCmd=Result&requestId=" + reqId + "&jsScriptStats=6488:23:78:1:0");
                    try
                    {
                        sw.Write(temp);
                        sw.Close();
                    }
                    catch
                    {
                        LogErrorAndSendEmailToAdmin("LOBot can't write to a file");
                    }
                }
/*
                Action<TextFileHelper.LqbTextWriter> writeHandler = delegate (TextFileHelper.LqbTextWriter sw)
                {
                    GoToUrl("http://loauth/lo/embeddedpml/main/agents.aspx?userid=fd087830-b1ec-4636-8d0b-356b465e6c6f&loanid=" + loanId[i]);
                    GoToUrl("http://loauth/lo/embeddedpml/main/main.aspx?loanId=" + loanId[i] + "&islogin=&indexurl=&tabkey=PROPINFO&previousIndex=3&maxVisitedIndex=2");
                    temp1 = "<request><data loanid=\"" + loanId[i] + "\" sLienQualifyModeT=\"0\" /></request>";
                    temp = RawUploadDataAndReturnResponse("http://loauth/lo/EmbeddedPml/main/mainservice.aspx?method=GetResult_SubmitUnderwriting", "text/xml", temp1);
                    Match match = requestId_regex.Match(temp);
                    if (match.Success)
                        reqId = match.Groups["requestId"].Value;
                    else
                        LogErrorAndSendEmailToAdmin("LOBot. Can't find request id. " + temp);
                    RawUploadDataAndReturnResponse("http://loauth/lo/EmbeddedPml/main/mainservice.aspx?method=GetResult_IsAvailable", "text/xml", "<request><data requestid=\"" + reqId + "\" /></request>");
                    temp = DownloadData("http://loauth/lo/embeddedpml/main/main.aspx?loanId=" + loanId[i] + "&islogin=&indexurl=&tabkey=RESULT&previousIndex=3&maxVisitedIndex=2&actionCmd=Result&requestId=" + reqId + "&jsScriptStats=6488:23:78:1:0");
                    try
                    {
                        sw.Append(temp);
                    }
                    catch
                    {
                        LogErrorAndSendEmailToAdmin("LOBot can't write to a file");
                    }
                };

                TextFileHelper.OpenNew(tempPath, writeHandler);
*/
            }

            return E_BotDownloadStatusT.Error;
		}
	}
}


