// Author Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots 
{
	public class FAMC : AbstractWebBot 
	{
		private static Regex AuthFailedRegex = new Regex(@"Authorization(\s+)Required");
        private static Regex loginFailedRegex = new Regex(@"Login\s+Failure", RegexOptions.IgnoreCase);
        private static Regex rsFormRegex = new Regex(@"<form name=""rateSheetForm"" action=""rateSheet"">\s+<input type=""hidden"" name=""email""\s+value=""(?<rsFormEmail>[^""]+)""\s+/>\s+<input type=""hidden"" name=""id"" value=""(?<rsFormId>[^""]+)""\s+/>\s+<input type=""hidden"" name=""npage"" value=""""\s+/>\s+<input type=""hidden"" name=""date"" value=""(?<rsFormDate>[^""]+)""\s+/>\s+<input type=""hidden"" name=""s"" value=""(?<rsFormS>[^""]+)""\s+/>\s+<input type=""hidden"" name=""mobile"" value=""""\s+/>\s+<input type=""hidden"" name=""w"" value=""(?<rsFormW>[^""]+)""\s+/>\s+<input type=""hidden"" name=""formSetup"" value=""(?<rsFormFormSetup>[^""]+)""\s+/>", RegexOptions.IgnoreCase);
        private static Regex encryptSessionIdRegex = new Regex(@"dwr.engine._origScriptSessionId\s+=\s+""(?<encryptSessionId>[^""]+)"";");
        private static Regex fpassRegex = new Regex(@"dwr.engine._remoteHandleCallback\(\'0\'\,\'0\'\,""(?<fpass>[^""]+)""");

		public FAMC(IDownloadInfo info) : base(info) { }

		public override string BotName 
		{
			get { return "FAMC" ; }
		}

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
			const string sHostUrl = "https://lendingpartners.franklinamerican.com/ext/" ;
			string sLoginUrl = string.Format("{0}general?npage=checkLockOnLogin", sHostUrl) ;
            string sSecurityCheck = string.Format("{0}j_security_check", sHostUrl);
            string sLogOutUrl = string.Format("{0}general?npage=home&logout=1", sHostUrl);
			string sRSHelper = "correspondent?npage=ratesChoose";

            // if its before 6am or after 6pm, we're not gonna download the RS. 
            if ((DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(6)) < 0) || (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(18)) > 0))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            // if its Saturday or Sunday, we're not gonna download the RS. 
            if ((DateTime.Now.DayOfWeek == DayOfWeek.Saturday) || (DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            if (dlinfo.Description.Contains("WHOLESALE"))
            {
                sRSHelper = "wholesale?npage=ratesChoose";
            }

			string sRSPage = string.Format("{0}{1}", sHostUrl, sRSHelper) ;
            string sRSKey = string.Format("{0}&date=", sRSHelper);
            string homeUrl = string.Format("{0}general?npage=home", sHostUrl);
            string encryptSessionUrl = string.Format("{0}dwr/engine.js", sHostUrl);
            string encryptPasswordUrl = string.Format("{0}dwr/call/plaincall/dwr.encryptValue.dwr", sHostUrl);
            string sFile = null;

            GoToUrl(sHostUrl);
            GoToUrl(homeUrl);

            string httpSessionId = GetCookieValue(sHostUrl + "general?npage=home", "JSESSIONID");
            
            //Get Encrypt Session Id
            string encryptSessionTemp = DownloadData(encryptSessionUrl, homeUrl);
            Match encryptSessionIdMatch = encryptSessionIdRegex.Match(encryptSessionTemp);
            if (encryptSessionIdMatch.Success)
            {
                string encryptId = encryptSessionIdMatch.Groups["encryptSessionId"].Value;
                encryptId = encryptId + "145";

                GoToUrl(sHostUrl + "dwr/interface/dwr.js", homeUrl);

                //construct Header collection for encrypting password
                NameValueCollection headersCollection = new NameValueCollection();
                headersCollection.Add("Origin", @"https://www.franklinamerican.com");
                headersCollection.Add("Accept-Encoding", "gzip,deflate");
                headersCollection.Add("Accept-Language", "en-US,en;q=0.8");

                string encryptPassString = "callCount=1\r\npage=/ext/general?npage=home\r\nhttpSessionId=" + httpSessionId + "\r\nscriptSessionId=" + encryptId + "\r\nc0-scriptName=dwr\r\nc0-methodName=encryptValue\r\nc0-id=0\r\nc0-param0=string:" + dlinfo.Password + "\r\nbatchId=0";

                //Retrieve the encrypted password
                string fpassTemp = RawUploadDataAndReturnResponse(encryptPasswordUrl, "text/plain", encryptPassString, headersCollection, sHostUrl + "general?npage=home");
                Match fpassMatch = fpassRegex.Match(fpassTemp);
                if (fpassMatch.Success)
                {
                    string fpassKey = fpassMatch.Groups["fpass"].Value;
                    NameValueCollection nvcPostData = new NameValueCollection();

                    nvcPostData.Add("fuser", dlinfo.Login);
                    nvcPostData.Add("fpassEntry", "");
                    nvcPostData.Add("fpass", fpassKey);

                    UploadData(sLoginUrl, nvcPostData);

                    // Cookie
                    GoToUrl(string.Format("{0}welcome", sHostUrl));

                    nvcPostData = new NameValueCollection();
                    nvcPostData.Add("j_username", dlinfo.Login);
                    nvcPostData.Add("j_password", fpassKey);
                    nvcPostData.Add("numOfLoginAttempt", "0");

                    string temp = UploadDataReturnResponse(sSecurityCheck, nvcPostData);

                    //if it detects login failure, login will be deactivated
                    Match loginFailedMatch = loginFailedRegex.Match(temp);
                    if (loginFailedMatch.Success)
                    {
                        dlinfo.Deactivate();
                        ReportDeactivation("Login to FAMC's website failed for login " + dlinfo.Login + ". Please update the password and re-enable the login in downloadlist");
                    }
                    else
                    {
                        // Download Correspondent rate sheet listing
                        if (dlinfo.Description == "FAMC_CORR")
                        {
                            string sHtml = DownloadData(sRSPage);
                            sHtml = sHtml.Replace("\r\n", "");
                            string sPattern = string.Format(@"Please\s+select\s+your\s+rate\s+sheet\.</i><br><br>\s+<table\s+border=""[0-9]""\s+cellpadding=""[0-9]""\s+cellspacing=""[0-9]"">\s+<tr>\s+<td\s+nowrap><a\s+href=""(?<rslink>[^""]+)""\s+target=""[^""]+"">" + DateTime.Today.DayOfWeek + DateTime.Today.Date.ToString(", MMMM dd"));
                            Regex rMyRegex = new Regex(sPattern, RegexOptions.IgnoreCase);
                            Match mFindRS;
                            mFindRS = rMyRegex.Match(sHtml);

                            // Download today's rates
                            if (mFindRS.Success)
                            {
                                /*sFile = string.Format(mFindRS.Groups["rslink"].Value);
                                sFile = DownloadFile(sFile);*/
                                sHtml = DownloadData(mFindRS.Groups["rslink"].Value);
                                Match excelFindRs = rsFormRegex.Match(sHtml);
                                if (excelFindRs.Success)
                                {
                                    string excelRs = sHostUrl + @"rateSheet?email=" + excelFindRs.Groups["rsFormEmail"].Value + @"&id=" + excelFindRs.Groups["rsFormId"].Value + @"&npage=excel&date=" + excelFindRs.Groups["rsFormDate"].Value + @"&s=" + excelFindRs.Groups["rsFormS"].Value + @"&mobile=&w=" + excelFindRs.Groups["rsFormW"].Value + @"&formSetup=" + excelFindRs.Groups["rsFormFormSetup"].Value;
                                    sFile = DownloadFile(excelRs);
                                }
                                else
                                    LogErrorAndSendEmailToAdmin(string.Format("ERROR - FAMC can't find the necessary information for excel rate sheet link.\nThe following was returned by their server for login {0}:\n{1}", dlinfo.Login, sHtml));
                            }
                            else
                            {
                                Match mFindProblem = AuthFailedRegex.Match(sHtml);
                                if (mFindProblem.Success)
                                    LogErrorAndSendEmailToAdmin(string.Format("ERROR - Login to FAMC's website failed for login {0}. SAE please contact client given by: {1}\nThe following was returned by their server:\n{2}", dlinfo.Login, dlinfo.Description, sHtml));
                                else if (sHtml.IndexOf("today's rates have not yet been posted") < 0)
                                    LogErrorAndSendEmailToAdmin(string.Format("ERROR - FAMC's rate sheet was not found.\nThe following was returned by their server for login {0}:\n{1}", dlinfo.Login, sHtml));
                            }
                        }
                        //Download Wholesale ratesheet listing
                        else if (dlinfo.Description.Contains("FAMC_WHOLESALE"))
                        {
                            string sHtml = DownloadData(sHostUrl + "wholesale?npage=ratesChoose");
                            // remove line terminators because '.' doesn't recognize line terminators
                            sHtml = sHtml.Replace("\r\n", "");
                            sHtml = sHtml.Replace("\n", "");

                            string rsStringTemp = null;

                            // decides whether we download the EMB rate sheet or the Broker rate sheet
                            if (dlinfo.Description.Contains("EMB"))
                            {
                                // Find "EMB Rate Sheets" then parse HTML until we find first instance of "<td" then get RS URL
                                rsStringTemp = @"<th>\s+EMB(\s)?Rate\s+Sheets</th>.+?(?=<td)<td\s+nowrap><a href=""(?<rsUrl>[^""]+)""\s+target=""_blank"">" + DateTime.Today.DayOfWeek + ", " + DateTime.Today.ToString("MMMM dd");
                            }
                            else
                            {
                                // Find "Broker Rate Sheets" then parse HTML until we find first instance of "<td" then get RS URL
                                rsStringTemp = @"<th>\s+Broker(\s)?Rate\s+Sheets</th>.+?(?=<td)<td\s+nowrap><a href=""(?<rsUrl>[^""]+)""\s+target=""_blank"">" + DateTime.Today.DayOfWeek + ", " + DateTime.Today.ToString("MMMM dd");
                            }

                            Regex rs_regex = new Regex(rsStringTemp,RegexOptions.IgnoreCase);
                            Match match = rs_regex.Match(sHtml);

                            // Download today's rates
                            if (match.Success)
                            {
                                string rsTemp = DownloadData(match.Groups["rsUrl"].Value);

                                //grab all the necessary information in order to download csv ratesheet
                                Regex csvRS_regex = new Regex(@"<input type=""hidden""\s+name=""id""\s+value=""(?<idCSV>[^""]+)""\s+/>\s+<input type=""hidden""\s+name=""npage""\s+value=""""\s+/>\s+<input type=""hidden""\s+name=""s""\s+value=""(?<sCSV>[^""]+)""\s+/>\s+<input type=""hidden""\s+name=""mobile""\s+value=""""\s+/>\s+<input type=""hidden""\s+name=""fullRateSheetLink""\s+value="""">\s+<input type=""hidden""\s+name=""date""\s+value=""(?<dateCSV>[^""]+)""\s+/>\s+<input type=""hidden""\s+name=""w""\s+value=""(?<wCSV>[^""]+)""\s+/>\s+<input type=""hidden""\s+name=""view""\s+value=""(?<viewCSV>[^""]+)""\s+/>");
                                Match csvRSMatch = csvRS_regex.Match(rsTemp);
                                if (csvRSMatch.Success)
                                {
                                    //we can either grab the csv flatfile or download the website itself that FAMC has a full rs, which doeesn't format as pretty, but has the adjustments
                                    if (dlinfo.Description.Contains("CSV"))
                                    {
                                        sFile = string.Format(sHostUrl + @"rateSheet?email=" + dlinfo.Login + @"&emb=null&id=" + csvRSMatch.Groups["idCSV"].Value + @"&npage=csv&s=" + csvRSMatch.Groups["sCSV"].Value + @"&mobile=&date=" + csvRSMatch.Groups["dateCSV"].Value + @"&w=" + csvRSMatch.Groups["wCSV"].Value + @"&view=" + csvRSMatch.Groups["viewCSV"].Value);
                                    }
                                    else
                                    {
                                        sFile = string.Format(sHostUrl + @"rateSheet?email=" + dlinfo.Login + @"&s=" + csvRSMatch.Groups["sCSV"].Value + @"&date=" + csvRSMatch.Groups["dateCSV"].Value + @"&w=" + csvRSMatch.Groups["wCSV"].Value);
                                    }
                                    //////////////////////////

                                    sFile = DownloadFile(sFile);
                               
                                }
                                else
                                    LogErrorAndSendEmailToAdmin(string.Format("ERROR - FAMC's CSV rate sheet was not found.\nThe following was returned by their server for login {0}:\n{1}", dlinfo.Login, rsTemp));
                            }
                            else
                            {
                                Match mFindProblem = AuthFailedRegex.Match(sHtml);
                                if (mFindProblem.Success)
                                    LogErrorAndSendEmailToAdmin(string.Format("ERROR - Login to FAMC's website failed for login {0}. SAE please contact client given by: {1}\nThe following was returned by their server:\n{2}", dlinfo.Login, dlinfo.Description, sHtml));
                                else if (sHtml.IndexOf("today's rates have not yet been posted") < 0)
                                    LogErrorAndSendEmailToAdmin(string.Format("ERROR - FAMC's rate sheet was not found.\nThe following was returned by their server for login {0}:\n{1}", dlinfo.Login, sHtml));
                            }
                        }
                    }
                }
                else
                    LogErrorAndSendEmailToAdmin("FAMCBot failed to retrieve fpass for login: " + dlinfo.Login + ". Content: " + fpassTemp);
            }
            else
                LogErrorAndSendEmailToAdmin("FAMCBot failed to retrieve encrypt Session Id for login: " + dlinfo.Login + ". Content: " + encryptSessionTemp);


			GoToUrl(sLogOutUrl) ;

            if (null != sFile)
            {
                RatesheetFileName = sFile;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }
		}
	}
}