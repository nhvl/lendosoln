﻿namespace LpeUpdateBotLib.WebBots
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using LpeUpdateBotLib.Common;
    using WinSCP;

    public class PacUnionZipBot : AbstractWebBot
    {
        public PacUnionZipBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "PACUNIONZIP"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            try
            {
                string lastDownloadFolder = Path.Combine(Config.FILES_PATH, "PacUnionZip_lastDownload"); // dd
                string currentDownloadFolder = Path.Combine(Config.FILES_PATH, "PacUnionZip_currentDownload"); // dd

                //Check if folder already exists already or not. If not, create folder then copy all old ratesheets to LastDownload folder
                if (!Directory.Exists(lastDownloadFolder))
                    Directory.CreateDirectory(lastDownloadFolder);

                if (!Directory.Exists(currentDownloadFolder))
                    Directory.CreateDirectory(currentDownloadFolder);

                if (Directory.GetFiles(currentDownloadFolder).Length != 0)
                {
                    foreach (var file in Directory.GetFiles(currentDownloadFolder))
                        File.Copy(file, Path.Combine(lastDownloadFolder, Path.GetFileName(file)), true);
                    /*
                                            FileOperationHelper.Copy(file, Path.Combine("PacUnion_lastDownload", Path.GetFileName(file)), true);
                    */
                }

                //Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    FtpSecure = FtpSecure.Implicit,
                    HostName = "mrcsf.sharefileftp.com",
                    UserName = dlinfo.Login,
                    Password = dlinfo.Password,
                    PortNumber = Convert.ToInt32("990"),
                    SslHostCertificateFingerprint = "18:38:6b:42:7d:f9:54:fb:50:73:03:c1:84:3f:4a:3a:70:fd:48:14"
                };

                using (Session session = new Session())
                {
                    //List of files path to be downloaded
                    string[,] clientFilesPath = new string[,]
                    {{@"/RateSheets/PML/PMLFiles.zip", Path.GetFullPath(currentDownloadFolder) + @"\PML0176_CUSTOM_CORR_v2_BASE.zip"}};

                    //Connect
                    session.Open(sessionOptions);

                    //Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Automatic;
                    transferOptions.PreserveTimestamp = false;

                    for (int i = 0; i < clientFilesPath.GetLength(0); i++)
                    {
                        int j = 0;
                        try
                        {
                            TransferOperationResult transferResult;
                            transferResult = session.GetFiles(clientFilesPath[i, j], clientFilesPath[i, j + 1], false, transferOptions);

                            // Throw on any error
                            transferResult.Check();
                        }
                        catch (Exception e)
                        {
                            LogErrorAndSendEmailToAdmin("PacUnion bot failed to download ratesheet for: " + clientFilesPath[i, j + 1] + "Exception: " + e);
                        }
                    }
                }

                if (Directory.GetFiles(lastDownloadFolder).Length == 0)
                {
                    foreach (var file in Directory.GetFiles(currentDownloadFolder))
                        File.Copy(file, PathCfg.BOT_DOWNLOAD_PATH + Path.GetFileName(file), true);
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }

                bool isNewRatesheet = IsNewRatesheet(lastDownloadFolder, currentDownloadFolder);
                if (isNewRatesheet)
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                else
                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }
            catch (Exception e)
            {
                LogErrorAndSendEmailToAdmin("PacUnionZip bot failed to download ratesheet for: " + dlinfo.Login + ". Content: " + e);
                return E_BotDownloadStatusT.Error;
            }
        }

        /// <summary>
        /// 9/14/2015 BS - Do a binary comparison between 2 files
        /// </summary>
        /// <returns>True if the files are identical</returns>
        protected static bool IsFileIdentical(string sFile1, string sFile2)
        {
            if (!File.Exists(sFile1) || !File.Exists(sFile2)) return false;
            /*
                        if (!FileOperationHelper.Exists(sFile1) || !FileOperationHelper.Exists(sFile2)) return false;
            */

            const int BUFSIZE = 1024;
            byte[] buf1 = new byte[BUFSIZE];
            byte[] buf2 = new byte[BUFSIZE];

            using (FileStream fs1 = new FileStream(sFile1, FileMode.Open))
            {
                using (FileStream fs2 = new FileStream(sFile2, FileMode.Open))
                {
                    while (true)
                    {
                        int n1 = fs1.Read(buf1, 0, BUFSIZE);
                        int n2 = fs2.Read(buf2, 0, BUFSIZE);

                        if (n1 != n2) return false;
                        if (n1 == 0) return true;

                        for (int i = 0; i < n1; i++)
                            if (buf1[i] != buf2[i])
                                return false;
                    }
                }
            }
        }

        /// <summary>
        /// 9/14/2015 BS - Compare each ratesheet between lastDownload and currentDownload
        /// </summary>
        /// <returns>If it's new, then it will copy new ratesheet to DownloadPath</returns>
        protected static bool IsNewRatesheet(string lastDownload, string currentDownload)
        {
            //Compare new and old ratesheet to see if current ratesheet is new
            var lastDownloadFiles = Directory.GetFiles(lastDownload);
            var currentDownloadFiles = Directory.GetFiles(currentDownload);
            bool newRatesheet = false;

            foreach (string lastDownloadFile in lastDownloadFiles)
            {
                string fileName = lastDownloadFile.Substring(lastDownload.Length + 1);
                if (File.Exists(Path.Combine(currentDownload, fileName)))
                /*
                                if (FileOperationHelper.Exists(Path.Combine(currentDownload, fileName)))
                */
                {
                    bool isIdenticalRatesheet = IsFileIdentical(lastDownloadFile, Path.Combine(currentDownload, fileName));
                    if (!isIdenticalRatesheet)
                    {
                        File.Copy(Path.Combine(currentDownload, fileName), PathCfg.BOT_DOWNLOAD_PATH + "/" + fileName, true);
                        /*
                                                FileOperationHelper.Copy(Path.Combine(currentDownload, fileName), PathCfg.BOT_DOWNLOAD_PATH + "/" + fileName, true);
                        */
                        newRatesheet = true;
                    }
                }
            }


            //if the ratesheet is new and without any previous history about it, then we copy it directly to DownloadPath
            foreach (string currentDownloadFile in currentDownloadFiles)
            {
                string tempName = currentDownloadFile.Substring(currentDownload.Length + 1);
                if (!File.Exists(Path.Combine(lastDownload, tempName)))
                {
                    File.Copy(tempName, PathCfg.BOT_DOWNLOAD_PATH + @"\" + tempName, true);
                    newRatesheet = true;
                }
                /*
                                if (!FileOperationHelper.Exists(Path.Combine(lastDownload, tempName)))
                                {
                                    FileOperationHelper.Copy(tempName, PathCfg.BOT_DOWNLOAD_PATH + @"\" + tempName, true);
                                    newRatesheet = true;
                                }
                */
            }

            return newRatesheet;
        }
    }
}