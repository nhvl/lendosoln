﻿// <copyright file="SonymaBot.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/31/2014 11:33:03 PM 
// </summary>
namespace LpeUpdateBotLib.WebBots
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LpeUpdateBotLib.Common;

    /// <summary>
    /// Extract the table from url http://www.nyshcr.org/Topics/Home/Buyers/CurrentInterestRatesfor30-YearMortgages.htm
    /// </summary>
    public class SonymaBot : AbstractWebBot
    {
        private const string Url = "http://www.nyshcr.org/Topics/Home/Buyers/CurrentInterestRatesfor30-YearMortgages.htm";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        public SonymaBot(IDownloadInfo info)
            : base(info)
        {
        }

        /// <summary>
        /// Gets the name of the bot
        /// </summary>
        public override string BotName
        {
            get { return "SONYMA"; }
        }

        /// <summary>
        /// Extract the rate table from the main html and save into a simplify html.
        /// </summary>
        /// <param name="dlinfo"></param>
        /// <returns></returns>
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // 6/1/2014 dd - For some unknown reason, HtmlToExcel conversion will cause Excel to hang when convert this page to Excel.
            // As a work around, I strip out all irrelevant html and only keep the essential data (rate table) .

            string html = DownloadData(Url);

            // Find the first table. Currently the rate table is contains in the first <table>
            int startIndex = html.IndexOf("<table");
            if (startIndex < 0)
            {
                LogErrorAndSendEmailToAdmin("[" + this.BotName + " Bot] : Could not locate <table> tag.");
                return E_BotDownloadStatusT.Error;
            }

            int endIndex = html.IndexOf("</table>", startIndex);
            if (endIndex < 0)
            {
                LogErrorAndSendEmailToAdmin("[" + this.BotName + " Bot] : Could not locate </table> tag.");
                return E_BotDownloadStatusT.Error;
            }

            string extractHtml = html.Substring(startIndex, endIndex - startIndex + "</table>".Length);

            string simplifyHtml = "<html><body>" + extractHtml + "</body></html>";

            RatesheetFileName = WriteToTempFile(simplifyHtml);
            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
        }
    }
}