﻿//Author: Budi Sulayman

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class AmerisaveBot : AbstractWebBot
    {
        private static string baseUrl = @"https://secure.amerisavetpo.com";
        private static string ajaxUrl = baseUrl + @"/resources/ajax/ajaxPanels.cfc";
        private static string loginUrl = baseUrl + @"/login/";
        
        private static Regex acctIdHash_regex = new Regex(@"a\s+href=""/\?taccountidhash=(?<acctIdHash>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex ratesArchive_regex = new Regex(@"a\s+href=""(?<rateArchive>[^""]+)"">\s+<img\s+src=""[^""]+"">\s+<span>Rates\s+Archive", RegexOptions.IgnoreCase);
        //private Regex wholesaleRatesheet_regex = new Regex(@"""nDocumentId"":""(?<docId>[^""]+)"",""tFundingDescription"":""wholesale"",""tDocumentKey"":""(?<docKey>[^""]+)""", RegexOptions.IgnoreCase);
        private Regex wholesaleRatesheet_regex = new Regex(@"""tEnteredBy"":""(?<enteredName>[^""]+)"",""nDocumentId"":""(?<docId>[^""]+)"",""nId"":""(?<nId>[^""]+)"",""tPdfDocumentKey"":""(?<pdfId>[^""]*)"",""tFundingDescription"":""Wholesale"",""tPricingName"":""Gold"",""tDocumentKey"":""(?<docKey>[^""]+)""", RegexOptions.IgnoreCase);
        //private Regex miniCRatesheet_regex = new Regex(@"""nDocumentId"":""(?<docId>[^""]+)"",""tFundingDescription"":""Mini-Correspondent"",""tDocumentKey"":""(?<docKey>[^""]+)""", RegexOptions.IgnoreCase);
        private Regex miniCRatesheet_regex = new Regex(@"""tEnteredBy"":""(?<enteredName>[^""]+)"",""nDocumentId"":""(?<docId>[^""]+)"",""tPdfDocumentKey"":"""",""tFundingDescription"":""Mini-Correspondent"",""tDocumentKey"":""(?<docKey>[^""]+)""", RegexOptions.IgnoreCase);

        public AmerisaveBot(IDownloadInfo info) : base(info)
        {
        }

        public override string BotName
        {
            get { return "AMERISAVE"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null;

            GoToUrl(loginUrl);

            string c0param0 = @"string:<wddxPacket version='1.0'><header/><data><struct><var name='page'><string>login</string></var><var name='layout'><string>_layout</string></var><var name='switch'><string>_switch</string></var><var name='folder'><string>PUBLIC/auth</string></var><var name='cfc'><string>WEB-INF.PUBLIC.auth.Login</string></var><var name='action'><string></string></var></struct></data></wddxPacket>";

                        
            NameValueCollection postData = new NameValueCollection();
            postData.Add("method","init");
            postData.Add("callCount","1");
            postData.Add("c0-scriptName","null");
            postData.Add("c0-methodName","loadCfc");
            postData.Add("c0-id", "1282_1418161442335");
            postData.Add("c0-param0",c0param0);
            postData.Add("xml","true");
            postData.Add("","");


            UploadData(ajaxUrl, postData);


            c0param0 = @"string:<wddxPacket version='1.0'><header/><data><struct><var name='tusername'><string>" + dlinfo.Login + "</string></var><var name='tpassword'><string>" + dlinfo.Password + "</string></var><var name=''><string></string></var><var name='cfc'><string>WEB-INF.PUBLIC.auth.Login</string></var><var name='action'><string>login</string></var></struct></data></wddxPacket>";


            postData = new NameValueCollection();
            postData.Add("method", "init");
            postData.Add("callCount", "1");
            postData.Add("c0-scriptName", "null");
            postData.Add("c0-methodName", "submitCfc");
            postData.Add("c0-id", "7279_1418161458766");
            postData.Add("c0-param0", c0param0);
            postData.Add("xml", "true");
            postData.Add("", "");


            UploadData(ajaxUrl, postData);

            //hard coded for PML0165 login "sistarmtg@pricemyloan.com"
            c0param0 = @"string:<wddxPacket version='1.0'><header/><data><struct><var name='tquestion1'><string>What color was your first car?</string></var><var name='tsecurityanswer1'><string>sipml</string></var><var name='tquestion2'><string>What color was your second car?</string></var><var name='tsecurityanswer2'><string>sipml</string></var><var name='tquestion3'><string>What's up?</string></var><var name='tsecurityanswer3'><string>sipml</string></var><var name=''><string></string></var><var name='cfc'><string>cfcs.CONDITIONAL.axs.AuthBrowser</string></var><var name='action'><string>answerquestions</string></var></struct></data></wddxPacket>";

            postData = new NameValueCollection();
            postData.Add("method", "init");
            postData.Add("callCount", "1");
            postData.Add("c0-scriptName", "null");
            postData.Add("c0-methodName", "submitCfc");
            postData.Add("c0-id", "7830_1427160863235");
            postData.Add("c0-param0", c0param0);
            postData.Add("xml", "true");
            postData.Add("", "");

            UploadData(ajaxUrl, postData);

            string temp = DownloadData(baseUrl + @"/home/");
            Match acctIdHash_match = acctIdHash_regex.Match(temp);
            
            if (acctIdHash_match.Success)
            {
                GoToUrl(baseUrl + @"/ratesheets/?taccountidhash=" + acctIdHash_match.Groups["acctIdHash"].Value);

                temp = DownloadData(@"https://atlas.amerisavetpo.com/" + acctIdHash_match.Groups["acctIdHash"].Value + @"/WholesaleRateSheet?taccountidhash=" + acctIdHash_match.Groups["acctIdHash"].Value);
                //Amerisave wholesale
                if (dlinfo.Description.Equals("AMERISAVE_WHOLESALE"))
                {
                    Match wholesaleRatesheet_match = wholesaleRatesheet_regex.Match(temp);
                   
                    if (wholesaleRatesheet_match.Success)
                        fileName = DownloadFile(@"https://secure.amerisavetpo.com/resources/ajax/ajaxPanels.cfc?method=downloadCfc&cfc=tools.cfcs.dms.dmsGenericDownload&nDocumentId=" + wholesaleRatesheet_match.Groups["docId"].Value + @"&tDocumentKey=" + wholesaleRatesheet_match.Groups["docKey"].Value);
                    else
                        LogErrorAndSendEmailToAdmin("AmerisaveBot failed to find docId and docKey for Wholesale ratesheet. Content: " + temp);
                }

                //Amerisave Mini-Correspondnet
                else if (dlinfo.Description.Equals("AMERISAVE_MINIC"))
                {
                    
                    Match miniCRatesheet_match = miniCRatesheet_regex.Match(temp);
                    if (miniCRatesheet_match.Success)
                        fileName = DownloadFile(@"https://secure.amerisavetpo.com/resources/ajax/ajaxPanels.cfc?method=downloadCfc&cfc=tools.cfcs.dms.dmsGenericDownload&nDocumentId=" + miniCRatesheet_match.Groups["docId"].Value + @"&tDocumentKey=" + miniCRatesheet_match.Groups["docKey"].Value);
                        //fileName = DownloadFile(@"https://atlas.amerisavetpo.com/" + acctIdHash_match.Groups["acctIdHash"].Value + @"/io/?method=downloadCfc&cfc=WEB-INF.default.SECURE.crm.lockdesk.WholesaleRateSheetFinder&nDocumentId=" + miniCRatesheet_match.Groups["docId"].Value + @"&tDocumentKey=" + miniCRatesheet_match.Groups["docKey"].Value + @"&callback=callback&_ioaction_=downloadCfc&_nocache_=1418335567400");

                    else
                        LogErrorAndSendEmailToAdmin("AmerisaveBot failed to find docId and docKey for mini-Correspondent ratesheet. Content: " + temp);
                }
                else
                    LogErrorAndSendEmailToAdmin("Description for the Amerisave Download list entry is wrong. Please check");

                GoToUrl(@"https://secure.amerisavetpo.com/signout/?taccountidhash=" + acctIdHash_match.Groups["acctIdHash"].Value);
            }
            else
                LogErrorAndSendEmailToAdmin("AmerisaveBot failed to find acctIdHash. Content: " + temp);


            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}