﻿//Kelvin Young
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class ProvidentWholesaleBot : AbstractWebBot
    {
        private static string baseUrl = @"https://pfloans.provident.com/";
        private static string altbaseUrl = @"https://a.pfloans.provident.com/";
        private static string loginUrl = baseUrl + @"secure/login.aspx?returnUrl=%2fdefault.aspx%3fgo%3drates";
        private static Regex viewState_regex = new Regex(@"<input\s+type=""hidden""\s+name=""__VIEWSTATE""\s+id=""__VIEWSTATE""\s+value=""(?<viewStateValue>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex invalidLogin_regex = new Regex(@"Invalid\s+login\s+credentials|expired", RegexOptions.IgnoreCase);
        private static Regex eventValid_regex = new Regex(@"<input\s+type=""hidden""\s+name=""__EVENTVALIDATION""\s+id=""__EVENTVALIDATION""\s+value=""(?<eventValidValue>[^""]+)""", RegexOptions.IgnoreCase);
        private static string rsUrl = baseUrl + @"secure/rates/export.aspx?State=&grade=Gap&margin=&clp=false&retention=false";
        private static string lockPeriodUrl = baseUrl + @"secure/rates/LockPeriodAdjustments.aspx?clp=false";
        private static string logOutUrl = baseUrl + @"common/logout.aspx";
        private static string ConfLLPAUrl = baseUrl + @"secure/plus/AmountAdjReport.aspx?LoanProgramGroup=2&Product=A1010&clp=true";
        private static string NonConfLLPAUrl = baseUrl + @"secure/plus/AmountAdjReport.aspx?LoanProgramGroup=2&Product=J2030";

        public ProvidentWholesaleBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "PROVIDENTWHOLESALE"; }
        }

        protected override bool CopyCookiesFromResponse
        {
            get
            {
                return true;
            }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null;

            GoToUrl(baseUrl + "menu.aspx?go=");

            string mySessionId = GetCookieValue(altbaseUrl + "menu.aspx?go=", "mySession");
            SetCookie("pfloans.provident.com", "mySession", mySessionId);
            string viewStateTemp = DownloadData(baseUrl + "secure/login.aspx?returnUrl=/default.aspx?go=rates");

            Match match = viewState_regex.Match(viewStateTemp);
            Match eventValidMatch = eventValid_regex.Match(viewStateTemp);
            if ((match.Success) && (eventValidMatch.Success))
            {
                string viewState = match.Groups["viewStateValue"].Value;
                string eventValid = eventValidMatch.Groups["eventValidValue"].Value;

                NameValueCollection postData = new NameValueCollection();
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", viewState);
                postData.Add("__EVENTVALIDATION", eventValid);
                postData.Add("txtUserName", dlinfo.Login);
                postData.Add("txtPassword", dlinfo.Password);
                postData.Add("btnLogin", "Login");

                AllowAutoRedirect = false;

                string temp = UploadDataReturnResponse(loginUrl, postData, @"https://pfloans.provident.com/secure/login.aspx?returnUrl=/default.aspx?go=rates");

                mySessionId = GetCookieValue(altbaseUrl + "secure/login.aspx?returnUrl=/default.asp?go=rates", "mySession");
                SetCookie("pfloans.provident.com", "mySession", mySessionId);

                AllowAutoRedirect = true;

                GoToUrl(baseUrl + "menu.aspx?go=rates");

                match = invalidLogin_regex.Match(temp);
                if (match.Success)
                {
                    dlinfo.Deactivate();
                    ReportDeactivation("Provident login is not working and it's been disabled in download list. Please update Provident login and re-enable login for username: " + dlinfo.Login);
                }
                else
                {
                    GoToUrl(baseUrl + "secure/rates/Rates_New.aspx?");
                    //check description on which one to donwload (RS, ADJUSTMENT TABLE, SRP)
                    StringBuilder rsPage = new StringBuilder();
                    string[] descriptionTokens = RateSheetType.Split('_');
                    int descriptionTokensCount = descriptionTokens.Length;
                    if (descriptionTokensCount < 2)
                        LogErrorAndSendEmailToAdmin("Provident Bot: Invalid Ratesheet Type");
                    else
                    {
                        if (descriptionTokens[1] == "RS")
                            fileName = DownloadFile(rsUrl);
                        else if (descriptionTokens[1] == "LOCKS")
                        {
                            rsPage = LockPeriodParser(lockPeriodUrl);
                            fileName = WriteToTempFile(rsPage.ToString());
                        }
                        else if (descriptionTokens[1] == "CONFLLPA")
                            fileName = DownloadFile(ConfLLPAUrl);
                        else if (descriptionTokens[1] == "NONCONFLLPA")
                            fileName = DownloadFile(NonConfLLPAUrl);
                        else
                            LogErrorAndSendEmailToAdmin("Provident Bot: Invalid Ratesheet Type");
                    }
                    AllowAutoRedirect = false;
                    GoToUrl(logOutUrl);
                }
            }

            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }

        private StringBuilder LockPeriodParser(string lockPeriodUrl)
        {
            string temp = DownloadData(lockPeriodUrl);
            StringBuilder rsPage = new StringBuilder();

            Regex day21Lock_regex = new Regex(@"21\s+day\s+lock\s+add\s+(?<dayLock21>[^\<]+)\<", RegexOptions.IgnoreCase);
            Regex day30Lock_regex = new Regex(@"30\s+day\s+lock\s+add\s+(?<dayLock30>[^\<]+)\<", RegexOptions.IgnoreCase);
            Regex day45Lock_regex = new Regex(@"45\s+day\s+lock\s+add\s+(?<dayLock45>[^\<]+)\<", RegexOptions.IgnoreCase);
            Regex day60Lock_regex = new Regex(@"60\s+day\s+lock\s+\(purchase\s+only\)\s+add\s+(?<dayLock60>[^\<]+)\<", RegexOptions.IgnoreCase);

            //Getting 21 day lock adjustments
            Match lockDayMatch = day21Lock_regex.Match(temp);
            if (lockDayMatch.Success)
                rsPage.AppendLine("21 day lock add, " + lockDayMatch.Groups["dayLock21"].Value);
            else
                rsPage.AppendLine("21 day lock add, Error getting 21 day lock adjustment\n");
            
            //Getting 30 day lock adjustments
            lockDayMatch = day30Lock_regex.Match(temp);
            if (lockDayMatch.Success)
                rsPage.AppendLine("30 day lock add, " + lockDayMatch.Groups["dayLock30"].Value);
            else
                rsPage.AppendLine("30 day lock add, Error getting 30 day lock adjustment\n");

            //Getting 45 day lock adjustments
            lockDayMatch = day45Lock_regex.Match(temp);
            if (lockDayMatch.Success)
                rsPage.AppendLine("45 day lock add, " + lockDayMatch.Groups["dayLock45"].Value);
            else
                rsPage.AppendLine("45 day lock add, Error getting 45 day lock adjustment\n");

            //Getting 60 day lock adjustments
            lockDayMatch = day60Lock_regex.Match(temp);
            if (lockDayMatch.Success)
                rsPage.AppendLine("60 day lock add, " + lockDayMatch.Groups["dayLock60"].Value);
            else
                rsPage.AppendLine("60 day lock add, Error getting 60 day lock adjustment\n");

            return rsPage;
        }
    }
}