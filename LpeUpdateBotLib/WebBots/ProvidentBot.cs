﻿//Author: Budi Sulayman

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class ProvidentBot : AbstractWebBot
    {
        private static string baseUrl = @"https://clp.provident.com/";
        private static string altbaseUrl = @"https://a.clp.provident.com/";
        private static string loginUrl = baseUrl + @"secure/login.aspx?clp=true";
        //private static Regex viewState_regex = new Regex(@"<input\s+type=""hidden""\s+name=""__VIEWSTATE""\s+id=""__VIEWSTATE""\s+value=""(?<viewStateValue>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex viewState_regex = new Regex(@"<input\s+type=""hidden""\s+name=""__VIEWSTATE""\s+id=""__VIEWSTATE""\s+value=""(?<viewStateValue>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex invalidLogin_regex = new Regex(@"Invalid\s+login\s+credentials", RegexOptions.IgnoreCase);
        private static string rsUrl = baseUrl + @"secure/rates/export.aspx?State=&grade=Gap&margin=&clp=true&retention=false";
        private static string lockPeriodUrl = baseUrl + @"secure/rates/LockPeriodAdjustments.aspx?clp=true";
        private static string logOutUrl = baseUrl + @"common/logout.aspx";
        private static string ConfLLPAUrl = baseUrl + @"secure/plus/AmountAdjReport.aspx?LoanProgramGroup=2&clp=true";

        public ProvidentBot(IDownloadInfo info): base(info)
        {
        }

        public override string BotName
        {
            get { return "PROVIDENT"; }
        }

        protected override bool CopyCookiesFromResponse
        {
            get
            {
                return true;
            }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null;
            
            GoToUrl(baseUrl);

            string mySessionId = GetCookieValue(altbaseUrl + "menu.aspx?go=", "mySession");
            SetCookie("clp.provident.com", "mySession", mySessionId);
            string viewStateTemp = DownloadData("https://pfloans.provident.com/secure/profile/login_msg.aspx?clp=true");

            Match match = viewState_regex.Match(viewStateTemp);
            if (match.Success)
            {
                string viewState = match.Groups["viewStateValue"].Value;
                viewState = match.Groups["viewStateValue"].Value;
                NameValueCollection postData = new NameValueCollection();
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", viewState);
                //sometimes need to comment out the next EVENTVAILDATION
                postData.Add("__EVENTVALIDATION", "/wEWBgLS+9/uDgKl1bKzCQK1qbSRCwLmhIGYCAKgt7D9CgKC3IeGDDfCalGy4Aq30wg43BKhV2I07fsw"); 
                postData.Add("txtUserName", dlinfo.Login);
                postData.Add("txtPassword", dlinfo.Password);
                postData.Add("btnLogin", "Login");

                string temp = UploadDataReturnResponse(loginUrl, postData);

                //GoToUrl(baseUrl + "default_clp.aspx");
                mySessionId = GetCookieValue(altbaseUrl + "default_clp.aspx", "mySession");
                SetCookie("clp.provident.com", "mySession", mySessionId);

                match = invalidLogin_regex.Match(temp);
                if (match.Success)
                {
                    dlinfo.Deactivate();
                    ReportDeactivation("Provident login is not working and it's been disabled in download list. Please update Provident login and re-enable login for username: " + dlinfo.Login);
                }
                else
                {
                    GoToUrl(baseUrl + "secure/rates/Rates_New.aspx?");
                    //check description on which one to donwload (RS, ADJUSTMENT TABLE, SRP)
                    StringBuilder rsPage = new StringBuilder();
                    string[] descriptionTokens = RateSheetType.Split('_');
                    int descriptionTokensCount = descriptionTokens.Length;
                    if (descriptionTokensCount < 2)
                        LogErrorAndSendEmailToAdmin("Provident Bot: Invalid Ratesheet Type");
                    else
                    {
                        if (descriptionTokens[1] == "RS")
                            fileName = DownloadFile(rsUrl);
                        else if (descriptionTokens[1] == "LOCKS")
                        {
                            rsPage = LockPeriodParser(lockPeriodUrl);
                            fileName = WriteToTempFile(rsPage.ToString());
                        }
                        else if (descriptionTokens[1] == "LLPA")
                            fileName = DownloadFile(ConfLLPAUrl);
                        else
                            LogErrorAndSendEmailToAdmin("Provident Bot: Invalid Ratesheet Type");
                    }
                    GoToUrl(logOutUrl);
                }
            }
            else
                LogErrorAndSendEmailToAdmin("Provident Bot: Can't find viewState on https://clp.provident.com/");

            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }

        private StringBuilder LockPeriodParser(string lockPeriodUrl)
        {
            string temp = DownloadData(lockPeriodUrl);
            StringBuilder rsPage = new StringBuilder();

            Regex day30Lock_regex = new Regex(@"30\s+day\s+lock\s+add\s+(?<dayLock30>[^\<]+)\<",RegexOptions.IgnoreCase);
            Regex day45Lock_regex = new Regex(@"45\s+day\s+lock\s+add\s+(?<dayLock45>[^\<]+)\<", RegexOptions.IgnoreCase);
            Regex day60Lock_regex = new Regex(@"60\s+day\s+lock\s+add\s+(?<dayLock60>[^\<]+)\<", RegexOptions.IgnoreCase);

            //Getting 30 day lock adjustments
            Match lockDayMatch = day30Lock_regex.Match(temp);
            if (lockDayMatch.Success)
                rsPage.AppendLine("30 day lock add, " + lockDayMatch.Groups["dayLock30"].Value);
            else
                rsPage.AppendLine("30 day lock add, Error getting 30 day lock adjustment\n");

            //Getting 45 day lock adjustments
            lockDayMatch = day45Lock_regex.Match(temp);
            if (lockDayMatch.Success)
                rsPage.AppendLine("45 day lock add, " + lockDayMatch.Groups["dayLock45"].Value);
            else
                rsPage.AppendLine("45 day lock add, Error getting 45 day lock adjustment\n");

            //Getting 60 day lock adjustments
            lockDayMatch = day60Lock_regex.Match(temp);
            if (lockDayMatch.Success)
                rsPage.AppendLine("60 day lock add, " + lockDayMatch.Groups["dayLock60"].Value);
            else
                rsPage.AppendLine("60 day lock add, Error getting 60 day lock adjustment\n");

            return rsPage;
        }
    }
}