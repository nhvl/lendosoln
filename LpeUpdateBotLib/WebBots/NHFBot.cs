﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots
{
    public class NHFBot : AbstractWebBot
    {
        static string url = @"https://nhfresportal.nhfloan.org/login.aspx";

        public NHFBot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "NHF"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = DownloadFile(url);

            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}