/// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
	public class RadianBot : AbstractWebBot
	{
        private static string baseUrl = @"http://www.radian.biz/";
        private static string guidesAndRatesUrl = baseUrl + @"apex/VFPageProxy";
        private static string cuBaseUrl = @"http://www.radiancu.biz/";
        private static string cuGuidesAndRatesUrl = cuBaseUrl + @"apex/VFPageProxy";

        private static string tempDate = @"</a></td><td class=""[^""]+"">\s+(?<date>[0-9/]+)";
        private static Regex matriceDate_regex = new Regex(@"Eligibility Matrices" + tempDate, RegexOptions.IgnoreCase);
        private static Regex uwDate_regex = new Regex(@"Underwriting Guidelines" + tempDate, RegexOptions.IgnoreCase);
        private static Regex BPMI_regex = new Regex(@"BPMI Prime Rates" + tempDate, RegexOptions.IgnoreCase);
        private static Regex NMBPMI_regex = new Regex(@"BPMI Prime Rates - New Mexico" + tempDate, RegexOptions.IgnoreCase);
        private static Regex LPMI_regex = new Regex(@"LPMI Prime Rates" + tempDate, RegexOptions.IgnoreCase);
        private static Regex NMLPMI_regex = new Regex(@"LPMI Prime Rates - New Mexico" + tempDate, RegexOptions.IgnoreCase);
        private static Regex splitEdge_regex = new Regex(@"SplitEdge Rates" + tempDate, RegexOptions.IgnoreCase);
        private static Regex aMinus_regex = new Regex(@"A Minus BPMI and LPMI rates" + tempDate, RegexOptions.IgnoreCase);
        private static Regex creditUnion_regex = new Regex(@"Credit Union Rates" + tempDate, RegexOptions.IgnoreCase);
        private static Regex NMcreditUnion_regex = new Regex(@"Credit Union Rates - New Mexico" + tempDate, RegexOptions.IgnoreCase);

        List<string> listOfChanges = new List<string>();
        List<string> listOfRegexError = new List<string>();

		public RadianBot(IDownloadInfo info) : base(info)
		{
		}
		public override string BotName 
		{
			get { return "RADIAN"; }
		}
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            NameValueCollection data = new NameValueCollection();
            string temp = null;

            switch (dlinfo.Description)
            {
                case "RADIAN_STD":
                    string matricesDate = "04/14/14";
                    string uwDate = "04/14/14";
                    string BPMIRatesDate = "04/14/14";
                    string LPMIRatesDate = "04/14/14";
                    string splitEdgeRatesDate = "04/14/14";
                    string aMinusDates = "03/14/11";
                    
                    data.Add("vfpgname", "IncludeComponent");
                    data.Add("cname", "contentdocsrg");
                    data.Add("category", "std");

                    temp = UploadDataReturnResponse(guidesAndRatesUrl, data);

                    //Check Eligibility matrices date
                    Match matricesMatch = matriceDate_regex.Match(temp);
                    if (matricesMatch.Success)
                    {
                        if (!matricesMatch.Groups["date"].Value.Equals(matricesDate))
                            listOfChanges.Add("Eligibility Matrices");
                    }
                    else
                        listOfRegexError.Add("Eligibility Matrices");

                    //Check Underwriting guidelines date
                    Match uwMatch = uwDate_regex.Match(temp);
                    if (uwMatch.Success)
                    {
                        if (!uwMatch.Groups["date"].Value.Equals(uwDate))
                            listOfChanges.Add("Underwriting Guidelines");
                    }
                    else
                        listOfRegexError.Add("Underwriting Guidelines");

                    //Check BPMI Prime Rates date
                    Match bpmiMatch = BPMI_regex.Match(temp);
                    if (bpmiMatch.Success)
                    {
                        if (!bpmiMatch.Groups["date"].Value.Equals(BPMIRatesDate))
                            listOfChanges.Add("BPMI Prime Rates");
                    }
                    else
                        listOfRegexError.Add("BPMI Prime Rates");

                    //Check LPMI Prime Rates date
                    Match lpmiMatch = LPMI_regex.Match(temp);
                    if (lpmiMatch.Success)
                    {
                        if (!lpmiMatch.Groups["date"].Value.Equals(LPMIRatesDate))
                            listOfChanges.Add("LPMI Prime Rates");
                    }
                    else
                        listOfRegexError.Add("LPMI Prime Rates");

                    //Check SplitEdge Rates date
                    Match splitedgeMatch = splitEdge_regex.Match(temp);
                    if (splitedgeMatch.Success)
                    {
                        if (!splitedgeMatch.Groups["date"].Value.Equals(splitEdgeRatesDate))
                            listOfChanges.Add("SplitEdge Rates");
                    }
                    else
                        listOfRegexError.Add("SplitEdge Rates");

                    //Check A Minus BPMI and LPMI Rates date
                    Match aminusMatch = aMinus_regex.Match(temp);
                    if (aminusMatch.Success)
                    {
                        if (!aminusMatch.Groups["date"].Value.Equals(aMinusDates))
                            listOfChanges.Add("A Minus BPMI and LPMI Rates");
                    }
                    else
                        listOfRegexError.Add("A Minus BPMI and LPMI Rates");

                    break;

                case "RADIAN_CU":
                    string cuMatricesDate = "04/14/14";
                    string cuUwDate = "04/14/14";
                    string cuRatesDate = "12/02/13";
                    string cuSplitEdgeRatesDate = "04/14/14";
                    string cuAMinusDates = "03/14/11";

                    data = new NameValueCollection();
                    data.Add("vfpgname", "IncludeComponent");
                    data.Add("cname", "contentdocsrg");
                    data.Add("category", "std");
                    data.Add("cu", "cu");

                    temp = UploadDataReturnResponse(cuGuidesAndRatesUrl, data);

                    //Check Eligibility matrices date
                    Match cuMatricesMatch = matriceDate_regex.Match(temp);
                    if (cuMatricesMatch.Success)
                    {
                        if (!cuMatricesMatch.Groups["date"].Value.Equals(cuMatricesDate))
                            listOfChanges.Add("CU Eligibility Matrices");
                    }
                    else
                        listOfRegexError.Add("CU Eligibility Matrices");

                    //Check Underwriting guidelines date
                    Match cuUwMatch = uwDate_regex.Match(temp);
                    if (cuUwMatch.Success)
                    {
                        if (!cuUwMatch.Groups["date"].Value.Equals(cuUwDate))
                            listOfChanges.Add("CU Underwriting Guidelines");
                    }
                    else
                        listOfRegexError.Add("CU Underwriting Guidelines");

                    //Check Credit Union Rates date
                    Match creditUnionMatch = creditUnion_regex.Match(temp);
                    if (creditUnionMatch.Success)
                    {
                        if (!creditUnionMatch.Groups["date"].Value.Equals(cuRatesDate))
                            listOfChanges.Add("Credit Union Rates");
                    }
                    else
                        listOfRegexError.Add("Credit Union Rates");

                    //Check SplitEdge Rates date
                    Match cuSplitedgeMatch = splitEdge_regex.Match(temp);
                    if (cuSplitedgeMatch.Success)
                    {
                        if (!cuSplitedgeMatch.Groups["date"].Value.Equals(cuSplitEdgeRatesDate))
                            listOfChanges.Add("CU SplitEdge Rates");
                    }
                    else
                        listOfRegexError.Add("CU SplitEdge Rates");

                    //Check A Minus BPMI and LPMI Rates date
                    Match cuAMinusMatch = aMinus_regex.Match(temp);
                    if (cuAMinusMatch.Success)
                    {
                        if (!cuAMinusMatch.Groups["date"].Value.Equals(cuAMinusDates))
                            listOfChanges.Add("CU A Minus BPMI and LPMI Rates");
                    }
                    else
                        listOfRegexError.Add("CU A Minus BPMI and LPMI Rates");    

                    break;

                default:
                    LogErrorAndSendEmailToAdmin("Incorrect Radian Bot description. Please set the correct description. It's either RADIAN_STD or RADIAN_CU");
                    break;
            }

            if (listOfChanges.Count != 0)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine("Radian Bot detect changes on the following guidelines/rates dates. Please check the changes and update the bot, templates, and/or map: ");
                foreach (string change in listOfChanges)
                    errorMessage.AppendLine(change);
                LogErrorAndSendEmailToAdmin(errorMessage.ToString());
                return E_BotDownloadStatusT.Error;
            }
            else if (listOfRegexError.Count != 0)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine("Radian Bot detect changes on the following guidelines/rates regex. Please check the changes and update the bot: ");
                foreach (string regexError in listOfRegexError)
                    errorMessage.AppendLine(regexError);
                LogErrorAndSendEmailToAdmin(errorMessage.ToString());
                return E_BotDownloadStatusT.Error;
            }

            return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
        }
	}
}


