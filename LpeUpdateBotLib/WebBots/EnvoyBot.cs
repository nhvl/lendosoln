﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;


namespace LpeUpdateBotLib.WebBots
{
    class EnvoyBot : AbstractWebBot
    {
        private static string baseUrl = @"https://cld.envoymortgage.com/";
        private static string loginUrl = @"Account/Login.aspx?ReturnUrl=%2f";
        private static Regex vs_regex = new Regex(@"id=""__VIEWSTATE"" value=""(?<vs>[^""]+)", RegexOptions.IgnoreCase);
        private static Regex vsg_regex = new Regex(@"id=""__VIEWSTATEGENERATOR"" value=""(?<vsg>[^""]+)", RegexOptions.IgnoreCase);
        private static Regex ev_regex = new Regex(@"id=""__EVENTVALIDATION"" value=""(?<ev>[^""]+)", RegexOptions.IgnoreCase);
        private static Regex rsUrl_regex = new Regex(@"id=""MainContent_CurrentRateSheet"" href=""(?<rsUrl>[^""]+)", RegexOptions.IgnoreCase);

        public EnvoyBot(IDownloadInfo info)
            : base(info)
        {
        }
        public override string BotName
        {
            get { return "ENVOY"; }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //login
            string currPage = DownloadData(baseUrl + loginUrl);
            
            NameValueCollection postData = new NameValueCollection();
            postData.Add("__EVENTTARGET", "");
            postData.Add("__EVENTARGUMENT", "");
            Match match = vs_regex.Match(currPage);
            postData.Add("__VIEWSTATE", match.Groups["vs"].Value);
            match = vsg_regex.Match(currPage);
            postData.Add("__VIEWSTATEGENERATOR", match.Groups["vsg"].Value);
            match = ev_regex.Match(currPage);
            postData.Add("__EVENTVALIDATION", match.Groups["ev"].Value);
            postData.Add("ctl00$MainContent$LoginUser$UserName", dlinfo.Login);
            postData.Add("ctl00$MainContent$LoginUser$Password", dlinfo.Password);
            postData.Add("ctl00$MainContent$LoginUser$LoginButton", "Log In");

            currPage = UploadDataReturnResponse(baseUrl + loginUrl, postData, baseUrl);

            if(currPage.Contains("Your login attempt was not successful"))
            {
                ReportDeactivation("Envoy password incorrect for login: " + dlinfo.Login + ". Please update it and re-enable any bot using this login.");
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }
            //get rate sheet
            match = rsUrl_regex.Match(currPage);
            string fileName = null;

            if (match.Success)
            {

                fileName = DownloadFile(baseUrl + match.Groups["rsUrl"].Value);
                
                //logout
                NameValueCollection logoutPostData = new NameValueCollection();
                match = vsg_regex.Match(currPage);
                logoutPostData.Add("__EVENTTARGET", "ctl00$HeadLoginView$HeadLoginStatus$ctl00");
                logoutPostData.Add("__EVENTARGUMENT", "");
                logoutPostData.Add("__VIEWSTATE", "");
                logoutPostData.Add("__VIEWSTATEGENERATOR", match.Groups["vsg"].Value);
                logoutPostData.Add("__VIEWSTATEENCRYPTED", "");
                logoutPostData.Add("ctl00$MainContent$hiddenPopupMessage", "");
                UploadData(baseUrl, logoutPostData);
            }

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Envoy bot failed: Unable to find rate sheet for login: " + dlinfo.Login + ". Data: " + currPage);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}
