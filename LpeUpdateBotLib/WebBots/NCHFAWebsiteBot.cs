﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;


namespace LpeUpdateBotLib.WebBots
{
    public class NCHFAWebsiteBot : AbstractWebBot
    {
        private static Regex VIEWSTATE_regex = new Regex(@"id=""__VIEWSTATE""\s+value=""(?<viewstate>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex VIEWSTATEGENERATOR_regex = new Regex(@"id=""__VIEWSTATEGENERATOR""\s+value=""(?<viewstategenerator>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex EVENTVALIDATION_regex = new Regex(@"id=""__EVENTVALIDATION""\s+value=""(?<eventvalidation>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex failedLogin_regex = new Regex(@"Login failed.", RegexOptions.IgnoreCase);

        public NCHFAWebsiteBot(IDownloadInfo info) : base(info)
        {
        }

        public override string BotName
        {
            get 
            { 
                return "NCHFA_WEBSITE_DOWNLOAD"; 
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            if (dlinfo.Description.Contains("DAILY") && !dlinfo.Description.Contains("AFTER") && dlinfo.LastRatesheetTimestamp.Date != DateTime.Today)
            {
                GoToUrl(dlinfo.Url);

                string temp = DownloadData(dlinfo.Url);
                Match viewstate_match = VIEWSTATE_regex.Match(temp);
                Match viewstategenerator_match = VIEWSTATEGENERATOR_regex.Match(temp);
                Match eventvalidation_match = EVENTVALIDATION_regex.Match(temp);

                NameValueCollection postData = new NameValueCollection();
                postData.Add("__LASTFOCUS", "");
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", viewstate_match.Groups["viewstate"].Value);
                postData.Add("__VIEWSTATEGENERATOR", viewstategenerator_match.Groups["viewstategenerator"].Value);
                postData.Add("__EVENTVALIDATION", eventvalidation_match.Groups["eventvalidation"].Value);
                postData.Add("LoginUser$UserName", dlinfo.Login);
                postData.Add("LoginUser$Password", dlinfo.Password);
                postData.Add("LoginUser$LoginButton", "Log In");

                UploadData(dlinfo.Url, postData);

                temp = DownloadData(dlinfo.Url);

                Match failedLogin_match = failedLogin_regex.Match(temp);
                if (failedLogin_match.Success)
                {
                    dlinfo.Deactivate();
                    ReportDeactivation("NCHFA website bot login failed. Please check login credentials." + temp);
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    RatesheetFileName = DownloadFile("https://www.nchfa.org/OLS/Site/RefRates.aspx");
                    dlinfo.RecordLastRatesheetTimestamp(DateTime.Now);
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
            }
            else if (dlinfo.Description.Contains("DAILY") && dlinfo.Description.Contains("AFTER_9") && dlinfo.LastRatesheetTimestamp.Date != DateTime.Today && (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(9)) > 0))
            {
                GoToUrl(dlinfo.Url);

                string temp = DownloadData(dlinfo.Url);
                Match viewstate_match = VIEWSTATE_regex.Match(temp);
                Match viewstategenerator_match = VIEWSTATEGENERATOR_regex.Match(temp);
                Match eventvalidation_match = EVENTVALIDATION_regex.Match(temp);

                NameValueCollection postData = new NameValueCollection();
                postData.Add("__LASTFOCUS", "");
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", viewstate_match.Groups["viewstate"].Value);
                postData.Add("__VIEWSTATEGENERATOR", viewstategenerator_match.Groups["viewstategenerator"].Value);
                postData.Add("__EVENTVALIDATION", eventvalidation_match.Groups["eventvalidation"].Value);
                postData.Add("LoginUser$UserName", dlinfo.Login);
                postData.Add("LoginUser$Password", dlinfo.Password);
                postData.Add("LoginUser$LoginButton", "Log In");

                UploadData(dlinfo.Url, postData);
                temp = DownloadData(dlinfo.Url);

                Match failedLogin_match = failedLogin_regex.Match(temp);
                if (failedLogin_match.Success)
                {
                    dlinfo.Deactivate();
                    ReportDeactivation("NCHFA website bot login failed. Please check login credentials." + temp);
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    RatesheetFileName = DownloadFile("https://www.nchfa.org/OLS/Site/RefRates.aspx");
                    dlinfo.RecordLastRatesheetTimestamp(DateTime.Now);
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                
            }
            else if (!dlinfo.Description.Contains("DAILY"))
            {
                GoToUrl(dlinfo.Url);

                string temp = DownloadData(dlinfo.Url);
                Match viewstate_match = VIEWSTATE_regex.Match(temp);
                Match viewstategenerator_match = VIEWSTATEGENERATOR_regex.Match(temp);
                Match eventvalidation_match = EVENTVALIDATION_regex.Match(temp);

                NameValueCollection postData = new NameValueCollection();
                postData.Add("__LASTFOCUS", "");
                postData.Add("__EVENTTARGET", "");
                postData.Add("__EVENTARGUMENT", "");
                postData.Add("__VIEWSTATE", viewstate_match.Groups["viewstate"].Value);
                postData.Add("__VIEWSTATEGENERATOR", viewstategenerator_match.Groups["viewstategenerator"].Value);
                postData.Add("__EVENTVALIDATION", eventvalidation_match.Groups["eventvalidation"].Value);
                postData.Add("LoginUser$UserName", dlinfo.Login);
                postData.Add("LoginUser$Password", dlinfo.Password);
                postData.Add("LoginUser$LoginButton", "Log In");

                UploadData(dlinfo.Url, postData);
                temp = DownloadData(dlinfo.Url);

                Match failedLogin_match = failedLogin_regex.Match(temp);
                if (failedLogin_match.Success)
                {
                    dlinfo.Deactivate();
                    ReportDeactivation("NCHFA website bot login failed. Please check login credentials." + temp);
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    RatesheetFileName = DownloadFile("https://www.nchfa.org/OLS/Site/RefRates.aspx");
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
            }
            else
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }
        }
    }
}
