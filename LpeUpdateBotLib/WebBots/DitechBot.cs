// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Threading;

namespace LpeUpdateBotLib.WebBots
{
	public class GreenTreeBot : AbstractWebBot
	{
		private static string LoginUrl = "https://businesslending.ditech.com/servlet/com.mortgagehub.web.LoginCasServlet";
		private static string LoginUrl2 = "https://businesslending.ditech.com/cas/login?service=/servlet/com.mortgagehub.web.LoginCasServlet&loginCustom=wholesaleError";
        private static string rsUrl = "https://businesslending.ditech.com/member/rates/index.jsp";
		string[] userId = new string [2];
		char[] splitter = {'/'};
        private static Regex s_regEx = new Regex(@"<a\s+href=""(?<ratesheetUrl>[^""]+)""\s+target=""[^""]+"">CLC\s+Ratesheet\s+XLS");
		private static Regex s_regEx1 = new Regex(@"a\s+href=""(?<url>[^""]+)""");
		private static Regex NoRatesheetRegex = new Regex(@"Sorry\,\s+we are currently undergoing a pricing change");
		private static Regex updatePasswordRegex = new Regex(@"Expired Password");
        private static Regex loginSuccessful_regex = new Regex(@"Login Successful",RegexOptions.IgnoreCase);
        string[] description = new string[3];

		public GreenTreeBot(IDownloadInfo info) : base(info)
		{
		}
		public override string BotName 
		{
			get { return "DITECH"; }
		}
        public override long TimeoutInMs
        {
            get
            {
                return 600000;
            }
        }
       protected override string BotUserAgentString
        {
            get
            {
                return "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0)";
            }
        }
        protected override bool SetDecompressionMethods
        {
            get
            {
                return true;
            }
        }
        protected override string DefaultAcceptLanguage 
        { 
            get 
            { 
                return "en-US"; 
            }
        }
        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true; 
            }
        }
        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html, application/xhtml+xml, */*";
            }
        }
		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
            string content = login(dlinfo);
            string fileName = null;


            //if content is null something has gone wrong, so we can exit at this point, error message has already been sent by the 'login' method
            if (content == null)
            {
                return E_BotDownloadStatusT.Error;
            }



            Match match = s_regEx.Match(content);
            Match noRatesheet = NoRatesheetRegex.Match(content);

            description = dlinfo.Description.Split(splitter);
            
			//Check if there's match ratesheet link. If it's, access the excel link
            //Correspondent
			if (match.Success) 
			{
				string url = match.Groups["ratesheetUrl"].Value;
				string rstemp = DownloadData("https://correspondent.ditech.com" + url);
                match = s_regEx1.Match(rstemp);
                if (match.Success)
                {
                    GoToUrl("https://correspondent.ditech.com" + match.Groups["url"].Value);
                    fileName = DownloadFile("https://correspondent.ditech.com" + url);
                }
			}
			else if (noRatesheet.Success)
			{
				LogInfo (dlinfo.MainFileName + " not available: Ditech Bot has detected a reprice notification");
				return E_BotDownloadStatusT.Error;
			}
			else 
			{
				LogErrorAndSendEmailToAdmin("Contents for Ditech bot change. Content=" + content);
				return E_BotDownloadStatusT.Error;
			}
            
			//logout
            GoToUrl("https://correspondent.ditech.com/cas/logout?serviceURL=/index&logoutCorrWeb=true");
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }
		}

        private string login(IDownloadInfo dlinfo)
        {
            userId = dlinfo.Login.Split(splitter);

            NameValueCollection postData = new NameValueCollection();
            //postData.Add("transID", "");
            postData.Add("txtPartnerID", userId[0]);
            postData.Add("txtUserName", userId[1]);
            postData.Add("pwdPassword", dlinfo.Password);
            //postData.Add("submit", "Submit");

            //GoToUrl(LoginUrl);

            //Pass 1st values
            UploadData(LoginUrl, postData);

            NameValueCollection postData2 = new NameValueCollection();
            postData2.Add("txtPartnerID", userId[0]);
            postData2.Add("username", userId[1]);
            postData2.Add("password", dlinfo.Password);

            //Pass 3rd values and grab the return response
            string returnResponse = UploadDataReturnResponse(LoginUrl2, postData2);

            //Check if the return response equals to url link. If it's, access the url link
            Match homePageUrl_match = s_regEx1.Match(returnResponse);
            Match updatePassword_match = updatePasswordRegex.Match(returnResponse);
            Match loginSuccessful_match = loginSuccessful_regex.Match(returnResponse);

            
            if (homePageUrl_match.Success && loginSuccessful_match.Success)
            {
                string url1 = homePageUrl_match.Groups["url"].Value;
                GoToUrlWithPostMethod("https://correspondent.ditech.com" + url1);  //"&txtPartnerID=" + userId[0]);    
            }
            else if (updatePassword_match.Success)
            {
                dlinfo.Deactivate();
                ReportDeactivation("Please update Ditech password and re-enable any dl using this login: " + dlinfo.Login);
                return null;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("DitechBot failed to login for login: " + dlinfo.Login +". "+ returnResponse);
                return null;
            }

            string content = DownloadData(rsUrl);
            return content;
        }
	}
}
