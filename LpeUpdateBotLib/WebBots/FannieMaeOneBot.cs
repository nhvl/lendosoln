﻿/// Author: Budi Sulayman

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class FannieMaeOneBot : AbstractWebBot
    {
        private static string loginUrl = "https://www.ecommitone.com/webapp/action/Login";
        private static string retrievePriceUrl = "https://www.ecommitone.com/webapp/action/BrowsePricesRetrieve";
        private static string rsUrl = "https://www.ecommitone.com/webapp/action/ExportBrowsePrice";
        private static string logoutUrl = "https://www.ecommitone.com/webapp/action/Logout";
        private static Regex loginPage_regex = new Regex(@"Please enter your user name and password to log in", RegexOptions.IgnoreCase);

        public FannieMaeOneBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "FANNIEMAEONE"; }
        }

        public override long TimeoutInMs
        {
            get
            {
                return 720000;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            //split dlinfo.login to 2 userId with splitter '/'. First one is user id for login and the second one is branch number to access ratesheet
            string[] userId = new string[2];
            char[] splitter = { '/' };
            userId = dlinfo.Login.Split(splitter);

      
            NameValueCollection postData = new NameValueCollection();
            postData.Add("userId", userId[0]);
            postData.Add("password", dlinfo.Password);
            postData.Add("lenderId", userId[1]);

            string loginTemp = UploadDataReturnResponse(loginUrl, postData);
            string[] LPList = null;

            /** Each code "XX:XXXXXX" corresponds to a particular program, described below
             * 10:A222AR:3/1 LIBOR ARM Plan 2723 Margin 1.75 Nonconvertible
             * 02:0005FR:30-Yr Fixed Rate
             * 02:MCP30A:30-Yr Fixed Rate My Community Mortgage
             * 02:125331:30-Yr Fixed Rate Refi Plus LTV 105.01 thru 125
             * 02:160958:30-Yr Fixed Rate Refi Plus LTV over 125
             * 02:104315:30-Year Fixed Rate HFA Preferred
             * 02:111202:30-Year Fixed Rate HFA Preferred Risk Sharing
             * 03:0015FR:20-Yr Fixed Rate
             * 03:130829:20-Year Fixed Rate Refi Plus LTV 105.01 to 125
             * 03:133212:20-Year Fixed Rate Refi Plus over 125
             * 04:0010FR:15-Yr Fixed Rate
             * 04:101304:15-Year Fixed Rate Refi Plus LTV 105.01 to 125
             * 04:132021:15-Year Fixed Rate Refi Plus over 125
             * 05:0020FR:10-Yr Fixed Rate
             * 06:102301:30-Yr Fixed Rate High Balance
             * 06:100645:15-Yr Fixed Rate High Balance
             * 06:102710:30-Yr Fixed Rate My Community Mortgage High Balance
             * 07:A230AR:10/1 LIBOR ARM Plan 2729 Margin 1.75 Nonconvertible
             * 08:A228AR:7/1 LIBOR ARM Plan 2727 Margin 1.75 Nonconvertible
             * 09:A224AR:5/1 LIBOR ARM Plan 2725 Margin 1.75 Nonconvertible 5% Life Cap
             * 09:094447:5/1 LIBOR ARM Plan 2737, Margin 1.75, Nonconvertible, 5/2/5
             * 
             **/
            switch (dlinfo.Description)
            {
                case "FNMA1_PML0214_AA":
                    LPList = new string[2] { "02:104315", "02:111202" };
                    break;
                case "FNMA1_PML0168_AA":
                    LPList = new string[16] { "02:0005FR", "02:125331", "02:160958", "03:0015FR", "03:130829", "03:133212", "04:0010FR", "04:101304", "04:132021",
                                               "06:102301", "06:100645", "07:A230AR", "08:A228AR", "09:A224AR", "09:094447", "10:A222AR" };
                    break;
                case "FNMA1_PML0243_AA":
                    LPList = new string[16] { "02:0005FR", "02:125331", "02:160958", "03:0015FR", "03:130829", "03:133212", "04:0010FR", "04:101304", "04:132021",
                                               "06:102301", "06:100645", "07:A230AR", "08:A228AR", "09:A224AR", "09:094447", "10:A222AR" };
                    break;
                case "FNMA1_PML0196_AA":
                    LPList = new string[16] { "02:0005FR", "02:125331", "02:160958", "03:0015FR", "03:130829", "03:133212", "04:0010FR", "04:101304", "04:132021",
                                               "06:102301", "06:100645", "07:A230AR", "08:A228AR", "09:A224AR", "09:094447", "10:A222AR" };
                    break;
                case "FNMA1_PML0256_AA":
                    LPList = new string[10] { "02:0005FR", "03:0015FR", "04:0010FR", "05:0020FR", "06:102301", "06:100645", "10:A222AR", "09:A224AR", "08:A228AR", "07:A230AR" };
                    break;
                case "FNMA1_PML0257_AA":
                    LPList = new string[19] { "10:A222AR", "02:0005FR", "02:MCP30A", "02:125331", "02:160958", "03:0015FR", "03:130829", "03:133212", "04:0010FR", "04:101304", "04:132021", "05:0020FR", "06:102301", "06:100645", "06:102710", "07:A230AR", "08:A228AR", "09:A224AR", "09:094447" };
                    break;
                default:
                    LPList = new string[0];
                    LogErrorAndSendEmailToAdmin("Invalid description for FannieMaeOne bot for user id: " + userId[0]);
                    break;
            }

            StringBuilder rsPage = new StringBuilder();

            if (LPList.Length != 0)
            {
                Match loginFailedMatch = loginPage_regex.Match(loginTemp);
                if (loginFailedMatch.Success)
                {
                    LogErrorAndSendEmailToAdmin("FannieMaeOne bot failed to login for user id: " + userId[0] + ". Content: " + loginTemp);
                    return E_BotDownloadStatusT.Error;
                }
                else
                {
                    if (dlinfo.Description.EndsWith("AA"))
                        rsPage = AAPostRsData(LPList, rsPage);
                    /*else if (dlinfo.Description.EndsWith("SS"))
                        rsPage = SSPostRsData(LPList, rsPage);*/
                    else
                    {
                        LogErrorAndSendEmailToAdmin("FannieMaeOne bot encounters error description for id: " + userId[0] + ". Please update it");
                        return E_BotDownloadStatusT.Error;
                    }
                }
            }

            GoToUrl(logoutUrl);

            if (rsPage.ToString().Contains("Underwriting Method"))
            {
                RatesheetFileName = WriteToTempFile(rsPage.ToString());
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("FannieMaeOne bot failed to pull pricing for user id: " + userId[0]);
                return E_BotDownloadStatusT.Error;
            }
        }

        private StringBuilder AAPostRsData(string[] LPList, StringBuilder rsPage)
        {
            foreach (string i in LPList)
            {
                //split Loan program Id to 2 with splitter ':'. First one is productGroupIdentifier and second one is productId
                string[] prodIds = new string[2];
                char[] splitter = { ':' };
                prodIds = i.Split(splitter);

                GoToUrl(@"https://www.ecommitone.com/webapp/action/BrowsePrices");

                //retrieve DU Pricing
                NameValueCollection postData = new NameValueCollection();
                postData.Add("productGroupIdentifier", prodIds[0]);
                postData.Add("productId", prodIds[1]);
                postData.Add("ealevel", "03");
                postData.Add("remittanceType", "01");
                postData.Add("underwritingMethod", "01");

                string temp = UploadDataReturnResponse(retrievePriceUrl, postData);
                Match match = loginPage_regex.Match(temp);

                if (match.Success)
                {
                    rsPage.AppendLine("Failed to pull AA with DU UW pricing for product id: " + i.ToString());
                    break;
                }
                else
                {
                    temp = DownloadData(rsUrl);
                    rsPage.AppendLine(temp);
                }
            }
            return rsPage;
        }

        private StringBuilder SSPostRsData(string[] LPList, StringBuilder rsPage)
        {
            foreach (string i in LPList)
            {
                //split Loan program Id to 2 with splitter ':'. First one is productGroupIdentifier and second one is productId
                string[] prodIds = new string[2];
                char[] splitter = { ':' };
                prodIds = i.Split(splitter);

                //retrieve DU Pricing
                NameValueCollection postData = new NameValueCollection();
                postData.Add("productGroupIdentifier", prodIds[0]);
                postData.Add("productId", prodIds[1]);
                postData.Add("ealevel", "03");
                postData.Add("remittanceType", "03");
                postData.Add("underwritingMethod", "01");

                string temp = UploadDataReturnResponse(retrievePriceUrl, postData);
                Match match = loginPage_regex.Match(temp);

                if (match.Success)
                {
                    rsPage.AppendLine("Failed to pull SS with DU UW pricing for product id: " + i.ToString());
                    break;
                }
                else
                {
                    temp = DownloadData(rsUrl);
                    rsPage.AppendLine(temp);
                }
            }
            return rsPage;
        }
    }
}