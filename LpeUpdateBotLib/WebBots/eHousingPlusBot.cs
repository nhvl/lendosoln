﻿// Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Text.RegularExpressions;

namespace LpeUpdateBotLib.WebBots
{
    public class eHousingPlusBot : AbstractWebBot
    {
        private static string baseUrl = @"https://services.ehousingplus.com";
        private static string loginUrl = baseUrl + @"/Security/Login.aspx";
        private static string contentsUrl = baseUrl + @"/HDSContents.aspx";
        private static string rsUrl = baseUrl + @"/SFManagement/Reserve/SubmitByRate.aspx";
        private static string logoutUrl = baseUrl + @"/Security/Logoff.aspx";

        private static Regex VIEWSTATE_regex = new Regex(@"id=""__VIEWSTATE""\s+value=""(?<viewstate>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex VIEWSTATEGENERATOR_regex = new Regex(@"id=""__VIEWSTATEGENERATOR""\s+value=""(?<viewstategenerator>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex EVENTVALIDATION_regex = new Regex(@"id=""__EVENTVALIDATION""\s+value=""(?<eventvalidation>[^""]+)""", RegexOptions.IgnoreCase);

        private static Regex invalid_login_regex = new Regex(@"Quick Tips", RegexOptions.IgnoreCase);

        public eHousingPlusBot(IDownloadInfo info) : base(info) { }

        public override string BotName
        {
            get { return "EHOUSINGPLUS"; }
        }

        protected override string DefaultAcceptHeader
        {
            get
            {
                return @"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            }
        }

        protected override string DefaultAcceptLanguage
        {
            get
            {
                return "en-US,en;q=0.9";
            }
        }

        protected override string BotUserAgentString
        {
            get
            {
                return "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36";
            }
        }

        protected override bool WebRequestKeepAlive
        {
            get
            {
                return true;
            }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // PMCIDA rates are only on rate sheet between 7am-5pm
            if (dlinfo.Description.Contains("PMCIDA"))
            {
                // if its before 7am or after 5pm, we're not going to download the RS. 
                if ((DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(7)) < 0) || (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(17)) > 0))
                {
                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                }
            }

            // create clientState strings for authentication
            string login_clientState = "|0|01" + dlinfo.Login + "||[[null,[],null],[{},[]],\"01" + dlinfo.Login + "\"]";
            string password_clientState = "|0|01" + dlinfo.Password + "||[[null,[],null],[{},[]],\"01" + dlinfo.Password + "\"]";

            // use regexes to grab authentication data from login URL HTML
            string temp = DownloadData(loginUrl);
            Match viewstate_match = VIEWSTATE_regex.Match(temp);
            Match viewstategenerator_match = VIEWSTATEGENERATOR_regex.Match(temp);
            Match eventvalidation_match = EVENTVALIDATION_regex.Match(temp);

            string fileName = null;

            // add all data we will be POSTing to login
            NameValueCollection postData = new NameValueCollection();
            postData.Add("myAuthentication_utxeUserName", dlinfo.Login);
            postData.Add("myAuthentication_utxeUserName_clientState", login_clientState);
            postData.Add("myAuthentication_utxeUserPassword", dlinfo.Password);
            postData.Add("myAuthentication_utxeUserPassword_clientState", password_clientState);
            postData.Add("__EVENTTARGET", "");
            postData.Add("__EVENTARGUMENT", "");
            postData.Add("__VIEWSTATE", viewstate_match.Groups["viewstate"].Value);
            postData.Add("__VIEWSTATEGENERATOR", viewstategenerator_match.Groups["viewstategenerator"].Value);
            postData.Add("__EVENTVALIDATION", eventvalidation_match.Groups["eventvalidation"].Value);
            postData.Add("myAuthentication$btnOk", "Sign In");

            // login
            UploadData(loginUrl, postData);

            // check to make sure we actually logged in
            temp = DownloadData(contentsUrl);
            Match invalid_login_match = invalid_login_regex.Match(temp);

            // if we didn't, throw an error
            if (invalid_login_match.Success)
            {
                LogErrorAndSendEmailToAdmin("DEACTIVATED. eHousingPlus Bot failed to login for: " + dlinfo.Login + ". Content: " + temp);
                dlinfo.Deactivate();
                return E_BotDownloadStatusT.Error;
            }
            // if we did, download the RS
            else
            {
                // download HTML of page with rates on it
                fileName = DownloadFile(rsUrl);

                if (fileName != null)
                {
                    RatesheetFileName = fileName;
                    GoToUrl(logoutUrl);
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                {
                    GoToUrl(logoutUrl);
                    LogErrorAndSendEmailToAdmin("eHousingPlus Bot failed to download RS for login: " + dlinfo.Login + ". Content: " + temp);
                    return E_BotDownloadStatusT.Error;
                }
            }
        }
    }
}