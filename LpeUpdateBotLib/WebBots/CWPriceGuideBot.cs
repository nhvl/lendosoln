using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
	/// <summary>
	/// CountryWide Price Guide Bot
	/// </summary>
	public class CWPriceGuideBot : AbstractWebBot
	{
		private const string BASE_URL = "https://cld.countrywide.com";
		private const string LOGIN_URL = "https://cldlogin.countrywide.com/cld/login";
		private const string LOGOUT_URL = BASE_URL + "/login/loggedout.asp";

		// Price Guide Pages
		private const string CONV_PRICEGUIDE_BASE = BASE_URL + "/cld/PriceGuide/";
		private const string CONV_PRICEGUIDE_PAGE = CONV_PRICEGUIDE_BASE + "ConventionalGovPriceGuides.asp";
		
		// Conventional Price Guide Download Link
		private static Regex ConvPriceGuidePage_regEx = new Regex(@"ConventionalGovPriceGuides\.asp\?getFile=" + "([a-zA-z0-9%]+)", RegexOptions.Compiled);

		
		private static Regex AuthFailedRegex = new Regex(@"password(\s+)has(\s+)expired");
	
		public CWPriceGuideBot(IDownloadInfo info) : base(info)
		{
		
		}
		public override string BotName 
		{
			get { return "CW_PRICEGUIDE"; }
		}

		protected override string DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
			// Steps to download ratesheet
			//    - Login
			//    - Hit the rs_url.
			//    - Search the content for xls link.
			//    - Download the xls file
			//    - Logout
			string fileName = null;
			string html = null;

			NameValueCollection postData = new NameValueCollection();
			postData.Add("usr_name", dlinfo.Login);
			postData.Add("usr_password", dlinfo.Password);
			
			html = UploadDataReturnResponse(LOGIN_URL, postData);
			Match mFindProblem = AuthFailedRegex.Match(html) ;
			if(mFindProblem.Success)
			{
				string sErrorMsg = "(CWPriceGuideBot) Account login is invalid. UserName=" + dlinfo.Login;
				LogErrorAndSendEmailToAdmin(sErrorMsg);
				return null;
			}

			string priceGuidePage;	// string to page content
			MatchCollection linkMatches; // link matcher
			AllowAutoRedirect = false;

			// The Conventional Price Guide page has 4 links with the exact same base; the only difference is in the getFile param value.
			// 1st link: PDF download for Conf Price Guide
			// 2nd link: XL download for Conf Price Guide
			// 3rd link: PDF download for NonConf Price Guide
			// 4th link: XL download for NonConf Price Guide
			switch (RateSheetType) 
			{
				case "CW_AGENCY_PRICEGUIDE":
					priceGuidePage = DownloadData(CONV_PRICEGUIDE_PAGE + "?nav=y");
					linkMatches = ConvPriceGuidePage_regEx.Matches(priceGuidePage);
					fileName = DownloadFile(CONV_PRICEGUIDE_PAGE + "?getFile=" + linkMatches[1].Groups[1]); // download from the 2nd link.
					break;
				case "CW_NONCONF_PRICEGUIDE":
					priceGuidePage = DownloadData(CONV_PRICEGUIDE_PAGE + "?nav=y");
					linkMatches = ConvPriceGuidePage_regEx.Matches(priceGuidePage);
					fileName = DownloadFile(CONV_PRICEGUIDE_PAGE + "?getFile=" + linkMatches[3].Groups[1]); // download from the 4th (last) link.
					break;
			}
			GoToUrl(LOGOUT_URL);
			return fileName;
		}
	}
}