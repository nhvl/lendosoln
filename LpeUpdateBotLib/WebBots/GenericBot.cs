/// Author: David Dao

using System;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class GenericBot : AbstractWebBot
    {
        public GenericBot(IDownloadInfo info) : base(info)
        {

        }
        public override string BotName 
        {
            get { return ""; }
        }
    }
}
