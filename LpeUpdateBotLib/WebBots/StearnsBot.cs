﻿// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.Net;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class StearnsBot : AbstractWebBot
    {
        private static string baseUrl = @"https://snap.stearns.com/";
        private static string loginUrl = baseUrl + @"LSLoginPQ.asp?tp=M";
        private static Regex companyID_regex = new Regex(@"name=""CompanyID"" type=""hidden"" value=""(?<companyID>[^""]+)""", RegexOptions.IgnoreCase);
        private static Regex ratesheet_regex = new Regex(@"function OnTodaysRateSheetDownload\(\)\s+\{\s+self\.location = ""(?<rslink>[^""]+)"";", RegexOptions.IgnoreCase);

        public StearnsBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "STEARNS"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string temp = DownloadData(baseUrl);
            string fileName = null;
            
            Match companyIDMatch = companyID_regex.Match(temp);
            if (companyIDMatch.Success)
            {
                NameValueCollection postData = new NameValueCollection();
                postData.Add("LoginName", dlinfo.Login);
                postData.Add("LoginPass", dlinfo.Password);
                postData.Add("CompanyID", companyIDMatch.Groups["companyID"].Value);

                UploadData(loginUrl, postData);
                GoToUrl(baseUrl + "MainMenu.asp");
                temp = DownloadData(baseUrl + @"DocumentList.asp?tp=1");

                Match rsMatch = ratesheet_regex.Match(temp);
                if (rsMatch.Success)
                    fileName = DownloadFile(baseUrl + rsMatch.Groups["rslink"].Value);
                else
                    LogErrorAndSendEmailToAdmin("Stearns bot can't find ratesheet link for login: " + dlinfo.Login + ". Content: " + temp);
            }
            else
                LogErrorAndSendEmailToAdmin("Stearns bot can't find companyID for login: " + dlinfo.Login + ". Content: " + temp);

            if (fileName != null)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}