﻿/// Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
	public class UWMBot : AbstractWebBot
	{
        private static Regex authentican_regex = new Regex(@"<INPUT type=""password"" name=""(?<passName>[^""]+)"" size=""[^""]+"" value=""(?<passValue>[^""]+)"">");
        private static Regex graphicsPath_regex = new Regex(@"<INPUT type=""hidden"" name=""GraphicsPath"" VALUE=""(?<GPValue>[^""]+)""");
        private static Regex RLDID_regex = new Regex(@"<INPUT type=""hidden"" name=""RLDID"" VALUE=""(?<RLDIDValue>[^""]+)""");
        private static Regex REID_regex = new Regex(@"<INPUT type=""hidden"" name=""REID"" VALUE=""(?<REIDValue>[^""]+)""");
        private static Regex rs_regex = new Regex(@"alt=""Today's Rates""\s+href=""(?<rslink>[^""]+)""\s+/>");

        public UWMBot(IDownloadInfo info) : base(info)
		{
		}
		public override string BotName 
		{
			get { return "UWM"; }
		}
		protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
		{
            string temp = DownloadData("https://broker.uwmco.com/authentication.html");
            Match match = authentican_regex.Match(temp);
            NameValueCollection postData = new NameValueCollection();
            if (match.Success)
                postData.Add(match.Groups["passName"].Value, match.Groups["passValue"].Value);
            else
            {
                LogErrorAndSendEmailToAdmin("UWMBot failed to get authentication password for login: "+dlinfo.Login);
                return E_BotDownloadStatusT.Error;
            }

            temp = UploadDataReturnResponse("https://broker.uwmco.com/WebAgent.exe?VLO/VLOAccess", postData);
            
            postData = new NameValueCollection();
            postData.Add ("VLOLogin", dlinfo.Login);
            postData.Add ("VLOPassword", dlinfo.Password);

            match = graphicsPath_regex.Match(temp);
            if (match.Success)
            {
                postData.Add("GraphicsPath", match.Groups["GPValue"].Value);
                postData.Add("BrokerLogo", "");
                match = RLDID_regex.Match(temp);
                if (match.Success)
                {
                    postData.Add("RLDID", match.Groups["RLDIDValue"].Value);
                    match = REID_regex.Match(temp);
                    if (match.Success)
                    {
                        postData.Add("REID", match.Groups["REIDValue"].Value);
                        postData.Add("Stat01", "");
                        postData.Add("Stat02", "");
                        postData.Add("Stat03", "");
                        postData.Add("Stat04", "");
                        postData.Add("Stat05", "");
                        postData.Add("Stat06", "");
                        postData.Add("Stat07", "");
                        postData.Add("Stat08", "");
                        postData.Add("Stat09", "");
                        postData.Add("Stat10", "");
                    }
                    else
                    {
                        LogErrorAndSendEmailToAdmin("UWMBot failed to get REID Value for login: " + dlinfo.Login);
                        return E_BotDownloadStatusT.Error;
                    }
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("UWMBot failed to get RLDID Value for login: " + dlinfo.Login);
                    return E_BotDownloadStatusT.Error;
                }
            }
            else
            {
                LogErrorAndSendEmailToAdmin("UWMBot failed to get Graphics Path Value for login: " + dlinfo.Login);
                return E_BotDownloadStatusT.Error;
            }

            temp = UploadDataReturnResponse("https://broker.uwmco.com/WebAgent.exe?VLO/LG/VLOLogin", postData);
            match = rs_regex.Match(temp);
            if (match.Success)
            {
                RatesheetFileName = DownloadFile(match.Groups["rslink"].Value);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("UWMBot failed to find today's rates link for login: " + dlinfo.Login);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}