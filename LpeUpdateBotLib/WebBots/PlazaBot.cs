﻿//Yin Seo
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;

namespace LpeUpdateBotLib.WebBots
{
    public class PlazaBot : AbstractWebBot
    {
        private static string hostUrl = @"http://plazahomemortgage.com";
        private static string loginUrl = hostUrl + @"/Login/";
        private static string logoutUrl = hostUrl + @"/Logout/";
        private static string masterRsUrl = hostUrl + @"/Correspondent/Rates/Master.aspx?Type=0";
        private static Regex masterRs_regex = new Regex(@"<td><a href=""(?<name>[^\"" >]+)""><img title=""Plaza Master CLP Rates Sheet", RegexOptions.IgnoreCase);
        private static Regex wholesalemasterRs_regex = new Regex(@"<td><a href=""(?<wholesale>[^\"" >]+)""><img title=""Plaza Master Wholesale Rates Sheet", RegexOptions.IgnoreCase);
        private static Regex loginfail_Response = new Regex(@"This area of our website is restricted to companies and individuals who are approved and have been issued user login");


        public PlazaBot(IDownloadInfo info)
            : base(info)
        {
        }

        public override string BotName
        {
            get { return "PLAZA"; }
        }

        protected override bool WebRequestKeepAlive
        {

            get { return true; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string fileName = null; //the RS that will be DLed
            string rs_Response = null;
            Match rs_match = null;
            Match wholesalers_match = null;
            Match loginfail_match = null;

            // if its before 7am, we're not gonna download the RS. 
            if (DateTime.Now.CompareTo(DateTime.Now.Date.AddHours(7)) < 0)
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            // if its Saturday or Sunday, we're not gonna download the RS. 
            if ((DateTime.Now.DayOfWeek == DayOfWeek.Saturday) || (DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
            {
                return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
            }

            GoToUrl(hostUrl);

            NameValueCollection loginInfo = new NameValueCollection();

            loginInfo.Add("txtbuid", dlinfo.Login);
            loginInfo.Add("txtbpid", dlinfo.Password);



            UploadData(loginUrl, loginInfo);

            rs_Response = DownloadData(masterRsUrl);
            //find rs url in web page
            rs_match = masterRs_regex.Match(rs_Response);
            wholesalers_match = wholesalemasterRs_regex.Match(rs_Response);

            //if Plaza doesn't like you logging in that way
            loginfail_match = loginfail_Response.Match(rs_Response);

            //look for the RS on the RS URL
            if (dlinfo.Description.Contains("WHOLESALE") && wholesalers_match.Success)
            {
                string rs = hostUrl + wholesalers_match.Groups["wholesale"].Value;
                fileName = DownloadFile(rs);
            }
            else if ((!dlinfo.Description.Contains("WHOLESALE")) && rs_match.Success)
            {
                string rs = hostUrl + rs_match.Groups["name"].Value;
                fileName = DownloadFile(rs);
            }
            else if (loginfail_match.Success)
            {

                //login using the page you get when initial login fails
                loginInfo = new NameValueCollection();
                loginInfo.Add("__VIEWSTATE", "/wEPDwULLTEyMDM3NjY4MDcPZBYCZg9kFgICAQ8PFgIeB1Zpc2libGVoZGRkPiChA/vpIBWxaA/JGjZn/O2srXlR7qr0agdl2QSpPPc=");
                loginInfo.Add("__VIEWSTATEGENERATOR", "25748CED");
                loginInfo.Add("ctl00$MainContent$txtUserName", dlinfo.Login);
                loginInfo.Add("ctl00$MainContent$txtPassword", dlinfo.Password);
                loginInfo.Add("ctl00$MainContent$btnLogin", "Sign In");
                UploadData(@"https://www.plazahomemortgage.com/login/?ReturnURL=%2fcorrespondent%2frates%2fmaster.aspx%3fType%3d0", loginInfo);


                rs_Response = DownloadData(masterRsUrl);
                rs_match = masterRs_regex.Match(rs_Response);
                wholesalers_match = wholesalemasterRs_regex.Match(rs_Response);
                //look for the RS on the RS URL
                if (dlinfo.Description.Contains("WHOLESALE") && wholesalers_match.Success)
                {
                    string rs = hostUrl + wholesalers_match.Groups["wholesale"].Value;
                    fileName = DownloadFile(rs);
                }
                else if ((!dlinfo.Description.Contains("WHOLESALE")) && rs_match.Success)
                {
                    string rs = hostUrl + rs_match.Groups["name"].Value;
                    fileName = DownloadFile(rs);
                }
                else
                {
                    //Send error msg to support
                    LogErrorAndSendEmailToAdmin("Plaza Bot: ratesheet was not found. The following was returned by server: \n{0}", rs_Response);
                }
            }
            else
            {
                //Send error msg to support
                LogErrorAndSendEmailToAdmin("Plaza Bot: ratesheet was not found. The following was returned by server: \n{0}", rs_Response);
            }

            //Logout after
            GoToUrl(logoutUrl);

            //Check for error
            if (null != fileName)
            {
                RatesheetFileName = fileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }
    }
}