/// Author: David Dao

using System;
using System.Collections.Generic;

namespace LpeUpdateBotLib.Common
{
    public interface IDownloadInfo
    {
        int Id { get; }
        string Description { get; }
        string Url { get; }
        string[] FileNames { get; }
        string MainFileName { get; }
        string Conversion { get; }
        string BotType { get; }
        string Login { get; }
        string Password { get; }
        DateTime LastRatesheetTimestamp { get; }
        string Token { get; }

        void RecordLastSuccessfulDLAndLastUpdated();
        void RecordLastUpdated();
        void RecordLastSuccessfulDL();
        void RecordLastRatesheetTimestamp(DateTime timestamp);
        void RecordErrorMessage(Exception exc);
		void Deactivate();
        void ExpireRatesheet(); // OPM 21897

    }
}
