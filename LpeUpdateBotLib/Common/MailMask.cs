﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.Common
{
    public class MailMask
    {
        public static Pop3Attachment EMPTY_ATTACHMENT = Pop3Attachment.EmptyAttachment;

        private IEmailDownloadInfo m_emailDownloadInfo = null;

        public IEmailDownloadInfo EmaiLDownloadInfo
        {
            get { return m_emailDownloadInfo; }
        }

        public MailMask(IEmailDownloadInfo emailDownloadInfo)
        {
            m_emailDownloadInfo = emailDownloadInfo; 
            m_id = emailDownloadInfo.Id;
            m_sSenderName = emailDownloadInfo.Sender.ToUpper();
            m_sBody = emailDownloadInfo.Body.ToUpper();
            m_sMailFileName = emailDownloadInfo.MailFileName.ToUpper();
            m_sTargetFileNames = emailDownloadInfo.TargetFileNames;
            m_sMainTargetFileName = emailDownloadInfo.MainTargetFileName;
            m_subject = emailDownloadInfo.Subject.ToUpper();
            m_description = emailDownloadInfo.Description.ToUpper();
        }

        public bool DoNonAttachmentFieldsMatch(Pop3Message msg)
        {
            if (m_sSenderName.Length > 0 &&
                msg.From.IndexOf(m_sSenderName) < 0 &&
                msg.FromName.IndexOf(m_sSenderName) < 0)
            {
                return false;
            }

            if (string.IsNullOrEmpty(m_sBody) == false &&
                (string.IsNullOrEmpty(msg.BodyText) == true || msg.BodyText.IndexOf(m_sBody, StringComparison.OrdinalIgnoreCase) < 0))
            {
                return false;
            }

            if (string.IsNullOrEmpty(m_subject) == false && !m_emailDownloadInfo.IsSubjectMatch(msg.Subject))
            {
                return false;
            }
 
            return true;
        }

        // db - Adding for OPM 21897 (Re-Price Bots)
        // This function is similar to the existing GetAttachment function except that it will only return true
        // if the e-mail contains an attachment and the bot has a matching attachment name set, or if there is 
        // no attachment to the e-mail and the bot has an empty attachment field.  The GetAttachment function
        // cannot be used for the reprice bots because it returns an attachment even if the bot has an empty attachment
        // field set.
        public Pop3Attachment GetAttachmentOnlyIfAllAttachmentFieldsMatch(Pop3Message msg)
        {
            foreach (var atm in msg.Attachments)
            {
                if (atm.Name.ToUpper().IndexOf(m_sMailFileName) >= 0)
                    return atm;
            }

            if (m_sMailFileName.Length == 0 && msg.Attachments.Count() == 0)
                return EMPTY_ATTACHMENT;

            return null;
        }

        public Pop3Attachment Match(Pop3Message msg)
        {
            if (DoNonAttachmentFieldsMatch(msg) == false)
                return null;

            return GetAttachment(msg);
        }

        private Pop3Attachment GetAttachment(Pop3Message msg)
        {
            foreach (var atm in msg.Attachments)
            {
                // 6/5/2012 dd - Per eOPM 357999 - Support regex in mask.
                // 6/5/2012 dd - Only perform regex if it start with "regex::"
                string regExKeyword = "regex::";
                bool isRegExMatch = m_sMailFileName.StartsWith(regExKeyword, StringComparison.OrdinalIgnoreCase);
                string regExPattern = string.Empty;
                if (isRegExMatch)
                {
                    regExPattern = m_sMailFileName.Substring(regExKeyword.Length);
                }
                if (m_sMailFileName.Length == 0 || atm.Name.ToUpper().IndexOf(m_sMailFileName) >= 0 || (isRegExMatch && Regex.IsMatch(atm.Name, regExPattern, RegexOptions.IgnoreCase)))
                    return atm;
            }

            if (m_sMailFileName.Length == 0 && m_sTargetFileNames.Length == 0)
                return EMPTY_ATTACHMENT;


            return null;
        }
        public string[] TargetFileNames
        {
            get { return m_sTargetFileNames; }
        }
        public string MainTargetFileName
        {
            get { return m_sMainTargetFileName; }
        }
        public int Id
        {
            get { return m_id; }
        }

        public string Subject
        {
            get { return m_subject; }
        }

        public string Description
        {
            get { return m_description; }
        }

        private int m_id = -1;
        private string m_sSenderName;
        private string m_sBody;
        private string m_sMailFileName;
        private string[] m_sTargetFileNames;
        private string m_sMainTargetFileName;
        private string m_subject;
        private string m_description;

        public void UpdateLastUpdated()
        {
            m_emailDownloadInfo.UpdateLastUpdated();
        }

    }

}
