﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpeUpdateBotLib.Common
{
    public enum E_BotDownloadStatusT
    {
        Error,
        SuccessfulWithRatesheet,
        SuccessfulWithNoRatesheet,
        SuccessfulWithManualRatesheet,
        NoRatesheetMarkRatesheetExpire,
    }
}
