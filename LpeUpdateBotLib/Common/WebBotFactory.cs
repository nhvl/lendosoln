/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.Common
{
    public static class WebBotFactory
    {
        public static string BotDllVersion
        {
            get
            {
                return typeof(WebBotFactory).Assembly.GetName().Version.ToString();
            }
        }
        private static Dictionary<string, ConstructorInfo> s_webBotDictionary;
        private static Dictionary<string, ConstructorInfo> s_emailBotDictionary;

        static WebBotFactory() 
        {
            s_webBotDictionary = new Dictionary<string, ConstructorInfo>(StringComparer.OrdinalIgnoreCase);
            s_emailBotDictionary = new Dictionary<string, ConstructorInfo>(StringComparer.OrdinalIgnoreCase);


            Assembly assembly = typeof(WebBotFactory).Assembly;
            Type[] list = assembly.GetTypes();
            
            Type webBotType = typeof(AbstractWebBot);
            Type emailBotType = typeof(AbstractEmailBot);
            foreach (Type type in list) 
            {
               
                if (type.IsClass && !type.IsAbstract) 
                {
                    if (type.IsSubclassOf(emailBotType))
                    {
                        // 12/29/2010 dd - Need to check if class is subclass of AbstractEmailBot before the AbstractWebBot. The reason is
                        // AbstractEmailBot is inherit from AbstractWebBot
                        ConstructorInfo constructor = type.GetConstructor(new Type[] { typeof(IDownloadInfo), typeof(Pop3Message) });
                        if (null != constructor)
                        {
                            try
                            {
                                object o = constructor.Invoke(new object[] { null, null });
                                AbstractEmailBot emailBot = o as AbstractEmailBot;
                                if (null != emailBot)
                                {
                                    s_emailBotDictionary.Add(emailBot.BotName, constructor);
                                }
                            }
                            catch (Exception exc)
                            {
                                Logger.LogErrorAndSendEmail(exc.ToString());
                            }
                        }
                    }
                    else if (type.IsSubclassOf(webBotType))
                    {
                        ConstructorInfo constructor = type.GetConstructor(new Type[] { typeof(IDownloadInfo) });
                        if (null != constructor)
                        {
                            try
                            {
                                object o = constructor.Invoke(new object[] { null });
                                AbstractWebBot webBot = o as AbstractWebBot;
                                if (null != webBot)
                                {
                                    s_webBotDictionary.Add(webBot.BotName, constructor);
                                }
                            }
                            catch (Exception exc)
                            {
                                Logger.LogErrorAndSendEmail(exc.ToString());
                            }
                        }
                    }


                }
            }
        }

        public static AbstractWebBot CreateBot(IDownloadInfo dlInfo) 
        {
            AbstractWebBot webBot = null;

            ConstructorInfo constructor = null;

            if (dlInfo != null)
            {
                if (s_webBotDictionary.TryGetValue(dlInfo.BotType, out constructor))
                {
                    if (null != constructor)
                    {
                        webBot = (AbstractWebBot)constructor.Invoke(new object[] { dlInfo });
                    }
                }
            }

            return webBot;
        }

        public static AbstractEmailBot CreateEmailBot(IDownloadInfo dlInfo, Pop3Message msg)
        {
            AbstractEmailBot emailBot = null;

            ConstructorInfo constructor = null;

            if (dlInfo != null)
            {
                if (s_emailBotDictionary.TryGetValue(dlInfo.BotType, out constructor))
                {
                    if (null != constructor)
                    {
                        emailBot = (AbstractEmailBot)constructor.Invoke(new object[] { dlInfo, msg });
                    }
                }
            }
            return emailBot;
        }
    }

}
