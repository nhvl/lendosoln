/// Author: David Dao
namespace LpeUpdateBotLib.Common
{
    using System;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Text.RegularExpressions;
    //using LendersOffice.Drivers.Gateways;

    public abstract class AbstractWebBot
    {
        /// <summary>
        /// 10/16/2015 - Need to make this overrideable becuase some websites won't set cookies without matching User Agent
        /// </summary>
        protected virtual string BotUserAgentString 
        { 
            get 
            { 
                return "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.0.3705)"; 
            } 
        }

        private CookieContainer m_cookieContainer = null;
        private IDownloadInfo m_info = null;
        private bool m_allowAutoRedirect = true;
        
        private HttpStatusCode m_httpStatusCode = HttpStatusCode.OK;
        private WebHeaderCollection m_webHeaderCollection = null;

        /// <summary>
        /// This was added for external opm 135588. If you get an IOException ...Connection closed  error 
        /// set this to true and it will change the protocol version to 1.0 in create request. This will hopefully fix it. 
        /// av 12-10-08
        /// </summary>
        protected bool UseHTTPProtocol10 {get;set;}
        /// <summary>
        /// This is passed into the web request. av eopm    152521      3-11-09
        /// </summary>
        protected virtual string DefaultAcceptHeader { get { return ""; } }
        /// <summary>
        /// 6/11/2014 - This is passed into the web request for eopm 550364. bs 
        /// </summary>
        /// 
        protected virtual bool SetDecompressionMethods
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 6/11/2014 - This is passed into the web request for eopm 550364. bs 
        /// </summary>
        /// 
        protected virtual string DefaultAcceptLanguage { get { return ""; } }

        /// <summary>
        /// 12/6/2016 - This is passed into the web request for eOPM 1040787. MS defaults this value to 50
        /// </summary>
        /// 
        protected virtual int MaxAutoRedirects { get { return 50; } }

        protected bool IsRedirection 
        {
            get 
            {
                if (m_httpStatusCode == HttpStatusCode.Found ||
                    m_httpStatusCode == HttpStatusCode.Redirect ||
                    m_httpStatusCode == HttpStatusCode.Moved ||
                    m_httpStatusCode == HttpStatusCode.MovedPermanently) 
                {
                    return true;
                }
                return false;
            }
        }
        protected string RedirectionUrl 
        {
            get 
            {
                if (null != m_webHeaderCollection) 
                {
                    string url = m_webHeaderCollection["location"];
                    if (null != url)
                        return url.Trim();
                }
                return "";
            }
        }
        protected bool AllowAutoRedirect 
        {
            get { return m_allowAutoRedirect; }
            set { m_allowAutoRedirect = value; }
        }

        public AbstractWebBot(IDownloadInfo info) 
        {
            m_info = info;
            m_cookieContainer = new CookieContainer();
        }

        protected string GetCookieValue(string url, string cookieName) 
        {
            CookieCollection coll = m_cookieContainer.GetCookies(new Uri(url));
            Cookie cookie = coll[cookieName];
            if (null != cookie)
                return cookie.Value;
            return "";
        }
        protected void SetCookie(string url, string cookieName, string value) 
        {
            m_cookieContainer.Add(new Cookie(cookieName, value, "/", url));
        }
        protected void DebugCookieContainer(Uri url) 
        {
            CookieCollection coll = m_cookieContainer.GetCookies(url);
            LogInfo("Cookies for " + url);
            foreach (Cookie c in coll) 
            {
                LogInfo("Cookie[" + c.Name + "] = [" + c.Value + "]");
            }
        }

        protected virtual bool NeedNetworkCredentials { get { return false; } }
        public virtual long TimeoutInMs
        {
            // 9/22/2011 dd - Default timeout to 5 minutes.
            get { return 300000; }
        }

        protected virtual bool WebRequestKeepAlive
        {
            // 9/9/2013 dd - Be default the value will be false. 
            // Per eOPM 454880, I made this property overridable by certain bot
            get { return false; }
        }
        private HttpWebRequest CreateRequest(string url, string method, string refererUrl) 
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                if (sslPolicyErrors != System.Net.Security.SslPolicyErrors.None && DateTime.Today < new DateTime(2009, 9, 11))
                {
                    if (sender is HttpWebRequest)
                    {
                        var req = sender as HttpWebRequest;
                        Logger.Log("INVALID-CERTIFICATE: " + req.RequestUri.ToString() + " ERROR " + sslPolicyErrors.ToString());
                    }
                    else
                    {
                        Logger.Log("INVALID-CERTIFICATE: " + sender.ToString() + " failed server certificate validation with " + sslPolicyErrors);
                    }
                }
                return true;
            };

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = BotUserAgentString;
            request.KeepAlive = WebRequestKeepAlive; // DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            request.Method = method;
            request.AllowAutoRedirect = m_allowAutoRedirect;

            request.MaximumAutomaticRedirections = MaxAutoRedirects;


            if (SetDecompressionMethods)
            {
                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            }
            if (!String.IsNullOrEmpty(DefaultAcceptLanguage))
            {
                request.Headers.Add(HttpRequestHeader.AcceptLanguage, DefaultAcceptLanguage);
            }

            if (NeedNetworkCredentials)
            {
                NetworkCredential nc = new NetworkCredential();
                nc.UserName = m_info.Login;
                nc.Password = m_info.Password;
                request.Credentials = nc;
            }

            if (!String.IsNullOrEmpty(DefaultAcceptHeader))
            {
                request.Accept = DefaultAcceptHeader;
            }
            if (method == "POST")
            {
                request.ContentType = "application/x-www-form-urlencoded";
            }

            if (UseHTTPProtocol10)
            {
                request.ProtocolVersion = HttpVersion.Version10;
            }

            if (null != refererUrl)
            {
                request.Referer = refererUrl;
            }
            request.CookieContainer = m_cookieContainer;

            return request;
        }


        private HttpWebResponse GetResponse(HttpWebRequest request) 
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                m_httpStatusCode = response.StatusCode;
                m_webHeaderCollection = response.Headers;
                if (CopyCookiesFromResponse)
                {
                    m_cookieContainer.Add(response.Cookies);
                }

                return response;
            }
            catch (Exception exc)
            {
                // 4/24/2015 dd - Add the URL for easy debugging.
                sw.Stop();
                if (request != null && request.Address != null)
                {
                    exc.Data.Add("BotName", this.BotName);
                    exc.Data.Add("RequestUrl", request.Method + " " + request.Address.ToString());
                    exc.Data.Add("ResponseTimeInMs", sw.ElapsedMilliseconds.ToString());
                }

                // 4/24/2015 dd - This will rethrow the exception with data information.
                throw; 
            }

        }

        protected virtual bool CopyCookiesFromResponse { get { return false; } }

        protected void LogInfo(string msg) 
        {
            string info = BotName + " / Login:" + ((this.Info != null) ? this.Info.Login + Environment.NewLine : "");
            Logger.Log(info + msg);
        }
        protected void LogErrorAndSendEmailToAdmin(string msg) 
        {
            Logger.LogErrorAndSendEmail(msg);
        }
        protected void LogErrorAndSendEmailToAdmin(string subject, string msg)
        {
            Logger.LogErrorAndSendEmail(subject, msg);
        }
        protected void LogErrorAndSendEmailToAdmin(Exception exc) 
        {
            LogErrorAndSendEmailToAdmin(exc.ToString());
        }

        // Meant to produce a subject that will stand out, in order to prevent spamming of login failure notices to eOPM
        protected void ReportDeactivation(string msg)
        {
            LogErrorAndSendEmailToAdmin(string.Format("LOGIN FAILURE: {0}", msg));
        }

        /// <summary>
        /// Store content of url to a temporary file. Return file name.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        protected string DownloadFile(string url) 
        {
            return DownloadFile(url, null);
        }

        private string DownloadFile(string url, string refererUrl) 
        {
            HttpWebRequest request = CreateRequest(url, "GET", refererUrl);

            string fileName = FileCfg.TEMP_FILE;

            using (HttpWebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {
                    WriteToFile(fileName, stream);
                }
            }

            return fileName;

        }

        protected string RawUploadDataAndReturnResponse(string url, string contentType, string rawData, NameValueCollection headersCollection, string refererUrl) 
        {
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(rawData);
            
            HttpWebRequest request = CreateRequest(url, "POST", refererUrl);
            request.ContentType = contentType;
            request.ContentLength = data.Length;

            if (null != headersCollection) 
            {
                request.Headers.Add(headersCollection);
            }
            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {
                    return GetString(stream);
                }
            }

        }
        protected string RawUploadDataAndReturnResponse(string url, string contentType, string rawData, NameValueCollection headersCollection) 
        {
            return RawUploadDataAndReturnResponse(url, contentType, rawData, null, null);
        }
        protected string RawUploadDataAndReturnResponse(string url, string contentType, string rawData) 
        {
            return RawUploadDataAndReturnResponse(url, contentType, rawData, null);
        }


        private string RawUploadDataDownloadFile(string url, string contentType, string rawData, NameValueCollection headersCollection, string refererUrl) 
        {
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(rawData);
            
            HttpWebRequest request = CreateRequest(url, "POST", refererUrl);
            request.ContentType = contentType;
            request.ContentLength = data.Length;

            if (null != headersCollection) 
            {
                request.Headers.Add(headersCollection);
            }
            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            string fileName = FileCfg.TEMP_FILE;
            using (HttpWebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {
                    WriteToFile(fileName, stream);
                }
            }
            return fileName;

        }

        protected string RawUploadDataDownloadFile(string url, string contentType, string rawData, NameValueCollection headersCollection) 
        {
            return RawUploadDataDownloadFile(url, contentType, rawData, headersCollection, null);

        }
        protected string RawUploadDataDownloadFile(string url, string contentType, string rawData) 
        {
            return RawUploadDataDownloadFile(url, contentType, rawData, null);
        }

        protected void UploadData(string url, NameValueCollection postData) 
        {
            UploadData(url, postData, null);
        }
        protected void UploadData(string url, StringBuilder sb)
        {
            UploadData(url, sb, null);
        }
        protected void UploadData(string url, StringBuilder sb, string refererUrl)
        {
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(sb.ToString());
            
            HttpWebRequest request = CreateRequest(url, "POST", refererUrl);
			
            request.ContentLength = data.Length;
            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                return; // NO-OP
            }
        }
        protected void UploadData(string url, NameValueCollection postData, string refererUrl) 
        {
            StringBuilder sb = new StringBuilder();
            bool bFirst = true;
            foreach (string key in postData.Keys) 
            {
                if (!bFirst) 
                {
                    sb.Append("&");
                }
                sb.AppendFormat("{0}={1}", key, UrlEncode(postData[key]));
                bFirst = false;

            }

            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(sb.ToString());
            
            HttpWebRequest request = CreateRequest(url, "POST", refererUrl);
			
            request.ContentLength = data.Length;
            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                return; // NO-OP
            }


        }
        protected string UploadDataDownloadFile(string url, NameValueCollection postData) 
        {
            return UploadDataDownloadFile(url, postData, null);
        }
        private string UploadDataDownloadFile(string url, NameValueCollection postData, string refererUrl) 
        {
            string fileName = FileCfg.TEMP_FILE;
            StringBuilder sb = new StringBuilder();
            bool bFirst = true;
            foreach (string key in postData.Keys) 
            {
                if (!bFirst) 
                {
                    sb.Append("&");

                }
                sb.AppendFormat("{0}={1}", key, UrlEncode(postData[key]));
                bFirst = false;
            }

            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(sb.ToString());
            
            HttpWebRequest request = CreateRequest(url, "POST", refererUrl);
            request.ContentLength = data.Length;

            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {
                    WriteToFile(fileName, stream);
                }
            }
            return fileName;

        }

        protected string PostXmlData(string url, string xml)
        {
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(xml.ToString());
            HttpWebRequest request = CreateRequest(url, "POST", null);
            request.ContentLength = data.Length;
            request.ContentType = "text/xml";
            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request))
            {
                using (Stream stream = response.GetResponseStream())
                {
                    return GetString(stream);
                }
            }

        }
        protected string UploadDataReturnResponse(string url, NameValueCollection postData) 
        {
            return UploadDataReturnResponse(url, postData, null);
        }
        protected string UploadDataReturnResponse(string url, StringBuilder sb)
        {
            return UploadDataReturnResponse(url, sb, null);
        }
        protected string UploadDataReturnResponse(string url, StringBuilder sb, string refererUrl)
        {
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(sb.ToString());

            HttpWebRequest request = CreateRequest(url, "POST", refererUrl);
            request.ContentLength = data.Length;

            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request))
            {
                using (Stream stream = response.GetResponseStream())
                {
                    return GetString(stream);
                }
            }
        }
        protected string UploadDataReturnResponse(string url, NameValueCollection postData, string refererUrl) 
        {
            StringBuilder sb = new StringBuilder();
            bool bFirst = true;
            foreach (string key in postData.Keys) 
            {
                if (!bFirst) 
                {
                    sb.Append("&");

                }

                sb.AppendFormat("{0}={1}", key, UrlEncode(postData[key]));
                bFirst = false;
            }

            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(sb.ToString());
            
            HttpWebRequest request = CreateRequest(url, "POST", refererUrl);
            request.ContentLength = data.Length;

            using (Stream stream = request.GetRequestStream()) 
            {
                stream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = GetResponse(request)) 
            {
                using (Stream stream = response.GetResponseStream()) 
                {
                    return GetString(stream);
                }
            }

        }

        public static string UrlEncode(string str) 
        {
            if (null == str)
                return "";

            StringBuilder sb = new StringBuilder();

            foreach (char ch in str.ToCharArray()) 
            {
                if (ch == ' ') 
                    sb.Append('+');
                else if (IsSafeChar(ch))
                    sb.Append(ch);
                else
                    sb.Append(ConvertToHex(ch));
            }
            return sb.ToString();


        }
        private static bool IsSafeChar(char ch) 
        {
            if (char.IsLetterOrDigit(ch))
                return true;
            switch (ch) 
            {
                case '\'':
                case '(':
                case ')':
                case '*':
                case '-':
                case '.':
                case '!':
                case '_':
                    return true;
                default:
                    return false;
            }
        }
        private static string ConvertToHex(char ch) 
        {

            ushort _v = (ushort) ch;
            string s = "%" + IntToHex((_v >> 4) & 15) + IntToHex(_v & 15);
            return s;
            
        }
        private static char IntToHex(int i) 
        {
            if (i < 10)
                return (char) (i + 48);
            else
                return (char) (i + 55);
        }
        /// <summary>
        /// Just go to the website without need to return html.
        /// </summary>
        /// <param name="url"></param>
        protected void GoToUrl(string url) 
        {
            GoToUrl(url, null);
        }

        protected void GoToUrl(string url, string refererUrl) 
        {
            HttpWebRequest request = CreateRequest(url, "GET", refererUrl);

            using (WebResponse response = GetResponse(request))
            {
                ; // NO-OP
            }
        }

        protected void GoToUrlWithPostMethod(string url)
        {
            HttpWebRequest request = CreateRequest(url, "POST", null);
            
            using (WebResponse response = GetResponse(request))
            {
                ; // NO-OP
            }
        }

        protected string DownloadData(string url) 
        {
            return DownloadData(url, null);
        }

        /// <summary>
        /// Go to url and return HTML content
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        protected string DownloadData(string url, string refererUrl) 
        {
            HttpWebRequest request = CreateRequest(url, "GET", refererUrl);

            using (WebResponse response = GetResponse(request)) 
            {

                using (Stream stream = response.GetResponseStream()) 
                {
                    return GetString(stream);
                }
            }
        }

        private void DebugResponse(WebResponse response) 
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Url={0}{1}",response.ResponseUri, Environment.NewLine);
            sb.AppendFormat("ContentType={0}{1}", response.ContentType, Environment.NewLine);
            foreach (string key in response.Headers.Keys) 
            {
                sb.AppendFormat("Headers[{0}]=[{1}]{2}", key, response.Headers[key], Environment.NewLine);
            }
            LogInfo(sb.ToString());
        }

        private void WriteToFile(string sFileName, Stream str)
        {
            using (FileStream fs = File.OpenWrite(sFileName))
            {
                const int BUFSIZ = 50000;
                byte[] buf = new byte[BUFSIZ];
                int n;
                try
                {
                    while ((n = str.Read(buf, 0, BUFSIZ)) > 0)
                    {
                        fs.Write(buf, 0, n);
                    }
                }
                catch (IOException)
                {
                    //NO-OP
                }
            }
            /*
                        try
                        {
                            BinaryFileHelper.WriteAllBytes(sFileName, str);
                        }
                        catch (IOException)
                        {
                            //NO-OP
                        }
            */
        }

        private string GetString(Stream stream) 
        {
            const int BUFFER_SIZE = 50000;
            byte[] buffer = new byte[BUFFER_SIZE];
            int n = 0;

            StringBuilder sb = new StringBuilder();
            while ((n = stream.Read(buffer, 0, BUFFER_SIZE)) > 0) 
            {
                sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, n));
            }
            return sb.ToString();
        }



        public abstract string BotName 
        {
            get;
        }
        public string RateSheetType 
        {
            get 
            {
                if (null != m_info)
                    return m_info.Description;
                else
                    return "";
            }
        }
        public IDownloadInfo Info 
        {
            get { return m_info; }
        }

        public virtual string ConversionMethod 
        {
            get 
            {
                if (null != m_info)
                    return m_info.Conversion;
                else
                    return "";
            }
        }

        public string RatesheetFileName { get; protected set; }
        public E_BotDownloadStatusT Download() 
        {
            E_BotDownloadStatusT status = DownloadFromWebsite(m_info);

            switch (status)
            {
                case E_BotDownloadStatusT.SuccessfulWithRatesheet:
                case E_BotDownloadStatusT.SuccessfulWithNoRatesheet:
                case E_BotDownloadStatusT.NoRatesheetMarkRatesheetExpire:
                case E_BotDownloadStatusT.Error:
                    return status;
                case E_BotDownloadStatusT.SuccessfulWithManualRatesheet:
                    throw new Exception("Invalid programming stage. BotDownloadStatus cannot be SuccessfulWithManualRatesheet");
                default:
                    throw new Exception("Unhandle E_BotDownloadStatusT enum value=" + status);
            }
        }

        /// <summary>
        /// Download actual ratesheet from the investor website, and return file name.
        /// </summary>
        /// <param name="dlinfo"></param>
        /// <returns></returns>
        protected virtual E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            RatesheetFileName = DownloadFile(dlinfo.Url);

            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            //return DownloadFile(dlinfo.Url);
        }

        public string LibraryKey 
        {
            get 
            {
                if (m_info != null)
                    return m_info.Id + "_" + m_info.Url + RateSheetType + m_info.Login;
                else
                    return "";
            }
        }

        protected void WriteToFile(string sFileName, byte[] rgb)
        {
            using (FileStream fs = File.OpenWrite(sFileName))
            {
                fs.Write(rgb, 0, rgb.Length);
            }
/*
            BinaryFileHelper.WriteAllBytes(sFileName, rgb);
*/
        }

        protected string WriteToTempFile(string content) 
        {
            string fileName = FileCfg.TEMP_FILE;
            
            WriteToFile(fileName, System.Text.Encoding.ASCII.GetBytes(content));

            return fileName;
        }
    }
}
