/// Author: David Dao
namespace LpeUpdateBotLib.Common
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.IO;
    //using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Only constants store in .config file store here.
    /// </summary>
    public class Config 
    {
        private static object s_hashLock = new object();
        private static Hashtable s_hash;

        static Config() 
        {
            lock (s_hashLock) 
            {
                s_hash = new Hashtable(ConfigurationManager.AppSettings.Keys.Count);
                foreach (string key in ConfigurationManager.AppSettings.Keys) 
                {
                    s_hash.Add(key, ConfigurationManager.AppSettings[key]);
                }
            }
        }

        /// <summary>
        /// Per Yin in OPM 126503
        /// The bots should download when ever IsDuringLpeUpdatePeriod is true AND its not saturday
        /// </summary>
        public static bool IsDuringDownloadBotPeriod
        {
            get
            {
                return IsDuringLpeUpdatePeriod && DateTime.Now.DayOfWeek != DayOfWeek.Saturday;
            }
        }

        public static bool IsDuringLpeUpdatePeriod
        {
            get
            {
                // 1/5/2011 dd - We use this property to determine if we should download rate from investor website.
                // We only run LpeUpdate process Mon-Friday 4:00AM - 8:00PM PST
                DateTime dt = DateTime.Now;
                if (dt.DayOfWeek == DayOfWeek.Saturday)
                {
                    return dt.Hour >= 4 && dt.Hour < 14; 
                }
                if (dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    // 2016-03-28 - OPM 239040 - Allow LPE update process to work on Sunday.
                    return dt.Hour >= 4 && dt.Hour < 14;
                }
                if (dt.Hour >= 4 && dt.Hour <= 20)
                {
                    // 4:00 - 20:00
                    return true;
                }
                return false;
            }
        }

        public static string FILES_PATH 
        {
            get { return GetString("FILES_PATH"); }
        }

        public static int NumberOfDownloadThreads 
        {
            get { return GetInt("NumberOfDownloadThreads", 3); }
        }

		public static string PmlRatesTestEmailAddress 
		{
			get { return GetString("PmlRatesTestEmailAddress", "pmlratestest@meridianlink.com"); }
		}

        public static string ReportingEmailAddressRecipient 
        {
            get { return GetString("ReportingEmailAddressRecipient", ""); }
        }

        public static string PmlRatesEmailAddressPassword 
        {
            get { return GetString("PmlRatesEmailAddressPassword", "krubkrub"); }
        }

		public static string PmlRatesTestEmailAddressPassword 
		{
			get { return GetString("PmlRatesTestEmailAddressPassword", "Balurdo125"); }
		}

        public static bool IsTestingMode 
        {
            get { return GetBool("IsTestingMode", false); }
        }

		public static bool IsPrintDiagnosticMsgMode 
		{
			get { return GetBool("IsPrintDiagnosticMsgMode", false); }
		}

		public static bool IsTestRatesheetMapMode 
		{
			get { return GetBool("IsTestRatesheetMapMode", false); }
		}

		public static int MaxRatesheetMapFilesToProcess 
		{
			get { return GetInt("MaxRatesheetMapFilesToProcess", 0); }
		}

		public static bool PrintRatesheetMapOutputIn1D
		{
			get { return GetBool("PrintRatesheetMapOutputIn1D", true); }
		}

		public static string RatesheetFileNameToProcess
		{
			get { return GetString("RatesheetFileNameToProcess", ""); }
		}

		public static string MarketDataFileName
		{
			get { return GetString("MarketDataFileName", ""); }
		}

		public static string MarketDataDependentFilesFileName
		{
			get { return GetString("ListOfMarketDataDepFilenames", ""); }
		}

		public static bool UseTemplatePathForRatesheetInput
		{
			get { return GetBool("UseTemplatePathForRatesheetInput", false); }
		}

		public static bool IsRunRSMapProcessorInTandem 
		{
			get { return GetBool("IsRunRSMapProcessorInTandem", false); }
		}

		public static bool IsSendRatesheetMapOPMNotif 
		{
			get { return GetBool("IsSendRatesheetMapOPMNotif", false); }
		}

        public static bool IsTestDownloadFromWeb 
        {
            get { return GetBool("IsTestDownloadFromWeb", false); }
        }

        public static bool IsTestDownloadFromEmail 
        {
            get { return GetBool("IsTestDownloadFromEmail", false); }
        }

        public static bool IsTestDownloadByBroker 
        {
            get { return GetBool("IsTestDownloadByBroker", false); }
        }

        public static string TestBotSqlQuery 
        {
            get { return GetString("TestBotSqlQuery", ""); }
        }

        public static string TestCWSqlQuery
        {
            get { return GetString("TestCWSqlQuery", ""); }
        }

        public static int CWPollingInterval 
        {
            get { return GetInt("CWPollingInterval", 30000); }
        }

        public static string AlternateHostUrl
        {
            get { return GetString("AlternateHostUrl", "http://localhost/ManualRSFile"); }
        }

        #region Config for LpeBotConsole
        public static string LpeBotConsoleTestMode 
        {
            get { return GetString("LpeBotConsoleTestMode", ""); }
        }
        public static string LpeBotConsoleTestModeValue 
        {
            get { return GetString("LpeBotConsoleTestModeValue", ""); }
        }
        public static string ManualBotInfo_BotType 
        {
            get { return GetString("ManualBotInfo_BotType", ""); }
        }
        public static string ManualBotInfo_Description 
        {
            get { return GetString("ManualBotInfo_Description", ""); }
        }
        public static string ManualBotInfo_FileName 
        {
            get { return GetString("ManualBotInfo_FileName", ""); }
        }
        public static string ManualBotInfo_Url 
        {
            get { return GetString("ManualBotInfo_Url", ""); }
        }
        public static string ManualBotInfo_Login 
        {
            get { return GetString("ManualBotInfo_Login", ""); }
        }
        public static string ManualBotInfo_Password 
        {
            get { return GetString("ManualBotInfo_Password", ""); }
        }
        public static DateTime ManualBotInfo_LastRatesheetTimestamp
        {
            get 
            {
                try
                {
                    return DateTime.Parse(GetString("ManualBotInfo_LastRatesheetTimestamp", ""));
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }
        public static string ManualBotInfo_Token
        {
            get { return GetString("ManualBotInfo_Token", ""); }
        }
        #endregion

        #region Utilities methods
        private static string GetString(string key) 
        {
            lock (s_hashLock) 
            {
                string ret = (string) s_hash[key];
                if (ret == null || ret == "")
                    throw new ApplicationException(key + " is not defined in config file.");

                return ret;
            }
        }

        private static string GetString(string key, string defaultValue) 
        {
            lock (s_hashLock) 
            {
                string ret = (string) s_hash[key];
                if (ret == null)
                    ret = defaultValue;

                return ret;
            }

        }

        private static int GetInt(string key) 
        {
            string ret = GetString(key);
            try 
            {
                int i = int.Parse(ret);
                return i;
            } 
            catch 
            {
                throw new ApplicationException(key + " defined in config file is not a valid integer value.  Value = " + ret);
            }
        }

        private static int GetInt(string key, int defaultValue) 
        {
            int value = defaultValue;
            try 
            {
                string ret = GetString(key);

                value = int.Parse(ret);
            } 
            catch {   }

            return value;
        }
        private static bool GetBool(string key) 
        {
            string ret = GetString(key);

            switch (ret.ToLower()) 
            {
                case "true": return true;
                case "false": return false;
                default:
                    throw new ApplicationException(key + " defined in config file is not a valid bool value ['True', 'False'].  Value = " + ret);
            }
        }

        private static bool GetBool(string key, bool defaultValue) 
        {
            bool value = defaultValue;
            try 
            {
                value = GetBool(key);
            } 
            catch {}
            return value;
        }

        #endregion

    }
    public class PathCfg
    {

        public static void VerifyAndSetupRelatedPaths()
        {
            string[] pathList = {
                                    DOWNLOAD_PATH,
                                    DOWNLOAD_PATH_VALIDATION_FAILED,
                                    MARKET_INDEX_PATH,
                                    RS_MAP_FILES_PATH,
                                    RS_MAP_TEST_OUTPUT_FILES_PATH,
                                    RS_MAP_INPUT_SCHEMA_PATH,
                                    RS_MAP_TEST_FILES_PATH,
                                    OUTPUT_FILE_CACHE_PATH,
                                    DOWNLOAD_RawCache,
                                    DOWNLOAD_ProcessedCache,
                                    DOWNLOAD_RsArchive,
                                    DOWNLOAD_RsLibrary,
                                    BOT_DOWNLOAD_PATH,
                                    Config.FILES_PATH + @"Logs\"
                                };

            foreach (var path in pathList)
            {
                if (Directory.Exists(path) == false)
                {
                    Directory.CreateDirectory(path);
                }
            }
        }
        public static string BOT_DOWNLOAD_PATH
        {
            get { return Config.FILES_PATH + @"BotDownloadPath\"; }
        }
        public static string DOWNLOAD_PATH
        {
            get { return Config.FILES_PATH + @"DownloadPath\" ; }
        }

        public static string DOWNLOAD_PATH_VALIDATION_FAILED
        {
            get { return Config.FILES_PATH + @"DownloadPathValidationFailed\"; }
        }

		//Adding for ratesheet map project
		public static string MARKET_INDEX_PATH
		{
			get { return Config.FILES_PATH + @"MarketIndexFiles\" ; }
		}

		public static string RS_MAP_FILES_PATH
		{
			get { return Config.FILES_PATH + @"RatesheetMapFiles\" ; }
		}
        public static string RS_MAP_INPUT_SCHEMA_PATH
        {
            get { return Config.FILES_PATH + @"InputSchemas\"; }
        }
		public static string RS_MAP_TEST_OUTPUT_FILES_PATH
		{
			get { return Config.FILES_PATH + @"TestRatesheetMapOutputFiles\" ; }
		}

		public static string RS_MAP_TEST_FILES_PATH
		{
			get { return Config.FILES_PATH + @"TestRatesheetMapFiles\" ; }
		}

		public static string OUTPUT_FILE_CACHE_PATH
		{
			get { return Config.FILES_PATH + @"OutputFileCache\" ; }
		}

        /// <summary>
        /// Temporary folder holding the Raw version of a downloaded Ratesheet
        /// </summary>
        public static string DOWNLOAD_RawCache
        {
            get { return DOWNLOAD_PATH + @"RawCache\" ; }
        }
        /// <summary>
        /// Temporary folder holding the Processed version of a downloaded Ratesheet
        /// </summary>
        public static string DOWNLOAD_ProcessedCache
        {
            get { return DOWNLOAD_PATH + @"ProcessedCache\" ; }
        }
        /// <summary>
        /// Hold a history of downloaded Ratesheet for customer service purposes
        /// </summary>
        public static string DOWNLOAD_RsArchive
        {
            get { return DOWNLOAD_PATH + @"RsArchive\" ; }
        }

        public static string DOWNLOAD_RsLibrary
        {
            get { return DOWNLOAD_PATH + @"RsLibrary\"; }
        }
        public static string MapSchemaPath(string sFileName)
        {
            return Config.FILES_PATH + sFileName ;
        }
        public static string MapCompactedSchemaPath(string sFileName)
        {
            return RS_MAP_INPUT_SCHEMA_PATH + sFileName;
        }
        public static string MapLibraryPath(string sFileName)
        {
            /// The RsLibrary holds a Raw and Processed copy of the current Ratesheet that's being in use in production.
            return DOWNLOAD_RsLibrary + sFileName;
        }


    }
    public class FileCfg
    {

        private static string s_currentRunTimeStr = "";
        public static DateTime CurrentRunTime 
        {
            //get { return s_currentRunTime; }
            set 
            {
//                int daysToKeepTemp = 3; // Due to space on HD we only keep 3 days of temp files.
//                int index = value.DayOfYear % daysToKeepTemp;

                // Only keep temp file for 1 hour.
                s_currentRunTimeStr = AppDomain.CurrentDomain.FriendlyName + "_" + value.ToString("mm");
                // 1/25/2011 dd - We are running continuous now, therefore need to reset the sTempIndex.
                s_TempIndex = 0;
            }
        }
        public static string TEMP_FILE
        {
            get 
            {
                string sTempFile ;
                while (true) 
                {
                    try 
                    {
                        lock(s_lock)
                        {
                            string folder = null;
                            if (s_currentRunTimeStr != "") 
                            {
                                folder = Config.FILES_PATH + @"Temp\" + s_currentRunTimeStr;
                            } 
                            else 
                            {
                                folder = Config.FILES_PATH + @"Temp";
                            }
                            if (!Directory.Exists(folder))
                            {
                                Directory.CreateDirectory(folder);
                            }
                            sTempFile = folder + @"\Temp" + (s_TempIndex++).ToString() + ".txt" ;
                        }
                        if (File.Exists(sTempFile))
                            File.Delete(sTempFile);
/*
                        if (FileOperationHelper.Exists(sTempFile))
                            FileOperationHelper.Delete(sTempFile);
*/
                        return sTempFile ;
                    } 
                    catch 
                    {
                        // Retries when there is error create new temp file name.
                    }
                }

            }
        }
        public static string LAST_MESSAGE_FLAG
        {
            get { return Config.FILES_PATH + "last_msg.txt" ; }
        }
        public static string DEPENDENCY_FILE
        {
            get { return Config.FILES_PATH + "DependConfig.xml" ; }
        }
        public static string DISABLE_EVENT_EXCEL
        {
            get { return Config.FILES_PATH + "DisableEvents.xls" ; }
        }

        static int s_TempIndex = 0 ;

        static object s_lock = new object() ;
    }

}
