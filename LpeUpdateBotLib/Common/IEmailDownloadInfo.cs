﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpeUpdateBotLib.Common
{
    public interface IEmailDownloadInfo
    {
        int Id { get; }
        DateTime DLastUpdated { get; }
        string Sender { get; }
        string Body { get; }
        string MailFileName { get; }
        string[] TargetFileNames { get; }
        string MainTargetFileName { get; }

        string BotType { get; }
        string Subject { get; }
        string Description { get; }

        void UpdateLastUpdated();
        void ExpireCurrentRateSheet();
        void RegisterRate(Guid priceGroupId, Guid templateId, string rate, string point, string margin, string qualrate);
        void RegisterLockPeriod(Guid priceGroupId, Guid templateId, int start, int end);
        void PersistRateData(DateTime startTime, DateTime endTime, string outputFileName);

        bool IsSubjectMatch(string subject);
    }
}
