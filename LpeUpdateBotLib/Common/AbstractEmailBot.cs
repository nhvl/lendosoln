/// Author: David Dao

using System;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.Common
{

	public abstract class AbstractEmailBot : AbstractWebBot
	{
        protected Pop3Message Message
        {
            get;
            private set;
        }
		public AbstractEmailBot(IDownloadInfo info, Pop3Message msg) : base(info)
		{
            Message = msg;
		}

        public abstract bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg);

        public virtual bool PreProcessEmailAndQuit()
        {
            return false;
        }
	}
}
