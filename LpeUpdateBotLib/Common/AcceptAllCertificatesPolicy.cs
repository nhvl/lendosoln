/// Author: David Dao

using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
namespace LpeUpdateBotLib.Common
{
    public class AcceptAllCertificatesPolicy : ICertificatePolicy
    {
        public bool CheckValidationResult(
            ServicePoint srvPoint, 
            System.Security.Cryptography.X509Certificates.X509Certificate certificate,
            WebRequest request, int certificateProblem)
        {
            // TODO:  Add AcceptAllCertificatesPolicy.CheckValidationResult implementation
            return true;
        }
    }
}
