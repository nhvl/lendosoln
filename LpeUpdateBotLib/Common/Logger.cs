/// Author: David Dao

using System;
using System.IO;
namespace LpeUpdateBotLib.Common
{
    public class Logger
    {
        private static ILogger m_loggerImpl = new DefaultLogger();
        public static void Log(string msg) 
        {
            if (null != m_loggerImpl)
                m_loggerImpl.Log(msg);
        }

        public static void LogErrorAndSendEmail(string msg) 
        {
            if (null != m_loggerImpl)
                m_loggerImpl.LogErrorAndSendEmail(msg);
        }

        public static void LogErrorAndSendEmail(string subject, string msg)
        {
            if (null != m_loggerImpl)
                m_loggerImpl.LogErrorAndSendEmail(subject, msg);
        }

        public static void SetImpl(ILogger loggerImpl) 
        {
            m_loggerImpl = loggerImpl;
        }
    }

    public interface ILogger 
    {
        void Log(string msg);
        void LogErrorAndSendEmail(string msg);
        void LogErrorAndSendEmail(string subject, string msg);
    }

    public class DefaultLogger : ILogger
    {
        private object m_lock = new object();
        public void Log(string msg) 
        {
            lock(m_lock) 
            {
                using (StreamWriter writer = new StreamWriter("log.txt", true)) 
                {
                    writer.WriteLine("[{0} {1}] - {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), msg);
                }
                System.Diagnostics.Debug.WriteLine(msg);
            }
        }

        public void LogErrorAndSendEmail(string msg) 
        {
            Log("[ERROR] - " + msg);
            // NO-OP
        }

        public void LogErrorAndSendEmail(string subject, string msg)
        {
            Log("[ERROR] - " + msg);
            // NO-OP
        }
    }
}
