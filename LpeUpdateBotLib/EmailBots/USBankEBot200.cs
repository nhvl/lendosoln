﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class USBankEBot200 : AbstractEmailBot
    {
        private static Regex messageURL100_regex = new Regex(@"<a href=""(?<rsURL>[^""]+)"" style=""color: black;"" target=""_blank"" title=""pull file"">USBank_\d{4}v2.xlsx</a>", RegexOptions.IgnoreCase);
        private static Regex messageURL200_regex = new Regex(@"<a href=""(?<rsURL>[^""]+)"" style=""color: black;"" target=""_blank"" title=""pull file"">CC_\d{4}v2.xlsx</a>", RegexOptions.IgnoreCase);
        private static Regex messageURL210_regex = new Regex(@"<a href=""(?<rsURL>[^""]+)"" style=""color: black;"" target=""_blank"" title=""pull file"">CC_T1_Conv_Govt_\d{4}v2.xlsx</a>", RegexOptions.IgnoreCase);
        private static Regex messageURL211_regex = new Regex(@"<a href=""(?<rsURL>[^""]+)"" style=""color: black;"" target=""_blank"" title=""pull file"">EasyD_CC_\d{4}v2.xlsx</a>", RegexOptions.IgnoreCase);
        private static Regex messageURL215_regex = new Regex(@"<a href=""(?<rsURL>[^""]+)"" style=""color: black;"" target=""_blank"" title=""pull file"">CC_T1_Conv_\d{4}v2.xlsx</a>", RegexOptions.IgnoreCase);
        private static Regex messageURL216_regex = new Regex(@"<a href=""(?<rsURL>[^""]+)"" style=""color: black;"" target=""_blank"" title=""pull file"">CC_T1_Govt_\d{4}v2.xlsx</a>", RegexOptions.IgnoreCase);
 
        public USBankEBot200(IDownloadInfo info, Pop3Message message) : base (info, message)
        {
        }

        public override string BotName
        {
            get { return "USBANKEMAIL200"; }
        }

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match messageURL_match = messageURL200_regex.Match(EmailBody);
            if (messageURL_match.Success)
            {
                string ratesheetURL = null;
                string newURL = null;

                ratesheetURL = messageURL_match.Groups["rsURL"].Value;
                newURL = ratesheetURL.Replace("xlsx", "xlsx?download=yes").Replace("/p/", "/cgi-bin/pull/DocPull/");

                RatesheetFileName = DownloadFile(newURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("US Bank Email bot failed to find URL link. Content: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            return true;
        }

    }
}
