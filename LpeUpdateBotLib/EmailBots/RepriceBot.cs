/// Author: Brian Beery
/// Date: 12-17-08
/// Purpose: Process a re-price notification

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;
using System.Linq;

namespace LpeUpdateBotLib.EmailBots
{
    public class RepriceBot : AbstractEmailBot
    {
        private string EmailBody
        {
            get
            {
                return Message.HTMLBody;
            }
        }
        private string EmailSubject
        {
            get
            {
                return Message.Subject;
            }
        }
        private int EmailAttachmentCount
        {
            get
            {
                return Message.Attachments.Count();
            }
        }

        public RepriceBot(IDownloadInfo info, Pop3Message message) : base(info, message)
        {
        }
        public override string BotName 
        {
            get { return "REPRICE" ; }
        }
        
        //Doesn't actually download anything; just using the current bot structure
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            dlinfo.ExpireRatesheet();
            LogInfo(string.Format("Reprice bot expired pricing for {0}.", dlinfo.MainFileName));
			return E_BotDownloadStatusT.Error ;
        }

        //Convert the time zone to a format recognizable by the DateTime class
        private string ConvertTZtoUTCOffset(string sTimezone)
        {
            switch (sTimezone.ToUpper())
            {
                case "EDT":
                    sTimezone = "-4" ;
                    break ;
                    
                case "EST":
                case "ET":
                case "CDT":
                    sTimezone = "-5" ;
                    break ;

                case "CST":
                case "CT":
                case "MDT":
                    sTimezone = "-6" ;
                    break ;

                case "MST":
                case "MT":
                case "PDT":
                    sTimezone = "-7" ;
                    break ;

                case "PST":
                case "PT":
                case "AKDT":
                    sTimezone = "-8" ;
                    break ;

                case "AKST":
                case "AKT":
                case "HADT":
                    sTimezone = "-9" ;
                    break ;

                case "HAST":
                case "HAT":
                    sTimezone = "-10" ;
                    break ;

                default:
                    sTimezone = "-8" ;
                    break ;
            }
            return sTimezone ;
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            IEmailDownloadInfo info = mailMask.EmaiLDownloadInfo;
            if (mailMask.DoNonAttachmentFieldsMatch(msg) == false)
            {
                return false;
            }

            var repriceAttachment = mailMask.GetAttachmentOnlyIfAllAttachmentFieldsMatch(msg);

            if (repriceAttachment == null)
            {
                return false;
            }

            return true;
        }

    }

}
