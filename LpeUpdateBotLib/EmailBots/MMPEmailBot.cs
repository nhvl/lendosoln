// Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class MMPEmailBot : AbstractEmailBot
    {
        // regex to find ratesheet URL in email HTML
        private static Regex messageURL_regex = new Regex(@"href=""(?<rsURL>[^""]+)""\s+target=""_blank""(?<linkGarbage>[^>]+)><(?<colorGarbage>[^>]+)><u><em><span(?<fontGarbage>[^>]+)>Click here for a printable rate sheet</span>", RegexOptions.IgnoreCase);

        public MMPEmailBot(IDownloadInfo info, Pop3Message message) : base (info, message)
        {
        }

        public override string BotName
        {
            get { return "MMPEMAIL"; }
        }

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match messageURL_match = messageURL_regex.Match(EmailBody);

            if (messageURL_match.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = HttpUtility.HtmlDecode(messageURL_match.Groups["rsURL"].Value);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("MMP Email Bot failed to find URL link. Content: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            return true;
        }

    }
}
