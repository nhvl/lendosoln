﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;


namespace LpeUpdateBotLib.EmailBots
{
    class ChenoaEmailBot : AbstractEmailBot
    {
        private static Regex messageURL1_regex = new Regex(@"href=""(?<rsURL1>[^""]+)""(?<garbage>[^>]+)>Daily Rate Sheet", RegexOptions.IgnoreCase);
        private static Regex messageURL2_regex = new Regex(@"href=""(?<rsURL2>[^""]+)""(?<garbage>[^>]+)>(?<garbage1>[^>]+)>Daily Rate Sheet", RegexOptions.IgnoreCase);
        private static Regex messageURL3_regex = new Regex(@"href=""(?<rsURL3>[^""]+)""(?<garbage>[^>]+)>(?<garbage1>[^>]+)><(?<garbage2>[^>]+)>Daily Rate Sheet", RegexOptions.IgnoreCase);
        private static Regex messageURL4_regex = new Regex(@"href=""(?<rsURL4>[^""]+)""(?<garbage>[^>]+)>(?<garbage1>[^>]+)><(?<garbage2>[^>]+)><(?<garbage3>[^>]+)>Daily Rate Sheet", RegexOptions.IgnoreCase);
        private static Regex messageURL5_regex = new Regex(@"href=""(?<rsURL5>[^""]+)""(?<garbage>[^>]+)>(?<garbage1>[^>]+)><(?<garbage2>[^>]+)><(?<garbage3>[^>]+)><(?<garbage4>[^>]+)>Daily Rate Sheet", RegexOptions.IgnoreCase);

        public ChenoaEmailBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "ChenoaEmail"; }
        }
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match messageURL_match1 = messageURL1_regex.Match(EmailBody);
            Match messageURL_match2 = messageURL2_regex.Match(EmailBody);
            Match messageURL_match3 = messageURL3_regex.Match(EmailBody);
            Match messageURL_match4 = messageURL4_regex.Match(EmailBody);
            Match messageURL_match5 = messageURL5_regex.Match(EmailBody);

            if (messageURL_match1.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = HttpUtility.HtmlDecode(messageURL_match1.Groups["rsURL1"].Value);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (messageURL_match2.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = HttpUtility.HtmlDecode(messageURL_match2.Groups["rsURL2"].Value);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (messageURL_match3.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = HttpUtility.HtmlDecode(messageURL_match3.Groups["rsURL3"].Value);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (messageURL_match4.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = HttpUtility.HtmlDecode(messageURL_match4.Groups["rsURL4"].Value);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else if (messageURL_match5.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = HttpUtility.HtmlDecode(messageURL_match5.Groups["rsURL5"].Value);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Chenoa Email Bot failed to find URL link. Content: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            return true;
        }
    }
}
