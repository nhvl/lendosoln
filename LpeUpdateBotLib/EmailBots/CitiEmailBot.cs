/// Author: Sam Y
/// Date: 3/22/2017
/// Purpose: Download Citi's RS via link in the email

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;
namespace LpeUpdateBotLib.EmailBots
{
    public class CitiEmailBot : AbstractEmailBot
    {
        private static Regex webversion_regex = new Regex(@"If you are having trouble reading this email,\s+<a href=""(?<webversion>[^""]+)", RegexOptions.IgnoreCase);
        private static Regex rslink_regex = new Regex(@"a href=""(?<rsFile>[^""]+)"" data-targettype=""file"" data-targetname=""{.+_Citi_Correspondent_Rates.xlsx", RegexOptions.IgnoreCase);

        public CitiEmailBot(IDownloadInfo info, Pop3Message message): base(info, message)
        {
        }
        public override string BotName 
        {
            get { return "CITIEMAILBOT"; }
        }
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            Match match = webversion_regex.Match(EmailBody);
            string webVersionText = null;
            string rsFile = null;
            if (match.Success)
            {
                webVersionText = DownloadData(match.Groups["webversion"].Value);

                Match match2 = rslink_regex.Match(webVersionText);
                if (match2.Success)
                {
                    rsFile = DownloadFile(match2.Groups["rsFile"].Value);
                }
                else
                {
                    LogErrorAndSendEmailToAdmin("Citi Email Bot can't find rsLink. Content: " + EmailBody);
                }
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Citi Email Bot can't find webversion. Content: " + EmailBody);
            }

            if (null != rsFile)
            {
                RatesheetFileName = rsFile;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            if (msg.Subject.Contains(mailMask.Subject))
            {
                return true;
            }
            else
            {
                string s = string.Format("Could not match Subject when processing Citi bot.  Looking for the text from download list ({2}) anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject, mailMask.Subject);
                string subject = string.Format("Error processing email: {0}, {1}", msg.From);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }

        }
    }

}
