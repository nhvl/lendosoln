﻿// Author Budi Sulayman
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;

namespace LpeUpdateBotLib.EmailBots
{
    public class MerrimackEBot : AbstractEmailBot
    {
        private static Regex rslink_regex = new Regex(@"href=\'(?<rstemp>[^\']+)\'>Click here for an Excel version of the ratesheet", RegexOptions.IgnoreCase);
        
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        public MerrimackEBot(IDownloadInfo info, jmail.Message msg)
            : base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "MERRIMACK"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match match = rslink_regex.Match(EmailBody);
            string sFileName = null;
            if (match.Success)
            {
                string temp = HttpUtility.UrlDecode(match.Groups["rstemp"].Value.ToString());
                sFileName = DownloadFile(temp);
            }
            else
                LogErrorAndSendEmailToAdmin("MerrimackEBot can't find ratesheet URL. EmailBody: " + EmailBody);
            if (sFileName != null)
            {
                RatesheetFileName = sFileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }


        public override bool IsValidEmailMessage(MailMask mailMask, jmail.Message msg)
        {
            if ((msg.Subject.IndexOf("Merrimack Mortgage Rates") >= 0) || (msg.Subject.IndexOf("Merrimack Mortgage Rate") >= 0) || (msg.Subject.IndexOf("Merrimack Mortgage rate") >= 0) || (msg.Subject.IndexOf("Merrimack Mortgage 11") >= 0))
                return true;
            else if (msg.Subject.IndexOf("Merrimack Mortgage Company Immediate Reprice Notification") >= 0)
                return false;
            else
            {
                string s = string.Format("Could not match Subject when processing MerrimackEBot.  Looking for the text 'Merrimack Mortgage Rates' anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }
        }
    }
}