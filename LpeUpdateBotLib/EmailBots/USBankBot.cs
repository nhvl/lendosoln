/// Author: David Dao

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

//USBank Standard bot
namespace LpeUpdateBotLib.EmailBots
{
    public class USBankBot : AbstractEmailBot
    {
        private static Regex s_XLSRegex = new Regex(@"<font face=""Times New Roman"" size=""3""><a href=""(?<rslink>[^""]+)"">.+XLSX</a>", RegexOptions.IgnoreCase);

        public USBankBot(IDownloadInfo info, Pop3Message msg) : base (info, msg)
        {
        }

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        public override string BotName 
        {
            get { return "USBANK"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string sFileName = null;
            Match mcLinks = s_XLSRegex.Match(EmailBody);
            if (mcLinks.Success)
            {
                sFileName = mcLinks.Groups["rslink"].Value.ToString();
                RatesheetFileName = DownloadFile(sFileName);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogInfo(string.Format("(USBankBot) Email did not include expected xls link.\n{0}", EmailBody));
                LogErrorAndSendEmailToAdmin(string.Format("(USBankBot) Email did not include expected xls link.\n{0}", EmailBody));
                return E_BotDownloadStatusT.Error;
            }
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            return true;
        }
    }

}
