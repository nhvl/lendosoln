//Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;
namespace LpeUpdateBotLib.EmailBots
{
    public class NHFSapphireEBot : AbstractEmailBot
    {
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        public NHFSapphireEBot(IDownloadInfo info, Pop3Message message)
            : base(info, message)
        {
        }
        public override string BotName
        {
            get { return "SAPPHIRE"; }
        }
        public override string ConversionMethod
        {
            get { return "HTML2EXCEL"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string badRatesheetFileName = null;
            string goodRatesheetFileName = null;

            // get incomple HTML of their email to pmlrates
            badRatesheetFileName = EmailBody.ToString();
            // add necessary tags to create complete HTML
            goodRatesheetFileName = badRatesheetFileName.Replace("<meta", "<html><head><meta").Replace("ascii\">", "ascii\"></head><body><div>");
            goodRatesheetFileName = goodRatesheetFileName.Insert(goodRatesheetFileName.Length - 1, "</div></body></html>");
            // DL new HTML
            RatesheetFileName = WriteToTempFile(goodRatesheetFileName);
            LogInfo("NHF Sapphire Email Bot just processed file" + RatesheetFileName);
            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
                return true;
        }
    }
}
