﻿//Kelvin Young
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;

namespace LpeUpdateBotLib.EmailBots
{
    public class NewPennEBot : AbstractEmailBot
    {
        private static Regex rslink_regex = new Regex(@"href=\'(?<rstemp>[^\']+)\'> &lt;img src=\';https://origin\.ih\.constantcontact\.com/fs012/1102555026005/img/104", RegexOptions.IgnoreCase);

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        public NewPennEBot(IDownloadInfo info, jmail.Message msg)
            : base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "NEWPENN"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match match = rslink_regex.Match(EmailBody);
            string sFileName = null;
            if (match.Success)
            {
                string temp = HttpUtility.UrlDecode(match.Groups["rstemp"].Value.ToString());
                sFileName = DownloadFile(temp);
            }
            else
                LogErrorAndSendEmailToAdmin("NewPennEBot can't find ratesheet URL. EmailBody: " + EmailBody);
            if (sFileName != null)
            {
                RatesheetFileName = sFileName;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
                return E_BotDownloadStatusT.Error;
        }


        public override bool IsValidEmailMessage(MailMask mailMask, jmail.Message msg)
        {
            if (msg.Subject.IndexOf("Rate Sheet") >= 0)
                return true;
            else
            {
                string s = string.Format("Could not match Subject when processing NewPennEBot.  Looking for the text 'Rate Sheet' anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }
        }
    }
}