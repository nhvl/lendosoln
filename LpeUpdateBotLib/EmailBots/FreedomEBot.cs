﻿// Author Budi Sulayman
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class FreedomEBot : AbstractEmailBot
    {
        //private static Regex rslink_regex = new Regex(@"<A title=""Daily Rates Available Here"" href=""(?<rslink>[^""]+)""");
        private static Regex rslink_regex = new Regex(@"A href=""(?<rslink>[^""]+)"">Daily Wholesale Rates Excel Version");
        private static Regex rslink2_regex = new Regex(@"<A title=""Pacific Region Wholesale Rates"" href=""(?<rslink>[^""]+)""(\s\S)*\(Fees In\)");

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        public FreedomEBot(IDownloadInfo info, Pop3Message msg): base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "FREEDOMEMAIL"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            if (dlinfo.Description == "FREEDOM_PHX")
            {
                Match match = rslink2_regex.Match(EmailBody);
                string rsFile = null;
                if (match.Success)
                    rsFile = DownloadFile(match.Groups["rslink"].Value);
                else
                {
                    LogErrorAndSendEmailToAdmin("Freedom Email Bot can't find ratesheet url. Content: " + EmailBody);
                }

                if (null != rsFile)
                {
                    RatesheetFileName = rsFile;
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                {
                    return E_BotDownloadStatusT.Error;
                }
            }
            else
            {
                Match match = rslink_regex.Match(EmailBody);
                string rsFile = null;
                if (match.Success)
                    rsFile = DownloadFile(match.Groups["rslink"].Value);
                else
                {
                    LogErrorAndSendEmailToAdmin("Freedom Email Bot can't find ratesheet url. Content: " + EmailBody);
                }

                if (null != rsFile)
                {
                    RatesheetFileName = rsFile;
                    return E_BotDownloadStatusT.SuccessfulWithRatesheet;
                }
                else
                {
                    return E_BotDownloadStatusT.Error;
                }
            }
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            if (msg.Subject.Contains(mailMask.Subject))
            {
                return true;
            }
            else
            {
                string s = string.Format("Could not match Subject when processing FREEDOM bot.  Looking for the text from download list ({2}) anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject, mailMask.Subject);
                string subject = string.Format("Error processing email: {0}, {1}", msg.From);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }

        }
    }
}