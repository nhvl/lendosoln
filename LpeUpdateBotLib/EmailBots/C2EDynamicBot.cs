﻿// Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;
using System.Collections.Generic;

namespace LpeUpdateBotLib.EmailBots
{
    public class C2EDynamicBot : AbstractEmailBot
    {
        private Pop3Attachment m_ATM;
        private string desc;

        public C2EDynamicBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "C2E_DYNAMIC"; }
        }
        public override string ConversionMethod
        {
            get { return "C2E_DYNAMIC_BYCOPY"; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            if (null == m_ATM)
            {
                return E_BotDownloadStatusT.Error;
            }
            string sRawFile = FileCfg.TEMP_FILE;
            m_ATM.SaveToFile(sRawFile);

            /* this section adds an extra row in between each rate stack so that we can use DynamicListBottom */

            String path = sRawFile;
            List<String> lines = new List<String>();

            // figure out which column to compare
            int comp;

            if (desc.EndsWith("0"))
                comp = 0;
            else if (desc.EndsWith("1"))
                comp = 1;
            else if (desc.EndsWith("2"))
                comp = 2;
            else if (desc.EndsWith("3"))
                comp = 3;
            else if (desc.EndsWith("4"))
                comp = 4;
            else if (desc.EndsWith("5"))
                comp = 5;
            else if (desc.EndsWith("6"))
                comp = 6;
            else if (desc.EndsWith("7"))
                comp = 7;
            else if (desc.EndsWith("8"))
                comp = 8;
            else if (desc.EndsWith("9"))
                comp = 9;
            else
                comp = 0;

            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    String line;
                    String[] temp_split = new String[10];

                    // loop thru all lines in the rate sheet file we downloaded
                    while ((line = reader.ReadLine()) != null)
                    {
                        // if this line contains a comma
                        if (line.Contains(","))
                        {
                            // split the line into an array of strings
                            String[] split = line.Split(',');

                            // if the 2nd value of the current line does not match the 2nd value of the previous line
                            if (!(split[comp].Equals(temp_split[comp])))
                            {
                                // add a blank row
                                line = "";
                                lines.Add(line);
                            }

                            // set the previous line equal to the current line for next iteration
                            temp_split = split;
                            // convert the string array into one csv line
                            line = String.Join(",", temp_split);
                        }
                        // add the line that we have read in
                        lines.Add(line);
                    }
                }

                // overwrite the rate sheet file so that it includes that newly added blank rows
                using (StreamWriter writer = new StreamWriter(path, false))
                {
                    foreach (String line in lines)
                        writer.WriteLine(line);
                }
            }

            RatesheetFileName = sRawFile;
            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            desc = mailMask.Description; // get the email dl list entry's description so we can use it within the DownloadFromWebsite method
            var attachment = mailMask.Match(msg);
            if (attachment == null)
            {
                return false;
            }

            if (attachment.Equals(MailMask.EMPTY_ATTACHMENT))
            {
                string s = string.Format("EMPTY_ATTACHMENT ERROR WHEN HANDLING C2E_DYNAMIC EMAIL BOT \nEMAIL: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);

                LogErrorAndSendEmailToAdmin(subject, s);

                return false;
            }
            m_ATM = attachment;
            return true;
        }
    }
}