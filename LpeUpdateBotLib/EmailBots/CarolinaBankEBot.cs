﻿// Author Budi Sulayman
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class CarolinaBankEBot : AbstractEmailBot
    {
        private static Regex messageURL_regex = new Regex(@"href=""(?<rsURL>[^""]+)""[^>]>\s*CLICK HERE TO VIEW RATES", RegexOptions.IgnoreCase);
        private static Regex messageURL_regex2 = new Regex(@"href=""(?<rsURL>[^""]+)""\s+target=""_blank""><span\s+style=""[^""]+"">CLICK HERE TO VIEW RATES", RegexOptions.IgnoreCase);

        public CarolinaBankEBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "CAROLINABANK"; }
        }
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match messageURL_match = messageURL_regex.Match(EmailBody);
            Match messageURL_match2 = messageURL_regex2.Match(EmailBody);
            string ratesheetURL = null;
            if ((messageURL_match.Success) || (messageURL_match2.Success))
            {
                string temp = null;
                if (messageURL_match.Success)
                    temp = messageURL_match.Groups["rsURL"].Value;
                else
                    temp = messageURL_match2.Groups["rsURL"].Value;

                int i = temp.IndexOf("http");
                temp = temp.Substring(i);
                ratesheetURL = HttpUtility.UrlDecode(temp);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Carolina Bank EBot failed to find URL link. Content: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            if (msg.Subject.Contains("Carolina Bank Rates"))
            {
                return true;
            }
            else
            {
                string s = string.Format("Could not match Subject when processing Caroline Bank EBot. Looking for the text from email download list ({2}) anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject, mailMask.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }
        }
    }
}