﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class ACHTrustEBot : AbstractEmailBot
    {
        private static Regex messageURL_regex = new Regex(@"href=""(?<rsURL>[^""]+)"" style=""color: rgb[^;]+; font-size: 16px; font-weight: bold"">ACH Trust XLS</a>", RegexOptions.IgnoreCase);
        
        public ACHTrustEBot(IDownloadInfo info, Pop3Message message) : base (info, message)
        {
        }

        public override string BotName
        {
            get { return "ACHTRUSTEMAIL"; }
        }

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match messageURL_match = messageURL_regex.Match(EmailBody);

            if (messageURL_match.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = messageURL_match.Groups["rsURL"].Value;

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("ACH Email Bot failed to find URL link. Content: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            if (msg.Subject.Contains("ACH Trust Rate Sheet"))
            {
                return true;
            }
            else
            {
                string s = string.Format("Could not match Subject when processing ACH Email Bot. Looking for the text from email download list ({2}) anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject, mailMask.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }
        }

    }
}
