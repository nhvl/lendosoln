﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Net.Mail;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class CMGEmailBot : AbstractEmailBot
    {
        public CMGEmailBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            if (msg.Subject.StartsWith("CMG Correspondent Ratesheet for") || msg.Subject.StartsWith("CMG Correspondent Jumbo Ratesheet for"))
            {
                return true;
            }
            return false;
        }

        public override string BotName
        {
            get { return "CMG"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            StringBuilder debugInfo = new StringBuilder();
            try
            {
                debugInfo.AppendLine("Subject=[" + this.Message.Subject + "]");
                // 3/3/2014 dd - OPM 150614 - Delay the download if the email is too new.
                string[] parts = this.Message.Subject.Split('|');

                long ticks = 0;

                long.TryParse(parts[1], out ticks);

                DateTime dt = new DateTime(ticks);
                DateTime _8_46 = DateTime.Today + new TimeSpan(8, 46, 0);
                debugInfo.AppendLine("Email Generated Time=" + dt.ToString());
                if (dt.AddMinutes(7) > DateTime.Now)
                {
                    // Too new resend email.
                    debugInfo.AppendLine("Resend Email");

                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(this.Message.From);
                    debugInfo.AppendLine("From=[" + this.Message.From + "]");
                    foreach (var recipient in Message.Recipients)
                    {
                        debugInfo.AppendLine("Recipients=[" + recipient.Email + "]");
                        msg.To.Add(new MailAddress(recipient.Email));
                    }
                    msg.Subject = Message.Subject;
                    msg.Body = Message.Body;

                    SmtpClient smtpClient = new SmtpClient("smtp.meridianlink.com");
                    smtpClient.Send(msg);

                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                }
                else if (this.Message.Subject.StartsWith("CMG Correspondent Jumbo Ratesheet") && DateTime.Now < _8_46)
                {
                    debugInfo.AppendLine("Jumbo Ratesheet is not available until 8:45 AM. Resend Email");

                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(this.Message.From);
                    debugInfo.AppendLine("From=[" + this.Message.From + "]");
                    foreach (var recipient in Message.Recipients)
                    {
                        debugInfo.AppendLine("Recipients=[" + recipient.Email + "]");
                        msg.To.Add(new MailAddress(recipient.Email));
                    }
                    msg.Subject = Message.Subject;
                    msg.Body = Message.Body;

                    SmtpClient smtpClient = new SmtpClient("smtp.meridianlink.com");
                    smtpClient.Send(msg);

                    return E_BotDownloadStatusT.SuccessfulWithNoRatesheet;
                }
                debugInfo.AppendLine("Download File.");
                // 1/16/2014 dd - Correspondent Ratesheet in Excel
                string url = "https://www.cmgclear.com/RateSheets/API/RateSheets/?type=Excel&office=Corr&region=&disposition=inline";
                RatesheetFileName = DownloadFile(url);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            catch (Exception exc)
            {
                debugInfo.AppendLine("Exception: " + exc);
                throw;
            }
            finally
            {
                LogInfo(debugInfo.ToString());
            }
        }
    }
}
