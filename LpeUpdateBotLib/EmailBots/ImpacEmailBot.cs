﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;


namespace LpeUpdateBotLib.EmailBots
{
    class ImpacEmailBot : AbstractEmailBot
    {
        //private static Regex messageURL_regex = new Regex(@"href=""(?<rsURL>[^""]+)""\s+target=""_blank""\s+rel=""link""><(?<imageGarbage>[^>]+)>\s+<strong>DOWNLOAD RATESHEETS</strong>\s+\(xls\)", RegexOptions.IgnoreCase);
        private static Regex messageURL_regex = new Regex(@"href=""(?<rsURL>[^""]+)""\s+rel=""link""(?<fontGarbage>[^>]+)><(?<imageGarbage>[^>]+)>\s+<strong>DOWNLOAD RATESHEETS</strong>\s+\(xls\)", RegexOptions.IgnoreCase);

        public ImpacEmailBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "ImpacEmail"; }
        }
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match messageURL_match = messageURL_regex.Match(EmailBody);
            
            if (messageURL_match.Success)
            {
                string ratesheetURL = null;
                ratesheetURL = HttpUtility.HtmlDecode(messageURL_match.Groups["rsURL"].Value);

                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Impac Email Bot failed to find URL link. Content: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            // 8/28/17 - removed if/else statement because reprice emails do not contain "Impac" in the title anymore [BB]
            //if (msg.Subject.Contains("Impac"))
            //{
                return true;
            /*}
            else
            {
                string s = string.Format("Could not match Subject when processing Impac Email Bot. Looking for the text from email download list ({2}) anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject, mailMask.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }*/
        }
    }
}
