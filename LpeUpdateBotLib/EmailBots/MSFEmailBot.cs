// Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class MSFEmailBot : AbstractEmailBot
    {
        private string ratesheetURL = "http://www.msofco.info/Rates/Correspondent.xlsx";
        // regex to find ratesheet URL in email HTML
       // private static Regex messageURL_regex = new Regex(@"alt=""(?<rsURL>[^""]+)""\starget=""_blank""><img\sheight=""(0-9)+""\svspace=""(0-9)+""\sborder=""(0-9)+""\sname=""ACCOUNT.IMAGE.576""", RegexOptions.IgnoreCase);
        //private static Regex messageURL_regex = new Regex(@"<href=""(?<rsURL>[^""]+)""\salt=""http://www\.msofco\.info/Rates/Correspondent\.xlsx""", RegexOptions.IgnoreCase);

        public MSFEmailBot(IDownloadInfo info, Pop3Message message) : base (info, message)
        {
        }

        public override string BotName
        {
            get { return "MSFEMAIL"; }
        }

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // see if our regex finds a match in the email HTML
           /* Match messageURL_match = messageURL_regex.Match(EmailBody);

            string ratesheetURL = null;

            if (messageURL_match.Success)
            {
                // convert catpured value to string
                ratesheetURL = messageURL_match.Groups["rsURL"].Value;*/
                // DL ratesheet using link from email
                RatesheetFileName = DownloadFile(ratesheetURL);
            if (RatesheetFileName != null)
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            else
            {
                LogErrorAndSendEmailToAdmin("Mortgage Solutions Financial Email bot failed to find URL link. URL: " + ratesheetURL + "\nEmail Body: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
            /*}
            else
            {
                LogErrorAndSendEmailToAdmin("Mortgage Solutions Financial Email bot failed to find URL link. URL: " + ratesheetURL + "\nEmail Body: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }*/
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            return true;
        }

    }
}
