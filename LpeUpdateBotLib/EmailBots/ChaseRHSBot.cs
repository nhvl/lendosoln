/// Author: Brian Beery
/// Date: 8-1-08
/// Purpose: Process a Chase RHS rate sheet attachment

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;
namespace LpeUpdateBotLib.EmailBots
{
    public class ChaseRHSBot : AbstractEmailBot
    {
		private Pop3Attachment m_ATM ;

        public ChaseRHSBot(IDownloadInfo info, Pop3Message message) : base (info, message)
        {
        }
        public override string BotName 
        {
            get { return "CHASERHS"; }
        }
        public override string ConversionMethod 
        {
            get { return "PDF2EXCEL"; }
        }
		// This will simply save and return the attachment, somewhat of a rigged solution based on existing structure
		// Assumes that the attachment is a valid pdf file
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo) 
        {
            if (null == m_ATM)
            {
                return E_BotDownloadStatusT.Error;
            }
			string sRawFile = FileCfg.TEMP_FILE ;
			m_ATM.SaveToFile(sRawFile) ;
            RatesheetFileName = sRawFile;
            return E_BotDownloadStatusT.SuccessfulWithRatesheet;

        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            var attachment = mailMask.Match(msg);
            if (attachment == null)
            {
                return false;
            }

            if (attachment.Equals(MailMask.EMPTY_ATTACHMENT))
            {
                string s = string.Format("EMPTY_ATTACHMENT ERROR WHEN HANDLING CHASERHS BOT \nEMAIL: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);

                LogErrorAndSendEmailToAdmin(subject, s);

                return false;
            }
            m_ATM = attachment; // 12/29/2010 dd - This will be use in Download method.

            return true;
        }
    }

}
