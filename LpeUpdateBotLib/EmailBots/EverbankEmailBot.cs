// Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class EverbankEmailBot : AbstractEmailBot
    {
        // regex to find ratesheet URL in email HTML
        private static Regex messageURL_regex1 = new Regex(@"href=""(?<rsURL1>[^""]+)""\s+target=""blank""(?<fontGarbage1>[^>]+)><(?<imageGarbage1>[^>]+)>&nbsp;&nbsp;Preferred\s*Access\sCorrespondent\sRate\sSheet", RegexOptions.IgnoreCase);
        private static Regex messageURL_regex2 = new Regex(@"href=""(?<rsURL2>[^""]+)""\s+target=""blank""><(?<fontGarbage2>[^>]+)><(?<imageGarbage2>[^>]+)>&nbsp;&nbsp;Preferred\s*Access\sCorrespondent\sRate\sSheet", RegexOptions.IgnoreCase);

        public EverbankEmailBot(IDownloadInfo info, Pop3Message message) : base (info, message)
        {
        }

        public override string BotName
        {
            get { return "EVERBANKEMAIL"; }
        }

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string ratesheetURL = null;
            // see if either of our regexes find a match in the email HTML
            Match messageURL_match1 = messageURL_regex1.Match(EmailBody);
            Match messageURL_match2 = messageURL_regex2.Match(EmailBody);

            if (messageURL_match1.Success)
            {
                
                // convert catpured value to string
                ratesheetURL = messageURL_match1.Groups["rsURL1"].Value;
                // DL ratesheet using link from email
                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            if (messageURL_match2.Success)
            {

                // convert catpured value to string
                ratesheetURL = messageURL_match2.Groups["rsURL2"].Value;
                // DL ratesheet using link from email
                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("Everbank Email bot failed to find URL link. URL: " + ratesheetURL + "\nEmail Body: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            return true;
        }

    }
}
