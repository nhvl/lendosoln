﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class NexBankEBot : AbstractEmailBot
    {
        private static Regex messageURL_regex = new Regex(@"href=""(?<rsURL>[^""]+)""[^>]>\s*Excel rate sheet", RegexOptions.IgnoreCase);
        
        public NexBankEBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }
        public override string BotName
        {
            get { return "NEXBANK"; }
        }
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            Match m = messageURL_regex.Match(EmailBody);
            if (m.Success)
            {
                string ratesheetURL = m.Groups["rsURL"].Value;
                RatesheetFileName = DownloadFile(ratesheetURL);
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }
            else
            {
                LogErrorAndSendEmailToAdmin("NexBankEBot failed to find URL link. Content: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            if (msg.Subject.Contains("Delegated Correspondent Rate Sheet"))
            {
                return true;
            }
            string s = string.Format("Could not match Subject when processing NexBankEBot. Looking for the text from email download list ({2}) anywhere in the Subject.  \nData from actual e-mail : FROM: {0}\nSUBJECT: {1}", msg.From, msg.Subject, mailMask.Subject);
            string subject = string.Format("Error processing email: {0}", msg.From);
            LogErrorAndSendEmailToAdmin(subject, s);
            return false;
        }
    }
}
