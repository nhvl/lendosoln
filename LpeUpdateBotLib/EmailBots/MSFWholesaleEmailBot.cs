// Author: Britton Barmeyer

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using System.Web;
using LendingQBPop3Mail;

namespace LpeUpdateBotLib.EmailBots
{
    public class MSFWholesaleEmailBot : AbstractEmailBot
    {
        // URL we go to in order to download the RS
        private string ratesheetURL = "http://www.msofco.info/Rates/Wholesale.xlsx";
        
        public MSFWholesaleEmailBot(IDownloadInfo info, Pop3Message message) : base (info, message)
        {
        }

        public override string BotName
        {
            get { return "MSFWSEMAIL"; }
        }

        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            // download the RS using URL above
            RatesheetFileName = DownloadFile(ratesheetURL);

            // if we actually downloaded something, return success. if not, log an error.
            if (RatesheetFileName != null)
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            else
            {
                LogErrorAndSendEmailToAdmin("MSF Wholesale Email bot failed to find URL link. URL: " + ratesheetURL + "\nEmail Body: " + EmailBody);
                return E_BotDownloadStatusT.Error;
            }
            
        }
        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            return true;
        }

    }
}
