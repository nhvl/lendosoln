﻿//Author: Budi Sulayman

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;
namespace LpeUpdateBotLib.EmailBots
{
    public class HTMLtoExcelEBot : AbstractEmailBot
    {
        private string EmailBody
        {
            get { return Message.HTMLBody; }
        }

        public HTMLtoExcelEBot(IDownloadInfo info, Pop3Message message)
            : base(info, message)
        {
        }
        public override string BotName
        {
            get { return "HTMLTOEXCEL"; }
        }
        public override string ConversionMethod
        {
            get { return "HTML2EXCEL"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            RatesheetFileName = WriteToTempFile(EmailBody.ToString());
            LogInfo("HTMLtoExcelEBot just processed file" + RatesheetFileName);
            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
                return true;
        }
    }
}
