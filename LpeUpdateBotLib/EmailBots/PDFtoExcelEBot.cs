﻿//Author: Budi Sulayman/Yin Seo

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using LendingQBPop3Mail;
namespace LpeUpdateBotLib.EmailBots
{
    public class PDFtoExcelEBot : AbstractEmailBot
    {
        private Pop3Attachment m_ATM;

        public PDFtoExcelEBot(IDownloadInfo info, Pop3Message message)
            : base(info, message)
        {
        }
        public override string BotName
        {
            get { return "PDFTOEXCEL"; }
        }
        public override string ConversionMethod
        {
            get { return "PDF2EXCEL"; }
        }
       
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            if (null == m_ATM)
            {
                return E_BotDownloadStatusT.Error;
            }
            string sRawFile = FileCfg.TEMP_FILE;
            m_ATM.SaveToFile(sRawFile);
            RatesheetFileName = sRawFile;
            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            var attachment = mailMask.Match(msg);
            if (attachment == null)
            {
                return false;
            }

            if (attachment.Equals(MailMask.EMPTY_ATTACHMENT))
            {
                string s = string.Format("EMPTY_ATTACHMENT ERROR WHEN HANDLING PDFTOEXCEL BOT \nEMAIL: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);

                LogErrorAndSendEmailToAdmin(subject, s);

                return false;
            }
            m_ATM = attachment; 
            return true;
        }
    }
}
