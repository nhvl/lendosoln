﻿//Author: Budi Sulayman
namespace LpeUpdateBotLib.EmailBots
{
    using System;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    //using LendersOffice.Drivers.Gateways;
    using LendingQBPop3Mail;
    using LpeUpdateBotLib.Common;

    public class FSCMBankEBot : AbstractEmailBot
    {
        private Pop3Attachment m_ATM;

        public FSCMBankEBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }

        public override string BotName
        {
            get { return "FSCMBANK"; }
        }

        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            if (null == m_ATM)
            {
                return E_BotDownloadStatusT.Error;
            }

            string uploadRatesheet = Config.FILES_PATH + @"UploadRatesheet\FSMCBankRS.txt";
            string newUploadRatesheet = Config.FILES_PATH + @"UploadRatesheet\FSMCBankRS.pdf";
            DirectoryInfo di = new DirectoryInfo(Config.FILES_PATH + @"UploadRatesheet");
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            m_ATM.SaveToFile(uploadRatesheet);
            File.Move(uploadRatesheet, newUploadRatesheet);
/*
            FileOperationHelper.Move(uploadRatesheet, newUploadRatesheet);
*/
            
            return E_BotDownloadStatusT.SuccessfulWithRatesheet;
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            var attachment = mailMask.Match(msg);
            if (attachment == null)
            {
                return false;
            }

            if (attachment.Equals(MailMask.EMPTY_ATTACHMENT))
            {
                string s = string.Format("EMPTY_ATTACHMENT ERROR WHEN HANDLING FSCMBank BOT \nEMAIL: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                string subject = string.Format("Error processing email: {0}", msg.From);

                LogErrorAndSendEmailToAdmin(subject, s);

                return false;
            }
            m_ATM = attachment;
            return true;
        }
    }
}