﻿using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using LpeUpdateBotLib.Common;
using Excel = Microsoft.Office.Interop.Excel;

namespace LpeUpdateBotLib.EmailBots
{
    public class PHHEBot : AbstractEmailBot
    {
        private jmail.Attachment m_ATM;

        public PHHEBot(IDownloadInfo info, string emailBody, jmail.Attachment atm)
            : base(info, emailBody)
        {
            m_ATM = atm;
        }
        public override string BotName
        {
            get { return "PHHRS"; }
        }

        //This will simply rename the worksheet to Aurora's worksheet since we built the product based on this ratesheet first
        protected override E_BotDownloadStatusT DownloadFromWebsite(IDownloadInfo dlinfo)
        {
            string sRawFile = FileCfg.TEMP_FILE;
            m_ATM.SaveToFile(sRawFile);
            LogInfo(string.Format("PHHEBot just saved the ratesheet attachment at: {0}", sRawFile));

            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;

            try
            {
                LogInfo("PHHEBot is going to open the ratesheet");
                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Open(sRawFile, 2, false, 4, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, ";", false, true, 1, false, true, Microsoft.Office.Interop.Excel.XlCorruptLoad.xlNormalLoad);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Name = "RS 7607-Aurora";
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
                xlWorkSheet.Name = "RS 7657-Aurora";
                LogInfo("PHHEBot just renamed the workbook and about to save the changes");

                xlWorkBook.Save();
                LogInfo("PHHEBot just saved the changes and about to close the workbook");
                xlWorkBook.Close(true, sRawFile, false);
                LogInfo("PHHEBot just closed the workbook");
                xlApp.Quit();

                RatesheetFileName = sRawFile;
                return E_BotDownloadStatusT.SuccessfulWithRatesheet;
            }

            catch (InvalidDataException ie)
            {
                LogInfo(string.Format("PHHEBot failed to open the ratesheet for {0}", m_ATM.Name));
                LogErrorAndSendEmailToAdmin("PHH Email Bot failed to modify the worksheet for: " + m_ATM.Name);
                return E_BotDownloadStatusT.Error;
            }
        }
    }
}