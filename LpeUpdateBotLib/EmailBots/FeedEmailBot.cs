﻿namespace LpeUpdateBotLib.EmailBots
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    //using LendersOffice.Drivers.Gateways;
    using LendingQBPop3Mail;
    using LpeUpdateBotLib.Common;

    public class FeedEmailBot : AbstractEmailBot
    {
        private Pop3Attachment m_attachment;
        protected IEmailDownloadInfo m_EmailInfo;
        private DateTime m_startTime;
        
        public FeedEmailBot(IDownloadInfo info, Pop3Message msg)
            : base(info, msg)
        {
        }

        protected virtual string DummyOutputFileName 
        {
            get
            {
                return "PML0217_CUSTOM_PML0217_CUSTOM_MAP_m-DATA_Output.csv"; 
                //return "YINTEST_RS_YINTEST_RS_MAP_m-test_Output.csv";
            }
        }

        public override bool IsValidEmailMessage(MailMask mailMask, Pop3Message msg)
        {
            m_startTime = DateTime.Now;
            m_EmailInfo = mailMask.EmaiLDownloadInfo;
            m_attachment = mailMask.GetAttachmentOnlyIfAllAttachmentFieldsMatch(msg);

            if (mailMask.DoNonAttachmentFieldsMatch(msg) && m_attachment != null && m_attachment != MailMask.EMPTY_ATTACHMENT)
            {
                LogInfo("Email Match for feed bot");
                return true;
            }
            else
            {
                string s = string.Format("There was no attachment. BodyText: {0}", msg.BodyText);
                string subject = string.Format("Error processing email: {0}, {1}", msg.From, msg.Subject);
                LogErrorAndSendEmailToAdmin(subject, s);
                return false;
            }
        }

        public override string BotName
        {
            get { return "FEEDBot"; }
        }

        /// <summary>
        /// Used by the feed bot to let it update the rates manually and skip the rest of lpeupdate.
        /// </summary>
        /// <returns></returns>
        public override bool PreProcessEmailAndQuit()
        {
            LogInfo("PreProcessEmailAndQuit");

            m_EmailInfo.ExpireCurrentRateSheet(); //we want to expire the current ratesheet
            try
            {
                LogInfo(String.Format("Begining processing of attachment as xml for {0}", m_EmailInfo.MainTargetFileName));
                ProcessAttachment();
                LogInfo(String.Format("Begining saving of rates as xml for {0}", m_EmailInfo.MainTargetFileName));
                m_EmailInfo.PersistRateData(m_startTime, DateTime.Now, DummyOutputFileName );
            }

            catch (Exception e)
            {
                LogInfo(String.Format("Ran into an error processing attachment for {0}", m_EmailInfo.MainTargetFileName));
                LogErrorAndSendEmailToAdmin(e);
            }

            return true;

        }



        protected virtual void ProcessAttachment()
        {
            string tempFile = FileCfg.TEMP_FILE;
            m_attachment.SaveToFile(tempFile);
            string lines = System.IO.File.ReadAllText(tempFile);
/*
            string lines = TextFileHelper.ReadFile(tempFile);
*/
            LogInfo("xmlDoc for feedbot : " + lines);
            XmlDocument doc = new XmlDocument();
            doc.Load(tempFile);
            System.IO.File.Delete(tempFile);
/*
            FileOperationHelper.Delete(tempFile);
*/
            foreach (XmlElement priceGroup in doc.SelectNodes("//BasePricing/PriceGroup"))
            {
                Guid priceGroupId;
                try
                {
                    priceGroupId = new Guid(priceGroup.GetAttribute("id").Trim());
                }
                catch (FormatException)
                {
                    LogErrorAndSendEmailToAdmin("Could not parse pricegroup id " + priceGroup.GetAttribute("id"));
                    continue;
                }
                foreach (XmlElement program in priceGroup.SelectNodes("LoanProgram"))
                {
                    Guid loanProgramId;
                    try
                    {
                        loanProgramId = new Guid(program.GetAttribute("id").Trim());
                    }
                    catch (FormatException)
                    {
                        continue;
                    }
                    int minLockPeriod = 0;
                    foreach (XmlElement lockPeriodNode in program.SelectNodes("LockPeriod"))
                    {
                        string days = lockPeriodNode.GetAttribute("Days");
                        m_EmailInfo.RegisterLockPeriod(priceGroupId, loanProgramId, minLockPeriod, int.Parse(days));
                        List<XmlElement> rateOptionNodes = new List<XmlElement>();
                        foreach (XmlElement rate in lockPeriodNode.SelectNodes("RateOption"))
                        {
                            rateOptionNodes.Add(rate);
                        }

                        foreach (XmlElement rate in rateOptionNodes.OrderByDescending(p=>decimal.Parse(p.GetAttribute("Rate"))))
                        {
                            m_EmailInfo.RegisterRate(priceGroupId, loanProgramId, rate.GetAttribute("Rate"), rate.GetAttribute("Point"), rate.GetAttribute("Margin"), rate.GetAttribute("QualRate"));
                        }

                        minLockPeriod = int.Parse(days) + 1;
                    }
                }
            }
        }
    }
}
