﻿namespace DataAccess.FannieMae
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;

    [TestFixture]
    [Category(LendingQB.Test.Developers.CategoryConstants.UnitTest)]
    public class FannieMaeThirdPartyProviderListTest
    {
        [Test]
        public static void CctorDefault_GetProvidersHasNoItems()
        {
            int count = 0;
            var providerList = new FannieMaeThirdPartyProviderList();

            Assert.AreEqual(true, providerList.IsDirty, nameof(FannieMaeThirdPartyProviderList.IsDirty));
            Assert.AreEqual(count, providerList.GetProviders().Count, "GetProviders().Count");
        }

        [Test]
        public static void CctorList_SingleProvider_GetProvidersHasOneItem()
        {
            var list = new List<FannieMaeThirdPartyProvider>()
            {
                CreateThirdPartyProvider(),
            };
            int count = list.Count;

            var providerList = new FannieMaeThirdPartyProviderList(list);

            Assert.AreEqual(true, providerList.IsDirty, nameof(FannieMaeThirdPartyProviderList.IsDirty));
            Assert.AreEqual(count, providerList.GetProviders().Count, "GetProviders().Count");
        }

        [Test]
        public static void CctorList_TwoEqualProviders_GetProvidersHasTwoItems()
        {
            var list = new List<FannieMaeThirdPartyProvider>()
            {
                CreateThirdPartyProvider(),
                CreateThirdPartyProvider(),
            };
            int count = list.Count;
            Assert.IsTrue(list[0].Equals(list[1]), "Two providers are not equal");

            var providerList = new FannieMaeThirdPartyProviderList(list);

            Assert.AreEqual(true, providerList.IsDirty, nameof(FannieMaeThirdPartyProviderList.IsDirty));
            Assert.AreEqual(count, providerList.GetProviders().Count, "GetProviders().Count");
        }

        [Test]
        public static void AddProviders_TwoEqualProviders_GetProvidersHasOneItem()
        {
            var providerList = new FannieMaeThirdPartyProviderList();
            var list = new List<FannieMaeThirdPartyProvider>()
            {
                CreateThirdPartyProvider(),
                CreateThirdPartyProvider(),
            };
            int count = list.Count;
            Assert.IsTrue(list[0].Equals(list[1]), "Two providers are not equal");

            providerList.AddProviders(list);

            Assert.AreEqual(true, providerList.IsDirty, nameof(FannieMaeThirdPartyProviderList.IsDirty));
            Assert.AreEqual(1, providerList.GetProviders().Count, "GetProviders().Count");
        }

        [Test]
        public static void AddProviders_AddEqualProvidersToExistingProviders_GetProvidersDoesNotAddItems()
        {
            var startingProviders = new[] { CreateThirdPartyProvider() };
            int count = startingProviders.Length;
            var providerList = new FannieMaeThirdPartyProviderList(startingProviders);
            var list = new List<FannieMaeThirdPartyProvider>()
            {
                CreateThirdPartyProvider(),
                CreateThirdPartyProvider(),
            };
            Assert.IsTrue(startingProviders[0].Equals(list[0]), "Starting provider does not equal item to add");
            Assert.IsTrue(list[0].Equals(list[1]), "Two providers being added are not equal");

            providerList.AddProviders(list);

            Assert.AreEqual(count, providerList.GetProviders().Count, "GetProviders().Count");
        }

        [Test]
        public static void AddProviders_AddProviderWithSameVendorAndFirst9DigitsAsExistingProvider_OldProviderIsRemoved()
        {
            const string existingReferenceNumber = "123456789:My-123-Other-Value";
            const string newReferenceNumber = "123-45-6789%%Another";
            var startingProviders = new[] { CreateThirdPartyProvider(referenceNumber: existingReferenceNumber) };
            var providerList = new FannieMaeThirdPartyProviderList(startingProviders);
            var providersToAdd = new[]
            {
                CreateThirdPartyProvider(referenceNumber: newReferenceNumber),
            };
            Assert.AreEqual(startingProviders[0].Name, providersToAdd[0].Name, "Providers at start and added should have the same name");
            Assert.IsFalse(startingProviders[0].Equals(providersToAdd[0]), "Starting provider should not equal item to add");

            providerList.AddProviders(providersToAdd);

            var resultingProviders = providerList.GetProviders();
            Assert.AreEqual(1, resultingProviders.Count, "GetProviders().Count");
            Assert.AreEqual(newReferenceNumber, resultingProviders[0].ReferenceNumber, "Reference number should be updated");
            Assert.IsTrue(resultingProviders[0].Equals(providersToAdd[0]), "Provider after is not equal to provider before");
        }

        [Test]
        public static void AddProviders_AddProviderWithSameVendorAndDifferentFirst9DigitsAsExistingProvider_OldProviderIsPreserved()
        {
            const string existingReferenceNumber = "123456789:My-123-Other-Value";
            const string newReferenceNumber = "321-45-6789%%Another";
            var startingProviders = new[] { CreateThirdPartyProvider(referenceNumber: existingReferenceNumber) };
            var providerList = new FannieMaeThirdPartyProviderList(startingProviders);
            var providersToAdd = new[]
            {
                CreateThirdPartyProvider(referenceNumber: newReferenceNumber),
            };
            Assert.AreEqual(startingProviders[0].Name, providersToAdd[0].Name, "Providers at start and added should have the same name");
            Assert.IsFalse(startingProviders[0].Equals(providersToAdd[0]), "Starting provider should not equal item to add");

            providerList.AddProviders(providersToAdd);

            var resultingProviders = providerList.GetProviders();
            Assert.AreEqual(2, resultingProviders.Count, "GetProviders().Count");
            Assert.AreEqual(existingReferenceNumber, resultingProviders[0].ReferenceNumber, "Old reference number should be present");
            Assert.AreEqual(newReferenceNumber, resultingProviders[1].ReferenceNumber, "New reference number should be present");
            Assert.IsTrue(resultingProviders[0].Equals(startingProviders[0]), "Resulting providers contains old item");
            Assert.IsTrue(resultingProviders[1].Equals(providersToAdd[0]), "Resulting providers contains new item");
        }

        [Test]
        public static void AddProviders_AddProviderWithDifferentVendorAndDifferentFirst9DigitsAsExistingProvider_OldProviderIsPreserved()
        {
            const string existingReferenceNumber = "123456789:My-123-Other-Value";
            const string existingProvider = "My Test Provider";
            const string newReferenceNumber = "321-45-6789%%Another";
            const string newProvider = "My other provider";
            var startingProviders = new[] { CreateThirdPartyProvider(providerName: existingProvider, referenceNumber: existingReferenceNumber) };
            var providerList = new FannieMaeThirdPartyProviderList(startingProviders);
            var providersToAdd = new[]
            {
                CreateThirdPartyProvider(providerName: newProvider, referenceNumber: newReferenceNumber),
            };
            Assert.AreNotEqual(startingProviders[0].Name, providersToAdd[0].Name, "Providers at start and added should not have the same name");
            Assert.IsFalse(startingProviders[0].Equals(providersToAdd[0]), "Starting provider should not equal item to add");

            providerList.AddProviders(providersToAdd);

            var resultingProviders = providerList.GetProviders();
            Assert.AreEqual(2, resultingProviders.Count, "GetProviders().Count");
            Assert.AreEqual(existingProvider, resultingProviders[0].Name, "Old provider should be present");
            Assert.AreEqual(existingReferenceNumber, resultingProviders[0].ReferenceNumber, "Old reference number should be present");
            Assert.AreEqual(newProvider, resultingProviders[1].Name, "New provider should be present");
            Assert.AreEqual(newReferenceNumber, resultingProviders[1].ReferenceNumber, "New reference number should be present");
            Assert.IsTrue(resultingProviders[0].Equals(startingProviders[0]), "Resulting providers contains old item");
            Assert.IsTrue(resultingProviders[1].Equals(providersToAdd[0]), "Resulting providers contains new item");
        }

        [Test]
        public static void AddProviders_AddProviderWithDifferentVendorAndSameFirst9DigitsAsExistingProvider_OldProviderIsPreserved()
        {
            const string referenceNumber = "123456789:My-123-Other-Value";
            const string existingProvider = "My Test Provider";
            const string newProvider = "My other provider";
            var startingProviders = new[] { CreateThirdPartyProvider(providerName: existingProvider, referenceNumber: referenceNumber) };
            var providerList = new FannieMaeThirdPartyProviderList(startingProviders);
            var providersToAdd = new[]
            {
                CreateThirdPartyProvider(providerName: newProvider, referenceNumber: referenceNumber),
            };
            Assert.AreNotEqual(startingProviders[0].Name, providersToAdd[0].Name, "Providers at start and added should not have the same name");
            Assert.IsFalse(startingProviders[0].Equals(providersToAdd[0]), "Starting provider should not equal item to add");

            providerList.AddProviders(providersToAdd);

            var resultingProviders = providerList.GetProviders();
            Assert.AreEqual(2, resultingProviders.Count, "GetProviders().Count");
            Assert.AreEqual(existingProvider, resultingProviders[0].Name, "Old provider should be present");
            Assert.AreEqual(referenceNumber, resultingProviders[0].ReferenceNumber, "Old reference number should be present");
            Assert.AreEqual(newProvider, resultingProviders[1].Name, "New provider should be present");
            Assert.AreEqual(referenceNumber, resultingProviders[1].ReferenceNumber, "New reference number should be present");
            Assert.IsTrue(resultingProviders[0].Equals(startingProviders[0]), "Resulting providers contains old item");
            Assert.IsTrue(resultingProviders[1].Equals(providersToAdd[0]), "Resulting providers contains new item");
        }

        private static FannieMaeThirdPartyProvider CreateThirdPartyProvider(string providerName = "Test Provider Name", string referenceNumber = "123456789:MyRefNum")
        {
            return new FannieMaeThirdPartyProvider(
                FannieMaeThirdPartyProviderTest.CreateDuServiceProvider(providerName: providerName),
                referenceNumber);
        }
    }
}
