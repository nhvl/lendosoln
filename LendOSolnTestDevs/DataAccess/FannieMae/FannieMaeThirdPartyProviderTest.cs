﻿namespace DataAccess.FannieMae
{
    using System;
    using DataAccess.FannieMae;
    using NUnit.Framework;

    [TestFixture]
    [Category(LendingQB.Test.Developers.CategoryConstants.UnitTest)]
    public class FannieMaeThirdPartyProviderTest
    {
        [Test]
        public static void Cctor_TestProvider_PropertiesAreObvious()
        {
            var duServiceProvider = CreateDuServiceProvider();
            string providerName = duServiceProvider.ProviderName;
            Assert.AreNotEqual(providerName, duServiceProvider.DisplayName, "Expected providerName and displayName to be different, so that the test is relevant");
            string referenceNumber = "123456789:MyRef";

            FannieMaeThirdPartyProvider provider = new FannieMaeThirdPartyProvider(duServiceProvider, referenceNumber);

            Assert.AreEqual(providerName, provider.Name, nameof(FannieMaeThirdPartyProvider.Name));
            Assert.AreEqual(referenceNumber, provider.ReferenceNumber, nameof(FannieMaeThirdPartyProvider.ReferenceNumber));
        }

        [Test]
        public static void Equals_EquivalentObjects_AreEqual()
        {
            string referenceNumber = "123456789:MyRef";
            var provider1 = new FannieMaeThirdPartyProvider(CreateDuServiceProvider(), referenceNumber);
            var provider2 = new FannieMaeThirdPartyProvider(CreateDuServiceProvider(), referenceNumber);

            Assert.IsTrue(provider1.Equals(provider1), "Equals for the same instance");
            Assert.IsTrue(provider1.Equals(provider2), "Equals for different equivalent instances");
            Assert.IsTrue(provider2.Equals(provider1), "Equals for different equivalent instances (flipped)");

            Assert.IsTrue(provider1.Equals((object)provider1), "Equals(object) for the same instance");
            Assert.IsTrue(provider1.Equals((object)provider2), "Equals(object) for different equivalent instances");
            Assert.IsTrue(provider2.Equals((object)provider1), "Equals(object) for different equivalent instances (flipped)");

            Assert.AreEqual(provider1.GetHashCode(), provider2.GetHashCode(), "GetHashCode");
        }

        [Test]
        public static void Equals_DifferentReferenceNumbersSameServiceProvider_AreNotEqual()
        {
            string referenceNumber1 = "123456789:MyRef";
            string referenceNumber2 = "123456789:YourRef";
            var provider1 = new FannieMaeThirdPartyProvider(CreateDuServiceProvider(), referenceNumber1);
            var provider2 = new FannieMaeThirdPartyProvider(CreateDuServiceProvider(), referenceNumber2);

            Assert.IsFalse(provider1.Equals(provider2), "Equals for different reference numbers");
            Assert.IsFalse(provider2.Equals(provider1), "Equals for different reference numbers (flipped)");
            Assert.IsFalse(provider1.Equals((object)provider2), "Equals(object) for different reference numbers");
            Assert.IsFalse(provider2.Equals((object)provider1), "Equals(object) for different reference numbers (flipped)");

            Assert.AreNotEqual(provider1.GetHashCode(), provider2.GetHashCode(), "GetHashCode - Passing this is not a requirement of GetHashCode, but the test should pass most implementations.");
        }

        [Test]
        public static void Equals_DifferentServiceProviderSameReferenceNumbers_AreNotEqual()
        {
            string referenceNumber = "123456789:MyRef";
            var provider1 = new FannieMaeThirdPartyProvider(CreateDuServiceProvider(providerName: "Test Provider #1"), referenceNumber);
            var provider2 = new FannieMaeThirdPartyProvider(CreateDuServiceProvider(providerName: "Test Provider #2"), referenceNumber);

            Assert.IsFalse(provider1.Equals(provider2), "Equals for different service providers");
            Assert.IsFalse(provider2.Equals(provider1), "Equals for different service providers (flipped)");
            Assert.IsFalse(provider1.Equals((object)provider2), "Equals(object) for different service providers");
            Assert.IsFalse(provider2.Equals((object)provider1), "Equals(object) for different service providers (flipped)");

            Assert.AreNotEqual(provider1.GetHashCode(), provider2.GetHashCode(), "GetHashCode - Passing this is not a requirement of GetHashCode, but the test should pass most implementations.");
        }

        [Test]
        public static void Equals_NullInstance_AreNotEqual()
        {
            string referenceNumber = "123456789:MyRef";
            var provider1 = new FannieMaeThirdPartyProvider(CreateDuServiceProvider(), referenceNumber);

            Assert.IsFalse(provider1.Equals(null), "Equals for null");
            Assert.IsFalse(provider1.Equals((object)null), "Equals(object) for null");
        }

        internal static DuServiceProviders.DuServiceProvider CreateDuServiceProvider(
            string providerName = "Test Provider Name",
            string displayName = "Test Display Name",
            int id = 47)
        {
            return DuServiceProviders.DuServiceProvider.CreateTestInstance(providerName, displayName, id);
        }
    }
}
