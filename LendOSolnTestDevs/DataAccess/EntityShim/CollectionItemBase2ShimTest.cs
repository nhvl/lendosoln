﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    /// <summary>
    /// Test the base class functionality for entity shims here so it only needs to be coded once.
    /// </summary>
    public abstract class CollectionItemBase2ShimTest
    {
        protected DataSet data;
        protected global::DataAccess.IRecordCollection collection;
        protected global::DataAccess.ICollectionItemBase2 baseShim;

        protected IEnumerable FormatTargetEnumerable()
        {
            return RangeLimits.FormatTarget.Enumerable();
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void RecordIdTest()
        {
            this.CreateCollection();

            var row = this.data.Tables[0].Rows[0];
            string id = (string)row["RecordId"];
            var identifier = Guid.Parse(id);

            Assert.AreNotEqual(Guid.Empty, identifier);
            Assert.AreEqual(this.baseShim.RecordId, identifier);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void RowPosTest()
        {
            this.CreateCollection();

            for (int i=0; i<this.collection.CountRegular; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                Assert.AreEqual(i, item.RowPos);
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void GetOrderRankValueTest()
        {
            this.CreateCollection();

            var orders = new double[this.collection.CountRegular];
            for (int i = 0; i < this.collection.CountRegular; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                orders[i] = item.OrderRankValue;
            }

            for (int i = 1; i < this.collection.CountRegular; ++i)
            {
                Assert.IsTrue(orders[i] > orders[i - 1]);
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void SetOrderRankValueTest_MoveBack()
        {
            this.CreateCollection();

            int count = this.collection.CountRegular;

            // We know that the order rank values are increasing.
            var proximo = double.MinValue;
            var max = double.MinValue;
            var ids = new Guid[count];
            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                ids[i] = item.RecordId;
                proximo = max;
                max = item.OrderRankValue;
            }

            // move the first item to the next to last position
            var first = this.collection.GetRegularRecordAt(0);
            first.OrderRankValue = (max + proximo) / 2;

            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                if (i < count - 2)
                {
                    Assert.AreEqual(item.RecordId, ids[i + 1]);
                }
                else if (i == count - 2)
                {
                    Assert.AreEqual(item.RecordId, ids[0]);
                }
                else
                {
                    Assert.AreEqual(item.RecordId, ids[i]);
                }
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void SetOrderRankValueTest_MoveForward()
        {
            this.CreateCollection();

            int count = this.collection.CountRegular;

            // We know that the order rank values are increasing.
            var proximo = double.MaxValue;
            var min = double.MaxValue;
            var ids = new Guid[count];
            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                ids[i] = item.RecordId;
                if (i == 0)
                {
                    min = item.OrderRankValue;
                }
                else if (i == 1)
                {
                    proximo = item.OrderRankValue;
                }
            }

            // move the last item to the second position
            var last = this.collection.GetRegularRecordAt(count - 1);
            last.OrderRankValue = (min + proximo) / 2;

            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                if (i == 0)
                {
                    Assert.AreEqual(item.RecordId, ids[i]);
                }
                else if (i == 1)
                {
                    Assert.AreEqual(item.RecordId, ids[count - 1]);
                }
                else
                {
                    Assert.AreEqual(item.RecordId, ids[i - 1]);
                }
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void MoveDownTest()
        {
            this.CreateCollection();

            // move down means move back, i.e., the index increases
            int count = this.collection.CountRegular;
            var ids = new Guid[count];
            ICollectionItemBase2 itemToMove = null;
            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                ids[i] = item.RecordId;
                if (i == 2)
                {
                    itemToMove = item;
                }
            }

            itemToMove.MoveDown();

            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                if (i == 2)
                {
                    Assert.AreEqual(item.RecordId, ids[3]);
                }
                else if (i == 3)
                {
                    Assert.AreEqual(item.RecordId, ids[2]);
                }
                else
                {
                    Assert.AreEqual(item.RecordId, ids[i]);
                }
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void MoveDownButNoMoveTest()
        {
            this.CreateCollection();

            // moving the last item down shouldn't do anything
            int count = this.collection.CountRegular;
            var ids = new Guid[count];
            ICollectionItemBase2 itemToMove = null;
            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                ids[i] = item.RecordId;
                if (i == count - 1)
                {
                    itemToMove = item;
                }
            }

            itemToMove.MoveDown();

            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                Assert.AreEqual(item.RecordId, ids[i]);
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void MoveUpTest()
        {
            this.CreateCollection();

            // move up means move forward, i.e., the index decreases
            int count = this.collection.CountRegular;
            var ids = new Guid[count];
            ICollectionItemBase2 itemToMove = null;
            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                ids[i] = item.RecordId;
                if (i == 2)
                {
                    itemToMove = item;
                }
            }

            itemToMove.MoveUp();

            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                if (i == 1)
                {
                    Assert.AreEqual(item.RecordId, ids[2]);
                }
                else if (i == 2)
                {
                    Assert.AreEqual(item.RecordId, ids[1]);
                }
                else
                {
                    Assert.AreEqual(item.RecordId, ids[i]);
                }
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void MoveUpButNoMoveTest()
        {
            this.CreateCollection();

            // moving the first item up shouldn't do anything
            int count = this.collection.CountRegular;
            var ids = new Guid[count];
            ICollectionItemBase2 itemToMove = null;
            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                ids[i] = item.RecordId;
                if (i == 0)
                {
                    itemToMove = item;
                }
            }

            itemToMove.MoveUp();

            for (int i = 0; i < count; ++i)
            {
                var item = this.collection.GetRegularRecordAt(i);
                Assert.AreEqual(item.RecordId, ids[i]);
            }
        }

        protected abstract void CreateCollection();
    }
}
