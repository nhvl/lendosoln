﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class AppLevelFlushTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                this.loanId = creator.CreateBlankUladLoanFile();

                var loan = this.GetLoan();
                loan.AddNewApp();
            }
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [Test]
        public void Assets_Flush_SetsCorrectAsstLiqTot()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = this.GetLoan();
                loan.InitLoad();

                CAppData app;
                IAssetCollection assets;
                IAssetRegular asset;

                // Set up the first app's asset.
                app = loan.GetAppData(0);
                assets = app.aAssetCollection;

                asset = assets.AddRegularRecord();
                asset.AssetT = E_AssetRegularT.Checking;
                asset.Val = 1;

                // Set up the second app's asset.
                app = loan.GetAppData(1);
                assets = app.aAssetCollection;

                asset = assets.AddRegularRecord();
                asset.AssetT = E_AssetRegularT.Checking;
                asset.Val = 2;

                loan.GetAppData(0).aAssetCollection.Flush();
                loan.GetAppData(1).aAssetCollection.Flush();

                // Make sure that flush is only dealing with the assets that belong to the app.
                Assert.AreEqual(1, loan.GetAppData(0).aAsstLiqTot);
                Assert.AreEqual(2, loan.GetAppData(1).aAsstLiqTot);
            }
        }

        [Test]
        public void Liabilities_Flush_SetsCorrectLiaBalTot()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = this.GetLoan();
                loan.InitLoad();

                CAppData app;
                ILiaCollection liabilities;
                ILiabilityRegular liability;

                app = loan.GetAppData(0);
                liabilities = app.aLiaCollection;

                liability = liabilities.AddRegularRecord();
                liability.Bal = 1;

                app = loan.GetAppData(1);
                liabilities = app.aLiaCollection;

                liability = liabilities.AddRegularRecord();
                liability.Bal = 2;

                loan.GetAppData(0).aLiaCollection.Flush();
                loan.GetAppData(1).aLiaCollection.Flush();

                // Make sure that flush is only dealing with the liabilities that belong to the app.
                Assert.AreEqual(1, loan.GetAppData(0).aLiaBalTot);
                Assert.AreEqual(2, loan.GetAppData(1).aLiaBalTot);
            }
        }

        [Test]
        public void RealPropertyCollection_Flush_SetsCorrectReTotVal()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = this.GetLoan();
                loan.InitLoad();

                CAppData app;
                IReCollection reos;
                IRealEstateOwned reo;

                // Set up the first app's reo.
                app = loan.GetAppData(0);
                reos = app.aReCollection;

                reo = reos.AddRegularRecord();
                reo.Val = 1;

                // Set up the second app's reo.
                app = loan.GetAppData(1);
                reos = app.aReCollection;

                reo = reos.AddRegularRecord();
                reo.Val = 2;

                loan.GetAppData(0).aReCollection.Flush();
                loan.GetAppData(1).aReCollection.Flush();

                // Make sure that flush is only dealing with the properties that belong to the app.
                Assert.AreEqual(1, loan.GetAppData(0).aReTotVal);
                Assert.AreEqual(2, loan.GetAppData(1).aReTotVal);

            }
        }

        private CPageData GetLoan()
        {
            return CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanId,
                typeof(AppLevelFlushTest));
        }
    }
}
