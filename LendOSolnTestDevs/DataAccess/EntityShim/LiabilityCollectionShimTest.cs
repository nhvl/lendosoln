﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityCollectionShimTest
    {
        [Test]
        public void ExtraNormalLiabilities_SevenRegularLiabilities_ReturnsEmptySubcollection()
        {
            var testData = GetDefaultLiabilities(7);
            var collection = this.GetCollection(testData);

            var extra = collection.ExtraNormalLiabilities;

            Assert.AreEqual(0, extra.Count);
        }

        [Test]
        public void ExtraNormalLiabilities_TenRegularLiabilities_ReturnsSubcollectionWithThreeElements()
        {
            var testData = GetDefaultLiabilities(10);
            var collection = this.GetCollection(testData);

            var extra = collection.ExtraNormalLiabilities;

            Assert.AreEqual(3, extra.Count);
            for (int i = 0; i < extra.Count; ++i)
            {
                var expectedId = testData[7 + i].Key;
                var item = extra.GetRecordAt(i);
                Assert.AreEqual(expectedId, ((ILiabilityRegular)item).RecordId);
            }
        }

        [Test]
        public void ExtraNormalLiabilitiesInNew1003_SixRegularLiabilities_ReturnsEmptySubcollection()
        {
            var testData = GetDefaultLiabilities(6);
            var collection = this.GetCollection(testData);

            var extra = collection.ExtraNormalLiabilitiesInNew1003;

            Assert.AreEqual(0, extra.Count);
        }

        [Test]
        public void ExtraNormalLiabilitiesInNew1003_SevenRegularLiabilities_ReturnsSubcollectionWithOneElement()
        {
            var testData = GetDefaultLiabilities(7);
            var collection = this.GetCollection(testData);

            var extra = collection.ExtraNormalLiabilitiesInNew1003;

            Assert.AreEqual(1, extra.Count);
            var expectedId = testData[6].Key;
            Assert.AreEqual(expectedId, ((ILiabilityRegular)extra.GetRecordAt(0)).RecordId);
        }

        [Test]
        public void First7NormalLiabilities_NoRegularLiabilities_ReturnsEmptySubcollection()
        {
            var collection = this.GetCollection();

            var first7 = collection.First7NormalLiabilities;

            Assert.AreEqual(0, first7.Count);
        }

        [Test]
        public void First7NormalLiabilities_ThreeRegularLiabilities_ReturnsSubcollectionWithThreeElements()
        {
            var testData = GetDefaultLiabilities(3);
            var collection = this.GetCollection(testData);

            var first7 = collection.First7NormalLiabilities;

            Assert.AreEqual(3, first7.Count);
            for (int i = 0; i < first7.Count; ++i)
            {
                var expectedId = testData[i].Key;
                var item = first7.GetRecordAt(i);
                Assert.AreEqual(expectedId, ((ILiabilityRegular)item).RecordId);
            }
        }

        [Test]
        public void First7NormalLiabilities_SevenRegularLiabilities_ReturnsSubcollectionWithSevenElements()
        {
            var testData = GetDefaultLiabilities(7);
            var collection = this.GetCollection(testData);

            var first7 = collection.First7NormalLiabilities;

            Assert.AreEqual(7, first7.Count);
            for (int i = 0; i < first7.Count; ++i)
            {
                var expectedId = testData[i].Key;
                var item = first7.GetRecordAt(i);
                Assert.AreEqual(expectedId, ((ILiabilityRegular)item).RecordId);
            }
        }

        [Test]
        public void First7NormalLiabilities_EightRegularLiabilities_ReturnsSubcollectionWithSevenElements()
        {
            var testData = GetDefaultLiabilities(8);
            var collection = this.GetCollection(testData);

            var first7 = collection.First7NormalLiabilities;

            Assert.AreEqual(7, first7.Count);
            for (int i = 0; i < first7.Count; ++i)
            {
                var expectedId = testData[i].Key;
                var item = first7.GetRecordAt(i);
                Assert.AreEqual(expectedId, ((ILiabilityRegular)item).RecordId);
            }
        }

        [Test]
        public void HasMoreThan7NormalLiablities_WithFourLiabilities_ReturnsFalse()
        {
            var testData = GetDefaultLiabilities(4);
            var collection = this.GetCollection(testData);

            Assert.AreEqual(false, collection.HasMoreThan7NormalLiablities);
        }

        [Test]
        public void HasMoreThan7NormalLiablities_WithNineLiabilities_ReturnsTrue()
        {
            var testData = GetDefaultLiabilities(9);
            var collection = this.GetCollection(testData);

            Assert.AreEqual(true, collection.HasMoreThan7NormalLiablities);
        }

        [Test]
        public void SortedView_NoRegularRecordsThreeSpecialRecords_ReturnsViewWithNoItems()
        {
            var testData = GetDefaultLiabilities(3);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            testData[1].Value.DebtType = E_DebtT.ChildSupport;
            testData[2].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var view = collection.SortedView;

            Assert.AreEqual(0, view.Count);
        }

        [Test]
        public void SortedView_ThreeRegularRecordsThreeSpecialRecords_ReturnsViewWithThreeRecords()
        {
            var testData = GetDefaultLiabilities(6);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            testData[1].Value.DebtType = E_DebtT.ChildSupport;
            testData[2].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var view = collection.SortedView;

            Assert.AreEqual(3, view.Count);
        }

        [Test]
        public void AddRegularRecord_Always_DefaultsDebtTypeToRevolving()
        {
            var collection = this.GetCollection();

            var liability = collection.AddRegularRecord();

            Assert.AreEqual(E_DebtRegularT.Revolving, liability.DebtT);
        }

        [Test]
        public void AddRegularRecord_Always_DefaultsAccountNameToSpecifiedDefault()
        {
            var app = Substitute.For<IShimAppDataProvider>();
            app.aBNm.Returns("DEFAULT");
            var collection = this.GetCollection(app: app);

            var liability = collection.AddRegularRecord();

            Assert.AreEqual("DEFAULT", liability.AccNm);
        }

        [Test]
        public void AddRegularRecord_NoExistingRecords_IncrementsCountRegular()
        {
            var collection = this.GetCollection();

            Assert.AreEqual(0, collection.CountRegular);
            var liability = collection.AddRegularRecord();

            Assert.AreEqual(1, collection.CountRegular);
        }

        [Test]
        public void AddRegularRecordAt_FourthIndexForCollectionWithFiveElements_Succeeds()
        {
            var testData = GetDefaultLiabilities(5);
            var collection = this.GetCollection(testData);

            var liability = collection.AddRegularRecordAt(3);
            liability.AccNm = "BONUS";

            Assert.AreEqual(6, collection.CountRegular);
            Assert.AreEqual("BONUS", collection.GetRegularRecordAt(3).AccNm);
        }

        [Test]
        public void BindFromObjectModel_Always_ThrowsNotImplementedException()
        {
            var collection = InitializeCollection(null);

            Assert.Throws<NotImplementedException>(() => collection.ToInputModel());
        }

        [Test]
        public void CreateNewJobExpenseAtFirstAvailSlot_NoExistingJobExpenses_AssignsJobExpense1()
        {
            var collection = this.GetCollection();

            var newJobExpense = collection.CreateNewJobExpenseAtFirstAvailSlot();

            Assert.AreEqual(collection.GetJobRelated1(false), newJobExpense);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense1, newJobExpense.DebtJobExpenseT);
        }

        [Test]
        public void Constructor_TwoAlimonyRecords_ThrowsCBaseException()
        {
            var tooManyAlimonyRecords = GetDefaultLiabilities(3);
            tooManyAlimonyRecords[0].Value.DebtType = E_DebtT.Alimony;
            tooManyAlimonyRecords[2].Value.DebtType = E_DebtT.Alimony;

            var collectionContainer = GetCollectionContainer(tooManyAlimonyRecords);

            Assert.Throws<CBaseException>(() => new LiabilityCollectionShim(collectionContainer as IShimContainer, Substitute.For<ILiabilityDefaultsProvider>(), Substitute.For<IShimAppDataProvider>(), Substitute.For<IShimOwnershipManager<Guid>>(), new LosConvert()));
        }

        [Test]
        public void Constructor_TwoChildSupport_ThrowsCBaseException()
        {
            var tooManyChildSupportRecords = GetDefaultLiabilities(3);
            tooManyChildSupportRecords[0].Value.DebtType = E_DebtT.ChildSupport;
            tooManyChildSupportRecords[2].Value.DebtType = E_DebtT.ChildSupport;

            var collectionContainer = GetCollectionContainer(tooManyChildSupportRecords);

            Assert.Throws<CBaseException>(() => new LiabilityCollectionShim(collectionContainer as IShimContainer, Substitute.For<ILiabilityDefaultsProvider>(), Substitute.For<IShimAppDataProvider>(), Substitute.For<IShimOwnershipManager<Guid>>(), new LosConvert()));
        }

        [Test]
        public void Constructor_ThreeJobExpenses_ThrowsCBaseException()
        {
            var tooManyJobExpenseRecords = GetDefaultLiabilities(4);
            tooManyJobExpenseRecords[1].Value.DebtType = E_DebtT.JobRelatedExpense;
            tooManyJobExpenseRecords[2].Value.DebtType = E_DebtT.JobRelatedExpense;
            tooManyJobExpenseRecords[3].Value.DebtType = E_DebtT.JobRelatedExpense;

            var collectionContainer = GetCollectionContainer(tooManyJobExpenseRecords);

            Assert.Throws<CBaseException>(() => new LiabilityCollectionShim(collectionContainer as IShimContainer, Substitute.For<ILiabilityDefaultsProvider>(), Substitute.For<IShimAppDataProvider>(), Substitute.For<IShimOwnershipManager<Guid>>(), new LosConvert()));
        }

        [Test]
        public void CreateNewJobExpenseAtFirstAvailSlot_JobExpense1Exists_AssignsJobExpense2()
        {
            var testData = GetDefaultLiabilities(1);
            testData[0].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var newJobExpense = collection.CreateNewJobExpenseAtFirstAvailSlot();

            Assert.AreEqual(collection.GetJobRelated2(false), newJobExpense);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense2, newJobExpense.DebtJobExpenseT);
        }

        [Test]
        public void CreateNewJobExpenseAtFirstAvailSlot_TwoExistingJobExpenses_ReturnsNull()
        {
            var testData = GetDefaultLiabilities(2);
            testData[0].Value.DebtType = E_DebtT.JobRelatedExpense;
            testData[1].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var newJobExpense = collection.CreateNewJobExpenseAtFirstAvailSlot();

            Assert.AreEqual(null, newJobExpense);
        }

        [Test]
        public void GetAlimony_NoAlimonyNoForceCreate_ReturnsNull()
        {
            var collection = this.GetCollection();

            var alimony = collection.GetAlimony(false);

            Assert.AreEqual(null, alimony);
        }

        [Test]
        public void GetAlimony_NoAlimonyForceCreate_ReturnsAlimony()
        {
            var collection = this.GetCollection();

            var alimony = collection.GetAlimony(true);

            Assert.IsNotNull(alimony);
            Assert.AreEqual(E_DebtSpecialT.Alimony, alimony.DebtT);
            Assert.AreEqual(collection.GetAlimony(false), alimony);
        }

        [Test]
        public void GetAlimony_AlimonyExists_ReturnsExistingAlimony()
        {
            var testData = GetDefaultLiabilities(1);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            var collection = this.GetCollection(testData);

            var alimony = collection.GetAlimony(false);

            Assert.IsNotNull(alimony);
            Assert.AreEqual(E_DebtSpecialT.Alimony, alimony.DebtT);
            Assert.AreEqual(testData[0].Key, alimony.RecordId);
        }

        [Test]
        public void GetChildSupport_NoChildSupportNoForceCreate_ReturnsNull()
        {
            var collection = this.GetCollection();

            var childSupport = collection.GetChildSupport(false);

            Assert.AreEqual(null, childSupport);
        }

        [Test]
        public void GetChildSupport_NoChildSupportForceCreate_ReturnsChildSupport()
        {
            var collection = this.GetCollection();

            var childSupport = collection.GetChildSupport(true);

            Assert.IsNotNull(childSupport);
            Assert.AreEqual(E_DebtSpecialT.ChildSupport, childSupport.DebtT);
            Assert.AreEqual(collection.GetChildSupport(false), childSupport);
        }

        [Test]
        public void GetChildSupport_ChildSupportExists_ReturnsExistingChildSupport()
        {
            var testData = GetDefaultLiabilities(1);
            testData[0].Value.DebtType = E_DebtT.ChildSupport;
            var collection = this.GetCollection(testData);

            var childSupport = collection.GetChildSupport(false);

            Assert.IsNotNull(childSupport);
            Assert.AreEqual(E_DebtSpecialT.ChildSupport, childSupport.DebtT);
            Assert.AreEqual(testData[0].Key, childSupport.RecordId);
        }

        [Test]
        public void GetJobRelated1_NoJobRelated1NoForceCreate_ReturnsNull()
        {
            var collection = this.GetCollection();

            var jobRelated1 = collection.GetJobRelated1(false);

            Assert.AreEqual(null, jobRelated1);
        }

        [Test]
        public void GetJobRelated1_NoJobRelated1ForceCreate_ReturnsJobRelated1()
        {
            var collection = this.GetCollection();

            var jobRelated1 = collection.GetJobRelated1(true);

            Assert.IsNotNull(jobRelated1);
            Assert.AreEqual(E_DebtSpecialT.JobRelatedExpense, jobRelated1.DebtT);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense1, jobRelated1.DebtJobExpenseT);
            Assert.AreEqual(collection.GetJobRelated1(false), jobRelated1);
        }

        [Test]
        public void GetJobRelated1_JobRelated1Exists_ReturnsExistingJobRelated1()
        {
            var testData = GetDefaultLiabilities(1);
            testData[0].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var jobRelated1 = collection.GetJobRelated1(true);

            Assert.IsNotNull(jobRelated1);
            Assert.AreEqual(E_DebtSpecialT.JobRelatedExpense, jobRelated1.DebtT);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense1, jobRelated1.DebtJobExpenseT);
            Assert.AreEqual(testData[0].Key, jobRelated1.RecordId);
        }

        [Test]
        public void GetJobRelated2_NoJobRelated2NoForceCreate_ReturnsNull()
        {
            var collection = this.GetCollection();

            var jobRelated2 = collection.GetJobRelated2(false);

            Assert.AreEqual(null, jobRelated2);
        }

        [Test]
        public void GetJobRelated2_NoJobRelated2ForceCreate_ReturnsJobRelated2()
        {
            // It is intentional to require that one JobExpense liability exists
            // when you're adding JobExpense2. If there is no JobExpense1 but the
            // user force creates JobExpense2, then it will become JobExpense1 when
            // the shim data is rebuilt.
            var testData = GetDefaultLiabilities(1);
            testData[0].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var jobRelated2 = collection.GetJobRelated2(true);

            Assert.IsNotNull(jobRelated2);
            Assert.AreEqual(E_DebtSpecialT.JobRelatedExpense, jobRelated2.DebtT);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense2, jobRelated2.DebtJobExpenseT);
            Assert.AreEqual(collection.GetJobRelated2(false), jobRelated2);
        }

        [Test]
        public void GetJobRelated2_JobRelated2Exists_ReturnsExistingJobRelated2()
        {
            // We need two job expenses if we're going to have a JobRelated2.
            var testData = GetDefaultLiabilities(2);
            testData[0].Value.DebtType = E_DebtT.JobRelatedExpense;
            testData[1].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var jobRelated2 = collection.GetJobRelated2(true);

            Assert.IsNotNull(jobRelated2);
            Assert.AreEqual(E_DebtSpecialT.JobRelatedExpense, jobRelated2.DebtT);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense2, jobRelated2.DebtJobExpenseT);
            Assert.AreEqual(testData[1].Key, jobRelated2.RecordId);
        }

        [Test]
        public void GetJobRelated2_ForceCreateWithoutJobExpense1_ReturnsNewJobRelated2()
        {
            var testData = GetDefaultLiabilities(0);
            var collection = this.GetCollection(testData);

            var jobRelated2 = collection.GetJobRelated2(true);

            Assert.IsNotNull(jobRelated2);
            Assert.AreEqual(E_DebtSpecialT.JobRelatedExpense, jobRelated2.DebtT);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense2, jobRelated2.DebtJobExpenseT);
        }

        [Test]
        public void GetJobRelated2_ForceCreateWithoutJobExpense1_AlsoCreatesJobExpense1()
        {
            var testData = GetDefaultLiabilities(0);
            var collection = this.GetCollection(testData);

            var jobRelated2 = collection.GetJobRelated2(true);
            var jobRelated1 = collection.GetJobRelated1(false);

            Assert.IsNotNull(jobRelated1);
            Assert.AreEqual(E_DebtSpecialT.JobRelatedExpense, jobRelated1.DebtT);
            Assert.AreEqual(E_DebtJobExpenseT.JobExpense1, jobRelated1.DebtJobExpenseT);
        }

        [Test]
        public void GetRegRecordOf_FiveRegularRecords_Succeeds()
        {
            var testData = GetDefaultLiabilities(5);
            var secondId = testData[0].Key;
            var collection = this.GetCollection(testData);

            var liability = collection.GetRegRecordOf(secondId);

            Assert.AreEqual(secondId, liability.RecordId);
        }

        [Test]
        public void GetRegRecordOf_PassInSpecialRecordId_ThrowsCBaseException()
        {
            var testData = GetDefaultLiabilities(3);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            testData[1].Value.DebtType = E_DebtT.ChildSupport;
            testData[2].Value.DebtType = E_DebtT.JobRelatedExpense;
            var thirdId = testData[2].Key;
            var collection = this.GetCollection(testData);

            Assert.Throws<CBaseException>(() => collection.GetRegRecordOf(thirdId));
        }

        [Test]
        public void GetRegularRecordAt_LastIndex_Succeeds()
        {
            var testData = GetDefaultLiabilities(5);
            var collection = this.GetCollection(testData);

            var liability = collection.GetRegularRecordAt(4);

            Assert.AreEqual(testData.Last().Key, liability.RecordId);
        }

        [Test]
        public void GetSpecialRecordAt_FirstIndex_Succeeds()
        {
            var testData = GetDefaultLiabilities(5);
            testData[4].Value.DebtType = E_DebtT.ChildSupport;
            var collection = this.GetCollection(testData);

            var liability = collection.GetSpecialRecordAt(0);

            Assert.AreEqual(testData[4].Key, liability.RecordId);
        }

        [Test]
        public void GetSubcollection_InclusiveSpecialGroup_ReturnsExpectedLength()
        {
            var testData = GetDefaultLiabilities(10);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            testData[1].Value.DebtType = E_DebtT.ChildSupport;
            testData[2].Value.DebtType = E_DebtT.JobRelatedExpense;
            testData[3].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var subcollection = collection.GetSubcollection(true, E_DebtGroupT.Special);

            Assert.AreEqual(4, subcollection.Count);
        }

        [Test]
        public void GetSubcollection_ExclusiveSpecialGroup_ReturnsExpectedLength()
        {
            var testData = GetDefaultLiabilities(10);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            testData[1].Value.DebtType = E_DebtT.ChildSupport;
            testData[2].Value.DebtType = E_DebtT.JobRelatedExpense;
            testData[3].Value.DebtType = E_DebtT.JobRelatedExpense;
            var collection = this.GetCollection(testData);

            var subcollection = collection.GetSubcollection(false, E_DebtGroupT.Special);

            Assert.AreEqual(6, subcollection.Count);
        }

        [Test]
        public void GetSubcollection_AlimonyOrInstallment_ReturnsExpectedLength()
        {
            var testData = GetDefaultLiabilities(10);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            testData[1].Value.DebtType = E_DebtT.Installment;
            var collection = this.GetCollection(testData);

            var subcollection = collection.GetSubcollection(true, E_DebtGroupT.Alimony | E_DebtGroupT.Installment);

            Assert.AreEqual(2, subcollection.Count);
        }

        [Test]
        public void ToInputModel_Always_ThrowsNotImplementedException()
        {
            var collection = InitializeCollection(null);

            Assert.Throws<NotImplementedException>(() => collection.ToInputModel());
        }

        [Test]
        public void AddRecord_Mortgage_AddsLiabilityOfMortgageType()
        {
            var collection = this.GetCollection();

            var liability = collection.AddRecord(E_DebtRegularT.Mortgage) as global::DataAccess.ILiability;

            Assert.AreEqual(E_DebtT.Mortgage, liability.DebtT);
        }

        [Test]
        public void AddRecord_AlimonyNoExistingAlimony_AddsLiabilityOfAlimonyType()
        {
            var collection = this.GetCollection();

            var liability = collection.AddRecord(E_DebtSpecialT.Alimony) as global::DataAccess.ILiability;

            Assert.AreEqual(E_DebtT.Alimony, liability.DebtT);
        }

        [Test]
        public void AddRecord_AlimonyWithExistingAlimony_ThrowsCBaseException()
        {
            var testData = GetDefaultLiabilities(1);
            testData[0].Value.DebtType = E_DebtT.Alimony;
            var collection = this.GetCollection(testData);

            Assert.Throws<CBaseException>(() => collection.AddRecord(E_DebtSpecialT.Alimony));
        }

        [Test]
        public void AddRecord_RegularRecordWith3ExistingRegularRecords_AddsToEndOfRegularRecords()
        {
            var testData = GetDefaultLiabilities(3);
            var collection = this.GetCollection(testData);

            var addedLiability = collection.AddRecord(E_DebtT.Open);
            var liabilityByPosition = collection.GetRegularRecordAt(3);

            Assert.AreEqual(addedLiability.RecordId, liabilityByPosition.RecordId);
        }

        [Test]
        public void Flush_Always_CallsAcceptLiabilityAggregateTotalsOnApp()
        {
            var testData = GetDefaultLiabilities(0);
            var appFake = Substitute.For<IShimAppDataProvider>();
            var collection = this.GetCollection(testData, appFake);

            collection.Flush();

            appFake.Received().AcceptLiabilityAggregateTotals(Arg.Is<CalculatedFields.Liability.AggregateData>(a =>
                a.LiaBalTot == 0
                    && a.LiaMonTot == 0
                    && a.LiaPdOffTot == 0));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void Flush_AfterRemovingLiability_RemovesLiability(bool useIsOnDeathRow)
        {
            var containerFactory = new LoanLqbCollectionContainerFactory();
            var provider = Substitute.ForPartsOf<LoanLqbEmptyCollectionProvider>();
            var liabilityCollection = provider.LoadLiabilities();
            var consumerLiabilityCollection = provider.LoadConsumerLiabilities();
            var consumerId = Guid.Parse("deadbeef-dead-dead-dead-beefbeefbeef").ToIdentifier<DataObjectKind.Consumer>();
            var legacyAppId = Guid.Empty.ToIdentifier<DataObjectKind.LegacyApplication>();
            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>())
            {
                DebtType = E_DebtT.Mortgage
            };
            var liabilityId = liabilityCollection.Add(liability);
            var ownership = new ConsumerLiabilityAssociation(consumerId, liabilityId, legacyAppId) { IsPrimary = true };
            var ownershipId = consumerLiabilityCollection.Add(ownership);
            provider.LoadLiabilities().Returns(liabilityCollection);
            provider.LoadConsumerLiabilities().Returns(consumerLiabilityCollection);
            var collectionContainer = containerFactory.Create(
                provider,
                Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>(),
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);
            collectionContainer.RegisterCollections(new[]
            {
                nameof(ILoanLqbCollectionContainer.Liabilities),
                nameof(ILoanLqbCollectionContainer.ConsumerLiabilities),
                nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities),
            });
            collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());
            var shimOwnershipManager = new ShimOwnershipManager<DataObjectKind.Liability, DataObjectKind.ConsumerLiabilityAssociation, Liability, ConsumerLiabilityAssociation>(
                (IShimContainer)collectionContainer,
                consumerId,
                Guid.Empty.ToIdentifier<DataObjectKind.Consumer>(),
                Guid.Empty.ToIdentifier<DataObjectKind.LegacyApplication>(),
                (a, b, c) => { throw new NotImplementedException(); });
            var collectionShim = new LiabilityCollectionShim(
                (IShimContainer)collectionContainer,
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimAppDataProvider>(),
                shimOwnershipManager,
                new LosConvert());
            Assert.AreEqual(1, collectionContainer.Liabilities.Count);
            Assert.AreEqual(1, collectionContainer.ConsumerLiabilities.Count);
            Assert.AreEqual(1, collectionShim.CountRegular);
            Assert.AreEqual(0, collectionShim.CountSpecial);
            if (useIsOnDeathRow)
            {
                collectionShim.GetRegularRecordAt(0).IsOnDeathRow = true;
            }
            else
            {
                collectionContainer.Remove(liabilityId);
            }

            collectionShim.Flush();

            Assert.AreEqual(0, collectionContainer.Liabilities.Count);
            Assert.AreEqual(0, collectionContainer.ConsumerLiabilities.Count);
            Assert.AreEqual(0, collectionShim.CountRegular);
            Assert.AreEqual(0, collectionShim.CountSpecial);
        }

        internal static IReadOnlyList<KeyValuePair<Guid, Liability>> GetDefaultLiabilities(int count)
        {
            var entities = new List<KeyValuePair<Guid, Liability>>(count);

            for (int i = 0; i < count; ++i)
            {
                entities.Add(new KeyValuePair<Guid, Liability>(Guid.NewGuid(), new Liability(Substitute.For<ILiabilityDefaultsProvider>())));
            }

            return entities;
        }

        private LiabilityCollectionShim GetCollection(
            IEnumerable<KeyValuePair<Guid, Liability>> entities = null,
            IShimAppDataProvider app = null)
        {
            return InitializeCollection(null, entities, app);
        }

        internal static LiabilityCollectionShim InitializeCollection(LosConvert converter, IEnumerable<KeyValuePair<Guid, Liability>> entities = null, IShimAppDataProvider app = null)
        {
            converter = converter ?? new LosConvert();

            var collectionContainer = GetCollectionContainer(entities, app);

            return new LiabilityCollectionShim(
                collectionContainer as IShimContainer,
                Substitute.For<ILiabilityDefaultsProvider>(),
                app ?? Substitute.For<IShimAppDataProvider>(),
                Substitute.For<IShimOwnershipManager<Guid>>(),
                converter);
        }

        internal static ILoanLqbCollectionContainer GetCollectionContainer(IEnumerable<KeyValuePair<Guid, Liability>> entities = null, IShimAppDataProvider app = null)
        {
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();

            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(app?.aBConsumerId ?? Guid.Empty);
            var coborrowerId = app != null && app.aCConsumerId == Guid.Empty ? (DataObjectIdentifier<DataObjectKind.Consumer, Guid>?)null : DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(app?.aCConsumerId ?? new Guid("22222222-2222-2222-2222-222222222222"));
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(app?.aAppId ?? Guid.Empty);
            var legacyAppConsumers = Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(appId, borrowerId, coborrowerId);
            loanData.LegacyApplicationConsumers.Returns(legacyAppConsumers);

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var collectionContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));

            collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());

            foreach (var entity in entities ?? Enumerable.Empty<KeyValuePair<Guid, Liability>>())
            {
                collectionContainer.Add(
                    borrowerId,
                    DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(entity.Key),
                    entity.Value);
            }

            return collectionContainer;
        }

        internal static Liability CreateLiability()
        {
            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            liability.DebtType = E_DebtT.Mortgage;
            liability.UsedInRatioData = true;
            liability.PmlAuditTrail = null;
            liability.Pmt = Money.Create(3000);
            liability.RemainMon = LiabilityRemainingMonths.Create("180");
            liability.WillBePdOff = false;
            liability.AccountName = DescriptionField.Create("Account Name");
            liability.AccountNum = ThirdPartyIdentifier.Create("Account Number");
            liability.Attention = EntityName.Create("Attention Person");
            liability.AutoYearMakeAsString = YearAsString.Create("1999");
            liability.Bal = Money.Create(500000);
            liability.CompanyAddress = StreetAddress.Create("1600 Sunflower Ave Ste 200");
            liability.CompanyCity = City.Create("Costa Mesa");
            liability.CompanyFax = null;
            liability.CompanyName = EntityName.Create("MeridianLink, Inc.");
            liability.CompanyPhone = PhoneNumber.Create("714-708-6950");
            liability.CompanyState = UnitedStatesPostalState.Create("CA");
            liability.CompanyZip = Zipcode.Create("92626");
            liability.Desc = DescriptionField.Create("Bone-crushing mortgage!!");
            liability.DueInMonAsString = CountString.Create("180");
            liability.ExcludeFromUw = false;
            liability.FullyIndexedPitiPmt = Money.Create(3250);
            liability.IncludeInBk = false;
            liability.IncludeInFc = false;
            liability.IncludeInReposession = false;
            liability.IsForAuto = false;
            liability.IsMtgFhaInsured = false;
            liability.IsPiggyBack = false;
            liability.IsSeeAttachment = false;
            liability.IsSp1stMtgData = true;
            liability.Late30AsString = CountString.Create("0");
            liability.Late60AsString = CountString.Create("0");
            liability.Late90PlusAsString = CountString.Create("0");
            liability.OriginalDebtAmt = Money.Create(700000);
            liability.OriginalTermAsString = CountString.Create("360");
            liability.PayoffAmtData = Money.Create(700000);
            liability.PayoffAmtLocked = false;
            liability.PayoffTimingData = PayoffTiming.BeforeClosing;
            liability.PayoffTimingLockedData = true;
            liability.PrepDate = UnzonedDate.Create(2033, 4, 27);
            liability.Rate = Percentage.Create(3.025m);
            liability.ReconcileStatusType = E_LiabilityReconcileStatusT.Matched;
            liability.UsedInRatioData = true;
            liability.VerifExpiresDate = UnzonedDate.Create(2050, 1, 1);
            liability.VerifRecvDate = UnzonedDate.Create(2018, 3, 25);
            liability.VerifReorderedDate = null;
            liability.VerifSentDate = UnzonedDate.Create(2018, 3, 20);
            liability.OwedTo = PersonName.Create("Big Boss McGee");
            liability.JobExpenseDesc = DescriptionField.Create("Job stuffs");

            return liability;
        }
    }
}
