﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using NSubstitute;
    using LqbGrammar.Drivers;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
     sealed class AssetCollectionShimTest
    {
        private static Guid id0;
        private static Guid id1;
        private static Guid id2;
        private static Guid id3;
        private static Guid id4;
        private static Guid id5;
        private static Guid id6;

        [Test]
        public void Construct()
        {
            var collectionShim = InitializeCollection(null);
            this.CheckSizes(3, 4, collectionShim);
        }

        [Test]
        public void CheckInitialOrder()
        {
            var collectionShim = InitializeCollection(null);
            this.CheckOrder(collectionShim, id0, id1, id2);
            this.CheckSpecialOrder(collectionShim, id3, id4, id5, id6);
        }

        [Test]
        public void AddRecord()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.AddRecord(E_AssetT.Auto);

            this.CheckSizes(4, 4, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2, shim.RecordId);
            this.CheckSpecialOrder(collectionShim, id3, id4, id5, id6);
        }

        [Test]
        public void AddRegularRecord()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.AddRegularRecord();

            this.CheckSizes(4, 4, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2, shim.RecordId);
            this.CheckSpecialOrder(collectionShim, id3, id4, id5, id6);
        }

        [Test]
        public void AddRegularRecordAt()
        {
            var collectionShim = InitializeCollection(null);
            var shim = collectionShim.AddRegularRecordAt(2);

            this.CheckSizes(4, 4, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, shim.RecordId, id2);
            this.CheckSpecialOrder(collectionShim, id3, id4, id5, id6);
        }

        [Test]
        public void ClearAll()
        {
            var collectionShim = InitializeCollection(null);

            collectionShim.ClearAll();

            this.CheckSizes(0, 0, collectionShim);
        }

        [Test]
        public void EnsureRegularRecordOf_NotExist()
        {
            var collectionShim = InitializeCollection(null);

            var guid = Guid.NewGuid();
            var shim = collectionShim.EnsureRegularRecordOf(guid);

            Assert.AreEqual(guid, shim.RecordId);
            this.CheckSizes(4, 4, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2, guid);
        }

        [Test]
        public void EnsureRegularRecordOf_YesExist()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.EnsureRegularRecordOf(id0);

            Assert.AreEqual(id0, shim.RecordId);
            this.CheckSizes(3, 4, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2);
        }

        [Test]
        public void GetRegRecordOf()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegRecordOf(id1);

            Assert.AreEqual(id1, shim.RecordId);
        }

        [Test]
        public void GetRegularRecordAt()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegularRecordAt(2);

            Assert.AreEqual(id2, shim.RecordId);
        }

        [Test]
        public void GetSpecialRecordAt()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetSpecialRecordAt(0);

            Assert.AreEqual(id3, shim.RecordId);

            Assert.Throws<IndexOutOfRangeException>(() => collectionShim.GetSpecialRecordAt(90));
        }

        [Test]
        public void HasRecordOf_False()
        {
            var collectionShim = InitializeCollection(null);

            var guid = Guid.NewGuid();
            bool has = collectionShim.HasRecordOf(guid);

            Assert.IsFalse(has);
        }

        [Test]
        public void HasRecordOf_True()
        {
            var collectionShim = InitializeCollection(null);

            bool has0 = collectionShim.HasRecordOf(id0);
            bool has1 = collectionShim.HasRecordOf(id1);
            bool has2 = collectionShim.HasRecordOf(id2);
            bool has3 = collectionShim.HasRecordOf(id3);
            bool has4 = collectionShim.HasRecordOf(id4);
            bool has5 = collectionShim.HasRecordOf(id5);
            bool has6 = collectionShim.HasRecordOf(id6);

            Assert.IsTrue(has0);
            Assert.IsTrue(has1);
            Assert.IsTrue(has2);
            Assert.IsTrue(has3);
            Assert.IsTrue(has4);
            Assert.IsTrue(has5);
            Assert.IsTrue(has6);
        }

        [Test]
        public void MoveRegularRecordDown()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegularRecordAt(1);
            collectionShim.MoveRegularRecordDown(shim);

            this.CheckOrder(collectionShim, id0, id2, id1);
            this.CheckSpecialOrder(collectionShim, id3, id4, id5, id6);
        }

        [Test]
        public void MoveRegularRecordUp()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegularRecordAt(1);
            collectionShim.MoveRegularRecordUp(shim);

            this.CheckOrder(collectionShim, id1, id0, id2);
            this.CheckSpecialOrder(collectionShim, id3, id4, id5, id6);
        }

        [Test]
        public void SortRegularRecordsByKey()
        {
            var collectionShim = InitializeCollection(null);

            // 0:"Auto", 1:"TrustFunds", 2:"Bonds"
            collectionShim.SortRegularRecordsByKey("AccNm", directionAsc: false); 
            this.CheckOrder(collectionShim, id1, id2, id0); 
        }

        private void CheckSizes(int expectedRegularSize, int expectedSpecial, IAssetCollection collection)
        {
            Assert.AreEqual(expectedRegularSize, collection.CountRegular);
            Assert.AreEqual(expectedSpecial, collection.CountSpecial);

            var ds = collection.GetDataSet();
            var table = ds.Tables[0];
            Assert.AreEqual(expectedRegularSize + expectedSpecial, table.Rows.Count);
        }

        private void CheckOrder(IAssetCollection collection, params Guid[] ids)
        {
            for (int i = 0; i < ids.Length; ++i)
            {
                var item = collection.GetRegularRecordAt(i);

                Assert.AreEqual(ids[i], item.RecordId);
            }
        }

        private void CheckSpecialOrder(IAssetCollection collection, params Guid[] ids)
        {
            for (int i = 0; i < ids.Length; ++i)
            {
                var item = collection.GetSpecialRecordAt(i);

                Assert.AreEqual(ids[i], item.RecordId);
            }
        }



        [Test]
        public void AssetCollection_AutomobileCollection()
        {
            var collectionShim = InitializeCollection(null);
            Assert.AreEqual(1, collectionShim.AutomobileCollection.Count);

            var record = collectionShim.AddRegularRecord();
            record.AssetT = E_AssetRegularT.Auto;

            Assert.AreEqual(2, collectionShim.AutomobileCollection.Count);
        }

        [Test]
        public void AssetCollection_AutomobileWorthTotal_rep()
        {
            var collectionShim = InitializeCollection(null);

            Assert.AreEqual("$18,520.11", collectionShim.AutomobileWorthTotal_rep);

            var record = collectionShim.AddRegularRecord();
            record.AssetT = E_AssetRegularT.Auto;
            record.Val = 100.00m;

            Assert.AreEqual("$18,620.11", collectionShim.AutomobileWorthTotal_rep);
        }
        [Test]
        public void AssetCollection_ExtraNormalAssets()
        {
            var collectionShim = InitializeCollection(null);
            Assert.AreEqual(0, collectionShim.ExtraNormalAssets.Count);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(0, collectionShim.ExtraNormalAssets.Count);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(0, collectionShim.ExtraNormalAssets.Count);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(0, collectionShim.ExtraNormalAssets.Count);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(1, collectionShim.ExtraNormalAssets.Count);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(2, collectionShim.ExtraNormalAssets.Count);
        }
        [Test]
        public void AssetCollection_First4NormalAssets()
        {
            var collectionShim = InitializeCollection(null);
            Assert.AreEqual(1, collectionShim.First4NormalAssets.Count);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(2, collectionShim.First4NormalAssets.Count);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(3, collectionShim.First4NormalAssets.Count);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(4, collectionShim.First4NormalAssets.Count);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.MoneyMarketFund;
            Assert.AreEqual(4, collectionShim.First4NormalAssets.Count);
        }
        [Test]
        public void AssetCollection_NormalAssetCollection()
        {
            var collectionShim = InitializeCollection(null);
            Assert.AreEqual(1, collectionShim.NormalAssetCollection.Count);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.TrustFunds;
            Assert.AreEqual(2, collectionShim.NormalAssetCollection.Count);

        }
        [Test]
        public void AssetCollection_OtherAssetCollection()
        {
            var collectionShim = InitializeCollection(null);
            Assert.AreEqual(0, collectionShim.OtherAssetCollection.Count);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.OtherIlliquidAsset;
            Assert.AreEqual(1, collectionShim.OtherAssetCollection.Count);
        }
        [Test]
        public void AssetCollection_StockCollection()
        {
            var collectionShim = InitializeCollection(null);
            Assert.AreEqual(1, collectionShim.StockCollection.Count);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Stocks;
            Assert.AreEqual(2, collectionShim.StockCollection.Count);
        }
        [Test]
        public void AssetCollection_GetSubcollection()
        {
           var collectionShim = InitializeCollection(null);
           Assert.AreEqual(1, collectionShim.GetSubcollection(true, E_AssetGroupT.Bonds).Count);

           Assert.AreEqual(7, collectionShim.GetSubcollection(true, E_AssetGroupT.All).Count);
        }

        [Test]
        public void AssetCollection_GetBusinessWorth_Missing()
        {
            var collectionShim = InitializeCollection(null, includeSpecials: false);
            var businessRecord = collectionShim.GetBusinessWorth(false);

            Assert.IsNull(businessRecord);

            businessRecord = collectionShim.GetBusinessWorth(true);
            Assert.IsNotNull(businessRecord);
        }

        [Test]
        public void AssetCollection_GetBusinessWorth_Present()
        {
            var collectionShim = InitializeCollection(null);
            var businessRecord = collectionShim.GetBusinessWorth(false);
            Assert.AreEqual(businessRecord.RecordId, id6);

            businessRecord = collectionShim.GetBusinessWorth(true);
            Assert.AreEqual(businessRecord.RecordId, id6);
        }

        [Test]
        public void AssetCollection_GetCashDeposit1_Missing()
        {
            var collectionShim = InitializeCollection(null, includeSpecials: false);
            Assert.IsNull(collectionShim.GetCashDeposit1(false));

            var newCashDepositRecord = collectionShim.GetCashDeposit1(true);
            Assert.IsNotNull(newCashDepositRecord);
        }

        [Test]
        public void AssetCollection_GetCashDeposit1_Present()
        {
            var collectionShim = InitializeCollection(null, includeSpecials: false);
            Assert.IsNull(collectionShim.GetCashDeposit1(false));

            var cashDepositRecord = (IAssetCashDeposit)collectionShim.AddRecord(E_AssetT.CashDeposit);
            cashDepositRecord.AssetCashDepositT = E_AssetCashDepositT.CashDeposit1;

            var newCashDepositRecord = collectionShim.GetCashDeposit1(false);
            Assert.AreEqual(newCashDepositRecord.RecordId, cashDepositRecord.RecordId);

            newCashDepositRecord = collectionShim.GetCashDeposit1(true);
            Assert.AreEqual(newCashDepositRecord.RecordId, cashDepositRecord.RecordId);
        }

        [Test]
        public void AssetCollection_GetCashDeposit2_Missing()
        {
            var collectionShim = InitializeCollection(null, includeSpecials: false);

            Assert.IsNull(collectionShim.GetCashDeposit2(false));

            var newCashDepositRecord = collectionShim.GetCashDeposit2(true);

            Assert.IsNotNull(newCashDepositRecord);

            // Make sure it does not slide into position 1 when 1 is missing.
            newCashDepositRecord = collectionShim.GetCashDeposit2(false);
            Assert.IsNotNull(newCashDepositRecord);
            Assert.AreEqual(E_AssetCashDepositT.CashDeposit2, newCashDepositRecord.AssetCashDepositT);
        }

        [Test]
        public void AssetCollection_GetCashDeposit2_Present()
        {
            var collectionShim = InitializeCollection(null, includeSpecials: false);

            Assert.IsNull(collectionShim.GetCashDeposit2(false));

            var cashDepositRecord = (IAssetCashDeposit)collectionShim.AddRecord(E_AssetT.CashDeposit);
            cashDepositRecord.AssetCashDepositT = E_AssetCashDepositT.CashDeposit2;

            var newCashDepositRecord = collectionShim.GetCashDeposit2(false);
            Assert.AreEqual(newCashDepositRecord.RecordId, cashDepositRecord.RecordId);

            newCashDepositRecord = collectionShim.GetCashDeposit2(true);
            Assert.AreEqual(newCashDepositRecord.RecordId, cashDepositRecord.RecordId);
        }

        [Test]
        public void AssetCollection_GetLifeInsurance_Missing()
        {
            var collectionShim = InitializeCollection(null, includeSpecials: false);
            var lifeInsuranceRecord = collectionShim.GetLifeInsurance(false);

            Assert.IsNull(lifeInsuranceRecord);

            lifeInsuranceRecord = collectionShim.GetLifeInsurance(true);
            Assert.IsNotNull(lifeInsuranceRecord);
        }

        [Test]
        public void AssetCollection_GetLifeInsurance_Present()
        {
            var collectionShim = InitializeCollection(null);
            var lifeInsuranceRecord = collectionShim.GetLifeInsurance(false);
            Assert.AreEqual(lifeInsuranceRecord.RecordId, id4);

            lifeInsuranceRecord = collectionShim.GetLifeInsurance(true);
            Assert.AreEqual(lifeInsuranceRecord.RecordId, id4);
        }

        [Test]
        public void AssetCollection_GetRetirement_Missing()
        {
            var collectionShim = InitializeCollection(null, includeSpecials: false);
            var retirementRecord = collectionShim.GetRetirement(false);

            Assert.IsNull(retirementRecord);

            retirementRecord = collectionShim.GetRetirement(true);
            Assert.IsNotNull(retirementRecord);
        }

        [Test]
        public void AssetCollection_GetRetirement_Present()
        {
            var collectionShim = InitializeCollection(null);
            var retirementRecord = collectionShim.GetRetirement(false);
            Assert.AreEqual(retirementRecord.RecordId, id3);

            retirementRecord = collectionShim.GetRetirement(true);
            Assert.AreEqual(retirementRecord.RecordId, id3);
        }

        [Test]
        public void AssetCollection_HasExtraAutomobiles()
        {
            var collectionShim = InitializeCollection(null);
            Assert.IsFalse(collectionShim.HasExtraAutomobiles);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Auto;
            Assert.IsFalse(collectionShim.HasExtraAutomobiles);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Auto;
            Assert.IsFalse(collectionShim.HasExtraAutomobiles);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Auto;
            Assert.IsTrue(collectionShim.HasExtraAutomobiles);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Auto;
            Assert.IsTrue(collectionShim.HasExtraAutomobiles);
        }
        [Test]
        public void AssetCollection_HasExtraOtherAssets()
        {
            var collectionShim = InitializeCollection(null);
            Assert.IsFalse(collectionShim.HasExtraOtherAssets);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.OtherIlliquidAsset;
            Assert.IsFalse(collectionShim.HasExtraOtherAssets);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.OtherIlliquidAsset;
            Assert.IsFalse(collectionShim.HasExtraOtherAssets);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.OtherIlliquidAsset;
            Assert.IsFalse(collectionShim.HasExtraOtherAssets);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.OtherIlliquidAsset;
            Assert.IsTrue(collectionShim.HasExtraOtherAssets);
        }
        [Test]
        public void AssetCollection_HasExtraStocks()
        {
            var collectionShim = InitializeCollection(null);
            Assert.IsFalse(collectionShim.HasExtraStocks);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Stocks;
            Assert.IsFalse(collectionShim.HasExtraStocks);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Stocks;
            Assert.IsFalse(collectionShim.HasExtraStocks);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Stocks;
            Assert.IsTrue(collectionShim.HasExtraStocks);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Stocks;
            Assert.IsTrue(collectionShim.HasExtraStocks);
        }
        [Test]
        public void AssetCollection_HasMoreThan4NormalAssets()
        {
            var collectionShim = InitializeCollection(null);
            Assert.IsFalse(collectionShim.HasMoreThan4NormalAssets);

            var newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Checking;
            Assert.IsFalse(collectionShim.HasMoreThan4NormalAssets);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Checking;
            Assert.IsFalse(collectionShim.HasMoreThan4NormalAssets);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Checking;
            Assert.IsFalse(collectionShim.HasMoreThan4NormalAssets);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Checking;
            Assert.IsTrue(collectionShim.HasMoreThan4NormalAssets);

            newRecord = collectionShim.AddRegularRecord();
            newRecord.AssetT = E_AssetRegularT.Checking;
            Assert.IsTrue(collectionShim.HasMoreThan4NormalAssets);
        }
        [Test]
        public void AssetCollection_OtherAssetTotal_rep()
        {
            var collectionShim = InitializeCollection(null);

            Assert.AreEqual("$0.00", collectionShim.OtherAssetTotal_rep);

            var record = collectionShim.AddRegularRecord();
            record.AssetT = E_AssetRegularT.OtherIlliquidAsset;
            record.Val = 100.00m;

            Assert.AreEqual("$100.00", collectionShim.OtherAssetTotal_rep);
        }
        [Test]
        public void AssetCollection_StockTotal_rep()
        {
            var collectionShim = InitializeCollection(null);

            Assert.AreEqual("$18,520.11", collectionShim.StockTotal_rep);

            var record = collectionShim.AddRegularRecord();
            record.AssetT = E_AssetRegularT.Stocks;
            record.Val = 100.00m;

            Assert.AreEqual("$18,620.11", collectionShim.StockTotal_rep);
        }

        [Test]
        public void AssetCollection_AddTooManySpecials_Throws()
        {
            var collectionShim = InitializeCollection(null, true);
            collectionShim.AddRecord(E_AssetT.CashDeposit);
            Assert.Throws<CBaseException>(() => collectionShim.AddRecord(E_AssetT.CashDeposit));
        }

        public static IAssetCollection InitializeCollection(LosConvert converter, bool includeSpecials = true)
        {
            converter = converter ?? new LosConvert();

            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("44444444-4444-4444-4444-444444444444"));
            loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(appId, borrowerId, coborrowerId));

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var collectionContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Assets));
            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerAssets));

            collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());

            var did0 = collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.Auto, "Auto"));
            var did1 = collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.TrustFunds, "TrustFunds"));
            var did2 = collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.Bonds, "Bonds"));

            var did3 = includeSpecials ?
                collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.Retirement, "Retirement"))
                : collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.MoneyMarketFund, "MoneyMarketFund"));

            var did4 = includeSpecials ?
                collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.LifeInsurance, "LifeInsurance"))
                : collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.MutualFunds, "MutualFunds"));

            var did5 = includeSpecials ?
                collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.CashDeposit, "CashDeposit"))
                : collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.PendingNetSaleProceedsFromRealEstateAssets, "PendingNetSaleProceedsFromRealEstateAssets"));

            var did6 = includeSpecials ?
                collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.Business, "Business"))
                : collectionContainer.Add(borrowerId, CreateAsset(E_AssetT.Savings, "Savings"));

            id0 = did0.Value;
            id1 = did1.Value;
            id2 = did2.Value;
            id3 = did3.Value;
            id4 = did4.Value;
            id5 = did5.Value;
            id6 = did6.Value;

            var loanSubstitute = Substitute.For<IShimLoanDataProvider>();
            var appSubstitute = Substitute.For<IShimAppDataProvider>();
            appSubstitute.aBNm.Returns("Janice");

            var ownershipManager = new ShimOwnershipManager<DataObjectKind.Asset, DataObjectKind.ConsumerAssetAssociation, Asset, ConsumerAssetAssociation>(
                collectionContainer as IShimContainer,
                borrowerId,
                coborrowerId,
                appId,
                (consumerId, recordId, AppId) => new ConsumerAssetAssociation(consumerId, recordId, AppId));

            return new AssetCollectionShim(loanSubstitute, appSubstitute, collectionContainer as IShimContainer, ownershipManager, converter);
        }

        private static Asset CreateAsset(E_AssetT? assetType, string AccountName)
        {
            var asset = new Asset();
            asset.AccountName = DescriptionField.Create(AccountName);
            asset.AccountNum = BankAccountNumber.Create("002792-82");
            asset.AssetCashDepositType = E_AssetCashDepositT.CashDeposit1;
            asset.AssetType = assetType ?? E_AssetT.Savings;
            asset.Attention = EntityName.Create("Billy Banker");
            asset.City = City.Create("Costa Mesa");
            asset.CompanyName = EntityName.Create("Wells Fargo");
            asset.DepartmentName = DescriptionField.Create("Accounts");
            asset.Desc = DescriptionField.Create("Wells Fargo Savings Acct.");
            asset.FaceValue = Money.Create(190493.78m);
            asset.GiftSourceData = E_GiftFundSourceT.Blank;
            asset.IsEmptyCreated = false;
            asset.IsSeeAttachment = false;
            asset.OtherTypeDesc = DescriptionField.Create("None.");
            asset.Phone = PhoneNumber.Create("(888) 555-5555");
            asset.PrepDate = UnzonedDate.Create("11/12/16");
            asset.State = UnitedStatesPostalState.Create("CA");
            asset.StreetAddress = StreetAddress.Create("123 Bank Blvd.");
            asset.Value = Money.Create(18520.11m);
            asset.VerifExpiresDate = UnzonedDate.Create("11/13/16");
            asset.VerifRecvDate = UnzonedDate.Create("11/14/16");
            asset.VerifReorderedDate = UnzonedDate.Create("11/15/16");
            asset.VerifSentDate = UnzonedDate.Create("11/16/16");
            asset.Zip = Zipcode.Create("92626");
            return asset;
        }
    }
}
