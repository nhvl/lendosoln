﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using global::DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class OwnershipCastTest
    {
        [Test]
        public void Liabilities()
        {
            Assert.AreEqual(Ownership.Borrower, (Ownership)E_LiaOwnerT.Borrower);
            Assert.AreEqual(Ownership.Coborrower, (Ownership)E_LiaOwnerT.CoBorrower);
            Assert.AreEqual(Ownership.Joint, (Ownership)E_LiaOwnerT.Joint);

            Assert.AreEqual(E_LiaOwnerT.Borrower, (E_LiaOwnerT)Ownership.Borrower);
            Assert.AreEqual(E_LiaOwnerT.CoBorrower, (E_LiaOwnerT)Ownership.Coborrower);
            Assert.AreEqual(E_LiaOwnerT.Joint, (E_LiaOwnerT)Ownership.Joint);

            Assert.AreEqual(3, Enum.GetValues(typeof(E_LiaOwnerT)).Length);
        }

        [Test]
        public void Assets()
        {
            Assert.AreEqual(Ownership.Borrower, (Ownership)E_AssetOwnerT.Borrower);
            Assert.AreEqual(Ownership.Coborrower, (Ownership)E_AssetOwnerT.CoBorrower);
            Assert.AreEqual(Ownership.Joint, (Ownership)E_AssetOwnerT.Joint);

            Assert.AreEqual(E_AssetOwnerT.Borrower, (E_AssetOwnerT)Ownership.Borrower);
            Assert.AreEqual(E_AssetOwnerT.CoBorrower, (E_AssetOwnerT)Ownership.Coborrower);
            Assert.AreEqual(E_AssetOwnerT.Joint, (E_AssetOwnerT)Ownership.Joint);

            Assert.AreEqual(3, Enum.GetValues(typeof(E_AssetOwnerT)).Length);
        }

        [Test]
        public void PublicRecords()
        {
            Assert.AreEqual(Ownership.Borrower, (Ownership)E_PublicRecordOwnerT.Borrower);
            Assert.AreEqual(Ownership.Coborrower, (Ownership)E_PublicRecordOwnerT.CoBorrower);
            Assert.AreEqual(Ownership.Joint, (Ownership)E_PublicRecordOwnerT.Joint);

            Assert.AreEqual(E_PublicRecordOwnerT.Borrower, (E_PublicRecordOwnerT)Ownership.Borrower);
            Assert.AreEqual(E_PublicRecordOwnerT.CoBorrower, (E_PublicRecordOwnerT)Ownership.Coborrower);
            Assert.AreEqual(E_PublicRecordOwnerT.Joint, (E_PublicRecordOwnerT)Ownership.Joint);

            Assert.AreEqual(3, Enum.GetValues(typeof(E_PublicRecordOwnerT)).Length);
        }

        [Test]
        public void RealProperties()
        {
            Assert.AreEqual(Ownership.Borrower, (Ownership)E_ReOwnerT.Borrower);
            Assert.AreEqual(Ownership.Coborrower, (Ownership)E_ReOwnerT.CoBorrower);
            Assert.AreEqual(Ownership.Joint, (Ownership)E_ReOwnerT.Joint);

            Assert.AreEqual(E_ReOwnerT.Borrower, (E_ReOwnerT)Ownership.Borrower);
            Assert.AreEqual(E_ReOwnerT.CoBorrower, (E_ReOwnerT)Ownership.Coborrower);
            Assert.AreEqual(E_ReOwnerT.Joint, (E_ReOwnerT)Ownership.Joint);

            Assert.AreEqual(3, Enum.GetValues(typeof(E_ReOwnerT)).Length);
        }
    }
}
