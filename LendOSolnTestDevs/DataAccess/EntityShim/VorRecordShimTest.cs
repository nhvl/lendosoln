﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class VorRecordShimTest
    {
        private DataSet data;
        private global::DataAccess.IVerificationOfRent shim;

        private IEnumerable FormatTargetEnumerable()
        {
            return RangeLimits.FormatTarget.Enumerable();
        }

        private IEnumerable VorTypeEnumerable()
        {
            return RangeLimits.VorType.Enumberable();
        }

        [Test]
        public void AccountNameTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.AccountName));
            this.AccountNameTest(this.shim, old, old.AccountName);

            this.CreateVorRecord(converter);
            this.AccountNameTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.AccountNameTest(this.shim, old, null);
        }

        [Test]
        public void AddressForTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.AddressFor));
            this.AddressForTest(this.shim, old, old.AddressFor);

            this.CreateVorRecord(converter);
            this.AddressForTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.AddressForTest(this.shim, old, null);
        }

        [Test]
        public void AddressFor_SingleLine_Test()
        {
            this.CreateVorRecord(null);
            string address = this.shim.AddressFor;
            string city = this.shim.CityFor;
            string state = this.shim.StateFor;
            string zip = this.shim.ZipFor;
            string line = this.shim.AddressFor_SingleLine;

            Assert.IsTrue(address.Length > 0);
            Assert.IsTrue(city.Length > 0);
            Assert.IsTrue(state.Length > 0);
            Assert.IsTrue(zip.Length > 0);
            Assert.IsTrue(line.Length > 0);

            Assert.IsTrue(line.IndexOf(address) >= 0);
            Assert.IsTrue(line.IndexOf(city) >= 0);
            Assert.IsTrue(line.IndexOf(state) >= 0);
            Assert.IsTrue(line.IndexOf(zip) >= 0);

            using (var reader = new System.IO.StringReader(line))
            {
                int count = 0;
                var read = reader.ReadLine();
                while (read != null)
                {
                    count++;
                    read = reader.ReadLine();
                }

                Assert.AreEqual(1, count);
            }
        }

        [Test]
        public void AddressToTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.AddressTo));
            this.AddressToTest(this.shim, old, old.AddressTo);

            this.CreateVorRecord(converter);
            this.AddressToTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.AddressToTest(this.shim, old, null);
        }

        [Test]
        public void AttentionTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.Attention));
            this.AttentionTest(this.shim, old, old.Attention);

            this.CreateVorRecord(converter);
            this.AttentionTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.AttentionTest(this.shim, old, null);
        }

        [Test]
        public void CityForTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.CityFor));
            this.CityForTest(this.shim, old, old.CityFor);

            this.CreateVorRecord(converter);
            this.CityForTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.CityForTest(this.shim, old, null);
        }

        [Test]
        public void CityToTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.CityTo));
            this.CityToTest(this.shim, old, old.CityTo);

            this.CreateVorRecord(converter);
            this.CityToTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.CityToTest(this.shim, old, null);
        }

        [Test]
        public void DescriptionTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.Description));
            this.DescriptionTest(this.shim, old, old.Description);

            this.CreateVorRecord(converter);
            this.DescriptionTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.DescriptionTest(this.shim, old, null);
        }

        [Test]
        public void FaxToTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.FaxTo));
            this.FaxToTest(this.shim, old, old.FaxTo);

            this.CreateVorRecord(converter);
            this.FaxToTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.FaxToTest(this.shim, old, null);
        }

        [Test]
        public void IsSeeAttachmentTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.IsSeeAttachment, old.IsSeeAttachment);

            this.shim.IsSeeAttachment = !this.shim.IsSeeAttachment;
            Assert.AreNotEqual(this.shim.IsSeeAttachment, old.IsSeeAttachment);
        }

        [Test]
        public void LandlordCreditorNameTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.LandlordCreditorName));
            this.LandlordCreditorNameTest(this.shim, old, old.LandlordCreditorName);

            this.CreateVorRecord(converter);
            this.LandlordCreditorNameTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.LandlordCreditorNameTest(this.shim, old, null);
        }

        [Test]
        public void PhoneToTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.PhoneTo));
            this.PhoneToTest(this.shim, old, old.PhoneTo);

            this.CreateVorRecord(converter);
            this.PhoneToTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.PhoneToTest(this.shim, old, null);
        }

        [Test]
        public void PrepDTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.PrepD, old.PrepD);

            var cdate = CDateTime.Create(null);
            Assert.IsFalse(cdate.IsValid);

            this.shim.PrepD = cdate;
            this.SetField("PrepD", cdate.ToString());
            Assert.AreEqual(this.shim.PrepD, old.PrepD);
        }

        [Test]
        public void StateForTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.StateFor));
            this.StateForTest(this.shim, old, old.StateFor);

            this.CreateVorRecord(converter);
            this.StateForTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.StateForTest(this.shim, old, null);
        }

        [Test]
        public void StateToTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.StateTo));
            this.StateToTest(this.shim, old, old.StateTo);

            this.CreateVorRecord(converter);
            this.StateToTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.StateToTest(this.shim, old, null);
        }

        [Test]
        public void VerifExpDTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifExpD, old.VerifExpD);

            var cdate = CDateTime.Create(null);
            Assert.IsFalse(cdate.IsValid);

            this.shim.VerifExpD = cdate;
            this.SetField("VerifExpD", cdate.ToString());
            Assert.AreEqual(this.shim.VerifExpD, old.VerifExpD);
        }

        [Test]
        public void VerifExpDRepTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.VerifExpD_rep));
            this.VerifExpDRepTest(this.shim, old, old.VerifExpD_rep);

            this.CreateVorRecord(converter);
            this.VerifExpDRepTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.VerifExpDRepTest(this.shim, old, null);
        }

        [Test]
        public void VerifHasSignatureTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifHasSignature, old.VerifHasSignature);
        }

        [Test]
        public void VerifRecvDTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifRecvD, old.VerifRecvD);

            var cdate = CDateTime.Create(null);
            Assert.IsFalse(cdate.IsValid);

            this.shim.VerifRecvD = cdate;
            this.SetField("VerifRecvD", cdate.ToString());
            Assert.AreEqual(this.shim.VerifRecvD, old.VerifRecvD);
        }

        [Test]
        public void VerifRecvDRepTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.VerifRecvD_rep));
            this.VerifRecvDRepTest(this.shim, old, old.VerifRecvD_rep);

            this.CreateVorRecord(converter);
            this.VerifRecvDRepTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.VerifRecvDRepTest(this.shim, old, null);
        }

        [Test]
        public void VerifReorderedDTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifReorderedD, old.VerifReorderedD);

            var cdate = CDateTime.Create(null);
            Assert.IsFalse(cdate.IsValid);

            this.shim.VerifReorderedD = cdate;
            this.SetField("VerifReorderedD", cdate.ToString());
            Assert.AreEqual(this.shim.VerifReorderedD, old.VerifReorderedD);
        }

        [Test]
        public void VerifReorderedDRepTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.VerifReorderedD_rep));
            this.VerifReorderedDRepTest(this.shim, old, old.VerifReorderedD_rep);

            this.CreateVorRecord(converter);
            this.VerifReorderedDRepTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.VerifReorderedDRepTest(this.shim, old, null);
        }

        [Test]
        public void VerifSentDTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifSentD, old.VerifSentD);

            var cdate = CDateTime.Create(null);
            Assert.IsFalse(cdate.IsValid);

            this.shim.VerifSentD = cdate;
            this.SetField("VerifSentD", cdate.ToString());
            Assert.AreEqual(this.shim.VerifSentD, old.VerifSentD);
        }

        [Test]
        public void VerifSentDRepTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.VerifSentD_rep));
            this.VerifSentDRepTest(this.shim, old, old.VerifSentD_rep);

            this.CreateVorRecord(converter);
            this.VerifSentDRepTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.VerifSentDRepTest(this.shim, old, null);
        }

        [Test]
        public void VerifSignatureImgIdTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifSignatureImgId, old.VerifSignatureImgId);
        }

        [Test]
        public void VerifSigningEmployeeIdTest()
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifSigningEmployeeId, old.VerifSigningEmployeeId);
        }

        [Test]
        public void VerifTTest([ValueSource("VorTypeEnumerable")] int vorType)
        {
            this.CreateVorRecord(null);

            var old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifT, old.VerifT);

            this.shim.VerifT = (E_VorT)vorType;
            this.SetField("VerifT", vorType.ToString());

            old = new CVorFields(this.data, null);
            Assert.AreEqual(this.shim.VerifT, old.VerifT);
        }

        [Test]
        public void ZipForTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.ZipFor));
            this.ZipForTest(this.shim, old, old.ZipFor);

            this.CreateVorRecord(converter);
            this.ZipForTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.ZipForTest(this.shim, old, null);
        }

        [Test]
        public void ZipToTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreateVorRecord(converter);

            var old = new CVorFields(this.data, converter);
            Assert.IsFalse(string.IsNullOrEmpty(old.ZipTo));
            this.ZipToTest(this.shim, old, old.ZipTo);

            this.CreateVorRecord(converter);
            this.ZipToTest(this.shim, old, string.Empty);

            this.CreateVorRecord(converter);
            this.ZipToTest(this.shim, old, null);
        }

        [Test]
        public void ApplySignatureTest()
        {
            this.CreateVorRecord(null);
            var old = new CVorFields(this.data, null);

            Assert.IsTrue(this.shim.VerifHasSignature);
            Assert.IsFalse(string.IsNullOrEmpty(this.shim.VerifSignatureImgId));
            Assert.AreEqual(this.shim.VerifHasSignature, old.VerifHasSignature);
            Assert.AreEqual(this.shim.VerifSignatureImgId, old.VerifSignatureImgId);
            Assert.AreEqual(this.shim.VerifSigningEmployeeId, old.VerifSigningEmployeeId);

            var empId = Guid.NewGuid();
            var imgId = Guid.NewGuid().ToString("D");
            this.shim.ApplySignature(empId, imgId);
            this.SetField("VerifSignatureImgId", imgId);
            this.SetField("VerifSigningEmployeeId", empId.ToString("D"));

            Assert.IsTrue(this.shim.VerifHasSignature);
            Assert.AreEqual(imgId, this.shim.VerifSignatureImgId);
            Assert.AreEqual(empId, this.shim.VerifSigningEmployeeId);
            Assert.AreEqual(this.shim.VerifHasSignature, old.VerifHasSignature);
            Assert.AreEqual(this.shim.VerifSignatureImgId, old.VerifSignatureImgId);
            Assert.AreEqual(this.shim.VerifSigningEmployeeId, old.VerifSigningEmployeeId);
        }

        [Test]
        public void ClearSignatureTest()
        {
            this.CreateVorRecord(null);
            var old = new CVorFields(this.data, null);

            Assert.IsTrue(this.shim.VerifHasSignature);
            Assert.IsFalse(string.IsNullOrEmpty(this.shim.VerifSignatureImgId));
            Assert.AreEqual(this.shim.VerifHasSignature, old.VerifHasSignature);
            Assert.AreEqual(this.shim.VerifSignatureImgId, old.VerifSignatureImgId);
            Assert.AreEqual(this.shim.VerifSigningEmployeeId, old.VerifSigningEmployeeId);

            this.shim.ClearSignature();
            this.SetField("VerifSignatureImgId", string.Empty);
            this.SetField("VerifSigningEmployeeId", string.Empty);

            Assert.IsFalse(this.shim.VerifHasSignature);
            Assert.AreEqual(string.Empty, this.shim.VerifSignatureImgId);
            Assert.AreEqual(Guid.Empty, this.shim.VerifSigningEmployeeId);
            Assert.AreEqual(this.shim.VerifHasSignature, old.VerifHasSignature);
            Assert.AreEqual(this.shim.VerifSignatureImgId, old.VerifSignatureImgId);
            Assert.AreEqual(this.shim.VerifSigningEmployeeId, old.VerifSigningEmployeeId);
        }

        [Test]
        public void Constructor_SpecialRecordTypeNotNull_SetsVerifTToRental()
        {
            var entity = new VorRecord();
            entity.VerifType = E_VorT.Other;
            entity.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPrevious1);

            var shim = VorRecordShim.Create(null, entity, Guid.NewGuid().ToIdentifier<DataObjectKind.VorRecord>(), null);

            Assert.AreEqual(E_VorT.Rental, entity.VerifType);
        }

        private static readonly IEnumerable<VorSpecialRecordType> SpecialRecordTypes = VorRecord.LegacyRecordIdToSpecialType.Values;

        [Test]
        public void Constructor_SpecialRecordTypeNotNull_SetsRecordIdToLegacyHardCodedId(
            [ValueSource(nameof(SpecialRecordTypes))]VorSpecialRecordType specialType)
        {
            var entity = new VorRecord();
            entity.SetLegacyRecordSpecialType(specialType);

            var shim = VorRecordShim.Create(null, entity, Guid.NewGuid().ToIdentifier<DataObjectKind.VorRecord>(), null);

            var expectedRecordId = VorRecord.LegacyRecordIdToSpecialType
                .Single(kvp => kvp.Value == specialType)
                .Key;
            Assert.AreEqual(expectedRecordId, shim.RecordId);
        }

        private void ZipToTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.ZipTo = value;
            old.ZipTo = value;
            Assert.AreEqual(old.ZipTo, shim.ZipTo);
        }

        private void ZipForTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.ZipFor = value;
            old.ZipFor = value;
            Assert.AreEqual(old.ZipFor, shim.ZipFor);
        }

        private void VerifSentDRepTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.VerifSentD_rep = value;
            old.VerifSentD_rep = value;
            Assert.AreEqual(old.VerifSentD_rep, shim.VerifSentD_rep);
        }


        private void VerifReorderedDRepTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.VerifReorderedD_rep = value;
            old.VerifReorderedD_rep = value;
            Assert.AreEqual(old.VerifReorderedD_rep, shim.VerifReorderedD_rep);
        }

        private void VerifRecvDRepTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.VerifRecvD_rep = value;
            old.VerifRecvD_rep = value;
            Assert.AreEqual(old.VerifRecvD_rep, shim.VerifRecvD_rep);
        }

        private void VerifExpDRepTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.VerifExpD_rep = value;
            old.VerifExpD_rep = value;
            Assert.AreEqual(old.VerifExpD_rep, shim.VerifExpD_rep);
        }

        private void StateToTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.StateTo = value;
            old.StateTo = value;
            Assert.AreEqual(old.StateTo, shim.StateTo);
        }

        private void StateForTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.StateFor = value;
            old.StateFor = value;
            Assert.AreEqual(old.StateFor, shim.StateFor);
        }

        private void PhoneToTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.PhoneTo = value;
            old.PhoneTo = value;
            Assert.AreEqual(old.PhoneTo, shim.PhoneTo);
        }

        private void LandlordCreditorNameTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.LandlordCreditorName = value;
            old.LandlordCreditorName = value;
            Assert.AreEqual(old.LandlordCreditorName, shim.LandlordCreditorName);
        }

        private void FaxToTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.FaxTo = value;
            old.FaxTo = value;
            Assert.AreEqual(old.FaxTo, shim.FaxTo);
        }

        private void DescriptionTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.Description = value;
            old.Description = value;
            Assert.AreEqual(old.Description, shim.Description);
        }

        private void CityToTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.CityTo = value;
            old.CityTo = value;
            Assert.AreEqual(old.CityTo, shim.CityTo);
        }

        private void CityForTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.CityFor = value;
            old.CityFor = value;
            Assert.AreEqual(old.CityFor, shim.CityFor);
        }

        private void AttentionTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.Attention = value;
            old.Attention = value;
            Assert.AreEqual(old.Attention, shim.Attention);
        }

        private void AddressToTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.AddressTo = value;
            old.AddressTo = value;
            Assert.AreEqual(old.AddressTo, shim.AddressTo);
        }

        private void AddressForTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.AddressFor = value;
            old.AddressFor = value;
            Assert.AreEqual(old.AddressFor, shim.AddressFor);
        }

        private void AccountNameTest(global::DataAccess.IVerificationOfRent shim, global::DataAccess.IVerificationOfRent old, string value)
        {
            shim.AccountName = value;
            old.AccountName = value;
            Assert.AreEqual(old.AccountName, shim.AccountName);
        }

        private void CreateVorRecord(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var tuple = VorRecordCollectionShimTest.InitializeCollection(converter);
            this.shim = tuple.Item1.GetRecordByIndex(0);
            this.data = tuple.Item2;
        }

        private void SetField(string name, string value)
        {
            // Since the new VOR shim is detached from the DataSet (because the interface doesn't present it to client code),
            // Modifying the VOR shim does not auto-update the DataSet held by the original VOR class.  To test consistency
            // after updating a VOR shim property, the corresponding DataSet cell must be manually adjusted, done here.
            var table = this.data.Tables[0];
            var row = table.Rows[0];
            row[name] = value;
        }
    }
}
