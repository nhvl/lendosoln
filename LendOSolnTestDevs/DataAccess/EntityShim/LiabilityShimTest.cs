﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using NSubstitute;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using Utils;
    using global::LendingQB.Core.Data;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityShimTest
    {
        private static readonly IEnumerable<FormatTarget> AllFormatTargets = Enum.GetValues(typeof(FormatTarget)).Cast<FormatTarget>();

        private static readonly IEnumerable<E_LiaOwnerT> AllOwnershipTypes = Enum.GetValues(typeof(E_LiaOwnerT)).Cast<E_LiaOwnerT>();

        [Test]
        public void DebtT_EntityDebtTypeSet_GetsEntityDebtType()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            entity.DebtType = E_DebtT.JobRelatedExpense;
            var shim = this.GetShim(entity);

            Assert.AreEqual(E_DebtT.JobRelatedExpense, shim.DebtT);
        }

        [Test]
        public void DebtT_EntityDebtTypeNull_DefaultsToRevolving()
        {
            var shim = this.GetShim(new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>()));

            Assert.AreEqual(E_DebtT.Revolving, shim.DebtT);
        }

        [Test]
        public void H4HIsRetirement_Always_ReturnsFalse()
        {
            var shim = this.GetShim(new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>()));

            Assert.AreEqual(false, shim.H4HIsRetirement);
            shim.H4HIsRetirement = true;
            Assert.AreEqual(false, shim.H4HIsRetirement);
        }

        [Test]
        public void KeyType_Always_ReturnsDebtTypeOfEntity()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            entity.DebtType = E_DebtT.Mortgage;
            var shim = this.GetShim(entity);

            Assert.AreEqual(E_DebtT.Mortgage, shim.KeyType);
        }

        [Test]
        public void KeyType_SetToInitLoad_SetsEntityDebtTypeToInstallment()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            var shim = this.GetShim(entity);

            shim.KeyType = E_DataState.InitLoad;

            Assert.AreEqual(E_DebtT.Installment, entity.DebtType);
        }

        [Test]
        public void KeyType_SetToMortgage_SetsEntityDebtTypeToMortgage()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            var shim = this.GetShim(entity);

            shim.KeyType = E_DebtT.Mortgage;

            Assert.AreEqual(E_DebtT.Mortgage, entity.DebtType);
        }

        [Test]
        public void NotUsedInRatio_EntityNotUsedInRatio_ReturnsTrue()
        {
            var entity = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            entity.UsedInRatioData = false;

            var shim = this.GetShim(entity);

            Assert.AreEqual(true, shim.NotUsedInRatio);
        }

        [Test]
        public void NotUsedInRatio_EntityValueNull_ReturnsFalse()
        {
            var shim = this.GetShim(new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>()));

            Assert.AreEqual(false, shim.NotUsedInRatio);
        }

        [Test, Combinatorial]
        public void NotUsedInRatio_VariousInputs_ReturnsMatchingValueForShimAndLegacyLiability(
            [Values(E_DebtRegularT.Open, E_DebtRegularT.Mortgage)] E_DebtRegularT debtType,
            [Values(true, false)] bool notUsedInRatio)
        {
            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(null);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.DebtT = (E_DebtT)debtType;
            shim.NotUsedInRatio = notUsedInRatio;

            liaRegular.DebtT = debtType;
            liaRegular.NotUsedInRatio = notUsedInRatio;

            Assert.AreEqual(liaRegular.NotUsedInRatio, shim.NotUsedInRatio);
        }

        [Test]
        public void NotUsedInRatio_SetValueToTrue_SetsEntityUsedInRatioToFalse()
        {
            var entity = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            var shim = this.GetShim(entity);

            shim.NotUsedInRatio = true;

            Assert.AreEqual(false, entity.UsedInRatioData);
        }

        [Test, Combinatorial]
        public void OwnerTRep_AllOwnershipTypes_ReturnsMatchingValueForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("AllOwnershipTypes")] E_LiaOwnerT ownershipType)
        {
            var converter = new LosConvert(formatTarget);

            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.OwnerT = ownershipType;
            liaRegular.OwnerT = ownershipType;

            Assert.AreEqual(liaRegular.OwnerT_rep, shim.OwnerT_rep);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(@"<History><Event UserName=""Test User"" LoginId=""testuser"" Action=""Field Update"" Field=""Pmt"" Value=""5.00"" EventDate=""5/1/2018 8:53:06 AM PDT"" /><Event UserName=""Test User"" LoginId=""testuser"" Action=""Field Update"" Field=""Used In Ratio"" Value=""Yes"" EventDate=""5/1/2018 8:53:06 AM PDT"" /><Event UserName=""Test User"" LoginId=""testuser"" Action=""Field Update"" Field=""Pd Off"" Value=""No"" EventDate=""5/1/2018 8:53:06 AM PDT"" /></History>")]
        public void PmlAuditTrailXmlContent_VariousValues_ReturnsMatchingValueForShimAndLegacyLiability(string xmlContent)
        {
            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(null);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.PmlAuditTrailXmlContent = xmlContent;
            liaRegular.PmlAuditTrailXmlContent = xmlContent;

            Assert.AreEqual(liaRegular.PmlAuditTrailXmlContent, shim.PmlAuditTrailXmlContent);
        }

        [Test]
        public void Pmt_EntityValueNull_Returns0()
        {
            var shim = this.GetShim(new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>()));

            Assert.AreEqual(0, shim.Pmt);
        }

        [Test]
        public void Pmt_EntityValue500_Returns500()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            entity.Pmt = Money.Create(500);

            var shim = this.GetShim(entity);

            Assert.AreEqual(500, shim.Pmt);
        }

        [Test]
        public void Pmt_SetValueTo200_SetsEntityValueTo200()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            var shim = this.GetShim(entity);

            shim.Pmt = 200;

            Assert.AreEqual(Money.Create(200), entity.Pmt);
        }

        private static readonly decimal[] PaymentRepDecimalValues = new[] { 0m, 1.23m, 45678.90m };

        [Test, Combinatorial]
        public void PmtRep_SetPmtToVariousValues_ReturnsMatchingValueForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget, 
            [ValueSource("PaymentRepDecimalValues")] decimal payment)
        {
            var converter = new LosConvert(formatTarget);

            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.Pmt = payment;
            liaRegular.Pmt = payment;

            Assert.AreEqual(liaRegular.Pmt_rep, shim.Pmt_rep);
        }

        private static readonly string[] PaymentRepStringValues = new[] { null, "", "0", "$1.25", "1.25", "$12,345.67", "123,456,789.01" };

        [Test, Combinatorial]
        public void PmtRep_SetPmtRepToVariousValues_ReturnsMatchingValueForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget, 
            [ValueSource("PaymentRepStringValues")] string payment)
        {
            var converter = new LosConvert(formatTarget);

            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.Pmt_rep = payment;
            liaRegular.Pmt_rep = payment;

            Assert.AreEqual(liaRegular.Pmt_rep, shim.Pmt_rep);
        }

        [Test, Combinatorial]
        public void PmtRepPoint_VariousTestInputs_ReturnsMatchingValueForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget, 
            [Values(E_DebtRegularT.Open, E_DebtRegularT.Mortgage)] E_DebtRegularT debtType,
            [Values(0, 1.23, 123.45, 1234.56)] decimal payment,
            [Values(true, false)] bool notUsedInRatio)
        {
            var converter = new LosConvert(formatTarget);

            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.DebtT = (E_DebtT)debtType;
            shim.Pmt = payment;
            shim.NotUsedInRatio = notUsedInRatio;

            liaRegular.DebtT = debtType;
            liaRegular.Pmt = payment;
            liaRegular.NotUsedInRatio = notUsedInRatio;

            Assert.AreEqual(liaRegular.Pmt_repPoint, shim.Pmt_repPoint);
        }

        [Test, Combinatorial]
        public void PmtRepPoint_SetVariousValues_HasSameSideEffectsForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget, 
            [Values(E_DebtRegularT.Open, E_DebtRegularT.Mortgage)] E_DebtRegularT debtType,
            [Values(true, false)] bool originalNotUsedInRatioValue,
            [Values(null, "0", "(0)", "1,234.56", "(1,234.56)", "($1,234.56)")] string repValueToSet)
        {
            var converter = new LosConvert(formatTarget);

            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.DebtT = (E_DebtT)debtType;
            shim.NotUsedInRatio = originalNotUsedInRatioValue;
            if (repValueToSet == null)
            {
                Assert.Throws<NullReferenceException>(() => shim.Pmt_repPoint = repValueToSet);
            }
            else
            {
                shim.Pmt_repPoint = repValueToSet;
            }

            liaRegular.DebtT = debtType;
            liaRegular.NotUsedInRatio = originalNotUsedInRatioValue;
            if (repValueToSet == null)
            {
                Assert.Throws<NullReferenceException>(() => liaRegular.Pmt_repPoint = repValueToSet);
            }
            else
            {
                liaRegular.Pmt_repPoint = repValueToSet;
            }

            Assert.AreEqual(liaRegular.Pmt_repPoint, shim.Pmt_repPoint);
            Assert.AreEqual(liaRegular.Pmt_rep, shim.Pmt_rep);
            Assert.AreEqual(liaRegular.NotUsedInRatio, shim.NotUsedInRatio);
        }

        [Test, Combinatorial]
        public void PmtRemainMonsRep1003_SetVariousValues_ReturnsMatchingValueForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [Values(true, false)] bool willBePdOff)
        {
            var converter = new LosConvert(formatTarget);

            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.Pmt_repPoint = "1500";
            shim.RemainMons_rep = "48";
            shim.WillBePdOff = willBePdOff;

            liaRegular.Pmt_repPoint = "1500";
            liaRegular.RemainMons_rep = "48";
            liaRegular.WillBePdOff = willBePdOff;

            Assert.AreEqual(liaRegular.PmtRemainMons_rep1003, shim.PmtRemainMons_rep1003);
        }

        [Test, Combinatorial]
        public void RemainMonsRep_SetVariousValues_ReturnsSameValueForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [Values(null, "", "0", "4", "4.5", "WORD")] string repValueToSet)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Logger);

                var converter = new LosConvert(formatTarget);

                Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
                var shim = liabilities.Item1;
                var liaRegular = liabilities.Item2;

                shim.RemainMons_rep = repValueToSet;
                liaRegular.RemainMons_rep = repValueToSet;

                Assert.AreEqual(liaRegular.RemainMons_rep, shim.RemainMons_rep);
            }
        }

        [Test, Combinatorial]
        public void RemainMonsRepRaw_SetVariousValues_ReturnsSameValueForShimAndLegacyLiability(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [Values(null, "", "0", "4", "4.5", "WORD")] string repValueToSet)
        {
            var converter = new LosConvert(formatTarget);

            Tuple<LiabilityShim, CLiaRegular> liabilities = this.GetShimAndLegacyLiability(converter);
            var shim = liabilities.Item1;
            var liaRegular = liabilities.Item2;

            shim.RemainMons_rep = repValueToSet;
            liaRegular.RemainMons_rep = repValueToSet;

            Assert.AreEqual(liaRegular.RemainMons_raw, shim.RemainMons_raw);
        }

        [Test]
        public void WillBePdOff_EntityValueTrue_ReturnsTrue()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            entity.WillBePdOff = true;

            var shim = this.GetShim(entity);

            Assert.AreEqual(true, shim.WillBePdOff);
        }

        [Test]
        public void WillBePdOff_EntityValueNull_ReturnsFalse()
        {
            var shim = this.GetShim(new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>()));

            Assert.AreEqual(false, shim.WillBePdOff);
        }

        [Test]
        public void WillBePdOff_SetValueToTrue_SetsEntityValueToTrue()
        {
            var entity = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            var shim = this.GetShim(entity);

            shim.WillBePdOff = true;

            Assert.AreEqual(true, entity.WillBePdOff);
        }

        [Test]
        public void OwnerT_Get_RetrievesValueFromShimContainer()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var defaultsProviderFake = Substitute.For<ILiabilityDefaultsProvider>();
            var liability = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(Guid.Empty).Returns(Ownership.Coborrower);
            var id = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty);
            var shim = LiabilityRegularShim.Create(shimContainer, recordCollection, liability, defaultsProviderFake, ownershipManager, id, new LosConvert());

            var ownershipType = shim.OwnerT;

            Assert.AreEqual(E_LiaOwnerT.CoBorrower, ownershipType);
        }

        [Test]
        public void OwnerT_Set_CallsShimContainerMethod()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var defaultsProviderFake = Substitute.For<ILiabilityDefaultsProvider>();
            var liability = new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>());
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            var id = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty);
            var shim = LiabilityRegularShim.Create(
                shimContainer,
                recordCollection,
                liability,
                defaultsProviderFake,
                ownershipManager,
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty),
                new LosConvert());

            shim.OwnerT = E_LiaOwnerT.Joint;

            ownershipManager.Received().SetOwnership(Guid.Empty, (Ownership)E_LiaOwnerT.Joint);
        }

        private Tuple<LiabilityShim, CLiaRegular> GetShimAndLegacyLiability(LosConvert converter)
        {
            converter = converter ?? new LosConvert();

            var defaultsProviderFake = Substitute.For<ILiabilityDefaultsProvider>();
            var recordId = Guid.Empty;
            var entity = new Liability(defaultsProviderFake);

            var shim = LiabilityRegularShim.Create(
                null,
                null,
                entity,
                defaultsProviderFake,
                new FakeOwnershipManager(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(recordId),
                converter);

            var collectionShim = LiabilityCollectionShimTest.InitializeCollection(converter, entities: new[] { new KeyValuePair<Guid, Liability>(recordId, entity) });
            var dataSet = collectionShim.GetDataSet();
            var liaRegular = new CLiaRegular(dataSet, converter, recordId);

            return Tuple.Create((LiabilityShim)shim, liaRegular);
        }

        private global::DataAccess.ILiability GetShim(global::LendingQB.Core.Data.Liability entity, LosConvert converter = null)
        {
            converter = converter ?? new LosConvert();

            return LiabilityRegularShim.Create(
                null,
                null,
                entity,
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimOwnershipManager<Guid>>(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty),
                converter);
        }

        private class FakeOwnershipManager : IShimOwnershipManager<Guid>
        {
            private Dictionary<Guid, Ownership> ownershipById = new Dictionary<Guid, Ownership>();

            public DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId => DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.Empty);

            public DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId => DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);

            public DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId => DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);

            public DataObjectIdentifier<DataObjectKind.Consumer, Guid> DefaultOwner => BorrowerId;

            public Ownership GetOwnership(Guid recordId)
            {
                return this.ownershipById[recordId];
            }

            public void SetOwnership(Guid recordId, Ownership ownership)
            {
                ownershipById[recordId] = ownership;
            }

            public void SwapBorrowers()
            {
                foreach (var key in this.ownershipById.Keys)
                {
                    switch (this.ownershipById[key])
                    {
                        case Ownership.Borrower:
                            this.ownershipById[key] = Ownership.Coborrower;
                            break;
                        case Ownership.Coborrower:
                            this.ownershipById[key] = Ownership.Borrower;
                            break;
                        case Ownership.Joint:
                            // do nothing
                            break;
                    }
                }
            }
        }
    }
}
