﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class VorRecordCollectionShimTest
    {
        private static Guid id0;
        private static Guid id1;
        private static Guid id2;
        private static Guid id3;
        private static Guid id4;

        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("33333333-3333-3333-3333-333333333333"));

        [Test]
        public void ClearTest()
        {
            var collection = InitializeCollection(null).Item1;

            collection.Clear();
            Assert.AreEqual(0, collection.Count());
        }

        [Test]
        public void CountTest()
        {
            var collection = InitializeCollection(null).Item1;
            Assert.AreEqual(5, collection.Count());
        }

        [Test]
        public void DeleteByRecordIdTest()
        {
            var collection = InitializeCollection(null).Item1;

            collection.DeleteByRecordId(id3);
            Assert.AreEqual(4, collection.Count());

            for (int i = 0; i < collection.Count(); ++i)
            {
                var item = collection.GetRecordByIndex(i);
                switch (i)
                {
                    case 0:
                        Assert.AreEqual(id0, item.RecordId);
                        break;
                    case 1:
                        Assert.AreEqual(id1, item.RecordId);
                        break;
                    case 2:
                        Assert.AreEqual(id2, item.RecordId);
                        break;
                    case 3:
                        Assert.AreEqual(id4, item.RecordId);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
            }
        }

        [Test]
        public void GetRecordByIndexTest()
        {
            var collection = InitializeCollection(null).Item1;

            for (int i = 0; i < collection.Count(); ++i)
            {
                var item = collection.GetRecordByIndex(i);
                switch (i)
                {
                    case 0:
                        Assert.AreEqual(id0, item.RecordId);
                        break;
                    case 1:
                        Assert.AreEqual(id1, item.RecordId);
                        break;
                    case 2:
                        Assert.AreEqual(id2, item.RecordId);
                        break;
                    case 3:
                        Assert.AreEqual(id3, item.RecordId);
                        break;
                    case 4:
                        Assert.AreEqual(id4, item.RecordId);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
            }
        }

        [Test]
        public void GetRecordByRecordIdTest_Exists()
        {
            var collection = InitializeCollection(null).Item1;
            int count = collection.Count();

            for (int i = 0; i < collection.Count(); ++i)
            {
                switch (i)
                {
                    case 0:
                        {
                            var item = collection.GetRecordByRecordId(id0);
                            Assert.AreEqual(count, collection.Count());
                            Assert.AreEqual(id0, item.RecordId);
                        }
                        break;
                    case 1:
                        {
                            var item = collection.GetRecordByRecordId(id1);
                            Assert.AreEqual(count, collection.Count());
                            Assert.AreEqual(id1, item.RecordId);
                        }
                        break;
                    case 2:
                        {
                            var item = collection.GetRecordByRecordId(id2);
                            Assert.AreEqual(count, collection.Count());
                            Assert.AreEqual(id2, item.RecordId);
                        }
                        break;
                    case 3:
                        {
                            var item = collection.GetRecordByRecordId(id3);
                            Assert.AreEqual(count, collection.Count());
                            Assert.AreEqual(id3, item.RecordId);
                        }
                        break;
                    case 4:
                        {
                            var item = collection.GetRecordByRecordId(id4);
                            Assert.AreEqual(count, collection.Count());
                            Assert.AreEqual(id4, item.RecordId);
                        }
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
            }
        }

        private static readonly IEnumerable<Guid> SpecialRecordIds = VorRecord.LegacyRecordIdToSpecialType.Keys;

        [Test]
        public void GetRecordByRecordId_ContainsEntityWithLegacyRecordId_LooksUpRecordByLegacyRecordId(
            [ValueSource(nameof(SpecialRecordIds))]Guid legacyRecordId)
        {
            var testData = InitializeCollection(null);
            var collectionShim = testData.Item1;
            collectionShim.Clear();
            Assert.AreEqual(0, collectionShim.Count());
            var collectionContainer = testData.Item3;
            var specialRecord = new VorRecord();
            specialRecord.SetLegacyRecordSpecialType(VorRecord.LegacyRecordIdToSpecialType[legacyRecordId]);

            var entityId = collectionContainer.Add(BorrowerId, specialRecord);

            Assert.IsNotNull(collectionShim.GetRecordByRecordId(legacyRecordId));
            Assert.IsFalse(collectionContainer.VorRecords.ContainsKey(legacyRecordId.ToIdentifier<DataObjectKind.VorRecord>()));
            Assert.AreEqual(1, collectionShim.Count());

            // When we attempt to get the record by the entity (not legacy) id, it will attempt
            // to add a new record with the same key which should throw.
            // This is to verify that the collection shim doesn't map both the legacy record id
            // and the entity id to the same record.
            Assert.Throws<ArgumentException>(() => collectionShim.GetRecordByRecordId(entityId.Value));
        }

        [Test]
        public void GetRecordByRecordId_DoesNotContainEntityWithLegacyRecordId_CreatesRecordWithLegacyRecordId(
            [ValueSource(nameof(SpecialRecordIds))]Guid legacyRecordId)
        {
            var testData = InitializeCollection(null);
            var collectionContainer = testData.Item3;
            var collectionShim = testData.Item1;
            collectionShim.Clear();
            Assert.AreEqual(0, collectionShim.Count());

            var recordShim = collectionShim.GetRecordByRecordId(legacyRecordId);

            Assert.AreEqual(1, collectionContainer.VorRecords.Values.Count(v => v.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[legacyRecordId]));
            Assert.IsFalse(collectionContainer.VorRecords.ContainsKey(legacyRecordId.ToIdentifier<DataObjectKind.VorRecord>()));
        }

        [Test]
        public void DeleteByRecordId_ContainsEntityWithLegacyRecordId_RemovesRecord(
            [ValueSource(nameof(SpecialRecordIds))]Guid legacyRecordId)

        {
            var testData = InitializeCollection(null);
            var collectionContainer = testData.Item3;
            var collectionShim = testData.Item1;
            collectionShim.Clear();
            Assert.AreEqual(0, collectionShim.Count());
            collectionShim.GetRecordByRecordId(legacyRecordId);
            Assert.AreEqual(1, collectionShim.Count());

            collectionShim.DeleteByRecordId(legacyRecordId);

            Assert.AreEqual(0, collectionShim.Count());
        }

        [Test]
        public void DeleteByRecordId_DoesNotContainEntityWithLegacyRecordId_DoesNotThrowException(
            [ValueSource(nameof(SpecialRecordIds))]Guid legacyRecordId)

        {
            var testData = InitializeCollection(null);
            var collectionContainer = testData.Item3;
            var collectionShim = testData.Item1;
            collectionShim.Clear();
            Assert.AreEqual(0, collectionShim.Count());

            collectionShim.DeleteByRecordId(legacyRecordId);
        }

        [Test]
        public void GetRecordByRecordIdTest_NotExists()
        {
            var collectionTuple = InitializeCollection(null);
            var collection = collectionTuple.Item1;
            var collectionContainer = collectionTuple.Item3;
            int count = collection.Count();
            int associationCount = collectionContainer.ConsumerVorRecords.Count;

            var guid = Guid.NewGuid();
            var item = collection.GetRecordByRecordId(guid);
            Assert.AreEqual(count + 1, collection.Count());

            Assert.AreEqual(1, collectionContainer.ConsumerVorRecords.Values.Count(a => a.VorRecordId.Value == guid));
            var association = collectionContainer.ConsumerVorRecords.Values.Single(a => a.VorRecordId.Value == guid);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(AppId, association.AppId);


            var pulled = collection.GetRecordByIndex(count);
            Assert.AreEqual(guid, pulled.RecordId);
        }

        [Test]
        public void GetDataSetTest()
        {
            var collection = InitializeCollection(null).Item1;

            var ds = collection.GetDataSet();

            var table = ds.Tables[0];
            Assert.AreEqual(5, table.Rows.Count);
            Assert.AreEqual(id0, Guid.Parse((string)table.Rows[0]["RecordId"]));
            Assert.AreEqual(id1, Guid.Parse((string)table.Rows[1]["RecordId"]));
            Assert.AreEqual(id2, Guid.Parse((string)table.Rows[2]["RecordId"]));
            Assert.AreEqual(id3, Guid.Parse((string)table.Rows[3]["RecordId"]));
            Assert.AreEqual(id4, Guid.Parse((string)table.Rows[4]["RecordId"]));
        }

        [Test]
        public void GetDataSet_AfterUpdatingRecord_IncludesUpdateInDataSet()
        {
            var collection = InitializeCollection(null).Item1;

            collection.GetRecordByIndex(0).AccountName = "Test Name!";
            var ds = collection.GetDataSet();

            var table = ds.Tables[0];
            Assert.AreEqual("Test Name!", table.Rows[0]["AccountName"]);
        }

        public static Tuple<IVerificationOfRentCollection, DataSet, ILoanLqbCollectionContainer> InitializeCollection(LosConvert converter)
        {
            converter = converter ?? new LosConvert();

            var ds = new DataSet();
            var table = VorRecordShim.CreateAndAddDataTable(ds);

            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(AppId, BorrowerId, CoborrowerId));

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var shimContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            shimContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.VorRecords));
            shimContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerVorRecords));

            shimContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());
            var did0 = shimContainer.Add(BorrowerId, CreateVorRecord(0));
            var did1 = shimContainer.Add(BorrowerId, CreateVorRecord(4));
            var did2 = shimContainer.Add(BorrowerId, CreateVorRecord(2));
            var did3 = shimContainer.Add(BorrowerId, CreateVorRecord(1));
            var did4 = shimContainer.Add(BorrowerId, CreateVorRecord(3));

            id0 = did0.Value;
            id1 = did1.Value;
            id2 = did2.Value;
            id3 = did3.Value;
            id4 = did4.Value;

            var collShim = new VorRecordCollectionShim(shimContainer as IShimContainer, AppId, BorrowerId, CoborrowerId, converter);

            for (int i = 0; i < collShim.Count(); ++i)
            {
                var shim = collShim.GetRecordByIndex(i);
                VorRecordShim.CreateAndAddDataRow(table, shim);
            }

            return new Tuple<IVerificationOfRentCollection, DataSet, ILoanLqbCollectionContainer>(collShim, ds, shimContainer);
        }

        private static VorRecord CreateVorRecord(int? flag)
        {
            string accountName = "AccountName" + flag == null ? string.Empty : "_" + flag.Value.ToString();
            string attention = "Jackson Pollack";
            string streetFor = "10" + flag == null ? string.Empty : flag.Value.ToString() + " Cattle Dr";
            string cityFor = "Formosa";
            string stateFor = "FL";
            string zipFor = "13579";
            string streetTo = "10" + flag == null ? string.Empty : flag.Value.ToString() + " Penny Ln";
            string cityTo = "Towanda";
            string stateTo = "NY";
            string zipTo = "33221";
            string faxTo = "315-555-4567";
            string phoneTo = "315-555-45676";
            string description = "DESCRIPTION";
            bool isSeeAttachment = true;
            string landlord = "Slumdog Millionaire";
            DateTime prepDate = new DateTime(2018, 4, 1);
            DateTime expDate = new DateTime(2018, 5, 1);
            DateTime recvDate = new DateTime(2018, 5, 1);
            DateTime reordDate = new DateTime(2018, 5, 1);
            DateTime sentDate = new DateTime(2018, 5, 1);
            E_VorT type = E_VorT.LandContract;
            Guid sigId = Guid.NewGuid();
            Guid signerId = Guid.NewGuid();

            var vr = new VorRecord();
            vr.AccountName = DescriptionField.Create(accountName);
            vr.Attention = EntityName.Create(attention);
            vr.StreetAddressFor = StreetAddress.Create(streetFor);
            vr.CityFor = City.Create(cityFor);
            vr.StateFor = UnitedStatesPostalState.Create(stateFor);
            vr.ZipFor = Zipcode.Create(zipFor);
            vr.StreetAddressTo = StreetAddress.Create(streetTo);
            vr.CityTo = City.Create(cityTo);
            vr.StateTo = UnitedStatesPostalState.Create(stateTo);
            vr.ZipTo = Zipcode.Create(zipTo);
            vr.PhoneTo = PhoneNumber.Create(phoneTo);
            vr.FaxTo = PhoneNumber.Create(faxTo);
            vr.Description = DescriptionField.Create(description);
            vr.IsSeeAttachment = isSeeAttachment;
            vr.LandlordCreditorName = EntityName.Create(landlord);
            vr.PrepDate = UnzonedDate.Create(prepDate);
            vr.VerifExpDate = UnzonedDate.Create(expDate);
            vr.VerifRecvDate = UnzonedDate.Create(recvDate);
            vr.VerifReorderedDate = UnzonedDate.Create(reordDate);
            vr.VerifSentDate = UnzonedDate.Create(sentDate);
            vr.VerifType = type;

            vr.SetVerifSignature(DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(signerId), DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(sigId));

            return vr;
        }
    }
}
