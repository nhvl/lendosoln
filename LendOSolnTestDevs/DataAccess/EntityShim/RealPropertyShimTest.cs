﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class RealPropertyShimTest : CollectionItemBase2ShimTest
    {
        private const int MinMarketValue = 500000;
        private const int MaxMarketValue = 1500000;
        private const int StepMarketValue = 100000;

        private const int MinMortgageAmount = 200000;
        private const int MaxMortgageAmount = 1500000;
        private const int StepMortgageAmount = 100000;

        private global::DataAccess.IRealEstateOwned shim;

        private IEnumerable ReoStatusEnumerable()
        {
            return RangeLimits.ReoStatus.Enumerable();
        }

        private IEnumerable ReoTypeEnumerable()
        {
            return RangeLimits.ReoType.Enumerable();
        }

        [Test]
        public void AddrTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.AddrTest(this.shim, old, old.Addr);

            this.CreateRealProperty(converter);
            this.AddrTest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.AddrTest(this.shim, old, null);
        }

        [Test]
        public void CityTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.CityTest(this.shim, old, old.City);

            this.CreateRealProperty(converter);
            this.CityTest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.CityTest(this.shim, old, null);
        }

        [Test]
        public void GrossRentITest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.GrossRentITest(this.shim, old, old.GrossRentI_rep);

            this.CreateRealProperty(converter);
            this.GrossRentITest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.GrossRentITest(this.shim, old, null);
        }

        [Test]
        public void GrossRentI_NonRental_Returns0()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.shim.StatT = E_ReoStatusT.Residence;
            this.shim.GrossRentI = 2500;
            old.StatT = E_ReoStatusT.Residence;
            old.GrossRentI = 2500;

            Assert.AreEqual(0, old.GrossRentI);
            Assert.AreEqual(0, this.shim.GrossRentI);
            Assert.AreEqual(old.GrossRentI_rep, this.shim.GrossRentI_rep);
        }

        [Test]
        public void GrossRentI_NonRental_ReturnsGrossRentI()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.shim.StatT = E_ReoStatusT.Rental;
            this.shim.GrossRentI = 2500;
            old.StatT = E_ReoStatusT.Rental;
            old.GrossRentI = 2500;

            Assert.AreEqual(2500, old.GrossRentI);
            Assert.AreEqual(2500, this.shim.GrossRentI);
            Assert.AreEqual(old.GrossRentI_rep, this.shim.GrossRentI_rep);
        }

        [Test]
        public void HExpTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.HExpTest(this.shim, old, old.HExp_rep);

            this.CreateRealProperty(converter);
            this.HExpTest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.HExpTest(this.shim, old, null);
        }

        [Test]
        public void IsForceCalcNetRentalIncTest()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.IsForceCalcNetRentalIncTest(this.shim, old, old.IsForceCalcNetRentalI);

            this.CreateRealProperty(converter);
            this.IsForceCalcNetRentalIncTest(this.shim, old, true);

            this.CreateRealProperty(converter);
            this.IsForceCalcNetRentalIncTest(this.shim, old, false);
        }

        [Test]
        public void IsForceCalcNetRentalInc_IsSubjectPropertyTrue_ReturnsFalse()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.shim.IsForceCalcNetRentalI = true;
            this.shim.IsSubjectProp = true;

            old.IsForceCalcNetRentalI = true;
            old.IsSubjectProp = true;

            Assert.AreEqual(false, old.IsForceCalcNetRentalI);
            Assert.AreEqual(false, this.shim.IsForceCalcNetRentalI);
        }

        [Test]
        public void IsForceCalcNetRentalIVisibleTest([ValueSource("ReoStatusEnumerable")] int status)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            old.StatT = (E_ReoStatusT)status;
            this.shim.StatT = (E_ReoStatusT)status;

            Assert.AreEqual(old.IsForceCalcNetRentalIVisible, this.shim.IsForceCalcNetRentalIVisible);
        }

        [Test]
        public void IsPrimaryResidenceTest()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.IsPrimaryResidenceTest(this.shim, old, old.IsPrimaryResidence);

            this.CreateRealProperty(converter);
            this.IsPrimaryResidenceTest(this.shim, old, true);

            this.CreateRealProperty(converter);
            this.IsPrimaryResidenceTest(this.shim, old, false);
        }

        [Test]
        public void IsStatusUpdatedTest([ValueSource("ReoStatusEnumerable")] int status)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);
            var original = old.StatT;

            var current = (E_ReoStatusT)status;
            old.StatT = current;
            this.shim.StatT = current;

            if (current == original)
            {
                Assert.IsFalse(old.IsStatusUpdated);
                Assert.IsFalse(this.shim.IsStatusUpdated);
            }
            else
            {
                Assert.IsTrue(old.IsStatusUpdated);
                Assert.IsTrue(this.shim.IsStatusUpdated);

                // The old behavior is incorrect under multiple changes, while the new behavior is correct:
                old.StatT = original;
                this.shim.StatT = original;

                Assert.IsTrue(old.IsStatusUpdated);
                Assert.IsFalse(this.shim.IsStatusUpdated);
            }
        }

        [Test]
        public void IsSubjectPropTest()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.IsSubjectPropTest(this.shim, old, old.IsSubjectProp);

            this.CreateRealProperty(converter);
            this.IsSubjectPropTest(this.shim, old, true);

            this.CreateRealProperty(converter);
            this.IsSubjectPropTest(this.shim, old, false);
        }

        [Test]
        public void MAmtTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.MAmtTest(this.shim, old, old.MAmt_rep);

            this.CreateRealProperty(converter);
            this.MAmtTest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.MAmtTest(this.shim, old, null);
        }

        [Test]
        public void MPmtTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.MPmtTest(this.shim, old, old.MPmt_rep);

            this.CreateRealProperty(converter);
            this.MPmtTest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.MPmtTest(this.shim, old, null);
        }

        [Test]
        public void NetRentITest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.NetRentITest(this.shim, old, old.NetRentI_rep);

            this.CreateRealProperty(converter);
            this.NetRentITest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.NetRentITest(this.shim, old, null);
        }

        private IEnumerable<decimal> grossRentIncomes = new[] { 0m, 1234.56m, 1500m };
        private IEnumerable<decimal> housingExpenses = new[] { 0m, 500.12m, 750m };
        private IEnumerable<decimal> mortgagePayments = new[] { 0m, 1750.57m, 2222m };

        [Test]
        public void NetRentI_Always_MatchesLegacyCalculation(
            [Values(true, false)] bool netRentILckd,
            [Values(E_ReoStatusT.Residence, E_ReoStatusT.Sale, E_ReoStatusT.PendingSale, E_ReoStatusT.Rental)] E_ReoStatusT status,
            [Values(true, false)] bool isForceCalcNetRentalI,
            [Values(0, 75, 100)] int occR,
            [ValueSource("grossRentIncomes")] decimal grossRentI,
            [ValueSource("housingExpenses")] decimal hExp,
            [ValueSource("mortgagePayments")] decimal mPmt)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.shim.NetRentILckd = netRentILckd;
            this.shim.NetRentI = 1500;
            this.shim.StatT = status;
            this.shim.IsForceCalcNetRentalI = isForceCalcNetRentalI;
            this.shim.OccR = occR;
            this.shim.GrossRentI = grossRentI;
            this.shim.HExp = hExp;
            this.shim.MPmt = mPmt;
            old.NetRentILckd = netRentILckd;
            old.NetRentI = 1500;
            old.StatT = status;
            old.IsForceCalcNetRentalI = isForceCalcNetRentalI;
            old.OccR = occR;
            old.GrossRentI = grossRentI;
            old.HExp = hExp;
            old.MPmt = mPmt;

            Assert.AreEqual(old.NetRentI, this.shim.NetRentI);
            Assert.AreEqual(old.NetRentI_rep, this.shim.NetRentI_rep);
        }

        [Test]
        public void NetRentILckdTest()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.NetRentILckdTest(this.shim, old, old.NetRentILckd);

            this.CreateRealProperty(converter);
            this.NetRentILckdTest(this.shim, old, true);

            this.CreateRealProperty(converter);
            this.NetRentILckdTest(this.shim, old, false);
        }

        [Test]
        public void NetValTest([Range(MinMarketValue, MaxMarketValue, StepMarketValue)] int marketValue, [Range(MinMortgageAmount, MaxMortgageAmount, StepMortgageAmount)] int mortgageAmount)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            decimal netold = this.GetNetVal(old, marketValue, mortgageAmount);
            decimal netshim = this.GetNetVal(this.shim, marketValue, mortgageAmount);
            Assert.AreEqual(netold, netshim);

            string netoldrep = this.GetNetValue_rep(old, marketValue, mortgageAmount);
            string netshimrep = this.GetNetValue_rep(this.shim, marketValue, mortgageAmount);
            Assert.AreEqual(netoldrep, netshimrep);
        }

        [Test]
        public void OccRTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.OccRTest(this.shim, old, old.OccR_rep);

            this.CreateRealProperty(converter);
            this.OccRTest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.OccRTest(this.shim, old, null);
        }

        [Test]
        public void OccR_Rental_ReturnsOccR()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.shim.StatT = E_ReoStatusT.Rental;
            this.shim.OccR = 25;
            old.StatT = E_ReoStatusT.Rental;
            old.OccR = 25;

            Assert.AreEqual(25, old.OccR);
            Assert.AreEqual(25, this.shim.OccR);
            Assert.AreEqual(old.OccR_rep, this.shim.OccR_rep);
        }

        [Test]
        public void OccR_NonRental_Returns0()
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.shim.StatT = E_ReoStatusT.Residence;
            this.shim.OccR = 25;
            old.StatT = E_ReoStatusT.Residence;
            old.OccR = 25;

            Assert.AreEqual(0, old.OccR);
            Assert.AreEqual(0, this.shim.OccR);
            Assert.AreEqual(old.OccR_rep, this.shim.OccR_rep);
        }

        [Test]
        public void OccR_repReadOnlyTest([ValueSource("ReoStatusEnumerable")] int status)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            var current = (E_ReoStatusT)status;
            old.StatT = current;
            this.shim.StatT = current;

            Assert.AreEqual(old.OccR_repReadOnly, this.shim.OccR_repReadOnly);
        }

        [Test]
        public void StatTest([Values("S", "R", "PS", "")] string code)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            old.Stat = code;
            this.shim.Stat = code;

            Assert.AreEqual(old.StatT, this.shim.StatT);
        }

        [Test]
        public void StateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.StateTest(this.shim, old, old.State);

            this.CreateRealProperty(converter);
            this.StateTest(this.shim, old, string.Empty);

            // Don't test null because the old code will attempt to log an error.
        }

        [Test]
        public void StatTTest([ValueSource("ReoStatusEnumerable")] int status)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            var current = (E_ReoStatusT)status;
            old.StatT = current;
            this.shim.StatT = current;

            Assert.AreEqual(old.StatT, this.shim.StatT);
        }

        [Test]
        public void TypeTest([ValueSource("ReoTypeEnumerable")] int type)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            var current = (E_ReoTypeT)type;
            string code = LendersOffice.CalculatedFields.RealProperty.TypeCode(current);
            old.Type = code;
            this.shim.Type = code;

            Assert.AreEqual(old.TypeT, this.shim.TypeT);
        }

        [Test]
        public void TypeTTest([ValueSource("ReoTypeEnumerable")] int type)
        {
            var converter = new LosConvert();
            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            var current = (E_ReoTypeT)type;
            old.TypeT = current;
            this.shim.TypeT = current;

            Assert.AreEqual(old.TypeT, this.shim.TypeT);
        }

        [Test]
        public void ValTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.ValTest(this.shim, old, old.Val_rep);

            this.CreateRealProperty(converter);
            this.ValTest(this.shim, old, string.Empty);

            this.CreateRealProperty(converter);
            this.ValTest(this.shim, old, null);
        }

        [Test]
        public void ZipTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreateRealProperty(converter);
            var old = new CReFields(this.data, converter, this.shim.RecordId);

            this.ZipTest(this.shim, old, old.Zip);

            this.CreateRealProperty(converter);
            this.ZipTest(this.shim, old, string.Empty);

            // Don't test null because the old code will attempt to log an error.
        }

        [Test]
        public void OwnerT_Get_RetrievesValueFromShimContainer()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var realProperty = new RealProperty();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(Guid.Empty).Returns(Ownership.Coborrower);
            var id = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(Guid.Empty);
            var shim = RealPropertyShim.Create(shimContainer, recordCollection, realProperty, ownershipManager, id, new LosConvert());

            var ownershipType = shim.ReOwnerT;

            Assert.AreEqual(E_ReOwnerT.CoBorrower, ownershipType);
        }

        [Test]
        public void OwnerT_Set_CallsShimContainerMethod()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var realProperty = new RealProperty();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            var id = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(Guid.Empty);
            var shim = RealPropertyShim.Create(shimContainer, recordCollection, realProperty, ownershipManager, id, new LosConvert());

            shim.ReOwnerT = E_ReOwnerT.Joint;

            ownershipManager.Received().SetOwnership(Guid.Empty, (Ownership)E_ReOwnerT.Joint);
        }

        protected override void CreateCollection()
        {
            this.CreateRealProperty(null);
        }

        private void AddrTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.Addr = value;
            old.Addr = value;
            Assert.AreEqual(old.Addr, shim.Addr);
        }

        private void CityTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.City = value;
            old.City = value;
            Assert.AreEqual(old.City, shim.City);
        }

        private void GrossRentITest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.GrossRentI_rep = value;
            old.GrossRentI_rep = value;
            Assert.AreEqual(old.GrossRentI, shim.GrossRentI);
            Assert.AreEqual(old.GrossRentI_rep, shim.GrossRentI_rep);
        }

        private void HExpTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.HExp_rep = value;
            old.HExp_rep = value;
            Assert.AreEqual(old.HExp, shim.HExp);
            Assert.AreEqual(old.HExp_rep, shim.HExp_rep);
        }

        private void IsForceCalcNetRentalIncTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, bool value)
        {
            shim.IsForceCalcNetRentalI = value;
            old.IsForceCalcNetRentalI = value;
            Assert.AreEqual(old.IsForceCalcNetRentalI, shim.IsForceCalcNetRentalI);
        }

        private void IsPrimaryResidenceTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, bool value)
        {
            shim.IsPrimaryResidence = value;
            old.IsPrimaryResidence = value;
            Assert.AreEqual(old.IsPrimaryResidence, shim.IsPrimaryResidence);
        }

        private void IsSubjectPropTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, bool value)
        {
            shim.IsSubjectProp = value;
            old.IsSubjectProp = value;
            Assert.AreEqual(old.IsSubjectProp, shim.IsSubjectProp);
        }

        private void MAmtTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.MAmt_rep = value;
            old.MAmt_rep = value;
            Assert.AreEqual(old.MAmt, shim.MAmt);
            Assert.AreEqual(old.MAmt_rep, shim.MAmt_rep);
        }

        private void MPmtTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.MPmt_rep = value;
            old.MPmt_rep = value;
            Assert.AreEqual(old.MPmt, shim.MPmt);
            Assert.AreEqual(old.MPmt_rep, shim.MPmt_rep);
        }

        private void NetRentITest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.NetRentI_rep = value;
            old.NetRentI_rep = value;
            Assert.AreEqual(old.NetRentI, shim.NetRentI);
            Assert.AreEqual(old.NetRentI_rep, shim.NetRentI_rep);
        }

        private void NetRentILckdTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, bool value)
        {
            shim.NetRentILckd = value;
            old.NetRentILckd = value;
            Assert.AreEqual(old.NetRentILckd, shim.NetRentILckd);
        }

        private decimal GetNetVal(IRealEstateOwned reo, decimal marketValue, decimal mortgageAmount)
        {
            reo.Val = marketValue;
            reo.MAmt = mortgageAmount;
            return reo.NetVal;
        }

        private string GetNetValue_rep(IRealEstateOwned reo, decimal marketValue, decimal mortgageAmount)
        {
            reo.Val = marketValue;
            reo.MAmt = mortgageAmount;
            return reo.NetValue_rep;
        }

        private void OccRTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.OccR_rep = value;
            old.OccR_rep = value;
            Assert.AreEqual(old.OccR, shim.OccR);
            Assert.AreEqual(old.OccR_rep, shim.OccR_rep);
        }

        private void StateTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.State = value;
            old.State = value;
            Assert.AreEqual(old.State, shim.State);
        }

        private void ValTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.Val_rep = value;
            old.Val_rep = value;
            Assert.AreEqual(old.Val, shim.Val);
            Assert.AreEqual(old.Val_rep, shim.Val_rep);
        }

        private void ZipTest(global::DataAccess.IRealEstateOwned shim, global::DataAccess.IRealEstateOwned old, string value)
        {
            shim.Zip = value;
            old.Zip = value;
            Assert.AreEqual(old.Zip, shim.Zip);
        }

        private void CreateRealProperty(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = RealPropertyCollectionShimTest.InitializeCollection(converter);
            this.data = collectionShim.GetDataSet();
            this.collection = collectionShim;
            this.baseShim = this.shim = collectionShim.GetRegularRecordAt(0);
        }
    }
}
