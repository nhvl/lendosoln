﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using NSubstitute;
    using LqbGrammar.Drivers;
    using LendersOffice.Common;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class EmploymentRecordCollectionShimTest : RecordCollectionShimTest
    {
        const string ConsumerPhone = "213-555-3456";

        private static Guid idPrimary;
        private static Guid id0;
        private static Guid id1;
        private static Guid id2;
        private static Guid id3;
        private static Guid id4;

        [Test]
        public void ExtraPreviousEmploymentsTest()
        {
            var collection = CreateAndReturnCollection(null);
            var extra = collection.ExtraPreviousEmployments;
            Assert.AreEqual(3, extra.Count);
        }
        
        [Test]
        public void HasMoreThan2PreviousEmploymentsTest()
        {
            var collection = CreateAndReturnCollection(null);
            Assert.IsTrue(collection.HasMoreThan2PreviousEmployments);
        }

        [Test]
        public void IsEmploymentHistoryValidTest()
        {
            var collection = CreateAndReturnCollection(null);
            Assert.IsTrue(collection.IsEmploymentHistoryValid);
        }

        [Test]
        public void IsEmplrBusPhoneLckdTest()
        {
            var collection = CreateAndReturnCollection(null);
            Assert.IsFalse(collection.IsEmplrBusPhoneLckd);
        }

        [Test]
        public void PersonBusPhoneTest()
        {
            var collection = CreateAndReturnCollection(null);

            var convert = new LosConvert();
            string phone = convert.ToPhoneNumFormat(ConsumerPhone, FormatDirection.ToDb);
            Assert.AreEqual(phone, collection.PersonBusPhone);
        }

        [Test]
        public void SortedViewTest()
        {
            // confirmed ordering in other cases, this case will confirm that primary is excluded
            var collection = CreateAndReturnCollection(null);
            var sorted = collection.SortedView;

            Assert.AreEqual(this.countRegular, sorted.Count);
            foreach (DataRow row in sorted.ToTable().Rows)
            {
                string recordid = (string)row["RecordId"];
                var guid = Guid.Parse(recordid);
                Assert.AreNotEqual(guid, idPrimary);
            }
        }

        [Test]
        public void AddRegularRecordTest()
        {
            var collection = CreateAndReturnCollection(null);

            var item = collection.AddRegularRecord();
            Assert.AreEqual(this.countRegular + 1, collection.CountRegular);
        }

        [Test]
        public void GetMaxEmploymentGapWithinTest()
        {
            var collection = CreateAndReturnCollection(null);
            var item = (IRegularEmploymentRecord)collection.GetRegularRecordAt(0);
            var start = item.EmplmtStartD_rep;
            var dtStart = DateTime.Parse(start);
            var dtEnd = DateTime.Today;

            var beginStart = new DateTime(dtStart.Year, 1, 1);
            var beginEnd = new DateTime(dtEnd.Year, 1, 1);
            int months = 12 * (dtEnd.Year - dtStart.Year) + dtEnd.Month - dtStart.Month;

            int gap = collection.GetMaxEmploymentGapWithin(months);
            Assert.IsTrue(gap > 0);
            Assert.IsTrue(gap < 12);
        }

        [Test]
        public void GetPrimaryEmpTest()
        {
            var collection = CreateAndReturnCollection(null);
            var primary = collection.GetPrimaryEmp(true);
            Assert.AreEqual(idPrimary, primary.RecordId);
        }

        [Test]
        public void GetRegRecordOfTest()
        {
            var collection = CreateAndReturnCollection(null);
            var item = collection.GetRegRecordOf(id0);
            Assert.IsNotNull(item);
        }

        [Test]
        public void GetRegularRecordAtTest()
        {
            var collection = CreateAndReturnCollection(null);
            var item = collection.GetRegularRecordAt(2);
            Assert.AreEqual(id2, item.RecordId);
        }

        [Test]
        public void ValidateEmploymentHistoryDatesTest()
        {
            var collection = CreateAndReturnCollection(null);

            var sb = new StringBuilder();
            bool good = collection.ValidateEmploymentHistoryDates(sb, "Harry Espalda");
            Assert.IsTrue(good);
            Assert.AreEqual(0, sb.Length);
        }

        [Test]
        public void GetSubcollectionTest()
        {
            var collection = CreateAndReturnCollection(null);

            var sub = collection.GetSubcollection(true, E_EmpGroupT.All);
            Assert.AreEqual(6, sub.Count);

            sub = collection.GetSubcollection(true, E_EmpGroupT.Current);
            Assert.AreEqual(1, sub.Count);

            sub = collection.GetSubcollection(true, E_EmpGroupT.Previous);
            Assert.AreEqual(5, sub.Count);

            sub = collection.GetSubcollection(true, E_EmpGroupT.Regular);
            Assert.AreEqual(5, sub.Count);

            sub = collection.GetSubcollection(true, E_EmpGroupT.Special);
            Assert.AreEqual(1, sub.Count);
        }

        [Test]
        public void AddRecordTest_Regular()
        {
            var collection = CreateAndReturnCollection(null);
            var item = collection.AddRecord(E_EmplmtStat.Previous);
            Assert.AreEqual(this.countRegular + 1, collection.CountRegular);
        }

        [Test]
        public void AddRecordTest_SpecialWhenExists()
        {
            var collection = CreateAndReturnCollection(null);
            Assert.Throws<CBaseException>(() => collection.AddRecord(E_EmplmtStat.Current));
        }

        [Test]
        public void AddRecordTest_SpecialWhenNotExists()
        {
            var collection = CreateAndReturnCollection(null, includeSpecial: false);
            Assert.AreEqual(0, collection.CountSpecial);

            var item = collection.AddRecord(E_EmplmtStat.Current);
            Assert.AreEqual(1, collection.CountSpecial);
        }

        [Test]
        public void AddRegularRecordAtTest()
        {
            var collection = CreateAndReturnCollection(null);
            var oldOrder = this.RegularRecordIds(collection);

            var item = collection.AddRegularRecordAt(2);
            var newOrder = this.RegularRecordIds(collection);

            Assert.AreEqual(this.countRegular + 1, collection.CountRegular);

            var p = collection.GetRegularRecordAt(0);
            Assert.AreEqual(id0, p.RecordId);

            p = collection.GetRegularRecordAt(1);
            Assert.AreEqual(id1, p.RecordId);

            p = collection.GetRegularRecordAt(2);
            Assert.AreEqual(item.RecordId, p.RecordId);

            p = collection.GetRegularRecordAt(3);
            Assert.AreEqual(id2, p.RecordId);

            p = collection.GetRegularRecordAt(4);
            Assert.AreEqual(id3, p.RecordId);

            p = collection.GetRegularRecordAt(5);
            Assert.AreEqual(id4, p.RecordId);
        }

        [Test]
        public void EnsureRegularRecordOfTest()
        {
            var collection = CreateAndReturnCollection(null);

            var item = collection.EnsureRegularRecordOf(id3);
            Assert.AreEqual(this.countRegular, collection.CountRegular);

            var guid = Guid.NewGuid();
            item = collection.EnsureRegularRecordOf(guid);
            Assert.AreEqual(this.countRegular + 1, collection.CountRegular);
        }

        [Test]
        public void GetPrimaryEmp_ForceCreateForNotDefinedCoborrower_ResultsInDefinedCoborrower()
        {
            var fakeApp = Substitute.For<IShimAppDataProvider>();
            var appId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();
            fakeApp.aAppId.Returns(appId.Value);
            fakeApp.aBConsumerId.Returns(new Guid("11111111-1111-1111-1111-111111111111"));
            fakeApp.aCConsumerId.Returns(new Guid("22222222-2222-2222-2222-222222222222"));
            var fakelegacyAppConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { fakeApp });
            var fakeBorrowerManagement = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionShim = new EmploymentRecordCollectionShim(
                fakeApp,
                fakeBorrowerManagement,
                isBorrower: false,
                shimContainer: (IShimContainer)GetCollectionContainer(fakelegacyAppConsumers),
                defaultsProvider: Substitute.For<IEmploymentRecordDefaultsProvider>(),
                converter: new LosConvert());

            collectionShim.GetPrimaryEmp(true);

            fakeBorrowerManagement.Received().AddCoborrowerToLegacyApplication(appId);
        }

        [Test]
        public void GetPrimaryEmp_ForceCreateForBorrower_DoesNotResultInDefinedCoborrower()
        {
            var fakeApp = Substitute.For<IShimAppDataProvider>();
            var appId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();
            fakeApp.aAppId.Returns(appId.Value);
            fakeApp.aBConsumerId.Returns(new Guid("11111111-1111-1111-1111-111111111111"));
            fakeApp.aCConsumerId.Returns(new Guid("22222222-2222-2222-2222-222222222222"));
            var fakelegacyAppConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { fakeApp });
            var fakeBorrowerManagement = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionShim = new EmploymentRecordCollectionShim(
                fakeApp,
                fakeBorrowerManagement,
                isBorrower: true,
                shimContainer: (IShimContainer)GetCollectionContainer(fakelegacyAppConsumers),
                defaultsProvider: Substitute.For<IEmploymentRecordDefaultsProvider>(),
                converter: new LosConvert());

            collectionShim.GetPrimaryEmp(true);

            fakeBorrowerManagement.DidNotReceive().SetCoborrowerActiveFlag(appId, true);
        }

        public static IEmpCollection InitializeCollection(LosConvert converter, bool includeSpecial = true, bool includeVOE = false)
        {
            converter = converter ?? new LosConvert();

            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("44444444-4444-4444-4444-444444444444"));
            var fakeLegacyAppConsumers = Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(appId, borrowerId, coborrowerId);
            ILoanLqbCollectionContainer collectionContainer = GetCollectionContainer(fakeLegacyAppConsumers);
            var did0 = collectionContainer.Add(borrowerId, CreateEmploymentRecord(0, includeVOE));
            var did1 = collectionContainer.Add(borrowerId, CreateEmploymentRecord(4, includeVOE));
            var did2 = collectionContainer.Add(borrowerId, CreateEmploymentRecord(2, includeVOE));
            var did3 = collectionContainer.Add(borrowerId, CreateEmploymentRecord(1, includeVOE));
            var did4 = collectionContainer.Add(borrowerId, CreateEmploymentRecord(3, includeVOE));

            id0 = did0.Value;
            id1 = did1.Value;
            id2 = did2.Value;
            id3 = did3.Value;
            id4 = did4.Value;
            idPrimary = Guid.Empty;

            if (includeSpecial)
            {
                var primary = CreateEmploymentRecord(5, includeVOE);
                primary.EmploymentStatusType = E_EmplmtStat.Current;
                primary.EmploymentEndDate = null;
                primary.IsCurrent = true;
                var did5 = collectionContainer.Add(borrowerId, primary);

                idPrimary = did5.Value;
            }

            var mockApp = Substitute.For<IShimAppDataProvider>();
            mockApp.aBEmplrBusPhoneLckd.Returns(false);
            mockApp.aBBusPhone.Returns(converter.ToPhoneNumFormat(ConsumerPhone, FormatDirection.ToDb));
            mockApp.aBConsumerId.Returns(borrowerId.Value);
            mockApp.aCConsumerId.Returns(coborrowerId.Value);

            return new EmploymentRecordCollectionShim(
                mockApp,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                true,
                collectionContainer as IShimContainer,
                Substitute.For<IEmploymentRecordDefaultsProvider>(),
                converter);
        }

        private static ILoanLqbCollectionContainer GetCollectionContainer(IReadOnlyLqbAssociationSet<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation> legacyAppConsumers)
        {
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanData.LegacyApplicationConsumers.Returns(legacyAppConsumers);

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var collectionContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.EmploymentRecords));
            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.IncomeSourceEmploymentRecords));

            collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());
            return collectionContainer;
        }

        private static EmploymentRecord CreateEmploymentRecord(int? markerValue, bool includeVOE)
        {
            string attention = "Captain Kangaroo";
            string houseNumber = markerValue == null ? "99" : "10" + markerValue.Value;
            string address = houseNumber + " Main St";
            string city = "Santa Ana";
            string name = "MeridianLink";
            string fax = "714-555-6789";
            string phone = "714-555-1234";
            string state = "CA";
            string zipcode = "92646";
            int year = markerValue == null ? 1999 : 2000 + markerValue.Value;
            int month = markerValue == null ? 1 : 2 + markerValue.Value;
            DateTime profStart = new DateTime(1999, 1, 1);
            DateTime start = new DateTime(year, month, 1);
            DateTime end = new DateTime(year, 12, 25);
            string title = "Sanitation Engineer";
            DateTime prep = new DateTime(2018, 4, 10);
            DateTime verfExp = new DateTime(2019, 4, 10);

            // Note: we want to let IsSeeAttachment get its default value of true so we can test that
            var er = new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
            er.Attention = EntityName.Create(attention);
            er.EmployerAddress = StreetAddress.Create(address);
            er.EmployerCity = City.Create(city);
            er.EmployerFax = PhoneNumber.Create(fax);
            er.EmployerName = EntityName.Create(name);
            er.EmployerPhone = PhoneNumber.Create(phone);
            er.EmployerState = UnitedStatesPostalState.Create(state);
            er.EmployerZip = Zipcode.Create(zipcode);
            er.EmploymentStartDate = UnzonedDate.Create(start);
            er.EmploymentEndDate = UnzonedDate.Create(end);
            er.EmploymentRecordVoeData = null;
            er.EmploymentStatusType = E_EmplmtStat.Previous;
            er.IsCurrent = false;
            er.IsSelfEmployed = false;
            er.JobTitle = DescriptionField.Create(title);
            er.MonthlyIncome = Money.Create(6525m);
            er.PrepDate = UnzonedDate.Create(prep);
            er.PrimaryEmploymentStartDate = markerValue == null || markerValue.Value < 5 ? null : er.EmploymentStartDate;
            er.ProfessionStartDate = UnzonedDate.Create(profStart);
            er.VerifExpiresDate = UnzonedDate.Create(verfExp);
            er.VerifRecvDate = er.PrepDate;
            er.VerifReorderedDate = er.PrepDate;
            er.VerifSentDate = er.PrepDate;
            er.SetVerifSignature(DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(Guid.NewGuid()), DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(Guid.NewGuid()));

            if (includeVOE)
            {
                var verif = new EmploymentRecordVOEData();
                verif.EmployeeIdVoe = er.VerifSigningEmployeeId.ToString();
                verif.EmployerCodeVoe = "verification_code";
                var list = new List<EmploymentRecordVOEData>();
                list.Add(verif);
                er.EmploymentRecordVoeData = list;
            }

            return er;
        }

        public override void CreateCollection()
        {
            this.CreateAndReturnCollection(null);
        }

        private IEmpCollection CreateAndReturnCollection(LosConvert converter, bool includeSpecial = true)
        {
            var collection = InitializeCollection(converter, includeSpecial: includeSpecial);
            this.baseShim = collection;
            this.firstRecordId = id0;
            this.countRegular = 5;
            this.countSpecial = includeSpecial ? 1 : 0;

            return collection;
        }

        protected override List<ICollectionItemBase2> RetrieveSpecialRecords()
        {
            var coll = (IEmpCollection)base.baseShim;
            var special = coll.GetPrimaryEmp(false);

            var list = new List<ICollectionItemBase2>();
            list.Add(special);
            return list;
        }

        private Guid[] RegularRecordIds(IEmpCollection collection)
        {
            var array = new Guid[collection.CountRegular];
            for (int i = 0; i < collection.CountRegular; ++i)
            {
                var item = collection.GetRegularRecordAt(i);
                array[i] = item.RecordId;
            }

            return array;
        }
    }
}
