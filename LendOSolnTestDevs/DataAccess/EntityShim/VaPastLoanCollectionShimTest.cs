﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class VaPastLoanCollectionShimTest
    {
        private static Guid id0;
        private static Guid id1;
        private static Guid id2;
        private static Guid id3;
        private static Guid id4;

        [Test]
        public void AddRecordTest()
        {
            var collection = InitializeCollection(null);

            Assert.AreEqual(5, collection.CountRegular);
            var shim = collection.AddRecord(E_VaPastLT.Regular);
            Assert.AreEqual(6, collection.CountRegular);
        }

        [Test]
        public void AddRegularRecordTest()
        {
            var collection = InitializeCollection(null);

            Assert.AreEqual(5, collection.CountRegular);
            var shim = collection.AddRegularRecord();
            Assert.AreEqual(6, collection.CountRegular);
        }

        [Test]
        public void AddRegularRecordAtTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var item = collection.AddRegularRecordAt(0);

            Assert.AreEqual(size + 1, collection.CountRegular);
            for (int i = 0; i < size + 1; ++i)
            {
                var curr = collection.GetRegularRecordAt(i);
                if (i == 0)
                {
                    Assert.AreEqual(item.RecordId, curr.RecordId);
                }
                else
                {
                    Assert.AreNotEqual(item.RecordId, curr.RecordId);
                }
            }
        }

        [Test]
        public void EnsureRegularRecordAtExistsTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var item = collection.EnsureRegularRecordAt(0);
            Assert.AreEqual(size, collection.CountRegular);
            Assert.AreEqual(id0, item.RecordId);
        }

        [Test]
        public void EnsureRegularRecordAtNotExistsTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var item = collection.EnsureRegularRecordAt(size);
            Assert.AreEqual(size + 1, collection.CountRegular);
            for (int i = 0; i < size + 1; ++i)
            {
                var curr = collection.GetRegularRecordAt(i);
                if (i == size)
                {
                    Assert.AreEqual(item.RecordId, curr.RecordId);
                }
                else
                {
                    Assert.AreNotEqual(item.RecordId, curr.RecordId);
                }
            }
        }

        [Test]
        public void EnsureRegularRecordOfExistsTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var first = collection.GetRegularRecordAt(0);
            var item = collection.EnsureRegularRecordOf(first.RecordId);

            Assert.AreEqual(size, collection.CountRegular);
            Assert.AreEqual(first.RecordId, item.RecordId);
        }

        [Test]
        public void EnsureRegularRecordOfNotExistsTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var guid = Guid.NewGuid();
            var item = collection.EnsureRegularRecordOf(guid);

            Assert.AreEqual(size + 1, collection.CountRegular);
            Assert.AreEqual(guid, item.RecordId);
        }

        [Test]
        public void GetRegRecordOfTest()
        {
            var collection = InitializeCollection(null);

            var item = collection.GetRegularRecordAt(2);
            var again = collection.GetRegRecordOf(item.RecordId);

            Assert.AreEqual(item.RecordId, again.RecordId);
        }

        [Test]
        public void GetRegularRecordAtTest()
        {
            var collection = InitializeCollection(null);

            var set = new HashSet<Guid>();
            for (int i = 0; i < collection.CountRegular; ++i)
            {
                var item = collection.GetRegularRecordAt(i);
                Assert.IsFalse(set.Contains(item.RecordId));
                set.Add(item.RecordId);
            }
        }

        [Test]
        public void GetSubcollectionTest()
        {
            var collection = InitializeCollection(null);

            var sub = collection.GetSubcollection(true, E_VaPastLGroupT.Regular);
            Assert.AreEqual(collection.CountRegular, sub.Count);
        }

        public static IVaPastLoanCollection InitializeCollection(LosConvert converter)
        {
            converter = converter ?? new LosConvert();

            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("44444444-4444-4444-4444-444444444444"));
            loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(appId, borrowerId, coborrowerId));

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var collectionContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.VaPreviousLoans));
            collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerVaPreviousLoans));

            collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());

            var did0 = collectionContainer.Add(borrowerId, CreateVaPreviousLoan(0));
            var did1 = collectionContainer.Add(borrowerId, CreateVaPreviousLoan(4));
            var did2 = collectionContainer.Add(borrowerId, CreateVaPreviousLoan(2));
            var did3 = collectionContainer.Add(borrowerId, CreateVaPreviousLoan(1));
            var did4 = collectionContainer.Add(borrowerId, CreateVaPreviousLoan(3));

            id0 = did0.Value;
            id1 = did1.Value;
            id2 = did2.Value;
            id3 = did3.Value;
            id4 = did4.Value;

            var ownershipManager = new ShimOwnershipManager<DataObjectKind.VaPreviousLoan, DataObjectKind.ConsumerVaPreviousLoanAssociation, VaPreviousLoan, ConsumerVaPreviousLoanAssociation>(
                collectionContainer as IShimContainer,
                borrowerId,
                coborrowerId,
                appId,
                (consumerId, recordId, AppId) => new ConsumerVaPreviousLoanAssociation(consumerId, recordId, AppId));

            return new VaPastLoanCollectionShim(collectionContainer as IShimContainer, ownershipManager, converter);
        }

        private static VaPreviousLoan CreateVaPreviousLoan(int streetNumber)
        {
            string loannumber = "VA-666-CAUTION";
            string loandesc = "Mortgage for a gigantic shoe for an elderly woman and a horde of children";
            string address = "10" + streetNumber.ToString() + " Main St";
            string city = "Timbuktu";
            string state = "CA";
            string zipcode = "92648";
            DateTime dateofloan = new DateTime(2010, 7, 4);
            DateTime solddate = new DateTime(2010, 8, 12);

            var vapl = new VaPreviousLoan();
            vapl.City = City.Create(city);
            vapl.State = UnitedStatesPostalState.Create(state);
            vapl.CityState = CityState.Create(city + " " + state);
            vapl.DateOfLoan = DescriptionField.Create(dateofloan.ToShortDateString());
            vapl.IsStillOwned = BooleanKleene.True;
            vapl.LoanDate = UnzonedDate.Create(dateofloan);
            vapl.LoanTypeDesc = DescriptionField.Create(loandesc);
            vapl.PropertySoldDate = UnzonedDate.Create(solddate);
            vapl.StreetAddress = StreetAddress.Create(address);
            vapl.VaLoanNumber = VaLoanNumber.Create(loannumber);
            vapl.Zip = Zipcode.Create(zipcode);

            return vapl;
        }
    }
}
