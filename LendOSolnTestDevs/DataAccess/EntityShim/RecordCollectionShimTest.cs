﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    /// <summary>
    /// Test the base class functionality for entity collection shims here so it only needs to be coded once.
    /// </summary>
    public abstract class RecordCollectionShimTest
    {
        protected Guid firstRecordId;
        protected int countRegular;
        protected int countSpecial;
        protected IRecordCollection baseShim;

        [Test]
        public void ConfirmOrderRankIncreasing()
        {
            this.CreateCollection();

            double previousOrderRank = double.MinValue;
            for (int i = 0; i < this.baseShim.CountRegular; ++i)
            {
                var item = this.baseShim.GetRegularRecordAt(i);
                var currentOrderRank = item.OrderRankValue;
                Assert.IsTrue(currentOrderRank > previousOrderRank);
                previousOrderRank = currentOrderRank;
            }
        }

        [Test]
        public void CountRegularTest()
        {
            this.CreateCollection();
            Assert.AreEqual(this.countRegular, this.baseShim.CountRegular);
        }

        [Test]
        public void CountSpecialTest()
        {
            this.CreateCollection();
            Assert.AreEqual(this.countSpecial, this.baseShim.CountSpecial);
        }

        [Test]
        public void RegularRecordsTest()
        {
            this.CreateCollection();
            var regcoll = this.RetrieveRegularRecords();
            Assert.AreEqual(countRegular, regcoll.Count);
        }

        [Test]
        public void SpecialRecordsTest()
        {
            this.CreateCollection();
            var speccoll = this.RetrieveSpecialRecords();
            Assert.AreEqual(countSpecial, speccoll.Count);
        }

        [Test]
        public void HasRecordOfTest()
        {
            this.CreateCollection();
            Assert.IsTrue(this.baseShim.HasRecordOf(this.firstRecordId));
        }

        [Test]
        public void GetDataSetTest()
        {
            this.CreateCollection();

            var ds = this.baseShim.GetDataSet();
            var table = ds.Tables[0];

            var count = this.countRegular + this.countSpecial;
            Assert.AreEqual(count, table.Rows.Count);
        }

        [Test]
        public void CloneFilteredTableTest()
        {
            this.CreateCollection();

            string recordid = this.firstRecordId.ToString("D");
            var table = this.baseShim.CloneFilteredTable(string.Format("RecordId = '{0}'", recordid));

            Assert.IsNotNull(table);
            Assert.AreEqual(1, table.Rows.Count);
            Assert.AreEqual(recordid, (string)table.Rows[0]["RecordId"]);
        }

        [Test]
        public void ClearAllTest()
        {
            this.CreateCollection();
            this.baseShim.ClearAll();
            Assert.AreEqual(0, this.baseShim.CountRegular);
            Assert.AreEqual(0, this.baseShim.CountSpecial);
        }

        [Test]
        public void MoveRegularRecordUpTest()
        {
            this.CreateCollection();

            var first = this.baseShim.GetRegularRecordAt(0);
            var second = this.baseShim.GetRegularRecordAt(1);
            this.baseShim.MoveRegularRecordUp(second);

            first = this.baseShim.GetRegularRecordAt(0);
            second = this.baseShim.GetRegularRecordAt(1);
            Assert.AreNotEqual(first.RecordId, this.firstRecordId);
            Assert.AreEqual(second.RecordId, this.firstRecordId);
       }

        [Test]
        public void MoveRegularRecordDownTest()
        {
            this.CreateCollection();

            var shim = this.baseShim.GetRegRecordOf(this.firstRecordId);
            this.baseShim.MoveRegularRecordDown(shim);

            var first = this.baseShim.GetRegularRecordAt(0);
            var second = this.baseShim.GetRegularRecordAt(1);
            Assert.AreNotEqual(first.RecordId, this.firstRecordId);
            Assert.AreEqual(second.RecordId, this.firstRecordId);
        }

        [Test]
        public void RemoveTest()
        {
            this.CreateCollection();

            var shim = this.baseShim.GetRegRecordOf(this.firstRecordId);
            this.CallRemove(shim);

            Assert.AreEqual(countRegular - 1, this.baseShim.CountRegular);
        }

        [Test]
        public void SortRegularRecordsByKeyTest()
        {
            this.CreateCollection();

            this.baseShim.SortRegularRecordsByKey("RecordId", true);

            string previous = string.Empty;
            foreach (var item in this.RetrieveRegularRecords())
            {
                string current = item.RecordId.ToString("D");
                Assert.IsTrue(string.Compare(previous, current) < 0);

                previous = current;
            }

            // We don't care about special records for "SortRegularRecords...".
        }

        [Test]
        public void InsertRecord_RemainsTest()
        {
            // With an earlier implementation, inserting an entity at
            // a location where it would not need to move in the order would
            // result in the item not being in the order at all, and therefore
            // inaccessible.
            //
            // Here we are inserting an item at position 0 in a collection
            // that has a count of 0.
            // Since it will already be in the correct position, it would 
            // not need to move in the order.  But it still needs to have its
            // order calculated so it can be found.

            this.CreateCollection();
            this.baseShim.ClearAll();

            // If is item is not given an order value on the insert,
            // we will get a developer exception here when trying to
            // pull from the order.
            var inserted = this.baseShim.AddRegularRecordAt(0);
            var item = this.baseShim.GetRegRecordOf(inserted.RecordId);

            Assert.AreEqual(0, item.OrderRankValue);
        }

        public abstract void CreateCollection();

        private List<ICollectionItemBase2> RetrieveRegularRecords()
        {
            var list = new List<ICollectionItemBase2>();
            for (int i = 0; i < this.baseShim.CountRegular; ++i)
            {
                list.Add(this.baseShim.GetRegularRecordAt(i));
            }

            return list;
        }

        private void CallRemove(ICollectionItemBase2 shim)
        {
            var remover = (IRemoveEntityShim)this.baseShim;
            remover.Remove(shim);
        }

        // force inheriting classes to test because they aren't in IRecordCollection but are in RecordCollectionShim base class.
        protected abstract List<ICollectionItemBase2> RetrieveSpecialRecords();
    }
}
