﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityAlimonyShimTest
    {
        [Test]
        public void DebtT()
        {
            var shim = LiabilityAlimonyShim.Create(
                null,
                null,
                new Liability(Substitute.For<ILiabilityDefaultsProvider>()),
                Substitute.For<IShimOwnershipManager<Guid>>(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty),
                null);

            Assert.AreEqual(E_DebtSpecialT.Alimony, shim.DebtT);
        }

        [Test]
        public void OwedTo([Values(null, "", "Generic Payee", "Payee 123")] string owedTo)
        {
            var tuple = this.GetShimAndLegacyLiability();
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.OwedTo = owedTo;
            legacyLiability.OwedTo = owedTo;

            Assert.AreEqual(legacyLiability.OwedTo, shim.OwedTo);
        }

        [Test]
        public void DataRow_AfterFlush_IsConsistentWithLegacy()
        {
            var entities = LiabilityCollectionShimTest.GetDefaultLiabilities(1);
            var liability = entities.First();
            liability.Value.DebtType = E_DebtT.Alimony;
            var collectionContainer = LiabilityCollectionShimTest.GetCollectionContainer(entities);
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(liability.Key).Returns(Ownership.Borrower);
            var converter = new LosConvert();
            var collectionShim = new LiabilityCollectionShim(
                (IShimContainer)collectionContainer,
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimAppDataProvider>(),
                ownershipManager,
                converter);
            var dataSetCopy = collectionShim.GetDataSet().Copy();

            var legacy = new CLiaAlimony(dataSetCopy, converter, liability.Key);
            var shim = collectionShim.GetAlimony(false);

            // Set the fields in an identical fashion.
            this.SetAvailableFields(legacy);
            this.SetAvailableFields(shim);

            // Get the data rows.
            var legacyRow = dataSetCopy.Tables[0].Rows[0];

            // Assert that the values are the same after shim flush.
            collectionShim.Flush();

            var shimRow = collectionShim.GetDataSet().Tables[0].Rows[0];
            LiabilityRegularShimTest.AssertLiabilityRowsEqual(legacyRow, shimRow);
        }

        private void SetAvailableFields(ILiabilityAlimony liability)
        {
            liability.NotUsedInRatio = true;
            liability.OwnerT = E_LiaOwnerT.Borrower;
            liability.PmlAuditTrailXmlContent = "";
            liability.Pmt_rep = "$7,890.12";
            liability.RemainMons_rep = "(R)";
            liability.WillBePdOff = true;
            liability.OwedTo = "Recipient";
        }

        private Tuple<LiabilityAlimonyShim, CLiaAlimony> GetShimAndLegacyLiability()
        {
            var converter = new LosConvert();

            var recordId = Guid.Empty;
            var entity = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            var shim = LiabilityAlimonyShim.Create(
                null,
                null,
                entity,
                Substitute.For<IShimOwnershipManager<Guid>>(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(recordId),
                converter);

            var collectionShim = LiabilityCollectionShimTest.InitializeCollection(converter, new[] { new KeyValuePair<Guid, Liability>(recordId, entity) });
            var dataSet = collectionShim.GetDataSet();
            var liaAlimony = new CLiaAlimony(dataSet, converter, recordId);

            return Tuple.Create((LiabilityAlimonyShim)shim, liaAlimony);
        }
    }
}
