﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityShimBaseTest : CollectionItemBase2ShimTest
    {
        protected override void CreateCollection()
        {
            var collectionShim = LiabilityCollectionShimBaseTest.InitializeCollection();

            this.data = collectionShim.GetDataSet();
            this.collection = collectionShim;
            this.baseShim =  collectionShim.GetRegularRecordAt(0);
        }
    }
}
