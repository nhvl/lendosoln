﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LiabilityRegularShimTest
    {
        private static readonly IEnumerable<FormatTarget> AllFormatTargets = Enum.GetValues(typeof(FormatTarget)).Cast<FormatTarget>();

        private static readonly IEnumerable<E_DebtRegularT> DebtRegularTValues = Enum.GetValues(typeof(E_DebtRegularT)).Cast<E_DebtRegularT>();

        private static readonly IEnumerable<E_LiabilityReconcileStatusT> LiabilityReconcileStatusTValues = Enum.GetValues(typeof(E_LiabilityReconcileStatusT)).Cast<E_LiabilityReconcileStatusT>();

        private static readonly IEnumerable<decimal> MoneyValues = new[]
        {
            -123.45m,
            0m,
            123.456m,
            123.45m,
            1234.56m,
            1234567.89m
        };

        private static readonly IEnumerable<string> MoneyRepValues = new[]
        {
            null,
            "",
            "-123.45",
            "(123.45)",
            "0",
            "123.456",
            "$1,234.56",
            "$1,234.56blah",
            "1234567"
        };

        private static readonly IEnumerable<bool> BoolValues = new[] { true, false };

        private static readonly IEnumerable<CDateTime> CDateTimeValues = new[]
        {
            null,
            CDateTime.InvalidWrapValue,
            CDateTime.Create(new DateTime(2018, 5, 2))
        };

        private static readonly IEnumerable<string> CDateTimeRepValues = new[]
        {
            null,
            "",
            "5/2/2018",
            "5/2/2018 abc",
            "May 2, 2018"
        };

        private static readonly IEnumerable<string> CountStringValues = new[]
        {
            null,
            "",
            "-1.5",
            "-1",
            "0",
            "1",
            "1.5",
            "1,500",
            "BLARG"
        };

        private static readonly IEnumerable<string> RateRepValues = new[]
        {
            null,
            "",
            "0",
            "1.2",
            "1.23",
            "1.234",
            "1.2%",
        };

        private static readonly IEnumerable<string> PhoneNumberValues = new[]
        {
            null,
            "",
            "8888888888",
            "888-888-8888",
            "(888)888-8888",
            "(888) 888-8888",
            "(888) 888-8888x88",
            "(888) 888-8888extABC"
        };

        /// <summary>
        /// These came from StateDropDownList.cs, and I added null just because.
        /// </summary>
        private static readonly IEnumerable<string> StateValues = new[]
        {
            null,
            "",
            "AK",
            "AL",
            "AR",
            "AS",
            "AZ",
            "CA",
            "CO",
            "CT",
            "DC",
            "DE",
            "FL", 
            "GA",
            "GU",
            "HI",
            "IA",
            "ID",
            "IL",
            "IN",
            "KS",
            "KY",
            "LA",
            "MA",
            "MD",
            "ME",
            "MI",
            "MN",
            "MO",
            "MP",
            "MS",
            "MT",
            "NC",
            "ND",
            "NE",  
            "NH",
            "NJ",
            "NM",
            "NV",
            "NY",
            "OH",
            "OK",
            "OR", 
            "PA",
            "PR",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VA",
            "VI",
            "VT", 
            "WA",
            "WI",
            "WV",
            "WY"
        };

        /// <summary>
        /// Contains test cases for the <see cref="RemainingMonthsForLpa(string, bool, decimal, decimal, string) method."/> 
        /// </summary>
        private IEnumerable<ITestCaseData> RemainingMonthsForLpaData = new List<ITestCaseData>
        {
            new TestCaseData("10", false, 100.0m, 10.0m, "10"),
            new TestCaseData("11", false, 100.0m, 10.0m, "11"),
            new TestCaseData("212", true, 0.0m, 0.0m, "212"),
            new TestCaseData("212", false, 0.0m, 0.0m, "212"),
            new TestCaseData("0", true, 0.0m, 0.0m, "0"),
            new TestCaseData("0", false, 0.0m, 0.0m, "0"),
            new TestCaseData("11", true, 100.0m, 10.0m, "11"),
            new TestCaseData("(R)", true, 100.0m, 10.0m, ""),
            new TestCaseData(null, true, 100.0m, 10.0m, "10"),
            new TestCaseData("", true, 100.0m, 10.0m, "10"),
            new TestCaseData("(R)", true, 100.0m, 0.0m, ""),
            new TestCaseData("(R)", false, 100.0m, 10.0m, ""),
            new TestCaseData(null, false, 100.0m, 10.0m, ""),
            new TestCaseData("", false, 100.0m, 10.0m, ""),
            new TestCaseData("(R)", true, 375.04m, 125.01m, ""),
            new TestCaseData("(R)", true, 375.02m, 125.01m, "")
        };

        [Test]
        public void AccNm(
            [Values(null, "", "Borrower Name", "Acc. 1234")] string accountName)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.AccNm = accountName;
            legacyLiability.AccNm = accountName;

            Assert.AreEqual(legacyLiability.AccNm, shim.AccNm);
        }

        [Test]
        public void AccNum(
            [Values(null, "", "123456789", "Word")] string accountNumber)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.AccNum = accountNumber;
            legacyLiability.AccNum = accountNumber;

            Assert.AreEqual(legacyLiability.AccNum, shim.AccNum);
        }

        [Test]
        public void Attention(
            [Values(null, "", "Attention Person Name", "Person Name 12345")] string attention)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Attention = attention;
            legacyLiability.Attention = attention;

            Assert.AreEqual(legacyLiability.Attention, shim.Attention);
        }

        [Test]
        public void AutoYearMake(
            [Values(null, "", "1999", "1767", "Two Thousand Eighteen")] string year)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.AutoYearMake = year;
            legacyLiability.AutoYearMake = year;

            Assert.AreEqual(legacyLiability.AutoYearMake, shim.AutoYearMake);
        }

        [Test]
        public void Bal([ValueSource("MoneyValues")] decimal balance)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Bal = balance;
            legacyLiability.Bal = balance;

            Assert.AreEqual(legacyLiability.Bal, shim.Bal);
        }

        [Test, Combinatorial]
        public void BalRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("MoneyRepValues")] string repValueToSet)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Bal_rep = repValueToSet;
            legacyLiability.Bal_rep = repValueToSet;

            Assert.AreEqual(legacyLiability.Bal_rep, shim.Bal_rep);
        }

        [Test]
        public void ComAddr(
            [Values(null, "", "123 Main St.", "Sunflower Ave Ste 200")] string comAddr)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ComAddr = comAddr;
            legacyLiability.ComAddr = comAddr;

            Assert.AreEqual(legacyLiability.ComAddr, shim.ComAddr);
        }

        [Test]
        public void ComCity(
            [Values(null, "", "Newport Beach")] string comCity)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ComCity = comCity;
            legacyLiability.ComCity = comCity;

            Assert.AreEqual(legacyLiability.ComCity, shim.ComCity);
        }

        [Test]
        public void ComFax(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("PhoneNumberValues")] string comFax)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ComFax = comFax;
            legacyLiability.ComFax = comFax;

            Assert.AreEqual(legacyLiability.ComFax, shim.ComFax);
        }

        [Test]
        public void ComNm(
            [Values(null, "", "Company Name", "MeridianLink, Inc.")] string comNm)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ComNm = comNm;
            legacyLiability.ComNm = comNm;

            Assert.AreEqual(legacyLiability.ComNm, shim.ComNm);
        }

        [Test, Combinatorial]
        public void ComPhone(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("PhoneNumberValues")] string comPhone)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ComPhone = comPhone;
            legacyLiability.ComPhone = comPhone;

            Assert.AreEqual(legacyLiability.ComPhone, shim.ComPhone);
        }

        [Test]
        public void ComState([ValueSource("StateValues")] string comState)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Logger);

                var tuple = GetShimAndLegacyLiability(null);
                var shim = tuple.Item1;
                var legacyLiability = tuple.Item2;

                shim.ComState = comState;
                legacyLiability.ComState = comState;

                Assert.AreEqual(legacyLiability.ComState, shim.ComState);
            }
        }

        [Test]
        public void ComZip(
            [Values(null, "", "12345", "12345-1234", "12345+1234")] string comZip)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Logger);

                var tuple = GetShimAndLegacyLiability(null);
                var shim = tuple.Item1;
                var legacyLiability = tuple.Item2;

                shim.ComZip = comZip;
                legacyLiability.ComZip = comZip;

                Assert.AreEqual(legacyLiability.ComZip, shim.ComZip);
            }
        }

        [Test]
        public void DebtT([ValueSource("DebtRegularTValues")] E_DebtRegularT debtT)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.DebtT = debtT;
            legacyLiability.DebtT = debtT;

            Assert.AreEqual(legacyLiability.DebtT, shim.DebtT);
        }

        [Test]
        public void DebtT_SetToInvalidValue_DefaultsToOther()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;
            E_DebtRegularT invalid = (E_DebtRegularT)100;

            shim.DebtT = invalid;
            legacyLiability.DebtT = invalid;

            Assert.AreEqual(E_DebtT.Other, (E_DebtT)legacyLiability.DebtT);
            Assert.AreEqual(legacyLiability.DebtT, shim.DebtT);
        }

        [Test]
        public void DebtTRep([ValueSource("DebtRegularTValues")] E_DebtRegularT debtT)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.DebtT = debtT;
            legacyLiability.DebtT = debtT;

            Assert.AreEqual(legacyLiability.DebtT_rep, shim.DebtT_rep);
        }

        [Test]
        public void Desc(
            [Values(null, "", "Desc", "Description, 12345")] string descValue)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Desc = descValue;
            legacyLiability.Desc = descValue;

            Assert.AreEqual(legacyLiability.Desc, shim.Desc);
        }

        [Test, Combinatorial]
        public void DueRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("CountStringValues")] string dueRep)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Logger);

                var converter = new LosConvert(formatTarget);

                var tuple = GetShimAndLegacyLiability(converter);
                var shim = tuple.Item1;
                var legacyLiability = tuple.Item2;

                shim.Due_rep = dueRep;
                legacyLiability.Due_rep = dueRep;

                Assert.AreEqual(legacyLiability.Due_rep, shim.Due_rep);
            }
        }

        [Test]
        public void ExcFromUnderwriting(
            [ValueSource("BoolValues")] bool excFromUnderwriting)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ExcFromUnderwriting = excFromUnderwriting;
            legacyLiability.ExcFromUnderwriting = excFromUnderwriting;

            Assert.AreEqual(legacyLiability.ExcFromUnderwriting, shim.ExcFromUnderwriting);
        }

        [Test]
        public void FullyIndexedPITIPayment_Always_Returns0()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;

            Assert.AreEqual(0, shim.FullyIndexedPITIPayment);
            shim.FullyIndexedPITIPayment = 100;
            Assert.AreEqual(0, shim.FullyIndexedPITIPayment);
        }

        [Test]
        public void IncInBankruptcy(
            [ValueSource("BoolValues")] bool incInBankruptcy)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.IncInBankruptcy = incInBankruptcy;
            legacyLiability.IncInBankruptcy = incInBankruptcy;

            Assert.AreEqual(legacyLiability.IncInBankruptcy, shim.IncInBankruptcy);
        }

        [Test]
        public void IncInForeclosure(
            [ValueSource("BoolValues")] bool incInForeclosure)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.IncInForeclosure = incInForeclosure;
            legacyLiability.IncInForeclosure = incInForeclosure;

            Assert.AreEqual(legacyLiability.IncInForeclosure, shim.IncInForeclosure);
        }

        [Test]
        public void IncInReposession(
            [ValueSource("BoolValues")] bool incInReposession)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.IncInReposession = incInReposession;
            legacyLiability.IncInReposession = incInReposession;

            Assert.AreEqual(legacyLiability.IncInReposession, shim.IncInReposession);
        }

        [Test]
        public void IsForAuto(
            [ValueSource("BoolValues")] bool isForAuto)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.IsForAuto = isForAuto;
            legacyLiability.IsForAuto = isForAuto;

            Assert.AreEqual(legacyLiability.IsForAuto, shim.IsForAuto);
        }

        [Test]
        public void IsMortFHAInsured(
            [ValueSource("BoolValues")] bool isMortFHAInsured)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.IsMortFHAInsured = isMortFHAInsured;
            shim.IsForAuto = !isMortFHAInsured; // Found bug where this was being reused.
            legacyLiability.IsMortFHAInsured = isMortFHAInsured;

            Assert.AreEqual(legacyLiability.IsMortFHAInsured, shim.IsMortFHAInsured);
        }

        [Test]
        public void IsPiggyBack(
            [ValueSource("BoolValues")] bool isPiggyBack)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.IsPiggyBack = isPiggyBack;
            legacyLiability.IsPiggyBack = isPiggyBack;

            Assert.AreEqual(legacyLiability.IsPiggyBack, shim.IsPiggyBack);
        }

        [Test]
        public void IsSeeAttachment(
            [ValueSource("BoolValues")] bool isSeeAttachment)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.IsSeeAttachment = isSeeAttachment;
            legacyLiability.IsSeeAttachment = isSeeAttachment;

            Assert.AreEqual(legacyLiability.IsSeeAttachment, shim.IsSeeAttachment);
        }

        [Test]
        public void IsSeeAttachment_Always_DefaultsToTrue()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            Assert.AreEqual(true, legacyLiability.IsSeeAttachment);
            Assert.AreEqual(true, shim.IsSeeAttachment);
        }

        [Test, Combinatorial]
        public void Late30Rep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("CountStringValues")] string late30Rep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Late30_rep = late30Rep;
            legacyLiability.Late30_rep = late30Rep;

            Assert.AreEqual(legacyLiability.Late30_rep, shim.Late30_rep);
        }

        [Test, Combinatorial]
        public void Late60Rep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("CountStringValues")] string late60Rep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Late60_rep = late60Rep;
            legacyLiability.Late60_rep = late60Rep;

            Assert.AreEqual(legacyLiability.Late60_rep, shim.Late60_rep);
        }

        [Test, Combinatorial]
        public void Late90PlusRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("CountStringValues")] string late90PlusRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Late90Plus_rep = late90PlusRep;
            legacyLiability.Late90Plus_rep = late90PlusRep;

            Assert.AreEqual(legacyLiability.Late90Plus_rep, shim.Late90Plus_rep);
        }

        [Test]
        public void MatchedReRecordId_Get_ReturnsValueFromShimContainer()
        {
            var realPropertyId = new Guid("11111111-1111-1111-1111-111111111111");
            var shimContainer = Substitute.For<IShimContainer>();
            shimContainer.GetRealPropertyIdLinkedToLiability(Arg.Any<Guid>()).Returns(realPropertyId);
            var shim = LiabilityRegularShim.Create(
                shimContainer,
                Substitute.For<IRecordCollection>(),
                new Liability(Substitute.For<ILiabilityDefaultsProvider>()),
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimOwnershipManager<Guid>>(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty),
                null);

            shim.DebtT = E_DebtRegularT.Mortgage;

            Assert.AreEqual(realPropertyId, shim.MatchedReRecordId);
            shimContainer.Received().GetRealPropertyIdLinkedToLiability(shim.RecordId);
        }

        [Test]
        public void MatchedReRecordId_Set_CallsShimContainerToCreateAssociation()
        {
            var realPropertyId = new Guid("11111111-1111-1111-1111-111111111111");
            var shimContainer = Substitute.For<IShimContainer>();
            var shim = LiabilityRegularShim.Create(
                shimContainer,
                Substitute.For<IRecordCollection>(),
                new Liability(Substitute.For<ILiabilityDefaultsProvider>()),
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimOwnershipManager<Guid>>(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty),
                null);

            shim.MatchedReRecordId = realPropertyId;

            shimContainer.Received().AssociateLiabilityWithRealProperty(shim.RecordId, realPropertyId);
        }

        [Test]
        public void OrigDebtAmt(
            [ValueSource("MoneyValues")] decimal origDebtAmt)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.OrigDebtAmt = origDebtAmt;
            legacyLiability.OrigDebtAmt = origDebtAmt;

            Assert.AreEqual(legacyLiability.OrigDebtAmt, shim.OrigDebtAmt);
        }

        [Test, Combinatorial]
        public void OrigDebtAmtRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("MoneyRepValues")] string origDebtAmtRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.OrigDebtAmt_rep = origDebtAmtRep;
            legacyLiability.OrigDebtAmt_rep = origDebtAmtRep;

            Assert.AreEqual(legacyLiability.OrigDebtAmt_rep, shim.OrigDebtAmt_rep);
        }

        [Test, Combinatorial]
        public void OrigTermRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("CountStringValues")] string origTermRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.OrigTerm_rep = origTermRep;
            legacyLiability.OrigTerm_rep = origTermRep;

            Assert.AreEqual(legacyLiability.OrigTerm_rep, shim.OrigTerm_rep);
        }

        [Test, Combinatorial]
        public void PayoffAmt(
            [ValueSource("MoneyValues")] decimal payoffAmt,
            [ValueSource("BoolValues")] bool payoffAmtLckd,
            [ValueSource("MoneyValues")] decimal bal)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Bal = bal;
            shim.PayoffAmtLckd = payoffAmtLckd;
            shim.PayoffAmt = payoffAmt;

            legacyLiability.Bal = bal;
            legacyLiability.PayoffAmtLckd = payoffAmtLckd;
            legacyLiability.PayoffAmt = payoffAmt;

            Assert.AreEqual(legacyLiability.PayoffAmt, shim.PayoffAmt);
        }

        [Test]
        public void PayoffAmt_NotExplicitlySettingPayoffAmtLckd_ReturnsSameValue()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Bal = 5;

            legacyLiability.Bal = 5;

            Assert.AreEqual(legacyLiability.PayoffAmt, shim.PayoffAmt);
        }

        [Test, Combinatorial]
        public void PayoffAmtRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("MoneyRepValues")] string payoffAmtRep,
            [ValueSource("BoolValues")] bool payoffAmtLckd,
            [ValueSource("MoneyValues")] decimal bal)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.Bal = bal;
            shim.PayoffAmtLckd = payoffAmtLckd;
            shim.PayoffAmt_rep = payoffAmtRep;

            legacyLiability.Bal = bal;
            legacyLiability.PayoffAmtLckd = payoffAmtLckd;
            legacyLiability.PayoffAmt_rep = payoffAmtRep;

            Assert.AreEqual(legacyLiability.PayoffAmt, shim.PayoffAmt);
        }

        [Test]
        public void PayoffAmtLckd(
            [ValueSource("BoolValues")] bool payoffAmtLckd)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.PayoffAmtLckd = payoffAmtLckd;
            legacyLiability.PayoffAmtLckd = payoffAmtLckd;

            Assert.AreEqual(legacyLiability.PayoffAmtLckd, shim.PayoffAmtLckd);
        }

        [Test, Combinatorial]
        public void PayoffTimingLckd(
            [ValueSource("BoolValues")] bool willBePdOff,
            [ValueSource("BoolValues")] bool payoffTimingLckd)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.WillBePdOff = willBePdOff;
            shim.PayoffTimingLckd = payoffTimingLckd;

            legacyLiability.WillBePdOff = willBePdOff;
            legacyLiability.PayoffTimingLckd = payoffTimingLckd;

            Assert.AreEqual(legacyLiability.PayoffTimingLckd, shim.PayoffTimingLckd);
        }

        [Test]
        public void PayoffTimingCast()
        {
            Assert.AreEqual(PayoffTiming.AtClosing, (PayoffTiming)E_Timing.At_Closing);
            Assert.AreEqual(PayoffTiming.BeforeClosing, (PayoffTiming)E_Timing.Before_Closing);
            Assert.AreEqual(PayoffTiming.Blank, (PayoffTiming)E_Timing.Blank);

            Assert.AreEqual(E_Timing.At_Closing, (E_Timing)PayoffTiming.AtClosing);
            Assert.AreEqual(E_Timing.Before_Closing, (E_Timing)PayoffTiming.BeforeClosing);
            Assert.AreEqual(E_Timing.Blank, (E_Timing)PayoffTiming.Blank);
        }

        [Test]
        public void PayoffTiming_SetToInvalidValue_DefaultsToBlank()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;
            E_Timing invalid = (E_Timing)10;

            shim.WillBePdOff = true;
            shim.PayoffTimingLckd = true;
            shim.PayoffTiming = invalid;

            legacyLiability.WillBePdOff = true;
            legacyLiability.PayoffTimingLckd = true;
            legacyLiability.PayoffTiming = invalid;

            Assert.AreEqual(E_Timing.Blank, legacyLiability.PayoffTiming);
            Assert.AreEqual(legacyLiability.PayoffTiming, shim.PayoffTiming);
        }

        [Test]
        public void PrepD(
            [ValueSource("CDateTimeValues")] CDateTime prepD)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.PrepD = prepD;
            legacyLiability.PrepD = prepD;

            Assert.AreEqual(legacyLiability.PrepD, shim.PrepD);
        }

        [Test]
        public void ReconcileStatusT_SetVariousValues_ReturnsSameValueForShimAndLegacyLiability(
            [ValueSource("LiabilityReconcileStatusTValues")] E_LiabilityReconcileStatusT reconcileStatusT)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ReconcileStatusT = reconcileStatusT;
            legacyLiability.ReconcileStatusT = reconcileStatusT;

            Assert.AreEqual(legacyLiability.ReconcileStatusT, shim.ReconcileStatusT);
        }

        [Test]
        public void ReconcileStatusT_Always_DefaultsToLoanFileOnly()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            Assert.AreEqual(E_LiabilityReconcileStatusT.LoanFileOnly, legacyLiability.ReconcileStatusT);
            Assert.AreEqual(legacyLiability.ReconcileStatusT, shim.ReconcileStatusT);
        }

        [Test]
        public void ReconcileStatusT_SetToInvalidIntValue_DefaultsToLoanFileOnly()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            E_LiabilityReconcileStatusT invalid = (E_LiabilityReconcileStatusT)9;
            shim.ReconcileStatusT = invalid;
            legacyLiability.ReconcileStatusT = invalid;

            Assert.AreEqual(E_LiabilityReconcileStatusT.LoanFileOnly, legacyLiability.ReconcileStatusT);
            Assert.AreEqual(legacyLiability.ReconcileStatusT, shim.ReconcileStatusT);
        }

        [Test]
        [TestCaseSource(nameof(RemainingMonthsForLpaData))]
        public void RemainingMonthsForLpa(
            string remainMonsRep,
            bool excludedFromUnderwriting,
            decimal bal,
            decimal pmt,
            string expected)
        {
            var tuple = GetShimAndLegacyLiability(new LosConvert(FormatTarget.FreddieMacAUS));
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.RemainMons_rep = remainMonsRep;
            shim.ExcFromUnderwriting = excludedFromUnderwriting;
            shim.Bal = bal;
            shim.Pmt = pmt;

            legacyLiability.RemainMons_rep = remainMonsRep;
            legacyLiability.ExcFromUnderwriting = excludedFromUnderwriting;
            legacyLiability.Bal = bal;
            legacyLiability.Pmt = pmt;

            Assert.AreEqual(expected, shim.GetRemainingMonthsForLpa());
            Assert.AreEqual(expected, legacyLiability.GetRemainingMonthsForLpa());
        }

        [Test]
        public void RRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("RateRepValues")] string rRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.R_rep = rRep;
            legacyLiability.R_rep = rRep;

            Assert.AreEqual(legacyLiability.R_rep, shim.R_rep);
        }

        [Test]
        public void UsedInRatio(
            [ValueSource("BoolValues")] bool usedInRatio)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.UsedInRatio = usedInRatio;
            legacyLiability.UsedInRatio = usedInRatio;

            Assert.AreEqual(legacyLiability.UsedInRatio, shim.UsedInRatio);
        }

        [Test]
        public void VerifExpD(
           [ValueSource("CDateTimeValues")] CDateTime verifExpD)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifExpD = verifExpD;
            legacyLiability.VerifExpD = verifExpD;

            Assert.AreEqual(legacyLiability.VerifExpD, shim.VerifExpD);
        }

        [Test, Combinatorial]
        public void VerifExpDRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("CDateTimeRepValues")] string verifExpDRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifExpD_rep = verifExpDRep;
            legacyLiability.VerifExpD_rep = verifExpDRep;

            Assert.AreEqual(legacyLiability.VerifExpD_rep, shim.VerifExpD_rep);
        }

        [Test]
        public void VerifRecvD(
           [ValueSource("CDateTimeValues")] CDateTime verifRecvD)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifRecvD = verifRecvD;
            legacyLiability.VerifRecvD = verifRecvD;

            Assert.AreEqual(legacyLiability.VerifRecvD, shim.VerifRecvD);
        }

        [Test, Combinatorial]
        public void VerifRecvDRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [ValueSource("CDateTimeRepValues")] string verifRecvDRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifRecvD_rep = verifRecvDRep;
            legacyLiability.VerifRecvD_rep = verifRecvDRep;

            Assert.AreEqual(legacyLiability.VerifRecvD_rep, shim.VerifRecvD_rep);
        }

        [Test]
        public void VerifReorderedD(
           [ValueSource("CDateTimeValues")] CDateTime verifReorderedD)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifReorderedD = verifReorderedD;
            legacyLiability.VerifReorderedD = verifReorderedD;

            Assert.AreEqual(legacyLiability.VerifReorderedD, shim.VerifReorderedD);
        }

        [Test, Combinatorial]
        public void VerifReorderedDRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [Values("CDateTimeRepValues")] string verifReorderedDRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifReorderedD_rep = verifReorderedDRep;
            legacyLiability.VerifReorderedD_rep = verifReorderedDRep;

            Assert.AreEqual(legacyLiability.VerifReorderedD_rep, shim.VerifReorderedD_rep);
        }

        [Test]
        public void VerifSentD(
           [ValueSource("CDateTimeValues")] CDateTime verifSentD)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifSentD = verifSentD;
            legacyLiability.VerifSentD = verifSentD;

            Assert.AreEqual(legacyLiability.VerifSentD, shim.VerifSentD);
        }

        [Test, Combinatorial]
        public void VerifSentDRep(
            [ValueSource("AllFormatTargets")] FormatTarget formatTarget,
            [Values("CDateTimeRepValues")] string verifSentDRep)
        {
            var converter = new LosConvert(formatTarget);
            var tuple = GetShimAndLegacyLiability(converter);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.VerifSentD_rep = verifSentDRep;
            legacyLiability.VerifSentD_rep = verifSentDRep;

            Assert.AreEqual(legacyLiability.VerifSentD_rep, shim.VerifSentD_rep);
        }

        [Test]
        public void SignatureProcessTest()
        {
            var employeeId = new Guid("11111111-1111-1111-1111-111111111111");
            var imgId = new Guid("22222222-2222-2222-2222-222222222222").ToString();
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            legacyLiability.ApplySignature(employeeId, imgId);
            Assert.AreEqual(employeeId, legacyLiability.VerifSigningEmployeeId);
            Assert.AreEqual(imgId, legacyLiability.VerifSignatureImgId);
            Assert.AreEqual(true, legacyLiability.VerifHasSignature);

            shim.ApplySignature(employeeId, imgId);
            Assert.AreEqual(employeeId, shim.VerifSigningEmployeeId);
            Assert.AreEqual(imgId, shim.VerifSignatureImgId);
            Assert.AreEqual(true, shim.VerifHasSignature);

            legacyLiability.ClearSignature();
            Assert.AreEqual(Guid.Empty, legacyLiability.VerifSigningEmployeeId);
            Assert.AreEqual(string.Empty, legacyLiability.VerifSignatureImgId);
            Assert.AreEqual(false, legacyLiability.VerifHasSignature);

            shim.ClearSignature();
            Assert.AreEqual(Guid.Empty, shim.VerifSigningEmployeeId);
            Assert.AreEqual(string.Empty, shim.VerifSignatureImgId);
            Assert.AreEqual(false, shim.VerifHasSignature);
        }

        [Test]
        public void FieldUpdateAudit_Always_ResultsInSameAuditXmlForShimAndLegacyLiability()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            var username = "username";
            var login = "login";
            var field = "fieldid";
            var value = "value";
            shim.FieldUpdateAudit(username, login, field, value);
            legacyLiability.FieldUpdateAudit(username, login, field, value);

            this.AssertPmlAuditTrailXmlEqual(legacyLiability.PmlAuditTrailXmlContent, shim.PmlAuditTrailXmlContent);
        }

        // Yes, this is bad. But it is done.
        private void AssertPmlAuditTrailXmlEqual(string legacyXml, string shimXml)
        {
            var legacyDoc = XDocument.Parse(legacyXml);
            var shimDoc = XDocument.Parse(shimXml);

            var legacyHistory = legacyDoc.Element("History");
            var shimHistory = shimDoc.Element("History");

            var legacyEvents = legacyHistory.Elements("Event").ToList();
            var shimEvents = shimHistory.Elements("Event").ToList();

            for (var i = 0; i < legacyEvents.Count; ++i)
            {
                var legacyEvent = legacyEvents[i];
                var shimEvent = shimEvents[i];

                Assert.AreEqual(legacyEvent.Attribute("UserName").Value, shimEvent.Attribute("UserName").Value);
                Assert.AreEqual(legacyEvent.Attribute("LoginId").Value, shimEvent.Attribute("LoginId").Value);
                Assert.AreEqual(legacyEvent.Attribute("Action").Value, shimEvent.Attribute("Action").Value);
                Assert.AreEqual(legacyEvent.Attribute("Field")?.Value, shimEvent.Attribute("Field")?.Value);
                Assert.AreEqual(legacyEvent.Attribute("Value")?.Value, shimEvent.Attribute("Value")?.Value);

                var legacyDateDesc = legacyEvent.Attribute("EventDate").Value;
                var shimDateDesc = shimEvent.Attribute("EventDate").Value;

                int legacyDateTimeEndIndex = 0;
                int shimDateTimeEndIndex = 0;
                for (int j = 1; j <= 3; ++j)
                {
                    legacyDateTimeEndIndex = legacyDateDesc.IndexOf(' ', legacyDateTimeEndIndex + 1);
                    shimDateTimeEndIndex = shimDateDesc.IndexOf(' ', shimDateTimeEndIndex + 1);
                }

                var legacyDate = DateTime.Parse(legacyDateDesc.Substring(0, legacyDateTimeEndIndex + 1));
                var shimDate = DateTime.Parse(shimDateDesc.Substring(0, shimDateTimeEndIndex + 1));

                var timeBetween = legacyDate.Subtract(shimDate);
                // We expect the dates to be within 5 minutes, but if we need to bump this we can.
                // Comparing the strings directly resulted in some issues when the test just happened
                // to generate the audits at different minutes :'(.
                Assert.Less(timeBetween.TotalMinutes, 5);


                var legacyTimeZoneDesc = legacyDateDesc.Skip(legacyDateTimeEndIndex + 1);
                var shimTimeZoneDesc = shimDateDesc.Skip(shimDateTimeEndIndex + 1);
                Assert.AreEqual(new string(legacyTimeZoneDesc.ToArray()), new string(shimTimeZoneDesc.ToArray()));
            }
        }

        [Test]
        [TestCase("12345", "Dog the Bounty Hunter")]
        [TestCase("12345", "dog the bounty hunter")]
        [TestCase("12345", "   Dog the Bounty Hunter   ")]
        [TestCase("   12345   ", "Dog the Bounty Hunter")]
        [TestCase("12345", "Cat the Bounty Hunter")]
        [TestCase("1234", "Dog the Bounty Hunter")]
        public void IsSameTradeline_Always_ReturnsSameValueForShimAndLegacyLiability(string scenarioAccNum, string scenarioComNm)
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            var accountNumber = "12345";
            var companyName = "Dog the Bounty Hunter";
            shim.AccNum = accountNumber;
            shim.ComNm = companyName;
            legacyLiability.AccNum = accountNumber;
            legacyLiability.ComNm = companyName;

            Assert.AreEqual(
                legacyLiability.IsSameTradeline(scenarioAccNum, scenarioComNm),
                shim.IsSameTradeline(scenarioAccNum, scenarioComNm));
        }

        [Test]
        public void LiabilityCreationAudit_Always_ReturnsSameValueForShimAndLegacyLiability()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            var username = "username";
            var login = "login";
            shim.LiabilityCreationAudit(username, login);
            legacyLiability.LiabilityCreationAudit(username, login);

            this.AssertPmlAuditTrailXmlEqual(legacyLiability.PmlAuditTrailXmlContent, shim.PmlAuditTrailXmlContent);
        }

        [Test]
        public void SetRecordId_Always_UpdatesRecordIdForShimAndLegacyLiability()
        {
            var tuple = GetShimAndLegacyLiability(null);
            var shim = tuple.Item1;
            var shimId = shim.RecordId;
            var legacyLiability = tuple.Item2;
            var legacyLiabilityId = legacyLiability.RecordId;
            // Idk what format they expect the string to be so here we go...
            var newIdStr = Encoding.ASCII.GetString(Guid.NewGuid().ToByteArray());
            var newId = CLiaRegular.ConvertStringToGuid(newIdStr);

            shim.SetRecordId(newIdStr);
            legacyLiability.SetRecordId(newIdStr);

            Assert.AreEqual(newId, shim.RecordId);
            Assert.AreEqual(newId, legacyLiability.RecordId);
        }

        [Test]
        public void IsSubjectProperty1stMortgage_GetAfterSetToTrue_ClearsOtherFirstMortgageLiaAndReturnsTrue()
        {
            var entities = LiabilityCollectionShimTest.GetDefaultLiabilities(3);
            entities[1].Value.IsSp1stMtgData = true;
            foreach (var entity in entities)
            {
                // Use all mortgage liabilities.
                entity.Value.DebtType = E_DebtT.Mortgage;
            }

            var collectionShim = LiabilityCollectionShimTest.InitializeCollection(null, entities);

            var secondRecord = collectionShim.GetRegularRecordAt(1);
            Assert.AreEqual(true, secondRecord.IsSubjectProperty1stMortgage);
            var newRecord = collectionShim.AddRegularRecord();
            newRecord.DebtT = E_DebtRegularT.Mortgage;
            // This should clear all other liabilities.
            newRecord.IsSubjectProperty1stMortgage = true;

            Assert.AreEqual(false, secondRecord.IsSubjectProperty1stMortgage);
            Assert.AreEqual(true, newRecord.IsSubjectProperty1stMortgage);
        }

        [Test]
        public void IsSubjectProperty1stMortgage_GetWhenNotSubjectPropertyMortgage_AlwaysReturnsFalse()
        {
            var entities = LiabilityCollectionShimTest.GetDefaultLiabilities(1);
            var liability = entities.First();
            liability.Value.DebtType = E_DebtT.Revolving;
            var collectionShim = LiabilityCollectionShimTest.InitializeCollection(null, entities);

            var record = collectionShim.GetRegularRecordAt(0);
            Assert.AreEqual(false, record.IsSubjectPropertyMortgage);
            // Even though we are setting it to true, we expect the value to be
            // false because it's not a mortgage liability.
            record.IsSubjectProperty1stMortgage = true;

            Assert.AreEqual(false, record.IsSubjectProperty1stMortgage);
        }

        [Test]
        public void DataRow_AfterFlush_IsConsistentWithLegacy()
        {
            var entities = LiabilityCollectionShimTest.GetDefaultLiabilities(1);
            var liability = entities.First();
            var collectionContainer = LiabilityCollectionShimTest.GetCollectionContainer(entities);
            var converter = new LosConvert();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(liability.Key).Returns(Ownership.Borrower);
            var collectionShim = new LiabilityCollectionShim(
                (IShimContainer)collectionContainer,
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimAppDataProvider>(),
                ownershipManager,
                converter);
            var dataSetCopy = collectionShim.GetDataSet().Copy();

            var legacy = new CLiaRegular(dataSetCopy, converter, liability.Key, null);
            var shim = collectionShim.GetRegularRecordAt(0);

            // Set the fields in an identical fashion.
            this.SetAvailableFields(legacy);
            this.SetAvailableFields(shim);

            // Get the data rows.
            var legacyRow = dataSetCopy.Tables[0].Rows[0];

            // Assert that the values are the same after shim flush.
            collectionShim.Flush();

            var shimRow = collectionShim.GetDataSet().Tables[0].Rows[0];
            AssertLiabilityRowsEqual(legacyRow, shimRow);
        }

        [Test]
        public void DebtT_ChangedFromMortgageWithLinkedReoToRevolving_RemovesAssociation()
        {
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var containerFactory = new LoanLqbCollectionContainerFactory();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(Guid.Empty, Guid.Empty, null));
            var collectionContainer = containerFactory.Create(
                providerFactory.CreateEmptyProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);
            collectionContainer.RegisterCollections(new[]
            {
                nameof(ILoanLqbCollectionContainer.ConsumerLiabilities),
                nameof(ILoanLqbCollectionContainer.Liabilities),
                nameof(ILoanLqbCollectionContainer.RealProperties),
                nameof(ILoanLqbCollectionContainer.ConsumerRealProperties),
                nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities),
            });
            collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());
            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            liability.DebtType = E_DebtT.Mortgage;
            var liabilityId = collectionContainer.Add(
                DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty),
                liability);
            var realProperty = new RealProperty();
            var realPropertyId = collectionContainer.Add(
                DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty),
                realProperty);
            ((IShimContainer)collectionContainer).AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);
            var collectionShim = new LiabilityCollectionShim(
                collectionContainer as IShimContainer,
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimAppDataProvider>(),
                Substitute.For<IShimOwnershipManager<Guid>>(),
                new LosConvert());
            var liabilityShim = collectionShim.GetRegRecordOf(liabilityId.Value);
            Assert.AreEqual(realPropertyId.Value, liabilityShim.MatchedReRecordId);
            Assert.AreEqual(1, collectionContainer.RealPropertyLiabilities.Count);

            liabilityShim.DebtT = E_DebtRegularT.Revolving;

            Assert.AreEqual(Guid.Empty, liabilityShim.MatchedReRecordId);
            Assert.AreEqual(0, collectionContainer.RealPropertyLiabilities.Count);
        }

        public static void AssertLiabilityRowsEqual(DataRow legacyRow, DataRow shimRow)
        {
            Assert.AreEqual(legacyRow["RecordId"], shimRow["RecordId"]);
            Assert.AreEqual(legacyRow["MatchedReRecordId"], shimRow["MatchedReRecordId"]);
            Assert.AreEqual(legacyRow["OwnerT"], shimRow["OwnerT"]);
            Assert.AreEqual(legacyRow["DebtT"], shimRow["DebtT"]);
            Assert.AreEqual(legacyRow["ComNm"], shimRow["ComNm"]);
            Assert.AreEqual(legacyRow["ComAddr"], shimRow["ComAddr"]);
            Assert.AreEqual(legacyRow["ComCity"], shimRow["ComCity"]);
            Assert.AreEqual(legacyRow["ComState"], shimRow["ComState"]);
            Assert.AreEqual(legacyRow["ComZip"], shimRow["ComZip"]);
            Assert.AreEqual(legacyRow["ComPhone"], shimRow["ComPhone"]);
            Assert.AreEqual(legacyRow["ComFax"], shimRow["ComFax"]);
            Assert.AreEqual(legacyRow["AccNum"], shimRow["AccNum"]);
            Assert.AreEqual(legacyRow["AccNm"], shimRow["AccNm"]);
            Assert.AreEqual(legacyRow["Bal"], shimRow["Bal"]);
            Assert.AreEqual(legacyRow["Pmt"], shimRow["Pmt"]);
            Assert.AreEqual(legacyRow["R"], shimRow["R"]);
            Assert.AreEqual(legacyRow["OrigTerm"], shimRow["OrigTerm"]);
            Assert.AreEqual(legacyRow["Due"], shimRow["Due"]);
            Assert.AreEqual(legacyRow["RemainMons"], shimRow["RemainMons"]);
            Assert.AreEqual(legacyRow["WillBePdOff"], shimRow["WillBePdOff"]);
            Assert.AreEqual(legacyRow["NotUsedInRatio"], shimRow["NotUsedInRatio"]);
            Assert.AreEqual(legacyRow["IsPiggyBack"], shimRow["IsPiggyBack"]);
            Assert.AreEqual(legacyRow["Late30"], shimRow["Late30"]);
            Assert.AreEqual(legacyRow["Late60"], shimRow["Late60"]);
            Assert.AreEqual(legacyRow["Late90Plus"], shimRow["Late90Plus"]);
            Assert.AreEqual(legacyRow["IncInReposession"], shimRow["IncInReposession"]);
            Assert.AreEqual(legacyRow["IncInBankruptcy"], shimRow["IncInBankruptcy"]);
            Assert.AreEqual(legacyRow["IncInForeclosure"], shimRow["IncInForeclosure"]);
            Assert.AreEqual(legacyRow["ExcFromUnderwriting"], shimRow["ExcFromUnderwriting"]);
            Assert.AreEqual(legacyRow["VerifSentD"], shimRow["VerifSentD"]);
            Assert.AreEqual(legacyRow["VerifRecvD"], shimRow["VerifRecvD"]);
            Assert.AreEqual(legacyRow["VerifExpD"], shimRow["VerifExpD"]);
            Assert.AreEqual(legacyRow["VerifReorderedD"], shimRow["VerifReorderedD"]);
            Assert.AreEqual(legacyRow["Desc"], shimRow["Desc"]);
            Assert.AreEqual(legacyRow["Attention"], shimRow["Attention"]);
            Assert.AreEqual(legacyRow["PrepD"], shimRow["PrepD"]);
            Assert.AreEqual(legacyRow["IsSeeAttachment"], shimRow["IsSeeAttachment"]);
            Assert.AreEqual(legacyRow["OrderRankValue"], shimRow["OrderRankValue"]);
            Assert.AreEqual(legacyRow["DebtJobExpenseT"], shimRow["DebtJobExpenseT"]);
            Assert.AreEqual(legacyRow["OrigDebtAmt"], shimRow["OrigDebtAmt"]);
            Assert.AreEqual(legacyRow["AutoYearMake"], shimRow["AutoYearMake"]);
            Assert.AreEqual(legacyRow["IsMortFHAInsured"], shimRow["IsMortFHAInsured"]);
            Assert.AreEqual(legacyRow["IsForAuto"], shimRow["IsForAuto"]);
            Assert.AreEqual(legacyRow["ReconcileStatusT"], shimRow["ReconcileStatusT"]);
            Assert.AreEqual(legacyRow["PmlAuditTrailXmlContent"], shimRow["PmlAuditTrailXmlContent"]);
            Assert.AreEqual(legacyRow["IsSubjectProperty1stMortgage"], shimRow["IsSubjectProperty1stMortgage"]);
            Assert.AreEqual(legacyRow["VerifSigningEmployeeId"], shimRow["VerifSigningEmployeeId"]);
            Assert.AreEqual(legacyRow["VerifSignatureImgId"], shimRow["VerifSignatureImgId"]);
            Assert.AreEqual(legacyRow["PayoffAmtLckd"], shimRow["PayoffAmtLckd"]);
            Assert.AreEqual(legacyRow["PayoffAmt"], shimRow["PayoffAmt"]);
            Assert.AreEqual(legacyRow["PayoffTimingLckd"], shimRow["PayoffTimingLckd"]);
            Assert.AreEqual(legacyRow["PayoffTiming"], shimRow["PayoffTiming"]);
        }

        private void SetAvailableFields(ILiabilityRegular liability)
        {
            liability.AccNm = "Account Name";
            liability.AccNum = "Account Number";
            liability.Attention = "";
            liability.AutoYearMake = "1984";
            liability.Bal_rep = "1.23";
            liability.ComAddr = "123 Main St.";
            liability.ComCity = "Costa Mesa";
            liability.ComFax = "123-456-7890";
            liability.ComNm = "Mustard, Inc.";
            liability.ComPhone = "000-111-2222";
            liability.ComState = "CA";
            liability.ComZip = "12345";
            liability.DebtT = E_DebtRegularT.Mortgage;
            liability.Desc = "Description";
            liability.Due_rep = "14";
            liability.ExcFromUnderwriting = true;
            //liability.FullyIndexedPITIPayment
            //liability.H4HIsRetirement
            liability.IncInBankruptcy = false;
            liability.IncInForeclosure = true;
            liability.IncInReposession = false;
            liability.IsForAuto = true;
            liability.IsMortFHAInsured = false;
            liability.IsPiggyBack = true;
            liability.IsSeeAttachment = false;
            liability.IsSubjectProperty1stMortgage = false; // There can only be one per collection set to true, so make this false.
            //liability.IsSubjectPropertyMortgage -- this will be true for mortgage with matched real property Guid.Empty
            liability.Late30_rep = "1";
            liability.Late60_rep = "2";
            liability.Late90Plus_rep = "3";
            liability.MatchedReRecordId = Guid.Empty;
            liability.NotUsedInRatio = true;
            liability.OrigDebtAmt_rep = "$1,234.56";
            liability.OrigTerm_rep = "360";
            liability.OwnerT = E_LiaOwnerT.Borrower;
            liability.PayoffAmt_rep = "555.55";
            liability.PayoffAmtLckd = true;
            liability.PayoffTiming = E_Timing.At_Closing;
            liability.PayoffTimingLckd = true;
            liability.PmlAuditTrailXmlContent = "";
            liability.Pmt_rep = "$7,890.12";
            liability.PrepD = CDateTime.Create(DateTime.Today);
            liability.ReconcileStatusT = E_LiabilityReconcileStatusT.Matched;
            liability.RemainMons_rep = "(R)";
            liability.R_rep = "3.275%";
            liability.VerifExpD = CDateTime.Create(DateTime.Today.AddDays(1));
            liability.VerifRecvD = CDateTime.Create(DateTime.Today.AddDays(2));
            liability.VerifReorderedD = CDateTime.Create(DateTime.Today.AddDays(3));
            liability.VerifSentD = CDateTime.Create(DateTime.Today.AddDays(4));
            liability.WillBePdOff = true;
        }

        internal static Tuple<LiabilityRegularShim, CLiaRegular> GetShimAndLegacyLiability(LosConvert converter, IShimContainer shimContainer = null, ILiabilityDefaultsProvider defaultsProvider = null, CAppBase app = null)
        {
            converter = converter ?? new LosConvert();

            var entities = LiabilityCollectionShimTest.GetDefaultLiabilities(1);
            var liability = entities.First();

            bool shimContainerWasCreated = shimContainer == null;
            if (shimContainer == null)
            {
                shimContainer = LiabilityCollectionShimTest.GetCollectionContainer(entities, app) as IShimContainer;
            }

            IShimOwnershipManager<Guid> ownershipManager = new ShimOwnershipManager<DataObjectKind.Liability, DataObjectKind.ConsumerLiabilityAssociation, Liability, ConsumerLiabilityAssociation>(
                shimContainer,
                (app?.aBConsumerId ?? Guid.Empty).ToIdentifier<DataObjectKind.Consumer>(),
                (app?.aCConsumerId ?? Guid.Empty).ToIdentifier<DataObjectKind.Consumer>(),
                (app?.aAppId ?? Guid.Empty).ToIdentifier<DataObjectKind.LegacyApplication>(),
                (c, l, a) => new ConsumerLiabilityAssociation(c, l, a));
            if (!shimContainerWasCreated)
            {
                shimContainer.Add(
                    ownershipManager.BorrowerId,
                    DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(liability.Key),
                    liability.Value);
            }

            var collectionShim = new LiabilityCollectionShim(
                shimContainer,
                defaultsProvider ?? Substitute.For<ILiabilityDefaultsProvider>(),
                app,
                ownershipManager,
                converter);
            var dataSet = collectionShim.GetDataSet();
            var liaRegular = new CLiaRegular(dataSet, converter, liability.Key, app);

            return Tuple.Create((LiabilityRegularShim)collectionShim.GetRegRecordOf(liability.Key), liaRegular);
        }
    }
}
