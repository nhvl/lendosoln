﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class ShimOwnershipManagerTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId =
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));

        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId =
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CrossAppBorrowerId =
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("33333333-3333-3333-3333-333333333333"));

        private static readonly DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> PublicRecordId =
            DataObjectIdentifier<DataObjectKind.PublicRecord, Guid>.Create(new Guid("44444444-4444-4444-4444-444444444444"));

        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> ApplicationId =
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("55555555-5555-5555-5555-555555555555"));

        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> CrossApplicationId =
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("66666666-6666-6666-6666-666666666666"));

        private ILoanLqbCollectionContainer aggregate;

        [SetUp]
        public void SetUp()
        {
            // Ensure that each unit test begins with a new aggregate.
            this.aggregate = null;
        }

        [Test]
        public void GetOwnership_AssociatedWithBorrower_ReturnsBorrower()
        {
            var association = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            var ownership = this.GetOwnershipManager(association);

            var ownershipType = ownership.GetOwnership(PublicRecordId.Value);

            Assert.AreEqual(E_PublicRecordOwnerT.Borrower, (E_PublicRecordOwnerT)ownershipType);
        }

        [Test]
        public void GetOwnership_AssociatedWithCoborrower_ReturnsCoborrower()
        {
            var association = CreateAssociation(CoborrowerId, PublicRecordId, ApplicationId);
            var ownership = this.GetOwnershipManager(association);

            var ownershipType = ownership.GetOwnership(PublicRecordId.Value);

            Assert.AreEqual(E_PublicRecordOwnerT.CoBorrower, (E_PublicRecordOwnerT)ownershipType);
        }

        [Test]
        public void GetOwnership_AssociatedWithBothBorrowers_ReturnsJoint()
        {
            var borrowerAssociation = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            var coborrowerAssociation = CreateAssociation(CoborrowerId, PublicRecordId, ApplicationId);
            var ownership = this.GetOwnershipManager(borrowerAssociation, coborrowerAssociation);

            var ownershipType = ownership.GetOwnership(PublicRecordId.Value);

            Assert.AreEqual(E_PublicRecordOwnerT.Joint, (E_PublicRecordOwnerT)ownershipType);
        }

        [Test]
        public void GetOwnership_AssociatedWithBorrowerAndCrossApp_ReturnsBorrower()
        {
            var borrowerAssociation = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            var crossAppAssociation = CreateAssociation(CrossAppBorrowerId, PublicRecordId, CrossApplicationId);
            var ownership = this.GetOwnershipManager(borrowerAssociation, crossAppAssociation);

            var ownershipType = ownership.GetOwnership(PublicRecordId.Value);

            Assert.AreEqual(E_PublicRecordOwnerT.Borrower, (E_PublicRecordOwnerT)ownershipType);
        }

        [Test]
        public void SetOwnership_ToBorrowerNoExistingAssociation_AddsNewPrimaryAssociation()
        {
            var associationSet = this.GetAssociations();
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Borrower);

            Assert.AreEqual(1, associationSet.Count);
            var association = associationSet.Values.First();
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(PublicRecordId, association.PublicRecordId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(ApplicationId, association.AppId);
        }

        [Test]
        public void SetOwnership_ToBorrowerWithPrimaryOwnershipFromDifferentApp_ThrowsException()
        {
            var crossAppPrimaryAssociation = CreateAssociation(CrossAppBorrowerId, PublicRecordId, CrossApplicationId);
            crossAppPrimaryAssociation.IsPrimary = true;
            var ownership = this.GetOwnershipManager(crossAppPrimaryAssociation);

            Assert.Throws<CBaseException>(() => ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Borrower));
        }

        [Test]
        public void SetOwnership_ToCoborrowerNoExistingAssociation_AddsNewPrimaryAssociation()
        {
            var associationSet = this.GetAssociations();
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.CoBorrower);

            Assert.AreEqual(1, associationSet.Count);
            var association = associationSet.Values.First();
            Assert.AreEqual(CoborrowerId, association.ConsumerId);
            Assert.AreEqual(PublicRecordId, association.PublicRecordId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(ApplicationId, association.AppId);
        }

        [Test]
        public void SetOwnership_ToJointNoExistingAssociation_AddsPrimaryBAndCAssociation()
        {
            var associationSet = this.GetAssociations();
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Joint);

            Assert.AreEqual(2, associationSet.Count);
            var borrowerAssociation = associationSet.Values.First(a => a.ConsumerId == BorrowerId);
            Assert.AreEqual(BorrowerId, borrowerAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, borrowerAssociation.PublicRecordId);
            Assert.AreEqual(true, borrowerAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, borrowerAssociation.AppId);
            var coborrowerAssociation = associationSet.Values.First(a => a.ConsumerId == CoborrowerId);
            Assert.AreEqual(CoborrowerId, coborrowerAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, coborrowerAssociation.PublicRecordId);
            Assert.AreEqual(false, coborrowerAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, coborrowerAssociation.AppId);
        }

        [Test]
        public void SetOwnership_ToJointExistingPrimaryB_AddsCAssociation()
        {
            var existingAssociation = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            existingAssociation.IsPrimary = true;
            var associationSet = this.GetAssociations(existingAssociation);
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Joint);

            Assert.AreEqual(2, associationSet.Count);
            var borrowerAssociation = associationSet.Values.First(a => a.ConsumerId == BorrowerId);
            Assert.AreEqual(BorrowerId, borrowerAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, borrowerAssociation.PublicRecordId);
            Assert.AreEqual(true, borrowerAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, borrowerAssociation.AppId);
            var coborrowerAssociation = associationSet.Values.First(a => a.ConsumerId == CoborrowerId);
            Assert.AreEqual(CoborrowerId, coborrowerAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, coborrowerAssociation.PublicRecordId);
            Assert.AreEqual(false, coborrowerAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, coborrowerAssociation.AppId);
        }

        [Test]
        public void SetOwnership_ToJointExistingPrimaryC_AddsBAssociation()
        {
            var existingAssociation = CreateAssociation(CoborrowerId, PublicRecordId, ApplicationId);
            existingAssociation.IsPrimary = true;
            var associationSet = this.GetAssociations(existingAssociation);
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Joint);

            Assert.AreEqual(2, associationSet.Count);
            var borrowerAssociation = associationSet.Values.First(a => a.ConsumerId == BorrowerId);
            Assert.AreEqual(BorrowerId, borrowerAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, borrowerAssociation.PublicRecordId);
            Assert.AreEqual(false, borrowerAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, borrowerAssociation.AppId);
            var coborrowerAssociation = associationSet.Values.First(a => a.ConsumerId == CoborrowerId);
            Assert.AreEqual(CoborrowerId, coborrowerAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, coborrowerAssociation.PublicRecordId);
            Assert.AreEqual(true, coborrowerAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, coborrowerAssociation.AppId);
        }

        [Test]
        public void SetOwnership_ToCWithExistingBAssociation_LeavesSinglePrimaryCAssociation()
        {
            var existingAssociation = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            var associationSet = this.GetAssociations(existingAssociation);
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.CoBorrower);

            Assert.AreEqual(1, associationSet.Count);
            var newAssociation = associationSet.Values.First();
            Assert.AreEqual(CoborrowerId, newAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, newAssociation.PublicRecordId);
            Assert.AreEqual(true, newAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, newAssociation.AppId);
        }

        [Test]
        public void SetOwnership_ToBWithExistingCAssociation_LeavesSinglePrimaryBAssociation()
        {
            var existingAssociation = CreateAssociation(CoborrowerId, PublicRecordId, ApplicationId);
            var associationSet = this.GetAssociations(existingAssociation);
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Borrower);

            Assert.AreEqual(1, associationSet.Count);
            var newAssociation = associationSet.Values.First();
            Assert.AreEqual(BorrowerId, newAssociation.ConsumerId);
            Assert.AreEqual(PublicRecordId, newAssociation.PublicRecordId);
            Assert.AreEqual(true, newAssociation.IsPrimary);
            Assert.AreEqual(ApplicationId, newAssociation.AppId);
        }

        [Test]
        public void SetOwnership_ToBWithExistingBAndPrimaryCAssociation_LeavesSinglePrimaryBAssociation()
        {
            var existingCoborrowerAssociation = CreateAssociation(CoborrowerId, PublicRecordId, ApplicationId);
            var existingBorrowerAssociation = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            var associationSet = this.GetAssociations(existingCoborrowerAssociation, existingBorrowerAssociation);
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Borrower);

            Assert.AreEqual(1, associationSet.Count);
            var association = associationSet.Values.First();
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(PublicRecordId, association.PublicRecordId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(ApplicationId, association.AppId);
        }

        [Test]
        public void SetOwnership_ToCWithExistingPrimaryBAndCAssociation_LeavesSinglePrimaryCAssociation()
        {
            var existingBorrowerAssociation = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            existingBorrowerAssociation.IsPrimary = true;
            var existingCoborrowerAssociation = CreateAssociation(CoborrowerId, PublicRecordId, ApplicationId);
            var associationSet = this.GetAssociations(existingBorrowerAssociation, existingCoborrowerAssociation);
            var ownership = this.GetOwnershipManager(associationSet);

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.CoBorrower);

            Assert.AreEqual(1, associationSet.Count);
            var association = associationSet.Values.First();
            Assert.AreEqual(CoborrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(ApplicationId, association.AppId);
        }

        [Test]
        public void SetOwnership_ToBWithExistingPrimaryBAssociation_LeavesSinglePrimaryBAssociation()
        {
            var existingBorrowerAssociation = CreateAssociation(BorrowerId, PublicRecordId, ApplicationId);
            existingBorrowerAssociation.IsPrimary = true;
            var associationSet = this.GetAssociations(existingBorrowerAssociation);
            var ownership = this.GetOwnershipManager();

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.Borrower);

            Assert.AreEqual(1, associationSet.Count);
            var association = associationSet.Values.First();
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(PublicRecordId, association.PublicRecordId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(ApplicationId, association.AppId);
        }

        [Test]
        public void SetOwnership_ToCWithExistingPrimaryCAssociation_LeavesSinglePrimaryCAssociation()
        {
            var existingCoborrowerAssociation = CreateAssociation(CoborrowerId, PublicRecordId, ApplicationId);
            existingCoborrowerAssociation.IsPrimary = true;
            var associationSet = this.GetAssociations(existingCoborrowerAssociation);
            var ownership = this.GetOwnershipManager();

            ownership.SetOwnership(PublicRecordId.Value, (Ownership)E_PublicRecordOwnerT.CoBorrower);

            Assert.AreEqual(1, associationSet.Count);
            var association = associationSet.Values.First();
            Assert.AreEqual(CoborrowerId, association.ConsumerId);
            Assert.AreEqual(PublicRecordId, association.PublicRecordId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(ApplicationId, association.AppId);
        }

        [Test]
        public void SwapBorrowers()
        {
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var applicationId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());

            Func<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.PublicRecord, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, ConsumerPublicRecordAssociation> func = null;
            var ownershipManager = new ShimOwnershipManager<DataObjectKind.PublicRecord, DataObjectKind.ConsumerPublicRecordAssociation, PublicRecord, ConsumerPublicRecordAssociation>(
                null,
                borrowerId,
                coborrowerId,
                applicationId,
                func);

            Assert.AreNotEqual(borrowerId, coborrowerId);
            Assert.AreEqual(borrowerId, ownershipManager.BorrowerId);
            Assert.AreEqual(coborrowerId, ownershipManager.CoborrowerId);

            ownershipManager.SwapBorrowers();

            Assert.AreEqual(borrowerId, ownershipManager.CoborrowerId);
            Assert.AreEqual(coborrowerId, ownershipManager.BorrowerId);
        }

        private static ConsumerPublicRecordAssociation CreateAssociation(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> ownerId,
            DataObjectIdentifier<DataObjectKind.PublicRecord, Guid> publicRecordId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> applicationId)
        {
            return new ConsumerPublicRecordAssociation(ownerId, publicRecordId, applicationId);
        }

        private IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> GetAssociations(
            params ConsumerPublicRecordAssociation[] associations)
        {
            var aggregate = this.GetAggregate();

            foreach (var association in associations)
            {
                if (aggregate.PublicRecords.ContainsKey(association.PublicRecordId))
                {
                    aggregate.AddOwnership(association.ConsumerId, association.PublicRecordId);
                }
                else
                {
                    aggregate.Add(association.ConsumerId, association.PublicRecordId, new PublicRecord());
                }
            }

            return aggregate.ConsumerPublicRecords;
        }

        private ShimOwnershipManager<DataObjectKind.PublicRecord, DataObjectKind.ConsumerPublicRecordAssociation, PublicRecord, ConsumerPublicRecordAssociation> GetOwnershipManager(
            params ConsumerPublicRecordAssociation[] associations)
        {
            var associationSet = this.GetAssociations(associations);
            return this.GetOwnershipManager(associationSet);
        }

        private ShimOwnershipManager<DataObjectKind.PublicRecord, DataObjectKind.ConsumerPublicRecordAssociation, PublicRecord, ConsumerPublicRecordAssociation> GetOwnershipManager(
            IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> associationSet)
        {
            var aggregate = this.GetAggregate();

            return new ShimOwnershipManager<DataObjectKind.PublicRecord, DataObjectKind.ConsumerPublicRecordAssociation, PublicRecord, ConsumerPublicRecordAssociation>(
                aggregate as IShimContainer,
                BorrowerId,
                CoborrowerId,
                ApplicationId,
                (consumerId, publicRecordId, appId) => new ConsumerPublicRecordAssociation(consumerId, publicRecordId, appId));
        }

        private ILoanLqbCollectionContainer GetAggregate()
        {
            if (aggregate == null)
            {
                var dummyLoanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(Guid.Empty);
                var dummyBrokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty);

                var providerFactory = new LoanLqbCollectionProviderFactory();
                var collectionProvider = providerFactory.CreateEmptyProvider();
                var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
                var legacyAppConsumers = Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(ApplicationId, BorrowerId, CoborrowerId);
                legacyAppConsumers.AddBorrower(CrossApplicationId, CrossAppBorrowerId, isPrimary: true);
                loanData.LegacyApplicationConsumers.Returns(legacyAppConsumers);

                var containerFactory = new LoanLqbCollectionContainerFactory();
                this.aggregate = containerFactory.Create(
                    collectionProvider,
                    loanData,
                    Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                    null);

                this.aggregate.RegisterCollections(LoanLqbCollectionContainer.LoanCollections);
                this.aggregate.Load(dummyBrokerId, null, null);
            }

            return this.aggregate;
        }
    }
}
