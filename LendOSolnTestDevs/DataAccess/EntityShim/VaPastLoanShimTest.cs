﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class VaPastLoanShimTest
    {
        private DataSet data;
        private global::DataAccess.IVaPastLoan shim;

        private IEnumerable FormatTargetEnumerable()
        {
            return RangeLimits.FormatTarget.Enumerable();
        }

        [Test]
        public void CityTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.City));
            this.CityTest(this.shim, old, old.City);

            this.CreatePastLoan(los);
            this.CityTest(this.shim, old, string.Empty);

            this.CreatePastLoan(los);
            this.CityTest(this.shim, old, null);
        }

        [Test]
        public void CityStateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.CityState));
            this.CityStateTest(this.shim, old, old.CityState);

            this.CreatePastLoan(los);
            this.CityStateTest(this.shim, old, string.Empty);

            this.CreatePastLoan(los);
            this.CityStateTest(this.shim, old, null);
        }

        [Test]
        public void DateOfLoanTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.DateOfLoan));
            this.DateOfLoanTest(this.shim, old, old.DateOfLoan);

            this.CreatePastLoan(los);
            this.DateOfLoanTest(this.shim, old, string.Empty);

            this.CreatePastLoan(los);
            this.DateOfLoanTest(this.shim, old, null);
        }

        [Test]
        public void IsStillOwnedTest()
        {
            this.CreatePastLoan(null);
            var old = new CVaPastL(this.data, null, this.shim.RecordId);

            this.IsStillOwnedTest(this.shim, old, true);

            this.CreatePastLoan(null);
            this.IsStillOwnedTest(this.shim, old, false);

            this.CreatePastLoan(null);
            this.IsStillOwnedTest(this.shim, old, null);
        }

        [Test]
        public void LoanDateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.LoanD_rep));
            this.LoanDateTest(this.shim, old, old.LoanD_rep);

            this.CreatePastLoan(los);
            this.LoanDateTest(this.shim, old, string.Empty);

            this.CreatePastLoan(los);
            this.LoanDateTest(this.shim, old, null);
        }

        [Test]
        public void PropertySoldDateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.PropSoldD_rep));
            this.PropertySoldDateTest(this.shim, old, old.PropSoldD_rep);

            this.CreatePastLoan(los);
            this.PropertySoldDateTest(this.shim, old, string.Empty);

            this.CreatePastLoan(los);
            this.PropertySoldDateTest(this.shim, old, null);
        }

        [Test]
        public void StAddrTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.StAddr));
            this.StAddrTest(this.shim, old, old.StAddr);

            this.CreatePastLoan(los);
            this.StAddrTest(this.shim, old, string.Empty);

            this.CreatePastLoan(los);
            this.StAddrTest(this.shim, old, null);
        }

        [Test]
        public void StateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.State));
            this.StateTest(this.shim, old, old.State);

            this.CreatePastLoan(los);
            this.StateTest(this.shim, old, string.Empty);

            // don't test null as it requires hitting the database for the LoggerManager initialization
        }

        [Test]
        public void VaLoanNumTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.VaLoanNum));
            this.VaLoanNumTest(this.shim, old, old.VaLoanNum);

            this.CreatePastLoan(los);
            this.VaLoanNumTest(this.shim, old, string.Empty);

            this.CreatePastLoan(los);
            this.VaLoanNumTest(this.shim, old, null);
        }

        [Test]
        public void ZipTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);

            this.CreatePastLoan(los);
            var old = new CVaPastL(this.data, los, this.shim.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.Zip));
            this.ZipTest(this.shim, old, old.Zip);

            this.CreatePastLoan(los);
            this.ZipTest(this.shim, old, string.Empty);

            // don't test null as it requires hitting the database for the LoggerManager initialization
        }

        private void CityTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.City = value;
            old.City = value;
            Assert.AreEqual(old.City, shim.City);
        }

        private void CityStateTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.CityState = value;
            old.CityState = value;
            Assert.AreEqual(old.CityState, shim.CityState);
        }

        private void DateOfLoanTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.DateOfLoan = value;
            old.DateOfLoan = value;
            Assert.AreEqual(old.DateOfLoan, shim.DateOfLoan);
        }

        private void IsStillOwnedTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, bool? value)
        {
            var tristate = E_TriState.Blank;
            if (value != null)
            {
                tristate = value.Value ? E_TriState.Yes : E_TriState.No;
            }

            shim.IsStillOwned = tristate;
            old.IsStillOwned = tristate;
            Assert.AreEqual(old.IsStillOwned, shim.IsStillOwned);
        }

        private void LoanDateTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.LoanD_rep = value;
            old.LoanD_rep = value;
            Assert.AreEqual(old.LoanD_rep, shim.LoanD_rep);
        }

        private void PropertySoldDateTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.PropSoldD_rep = value;
            old.PropSoldD_rep = value;
            Assert.AreEqual(old.PropSoldD_rep, shim.PropSoldD_rep);
        }

        private void StAddrTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.StAddr = value;
            old.StAddr = value;
            Assert.AreEqual(old.StAddr, shim.StAddr);
        }

        private void StateTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.State = value;
            old.State = value;
            Assert.AreEqual(old.State, shim.State);
        }

        private void VaLoanNumTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.VaLoanNum = value;
            old.VaLoanNum = value;
            Assert.AreEqual(old.VaLoanNum, shim.VaLoanNum);
        }

        private void ZipTest(global::DataAccess.IVaPastLoan shim, global::DataAccess.IVaPastLoan old, string value)
        {
            shim.Zip = value;
            old.Zip = value;
            Assert.AreEqual(old.Zip, shim.Zip);
        }

        private void CreatePastLoan(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = VaPastLoanCollectionShimTest.InitializeCollection(converter);
            this.data = collectionShim.GetDataSet();
            this.shim = collectionShim.GetRegularRecordAt(0);
        }
    }
}
