﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections;
    using System.Data;
    using global::DataAccess;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class PublicRecordShimTest : CollectionItemBase2ShimTest
    {
        private IPublicRecord shim;
        private IPublicRecord shimWithHistory;
        private IPublicRecord shimWithoutHistory;

        private IEnumerable DispositionTypeEnumerable()
        {
            return RangeLimits.DispositionType.Enumerable();
        }

        private IEnumerable CreditPublicRecordTypeEnumerable()
        {
            return RangeLimits.CreditPublicRecordType.Enumerable();
        }

        [Test]
        public void DataRowTest()
        {
            var dr = this.shim.DataRow;
            var table = dr.Table;

            Assert.IsTrue(table.Columns.Contains("RecordId"));
            Assert.IsTrue(table.Columns.Contains("OrderRankValue"));
            Assert.IsTrue(table.Columns.Contains("IdFromCreditReport"));
            Assert.IsTrue(table.Columns.Contains("CourtName"));
            Assert.IsTrue(table.Columns.Contains("ReportedD"));
            Assert.IsTrue(table.Columns.Contains("DispositionD"));
            Assert.IsTrue(table.Columns.Contains("DispositionT"));
            Assert.IsTrue(table.Columns.Contains("BankruptcyLiabilitiesAmount"));
            Assert.IsTrue(table.Columns.Contains("Type"));
            Assert.IsTrue(table.Columns.Contains("LastEffectiveD"));
            Assert.IsTrue(table.Columns.Contains("BkFileD"));
            Assert.IsTrue(table.Columns.Contains("IncludeInPricing"));
            Assert.IsTrue(table.Columns.Contains("OwnerT"));
            Assert.IsTrue(table.Columns.Contains("AuditTrailXmlContent"));
        }

        [Test]
        public void IdFromCreditReportTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreatePublicRecord(converter);
            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.IdFromCreditReportTest(this.shim, old, old.IdFromCreditReport);

            this.CreatePublicRecord(converter);
            this.IdFromCreditReportTest(this.shim, old, string.Empty);

            this.CreatePublicRecord(converter);
            this.IdFromCreditReportTest(this.shim, old, null);
        }

        [Test]
        public void CourtNameTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreatePublicRecord(converter);

            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.CourtNameTest(this.shim, old, old.CourtName);

            this.CreatePublicRecord(converter);
            this.CourtNameTest(this.shim, old, string.Empty);

            this.CreatePublicRecord(converter);
            this.CourtNameTest(this.shim, old, null);
        }

        [Test]
        public void ReportedDateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreatePublicRecord(converter);
            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.ReportedDateTest(this.shim, old, old.ReportedD_rep);

            this.CreatePublicRecord(converter);
            this.ReportedDateTest(this.shim, old, string.Empty);

            this.CreatePublicRecord(converter);
            this.ReportedDateTest(this.shim, old, null);
        }

        [Test]
        public void DispositionDateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreatePublicRecord(converter);
            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.DispositionDateTest(this.shim, old, old.DispositionD_rep);

            this.CreatePublicRecord(converter);
            this.DispositionDateTest(this.shim, old, string.Empty);

            this.CreatePublicRecord(converter);
            this.DispositionDateTest(this.shim, old, null);
        }

        [Test]
        public void DispositionTypeTest([ValueSource("FormatTargetEnumerable")] int formatTarget, [ValueSource("DispositionTypeEnumerable")] int dispType)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreatePublicRecord(converter);

            var type = (E_CreditPublicRecordDispositionType)dispType;

            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);
            this.DispositionTypeTest(this.shim, old, type);
        }

        [Test]
        public void BankruptcyLiabilitiesAmountTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreatePublicRecord(converter);
            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.BankruptcyLiabilitiesAmountTest(this.shim, old, old.BankruptcyLiabilitiesAmount_rep);

            this.CreatePublicRecord(converter);
            this.BankruptcyLiabilitiesAmountTest(this.shim, old, string.Empty);

            this.CreatePublicRecord(converter);
            this.BankruptcyLiabilitiesAmountTest(this.shim, old, null);
        }

        [Test]
        public void TypeTest([ValueSource("FormatTargetEnumerable")] int formatTarget, [ValueSource("CreditPublicRecordTypeEnumerable")] int cprType)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);
            this.CreatePublicRecord(converter);

            var type = (E_CreditPublicRecordType)cprType;

            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);
            this.TypeTest(this.shim, old, type);
        }

        [Test]
        public void LastEffectiveDateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreatePublicRecord(converter);
            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.LastEffectiveDateTest(this.shim, old, old.LastEffectiveD_rep);

            this.CreatePublicRecord(converter);
            this.LastEffectiveDateTest(this.shim, old, string.Empty);

            this.CreatePublicRecord(converter);
            this.LastEffectiveDateTest(this.shim, old, null);
        }

        [Test]
        public void BkFileDateTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreatePublicRecord(converter);
            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.BkFileDateTest(this.shim, old, old.BkFileD_rep);

            this.CreatePublicRecord(converter);
            this.BkFileDateTest(this.shim, old, string.Empty);

            this.CreatePublicRecord(converter);
            this.BkFileDateTest(this.shim, old, null);
        }

        [Test]
        public void IncludeInPricingTest([ValueSource("FormatTargetEnumerable")] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var converter = new LosConvert(target);

            this.CreatePublicRecord(converter);
            var old = new CPublicRecordFields(this.data, converter, this.shim.RecordId);

            this.IncludeInPricingTest(this.shim, old, old.IncludeInPricing);

            this.CreatePublicRecord(converter);
            this.IncludeInPricingTest(this.shim, old, true);

            this.CreatePublicRecord(converter);
            this.IncludeInPricingTest(this.shim, old, false);
        }

        [Test]
        public void OwnerT_Get_RetrievesValueFromShimContainer()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(Guid.Empty).Returns(Ownership.Coborrower);
            var id = DataObjectIdentifier<DataObjectKind.PublicRecord, Guid>.Create(Guid.Empty);
            var shim = PublicRecordShim.Create(shimContainer, recordCollection, null, ownershipManager, id, new LosConvert());

            var ownershipType = shim.OwnerT;

            Assert.AreEqual(E_PublicRecordOwnerT.CoBorrower, ownershipType);
        }

        [Test]
        public void OwnerT_Set_CallsShimContainerMethod()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            var id = DataObjectIdentifier<DataObjectKind.PublicRecord, Guid>.Create(Guid.Empty);
            var shim = PublicRecordShim.Create(shimContainer, recordCollection, null, ownershipManager, id, new LosConvert());

            shim.OwnerT = E_PublicRecordOwnerT.Joint;

            ownershipManager.Received().SetOwnership(Guid.Empty, Ownership.Joint);
        }

        [Test]
        public void IsModified_NoAuditHistoryButHasCurrentChanges_ReturnsFalse()
        {
            this.CreatePublicRecord(null);
            Assert.IsNotNull(this.shimWithoutHistory);

            this.shimWithoutHistory.CourtName = "Star Chamber";

            Assert.IsFalse(this.shimWithoutHistory.IsModified);
        }

        [Test]
        public void IsModified_HasAuditHistoryButNoCurrentChanges_ReturnsTrue()
        {
            this.CreatePublicRecord(null);
            Assert.IsNotNull(this.shimWithHistory);

            Assert.IsTrue(this.shimWithHistory.IsModified);
        }

        protected override void CreateCollection()
        {
            this.CreatePublicRecord(null);
        }

        private void IdFromCreditReportTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, string value)
        {
            shim.IdFromCreditReport = value;
            old.IdFromCreditReport = value;
            Assert.AreEqual(old.IdFromCreditReport, shim.IdFromCreditReport);
        }

        private void CourtNameTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, string value)
        {
            shim.CourtName = value;
            old.CourtName = value;
            Assert.AreEqual(old.CourtName, shim.CourtName);
        }

        private void ReportedDateTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, string value)
        {
            shim.ReportedD_rep = value;
            old.ReportedD_rep = value;
            Assert.AreEqual(old.ReportedD, shim.ReportedD);
            Assert.AreEqual(old.ReportedD_rep, shim.ReportedD_rep);
        }

        private void DispositionDateTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, string value)
        {
            shim.DispositionD_rep = value;
            old.DispositionD_rep = value;
            Assert.AreEqual(old.DispositionD, shim.DispositionD);
            Assert.AreEqual(old.DispositionD_rep, shim.DispositionD_rep);
        }

        private void DispositionTypeTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, E_CreditPublicRecordDispositionType value)
        {
            shim.DispositionT = value;
            old.DispositionT = value;
            Assert.AreEqual(old.DispositionT, shim.DispositionT);
        }

        private void BankruptcyLiabilitiesAmountTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, string value)
        {
            shim.BankruptcyLiabilitiesAmount_rep = value;
            old.BankruptcyLiabilitiesAmount_rep = value;
            Assert.AreEqual(old.BankruptcyLiabilitiesAmount, shim.BankruptcyLiabilitiesAmount);
            Assert.AreEqual(old.BankruptcyLiabilitiesAmount_rep, shim.BankruptcyLiabilitiesAmount_rep);
        }

        private void TypeTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, E_CreditPublicRecordType value)
        {
            shim.Type = value;
            old.Type = value;
            Assert.AreEqual(old.Type, shim.Type);
        }

        private void LastEffectiveDateTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, string value)
        {
            shim.LastEffectiveD_rep = value;
            old.LastEffectiveD_rep = value;
            Assert.AreEqual(old.LastEffectiveD, shim.LastEffectiveD);
            Assert.AreEqual(old.LastEffectiveD_rep, shim.LastEffectiveD_rep);
        }

        private void BkFileDateTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, string value)
        {
            shim.BkFileD_rep = value;
            old.BkFileD_rep = value;
            Assert.AreEqual(old.BkFileD, shim.BkFileD);
            Assert.AreEqual(old.BkFileD_rep, shim.BkFileD_rep);
        }

        private void IncludeInPricingTest(global::DataAccess.IPublicRecord shim, global::DataAccess.IPublicRecord old, bool value)
        {
            shim.IncludeInPricing = value;
            old.IncludeInPricing = value;
            Assert.AreEqual(old.IncludeInPricing, shim.IncludeInPricing);
        }

        private void CreatePublicRecord(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = PublicRecordCollectionShimTest.InitializeCollection(converter);

            for (int i=0; i<collectionShim.CountRegular; ++i)
            {
                var itemShim = collectionShim.GetRegularRecordAt(i);
                if (itemShim.AuditTrailItems != null && itemShim.AuditTrailItems.Count > 0)
                {
                    this.shimWithHistory = itemShim;
                }
                else
                {
                    this.shimWithoutHistory = itemShim;
                }
            }

            this.data = collectionShim.GetDataSet();
            this.collection = collectionShim;
            this.baseShim = this.shim = collectionShim.GetRegularRecordAt(0);
        }
    }
}
