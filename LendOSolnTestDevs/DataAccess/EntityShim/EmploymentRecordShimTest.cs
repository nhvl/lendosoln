﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using global::DataAccess;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    public sealed class EmploymentRecordShimTest : CollectionItemBase2ShimTest
    {
        private IRegularEmploymentRecord shim;
        private IPrimaryEmploymentRecord primary;

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void AllDataPopulatedTest()
        {
            this.CreateEmploymentRecord(null);

            string shimProblem = this.CheckRequiredFields(this.shim);
            Assert.IsNull(shimProblem, shimProblem);

            string primaryProblem = this.CheckRequiredFields(this.primary);
            Assert.IsNull(primaryProblem, primaryProblem);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void AttentionTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.Attention));
            this.AttentionTest(shimOfInterest, old, old.Attention);

            this.CreateEmploymentRecord(converter);
            this.AttentionTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.AttentionTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtEndDTest_NotCurrent([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            // exclude the following because the legacy code is incorrect for them
            if (target == FormatTarget.FreddieMacAUS) return;
            if (target == FormatTarget.GinnieNet) return;

            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplmtEndD_rep));
            this.EmplmtEndDTest(shimOfInterest, old, old.EmplmtEndD_rep);

            this.CreateEmploymentRecord(converter);
            this.EmplmtEndDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplmtEndDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtEndDTest_IsCurrent([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;
            shimOfInterest.IsCurrent = true; // Crazy that you can set IsCurrent property for a regular employment...but anyway the old interface supports this.

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            // Note that some of the FormatTarget values return string.Empty for EmplmtEndD_rep, so we won't don't have an Alert to check for empty string here.
            this.EmplmtEndDTest(shimOfInterest, old, old.EmplmtEndD_rep);

            this.CreateEmploymentRecord(converter);
            this.EmplmtEndDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplmtEndDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtLenInMonthsTest_Primary([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.EmplmtLenInMonths, shimOfInterest.EmplmtLenInMonths);
            Assert.AreEqual(old.EmplmtLenInMonths_rep, shimOfInterest.EmplmtLenInMonths_rep);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtLenInMonthsTest_Previous([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            // exclude the following because the legacy code is incorrect for them
            if (target == FormatTarget.FannieMaeMornetPlus) return;
            if (target == FormatTarget.FreddieMacAUS) return;
            if (target == FormatTarget.GinnieNet) return;

            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.AreEqual(old.EmplmtLenInMonths, shimOfInterest.EmplmtLenInMonths);
            Assert.AreEqual(old.EmplmtLenInMonths_rep, shimOfInterest.EmplmtLenInMonths_rep);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtLenInYearsTest_Primary([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.EmplmtLenInYrs, shimOfInterest.EmplmtLenInYrs);
            Assert.AreEqual(old.EmplmtLenInYrs_rep, shimOfInterest.EmplmtLenInYrs_rep);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtLenInYearsTest_Previous([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            // exclude the following because the legacy code is incorrect for them
            if (target == FormatTarget.FannieMaeMornetPlus) return;
            if (target == FormatTarget.FreddieMacAUS) return;
            if (target == FormatTarget.GinnieNet) return;

            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.AreEqual(old.EmplmtLenInYrs, shimOfInterest.EmplmtLenInYrs);
            Assert.AreEqual(old.EmplmtLenInYrs_rep, shimOfInterest.EmplmtLenInYrs_rep);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtLenTotalMonthsTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplmtLenTotalMonths));
            this.EmplmtLenTotalMonthsTest(shimOfInterest, old, old.EmplmtLenTotalMonths);

            this.CreateEmploymentRecord(converter);
            this.EmplmtLenTotalMonthsTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplmtLenTotalMonthsTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtLenTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplmtLen_rep));
            this.EmplmtLenTest(shimOfInterest, old, old.EmplmtLen_rep);

            this.CreateEmploymentRecord(converter);
            this.EmplmtLenTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplmtLenTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtStartDTest_Primary([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            // exclude the following because the legacy code is incorrect for them
            if (target == FormatTarget.FannieMaeMornetPlus) return;
            if (target == FormatTarget.FreddieMacAUS) return;
            if (target == FormatTarget.GinnieNet) return;

            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmpltStartD_rep));
            this.EmplmtStartDTest(shimOfInterest, old, old.EmpltStartD_rep);

            this.CreateEmploymentRecord(converter);
            this.EmplmtStartDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplmtStartDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtStartDTest_Previous([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            // exclude the following because the legacy code is incorrect for them
            if (target == FormatTarget.FreddieMacAUS) return;
            if (target == FormatTarget.GinnieNet) return;

            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplmtStartD_rep));
            this.EmplmtStartDTest(shimOfInterest, old, old.EmplmtStartD_rep);

            this.CreateEmploymentRecord(converter);
            this.EmplmtStartDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplmtStartDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplmtStatTest()
        {
            this.CreateEmploymentRecord(null);
            Assert.AreEqual(this.shim.EmplmtStat, E_EmplmtStat.Previous);
            Assert.AreEqual(this.primary.EmplmtStat, E_EmplmtStat.Current);
        }

        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        public void EmployeeIdVoeTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                this.CreateEmploymentRecord(converter, true);
                var shimOfInterest = this.shim;

                var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

                Assert.IsFalse(string.IsNullOrEmpty(old.EmployeeIdVoe));
                this.EmployeeIdVoeTest(shimOfInterest, old, old.EmployeeIdVoe);

                this.CreateEmploymentRecord(converter);
                this.EmployeeIdVoeTest(shimOfInterest, old, string.Empty);

                this.CreateEmploymentRecord(converter);
                this.EmployeeIdVoeTest(shimOfInterest, old, null);
            }
        }

        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        public void EmployerCodeVoeTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                this.CreateEmploymentRecord(converter, true);
                var shimOfInterest = this.shim;

                var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

                Assert.IsFalse(string.IsNullOrEmpty(old.EmployerCodeVoe));
                this.EmployerCodeVoeTest(shimOfInterest, old, old.EmployerCodeVoe);

                this.CreateEmploymentRecord(converter);
                this.EmployerCodeVoeTest(shimOfInterest, old, string.Empty);

                this.CreateEmploymentRecord(converter);
                this.EmployerCodeVoeTest(shimOfInterest, old, null);
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrAddrTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrAddr));
            this.EmplrAddrTest(shimOfInterest, old, old.EmplrAddr);

            this.CreateEmploymentRecord(converter);
            this.EmplrAddrTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplrAddrTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrBusPhoneTest_Previous([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrBusPhone));
            this.EmplrBusPhoneTest(shimOfInterest, old, old.EmplrBusPhone);

            this.CreateEmploymentRecord(converter);
            this.EmplrBusPhoneTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplrBusPhoneTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrBusPhoneTest_Primary([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, (IEmpCollection)this.collection);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrBusPhone));
            Assert.AreEqual(old.EmplrBusPhone, shimOfInterest.EmplrBusPhone);
            this.EmplrBusPhoneTest(shimOfInterest, old, old.EmplrBusPhone);

            this.CreateEmploymentRecord(converter);
            this.EmplrBusPhoneTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplrBusPhoneTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrCityTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrCity));
            this.EmplrCityTest(shimOfInterest, old, old.EmplrCity);

            this.CreateEmploymentRecord(converter);
            this.EmplrCityTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplrCityTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrFaxTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrFax));
            this.EmplrFaxTest(shimOfInterest, old, old.EmplrFax);

            this.CreateEmploymentRecord(converter);
            this.EmplrFaxTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplrFaxTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrNmTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrNm));
            this.EmplrNmTest(shimOfInterest, old, old.EmplrNm);

            this.CreateEmploymentRecord(converter);
            this.EmplrNmTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.EmplrNmTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrStateTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrState));
            this.EmplrStateTest(shimOfInterest, old, old.EmplrState);

            this.CreateEmploymentRecord(converter);
            this.EmplrStateTest(shimOfInterest, old, string.Empty);

            // don't bother testing null since it invokes Tools.LogBug
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void EmplrZipTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.EmplrZip));
            this.EmplrZipTest(shimOfInterest, old, old.EmplrZip);

            this.CreateEmploymentRecord(converter);
            this.EmplrZipTest(shimOfInterest, old, string.Empty);

            // don't bother testing null since it invokes Tools.LogBug
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsCurrent_Previous()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, null, shimOfInterest.RecordId);

            Assert.AreEqual(old.IsCurrent, shimOfInterest.IsCurrent);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsCurrent_Primary()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.IsCurrent, shimOfInterest.IsCurrent);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsPrimaryEmpTest()
        {
            this.CreateEmploymentRecord(null);

            for (int i = 0; i < this.collection.CountRegular; ++i)
            {
                var item = (IRegularEmploymentRecord)this.collection.GetRegularRecordAt(0);
                var old = new CEmpRegRec(this.data, null, item.RecordId);

                Assert.IsFalse(old.IsPrimaryEmp);
                Assert.IsFalse(item.IsPrimaryEmp);
            }

            for (int i = 0; i < this.collection.CountSpecial; ++i)
            {
                var item = (IPrimaryEmploymentRecord)this.collection.GetSpecialRecordAt(0);
                var old = new CEmpPrimaryRec(this.data, null, item.RecordId, null);

                Assert.IsTrue(old.IsPrimaryEmp);
                Assert.IsTrue(item.IsPrimaryEmp);
                Assert.AreEqual(this.primary.RecordId, old.RecordId);
                Assert.AreEqual(this.primary.RecordId, item.RecordId);
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsSeeAttachmentTest_DefaultShouldBeTrue()
        {
            this.CreateEmploymentRecord(null);

            Assert.IsTrue(this.shim.IsSeeAttachment);
            Assert.IsTrue(this.primary.IsSeeAttachment);

            var old = new CEmpRegRec(this.data, null, this.shim.RecordId);
            Assert.IsTrue(old.IsSeeAttachment);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsSeeAttachmentTest_Previous()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.shim;

            shimOfInterest.IsSeeAttachment = true;
            var old = new CEmpRegRec(this.data, null, shimOfInterest.RecordId);
            Assert.IsTrue(old.IsSeeAttachment);

            shimOfInterest.IsSeeAttachment = false;
            old = new CEmpRegRec(this.data, null, shimOfInterest.RecordId);
            Assert.IsFalse(old.IsSeeAttachment);
            Assert.AreEqual(old.IsSeeAttachment, shimOfInterest.IsSeeAttachment);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsSeeAttachmentTest_Primary()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            shimOfInterest.IsSeeAttachment = true;
            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);
            Assert.IsTrue(old.IsSeeAttachment);

            shimOfInterest.IsSeeAttachment = false;
            old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);
            Assert.IsFalse(old.IsSeeAttachment);
            Assert.AreEqual(old.IsSeeAttachment, shimOfInterest.IsSeeAttachment);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsSelfEmplmtTest_Previous()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.shim;

            shimOfInterest.IsSelfEmplmt = true;
            var old = new CEmpRegRec(this.data, null, shimOfInterest.RecordId);
            Assert.IsTrue(old.IsSelfEmplmt);

            shimOfInterest.IsSelfEmplmt = false;
            old = new CEmpRegRec(this.data, null, shimOfInterest.RecordId);
            Assert.IsFalse(old.IsSelfEmplmt);
            Assert.AreEqual(old.IsSelfEmplmt, shimOfInterest.IsSelfEmplmt);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void IsSelfEmplmtTest_Primary()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            shimOfInterest.IsSelfEmplmt = true;
            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);
            Assert.IsTrue(old.IsSelfEmplmt);

            shimOfInterest.IsSelfEmplmt = false;
            old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);
            Assert.IsFalse(old.IsSelfEmplmt);
            Assert.AreEqual(old.IsSelfEmplmt, shimOfInterest.IsSelfEmplmt);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void JobTitleTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.JobTitle));
            this.JobTitleTest(shimOfInterest, old, old.JobTitle);

            this.CreateEmploymentRecord(converter);
            this.JobTitleTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.JobTitleTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void KeyTypeTest_Primary()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.KeyType, shimOfInterest.KeyType);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void KeyTypeTest_Previous()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, null, shimOfInterest.RecordId);

            Assert.AreEqual(old.KeyType, shimOfInterest.KeyType);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void MonITest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, converter, shimOfInterest.RecordId);

            Assert.IsFalse(string.IsNullOrEmpty(old.MonI_rep));
            this.MonITest(shimOfInterest, old, old.MonI_rep);

            this.CreateEmploymentRecord(converter);
            this.MonITest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.MonITest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void PrepDTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.shim;

            var old = new CEmpRegRec(this.data, null, shimOfInterest.RecordId);

            Assert.AreEqual(old.PrepD.DateTimeForComputation, shimOfInterest.PrepD.DateTimeForComputation);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void ProfLenTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.ProfLen_rep));
            this.ProfLenTest(shimOfInterest, old, old.ProfLen_rep);

            this.CreateEmploymentRecord(null);
            this.ProfLenTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(null);
            this.ProfLenTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void ProfLenRemainderMonthsTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.ProfLenRemainderMonths, shimOfInterest.ProfLenRemainderMonths);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void ProfLenTotalMonthsTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.ProfLenTotalMonths));
            this.ProfLenTotalMonthsTest(shimOfInterest, old, old.ProfLenTotalMonths);

            this.CreateEmploymentRecord(converter);
            this.ProfLenTotalMonthsTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.ProfLenTotalMonthsTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void ProfLenYearsTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.ProfLenYears));
            Assert.AreEqual(old.ProfLenYears, shimOfInterest.ProfLenYears);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void ProfStartDTest([ValueSource("FormatTargetEnumerable")] FormatTarget target)
        {
            // exclude the following because the legacy code is incorrect for them
            if (target == FormatTarget.FannieMaeMornetPlus) return;
            if (target == FormatTarget.FreddieMacAUS) return;
            if (target == FormatTarget.GinnieNet) return;

            var converter = new LosConvert(target);

            this.CreateEmploymentRecord(converter);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, converter, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.ProfStartD_rep));
            this.ProfStartDTest(shimOfInterest, old, old.ProfStartD_rep);

            this.CreateEmploymentRecord(converter);
            this.ProfStartDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(converter);
            this.ProfStartDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void VerifHasSignatureTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.VerifHasSignature, shimOfInterest.VerifHasSignature);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void VerifRecvDTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.VerifRecvD_rep));
            this.VerifRecvDTest(shimOfInterest, old, old.VerifRecvD_rep);

            this.CreateEmploymentRecord(null);
            this.VerifRecvDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(null);
            this.VerifRecvDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void VerifReorderedDTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.VerifReorderedD_rep));
            this.VerifReorderedDTest(shimOfInterest, old, old.VerifReorderedD_rep);

            this.CreateEmploymentRecord(null);
            this.VerifReorderedDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(null);
            this.VerifReorderedDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void VerifSentDTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.IsFalse(string.IsNullOrEmpty(old.VerifSentD_rep));
            this.VerifSentDTest(shimOfInterest, old, old.VerifSentD_rep);

            this.CreateEmploymentRecord(null);
            this.VerifSentDTest(shimOfInterest, old, string.Empty);

            this.CreateEmploymentRecord(null);
            this.VerifSentDTest(shimOfInterest, old, null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void VerifSignatureImgIdTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.VerifSignatureImgId, shimOfInterest.VerifSignatureImgId);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void VerifSigningEmployeeIdTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var old = new CEmpPrimaryRec(this.data, null, shimOfInterest.RecordId, null);

            Assert.AreEqual(old.VerifSigningEmployeeId, shimOfInterest.VerifSigningEmployeeId);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void ApplySignatureTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var empId = Guid.NewGuid();
            var imgId = Guid.NewGuid().ToString("D");
            shimOfInterest.ApplySignature(empId, imgId);

            Assert.AreEqual(empId, shimOfInterest.VerifSigningEmployeeId);
            Assert.AreEqual(imgId, shimOfInterest.VerifSignatureImgId);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void ClearSignatureTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            Assert.AreNotEqual(Guid.Empty, shimOfInterest.VerifSigningEmployeeId);
            Assert.IsFalse(string.IsNullOrEmpty(shimOfInterest.VerifSignatureImgId));

            shimOfInterest.ClearSignature();
            Assert.AreEqual(Guid.Empty, shimOfInterest.VerifSigningEmployeeId);
            Assert.IsTrue(string.IsNullOrEmpty(shimOfInterest.VerifSignatureImgId));
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void SetEmploymentStartDateTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var date = new DateTime(1963, 11, 22);
            shimOfInterest.SetEmploymentStartDate(date.ToString());
            var start = DateTime.Parse(shimOfInterest.EmpltStartD_rep);

            Assert.AreEqual(date, start);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void SetProfessionStartDateTest()
        {
            this.CreateEmploymentRecord(null);
            var shimOfInterest = this.primary;

            var date = new DateTime(1968, 4, 4);
            shimOfInterest.SetEmploymentStartDate(date.ToString());
            var start = DateTime.Parse(shimOfInterest.EmpltStartD_rep);

            Assert.AreEqual(date, start);
        }

        protected override void CreateCollection()
        {
            this.CreateEmploymentRecord(null);
        }

        private void AttentionTest(global::DataAccess.IEmploymentRecord shim, global::DataAccess.IEmploymentRecord old, string value)
        {
            shim.Attention = value;
            old.Attention = value;
            Assert.AreEqual(old.Attention, shim.Attention);
        }

        private void EmplmtEndDTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplmtEndD_rep = value;
            old.EmplmtEndD_rep = value;
            Assert.AreEqual(old.EmplmtEndD_rep, shim.EmplmtEndD_rep);
        }

        private void EmplmtLenTotalMonthsTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            // If the old record gives the wrong value (e.g., on 7/30/2018 it gave 157, instead of the correct value 156), the greater accuracy of the shim won't help.
            // We just have to accept off-by-one errors.
            shim.EmplmtLenTotalMonths = value;
            old.EmplmtLenTotalMonths = value;
            if (string.IsNullOrEmpty(value))
            {
                Assert.AreEqual(old.EmplmtLenTotalMonths, shim.EmplmtLenTotalMonths);
            }
            else
            {
                int oldValue = Convert.ToInt32(old.EmplmtLenTotalMonths);
                int shimValue = Convert.ToInt32(shim.EmplmtLenTotalMonths);
                int diff = oldValue > shimValue ? oldValue - shimValue : shimValue - oldValue;
                Assert.IsTrue(diff <= 1);
            }
        }

        private void EmplmtLenTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.EmplmtLen_rep = value;
            old.EmplmtLen_rep = value;
            Assert.AreEqual(old.EmplmtLen_rep, shim.EmplmtLen_rep);
        }

        private void EmplmtStartDTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.EmpltStartD_rep = value;
            old.EmpltStartD_rep = value;
            Assert.AreEqual(old.EmpltStartD_rep, shim.EmpltStartD_rep);
        }

        private void EmplmtStartDTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplmtStartD_rep = value;
            old.EmplmtStartD_rep = value;
            Assert.AreEqual(old.EmplmtStartD_rep, shim.EmplmtStartD_rep);
        }

        private void EmployeeIdVoeTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmployeeIdVoe = value;
            old.EmployeeIdVoe = value;
            Assert.AreEqual(old.EmployeeIdVoe, shim.EmployeeIdVoe);
        }

        private void EmployerCodeVoeTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmployerCodeVoe = value;
            old.EmployerCodeVoe = value;
            Assert.AreEqual(old.EmployerCodeVoe, shim.EmployerCodeVoe);
        }

        private void EmplrAddrTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplrAddr = value;
            old.EmplrAddr = value;
            Assert.AreEqual(old.EmplrAddr, shim.EmplrAddr);
        }

        private void EmplrBusPhoneTest(global::DataAccess.IEmploymentRecord shim, global::DataAccess.IEmploymentRecord old, string value)
        {
            shim.EmplrBusPhone = value;
            old.EmplrBusPhone = value;
            Assert.AreEqual(old.EmplrBusPhone, shim.EmplrBusPhone);
        }

        private void EmplrCityTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplrCity = value;
            old.EmplrCity = value;
            Assert.AreEqual(old.EmplrCity, shim.EmplrCity);
        }

        private void EmplrFaxTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplrFax = value;
            old.EmplrFax = value;
            Assert.AreEqual(old.EmplrFax, shim.EmplrFax);
        }

        private void EmplrNmTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplrNm = value;
            old.EmplrNm = value;
            Assert.AreEqual(old.EmplrNm, shim.EmplrNm);
        }

        private void EmplrStateTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplrState = value;
            old.EmplrState = value;
            Assert.AreEqual(old.EmplrState, shim.EmplrState);
        }

        private void EmplrZipTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.EmplrZip = value;
            old.EmplrZip = value;
            Assert.AreEqual(old.EmplrZip, shim.EmplrZip);
        }

        private void JobTitleTest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.JobTitle = value;
            old.JobTitle = value;
            Assert.AreEqual(old.JobTitle, shim.JobTitle);
        }

        private void MonITest(global::DataAccess.IRegularEmploymentRecord shim, global::DataAccess.IRegularEmploymentRecord old, string value)
        {
            shim.MonI_rep = value;
            old.MonI_rep = value;
            Assert.AreEqual(old.MonI_rep, shim.MonI_rep);
        }

        private void ProfLenTotalMonthsTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.ProfLenTotalMonths = value;
            old.ProfLenTotalMonths = value;
            Assert.AreEqual(old.ProfLenTotalMonths, shim.ProfLenTotalMonths);
        }

        private void ProfLenTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.ProfLen_rep = value;
            old.ProfLen_rep = value;
            Assert.AreEqual(old.ProfLen, shim.ProfLen);
            Assert.AreEqual(old.ProfLen_rep, shim.ProfLen_rep);
        }

        private void ProfStartDTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.ProfStartD_rep = value;
            old.ProfStartD_rep = value;
            Assert.AreEqual(old.ProfStartD_rep, shim.ProfStartD_rep);
        }

        private void VerifExpDTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.VerifExpD_rep = value;
            old.VerifExpD_rep = value;
            Assert.AreEqual(old.VerifExpD.IsValid, shim.VerifExpD.IsValid);
            if (old.VerifExpD.IsValid)
            {
                Assert.AreEqual(old.VerifExpD.DateTimeForComputation, shim.VerifExpD.DateTimeForComputation);
            }
            Assert.AreEqual(old.VerifExpD_rep, shim.VerifExpD_rep);
        }

        private void VerifRecvDTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.VerifRecvD_rep = value;
            old.VerifRecvD_rep = value;
            Assert.AreEqual(old.VerifRecvD.IsValid, shim.VerifRecvD.IsValid);
            if (old.VerifRecvD.IsValid)
            {
                Assert.AreEqual(old.VerifRecvD.DateTimeForComputation, shim.VerifRecvD.DateTimeForComputation);
            }
            Assert.AreEqual(old.VerifRecvD_rep, shim.VerifRecvD_rep);
        }

        private void VerifReorderedDTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.VerifReorderedD_rep = value;
            old.VerifReorderedD_rep = value;
            Assert.AreEqual(old.VerifReorderedD.IsValid, shim.VerifReorderedD.IsValid);
            if (old.VerifReorderedD.IsValid)
            {
                Assert.AreEqual(old.VerifReorderedD.DateTimeForComputation, shim.VerifReorderedD.DateTimeForComputation);
            }
            Assert.AreEqual(old.VerifReorderedD_rep, shim.VerifReorderedD_rep);
        }

        private void VerifSentDTest(global::DataAccess.IPrimaryEmploymentRecord shim, global::DataAccess.IPrimaryEmploymentRecord old, string value)
        {
            shim.VerifSentD_rep = value;
            old.VerifSentD_rep = value;
            Assert.AreEqual(old.VerifSentD.IsValid, shim.VerifSentD.IsValid);
            if (old.VerifSentD.IsValid)
            {
                Assert.AreEqual(old.VerifSentD.DateTimeForComputation, shim.VerifSentD.DateTimeForComputation);
            }
            Assert.AreEqual(old.VerifSentD_rep, shim.VerifSentD_rep);
        }

        private void CreateEmploymentRecord(LosConvert converter)
        {
            this.CreateEmploymentRecord(converter, false);
        }

        private void CreateEmploymentRecord(LosConvert converter, bool includeVOE)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = EmploymentRecordCollectionShimTest.InitializeCollection(converter, includeVOE: includeVOE);

            this.data = collectionShim.GetDataSet();
            this.collection = collectionShim;
            this.baseShim = this.shim = collectionShim.GetRegularRecordAt(0);
            this.primary = collectionShim.GetPrimaryEmp(true);
        }

        private string CheckRequiredFields(global::DataAccess.IEmploymentRecord item)
        {
            // These are the fields that the testing needs to be populated, NOT in any way required by the business logic.
            var row = item.DataRow;
            if (string.IsNullOrEmpty((string)row["EmplmtStartD"])) return "EmplmtStartD";
            if (string.IsNullOrEmpty((string)row["EmplrNm"])) return "EmplrNm";
            if (string.IsNullOrEmpty((string)row["EmplrBusPhone"])) return "EmplrBusPhone";
            if (string.IsNullOrEmpty((string)row["EmplrFax"])) return "EmplrFax";
            if (string.IsNullOrEmpty((string)row["EmplrAddr"])) return "EmplrAddr";
            if (string.IsNullOrEmpty((string)row["EmplrCity"])) return "EmplrCity";
            if (string.IsNullOrEmpty((string)row["EmplrState"])) return "EmplrState";
            if (string.IsNullOrEmpty((string)row["JobTitle"])) return "JobTitle";
            if (string.IsNullOrEmpty((string)row["EmplrZip"])) return "EmplrZip";
            if (string.IsNullOrEmpty((string)row["VerifSentD"])) return "VerifSentD";
            if (string.IsNullOrEmpty((string)row["VerifRecvD"])) return "VerifRecvD";
            if (string.IsNullOrEmpty((string)row["VerifExpD"])) return "VerifExpD";
            if (string.IsNullOrEmpty((string)row["IsSelfEmplmt"])) return "IsSelfEmplmt";
            if (string.IsNullOrEmpty((string)row["MonI"])) return "MonI";
            if (string.IsNullOrEmpty((string)row["EmplmtStat"])) return "EmplmtStat";
            if (string.IsNullOrEmpty((string)row["EmpltStartD"])) return "EmpltStartD";
            if (string.IsNullOrEmpty((string)row["EmplmtLen"])) return "EmplmtLen";
            if (string.IsNullOrEmpty((string)row["ProfStartD"])) return "ProfStartD";
            if (string.IsNullOrEmpty((string)row["ProfLen"])) return "ProfLen";
            if (string.IsNullOrEmpty((string)row["Attention"])) return "Attention";
            if (string.IsNullOrEmpty((string)row["PrepD"])) return "PrepD";
            if (string.IsNullOrEmpty((string)row["IsSeeAttachment"])) return "IsSeeAttachment";
            if (string.IsNullOrEmpty((string)row["VerifSigningEmployeeId"])) return "VerifSigningEmployeeId";
            if (string.IsNullOrEmpty((string)row["VerifSignatureImgId"])) return "VerifSignatureImgId";
            if (string.IsNullOrEmpty((string)row["IsCurrent"])) return "IsCurrent";
            return null;
        }
    }
}
