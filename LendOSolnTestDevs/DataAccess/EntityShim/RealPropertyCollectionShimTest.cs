﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class RealPropertyCollectionShimTest : RecordCollectionShimTest
    {
        private const int MinStatus = RangeLimits.ReoStatus.Min;
        private const int MaxStatus = RangeLimits.ReoStatus.Max;

        private static Guid id0;
        private static Guid id1;
        private static Guid id2;
        private static Guid id3;
        private static Guid id4;

        [Test]
        public void HasExtraREOTest()
        {
            var collection = InitializeCollection(null);
            Assert.IsTrue(collection.HasExtraREO);
        }

        [Test]
        public void AddRecordTest([Range(MinStatus, MaxStatus)] int status)
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var estatus = (E_ReoStatusT)status;
            var added = collection.AddRecord(estatus);

            Assert.AreEqual(size + 1, collection.CountRegular);
            Assert.IsTrue(estatus == (E_ReoStatusT)added.KeyType);
        }
        
        [Test]
        public void AddRegularRecordTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var item = collection.AddRegularRecord();

            Assert.AreEqual(size + 1, collection.CountRegular);
            for (int i = 0; i < size; ++i)
            {
                var prev = collection.GetRegularRecordAt(i);
                Assert.AreNotEqual(item.RecordId, prev.RecordId);
            }
        }

        [Test]
        public void AddRegularRecordAtTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var item = collection.AddRegularRecordAt(0);

            Assert.AreEqual(size + 1, collection.CountRegular);
            for (int i = 0; i < size + 1; ++i)
            {
                var curr = collection.GetRegularRecordAt(i);
                if (i == 0)
                {
                    Assert.AreEqual(item.RecordId, curr.RecordId);
                }
                else
                {
                    Assert.AreNotEqual(item.RecordId, curr.RecordId);
                }
            }
        }

        [Test]
        public void EnsureRegularRecordOfExistsTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var first = collection.GetRegularRecordAt(0);
            var item = collection.EnsureRegularRecordOf(first.RecordId);

            Assert.AreEqual(size, collection.CountRegular);
            Assert.AreEqual(first.RecordId, item.RecordId);
        }

        [Test]
        public void EnsureRegularRecordOfNotExistsTest()
        {
            var collection = InitializeCollection(null);
            int size = collection.CountRegular;

            var guid = Guid.NewGuid();
            var item = collection.EnsureRegularRecordOf(guid);

            Assert.AreEqual(size + 1, collection.CountRegular);
            Assert.AreEqual(guid, item.RecordId);
        }

        [Test]
        public void GetRegRecordOfTest()
        {
            var collection = InitializeCollection(null);

            var item = collection.GetRegularRecordAt(2);
            var again = collection.GetRegRecordOf(item.RecordId);

            Assert.AreEqual(item.RecordId, again.RecordId);
        }

        [Test]
        public void GetRegularRecordAtTest()
        {
            var collection = InitializeCollection(null);

            var set = new HashSet<Guid>();
            for (int i=0; i<collection.CountRegular; ++i)
            {
                var item = collection.GetRegularRecordAt(i);
                Assert.IsFalse(set.Contains(item.RecordId));
                set.Add(item.RecordId);
            }
        }

        [Test]
        public void GetSpecialRecordAtTest()
        {
            var collection = InitializeCollection(null);
            Assert.Throws<IndexOutOfRangeException>(() => collection.GetSpecialRecordAt(0));
        }

        [Test]
        public void AggregateCalculationTest()
        {
            var collection = InitializeCollection(null);

            int count = collection.CountRegular;
            var item = collection.GetRegularRecordAt(0);
            var subcoll = collection.GetSubcollection(true, E_ReoGroupT.All);

            var aggData = LendersOffice.CalculatedFields.RealProperty.CalculateAggregateData(subcoll.Cast<IRealEstateOwned>(), false);

            Assert.AreEqual(count * item.Val, (decimal)aggData.ReTotVal);
            Assert.AreEqual(count * item.MAmt, (decimal)aggData.ReTotMAmt);
            Assert.AreEqual(count * item.GrossRentI, (decimal)aggData.ReTotGrossRentI);
            Assert.AreEqual(count * item.MPmt, (decimal)aggData.ReTotMPmt);
            Assert.AreEqual(count * item.HExp, (decimal)aggData.ReTotHExp);
            Assert.AreEqual(count * item.NetRentI, (decimal)aggData.ReNetRentI);
        }

        [Test] 
        public void GetRecordOf_CollectionContainsId_ReturnsRecord()
        {
            var collection = InitializeCollection(null);

            var item = collection.GetRecordOf(id0);

            Assert.IsNotNull(item);
        }

        [Test]
        public void GetRecordOf_CollectionDoesNotContainId_ReturnsNull()
        {
            var collection = InitializeCollection(null);

            var item = collection.GetRecordOf(Guid.NewGuid());

            Assert.IsNull(item);
        }

        public static IReCollection InitializeCollection(LosConvert converter)
        {
            converter = converter ?? new LosConvert();

            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("44444444-4444-4444-4444-444444444444"));
            loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(appId, borrowerId, coborrowerId));

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var shimContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            shimContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
            shimContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));

            shimContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());

            var did0 = shimContainer.Add(borrowerId, CreateRealProperty(0));
            var did1 = shimContainer.Add(borrowerId, CreateRealProperty(4));
            var did2 = shimContainer.Add(borrowerId, CreateRealProperty(2));
            var did3 = shimContainer.Add(borrowerId, CreateRealProperty(1));
            var did4 = shimContainer.Add(borrowerId, CreateRealProperty(3));

            id0 = did0.Value;
            id1 = did1.Value;
            id2 = did2.Value;
            id3 = did3.Value;
            id4 = did4.Value;

            var appFake = Substitute.For<IShimAppDataProvider>();

            var ownershipManager = new ShimOwnershipManager<DataObjectKind.RealProperty, DataObjectKind.ConsumerRealPropertyAssociation, RealProperty, ConsumerRealPropertyAssociation>(
                shimContainer as IShimContainer,
                borrowerId,
                coborrowerId,
                appId,
                (consumerId, realPropertyId, AppId) => new ConsumerRealPropertyAssociation(consumerId, realPropertyId, AppId));
            
            return new RealPropertyCollectionShim(
                appFake,
                shimContainer as IShimContainer,
                ownershipManager,
                converter);
        }

        private static RealProperty CreateRealProperty(int streetNumber)
        {
            string city = "Timbuktu";
            decimal rent = 2400m;
            decimal expenses = 500m;
            decimal value = 687000m;
            decimal mtgamt = 518212m;
            decimal mtgpmt = 1654m;
            decimal rentinc = 1900m;
            int occrate = 85;
            string state = "CA";
            string address = "10" + streetNumber.ToString() + " Main St";
            string zipcode = "92648";

            var rp = new RealProperty();
            rp.City = City.Create(city);
            rp.GrossRentInc = Money.Create(rent);
            rp.HExp = Money.Create(expenses);
            rp.IsForceCalcNetRentalInc = true;
            rp.IsPrimaryResidence = false;
            rp.IsSubjectProp = false;
            rp.MarketValue = Money.Create(value);
            rp.MtgAmt = Money.Create(mtgamt);
            rp.MtgPmt = Money.Create(mtgpmt);
            rp.NetRentIncData = Money.Create(rentinc);
            rp.NetRentIncLocked = true;
            rp.OccR = Percentage.Create(occrate);
            rp.State = UnitedStatesPostalState.Create(state);
            rp.Status = E_ReoStatusT.Rental;
            rp.StreetAddress = StreetAddress.Create(address);
            rp.Type = E_ReoTypeT.SFR;
            rp.Zip = Zipcode.Create(zipcode);

            return rp;
        }

        public override void CreateCollection()
        {
            var collection = InitializeCollection(null);
            this.baseShim = collection;
            this.firstRecordId = id0;
            this.countRegular = 5;
            this.countSpecial = 0;
        }

        protected override List<ICollectionItemBase2> RetrieveSpecialRecords()
        {
            return new List<ICollectionItemBase2>();
        }
    }
}
