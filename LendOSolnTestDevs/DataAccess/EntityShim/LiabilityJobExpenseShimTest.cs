﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityJobExpenseShimTest
    {
        [Test]
        public void DebtT([Values(E_DebtJobExpenseT.JobExpense1, E_DebtJobExpenseT.JobExpense2)] E_DebtJobExpenseT expenseType)
        {
            var shim = LiabilityJobExpenseShim.Create(
                null,
                null,
                new Liability(NSubstitute.Substitute.For<ILiabilityDefaultsProvider>())/*NSubstitute.Substitute.For<global::LendingQB.Core.Data.ILiability>()*/,
                Substitute.For<IShimOwnershipManager<Guid>>(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.Empty),
                expenseType,
                null);

            Assert.AreEqual(E_DebtSpecialT.JobRelatedExpense, shim.DebtT);
        }

        [Test]
        public void DebtJobExpenseT([Values(E_DebtJobExpenseT.JobExpense1, E_DebtJobExpenseT.JobExpense2)] E_DebtJobExpenseT expenseType)
        {
            var tuple = this.GetShimAndLegacyLiability();
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.DebtJobExpenseT = expenseType;
            legacyLiability.DebtJobExpenseT = expenseType;

            Assert.AreEqual(legacyLiability.DebtJobExpenseT, shim.DebtJobExpenseT);
        }

        [Test]
        public void ExpenseDesc([Values(null, "", "Generic Description", "Expense 123")] string expenseDesc)
        {
            var tuple = this.GetShimAndLegacyLiability();
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.ExpenseDesc = expenseDesc;
            legacyLiability.ExpenseDesc = expenseDesc;

            Assert.AreEqual(legacyLiability.ExpenseDesc, shim.ExpenseDesc);
        }

        [Test]
        public void DataRow_AfterFlush_IsConsistentWithLegacy()
        {
            var entities = LiabilityCollectionShimTest.GetDefaultLiabilities(1);
            var liability = entities.First();
            liability.Value.DebtType = E_DebtT.JobRelatedExpense;
            var collectionContainer = LiabilityCollectionShimTest.GetCollectionContainer(entities);
            var converter = new LosConvert();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(liability.Key).Returns(Ownership.Joint);
            var collectionShim = new LiabilityCollectionShim(
                (IShimContainer)collectionContainer,
                Substitute.For<ILiabilityDefaultsProvider>(),
                Substitute.For<IShimAppDataProvider>(),
                ownershipManager,
                converter);
            var dataSetCopy = collectionShim.GetDataSet().Copy();

            var legacy = new CLiaJobExpense(dataSetCopy, converter, liability.Key);
            legacy.DebtJobExpenseT = E_DebtJobExpenseT.JobExpense1;
            var shim = collectionShim.GetJobRelated1(false);

            // Set the fields in an identical fashion.
            this.SetAvailableFields(legacy);
            this.SetAvailableFields(shim);

            // Get the data rows.
            var legacyRow = dataSetCopy.Tables[0].Rows[0];

            // Assert that the values are the same after shim flush.
            collectionShim.Flush();

            var shimRow = collectionShim.GetDataSet().Tables[0].Rows[0];
            LiabilityRegularShimTest.AssertLiabilityRowsEqual(legacyRow, shimRow);
        }

        private void SetAvailableFields(ILiabilityJobExpense liability)
        {
            liability.NotUsedInRatio = true;
            liability.OwnerT = E_LiaOwnerT.Joint;
            liability.PmlAuditTrailXmlContent = "";
            liability.Pmt_rep = "$7,890.12";
            liability.RemainMons_rep = "(R)";
            liability.WillBePdOff = true;
            liability.ExpenseDesc = "Super long commute";
        }

        private Tuple<LiabilityJobExpenseShim, CLiaJobExpense> GetShimAndLegacyLiability()
        {
            var converter = new LosConvert();

            var recordId = Guid.Empty;
            var entity = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            var shim = LiabilityJobExpenseShim.Create(
                null,
                null,
                entity,
                Substitute.For<IShimOwnershipManager<Guid>>(),
                DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(recordId),
                E_DebtJobExpenseT.JobExpense1,
                converter);

            var collectionShim = LiabilityCollectionShimTest.InitializeCollection(converter, new[] { new KeyValuePair<Guid, Liability>(recordId, entity) });
            var dataSet = collectionShim.GetDataSet();
            var liaJobExpense = new CLiaJobExpense(dataSet, converter, recordId);

            return Tuple.Create((LiabilityJobExpenseShim)shim, liaJobExpense);
        }
    }
}
