﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class AssetShimTest
    {
        private const int MinFormatTarget = 0;
        private const int MaxFormatTarget = 14;

        private DataSet data;
        private IAsset shim;


        [Test]
        public void AssetBusiness_AssetTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetBusiness(los);
            var old = new CAssetBusiness(this.data, los, this.shim.RecordId);
            this.AssetTypeTest((IAssetBusiness)this.shim, old, old.AssetT);

        }

        [Test]
        public void AssetBusiness_DescTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetBusiness(los);
            var old = new CAssetBusiness(this.data, los, this.shim.RecordId);
            this.DescTest((IAssetBusiness)this.shim, old, old.Desc);

            this.DescTest((IAssetBusiness)this.shim, old, string.Empty);
            this.DescTest((IAssetBusiness)this.shim, old, null);

        }

        [Test]
        public void AssetBusiness_IsEmptyCreatedTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetBusiness(los);
            var old = new CAssetBusiness(this.data, los, this.shim.RecordId);
            this.IsEmptyCreatedTest((IAssetBusiness)this.shim, old, old.IsEmptyCreated);

        }

        [Test]
        public void AssetBusiness_OwnerTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetBusiness(los);
            var old = new CAssetBusiness(this.data, los, this.shim.RecordId);

            this.OwnerTypeTest((IAssetBusiness)this.shim, old, old.OwnerT);

        }

        [Test]
        public void AssetBusiness_PhoneTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetBusiness(los);
            var old = new CAssetBusiness(this.data, los, this.shim.RecordId);
            this.PhoneTest((IAssetBusiness)this.shim, old, old.PhoneNumber);

            this.PhoneTest((IAssetBusiness)this.shim, old, string.Empty);
            this.PhoneTest((IAssetBusiness)this.shim, old, null);

        }

        [Test]
        public void AssetBusiness_ValueTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetBusiness(los);
            var old = new CAssetBusiness(this.data, los, this.shim.RecordId);
            this.ValueTest((IAssetBusiness)this.shim, old, old.Val_rep);

            this.ValueTest((IAssetBusiness)this.shim, old, string.Empty);
            this.ValueTest((IAssetBusiness)this.shim, old, null);

        }

        [Test]
        public void AssetCashDeposit_AssetCashDepositTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetCashDeposit(los);
            var old = new CAssetCashDeposit(this.data, los, this.shim.RecordId);
            this.AssetCashDepositTypeTest((IAssetCashDeposit)this.shim, old, old.AssetCashDepositT);

        }

        [Test]
        public void AssetCashDeposit_AssetTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetCashDeposit(los);
            var old = new CAssetCashDeposit(this.data, los, this.shim.RecordId);
            this.AssetTypeTest((IAssetCashDeposit)this.shim, old, old.AssetT);

        }

        [Test]
        public void AssetCashDeposit_DescTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetCashDeposit(los);
            var old = new CAssetCashDeposit(this.data, los, this.shim.RecordId);
            this.DescTest((IAssetCashDeposit)this.shim, old, old.Desc);

            this.DescTest((IAssetCashDeposit)this.shim, old, string.Empty);
            this.DescTest((IAssetCashDeposit)this.shim, old, null);

        }

        [Test]
        public void AssetCashDeposit_IsEmptyCreatedTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetCashDeposit(los);
            var old = new CAssetCashDeposit(this.data, los, this.shim.RecordId);
            this.IsEmptyCreatedTest((IAssetCashDeposit)this.shim, old, old.IsEmptyCreated);

        }

        [Test]
        public void AssetCashDeposit_OwnerTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetCashDeposit(los);
            var old = new CAssetCashDeposit(this.data, los, this.shim.RecordId);

            this.OwnerTypeTest((IAssetCashDeposit)this.shim, old, old.OwnerT);

        }

        [Test]
        public void AssetCashDeposit_PhoneTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetCashDeposit(los);
            var old = new CAssetCashDeposit(this.data, los, this.shim.RecordId);
            this.PhoneTest((IAssetCashDeposit)this.shim, old, old.PhoneNumber);

            this.PhoneTest((IAssetCashDeposit)this.shim, old, string.Empty);
            this.PhoneTest((IAssetCashDeposit)this.shim, old, null);

        }

        [Test]
        public void AssetCashDeposit_ValueTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetCashDeposit(los);
            var old = new CAssetCashDeposit(this.data, los, this.shim.RecordId);
            this.ValueTest((IAssetCashDeposit)this.shim, old, old.Val_rep);

            this.ValueTest((IAssetCashDeposit)this.shim, old, string.Empty);
            this.ValueTest((IAssetCashDeposit)this.shim, old, null);

        }

        [Test]
        public void AssetLifeInsurance_AssetTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetLifeInsurance(los);
            var old = new CAssetLifeIns(this.data, los, this.shim.RecordId);
            this.AssetTypeTest((IAssetLifeInsurance)this.shim, old, old.AssetT);

        }

        [Test]
        public void AssetLifeInsurance_DescTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetLifeInsurance(los);
            var old = new CAssetLifeIns(this.data, los, this.shim.RecordId);
            this.DescTest((IAssetLifeInsurance)this.shim, old, old.Desc);

            this.DescTest((IAssetLifeInsurance)this.shim, old, string.Empty);
            this.DescTest((IAssetLifeInsurance)this.shim, old, null);

        }

        [Test]
        public void AssetLifeInsurance_FaceValueTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetLifeInsurance(los);
            var old = new CAssetLifeIns(this.data, los, this.shim.RecordId);
            this.FaceValueTest((IAssetLifeInsurance)this.shim, old, old.FaceVal_rep);

            this.FaceValueTest((IAssetLifeInsurance)this.shim, old, string.Empty);
            this.FaceValueTest((IAssetLifeInsurance)this.shim, old, null);

        }

        [Test]
        public void AssetLifeInsurance_IsEmptyCreatedTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetLifeInsurance(los);
            var old = new CAssetLifeIns(this.data, los, this.shim.RecordId);
            this.IsEmptyCreatedTest((IAssetLifeInsurance)this.shim, old, old.IsEmptyCreated);

        }

        [Test]
        public void AssetLifeInsurance_OwnerTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetLifeInsurance(los);
            var old = new CAssetLifeIns(this.data, los, this.shim.RecordId);

            this.OwnerTypeTest((IAssetLifeInsurance)this.shim, old, old.OwnerT);

        }

        [Test]
        public void AssetLifeInsurance_PhoneTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetLifeInsurance(los);
            var old = new CAssetLifeIns(this.data, los, this.shim.RecordId);
            this.PhoneTest((IAssetLifeInsurance)this.shim, old, old.PhoneNumber);

            this.PhoneTest((IAssetLifeInsurance)this.shim, old, string.Empty);
            this.PhoneTest((IAssetLifeInsurance)this.shim, old, null);

        }

        [Test]
        public void AssetLifeInsurance_ValueTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetLifeInsurance(los);
            var old = new CAssetLifeIns(this.data, los, this.shim.RecordId);
            this.ValueTest((IAssetLifeInsurance)this.shim, old, old.Val_rep);

            this.ValueTest((IAssetLifeInsurance)this.shim, old, string.Empty);
            this.ValueTest((IAssetLifeInsurance)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_AccountNameTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.AccountNameTest((IAssetRegular)this.shim, old, old.AccNm);

            this.AccountNameTest((IAssetRegular)this.shim, old, string.Empty);
            this.AccountNameTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_AccountNumTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.AccountNumTest((IAssetRegular)this.shim, old, old.AccNum.Value);

            this.AccountNumTest((IAssetRegular)this.shim, old, string.Empty);
            this.AccountNumTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_AssetTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.AssetTypeTest((IAssetRegular)this.shim, old, old.AssetT);

        }

        [Test]
        public void AssetRegular_AssetType_RepTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);

            Assert.AreEqual(old.AssetT_rep, ((IAssetRegular)this.shim).AssetT_rep);
        }

        [Test]
        public void AssetRegular_AttentionTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.AttentionTest((IAssetRegular)this.shim, old, old.Attention);

            this.AttentionTest((IAssetRegular)this.shim, old, string.Empty);
            this.AttentionTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_CityTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.CityTest((IAssetRegular)this.shim, old, old.City);

            this.CityTest((IAssetRegular)this.shim, old, string.Empty);
            this.CityTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_CompanyNameTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.CompanyNameTest((IAssetRegular)this.shim, old, old.ComNm);

            this.CompanyNameTest((IAssetRegular)this.shim, old, string.Empty);
            this.CompanyNameTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_DepartmentNameTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.DepartmentNameTest((IAssetRegular)this.shim, old, old.DepartmentName);

            this.DepartmentNameTest((IAssetRegular)this.shim, old, string.Empty);
            this.DepartmentNameTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_DescTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.DescTest((IAssetRegular)this.shim, old, old.Desc);

            this.DescTest((IAssetRegular)this.shim, old, string.Empty);
            this.DescTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_GiftSourceTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.GiftSourceTest((IAssetRegular)this.shim, old, old.GiftSource);

        }

        [Test]
        public void AssetRegular_IsEmptyCreatedTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.IsEmptyCreatedTest((IAssetRegular)this.shim, old, old.IsEmptyCreated);

        }

        [Test]
        public void AssetRegular_IsSeeAttachmentTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.IsSeeAttachmentTest((IAssetRegular)this.shim, old, old.IsSeeAttachment);

        }

        [Test]
        public void AssetRegular_OtherTypeDescTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.OtherTypeDescTest((IAssetRegular)this.shim, old, old.OtherTypeDesc);

            this.OtherTypeDescTest((IAssetRegular)this.shim, old, string.Empty);
            this.OtherTypeDescTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_OwnerTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);

            this.OwnerTypeTest((IAssetRegular)this.shim, old, old.OwnerT);

        }

        [Test]
        public void AssetRegular_PhoneTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.PhoneTest((IAssetRegular)this.shim, old, old.PhoneNumber);

            this.PhoneTest((IAssetRegular)this.shim, old, string.Empty);
            this.PhoneTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_PrepDateTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.PrepDateTest((IAssetRegular)this.shim, old, old.PrepD_rep);

            this.PrepDateTest((IAssetRegular)this.shim, old, string.Empty);
            this.PrepDateTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_StateTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.StateTest((IAssetRegular)this.shim, old, old.State);

            this.StateTest((IAssetRegular)this.shim, old, string.Empty);
            this.StateTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_StreetAddressTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.StreetAddressTest((IAssetRegular)this.shim, old, old.StAddr);

            this.StreetAddressTest((IAssetRegular)this.shim, old, string.Empty);
            this.StreetAddressTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_ValueTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.ValueTest((IAssetRegular)this.shim, old, old.Val_rep);

            this.ValueTest((IAssetRegular)this.shim, old, string.Empty);
            this.ValueTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_VerifExpiresDateTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.VerifExpiresDateTest((IAssetRegular)this.shim, old, old.VerifExpD_rep);

            this.VerifExpiresDateTest((IAssetRegular)this.shim, old, string.Empty);
            this.VerifExpiresDateTest((IAssetRegular)this.shim, old, null);

        }

        [Test]
        public void AssetRegular_VerifRecvDateTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.VerifRecvDateTest((IAssetRegular)this.shim, old, old.VerifRecvD_rep);

            this.VerifRecvDateTest((IAssetRegular)this.shim, old, string.Empty);
            this.VerifRecvDateTest((IAssetRegular)this.shim, old, null);
        }

        [Test]
        public void AssetRegular_VerifReorderedDateTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.VerifReorderedDateTest((IAssetRegular)this.shim, old, old.VerifReorderedD_rep);

            this.VerifReorderedDateTest((IAssetRegular)this.shim, old, string.Empty);
            this.VerifReorderedDateTest((IAssetRegular)this.shim, old, null);
        }

        [Test]
        public void AssetRegular_VerifSentDateTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.VerifSentDateTest((IAssetRegular)this.shim, old, old.VerifSentD_rep);

            this.VerifSentDateTest((IAssetRegular)this.shim, old, string.Empty);
            this.VerifSentDateTest((IAssetRegular)this.shim, old, null);
        }

        [Test]
        public void AssetRegular_ZipTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);
            this.ZipTest((IAssetRegular)this.shim, old, old.Zip);

            this.ZipTest((IAssetRegular)this.shim, old, string.Empty);
            this.ZipTest((IAssetRegular)this.shim, old, null);
        }

        [Test]
        public void AssetRegular_Signature()
        {
            var los = new LosConvert();
            this.Create_AssetRegular(los);
            var old = new CAssetRegular(this.data, los, this.shim.RecordId);

            var signerId = Guid.NewGuid();
            var imgId = Guid.NewGuid().ToString();

            var regularAssetShim = (IAssetRegular)this.shim;

            Assert.AreEqual(false, regularAssetShim.VerifHasSignature);
            Assert.AreEqual(false, old.VerifHasSignature);

            old.ApplySignature(signerId, imgId);
            regularAssetShim.ApplySignature(signerId, imgId);

            Assert.AreEqual(true, regularAssetShim.VerifHasSignature);
            Assert.AreEqual(true, old.VerifHasSignature);

            Assert.AreEqual(old.VerifSignatureImgId, regularAssetShim.VerifSignatureImgId);
            Assert.AreEqual(old.VerifSigningEmployeeId, regularAssetShim.VerifSigningEmployeeId);

            old.ClearSignature();
            regularAssetShim.ClearSignature();

            Assert.AreEqual(false, regularAssetShim.VerifHasSignature);
            Assert.AreEqual(false, old.VerifHasSignature);

            Assert.AreEqual(old.VerifSignatureImgId, regularAssetShim.VerifSignatureImgId);
            Assert.AreEqual(old.VerifSigningEmployeeId, regularAssetShim.VerifSigningEmployeeId);
        }

        [Test]
        public void AssetRetirement_AssetTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRetirement(los);
            var old = new CAssetRetirement(this.data, los, this.shim.RecordId);
            this.AssetTypeTest((IAssetRetirement)this.shim, old, old.AssetT);
        }

        [Test]
        public void AssetRetirement_DescTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRetirement(los);
            var old = new CAssetRetirement(this.data, los, this.shim.RecordId);
            this.DescTest((IAssetRetirement)this.shim, old, old.Desc);

            this.DescTest((IAssetRetirement)this.shim, old, string.Empty);
            this.DescTest((IAssetRetirement)this.shim, old, null);
        }

        [Test]
        public void AssetRetirement_IsEmptyCreatedTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRetirement(los);
            var old = new CAssetRetirement(this.data, los, this.shim.RecordId);
            this.IsEmptyCreatedTest((IAssetRetirement)this.shim, old, old.IsEmptyCreated);
        }

        [Test]
        public void AssetRetirement_OwnerTypeTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRetirement(los);
            var old = new CAssetRetirement(this.data, los, this.shim.RecordId);

            this.OwnerTypeTest((IAssetRetirement)this.shim, old, old.OwnerT);
        }

        [Test]
        public void AssetRetirement_PhoneTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRetirement(los);
            var old = new CAssetRetirement(this.data, los, this.shim.RecordId);
            this.PhoneTest((IAssetRetirement)this.shim, old, old.PhoneNumber);

            this.PhoneTest((IAssetRetirement)this.shim, old, string.Empty);
            this.PhoneTest((IAssetRetirement)this.shim, old, null);
        }

        [Test]
        public void AssetRetirement_ValueTest([Range(MinFormatTarget, MaxFormatTarget)] int formatTarget)
        {
            var target = (FormatTarget)formatTarget;
            var los = new LosConvert(target);
            this.Create_AssetRetirement(los);
            var old = new CAssetRetirement(this.data, los, this.shim.RecordId);
            this.ValueTest((IAssetRetirement)this.shim, old, old.Val_rep);

            this.ValueTest((IAssetRetirement)this.shim, old, string.Empty);
            this.ValueTest((IAssetRetirement)this.shim, old, null);
        }

        [Test]
        public void OwnerT_Get_RetrievesValueFromShimContainer()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var appFake = Substitute.For<IShimAppDataProvider>();
            var asset = new Asset();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(Guid.Empty).Returns(Ownership.Coborrower);
            var id = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(Guid.Empty);
            var shim = AssetRegularShim.Create(shimContainer, recordCollection, appFake, asset, ownershipManager, id, new LosConvert());

            var ownershipType = shim.OwnerT;

            Assert.AreEqual(E_AssetOwnerT.CoBorrower, ownershipType);
        }

        [Test]
        public void OwnerT_Set_CallsShimContainerMethod()
        {
            var shimContainer = Substitute.For<IShimContainer>();
            var recordCollection = Substitute.For<IRecordCollection>();
            var appFake = Substitute.For<IShimAppDataProvider>();
            var asset = new Asset();
            var ownershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            ownershipManager.GetOwnership(Guid.Empty).Returns(Ownership.Coborrower);
            var id = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(Guid.Empty);
            var shim = AssetRegularShim.Create(shimContainer, recordCollection, appFake, asset, ownershipManager, id, new LosConvert());

            shim.OwnerT = E_AssetOwnerT.Joint;

            ownershipManager.Received().SetOwnership(Guid.Empty, (Ownership)E_AssetOwnerT.Joint);
        }

        [Test]
        public void BorrowerOwned_SwapBorrowers_BecomesCoborrowerOwned()
        {
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var asset = new Asset();
            asset.AccountName = DescriptionField.Create("Borrower");

            var container = this.CreateCollectionContainerForSwapBorrowers(borrowerId, null, legacyAppId, asset);
            var ownershipMgr = this.CreateShimOwnershipManagerForSwapBorrowers(container, borrowerId, null, legacyAppId);

            var aggregate = (ILoanLqbCollectionContainer)container;
            var assetId = aggregate.Assets.Keys.First();
            var shim = this.CreateAssetShimForSwapBorrowers(container, ownershipMgr, assetId, asset);

            var ownership = shim.OwnerT;
            Assert.AreEqual(E_AssetOwnerT.Borrower, ownership);

            var swapInterface = (IRememberBorrowers)shim;
            swapInterface.SwapBorrowers();

            ownership = shim.OwnerT;
            Assert.AreEqual(E_AssetOwnerT.CoBorrower, ownership);
        }

        [Test]
        public void CoborrowerOwned_SwapBorrowers_BecomesBorrowerOwned()
        {
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var asset = new Asset();
            asset.AccountName = DescriptionField.Create("Coborrower");

            var container = this.CreateCollectionContainerForSwapBorrowers(null, coborrowerId, legacyAppId, asset);
            var ownershipMgr = this.CreateShimOwnershipManagerForSwapBorrowers(container, null, coborrowerId, legacyAppId);

            var aggregate = (ILoanLqbCollectionContainer)container;
            var assetId = aggregate.Assets.Keys.First();
            var shim = this.CreateAssetShimForSwapBorrowers(container, ownershipMgr, assetId, asset);

            var ownership = shim.OwnerT;
            Assert.AreEqual(E_AssetOwnerT.CoBorrower, ownership);

            var swapInterface = (IRememberBorrowers)shim;
            swapInterface.SwapBorrowers();

            ownership = shim.OwnerT;
            Assert.AreEqual(E_AssetOwnerT.Borrower, ownership);
        }

        [Test]
        public void JointOwned_SwapBorrowers_RemainsJointOwned()
        {
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var asset = new Asset();
            asset.AccountName = DescriptionField.Create("Joint");

            var container = this.CreateCollectionContainerForSwapBorrowers(borrowerId, coborrowerId, legacyAppId, asset);
            var ownershipMgr = this.CreateShimOwnershipManagerForSwapBorrowers(container, borrowerId, coborrowerId, legacyAppId);

            var aggregate = (ILoanLqbCollectionContainer)container;
            var assetId = aggregate.Assets.Keys.First();
            var shim = this.CreateAssetShimForSwapBorrowers(container, ownershipMgr, assetId, asset);

            var ownership = shim.OwnerT;
            Assert.AreEqual(E_AssetOwnerT.Joint, ownership);

            var swapInterface = (IRememberBorrowers)shim;
            swapInterface.SwapBorrowers();

            ownership = shim.OwnerT;
            Assert.AreEqual(E_AssetOwnerT.Joint, ownership);
        }

        private IAssetRegular CreateAssetShimForSwapBorrowers(
            IShimContainer container,
            ShimOwnershipManager<DataObjectKind.Asset, DataObjectKind.ConsumerAssetAssociation, Asset, ConsumerAssetAssociation> ownershipMgr,
            DataObjectIdentifier<DataObjectKind.Asset, Guid> assetId,
            Asset asset)
        {
            return AssetRegularShim.Create(container, null, null, asset, ownershipMgr, assetId, null);
        }

        private ShimOwnershipManager<DataObjectKind.Asset, DataObjectKind.ConsumerAssetAssociation, Asset, ConsumerAssetAssociation> CreateShimOwnershipManagerForSwapBorrowers(
            IShimContainer container,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? borrowerId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId)
        {
            var borrowerIdentifier = borrowerId ?? DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var coborrowerIdentifier = coborrowerId ?? DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());

            Func<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Asset, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, ConsumerAssetAssociation> func = null;
            return new ShimOwnershipManager<DataObjectKind.Asset, DataObjectKind.ConsumerAssetAssociation, Asset, ConsumerAssetAssociation>(
                container,
                borrowerIdentifier,
                coborrowerIdentifier,
                legacyAppId,
                func);
        }

        private IShimContainer CreateCollectionContainerForSwapBorrowers(
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? borrowerId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            Asset asset)
        {
            var loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(Guid.NewGuid());
            var brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.NewGuid());

            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(legacyAppId, borrowerId ?? coborrowerId.Value, borrowerId != null ? coborrowerId : null));
            var container = new LoanLqbCollectionContainer(new LoanLqbEmptyCollectionProvider(), loanData, null, null);
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.Assets), nameof(ILoanLqbCollectionContainer.ConsumerAssets) });
            container.Load(brokerId, null, null);

            if (borrowerId != null)
            {
                var assetId = container.Add(borrowerId.Value, asset);

                if (coborrowerId != null)
                {
                    container.AddOwnership(coborrowerId.Value, assetId);
                }
            }
            else if (coborrowerId != null)
            {
                container.Add(coborrowerId.Value, asset);
            }

            return container;
        }

        private void AssetTypeTest(IAssetBusiness shim, IAssetBusiness old, E_AssetSpecialT value)
        {
            shim.AssetT = value;
            old.AssetT = value;
            Assert.AreEqual(old.AssetT, shim.AssetT);
        }

        private void DescTest(IAssetBusiness shim, IAssetBusiness old, string value)
        {
            shim.Desc = value;
            old.Desc = value;
            Assert.AreEqual(old.Desc, shim.Desc);
        }

        private void IsEmptyCreatedTest(IAssetBusiness shim, IAssetBusiness old, bool value)
        {
            shim.IsEmptyCreated = value;
            old.IsEmptyCreated = value;
            Assert.AreEqual(old.IsEmptyCreated, shim.IsEmptyCreated);
        }

        private void OwnerTypeTest(IAssetBusiness shim, IAssetBusiness old, E_AssetOwnerT value)
        {
            shim.OwnerT = value;
            old.OwnerT = value;
            Assert.AreEqual(old.OwnerT, shim.OwnerT);
        }

        private void PhoneTest(IAssetBusiness shim, IAssetBusiness old, string value)
        {
            shim.PhoneNumber = value;
            old.PhoneNumber = value;
            Assert.AreEqual(old.PhoneNumber, shim.PhoneNumber);
        }

        private void ValueTest(IAssetBusiness shim, IAssetBusiness old, string value)
        {
            shim.Val_rep = value;
            old.Val_rep = value;
            Assert.AreEqual(old.Val_rep, shim.Val_rep);
        }

        private void AssetCashDepositTypeTest(IAssetCashDeposit shim, IAssetCashDeposit old, E_AssetCashDepositT value)
        {
            shim.AssetCashDepositT = value;
            old.AssetCashDepositT = value;
            Assert.AreEqual(old.AssetCashDepositT, shim.AssetCashDepositT);
        }

        private void AssetTypeTest(IAssetCashDeposit shim, IAssetCashDeposit old, E_AssetSpecialT value)
        {
            shim.AssetT = value;
            old.AssetT = value;
            Assert.AreEqual(old.AssetT, shim.AssetT);
        }

        private void DescTest(IAssetCashDeposit shim, IAssetCashDeposit old, string value)
        {
            shim.Desc = value;
            old.Desc = value;
            Assert.AreEqual(old.Desc, shim.Desc);
        }

        private void IsEmptyCreatedTest(IAssetCashDeposit shim, IAssetCashDeposit old, bool value)
        {
            shim.IsEmptyCreated = value;
            old.IsEmptyCreated = value;
            Assert.AreEqual(old.IsEmptyCreated, shim.IsEmptyCreated);
        }

        private void OwnerTypeTest(IAssetCashDeposit shim, IAssetCashDeposit old, E_AssetOwnerT value)
        {
            shim.OwnerT = value;
            old.OwnerT = value;
            Assert.AreEqual(old.OwnerT, shim.OwnerT);
        }

        private void PhoneTest(IAssetCashDeposit shim, IAssetCashDeposit old, string value)
        {
            shim.PhoneNumber = value;
            old.PhoneNumber = value;
            Assert.AreEqual(old.PhoneNumber, shim.PhoneNumber);
        }

        private void ValueTest(IAssetCashDeposit shim, IAssetCashDeposit old, string value)
        {
            shim.Val_rep = value;
            old.Val_rep = value;
            Assert.AreEqual(old.Val_rep, shim.Val_rep);
        }

        private void AssetTypeTest(IAssetLifeInsurance shim, IAssetLifeInsurance old, E_AssetSpecialT value)
        {
            shim.AssetT = value;
            old.AssetT = value;
            Assert.AreEqual(old.AssetT, shim.AssetT);
        }

        private void DescTest(IAssetLifeInsurance shim, IAssetLifeInsurance old, string value)
        {
            shim.Desc = value;
            old.Desc = value;
            Assert.AreEqual(old.Desc, shim.Desc);
        }

        private void FaceValueTest(IAssetLifeInsurance shim, IAssetLifeInsurance old, string value)
        {
            shim.FaceVal_rep = value;
            old.FaceVal_rep = value;
            Assert.AreEqual(old.FaceVal_rep, shim.FaceVal_rep);
        }

        private void IsEmptyCreatedTest(IAssetLifeInsurance shim, IAssetLifeInsurance old, bool value)
        {
            shim.IsEmptyCreated = value;
            old.IsEmptyCreated = value;
            Assert.AreEqual(old.IsEmptyCreated, shim.IsEmptyCreated);
        }

        private void OwnerTypeTest(IAssetLifeInsurance shim, IAssetLifeInsurance old, E_AssetOwnerT value)
        {
            shim.OwnerT = value;
            old.OwnerT = value;
            Assert.AreEqual(old.OwnerT, shim.OwnerT);
        }

        private void PhoneTest(IAssetLifeInsurance shim, IAssetLifeInsurance old, string value)
        {
            shim.PhoneNumber = value;
            old.PhoneNumber = value;
            Assert.AreEqual(old.PhoneNumber, shim.PhoneNumber);
        }

        private void ValueTest(IAssetLifeInsurance shim, IAssetLifeInsurance old, string value)
        {
            shim.Val_rep = value;
            old.Val_rep = value;
            Assert.AreEqual(old.Val_rep, shim.Val_rep);
        }

        private void AccountNameTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.AccNm = value;
            old.AccNm = value;
            Assert.AreEqual(old.AccNm, shim.AccNm);
        }

        private void AccountNumTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.AccNum = value;
            old.AccNum = value;
            Assert.AreEqual(old.AccNum, shim.AccNum);
        }

        private void AssetTypeTest(IAssetRegular shim, IAssetRegular old, E_AssetRegularT value)
        {
            shim.AssetT = value;
            old.AssetT = value;
            Assert.AreEqual(old.AssetT, shim.AssetT);
        }

        private void AttentionTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.Attention = value;
            old.Attention = value;
            Assert.AreEqual(old.Attention, shim.Attention);
        }

        private void CityTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.City = value;
            old.City = value;
            Assert.AreEqual(old.City, shim.City);
        }

        private void CompanyNameTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.ComNm = value;
            old.ComNm = value;
            Assert.AreEqual(old.ComNm, shim.ComNm);
        }

        private void DepartmentNameTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.DepartmentName = value;
            old.DepartmentName = value;
            Assert.AreEqual(old.DepartmentName, shim.DepartmentName);
        }

        private void DescTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.Desc = value;
            old.Desc = value;
            Assert.AreEqual(old.Desc, shim.Desc);
        }

        private void GiftSourceTest(IAssetRegular shim, IAssetRegular old, E_GiftFundSourceT value)
        {
            shim.GiftSource = value;
            old.GiftSource = value;
            Assert.AreEqual(old.GiftSource, shim.GiftSource);
        }

        private void IsEmptyCreatedTest(IAssetRegular shim, IAssetRegular old, bool value)
        {
            shim.IsEmptyCreated = value;
            old.IsEmptyCreated = value;
            Assert.AreEqual(old.IsEmptyCreated, shim.IsEmptyCreated);
        }

        private void IsSeeAttachmentTest(IAssetRegular shim, IAssetRegular old, bool value)
        {
            shim.IsSeeAttachment = value;
            old.IsSeeAttachment = value;
            Assert.AreEqual(old.IsSeeAttachment, shim.IsSeeAttachment);
        }

        private void OtherTypeDescTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.OtherTypeDesc = value;
            old.OtherTypeDesc = value;
            Assert.AreEqual(old.OtherTypeDesc, shim.OtherTypeDesc);
        }

        private void OwnerTypeTest(IAssetRegular shim, IAssetRegular old, E_AssetOwnerT value)
        {
            shim.OwnerT = value;
            old.OwnerT = value;
            Assert.AreEqual(old.OwnerT, shim.OwnerT);
        }

        private void PhoneTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.PhoneNumber = value;
            old.PhoneNumber = value;
            Assert.AreEqual(old.PhoneNumber, shim.PhoneNumber);
        }

        private void PrepDateTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.PrepD_rep = value;
            old.PrepD_rep = value;
            Assert.AreEqual(old.PrepD_rep, shim.PrepD_rep);
        }

        private void StateTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory<LendersOffice.Logging.ILoggerFactory>(new FOOL.FakeLogger.Factory());

                shim.State = value;
                old.State = value;
                Assert.AreEqual(old.State, shim.State);
            }
        }

        private void StreetAddressTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.StAddr = value;
            old.StAddr = value;
            Assert.AreEqual(old.StAddr, shim.StAddr);
        }

        private void ValueTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.Val_rep = value;
            old.Val_rep = value;
            Assert.AreEqual(old.Val_rep, shim.Val_rep);
        }

        private void VerifExpiresDateTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.VerifExpD_rep = value;
            old.VerifExpD_rep = value;
            Assert.AreEqual(old.VerifExpD_rep, shim.VerifExpD_rep);
        }

        private void VerifRecvDateTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.VerifRecvD_rep = value;
            old.VerifRecvD_rep = value;
            Assert.AreEqual(old.VerifRecvD_rep, shim.VerifRecvD_rep);
        }

        private void VerifReorderedDateTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.VerifReorderedD_rep = value;
            old.VerifReorderedD_rep = value;
            Assert.AreEqual(old.VerifReorderedD_rep, shim.VerifReorderedD_rep);
        }

        private void VerifSentDateTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            shim.VerifSentD_rep = value;
            old.VerifSentD_rep = value;
            Assert.AreEqual(old.VerifSentD_rep, shim.VerifSentD_rep);
        }

        private void ZipTest(IAssetRegular shim, IAssetRegular old, string value)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory<LendersOffice.Logging.ILoggerFactory>(new FOOL.FakeLogger.Factory());
                shim.Zip = value;
                old.Zip = value;
                Assert.AreEqual(old.Zip, shim.Zip);
            }
        }

        private void AssetTypeTest(IAssetRetirement shim, IAssetRetirement old, E_AssetSpecialT value)
        {
            shim.AssetT = value;
            old.AssetT = value;
            Assert.AreEqual(old.AssetT, shim.AssetT);
        }

        private void DescTest(IAssetRetirement shim, IAssetRetirement old, string value)
        {
            shim.Desc = value;
            old.Desc = value;
            Assert.AreEqual(old.Desc, shim.Desc);
        }

        private void IsEmptyCreatedTest(IAssetRetirement shim, IAssetRetirement old, bool value)
        {
            shim.IsEmptyCreated = value;
            old.IsEmptyCreated = value;
            Assert.AreEqual(old.IsEmptyCreated, shim.IsEmptyCreated);
        }

        private void OwnerTypeTest(IAssetRetirement shim, IAssetRetirement old, E_AssetOwnerT value)
        {
            shim.OwnerT = value;
            old.OwnerT = value;
            Assert.AreEqual(old.OwnerT, shim.OwnerT);
        }

        private void PhoneTest(IAssetRetirement shim, IAssetRetirement old, string value)
        {
            shim.PhoneNumber = value;
            old.PhoneNumber = value;
            Assert.AreEqual(old.PhoneNumber, shim.PhoneNumber);
        }

        private void ValueTest(IAssetRetirement shim, IAssetRetirement old, string value)
        {
            shim.Val_rep = value;
            old.Val_rep = value;
            Assert.AreEqual(old.Val_rep, shim.Val_rep);
        }

        private void Create_AssetRegular(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = AssetCollectionShimTest.InitializeCollection(converter);
            this.data = collectionShim.GetDataSet();
            this.shim = collectionShim.GetRegularRecordAt(0);
        }

        private void Create_AssetRetirement(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = AssetCollectionShimTest.InitializeCollection(converter);
            this.data = collectionShim.GetDataSet();
            this.shim = collectionShim.GetRetirement(false);
        }

        private void Create_AssetLifeInsurance(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = AssetCollectionShimTest.InitializeCollection(converter);
            this.data = collectionShim.GetDataSet();
            this.shim = collectionShim.GetLifeInsurance(false);
        }

        private void Create_AssetCashDeposit(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = AssetCollectionShimTest.InitializeCollection(converter);
            this.data = collectionShim.GetDataSet();
            this.shim = collectionShim.GetCashDeposit1(false);
        }

        private void Create_AssetBusiness(LosConvert converter)
        {
            converter = converter ?? new LosConvert();
            var collectionShim = AssetCollectionShimTest.InitializeCollection(converter);
            this.data = collectionShim.GetDataSet();
            this.shim = collectionShim.GetBusinessWorth(false);
        } 
    }
}
