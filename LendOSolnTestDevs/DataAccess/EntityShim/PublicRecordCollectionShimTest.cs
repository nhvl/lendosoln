﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class PublicRecordCollectionShimTest : RecordCollectionShimTest
    {
        private static Guid id0;
        private static Guid id1;
        private static Guid id2;
        private static Guid id3;
        private static Guid id4;

        [Test]
        public void Construct()
        {
            var collectionShim = InitializeCollection(null);
            this.CheckSizes(5, collectionShim);
        }

        [Test]
        public void CheckInitialOrder()
        {
            var collectionShim = InitializeCollection(null);
            this.CheckOrder(collectionShim, id0, id1, id2, id3, id4);
        }

        [Test]
        public void AddRecord()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.AddRecord(E_CreditPublicRecordType.Annulment);

            this.CheckSizes(6, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2, id3, id4, shim.RecordId);
        }

        [Test]
        public void AddRegularRecord()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.AddRegularRecord();

            this.CheckSizes(6, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2, id3, id4, shim.RecordId);
        }

        [Test]
        public void AddRegularRecordAt()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.AddRegularRecordAt(2);

            this.CheckSizes(6, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, shim.RecordId, id2, id3, id4);
        }

        [Test]
        public void EnsureRegularRecordOf_NotExist()
        {
            var collectionShim = InitializeCollection(null);

            var guid = Guid.NewGuid();
            var shim = collectionShim.EnsureRegularRecordOf(guid);

            Assert.AreEqual(guid, shim.RecordId);
            this.CheckSizes(6, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2, id3, id4, guid);
        }

        [Test]
        public void EnsureRegularRecordOf_YesExist()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.EnsureRegularRecordOf(id3);

            Assert.AreEqual(id3, shim.RecordId);
            this.CheckSizes(5, collectionShim);
            this.CheckOrder(collectionShim, id0, id1, id2, id3, id4);
        }

        [Test]
        public void GetRegRecordOf()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegRecordOf(id4);

            Assert.AreEqual(id4, shim.RecordId);
        }

        [Test]
        public void GetRegularRecordAt()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegularRecordAt(4);

            Assert.AreEqual(id4, shim.RecordId);
        }

        [Test]
        public void GetSpecialRecordAt()
        {
            var collectionShim = InitializeCollection(null);

            Assert.Throws<IndexOutOfRangeException>(() => collectionShim.GetSpecialRecordAt(2));
        }

        [Test]
        public void HasRecordOf_False()
        {
            var collectionShim = InitializeCollection(null);

            var guid = Guid.NewGuid();
            bool has = collectionShim.HasRecordOf(guid);

            Assert.IsFalse(has);
        }

        [Test]
        public void HasRecordOf_True()
        {
            var collectionShim = InitializeCollection(null);

            bool has0 = collectionShim.HasRecordOf(id0);
            bool has1 = collectionShim.HasRecordOf(id1);
            bool has2 = collectionShim.HasRecordOf(id2);
            bool has3 = collectionShim.HasRecordOf(id3);
            bool has4 = collectionShim.HasRecordOf(id4);

            Assert.IsTrue(has0);
            Assert.IsTrue(has1);
            Assert.IsTrue(has2);
            Assert.IsTrue(has3);
            Assert.IsTrue(has4);
        }

        [Test]
        public void MoveRegularRecordDown()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegularRecordAt(2);
            collectionShim.MoveRegularRecordDown(shim);

            this.CheckOrder(collectionShim, id0, id1, id3, id2, id4);
        }

        [Test]
        public void MoveRegularRecordUp()
        {
            var collectionShim = InitializeCollection(null);

            var shim = collectionShim.GetRegularRecordAt(3);
            collectionShim.MoveRegularRecordUp(shim);

            this.CheckOrder(collectionShim, id0, id1, id3, id2, id4);
        }

        [Test]
        public void SortRegularRecordsByKey()
        {
            var collectionShim = InitializeCollection(null);

            collectionShim.SortRegularRecordsByKey("CourtName", false); // descending order

            this.CheckOrder(collectionShim, id1, id4, id2, id3, id0); // order determined in InitializeCollection
        }

        private void CheckSizes(int expectedSize, IPublicRecordCollection collection)
        {
            Assert.AreEqual(expectedSize, collection.CountRegular);
            Assert.AreEqual(0, collection.CountSpecial);

            var ds = collection.GetDataSet();
            var table = ds.Tables[0];
            Assert.AreEqual(expectedSize, table.Rows.Count);
        }

        private void CheckOrder(IPublicRecordCollection collection, params Guid[] ids)
        {
            for (int i = 0; i < ids.Length; ++i)
            {
                var item = collection.GetRegularRecordAt(i);
                Assert.AreEqual(ids[i], item.RecordId);
            }
        }

        public static IPublicRecordCollection InitializeCollection(LosConvert converter)
        {
            converter = converter ?? new LosConvert();

            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("33333333-3333-3333-3333-333333333333"));
            loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(appId, borrowerId, coborrowerId));

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var shimContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            shimContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.PublicRecords));
            shimContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerPublicRecords));

            shimContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());
            var did0 = shimContainer.Add(borrowerId, CreatePublicRecord(0));
            var did1 = shimContainer.Add(borrowerId, CreatePublicRecord(4));
            var did2 = shimContainer.Add(borrowerId, CreatePublicRecord(2));
            var did3 = shimContainer.Add(borrowerId, CreatePublicRecord(1));
            var did4 = shimContainer.Add(borrowerId, CreatePublicRecord(3));

            id0 = did0.Value;
            id1 = did1.Value;
            id2 = did2.Value;
            id3 = did3.Value;
            id4 = did4.Value;

            var ownershipManager = new ShimOwnershipManager<DataObjectKind.PublicRecord, DataObjectKind.ConsumerPublicRecordAssociation, PublicRecord, ConsumerPublicRecordAssociation>(
                shimContainer as IShimContainer,
                borrowerId,
                coborrowerId,
                appId,
                (consumerId, publicRecordId, AppId) => new ConsumerPublicRecordAssociation(consumerId, publicRecordId, AppId));

            return new PublicRecordCollectionShim(shimContainer as IShimContainer, ownershipManager, converter);
        }

        private static PublicRecord CreatePublicRecord(int? courtNameSuffix)
        {
            string creditReportId = "PubRec_0004";
            string courtName = "Kangaroo";
            if (courtNameSuffix != null) courtName += courtNameSuffix.Value.ToString();
            DateTime reported = new DateTime(2018, 3, 24);
            DateTime disposed = new DateTime(2018, 3, 4);
            E_CreditPublicRecordDispositionType dispType = E_CreditPublicRecordDispositionType.RealEstateSold;
            decimal bla = 1234m;
            E_CreditPublicRecordType type = E_CreditPublicRecordType.PublicSale;
            DateTime lastEff = new DateTime(2018, 4, 10);
            DateTime bkfile = new DateTime(2018, 4, 1);
            bool include = true;

            var pr = new PublicRecord();
            pr.IdFromCreditReport = ThirdPartyIdentifier.Create(creditReportId);
            pr.CourtName = EntityName.Create(courtName);
            pr.ReportedDate = UnzonedDate.Create(reported);
            pr.DispositionDate = UnzonedDate.Create(disposed);
            pr.DispositionType = dispType;
            pr.BankruptcyLiabilitiesAmount = Money.Create(bla);
            pr.Type = type;
            pr.LastEffectiveDate = UnzonedDate.Create(lastEff);
            pr.BkFileDate = UnzonedDate.Create(bkfile);
            pr.IncludeInPricing = include;

            if (courtNameSuffix != null && courtNameSuffix.Value == 4)
            {
                var audit = new CPublicRecordAuditItem("user", "11/6/2018", "description");
                pr.AuditTrail = new List<CPublicRecordAuditItem>();
                pr.AuditTrail.Add(audit);
            }

            return PublicRecord.CopyAsIfUnmodified(pr);
        }

        public override void CreateCollection()
        {
            var collection = InitializeCollection(null);
            this.baseShim = collection;
            this.firstRecordId = id0;
            this.countRegular = 5;
            this.countSpecial = 0;
        }

        protected override List<ICollectionItemBase2> RetrieveSpecialRecords()
        {
            return new List<ICollectionItemBase2>();
        }
    }
}
