﻿namespace DataAccess
{
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers;
    using LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    /// <summary>
    /// Tests for <see cref="HousingHistoryFieldShim"/>, via the use case in the static loan fields.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class HousingHistoryFieldShimIntegrationTest
    {
        private LoanForIntegrationTest loanRefWithHousingHistoryCollection;
        private LoanForIntegrationTest loanRefWithLegacyHousingHistory;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            try
            {
                this.loanRefWithLegacyHousingHistory = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false);
                this.loanRefWithHousingHistoryCollection = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true);

                CPageData loanWithLegacyHousingHistory = this.loanRefWithLegacyHousingHistory.CreateNewPageDataWithBypass();
                loanWithLegacyHousingHistory.InitLoad();
                Assert.That(loanWithLegacyHousingHistory, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).False);

                CPageData loanWithHousingHistoryCollection = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
                loanWithHousingHistoryCollection.InitLoad();
                Assert.That(loanWithHousingHistoryCollection, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).True);
            }
            catch
            {
                this.loanRefWithLegacyHousingHistory?.Dispose();
                this.loanRefWithLegacyHousingHistory = null;
                this.loanRefWithHousingHistoryCollection?.Dispose();
                this.loanRefWithHousingHistoryCollection = null;
                throw;
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            this.loanRefWithLegacyHousingHistory?.Dispose();
            this.loanRefWithHousingHistoryCollection?.Dispose();
        }

        [TestCase(nameof(loanRefWithHousingHistoryCollection))]
        [TestCase(nameof(loanRefWithLegacyHousingHistory))]
        public void SetAddrYrs_FractionalYearsAtAddress_PreservesTheRoundtrip(string fieldName)
        {
            CPageData loan = this.GetFieldValue<LoanForIntegrationTest>(fieldName).CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBAddrYrs = "4.25";
            app.aCAddrYrs = "3.5";
            app.aBPrev1AddrYrs = "8.08333";
            app.aCPrev1AddrYrs = "0.7500";
            app.aBPrev2AddrYrs = "-2.5";
            app.aCPrev2AddrYrs = "0";

            Assert.AreEqual("4.25", app.aBAddrYrs);
            Assert.AreEqual("3.5", app.aCAddrYrs);
            Assert.AreEqual("8.08", app.aBPrev1AddrYrs);
            Assert.AreEqual("0.75", app.aCPrev1AddrYrs);
            Assert.AreEqual("-2.5", app.aBPrev2AddrYrs);
            Assert.AreEqual("0", app.aCPrev2AddrYrs);
        }

        [Test]
        public void SetAddrYrs_LoanWithoutCollection_PreservesNonsense()
        {
            CPageData loan = this.loanRefWithLegacyHousingHistory.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBAddrYrs = "42";
            app.aCAddrYrs = "42";
            app.aBPrev1AddrYrs = "42";
            app.aCPrev1AddrYrs = "42";
            app.aBPrev2AddrYrs = "42";
            app.aCPrev2AddrYrs = "42";

            app.aBAddrYrs = " 4.0";
            app.aCAddrYrs = "ponies";
            app.aBPrev1AddrYrs = "4.30";
            app.aCPrev1AddrYrs = "025.3";
            app.aBPrev2AddrYrs = "123.4";
            app.aCPrev2AddrYrs = ".000";

            Assert.AreEqual("4.0", app.aBAddrYrs);
            Assert.AreEqual("poni", app.aCAddrYrs);
            Assert.AreEqual("4.30", app.aBPrev1AddrYrs);
            Assert.AreEqual("025.", app.aCPrev1AddrYrs);
            Assert.AreEqual("123.", app.aBPrev2AddrYrs);
            Assert.AreEqual(".000", app.aCPrev2AddrYrs);
        }

        [Test]
        public void SetAddrYrs_LoanWithCollection_SimplifiesToDecimal()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBAddrYrs = "42";
            app.aCAddrYrs = "42";
            app.aBPrev1AddrYrs = "42";
            app.aCPrev1AddrYrs = "42";
            app.aBPrev2AddrYrs = "42";
            app.aCPrev2AddrYrs = "42";

            app.aBAddrYrs = " 4.0";
            app.aCAddrYrs = "ponies";
            app.aBPrev1AddrYrs = "4.30";
            app.aCPrev1AddrYrs = "025.3";
            app.aBPrev2AddrYrs = "123.4";
            app.aCPrev2AddrYrs = ".000";

            Assert.AreEqual("4", app.aBAddrYrs);
            Assert.AreEqual(string.Empty, app.aCAddrYrs);
            Assert.AreEqual("4.33", app.aBPrev1AddrYrs);
            Assert.AreEqual("25.3", app.aCPrev1AddrYrs);
            Assert.AreEqual("123", app.aBPrev2AddrYrs);
            Assert.AreEqual("0", app.aCPrev2AddrYrs);
        }

        [Test]
        public void SetHousingHistory_BlankValues_ConvertedToEmptyString(
            [Values(nameof(loanRefWithHousingHistoryCollection), nameof(loanRefWithLegacyHousingHistory))]string fieldName,
            [Values(null, "")]string blankValue)
        {
            CPageData loan = this.GetFieldValue<LoanForIntegrationTest>(fieldName).CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBAddr = blankValue;
            app.aBCity = blankValue;
            app.aBState = blankValue;
            app.aBZip = blankValue;
            app.aBAddrYrs = blankValue;

            Assert.AreEqual(string.Empty, app.aBAddr);
            Assert.AreEqual(string.Empty, app.aBCity);
            Assert.AreEqual(string.Empty, app.aBState);
            Assert.AreEqual(string.Empty, app.aBZip);
            Assert.AreEqual(string.Empty, app.aBAddrYrs);
        }

        [TestCase(nameof(loanRefWithHousingHistoryCollection))]
        [TestCase(nameof(loanRefWithLegacyHousingHistory))]
        public void SetAndGetAllFixedFields_Loan_UpdatesFixedFieldsAsExpected(string fieldName)
        {
            CPageData loan = this.GetFieldValue<LoanForIntegrationTest>(fieldName).CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);
            AssertBlankMailingAddressFields(app);
            AssertBlankPostClosingAddressFields(app);

            SetBorrPresentAddressFields(app);
            SetCoboPresentAddressFields(app);
            SetBorrPreviousAddress1Fields(app);
            SetCoboPreviousAddress1Fields(app);
            SetPreviousAddress2Fields(app);
            SetLegacyMailingAddressFields(app);
            SetLegacyPostClosingAddressFields(app);

            AssertSetPresentAddressFields(app);
            AssertSetPreviousAddress1Fields(app);
            AssertSetPreviousAddress2Fields(app);
            AssertSetLegacyMailingAddressFields(app);
            AssertSetLegacyMailingAddressFields(app);
        }

        [Test]
        public void AddHousingHistoryEntry_LoanWithoutCollection_ThrowsException()
        {
            CPageData loan = this.loanRefWithLegacyHousingHistory.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);
            var housingHistoryEntry = new HousingHistoryEntry();

            Assert.Catch(() => loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, housingHistoryEntry));
        }

        [Test]
        public void GetPresentHousingHistory_AddEntryToLoan_UpdatesOnlyFixedPresentAddressFieldsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);
            var borrEntry = new HousingHistoryEntry() { IsPresentAddress = true, };
            SetBorrPresentAddressFields(borrEntry);
            var coboEntry = new HousingHistoryEntry() { IsPresentAddress = true, };
            SetCoboPresentAddressFields(coboEntry);

            loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, borrEntry);
            loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, coboEntry);

            loan.InvalidateCache(); // triggers the loan to recalculate the shim
            AssertSetPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);
        }

        [Test]
        public void SetPresentHousingHistory_OnlyPresent_AddsOnlyNeededEntries()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);

            Assert.AreEqual(0, loan.HousingHistoryEntries.Count);

            SetBorrPresentAddressFields(app);

            Assert.AreEqual(1, loan.HousingHistoryEntries.Count);

            SetCoboPresentAddressFields(app);

            Assert.AreEqual(2, loan.HousingHistoryEntries.Count);
        }

        [Test]
        public void SetPresentHousingHistory_FieldsAlreadySetWithComponents_DoesNotOverwrite()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            var borrEntry = new HousingHistoryEntry() { IsPresentAddress = true, };
            SetBorrPresentAddressFields(borrEntry);
            loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, borrEntry);
            var coboEntry = new HousingHistoryEntry() { IsPresentAddress = true, };
            SetCoboPresentAddressFields(coboEntry);
            loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, coboEntry);
            AssertBorrPresentStreetAddressComponents(loan.HousingHistoryEntries.Values.Single(e => e.ConsumerId == app.aBConsumerIdentifier).Address);
            AssertCoboPresentStreetAddressComponents(loan.HousingHistoryEntries.Values.Single(e => e.ConsumerId == app.aCConsumerIdentifier).Address);

            SetBorrPresentAddressFields(app);
            SetCoboPresentAddressFields(app);

            AssertBorrPresentStreetAddressComponents(loan.HousingHistoryEntries.Values.Single(e => e.ConsumerId == app.aBConsumerIdentifier).Address);
            AssertCoboPresentStreetAddressComponents(loan.HousingHistoryEntries.Values.Single(e => e.ConsumerId == app.aCConsumerIdentifier).Address);
        }

        [Test]
        public void GetPrevious1HousingHistory_AddEntryToLoan_UpdatesOnlyFixedPrevious1AddressFieldsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);
            var borrEntry = new HousingHistoryEntry() { IsPresentAddress = false, };
            SetBorrPreviousAddress1Fields(borrEntry);
            var coboEntry = new HousingHistoryEntry() { IsPresentAddress = false, };
            SetCoboPreviousAddress1Fields(coboEntry);

            loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, borrEntry);
            loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, coboEntry);

            loan.InvalidateCache(); // triggers the loan to recalculate the shim
            AssertBlankPresentAddressFields(app);
            AssertSetPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);
        }

        [Test]
        public void SetPrevious1HousingHistory_OnlyPresent_AddsOnlyNeededEntries()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);

            Assert.AreEqual(0, loan.HousingHistoryEntries.Count);

            SetBorrPreviousAddress1Fields(app);

            Assert.AreEqual(1, loan.HousingHistoryEntries.Count);

            SetCoboPreviousAddress1Fields(app);

            Assert.AreEqual(2, loan.HousingHistoryEntries.Count);
        }

        [Test]
        public void GetPrevious2HousingHistory_AddManyEntriesToLoanOnlySecondFilledOut_UpdatesOnlyPrevious2AddressFieldsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);

            loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = true, });
            loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = true, });
            var borrHousingHistory1 = loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });
            var coboHousingHistory1 = loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });
            var borrHousingHistory2 = loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });
            var coboHousingHistory2 = loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });

            loan.InvalidateCache(); // triggers the loan to recalculate the shim
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);

            // This is all a hack to deal with not knowing insertion order in advance since borrHousingHistory1,2 might not be ordered 1,2 after insertion
            var borrHousingHistoryEntry2 = loan.HousingHistoryEntries.Last(e => e.Key == borrHousingHistory1 || e.Key == borrHousingHistory2).Value;
            var coboHousingHistoryEntry2 = loan.HousingHistoryEntries.Last(e => e.Key == coboHousingHistory1 || e.Key == coboHousingHistory2).Value;
            SetBorrPreviousAddress2Fields(borrHousingHistoryEntry2);
            SetCoboPreviousAddress2Fields(coboHousingHistoryEntry2);

            loan.InvalidateCache(); // triggers the loan to recalculate the shim
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertSetPreviousAddress2Fields(app);
        }

        [Test]
        public void GetPrevious2HousingHistory_AddPreviousEntriesToLoan_UpdatesFixedPrevious2AddressFieldsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankPresentAddressFields(app);
            AssertBlankPreviousAddress1Fields(app);
            AssertBlankPreviousAddress2Fields(app);

            var borrHousingHistory1 = loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });
            var coboHousingHistory1 = loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });
            var borrHousingHistory2 = loan.AddHousingHistoryEntry(app.aBConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });
            var coboHousingHistory2 = loan.AddHousingHistoryEntry(app.aCConsumerIdentifier, new HousingHistoryEntry() { IsPresentAddress = false, });

            // This is all a hack to deal with not knowing insertion order in advance since borrHousingHistory1,2 might not be ordered 1,2 after insertion
            var borrHousingHistoryIds = loan.HousingHistoryEntries.Keys.Where(e => e == borrHousingHistory1 || e == borrHousingHistory2).ToList();
            var coboHousingHistoryIds = loan.HousingHistoryEntries.Keys.Where(e => e == coboHousingHistory1 || e == coboHousingHistory2).ToList();
            SetBorrPreviousAddress1Fields(loan.HousingHistoryEntries[borrHousingHistoryIds[0]]);
            SetCoboPreviousAddress1Fields(loan.HousingHistoryEntries[coboHousingHistoryIds[0]]);
            SetBorrPreviousAddress2Fields(loan.HousingHistoryEntries[borrHousingHistoryIds[1]]);
            SetCoboPreviousAddress2Fields(loan.HousingHistoryEntries[coboHousingHistoryIds[1]]);

            loan.InvalidateCache(); // triggers the loan to recalculate the shim
            AssertBlankPresentAddressFields(app);
            AssertSetPreviousAddress1Fields(app);
            AssertSetPreviousAddress2Fields(app);
        }

        [Test]
        public void GetLegacyMailingAddressFields_SetUsingNewData_ReadsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankMailingAddressFields(app);
            AssertBlankPostClosingAddressFields(app);

            app.aBAddrMailSourceT = E_aAddrMailSourceT.Other;
            app.aCAddrMailSourceT = E_aAddrMailSourceT.Other;
            app.aBMailingAddressData = BorrMailingAddress;
            app.aCMailingAddressData = CoboMailingAddress;

            AssertSetLegacyMailingAddressFields(app);
        }

        [Test]
        public void GetLegacyPostClosingAddressFields_SetUsingNewData_ReadsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankMailingAddressFields(app);
            AssertBlankPostClosingAddressFields(app);

            app.aBAddrPostSourceTLckd = true;
            app.aCAddrPostSourceTLckd = true;
            app.aBAddrPostSourceT = E_aAddrPostSourceT.Other;
            app.aCAddrPostSourceT = E_aAddrPostSourceT.Other;
            app.aBPostClosingAddressData = BorrPostClosingAddress;
            app.aCPostClosingAddressData = CoboPostClosingAddress;

            AssertSetLegacyPostClosingAddressFields(app);
        }

        [Test]
        public void GetNewMailingAddressFields_SetUsingLegacyData_ReadsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankMailingAddressFields(app);
            AssertBlankPostClosingAddressFields(app);

            SetLegacyMailingAddressFields(app);

            Assert.AreEqual(BorrMailingAddress.StreetAddress, app.aBMailingAddress?.StreetAddress);
            Assert.AreEqual(BorrMailingAddress.City, app.aBMailingAddress?.City);
            Assert.AreEqual(BorrMailingAddress.State, app.aBMailingAddress?.State);
            Assert.AreEqual(BorrMailingAddress.PostalCode, app.aBMailingAddress?.PostalCode);
            Assert.AreEqual(BorrMailingAddress.CountryCode, app.aBMailingAddress?.CountryCode);
            Assert.AreEqual(CoboMailingAddress.StreetAddress, app.aCMailingAddress?.StreetAddress);
            Assert.AreEqual(CoboMailingAddress.City, app.aCMailingAddress?.City);
            Assert.AreEqual(CoboMailingAddress.State, app.aCMailingAddress?.State);
            Assert.AreEqual(CoboMailingAddress.PostalCode, app.aCMailingAddress?.PostalCode);
            Assert.AreEqual(CoboMailingAddress.CountryCode, app.aCMailingAddress?.CountryCode);
        }

        [Test]
        public void GetNewPostClosingAddressFields_SetUsingLegacyData_ReadsAsExpected()
        {
            CPageData loan = this.loanRefWithHousingHistoryCollection.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            AssertBlankMailingAddressFields(app);
            AssertBlankPostClosingAddressFields(app);

            SetLegacyPostClosingAddressFields(app);

            Assert.AreEqual(BorrPostClosingAddress.StreetAddress, app.aBPostClosingAddress?.StreetAddress);
            Assert.AreEqual(BorrPostClosingAddress.City, app.aBPostClosingAddress?.City);
            Assert.AreEqual(BorrPostClosingAddress.State, app.aBPostClosingAddress?.State);
            Assert.AreEqual(BorrPostClosingAddress.PostalCode, app.aBPostClosingAddress?.PostalCode);
            Assert.AreEqual(BorrPostClosingAddress.CountryCode, app.aBPostClosingAddress?.CountryCode);
            Assert.AreEqual(CoboPostClosingAddress.StreetAddress, app.aCPostClosingAddress?.StreetAddress);
            Assert.AreEqual(CoboPostClosingAddress.City, app.aCPostClosingAddress?.City);
            Assert.AreEqual(CoboPostClosingAddress.State, app.aCPostClosingAddress?.State);
            Assert.AreEqual(CoboPostClosingAddress.PostalCode, app.aCPostClosingAddress?.PostalCode);
            Assert.AreEqual(CoboPostClosingAddress.CountryCode, app.aCPostClosingAddress?.CountryCode);
        }

        private static void AssertBlankPresentAddressFields(CAppData app)
        {
            Assert.AreEqual(string.Empty, app.aBAddr);
            Assert.AreEqual(string.Empty, app.aBCity);
            Assert.AreEqual(string.Empty, app.aBState);
            Assert.AreEqual(string.Empty, app.aBZip);
            Assert.AreEqual(E_aBAddrT.LeaveBlank, app.aBAddrT);
            Assert.AreEqual(string.Empty, app.aBAddrYrs);

            Assert.AreEqual(string.Empty, app.aCAddr);
            Assert.AreEqual(string.Empty, app.aCCity);
            Assert.AreEqual(string.Empty, app.aCState);
            Assert.AreEqual(string.Empty, app.aCZip);
            Assert.AreEqual(E_aCAddrT.LeaveBlank, app.aCAddrT);
            Assert.AreEqual(string.Empty, app.aCAddrYrs);
        }

        private static void AssertBlankPreviousAddress1Fields(CAppData app)
        {
            Assert.AreEqual(string.Empty, app.aBPrev1Addr);
            Assert.AreEqual(string.Empty, app.aBPrev1City);
            Assert.AreEqual(string.Empty, app.aBPrev1State);
            Assert.AreEqual(string.Empty, app.aBPrev1Zip);
            Assert.AreEqual(E_aBPrev1AddrT.LeaveBlank, app.aBPrev1AddrT);
            Assert.AreEqual(string.Empty, app.aBPrev1AddrYrs);

            Assert.AreEqual(string.Empty, app.aCPrev1Addr);
            Assert.AreEqual(string.Empty, app.aCPrev1City);
            Assert.AreEqual(string.Empty, app.aCPrev1State);
            Assert.AreEqual(string.Empty, app.aCPrev1Zip);
            Assert.AreEqual(E_aCPrev1AddrT.LeaveBlank, app.aCPrev1AddrT);
            Assert.AreEqual(string.Empty, app.aCPrev1AddrYrs);
        }

        private static void AssertBlankPreviousAddress2Fields(CAppData app)
        {
            Assert.AreEqual(string.Empty, app.aBPrev2Addr);
            Assert.AreEqual(string.Empty, app.aBPrev2City);
            Assert.AreEqual(string.Empty, app.aBPrev2State);
            Assert.AreEqual(string.Empty, app.aBPrev2Zip);
            Assert.AreEqual(E_aBPrev2AddrT.LeaveBlank, app.aBPrev2AddrT);
            Assert.AreEqual(string.Empty, app.aBPrev2AddrYrs);

            Assert.AreEqual(string.Empty, app.aCPrev2Addr);
            Assert.AreEqual(string.Empty, app.aCPrev2City);
            Assert.AreEqual(string.Empty, app.aCPrev2State);
            Assert.AreEqual(string.Empty, app.aCPrev2Zip);
            Assert.AreEqual(E_aCPrev2AddrT.LeaveBlank, app.aCPrev2AddrT);
            Assert.AreEqual(string.Empty, app.aCPrev2AddrYrs);
        }

        private static void AssertBlankMailingAddressFields(CAppData app)
        {
            Assert.AreEqual(E_aAddrMailSourceT.PresentAddress, app.aBAddrMailSourceT);
            Assert.AreEqual(string.Empty, app.aBAddrMail);
            Assert.AreEqual(string.Empty, app.aBCityMail);
            Assert.AreEqual(string.Empty, app.aBStateMail);
            Assert.AreEqual(string.Empty, app.aBZipMail);
            Assert.IsNull(app.aBMailingAddress);

            Assert.AreEqual(E_aAddrMailSourceT.PresentAddress, app.aCAddrMailSourceT);
            Assert.AreEqual(string.Empty, app.aCAddrMail);
            Assert.AreEqual(string.Empty, app.aCCityMail);
            Assert.AreEqual(string.Empty, app.aCStateMail);
            Assert.AreEqual(string.Empty, app.aCZipMail);
            Assert.IsNull(app.aCMailingAddress);
        }

        private static void AssertBlankPostClosingAddressFields(CAppData app)
        {
            Assert.AreEqual(E_aAddrPostSourceT.MailingAddress, app.aBAddrPostSourceT);
            Assert.AreEqual(string.Empty, app.aBAddrPost);
            Assert.AreEqual(string.Empty, app.aBCityPost);
            Assert.AreEqual(string.Empty, app.aBStatePost);
            Assert.AreEqual(string.Empty, app.aBZipPost);
            Assert.IsNull(app.aBPostClosingAddress);

            Assert.AreEqual(E_aAddrPostSourceT.MailingAddress, app.aCAddrPostSourceT);
            Assert.AreEqual(string.Empty, app.aCAddrPost);
            Assert.AreEqual(string.Empty, app.aCCityPost);
            Assert.AreEqual(string.Empty, app.aCStatePost);
            Assert.AreEqual(string.Empty, app.aCZipPost);
            Assert.IsNull(app.aCPostClosingAddress);
        }

        private static PostalAddress BorrPresentAddress { get; } = (new UnitedStatesPostalAddress.Builder()
        {
            AddressNumber = UspsAddressNumber.Create("362").ForceValue(),
            StreetName = StreetName.Create("Duane").ForceValue(),
            StreetSuffix = StreetSuffix.Create("Ave").ForceValue(),
            City = City.Create("Schenectady").ForceValue(),
            UsState = UnitedStatesPostalState.CreateWithValidation("NY").ForceValue(),
            Zipcode = Zipcode.CreateWithValidation("12345").ForceValue(),
        }).GetAddress();

        private static PostalAddress CoboPresentAddress { get; } = (new UnitedStatesPostalAddress.Builder()
        {
            AddressNumber = UspsAddressNumber.Create("3016").ForceValue(),
            StreetName = StreetName.Create("Killybrooke").ForceValue(),
            StreetSuffix = StreetSuffix.Create("Ln").ForceValue(),
            City = City.Create("Costa Mesa").ForceValue(),
            UsState = UnitedStatesPostalState.CreateWithValidation("CA").ForceValue(),
            Zipcode = Zipcode.CreateWithValidation("92626").ForceValue(),
        }).GetAddress();

        private static void AssertSetPresentAddressFields(CAppData app)
        {
            Assert.AreEqual("362 Duane Ave", app.aBAddr);
            Assert.AreEqual("Schenectady", app.aBCity);
            Assert.AreEqual("NY", app.aBState);
            Assert.AreEqual("12345", app.aBZip);
            Assert.AreEqual(E_aBAddrT.Own, app.aBAddrT);
            Assert.AreEqual("1", app.aBAddrYrs);

            Assert.AreEqual("3016 Killybrooke Ln", app.aCAddr);
            Assert.AreEqual("Costa Mesa", app.aCCity);
            Assert.AreEqual("CA", app.aCState);
            Assert.AreEqual("92626", app.aCZip);
            Assert.AreEqual(E_aCAddrT.Rent, app.aCAddrT);
            Assert.AreEqual("2", app.aCAddrYrs);
        }

        private static void SetBorrPresentAddressFields(CAppData app)
        {
            app.aBAddr = "362 Duane Ave";
            app.aBCity = "Schenectady";
            app.aBState = "NY";
            app.aBZip = "12345";
            app.aBAddrT = E_aBAddrT.Own;
            app.aBAddrYrs = "1";
        }

        private static void SetCoboPresentAddressFields(CAppData app)
        {
            app.aCAddr = "3016 Killybrooke Ln";
            app.aCCity = "Costa Mesa";
            app.aCState = "CA";
            app.aCZip = "92626";
            app.aCAddrT = E_aCAddrT.Rent;
            app.aCAddrYrs = "2";
        }

        private static void AssertBorrPresentStreetAddressComponents(PostalAddress address)
        {
            Assert.IsInstanceOf(typeof(UnitedStatesPostalAddress), address);
            var a = (UnitedStatesPostalAddress)address;
            Assert.AreEqual(StreetAddress.Create("362 Duane Ave").ForceValue(), a.StreetAddress);
            Assert.AreEqual(null, a.UnparsedStreetAddress);
            Assert.AreEqual(UspsAddressNumber.Create("362").ForceValue(), a.AddressNumber);
            Assert.AreEqual(null, a.StreetPreDirection);
            Assert.AreEqual(StreetName.Create("Duane").ForceValue(), a.StreetName);
            Assert.AreEqual(null, a.StreetPostDirection);
            Assert.AreEqual(StreetSuffix.Create("Ave").ForceValue(), a.StreetSuffix);
            Assert.AreEqual(null, a.UnitType);
            Assert.AreEqual(null, a.UnitIdentifier);
        }

        private static void AssertCoboPresentStreetAddressComponents(PostalAddress address)
        {
            Assert.IsInstanceOf(typeof(UnitedStatesPostalAddress), address);
            var a = (UnitedStatesPostalAddress)address;
            Assert.AreEqual(StreetAddress.Create("3016 Killybrooke Ln").ForceValue(), a.StreetAddress);
            Assert.AreEqual(null, a.UnparsedStreetAddress);
            Assert.AreEqual(UspsAddressNumber.Create("3016").ForceValue(), a.AddressNumber);
            Assert.AreEqual(null, a.StreetPreDirection);
            Assert.AreEqual(StreetName.Create("Killybrooke").ForceValue(), a.StreetName);
            Assert.AreEqual(null, a.StreetPostDirection);
            Assert.AreEqual(StreetSuffix.Create("Ln").ForceValue(), a.StreetSuffix);
            Assert.AreEqual(null, a.UnitType);
            Assert.AreEqual(null, a.UnitIdentifier);
        }

        private static void SetBorrPresentAddressFields(HousingHistoryEntry entry)
        {
            entry.Address = BorrPresentAddress;
            entry.ResidencyType = ResidencyType.Own;
            entry.TimeAtAddress = (Count<UnitType.Month>)(12 * 1);
        }

        private static void SetCoboPresentAddressFields(HousingHistoryEntry entry)
        {
            entry.Address = CoboPresentAddress;
            entry.ResidencyType = ResidencyType.Rent;
            entry.TimeAtAddress = (Count<UnitType.Month>)(12 * 2);
        }

        private static void AssertSetPreviousAddress1Fields(CAppData app)
        {
            Assert.AreEqual("151 Fort Pitt Blvd APT 1406", app.aBPrev1Addr);
            Assert.AreEqual("Pittsburgh", app.aBPrev1City);
            Assert.AreEqual("PA", app.aBPrev1State);
            Assert.AreEqual("15222", app.aBPrev1Zip);
            Assert.AreEqual(E_aBPrev1AddrT.Rent, app.aBPrev1AddrT);
            Assert.AreEqual("3", app.aBPrev1AddrYrs);

            Assert.AreEqual("1330 W 61st St", app.aCPrev1Addr);
            Assert.AreEqual("Cleveland", app.aCPrev1City);
            Assert.AreEqual("OH", app.aCPrev1State);
            Assert.AreEqual("44102", app.aCPrev1Zip);
            Assert.AreEqual(E_aCPrev1AddrT.LivingRentFree, app.aCPrev1AddrT);
            Assert.AreEqual("4", app.aCPrev1AddrYrs);
        }

        private static void SetBorrPreviousAddress1Fields(CAppData app)
        {
            app.aBPrev1Addr = "151 Fort Pitt Blvd APT 1406";
            app.aBPrev1City = "Pittsburgh";
            app.aBPrev1State = "PA";
            app.aBPrev1Zip = "15222";
            app.aBPrev1AddrT = E_aBPrev1AddrT.Rent;
            app.aBPrev1AddrYrs = "3";
        }

        private static void SetCoboPreviousAddress1Fields(CAppData app)
        {
            app.aCPrev1Addr = "1330 W 61st St";
            app.aCPrev1City = "Cleveland";
            app.aCPrev1State = "OH";
            app.aCPrev1Zip = "44102";
            app.aCPrev1AddrT = E_aCPrev1AddrT.LivingRentFree;
            app.aCPrev1AddrYrs = "4";
        }

        private static void SetBorrPreviousAddress1Fields(HousingHistoryEntry entry)
        {
            entry.Address = (new UnitedStatesPostalAddress.Builder()
            {
                AddressNumber = UspsAddressNumber.Create("151"),
                StreetName = StreetName.Create("Fort Pitt"),
                StreetSuffix = StreetSuffix.Create("Blvd"),
                UnitType = AddressUnitType.Create("APT"),
                UnitIdentifier = AddressUnitIdentifier.Create("1406"),
                City = City.Create("Pittsburgh"),
                UsState = UnitedStatesPostalState.CreateWithValidation("PA"),
                Zipcode = Zipcode.CreateWithValidation("15222"),
            }).GetAddress();
            entry.ResidencyType = ResidencyType.Rent;
            entry.TimeAtAddress = (Count<UnitType.Month>)(12 * 3);
        }

        private static void SetCoboPreviousAddress1Fields(HousingHistoryEntry entry)
        {
            entry.Address = (new UnitedStatesPostalAddress.Builder()
            {
                AddressNumber = UspsAddressNumber.Create("1330"),
                StreetPreDirection = UspsDirectional.Create("W"),
                StreetName = StreetName.Create("61st"),
                StreetSuffix = StreetSuffix.Create("St"),
                City = City.Create("Cleveland"),
                UsState = UnitedStatesPostalState.CreateWithValidation("OH"),
                Zipcode = Zipcode.CreateWithValidation("44102"),
            }).GetAddress();
            entry.ResidencyType = ResidencyType.LivingRentFree;
            entry.TimeAtAddress = (Count<UnitType.Month>)(12 * 4);
        }

        private static void SetBorrPreviousAddress2Fields(HousingHistoryEntry entry)
        {
            entry.Address = (new UnitedStatesPostalAddress.Builder()
            {
                AddressNumber = UspsAddressNumber.Create("285"),
                StreetName = StreetName.Create("Philip"),
                StreetSuffix = StreetSuffix.Create("St"),
                City = City.Create("Detroit"),
                UsState = UnitedStatesPostalState.CreateWithValidation("MI"),
                Zipcode = Zipcode.CreateWithValidation("48215"),
            }).GetAddress();
            entry.ResidencyType = ResidencyType.LivingRentFree;
            entry.TimeAtAddress = (Count<UnitType.Month>)(12 * 5);
        }

        private static void SetCoboPreviousAddress2Fields(HousingHistoryEntry entry)
        {
            entry.Address = (new UnitedStatesPostalAddress.Builder()
            {
                AddressNumber = UspsAddressNumber.Create("2318"),
                StreetPreDirection = UspsDirectional.Create("N"),
                StreetName = StreetName.Create("Menard"),
                StreetSuffix = StreetSuffix.Create("Ave"),
                City = City.Create("Chicago"),
                UsState = UnitedStatesPostalState.CreateWithValidation("IL"),
                Zipcode = Zipcode.CreateWithValidation("60639"),
            }).GetAddress();
            entry.ResidencyType = ResidencyType.Own;
            entry.TimeAtAddress = (Count<UnitType.Month>)(12 * 6);
        }

        private static void SetPreviousAddress2Fields(CAppData app)
        {
            app.aBPrev2Addr = "285 Philip St";
            app.aBPrev2City = "Detroit";
            app.aBPrev2State = "MI";
            app.aBPrev2Zip = "48215";
            app.aBPrev2AddrT = E_aBPrev2AddrT.LivingRentFree;
            app.aBPrev2AddrYrs = "5";

            app.aCPrev2Addr = "2318 N Menard Ave";
            app.aCPrev2City = "Chicago";
            app.aCPrev2State = "IL";
            app.aCPrev2Zip = "60639";
            app.aCPrev2AddrT = E_aCPrev2AddrT.Own;
            app.aCPrev2AddrYrs = "6";
        }

        private static void AssertSetPreviousAddress2Fields(CAppData app)
        {
            Assert.AreEqual("285 Philip St", app.aBPrev2Addr);
            Assert.AreEqual("Detroit", app.aBPrev2City);
            Assert.AreEqual("MI", app.aBPrev2State);
            Assert.AreEqual("48215", app.aBPrev2Zip);
            Assert.AreEqual(E_aBPrev2AddrT.LivingRentFree, app.aBPrev2AddrT);
            Assert.AreEqual("5", app.aBPrev2AddrYrs);

            Assert.AreEqual("2318 N Menard Ave", app.aCPrev2Addr);
            Assert.AreEqual("Chicago", app.aCPrev2City);
            Assert.AreEqual("IL", app.aCPrev2State);
            Assert.AreEqual("60639", app.aCPrev2Zip);
            Assert.AreEqual(E_aCPrev2AddrT.Own, app.aCPrev2AddrT);
            Assert.AreEqual("6", app.aCPrev2AddrYrs);
        }

        private static PostalAddress BorrMailingAddress { get; } = (new UnitedStatesPostalAddress.Builder()
        {
            AddressNumber = UspsAddressNumber.Create("2847"),
            StreetName = StreetName.Create("Doe"),
            StreetSuffix = StreetSuffix.Create("Trl"),
            City = City.Create("Green Bay"),
            UsState = UnitedStatesPostalState.CreateWithValidation("WI"),
            Zipcode = Zipcode.CreateWithValidation("54313"),
        }).GetAddress();

        private static PostalAddress CoboMailingAddress { get; } = (new UnitedStatesPostalAddress.Builder()
        {
            AddressNumber = UspsAddressNumber.Create("1300"),
            StreetName = StreetName.Create("Mount Curve"),
            StreetSuffix = StreetSuffix.Create("Ave"),
            City = City.Create("Minneapolis"),
            UsState = UnitedStatesPostalState.CreateWithValidation("MN"),
            Zipcode = Zipcode.CreateWithValidation("55403"),
        }).GetAddress();

        private static void SetLegacyMailingAddressFields(CAppData app)
        {
            PostalAddress borrAddress = BorrMailingAddress;
            PostalAddress coboAddress = CoboMailingAddress;
            app.aBAddrMailSourceT = E_aAddrMailSourceT.Other;
            app.aBAddrMail = borrAddress.StreetAddress.ToString();
            app.aBCityMail = borrAddress.City.ToString();
            app.aBStateMail = borrAddress.State.ToString();
            app.aBZipMail = borrAddress.PostalCode.ToString();

            app.aCAddrMailSourceT = E_aAddrMailSourceT.Other;
            app.aCAddrMail = coboAddress.StreetAddress.ToString();
            app.aCCityMail = coboAddress.City.ToString();
            app.aCStateMail = coboAddress.State.ToString();
            app.aCZipMail = coboAddress.PostalCode.ToString();
        }

        private static void AssertSetLegacyMailingAddressFields(CAppData app)
        {
            PostalAddress borrAddress = BorrMailingAddress;
            PostalAddress coboAddress = CoboMailingAddress;
            Assert.AreEqual(borrAddress.StreetAddress.ToString(), app.aBAddrMail);
            Assert.AreEqual(borrAddress.City.ToString(), app.aBCityMail);
            Assert.AreEqual(borrAddress.State.ToString(), app.aBStateMail);
            Assert.AreEqual(borrAddress.PostalCode.ToString(), app.aBZipMail);

            Assert.AreEqual(coboAddress.StreetAddress.ToString(), app.aCAddrMail);
            Assert.AreEqual(coboAddress.City.ToString(), app.aCCityMail);
            Assert.AreEqual(coboAddress.State.ToString(), app.aCStateMail);
            Assert.AreEqual(coboAddress.PostalCode.ToString(), app.aCZipMail);
        }

        private static PostalAddress BorrPostClosingAddress { get; } = (new UnitedStatesPostalAddress.Builder()
        {
            AddressNumber = UspsAddressNumber.Create("2501"),
            StreetPreDirection = UspsDirectional.Create("S"),
            StreetName = StreetName.Create("Ascot"),
            StreetSuffix = StreetSuffix.Create("Ave"),
            City = City.Create("Sioux Falls"),
            UsState = UnitedStatesPostalState.CreateWithValidation("SD"),
            Zipcode = Zipcode.CreateWithValidation("57103"),
        }).GetAddress();

        private static PostalAddress CoboPostClosingAddress { get; } = (new UnitedStatesPostalAddress.Builder()
        {
            AddressNumber = UspsAddressNumber.Create("1300"),
            StreetName = StreetName.Create("Mount Curve"),
            StreetSuffix = StreetSuffix.Create("Ave"),
            City = City.Create("Minneapolis"),
            UsState = UnitedStatesPostalState.CreateWithValidation("MN"),
            Zipcode = Zipcode.CreateWithValidation("55403"),
        }).GetAddress();

        private static void SetLegacyPostClosingAddressFields(CAppData app)
        {
            PostalAddress borrAddress = BorrPostClosingAddress;
            PostalAddress coboAddress = CoboPostClosingAddress;
            app.aBAddrPostSourceTLckd = true;
            app.aBAddrPostSourceT = E_aAddrPostSourceT.Other;
            app.aBAddrPost = borrAddress.StreetAddress.ToString();
            app.aBCityPost = borrAddress.City.ToString();
            app.aBStatePost = borrAddress.State.ToString();
            app.aBZipPost = borrAddress.PostalCode.ToString();

            app.aCAddrPostSourceTLckd = true;
            app.aCAddrPostSourceT = E_aAddrPostSourceT.Other;
            app.aCAddrPost = coboAddress.StreetAddress.ToString();
            app.aCCityPost = coboAddress.City.ToString();
            app.aCStatePost = coboAddress.State.ToString();
            app.aCZipPost = coboAddress.PostalCode.ToString();
        }

        private static void AssertSetLegacyPostClosingAddressFields(CAppData app)
        {
            PostalAddress borrAddress = BorrPostClosingAddress;
            PostalAddress coboAddress = CoboPostClosingAddress;
            Assert.AreEqual(borrAddress.StreetAddress.ToString(), app.aBAddrPost);
            Assert.AreEqual(borrAddress.City.ToString(), app.aBCityPost);
            Assert.AreEqual(borrAddress.State.ToString(), app.aBStatePost);
            Assert.AreEqual(borrAddress.PostalCode.ToString(), app.aBZipPost);

            Assert.AreEqual(coboAddress.StreetAddress.ToString(), app.aCAddrPost);
            Assert.AreEqual(coboAddress.City.ToString(), app.aCCityPost);
            Assert.AreEqual(coboAddress.State.ToString(), app.aCStatePost);
            Assert.AreEqual(coboAddress.PostalCode.ToString(), app.aCZipPost);
        }
    }
}
