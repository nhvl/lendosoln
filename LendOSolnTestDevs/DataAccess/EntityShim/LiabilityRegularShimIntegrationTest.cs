﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Security;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LiabilityRegularShimIntegrationTest
    {
        private static readonly IEnumerable<E_Timing> TimingValues = Enum.GetValues(typeof(E_Timing)).Cast<E_Timing>();

        private static readonly IEnumerable<bool> BoolValues = new[] { true, false };

        private static readonly IEnumerable<Guid> MatchedReRecordIdValues = new[]
        {
            Guid.Empty,
            new Guid("11111111-1111-1111-1111-111111111111"), // Expect the shim/legacy liability to have this record.

            // We don't expect non-existent IDs to be an issue, so I'm not going to ensure identical behavior.
            //new Guid("22222222-2222-2222-2222-222222222222")  // Expect the shim/legacy liability to **NOT** have this record.
        };

        private AbstractUserPrincipal principal;
        private CPageBase loan;
        private CAppBase app;
        private ILoanLqbCollectionContainer collectionContainer;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                var loanId = creator.CreateBlankLoanFile();
                var selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, new[] { "sIsRefinancing" });
                this.loan = new CPageBaseWrapped(loanId, nameof(LiabilityRegularShimIntegrationTest), selectProvider);
                this.loan.InitLoad();
                this.app = (CAppBase)this.loan.GetAppData(0);

                // Add expected record to legacy REO collection.
                var guid = new Guid("11111111-1111-1111-1111-111111111111");
                this.app.aReCollection.EnsureRegularRecordOf(guid);

                var providerFactory = new LoanLqbCollectionProviderFactory();
                var collectionProvider = providerFactory.CreateEmptyProvider();
                var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
                loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(loan.Apps.Cast<IUladDataLayerMigrationAppDataProvider>()));

                var containerFactory = new LoanLqbCollectionContainerFactory();
                this.collectionContainer = containerFactory.Create(
                    collectionProvider,
                    loanData,
                    Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                    null);

                this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));

                this.collectionContainer.Load(
                    DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.loan.sBrokerId),
                    Substitute.For<IDbConnection>(),
                    Substitute.For<IDbTransaction>());

                var ownerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aBConsumerId);
                var recordId = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(guid);
                this.collectionContainer.Add(ownerId, recordId, new RealProperty());
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loan.sLId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            // Add expected record to legacy REO collection.
            var guid = new Guid("11111111-1111-1111-1111-111111111111");
            this.app.aReCollection.EnsureRegularRecordOf(guid);

            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanData.LegacyApplicationConsumers.Returns(this.loan.LegacyApplicationConsumers);

            var containerFactory = new LoanLqbCollectionContainerFactory();
            this.collectionContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
            this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
            this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
            this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
            this.collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));

            this.collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.loan.sBrokerId),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());

            var ownerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aBConsumerId);
            var recordId = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(guid);
            this.collectionContainer.Add(ownerId, recordId, new RealProperty());
        }

        [TearDown]
        public void TearDown()
        {
            this.app.aReCollection.ClearAll();
        }

        [Test, Combinatorial]
        public void PayoffTiming(
            [ValueSource("TimingValues")] E_Timing payoffTiming,
            [ValueSource("BoolValues")] bool willBePdOff,
            [ValueSource("BoolValues")] bool payoffTimingLckd,
            [ValueSource("BoolValues")] bool isRefinancing)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory<LendersOffice.Logging.ILoggerFactory>(new FOOL.FakeLogger.Factory());
                var loanFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
                loanFake.sIsRefinancing.Returns(isRefinancing);

                var defaultsFactory = new LiabilityDefaultsFactory();
                var defaultsProvider = defaultsFactory.Create(loanFake);

                this.loan.sLPurposeT = isRefinancing ? E_sLPurposeT.Refin : E_sLPurposeT.Purchase;
                var tuple = LiabilityRegularShimTest.GetShimAndLegacyLiability(null, defaultsProvider: defaultsProvider, app: this.app);
                var shim = tuple.Item1;
                var legacyLiability = tuple.Item2;

                shim.WillBePdOff = willBePdOff;
                shim.PayoffTimingLckd = payoffTimingLckd;
                if (payoffTiming == E_Timing.After_Closing)
                {
                    Assert.Throws<CBaseException>(() => shim.PayoffTiming = payoffTiming);
                }
                else
                {
                    shim.PayoffTiming = payoffTiming;
                }

                legacyLiability.WillBePdOff = willBePdOff;
                legacyLiability.PayoffTimingLckd = payoffTimingLckd;
                if (payoffTiming == E_Timing.After_Closing)
                {
                    Assert.Throws<CBaseException>(() => legacyLiability.PayoffTiming = payoffTiming);
                }
                else
                {
                    legacyLiability.PayoffTiming = payoffTiming;
                }

                Assert.AreEqual(legacyLiability.PayoffTiming, shim.PayoffTiming);
            }
        }

        [Test, Combinatorial]
        public void MatchedReRecordId(
            [Values(E_DebtRegularT.Revolving, E_DebtRegularT.Mortgage)] E_DebtRegularT debtType,
            [ValueSource("MatchedReRecordIdValues")] Guid matchedReRecordId)
        {
            var tuple = LiabilityRegularShimTest.GetShimAndLegacyLiability(null, shimContainer: this.collectionContainer as IShimContainer, defaultsProvider: this.GetDefaultsProvider(), app: this.app);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.DebtT = debtType;
            shim.MatchedReRecordId = matchedReRecordId;

            legacyLiability.DebtT = debtType;
            legacyLiability.MatchedReRecordId = matchedReRecordId;

            Assert.AreEqual(legacyLiability.MatchedReRecordId, shim.MatchedReRecordId);
        }

        [Test]
        public void IsSubjectPropertyMortgage_Get(
            [Values(E_DebtRegularT.Revolving, E_DebtRegularT.Mortgage)] E_DebtRegularT debtType,
            [ValueSource("MatchedReRecordIdValues")] Guid matchedReRecordId,
            [ValueSource("BoolValues")] bool reoIsSubjectProperty)
        {
            try
            {
                var legacyReo = this.app.aReCollection.GetRegRecordOf(matchedReRecordId);
                legacyReo.IsSubjectProp = reoIsSubjectProperty;
            }
            catch (CBaseException)
            {
                // Record doesn't exist.
            }

            RealProperty newReo;
            if (this.collectionContainer.RealProperties.TryGetValue(DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(matchedReRecordId), out newReo))
            {
                newReo.IsSubjectProp = reoIsSubjectProperty;
            }

            var tuple = LiabilityRegularShimTest.GetShimAndLegacyLiability(null, shimContainer: this.collectionContainer as IShimContainer, defaultsProvider: this.GetDefaultsProvider(), app: this.app);
            var shim = tuple.Item1;
            var legacyLiability = tuple.Item2;

            shim.DebtT = debtType;
            shim.MatchedReRecordId = matchedReRecordId;

            legacyLiability.DebtT = debtType;
            legacyLiability.MatchedReRecordId = matchedReRecordId;

            Assert.AreEqual(legacyLiability.IsSubjectPropertyMortgage, shim.IsSubjectPropertyMortgage);
        }

        [Test]
        public void IsSubjectPropertyMortgage_SetToTrueWithNoExistingSubjectPropertyReo_CreatesSubjectPropertyReoAndAssociation()
        {
            // Remove the REO record that all of these tests start with...
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aBConsumerId);
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aCConsumerId);
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.app.aAppId);
            this.collectionContainer.RemoveRealPropertyFromLegacyApplication(this.collectionContainer.RealProperties.Keys.First(), appId, borrowerId, coborrowerId);

            var tuple = LiabilityRegularShimTest.GetShimAndLegacyLiability(null, shimContainer: this.collectionContainer as IShimContainer, defaultsProvider: this.GetDefaultsProvider(), app: this.app);
            var shim = tuple.Item1;
            shim.DebtT = E_DebtRegularT.Mortgage;
            shim.IsSubjectPropertyMortgage = true;

            Assert.AreEqual(1, this.collectionContainer.RealProperties.Count);
            var realPropertyId = this.collectionContainer.RealProperties.First().Key;

            Assert.AreEqual(1, this.collectionContainer.RealPropertyLiabilities.Count);
            var association = this.collectionContainer.RealPropertyLiabilities.Values.Single();
            Assert.AreEqual(shim.RecordId, association.LiabilityId.Value);
            Assert.AreEqual(realPropertyId, association.RealPropertyId);
        }

        [Test]
        public void IsSubjectPropertyMortgage_SetToTrueWithExistingSubjectPropertyReo_CreatesAssociationWithExistingReo()
        {
            // Make the existing REO a subject property REO.
            var realPropertyKvp = this.collectionContainer.RealProperties.Single();
            var realPropertyId = realPropertyKvp.Key;
            var realProperty = realPropertyKvp.Value;
            realProperty.IsSubjectProp = true;
            var tuple = LiabilityRegularShimTest.GetShimAndLegacyLiability(null, shimContainer: this.collectionContainer as IShimContainer, defaultsProvider: this.GetDefaultsProvider(), app: this.app);
            var shim = tuple.Item1;
            shim.DebtT = E_DebtRegularT.Mortgage;

            // Just to be safe, make sure this is 0.
            Assert.AreEqual(0, this.collectionContainer.RealPropertyLiabilities.Count);

            shim.IsSubjectPropertyMortgage = true;

            // Don't want this to increment.
            Assert.AreEqual(1, this.collectionContainer.RealProperties.Count);

            // There should be one association.
            Assert.AreEqual(1, this.collectionContainer.RealPropertyLiabilities.Count);
            var association = this.collectionContainer.RealPropertyLiabilities.Values.First();
            Assert.AreEqual(shim.RecordId, association.LiabilityId.Value);
            Assert.AreEqual(realPropertyId, association.RealPropertyId);
        }

        [Test]
        public void IsSubjectPropertyMortgage_SetToFalseAssociatedWithSubjectPropertyReo_RemovesAssociation()
        {
            // Make the existing REO a subject property REO.
            var realPropertyKvp = this.collectionContainer.RealProperties.Single();
            var realPropertyId = realPropertyKvp.Key;
            var realProperty = realPropertyKvp.Value;
            realProperty.IsSubjectProp = true;
            var tuple = LiabilityRegularShimTest.GetShimAndLegacyLiability(null, shimContainer: this.collectionContainer as IShimContainer, defaultsProvider: this.GetDefaultsProvider(), app: this.app);
            var shim = tuple.Item1;
            shim.DebtT = E_DebtRegularT.Mortgage;

            var liabilityId = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(shim.RecordId);

            var aggregateShim = (IShimContainer)this.collectionContainer;
            aggregateShim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

            // Just to be safe, make sure this is 1.
            Assert.AreEqual(1, this.collectionContainer.RealPropertyLiabilities.Count);

            shim.IsSubjectPropertyMortgage = false;

            // This should decrement.
            Assert.AreEqual(0, this.collectionContainer.RealPropertyLiabilities.Count);
        }

        [Test]
        public void IsSubjectPropertyMortgage_SetToFalseAssociatedWithNonSubjectPropertyReo_DoesNotRemoveAssociation()
        {
            var realPropertyKvp = this.collectionContainer.RealProperties.Single();
            var realPropertyId = realPropertyKvp.Key;
            var realProperty = realPropertyKvp.Value;
            realProperty.IsSubjectProp = false;
            var tuple = LiabilityRegularShimTest.GetShimAndLegacyLiability(null, shimContainer: this.collectionContainer as IShimContainer, defaultsProvider: this.GetDefaultsProvider(), app: this.app);
            var shim = tuple.Item1;
            shim.DebtT = E_DebtRegularT.Mortgage;

            var liabilityId = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(shim.RecordId);

            var aggregateShim = (IShimContainer)this.collectionContainer;
            aggregateShim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

            // Just to be safe, make sure this is 1.
            Assert.AreEqual(1, this.collectionContainer.RealPropertyLiabilities.Count);

            shim.IsSubjectPropertyMortgage = false;

            // This should not decrement.
            Assert.AreEqual(1, this.collectionContainer.RealProperties.Count);
        }

        private ILiabilityDefaultsProvider GetDefaultsProvider()
        {
            var defaultsFactory = new LiabilityDefaultsFactory();
            return defaultsFactory.Create(this.loan);
        }
    }
}
