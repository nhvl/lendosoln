﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityCollectionShimBaseTest : RecordCollectionShimTest
    {
        private static Guid id0;

        public override void CreateCollection()
        {
            var collection = InitializeCollection();

            this.baseShim = collection;
            this.firstRecordId = id0;
            this.countRegular = 4;
            this.countSpecial = 4;
        }

        public static ILiaCollection InitializeCollection()
        {
            var converter = new LosConvert();

            var entities = LiabilityCollectionShimTest.GetDefaultLiabilities(8);
            id0 = entities[0].Key;
            entities[4].Value.DebtType = E_DebtT.Alimony;
            entities[5].Value.DebtType = E_DebtT.ChildSupport;
            entities[6].Value.DebtType = E_DebtT.JobRelatedExpense;
            entities[7].Value.DebtType = E_DebtT.JobRelatedExpense;

            var collectionContainer = LiabilityCollectionShimTest.GetCollectionContainer(entities);

            var fakeApp = Substitute.For<IShimAppDataProvider>();
            var fakeDefaultsProvider = Substitute.For<ILiabilityDefaultsProvider>();
            var fakeOwnershipManager = Substitute.For<IShimOwnershipManager<Guid>>();
            return new LiabilityCollectionShim(collectionContainer as IShimContainer, fakeDefaultsProvider, fakeApp, fakeOwnershipManager, converter);
        }

        protected override List<ICollectionItemBase2> RetrieveSpecialRecords()
        {
            var collection = (ILiaCollection)this.baseShim;

            var records = new List<ICollectionItemBase2>();
            records.Add(collection.GetAlimony(false));
            records.Add(collection.GetChildSupport(false));
            records.Add(collection.GetJobRelated1(false));
            records.Add(collection.GetJobRelated2(false));

            return records;
        }
    }
}
