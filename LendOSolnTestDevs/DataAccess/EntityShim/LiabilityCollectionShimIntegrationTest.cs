﻿namespace LendingQB.Test.Developers.DataAccess.EntityShim
{
    using System;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LiabilityCollectionShimIntegrationTest
    {
        private AbstractUserPrincipal principal;
        private CPageBase loan;
        private CAppBase app;
        private ILiaCollection legacyLiabilities;
        private IReCollection legacyRealProperties;
        private ILiaCollection shimLiabilities;
        private IReCollection shimRealProperties;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                var loanId = creator.CreateBlankLoanFile();
                var selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, new[] { "sIsRefinancing", "aLiaCollection", "aReCollection" });
                this.loan = new CPageBaseWrapped(loanId, nameof(LiabilityRegularShimIntegrationTest), selectProvider);
                this.loan.InitLoad();
                this.app = (CAppBase)this.loan.GetAppData(0);
                this.app.aBRetainedNegCf_ = 8;
                this.app.aCRetainedNegCf_ = 9;

                this.legacyLiabilities = this.app.aLiaCollection;
                this.legacyRealProperties = this.app.aReCollection;

                var providerFactory = new LoanLqbCollectionProviderFactory();
                var collectionProvider = providerFactory.CreateEmptyProvider();

                var containerFactory = new LoanLqbCollectionContainerFactory();
                var collectionContainer = containerFactory.Create(
                    collectionProvider,
                    this.loan,
                    Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                    null);

                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));

                collectionContainer.Load(
                    DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.loan.sBrokerId),
                    Substitute.For<IDbConnection>(),
                    Substitute.For<IDbTransaction>());

                var ownershipManager = new ShimOwnershipManager<DataObjectKind.Liability, DataObjectKind.ConsumerLiabilityAssociation, Liability, ConsumerLiabilityAssociation>(
                    (IShimContainer)collectionContainer,
                    DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aBConsumerId),
                    DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aCConsumerId),
                    DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.app.aAppId),
                    (consumerId, recordId, appId) => new ConsumerLiabilityAssociation(consumerId, recordId, appId));

                var defaultsFactory = new LiabilityDefaultsFactory();
                this.shimLiabilities = new LiabilityCollectionShim(
                    (IShimContainer)collectionContainer,
                    defaultsFactory.Create(this.loan),
                    this.app,
                    ownershipManager,
                    new LosConvert());

                var reoOwnershipManager = new ShimOwnershipManager<DataObjectKind.RealProperty, DataObjectKind.ConsumerRealPropertyAssociation, RealProperty, ConsumerRealPropertyAssociation>(
                    (IShimContainer)collectionContainer,
                    DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aBConsumerId),
                    DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.app.aCConsumerId),
                    DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.app.aAppId),
                    (consumerId, recordId, appId) => new ConsumerRealPropertyAssociation(consumerId, recordId, appId));

                this.shimRealProperties = new RealPropertyCollectionShim(
                    this.app,
                    (IShimContainer)collectionContainer,
                    reoOwnershipManager,
                    new LosConvert());
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loan.sLId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [TearDown]
        public void TearDown()
        {
            this.legacyLiabilities.ClearAll();
            this.shimLiabilities.ClearAll();

            this.legacyRealProperties.ClearAll();
            this.shimRealProperties.ClearAll();
        }

        [Test]
        public void Flush_VariousLiabilities_ResultsInSameAggregateDataAsLegacyCollection()
        {
            this.InitializeCollectionsForFlushTest(this.legacyLiabilities, this.legacyRealProperties);
            this.InitializeCollectionsForFlushTest(this.shimLiabilities, this.shimRealProperties);

            this.legacyLiabilities.Flush();
            Assert.AreEqual(11m, this.app.aLiaBalTot);
            Assert.AreEqual(30m, this.app.aLiaMonTot);
            Assert.AreEqual(4, this.app.aLiaPdOffTot);

            this.app.AcceptLiabilityAggregateTotals(new CalculatedFields.Liability.AggregateData(0, 0, 0));
            Assert.AreEqual(0m, this.app.aLiaBalTot);
            Assert.AreEqual(0m, this.app.aLiaMonTot);
            Assert.AreEqual(0m, this.app.aLiaPdOffTot);

            this.shimLiabilities.Flush();
            Assert.AreEqual(11m, this.app.aLiaBalTot);
            Assert.AreEqual(30m, this.app.aLiaMonTot);
            Assert.AreEqual(4, this.app.aLiaPdOffTot);

        }

        private void InitializeCollectionsForFlushTest(
            ILiaCollection liabilities,
            IReCollection realProperties)
        {
            // We need a liability associated with an REO that is sold.
            var regularRecord1 = liabilities.AddRegularRecord();
            regularRecord1.Bal = 1;
            regularRecord1.PayoffAmtLckd = true;
            regularRecord1.PayoffAmt = 1;
            regularRecord1.Pmt = 1;
            regularRecord1.DebtT = E_DebtRegularT.Mortgage;

            var realProperty1 = realProperties.AddRegularRecord();
            realProperty1.StatT = E_ReoStatusT.Sale;
            regularRecord1.MatchedReRecordId = realProperty1.RecordId;

            // We need a liability associated with an REO that is not sold.
            var regularRecord2 = liabilities.AddRegularRecord();
            regularRecord2.Bal = 2;
            regularRecord2.PayoffAmtLckd = true;
            regularRecord2.PayoffAmt = 2;
            regularRecord2.Pmt = 2;
            regularRecord2.DebtT = E_DebtRegularT.Mortgage;

            var realProperty2 = realProperties.AddRegularRecord();
            realProperty2.StatT = E_ReoStatusT.Rental;
            regularRecord2.MatchedReRecordId = realProperty2.RecordId;

            // We need a liability that is marked as exclude from underwriting
            var regularRecord3 = liabilities.AddRegularRecord();
            regularRecord3.Bal = 3;
            regularRecord3.PayoffAmtLckd = true;
            regularRecord3.PayoffAmt = 3;
            regularRecord3.Pmt = 3;
            regularRecord3.ExcFromUnderwriting = true;

            // We need a liability that is marked as will be paid off with payoff timing at closing
            var regularRecord4 = liabilities.AddRegularRecord();
            regularRecord4.Bal = 4;
            regularRecord4.PayoffAmtLckd = true;
            regularRecord4.PayoffAmt = 4;
            regularRecord4.Pmt = 4;
            regularRecord4.WillBePdOff = true;
            regularRecord4.PayoffTimingLckd = true;
            regularRecord4.PayoffTiming = E_Timing.At_Closing;

            // We need a liability marked as not used in ratio
            var regularRecord5 = liabilities.AddRegularRecord();
            regularRecord5.Bal = 5;
            regularRecord5.PayoffAmtLckd = true;
            regularRecord5.PayoffAmt = 5;
            regularRecord5.Pmt = 5;
            regularRecord5.NotUsedInRatio = true;

            // We need a special liability
            var specialRecord1 = liabilities.GetAlimony(true);
            specialRecord1.Pmt = 6;

            // We also need a special liability that is marked not used in ratio
            var specialRecord2 = liabilities.GetChildSupport(true);
            specialRecord2.Pmt = 7;
            specialRecord2.NotUsedInRatio = true;
        }

    }
}
