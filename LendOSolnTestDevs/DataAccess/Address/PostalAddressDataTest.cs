﻿namespace LendingQB.Test.Developers.DataAccess.Address
{
    using global::DataAccess;
    using global::LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class PostalAddressDataTest
    {
        [Test]
        public void Construct_FromGeneral_BuildMatches()
        {
            var addressStart = GeneralPostalAddressTest.CreateAddress();

            var data = new PostalAddressData(addressStart);

            var addressEnd = data.BuildPostalAddress();
            Assert.IsTrue(addressStart.GetType() == addressEnd.GetType());
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.City.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.Country.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.CountryCode.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.PostalCode.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.State.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.StreetAddress.ToString()));

            Assert.AreEqual(addressStart.City, addressEnd.City);
            Assert.AreEqual(addressStart.Country, addressEnd.Country);
            Assert.AreEqual(addressStart.CountryCode, addressEnd.CountryCode);
            Assert.AreEqual(addressStart.PostalCode, addressEnd.PostalCode);
            Assert.AreEqual(addressStart.State, addressEnd.State);
            Assert.AreEqual(addressStart.StreetAddress, addressEnd.StreetAddress);
        }

        [Test]
        public void Construct_FromUnparsedUS_BuildMatches()
        {
            var addressStart = UnitedStatesPostalAddressTest.CreateUnparsedAddress();

            var data = new PostalAddressData(addressStart);

            var addressEnd = data.BuildPostalAddress();
            Assert.IsTrue(addressStart.GetType() == addressEnd.GetType());
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.City.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.Country.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.CountryCode.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.PostalCode.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.State.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.StreetAddress.ToString()));

            Assert.AreEqual(addressStart.City, addressEnd.City);
            Assert.AreEqual(addressStart.Country, addressEnd.Country);
            Assert.AreEqual(addressStart.CountryCode, addressEnd.CountryCode);
            Assert.AreEqual(addressStart.Zipcode.ToString(), addressEnd.PostalCode.ToString());
            Assert.AreEqual(addressStart.UsState.ToString(), addressEnd.State.ToString());
            Assert.AreEqual(addressStart.StreetAddress, addressEnd.StreetAddress);
        }

        [Test]
        public void Construct_FromParsedUS_BuildMatches()
        {
            var addressStart = UnitedStatesPostalAddressTest.CreateParsedAddress();

            var data = new PostalAddressData(addressStart);

            var addressEnd = data.BuildPostalAddress();
            Assert.IsTrue(addressStart.GetType() == addressEnd.GetType());
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.City.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.Country.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.CountryCode.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.PostalCode.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEnd.State.ToString()));

            var addressEndUSA = (UnitedStatesPostalAddress)addressEnd;
            Assert.IsFalse(string.IsNullOrEmpty(addressEndUSA.AddressNumber.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEndUSA.StreetPreDirection.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEndUSA.StreetName.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEndUSA.StreetSuffix.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEndUSA.StreetPostDirection.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEndUSA.UnitType.ToString()));
            Assert.IsFalse(string.IsNullOrEmpty(addressEndUSA.UnitIdentifier.ToString()));

            Assert.AreEqual(addressStart.City, addressEnd.City);
            Assert.AreEqual(addressStart.Country, addressEnd.Country);
            Assert.AreEqual(addressStart.CountryCode, addressEnd.CountryCode);
            Assert.AreEqual(addressStart.Zipcode.ToString(), addressEnd.PostalCode.ToString());
            Assert.AreEqual(addressStart.UsState.ToString(), addressEnd.State.ToString());
            Assert.AreEqual(addressStart.StreetAddress, addressEnd.StreetAddress);

            Assert.AreEqual(addressStart.AddressNumber, addressEndUSA.AddressNumber);
            Assert.AreEqual(addressStart.StreetPreDirection, addressEndUSA.StreetPreDirection);
            Assert.AreEqual(addressStart.StreetName, addressEndUSA.StreetName);
            Assert.AreEqual(addressStart.StreetSuffix, addressEndUSA.StreetSuffix);
            Assert.AreEqual(addressStart.StreetPostDirection, addressEndUSA.StreetPostDirection);
            Assert.AreEqual(addressStart.UnitType, addressEndUSA.UnitType);
            Assert.AreEqual(addressStart.UnitIdentifier, addressEndUSA.UnitIdentifier);
        }
    }
}
