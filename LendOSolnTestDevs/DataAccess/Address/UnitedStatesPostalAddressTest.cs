﻿namespace LendingQB.Test.Developers.DataAccess.Address
{
    using global::DataAccess;
    using global::LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class UnitedStatesPostalAddressTest
    {
        private const string country = "United States";
        private const string countryCode = "USA";
        private const string postalCode = "92646";
        private const string state = "CA";
        private const string city = "Costa Mesa";
        private const string streetAddress = "1600 Sunflower Ave";
        private const string number = "1600";
        private const string preDir = "East";
        private const string street = "Sunflower";
        private const string postDir = "Southwest";
        private const string streetType = "Ave";
        private const string unitType = "Suite";
        private const string unitNum = "100";
        private const string combined = "1600 East Sunflower Ave Southwest Suite 100";

        [Test]
        public void UnparsedAddress_GetAddress_FieldsMatch()
        {
            var address = CreateUnparsedAddress();

            Assert.AreEqual(country, address.Country.ToString());
            Assert.AreEqual(countryCode, address.CountryCode.ToString());
            Assert.AreEqual(postalCode, address.PostalCode.ToString());
            Assert.AreEqual(state, address.State.ToString());
            Assert.AreEqual(city, address.City.ToString());
            Assert.AreEqual(streetAddress, address.StreetAddress.ToString());
        }

        [Test]
        public void ParsedAddress_GetAddress_FieldsMatch()
        {
            var address = CreateParsedAddress();

            Assert.AreEqual(country, address.Country.ToString());
            Assert.AreEqual(countryCode, address.CountryCode.ToString());
            Assert.AreEqual(postalCode, address.PostalCode.ToString());
            Assert.AreEqual(state, address.State.ToString());
            Assert.AreEqual(city, address.City.ToString());
            Assert.AreEqual(number, address.AddressNumber.ToString());
            Assert.AreEqual(preDir, address.StreetPreDirection.ToString());
            Assert.AreEqual(street, address.StreetName.ToString());
            Assert.AreEqual(postDir, address.StreetPostDirection.ToString());
            Assert.AreEqual(streetType, address.StreetSuffix.ToString());
            Assert.AreEqual(unitType, address.UnitType.ToString());
            Assert.AreEqual(unitNum, address.UnitIdentifier.ToString());
            Assert.AreEqual(combined, address.StreetAddress.ToString());
        }

        public static UnitedStatesPostalAddress CreateUnparsedAddress()
        {
            var builder = new UnitedStatesPostalAddress.Builder();
            builder.Zipcode = Zipcode.CreateWithValidation(postalCode).Value;
            builder.UsState = UnitedStatesPostalState.Create(state).Value;
            builder.City = City.Create(city).Value;
            builder.UnparsedStreetAddress = StreetAddress.Create(streetAddress).Value;

            return builder.GetAddress();
        }

        public static UnitedStatesPostalAddress CreateParsedAddress()
        {
            var builder = new UnitedStatesPostalAddress.Builder();
            builder.Zipcode = Zipcode.CreateWithValidation(postalCode).Value;
            builder.UsState = UnitedStatesPostalState.Create(state).Value;
            builder.City = City.Create(city).Value;
            builder.AddressNumber = UspsAddressNumber.Create(number).Value;
            builder.StreetPreDirection = UspsDirectional.CreateWithValidation(preDir);
            builder.StreetName = StreetName.Create(street);
            builder.StreetPostDirection = UspsDirectional.CreateWithValidation(postDir);
            builder.StreetSuffix = StreetSuffix.Create(streetType);
            builder.UnitType = AddressUnitType.Create(unitType);
            builder.UnitIdentifier = AddressUnitIdentifier.Create(unitNum);

            return builder.GetAddress();
        }
    }
}
