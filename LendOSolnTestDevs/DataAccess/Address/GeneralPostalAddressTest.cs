﻿namespace LendingQB.Test.Developers.DataAccess.Address
{
    using global::DataAccess;
    using global::LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class GeneralPostalAddressTest
    {
        private const string country = "Mexico";
        private const string countryCode = "MEX";
        private const string postalCode = "MX-561";
        private const string state = "DF";
        private const string city = "Ciudad de Mexico";
        private const string streetAddress = "541 Camino Ejercito";

        [Test]
        public void Builder_GetAddress_FieldsMatch()
        {
            var address = CreateAddress();

            Assert.AreEqual(country, address.Country.ToString());
            Assert.AreEqual(countryCode, address.CountryCode.ToString());
            Assert.AreEqual(postalCode, address.PostalCode.ToString());
            Assert.AreEqual(state, address.State.ToString());
            Assert.AreEqual(city, address.City.ToString());
            Assert.AreEqual(streetAddress, address.StreetAddress.ToString());
        }

        public static GeneralPostalAddress CreateAddress()
        {
            var builder = new GeneralPostalAddress.Builder();
            builder.CountryCode = CountryCodeIso3.CreateWithValidation(countryCode);
            builder.PostalCode = PostalCode.CreateWithValidation(postalCode, builder.CountryCode.Value);
            builder.State = AdministrativeArea.Create(state);
            builder.City = City.Create(city);
            builder.StreetAddress = StreetAddress.Create(streetAddress);

            return builder.GetAddress();
        }
    }
}
