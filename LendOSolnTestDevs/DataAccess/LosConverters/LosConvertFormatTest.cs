﻿// NOTE: This was copied from code written by David Dao in the test module LendOSolnTest.
//       I've made some changes to include tests of the refactored version of LosConvert.
namespace LendingQB.Test.Developers.DataAccess.LosConverters
{
    using System;
    using global::DataAccess;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LosConvertFormatTest
    {
        [Test]
        public void NewInterfaceTest()
        {
            // This test was added specifically to test the pure usage of the refactored LosConvert.  The
            // remaining tests were designed to test the original LosConvert, but have been modifed to 
            // also test the refactored LosConvert.

            string input = "123-45-6789";
            string expected = "123456789";
            string desc = string.Format("FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, Input=[{0}]", input);

            var direction = FormatDirection.ToRep;
            var testConverter = LosConvert.Create(FormatTarget.FannieMaeMornetPlus);

            string output = testConverter.ToSsnFormat(input, direction);

            Assert.AreEqual(expected, output, desc);
        }

        [Test]
        public void ToSsnFormatTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, FormatDirection, inputString, expectedOutput}
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "", ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, null, ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "111-11-1111", "111-11-1111"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, " 111 -11-1111 ", "111-11-1111"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "   111-11", "111-11"},
                new object[] { FormatTarget.PrintoutNormalFields, FormatDirection.ToRep, "111-11-1111", "111-11-1111"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToDb, "123456789", "123-45-6789"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, "123456789", "123456789"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToDb, "123-45-6789", "123-45-6789"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, "123-45-6789", "123456789"},

            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];
                FormatDirection direction = (FormatDirection)testCase[1];
                string input = testCase[2] as string;
                string expectedOutput = testCase[3] as string;

                string desc = string.Format("FormatTarget.{0}, FormatDirection{1}, Input=[{2}]", target, direction, input);

                LosConvert testConveter = new LosConvert(target);
                string output = testConveter.ToSsnFormat(input, direction);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                var newDirection = direction;
                string newOutput = newConverter.ToSsnFormat(input, newDirection);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToEmployerTaxIdentifierFormatTest()
        {
            // Test has following format { FormatTarget, inputString, expectedOutput}
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, "", ""},
                new object[] { FormatTarget.Webform, null, ""},
                new object[] { FormatTarget.Webform, "111111111", "11-1111111"},
                new object[] { FormatTarget.FannieMaeMornetPlus, "111111111", "111111111"},

            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];
                string input = testCase[1] as string;
                string expectedOutput = testCase[2] as string;

                string desc = string.Format("FormatTarget.{0}, , Input=[{1}]", target, input);

                LqbGrammar.DataTypes.EmployerTaxIdentificationNumber? ein = null;
                if (input != null)
                {
                    ein = LqbGrammar.DataTypes.EmployerTaxIdentificationNumber.Create(input);
                }

                LosConvert testConveter = new LosConvert(target);
                string output = testConveter.ToEmployerTaxIdentificationNumberString(ein);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToEmployerTaxIdentificationNumberString(ein);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToPhoneNumFormatTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, FormatDirection, inputString, expectedOutput}
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, null, ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "", ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToDb, "1234567890", "(123) 456-7890"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "1234567890", "1234567890"},

                new object[] { FormatTarget.PointNative, FormatDirection.ToDb, "1234567890", "(123) 456-7890"},
                new object[] { FormatTarget.PointNative, FormatDirection.ToRep, "1234567890", "123-456-7890"},

            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];
                FormatDirection direction = (FormatDirection)testCase[1];
                string input = testCase[2] as string;
                string expectedOutput = testCase[3] as string;

                string desc = string.Format("FormatTarget.{0}, FormatDirection{1}, Input=[{2}]", target, direction, input);

                LosConvert testConveter = new LosConvert(target);
                string output = testConveter.ToPhoneNumFormat(input, direction);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToPhoneNumFormat(input, direction);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }


        [Test]
        public void ToDateTimeStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, DateTime.MinValue, ""},
                new object[] {FormatTarget.Webform, new DateTime(2017, 8, 29), (new DateTime(2017, 8, 29)).ToShortDateString()},
                new object[] {FormatTarget.GinnieNet, new DateTime(2017, 8, 29), "20170829"},
                new object[] {FormatTarget.MismoClosing, new DateTime(2017, 8, 29), "2017-08-29"},
                new object[] {FormatTarget.DocMagic, new DateTime(2017, 8, 29), "2017/08/29"},
                new object[] {FormatTarget.XsltExport, new DateTime(2017, 8, 29), "08/29/2017"},
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                DateTime input = (DateTime)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToDateTimeString(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToDateTimeString(input, false);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }


        [Test]
        public void ToDateTimeOffsetStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, DateTimeOffset.MinValue, ""},
                new object[] {FormatTarget.Webform, new DateTimeOffset(new DateTime(2017, 8, 29)), (new DateTimeOffset(new DateTime(2017, 8, 29))).DateTime.ToShortDateString()},
                new object[] {FormatTarget.GinnieNet, new DateTimeOffset(new DateTime(2017, 8, 29)), "20170829"},
                new object[] {FormatTarget.MismoClosing, new DateTimeOffset(new DateTime(2017, 8, 29)), "2017-08-29"},
                new object[] {FormatTarget.DocMagic, new DateTimeOffset(new DateTime(2017, 8, 29)), "2017/08/29"},
                new object[] {FormatTarget.XsltExport, new DateTimeOffset(new DateTime(2017, 8, 29)), "08/29/2017"},
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                DateTimeOffset input = (DateTimeOffset)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToDateTimeString(input, false);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToDateTimeString(input, false);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToDateTimeVarCharStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, null, ""},
                new object[] {FormatTarget.Webform, "8/29/2017", "8/29/2017"},
                new object[] {FormatTarget.GinnieNet, "8/29/2017", "20170829"},
                new object[] {FormatTarget.MismoClosing, "8/29/2017", "2017-08-29"},
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                string input = testCase[1] as string;
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToDateTimeVarCharString(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToDateTimeVarCharString(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToMoneyStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 0.0M, "$0.00"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 1000.999M, "$1,001.00"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 1000.96M, "$1,000.96"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, -1000.96M, "($1,000.96)"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, -1234M, "($1,234.00)"},
                new object[] { FormatTarget.PrintoutNormalFields, FormatDirection.ToRep, 0.0M, ""},
                new object[] { FormatTarget.PrintoutImportantFields, FormatDirection.ToRep, 0.0M, "0.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, 0.0M, ""},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, 123.0M,   "         123.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, 1234.0M,  "        1234.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, -123.0M,  "        -123.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, -1234.0M, "       -1234.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, 0.0M, "0.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, 123.0M,   "123.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, 1234.0M,  "1234.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, -123.0M,  "-123.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, -1234.0M, "-1234.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, 0.0M, "0.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, 123.0M,   "123.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, 1234.0M,  "1234.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, -123.0M,  "-123.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, -1234.0M, "-1234.00"},

                // 2/4/2011 dd - These test are for banker rounding.
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.125M, "$10.12"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.135M, "$10.14"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.125001M, "$10.13"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.135001M, "$10.14"},

            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];
                FormatDirection direction = (FormatDirection)testCase[1];
                decimal input = (decimal)testCase[2];
                string expectedOutput = (string)testCase[3];

                string desc = string.Format("FormatTarget.{0}, FormatDirection.{1}, Input=[{2}]", target, direction, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToMoneyString(input, direction);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToMoneyString(input, direction);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToMoneyVarCharStringRepTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, null, "$0.00"},
                new object[] { FormatTarget.GinnieNet, "howdy", string.Empty},
                new object[] { FormatTarget.Webform, "doody", "doody"},
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                string input = testCase[1] as string;
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToMoneyVarCharStringRep(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToMoneyVarCharStringRep(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToDecimalStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, FormatDirection, input, expectedOutput }
            //  8/30/2017 ad - Note that the direction is ignored so don't try to vary it now...may become important in the future though.
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, FormatDirection.ToDb, 0m, "0.00" },
                new object[] { FormatTarget.PointNative, FormatDirection.ToDb, 0m, "" },
                new object[] { FormatTarget.XsltExport, FormatDirection.ToDb, 0m, "0" },
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToDb, 1.234m, "   1.23" },
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];
                FormatDirection direction = (FormatDirection)testCase[1];

                decimal input = (decimal)testCase[2];
                string expectedOutput = (string)testCase[3];

                string desc = string.Format("FormatTarget.{0}, FormatDirection.{1}, Input=[{2}]", target, direction, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToDecimalString(input, direction);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToDecimalString(input, direction);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToRateStringNoPercentTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            //  8/30/2017 ad - Note that the direction is ignored so don't try to vary it now...may become important in the future though.
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, 0m, "0.000" },
                new object[] { FormatTarget.PointNative, 0m, "" },
                new object[] { FormatTarget.XsltExport, 0m, "0" },
                new object[] { FormatTarget.FannieMaeMornetPlus, 1.234m, "  1.234" },
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                decimal input = (decimal)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToRateStringNoPercent(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToRateStringNoPercent(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToRateStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, 0.0M, "0.000%"},
                new object[] { FormatTarget.Webform, 97.0M, "97.000%"},
                new object[] { FormatTarget.Webform, -97.0M, "-97.000%"},

                // 2/4/2011 dd - Add for banker rounding test
                new object[] { FormatTarget.Webform, 97.1235M, "97.124%"},
                new object[] { FormatTarget.Webform, 97.1245M, "97.124%"},
                new object[] { FormatTarget.Webform, 97.1235001M, "97.124%"},
                new object[] { FormatTarget.Webform, 97.1245001M, "97.125%"}
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                decimal input = (decimal)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToRateString(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToRateString(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToRateString4DigitsTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, 0.0M, "0.0000%"},
                new object[] { FormatTarget.Webform, 97.0M, "97.0000%"},
                new object[] { FormatTarget.Webform, -97.0M, "-97.0000%"},
                new object[] { FormatTarget.Webform, 97.1235M, "97.1235%"},
                new object[] { FormatTarget.Webform, 97.1245M, "97.1245%"},
                new object[] { FormatTarget.Webform, 97.1235001M, "97.1235%"},
                new object[] { FormatTarget.Webform, 97.1245001M, "97.1245%"},
                new object[] { FormatTarget.FannieMaeMornetPlus, 97.1245001M, " 97.125"}
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                decimal input = (decimal)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToRateString4DecimalDigits(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToRateString4DecimalDigits(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToRateString6DigitsTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, 0.0M, "0.000000%"},
                new object[] { FormatTarget.Webform, 97.0M, "97.000000%"},
                new object[] { FormatTarget.Webform, -97.0M, "-97.000000%"},
                new object[] { FormatTarget.Webform, 97.1235M, "97.123500%"},
                new object[] { FormatTarget.Webform, 97.1245M, "97.124500%"},
                new object[] { FormatTarget.Webform, 97.1235001M, "97.123500%"},
                new object[] { FormatTarget.Webform, 97.1245007M, "97.124501%"},
                new object[] { FormatTarget.FannieMaeMornetPlus, 97.1245001M, " 97.125"}
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                decimal input = (decimal)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToRateString6DecimalDigits(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToRateString6DecimalDigits(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToRateString9DigitsTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, 0.0M, "0.000000000%"},
                new object[] { FormatTarget.Webform, 97.0M, "97.000000000%"},
                new object[] { FormatTarget.Webform, -97.0M, "-97.000000000%"},
                new object[] { FormatTarget.Webform, 97.1235M, "97.123500000%"},
                new object[] { FormatTarget.Webform, 97.1245M, "97.124500000%"},
                new object[] { FormatTarget.Webform, 97.1235001M, "97.123500100%"},
                new object[] { FormatTarget.Webform, 97.1245007M, "97.124500700%"},
                new object[] { FormatTarget.FannieMaeMornetPlus, 97.1245001M, " 97.125"}
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                decimal input = (decimal)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToRateString9DecimalDigits(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToRateString9DecimalDigits(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToCountVarCharStringRepTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, "0", "0"},
                new object[] { FormatTarget.Webform, "123", "123"},
                new object[] { FormatTarget.Webform, "1234", "1234"},
                new object[] { FormatTarget.PrintoutImportantFields, "0", "0"},
                new object[] { FormatTarget.PrintoutNormalFields, "0", ""},
                new object[] { FormatTarget.GinnieNet, "howdy", ""},
                new object[] { FormatTarget.PointNative, "doody", "doody"},
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                string input = (string)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToCountVarCharStringRep(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToCountVarCharStringRep(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToCountStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, 0, "0"},
                new object[] { FormatTarget.Webform, 123, "123"},
                new object[] { FormatTarget.Webform, 1234, "1234"},
                new object[] { FormatTarget.PrintoutImportantFields, 0, "0"},
                new object[] { FormatTarget.PrintoutNormalFields, 0, ""},
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                int input = (int)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToCountString(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToCountString(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToBigCountStringTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, 0L, "0"},
                new object[] { FormatTarget.Webform, 10L, "10"},

            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                long input = (long)testCase[1];
                string expectedOutput = (string)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToBigCountString(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                string newOutput = newConverter.ToBigCountString(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }


        [Test]
        public void ToMoneyTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases =
            {
                new object[] { FormatTarget.Webform, null, 0.0M},
                new object[] { FormatTarget.Webform, "", 0.0M},
                new object[] { FormatTarget.Webform, "Blah", 0.0M},
                new object[] { FormatTarget.Webform, "$100.12", 100.12M},
                new object[] { FormatTarget.Webform, "$1,000.12", 1000.12M},
                new object[] { FormatTarget.Webform, "1000.12", 1000.12M},
                new object[] { FormatTarget.Webform, "($1,000.12)", -1000.12M},
                new object[] { FormatTarget.Webform, "(1000.12)", -1000.12M},
                new object[] { FormatTarget.Webform, "$0.00", 0M},
                new object[] { FormatTarget.Webform, "($0.00)", 0M},
            };

            foreach (object[] testCase in testCases)
            {
                FormatTarget target = (FormatTarget)testCase[0];

                string input = testCase[1] as string;
                decimal expectedOutput = (decimal)testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                decimal output = testConveter.ToMoney(input);

                Assert.AreEqual(expectedOutput, output, desc);

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(target);
                decimal newOutput = newConverter.ToMoney(input);

                Assert.AreEqual(expectedOutput, newOutput, desc);
            }
        }

        [Test]
        public void ToRateTest()
        {
            var testCases = new[] {
                new { Target = FormatTarget.Webform, Input=(string) null, ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="BLAH", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="100.12", ExpectedValue = 100.12M}
                , new { Target = FormatTarget.Webform, Input="100.12%", ExpectedValue = 100.12M}
                , new { Target = FormatTarget.Webform, Input="10.345%", ExpectedValue = 10.345M}
                , new { Target = FormatTarget.Webform, Input="100.0% ", ExpectedValue = 100M}
                , new { Target = FormatTarget.Webform, Input="  ", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="  100.123% ", ExpectedValue = 100.123M}
                , new { Target = FormatTarget.Webform, Input="  $100.246", ExpectedValue = 100.246M}
                , new { Target = FormatTarget.Webform, Input="0.000%", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="-1.000", ExpectedValue = -1M}
                , new { Target = FormatTarget.Webform, Input="-1.000%", ExpectedValue = -1M}
                , new { Target = FormatTarget.Webform, Input="(1.000%)", ExpectedValue = 0.0M}
            };


            foreach (var o in testCases)
            {
                LosConvert testConveter = new LosConvert(o.Target);
                Assert.AreEqual(o.ExpectedValue, testConveter.ToRate(o.Input), o.ToString());

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(o.Target);
                decimal newOutput = newConverter.ToRate(o.Input);

                Assert.AreEqual(o.ExpectedValue, newOutput, o.ToString());
            }
        }


        [Test]
        public void ToCountTest()
        {
            var testCases = new[] {
                new { Target = FormatTarget.Webform, Input=(string) null, ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="", ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="BLAH", ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="100.12", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="100.99", ExpectedValue = 101}
                , new { Target = FormatTarget.Webform, Input="100.50", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="100.51", ExpectedValue = 101}
                , new { Target = FormatTarget.Webform, Input="100.49", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="10.345%", ExpectedValue = 10}
                , new { Target = FormatTarget.Webform, Input="100.0% ", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="  ", ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="  100.123% ", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="  $100.246", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="  $99", ExpectedValue = 99}
                , new { Target = FormatTarget.Webform, Input="$100", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="-12", ExpectedValue = -12}
                , new { Target = FormatTarget.Webform, Input="-1,001", ExpectedValue = -1001}
                , new { Target = FormatTarget.Webform, Input="-1,001-12", ExpectedValue = 0}

            };

            foreach (var o in testCases)
            {
                LosConvert testConveter = new LosConvert(o.Target);

                Assert.AreEqual(o.ExpectedValue, testConveter.ToCount(o.Input), o.ToString());

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(o.Target);
                long newOutput = newConverter.ToCount(o.Input);

                Assert.AreEqual(o.ExpectedValue, newOutput, o.ToString());
            }
        }

        [Test]
        public void ToBigCountTest()
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            var testCases = new[] {
                new { Target = FormatTarget.Webform, Input=(string) null, ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="", ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="BLAH", ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="100.12", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="100.99", ExpectedValue = 101L}
                , new { Target = FormatTarget.Webform, Input="100.50", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="100.51", ExpectedValue = 101L}
                , new { Target = FormatTarget.Webform, Input="100.49", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="10.345%", ExpectedValue = 10L}
                , new { Target = FormatTarget.Webform, Input="100.0% ", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="  ", ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="  100.123% ", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="  $1,000.246", ExpectedValue = 1000L}
                , new { Target = FormatTarget.Webform, Input="  $99", ExpectedValue = 99L}
                , new { Target = FormatTarget.Webform, Input="$100", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="-12", ExpectedValue = -12L}
                , new { Target = FormatTarget.Webform, Input="-1,001", ExpectedValue = -1001L}
                , new { Target = FormatTarget.Webform, Input="-1,001-12", ExpectedValue = 0L}


            };

            foreach (var o in testCases)
            {
                LosConvert testConveter = new LosConvert(o.Target);

                Assert.AreEqual(o.ExpectedValue, testConveter.ToBigCount(o.Input), o.ToString());

                // Test refactored LosConvert
                var newConverter = LosConvert.Create(o.Target);
                long newOutput = newConverter.ToBigCount(o.Input);

                Assert.AreEqual(o.ExpectedValue, newOutput, o.ToString());
            }
        }
    }
}
