﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class AssetMigratorTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = new Guid("11111111-1111-1111-1111-111111111111").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId = new Guid("22222222-2222-2222-2222-222222222222").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();

        #region Fake Data Points
        private readonly string Desc = "Base Description";
        private readonly bool IsEmptyCreated = true;
        private readonly string PhoneNumber = "3333333333";
        private readonly decimal Val = 1;
        private readonly string AccNm = "Account Name";
        private readonly Sensitive<string> AccNum = "So Secret";
        private readonly E_AssetRegularT AssetRegularT = E_AssetRegularT.Checking;
        private readonly string Attention = "Dawg";
        private readonly string City = "Newport Beach";
        private readonly string ComNm = "Just Food For";
        private readonly string DepartmentName = "Dogs";
        private readonly E_GiftFundSourceT GiftSource = E_GiftFundSourceT.Nonprofit;
        private readonly bool IsSeeAttachment = true;
        private readonly string OtherTypeDesc = "Other gifts and such";
        private readonly CDateTime PrepD = CDateTime.Create(new DateTime(2018, 1, 1));
        private readonly string StAddr = "Out in the Street";
        private readonly string State = "NJ";
        private readonly CDateTime VerifExpD = CDateTime.Create(new DateTime(2018, 1, 2));
        private readonly bool VerifHasSignature = true;
        private readonly CDateTime VerifRecvD = CDateTime.Create(new DateTime(2018, 1, 3));
        private readonly CDateTime VerifReorderedD = CDateTime.Create(new DateTime(2018, 1, 4));
        private readonly CDateTime VerifSentD = null;
        private readonly string VerifSignatureImgId = new Guid("44444444-4444-4444-4444-444444444444").ToString();
        private readonly Guid VerifSigningEmployeeId = new Guid("55555555-5555-5555-5555-555555555555");
        private readonly string Zip = "90210";
        #endregion

        [Test]
        public void Migrate_BorrowerOwnedRecord_CreatesOwnershipAssociationForBorrower()
        {
            var appFake = this.GetAppFake(E_AssetOwnerT.Borrower);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new AssetMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerAssets.Count);
            var association = container.ConsumerAssets.Values.Single();
            Assert.AreEqual(container.Assets.Keys.Single(), association.AssetId);
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecord_CreatesOwnershipAssociationForCoborrower()
        {
            var appFake = this.GetAppFake(E_AssetOwnerT.CoBorrower, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new AssetMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerAssets.Count);
            var association = container.ConsumerAssets.Values.Single();
            Assert.AreEqual(container.Assets.Keys.Single(), association.AssetId);
            Assert.AreEqual(CoborrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_AssetOwnerT.CoBorrower, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new AssetMigrator(appFake, container);

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        [Test]
        public void Migrate_JointlyOwnedRecord_CreatesOwnershipAssociationForBothConsumers()
        {
            var appFake = this.GetAppFake(E_AssetOwnerT.Joint, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new AssetMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(2, container.ConsumerAssets.Count);
            Assert.AreEqual(1, container.ConsumerAssets.Values.Count(a => a.ConsumerId == BorrowerId));
            Assert.AreEqual(1, container.ConsumerAssets.Values.Count(a => a.ConsumerId == CoborrowerId));
            var borrowerAssociation = container.ConsumerAssets.Values.Single(a => a.ConsumerId == BorrowerId);
            Assert.AreEqual(container.Assets.Keys.Single(), borrowerAssociation.AssetId);
            Assert.AreEqual(true, borrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, borrowerAssociation.AppId);

            var coborrowerAssociation = container.ConsumerAssets.Values.Single(a => a.ConsumerId == CoborrowerId);
            Assert.AreEqual(container.Assets.Keys.Single(), coborrowerAssociation.AssetId);
            Assert.AreEqual(false, coborrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, coborrowerAssociation.AppId);
        }

        [Test]
        public void Migrate_JointlyOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_AssetOwnerT.Joint, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new AssetMigrator(appFake, container);

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        private IUladDataLayerMigrationAppDataProvider GetAppFake(E_AssetOwnerT fakeOwnerType, bool hasCoborrower = false)
        {
            var appFake = Substitute.For<IUladDataLayerMigrationAppDataProvider>();
            appFake.aAppId.Returns(AppId.Value);
            appFake.aBConsumerId.Returns(BorrowerId.Value);
            appFake.aCConsumerId.Returns(CoborrowerId.Value);
            appFake.aHasCoborrower.Returns(hasCoborrower);

            var recordFake = Substitute.For<ICollectionItemBase2, IAsset>();
            ((IAsset)recordFake).OwnerT.Returns(fakeOwnerType);
            var collectionFake = Substitute.For<IRecordCollection, IAssetCollection>();
            collectionFake.CountRegular.Returns(1);
            collectionFake.GetRegularRecordAt(Arg.Any<int>()).Returns(recordFake);
            appFake.aAssetCollection.Returns(collectionFake);

            return appFake;
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(IUladDataLayerMigrationAppDataProvider app)
        {
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var appConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { app });
            loanData.LegacyApplicationConsumers.Returns(appConsumers);
            ILoanLqbCollectionContainer container = new LoanLqbCollectionContainer(
                new LoanLqbEmptyCollectionProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.Assets), nameof(ILoanLqbCollectionContainer.ConsumerAssets) });
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());
            return container;
        }

        [Test]
        public void CreateEntity_FilledOutRegularLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeRegularAsset();
            var testMigrator = new TestAssetMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertRegularDataCopiedOver(entity);
            this.AssertSpecialDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutBusinessAsset_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeBusinessAsset();
            var testMigrator = new TestAssetMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertBusinessDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutCashDepositAsset_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeCashDepositAsset();
            var testMigrator = new TestAssetMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertCashDepositDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutLifeInsuranceAsset_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeLifeInsuranceAsset();
            var testMigrator = new TestAssetMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertLifeInsuranceDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutRetirementAsset_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeRetirementAsset();
            var testMigrator = new TestAssetMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertRetirementDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_InvalidVerifSignatureImgIdInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IAssetRegular>();
            fakeLegacyRecord.VerifHasSignature.Returns(true);
            fakeLegacyRecord.VerifSignatureImgId.Returns("Meow");
            var testMigrator = new TestAssetMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        private IAsset GetFakeRegularAsset()
        {
            var legacy = Substitute.For<IAssetRegular>();
            this.FakeBaseAssetFields(legacy);
            legacy.AccNm = AccNm;
            legacy.AccNum = AccNum;
            ((IAsset)legacy).AssetT.Returns((E_AssetT)AssetRegularT);
            legacy.AssetT = AssetRegularT;
            legacy.Attention = Attention;
            legacy.City = City;
            legacy.ComNm = ComNm;
            legacy.DepartmentName = DepartmentName;
            legacy.GiftSource = GiftSource;
            legacy.IsSeeAttachment = IsSeeAttachment;
            legacy.OtherTypeDesc = OtherTypeDesc;
            legacy.PrepD = PrepD;
            legacy.StAddr = StAddr;
            legacy.State = State;
            legacy.VerifExpD = VerifExpD;
            legacy.VerifHasSignature.Returns(VerifHasSignature);
            legacy.VerifRecvD = VerifRecvD;
            legacy.VerifReorderedD = VerifReorderedD;
            legacy.VerifSentD = VerifSentD;
            legacy.VerifSignatureImgId.Returns(VerifSignatureImgId);
            legacy.VerifSigningEmployeeId.Returns(VerifSigningEmployeeId);
            legacy.Zip = Zip;
            return legacy;
        }

        private IAssetBusiness GetFakeBusinessAsset()
        {
            var legacy = Substitute.For<IAssetBusiness>();
            this.FakeBaseAssetFields(legacy);
            ((IAsset)legacy).AssetT.Returns((E_AssetT)E_AssetSpecialT.Business);
            legacy.AssetT = E_AssetSpecialT.Business;
            return legacy;
        }

        private IAssetCashDeposit GetFakeCashDepositAsset()
        {
            var legacy = Substitute.For<IAssetCashDeposit>();
            this.FakeBaseAssetFields(legacy);
            ((IAsset)legacy).AssetT.Returns((E_AssetT)E_AssetSpecialT.CashDeposit);
            legacy.AssetT = E_AssetSpecialT.CashDeposit;
            legacy.AssetCashDepositT = E_AssetCashDepositT.CashDeposit2;
            return legacy;
        }

        private IAssetLifeInsurance GetFakeLifeInsuranceAsset()
        {
            var legacy = Substitute.For<IAssetLifeInsurance>();
            this.FakeBaseAssetFields(legacy);
            ((IAsset)legacy).AssetT.Returns((E_AssetT)E_AssetSpecialT.LifeInsurance);
            legacy.AssetT = E_AssetSpecialT.LifeInsurance;
            legacy.FaceVal = 1;
            return legacy;
        }

        private IAssetRetirement GetFakeRetirementAsset()
        {
            var legacy = Substitute.For<IAssetRetirement>();
            this.FakeBaseAssetFields(legacy);
            ((IAsset)legacy).AssetT.Returns((E_AssetT)E_AssetSpecialT.Retirement);
            legacy.AssetT = E_AssetSpecialT.Retirement;
            return legacy;
        }

        private void FakeBaseAssetFields(IAsset asset)
        {
            asset.Desc = Desc;
            asset.IsEmptyCreated = IsEmptyCreated;
            asset.PhoneNumber = PhoneNumber;
            asset.Val = Val;
        }

        private void AssertRegularDataCopiedOver(global::LendingQB.Core.Data.Asset entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual(AccNm, entity.AccountName.ToString());
            Assert.AreEqual(AccNum.Value, entity.AccountNum.ToString());
            Assert.AreEqual((E_AssetT)AssetRegularT, entity.AssetType);
            Assert.AreEqual(Attention, entity.Attention.ToString());
            Assert.AreEqual(City, entity.City.ToString());
            Assert.AreEqual(ComNm, entity.CompanyName.ToString());
            Assert.AreEqual(DepartmentName, entity.DepartmentName.ToString());
            Assert.AreEqual(GiftSource, entity.GiftSourceData);
            Assert.AreEqual(IsSeeAttachment, entity.IsSeeAttachment);
            Assert.AreEqual(OtherTypeDesc, entity.OtherTypeDesc.ToString());
            Assert.AreEqual(PrepD.DateTimeForComputation, entity.PrepDate.Value.Date);
            Assert.AreEqual(State, entity.State.ToString());
            Assert.AreEqual(StAddr, entity.StreetAddress.ToString());
            Assert.AreEqual(VerifExpD.DateTimeForComputation, entity.VerifExpiresDate.Value.Date);
            Assert.AreEqual(VerifRecvD.DateTimeForComputation, entity.VerifRecvDate.Value.Date);
            Assert.AreEqual(VerifReorderedD.DateTimeForComputation, entity.VerifReorderedDate.Value.Date);
            Assert.AreEqual(VerifSentD, entity.VerifSentDate);
            Assert.AreEqual(VerifSignatureImgId, entity.VerifSignatureImgId.ToString());
            Assert.AreEqual(VerifSigningEmployeeId, entity.VerifSigningEmployeeId.Value.Value);
            Assert.AreEqual(Zip, entity.Zip.ToString());
        }

        private void AssertSpecialDataNull(global::LendingQB.Core.Data.Asset entity)
        {
            Assert.AreEqual(null, entity.AssetCashDepositType);
            Assert.AreEqual(null, entity.FaceValue);
        }

        private void AssertBusinessDataCopiedOver(global::LendingQB.Core.Data.Asset entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual((E_AssetT)E_AssetSpecialT.Business, entity.AssetType);
        }

        private void AssertCashDepositDataCopiedOver(global::LendingQB.Core.Data.Asset entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual((E_AssetT)E_AssetSpecialT.CashDeposit, entity.AssetType);
            Assert.AreEqual(E_AssetCashDepositT.CashDeposit2, entity.AssetCashDepositType);
        }

        private void AssertLifeInsuranceDataCopiedOver(global::LendingQB.Core.Data.Asset entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual((E_AssetT)E_AssetSpecialT.LifeInsurance, entity.AssetType);
            Assert.AreEqual(1, (decimal)entity.FaceValue);
        }

        private void AssertRetirementDataCopiedOver(global::LendingQB.Core.Data.Asset entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual((E_AssetT)E_AssetSpecialT.Retirement, entity.AssetType);
        }

        private void AssertRegularDataNull(global::LendingQB.Core.Data.Asset entity)
        {
            Assert.AreEqual(null, entity.AccountName);
            Assert.AreEqual(null, entity.AccountNum);
            Assert.AreEqual(null, entity.Attention);
            Assert.AreEqual(null, entity.City);
            Assert.AreEqual(null, entity.CompanyName);
            Assert.AreEqual(null, entity.DepartmentName);
            Assert.AreEqual(null, entity.GiftSourceData);
            Assert.AreEqual(null, entity.IsSeeAttachment);
            Assert.AreEqual(null, entity.OtherTypeDesc);
            Assert.AreEqual(null, entity.PrepDate);
            Assert.AreEqual(null, entity.State);
            Assert.AreEqual(null, entity.StreetAddress);
            Assert.AreEqual(null, entity.VerifExpiresDate);
            Assert.AreEqual(null, entity.VerifRecvDate);
            Assert.AreEqual(null, entity.VerifReorderedDate);
            Assert.AreEqual(null, entity.VerifSentDate);
            Assert.AreEqual(null, entity.VerifSignatureImgId);
            Assert.AreEqual(null, entity.VerifSigningEmployeeId);
            Assert.AreEqual(null, entity.Zip);
        }

        private void AssertBaseDataCopiedOver(global::LendingQB.Core.Data.Asset entity)
        {
            Assert.AreEqual(Desc, entity.Desc.ToString());
            Assert.AreEqual(IsEmptyCreated, entity.IsEmptyCreated);
            Assert.AreEqual(PhoneNumber, entity.Phone.ToString());
            Assert.AreEqual(Val, (decimal)entity.Value);
        }
    }

    internal class TestAssetMigrator : AssetMigrator
    {
        public TestAssetMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer container)
            : base(app, container)
        {
        }

        public new global::LendingQB.Core.Data.Asset CreateEntity(IAsset legacy)
        {
            return base.CreateEntity(legacy);
        }
    }
}
