﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Commands;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Test.Developers.Utils;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LoanFileCreationDuplicateLqbCollectionTest
    {
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid sourceLoanId;
        private Guid destinationLoanId;
        private AbstractUserPrincipal principal;

        [SetUp]
        public void Setup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

                var loanFileCreator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.sourceLoanId = loanFileCreator.CreateBlankUladLoanFile();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.sourceLoanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.destinationLoanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [Test]
        public void DuplicateLoanFile_SingleUladAppSingleAsset_CreatesExpectedEntitiesAndAssociations()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var source = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.sourceLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                source.InitSave();
                var consumerId = source.Consumers.Keys.First();
                source.AddAsset(consumerId, new Asset());
                source.Save();
                Assert.AreEqual(1, source.UladApplications.Count);
                Assert.AreEqual(1, source.UladApplicationConsumers.Count);
                Assert.AreEqual(1, source.Assets.Count);
                Assert.AreEqual(1, source.ConsumerAssets.Count);

                // Gather identifiers for later comparison
                var sourceConsumerId = source.Consumers.Keys.First();
                var sourceLegacyAppId = source.LegacyApplications.Keys.First();
                var sourceUladAppId = source.UladApplications.Keys.First();
                var sourceAssetId = source.Assets.Keys.First();

                var loanCreator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                this.destinationLoanId = loanCreator.DuplicateLoanFile(this.sourceLoanId);

                var destination = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.destinationLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                destination.InitLoad();
                Assert.AreEqual(1, destination.UladApplications.Count);
                Assert.AreEqual(1, destination.UladApplicationConsumers.Count);
                Assert.AreEqual(1, destination.Assets.Count);
                Assert.AreEqual(1, destination.ConsumerAssets.Count);

                // Make sure the duplicated identifiers differ
                var destinationConsumerId = destination.Consumers.Keys.First();
                var destinationLegacyAppId = destination.LegacyApplications.Keys.First();
                var destinationUladAppId = destination.UladApplications.Keys.First();
                var destinationAssetId = destination.Assets.Keys.First();

                Assert.AreNotEqual(destinationConsumerId, sourceConsumerId);
                Assert.AreNotEqual(destinationLegacyAppId, sourceLegacyAppId);
                Assert.AreNotEqual(destinationUladAppId, sourceUladAppId);
                Assert.AreNotEqual(destinationAssetId, sourceAssetId);

                // Make sure the associations reference the correct identifiers
                var destinationOwnershipAssoc = destination.ConsumerAssets.Values.First();
                Assert.AreEqual(destinationAssetId, destinationOwnershipAssoc.AssetId);
                Assert.AreEqual(destinationConsumerId, destinationOwnershipAssoc.ConsumerId);
                Assert.AreEqual(destinationLegacyAppId, destinationOwnershipAssoc.AppId);

                var destinationUladAssoc = destination.UladApplicationConsumers.Values.First();
                Assert.AreEqual(destinationConsumerId, destinationUladAssoc.ConsumerId);
                Assert.AreEqual(destinationLegacyAppId, destinationUladAssoc.LegacyAppId);
                Assert.AreEqual(destinationUladAppId, destinationUladAssoc.UladApplicationId);
            }
        }

        [Test]
        public void DuplicateLoanFile_SingleUladAppTwoAssets_CreatesExpectedEntitiesAndAssociations()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var source = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.sourceLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                source.InitSave();
                var consumerId = source.Consumers.Keys.First();
                source.AddAsset(consumerId, new Asset());
                source.AddAsset(consumerId, new Asset());
                source.Save();
                Assert.AreEqual(1, source.UladApplications.Count);
                Assert.AreEqual(1, source.UladApplicationConsumers.Count);
                Assert.AreEqual(2, source.Assets.Count);
                Assert.AreEqual(2, source.ConsumerAssets.Count);

                // Gather identifiers for later comparison
                var sourceConsumerId = source.Consumers.Keys.First();
                var sourceLegacyAppId = source.LegacyApplications.Keys.First();
                var sourceUladAppId = source.UladApplications.Keys.First();
                var sourceAssetId1 = source.Assets.Keys.ElementAt(0);
                var sourceAssetId2 = source.Assets.Keys.ElementAt(1);

                var loanCreator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                this.destinationLoanId = loanCreator.DuplicateLoanFile(this.sourceLoanId);

                var destination = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.destinationLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                destination.InitLoad();
                Assert.AreEqual(1, destination.UladApplications.Count);
                Assert.AreEqual(1, destination.UladApplicationConsumers.Count);
                Assert.AreEqual(2, destination.Assets.Count);
                Assert.AreEqual(2, destination.ConsumerAssets.Count);

                // Make sure the duplicated identifiers differ
                var destinationConsumerId = destination.Consumers.Keys.First();
                var destinationLegacyAppId = destination.LegacyApplications.Keys.First();
                var destinationUladAppId = destination.UladApplications.Keys.First();
                var destinationAssetId1 = destination.Assets.Keys.ElementAt(0);
                var destinationAssetId2 = destination.Assets.Keys.ElementAt(1);

                Assert.AreNotEqual(destinationConsumerId, sourceConsumerId);
                Assert.AreNotEqual(destinationLegacyAppId, sourceLegacyAppId);
                Assert.AreNotEqual(destinationUladAppId, sourceUladAppId);
                Assert.AreNotEqual(destinationAssetId1, sourceAssetId1);
                Assert.AreNotEqual(destinationAssetId2, sourceAssetId2);

                // Make sure the associations reference the correct identifiers
                var destinationOwnershipAssoc1 = destination.ConsumerAssets.Values.Single(a => a.AssetId == destinationAssetId1);
                var destinationOwnershipAssoc2 = destination.ConsumerAssets.Values.Single(a => a.AssetId == destinationAssetId2);
                Assert.AreEqual(destinationConsumerId, destinationOwnershipAssoc1.ConsumerId);
                Assert.AreEqual(destinationConsumerId, destinationOwnershipAssoc2.ConsumerId);
                Assert.AreEqual(destinationLegacyAppId, destinationOwnershipAssoc1.AppId);
                Assert.AreEqual(destinationLegacyAppId, destinationOwnershipAssoc2.AppId);

                var destinationUladAssoc = destination.UladApplicationConsumers.Values.First();
                Assert.AreEqual(destinationConsumerId, destinationUladAssoc.ConsumerId);
                Assert.AreEqual(destinationLegacyAppId, destinationUladAssoc.LegacyAppId);
                Assert.AreEqual(destinationUladAppId, destinationUladAssoc.UladApplicationId);
            }
        }

        [Test]
        public void DuplicateLoanFile_TwoConsumersShareSingleAsset_CreatesExpectedEntitiesAndAssociations()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var source = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.sourceLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                source.InitSave();
                var appData = source.GetAppData(0);
                source.AddCoborrowerToLegacyApplication(appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());

                var borrowerId = source.Consumers.Keys.ElementAt(0);
                var coborrowerId = source.Consumers.Keys.ElementAt(1);
                var assetId1 = source.AddAsset(borrowerId, new Asset());
                source.AddAssetOwnership(coborrowerId, assetId1);
                source.Save();

                source = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.sourceLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                source.InitLoad();
                Assert.AreEqual(1, source.UladApplications.Count);
                Assert.AreEqual(2, source.UladApplicationConsumers.Count);
                Assert.AreEqual(1, source.Assets.Count);
                Assert.AreEqual(2, source.ConsumerAssets.Count);

                // Gather identifiers for later comparison
                var sourceBorrowerId = source.Consumers.Keys.ElementAt(0);
                var sourceCoborrowerId = source.Consumers.Keys.ElementAt(1);
                var sourceLegacyAppId = source.LegacyApplications.Keys.First();
                var sourceUladAppId = source.UladApplications.Keys.First();
                var sourceAssetId = source.Assets.Keys.First();

                var loanCreator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                this.destinationLoanId = loanCreator.DuplicateLoanFile(this.sourceLoanId);

                var destination = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.destinationLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                destination.InitLoad();
                Assert.AreEqual(1, destination.UladApplications.Count);
                Assert.AreEqual(2, destination.UladApplicationConsumers.Count);
                Assert.AreEqual(1, destination.Assets.Count);
                Assert.AreEqual(2, destination.ConsumerAssets.Count);

                // Make sure the duplicated identifiers differ
                var destinationBorrowerId = destination.Consumers.Keys.ElementAt(0);
                var destinationCoborrowerId = destination.Consumers.Keys.ElementAt(1);
                var destinationLegacyAppId = destination.LegacyApplications.Keys.First();
                var destinationUladAppId = destination.UladApplications.Keys.First();
                var destinationAssetId = destination.Assets.Keys.First();

                Assert.AreNotEqual(destinationBorrowerId, sourceBorrowerId);
                Assert.AreNotEqual(destinationCoborrowerId, sourceCoborrowerId);
                Assert.AreNotEqual(destinationLegacyAppId, sourceLegacyAppId);
                Assert.AreNotEqual(destinationUladAppId, sourceUladAppId);
                Assert.AreNotEqual(destinationAssetId, sourceAssetId);

                // Make sure the associations reference the correct identifiers
                var destinationOwnershipAssoc1 = destination.ConsumerAssets.Values.Single(a => a.ConsumerId == destinationBorrowerId);
                var destinationOwnershipAssoc2 = destination.ConsumerAssets.Values.Single(a => a.ConsumerId == destinationCoborrowerId);
                Assert.AreEqual(destinationAssetId, destinationOwnershipAssoc1.AssetId);
                Assert.AreEqual(destinationAssetId, destinationOwnershipAssoc2.AssetId);
                Assert.AreEqual(destinationLegacyAppId, destinationOwnershipAssoc1.AppId);
                Assert.AreEqual(destinationLegacyAppId, destinationOwnershipAssoc2.AppId);

                var destinationUladAssoc1 = destination.UladApplicationConsumers.Values.Single(a => a.ConsumerId == destinationBorrowerId);
                var destinationUladAssoc2 = destination.UladApplicationConsumers.Values.Single(a => a.ConsumerId == destinationCoborrowerId);
                Assert.AreEqual(destinationLegacyAppId, destinationUladAssoc1.LegacyAppId);
                Assert.AreEqual(destinationLegacyAppId, destinationUladAssoc2.LegacyAppId);
                Assert.AreEqual(destinationUladAppId, destinationUladAssoc1.UladApplicationId);
                Assert.AreEqual(destinationUladAppId, destinationUladAssoc2.UladApplicationId);
            }
        }

        [Test]
        public void DuplicateLoanFile_SingleUladAppThreeConsumersCustomOrder_CopiesOrder()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var source = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.sourceLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                source.InitLoad();
                var uladAppId = source.UladApplications.Keys.Single();
                var consumer1Id = source.Consumers.Keys.Single();
                var consumer2Id = CPageData.AddConsumerToUladApplication(source.sLId.ToIdentifier<DataObjectKind.Loan>(), uladAppId, true);
                var consumer3Id = CPageData.AddConsumerToUladApplication(source.sLId.ToIdentifier<DataObjectKind.Loan>(), uladAppId, true);
                source.InitSave();
                Assert.AreEqual(1, source.UladApplications.Count);
                Assert.AreEqual(3, source.UladApplicationConsumers.Count);
                source.Consumers[consumer1Id].FirstName = PersonName.Create("AA").ForceValue();
                source.Consumers[consumer2Id].FirstName = PersonName.Create("BB").ForceValue();
                source.Consumers[consumer3Id].FirstName = PersonName.Create("CC").ForceValue();
                source.Save();
                source.InitSave();
                CollectionAssert.AreEqual(
                    new[] { PersonName.Create("AA"), PersonName.Create("BB"), PersonName.Create("CC") },
                    source.Consumers.Values.Select(c => c.FirstName));
                var command = new SetOrder(DataPath.Create("loan"), "Consumers", uladAppId.Value, new[] { consumer1Id.Value, consumer3Id.Value, consumer2Id.Value });
                source.HandleSetOrder(command);
                source.Save();

                var loanCreator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                this.destinationLoanId = loanCreator.DuplicateLoanFile(this.sourceLoanId);

                var destination = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.destinationLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                destination.InitLoad();
                CollectionAssert.AreEqual(
                    new[] { PersonName.Create("AA"), PersonName.Create("CC"), PersonName.Create("BB") },
                    destination.Consumers.Values.Select(c => c.FirstName));
            }
        }

        [Test]
        public void DuplicateLoanFile_MultipleEntities_OrderRowsMatch()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var source = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.sourceLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                source.InitSave();
                var pageData = CPageBaseExtractor.Extract(source);
                var appData = source.GetAppData(0);
                source.AddCoborrowerToLegacyApplication(appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());

                var legacyAppId = source.LegacyApplications.Keys.First();
                var borrowerId = source.Consumers.Keys.ElementAt(0);
                var coborrowerId = source.Consumers.Keys.ElementAt(1);
                var uladAppId = source.UladApplications.Keys.First();

                var assetId1 = source.AddAsset(borrowerId, new Asset());
                source.AddAssetOwnership(coborrowerId, assetId1);

                var assetId2 = source.AddAsset(borrowerId, new Asset());

                var empId1 = source.AddEmploymentRecord(borrowerId, new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>()) { IsPrimary = true });
                var empId2 = source.AddEmploymentRecord(borrowerId, new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>()));
                var empId3 = source.AddEmploymentRecord(coborrowerId, new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>()) { IsPrimary = true });

                var incomeId1 = source.AddIncomeSource(borrowerId, new IncomeSource());
                source.AddIncomeSourceOwnership(coborrowerId, incomeId1);

                var defaultsProvider = source.CreateLiabilityDefaultsProvider();
                var liabilityId1 = source.AddLiability(borrowerId, new Liability(defaultsProvider));
                source.AddLiabilityOwnership(coborrowerId, liabilityId1);

                var pubrecId1 = source.AddPublicRecord(borrowerId, new PublicRecord());
                source.AddPublicRecordOwnership(coborrowerId, pubrecId1);

                var propertyId1 = source.AddRealProperty(borrowerId, new RealProperty());
                source.AddRealPropertyOwnership(coborrowerId, propertyId1);

                var vaprevId1 = source.AddVaPreviousLoan(borrowerId, new VaPreviousLoan());
                source.AddVaPreviousLoanOwnership(coborrowerId, vaprevId1);

                var vorId1 = source.AddVorRecord(borrowerId, new VorRecord());
                source.AddVorRecordOwnership(coborrowerId, vorId1);

                // change the order for different order levels to ensure duplication process preserves all orders
                var pageBase = CPageBaseExtractor.Extract(source);
                var aggregate = pageBase.loanLqbCollectionContainer as LoanLqbCollectionContainer;

                var assetOrder = aggregate.GetOrderForApplication<DataObjectKind.Asset>(appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                assetOrder.MoveBack(assetId1);

                var empOrder = aggregate.GetOrderForConsumer<DataObjectKind.EmploymentRecord>(borrowerId);
                empOrder.MoveBack(empId1);

                var consumerOrder = aggregate.GetConsumerOrderingForUladApplication(uladAppId);
                consumerOrder.MoveBack(borrowerId);

                source.Save();

                var loanCreator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                this.destinationLoanId = loanCreator.DuplicateLoanFile(this.sourceLoanId);
                var destination = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.destinationLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                destination.InitLoad();

                appData = destination.GetAppData(0);
                var legacyAppId_Destination = appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>();
                var borrowerId_Destination = appData.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coborrowerId_Destination = appData.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var uladAppId_Destination = destination.UladApplications.Keys.First();

                var assetId1_Destination = destination.ConsumerAssets.Values.First().AssetId;
                var assetId2_Destination = destination.Assets.Single(c => c.Key != assetId1_Destination).Key;
                var incomeId1_Destination = destination.ConsumerIncomeSources.Values.First().IncomeSourceId;
                var liabilityId1_Destination = destination.ConsumerLiabilities.Values.First().LiabilityId;
                var pubrecId1_Destination = destination.ConsumerPublicRecords.Values.First().PublicRecordId;
                var propertyId1_Destination = destination.ConsumerRealProperties.Values.First().RealPropertyId;
                var vaprevId1_Destination = destination.ConsumerVaPreviousLoans.Values.First().VaPreviousLoanId;
                var vorId1_Destination = destination.ConsumerVorRecords.Values.First().VorRecordId;

                var empId1_Destination = destination.EmploymentRecords.Keys.ElementAt(0);
                var empId2_Destination = destination.EmploymentRecords.Keys.ElementAt(1);
                var empId3_Destination = destination.EmploymentRecords.Keys.ElementAt(2);

                var idMap = new Dictionary<Guid, Guid>();
                idMap[legacyAppId.Value] = legacyAppId_Destination.Value;
                idMap[borrowerId.Value] = borrowerId_Destination.Value;
                idMap[coborrowerId.Value] = coborrowerId_Destination.Value;
                idMap[uladAppId.Value] = uladAppId_Destination.Value;
                idMap[assetId1.Value] = assetId1_Destination.Value;
                idMap[assetId2.Value] = assetId2_Destination.Value;
                idMap[empId1.Value] = empId1_Destination.Value;
                idMap[empId2.Value] = empId2_Destination.Value;
                idMap[empId3.Value] = empId3_Destination.Value;
                idMap[incomeId1.Value] = incomeId1_Destination.Value;
                idMap[liabilityId1.Value] = liabilityId1_Destination.Value;
                idMap[pubrecId1.Value] = pubrecId1_Destination.Value;
                idMap[propertyId1.Value] = propertyId1_Destination.Value;
                idMap[vaprevId1.Value] = vaprevId1_Destination.Value;
                idMap[vorId1.Value] = vorId1_Destination.Value;

                var sourceOrders = new SortOrderData(this.sourceLoanId);
                var destinationOrders = new SortOrderData(this.destinationLoanId);

                Assert.AreEqual(sourceOrders.Rows.Count, destinationOrders.Rows.Count);
                foreach (var sourceRow in sourceOrders.Rows)
                {
                    var destinationRow = this.GetMatchingSortOrderRow(sourceRow, destinationOrders, idMap);
                    Assert.IsNotNull(destinationRow);

                    string type;
                    if (sourceRow.AppId != null) type = "legacy-application";
                    else if (sourceRow.ConsumerId != null) type = "consumer";
                    else if (sourceRow.UladApplicationId != null) type = "ulad-application";
                    else type = "loan";

                    string message = string.Format("sourceId = {0}; destinationId = {1}; collection = {2}; level = {3}", sourceLoanId.ToString("D"), destinationLoanId.ToString("D"), sourceRow.CollectionName, type);
                    if (sourceRow.SortOrder.Length == destinationRow.SortOrder.Length)
                    {
                        for (int i = 0; i < sourceRow.SortOrder.Length; ++i)
                        {
                            Assert.AreEqual(destinationRow.SortOrder[i], idMap[sourceRow.SortOrder[i]], message);
                        }
                    }
                    else
                    {
                        Assert.Fail(message);
                    }
                }
            }
        }

        [Test]
        public void DuplicateloanFile_RecordForInvalidCoborrower_DuplicatesSuccessfully()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var source = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.sourceLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                source.InitSave();
                var sourceApp = source.GetAppData(0);
                sourceApp.aCEmpCollection.GetPrimaryEmp(forceCreate: true);
                sourceApp.aHasCoborrowerData = false;
                source.Save();
                Assert.IsFalse(sourceApp.aHasCoborrower);
                Assert.AreEqual(1, sourceApp.aCEmpCollection.CountSpecial);
                Assert.AreEqual(1, source.EmploymentRecords.Count);
                
                var loanCreator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                this.destinationLoanId = loanCreator.DuplicateLoanFile(this.sourceLoanId);
                var destination = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.destinationLoanId,
                    typeof(LoanFileCreationDuplicateLqbCollectionTest));
                destination.InitLoad();

                // Expect the coborrower record to have copied.
                Assert.AreEqual(1, destination.GetAppData(0).aCEmpCollection.CountSpecial);
            }
        }
        
        private SortOrderRow GetMatchingSortOrderRow(SortOrderRow reference, SortOrderData orders, Dictionary<Guid, Guid> idMap)
        {
            foreach (var row in orders.Rows)
            {
                if (this.HasMatchingKey(reference, row, idMap)) return row;
            }

            return null;
        }

        private bool HasMatchingKey(SortOrderRow reference, SortOrderRow candidate, Dictionary<Guid, Guid> idMap)
        {
            if (reference.CollectionName != candidate.CollectionName) return false;

            if (this.IsLegacyAppLevelOrder(reference) && this.IsLegacyAppLevelOrder(candidate))
            {
                return candidate.AppId.Value == idMap[reference.AppId.Value];
            }
            else if (this.IsConsumerLevelOrder(reference) && this.IsConsumerLevelOrder(candidate))
            {
                return candidate.ConsumerId.Value == idMap[reference.ConsumerId.Value];
            }
            else if (this.IsUladAppLevelOrder(reference) && this.IsUladAppLevelOrder(candidate))
            {
                return candidate.UladApplicationId.Value == idMap[reference.UladApplicationId.Value];
            }
            else if (this.IsLoanLevelOrder(reference) && this.IsLoanLevelOrder(candidate))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsLoanLevelOrder(SortOrderRow row)
        {
            return row.AppId == null && row.ConsumerId == null && row.UladApplicationId == null;
        }

        private bool IsLegacyAppLevelOrder(SortOrderRow row)
        {
            return row.AppId != null && row.ConsumerId == null && row.UladApplicationId == null;
        }

        private bool IsConsumerLevelOrder(SortOrderRow row)
        {
            return row.AppId == null && row.ConsumerId != null && row.UladApplicationId == null;
        }

        private bool IsUladAppLevelOrder(SortOrderRow row)
        {
            return row.AppId == null && row.ConsumerId == null && row.UladApplicationId != null;
        }

        private class SortOrderData
        {
            const string Storp = "LOAN_COLLECTION_SORT_ORDER_RetrieveByLoanId";

            public SortOrderData(Guid loanId)
            {
                this.Rows = new List<SortOrderRow>();

                var pram = new SqlParameter("@LoanId", loanId);
                var prams = new SqlParameter[] { pram };

                Guid brokerId;
                var connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, Storp, prams))
                {
                    while (reader.Read())
                    {
                        var row = new SortOrderRow(reader);
                        this.Rows.Add(row);
                    }
                }
            }

            public List<SortOrderRow> Rows { get; private set; }
        }

        private class SortOrderRow
        {
            public SortOrderRow(DbDataReader reader)
            {
                this.AppId = Convert.IsDBNull(reader["AppId"]) ? default(Guid?) : (Guid)reader["AppId"];
                this.ConsumerId = Convert.IsDBNull(reader["ConsumerId"]) ? default(Guid?) : (Guid)reader["ConsumerId"];
                this.UladApplicationId = Convert.IsDBNull(reader["UladApplicationId"]) ? default(Guid?) : (Guid)reader["UladApplicationId"];

                this.CollectionName = Convert.ToString(reader["CollectionName"]);

                var delim = new char[] { ',' };
                string order = Convert.ToString(reader["SortOrder"]);
                string[] ids = order.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                this.SortOrder = new Guid[ids.Length];
                for (int i = 0; i < ids.Length; ++i)
                {
                    this.SortOrder[i] = Guid.Parse(ids[i]);
                }
            }

            public string CollectionName { get; private set; }
            public Guid[] SortOrder { get; private set; }
            public Guid? AppId { get; private set; }
            public Guid? ConsumerId { get; private set; }
            public Guid? UladApplicationId { get; private set; }
        }
    }
}
