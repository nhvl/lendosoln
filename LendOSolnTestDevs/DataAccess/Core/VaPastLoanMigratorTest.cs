﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class VaPastLoanMigratorTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = new Guid("11111111-1111-1111-1111-111111111111").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();

        #region Fake Data Points
        private readonly string LoanTypeDesc = "Test Desc";
        private readonly string City = "Costa Mesa";
        private readonly string CityState = "Costa Mesa, CA";
        private readonly string DateOfLoan = "June Nineteen 84";
        private readonly E_TriState IsStillOwned = E_TriState.Yes;
        private readonly CDateTime LoanD = CDateTime.InvalidWrapValue;
        private readonly CDateTime PropSoldD = CDateTime.Create(new DateTime(2018, 10, 1));
        private readonly string StAddr = "Street";
        private readonly string State = "CA";
        private readonly string VaLoanNum = "VA #123";
        private readonly string Zip = "92626";
        #endregion

        [Test]
        public void Migrate_WhenCalled_CreatesOwnershipAssociationForBorrower()
        {
            // These records don't support ownership, always give to borrower.
            var recordFake = Substitute.For<ICollectionItemBase2, IVaPastLoan>();
            var collectionFake = Substitute.For<IRecordCollection, IVaPastLoanCollection>();
            collectionFake.CountRegular.Returns(1);
            collectionFake.GetRegularRecordAt(Arg.Any<int>()).Returns(recordFake);
            var appFake = Substitute.For<IUladDataLayerMigrationAppDataProvider>();
            appFake.aAppId.Returns(AppId.Value);
            appFake.aBConsumerId.Returns(BorrowerId.Value);
            appFake.aVaPastLCollection.Returns(collectionFake);
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var appConsumers = Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(AppId, BorrowerId, null);
            loanData.LegacyApplicationConsumers.Returns(appConsumers);
            var container = new LoanLqbCollectionContainer(
                new LoanLqbEmptyCollectionProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null) as ILoanLqbCollectionContainer;
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.VaPreviousLoans), nameof(ILoanLqbCollectionContainer.ConsumerVaPreviousLoans) });
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());
            var migrator = new VaPastLoanMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerVaPreviousLoans.Count);
            var association = container.ConsumerVaPreviousLoans.Values.Single();
            Assert.AreEqual(container.VaPreviousLoans.Keys.Single(), association.VaPreviousLoanId);
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void CreateEntity_FilledOutLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeVaPastLoan();
            var testMigrator = new TestVaPastLoanMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertDataCopiedOver(entity);
        }

        private IVaPastLoan GetFakeVaPastLoan()
        {
            var fakeEntity = Substitute.For<IVaPastLoan>();
            fakeEntity.City = City;
            fakeEntity.CityState = CityState;
            fakeEntity.DateOfLoan = DateOfLoan;
            fakeEntity.IsStillOwned = IsStillOwned;
            fakeEntity.LoanD = LoanD;
            fakeEntity.LoanTypeDesc = LoanTypeDesc;
            fakeEntity.PropSoldD = PropSoldD;
            fakeEntity.StAddr = StAddr;
            fakeEntity.State = State;
            fakeEntity.VaLoanNum = VaLoanNum;
            fakeEntity.Zip = Zip;
            return fakeEntity;
        }

        private void AssertDataCopiedOver(VaPreviousLoan entity)
        {
            Assert.AreEqual(City, entity.City.ToString());
            Assert.AreEqual(CityState, entity.CityState.ToString());
            Assert.AreEqual(DateOfLoan, entity.DateOfLoan.ToString());
            Assert.AreEqual(BooleanKleene.True, entity.IsStillOwned);
            Assert.AreEqual(LoanTypeDesc, entity.LoanTypeDesc.ToString());
            Assert.AreEqual(State, entity.State.ToString());
            Assert.AreEqual(Zip, entity.Zip.ToString());
            Assert.AreEqual(null, entity.LoanDate);
            Assert.AreEqual(PropSoldD.DateTimeForComputation, entity.PropertySoldDate.Value.Date);
            Assert.AreEqual(StAddr, entity.StreetAddress.ToString());
            Assert.AreEqual(VaLoanNum, entity.VaLoanNumber.ToString());
        }
    }

    internal class TestVaPastLoanMigrator : VaPastLoanMigrator
    {
        public TestVaPastLoanMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer container)
            : base(app, container)
        {
        }

        public new VaPreviousLoan CreateEntity(IVaPastLoan legacy)
        {
            return base.CreateEntity(legacy);
        }
    }
}
