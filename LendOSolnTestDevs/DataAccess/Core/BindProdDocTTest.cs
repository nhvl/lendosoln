﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Linq;
    using System.Web.UI.WebControls;
    using global::DataAccess;
    using NUnit.Framework;

    public class BindProdDocTTest
    {
        #region Test case sources
        private static object[] BindsProdDocT_Always_IncludesSelectedValue_Source = (
            from enumValue in Enum.GetValues(typeof(E_sProdDocT)).Cast<E_sProdDocT>().Where(e => e != E_sProdDocT.Streamline /*Not displayed in dropdown*/)
            from renameAlternativeDocType in new[] { true, false }
            from showLegacyMappings in new[] { true, false }
            from showEnhancedMappings in new[] { true, false }
            select new object[] { enumValue, renameAlternativeDocType, showLegacyMappings, showEnhancedMappings }).ToArray();

        private static object[] Bind_sProdDocT_ShowLegacyFalse_DoesNotShowLegacy_Source = new object[]
        {
            E_sProdDocT.Alt,
            E_sProdDocT.Light,
            E_sProdDocT.NINA,
            E_sProdDocT.NISA,
            E_sProdDocT.NINANE,
            E_sProdDocT.NIVA,
            E_sProdDocT.SISA,
            E_sProdDocT.SIVA,
            E_sProdDocT.VISA,
            E_sProdDocT.NIVANE,
            E_sProdDocT.VINA
        };

        private static object[] Bind_sProdDocT_ShowEnhancedFalse_DoesNotShowEnhanced_Source = new object[]
        {
            E_sProdDocT._12MoPersonalBankStatements,
            E_sProdDocT._24MoPersonalBankStatements,
            E_sProdDocT._12MoBusinessBankStatements,
            E_sProdDocT._24MoBusinessBankStatements,
            E_sProdDocT.OtherBankStatements,
            E_sProdDocT._1YrTaxReturns,
            E_sProdDocT.AssetUtilization,
            E_sProdDocT.DebtServiceCoverage,
            E_sProdDocT.NoIncome,
            E_sProdDocT.Voe
        };
        #endregion

        [Test]
        [TestCaseSource(nameof(BindsProdDocT_Always_IncludesSelectedValue_Source))]
        public void Bind_sProdDocT_Always_IncludesSelectedValue(E_sProdDocT selectedValue, bool renameAlternativeDocType, bool showLegacyMappings, bool showEnhancedMappings)
        {
            var itemCollection = new ListItemCollection();
            Tools.Bind_sProdDocT(itemCollection, renameAlternativeDocType, showLegacyMappings, showEnhancedMappings, selectedValue);

            var selectedValueItem = itemCollection.FindByValue(selectedValue.ToString("D"));
            Assert.IsNotNull(selectedValueItem);
        }

        [Test]
        public void Bind_sProdDocT_StreamlineCurrentValue_DoesNotIncludeInDropdown(
           [Values(true, false)] bool renameAlternativeDocType,
           [Values(true, false)] bool showLegacyMappings,
           [Values(true, false)] bool showEnhancedMappings)
        {
            var itemCollection = new ListItemCollection();
            Tools.Bind_sProdDocT(itemCollection, renameAlternativeDocType, showLegacyMappings, showEnhancedMappings, E_sProdDocT.Streamline);

            var selectedValueItem = itemCollection.FindByValue(E_sProdDocT.Streamline.ToString("D"));
            Assert.IsNull(selectedValueItem);
        }

        [Test]
        [TestCase(true, "Alt Doc")]
        [TestCase(false, "Alt - 12 months bank stmts")]
        public void Bind_sProdDocT_RenameAlternativeDocType_PerformsRename(bool renameAlternativeDocType, string expectedLabel)
        {
            var itemCollection = new ListItemCollection();
            Tools.Bind_sProdDocT(itemCollection, renameAlternativeDocType, showLegacy: true, showEnhanced: false, currentValue: E_sProdDocT.Full);

            var selectedValueItem = itemCollection.FindByValue(E_sProdDocT.Alt.ToString("D"));
            Assert.IsNotNull(selectedValueItem);
            Assert.AreEqual(expectedLabel, selectedValueItem.Text);
        }

        [Test]
        [TestCaseSource(nameof(Bind_sProdDocT_ShowLegacyFalse_DoesNotShowLegacy_Source))]
        public void Bind_sProdDocT_ShowLegacyFalse_DoesNotShowLegacy(E_sProdDocT legacyValue)
        {
            var itemCollection = new ListItemCollection();
            Tools.Bind_sProdDocT(itemCollection, renameAlternativeDocType: false, showLegacy: false, showEnhanced: true, currentValue: E_sProdDocT.Full);

            var selectedValueItem = itemCollection.FindByValue(legacyValue.ToString("D"));
            Assert.IsNull(selectedValueItem);
        }

        [Test]
        [TestCaseSource(nameof(Bind_sProdDocT_ShowEnhancedFalse_DoesNotShowEnhanced_Source))]
        public void Bind_sProdDocT_ShowEnhancedFalse_DoesNotShowEnhanced(E_sProdDocT enhancedValue)
        {
            var itemCollection = new ListItemCollection();
            Tools.Bind_sProdDocT(itemCollection, renameAlternativeDocType: false, showLegacy: true, showEnhanced: false, currentValue: E_sProdDocT.Full);

            var selectedValueItem = itemCollection.FindByValue(enhancedValue.ToString("D"));
            Assert.IsNull(selectedValueItem);
        }
    }
}
