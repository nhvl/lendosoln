﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using global::DataAccess;
    using global::LendingQB.Test.Developers.Fakes;
    using global::LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;

    [TestFixture]
    public class LoanIPathSettableTest
    {
        [Test]
        public void SetElement_NotBasicPathElement_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var bogusSelector = new DataPathSelectionElement(new SelectIdExpression(Guid.Empty.ToString()));
                var loan = FakePageData.Create();
                loan.InitLoad();

                Assert.Throws<CBaseException>(() => loan.SetElement(bogusSelector, "BOGUS_VALUE"));
            }
        }

        [Test]
        public void SetElement_InvalidFieldId_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var bogusField = new DataPathBasicElement("sBogusFieldId");
                var loan = FakePageData.Create();
                loan.InitLoad();

                Assert.Throws<CBaseException>(() => loan.SetElement(bogusField, "BOGUS_VALUE"));
            }
        }

        [Test]
        public void SetElement_ValidFieldIdNonStringValue_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var customField1Date = new DataPathBasicElement(nameof(CPageBase.sCustomField1D));
                var loan = FakePageData.Create();
                loan.InitLoad();

                Assert.Throws<CBaseException>(() => loan.SetElement(customField1Date, CDateTime.Create(new DateTime(2018, 8, 1))));
            }
        }

        [Test]
        public void SetElement_ValidFieldIdValidValue_Succeeds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var customField1Date = new DataPathBasicElement(nameof(CPageBase.sCustomField1D));
                var loan = FakePageData.Create();
                loan.InitLoad();

                loan.SetElement(customField1Date, "08/01/2018");

                Assert.AreEqual(new DateTime(2018, 8, 1), loan.sCustomField1D.DateTimeForComputation);
            }
        }
    }
}
