﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class FieldTrackInfoTest
    {
        private readonly Dictionary<E_sProdDocT, string> DocTypeToExpectedStringValue = new Dictionary<E_sProdDocT, string>()
        {
            [E_sProdDocT.Full] = "Full Document",
            [E_sProdDocT.Alt] = "Alt - 12 months bank stmts",
            [E_sProdDocT.Light] = "Lite - 6 months bank stmts",
            [E_sProdDocT.SIVA] = "NIV (SIVA) - Stated Income, Verified Assets",
            [E_sProdDocT.VISA] = "VISA - Verified Income, Stated Assets",
            [E_sProdDocT.SISA] = "NIV (SISA) - Stated Income, Stated Assets",
            [E_sProdDocT.NIVA] = "No Ratio - No Income, Verified Assets",
            [E_sProdDocT.NINA] = "NINA - No Income, No Assets",
            [E_sProdDocT.NISA] = "NISA - No Income, Stated Assets",
            [E_sProdDocT.NINANE] = "No Doc - No Income, No Assets, No Empl",
            [E_sProdDocT.NIVANE] = "NoDoc Verif Assets - No Income, Verified Assets, No Empl",
            [E_sProdDocT.VINA] = "VINA - Verified Income, No Assets",
            [E_sProdDocT.Streamline] = "FHA Streamline Refinance",
            [E_sProdDocT._12MoPersonalBankStatements] = "12 Mo. Personal Bank Statements",
            [E_sProdDocT._24MoPersonalBankStatements] = "24 Mo. Personal Bank Statements",
            [E_sProdDocT._12MoBusinessBankStatements] = "12 Mo. Business Bank Statements",
            [E_sProdDocT._24MoBusinessBankStatements] = "24 Mo. Business Bank Statements",
            [E_sProdDocT.OtherBankStatements] = "Other Bank Statements",
            [E_sProdDocT._1YrTaxReturns] = "1 Yr. Tax Returns",
            [E_sProdDocT.AssetUtilization] = "Asset Utilization",
            [E_sProdDocT.DebtServiceCoverage] = "Debt Service Coverage (DSCR)",
            [E_sProdDocT.NoIncome] = "No Ratio",
            [E_sProdDocT.Voe] = "VOE"
        };

        private static object[] sProdDocTDataSource = (
            from originalValue in Enum.GetValues(typeof(E_sProdDocT)).Cast<E_sProdDocT>()
            from currentValue in Enum.GetValues(typeof(E_sProdDocT)).Cast<E_sProdDocT>()
            where originalValue < currentValue
            select new object[] { originalValue, currentValue }).ToArray();

        [Test]
        [TestCaseSource(nameof(sProdDocTDataSource))]
        public void NewValue_sProdDocT_ReturnsExpectedStringValue(E_sProdDocT original, E_sProdDocT current)
        {
            var trackInfo = new FieldTrackInfo("sProdDocT", original.ToString("D"), current.ToString("D"), E_FieldTrackInfoTypeT.New, recordIndex: 0);
            Assert.AreEqual(DocTypeToExpectedStringValue[original], trackInfo.OldValue);
            Assert.AreEqual(DocTypeToExpectedStringValue[current], trackInfo.NewValue);
        }
    }
}
