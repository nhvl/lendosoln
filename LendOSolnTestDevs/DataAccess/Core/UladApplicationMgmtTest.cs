﻿using LendersOffice.Common;

namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.Audit;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class UladApplicationMgmtTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.loanId = CreateLoanInDatabase(this.principal);
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                if (this.loanId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(this.principal, this.loanId, false, false);
                }
            }
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void SetPrimaryBorrower_WhenAlreadyPrimary(bool keepAppsInSync)
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            var secondLegacy = loanData.GetAppData(secondAppId);
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aBConsumerId);
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aCConsumerId);
            Assert.IsTrue(secondLegacy.aHasCoborrower);
            Assert.AreNotEqual(borrowerId, coborrowerId);

            var firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            var secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            var secondUlad = aggregate.UladApplications[secondUladId];
            var secondUladBorrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == borrowerId).Value;
            var secondUladCoborrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == coborrowerId).Value;
            Assert.IsTrue(secondUladBorrowerAssoc.IsPrimary);
            Assert.IsFalse(secondUladCoborrowerAssoc.IsPrimary);

            SetPrimaryBorrowerOnUladApplication(aggregate, secondUladId, borrowerId);
            SaveLoan(loan);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            secondLegacy = loanData.GetAppData(secondAppId);
            borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aBConsumerId);
            coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aCConsumerId);
            Assert.IsTrue(secondLegacy.aHasCoborrower);
            Assert.AreNotEqual(borrowerId, coborrowerId);

            firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            secondUlad = aggregate.UladApplications[secondUladId];
            secondUladBorrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == borrowerId).Value;
            secondUladCoborrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == coborrowerId).Value;
            Assert.IsTrue(secondUladBorrowerAssoc.IsPrimary);
            Assert.IsFalse(secondUladCoborrowerAssoc.IsPrimary);
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void SetPrimaryBorrower_CoborrowerToBePrimary_SwapBorrowersDone(bool keepAppsInSync)
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            var secondLegacy = loanData.GetAppData(secondAppId);
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aBConsumerId);
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aCConsumerId);
            Assert.IsTrue(secondLegacy.aHasCoborrower);
            Assert.AreNotEqual(borrowerId, coborrowerId);

            var originalBorrowerId = borrowerId;
            var originalCoborrowerId = coborrowerId;

            var firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            var secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            var secondUlad = aggregate.UladApplications[secondUladId];
            var secondUladBorrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == borrowerId).Value;
            var secondUladCoborrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == coborrowerId).Value;
            Assert.IsTrue(secondUladBorrowerAssoc.IsPrimary);
            Assert.IsFalse(secondUladCoborrowerAssoc.IsPrimary);

            SetPrimaryBorrowerOnUladApplication(aggregate, secondUladId, coborrowerId);
            SaveLoan(loan);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            secondLegacy = loanData.GetAppData(secondAppId);
            borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aBConsumerId);
            coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondLegacy.aCConsumerId);
            Assert.IsTrue(secondLegacy.aHasCoborrower);
            Assert.AreNotEqual(borrowerId, coborrowerId);

            // Setting coborrower to be the primary always causes a swap borrowers
            Assert.AreEqual(originalBorrowerId, coborrowerId);
            Assert.AreEqual(originalCoborrowerId, borrowerId);

            firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            secondUlad = aggregate.UladApplications[secondUladId];
            secondUladBorrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == borrowerId).Value;
            secondUladCoborrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == secondUladId && a.Value.ConsumerId == coborrowerId).Value;

            // In all cases, the coborrower can never be the primary borrower
            Assert.IsTrue(secondUladBorrowerAssoc.IsPrimary);
            Assert.IsFalse(secondUladCoborrowerAssoc.IsPrimary);
        }

        [Test]
        public void InvalidUladIdentifier()
        {
            var invalidId = DataObjectIdentifier<DataObjectKind.UladApplication, Guid>.Create(Guid.NewGuid());

            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            Assert.Throws<CBaseException>(() => aggregate.RemoveUladApplication(invalidId));
        }

        [Test]
        public void AttemptRemoveLastApplication()
        {
            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var uladId = loanData.UladApplications.Keys.First();
            Assert.Throws<CBaseException>(() => aggregate.RemoveUladApplication(uladId));
        }

        [Test]
        public void RemoveUladWithSingleLegacyBorrower()
        {
            var secondAppId = CreateSecondApplication(this.loanId, false);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var appData = loanData.GetAppData(secondAppId);
            var borrowerId = appData.aBConsumerId;
            Assert.IsFalse(appData.aHasCoborrower);
            Assert.AreEqual(2, loan.LegacyApplications.Count);
            Assert.AreEqual(2, loan.LegacyApplicationConsumers.Count);
            Assert.AreEqual(2, aggregate.UladApplications.Count);
            Assert.AreEqual(2, aggregate.UladApplicationConsumers.Count);

            var uladId = aggregate.UladApplicationConsumers.Values.Where(a => a.ConsumerId.Value == borrowerId).First().UladApplicationId;
            RemoveUladApplication(aggregate, uladId); // NOTE: Save doesn't need to be called because it is done within this method

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            Assert.AreEqual(1, loan.LegacyApplications.Count);
            Assert.AreEqual(1, loan.LegacyApplicationConsumers.Count);
            Assert.AreEqual(1, aggregate.UladApplications.Count);
            Assert.AreEqual(1, aggregate.UladApplicationConsumers.Count);
            Assert.AreEqual(0, aggregate.UladApplicationConsumers.Values.Where(a => a.ConsumerId.Value == borrowerId).Count());
        }

        [Test]
        public void RemoveUladWithTwoLegacyBorrowers()
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var appData = loanData.GetAppData(secondAppId);
            var borrowerId = appData.aBConsumerId;
            Assert.IsTrue(appData.aHasCoborrower);
            Assert.AreEqual(2, loan.LegacyApplications.Count);
            Assert.AreEqual(3, loan.LegacyApplicationConsumers.Count);
            Assert.AreEqual(2, aggregate.UladApplications.Count);
            Assert.AreEqual(3, aggregate.UladApplicationConsumers.Count);

            var uladId = aggregate.UladApplicationConsumers.Where(a => a.Value.ConsumerId.Value == borrowerId).First().Value.UladApplicationId;
            RemoveUladApplication(aggregate, uladId); // NOTE: Save doesn't need to be called because it is done within this method

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            Assert.AreEqual(1, loan.LegacyApplications.Count);
            Assert.AreEqual(1, loan.LegacyApplicationConsumers.Count);
            Assert.AreEqual(1, aggregate.UladApplications.Count);
            Assert.AreEqual(1, aggregate.UladApplicationConsumers.Count);
            Assert.AreEqual(0, aggregate.UladApplicationConsumers.Where(a => a.Value.ConsumerId.Value == borrowerId).Count());
        }

        [Test]
        public void SingleOwnershipsGetDeleted_JointCrossAppOwnershipRemains()
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);
            this.AddOwnerships(secondAppId);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            Assert.AreEqual(3, loan.Assets.Count);
            Assert.AreEqual(4, loan.ConsumerAssets.Count);

            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);
            var appData = loanData.GetAppData(secondAppId);
            var borrowerId = appData.aBConsumerId;
            var coborrowerId = appData.aCConsumerId;

            var uladId = aggregate.UladApplicationConsumers.Where(a => a.Value.ConsumerId.Value == borrowerId).First().Value.UladApplicationId;
            RemoveUladApplication(aggregate, uladId); // NOTE: Save doesn't need to be called because it is done within this method

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            Assert.AreEqual(1, loan.Assets.Count);
            Assert.AreEqual(1, loan.ConsumerAssets.Count);
        }

        [Test]
        public void AddWithoutTracking_NoExceptionThrown()
        {
            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            var pageBase = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(pageBase);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                using (var connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection())
                {
                    connection.Open();
                    var transaction = connection.BeginTransaction();

                    // tested code
                    var brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
                    var uladData = aggregate.AddBlankUladApplication();
                    aggregate.Save(connection, transaction); // this is to confirm that the AddWithoutTracking calls worked as expected and no database exception is thrown

                    transaction.Rollback();
                    connection.Close();
                }
            }
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void AddLegacyApplicationTest_WithSyncFlag(bool keepAppsInSync)
        {
            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            int appCount = loan.GetAppNames().Count;
            int expectedCount = appCount + 1;

            var pageBase = ExtractLoanData(loan);
            var container = ExtractAggregate(pageBase);

            var brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var addedAppId = AddBlankLegacyApplication(brokerId, container);
            SaveLoan(loan);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            Assert.AreEqual(expectedCount, loan.GetAppNames().Count);

            pageBase = ExtractLoanData(loan);
            container = ExtractAggregate(pageBase);
            Assert.AreEqual(2, container.UladApplications.Count);

            var assoc = container.UladApplicationConsumers.Values.Last();
            var app = loan.GetAppData(expectedCount - 1);
            Assert.AreEqual(addedAppId, assoc.LegacyAppId);
            Assert.AreEqual(app.aAppId, assoc.LegacyAppId.Value);
            Assert.AreEqual(app.aBConsumerId, assoc.ConsumerId.Value);
        }


        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void AddUladApplicationTest_WithSyncFlag(bool keepAppsInSync)
        {
            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            Assert.AreEqual(1, loan.GetAppNames().Count);
            Assert.AreEqual(1, aggregate.UladApplications.Count);
            Assert.IsTrue(aggregate.UladApplications.First().Value.IsPrimary);

            var brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var uladData = AddBlankUladApplication(brokerId, aggregate);
            var uladId = uladData.Key;

            SaveLoan(loan);

            loan = CreateLoan(loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            var uladApp = aggregate.UladApplications[uladId];
            Assert.AreEqual(2, loan.GetAppNames().Count);
            Assert.AreEqual(2, aggregate.UladApplications.Count);
            Assert.IsFalse(uladApp.IsPrimary);

            var assoc = aggregate.UladApplicationConsumers.Single(a => a.Value.UladApplicationId == uladId).Value;

            var app = loan.GetAppData(1);
            Assert.AreEqual(app.aAppId, assoc.LegacyAppId.Value);
            Assert.AreEqual(app.aBConsumerId, assoc.ConsumerId.Value);
        }

        [Test]
        public void RetrieveTwoBorrowerTest()
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var uladId = aggregate.UladApplications.Last().Key;
            var list = aggregate.UladApplicationConsumers.Values.Where(association => association.UladApplicationId == uladId);
            Assert.AreEqual(2, list.Count());
        }

        [Test]
        public void AddConsumerToUladApplication_SyncedWithTwoBorrowersAssociated_ThrowsException()
        {
            var pageData = CreateLoan(this.loanId);
            InitLoadLoan(pageData);
            var pageBase = ExtractLoanData(pageData);
            pageBase.sSyncUladAndLegacyApps = true;
            var legacyAppId = pageBase.LegacyApplications.Keys.First();
            var aggregate = ExtractAggregate(pageBase);
            var uladAppId = aggregate.UladApplications.Keys.Single();
            var existingUladAssociation = aggregate.UladApplicationConsumers.Single();
            // The file is loaded and we can call this method on the aggregate.
            // Add second association for coborrower.
            AddConsumerToUladApplication(aggregate, uladAppId);
            pageBase.DoneLoad();

            // We need to reload the file to perform another addition.
            InitLoadLoan(pageData);
            pageBase = ExtractLoanData(pageData);
            pageBase.sSyncUladAndLegacyApps = true;
            aggregate = ExtractAggregate(pageBase);
            Assert.Throws<CBaseException>(() => AddConsumerToUladApplication(aggregate, uladAppId));
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void AddConsumerToUladApplication_SingleBorrowerAssociated_AddsToSameLegacyAppAsBorrower(bool keepAppsInSync)
        {
            var pageData = CreateLoan(this.loanId);
            InitLoadLoan(pageData);
            var pageBase = ExtractLoanData(pageData);
            pageBase.sSyncUladAndLegacyApps = keepAppsInSync;
            var legacyAppId = pageBase.LegacyApplications.Keys.First();
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(pageBase.GetAppData(0).aCConsumerId);
            var aggregate = ExtractAggregate(pageBase);
            var uladAppId = aggregate.UladApplications.Keys.Single();
            var existingUladAssociation = aggregate.UladApplicationConsumers.Single();

            AddConsumerToUladApplication(aggregate, uladAppId);
            pageBase.DoneLoad();

            // Need to reload the loan file.
            InitLoadLoan(pageData);
            pageBase = ExtractLoanData(pageData);
            aggregate = ExtractAggregate(pageBase);

            Assert.AreEqual(2, aggregate.UladApplicationConsumers.Count);
            var newUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Key != existingUladAssociation.Key).Value;
            Assert.AreEqual(uladAppId, newUladAssociation.UladApplicationId);
            Assert.AreEqual(coborrowerId, newUladAssociation.ConsumerId);
            Assert.AreEqual(legacyAppId, newUladAssociation.LegacyAppId);
            Assert.AreEqual(false, newUladAssociation.IsPrimary);
        }

        [Test]
        public void AddConsumerToUladApplication_NotSyncedWithNoClearLegacyApp_AddsNewLegacyApplication()
        {
            var pageData = CreateLoan(this.loanId);
            InitLoadLoan(pageData);
            pageData.sSyncUladAndLegacyApps = false;
            var pageBase = ExtractLoanData(pageData);
            var existingLegacyAppId = pageBase.LegacyApplications.Keys.First();
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(pageBase.GetAppData(0).aBConsumerId);
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(pageBase.GetAppData(0).aCConsumerId);
            var aggregate = ExtractAggregate(pageBase);
            var uladAppId = aggregate.UladApplications.Keys.Single();
            // Add second association for coborrower.
            AddConsumerToUladApplication(aggregate, uladAppId);
            pageBase.DoneLoad();
            var existingUladConsumerAssociations = aggregate.UladApplicationConsumers.Keys.ToList();
            // Need to reload the loan file.
            InitLoadLoan(pageData);
            pageData.sSyncUladAndLegacyApps = false;
            pageBase = ExtractLoanData(pageData);
            aggregate = ExtractAggregate(pageBase);

            var thirdBorrowerId = AddConsumerToUladApplication(aggregate, uladAppId);
            pageBase.DoneLoad();

            // Need to reload the loan file.
            InitLoadLoan(pageData);
            pageBase = ExtractLoanData(pageData);
            aggregate = ExtractAggregate(pageBase);

            Assert.AreEqual(3, aggregate.UladApplicationConsumers.Count);
            var newUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == thirdBorrowerId).Value;
            var newConsumerId = newUladAssociation.ConsumerId;
            var newLegacyAppId = newUladAssociation.LegacyAppId;
            Assert.AreEqual(uladAppId, newUladAssociation.UladApplicationId);
            Assert.AreNotEqual(borrowerId, newUladAssociation.ConsumerId);
            Assert.AreNotEqual(coborrowerId, newUladAssociation.ConsumerId);
            Assert.AreNotEqual(existingLegacyAppId, newUladAssociation.LegacyAppId);
            Assert.IsNotNull(newUladAssociation.LegacyAppId);
            Assert.AreEqual(false, newUladAssociation.IsPrimary);

            Assert.AreEqual(2, pageBase.LegacyApplications.Count);
            Assert.AreEqual(3, pageBase.LegacyApplicationConsumers.Count);
            var newLegacyAppAssociation = pageBase.LegacyApplicationConsumers.Values.Single(a => a.LegacyApplicationId == newLegacyAppId);
            Assert.AreEqual(newConsumerId, newLegacyAppAssociation.ConsumerId);
            Assert.AreEqual(true, newLegacyAppAssociation.IsPrimary);
        }

        [TestCase(false)]
        [TestCase(true)]
        public void SetUladPrimary_WhenAlreadyPrimary(bool keepAppsInSync)
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            var firstLegacy = loanData.GetAppData(firstAppId);
            var secondLegacy = loanData.GetAppData(secondAppId);
            Assert.IsTrue(firstLegacy.aIsPrimary);
            Assert.IsFalse(secondLegacy.aIsPrimary);

            var firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            var secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            var firstUlad = aggregate.UladApplications[firstUladId];
            var secondUlad = aggregate.UladApplications[secondUladId];
            Assert.IsTrue(firstUlad.IsPrimary);
            Assert.IsFalse(secondUlad.IsPrimary);

            aggregate.SetPrimaryUladApplication(firstUladId);
            SaveLoan(loan);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            firstLegacy = loanData.GetAppData(firstAppId);
            secondLegacy = loanData.GetAppData(secondAppId);
            Assert.IsTrue(firstLegacy.aIsPrimary);
            Assert.IsFalse(secondLegacy.aIsPrimary);

            firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            firstUlad = aggregate.UladApplications[firstUladId];
            secondUlad = aggregate.UladApplications[secondUladId];
            Assert.IsTrue(firstUlad.IsPrimary);
            Assert.IsFalse(secondUlad.IsPrimary);
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void SetUladPrimary_WhenOtherIsPrimary(bool keepAppsInSync)
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            var firstLegacy = loanData.GetAppData(firstAppId);
            var secondLegacy = loanData.GetAppData(secondAppId);
            Assert.IsTrue(firstLegacy.aIsPrimary);
            Assert.IsFalse(secondLegacy.aIsPrimary);

            var firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            var secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            var firstUlad = aggregate.UladApplications[firstUladId];
            var secondUlad = aggregate.UladApplications[secondUladId];
            Assert.IsTrue(firstUlad.IsPrimary);
            Assert.IsFalse(secondUlad.IsPrimary);

            aggregate.SetPrimaryUladApplication(secondUladId);
            SaveLoan(loan);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            int firstAppIndex = keepAppsInSync ? 1 : 0; // primary is the first (index = 0) so setting the 2nd as primary means turning it into the first
            firstAppId = loanData.GetAppData(firstAppIndex).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            firstLegacy = loanData.GetAppData(firstAppId);
            secondLegacy = loanData.GetAppData(secondAppId);
            if (keepAppsInSync)
            {
                Assert.IsFalse(firstLegacy.aIsPrimary);
                Assert.IsTrue(secondLegacy.aIsPrimary);
            }
            else
            {
                Assert.IsTrue(firstLegacy.aIsPrimary);
                Assert.IsFalse(secondLegacy.aIsPrimary);
            }

            firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            firstUlad = aggregate.UladApplications[firstUladId];
            secondUlad = aggregate.UladApplications[secondUladId];
            Assert.IsFalse(firstUlad.IsPrimary);
            Assert.IsTrue(secondUlad.IsPrimary);
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void SetLegacyPrimary_WhenAlreadyPrimary(bool keepAppsInSync)
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            var firstLegacy = loanData.GetAppData(firstAppId);
            var secondLegacy = loanData.GetAppData(secondAppId);
            Assert.IsTrue(firstLegacy.aIsPrimary);
            Assert.IsFalse(secondLegacy.aIsPrimary);

            var firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            var secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            var firstUlad = aggregate.UladApplications[firstUladId];
            var secondUlad = aggregate.UladApplications[secondUladId];
            Assert.IsTrue(firstUlad.IsPrimary);
            Assert.IsFalse(secondUlad.IsPrimary);

            aggregate.SetPrimaryLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(firstAppId));
            SaveLoan(loan);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            firstLegacy = loanData.GetAppData(firstAppId);
            secondLegacy = loanData.GetAppData(secondAppId);
            Assert.IsTrue(firstLegacy.aIsPrimary);
            Assert.IsFalse(secondLegacy.aIsPrimary);

            firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            firstUlad = aggregate.UladApplications[firstUladId];
            secondUlad = aggregate.UladApplications[secondUladId];
            Assert.IsTrue(firstUlad.IsPrimary);
            Assert.IsFalse(secondUlad.IsPrimary);
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void SetLegacyPrimary_WhenOtherIsPrimary(bool keepAppsInSync)
        {
            var secondAppId = CreateSecondApplication(this.loanId, true);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, keepAppsInSync);
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var firstAppId = loanData.GetAppData(0).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            var firstLegacy = loanData.GetAppData(firstAppId);
            var secondLegacy = loanData.GetAppData(secondAppId);
            Assert.IsTrue(firstLegacy.aIsPrimary);
            Assert.IsFalse(secondLegacy.aIsPrimary);

            var firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            var secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            var firstUlad = aggregate.UladApplications[firstUladId];
            var secondUlad = aggregate.UladApplications[secondUladId];
            Assert.IsTrue(firstUlad.IsPrimary);
            Assert.IsFalse(secondUlad.IsPrimary);

            aggregate.SetPrimaryLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(secondAppId));
            SaveLoan(loan);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);

            firstAppId = loanData.GetAppData(1).aAppId;
            Assert.AreNotEqual(secondAppId, firstAppId);

            firstLegacy = loanData.GetAppData(firstAppId);
            secondLegacy = loanData.GetAppData(secondAppId);
            Assert.IsFalse(firstLegacy.aIsPrimary);
            Assert.IsTrue(secondLegacy.aIsPrimary);

            firstUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == firstAppId).First().UladApplicationId;
            secondUladId = aggregate.UladApplicationConsumers.Values.Where(a => a.LegacyAppId.Value == secondAppId).First().UladApplicationId;
            Assert.AreNotEqual(secondUladId, firstUladId);

            firstUlad = aggregate.UladApplications[firstUladId];
            secondUlad = aggregate.UladApplications[secondUladId];
            if (keepAppsInSync)
            {
                Assert.IsFalse(firstUlad.IsPrimary);
                Assert.IsTrue(secondUlad.IsPrimary);
            }
            else
            {
                Assert.IsTrue(firstUlad.IsPrimary);
                Assert.IsFalse(secondUlad.IsPrimary);
            }
        }

        [Test]
        public void sCanSyncUladAndLegacyApps_SingleUladAppSingleBorrower_ReturnsTrue()
        {
            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(1, loan.UladApplicationConsumers.Count);
            Assert.AreEqual(1, loan.LegacyApplications.Count);
            Assert.AreEqual(1, loan.LegacyApplicationConsumers.Count);

            bool canSync = loan.sCanSyncUladAndLegacyApps;
            
            Assert.IsTrue(canSync);
        }

        [Test]
        public void sCanSyncUladAndLegacyApps_UladAppWithTwoConsumersOnSeparateLegacyApps_ReturnsFalse()
        {
            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            var appId = loan.UladApplications.Keys.First();
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                
                var consumerIdToRemove = CPageData.AddConsumerToUladApplication(
                    this.loanId.ToIdentifier<DataObjectKind.Loan>(),
                    appId);
                
                CPageData.AddConsumerToUladApplication(
                    this.loanId.ToIdentifier<DataObjectKind.Loan>(),
                    appId);
                
                CPageData.RemoveBorrower(this.loanId.ToIdentifier<DataObjectKind.Loan>(),  consumerIdToRemove);
            }
            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(2, loan.UladApplicationConsumers.Count);
            Assert.AreEqual(2, loan.LegacyApplications.Count);
            Assert.AreEqual(2, loan.LegacyApplicationConsumers.Count);
            
            bool canSync = loan.sCanSyncUladAndLegacyApps;
            
            Assert.IsFalse(canSync);
        }
        
        [Test]
        public void sCanSyncUladAndLegacyApps_SingleUladAppTwoBorrowers_ReturnsTrue()
        {
            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                
                CPageData.AddConsumerToUladApplication(
                    this.loanId.ToIdentifier<DataObjectKind.Loan>(),
                    loan.UladApplications.Keys.First());
            }

            loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(2, loan.UladApplicationConsumers.Count);
            Assert.AreEqual(1, loan.LegacyApplications.Count);
            Assert.AreEqual(2, loan.LegacyApplicationConsumers.Count);

            bool canSync = loan.sCanSyncUladAndLegacyApps;
            
            Assert.IsTrue(canSync);
        }
        
        [Test]
        public void sCanSyncUladAndLegacyApps_SingleUladAppThreeBorrowers_ReturnsFalse()
        {
            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            var appId = loan.UladApplications.Keys.First();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                
                CPageData.AddConsumerToUladApplication(
                    this.loanId.ToIdentifier<DataObjectKind.Loan>(),
                    appId);
                CPageData.AddConsumerToUladApplication(
                    this.loanId.ToIdentifier<DataObjectKind.Loan>(),
                    appId);
            }
            
            loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(3, loan.UladApplicationConsumers.Count);
            Assert.AreEqual(2, loan.LegacyApplications.Count);
            Assert.AreEqual(3, loan.LegacyApplicationConsumers.Count);

            bool canSync = loan.sCanSyncUladAndLegacyApps;
            
            Assert.IsFalse(canSync);
        }

        [Test]
        public void sCanSyncUladAndLegacyApps_ThreeUladAppsWithSingleBorrowerPerApp_ReturnsTrue()
        {
            var loanIdentifier = this.loanId.ToIdentifier<DataObjectKind.Loan>();
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                CPageData.AddBlankUladApplication(loanIdentifier);
                CPageData.AddBlankUladApplication(loanIdentifier);
            }
            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            Assert.AreEqual(3, loan.UladApplications.Count);
            Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

            var canSync = loan.sCanSyncUladAndLegacyApps;

            Assert.IsTrue(canSync);
        }

        [Test]
        public void sSyncUladAndLegacyApps_SetToTrueWhenCanSync_Succeeds()
        {
            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            Assert.IsTrue(loan.sCanSyncUladAndLegacyApps);

            // No exception => success.
            loan.sSyncUladAndLegacyApps = true;
        }
        
        [Test]
        public void sSyncUladAndLegacyApps_SetToTrueWhenCannotSync_ThrowsException()
        {
            var loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);
            var appId = loan.UladApplications.Keys.First();
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                
                CPageData.AddConsumerToUladApplication(
                    this.loanId.ToIdentifier<DataObjectKind.Loan>(),
                    appId);
                CPageData.AddConsumerToUladApplication(
                    this.loanId.ToIdentifier<DataObjectKind.Loan>(),
                    appId);
            }
            loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);
            Assert.IsFalse(loan.sCanSyncUladAndLegacyApps);

            Assert.Throws<CBaseException>(() => loan.sSyncUladAndLegacyApps = true);
        }

        [Test]
        public void StartWithSingleBorrowerLoan_AddThreeBorrowersToUladApplication_ThreeLegacyApplicationsResult()
        {
            // NOTE: This is confirming counter-intuitive behavior.  The normal expectation is that adding 4 consumers
            //       to a ULAD application will result in two legacy applications.  However, per the requirements only
            //       the second consumer can be placed as a coborrower on a legacy application.  Subsequent consumers always end
            //       up as new borrowers on new legacy applications.  The reason is that for any historical process there may
            //       be multiple legacy applications that only have the borrower defined, so it would be difficult to determine
            //       which should get the coborrower (the legacy application ordering is somewhat arbitrary so shouldn't be used).
            var loan = CreateLoan(this.loanId); // first consumer created here
            InitLoadLoan(loan);

            Assert.AreEqual(1, loan.LegacyApplications.Count);
            Assert.AreEqual(1, loan.Consumers.Count);
            Assert.AreEqual(1, loan.UladApplications.Count);

            var uladAppId = loan.UladApplications.Single().Key;

            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var secondConsumerId = AddConsumerToUladApplication(aggregate, uladAppId);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);

            // second consumer should be coborrower on existing legacy application
            Assert.AreEqual(1, loan.LegacyApplications.Count);
            Assert.AreEqual(2, loan.Consumers.Count);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(secondConsumerId.Value, loan.GetAppData(0).aCConsumerId);

            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);
            var thirdConsumerId = AddConsumerToUladApplication(aggregate, uladAppId);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);

            // third consumer should be borrower on a new legacy application
            Assert.AreEqual(2, loan.LegacyApplications.Count);
            Assert.AreEqual(3, loan.Consumers.Count);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(thirdConsumerId.Value, loan.GetAppData(1).aBConsumerId);

            loanData = ExtractLoanData(loan);
            aggregate = ExtractAggregate(loanData);
            var fourthConsumerId = AddConsumerToUladApplication(aggregate, uladAppId);

            loan = CreateLoan(this.loanId);
            InitLoadLoan(loan);

            // fourth consumer should be borrower on a third legacy application
            Assert.AreEqual(3, loan.LegacyApplications.Count);
            Assert.AreEqual(4, loan.Consumers.Count);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(fourthConsumerId.Value, loan.GetAppData(2).aBConsumerId);
        }

        private void AddOwnerships(Guid appId)
        {
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appId);

            var loan = CreateLoan(this.loanId);
            InitSaveLoan(loan, false);

            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var appData = loan.GetAppData(appId);
            var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
            var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);

            var firstAppData = loan.GetAppData(0); // this is the original legacy app, different from the app data from appId
            var firstBorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(firstAppData.aBConsumerId);
            var firstAppIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(firstAppData.aAppId);

            var asset1 = new Asset();
            var assetId1 = aggregate.Add(borrowerId, asset1);

            var asset2 = new Asset();
            var assetId2 = aggregate.Add(coborrowerId, asset2);

            var asset3 = new Asset();
            var assetId3 = aggregate.Add(coborrowerId, asset3);
            var assetId4 = aggregate.AddOwnership(firstBorrowerId, assetId3); ; // asset owned by both borrowers

            SaveLoan(loan);
        }

        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> AddConsumerToUladApplication(ILoanLqbCollectionContainer aggregate, DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.StoredProcedure);

                return aggregate.AddConsumerToUladApplication(uladAppId);
            }
        }

        private static void SetPrimaryBorrowerOnUladApplication(
            ILoanLqbCollectionContainer container,
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                container.SetPrimaryBorrowerOnUladApplication(consumerId);
            }
        }

        private static DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AddBlankLegacyApplication(DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId, ILoanLqbCollectionContainer container)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                return container.AddBlankLegacyApplication();
            }
        }

        private static void RemoveUladApplication(ILoanLqbCollectionContainer aggregate, DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladId)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                aggregate.RemoveUladApplication(uladId);
            }
        }

        private static Guid CreateSecondApplication(Guid loanId, bool hasCoborrower)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                return LoanTestUtilities.AddNewLegacyAndUladApplicationToLoan(loanId, hasCoborrower);
            }
        }

        private static Guid CreateLoanInDatabase(AbstractUserPrincipal principal)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var creator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
                return creator.CreateBlankUladLoanFile();
            }
        }

        private static CPageData CreateLoan(Guid loanId)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                return new CPageData(loanId, nameof(UladApplicationMgmtTest), new string[] { "sSyncUladAndLegacyApps", "afDelMarriedCobor", "afSwapMarriedBorAndCobor", "UladApplications", "UladApplicationConsumers" });
            }
        }

        private static void InitLoadLoan(CPageData loan)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                loan.InitLoad();
            }
        }

        private static void InitSaveLoan(CPageData loan, bool keepAppsInSync)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.sSyncUladAndLegacyApps = keepAppsInSync;
            }
        }

        private static void SaveLoan(CPageData loan)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                loan.Save();
            }
        }

        private static KeyValuePair<DataObjectIdentifier<DataObjectKind.UladApplication, Guid>, UladApplication> AddBlankUladApplication(DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId, ILoanLqbCollectionContainer container)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                return container.AddBlankUladApplication();
            }
        }

        private static CPageBase ExtractLoanData(CPageData pageData)
        {
            return CPageBaseExtractor.Extract(pageData);
        }

        private static ILoanLqbCollectionContainer ExtractAggregate(CPageBase pageBase)
        {
            return pageBase.loanLqbCollectionContainer;
        }
    }
}
