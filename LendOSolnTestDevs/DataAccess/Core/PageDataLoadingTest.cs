﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Developers.Utils;
    using global::DataAccess;
    using NSubstitute;
    using NUnit.Framework;
    using BrokerId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.ClientCompany, System.Guid>;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class PageDataLoadingTest
    {
        private static readonly IReadOnlyCollection<string> assetCollectionDependencyFields = new string[]
        {
            nameof(CPageData.Assets),
            nameof(CPageData.ConsumerAssets),
        };

        private static readonly IReadOnlyCollection<string> incomeCollectionDependencyFields = new string[]
        {
            nameof(CPageData.IncomeSources),
            nameof(CPageData.ConsumerIncomeSources),
        };

        private static readonly IReadOnlyCollection<string> fullDependencyFields = assetCollectionDependencyFields.Concat(incomeCollectionDependencyFields).ToArray();

        [Test]
        public void InitLoad_DoesNotHaveNewCollectionsOrIncome_ShouldNotLoadCollectionContainer()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = new CFullAccessPageData(loanRef.Id, fullDependencyFields);
                var baseLoan = CPageBaseExtractor.Extract(loan);
                baseLoan.InjectTestLoanLqbCollectionContainer = CreateSubstituteWrapper;

                loan.InitLoad();

                Assert.AreEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
                Assert.AreEqual(false, loan.sIsIncomeCollectionEnabled);
                baseLoan.loanLqbCollectionContainer.DidNotReceive().Load(Arg.Any<BrokerId>(), Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>());
            }
        }

        [Test]
        public void InitLoad_HasNewCollectionsOrIncome_ShouldLoadCollectionContainer()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = new CFullAccessPageData(loanRef.Id, fullDependencyFields);
                var baseLoan = CPageBaseExtractor.Extract(loan);
                baseLoan.InjectTestLoanLqbCollectionContainer = CreateSubstituteWrapper;

                loan.InitLoad();

                Assert.GreaterOrEqual(loan.sBorrowerApplicationCollectionT, E_sLqbCollectionT.UseLqbCollections);
                baseLoan.loanLqbCollectionContainer.Received().Load(Arg.Any<BrokerId>(), Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>());
            }
        }

        [Test]
        public void InitSave_DoesNotHaveNewCollectionsOrIncome_ShouldNotLoadCollectionContainer()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = new CFullAccessPageData(loanRef.Id, fullDependencyFields);
                var baseLoan = CPageBaseExtractor.Extract(loan);
                baseLoan.InjectTestLoanLqbCollectionContainer = CreateSubstituteWrapper;

                loan.InitSave();

                Assert.AreEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
                Assert.AreEqual(false, loan.sIsIncomeCollectionEnabled);
                baseLoan.loanLqbCollectionContainer.DidNotReceive().Load(Arg.Any<BrokerId>(), Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>());
            }
        }

        [Test]
        public void InitSave_HasNewCollectionsOrIncome_ShouldLoadCollectionContainer()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = new CFullAccessPageData(loanRef.Id, fullDependencyFields);
                var baseLoan = CPageBaseExtractor.Extract(loan);
                baseLoan.InjectTestLoanLqbCollectionContainer = CreateSubstituteWrapper;

                loan.InitSave();

                Assert.GreaterOrEqual(loan.sBorrowerApplicationCollectionT, E_sLqbCollectionT.UseLqbCollections);
                baseLoan.loanLqbCollectionContainer.Received().Load(Arg.Any<BrokerId>(), Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>());
            }
        }

        [Test]
        public void Save_DoesNotHaveNewCollectionsOrIncome_ShouldNotSaveCollectionContainer()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = new CFullAccessPageData(loanRef.Id, fullDependencyFields);
                var baseLoan = CPageBaseExtractor.Extract(loan);
                baseLoan.InjectTestLoanLqbCollectionContainer = CreateSubstituteWrapper;
                loan.InitSave();
                Assert.AreEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
                Assert.AreEqual(false, loan.sIsIncomeCollectionEnabled);

                loan.Save();

                baseLoan.loanLqbCollectionContainer.DidNotReceive().Save(Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>());
            }
        }

        [Test]
        public void Save_HasNewCollectionsOrIncome_ShouldSaveCollectionContainer()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = new CFullAccessPageData(loanRef.Id, fullDependencyFields);
                var baseLoan = CPageBaseExtractor.Extract(loan);
                baseLoan.InjectTestLoanLqbCollectionContainer = CreateSubstituteWrapper;
                loan.InitSave();
                Assert.GreaterOrEqual(loan.sBorrowerApplicationCollectionT, E_sLqbCollectionT.UseLqbCollections);

                loan.Save();

                baseLoan.loanLqbCollectionContainer.Received().Save(Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>());
            }
        }

        private static ILoanLqbCollectionContainer CreateSubstituteWrapper(ILoanLqbCollectionContainer actualLoanCollectionContainer)
        {
            var fakeLoanCollectionContainer = Substitute.For<ILoanLqbCollectionContainer, IShimContainer>(); // IShimContainer is used by workflow checks
            var interfaceType = typeof(ILoanLqbCollectionContainer);
            fakeLoanCollectionContainer.When(c => c.RegisterCollections(Arg.Any<IEnumerable<string>>())).Do(call => actualLoanCollectionContainer.RegisterCollections((IEnumerable<string>)call[0]));
            fakeLoanCollectionContainer.HasRegisteredCollections.Returns(callInfo => actualLoanCollectionContainer.HasRegisteredCollections);
            fakeLoanCollectionContainer
                .When(c => c.Load(Arg.Any<BrokerId>(), Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>()))
                .Do(call => interfaceType.GetMethod(nameof(ILoanLqbCollectionContainer.Load)).Invoke(actualLoanCollectionContainer, call.Args()));
            return fakeLoanCollectionContainer;
        }
    }
}
