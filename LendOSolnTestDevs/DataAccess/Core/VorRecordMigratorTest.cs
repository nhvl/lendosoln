﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class VorRecordMigratorTest
    {
        private static readonly Guid BorrowerFirstAddressId = new Guid("11111111-1111-1111-1111-111111111111");
        private static readonly Guid BorrowerSecondAddressId = new Guid("22222222-2222-2222-2222-222222222222");
        private static readonly Guid BorrowerThirdAddressId = new Guid("33333333-3333-3333-3333-333333333333");
        private static readonly Guid CoborrowerFirstAddressId = new Guid("44444444-4444-4444-4444-444444444444");
        private static readonly Guid CoborrowerSecondAddressId = new Guid("55555555-5555-5555-5555-555555555555");
        private static readonly Guid CoborrowerThirdAddressId = new Guid("66666666-6666-6666-6666-666666666666");
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = new Guid("77777777-7777-7777-7777-777777777777").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId = new Guid("88888888-8888-8888-8888-888888888888").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = new Guid("99999999-9999-9999-9999-999999999999").ToIdentifier<DataObjectKind.LegacyApplication>();

        #region Fake Data Points
        private readonly string VerifSignatureImgId = new Guid("11111111-1111-1111-1111-111111111112").ToString();
        private readonly Guid VerifSigningEmployeeId = new Guid("11111111-1111-1111-1111-111111111113");
        private readonly string AccountName = "Test Account Name";
        private readonly string AddressFor = "123 For Rd.";
        private readonly string AddressTo = "456 To St.";
        private readonly string Attention = "Important Person";
        private readonly string CityFor = "Four City";
        private readonly string CityTo = "Too City";
        private readonly string Description = "Desc";
        private readonly string FaxTo = "1111111111";
        private readonly bool IsSeeAttachment = false;
        private readonly string LandlordCreditorName = "Landlord";
        private readonly string PhoneTo = "2222222222";
        private readonly CDateTime PrepD = CDateTime.InvalidWrapValue;
        private readonly string StateFor = "CA";
        private readonly string StateTo = "AZ";
        private readonly CDateTime VerifExpD = CDateTime.Create(new DateTime(2018, 10, 1));
        private readonly CDateTime VerifRecvD = CDateTime.Create(new DateTime(2018, 10, 2));
        private readonly CDateTime VerifReorderedD = CDateTime.Create(new DateTime(2018, 10, 2));
        private readonly CDateTime VerifSentD = CDateTime.Create(new DateTime(2018, 10, 3));
        private readonly E_VorT VerifT = E_VorT.Mortgage;
        private readonly string ZipFor = "12345";
        private readonly string ZipTo = "93117";
        #endregion

        [Test]
        public void Migrate_SpecialBorrowerRecords_CreatesOwnershipAssociationsForBorrower()
        {
            var collectionFake = Substitute.For<IVerificationOfRentCollection>();
            collectionFake.Count().Returns(3);
            var firstFake = Substitute.For<IVerificationOfRent>();
            firstFake.RecordId.Returns(BorrowerFirstAddressId);
            collectionFake.GetRecordByIndex(0).Returns(firstFake);
            var secondFake = Substitute.For<IVerificationOfRent>();
            secondFake.RecordId.Returns(BorrowerSecondAddressId);
            collectionFake.GetRecordByIndex(1).Returns(secondFake);
            var thirdFake = Substitute.For<IVerificationOfRent>();
            thirdFake.RecordId.Returns(BorrowerThirdAddressId);
            collectionFake.GetRecordByIndex(2).Returns(thirdFake);
            var appFake = this.GetAppFake(collectionFake, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new VorRecordMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(3, container.ConsumerVorRecords.Count);
            Assert.IsTrue(container.ConsumerVorRecords.Values.All(a => a.ConsumerId == BorrowerId));
            Assert.IsTrue(container.ConsumerVorRecords.Values.All(a => a.IsPrimary));
            Assert.IsTrue(container.ConsumerVorRecords.Values.All(a => a.AppId == AppId));
            Assert.IsTrue(container.VorRecords.ContainsKey(container.ConsumerVorRecords.Values.ElementAt(0).VorRecordId));
            Assert.IsTrue(container.VorRecords.ContainsKey(container.ConsumerVorRecords.Values.ElementAt(1).VorRecordId));
            Assert.IsTrue(container.VorRecords.ContainsKey(container.ConsumerVorRecords.Values.ElementAt(2).VorRecordId));
            Assert.AreEqual(1, container.VorRecords.Values.Count(v => v.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[BorrowerFirstAddressId]));
            Assert.AreEqual(1, container.VorRecords.Values.Count(v => v.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[BorrowerSecondAddressId]));
            Assert.AreEqual(1, container.VorRecords.Values.Count(v => v.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[BorrowerThirdAddressId]));
        }

        [Test]
        public void Migrate_SpecialCoborrowerRecords_CreatesOwnershipAssociationsForCoborrower()
        {
            var collectionFake = Substitute.For<IVerificationOfRentCollection>();
            collectionFake.Count().Returns(3);
            var firstFake = Substitute.For<IVerificationOfRent>();
            firstFake.RecordId.Returns(CoborrowerFirstAddressId);
            collectionFake.GetRecordByIndex(0).Returns(firstFake);
            var secondFake = Substitute.For<IVerificationOfRent>();
            secondFake.RecordId.Returns(CoborrowerSecondAddressId);
            collectionFake.GetRecordByIndex(1).Returns(secondFake);
            var thirdFake = Substitute.For<IVerificationOfRent>();
            thirdFake.RecordId.Returns(CoborrowerThirdAddressId);
            collectionFake.GetRecordByIndex(2).Returns(thirdFake);
            var appFake = this.GetAppFake(collectionFake, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new VorRecordMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(3, container.ConsumerVorRecords.Count);
            Assert.IsTrue(container.ConsumerVorRecords.Values.All(a => a.ConsumerId == CoborrowerId));
            Assert.IsTrue(container.ConsumerVorRecords.Values.All(a => a.IsPrimary));
            Assert.IsTrue(container.ConsumerVorRecords.Values.All(a => a.AppId == AppId));
            Assert.IsTrue(container.VorRecords.ContainsKey(container.ConsumerVorRecords.Values.ElementAt(0).VorRecordId));
            Assert.IsTrue(container.VorRecords.ContainsKey(container.ConsumerVorRecords.Values.ElementAt(1).VorRecordId));
            Assert.IsTrue(container.VorRecords.ContainsKey(container.ConsumerVorRecords.Values.ElementAt(2).VorRecordId));
            Assert.AreEqual(1, container.VorRecords.Values.Count(v => v.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[CoborrowerFirstAddressId]));
            Assert.AreEqual(1, container.VorRecords.Values.Count(v => v.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[CoborrowerSecondAddressId]));
            Assert.AreEqual(1, container.VorRecords.Values.Count(v => v.LegacyRecordSpecialType == VorRecord.LegacyRecordIdToSpecialType[CoborrowerThirdAddressId]));
        }

        [Test]
        public void Migrate_RegularRecordWithoutSpecialId_CreatesOwnershipAssociationForBorrower()
        {
            var collectionFake = Substitute.For<IVerificationOfRentCollection>();
            collectionFake.Count().Returns(1);
            var firstFake = Substitute.For<IVerificationOfRent>();
            firstFake.RecordId.Returns(Guid.NewGuid());
            collectionFake.GetRecordByIndex(0).Returns(firstFake);
            var appFake = this.GetAppFake(collectionFake, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new VorRecordMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerVorRecords.Count);
            var association = container.ConsumerVorRecords.Values.Single();
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
            Assert.IsTrue(container.VorRecords.ContainsKey(association.VorRecordId));
            Assert.IsNull(container.VorRecords.Values.Single().LegacyRecordSpecialType);
        }

        [Test]
        public void CreateEntity_FilledOutLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeVorRecord();
            var testMigrator = new TestVorRecordMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertDataCopiedOver(entity);
        }

        [Test]
        public void CreateEntity_InvalidVerifSignatureImgIdInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IVerificationOfRent>();
            fakeLegacyRecord.VerifHasSignature.Returns(true);
            fakeLegacyRecord.VerifSignatureImgId.Returns("self-portrait.jpeg");
            var testMigrator = new TestVorRecordMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        private static readonly Guid[] CoborrowerSpecialRecords = new[]
        {
            CoborrowerFirstAddressId,
            CoborrowerSecondAddressId,
            CoborrowerThirdAddressId
        };

        [Test]
        public void Migrate_SpecialCoborrowerRecordButNoCoborrower_Throws(
            [ValueSource(nameof(CoborrowerSpecialRecords))] Guid legacyRecordId)
        {
            var recordFake = Substitute.For<IVerificationOfRent>();
            recordFake.RecordId.Returns(legacyRecordId);
            var collectionFake = Substitute.For<IVerificationOfRentCollection>();
            collectionFake.Count().Returns(1);
            collectionFake.GetRecordByIndex(Arg.Any<int>()).Returns(recordFake);
            var appFake = this.GetAppFake(collectionFake, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var testMigrator = new TestVorRecordMigrator(
                appFake,
                container);

            Assert.Catch<Exception>(() => testMigrator.MigrateAppLevelRecordsToLoanLevel());
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(IUladDataLayerMigrationAppDataProvider app)
        {
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var appConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { app });
            loanData.LegacyApplicationConsumers.Returns(appConsumers);
            var container = new LoanLqbCollectionContainer(
                new LoanLqbEmptyCollectionProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null) as ILoanLqbCollectionContainer;
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.VorRecords), nameof(ILoanLqbCollectionContainer.ConsumerVorRecords) });
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());
            return container;
        }

        private IUladDataLayerMigrationAppDataProvider GetAppFake(IVerificationOfRentCollection collection, bool hasCoborrower = false)
        {
            var appFake = Substitute.For<IUladDataLayerMigrationAppDataProvider>();
            appFake.aAppId.Returns(AppId.Value);
            appFake.aBConsumerId.Returns(BorrowerId.Value);
            appFake.aCConsumerId.Returns(CoborrowerId.Value);
            appFake.aHasCoborrower.Returns(hasCoborrower);
            appFake.aVorCollection.Returns(collection);
            return appFake;
        }

        private IVerificationOfRent GetFakeVorRecord()
        {
            var fake = Substitute.For<IVerificationOfRent>();
            fake.VerifHasSignature.Returns(true);
            fake.VerifSignatureImgId.Returns(VerifSignatureImgId);
            fake.VerifSigningEmployeeId.Returns(VerifSigningEmployeeId);
            fake.AccountName.Returns(AccountName);
            fake.AddressFor.Returns(AddressFor);
            fake.AddressTo.Returns(AddressTo);
            fake.Attention.Returns(Attention);
            fake.CityFor.Returns(CityFor);
            fake.CityTo.Returns(CityTo);
            fake.Description.Returns(Description);
            fake.FaxTo.Returns(FaxTo);
            fake.IsSeeAttachment.Returns(IsSeeAttachment);
            fake.LandlordCreditorName.Returns(LandlordCreditorName);
            fake.PhoneTo.Returns(PhoneTo);
            fake.PrepD.Returns(PrepD);
            fake.StateFor.Returns(StateFor);
            fake.StateTo.Returns(StateTo);
            fake.VerifExpD.Returns(VerifExpD);
            fake.VerifRecvD.Returns(VerifRecvD);
            fake.VerifReorderedD.Returns(VerifReorderedD);
            fake.VerifSentD.Returns(VerifSentD);
            fake.VerifT.Returns(VerifT);
            fake.ZipFor.Returns(ZipFor);
            fake.ZipTo.Returns(ZipTo);
            return fake;
        }

        private void AssertDataCopiedOver(VorRecord entity)
        {
            Assert.AreEqual(VerifSignatureImgId, entity.VerifSignatureImgId.ToString());
            Assert.AreEqual(VerifSigningEmployeeId, entity.VerifSigningEmployeeId.Value.Value);
            Assert.AreEqual(AccountName, entity.AccountName.ToString());
            Assert.AreEqual(AddressFor, entity.StreetAddressFor.ToString());
            Assert.AreEqual(AddressTo, entity.StreetAddressTo.ToString());
            Assert.AreEqual(Attention, entity.Attention.ToString());
            Assert.AreEqual(CityFor, entity.CityFor.ToString());
            Assert.AreEqual(CityTo, entity.CityTo.ToString());
            Assert.AreEqual(Description, entity.Description.ToString());
            Assert.AreEqual(FaxTo, entity.FaxTo.ToString());
            Assert.AreEqual(IsSeeAttachment, entity.IsSeeAttachment);
            Assert.AreEqual(LandlordCreditorName, entity.LandlordCreditorName.ToString());
            Assert.AreEqual(PhoneTo, entity.PhoneTo.ToString());
            Assert.AreEqual(null, entity.PrepDate);
            Assert.AreEqual(StateFor, entity.StateFor.ToString());
            Assert.AreEqual(StateTo, entity.StateTo.ToString());
            Assert.AreEqual(VerifExpD.DateTimeForComputation, entity.VerifExpDate.Value.Date);
            Assert.AreEqual(VerifRecvD.DateTimeForComputation, entity.VerifRecvDate.Value.Date);
            Assert.AreEqual(VerifReorderedD.DateTimeForComputation, entity.VerifReorderedDate.Value.Date);
            Assert.AreEqual(VerifSentD.DateTimeForComputation, entity.VerifSentDate.Value.Date);
            Assert.AreEqual(VerifT, entity.VerifType);
            Assert.AreEqual(ZipFor, entity.ZipFor.ToString());
            Assert.AreEqual(ZipTo, entity.ZipTo.ToString());
        }
    }

    internal class TestVorRecordMigrator : VorRecordMigrator
    {
        public TestVorRecordMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer container)
            : base(app, container)
        {
        }

        public new VorRecord CreateEntity(IVerificationOfRent legacy)
        {
            return base.CreateEntity(legacy);
        }
    }
}
