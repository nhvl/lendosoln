﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class ReoMigratorTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = new Guid("11111111-1111-1111-1111-111111111111").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId = new Guid("22222222-2222-2222-2222-222222222222").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();

        #region Fake Data Points
        private readonly string Addr = "1 Infinite Loop";
        private readonly string City = "Cupertino";
        private readonly decimal GrossRentI = 1;
        private readonly decimal HExp = 2;
        private readonly bool IsEmptyCreated = false;
        private readonly bool IsForceCalcNetRentalI = true;
        private readonly bool IsPrimaryResidence = true;
        private readonly bool IsSubjectProp = false;
        private readonly decimal MAmt = 3;
        private readonly decimal MPmt = 4;
        private readonly decimal NetRentI = 5;
        private readonly bool NetRentILckd = true;
        private readonly int OccR = 6;
        private readonly E_ReoStatusT StatT = E_ReoStatusT.PendingSale;
        private readonly string State = "CA";
        private readonly E_ReoTypeT TypeT = E_ReoTypeT.ComR;
        private readonly decimal Val = 7;
        private readonly string Zip = "92708" ;
        #endregion

        [Test]
        public void Migrate_BorrowerOwnedRecord_CreatesOwnershipAssociationForBorrower()
        {
            var appFake = this.GetAppFake(E_ReOwnerT.Borrower);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new ReoMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerRealProperties.Count);
            var association = container.ConsumerRealProperties.Values.Single();
            Assert.AreEqual(container.RealProperties.Keys.Single(), association.RealPropertyId);
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecord_CreatesOwnershipAssociationForCoborrower()
        {
            var appFake = this.GetAppFake(E_ReOwnerT.CoBorrower, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new ReoMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerRealProperties.Count);
            var association = container.ConsumerRealProperties.Values.Single();
            Assert.AreEqual(container.RealProperties.Keys.Single(), association.RealPropertyId);
            Assert.AreEqual(CoborrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_ReOwnerT.CoBorrower, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new ReoMigrator(appFake, container);

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        [Test]
        public void Migrate_JointlyOwnedRecord_CreatesOwnershipAssociationForBothConsumers()
        {
            var appFake = this.GetAppFake(E_ReOwnerT.Joint, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new ReoMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(2, container.ConsumerRealProperties.Count);
            Assert.AreEqual(1, container.ConsumerRealProperties.Values.Count(a => a.ConsumerId == BorrowerId));
            Assert.AreEqual(1, container.ConsumerRealProperties.Values.Count(a => a.ConsumerId == CoborrowerId));
            var borrowerAssociation = container.ConsumerRealProperties.Values.Single(a => a.ConsumerId == BorrowerId);
            Assert.AreEqual(container.RealProperties.Keys.Single(), borrowerAssociation.RealPropertyId);
            Assert.AreEqual(true, borrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, borrowerAssociation.AppId);

            var coborrowerAssociation = container.ConsumerRealProperties.Values.Single(a => a.ConsumerId == CoborrowerId);
            Assert.AreEqual(container.RealProperties.Keys.Single(), coborrowerAssociation.RealPropertyId);
            Assert.AreEqual(false, coborrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, coborrowerAssociation.AppId);
        }

        [Test]
        public void Migrate_JointlyOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_ReOwnerT.Joint, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new ReoMigrator(appFake, container);

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        private IUladDataLayerMigrationAppDataProvider GetAppFake(E_ReOwnerT fakeOwnerType, bool hasCoborrower = false)
        {
            var appFake = Substitute.For<IUladDataLayerMigrationAppDataProvider>();
            appFake.aAppId.Returns(AppId.Value);
            appFake.aBConsumerId.Returns(BorrowerId.Value);
            appFake.aCConsumerId.Returns(CoborrowerId.Value);
            appFake.aHasCoborrower.Returns(hasCoborrower);

            var recordFake = Substitute.For<ICollectionItemBase2, IRealEstateOwned>();
            ((IRealEstateOwned)recordFake).ReOwnerT.Returns(fakeOwnerType);
            var collectionFake = Substitute.For<IRecordCollection, IReCollection>();
            collectionFake.CountRegular.Returns(1);
            collectionFake.GetRegularRecordAt(Arg.Any<int>()).Returns(recordFake);
            appFake.aReCollection.Returns(collectionFake);

            return appFake;
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(IUladDataLayerMigrationAppDataProvider app)
        {
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var appConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { app });
            loanData.LegacyApplicationConsumers.Returns(appConsumers);
            var container = new LoanLqbCollectionContainer(
                new LoanLqbEmptyCollectionProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null) as ILoanLqbCollectionContainer;
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.RealProperties), nameof(ILoanLqbCollectionContainer.ConsumerRealProperties) });
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());
            return container;
        }

        [Test]
        public void CreateEntity_FilledOutLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeReo();
            var testMigrator = new TestReoMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertDataCopiedOver(entity);
        }

        private IRealEstateOwned GetFakeReo()
        {
            IRealEstateOwned legacy = Substitute.For<IRealEstateOwned>();
            legacy.Addr = Addr;
            legacy.City = City;
            legacy.GrossRentI = GrossRentI;
            legacy.HExp = HExp;
            legacy.IsEmptyCreated = IsEmptyCreated;
            legacy.IsForceCalcNetRentalI = IsForceCalcNetRentalI;
            legacy.IsPrimaryResidence = IsPrimaryResidence;
            legacy.IsSubjectProp = IsSubjectProp;
            legacy.MAmt = MAmt;
            legacy.MPmt = MPmt;
            legacy.NetRentI = NetRentI;
            legacy.NetRentILckd = NetRentILckd;
            legacy.OccR = OccR;
            legacy.StatT = StatT;
            legacy.State = State;
            legacy.TypeT = TypeT;
            legacy.Val = Val;
            legacy.Zip = Zip;
            return legacy;
        }

        private void AssertDataCopiedOver(RealProperty entity)
        {
            Assert.AreEqual(Addr, entity.StreetAddress.ToString());
            Assert.AreEqual(City, entity.City.ToString());
            Assert.AreEqual(GrossRentI, entity.GrossRentInc.Value.Value);
            Assert.AreEqual(HExp, entity.HExp.Value.Value);
            Assert.AreEqual(IsEmptyCreated, entity.IsEmptyCreated);
            Assert.AreEqual(IsForceCalcNetRentalI, entity.IsForceCalcNetRentalInc);
            Assert.AreEqual(IsPrimaryResidence, entity.IsPrimaryResidence);
            Assert.AreEqual(IsSubjectProp, entity.IsSubjectProp);
            Assert.AreEqual(MAmt, entity.MtgAmt.Value.Value);
            Assert.AreEqual(MPmt, entity.MtgPmt.Value.Value);
            Assert.AreEqual(NetRentI, entity.NetRentInc.Value.Value);
            Assert.AreEqual(NetRentILckd, entity.NetRentIncLocked);
            Assert.AreEqual(OccR, (decimal)entity.OccR);
            Assert.AreEqual(StatT, entity.Status);
            Assert.AreEqual(State, entity.State.ToString());
            Assert.AreEqual(TypeT, entity.Type);
            Assert.AreEqual(Val, entity.MarketValue.Value.Value);
            Assert.AreEqual(Zip, entity.Zip.ToString());
        }
    }

    internal class TestReoMigrator : ReoMigrator
    {
        public TestReoMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer container)
            : base(app, container)
        {
        }

        public new RealProperty CreateEntity(IRealEstateOwned legacy)
        {
            return base.CreateEntity(legacy);
        }
    }
}
