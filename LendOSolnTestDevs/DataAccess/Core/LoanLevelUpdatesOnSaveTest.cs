﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data.SqlClient;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class LoanLevelUpdatesOnSaveTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;
        private Guid firstAppId;
        private Guid secondAppId;

        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> firstBorrowerId;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> secondBorrowerId;

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                this.loanId = creator.CreateBlankUladLoanFile();

                var loan = GetLoan(this.loanId);
                loan.AddNewApp();

                var app = loan.GetAppData(0);
                this.firstAppId = app.aAppId;
                this.firstBorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(app.aBConsumerId);

                app = loan.GetAppData(1);
                this.secondAppId = app.aAppId;
                this.secondBorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(app.aBConsumerId);
            }
        }

        [TearDown]
        public void TearDown()
        {
            if (this.principal != null)
            {
                using (var helper = new FoolHelper())
                {
                    helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                    Tools.DeclareLoanFileInvalid(
                        this.principal,
                        this.loanId,
                        sendNotifications: false,
                        generateLinkLoanMsgEvent: false);
                }
            }
        }

        [Test]
        public void InitialSetupState_LoadLoan_ConfirmAssumedState()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = GetLoan(this.loanId);
                loan.InitLoad();

                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
                Assert.AreEqual(2, loan.nApps);
            }
        }

        [Test]
        public void Assets_Save_SetsCorrectAsstLiqTot()
        {
            // This is a re-write of AppLevelFlushTest.Assets_Flush_SetsCorrectAsstLiqTot.
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = GetLoan(this.loanId);
                loan.InitSave();

                // Set up the first app's asset.
                var firstAsset = new Asset();
                firstAsset.AssetType = E_AssetT.Checking;
                firstAsset.Value = 1;
                var firstId = loan.AddAsset(firstBorrowerId, firstAsset);

                // Set up the second app's asset.
                var secondAsset = new Asset();
                secondAsset.AssetType = E_AssetT.Checking;
                secondAsset.Value = 2;
                var secondId = loan.AddAsset(secondBorrowerId, secondAsset);

                loan.Save();

                loan = GetLoan(this.loanId);
                loan.InitLoad();

                // Make sure that flush is only dealing with the assets that belong to the app.
                Assert.AreEqual(1, loan.GetAppData(0).aAsstLiqTot);
                Assert.AreEqual(2, loan.GetAppData(1).aAsstLiqTot);
            }
        }

        [Test]
        public void Liabilities_Save_SetsCorrectLiaBalTot()
        {
            // This is a re-write of AppLevelFlushTest.Liabilities_Flush_SetsCorrectLiaBalTot.
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = GetLoan(this.loanId);
                loan.InitSave();
                var defProvider = loan.CreateLiabilityDefaultsProvider();

                // Set up the first app's asset.
                var firstLiability = new Liability(defProvider);
                firstLiability.Bal = 1;
                var firstId = loan.AddLiability(firstBorrowerId, firstLiability);

                // Set up the second app's asset.
                var secondLiability = new Liability(defProvider);
                secondLiability.Bal = 2;
                var secondId = loan.AddLiability(secondBorrowerId, secondLiability);

                loan.Save();

                loan = GetLoan(this.loanId);
                loan.InitLoad();

                // Make sure that flush is only dealing with the liabilities that belong to the app.
                Assert.AreEqual(1, loan.GetAppData(0).aLiaBalTot);
                Assert.AreEqual(2, loan.GetAppData(1).aLiaBalTot);
            }
        }

        [Test]
        public void Liabilities_UpdateRelatedReo_SetsCorrectLiabilityAggregate()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = GetLoan(this.loanId);
                loan.InitSave();

                var firstLiability = new Liability(loan.CreateLiabilityDefaultsProvider())
                {
                    Bal = 1,
                    PayoffAmtLocked = true,
                    PayoffAmtData = 300,
                    DebtType = E_DebtT.Mortgage,
                    WillBePdOff = true,
                    ExcludeFromUw = false,
                    PayoffTimingLockedData = true,
                    PayoffTimingData = PayoffTiming.AtClosing,
                };
                DataObjectIdentifier<DataObjectKind.Liability, Guid> firstLiabilityId = loan.AddLiability(this.firstBorrowerId, firstLiability);
                DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId = loan.AddRealProperty(
                    this.firstBorrowerId,
                    new RealProperty { Status = E_ReoStatusT.Sale });
                loan.AddRealPropertyLiabilityAssociation(new RealPropertyLiabilityAssociation(realPropertyId, firstLiabilityId));
                loan.Save();

                loan = new CPageData(this.loanId, new[] { nameof(CPageData.RealProperties) });
                loan.InitSave();
                Assert.AreEqual(0, loan.GetAppData(0).aLiaPdOffTot);

                loan.RealProperties[realPropertyId].Status = E_ReoStatusT.PendingSale;
                loan.Save();

                loan = GetLoan(this.loanId);
                loan.InitLoad();
                Assert.AreEqual(300, loan.GetAppData(0).aLiaPdOffTot);
            }
        }

        [Test]
        public void RealProperties_Save_SetsCorrectReTotVal()
        {
            // This is a re-write of AppLevelFlushTest.RealPropertyCollection_Flush_SetsCorrectReTotVal.
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = GetLoan(this.loanId);
                loan.InitSave();

                // Set up the first app's asset.
                var firstProperty = new RealProperty();
                firstProperty.MarketValue = 1;
                var firstId = loan.AddRealProperty(firstBorrowerId, firstProperty);

                // Set up the second app's asset.
                var secondProperty = new RealProperty();
                secondProperty.MarketValue = 2;
                var secondId = loan.AddRealProperty(secondBorrowerId, secondProperty);

                loan.Save();

                loan = GetLoan(this.loanId);
                loan.InitLoad();

                // Make sure that flush is only dealing with the properties that belong to the app.
                Assert.AreEqual(1, loan.GetAppData(0).aReTotVal);
                Assert.AreEqual(2, loan.GetAppData(1).aReTotVal);
            }
        }

        [Test]
        public void Employments_SaveUladCollections_SetsCorrectCachedEmployerAddress()
        {
            // NOTE that all the fields that call GetStringForCacheTable are employment record properties.
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.ConfigurationQuery);

                var loan = GetLoan(this.loanId);
                loan.InitSave();

                var firstAddress = StreetAddress.Create("10101 Slater Ave").Value; // Original MeridianLink address!
                var secondAddress = StreetAddress.Create("1600 Sunflower Ave").Value;

                // Set up employments for the first borrower
                var firstEmployment = new EmploymentRecord(loan.CreateEmploymentRecordDefaultsProvider());
                firstEmployment.IsCurrent = false; // it isn't really necessary to set this
                firstEmployment.EmploymentStatusType = E_EmplmtStat.Previous;
                firstEmployment.EmployerAddress = firstAddress;
                var firstId = loan.AddEmploymentRecord(firstBorrowerId, firstEmployment);

                var secondEmployment = new EmploymentRecord(loan.CreateEmploymentRecordDefaultsProvider());
                secondEmployment.IsCurrent = true; // it isn't really necessary to set this
                secondEmployment.EmploymentStatusType = E_EmplmtStat.Current;
                secondEmployment.EmployerAddress = secondAddress;
                secondEmployment.IsPrimary = true;
                var secondId = loan.AddEmploymentRecord(firstBorrowerId, secondEmployment);

                loan.Save();

                loan = GetLoan(this.loanId);
                loan.InitLoad();

                Assert.AreEqual(secondAddress.ToString(), loan.GetAppData(0).aBEmplrAddr);

                string sql = $"SELECT aBEmplrAddr FROM [dbo].[LOAN_FILE_CACHE_2] WHERE sLId = @loanId";
                var pram = new SqlParameter("@loanId", this.loanId);

                string cachedAddress = null;
                var query = SQLQueryString.Create(sql).Value;
                var prams = new SqlParameter[] { pram };
                using (var conn = SqlConnectionManager.Get(this.principal.BrokerId).GetConnection())
                {
                    var factory = GenericLocator<ISqlDriverFactory>.Factory;
                    var driver = factory.Create(TimeoutInSeconds.Thirty);
                    using (var reader = driver.Select(conn, null, query, prams))
                    {
                        Assert.IsTrue(reader.Read());

                        cachedAddress = Convert.ToString(reader["aBEmplrAddr"]);
                    }
                }

                Assert.AreEqual(secondAddress.ToString(), cachedAddress);
            }
        }

        private static CPageData GetLoan(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                loanId,
                typeof(LoanLevelUpdatesOnSaveTest));
        }
    }
}
