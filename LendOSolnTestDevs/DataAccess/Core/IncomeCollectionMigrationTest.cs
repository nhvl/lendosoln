﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers;
    using LendingQB.Test.Developers.Fakes;
    using LendingQB.Test.Developers.FOOL;
    using LendingQB.Test.Developers.Utils;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class IncomeCollectionMigrationTest
    {
        private static readonly string SubjectPropertyNetCashFlowDescription = OtherIncome.GetDescription(E_aOIDescT.SubjPropNetCashFlow);
        private static readonly IReadOnlyList<string> NormalOtherIncomeDescriptions = ((E_aOIDescT[])Enum.GetValues(typeof(E_aOIDescT)))
                .Where(v => v != E_aOIDescT.SubjPropNetCashFlow)
                .Select(v => OtherIncome.GetDescription(v))
                .Where(v => !string.IsNullOrEmpty(v))
                .Concat(new[] { "a few", "more", "for good measure" })
                .ToList();

        private Guid readOnlyLoanId;

        private Guid readOnlyTestFileId;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var creator = CLoanFileCreator.GetCreator(
                    LoginTools.LoginAs(TestAccountType.LoTest001),
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.readOnlyLoanId = creator.CreateBlankLoanFile();
                this.readOnlyTestFileId = creator.CreateNewTestFile();
            }
        }

        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var p = LoginTools.LoginAs(TestAccountType.LoTest001);
                Tools.DeclareLoanFileInvalid(p, this.readOnlyLoanId, false, false);
                Tools.DeclareLoanFileInvalid(p, this.readOnlyTestFileId, false, false);
            }
        }

        [Test]
        public void GetErrorMessageForMigration_LegacyCollectionVersion_ReturnsAnErrorMessage()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                Assert.AreEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
                Assert.IsFalse(loan.sIsIncomeCollectionEnabled);

                string message = IncomeCollectionMigration.GetErrorMessageForMigration(loan);

                Assert.IsNotNullOrEmpty(message);
            }
        }

        [Test]
        public void GetErrorMessageForMigration_AlreadyMigrated_ReturnsAnErrorMessage()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                loan.MigrateToUseLqbCollections();
                loan.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loan.sIsIncomeCollectionEnabled);

                string message = IncomeCollectionMigration.GetErrorMessageForMigration(loan);

                Assert.IsNotNullOrEmpty(message);
            }
        }

        [Test]
        public void MigrateToIncomeCollection_AlreadyMigrated_DoesNothing()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                loan.MigrateToUseLqbCollections();
                loan.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loan.sIsIncomeCollectionEnabled);

                Assert.DoesNotThrow(() => loan.MigrateToIncomeSourceCollection());
            }
        }

        [Test]
        public void GetErrorMessageForMigration_CoborrowerNotDefinedButHasIncome_ReturnsAnErrorMessage()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                SetupNormalIncome(loan);
                loan.RemoveCoborrowerFromLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                Assert.IsFalse(loan.GetAppData(0).aHasCoborrower);
                Assert.IsTrue(loan.GetAppData(0).aCTotI > 0);

                string message = IncomeCollectionMigration.GetErrorMessageForMigration(loan);

                Assert.IsNotNullOrEmpty(message);
            }
        }

        [Test]
        public void MigrateToIncomeCollection_CoborrowerNotDefinedButHasIncome_Throws()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                SetupNormalIncome(loan);
                loan.RemoveCoborrowerFromLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                Assert.IsFalse(loan.GetAppData(0).aHasCoborrower);
                Assert.IsTrue(loan.GetAppData(0).aCTotI > 0);
                loan.MigrateToUseLqbCollections();

                Assert.Catch<Exception>(() => loan.MigrateToIncomeSourceCollection());
            }
        }

        [Test]
        public void GetErrorMessageForMigration_OtherIncomeHasSubjectPropertyNetCashFlow_ReturnsAnErrorMessage()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                SetupNormalIncome(loan);
                loan.Apps.First().aOtherIncomeList.Add(new OtherIncome() { Amount = 234, Desc = SubjectPropertyNetCashFlowDescription, IsForCoBorrower = true });

                string message = IncomeCollectionMigration.GetErrorMessageForMigration(loan);

                Assert.IsNotNullOrEmpty(message);
            }
        }

        [Test]
        public void MigrateToIncomeCollection_OtherIncomeHasSubjectPropertyNetCashFlow_Throws()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                SetupNormalIncome(loan);
                loan.Apps.First().aOtherIncomeList.Add(new OtherIncome() { Amount = 234, Desc = SubjectPropertyNetCashFlowDescription, IsForCoBorrower = true });
                loan.MigrateToUseLqbCollections();

                Assert.Catch<Exception>(() => loan.MigrateToIncomeSourceCollection());
            }
        }

        [Test]
        public void MigrateToIncomeCollection_OtherIncomeNormal_WorksInMemory()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitLoad();
                loan.MigrateToUseLqbCollections();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                SetupNormalIncome(loan);
                decimal borrowerOldOtherIncomeSum = Tools.SumMoney(loan.GetAppData(0).aOtherIncomeList.Where(i => !i.IsForCoBorrower).Select(i => i.Amount));
                decimal coborrowOldOtherIncomeSum = Tools.SumMoney(loan.GetAppData(0).aOtherIncomeList.Where(i => i.IsForCoBorrower).Select(i => i.Amount));
                var standardIncomeTypes = new HashSet<IncomeType>(new[] { IncomeType.BaseIncome, IncomeType.Overtime, IncomeType.Bonuses, IncomeType.Commission, IncomeType.DividendsOrInterest });
                Assert.IsFalse(loan.sIsIncomeCollectionEnabled);

                loan.MigrateToIncomeSourceCollection();

                CollectionAssert.AllItemsAreNotNull(loan.IncomeSources.Values.Select(i => i.IncomeType));
                CollectionAssert.AllItemsAreNotNull(loan.IncomeSources.Values.Select(i => i.MonthlyAmountData));
                var firstApp = loan.Apps.First();
                var borrowerConsumerId = firstApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var borrowerIncomeSources = loan.ConsumerIncomeSources.Values.Where(c => c.ConsumerId == borrowerConsumerId).Select(c => loan.IncomeSources[c.IncomeSourceId]).ToList();

                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.BaseIncome, 10.01M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.Overtime, 10.02M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.Bonuses, 10.03M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.Commission, 10.04M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.DividendsOrInterest, 10.05M);

                Money? borrowerNewOtherIncomeSum = borrowerIncomeSources.Where(i => !standardIncomeTypes.Contains(i.IncomeType.Value)).Aggregate((Money?)Money.Zero, (total, income) => total + income.MonthlyAmountData);
                Assert.AreEqual((Money)borrowerOldOtherIncomeSum, borrowerNewOtherIncomeSum);

                var coborrowConsumerId = firstApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coborrowIncomeSources = loan.ConsumerIncomeSources.Values.Where(c => c.ConsumerId == coborrowConsumerId).Select(c => loan.IncomeSources[c.IncomeSourceId]).ToList();

                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.BaseIncome, 20.01M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.Overtime, 20.02M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.Bonuses, 20.03M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.Commission, 20.04M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.DividendsOrInterest, 20.05M);

                Money? coborrowNewOtherIncomeSum = coborrowIncomeSources.Where(i => !standardIncomeTypes.Contains(i.IncomeType.Value)).Aggregate((Money?)Money.Zero, (total, income) => total + income.MonthlyAmountData);
                Assert.AreEqual((Money)coborrowOldOtherIncomeSum, coborrowNewOtherIncomeSum);
            }
        }

        [Test]
        public void MigrateToIncomeCollection_OtherIncomeNormal_WorksThroughSave()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                SetupNormalIncome(loanBefore);
                decimal borrowerOldOtherIncomeSum = Tools.SumMoney(loanBefore.GetAppData(0).aOtherIncomeList.Where(i => !i.IsForCoBorrower).Select(i => i.Amount));
                decimal coborrowOldOtherIncomeSum = Tools.SumMoney(loanBefore.GetAppData(0).aOtherIncomeList.Where(i => i.IsForCoBorrower).Select(i => i.Amount));
                var standardIncomeTypes = new HashSet<IncomeType>(new[] { IncomeType.BaseIncome, IncomeType.Overtime, IncomeType.Bonuses, IncomeType.Commission, IncomeType.DividendsOrInterest });
                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, loanBefore.sBorrowerApplicationCollectionT);
                Assert.IsFalse(loanBefore.sIsIncomeCollectionEnabled);

                loanBefore.MigrateToIncomeSourceCollection();
                loanBefore.Save();

                var loan = loanRef.CreateNewPageDataWithBypass();
                loan.InitLoad();
                CollectionAssert.AllItemsAreNotNull(loan.IncomeSources.Values.Select(i => i.IncomeType));
                CollectionAssert.AllItemsAreNotNull(loan.IncomeSources.Values.Select(i => i.MonthlyAmountData));
                var firstApp = loan.Apps.First();
                var borrowerConsumerId = firstApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var borrowerIncomeSources = loan.ConsumerIncomeSources.Values.Where(c => c.ConsumerId == borrowerConsumerId).Select(c => loan.IncomeSources[c.IncomeSourceId]).ToList();

                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.BaseIncome, 10.01M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.Overtime, 10.02M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.Bonuses, 10.03M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.Commission, 10.04M);
                AssertExactlyOneIncomeOfType(borrowerIncomeSources, IncomeType.DividendsOrInterest, 10.05M);

                Money? borrowerNewOtherIncomeSum = borrowerIncomeSources.Where(i => !standardIncomeTypes.Contains(i.IncomeType.Value)).Aggregate((Money?)Money.Zero, (total, income) => total + income.MonthlyAmountData);
                Assert.AreEqual((Money)borrowerOldOtherIncomeSum, borrowerNewOtherIncomeSum);

                var coborrowConsumerId = firstApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coborrowIncomeSources = loan.ConsumerIncomeSources.Values.Where(c => c.ConsumerId == coborrowConsumerId).Select(c => loan.IncomeSources[c.IncomeSourceId]).ToList();

                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.BaseIncome, 20.01M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.Overtime, 20.02M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.Bonuses, 20.03M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.Commission, 20.04M);
                AssertExactlyOneIncomeOfType(coborrowIncomeSources, IncomeType.DividendsOrInterest, 20.05M);

                Money? coborrowNewOtherIncomeSum = coborrowIncomeSources.Where(i => !standardIncomeTypes.Contains(i.IncomeType.Value)).Aggregate((Money?)Money.Zero, (total, income) => total + income.MonthlyAmountData);
                Assert.AreEqual((Money)coborrowOldOtherIncomeSum, coborrowNewOtherIncomeSum);
            }
        }

        [Test]
        public void MigrateToIncomeCollection_NonTestFileWithStageConstantDisablingUpdates_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyLoanId, this.GetType());
                loan.InitSave();
                loan.MigrateToUseLqbCollections();
                Assert.IsFalse(loan.sIsIncomeCollectionEnabled);
                IConfigurationQueryFactory fakeConfigFactory = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: true);
                helper.RegisterMockDriverFactory(fakeConfigFactory);

                Assert.Throws<CBaseException>(() => loan.MigrateToIncomeSourceCollection());
            }
        }

        [Test]
        public void MigrateToIncomeCollection_TestFileWithStageConstantDisablingUpdates_DoesNotThrow()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.readOnlyTestFileId, this.GetType());
                loan.InitSave();
                loan.MigrateToUseLqbCollections();
                Assert.IsFalse(loan.sIsIncomeCollectionEnabled);
                IConfigurationQueryFactory fakeConfigFactory = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: true);
                helper.RegisterMockDriverFactory(fakeConfigFactory);

                loan.MigrateToIncomeSourceCollection();
            }
        }

        [TestCase(true)]
        [TestCase(false)]
        public void MigrateToIncomeCollection_VariousFileTypesWithStageConstantAllowingUpdates_DoesNotThrow(bool isTestFile)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.EmailSender);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(isTestFile ? this.readOnlyTestFileId : this.readOnlyLoanId, this.GetType());
                loan.InitSave();
                loan.MigrateToUseLqbCollections();
                Assert.IsFalse(loan.sIsIncomeCollectionEnabled);
                IConfigurationQueryFactory fakeConfigFactory = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: false);
                helper.RegisterMockDriverFactory(fakeConfigFactory);

                loan.MigrateToIncomeSourceCollection();
            }
        }

        private IConfigurationQueryFactory GetFakeStageConfig(bool disableBorrowerApplicationCollectionTUpdates)
        {
            var fakeValues = new Dictionary<string, Tuple<int, string>>()
            {
                ["DisableBorrowerApplicationCollectionTUpdates"] = Tuple.Create(disableBorrowerApplicationCollectionTUpdates ? 1 : 0, "")
            };
            var fakeStageConfig = new FakeStageConfigurationQuery(fakeValues);
            var fakeConfigFactory = new FakeConfigurationQueryFactory(Substitute.For<ISiteConfigurationQuery>(), fakeStageConfig);
            return fakeConfigFactory;
        }

        private void AssertExactlyOneIncomeOfType(IEnumerable<IncomeSource> incomeSources, IncomeType type, Money amount)
        {
            var incomeSourcesOfType = incomeSources.Where(i => i.IncomeType == type).ToList();
            Assert.AreEqual(1, incomeSourcesOfType.Count);
            Assert.AreEqual(amount, incomeSourcesOfType.Single().MonthlyAmountData);
        }

        private void SetupNormalIncome(CPageData loan)
        {
            int borrowerCount = 0;
            foreach (var app in loan.Apps)
            {
                decimal borrowerIncomeAmount = ++borrowerCount * 10M;
                app.aBBaseI = borrowerIncomeAmount + .01M;
                app.aBOvertimeI = borrowerIncomeAmount + .02M;
                app.aBBonusesI = borrowerIncomeAmount + .03M;
                app.aBCommisionI = borrowerIncomeAmount + .04M;
                app.aBDividendI = borrowerIncomeAmount + .05M;

                decimal centCounter = .06M;
                foreach (var description in NormalOtherIncomeDescriptions)
                {
                    app.aOtherIncomeList.Add(new OtherIncome() { Amount = borrowerIncomeAmount + centCounter, Desc = description, IsForCoBorrower = false });
                    centCounter += .01M;
                }

                if (!app.aHasCoborrower)
                {
                    continue;
                }

                borrowerIncomeAmount = ++borrowerCount * 10M;
                app.aCBaseI = borrowerIncomeAmount + .01M;
                app.aCOvertimeI = borrowerIncomeAmount + .02M;
                app.aCBonusesI = borrowerIncomeAmount + .03M;
                app.aCCommisionI = borrowerIncomeAmount + .04M;
                app.aCDividendI = borrowerIncomeAmount + .05M;

                centCounter = .06M;
                foreach (var description in NormalOtherIncomeDescriptions)
                {
                    app.aOtherIncomeList.Add(new OtherIncome() { Amount = borrowerIncomeAmount + centCounter, Desc = description, IsForCoBorrower = true });
                    centCounter += .01M;
                }
            }
        }
    }
}
