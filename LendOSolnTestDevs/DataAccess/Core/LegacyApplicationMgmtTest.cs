﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class LegacyApplicationMgmtTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var creator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankUladLoanFile();
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                if (this.loanId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(this.principal, this.loanId, false, false);
                }
            }
        }

        [Test]
        public void EndPointAccess_aHasCoborrowerData_ReturnsValue()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loan = this.CreateLoan();
                this.InitSaveLoan(loan);

                var appData = loan.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.Save();

                loan = this.CreateLoan();
                loan.InitLoad();

                appData = loan.GetAppData(0);
                string flag = PageDataUtilities.GetValue(loan, appData, "aHasCoborrowerData");

                Assert.AreEqual("Yes", flag);
            }
        }

        [Test]
        public void SwapBorrowers_InvalidLegacyApplicationId_ShouldThrow()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var secondAppId = this.CreateSecondApplication(hasCoborrower: true);

                var loan = this.CreateLoan();
                this.InitSaveLoan(loan);
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                Assert.Throws<CBaseException>(() => this.SwapBorrowers(aggregate, Guid.NewGuid()));
            }
        }

        [Test]
        public void SwapBorrowers_UladWithBorrowerAndCoborrower_BorrowerOrderingUnchanged()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var secondAppId = this.CreateSecondApplication(hasCoborrower: true);

                var loan = this.CreateLoan();
                this.InitSaveLoan(loan);
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var firstAppData = loanData.GetAppData(0);
                var firstAppId = firstAppData.aAppId;

                var secondAppData = loanData.GetAppData(secondAppId);

                var firstConsumerId = firstAppData.aBConsumerId;
                var secondConsumerId = secondAppData.aBConsumerId;
                var thirdConsumerId = secondAppData.aCConsumerId;

                var consumers = loan.Consumers.Keys;
                Assert.AreEqual(firstConsumerId, consumers.ElementAt(0).Value);
                Assert.AreEqual(secondConsumerId, consumers.ElementAt(1).Value);
                Assert.AreEqual(thirdConsumerId, consumers.ElementAt(2).Value);

                aggregate.SwapBorrowers(secondAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                this.SaveLoan(loan);

                loan = this.CreateLoan();
                this.InitLoadLoan(loan);
                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);

                consumers = loan.Consumers.Keys;
                Assert.AreEqual(firstConsumerId, consumers.ElementAt(0).Value);
                Assert.AreEqual(secondConsumerId, consumers.ElementAt(1).Value);
                Assert.AreEqual(thirdConsumerId, consumers.ElementAt(2).Value);
            }
        }

        [Test]
        public void SwapBorrowers_UladWithBorrowerAndCoborrower_CoborrowerBecomesPrimary()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var secondAppId = this.CreateSecondApplication(hasCoborrower: true);

                var loan = this.CreateLoan();
                this.InitSaveLoan(loan);
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var appData = loanData.GetAppData(secondAppId);
                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);

                var borrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == borrowerId).Value;
                var coborrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == coborrowerId).Value;
                Assert.IsTrue(borrowerAssoc.IsPrimary);
                Assert.IsFalse(coborrowerAssoc.IsPrimary);

                this.SwapBorrowers(aggregate, secondAppId);
                this.SaveLoan(loan);

                loan = this.CreateLoan();
                this.InitLoadLoan(loan);

                appData = loanData.GetAppData(secondAppId);
                var newBorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                var newCoborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);
                Assert.AreEqual(borrowerId, newCoborrowerId);
                Assert.AreEqual(coborrowerId, newBorrowerId);

                var newBorrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == newBorrowerId).Value;
                var newCoborrowerAssoc = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == newCoborrowerId).Value;
                Assert.IsTrue(newBorrowerAssoc.IsPrimary);
                Assert.IsFalse(newCoborrowerAssoc.IsPrimary);
            }
        }

        [Test]
        public void SwapBorrowers_NeitherBorrowerIsPrimary_PrimaryUnchanged()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                // Setup loan with one legacy/ulad app pair with two borrowers
                var loan = this.CreateLoan();
                this.InitSaveLoan(loan);

                Assert.AreEqual(1, loan.UladApplications.Count);
                Assert.AreEqual(1, loan.LegacyApplications.Count);
                Assert.AreEqual(1, loan.Consumers.Count);
                Assert.AreEqual(1, loan.UladApplicationConsumers.Count);

                var loanData = this.ExtractLoanData(loan);
                var firstAppData = loanData.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(firstAppData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                this.SaveLoan(loan);

                // Add a third borrower to the ULAD application and set it as the primary
                loan = this.CreateLoan();
                this.InitSaveLoan(loan);

                Assert.AreEqual(1, loan.UladApplications.Count);
                Assert.AreEqual(1, loan.LegacyApplications.Count);
                Assert.AreEqual(2, loan.Consumers.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var uladAppId = loanData.UladApplications.Single().Key;
                var secondBorrowerId = aggregate.AddConsumerToUladApplication(uladAppId); // should create a new (legacy app, borrower)
                this.SaveLoan(loan);

                loan = this.CreateLoan();
                this.InitSaveLoan(loan);

                Assert.AreEqual(1, loan.UladApplications.Count); // Notice that the same ULAD application holds all three borrowers
                Assert.AreEqual(2, loan.LegacyApplications.Count); // Notice that a legacy application was added
                Assert.AreEqual(3, loan.Consumers.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);
                aggregate.SetPrimaryBorrowerOnUladApplication(secondBorrowerId); // borrower on second legacy application is now the primary borrower for the ULAD application
                this.SaveLoan(loan);

                // Swap the first two borrowers
                loan = this.CreateLoan();
                this.InitSaveLoan(loan);

                Assert.AreEqual(1, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.LegacyApplications.Count);
                Assert.AreEqual(3, loan.Consumers.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);

                firstAppData = loanData.GetAppData(0);
                var firstAppId = firstAppData.aAppId;
                var firstBorrowerId = firstAppData.aBConsumerId;
                var firstCoborrowerId = firstAppData.aCConsumerId;

                var firstBorrowerAssoc = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId.Value == firstBorrowerId);
                var firstCoborrowerAssoc = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId.Value == firstCoborrowerId);
                var secondBorrowerAssoc = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId == secondBorrowerId);

                // These asserts define the IsPrimary for the ULAD application prior to swapping the borrowers.
                Assert.IsFalse(firstBorrowerAssoc.IsPrimary);
                Assert.IsFalse(firstCoborrowerAssoc.IsPrimary);
                Assert.IsTrue(secondBorrowerAssoc.IsPrimary);

                this.SwapBorrowers(aggregate, firstAppId);
                this.SaveLoan(loan);

                loan = this.CreateLoan();
                this.InitLoadLoan(loan);

                firstAppData = loanData.GetAppData(0);
                var secondAppData = loanData.GetAppData(1);

                Assert.AreEqual(firstBorrowerId, firstAppData.aCConsumerId); // previous coborrower is now borrower on first legacy application
                Assert.AreEqual(firstCoborrowerId, firstAppData.aBConsumerId); // previous borrower is now coborrower on first legacy application
                Assert.AreEqual(secondBorrowerId.Value, secondAppData.aBConsumerId); // borrower on second legacy application remains

                firstBorrowerAssoc = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId.Value == firstAppData.aBConsumerId);
                firstCoborrowerAssoc = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId.Value == firstAppData.aCConsumerId);
                secondBorrowerAssoc = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId.Value == secondAppData.aBConsumerId);

                // These asserts demonstrate that the IsPrimary for the ULAD application after swapping the borrowers remains the same.
                Assert.IsFalse(firstBorrowerAssoc.IsPrimary);
                Assert.IsFalse(firstCoborrowerAssoc.IsPrimary);
                Assert.IsTrue(secondBorrowerAssoc.IsPrimary);
            }
        }

        [Test]
        public void SwapBorrowers_CoborrowerNotDefined_MakesCoborrowerPrimaryBorrowerOnUladApp()
        {
            // When a legacy application has a single borrower but no defined coborrower
            // a swap will bring the coborrower into the primary borrower position and
            // thus make that borrower defined. In this case, we will need to associate the
            // borrower with a ULAD application, also making them the primary ULAD borrower.
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loan = this.CreateLoan();
                this.InitSaveLoan(loan);
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var coborrowerId = loan.GetAppData(0).aCConsumerId;
                Assert.IsFalse(loan.GetAppData(0).aHasCoborrower);
                Assert.AreEqual(1, loan.UladApplicationConsumers.Count);

                this.SwapBorrowers(aggregate, loan.GetAppData(0).aAppId);
                this.SaveLoan(loan);

                loan = this.CreateLoan();
                this.InitLoadLoan(loan);

                Assert.AreEqual(2, loan.UladApplicationConsumers.Count); // the number of borrowers has increased and association was added
                var association = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId.Value == coborrowerId); // association for the activated borrower (was inactive coborrower)
                Assert.AreEqual(loan.GetAppData(0).aAppId, association.LegacyAppId.Value); // association has correct legacy application
                Assert.AreEqual(loan.UladApplications.Single().Key, association.UladApplicationId); // association has correct ULAD application
                Assert.AreEqual(true, association.IsPrimary); // demonstrate that the primary has changed
            }
        }

        [Test]
        public void SwapBorrowers_CoborrowerNotDefinedBeforeSwap_MarksCoborrowerAsDefinedAfterSwap()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loan = this.CreateLoan();
                this.InitSaveLoan(loan);
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var appData = loan.GetAppData(0);
                Assert.IsFalse(appData.aHasCoborrower);

                this.SwapBorrowers(aggregate, appData.aAppId);
                this.SaveLoan(loan);

                loan = this.CreateLoan();
                this.InitLoadLoan(loan);

                Assert.IsTrue(loan.GetAppData(0).aHasCoborrower);
            }
        }

        [Test]
        public void RemoveLegacyApp_RemovedIsPrimaryApp_RemainingLegacyAppBecomesPrimary()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var expectedValues = new ExpectedValues
                {
                    InitialIdentifiers = new InitialIdentifiers(),
                    LegacyAppCount = 2,
                    UladAppCount = 2,
                    ConsumerCount = 2,
                };

                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> primaryAppBefore, primaryAppAfter;

                CPageData.AddBlankUladApplication(this.loanId.ToIdentifier<DataObjectKind.Loan>());

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(LegacyApplicationMgmtTest));
                loan.InitLoad();
                Assert.AreEqual(2, loan.LegacyApplications.Count());
                primaryAppBefore = loan.LegacyApplications.Keys.First();
                primaryAppAfter = loan.LegacyApplications.Keys.Skip(1).First();
                Assert.AreNotEqual(Guid.Empty, primaryAppBefore.Value);
                Assert.AreNotEqual(Guid.Empty, primaryAppAfter.Value);
                Assert.AreEqual(2, loan.UladApplications.Count());

                expectedValues.PrimaryLegacyApplicationIdentifier = primaryAppBefore.Value;
                this.CheckExpectations(expectedValues);

                CPageData.RemoveLegacyApplication(this.loanId.ToIdentifier<DataObjectKind.Loan>(), primaryAppBefore);
                expectedValues.LegacyAppCount -= 1;
                expectedValues.UladAppCount -= 1;
                expectedValues.ConsumerCount -= 1;
                expectedValues.PrimaryLegacyApplicationIdentifier = primaryAppAfter.Value;

                loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(LegacyApplicationMgmtTest));
                loan.InitLoad();
                Assert.AreEqual(1, loan.LegacyApplications.Count());
                Assert.IsTrue(loan.LegacyApplications.Single().Value.IsPrimary);
                Assert.AreEqual(primaryAppAfter, loan.LegacyApplications.Single().Key);
                Assert.AreEqual(1, loan.UladApplications.Count());

                this.CheckExpectations(expectedValues);
            }
        }

        [Test]
        public void RemoveLegacyApp_RemovedHasOneBorrower_UladAppIsRemoved()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var expectedValues = this.SetupForRemoveLegacyTests(hasCoborrower: false, isPrimary: true, additionalBorrowers: false);
                CheckExpectations(expectedValues);

                var loan = this.CreateLoan();
                InitSaveLoan(loan);

                var loanData = ExtractLoanData(loan);
                var aggregate = ExtractAggregate(loanData);

                this.RemoveLegacyApplication(aggregate, expectedValues.InitialIdentifiers.SecondAppId);
                expectedValues.ConsumerCount -= 1;
                expectedValues.LegacyAppCount -= 1;
                expectedValues.UladAppCount -= 1; // This is the critical item getting tested here

                CheckExpectations(expectedValues);
            }
        }

        [Test]
        public void RemoveLegacyApp_RemovedBorrowerIsNotPrimaryOnUladApp_UladAppAndItsPrimaryBorrowerRemain()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var expectedValues = this.SetupForRemoveLegacyTests(hasCoborrower: false, isPrimary: false, additionalBorrowers: true);
                this.CheckExpectations(expectedValues);

                var loan = this.CreateLoan();
                InitSaveLoan(loan);

                var loanData = ExtractLoanData(loan);
                var aggregate = ExtractAggregate(loanData);

                this.RemoveLegacyApplication(aggregate, expectedValues.InitialIdentifiers.SecondAppId);
                expectedValues.ConsumerCount -= 1;
                expectedValues.LegacyAppCount -= 1;
                // Note that the UladAppCount isn't modified

                CheckExpectations(expectedValues);

                var primaryConsumerId = aggregate.UladApplicationConsumers.Values.Single(a => a.LegacyAppId.Value == expectedValues.InitialIdentifiers.ThirdAppId && a.IsPrimary).ConsumerId;
                Assert.AreEqual(expectedValues.PrimaryConsumerOnUladApp, primaryConsumerId.Value);
            }
        }

        [Test]
        public void RemoveLegacyApp_RemovedBorrowerIsPrimaryOnUladAppWhenAdditionalsExist_UladAppRemainsButPrimaryReassigned()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var expectedValues = this.SetupForRemoveLegacyTests(hasCoborrower: false, isPrimary: true, additionalBorrowers: true);
                this.CheckExpectations(expectedValues);
                Assert.AreEqual(expectedValues.PrimaryConsumerOnUladApp, expectedValues.InitialIdentifiers.BorrowerOnSecondApp);

                var loan = this.CreateLoan();
                InitSaveLoan(loan);

                var loanData = ExtractLoanData(loan);
                var appData = loanData.GetAppData(expectedValues.InitialIdentifiers.SecondAppId);
                var aggregate = ExtractAggregate(loanData);
                Assert.AreEqual(expectedValues.PrimaryConsumerOnUladApp, appData.aBConsumerId);
                Assert.AreEqual(expectedValues.PrimaryConsumerOnUladApp, expectedValues.InitialIdentifiers.BorrowerOnSecondApp);

                this.RemoveLegacyApplication(aggregate, expectedValues.InitialIdentifiers.SecondAppId);
                expectedValues.ConsumerCount -= 1;
                expectedValues.LegacyAppCount -= 1;
                // Note that the UladAppCount isn't modified
                expectedValues.PrimaryConsumerOnUladApp = expectedValues.InitialIdentifiers.BorrowerOnThirdApp;

                CheckExpectations(expectedValues);

                var primaryConsumerId = aggregate.UladApplicationConsumers.Values.Single(a => a.LegacyAppId.Value == expectedValues.InitialIdentifiers.ThirdAppId && a.IsPrimary).ConsumerId;
                Assert.AreEqual(expectedValues.PrimaryConsumerOnUladApp, primaryConsumerId.Value);
            }
        }

        [Test]
        public void RemoveLegacyApp_RemovedAppIsSecondLegacyApp_ExpectedValuesAreCorrect()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var expectedValues = this.SetupForRemoveLegacyTests(hasCoborrower: true, isPrimary: true, additionalBorrowers: false);
                this.CheckExpectations(expectedValues);
                Assert.AreNotEqual(expectedValues.PrimaryLegacyApplicationIdentifier, expectedValues.InitialIdentifiers.SecondAppId);

                var loan = this.CreateLoan();
                InitSaveLoan(loan);

                var loanData = ExtractLoanData(loan);
                var aggregate = ExtractAggregate(loanData);

                this.RemoveLegacyApplication(aggregate, expectedValues.InitialIdentifiers.SecondAppId);
                expectedValues.ConsumerCount -= 2;
                expectedValues.LegacyAppCount -= 1;
                expectedValues.UladAppCount -= 1;

                CheckExpectations(expectedValues);
            }
        }

        [Test]
        public void RemoveLegacyApp_RemovedAppIsPrimaryLegacyApp_ExpectedValuesAreCorrect()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var expectedValues = this.SetupForRemoveLegacyTests(hasCoborrower: true, isPrimary: true, additionalBorrowers: false);
                this.CheckExpectations(expectedValues);
                Assert.AreNotEqual(expectedValues.PrimaryLegacyApplicationIdentifier, expectedValues.InitialIdentifiers.SecondAppId);

                var loan = this.CreateLoan();
                InitSaveLoan(loan);

                var loanData = ExtractLoanData(loan);
                var aggregate = ExtractAggregate(loanData);

                this.RemoveLegacyApplication(aggregate, expectedValues.PrimaryLegacyApplicationIdentifier);
                expectedValues.ConsumerCount -= 1;
                expectedValues.LegacyAppCount -= 1;
                expectedValues.UladAppCount -= 1;
                expectedValues.PrimaryLegacyApplicationIdentifier = expectedValues.InitialIdentifiers.SecondAppId;

                CheckExpectations(expectedValues, mainAppDeleted: false);
            }
        }

        private void CheckExpectations(ExpectedValues expectedValues, bool mainAppDeleted = true)
        {
            var loan = CreateLoan();
            InitLoadLoan(loan);

            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            // Check the expected counts match the real counts
            Assert.AreEqual(expectedValues.UladAppCount, aggregate.UladApplications.Count);
            Assert.AreEqual(expectedValues.LegacyAppCount, loan.LegacyApplications.Count);
            Assert.AreEqual(expectedValues.ConsumerCount, loan.Consumers.Count);

            // Check that every ULAD application has a single primary consumer
            foreach (var uladAppId in aggregate.UladApplications.Keys)
            {
                Assert.AreEqual(1, aggregate.UladApplicationConsumers.Count(a => a.Value.UladApplicationId == uladAppId && (a.Value.IsPrimary)));
            }

            // Check there is one legacy application marked as primary
            Assert.AreEqual(1, loan.LegacyApplications.Count(a => a.Value.IsPrimary));

            // Check there is one ULAD application marked as primary
            Assert.AreEqual(1, aggregate.UladApplications.Count(a => a.Value.IsPrimary));

            if (expectedValues.PrimaryLegacyApplicationIdentifier != Guid.Empty)
            {
                // Check the identifier for the primary legacy application is correct
                Assert.AreEqual(expectedValues.PrimaryLegacyApplicationIdentifier, loan.LegacyApplications.Single(a => a.Value.IsPrimary).Key.Value);
            }

            if (expectedValues.UladAppCount == 2 && expectedValues.InitialIdentifiers.SecondUladAppId != Guid.Empty)
            {
                // Check the primary consumer on the ULAD application
                Assert.AreEqual(expectedValues.PrimaryConsumerOnUladApp, loan.UladApplicationConsumers.Values.Single(a => a.UladApplicationId.Value == expectedValues.InitialIdentifiers.SecondUladAppId && a.IsPrimary).ConsumerId.Value);
            }
        }

        /// <summary>
        /// Setup a main legacy application (which will get deleted), other applications with consumers, and associated ULAD applications.
        /// </summary>
        /// <param name="hasCoborrower">True if the main legacy application has a coborrower.</param>
        /// <param name="isPrimary">True if the primary for the ULAD app is the borrower for the main app, false means the additional borrower is the primary.</param>
        /// <param name="additionalBorrowers">If true, the ULAD application will have the primary as the additional borrower.</param>
        private ExpectedValues SetupForRemoveLegacyTests(bool hasCoborrower, bool isPrimary, bool additionalBorrowers)
        {
            if (!isPrimary && !additionalBorrowers)
            {
                throw new ArgumentException("When no additional borrowers, the borrower on the main legacy application must be the primary");
            }

            var expected = new ExpectedValues();
            expected.InitialIdentifiers = new InitialIdentifiers();

            expected.LegacyAppCount = 1;
            expected.UladAppCount = 1;
            expected.ConsumerCount = 1;

            var loan = this.CreateLoan();
            this.InitSaveLoan(loan);

            expected.PrimaryLegacyApplicationIdentifier = loan.LegacyApplications.Single(a => a.Value.IsPrimary).Key.Value;

            // Add main legacy and ulad apps
            expected.InitialIdentifiers.SecondAppId = LoanTestUtilities.AddNewLegacyAndUladApplicationToLoan(this.loanId, hasCoborrower);
            ++expected.LegacyAppCount;
            ++expected.UladAppCount;
            expected.ConsumerCount += hasCoborrower ? 2 : 1;
            this.SaveLoan(loan);

            loan = this.CreateLoan();
            this.InitLoadLoan(loan);
            var loanData = ExtractLoanData(loan);
            var appData = loanData.GetAppData(expected.InitialIdentifiers.SecondAppId);

            expected.InitialIdentifiers.SecondUladAppId = loan.UladApplicationConsumers.Values.First(a => a.LegacyAppId.Value == expected.InitialIdentifiers.SecondAppId).UladApplicationId.Value;
            expected.PrimaryConsumerOnUladApp = appData.aBConsumerId;
            expected.InitialIdentifiers.BorrowerOnSecondApp = appData.aBConsumerId;
            if (hasCoborrower)
            {
                expected.InitialIdentifiers.CoborrowerOnSecondApp = appData.aCConsumerId;
            }

            if (additionalBorrowers)
            {
                var aggregate = ExtractAggregate(loanData);

                var uladAppId = expected.InitialIdentifiers.SecondUladAppId.ToIdentifier<DataObjectKind.UladApplication>();
                expected.InitialIdentifiers.ThirdAppId = LoanTestUtilities.AddNewLegacyApplicationToExistingUladApplication(this.loanId, uladAppId, hasCoborrower: true);
                ++expected.LegacyAppCount;
                expected.ConsumerCount += 2;

                loan = this.CreateLoan();
                this.InitSaveLoan(loan);
                loanData = ExtractLoanData(loan);
                appData = loanData.GetAppData(expected.InitialIdentifiers.ThirdAppId);

                expected.InitialIdentifiers.BorrowerOnThirdApp = appData.aBConsumerId;
                expected.InitialIdentifiers.CoborrowerOnThirdApp = appData.aCConsumerId;

                if (!isPrimary)
                {
                    expected.PrimaryConsumerOnUladApp = appData.aBConsumerId;

                    var borrowerIdentifier = appData.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    aggregate = ExtractAggregate(loanData);
                    aggregate.SetPrimaryBorrowerOnUladApplication(borrowerIdentifier);

                    this.SaveLoan(loan);
                }
            }

            return expected;
        }

        /// <summary>
        /// Gathers together expected values that can be used in asserts.  Most of the checks are the same so we can handle them all together.
        /// </summary>
        /// <remarks>
        /// What the initial loan is created, there is one borrower assigned to one legacy and ULAD application.  In the setup method, another
        /// legacy and ULAD application get created.  This second legacy application is usually the
        /// target of various operations.  It may or may not have a coborrower.  The second ULAD application gets the borrower(s)
        /// of the second legacy application.  If there are additional borrowers, they will be created as the borrower and coborrower of a third
        /// legacy application.  These borrowers get associated with the second
        /// ULAD application.
        /// </remarks>
        private class ExpectedValues
        {
            /// <summary>
            /// The identifiers as initially setup.
            /// </summary>
            public InitialIdentifiers InitialIdentifiers;

            /// <summary>
            /// Expected count of the legacy applications.
            /// </summary>
            public int LegacyAppCount;

            /// <summary>
            /// Expected count of the ulad applications.
            /// </summary>
            public int UladAppCount;

            /// <summary>
            /// Expected count of the consumers.
            /// </summary>
            public int ConsumerCount;

            /// <summary>
            /// Identifier for the Legacy Application identified as the primary.
            /// </summary>
            public Guid PrimaryLegacyApplicationIdentifier;

            /// <summary>
            /// Identifier for the consumer that is the primary for the ULAD app.
            /// </summary>
            public Guid PrimaryConsumerOnUladApp;
        }

        /// <summary>
        /// The identifiers that are setup initially.
        /// </summary>
        private class InitialIdentifiers
        {
            /// <summary>
            /// Identifier for the second legacy application, which is the one that is usually going to be removed.  It holds consumers
            /// which can be the primary borrowers for ULAD applications.
            /// </summary>
            public Guid SecondAppId;

            /// <summary>
            /// Identifier for the second ULAD application, which is associated with the borrowers from both the second and third legacy applications.
            /// </summary>
            public Guid SecondUladAppId;

            /// <summary>
            /// Identifier for a legacy application that can hold additional, non-primary borrowers for ULAD applications.
            /// </summary>
            public Guid ThirdAppId;

            /// <summary>
            /// Identifier for the borrower on the second legacy application.
            /// </summary>
            public Guid BorrowerOnSecondApp;

            /// <summary>
            /// Identifier for the coborrower on the second legacy application, or Guid.Empty.
            /// </summary>
            public Guid CoborrowerOnSecondApp;

            /// <summary>
            /// Identifier for the borrower on the third legacy application, or Guid.Empty.
            /// </summary>
            public Guid BorrowerOnThirdApp;

            /// <summary>
            /// Identifier for the coborrower on the third legacy application, or Guid.Empty.
            /// </summary>
            public Guid CoborrowerOnThirdApp;
        }

        private void SwapBorrowers(ILoanLqbCollectionContainer aggregate, Guid appId)
        {
            aggregate.SwapBorrowers(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appId));
        }

        private void RemoveLegacyApplication(ILoanLqbCollectionContainer aggregate, Guid legacyAppId)
        {
            aggregate.RemoveLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(legacyAppId));
        }

        private Guid CreateSecondApplication(bool hasCoborrower)
        {
            var loan = this.CreateLoan();
            this.InitSaveLoan(loan);
            loan.sSyncUladAndLegacyApps = true;
            this.SaveLoan(loan);

            var appId = this.AddAppToLoan(hasCoborrower);

            loan = this.CreateLoan();
            this.InitSaveLoan(loan);
            loan.sSyncUladAndLegacyApps = false;
            this.SaveLoan(loan);

            return appId;
        }

        private CPageData CreateLoan()
        {
            return new CPageData(this.loanId, nameof(LegacyApplicationMgmtTest), new string[] { "sSyncUladAndLegacyApps", "afDelMarriedCobor", "afSwapMarriedBorAndCobor", "UladApplications", "UladApplicationConsumers" });
        }

        private void InitLoadLoan(CPageData loan)
        {
            loan.InitLoad();
        }

        private void InitSaveLoan(CPageData loan)
        {
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        private void SaveLoan(CPageData loan)
        {
            loan.Save();
        }

        private Guid AddAppToLoan(bool hasCoborrower)
        {
            var loan = this.CreateLoan();
            int index = loan.AddNewApp();

            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var appData = loan.GetAppData(index);
            if (hasCoborrower)
            {
                loan.AddCoborrowerToLegacyApplication(appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
            }
            else
            {
                loan.RemoveCoborrowerFromLegacyApplication(appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
            }
            Guid appId = appData.aAppId;
            loan.Save();

            return appId;
        }

        private CPageBase ExtractLoanData(CPageData pageData)
        {
            return CPageBaseExtractor.Extract(pageData);
        }

        private ILoanLqbCollectionContainer ExtractAggregate(CPageBase pageBase)
        {
            return pageBase.loanLqbCollectionContainer;
        }
    }
}