﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Test.Developers.Utils;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class BorrowerManagementShimTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        public void CreateNewLoan(E_sLqbCollectionT borrowerApplicationCollectionMode, bool addSecondApp)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                if (borrowerApplicationCollectionMode == E_sLqbCollectionT.Legacy)
                {
                    this.loanId = loanCreator.CreateBlankLoanFile();
                }
                else
                {
                    this.loanId = loanCreator.CreateBlankUladLoanFile();
                }

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(BorrowerManagementShimTest));

                if (addSecondApp)
                {
                    loan.AddNewApp();
                }

                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.sBorrowerApplicationCollectionT = borrowerApplicationCollectionMode;
                loan.Save();
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void AddNewApp_Always_AddsLegacyApplication(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.CreateNewLoan(borrowerApplicationCollectionMode, addSecondApp: false);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(BorrowerManagementShimTest));

                loan.AddNewApp();
                loan.InitLoad(); 

                Assert.AreEqual(2, loan.nApps);
                if (borrowerApplicationCollectionMode != E_sLqbCollectionT.Legacy)
                {
                    Assert.AreEqual(2, loan.UladApplications.Count);
                }
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void DelApp_Always_RemovesApp(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.CreateNewLoan(borrowerApplicationCollectionMode, addSecondApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(BorrowerManagementShimTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.DelApp(1);

                // This is needed to re-load the old apps. Without it they are still in the apps array.
                loan.InitLoad(); 
                Assert.AreEqual(1, loan.nApps);
                if (borrowerApplicationCollectionMode != E_sLqbCollectionT.Legacy)
                {
                    Assert.AreEqual(1, loan.UladApplications.Count);
                }
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void aIsPrimary_SetToTrueForNonPrimaryApplication_SetsAppAsPrimary(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.CreateNewLoan(borrowerApplicationCollectionMode, addSecondApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(BorrowerManagementShimTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var appId1 = loan.GetAppData(0).aAppId;
                var appId2 = loan.GetAppData(1).aAppId;
                // Sanity checks.
                Assert.AreEqual(true, loan.GetAppData(appId1).aIsPrimary);
                Assert.AreEqual(false, loan.GetAppData(appId2).aIsPrimary);

                loan.GetAppData(appId2).aIsPrimary = true;

                Assert.AreEqual(false, loan.GetAppData(appId1).aIsPrimary);
                Assert.AreEqual(true, loan.GetAppData(appId2).aIsPrimary);
                loan.Save();
                loan.InitLoad();
                Assert.AreEqual(false, loan.GetAppData(appId1).aIsPrimary);
                Assert.AreEqual(true, loan.GetAppData(appId2).aIsPrimary);
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void aIsPrimary_SetToFalseForPrimaryApplication_SetsNextAppInOrderAsPrimary(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.CreateNewLoan(borrowerApplicationCollectionMode, addSecondApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(BorrowerManagementShimTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var appId1 = loan.GetAppData(0).aAppId;
                var appId2 = loan.GetAppData(1).aAppId;
                // Sanity checks.
                Assert.AreEqual(true, loan.GetAppData(appId1).aIsPrimary);
                Assert.AreEqual(false, loan.GetAppData(appId2).aIsPrimary);

                loan.GetAppData(appId1).aIsPrimary = false;

                Assert.AreEqual(false, loan.GetAppData(appId1).aIsPrimary);
                Assert.AreEqual(true, loan.GetAppData(appId2).aIsPrimary);
                loan.Save();
                loan.InitLoad();
                Assert.AreEqual(false, loan.GetAppData(appId1).aIsPrimary);
                Assert.AreEqual(true, loan.GetAppData(appId2).aIsPrimary);
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                if (this.loanId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(this.principal, this.loanId, false, false);
                }
            }
        }
    }
}
