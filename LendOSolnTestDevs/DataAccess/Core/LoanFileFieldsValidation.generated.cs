﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data;
    using global::DataAccess;
    using global::LendersOffice.Constants;
    using global::LendersOffice.Security;
    using global::LendingQB.Core.Data;
    using global::LqbGrammar.DataTypes;
    using NUnit.Framework;
    using NSubstitute;
    using Utils;
    using global::LendingQB.Core.Commands;
    using LqbGrammar.DataTypes.PathDispatch;
    using System.Linq;

    public sealed partial class LoanFileFieldsValidation
    {
        [Test]
        public void Save_AfterAddingRecordToLoanLevelAssets_CallsUpdateCollectionWithExtraRow()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.Assets) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CAssetCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Assets), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CAssetCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [Test]
        public void Save_AfterModifyingLoanLevelAssets_CallsUpdateCollectionWithUpdatedDataSet()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelAsset(addForEachApp: false);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.Assets) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CAssetCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var asset = pageData.Assets.Values.First();

                asset.AccountName = DescriptionField.Create("Asset Account Name");
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CAssetCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("Asset Account Name", originalRow["AccNm"].ToString());
                Assert.AreEqual("Asset Account Name", updatedRow["AccNm"].ToString());
            }
        }

        [Test]
        public void Save_AfterAddingAssetOnMultiAppFile_CallsUpdateCollectionWithAllAssets()
        {
            this.AddNewApp();
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelAsset(addForEachApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.Assets) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CAssetCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Assets), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CAssetCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(2, original.Tables[0].Rows.Count);
                Assert.AreEqual(3, updated.Tables[0].Rows.Count);
            }
        }

        private void AddLoanLevelAsset(bool addForEachApp)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.InitSave();

                if (addForEachApp)
                {
                    foreach (var app in loan.Apps)
                    {
                        var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Assets), app.aBConsumerId, null, null, null);
                        loan.HandleAdd(addCommand, null);
                    }
                }
                else
                {
                    var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Assets), loan.GetAppData(0).aBConsumerId, null, null, null);
                    loan.HandleAdd(addCommand, null);
                }

                loan.Save();
            }
        }

        [Test]
        public void Save_AfterAddingRecordToLoanLevelEmploymentRecords_CallsUpdateCollectionWithExtraRow()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.EmploymentRecords) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CEmpCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.EmploymentRecords), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CEmpCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [Test]
        public void Save_AfterModifyingLoanLevelEmploymentRecords_CallsUpdateCollectionWithUpdatedDataSet()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelEmploymentRecord(addForEachApp: false);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.EmploymentRecords) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CEmpCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var employmentRecord = pageData.EmploymentRecords.Values.First();

                employmentRecord.JobTitle = DescriptionField.Create("Employment Record Job Title");
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CEmpCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("Employment Record Job Title", originalRow["JobTitle"].ToString());
                Assert.AreEqual("Employment Record Job Title", updatedRow["JobTitle"].ToString());
            }
        }

        [Test]
        public void Save_AfterAddingEmploymentRecordOnMultiAppFile_CallsUpdateCollectionWithAllEmploymentRecords()
        {
            this.AddNewApp();
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelEmploymentRecord(addForEachApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.EmploymentRecords) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CEmpCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.EmploymentRecords), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CEmpCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(2, original.Tables[0].Rows.Count);
                Assert.AreEqual(3, updated.Tables[0].Rows.Count);
            }
        }

        private void AddLoanLevelEmploymentRecord(bool addForEachApp)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.InitSave();

                if (addForEachApp)
                {
                    foreach (var app in loan.Apps)
                    {
                        var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.EmploymentRecords), app.aBConsumerId, null, null, null);
                        loan.HandleAdd(addCommand, null);
                    }
                }
                else
                {
                    var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.EmploymentRecords), loan.GetAppData(0).aBConsumerId, null, null, null);
                    loan.HandleAdd(addCommand, null);
                }

                loan.Save();
            }
        }

        [Test]
        public void Save_AfterAddingRecordToLoanLevelLiabilities_CallsUpdateCollectionWithExtraRow()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.Liabilities) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CLiaCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Liabilities), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CLiaCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [Test]
        public void Save_AfterModifyingLoanLevelLiabilities_CallsUpdateCollectionWithUpdatedDataSet()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelLiability(addForEachApp: false);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.Liabilities) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CLiaCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var liability = pageData.Liabilities.Values.First();

                liability.AccountName = DescriptionField.Create("Liability Account Name");
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CLiaCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("Liability Account Name", originalRow["AccNm"].ToString());
                Assert.AreEqual("Liability Account Name", updatedRow["AccNm"].ToString());
            }
        }

        [Test]
        public void Save_AfterAddingLiabilityOnMultiAppFile_CallsUpdateCollectionWithAllLiabilities()
        {
            this.AddNewApp();
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelLiability(addForEachApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.Liabilities) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CLiaCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Liabilities), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CLiaCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(2, original.Tables[0].Rows.Count);
                Assert.AreEqual(3, updated.Tables[0].Rows.Count);
            }
        }

        private void AddLoanLevelLiability(bool addForEachApp)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.InitSave();

                if (addForEachApp)
                {
                    foreach (var app in loan.Apps)
                    {
                        var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Liabilities), app.aBConsumerId, null, null, null);
                        loan.HandleAdd(addCommand, null);
                    }
                }
                else
                {
                    var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Liabilities), loan.GetAppData(0).aBConsumerId, null, null, null);
                    loan.HandleAdd(addCommand, null);
                }

                loan.Save();
            }
        }

        [Test]
        public void Save_AfterAddingRecordToLoanLevelRealProperties_CallsUpdateCollectionWithExtraRow()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.RealProperties) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CReCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.RealProperties), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CReCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [Test]
        public void Save_AfterModifyingLoanLevelRealProperties_CallsUpdateCollectionWithUpdatedDataSet()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelRealProperty(addForEachApp: false);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.RealProperties) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CReCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var realProperty = pageData.RealProperties.Values.First();

                realProperty.StreetAddress = StreetAddress.Create("123 Test Dr.");
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CReCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("123 Test Dr.", originalRow["Addr"].ToString());
                Assert.AreEqual("123 Test Dr.", updatedRow["Addr"].ToString());
            }
        }

        [Test]
        public void Save_AfterAddingRealPropertyOnMultiAppFile_CallsUpdateCollectionWithAllRealProperties()
        {
            this.AddNewApp();
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelRealProperty(addForEachApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.RealProperties) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CReCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.RealProperties), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CReCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(2, original.Tables[0].Rows.Count);
                Assert.AreEqual(3, updated.Tables[0].Rows.Count);
            }
        }

        private void AddLoanLevelRealProperty(bool addForEachApp)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.InitSave();

                if (addForEachApp)
                {
                    foreach (var app in loan.Apps)
                    {
                        var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.RealProperties), app.aBConsumerId, null, null, null);
                        loan.HandleAdd(addCommand, null);
                    }
                }
                else
                {
                    var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.RealProperties), loan.GetAppData(0).aBConsumerId, null, null, null);
                    loan.HandleAdd(addCommand, null);
                }

                loan.Save();
            }
        }

        [Test]
        public void Save_AfterAddingRecordToLoanLevelVaPreviousLoans_CallsUpdateCollectionWithExtraRow()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.VaPreviousLoans) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CVaPastLCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VaPreviousLoans), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CVaPastLCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [Test]
        public void Save_AfterModifyingLoanLevelVaPreviousLoans_CallsUpdateCollectionWithUpdatedDataSet()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelVaPreviousLoan(addForEachApp: false);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.VaPreviousLoans) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CVaPastLCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var vaPreviousLoan = pageData.VaPreviousLoans.Values.First();

                vaPreviousLoan.LoanTypeDesc = DescriptionField.Create("VA Prev Loan Loan Type Desc");
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CVaPastLCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("VA Prev Loan Loan Type Desc", originalRow["LoanTypeDesc"].ToString());
                Assert.AreEqual("VA Prev Loan Loan Type Desc", updatedRow["LoanTypeDesc"].ToString());
            }
        }

        [Test]
        public void Save_AfterAddingVaPreviousLoanOnMultiAppFile_CallsUpdateCollectionWithAllVaPreviousLoans()
        {
            this.AddNewApp();
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelVaPreviousLoan(addForEachApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.VaPreviousLoans) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CVaPastLCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VaPreviousLoans), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CVaPastLCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(2, original.Tables[0].Rows.Count);
                Assert.AreEqual(3, updated.Tables[0].Rows.Count);
            }
        }

        private void AddLoanLevelVaPreviousLoan(bool addForEachApp)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.InitSave();

                if (addForEachApp)
                {
                    foreach (var app in loan.Apps)
                    {
                        var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VaPreviousLoans), app.aBConsumerId, null, null, null);
                        loan.HandleAdd(addCommand, null);
                    }
                }
                else
                {
                    var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VaPreviousLoans), loan.GetAppData(0).aBConsumerId, null, null, null);
                    loan.HandleAdd(addCommand, null);
                }

                loan.Save();
            }
        }

        [Test]
        public void Save_AfterAddingRecordToLoanLevelVorRecords_CallsUpdateCollectionWithExtraRow()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.VorRecords) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CVorFields", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VorRecords), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CVorFields", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [Test]
        public void Save_AfterModifyingLoanLevelVorRecords_CallsUpdateCollectionWithUpdatedDataSet()
        {
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelVorRecord(addForEachApp: false);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.VorRecords) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CVorFields", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var vorRecord = pageData.VorRecords.Values.First();

                vorRecord.AccountName = DescriptionField.Create("VA Prev Loan Account Name");
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CVorFields", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("VA Prev Loan Account Name", originalRow["AccountName"].ToString());
                Assert.AreEqual("VA Prev Loan Account Name", updatedRow["AccountName"].ToString());
            }
        }

        [Test]
        public void Save_AfterAddingVorRecordOnMultiAppFile_CallsUpdateCollectionWithAllVorRecords()
        {
            this.AddNewApp();
            this.SetBorrowerApplicationCollectionMode(E_sLqbCollectionT.UseLqbCollections);
            this.AddLoanLevelVorRecord(addForEachApp: true);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.VorRecords) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CVorFields", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var primaryBorrowerId = pageData.GetAppData(0).aBConsumerId;

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VorRecords), primaryBorrowerId, null, null, null);
                pageData.HandleAdd(addCommand, null);
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CVorFields", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(2, original.Tables[0].Rows.Count);
                Assert.AreEqual(3, updated.Tables[0].Rows.Count);
            }
        }

        private void AddLoanLevelVorRecord(bool addForEachApp)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.InitSave();

                if (addForEachApp)
                {
                    foreach (var app in loan.Apps)
                    {
                        var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VorRecords), app.aBConsumerId, null, null, null);
                        loan.HandleAdd(addCommand, null);
                    }
                }
                else
                {
                    var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VorRecords), loan.GetAppData(0).aBConsumerId, null, null, null);
                    loan.HandleAdd(addCommand, null);
                }

                loan.Save();
            }
        }

    }
}
