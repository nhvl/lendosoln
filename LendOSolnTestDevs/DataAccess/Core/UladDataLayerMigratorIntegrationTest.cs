﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Test.Developers.FOOL;
    using global::LendingQB.Test.Developers.Utils;
    using LendersOffice.Constants;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class UladDataLayerMigratorIntegrationTest
    {
        [Test]
        public void Migrate_LiabilityLinkedToReo_CreatesCorrectAssociations()
        {
            var liabilityId = new Guid("11111111-1111-1111-1111-111111111111");
            var reoId = new Guid("22222222-2222-2222-2222-222222222222");
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                var app = loan.GetAppData(0);
                var liability = app.aLiaCollection.EnsureRegularRecordOf(liabilityId) as ILiabilityRegular;
                var realProperty = app.aReCollection.EnsureRegularRecordOf(reoId) as IRealEstateOwned;
                liability.DebtT = E_DebtRegularT.Mortgage;
                liability.MatchedReRecordId = reoId;
                
                loan.MigrateToUseLqbCollections();
                
                Assert.AreEqual(1, loan.RealPropertyLiabilities.Count);
                var association = loan.RealPropertyLiabilities.Values.Single();
                Assert.AreEqual(loan.RealProperties.Single().Key, association.RealPropertyId);
                Assert.AreEqual(loan.Liabilities.Single().Key, association.LiabilityId);
            }
        }

        [Test]
        public void MigrateAndSave_SingleAsset_PersistsChanges()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                var app = loan.GetAppData(0);
                app.aAssetCollection.AddRegularRecord();
                
                loan.MigrateToUseLqbCollections();
                loan.Save();

                loan.InitLoad();
                Assert.AreEqual(E_sLqbCollectionT.UseLqbCollections, loan.sBorrowerApplicationCollectionT);
                Assert.AreEqual(1, loan.Assets.Count);
                Assert.AreEqual(1, loan.ConsumerAssets.Count);
            }
        }

        [Test]
        public void Migrate_BlankFile_DoesNotSetFileToUseNewIncome()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();

                loan.MigrateToUseLqbCollections();

                Assert.IsFalse(loan.sIsIncomeCollectionEnabled);
            }
        }

        [Test]
        public void Migrate_NonEmptyCoborrowerEmploymentButNoCoborrower_Throws()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                var app = loan.GetAppData(0);
                var record = app.aCEmpCollection.AddRegularRecord();
                record.EmplrNm = "James Bond";

                Assert.That(app, Has.Property(nameof(CAppData.aHasCoborrower)).EqualTo(false));
                Assert.That(() => loan.MigrateToUseLqbCollections(), Throws.InstanceOf<Exception>());

                // Now, to show that it was only the lack of a co-borrower that led to the issue:
                app.aHasCoborrowerData = true;
                Assert.That(app, Has.Property(nameof(CAppData.aHasCoborrower)).EqualTo(true));
                Assert.That(loan.EmploymentRecords, Is.Null.Or.Empty);
                Assert.That(() => loan.MigrateToUseLqbCollections(), Throws.Nothing);

                Assert.That(loan.EmploymentRecords, Has.Count.EqualTo(1));
            }
        }

        [Test]
        public void Migrate_EmptyCoborrowerEmploymentWithoutCoborrower_DoesNotThrow()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                var app = loan.GetAppData(0);
                app.aCEmpCollection.AddRegularRecord();

                Assert.IsFalse(app.aHasCoborrower);
                Assert.DoesNotThrow(() => loan.MigrateToUseLqbCollections());
                Assert.AreEqual(0, loan.EmploymentRecords.Count);
            }
        }

        [Test]
        public void Migrate_EmptyCoborrPrimaryEmploymentWithoutCoborrower_DoesNotThrow()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                var app = loan.GetAppData(0);
                app.aCEmpCollection.GetPrimaryEmp(true);
                loan.Save();
                loan.InitSave();

                Assert.IsFalse(app.aHasCoborrower);
                Assert.DoesNotThrow(() => loan.MigrateToUseLqbCollections());
                Assert.AreEqual(0, loan.EmploymentRecords.Count);
            }
        }

        [Test]
        public void MigrateVorRecords_NonTestFileWithUladStageBitDisabled_Throws()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.UladLoan))
            {
                try
                {
                    var loan = testLoan.CreateNewPageDataWithBypass();
                    loan.InitSave();
                    var fakeConfig = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: true);
                    testLoan.Helper.RegisterMockDriverFactory(fakeConfig);

                    Assert.Throws<CBaseException>(() => loan.MigrateVorRecordsToUladDataLayer());
                }
                finally
                {
                    testLoan.Helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);
                }
            }
        }

        [Test]
        public void MigrateVorRecords_TestFileWithUladStageBitDisabled_DoesNotThrow()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                try
                {
                    var loan = testLoan.CreateNewPageDataWithBypass();
                    loan.InitSave();
                    loan.MigrateToUseLqbCollections();
                    var fakeConfig = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: true);
                    testLoan.Helper.RegisterMockDriverFactory(fakeConfig);

                    loan.MigrateVorRecordsToUladDataLayer();
                }
                finally
                {
                    testLoan.Helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);
                }
            }
        }

        [Test]
        public void MigrateVorRecords_LegacyFile_Throws()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.RegularLoan))
            {
                try
                {
                    var loan = testLoan.CreateNewPageDataWithBypass();
                    loan.InitSave();
                    var fakeConfig = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: false);
                    testLoan.Helper.RegisterMockDriverFactory(fakeConfig);

                    Assert.Throws<CBaseException>(() => loan.MigrateVorRecordsToUladDataLayer());
                }
                finally
                {
                    testLoan.Helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);
                }
            }
        }

        [Test]
        public void MigrateVorRecords_LqbCollectionFileWithLegacyVorRecords_AddsVorRecords()
        {
            const string VorXml = @"
<VorXmlTable>
  <VorXmlContent>
    <RecordId>789aeb01-7d1c-4697-9e49-4450fc242f6b</RecordId>
    <LandlordCreditorName>VIP</LandlordCreditorName>
    <AddressFor>123 Main St</AddressFor>
    <CityFor>Brooklyn</CityFor>
    <StateFor>NY</StateFor>
    <ZipFor>11223</ZipFor>
    <AddressTo>123 State St</AddressTo>
    <CityTo>Santa Barbara</CityTo>
    <StateTo>CA</StateTo>
    <ZipTo>93107</ZipTo>
    <AccountName>ACC</AccountName>
    <VerifSentD />
    <VerifRecvD />
    <VerifExpD />
    <VerifReorderedD />
    <VerifT>1</VerifT>
    <Attention>IMPORTANT DEPT</Attention>
    <IsSeeAttachment>False</IsSeeAttachment>
  </VorXmlContent>
</VorXmlTable>";

            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.UladLoan))
            {
                try
                {
                    this.SetVorRecordXmlContent(testLoan.Principal.BrokerId, testLoan.Id, VorXml);
                    var loan = testLoan.CreateNewPageDataWithBypass();
                    loan.InitSave();
                    Assert.AreEqual(0, loan.VorRecords.Count);
                    Assert.IsFalse(string.IsNullOrEmpty(loan.GetAppData(0).aVorXmlContent));
                    var fakeConfig = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: false);
                    testLoan.Helper.RegisterMockDriverFactory(fakeConfig);

                    loan.MigrateVorRecordsToUladDataLayer();

                    Assert.AreEqual(1, loan.VorRecords.Count);
                    Assert.AreNotEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
                }
                finally
                {
                    testLoan.Helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);
                }
            }
        }

        [Test]
        public void MigrateVorRecords_LqbCollectionFileWithNewVorRecords_Throws()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.UladLoan))
            {
                try
                {
                    var loan = testLoan.CreateNewPageDataWithBypass();
                    loan.InitSave();
                    loan.AddVorRecord(loan.Consumers.Keys.First(), new global::LendingQB.Core.Data.VorRecord());
                    Assert.AreEqual(1, loan.VorRecords.Count);
                    var fakeConfig = this.GetFakeStageConfig(disableBorrowerApplicationCollectionTUpdates: false);
                    testLoan.Helper.RegisterMockDriverFactory(fakeConfig);

                    Assert.Throws<CBaseException>(() => loan.MigrateVorRecordsToUladDataLayer());
                }
                finally
                {
                    testLoan.Helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);
                }
            }
        }

        private void SetVorRecordXmlContent(Guid brokerId, Guid loanId, string xml)
        {
            var sql = @"
UPDATE APPLICATION_B
SET aVorXmlContent = @Xml
WHERE sLId = @LoanId";
            var parameters = new[]
            {
                new SqlParameter("Xml", xml),
                new SqlParameter("LoanId", loanId)
            };

            DBUpdateUtility.Update(brokerId, sql, null, parameters);
        }

        private IConfigurationQueryFactory GetFakeStageConfig(bool disableBorrowerApplicationCollectionTUpdates)
        {
            var fakeValues = new Dictionary<string, Tuple<int, string>>()
            {
                ["DisableBorrowerApplicationCollectionTUpdates"] = Tuple.Create(disableBorrowerApplicationCollectionTUpdates ? 1 : 0, "")
            };
            var fakeStageConfig = new FakeStageConfigurationQuery(fakeValues);
            var fakeConfigFactory = new FakeConfigurationQueryFactory(Substitute.For<ISiteConfigurationQuery>(), fakeStageConfig);
            return fakeConfigFactory;
        }
    }
}
