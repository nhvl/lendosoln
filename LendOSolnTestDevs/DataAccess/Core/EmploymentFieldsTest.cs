﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Test.Developers.Utils;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class EmploymentFieldsTest
    {
        LoanForIntegrationTest testFile;

        private static readonly object[] BaseRecordFieldSetters = new object[]
        {
            new object[] { "EmployeeIdVoe", (Action<IEmploymentRecord>)(e => e.EmployeeIdVoe = "Emp123") },
            new object[] { "EmployerCodeVoe", (Action<IEmploymentRecord>)(e => e.EmployerCodeVoe = "CODE_RED") },
            new object[] { "EmplrNm", (Action<IEmploymentRecord>)(e => e.EmplrNm = "MeridianLink")},
            new object[] { "IsSelfEmplmt", (Action<IEmploymentRecord>)(e => e.IsSelfEmplmt = true)},
            new object[] { "EmplrAddr", (Action<IEmploymentRecord>)(e => e.EmplrAddr = "1600 Sunflower")},
            new object[] { "EmplrCity", (Action<IEmploymentRecord>)(e => e.EmplrCity = "Costa Mesa")},
            new object[] { "EmplrZip", (Action<IEmploymentRecord>)(e => e.EmplrZip = "92626")},
            new object[] { "EmplrBusPhone", (Action<IEmploymentRecord>)(e => e.EmplrBusPhone = "800-123-4567")},
            new object[] { "EmplrFax", (Action<IEmploymentRecord>)(e => e.EmplrFax = "800-789-1234")},
            new object[] { "JobTitle", (Action<IEmploymentRecord>)(e => e.JobTitle = "Tester of All The Things")},
            new object[] { "VerifReorderedD", (Action<IEmploymentRecord>)(e => e.VerifReorderedD = CDateTime.Create(new DateTime(2018, 12, 20)))},
            new object[] { "VerifReorderedD_rep", (Action<IEmploymentRecord>)(e => e.VerifReorderedD_rep = "12/20/2018")},
            new object[] { "VerifExpD", (Action<IEmploymentRecord>)(e => e.VerifExpD = CDateTime.Create(new DateTime(2018, 12, 20)))},
            new object[] { "VerifExpD_rep", (Action<IEmploymentRecord>)(e => e.VerifExpD_rep = "12/20/2018")},
            new object[] { "VerifRecvD", (Action<IEmploymentRecord>)(e => e.VerifRecvD = CDateTime.Create(new DateTime(2018, 12, 20)))},
            new object[] { "VerifRecvD_rep", (Action<IEmploymentRecord>)(e => e.VerifRecvD_rep = "12/20/2018")},
            new object[] { "VerifSentD", (Action<IEmploymentRecord>)(e => e.VerifSentD = CDateTime.Create(new DateTime(2018, 12, 20)))},
            new object[] { "VerifSentD_rep", (Action<IEmploymentRecord>)(e => e.VerifSentD_rep  = "12/20/2018")},
            new object[] { "EmplrState", (Action<IEmploymentRecord>)(e => e.EmplrState = "CA")},
            new object[] { "Attention", (Action<IEmploymentRecord>)(e => e.Attention = "ATTN: H/R Dept")},
            new object[] { "PrepD", (Action<IEmploymentRecord>)(e => e.PrepD = CDateTime.Create(new DateTime(2018, 12, 20)))},
            new object[] { "IsSeeAttachment", (Action<IEmploymentRecord>)(e => e.IsSeeAttachment = false)},
            new object[] { "IsSpecialBorrowerEmployerRelationship", (Action<IEmploymentRecord>)(e => { e.IsSelfEmplmt = true; e.IsSpecialBorrowerEmployerRelationship = true;})},
            new object[] { "SelfOwnershipShareT", (Action<IEmploymentRecord>)(e => { e.IsSelfEmplmt = true; e.SelfOwnershipShareT = SelfOwnershipShare.LessThan25; })},
            new object[] { "ApplySignature", (Action<IEmploymentRecord>)(e => e.ApplySignature(Guid.NewGuid(), "signature.jpg"))}
        };

        private static readonly object[] RegularRecordFieldSetters = BaseRecordFieldSetters.Concat(new[]
        {
            new object[] { "IsCurrent", (Action<IEmploymentRecord>)(e => e.IsCurrent = true)},
            new object[] { "MonI_rep", (Action<IRegularEmploymentRecord>)(e => e.MonI_rep = "5")},
            new object[] { "EmplmtStartD_rep", (Action<IRegularEmploymentRecord>)(e => e.EmplmtStartD_rep = "12/20/2018")},
            new object[] { "EmplmtEndD_rep", (Action<IRegularEmploymentRecord>)(e => e.EmplmtEndD_rep = "12/20/2018")}
        }).ToArray();

        private static readonly object[] SpecialRecordFieldSetters = BaseRecordFieldSetters.Concat(new[]
        {
            new object[] { "EmplmtLen_rep", (Action<IPrimaryEmploymentRecord>)(e => e.EmplmtLen_rep = "10")},
            new object[] { "EmpltStartD_rep", (Action<IPrimaryEmploymentRecord>)(e => e.EmpltStartD_rep = "12/20/2018")},
            new object[] { "ProfStartD_rep", (Action<IPrimaryEmploymentRecord>)(e => e.ProfStartD_rep  = "12/20/2018")},
            new object[] { "ProfLen_rep", (Action<IPrimaryEmploymentRecord>)(e => e.ProfLen_rep = "10")},
            new object[] { "ProfLenTotalMonths", (Action<IPrimaryEmploymentRecord>)(e => e.ProfLenTotalMonths = "10")},
            new object[] { "EmplmtLenTotalMonths", (Action<IPrimaryEmploymentRecord>)(e => e.EmplmtLenTotalMonths = "10")}
        }).ToArray();

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            this.testFile = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false);
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            this.testFile?.Dispose();
        }

        [Test]
        public void IsEmpty_NewRegularRecord_ReturnsTrue()
        {
            var collection = this.GetCollection();
            var record = collection.AddRegularRecord() as CEmpRegRec;

            bool empty = record.IsEmpty;

            Assert.IsTrue(empty);
        }

        [Test]
        public void IsEmpty_NewSpecialRecord_ReturnsTrue()
        {
            var collection = this.GetCollection();
            var record = collection.GetPrimaryEmp(forceCreate: true) as CEmpPrimaryRec;

            bool empty = record.IsEmpty;

            Assert.IsTrue(empty);
        }

        [Test]
        public void IsEmpty_RegularRecordWithDefaultUiMonthlyIncome_ReturnsTrue()
        {
            var collection = this.GetCollection();
            var record = collection.AddRegularRecord() as CEmpRegRec;
            record.MonI_rep = "$0.00";

            bool empty = record.IsEmpty;

            Assert.IsTrue(empty);
        }

        [TestCase("$1.00")]
        [TestCase("ponies")]
        [TestCase("0 dollars")]
        public void IsEmpty_RegularRecordWithVariousNonDefaultIncome_ReturnsFalse(string monthlyIncome)
        {
            var collection = this.GetCollection();
            var record = collection.AddRegularRecord() as CEmpRegRec;
            record.MonI_rep = monthlyIncome;

            bool empty = record.IsEmpty;

            Assert.IsFalse(empty);
        }

        [TestCaseSource(nameof(RegularRecordFieldSetters))]
        public void IsEmpty_RegularRecordWithFieldSet_ReturnsFalse(string fieldId, Action<IRegularEmploymentRecord> setField)
        {
            var collection = this.GetCollection();
            var record = collection.AddRegularRecord() as CEmpRegRec;

            setField(record);
            bool empty = record.IsEmpty;

            Assert.IsFalse(empty);
        }

        [TestCaseSource(nameof(SpecialRecordFieldSetters))]
        public void IsEmpty_SpecialRecordWithFieldSet_ReturnsFalse(string fieldId, Action<IPrimaryEmploymentRecord> setField)
        {
            var collection = this.GetCollection();
            var record = collection.GetPrimaryEmp(forceCreate: true) as CEmpPrimaryRec;

            setField(record);
            bool empty = record.IsEmpty;

            Assert.IsFalse(empty);
        }

        public IEmpCollection GetCollection()
        {
            var loan = this.testFile.CreateNewPageDataWithBypass();
            loan.InitLoad();
            return loan.GetAppData(0).aBEmpCollection;
        }
    }
}
