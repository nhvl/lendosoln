﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LoanLqbCollectionDuplicatorTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> SourceConsumerId1 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> SourceConsumerId2 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> SourceLegacyAppId1 = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("33333333-3333-3333-3333-333333333333"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> DestinationConsumerId1 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("44444444-4444-4444-4444-444444444444"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> DestinationConsumerId2 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("55555555-5555-5555-5555-555555555555"));
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> DestinationLegacyAppId1 = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("66666666-6666-6666-6666-666666666666"));

        private static readonly Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>> ConsumerIdMap =
            new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>>()
            {
                [SourceConsumerId1] = DestinationConsumerId1,
                [SourceConsumerId2] = DestinationConsumerId2,
            };

        private static Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>> LegacyAppIdMap =
            new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>()
            {
                [SourceLegacyAppId1] = DestinationLegacyAppId1,
            };

        [Test]
        public void DuplicateAll_SingleAsset_CreatesNewAssetWithEqualFields()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var asset = this.GetDefaultAsset();
            sourceLoan.Add(SourceConsumerId1, asset);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(1, destinationLoan.Assets.Count);
            this.AssertEqualsDefaultAsset(destinationLoan.Assets.Single().Value);
        }

        [Test]
        public void DuplicateAll_ThreeAssets_CreatesThreeNewAssets()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var asset = this.GetDefaultAsset(accountName: "0");
            sourceLoan.Add(SourceConsumerId1, asset);
            asset = this.GetDefaultAsset(accountName: "1");
            sourceLoan.Add(SourceConsumerId1, asset);
            asset = this.GetDefaultAsset(accountName: "2");
            sourceLoan.Add(SourceConsumerId1, asset);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(3, destinationLoan.Assets.Count);
            this.AssertEqualsDefaultAsset(destinationLoan.Assets.ElementAt(0).Value, accountName: "0");
            this.AssertEqualsDefaultAsset(destinationLoan.Assets.ElementAt(1).Value, accountName: "1");
            this.AssertEqualsDefaultAsset(destinationLoan.Assets.ElementAt(2).Value, accountName: "2");
        }

        [Test]
        public void DuplicateAll_SingleAsset_CreatesAssetWithExpectedPrimaryOwner()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var asset = this.GetDefaultAsset();
            sourceLoan.Add(SourceConsumerId1, asset);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(DestinationConsumerId1, destinationLoan.ConsumerAssets.Single().Value.ConsumerId);
        }

        [Test]
        public void DuplicateAll_SingleAssetWithMultipleOwners_CreatesAssetWithExpectedOwners()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var asset = this.GetDefaultAsset();
            var assetId = sourceLoan.Add(SourceConsumerId1, asset);
            sourceLoan.AddOwnership(SourceConsumerId2, assetId);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(2, destinationLoan.ConsumerAssets.Count);
            CollectionAssert.AreEquivalent(new[] { DestinationConsumerId1, DestinationConsumerId2 }, destinationLoan.ConsumerAssets.Values.Select(a => a.ConsumerId));
            Assert.AreEqual(true, destinationLoan.ConsumerAssets.Values.All(a => a.AppId == DestinationLegacyAppId1));
        }

        [Test]
        public void DuplicateAll_SinglePrimaryEmployment_CreatesSinglePrimaryEmployment()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var employment = new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
            employment.IsPrimary = true;
            sourceLoan.Add(SourceConsumerId1, employment);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(1, destinationLoan.EmploymentRecords.Count);
            var entity = destinationLoan.EmploymentRecords.Values.Single();
            Assert.AreEqual(DestinationConsumerId1, entity.ConsumerId);
            Assert.AreEqual(true, entity.IsPrimary);
            Assert.AreEqual(DestinationLegacyAppId1, entity.LegacyAppId);
        }

        [Test]
        public void DuplicateAll_PrimaryEmploymentWithExtra_CreatesPrimaryEmploymentAndExtra()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var employment = new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
            employment.IsPrimary = true;
            sourceLoan.Add(SourceConsumerId1, employment);
            employment = new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
            sourceLoan.Add(SourceConsumerId1, employment);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(2, destinationLoan.EmploymentRecords.Count);

            var destinationPrimaryEntity = destinationLoan.EmploymentRecords.Values.Single(a => a.IsPrimary);
            Assert.AreEqual(DestinationConsumerId1, destinationPrimaryEntity.ConsumerId);
            Assert.AreEqual(DestinationLegacyAppId1, destinationPrimaryEntity.LegacyAppId);

            var destinationExtraEntity = destinationLoan.EmploymentRecords.Values.Single(a => !a.IsPrimary);
            Assert.AreEqual(DestinationConsumerId1, destinationExtraEntity.ConsumerId);
            Assert.AreEqual(DestinationLegacyAppId1, destinationExtraEntity.LegacyAppId);
        }

        [Test]
        public void DuplicateAll_MultipleConsumersWithPrimaryEmployment_CreatesExpectedPrimaryEmployments()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var employment = new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
            employment.IsPrimary = true;
            sourceLoan.Add(SourceConsumerId1, employment);
            employment = new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
            employment.IsPrimary = true;
            sourceLoan.Add(SourceConsumerId2, employment);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(2, destinationLoan.EmploymentRecords.Count);

            var firstPrimaryEntity = destinationLoan.EmploymentRecords.Values
                .Single(a => a.ConsumerId == DestinationConsumerId1 && a.IsPrimary);
            Assert.AreEqual(DestinationLegacyAppId1, firstPrimaryEntity.LegacyAppId);

            var secondPrimaryEntity = destinationLoan.EmploymentRecords.Values
                .Single(a => a.ConsumerId == DestinationConsumerId2 && a.IsPrimary);
            Assert.AreEqual(DestinationLegacyAppId1, secondPrimaryEntity.LegacyAppId);
        }

        [Test]
        public void DuplicateAll_SingleRealPropertyLiabilityAssociation_CreatesCorrectlyMappedAssociation()
        {
            var sourceLoan = this.GetCollectionContainer(SourceLegacyAppId1);
            var sourceRealProperty = new RealProperty();
            var sourceRealPropertyId = sourceLoan.Add(SourceConsumerId1, sourceRealProperty);
            var sourceLiability = new Liability(defaultsProvider: null);
            var sourceLiabilityId = sourceLoan.Add(SourceConsumerId1, sourceLiability);
            sourceLoan.AddRealPropertyLiabilityAssociation(sourceRealPropertyId, sourceLiabilityId);
            var destinationLoan = this.GetCollectionContainer(DestinationLegacyAppId1);

            var duplication = new LoanLqbCollectionDuplicator(sourceLoan, destinationLoan, ConsumerIdMap, LegacyAppIdMap);
            duplication.DuplicateEntities();

            Assert.AreEqual(1, destinationLoan.RealPropertyLiabilities.Count);
            var association = destinationLoan.RealPropertyLiabilities.Values.Single();
            Assert.AreEqual(destinationLoan.RealProperties.Keys.Single(), association.RealPropertyId);
            Assert.AreEqual(destinationLoan.Liabilities.Keys.Single(), association.LiabilityId);
        }

        private Asset GetDefaultAsset(string accountName = "AccountName")
        {
            var asset = new Asset()
            {
                AccountName = DescriptionField.Create(accountName),
                AccountNum = BankAccountNumber.Create("BankAccountNumber"),
                AssetCashDepositType = E_AssetCashDepositT.CashDeposit1,
                AssetType = E_AssetT.Checking,
                Attention = EntityName.Create("Attention"),
                City = City.Create("City"),
                CompanyName = EntityName.Create("CompanyName"),
                DepartmentName = DescriptionField.Create("DepartmentName"),
                Desc = DescriptionField.Create("Desc"),
                FaceValue = Money.Create(1),
                GiftSourceData = E_GiftFundSourceT.Other,
                IsEmptyCreated = false,
                IsSeeAttachment = true,
                OtherTypeDesc = DescriptionField.Create("OtherTypeDesc"),
                Phone = PhoneNumber.Create("888-593-8970"),
                PrepDate = UnzonedDate.Create(2018, 8, 1),
                State = UnitedStatesPostalState.Create("CA"),
                StreetAddress = StreetAddress.Create("1600 Sunflower Ave #200"),
                Value = Money.Create(2),
                VerifExpiresDate = UnzonedDate.Create(2018, 9, 1),
                VerifRecvDate = UnzonedDate.Create(2018, 8, 1),
                VerifReorderedDate = UnzonedDate.Create(2018, 7, 1),
                VerifSentDate = UnzonedDate.Create(2018, 6, 1),
                Zip = Zipcode.Create("92626")
            };

            asset.SetVerifSignature(
                DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create("11111111-1111-1111-1111-111111111111").Value,
                DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create("22222222-2222-2222-2222-222222222222").Value);

            return asset;
        }

        private void AssertEqualsDefaultAsset(Asset asset, string accountName = "AccountName")
        {
            Assert.AreEqual(asset.AccountName, DescriptionField.Create(accountName));
            Assert.AreEqual(asset.AccountNum, BankAccountNumber.Create("BankAccountNumber"));
            Assert.AreEqual(asset.AssetCashDepositType, E_AssetCashDepositT.CashDeposit1);
            Assert.AreEqual(asset.AssetType, E_AssetT.Checking);
            Assert.AreEqual(asset.Attention, EntityName.Create("Attention"));
            Assert.AreEqual(asset.City, City.Create("City"));
            Assert.AreEqual(asset.CompanyName, EntityName.Create("CompanyName"));
            Assert.AreEqual(asset.DepartmentName, DescriptionField.Create("DepartmentName"));
            Assert.AreEqual(asset.Desc, DescriptionField.Create("Desc"));
            Assert.AreEqual(asset.FaceValue, Money.Create(1));
            Assert.AreEqual(asset.GiftSourceData, E_GiftFundSourceT.Other);
            Assert.AreEqual(asset.IsEmptyCreated, false);
            Assert.AreEqual(asset.IsSeeAttachment, true);
            Assert.AreEqual(asset.OtherTypeDesc, DescriptionField.Create("OtherTypeDesc"));
            Assert.AreEqual(asset.Phone, PhoneNumber.Create("888-593-8970"));
            Assert.AreEqual(asset.PrepDate, UnzonedDate.Create(2018, 8, 1));
            Assert.AreEqual(asset.State, UnitedStatesPostalState.Create("CA"));
            Assert.AreEqual(asset.StreetAddress, StreetAddress.Create("1600 Sunflower Ave #200"));
            Assert.AreEqual(asset.Value, Money.Create(2));
            Assert.AreEqual(asset.VerifExpiresDate, UnzonedDate.Create(2018, 9, 1));
            Assert.AreEqual(asset.VerifRecvDate, UnzonedDate.Create(2018, 8, 1));
            Assert.AreEqual(asset.VerifReorderedDate, UnzonedDate.Create(2018, 7, 1));
            Assert.AreEqual(asset.VerifSentDate, UnzonedDate.Create(2018, 6, 1));
            Assert.AreEqual(asset.Zip, Zipcode.Create("92626"));
            Assert.AreEqual(asset.VerifSigningEmployeeId, DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create("11111111-1111-1111-1111-111111111111"));
            Assert.AreEqual(asset.VerifSignatureImgId, DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create("22222222-2222-2222-2222-222222222222"));
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppIdentifier)
        {
            Fakes.FakeLegacyApplicationConsumers legacyAppConsumers;
            if (legacyAppIdentifier == SourceLegacyAppId1)
            {
                legacyAppConsumers = Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(SourceLegacyAppId1, SourceConsumerId1, SourceConsumerId2);
            }
            else if (legacyAppIdentifier == DestinationLegacyAppId1)
            {
                legacyAppConsumers = Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(DestinationLegacyAppId1, DestinationConsumerId1, DestinationConsumerId2);
            }
            else
            {
                throw new InvalidOperationException("Expected the legacy app to be the source or destination primary, but was '" + legacyAppIdentifier.Value + "'");
            }

            var providerFactory = new LoanLqbCollectionProviderFactory();
            ILoanLqbCollectionProvider collectionProvider = providerFactory.CreateEmptyProvider();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanData.LegacyApplicationConsumers.Returns(legacyAppConsumers);

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var collectionContainer = containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For< ILoanLqbCollectionBorrowerManagement>(),
                null);
            foreach (var collection in LoanLqbCollectionContainer.LoanCollections)
            {
                collectionContainer.RegisterCollection(collection);
            }

            collectionContainer.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                null,
                null);

            return collectionContainer;
        }
    }
}
