﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using Utils;
    using NUnit.Framework;
    using global::DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LoanFileCreationConsumerIdTest
    {
        [Test]
        public void CreateBlankLoanFile_Always_AssignsNonEmptyGuidToConsumerIds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var loanId = creator.CreateBlankLoanFile();
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(LoanFileCreationConsumerIdTest));
                loan.InitLoad();
                var app = loan.GetAppData(0);

                Assert.AreNotEqual(Guid.Empty, app.aBConsumerId);
                Assert.AreNotEqual(Guid.Empty, app.aCConsumerId);
            }
        }

        [Test]
        public void CreateBlankLeadFile_Always_AssignsNonEmptyGuidToConsumerIds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var leadId = creator.CreateLead(Guid.Empty);
                var lead = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    leadId,
                    typeof(LoanFileCreationConsumerIdTest));
                lead.InitLoad();
                var app = lead.GetAppData(0);

                Assert.AreNotEqual(Guid.Empty, app.aBConsumerId);
                Assert.AreNotEqual(Guid.Empty, app.aCConsumerId);
            }
        }

        [Test]
        public void DuplicateLoanFile_Always_AssignsNonEmptyGuidToConsumerIds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var sourceLoanId = creator.CreateBlankLoanFile();
                var duplicateLoanId = creator.CreateLoanFromTemplate(sourceLoanId);
                var duplicateLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    duplicateLoanId,
                    typeof(LoanFileCreationConsumerIdTest));
                duplicateLoan.InitLoad();
                var app = duplicateLoan.GetAppData(0);

                Assert.AreNotEqual(Guid.Empty, app.aBConsumerId);
                Assert.AreNotEqual(Guid.Empty, app.aCConsumerId);
            }
        }

        [Test]
        public void DuplicateLoanFile_Never_CopiesConsumerIdsFromSourceFile()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var sourceLoanId = creator.CreateBlankLoanFile();
                var sourceLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    sourceLoanId,
                    typeof(LoanFileCreationConsumerIdTest));
                sourceLoan.InitLoad();
                var sourceApp = sourceLoan.GetAppData(0);
                var source_aBConsumerId = sourceApp.aBConsumerId;
                var source_aCConsumerId = sourceApp.aCConsumerId;
                var duplicateLoanId = creator.CreateLoanFromTemplate(sourceLoanId);
                var duplicateLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    duplicateLoanId,
                    typeof(LoanFileCreationConsumerIdTest));
                duplicateLoan.InitLoad();
                var duplicateApp = duplicateLoan.GetAppData(0);

                Assert.AreNotEqual(source_aBConsumerId, duplicateApp.aBConsumerId);
                Assert.AreNotEqual(source_aCConsumerId, duplicateApp.aCConsumerId);
            }
        }

        [Test]
        public void AddNewApp_Always_AssignsNonEmptyGuidToConsumerIds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var loanId = creator.CreateBlankLoanFile();
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(LoanFileCreationConsumerIdTest));
                loan.AddNewApp();
                loan.InitLoad();
                var app = loan.GetAppData(1);

                Assert.AreNotEqual(Guid.Empty, app.aBConsumerId);
                Assert.AreNotEqual(Guid.Empty, app.aCConsumerId);
            }
        }

        [Test]
        public void AddNewApp_Never_CopiesConsumerIdsFromPrimaryApp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var loanId = creator.CreateBlankLoanFile();
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(LoanFileCreationConsumerIdTest));
                loan.AddNewApp();
                loan.InitLoad();
                var primaryApp = loan.GetAppData(0);
                var newApp = loan.GetAppData(1);

                Assert.AreNotEqual(primaryApp.aBConsumerId, newApp.aBConsumerId);
                Assert.AreNotEqual(primaryApp.aCConsumerId, newApp.aCConsumerId);
            }
        }

        [Test]
        public void AddNewApp_Only_Borrowers_As_Consumers()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var loanId = creator.CreateBlankUladLoanFile();
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(LoanFileCreationConsumerIdTest));
                loan.AddNewApp();
                loan.InitLoad();
                var primaryApp = loan.GetAppData(0);
                var newApp = loan.GetAppData(1);
                var firstBorrowerId = primaryApp.aBConsumerId;
                var secondBorrowerId = newApp.aBConsumerId;

                var consumers = loan.Consumers;
                Assert.AreEqual(2, consumers.Count);
                foreach (var id in consumers.Keys)
                {
                    Assert.IsTrue(id.Value == firstBorrowerId || id.Value == secondBorrowerId);
                }

                var associations = loan.LegacyApplicationConsumers;
                Assert.AreEqual(2, associations.Count);
                foreach (var id in associations.Keys)
                {
                    Assert.IsTrue(id.Value == firstBorrowerId || id.Value == secondBorrowerId);
                }
            }
        }

        [Test]
        public void AddNewApp_All_Borrowers_As_Consumers()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = this.GetLoanFileCreator();

                var loanId = creator.CreateBlankUladLoanFile();
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(LoanFileCreationConsumerIdTest));
                loan.AddNewApp();
                loan.InitLoad();
                var primaryApp = loan.GetAppData(0);
                var newApp = loan.GetAppData(1);
                loan.AddCoborrowerToLegacyApplication(primaryApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.AddCoborrowerToLegacyApplication(newApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());

                var consumers = loan.Consumers;
                Assert.AreEqual(4, consumers.Count);

                var associations = loan.LegacyApplicationConsumers;
                Assert.AreEqual(4, associations.Count);
            }
        }

        private CLoanFileCreator GetLoanFileCreator()
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            return CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
        }
    }
}
