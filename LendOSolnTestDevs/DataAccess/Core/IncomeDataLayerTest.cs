﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers;
    using LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class IncomeDataLayerTest
    {
        public const decimal Borr1BaseIncome = 10.01M;
        public const string Borr1BaseIncomeRep = "$10.01";
        public const decimal Borr1OvertimeIncome = 11.12M;
        public const string Borr1OvertimeIncomeRep = "$11.12";
        public const decimal Borr1BonusesIncome = 12.23M;
        public const string Borr1BonusesIncomeRep = "$12.23";
        public const decimal Borr1CommissionIncome = 13.34M;
        public const string Borr1CommissionIncomeRep = "$13.34";
        public const decimal Borr1DividendIncome = 14.45M;
        public const string Borr1DividendIncomeRep = "$14.45";

        public const decimal Borr1TotalIncome = 61.15M;
        public const string Borr1TotalIncomeRep = "$61.15";

        public const decimal CoBo1BaseIncome = 15.56M;
        public const string CoBo1BaseIncomeRep = "$15.56";
        public const decimal CoBo1OvertimeIncome = 16.67M;
        public const string CoBo1OvertimeIncomeRep = "$16.67";
        public const decimal CoBo1BonusesIncome = 17.78M;
        public const string CoBo1BonusesIncomeRep = "$17.78";
        public const decimal CoBo1CommissionIncome = 18.89M;
        public const string CoBo1CommissionIncomeRep = "$18.89";
        public const decimal CoBo1DividendIncome = 19.90M;
        public const string CoBo1DividendIncomeRep = "$19.90";

        public const decimal CoBo1TotalIncome = 88.80M;
        public const string CoBo1TotalIncomeRep = "$88.80";

        public const decimal Borr2BaseIncome = 20.01M;
        public const string Borr2BaseIncomeRep = "$20.01";
        public const decimal Borr2OvertimeIncome = 21.12M;
        public const string Borr2OvertimeIncomeRep = "$21.12";
        public const decimal Borr2BonusesIncome = 22.23M;
        public const string Borr2BonusesIncomeRep = "$22.23";
        public const decimal Borr2CommissionIncome = 23.34M;
        public const string Borr2CommissionIncomeRep = "$23.34";
        public const decimal Borr2DividendIncome = 24.45M;
        public const string Borr2DividendIncomeRep = "$24.45";

        public const decimal Borr2TotalIncome = 111.15M;
        public const string Borr2TotalIncomeRep = "$111.15";

        public const decimal CoBo2BaseIncome = 25.56M;
        public const string CoBo2BaseIncomeRep = "$25.56";
        public const decimal CoBo2OvertimeIncome = 26.67M;
        public const string CoBo2OvertimeIncomeRep = "$26.67";
        public const decimal CoBo2BonusesIncome = 27.78M;
        public const string CoBo2BonusesIncomeRep = "$27.78";
        public const decimal CoBo2CommissionIncome = 28.89M;
        public const string CoBo2CommissionIncomeRep = "$28.89";
        public const decimal CoBo2DividendIncome = 29.90M;
        public const string CoBo2DividendIncomeRep = "$29.90";

        public const decimal CoBo2TotalIncome = 138.80M;
        public const string CoBo2TotalIncomeRep = "$138.80";

        private static readonly E_sLqbCollectionT[] allCollectionTypes = (E_sLqbCollectionT[])Enum.GetValues(typeof(E_sLqbCollectionT));

        [TestCaseSource(nameof(allCollectionTypes))]
        public void SetPmlPrimaryAppTotNonspI_ParameterizedCollectionMode_Works(E_sLqbCollectionT datalayerMode)
        {
            using (var loanForTest = CreateLoan(datalayerMode))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.AddNewApp();
                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();

                var app1 = loan.GetAppData(0);
                var app2 = loan.GetAppData(1);
                loan.AddCoborrowerToLegacyApplication(app1.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.AddCoborrowerToLegacyApplication(app2.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                const decimal Borr1NetRentIncome = 627.38M;
                const decimal CoBo1NetRentIncome = 891.79M;
                const decimal Borr2NetRentIncome = 697.16M;
                const decimal CoBo2NetRentIncome = 243.07M;
                app1.aNetRentI1003Lckd = true;
                app1.aBNetRentI1003 = Borr1NetRentIncome;
                app1.aCNetRentI1003 = CoBo1NetRentIncome;
                app2.aNetRentI1003Lckd = true;
                app2.aBNetRentI1003 = Borr2NetRentIncome;
                app2.aCNetRentI1003 = CoBo2NetRentIncome;
                if (loan.sIsIncomeCollectionEnabled)
                {
                    var borr1ConsumerId = app1.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    var cobo1ConsumerId = app1.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    loan.AddIncomeSource(borr1ConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr1BaseIncome, });
                    loan.AddIncomeSource(borr1ConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr1OvertimeIncome, });
                    loan.AddIncomeSource(borr1ConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr1BonusesIncome, });
                    loan.AddIncomeSource(borr1ConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr1CommissionIncome, });
                    loan.AddIncomeSource(borr1ConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr1DividendIncome, });
                    loan.AddIncomeSource(cobo1ConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo1BaseIncome, });
                    loan.AddIncomeSource(cobo1ConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo1OvertimeIncome, });
                    loan.AddIncomeSource(cobo1ConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo1BonusesIncome, });
                    loan.AddIncomeSource(cobo1ConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo1CommissionIncome, });
                    loan.AddIncomeSource(cobo1ConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo1DividendIncome, });
                    var borr2ConsumerId = app2.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    var cobo2ConsumerId = app2.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    loan.AddIncomeSource(borr2ConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr2BaseIncome, });
                    loan.AddIncomeSource(borr2ConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr2OvertimeIncome, });
                    loan.AddIncomeSource(borr2ConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr2BonusesIncome, });
                    loan.AddIncomeSource(borr2ConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr2CommissionIncome, });
                    loan.AddIncomeSource(borr2ConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr2DividendIncome, });
                    loan.AddIncomeSource(cobo2ConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo2BaseIncome, });
                    loan.AddIncomeSource(cobo2ConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo2OvertimeIncome, });
                    loan.AddIncomeSource(cobo2ConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo2BonusesIncome, });
                    loan.AddIncomeSource(cobo2ConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo2CommissionIncome, });
                    loan.AddIncomeSource(cobo2ConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo2DividendIncome, });
                }
                else
                {
                    app1.aBBaseI = Borr1BaseIncome;
                    app1.aBOvertimeI = Borr1OvertimeIncome;
                    app1.aBBonusesI = Borr1BonusesIncome;
                    app1.aBCommisionI = Borr1CommissionIncome;
                    app1.aBDividendI = Borr1DividendIncome;
                    app1.aCBaseI = CoBo1BaseIncome;
                    app1.aCOvertimeI = CoBo1OvertimeIncome;
                    app1.aCBonusesI = CoBo1BonusesIncome;
                    app1.aCCommisionI = CoBo1CommissionIncome;
                    app1.aCDividendI = CoBo1DividendIncome;
                    app2.aBBaseI = Borr2BaseIncome;
                    app2.aBOvertimeI = Borr2OvertimeIncome;
                    app2.aBBonusesI = Borr2BonusesIncome;
                    app2.aBCommisionI = Borr2CommissionIncome;
                    app2.aBDividendI = Borr2DividendIncome;
                    app2.aCBaseI = CoBo2BaseIncome;
                    app2.aCOvertimeI = CoBo2OvertimeIncome;
                    app2.aCBonusesI = CoBo2BonusesIncome;
                    app2.aCCommisionI = CoBo2CommissionIncome;
                    app2.aCDividendI = CoBo2DividendIncome;
                }

                const decimal Borr1TotalIncomeBefore = Borr1TotalIncome + Borr1NetRentIncome;
                const decimal CoBo1TotalIncomeBefore = CoBo1TotalIncome + CoBo1NetRentIncome;
                const decimal Borr2TotalIncomeBefore = Borr2TotalIncome + Borr2NetRentIncome;
                const decimal CoBo2TotalIncomeBefore = CoBo2TotalIncome + CoBo2NetRentIncome;
                Assert.That(app1, Has.Property(nameof(CAppData.aBTotI)).EqualTo(Borr1TotalIncomeBefore));
                Assert.That(app1, Has.Property(nameof(CAppData.aCTotI)).EqualTo(CoBo1TotalIncomeBefore));
                Assert.That(app2, Has.Property(nameof(CAppData.aBTotI)).EqualTo(Borr2TotalIncomeBefore));
                Assert.That(app2, Has.Property(nameof(CAppData.aCTotI)).EqualTo(CoBo2TotalIncomeBefore));
                const decimal PrimaryAppIncomeBefore = Borr1TotalIncomeBefore + CoBo1TotalIncomeBefore;
                Assert.That(loan, Has.Property(nameof(CPageData.sPrimAppTotNonspIPe)).EqualTo(PrimaryAppIncomeBefore));
                CPageBaseExtractor.Extract(loan).sPrimAppTotNonspIPeval = loan.sPrimAppTotNonspIPe;
                loan.CalcModeT = E_CalcModeT.PriceMyLoan;
                Assert.That(loan, Has.Property(nameof(CPageData.sPrimAppTotNonspIPe)).EqualTo(PrimaryAppIncomeBefore));

                const decimal TotalIncomeToSet = 987.65M;
                Assert.AreNotEqual(TotalIncomeToSet, PrimaryAppIncomeBefore);

                // Act
                loan.sPrimAppTotNonspIPe = TotalIncomeToSet;

                Assert.That(loan, Has.Property(nameof(CPageData.sPrimAppTotNonspIPe)).EqualTo(TotalIncomeToSet));
                Assert.That(app1, Has.Property(nameof(CAppData.aBTotI)).EqualTo(TotalIncomeToSet));
                Assert.That(app1, Has.Property(nameof(CAppData.aCTotI)).EqualTo(0M));
                Assert.That(app2, Has.Property(nameof(CAppData.aBTotI)).EqualTo(0M));
                Assert.That(app2, Has.Property(nameof(CAppData.aCTotI)).EqualTo(0M));
                loan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                Assert.That(loan, Has.Property(nameof(CPageData.sPrimAppTotNonspIPe)).EqualTo(TotalIncomeToSet));
                Assert.That(app1, Has.Property(nameof(CAppData.aBTotI)).EqualTo(TotalIncomeToSet));
                Assert.That(app1, Has.Property(nameof(CAppData.aCTotI)).EqualTo(0M));
                Assert.That(app2, Has.Property(nameof(CAppData.aBTotI)).EqualTo(0M));
                Assert.That(app2, Has.Property(nameof(CAppData.aCTotI)).EqualTo(0M));
            }
        }

        [Test]
        public void SetPmlBorrowerTotalIncome_ParameterizedCollectionMode_Works(
            [ValueSource(nameof(allCollectionTypes))]E_sLqbCollectionT datalayerMode,
            [Values(E_BorrowerModeT.Borrower, E_BorrowerModeT.Coborrower)]E_BorrowerModeT borrowerMode)
        {
            using (var loanForTest = CreateLoan(datalayerMode))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();

                var app = loan.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(app.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                const decimal BorrNetRentIncome = 627.38M;
                const decimal CoBoNetRentIncome = 891.79M;
                app.aNetRentI1003Lckd = true;
                app.aBNetRentI1003 = BorrNetRentIncome;
                app.aCNetRentI1003 = CoBoNetRentIncome;
                Assert.That(app, Has.Property(nameof(CAppData.aBNetRentI1003)).EqualTo(BorrNetRentIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCNetRentI1003)).EqualTo(CoBoNetRentIncome));
                if (loan.sIsIncomeCollectionEnabled)
                {
                    var borrConsumerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    var coboConsumerId = app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr1BaseIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr1OvertimeIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr1BonusesIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr1CommissionIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr1DividendIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo1BaseIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo1OvertimeIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo1BonusesIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo1CommissionIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo1DividendIncome, });
                }
                else
                {
                    app.aBBaseI = Borr1BaseIncome;
                    app.aBOvertimeI = Borr1OvertimeIncome;
                    app.aBBonusesI = Borr1BonusesIncome;
                    app.aBCommisionI = Borr1CommissionIncome;
                    app.aBDividendI = Borr1DividendIncome;
                    app.aCBaseI = CoBo1BaseIncome;
                    app.aCOvertimeI = CoBo1OvertimeIncome;
                    app.aCBonusesI = CoBo1BonusesIncome;
                    app.aCCommisionI = CoBo1CommissionIncome;
                    app.aCDividendI = CoBo1DividendIncome;
                }

                const decimal BorrTotalIncomeBefore = Borr1TotalIncome + BorrNetRentIncome;
                const decimal CoBoTotalIncomeBefore = CoBo1TotalIncome + CoBoNetRentIncome;
                Assert.That(app, Has.Property(nameof(CAppData.aBTotI)).EqualTo(BorrTotalIncomeBefore));
                Assert.That(app, Has.Property(nameof(CAppData.aCTotI)).EqualTo(CoBoTotalIncomeBefore));
                const decimal TotalIncomeToSet = 987.65M;
                Assert.AreNotEqual(TotalIncomeToSet, BorrTotalIncomeBefore);
                Assert.AreNotEqual(TotalIncomeToSet, CoBoTotalIncomeBefore);

                // Act
                if (borrowerMode == E_BorrowerModeT.Borrower)
                {
                    app.aBTotIPe = TotalIncomeToSet;
                }
                else
                {
                    app.aCTotIPe = TotalIncomeToSet;
                }

                // whichever mode was set, should have it's total Income (and PE field) updated, but the other consumer's values should be unaffected
                if (borrowerMode == E_BorrowerModeT.Borrower)
                {
                    Assert.That(app, Has.Property(nameof(CAppData.aBTotIPe)).EqualTo(TotalIncomeToSet));
                    Assert.That(app, Has.Property(nameof(CAppData.aBTotI)).EqualTo(TotalIncomeToSet));
                    Assert.That(app, Has.Property(nameof(CAppData.aCTotIPe)).EqualTo(CoBoTotalIncomeBefore));
                    Assert.That(app, Has.Property(nameof(CAppData.aCTotI)).EqualTo(CoBoTotalIncomeBefore));
                    AssertFixedFieldValues(app, borrowerMode: E_BorrowerModeT.Coborrower);
                    Assert.That(app, Has.Property(nameof(CAppData.aCNetRentI1003)).EqualTo(CoBoNetRentIncome));
                }
                else
                {
                    Assert.That(app, Has.Property(nameof(CAppData.aBTotIPe)).EqualTo(BorrTotalIncomeBefore));
                    Assert.That(app, Has.Property(nameof(CAppData.aBTotI)).EqualTo(BorrTotalIncomeBefore));
                    AssertFixedFieldValues(app, borrowerMode: E_BorrowerModeT.Borrower);
                    Assert.That(app, Has.Property(nameof(CAppData.aBNetRentI1003)).EqualTo(BorrNetRentIncome));
                    Assert.That(app, Has.Property(nameof(CAppData.aCTotIPe)).EqualTo(TotalIncomeToSet));
                    Assert.That(app, Has.Property(nameof(CAppData.aCTotI)).EqualTo(TotalIncomeToSet));
                }
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        [TestCase(E_sLqbCollectionT.IncomeSourceCollection)]
        public void SwapBorrowers_ForANormalishLoan_SwapsTheValues(E_sLqbCollectionT datalayerMode)
        {
            using (var loanForTest = CreateLoan(datalayerMode))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.Save();
                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();
                const decimal BorrNetRentIncome = 627.38M;
                const decimal CoBoNetRentIncome = 891.79M;
                var app = loan.GetAppData(0);
                app.aNetRentI1003Lckd = true;
                app.aBNetRentI1003 = BorrNetRentIncome;
                app.aCNetRentI1003 = CoBoNetRentIncome;
                if (loan.sIsIncomeCollectionEnabled)
                {
                    var borrConsumerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    var coboConsumerId = app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr1BaseIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr1OvertimeIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr1BonusesIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr1CommissionIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr1DividendIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo1BaseIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo1OvertimeIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo1BonusesIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo1CommissionIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo1DividendIncome, });
                }
                else
                {
                    app.aBBaseI = Borr1BaseIncome;
                    app.aBOvertimeI = Borr1OvertimeIncome;
                    app.aBBonusesI = Borr1BonusesIncome;
                    app.aBCommisionI = Borr1CommissionIncome;
                    app.aBDividendI = Borr1DividendIncome;
                    app.aCBaseI = CoBo1BaseIncome;
                    app.aCOvertimeI = CoBo1OvertimeIncome;
                    app.aCBonusesI = CoBo1BonusesIncome;
                    app.aCCommisionI = CoBo1CommissionIncome;
                    app.aCDividendI = CoBo1DividendIncome;
                }

                const decimal BorrTotalIncomeBefore = Borr1TotalIncome + BorrNetRentIncome;
                const decimal CoBoTotalIncomeBefore = CoBo1TotalIncome + CoBoNetRentIncome;
                Assert.That(app, Has.Property(nameof(CAppData.aBTotI)).EqualTo(BorrTotalIncomeBefore));
                Assert.That(app, Has.Property(nameof(CAppData.aCTotI)).EqualTo(CoBoTotalIncomeBefore));

                app.SwapMarriedBorAndCobor();

                Assert.That(app, Has.Property(nameof(CAppData.aBTotI)).EqualTo(CoBoTotalIncomeBefore));
                Assert.That(app, Has.Property(nameof(CAppData.aCTotI)).EqualTo(BorrTotalIncomeBefore));
                Assert.That(app, Has.Property(nameof(CAppData.aBBaseI)).EqualTo(CoBo1BaseIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBOvertimeI)).EqualTo(CoBo1OvertimeIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBBonusesI)).EqualTo(CoBo1BonusesIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBCommisionI)).EqualTo(CoBo1CommissionIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBDividendI)).EqualTo(CoBo1DividendIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCBaseI)).EqualTo(Borr1BaseIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCOvertimeI)).EqualTo(Borr1OvertimeIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCBonusesI)).EqualTo(Borr1BonusesIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCCommisionI)).EqualTo(Borr1CommissionIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCDividendI)).EqualTo(Borr1DividendIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBNetRentI1003)).EqualTo(CoBoNetRentIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCNetRentI1003)).EqualTo(BorrNetRentIncome));
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        [TestCase(E_sLqbCollectionT.IncomeSourceCollection)]
        public void DeleteCoborrower_ForANormalishLoan_RemovesIncomeValues(E_sLqbCollectionT datalayerMode)
        {
            using (var loanForTest = CreateLoan(datalayerMode))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.Save();
                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();
                const decimal BorrNetRentIncome = 627.38M;
                const decimal CoBoNetRentIncome = 891.79M;
                var app = loan.GetAppData(0);
                app.aNetRentI1003Lckd = true;
                app.aBNetRentI1003 = BorrNetRentIncome;
                app.aCNetRentI1003 = CoBoNetRentIncome;
                if (loan.sIsIncomeCollectionEnabled)
                {
                    var borrConsumerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    var coboConsumerId = app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr1BaseIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr1OvertimeIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr1BonusesIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr1CommissionIncome, });
                    loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr1DividendIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo1BaseIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo1OvertimeIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo1BonusesIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo1CommissionIncome, });
                    loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo1DividendIncome, });
                }
                else
                {
                    app.aBBaseI = Borr1BaseIncome;
                    app.aBOvertimeI = Borr1OvertimeIncome;
                    app.aBBonusesI = Borr1BonusesIncome;
                    app.aBCommisionI = Borr1CommissionIncome;
                    app.aBDividendI = Borr1DividendIncome;
                    app.aCBaseI = CoBo1BaseIncome;
                    app.aCOvertimeI = CoBo1OvertimeIncome;
                    app.aCBonusesI = CoBo1BonusesIncome;
                    app.aCCommisionI = CoBo1CommissionIncome;
                    app.aCDividendI = CoBo1DividendIncome;
                }

                const decimal BorrTotalIncomeBefore = Borr1TotalIncome + BorrNetRentIncome;
                const decimal CoBoTotalIncomeBefore = CoBo1TotalIncome + CoBoNetRentIncome;
                Assert.That(app, Has.Property(nameof(CAppData.aBTotI)).EqualTo(BorrTotalIncomeBefore));
                Assert.That(app, Has.Property(nameof(CAppData.aCTotI)).EqualTo(CoBoTotalIncomeBefore));

                if (datalayerMode >= E_sLqbCollectionT.IncomeSourceCollection)
                {
                    // The new ULAD collections require consistency, so instead of massive cascading
                    // data loss, the coborrower deletion is blocked.
                    Assert.Throws<CBaseException>(() => app.DelMarriedCobor());
                }
                else
                {
                    // The classic behavior allowed for disconnected associations because data isn't actually lost
                    app.DelMarriedCobor();

                    Assert.That(app, Has.Property(nameof(CAppData.aBTotI)).EqualTo(BorrTotalIncomeBefore));
                    Assert.That(app, Has.Property(nameof(CAppData.aCTotI)).EqualTo(0M));
                    AssertFixedFieldValues(app, borrowerMode: E_BorrowerModeT.Borrower);
                    AssertFixedFieldValues(app, 0M, E_BorrowerModeT.Coborrower);
                    Assert.That(app, Has.Property(nameof(CAppData.aBNetRentI1003)).EqualTo(BorrNetRentIncome));
                    Assert.That(app, Has.Property(nameof(CAppData.aCNetRentI1003)).EqualTo(0M));
                }
            }
        }

        [Test]
        public void OtherIncomeList_LoanWithoutIncomeCollection_AlwaysHas3EntriesOnInitialLoad()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();

                var app = loan.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(app.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                Assert.That(loan, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(false));

                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                app.aOtherIncomeList.Clear();
                app.aOtherIncomeList = app.aOtherIncomeList;
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                app = loan.GetAppData(0);
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                app.aOtherIncomeList.Clear();
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#1", Amount = 123.45M, });
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#2", Amount = 234.56M, });
                app.aOtherIncomeList = app.aOtherIncomeList;
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                app = loan.GetAppData(0);
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                app.aOtherIncomeList.Clear();
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#1", Amount = 123.45M, });
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#2", Amount = 234.56M, });
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#3", Amount = 345.67M, });
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#4", Amount = 456.78M, });
                Assert.That(app.aOtherIncomeList, Has.Count.EqualTo(4));
                app.aOtherIncomeList = app.aOtherIncomeList;
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();
                app = loan.GetAppData(0);
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
            }
        }

        [Test]
        public void OtherIncomeList_LoanWithIncomeCollection_AlwaysHas3Entries()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();

                var app = loan.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(app.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                Assert.That(loan, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(true));

                Assert.That(loan.IncomeSources, Has.Count.EqualTo(0));
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                app = loan.GetAppData(0);
                var borrConsumerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Alimony, MonthlyAmountData = 123.45M, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.FosterCare, MonthlyAmountData = 234.56M, });
                Assert.That(loan.IncomeSources, Has.Count.EqualTo(2));
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                app = loan.GetAppData(0);
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                Assert.That(loan.IncomeSources, Has.Count.EqualTo(2));
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Alimony, MonthlyAmountData = 345.67M, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.FosterCare, MonthlyAmountData = 456.78M, });
                Assert.That(loan.IncomeSources, Has.Count.EqualTo(4));
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();
                app = loan.GetAppData(0);
                Assert.That(loan.IncomeSources, Has.Count.EqualTo(4));
                Assert.That(app.aOtherIncomeList, Has.Count.AtLeast(3));
            }
        }

        [Test]
        public void OtherIncomeList_BeforeAndAfterIncomeCollectionMigration_StillReturnsValues()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                var app = loan.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(app.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                app.aBBaseI = 001.23M;
                app.aCBaseI = 012.34M;
                app.aOtherIncomeList.Clear();
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#1", Amount = 123.45M, IsForCoBorrower = false, });
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#2", Amount = 234.56M, IsForCoBorrower = true, });
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#3", Amount = 345.67M, IsForCoBorrower = false, });
                app.aOtherIncomeList.Add(new OtherIncome { Desc = "#4", Amount = 456.78M, IsForCoBorrower = true, });
                app.aOtherIncomeList = app.aOtherIncomeList;
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                app = loan.GetAppData(0);
                Assert.That(app.aOtherIncomeList, Has.Count.EqualTo(4));
                Assert.AreEqual(1160.46M, app.aTotOI);
                Assert.AreEqual(1160.46M, app.aOtherIncomeList.Sum(o => o.Amount));
                app.aOtherIncomeList.RemoveAt(2);
                app.aOtherIncomeList = app.aOtherIncomeList;
                Assert.That(app.aOtherIncomeList, Has.Count.EqualTo(3));
                Assert.AreEqual(814.79M, app.aTotOI);
                Assert.AreEqual(814.79M, app.aOtherIncomeList.Sum(o => o.Amount));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                app = loan.GetAppData(0);
                var borrConsumerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                Assert.That(loan, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(false));
                loan.MigrateToUseLqbCollections();
                loan.MigrateToIncomeSourceCollection();
                Assert.That(loan.IncomeSources, Has.Count.EqualTo(5));
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.PublicAssistance, MonthlyAmountData = 567.89M, });
                Assert.That(loan, Has.Property(nameof(CPageData.IncomeSources)).Count.EqualTo(6));
                Assert.That(app, Has.Property(nameof(CAppData.aOtherIncomeList)).Count.EqualTo(4));
                Assert.That(app.aOtherIncomeList.Select(o => o.Amount), Is.EquivalentTo(new[] { 123.45M, 234.56M, 456.78M, 567.89M, }));
                Assert.That(loan.IncomeSources.Values.Select(o => (decimal?)o.MonthlyAmountData), Is.EquivalentTo(new[] { 001.23M, 012.34M, 123.45M, 234.56M, 456.78M, 567.89M, }));
                Assert.That(app, Has.Property(nameof(CAppData.aTotOI)).EqualTo(1382.68M));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                app = loan.GetAppData(0);
                Assert.That(app, Has.Property(nameof(CAppData.aTotOI)).EqualTo(1382.68M));
                loan.IncomeSources.Values.Single(kvp => kvp.MonthlyAmountData == 234.56M).MonthlyAmountData = 231.86M;
                Assert.That(app.aOtherIncomeList.Select(o => o.Amount), Is.EquivalentTo(new[] { 123.45M, 231.86M, 456.78M, 567.89M, }));
                loan.Save();

                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();
                app = loan.GetAppData(0);
                Assert.That(loan.IncomeSources.Values.Select(o => (decimal?)o.MonthlyAmountData), Is.EquivalentTo(new[] { 001.23M, 012.34M, 123.45M, 231.86M, 456.78M, 567.89M, }));
                Assert.That(app.aOtherIncomeList.Select(o => o.Amount), Is.EquivalentTo(new[] { 123.45M, 231.86M, 456.78M, 567.89M, }));
            }
        }

        [TestCaseSource(nameof(allCollectionTypes))]
        public void GetLegacyFixedFieldsAndReps_ParameterizedCollectionMode_Works(E_sLqbCollectionT datalayerMode)
        {
            using (var loanForTest = CreateLoan(datalayerMode))
            {
                var loanBefore = loanForTest.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var appBefore = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                if (loanBefore.sIsIncomeCollectionEnabled)
                {
                    var borrConsumerId = appBefore.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    var coboConsumerId = appBefore.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    loanBefore.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr1BaseIncome, });
                    loanBefore.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr1OvertimeIncome, });
                    loanBefore.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr1BonusesIncome, });
                    loanBefore.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr1CommissionIncome, });
                    loanBefore.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr1DividendIncome, });
                    loanBefore.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo1BaseIncome, });
                    loanBefore.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo1OvertimeIncome, });
                    loanBefore.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo1BonusesIncome, });
                    loanBefore.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo1CommissionIncome, });
                    loanBefore.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo1DividendIncome, });
                }
                else
                {
                    appBefore.aBBaseI = Borr1BaseIncome;
                    appBefore.aBOvertimeI = Borr1OvertimeIncome;
                    appBefore.aBBonusesI = Borr1BonusesIncome;
                    appBefore.aBCommisionI = Borr1CommissionIncome;
                    appBefore.aBDividendI = Borr1DividendIncome;
                    appBefore.aCBaseI = CoBo1BaseIncome;
                    appBefore.aCOvertimeI = CoBo1OvertimeIncome;
                    appBefore.aCBonusesI = CoBo1BonusesIncome;
                    appBefore.aCCommisionI = CoBo1CommissionIncome;
                    appBefore.aCDividendI = CoBo1DividendIncome;
                }

                loanBefore.Save();

                var loanAfter = loanForTest.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);

                // Act is the get that happens inside the assert
                AssertFixedFieldValues(appAfter);
                AssertFixedFieldRepValues(appAfter);
                Assert.That(appAfter, Has.Property(nameof(CAppData.aBTotIPe)).EqualTo(Borr1TotalIncome));
                Assert.That(appAfter, Has.Property(nameof(CAppData.aBTotIPe_rep)).EqualTo(Borr1TotalIncomeRep));
                Assert.That(appAfter, Has.Property(nameof(CAppData.aCTotIPe)).EqualTo(CoBo1TotalIncome));
                Assert.That(appAfter, Has.Property(nameof(CAppData.aCTotIPe_rep)).EqualTo(CoBo1TotalIncomeRep));
            }
        }

        [Test]
        public void GetLegacyFixedFieldsAndReps_LoanWithMixedIncomeOwnership_ReturnsPrimaryIncomesOnly()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.Save();
                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();
                var app = loan.GetAppData(0);
                var borrConsumerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coboConsumerId = app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var borrBaseI = loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr1BaseIncome, });
                var borrOverI = loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr1OvertimeIncome, });
                var borrBonuI = loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr1BonusesIncome, });
                var borrCommI = loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr1CommissionIncome, });
                var borrDiviI = loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr1DividendIncome, });
                var coboBaseI = loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo1BaseIncome, });
                var coboOverI = loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo1OvertimeIncome, });
                var coboBonuI = loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo1BonusesIncome, });
                var coboCommI = loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo1CommissionIncome, });
                var coboDiviI = loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo1DividendIncome, });
                loan.AddIncomeSourceOwnership(coboConsumerId, borrBaseI);
                loan.AddIncomeSourceOwnership(coboConsumerId, borrOverI);
                loan.AddIncomeSourceOwnership(coboConsumerId, borrBonuI);
                loan.AddIncomeSourceOwnership(coboConsumerId, borrCommI);
                loan.AddIncomeSourceOwnership(coboConsumerId, borrDiviI);
                loan.AddIncomeSourceOwnership(borrConsumerId, coboBaseI);
                loan.AddIncomeSourceOwnership(borrConsumerId, coboOverI);
                loan.AddIncomeSourceOwnership(borrConsumerId, coboBonuI);
                loan.AddIncomeSourceOwnership(borrConsumerId, coboCommI);
                loan.AddIncomeSourceOwnership(borrConsumerId, coboDiviI);
                Assert.AreEqual(10, loan.IncomeSources.Count);
                Dictionary<DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>, IncomeSource> borrIncomeSources = CPageBaseExtractor.Extract(loan).loanLqbCollectionContainer.GetIncomeSourcesForConsumer(borrConsumerId).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                Dictionary<DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>, IncomeSource> coboIncomeSources = CPageBaseExtractor.Extract(loan).loanLqbCollectionContainer.GetIncomeSourcesForConsumer(coboConsumerId).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                Assert.AreEqual(10, borrIncomeSources.Count);
                Assert.AreEqual(10, coboIncomeSources.Count);

                // Act is the get that happens inside the assert
                AssertFixedFieldValues(app);
                AssertFixedFieldRepValues(app);
            }
        }

        [Test]
        public void GetLegacyFixedFields_LoanWithMultipleIncomesPerType_ReturnsSumOfIncomes()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.Save();
                loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitLoad();
                var app = loan.GetAppData(0);
                var borrConsumerId = app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coboConsumerId = app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr1BaseIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr1OvertimeIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr1BonusesIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr1CommissionIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr1DividendIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo1BaseIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo1OvertimeIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo1BonusesIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo1CommissionIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo1DividendIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = Borr2BaseIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = Borr2OvertimeIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = Borr2BonusesIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = Borr2CommissionIncome, });
                loan.AddIncomeSource(borrConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = Borr2DividendIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = CoBo2BaseIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = CoBo2OvertimeIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = CoBo2BonusesIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.Commission, MonthlyAmountData = CoBo2CommissionIncome, });
                loan.AddIncomeSource(coboConsumerId, new IncomeSource { IncomeType = IncomeType.DividendsOrInterest, MonthlyAmountData = CoBo2DividendIncome, });
                Assert.AreEqual(20, loan.IncomeSources.Count);
                Dictionary<DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>, IncomeSource> borrIncomeSources = CPageBaseExtractor.Extract(loan).loanLqbCollectionContainer.GetIncomeSourcesForConsumer(borrConsumerId).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                Dictionary<DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>, IncomeSource> coboIncomeSources = CPageBaseExtractor.Extract(loan).loanLqbCollectionContainer.GetIncomeSourcesForConsumer(coboConsumerId).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                Assert.AreEqual(10, borrIncomeSources.Count);
                Assert.AreEqual(10, coboIncomeSources.Count);

                // Act is the get that happens inside the assert
                Assert.That(app, Has.Property(nameof(CAppData.aBBaseI)).EqualTo(Borr1BaseIncome + Borr2BaseIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBOvertimeI)).EqualTo(Borr1OvertimeIncome + Borr2OvertimeIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBBonusesI)).EqualTo(Borr1BonusesIncome + Borr2BonusesIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBCommisionI)).EqualTo(Borr1CommissionIncome + Borr2CommissionIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBDividendI)).EqualTo(Borr1DividendIncome + Borr2DividendIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCBaseI)).EqualTo(CoBo1BaseIncome + CoBo2BaseIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCOvertimeI)).EqualTo(CoBo1OvertimeIncome + CoBo2OvertimeIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCBonusesI)).EqualTo(CoBo1BonusesIncome + CoBo2BonusesIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCCommisionI)).EqualTo(CoBo1CommissionIncome + CoBo2CommissionIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCDividendI)).EqualTo(CoBo1DividendIncome + CoBo2DividendIncome));
            }
        }

        /// <remarks>
        /// <c>LoanWithoutIncomeCollection</c> is covered by <see cref="GetLegacyFixedFieldsAndReps_SimpleValues_Works(string)"/>.
        /// </remarks>
        [Test]
        public void SetLegacyFixedFields_LoanWithIncomeCollection_ThrowsException()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                Assert.IsTrue(loan.sIsIncomeCollectionEnabled);
                var app = loan.GetAppData(0);
                AssertFixedFieldValues(app, 0.00M);
                AssertFixedFieldRepValues(app, "$0.00");

                Assert.Catch<CBaseException>(() => app.aBBaseI = Borr1BaseIncome);
                Assert.Catch<CBaseException>(() => app.aBOvertimeI = Borr1OvertimeIncome);
                Assert.Catch<CBaseException>(() => app.aBBonusesI = Borr1BonusesIncome);
                Assert.Catch<CBaseException>(() => app.aBCommisionI = Borr1CommissionIncome);
                Assert.Catch<CBaseException>(() => app.aBDividendI = Borr1DividendIncome);
                Assert.Catch<CBaseException>(() => app.aCBaseI = CoBo1BaseIncome);
                Assert.Catch<CBaseException>(() => app.aCOvertimeI = CoBo1OvertimeIncome);
                Assert.Catch<CBaseException>(() => app.aCBonusesI = CoBo1BonusesIncome);
                Assert.Catch<CBaseException>(() => app.aCCommisionI = CoBo1CommissionIncome);
                Assert.Catch<CBaseException>(() => app.aCDividendI = CoBo1DividendIncome);

                AssertFixedFieldValues(app, 0.00M);
                AssertFixedFieldRepValues(app, "$0.00");
            }
        }

        [Test]
        public void SetLegacyFixedFieldReps_LoanWithoutIncomeCollection_SetsValues()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                Assert.IsFalse(loan.sIsIncomeCollectionEnabled);
                var app = loan.GetAppData(0);
                AssertFixedFieldValues(app, 0.00M);
                AssertFixedFieldRepValues(app, "$0.00");

                app.aBBaseI_rep = Borr1BaseIncomeRep;
                app.aBOvertimeI_rep = Borr1OvertimeIncomeRep;
                app.aBBonusesI_rep = Borr1BonusesIncomeRep;
                app.aBCommisionI_rep = Borr1CommissionIncomeRep;
                app.aBDividendI_rep = Borr1DividendIncomeRep;
                app.aCBaseI_rep = CoBo1BaseIncomeRep;
                app.aCOvertimeI_rep = CoBo1OvertimeIncomeRep;
                app.aCBonusesI_rep = CoBo1BonusesIncomeRep;
                app.aCCommisionI_rep = CoBo1CommissionIncomeRep;
                app.aCDividendI_rep = CoBo1DividendIncomeRep;

                AssertFixedFieldValues(app);
                AssertFixedFieldRepValues(app);
                loan.Save();

                var loanAfter = loanForTest.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertFixedFieldValues(loanAfter.GetAppData(0));
            }
        }

        [Test]
        public void SetLegacyFixedFieldReps_LoanWithIncomeCollection_ThrowsException()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = loanForTest.CreateNewPageDataWithBypass();
                loan.InitSave();
                Assert.IsTrue(loan.sIsIncomeCollectionEnabled);
                var app = loan.GetAppData(0);

                Assert.Catch<CBaseException>(() => app.aBBaseI_rep = Borr1BaseIncomeRep);
                Assert.Catch<CBaseException>(() => app.aBOvertimeI_rep = Borr1OvertimeIncomeRep);
                Assert.Catch<CBaseException>(() => app.aBBonusesI_rep = Borr1BonusesIncomeRep);
                Assert.Catch<CBaseException>(() => app.aBCommisionI_rep = Borr1CommissionIncomeRep);
                Assert.Catch<CBaseException>(() => app.aBDividendI_rep = Borr1DividendIncomeRep);
                Assert.Catch<CBaseException>(() => app.aCBaseI_rep = CoBo1BaseIncomeRep);
                Assert.Catch<CBaseException>(() => app.aCOvertimeI_rep = CoBo1OvertimeIncomeRep);
                Assert.Catch<CBaseException>(() => app.aCBonusesI_rep = CoBo1BonusesIncomeRep);
                Assert.Catch<CBaseException>(() => app.aCCommisionI_rep = CoBo1CommissionIncomeRep);
                Assert.Catch<CBaseException>(() => app.aCDividendI_rep = CoBo1DividendIncomeRep);

                AssertFixedFieldValues(app, 0.00M);
                AssertFixedFieldRepValues(app, "$0.00");
            }
        }

        private static void AssertFixedFieldValues(CAppData app, decimal? valueForAllFields = null, E_BorrowerModeT? borrowerMode = null)
        {
            if (borrowerMode != E_BorrowerModeT.Coborrower)
            {
                Assert.That(app, Has.Property(nameof(CAppData.aBBaseI)).EqualTo(valueForAllFields ?? Borr1BaseIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBOvertimeI)).EqualTo(valueForAllFields ?? Borr1OvertimeIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBBonusesI)).EqualTo(valueForAllFields ?? Borr1BonusesIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBCommisionI)).EqualTo(valueForAllFields ?? Borr1CommissionIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aBDividendI)).EqualTo(valueForAllFields ?? Borr1DividendIncome));
            }

            if (borrowerMode != E_BorrowerModeT.Borrower)
            {
                Assert.That(app, Has.Property(nameof(CAppData.aCBaseI)).EqualTo(valueForAllFields ?? CoBo1BaseIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCOvertimeI)).EqualTo(valueForAllFields ?? CoBo1OvertimeIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCBonusesI)).EqualTo(valueForAllFields ?? CoBo1BonusesIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCCommisionI)).EqualTo(valueForAllFields ?? CoBo1CommissionIncome));
                Assert.That(app, Has.Property(nameof(CAppData.aCDividendI)).EqualTo(valueForAllFields ?? CoBo1DividendIncome));
            }
        }

        private static void AssertFixedFieldRepValues(CAppData app, string valueForAllFields = null, E_BorrowerModeT? borrowerMode = null)
        {
            if (borrowerMode != E_BorrowerModeT.Coborrower)
            {
                Assert.That(app, Has.Property(nameof(CAppData.aBBaseI_rep)).EqualTo(valueForAllFields ?? Borr1BaseIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aBOvertimeI_rep)).EqualTo(valueForAllFields ?? Borr1OvertimeIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aBBonusesI_rep)).EqualTo(valueForAllFields ?? Borr1BonusesIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aBCommisionI_rep)).EqualTo(valueForAllFields ?? Borr1CommissionIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aBDividendI_rep)).EqualTo(valueForAllFields ?? Borr1DividendIncomeRep));
            }

            if (borrowerMode != E_BorrowerModeT.Coborrower)
            {
                Assert.That(app, Has.Property(nameof(CAppData.aCBaseI_rep)).EqualTo(valueForAllFields ?? CoBo1BaseIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aCOvertimeI_rep)).EqualTo(valueForAllFields ?? CoBo1OvertimeIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aCBonusesI_rep)).EqualTo(valueForAllFields ?? CoBo1BonusesIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aCCommisionI_rep)).EqualTo(valueForAllFields ?? CoBo1CommissionIncomeRep));
                Assert.That(app, Has.Property(nameof(CAppData.aCDividendI_rep)).EqualTo(valueForAllFields ?? CoBo1DividendIncomeRep));
            }
        }

        private static LoanForIntegrationTest CreateLoan(E_sLqbCollectionT datalayerMode)
        {
            var tempLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false);
            try
            {
                var loan = tempLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.EnsureMigratedToAtLeastUladCollectionVersion(datalayerMode);
                Assert.That(loan, Has.Property(nameof(CPageData.sBorrowerApplicationCollectionT)).EqualTo(datalayerMode));
                loan.Save();

                return tempLoan;
            }
            catch
            {
                tempLoan?.Dispose();
                throw;
            }
        }
    }
}
