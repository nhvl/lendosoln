﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::DataAccess;
    using global::LendingQB.Test.Developers.Utils;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class VirtualLegacyApplicationConsumerAssociationTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankLoanFile());
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [TearDown]
        public void TearDown()
        {
            var loan = this.GetLoan();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                int numApps = loan.nApps;

                if (numApps > 1)
                {
                    for (int i = 1; i < numApps; ++i)
                    {
                        loan.DelApp(i);
                        loan.InitSave(ConstAppDavid.SkipVersionCheck);
                    }
                }
            }
        }

        [Test]
        public void LegacyApplicationConsumers_BlankFileNoCoborrowerDefined_ReturnsAssociationSetWithSingleRecord()
        {
            var loan = this.GetLoan();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                loan.InitLoad();

                var app = loan.GetAppData(0);
                var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(app.aAppId);
                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(app.aBConsumerId);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(app.aCConsumerId);
                var associationSet = loan.LegacyApplicationConsumers;

                Assert.AreEqual(1, associationSet.Count);
                Assert.AreEqual(1, associationSet.Values.Count(a => a.ConsumerId == borrowerId));
                Assert.AreEqual(true, associationSet.Values.All(a => a.LegacyApplicationId == appId));
            }
        }

        [Test]
        public void LegacyApplicationConsumers_TwoApplicationsWithCoborrowerDefined_ReturnsAssociationSetWithFourRecords()
        {
            var loan = this.GetLoan();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                loan.AddNewApp();
                loan.InitLoad();
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(1).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                var associationSet = loan.LegacyApplicationConsumers;

                Assert.AreEqual(4, associationSet.Count);
            }
        }

        [Test]
        public void LegacyApplicationConsumers_Always_UsesConsumerIdAsAssociationKey()
        {
            var loan = this.GetLoan();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                loan.InitLoad();

                var app = loan.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(app.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());

                var borrowerAssociationId = DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>.Create(app.aBConsumerId);
                var coborrowerAssociationId = DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>.Create(app.aCConsumerId);
                var associationSet = loan.LegacyApplicationConsumers;

                Assert.AreEqual(true, associationSet.ContainsKey(borrowerAssociationId));
                Assert.AreEqual(true, associationSet.ContainsKey(coborrowerAssociationId));
            }
        }

        [Test]
        public void LegacyApplicationConsumers_Always_IndicatesPrimaryBorrowerCorrectly()
        {
            var loan = this.GetLoan();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                loan.InitLoad();

                var app = loan.GetAppData(0);
                loan.AddCoborrowerToLegacyApplication(app.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(app.aAppId);
                var borrowerAssociationId = DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>.Create(app.aBConsumerId);
                var coborrowerAssociationId = DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>.Create(app.aCConsumerId);
                var associationSet = loan.LegacyApplicationConsumers;

                var primaryAssociation = associationSet[borrowerAssociationId];
                Assert.AreEqual(true, primaryAssociation.IsPrimary);
                var coborrowerAssociation = associationSet[coborrowerAssociationId];
                Assert.AreEqual(false, coborrowerAssociation.IsPrimary);
            }
        }

        private CFullAccessPageData GetLoan()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                return CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(VirtualLegacyApplicationConsumerAssociationTest));
            }
        }

    }
}
