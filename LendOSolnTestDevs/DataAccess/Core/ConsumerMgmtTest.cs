﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data.Common;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ConsumerMgmtTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var creator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankUladLoanFile();
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                if (this.loanId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(this.principal, this.loanId, false, false);
                }
            }
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void SimpleAddCoborrower_WithSyncFlag(bool sync)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);
                loan.sSyncUladAndLegacyApps = sync;

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var appData = loan.GetAppData(0);
                var appId = appData.aAppId;
                var borrowerId = appData.aBConsumerId;
                var coborrowerId = appData.aCConsumerId;
                Assert.AreEqual(1, loan.Consumers.Count);
                Assert.AreEqual(1, loan.LegacyApplications.Count);
                Assert.AreEqual(1, loan.LegacyApplicationConsumers.Count);
                Assert.AreEqual(1, loan.UladApplications.Count);
                Assert.AreEqual(1, loan.UladApplicationConsumers.Count);

                var returnedCoborrowerId = aggregate.AddCoborrowerToLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appId));
                this.SaveLoan(loan);

                Assert.AreEqual(coborrowerId, returnedCoborrowerId.Value);
                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);
                loan.sSyncUladAndLegacyApps = sync;
                appData = loan.GetAppData(0);
                Assert.AreEqual(appId, appData.aAppId);
                Assert.AreEqual(borrowerId, appData.aBConsumerId);
                Assert.AreEqual(coborrowerId, appData.aCConsumerId);
                Assert.AreEqual(2, loan.Consumers.Count);
                Assert.AreEqual(1, loan.LegacyApplications.Count);
                Assert.AreEqual(2, loan.LegacyApplicationConsumers.Count);
                Assert.AreEqual(1, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                bool borrowerSeen = false;
                bool coborrowerSeen = false;
                foreach (var assoc in loan.LegacyApplicationConsumers.Values)
                {
                    if (assoc.ConsumerId.Value == borrowerId)
                    {
                        borrowerSeen = true;
                    }
                    else if (assoc.ConsumerId.Value == coborrowerId)
                    {
                        coborrowerSeen = true;
                    }
                }

                Assert.IsTrue(borrowerSeen);
                Assert.IsTrue(coborrowerSeen);

                borrowerSeen = false;
                coborrowerSeen = false;
                foreach (var assoc in loan.UladApplicationConsumers.Values)
                {
                    if (assoc.ConsumerId.Value == borrowerId)
                    {
                        borrowerSeen = true;
                    }
                    else if (assoc.ConsumerId.Value == coborrowerId)
                    {
                        coborrowerSeen = true;
                    }
                }

                Assert.IsTrue(borrowerSeen);
                Assert.IsTrue(coborrowerSeen);
            }
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void WhileExists_TryAddCoborrower_WithSyncFlag(bool sync)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);
                loan.sSyncUladAndLegacyApps = sync;
                var appData = loan.GetAppData(0);

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var appId = appData.aAppId;
                var borrowerId = appData.aBConsumerId;
                var coborrowerId = appData.aCConsumerId;
                Assert.AreNotEqual(borrowerId, coborrowerId);

                var returnedCoborrowerId = aggregate.AddCoborrowerToLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appId));
                this.SaveLoan(loan);

                Assert.AreEqual(coborrowerId, returnedCoborrowerId.Value);
                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);
                loan.sSyncUladAndLegacyApps = sync;
                appData = loan.GetAppData(0);
                Assert.AreEqual(sync, loan.sSyncUladAndLegacyApps);
                Assert.IsTrue(appData.aHasCoborrower);
                Assert.AreEqual(appId, appData.aAppId);
                Assert.AreEqual(borrowerId, appData.aBConsumerId);
                Assert.AreEqual(coborrowerId, appData.aCConsumerId);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);

                Assert.Throws<CBaseException>(() => aggregate.AddCoborrowerToLegacyApplication(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appId)));
            }
        }

        [Test]
        public void RemoveBorrower_OnlyOneBorrower_Throws()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                var appData = loan.GetAppData(0);
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                Assert.Throws<CBaseException>(() => aggregate.RemoveBorrower(consumerId));
            }
        }

        [Test]
        public void RemoveBorrower_InvalidConsumerId_Throws()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loanForUpdate = CreateLoan(this.loanId);
                InitSaveLoan(loanForUpdate);
                var aggregateForUpdate = this.ExtractAggregate(this.ExtractLoanData(loanForUpdate));
                aggregateForUpdate.AddCoborrowerToLegacyApplication(loanForUpdate.LegacyApplications.Single().Key);
                SaveLoan(loanForUpdate);

                var loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                var appData = loan.GetAppData(0);
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var invalidId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
                Assert.Throws<InvalidOperationException>(() => aggregate.RemoveBorrower(invalidId));
            }
        }

        [Test]
        public void AttemptRemoveCoborrowerWhenNotDefined_Throws()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var secondAppId = this.CreateSecondApplication(true);

                var loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                var appData = loan.GetAppData(1);
                appData.aHasCoborrowerData = false; // this means the coborrower is associated with a ulad application but now isn't considered as defined

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);
                Assert.IsFalse(appData.aHasCoborrower);
                Assert.Throws<CBaseException>(() => aggregate.RemoveBorrower(coborrowerId));
            }
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void RemoveBorrower_SingleLegacy_SingleUlad(bool sync)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                // There is a single borrower on the legacy application, and a ULAD application is associated only with that borrower.
                // Deleting the borrower should result in the deletion of the legacy and ULAD applications.
                var secondAppId = this.CreateSecondApplication(false);

                var loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                Assert.AreEqual(2, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(2, aggregate.UladApplicationConsumers.Count);

                var firstAppData = loanData.GetAppData(0);
                var secondAppData = loanData.GetAppData(1);
                Assert.IsFalse(firstAppData.aHasCoborrower);
                Assert.IsFalse(secondAppData.aHasCoborrower);

                var deletedBorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondAppData.aBConsumerId);
                RemoveBorrower(aggregate, deletedBorrowerId);
                // NOTE: don't call Save() after RemoveBorrower()

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);

                Assert.AreEqual(1, loanData.nApps);
                Assert.AreEqual(1, aggregate.UladApplications.Count);
                Assert.AreEqual(1, aggregate.UladApplicationConsumers.Count);

                firstAppData = loanData.GetAppData(0);
                Assert.IsFalse(firstAppData.aHasCoborrower);
                Assert.AreNotEqual(deletedBorrowerId, firstAppData.aBConsumerId);
            }
        }

        [Test]
        public void RemoveBorrower_SingleLegacy_JointUlad_BorrowerIsUladPrimary()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                // Two legacy applications each with a single borrower.
                // One ULAD application associated with the two borrowers from the two legacy applications, with the primary being the first borrower.
                // Deleting the first borrower results in the deletion of the first legacy application, but not the ULAD application.
                // The primary changes for the ULAD application.
                var secondAppId = this.CreateSecondApplication(false);

                var loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);
                var uladAppId = loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId.Value == secondAppId).UladApplicationId;
                var thirdAppId = this.AddNewLegacyApplicationToExistingUladApplication(uladAppId, false);

                loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var secondAppData = loanData.GetAppData(secondAppId);
                var thirdAppData = loan.GetAppData(thirdAppId);
                var secondUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.LegacyAppId.Value == secondAppId).Value;
                var thirdUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.LegacyAppId.Value == thirdAppId).Value;

                Assert.AreEqual(3, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(3, aggregate.UladApplicationConsumers.Count);
                Assert.AreEqual(secondAppData.aBConsumerId, secondUladAssociation.ConsumerId.Value);
                Assert.AreEqual(thirdAppData.aBConsumerId, thirdUladAssociation.ConsumerId.Value);
                Assert.AreEqual(secondUladAssociation.UladApplicationId, thirdUladAssociation.UladApplicationId);
                Assert.IsTrue(secondUladAssociation.IsPrimary);
                Assert.IsFalse(thirdUladAssociation.IsPrimary);

                var deletedBorrowerId = secondUladAssociation.ConsumerId;
                RemoveBorrower(aggregate, deletedBorrowerId);
                // NOTE: don't call Save() after RemoveBorrower()

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);
                thirdUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.LegacyAppId.Value == thirdAppId).Value;

                Assert.AreEqual(2, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(2, aggregate.UladApplicationConsumers.Count);
                Assert.IsFalse(aggregate.UladApplicationConsumers.Where(a => a.Value.ConsumerId == deletedBorrowerId).Any());
                Assert.IsTrue(thirdUladAssociation.IsPrimary);
            }
        }

        [Test]
        public void RemoveBorrower_SingleLegacy_JointUlad_BorrowerIsNotUladPrimary()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                // Two legacy applications each with a single borrower.
                // One ULAD application associated with the two borrowers from the two legacy applications, with the primary being the second borrower.
                // Deleting the first borrower results in the deletion of the first legacy application, but not the ULAD application.
                // The primary remains the same for the ULAD application.
                var secondAppId = this.CreateSecondApplication(false);

                var loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);
                var uladAppId = loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId.Value == secondAppId).UladApplicationId;
                var thirdAppId = this.AddNewLegacyApplicationToExistingUladApplication(uladAppId, false);

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var secondAppData = loanData.GetAppData(secondAppId);
                var thirdAppData = loan.GetAppData(thirdAppId);
                var secondUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.LegacyAppId.Value == secondAppId).Value;
                var thirdUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.LegacyAppId.Value == thirdAppId).Value;

                loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);

                Assert.AreEqual(3, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(3, aggregate.UladApplicationConsumers.Count);
                Assert.AreEqual(secondAppData.aBConsumerId, secondUladAssociation.ConsumerId.Value);
                Assert.AreEqual(thirdAppData.aBConsumerId, thirdUladAssociation.ConsumerId.Value);
                Assert.AreEqual(secondUladAssociation.UladApplicationId, thirdUladAssociation.UladApplicationId);
                Assert.IsTrue(secondUladAssociation.IsPrimary);
                Assert.IsFalse(thirdUladAssociation.IsPrimary);

                var deletedBorrowerId = thirdUladAssociation.ConsumerId;
                RemoveBorrower(aggregate, deletedBorrowerId);
                // NOTE: don't call Save() after RemoveBorrower()

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);
                secondUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.LegacyAppId.Value == secondAppId).Value;

                Assert.AreEqual(2, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(2, aggregate.UladApplicationConsumers.Count);
                Assert.IsFalse(aggregate.UladApplicationConsumers.Where(a => a.Value.ConsumerId == deletedBorrowerId).Any());
                Assert.IsTrue(secondUladAssociation.IsPrimary);
            }
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void RemoveBorrower_JointLegacy_JointUlad_BorrowerIsUladPrimary(bool sync)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                // A single legacy application with two borrowers.
                // A single ULAD application associated with both borrowers, with the first borrower as the primary.
                // Removing the borrower causes a borrower sway, the ULAD application survives with the coborrower now the primary consumer.
                var secondAppId = this.CreateSecondApplication(true);

                var loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var firstAppData = loanData.GetAppData(0);
                var secondAppData = loanData.GetAppData(1);
                Assert.IsFalse(firstAppData.aHasCoborrower);
                Assert.IsTrue(secondAppData.aHasCoborrower);

                var secondBorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondAppData.aBConsumerId);
                var secondCoborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondAppData.aCConsumerId);

                var secondUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == secondBorrowerId).Value;
                var thirdUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == secondCoborrowerId).Value;

                Assert.AreEqual(2, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(3, aggregate.UladApplicationConsumers.Count);
                Assert.IsTrue(secondUladAssociation.IsPrimary);
                Assert.IsFalse(thirdUladAssociation.IsPrimary);

                var deletedBorrowerId = secondBorrowerId;
                RemoveBorrower(aggregate, deletedBorrowerId);
                // NOTE: don't call Save() after RemoveBorrower()

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);
                secondAppData = loanData.GetAppData(1);

                Assert.AreEqual(2, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(2, aggregate.UladApplicationConsumers.Count);
                Assert.IsFalse(aggregate.UladApplicationConsumers.Where(a => a.Value.ConsumerId == deletedBorrowerId).Any());
                Assert.IsFalse(secondAppData.aHasCoborrower);
                Assert.AreEqual(secondCoborrowerId.Value, secondAppData.aBConsumerId);

                thirdUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == secondCoborrowerId).Value;
                Assert.IsTrue(thirdUladAssociation.IsPrimary);
            }
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public void RemoveCoBorrower_JointLegacy_JointUlad_CoborrowerIsNotUladPrimary(bool sync)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                // A single legacy application with two borrowers.
                // A single ULAD application associated with both borrowers, with the first borrower as the primary.
                // Deleting the coborrower removes it from the legacy application.  The ULAD application survives but the association with the coborrower is removed.
                var secondAppId = this.CreateSecondApplication(true);

                var loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);

                var firstAppData = loanData.GetAppData(0);
                var secondAppData = loanData.GetAppData(1);
                Assert.IsFalse(firstAppData.aHasCoborrower);
                Assert.IsTrue(secondAppData.aHasCoborrower);

                var secondBorrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondAppData.aBConsumerId);
                var secondCoborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondAppData.aCConsumerId);

                var secondUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == secondBorrowerId).Value;
                var thirdUladAssociation = aggregate.UladApplicationConsumers.Single(a => a.Value.ConsumerId == secondCoborrowerId).Value;

                Assert.AreEqual(2, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(3, aggregate.UladApplicationConsumers.Count);
                Assert.IsTrue(secondUladAssociation.IsPrimary);
                Assert.IsFalse(thirdUladAssociation.IsPrimary);

                var deletedBorrowerId = secondCoborrowerId;
                RemoveBorrower(aggregate, deletedBorrowerId);
                // NOTE: don't call Save() after RemoveBorrower()

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                loanData = this.ExtractLoanData(loan);
                aggregate = this.ExtractAggregate(loanData);
                secondAppData = loanData.GetAppData(1);

                Assert.AreEqual(2, loanData.nApps);
                Assert.AreEqual(2, aggregate.UladApplications.Count);
                Assert.AreEqual(2, aggregate.UladApplicationConsumers.Count);
                Assert.IsFalse(aggregate.UladApplicationConsumers.Where(a => a.Value.ConsumerId == deletedBorrowerId).Any());
                Assert.IsFalse(secondAppData.aHasCoborrower);
            }
        }

        [Test]
        public void SingleBorrowerLegacy_AddCoborrowerViaFlag_OrderingHandledCorrectly()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                var loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                var uladAppId = loan.UladApplications.Keys.First();
                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);
                Assert.AreEqual(2, aggregate.GetConsumerOrderingForUladApplication(uladAppId).Count);
            }
        }

        private static CPageData CreateLoan(Guid loanId)
        {
            return new CPageData(loanId, nameof(UladApplicationMgmtTest), new string[] { "sSyncUladAndLegacyApps", "afDelMarriedCobor", "afSwapMarriedBorAndCobor", "UladApplications", "UladApplicationConsumers" });
        }

        private void InitLoadLoan(CPageData loan)
        {
            loan.InitLoad();
        }

        private void InitSaveLoan(CPageData loan)
        {
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        private void SaveLoan(CPageData loan)
        {
            loan.Save();
        }

        private Guid CreateSecondApplication(bool hasCoborrower)
        {
            return LoanTestUtilities.AddNewLegacyAndUladApplicationToLoan(this.loanId, hasCoborrower);
        }

        private Guid AddNewLegacyApplicationToExistingUladApplication(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId, bool hasCoborrower)
        {
            return LoanTestUtilities.AddNewLegacyApplicationToExistingUladApplication(this.loanId, uladAppId, hasCoborrower);
        }

        private void RemoveBorrower(ILoanLqbCollectionContainer aggregate, DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            aggregate.RemoveBorrower(consumerId);
        }

        private CPageBase ExtractLoanData(CPageData pageData)
        {
            return CPageBaseExtractor.Extract(pageData);
        }

        private ILoanLqbCollectionContainer ExtractAggregate(CPageBase pageBase)
        {
            return pageBase.loanLqbCollectionContainer;
        }
    }
}
