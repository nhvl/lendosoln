﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using global::DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class AbstractAppRecordCollectionMigratorTest
    {
        [Test]
        public void Migrate_SingleRegularRecord_CreatesEntityAndAdds()
        {
            var fakeCollection = Substitute.For<IRecordCollection>();
            fakeCollection.CountRegular.Returns(1);
            var migrator = new TestAppRecordCollectionMigrator(fakeCollection);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, migrator.AddCount);
            Assert.AreEqual(1, migrator.CreateCount);
        }

        [Test]
        public void Migrate_SingleSpecialRecord_CreatesEntityAndAdds()
        {
            var fakeCollection = Substitute.For<IRecordCollection>();
            fakeCollection.CountSpecial.Returns(1);
            var migrator = new TestAppRecordCollectionMigrator(fakeCollection);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, migrator.AddCount);
            Assert.AreEqual(1, migrator.CreateCount);
        }

        [Test]
        public void Migrate_SingleRegularSingleSpecial_CreatesAndAddsBoth()
        {
            var fakeCollection = Substitute.For<IRecordCollection>();
            fakeCollection.CountRegular.Returns(1);
            fakeCollection.GetRegularRecordAt(Arg.Any<int>()).RecordId.Returns(Guid.NewGuid());
            fakeCollection.CountSpecial.Returns(1);
            fakeCollection.GetSpecialRecordAt(Arg.Any<int>()).RecordId.Returns(Guid.NewGuid());
            var migrator = new TestAppRecordCollectionMigrator(fakeCollection);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(2, migrator.AddCount);
            Assert.AreEqual(2, migrator.CreateCount);
        }

        [Test]
        public void Migrate_SingleRegularSingleSpecial_ReturnsExpectedIdMap()
        {
            var regularId = new Guid("11111111-1111-1111-1111-111111111111");
            var specialId = new Guid("22222222-2222-2222-2222-222222222222");
            var fakeCollection = Substitute.For<IRecordCollection>();
            fakeCollection.CountRegular.Returns(1);
            fakeCollection.GetRegularRecordAt(Arg.Any<int>()).RecordId.Returns(regularId);
            fakeCollection.CountSpecial.Returns(1);
            fakeCollection.GetSpecialRecordAt(Arg.Any<int>()).RecordId.Returns(specialId);
            var migrator = new TestAppRecordCollectionMigrator(fakeCollection);

            var idMap = migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.IsTrue(idMap.ContainsKey(regularId));
            Assert.IsTrue(idMap.ContainsKey(specialId));
        }
    }

    internal class TestEntity
    {
    }

    internal class TestKind : DataObjectKind
    {
    }

    internal class TestAppRecordCollectionMigrator : AbstractAppRecordCollectionMigrator<IRecordCollection, ICollectionItemBase2, TestEntity, TestKind>
    {
        public TestAppRecordCollectionMigrator(IRecordCollection collection)
            : base(collection)
        {
        }

        public int AddCount { get; private set; } 

        public int CreateCount { get; private set; } 

        protected override DataObjectIdentifier<TestKind, Guid> AddEntity(ICollectionItemBase2 legacyRecord, TestEntity entity)
        {
            ++this.AddCount;
            return Guid.NewGuid().ToIdentifier<TestKind>();
        }

        protected override TestEntity CreateEntity(ICollectionItemBase2 legacyRecord)
        {
            ++this.CreateCount;
            return new TestEntity();
        }
    }
}
