﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Linq;
    using System.Data.SqlClient;
    using global::DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class EnsureLogicalConstraintsTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

                var creator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankUladLoanFile();
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                if (this.loanId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(this.principal, this.loanId, false, false); // calls Save() on the loan 1 time!
                }
            }
        }

        [Test]
        public void LoadInitialLoan_DoNothing_VerifyAssumedState()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                Assert.AreEqual(1, loan.nApps);

                var appData = loan.GetAppData(0);
                Assert.IsFalse(appData.aHasCoborrower);

                Assert.AreEqual(1, loan.UladApplications.Count);
                Assert.AreEqual(1, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void CreateLoanWithTwoUladApps_ManuallyEmptySecondUladApp_SaveRemovesEmptyUladApp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(false);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan's initial correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                var appData = loan.GetAppData(1);
                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                this.RemoveUladToConsumerAssociation(consumerId);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(1, loan.UladApplicationConsumers.Count);

                this.SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void CreateLoanWithTwoUladApps_AddBadAssociationToSecondUladApp_SaveRemovesBadAssociation()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(false);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan's initial correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                var appData = loan.GetAppData(1);
                var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appData.aAppId);
                var badConsumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);
                this.AddUladToConsumerAssociation(appId, badConsumerId);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

                this.SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void CreateLoanWithTwoUladApps_NoPrimaryUladApplication_SaveAssignsPrimary()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(false);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan's initial correct state
                Assert.AreEqual(1, loan.UladApplications.Values.Count(a => a.IsPrimary));

                var uladAppId = loan.UladApplications.Single(a => a.Value.IsPrimary).Key;
                this.SetPrimaryFlag(uladAppId, false);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(0, loan.UladApplications.Values.Count(a => a.IsPrimary));

                this.SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(1, loan.UladApplications.Values.Count(a => a.IsPrimary));

                var assignedPrimaryUladAppId = loan.UladApplications.Single(a => a.Value.IsPrimary).Key;
                Assert.AreEqual(uladAppId, assignedPrimaryUladAppId);
            }
        }

        [Test]
        public void CreateLoanWithTwoUladApps_SecondHasNoPrimaryBorrower_SaveAssignsPrimary()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(true);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                var appData = loan.GetAppData(1);
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appData.aAppId);
                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);

                // Verify the loan's initial correct state
                Assert.AreEqual(1, loan.UladApplicationConsumers.Values.Count(a => a.LegacyAppId == legacyAppId && a.IsPrimary));
                Assert.AreEqual(borrowerId, loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId == legacyAppId && a.IsPrimary).ConsumerId);
                this.SetPrimaryFlag(borrowerId, false);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(0, loan.UladApplicationConsumers.Values.Count(a => a.LegacyAppId == legacyAppId && a.IsPrimary));

                this.SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(1, loan.UladApplicationConsumers.Values.Count(a => a.LegacyAppId == legacyAppId && a.IsPrimary));
                Assert.AreEqual(borrowerId, loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId == legacyAppId && a.IsPrimary).ConsumerId);
            }
        }

        [Test]
        public void CreateLoanWithTwoUladApps_SecondHasBothConsumersAsPrimary_SaveLeavesOnlyBorrowerAsPrimary()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(true);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                var appData = loan.GetAppData(1);
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appData.aAppId);
                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);

                // Verify the loan's initial correct state
                Assert.AreEqual(1, loan.UladApplicationConsumers.Values.Count(a => a.LegacyAppId == legacyAppId && a.IsPrimary));
                Assert.AreEqual(borrowerId, loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId == legacyAppId && a.IsPrimary).ConsumerId);
                this.SetPrimaryFlag(coborrowerId, true);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(2, loan.UladApplicationConsumers.Values.Count(a => a.LegacyAppId == legacyAppId && a.IsPrimary));

                this.SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(1, loan.UladApplicationConsumers.Values.Count(a => a.LegacyAppId == legacyAppId && a.IsPrimary));
                Assert.AreEqual(borrowerId, loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId == legacyAppId && a.IsPrimary).ConsumerId);
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNULL_CoborrowerAssocNULL_CoborrowerDefinedFALSE_SaveCreatesNewUladAppForBorrower()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(true);

                var loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                var appData = loan.GetAppData(1);
                var uladAppId = loan.UladApplicationConsumers.Values.First(a => a.LegacyAppId.Value == appData.aAppId).UladApplicationId;

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);
                aggregate.AddConsumerToUladApplication(uladAppId); // this will keep the ulad application alive when we remove the original coborrower and the borrower association

                appData.aHasCoborrowerData = false;
                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                this.SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan's initial correct state
                Assert.AreEqual(3, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

                this.RemoveUladToConsumerAssociation(borrowerId);

                loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(3, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                SaveLoan(loan); // Since both (borrower, coborrower) were removed from the associations, a new ULAD application gets created for the borrower

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(3, loan.nApps);
                Assert.AreEqual(3, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNULL_CoborrowerAssocNULL_CoborrowerDefinedTRUE_SaveCreatesNewUladAppForBothBorrowers()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(true);

                var loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                var appData = loan.GetAppData(1);
                var uladAppId = loan.UladApplicationConsumers.Values.First(a => a.LegacyAppId.Value == appData.aAppId).UladApplicationId;

                var loanData = this.ExtractLoanData(loan);
                var aggregate = this.ExtractAggregate(loanData);
                aggregate.AddConsumerToUladApplication(uladAppId); // this will keep the ulad application alive when we remove the original borrower and coborrower associations

                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);
                this.SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan's initial correct state
                Assert.AreEqual(3, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(4, loan.UladApplicationConsumers.Count);

                this.RemoveUladToConsumerAssociation(borrowerId);
                this.RemoveUladToConsumerAssociation(coborrowerId);

                loan = CreateLoan(this.loanId);
                InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(3, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                SaveLoan(loan); // Since both (borrower, coborrower) were removed from the associations, a new ULAD application gets created for them

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(3, loan.nApps);
                Assert.AreEqual(3, loan.UladApplications.Count);
                Assert.AreEqual(4, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNULL_CoborrowerAssocNOTNULL_CoborrowerDefinedFALSE_SaveAssignsBorrowerToUladAppAndRemovesCoborrower()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(false);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                var appData = loan.GetAppData(1);
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appData.aAppId);
                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);

                this.AddUladToConsumerAssociation(legacyAppId, coborrowerId);
                this.RemoveUladToConsumerAssociation(borrowerId);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count); // looks correct, but is the wrong borrower...demonstrated by:
                Assert.AreEqual(coborrowerId, loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId == legacyAppId).ConsumerId);

                SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);
                Assert.AreEqual(borrowerId, loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId == legacyAppId).ConsumerId);
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNULL_CoborrowerAssocNOTNULL_CoborrowerDefinedTRUE_SaveAssignsBorrowerToUladApp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(true);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is in a valid state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

                var appData = loan.GetAppData(1);
                var borrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aBConsumerId);

                this.RemoveUladToConsumerAssociation(borrowerId);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNOTNULL_CoborrowerAssocNULL_CoborrowerDefinedFALSE_SaveRestoresOrder()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(false);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                var appData = loan.GetAppData(1);
                var uladAppId = loan.UladApplicationConsumers.Values.Single(a => a.LegacyAppId.Value == appData.aAppId).UladApplicationId;

                this.RemoveConsumerOrder(uladAppId);

                // Verify the loan is in an invalid state
                Assert.AreEqual(0, this.CountConsumerOrder(uladAppId));

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);
                this.SaveLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(1, this.CountConsumerOrder(uladAppId));
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNOTNULL_CoborrowerAssocNULL_CoborrowerDefinedTRUE_SaveAssignsCoorrowerToUladApp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(true);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is in a valid state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

                var appData = loan.GetAppData(1);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);

                this.RemoveUladToConsumerAssociation(coborrowerId);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNOTNULL_CoborrowerAssocNOTNULL_CoborrowerDefinedFALSE_SaveRemovesCoborrowerAssociation()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(false);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                // Verify the loan is in a valid state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);

                var appData = loan.GetAppData(1);
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(appData.aAppId);
                var coborrowerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(appData.aCConsumerId);
                this.AddUladToConsumerAssociation(legacyAppId, coborrowerId);

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);

                // Verify the loan is in a bad state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(3, loan.UladApplicationConsumers.Count);

                SaveLoan(loan);

                loan = CreateLoan(this.loanId);
                InitLoadLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(2, loan.nApps);
                Assert.AreEqual(2, loan.UladApplications.Count);
                Assert.AreEqual(2, loan.UladApplicationConsumers.Count);
            }
        }

        [Test]
        public void InvalidConsumerSetup_BorrowerAssocNOTNULL_CoborrowerAssocNOTNULL_CoborrowerDefinedTRUE_SaveRestoresOrder()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.AddNewLegacyAndUladApplicationToLoan(true);

                var loan = CreateLoan(this.loanId);
                this.InitLoadLoan(loan);

                var appData = loan.GetAppData(1);
                var uladAppId = loan.UladApplicationConsumers.Values.First(a => a.LegacyAppId.Value == appData.aAppId).UladApplicationId;

                this.RemoveConsumerOrder(uladAppId);

                // Verify the loan is in an invalid state
                Assert.AreEqual(0, this.CountConsumerOrder(uladAppId));

                loan = CreateLoan(this.loanId);
                this.InitSaveLoan(loan);
                this.SaveLoan(loan);

                // Verify the loan is restored to the correct state
                Assert.AreEqual(2, this.CountConsumerOrder(uladAppId));
            }
        }

        private static CPageData CreateLoan(Guid loanId)
        {
            return new CPageData(loanId, nameof(EnsureLogicalConstraintsTest), new string[] { "aHasCoborrower", "UladApplications", "UladApplicationConsumers" });
        }

        private void InitLoadLoan(CPageData loan)
        {
            loan.InitLoad();
        }

        private void InitSaveLoan(CPageData loan)
        {
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        private void SaveLoan(CPageData loan)
        {
            loan.Save();
        }

        private CPageBase ExtractLoanData(CPageData pageData)
        {
            return CPageBaseExtractor.Extract(pageData);
        }

        private ILoanLqbCollectionContainer ExtractAggregate(CPageBase pageBase)
        {
            return pageBase.loanLqbCollectionContainer;
        }

        private void AddNewLegacyAndUladApplicationToLoan(bool hasCoborrower)
        {
            LoanTestUtilities.AddNewLegacyAndUladApplicationToLoan(this.loanId, hasCoborrower);
        }

        private int RemoveUladToConsumerAssociation(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            // The goal is to remove an association that our code shouldn't allow to happen so we can test the repair.
            string query = "DELETE FROM [dbo].[ULAD_APPLICATION_X_CONSUMER] WHERE ConsumerId = @id;";
            return this.ExecuteSqlCall(query, this.CreateSqlParameter("@id", consumerId.Value));
        }

        private void AddUladToConsumerAssociation(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId, DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId)
        {
            // The goal is to setup a spurious association, which our code shouldn't allow to happen, so we can test the repair.
            string query = "DECLARE @uladAppId uniqueidentifier; DECLARE @loanId uniqueidentifier; SELECT TOP 1 @loanId = LoanId, @uladAppId = UladApplicationId FROM [dbo].[ULAD_APPLICATION_X_CONSUMER] WHERE LegacyAppId = @appId; INSERT INTO [dbo].[ULAD_APPLICATION_X_CONSUMER] (UladApplicationConsumerAssociationId, LoanId, UladApplicationId, ConsumerId, LegacyAppId, IsPrimary) VALUES (NEWID(), @loanId, @uladAppId, @consumerId, @appId, 0);";
            this.ExecuteSqlCall(query, this.CreateSqlParameter("@appId", legacyAppId.Value), this.CreateSqlParameter("@consumerId", consumerId.Value));
        }

        private int SetPrimaryFlag(DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId, bool value)
        {
            // The goal is to setup either zero or multiple primaries, which our code shouldn't allow to happen, so we can test the repair.
            string query = "UPDATE [dbo].[ULAD_APPLICATION_X_CONSUMER] SET IsPrimary = @isPrimary WHERE ConsumerId = @id;";
            return this.ExecuteSqlCall(query, this.CreateSqlParameter("@isPrimary", value), this.CreateSqlParameter("@id", consumerId.Value));
        }

        private int SetPrimaryFlag(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId, bool value)
        {
            // The goal is to setup either zero or multiple primaries, which our code shouldn't allow to happen, so we can test the repair.
            string query = "UPDATE [dbo].[ULAD_APPLICATION] SET IsPrimary = @isPrimary WHERE UladApplicationId = @id;";
            return this.ExecuteSqlCall(query, this.CreateSqlParameter("@isPrimary", value), this.CreateSqlParameter("@id", uladAppId.Value));
        }

        private int SetPrimaryFlag(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId, bool value)
        {
            // The goal is to setup either zero or multiple primaries, which our code shouldn't allow to happen, so we can test the repair.
            // Note that the primary legacy app has the smallest PrimaryRankIndex
            string query = null;
            if (value)
            {
                query = "DECLARE @loanId UNIQUEIDENTIFIER; SELECT @loanId = sLId FROM [dbo].[APPLICATION_A] WHERE aAppId = @id; UPDATE [dbo].[APPLICATION_A] SET PrimaryRankIndex = (SELECT MIN(PrimaryRankIndex) FROM [dbo].[APPLICATION_A] WHERE sLId = @loanId);";
            }
            else
            {
                query = "DECLARE @loanId UNIQUEIDENTIFIER; SELECT @loanId = sLId FROM [dbo].[APPLICATION_A] WHERE aAppId = @id; UPDATE [dbo].[APPLICATION_A] SET PrimaryRankIndex = 100 + (SELECT MAX(PrimaryRankIndex) FROM [dbo].[APPLICATION_A] WHERE sLId = @loanId);";
            }

            return this.ExecuteSqlCall(query, this.CreateSqlParameter("@id", legacyAppId.Value));
        }

        private int RemoveConsumerOrder(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            string query = "DELETE FROM [dbo].[LOAN_COLLECTION_SORT_ORDER] WHERE CollectionName = 'Consumer' AND UladApplicationId = @uladId;";
            return this.ExecuteSqlCall(query, this.CreateSqlParameter("@uladId", uladAppId.Value));
        }

        private int CountConsumerOrder(DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            string query = "SELECT LEN(SortOrder) - LEN(REPLACE(SortOrder, ',', '')) + 1 AS OrderCount FROM [dbo].[LOAN_COLLECTION_SORT_ORDER] WHERE CollectionName = 'Consumer' AND UladApplicationId = @uladId;";
            return this.ExecuteSqlCallToRetrieveInteger(query, "OrderCount", this.CreateSqlParameter("@uladId", uladAppId.Value));
        }

        private int ExecuteSqlCall(string query, params SqlParameter[] prams)
        {
            var sqlQuery = SQLQueryString.Create(query).Value;
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare, shouldProfile: false))
            {
                var rowCount = SqlServerHelper.Update(connection, null, sqlQuery, prams, TimeoutInSeconds.Sixty);
                return rowCount.Value;
            }
        }

        private int ExecuteSqlCallToRetrieveInteger(string query, string resultColumnName, params SqlParameter[] prams)
        {
            var sqlQuery = SQLQueryString.Create(query).Value;
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare, shouldProfile: false))
            {
                using (var reader = SqlServerHelper.Select(connection, null, sqlQuery, prams, TimeoutInSeconds.Sixty))
                {
                    if (reader.Read())
                    {
                        return Convert.ToInt32(reader[resultColumnName]);
                    }
                }
            }

            return 0;
        }

        private SqlParameter CreateSqlParameter(string name, Guid value)
        {
            var pram = new SqlParameter(name, value);
            pram.SqlDbType = System.Data.SqlDbType.UniqueIdentifier;
            return pram;
        }

        private SqlParameter CreateSqlParameter(string name, bool value)
        {
            var pram = new SqlParameter(name, value);
            pram.SqlDbType = System.Data.SqlDbType.Bit;
            return pram;
        }
    }
}
