﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data;
    using global::DataAccess;
    using global::LendersOffice.Constants;
    using global::LendersOffice.Security;
    using NUnit.Framework;
    using NSubstitute;
    using Utils;
    using global::LendingQB.Core.Commands;
    using LqbGrammar.DataTypes.PathDispatch;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed partial class LoanFileFieldsValidation
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [SetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankLoanFile();

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(LoanFileFieldsValidation));
                loan.InitSave();
                loan.MigrateToUseLqbCollections();
                loan.Save();
            }
        }

        [TearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [TestCase]
        public void NormalLoanFile_StillValidatesCollectionNotIncludedDirectly()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CPageBase.RealProperties), }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);

                pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                Assert.IsTrue(pageData.IsCollectionRegistered("CLiaCollection"), "Expecting CLiaCollection to be registered with the PageData");
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void ValidatorForReoGetsCorrectDataSets_ForMigrationStage(E_sLqbCollectionT lqbCollectionType)
        {
            this.SetBorrowerApplicationCollectionMode(lqbCollectionType);
            string testAddr = "Test Address";
            string collectionName = "CReCollection";
            string collectionPropertyName = nameof(CAppBase.aReCollection);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { collectionPropertyName }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet originalData = null;
                DataSet updatedData = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection(collectionName, Arg.Do<DataSet>(d => originalData = d), Arg.Do<DataSet>(d => updatedData = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var collection = app.aReCollection;
                var entity = collection.AddRegularRecord();
                entity.Addr = testAddr;
                pageData.Save();

                fakeValidator.Received().UpdateCollection(collectionName, Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.IsNotNull(updatedData);
                Assert.IsNotNull(originalData);

                var table = originalData.Tables[0];
                Assert.AreEqual(0, table.Rows.Count);

                table = updatedData.Tables[0];
                Assert.AreEqual(1, table.Rows.Count);

                var row = table.Rows[0];
                Assert.AreEqual(testAddr, row["Addr"]);
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void ValidatorGetsCorrectAssetDataSets_ForMigrationStage(E_sLqbCollectionT lqbCollectionType)
        {
            this.SetBorrowerApplicationCollectionMode(lqbCollectionType);
            string testAccNm = "Test Account Name";
            string collectionName = "CAssetCollection";
            string collectionPropertyName = nameof(CAppBase.aAssetCollection);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { collectionPropertyName }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet originalData = null;
                DataSet updatedData = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection(collectionName, Arg.Do<DataSet>(d => originalData = d), Arg.Do<DataSet>(d => updatedData = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var collection = app.aAssetCollection;
                var entity = collection.AddRegularRecord();
                entity.AccNm = testAccNm;
                pageData.Save();

                fakeValidator.Received().UpdateCollection(collectionName, Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.IsNotNull(updatedData);
                Assert.IsNotNull(originalData);

                var table = originalData.Tables[0];
                Assert.AreEqual(0, table.Rows.Count);

                table = updatedData.Tables[0];
                Assert.AreEqual(1, table.Rows.Count);

                var row = table.Rows[0];
                Assert.AreEqual(testAccNm, row["AccNm"]);
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void ValidatorGetsCorrectVorDataSets_ForMigrationStage(E_sLqbCollectionT lqbCollectionType)
        {
            this.SetBorrowerApplicationCollectionMode(lqbCollectionType);
            this.AddVorRecord(lqbCollectionType);

            string collectionName = "CVorFields";
            string collectionPropertyName = nameof(CAppBase.aVorCollection);
            string description = "Test Description";
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { collectionPropertyName }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet originalData = null;
                DataSet updatedData = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection(collectionName, Arg.Do<DataSet>(d => originalData = d), Arg.Do<DataSet>(d => updatedData = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var collection = app.aVorCollection;
                var item = collection.GetRecordByIndex(0);
                item.Description = description;
                collection.Update(); // For VOR, there is an Update() method, not a Flush() method, and it is called on the collection directly.
                pageData.Save();

                fakeValidator.Received().UpdateCollection(collectionName, Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.IsNotNull(updatedData);
                Assert.IsNotNull(originalData);

                var table = originalData.Tables[0];
                Assert.AreEqual(1, table.Rows.Count);

                var row = table.Rows[0];
                Assert.IsTrue(string.IsNullOrEmpty((string)row["Description"]));

                table = updatedData.Tables[0];
                Assert.AreEqual(1, table.Rows.Count);

                row = table.Rows[0];
                Assert.AreEqual(description, (string)row["Description"]);
            }
        }

        private void AddVorRecord(E_sLqbCollectionT lqbCollectionType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.InitSave();
                var app = loan.GetAppData(0);

                if (lqbCollectionType == E_sLqbCollectionT.Legacy)
                {
                    // There is no way to add items other than via the full xml
                    var appBase = CAppBaseExtractor.Extract(app);
                    appBase.aVorXmlContent = this.FormulateVorCollectionXml();
                }
                else
                {
                    var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.VorRecords), app.aBConsumerId, null, null, null);
                    loan.HandleAdd(addCommand, null);
                }

                loan.Save();
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void ValidatorGetsCorrectVaPastLoanDataSets_ForMigrationStage(E_sLqbCollectionT lqbCollectionType)
        {
            this.SetBorrowerApplicationCollectionMode(lqbCollectionType);
            string testAddr = "Test Address";
            string collectionName = "CVaPastLCollection";
            string collectionPropertyName = nameof(CAppBase.aVaPastLCollection);
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { collectionPropertyName }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet originalData = null;
                DataSet updatedData = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection(collectionName, Arg.Do<DataSet>(d => originalData = d), Arg.Do<DataSet>(d => updatedData = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var collection = app.aVaPastLCollection;
                var entity = collection.AddRegularRecord();
                entity.StAddr = testAddr;
                collection.Flush(); // For VA Past Loan, the Flush method is only called directly on the collection, and is not called via app.Flush().
                pageData.Save();

                fakeValidator.Received().UpdateCollection(collectionName, Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.IsNotNull(updatedData);
                Assert.IsNotNull(originalData);

                var table = originalData.Tables[0];
                Assert.AreEqual(0, table.Rows.Count);

                table = updatedData.Tables[0];
                Assert.AreEqual(1, table.Rows.Count);

                var row = table.Rows[0];
                Assert.AreEqual(testAddr, row["StAddr"]);
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void Save_AfterAddingNewRowToBEmpCollection_CallsUpdateCollectionWithExtraRow(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.SetBorrowerApplicationCollectionMode(borrowerApplicationCollectionMode);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CAppBase.aBEmpCollection) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CEmpCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var employmentCollection = app.aBEmpCollection;
                employmentCollection.AddRegularRecord();
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CEmpCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void Save_AfterAddingNewRowToCEmpCollection_CallsUpdateCollectionWithExtraRow(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.SetBorrowerApplicationCollectionMode(borrowerApplicationCollectionMode);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CAppBase.aCEmpCollection) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CEmpCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var employmentCollection = app.aCEmpCollection;
                employmentCollection.AddRegularRecord();
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CEmpCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void Save_AfterModifyingBPrimaryEmployment_CallsUpdateCollectionWithUpdatedDataSet(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.SetBorrowerApplicationCollectionMode(borrowerApplicationCollectionMode);
            this.AddPrimaryEmployment(E_BorrowerModeT.Borrower);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CAppBase.aBEmpCollection) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CEmpCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var employmentCollection = app.aBEmpCollection;
                var primaryEmployment = employmentCollection.GetPrimaryEmp(forceCreate: false);
                primaryEmployment.EmplrNm = "Borrower Meow";
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CEmpCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("Borrower Meow", originalRow["EmplrNm"].ToString());
                Assert.AreEqual("Borrower Meow", updatedRow["EmplrNm"].ToString());
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void Save_AfterModifyingCPrimaryEmployment_CallsUpdateCollectionWithUpdatedDataSet(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.SetBorrowerApplicationCollectionMode(borrowerApplicationCollectionMode);
            this.AddPrimaryEmployment(E_BorrowerModeT.Coborrower);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CAppBase.aCEmpCollection) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CEmpCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var employmentCollection = app.aCEmpCollection;
                var primary = employmentCollection.GetPrimaryEmp(forceCreate: false);
                primary.EmplrNm = "Coborrower Meow";
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CEmpCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("Coborrower Meow", originalRow["EmplrNm"].ToString());
                Assert.AreEqual("Coborrower Meow", updatedRow["EmplrNm"].ToString());
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void Save_AfterAddingNewRecordToLiaCollection_CallsUpdateCollectionWithExtraRow(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.SetBorrowerApplicationCollectionMode(borrowerApplicationCollectionMode);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CAppBase.aLiaCollection) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CLiaCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var collection = app.aLiaCollection;
                collection.AddRegularRecord();
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CLiaCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                Assert.AreEqual(0, original.Tables[0].Rows.Count);
                Assert.AreEqual(1, updated.Tables[0].Rows.Count);
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void Save_AfterModifyingExistingLiaRecord_CallsUpdateCollectionWithUpdatedDataSet(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            this.SetBorrowerApplicationCollectionMode(borrowerApplicationCollectionMode);
            this.AddRegularLiability();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var sqlProvider = CSelectStatementProvider.GetProviderForTargets(new[] { nameof(CAppBase.aCEmpCollection) }, true);
                var pageBase = new CPageBaseWrapped(this.loanId, nameof(LoanFileFieldsValidation), sqlProvider);
                var pageData = new TestPageData(pageBase, sqlProvider);
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                // Calling InitSave will actually re-assign the validator.
                // We need to insert our fake before we access the app to
                // ensure it is used by the collection.
                DataSet original = null;
                DataSet updated = null;
                var fakeValidator = Substitute.For<ILoanFileFieldValidator>();
                fakeValidator.UpdateCollection("CLiaCollection", Arg.Do<DataSet>(d => original = d), Arg.Do<DataSet>(d => updated = d));
                fakeValidator.CanSave.Returns(true);
                pageData.SetFieldValidator(fakeValidator);
                var app = pageData.GetAppData(0);

                var collection = app.aLiaCollection;
                var liability = collection.GetRegularRecordAt(0);
                liability.AccNm = "Updated Account Name";
                pageData.Save();

                fakeValidator.Received().UpdateCollection("CLiaCollection", Arg.Any<DataSet>(), Arg.Any<DataSet>());
                var originalRow = original.Tables[0].Rows[0];
                var updatedRow = updated.Tables[0].Rows[0];
                Assert.AreNotEqual("Updated Account Name", originalRow["AccNm"].ToString());
                Assert.AreEqual("Updated Account Name", updatedRow["AccNm"].ToString());
            }
        }

        private string FormulateVorCollectionXml()
        {
            string xml = @"<VorXmlTable>
    <VorXmlContent>
        <RecordId>BE390FDD-6E7D-4D21-843F-E269DCFF284C</RecordId>
        <LandlordCreditorName></LandlordCreditorName>
        <Description></Description>
        <AddressFor></AddressFor>
        <CityFor></CityFor>
        <StateFor></StateFor>
        <ZipFor></ZipFor>
        <AddressTo></AddressTo>
        <CityTo></CityTo>
        <StateTo></StateTo>
        <ZipTo></ZipTo>
        <PhoneTo></PhoneTo>
        <FaxTo></FaxTo>
        <AccountName></AccountName>
        <VerifSentD></VerifSentD>
        <VerifRecvD></VerifRecvD>
        <VerifExpD></VerifExpD>
        <VerifReorderedD></VerifReorderedD>
        <IsForBorrower></IsForBorrower>
        <VerifT></VerifT>
        <Attention></Attention>
        <PrepD></PrepD>
        <IsSeeAttachment></IsSeeAttachment>
        <VerifSigningEmployeeId></VerifSigningEmployeeId>
        <VerifSignatureImgId></VerifSignatureImgId>
    </VorXmlContent>
</VorXmlTable>";
            return xml;
        }

        private void SetBorrowerApplicationCollectionMode(E_sLqbCollectionT borrowerApplicationCollectionMode)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(LoanFileFieldsValidation));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.sBorrowerApplicationCollectionT = borrowerApplicationCollectionMode;
                loan.Save();
            }
        }

        private void AddPrimaryEmployment(E_BorrowerModeT borrower)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(LoanFileFieldsValidation));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var app = loan.GetAppData(0);
                var collection = borrower == E_BorrowerModeT.Borrower 
                    ? app.aBEmpCollection 
                    : app.aCEmpCollection;
                collection.GetPrimaryEmp(forceCreate: true);
                loan.Save();
            }
        }

        private void AddRegularLiability()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(LoanFileFieldsValidation));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var app = loan.GetAppData(0);
                var collection = app.aLiaCollection;
                collection.AddRegularRecord();
                loan.Save();
            }
        }

        private void AddNewApp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(LoanFileFieldsValidation));
                loan.AddNewApp();
            }
        }

        private sealed class TestPageData : CPageData
        {
            public TestPageData(CPageBase pageBase, ISelectStatementProvider sqlProvider)
                : base(pageBase, sqlProvider, null)
            {
            }

            public void SetFieldValidator(ILoanFileFieldValidator recorder)
            {
                this.m_validator = recorder;
            }

            public bool IsCollectionRegistered(string collectionName)
            {
                return this.originalCollectionSnapshotsForLegacyWorkflow.ContainsKey(collectionName);
            }
        }
    }
}