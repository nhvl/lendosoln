﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using Utils;
    using NUnit.Framework;
    using LendersOffice.Security;
    using global::DataAccess;

    /// <summary>
    ///
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class CAppBasePrevAddrYrsTest
    {
        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

                var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankLoanFile();
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        private CAppBase GetApp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                IEnumerable<string> dependentFields = new[]
                {
                    nameof(CAppBase.aBPrev1AddrYrs),
                    nameof(CAppBase.aCPrev1AddrYrs),
                    nameof(CAppBase.aBPrev1AddrRemainderMonths),
                    nameof(CAppBase.aCPrev1AddrRemainderMonths),
                    nameof(CAppBase.aPrev1AddrRemainderMonths),
                    nameof(CAppBase.aBPrev1AddrWholeYears),
                    nameof(CAppBase.aCPrev1AddrWholeYears),
                    nameof(CAppBase.aPrev1AddrWholeYears),
                    nameof(CAppBase.aBPrev2AddrYrs),
                    nameof(CAppBase.aCPrev2AddrYrs),
                    nameof(CAppBase.aBPrev2AddrRemainderMonths),
                    nameof(CAppBase.aCPrev2AddrRemainderMonths),
                    nameof(CAppBase.aPrev2AddrRemainderMonths),
                    nameof(CAppBase.aBPrev2AddrWholeYears),
                    nameof(CAppBase.aCPrev2AddrWholeYears),
                    nameof(CAppBase.aPrev2AddrWholeYears),
                };

                var provider = CSelectStatementProvider.GetProviderForTargets(dependentFields, false);
                var loan = new CPageBaseWrapped(
                    this.loanId,
                    nameof(CAppBasePrevAddrYrsTest),
                    provider);
                loan.InitLoad();
                return loan.GetAppData(0);
            }
        }

        [Test]
        public void aBPrev1AddrRemainderMonths_Always_ReturnsExpectedValue() 
        {
            var app = this.GetApp();

            app.aBPrev1AddrYrs = "4.5";

            Assert.AreEqual(6, app.aBPrev1AddrRemainderMonths);
        }

        [Test]
        public void aCPrev1AddrRemainderMonths_Always_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            app.aCPrev1AddrYrs = "5.25";

            Assert.AreEqual(3, app.aCPrev1AddrRemainderMonths);
        }

        [Test]
        public void aPrev1AddrRemainderMonths_SetBorrowerField_ReturnsExpectedValue() 
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aBPrev1AddrYrs = "4.5";

                Assert.AreEqual(6, app.aPrev1AddrRemainderMonths);
            }
        }

        [Test]
        public void aPrev1AddrRemainderMonths_SetCoborrowerField_ReturnsExpectedValue() 
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aCPrev1AddrYrs = "5.25";
                app.BorrowerModeT = E_BorrowerModeT.Coborrower;

                Assert.AreEqual(3, app.aPrev1AddrRemainderMonths);
            }
        }

        [Test]
        public void aBPrev1AddrWholeYears_Always_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            app.aBPrev1AddrYrs = "4.5";

            Assert.AreEqual(4, app.aBPrev1AddrWholeYears);
        }

        [Test]
        public void aCPrev1AddrWholeYears_Always_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            app.aCPrev1AddrYrs = "5.25";

            Assert.AreEqual(5, app.aCPrev1AddrWholeYears);
        }

        [Test]
        public void aPrev1AddrWholeYears_SetBorrowerField_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aBPrev1AddrYrs = "4.5";

                Assert.AreEqual(4, app.aPrev1AddrWholeYears);
            }
        }

        [Test]
        public void aPrev1AddrWholeYears_SetCoborrowerField_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aCPrev1AddrYrs = "5.25";
                app.BorrowerModeT = E_BorrowerModeT.Coborrower;

                Assert.AreEqual(5, app.aPrev1AddrWholeYears);
            }
        }

        [Test]
        public void aBPrev2AddrRemainderMonths_Always_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            app.aBPrev2AddrYrs = "4.5";

            Assert.AreEqual(6, app.aBPrev2AddrRemainderMonths);
        }

        [Test]
        public void aCPrev2AddrRemainderMonths_Always_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            app.aCPrev2AddrYrs = "5.25";

            Assert.AreEqual(3, app.aCPrev2AddrRemainderMonths);
        }

        [Test]
        public void aPrev2AddrRemainderMonths_SetBorrowerField_ReturnsExpectedValue() 
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aBPrev2AddrYrs = "4.5";

                Assert.AreEqual(6, app.aPrev2AddrRemainderMonths);
            }
        }

        [Test]
        public void aPrev2AddrRemainderMonths_SetCoborrowerField_ReturnsExpectedValue() 
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aCPrev2AddrYrs = "5.25";
                app.BorrowerModeT = E_BorrowerModeT.Coborrower;

                Assert.AreEqual(3, app.aPrev2AddrRemainderMonths);
            }
        }

        [Test]
        public void aBPrev2AddrWholeYears_Always_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            app.aBPrev2AddrYrs = "4.5";

            Assert.AreEqual(4, app.aBPrev2AddrWholeYears);
        }

        [Test]
        public void aCPrev2AddrWholeYears_Always_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            app.aCPrev2AddrYrs = "5.25";

            Assert.AreEqual(5, app.aCPrev2AddrWholeYears);
        }

        [Test]
        public void aPrev2AddrWholeYears_SetBorrowerField_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aBPrev2AddrYrs = "4.5";

                Assert.AreEqual(4, app.aPrev2AddrWholeYears);
            }
        }

        [Test]
        public void aPrev2AddrWholeYears_SetCoborrowerField_ReturnsExpectedValue()
        {
            var app = this.GetApp();

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                app.aCPrev2AddrYrs = "5.25";
                app.BorrowerModeT = E_BorrowerModeT.Coborrower;

                Assert.AreEqual(5, app.aPrev2AddrWholeYears);
            }
        }
    }
}
