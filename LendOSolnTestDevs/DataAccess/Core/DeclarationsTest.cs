﻿namespace DataAccess
{
    using System;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendingQB.Test.Developers.Fakes;
    using LendingQB.Test.Developers.Utils;
    using NUnit.Framework;

    [TestFixture]
    [Category(LendingQB.Test.Developers.CategoryConstants.IntegrationTest)]
    public class DeclarationsTest
    {
        private const string CoborrowerName = "Alice";
        private const string CoborrowerSsn = "123-12-1234";

        private AbstractUserPrincipal principal;
        private Guid uladLoanId;

        [TestFixtureSetUp]
        public void Setup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

                var creator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.uladLoanId = creator.CreateBlankUladLoanFile();
            }
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                Tools.SystemDeclareLoanFileInvalid(this.principal, this.uladLoanId, false, false);
            }
        }

        #region Test case sources
        private static readonly object[][] allCitizenshipResidencyValues =
            (from citizenshipValue in new[] { "Y", "N", string.Empty }
             from residencyValue in new[] { "Y", "N", string.Empty }
             select new[] { citizenshipValue, residencyValue }).ToArray();

        private static readonly object[][] CitizenDescription_NonUlad_ReturnsCalculatedValue_Source = new[]
        {
            new[] { "Y", "Y", "US Citizen" },
            new[] { "Y", "N", "US Citizen" },
            new[] { "Y", string.Empty, string.Empty },
            new[] { "N", "Y", "Permanent Resident Alien" },
            new[] { "N", "N", "Non-Permanent Resident Alien" },
            new[] { "N", string.Empty, string.Empty },
            new[] { string.Empty, "Y", string.Empty },
            new[] { string.Empty, "N", string.Empty },
            new[] { string.Empty, string.Empty, string.Empty },
        };

        private static readonly object[][] CitizenDescription_Ulad_ReturnsEnumerationValue_Source = new[]
        {
            new object[] { E_aProdCitizenT.USCitizen, "US Citizen" },
            new object[] { E_aProdCitizenT.PermanentResident, "Permanent Resident" },
            new object[] { E_aProdCitizenT.NonpermanentResident, "Non-permanent Resident" },
            new object[] { E_aProdCitizenT.ForeignNational, "Non-Resident Alien (Foreign National)" },
        };

        private static readonly object[][] TotalScorecardWarning_NonUlad_ReturnsExpectedValue_Source = new[]
        {
            new[] { "Y", "Y", string.Empty },
            new[] { "Y", "N", string.Empty },
            new[] { "Y", string.Empty, ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus },
            new[] { "N", "Y", string.Empty },
            new[] { "N", "N", string.Empty },
            new[] { "N", string.Empty, ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus },
            new[] { string.Empty, "Y", ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus },
            new[] { string.Empty, "N", ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus },
            new[] { string.Empty, string.Empty, ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus },
        };

        private static readonly object[][] PmlImportWarning_NonUlad_ReturnsExpectedValue_Source = new[]
        {
            new[] { "Y", "Y", string.Empty },
            new[] { "Y", "N", string.Empty },
            new[] { "Y", string.Empty, ErrorMessages.PmlImportWarning_MissingCitizenshipStatus },
            new[] { "N", "Y", string.Empty },
            new[] { "N", "N", string.Empty },
            new[] { "N", string.Empty, ErrorMessages.PmlImportWarning_MissingCitizenshipStatus },
            new[] { string.Empty, "Y", ErrorMessages.PmlImportWarning_MissingCitizenshipStatus },
            new[] { string.Empty, "N", ErrorMessages.PmlImportWarning_MissingCitizenshipStatus },
            new[] { string.Empty, string.Empty, ErrorMessages.PmlImportWarning_MissingCitizenshipStatus },
        };

        private static readonly object[][] DecForeignNational_NonUlad_ReturnsExpectedValue_Source = new[]
        {
            new[] { "Y", "Y", "N" },
            new[] { "Y", "N", "N" },
            new[] { "Y", string.Empty, "N" },
            new[] { "N", "Y", "N" },
            new[] { "N", string.Empty, "N" },
            new[] { string.Empty, "Y", "N" },
            new[] { string.Empty, "N", "N" },
            new[] { string.Empty, string.Empty, "N" },
        };

        private static readonly object[][] DecForeignNational_Ulad_ReturnsExpectedValue_Source = new[]
        {
            new object[] { E_aProdCitizenT.USCitizen, "N" },
            new object[] { E_aProdCitizenT.PermanentResident, "N" },
            new object[] { E_aProdCitizenT.NonpermanentResident, "N" },
            new object[] { E_aProdCitizenT.ForeignNational, "Y" },
        };

        private static readonly object[][] TotalScoreIsFthb_NonUlad_Always_UsesCalculationFallback_Source = new[]
        {
            new object[] { "Y", false },
            new object[] { "N", true },
            new object[] { "", false },
        };

        private static readonly object[][] TotalScoreIsFthb_Ulad_Always_UsesCalculationFallback_Source = new[]
        {
            new object[] { E_TriState.Yes, false },
            new object[] { E_TriState.No, true },
            new object[] { E_TriState.Blank, false },
        };

        private static readonly object[][] TotalScoreIsFthbReadOnly_NonUlad_Always_UsesCalculationFallback_Source = new[]
        {
            new object[] { "Y", false },
            new object[] { "N", true },
            new object[] { "", false },
        };

        private static readonly object[][] TotalScoreIsFthbReadOnly_Ulad_Always_UsesCalculationFallback_Source = new[]
        {
            new object[] { E_TriState.Yes, false },
            new object[] { E_TriState.No, true },
            new object[] { E_TriState.Blank, false },
        };

        private static readonly object[][] aWillOccupyRefiResidence_NonUlad_ReturnsExpectedValue_Source = new[]
        {
            new object[] { "Y", "Y", E_TriState.Yes },
            new object[] { "Y", "N", E_TriState.Blank },
            new object[] { "Y", "", E_TriState.Yes },
            new object[] { "N", "Y", E_TriState.Blank },
            new object[] { "N", "N", E_TriState.No },
            new object[] { "N", "", E_TriState.No },
            new object[] { "", "Y", E_TriState.Yes },
            new object[] { "", "N", E_TriState.No },
            new object[] { "", "", E_TriState.Blank },
        };

        private static readonly object[][] aWillOccupyRefiResidence_Ulad_ReturnsExpectedValue_Source = new[]
        {
            new object[] { E_TriState.Yes, E_TriState.Yes, E_TriState.Yes },
            new object[] { E_TriState.Yes, E_TriState.No, E_TriState.Blank },
            new object[] { E_TriState.Yes, E_TriState.Blank, E_TriState.Yes },
            new object[] { E_TriState.No, E_TriState.Yes, E_TriState.Blank },
            new object[] { E_TriState.No, E_TriState.No, E_TriState.No },
            new object[] { E_TriState.No, E_TriState.Blank, E_TriState.No },
            new object[] { E_TriState.Blank, E_TriState.Yes, E_TriState.Yes },
            new object[] { E_TriState.Blank, E_TriState.No, E_TriState.No },
            new object[] { E_TriState.Blank, E_TriState.Blank, E_TriState.Blank },
        };

        private static readonly object[][] sHasNonOccupantCoborrower_NonPrimaryResidence_AlwaysReturnsFalse_Source =
            (from targetAppType in Enum.GetValues(typeof(GseTargetApplicationT)).Cast<GseTargetApplicationT>()
             from occupancyType in new[] { E_aOccT.Investment, E_aOccT.SecondaryResidence }
             select new object[] { targetAppType, occupancyType }).ToArray();

        private static readonly object[][] sHasNonOccupantCoborrower_NonUlad_ReturnsExpectedValues_Source = new[]
        {
            new object[] { "Y", "Y", E_aOccT.PrimaryResidence, false },
            new object[] { "Y", "Y", E_aOccT.SecondaryResidence, true },
            new object[] { "Y", "Y", E_aOccT.Investment, true },
            new object[] { "Y", "N", E_aOccT.PrimaryResidence, true },
            new object[] { "Y", "N", E_aOccT.SecondaryResidence, true },
            new object[] { "Y", "N", E_aOccT.Investment, true },
            new object[] { "Y", "", E_aOccT.PrimaryResidence, false },
            new object[] { "Y", "", E_aOccT.SecondaryResidence, true },
            new object[] { "Y", "", E_aOccT.Investment, true },
            new object[] { "N", "Y", E_aOccT.PrimaryResidence, true },
            new object[] { "N", "Y", E_aOccT.SecondaryResidence, true },
            new object[] { "N", "Y", E_aOccT.Investment, true },
            new object[] { "N", "N", E_aOccT.PrimaryResidence, true },
            new object[] { "N", "N", E_aOccT.SecondaryResidence, true },
            new object[] { "N", "N", E_aOccT.Investment, true },
            new object[] { "N", "", E_aOccT.PrimaryResidence, true },
            new object[] { "N", "", E_aOccT.SecondaryResidence, true },
            new object[] { "N", "", E_aOccT.Investment, true },
            new object[] { "", "Y", E_aOccT.PrimaryResidence, false },
            new object[] { "", "Y", E_aOccT.SecondaryResidence, true },
            new object[] { "", "Y", E_aOccT.Investment, true },
            new object[] { "", "N", E_aOccT.PrimaryResidence, true },
            new object[] { "", "N", E_aOccT.SecondaryResidence, true },
            new object[] { "", "N", E_aOccT.Investment, true },
            new object[] { "", "", E_aOccT.PrimaryResidence, false },
            new object[] { "", "", E_aOccT.SecondaryResidence, true },
            new object[] { "", "", E_aOccT.Investment, true },
        };

        private static readonly object[][] sHasNonOccupantCoborrower_Ulad_ReturnsExpectedValues_Source = new[]
        {
            new object[] { E_TriState.Yes, E_TriState.Yes, E_aOccT.PrimaryResidence, false },
            new object[] { E_TriState.Yes, E_TriState.Yes, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.Yes, E_TriState.Yes, E_aOccT.Investment, true },
            new object[] { E_TriState.Yes, E_TriState.No, E_aOccT.PrimaryResidence, true },
            new object[] { E_TriState.Yes, E_TriState.No, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.Yes, E_TriState.No, E_aOccT.Investment, true },
            new object[] { E_TriState.Yes, E_TriState.Blank, E_aOccT.PrimaryResidence, false },
            new object[] { E_TriState.Yes, E_TriState.Blank, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.Yes, E_TriState.Blank, E_aOccT.Investment, true },
            new object[] { E_TriState.No, E_TriState.Yes, E_aOccT.PrimaryResidence, true },
            new object[] { E_TriState.No, E_TriState.Yes, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.No, E_TriState.Yes, E_aOccT.Investment, true },
            new object[] { E_TriState.No, E_TriState.No, E_aOccT.PrimaryResidence, true },
            new object[] { E_TriState.No, E_TriState.No, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.No, E_TriState.No, E_aOccT.Investment, true },
            new object[] { E_TriState.No, E_TriState.Blank, E_aOccT.PrimaryResidence, true },
            new object[] { E_TriState.No, E_TriState.Blank, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.No, E_TriState.Blank, E_aOccT.Investment, true },
            new object[] { E_TriState.Blank, E_TriState.Yes, E_aOccT.PrimaryResidence, false },
            new object[] { E_TriState.Blank, E_TriState.Yes, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.Blank, E_TriState.Yes, E_aOccT.Investment, true },
            new object[] { E_TriState.Blank, E_TriState.No, E_aOccT.PrimaryResidence, true },
            new object[] { E_TriState.Blank, E_TriState.No, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.Blank, E_TriState.No, E_aOccT.Investment, true },
            new object[] { E_TriState.Blank, E_TriState.Blank, E_aOccT.PrimaryResidence, false },
            new object[] { E_TriState.Blank, E_TriState.Blank, E_aOccT.SecondaryResidence, true },
            new object[] { E_TriState.Blank, E_TriState.Blank, E_aOccT.Investment, true },
        };
        #endregion

        #region Borrower Tests
        [Test, TestCaseSource(nameof(CitizenDescription_NonUlad_ReturnsCalculatedValue_Source))]
        public void aBCitizenDescription_NonUlad_ReturnsCalculatedValue(string citizenship, string residency, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBDecCitizen = citizenship;
                app.aBDecResidency = residency;

                Assert.AreEqual(expected, app.aBCitizenDescription);
            }
        }

        [Test, TestCaseSource(nameof(CitizenDescription_Ulad_ReturnsEnumerationValue_Source))]
        public void aBCitizenDescription_Ulad_ReturnsEnumerationValue(E_aProdCitizenT citizenshipType, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aProdBCitizenT = citizenshipType;

                Assert.AreEqual(expected, app.aBCitizenDescription);
            }
        }

        [Test, TestCaseSource(nameof(TotalScorecardWarning_NonUlad_ReturnsExpectedValue_Source))]
        public void aBDecResidencyTotalScorecardWarning_NonUlad_ReturnsExpectedValue(string citizenship, string residency, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Legacy);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecCitizen = citizenship;
                app.aBDecResidency = residency;

                Assert.AreEqual(expected, app.aBDecResidencyTotalScorecardWarning);
            }
        }

        [Test, TestCaseSource(nameof(allCitizenshipResidencyValues))]
        public void aBDecResidencyTotalScorecardWarning_Ulad_Always_ReturnsBlank(string citizenship, string residency)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecCitizen = citizenship;
                app.aBDecResidency = residency;

                Assert.AreEqual(string.Empty, app.aBDecResidencyTotalScorecardWarning);
            }
        }

        [Test, TestCaseSource(nameof(PmlImportWarning_NonUlad_ReturnsExpectedValue_Source))]
        public void aBDecResidencyPmlImportWarning_NonUlad_ReturnsExpectedValue(string citizenship, string residency, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Legacy);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecCitizen = citizenship;
                app.aBDecResidency = residency;

                Assert.AreEqual(expected, app.aBDecResidencyPmlImportWarning);
            }
        }

        [Test, TestCaseSource(nameof(allCitizenshipResidencyValues))]
        public void aBDecResidencyPmlImportWarning_Ulad_Always_ReturnsBlank(string citizenship, string residency)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecCitizen = citizenship;
                app.aBDecResidency = residency;

                Assert.AreEqual(string.Empty, app.aBDecResidencyPmlImportWarning);
            }
        }

        [Test, TestCaseSource(nameof(DecForeignNational_NonUlad_ReturnsExpectedValue_Source))]
        public void aBDecForeignNational_NonUlad_ReturnsExpectedValue(string citizenship, string residency, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Legacy);
                var app = loan.GetAppData(0);

                app.aBDecCitizen = citizenship;
                app.aBDecResidency = residency;

                Assert.AreEqual(expected, app.aBDecForeignNational);
            }
        }

        [Test, TestCaseSource(nameof(DecForeignNational_Ulad_ReturnsExpectedValue_Source))]
        public void aBDecForeignNational_Ulad_ReturnsExpectedValue(E_aProdCitizenT citizenshipType, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aProdBCitizenT = citizenshipType;

                Assert.AreEqual(expected, app.aBDecForeignNational);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aBTotalScoreIsFthb_Always_ReturnsFalseWhenNotAcquiringDwelling(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                app.aBTotalScoreIsFthb = true;
                loan.sLPurposeT = E_sLPurposeT.HomeEquity;

                Assert.IsFalse(app.aBTotalScoreIsFthb);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aBTotalScoreIsFthb_Always_ReturnsTrueWhenDbValueTrue(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                app.aBTotalScoreIsFthb = true;
                loan.sLPurposeT = E_sLPurposeT.Purchase;

                Assert.IsTrue(app.aBTotalScoreIsFthb);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthb_NonUlad_Always_UsesCalculationFallback_Source))]
        public void aBTotalScoreIsFthb_NonUlad_Always_UsesCalculationFallback(string pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBDecPastOwnership = pastOwnershipValue;

                Assert.AreEqual(expected, app.aBTotalScoreIsFthb);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthb_Ulad_Always_UsesCalculationFallback_Source))]
        public void aBTotalScoreIsFthb_Ulad_Always_UsesCalculationFallback(E_TriState pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad = pastOwnershipValue;

                Assert.AreEqual(expected, app.aBTotalScoreIsFthb);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aBTotalScoreIsFthbReadOnly_Always_ReturnsTrueWhenNotAcquiringDwelling(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                app.aBTotalScoreIsFthb = true;
                loan.sLPurposeT = E_sLPurposeT.HomeEquity;

                Assert.IsTrue(app.aBTotalScoreIsFthbReadOnly);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthbReadOnly_NonUlad_Always_UsesCalculationFallback_Source))]
        public void aBTotalScoreIsFthbReadOnly_NonUlad_Always_UsesCalculationFallback(string pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBDecPastOwnership = pastOwnershipValue;

                Assert.AreEqual(expected, app.aBTotalScoreIsFthbReadOnly);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthbReadOnly_Ulad_Always_UsesCalculationFallback_Source))]
        public void aBTotalScoreIsFthbReadOnly_Ulad_Always_UsesCalculationFallback(E_TriState pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBDecHasOwnershipIntOtherPropLastThreeYearsUlad = pastOwnershipValue;

                Assert.AreEqual(expected, app.aBTotalScoreIsFthbReadOnly);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aBAddrPostSourceT_WillOccupyAsPrimaryResidence_AlwaysReturnsSubjectPropertyAddress(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                if (targetApplicationType == GseTargetApplicationT.Ulad2019)
                {
                    app.aBDecIsPrimaryResidenceUlad = E_TriState.Yes;
                }
                else
                {
                    app.aBDecOcc = "Y";
                }

                Assert.AreEqual(E_aAddrPostSourceT.SubjectPropertyAddress, app.aBAddrPostSourceT);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aBAddrPostSourceT_WillNotOccupyAsPrimaryResidence_AlwaysReturnsMailingAddress(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                if (targetApplicationType == GseTargetApplicationT.Ulad2019)
                {
                    app.aBDecIsPrimaryResidenceUlad = E_TriState.No;
                }
                else
                {
                    app.aBDecOcc = "N";
                }

                Assert.AreEqual(E_aAddrPostSourceT.MailingAddress, app.aBAddrPostSourceT);
            }
        }

        [Test]
        [TestCase(E_aBAddrT.LeaveBlank, false)]
        [TestCase(E_aBAddrT.Own, false)]
        [TestCase(E_aBAddrT.LivingRentFree, true)]
        [TestCase(E_aBAddrT.Rent, true)]
        public void aBIntendsToOccupyWithoutOwning_NonUlad_WillOccupyAsPrimaryResidence_ReturnsCalcuatedValue(E_aBAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBDecOcc = "Y";
                app.aBAddrT = addressType;

                Assert.AreEqual(expected, app.aBIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(E_aBAddrT.LeaveBlank, false)]
        [TestCase(E_aBAddrT.Own, false)]
        [TestCase(E_aBAddrT.LivingRentFree, true)]
        [TestCase(E_aBAddrT.Rent, true)]
        public void aBIntendsToOccupyWithoutOwning_Ulad_WillOccupyAsPrimaryResidence_ReturnsCalcuatedValue(E_aBAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBDecIsPrimaryResidenceUlad = E_TriState.Yes;
                app.aBAddrT = addressType;

                Assert.AreEqual(expected, app.aBIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(E_aBAddrT.LeaveBlank, false)]
        [TestCase(E_aBAddrT.Own, false)]
        [TestCase(E_aBAddrT.LivingRentFree, true)]
        [TestCase(E_aBAddrT.Rent, true)]
        public void aBIntendsToOccupyWithoutOwning_NonUlad_WillNotOccupyAsPrimaryResidence_AlwaysReturnsFalse(E_aBAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBDecOcc = "N";
                app.aBAddrT = addressType;

                Assert.IsFalse(app.aBIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(E_aBAddrT.LeaveBlank, false)]
        [TestCase(E_aBAddrT.Own, false)]
        [TestCase(E_aBAddrT.LivingRentFree, true)]
        [TestCase(E_aBAddrT.Rent, true)]
        public void aBIntendsToOccupyWithoutOwning_Ulad_WillNotOccupyAsPrimaryResidence_AlwaysReturnsFalse(E_aBAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBDecIsPrimaryResidenceUlad = E_TriState.No;
                app.aBAddrT = addressType;

                Assert.IsFalse(app.aBIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aBDecOccTotalScorecardWarning_BlankBorrowerName_AlwaysReturnsEmpty(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBFirstNm = string.Empty;
                app.aBLastNm = string.Empty;

                Assert.AreEqual(string.Empty, app.aBDecOccTotalScorecardWarning);
            }
        }

        [Test]
        [TestCase("Y", "")]
        [TestCase("N", "")]
        [TestCase("", ErrorMessages.TotalScorecardWarning_MissingIntentToOccupy)]
        public void aBDecOccTotalScorecardWarning_NonUlad_ReturnsExpectedValue(string declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecOcc = declarationValue;

                Assert.AreEqual(expected, app.aBDecOccTotalScorecardWarning);
            }
        }

        [Test]
        [TestCase(E_TriState.Yes, "")]
        [TestCase(E_TriState.No, "")]
        [TestCase(E_TriState.Blank, ErrorMessages.TotalScorecardWarning_MissingIntentToOccupy)]
        public void aBDecOccTotalScorecardWarning_Ulad_ReturnsExpectedValue(E_TriState declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecIsPrimaryResidenceUlad = declarationValue;

                Assert.AreEqual(expected, app.aBDecOccTotalScorecardWarning);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aBDecOccPmlImportWarning_BlankBorrowerName_AlwaysReturnsEmpty(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBFirstNm = string.Empty;
                app.aBLastNm = string.Empty;

                Assert.AreEqual(string.Empty, app.aBDecOccPmlImportWarning);
            }
        }

        [Test]
        [TestCase("Y", "")]
        [TestCase("N", "")]
        [TestCase("", ErrorMessages.PmlImportWarning_MissingIntentToOccupy)]
        public void aBDecOccPmlImportWarning_NonUlad_ReturnsExpectedValue(string declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecOcc = declarationValue;

                Assert.AreEqual(expected, app.aBDecOccPmlImportWarning);
            }
        }

        [Test]
        [TestCase(E_TriState.Yes, "")]
        [TestCase(E_TriState.No, "")]
        [TestCase(E_TriState.Blank, ErrorMessages.PmlImportWarning_MissingIntentToOccupy)]
        public void aBDecOccPmlImportWarning_Ulad_ReturnsExpectedValue(E_TriState declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBFirstNm = CoborrowerName;
                app.aBLastNm = CoborrowerName;
                app.aBDecIsPrimaryResidenceUlad = declarationValue;

                Assert.AreEqual(expected, app.aBDecOccPmlImportWarning);
            }
        }
        #endregion

        #region Coborrower Tests
        [Test, TestCaseSource(nameof(CitizenDescription_NonUlad_ReturnsCalculatedValue_Source))]
        public void aCCitizenDescription_NonUlad_ReturnsCalculatedValue(string citizenship, string residency, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);

                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aCDecCitizen = citizenship;
                app.aCDecResidency = residency;

                Assert.AreEqual(expected, app.aCCitizenDescription);
            }
        }

        [Test, TestCaseSource(nameof(CitizenDescription_Ulad_ReturnsEnumerationValue_Source))]
        public void aCCitizenDescription_Ulad_ReturnsEnumerationValue(E_aProdCitizenT citizenshipType, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aProdCCitizenT = citizenshipType;

                Assert.AreEqual(expected, app.aCCitizenDescription);
            }
        }

        [Test, TestCaseSource(nameof(TotalScorecardWarning_NonUlad_ReturnsExpectedValue_Source))]
        public void aCDecResidencyTotalScorecardWarning_NonUlad_ReturnsExpectedValue(string citizenship, string residency, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Legacy);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecCitizen = citizenship;
                app.aCDecResidency = residency;

                Assert.AreEqual(expected, app.aCDecResidencyTotalScorecardWarning);
            }
        }

        [Test, TestCaseSource(nameof(allCitizenshipResidencyValues))]
        public void aCDecResidencyTotalScorecardWarning_Ulad_Always_ReturnsBlank(string citizenship, string residency)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecCitizen = citizenship;
                app.aCDecResidency = residency;

                Assert.AreEqual(string.Empty, app.aCDecResidencyTotalScorecardWarning);
            }
        }

        [Test, TestCaseSource(nameof(PmlImportWarning_NonUlad_ReturnsExpectedValue_Source))]
        public void aCDecResidencyPmlImportWarning_NonUlad_ReturnsExpectedValue(string citizenship, string residency, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Legacy);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecCitizen = citizenship;
                app.aCDecResidency = residency;

                Assert.AreEqual(expected, app.aCDecResidencyPmlImportWarning);
            }
        }

        [Test, TestCaseSource(nameof(allCitizenshipResidencyValues))]
        public void aCDecResidencyPmlImportWarning_Ulad_Always_ReturnsBlank(string citizenship, string residency)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecCitizen = citizenship;
                app.aCDecResidency = residency;

                Assert.AreEqual(string.Empty, app.aCDecResidencyPmlImportWarning);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aCTotalScoreIsFthb_Always_ReturnsFalseWhenNotAcquiringDwelling(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                app.aCTotalScoreIsFthb = true;
                loan.sLPurposeT = E_sLPurposeT.HomeEquity;

                Assert.IsFalse(app.aCTotalScoreIsFthb);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aCTotalScoreIsFthb_Always_ReturnsTrueWhenDbValueTrue(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                app.aCTotalScoreIsFthb = true;
                loan.sLPurposeT = E_sLPurposeT.Purchase;

                Assert.IsTrue(app.aCTotalScoreIsFthb);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthb_NonUlad_Always_UsesCalculationFallback_Source))]
        public void aCTotalScoreIsFthb_NonUlad_Always_UsesCalculationFallback(string pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aCDecPastOwnership = pastOwnershipValue;

                Assert.AreEqual(expected, app.aCTotalScoreIsFthb);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthb_Ulad_Always_UsesCalculationFallback_Source))]
        public void aCTotalScoreIsFthb_Ulad_Always_UsesCalculationFallback(E_TriState pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad = pastOwnershipValue;

                Assert.AreEqual(expected, app.aCTotalScoreIsFthb);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aCTotalScoreIsFthbReadOnly_Always_ReturnsTrueWhenNotAcquiringDwelling(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                app.aCTotalScoreIsFthb = true;
                loan.sLPurposeT = E_sLPurposeT.HomeEquity;

                Assert.IsTrue(app.aCTotalScoreIsFthbReadOnly);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthbReadOnly_NonUlad_Always_UsesCalculationFallback_Source))]
        public void aCTotalScoreIsFthbReadOnly_NonUlad_Always_UsesCalculationFallback(string pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aCDecPastOwnership = pastOwnershipValue;

                Assert.AreEqual(expected, app.aCTotalScoreIsFthbReadOnly);
            }
        }

        [Test, TestCaseSource(nameof(TotalScoreIsFthbReadOnly_Ulad_Always_UsesCalculationFallback_Source))]
        public void aCTotalScoreIsFthbReadOnly_Ulad_Always_UsesCalculationFallback(E_TriState pastOwnershipValue, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCDecHasOwnershipIntOtherPropLastThreeYearsUlad = pastOwnershipValue;

                Assert.AreEqual(expected, app.aCTotalScoreIsFthbReadOnly);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aCAddrPostSourceT_WillOccupyAsPrimaryResidence_AlwaysReturnsSubjectPropertyAddress(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                if (targetApplicationType == GseTargetApplicationT.Ulad2019)
                {
                    app.aCDecIsPrimaryResidenceUlad = E_TriState.Yes;
                }
                else
                {
                    app.aCDecOcc = "Y";
                }

                Assert.AreEqual(E_aAddrPostSourceT.SubjectPropertyAddress, app.aCAddrPostSourceT);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aCAddrPostSourceT_WillNotOccupyAsPrimaryResidence_AlwaysReturnsMailingAddress(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                if (targetApplicationType == GseTargetApplicationT.Ulad2019)
                {
                    app.aCDecIsPrimaryResidenceUlad = E_TriState.No;
                }
                else
                {
                    app.aCDecOcc = "N";
                }

                Assert.AreEqual(E_aAddrPostSourceT.MailingAddress, app.aCAddrPostSourceT);
            }
        }

        [Test]
        [TestCase(E_aCAddrT.LeaveBlank, false)]
        [TestCase(E_aCAddrT.Own, false)]
        [TestCase(E_aCAddrT.LivingRentFree, true)]
        [TestCase(E_aCAddrT.Rent, true)]
        public void aCIntendsToOccupyWithoutOwning_NonUlad_WillOccupyAsPrimaryResidence_ReturnsCalcuatedValue(E_aCAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aCDecOcc = "Y";
                app.aCAddrT = addressType;

                Assert.AreEqual(expected, app.aCIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(E_aCAddrT.LeaveBlank, false)]
        [TestCase(E_aCAddrT.Own, false)]
        [TestCase(E_aCAddrT.LivingRentFree, true)]
        [TestCase(E_aCAddrT.Rent, true)]
        public void aCIntendsToOccupyWithoutOwning_Ulad_WillOccupyAsPrimaryResidence_ReturnsCalcuatedValue(E_aCAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCDecIsPrimaryResidenceUlad = E_TriState.Yes;
                app.aCAddrT = addressType;

                Assert.AreEqual(expected, app.aCIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(E_aCAddrT.LeaveBlank, false)]
        [TestCase(E_aCAddrT.Own, false)]
        [TestCase(E_aCAddrT.LivingRentFree, true)]
        [TestCase(E_aCAddrT.Rent, true)]
        public void aCIntendsToOccupyWithoutOwning_NonUlad_WillNotOccupyAsPrimaryResidence_AlwaysReturnsFalse(E_aCAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aCDecOcc = "N";
                app.aCAddrT = addressType;

                Assert.IsFalse(app.aCIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(E_aCAddrT.LeaveBlank, false)]
        [TestCase(E_aCAddrT.Own, false)]
        [TestCase(E_aCAddrT.LivingRentFree, true)]
        [TestCase(E_aCAddrT.Rent, true)]
        public void aCIntendsToOccupyWithoutOwning_Ulad_WillNotOccupyAsPrimaryResidence_AlwaysReturnsFalse(E_aCAddrT addressType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCDecIsPrimaryResidenceUlad = E_TriState.No;
                app.aCAddrT = addressType;

                Assert.IsFalse(app.aCIntendsToOccupyWithoutOwning);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aCDecOccTotalScorecardWarning_BlankBorrowerName_AlwaysReturnsEmpty(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCFirstNm = string.Empty;
                app.aCLastNm = string.Empty;

                Assert.AreEqual(string.Empty, app.aCDecOccTotalScorecardWarning);
            }
        }

        [Test]
        [TestCase("Y", "")]
        [TestCase("N", "")]
        [TestCase("", ErrorMessages.TotalScorecardWarning_MissingIntentToOccupy)]
        public void aCDecOccTotalScorecardWarning_NonUlad_ReturnsExpectedValue(string declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecOcc = declarationValue;

                Assert.AreEqual(expected, app.aCDecOccTotalScorecardWarning);
            }
        }

        [Test]
        [TestCase(E_TriState.Yes, "")]
        [TestCase(E_TriState.No, "")]
        [TestCase(E_TriState.Blank, ErrorMessages.TotalScorecardWarning_MissingIntentToOccupy)]
        public void aCDecOccTotalScorecardWarning_Ulad_ReturnsExpectedValue(E_TriState declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecIsPrimaryResidenceUlad = declarationValue;

                Assert.AreEqual(expected, app.aCDecOccTotalScorecardWarning);
            }
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        [TestCase(GseTargetApplicationT.Ulad2019)]
        public void aCDecOccPmlImportWarning_BlankBorrowerName_AlwaysReturnsEmpty(GseTargetApplicationT targetApplicationType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCFirstNm = string.Empty;
                app.aCLastNm = string.Empty;

                Assert.AreEqual(string.Empty, app.aCDecOccPmlImportWarning);
            }
        }

        [Test]
        [TestCase("Y", "")]
        [TestCase("N", "")]
        [TestCase("", ErrorMessages.PmlImportWarning_MissingIntentToOccupy)]
        public void aCDecOccPmlImportWarning_NonUlad_ReturnsExpectedValue(string declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecOcc = declarationValue;

                Assert.AreEqual(expected, app.aCDecOccPmlImportWarning);
            }
        }

        [Test]
        [TestCase(E_TriState.Yes, "")]
        [TestCase(E_TriState.No, "")]
        [TestCase(E_TriState.Blank, ErrorMessages.PmlImportWarning_MissingIntentToOccupy)]
        public void aCDecOccPmlImportWarning_Ulad_ReturnsExpectedValue(E_TriState declarationValue, string expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCDecIsPrimaryResidenceUlad = declarationValue;

                Assert.AreEqual(expected, app.aCDecOccPmlImportWarning);
            }
        }
        #endregion

        [Test, TestCaseSource(nameof(aWillOccupyRefiResidence_NonUlad_ReturnsExpectedValue_Source))]
        public void aWillOccupyRefiResidence_NonUlad_ReturnsExpectedValue(string borrowerDeclaration, string coborrowerDeclaration, E_TriState expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                app.aBDecOcc = borrowerDeclaration;
                app.aCDecOcc = coborrowerDeclaration;

                Assert.AreEqual(expected, app.aWillOccupyRefiResidence);
            }
        }

        [Test, TestCaseSource(nameof(aWillOccupyRefiResidence_Ulad_ReturnsExpectedValue_Source))]
        public void aWillOccupyRefiResidence_Ulad_ReturnsExpectedValue(E_TriState borrowerDeclaration, E_TriState coborrowerDeclaration, E_TriState expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                app.aBDecIsPrimaryResidenceUlad = borrowerDeclaration;
                app.aCDecIsPrimaryResidenceUlad = coborrowerDeclaration;

                Assert.AreEqual(expected, app.aWillOccupyRefiResidence);
            }
        }

        [Test, TestCaseSource(nameof(sHasNonOccupantCoborrower_NonPrimaryResidence_AlwaysReturnsFalse_Source))]
        public void sHasNonOccupantCoborrower_NonPrimaryResidence_AlwaysReturnsFalse(GseTargetApplicationT targetApplicationType, E_aOccT occupancyType)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(targetApplicationType);
                var app = loan.GetAppData(0);

                app.aOccT = occupancyType;

                Assert.IsFalse(loan.sHasNonOccupantCoborrower);
            }
        }

        [Test, TestCaseSource(nameof(sHasNonOccupantCoborrower_NonUlad_ReturnsExpectedValues_Source))]
        public void sHasNonOccupantCoborrower_NonUlad_ReturnsExpectedValues(string borrowerDeclaration, string coborrowerDeclaration, E_aOccT occupancyType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank, addApp: true);
                loan.GetAppData(0).aOccT = E_aOccT.PrimaryResidence;

                var secondApp = loan.GetAppData(1);
                secondApp.aOccT = occupancyType;
                secondApp.aBDecOcc = borrowerDeclaration;
                secondApp.aCDecOcc = coborrowerDeclaration;

                Assert.AreEqual(expected, loan.sHasNonOccupantCoborrower);
            }
        }

        [Test, TestCaseSource(nameof(sHasNonOccupantCoborrower_Ulad_ReturnsExpectedValues_Source))]
        public void sHasNonOccupantCoborrower_Ulad_ReturnsExpectedValues(E_TriState borrowerDeclaration, E_TriState coborrowerDeclaration, E_aOccT occupancyType, bool expected)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019, addApp: true);
                loan.GetAppData(0).aOccT = E_aOccT.PrimaryResidence;

                var secondApp = loan.GetAppData(1);
                secondApp.aOccT = occupancyType;
                secondApp.aBDecIsPrimaryResidenceUlad = borrowerDeclaration;
                secondApp.aCDecIsPrimaryResidenceUlad = coborrowerDeclaration;

                Assert.AreEqual(expected, loan.sHasNonOccupantCoborrower);
            }
        }

        [Test]
        public void sHasNonOccupantCoborrowerPe_NonUlad_SetFalse_UpdatesDeclarationValues()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Blank);
                var app = loan.GetAppData(0);

                loan.CalcModeT = E_CalcModeT.PriceMyLoan;

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCSsn = CoborrowerSsn;

                app.aBDecOcc = "N";
                app.aCDecOcc = "N";

                loan.sHasNonOccupantCoborrowerPe = false;

                Assert.AreEqual("Y", app.aBDecOcc);
                Assert.AreEqual("Y", app.aCDecOcc);
            }
        }

        [Test]
        public void sHasNonOccupantCoborrowerPe_Ulad_SetFalse_UpdatesDeclarationValues()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);
                var loan = this.GetInitializedLoan(GseTargetApplicationT.Ulad2019);
                var app = loan.GetAppData(0);

                loan.CalcModeT = E_CalcModeT.PriceMyLoan;

                app.aCFirstNm = CoborrowerName;
                app.aCLastNm = CoborrowerName;
                app.aCSsn = CoborrowerSsn;

                app.aBDecIsPrimaryResidenceUlad = E_TriState.No;
                app.aCDecIsPrimaryResidenceUlad = E_TriState.No;

                loan.sHasNonOccupantCoborrowerPe = false;

                Assert.AreEqual(E_TriState.Yes, app.aBDecIsPrimaryResidenceUlad);
                Assert.AreEqual(E_TriState.Yes, app.aCDecIsPrimaryResidenceUlad);
            }
        }

        private CPageData GetInitializedLoan(GseTargetApplicationT targetApplicationType, bool addApp = false)
        {
            CPageData loan;
            if (targetApplicationType == GseTargetApplicationT.Ulad2019)
            {
                loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.uladLoanId, typeof(DeclarationsTest));

                if (addApp)
                {
                    loan.AddNewApp();
                }
            }
            else
            {
                loan = FakePageData.Create(appCount: addApp ? 2 : 1);
            }

            loan.InitLoad();

            loan.sGseTargetApplicationTLckd = true;
            loan.sGseTargetApplicationT = targetApplicationType;

            return loan;
        }
    }
}
