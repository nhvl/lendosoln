namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System.Linq;
    using global::DataAccess;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class UladDataLayerMigratorTest
    {
        [Test]
        public void Migrate_WhenCalled_SetsFileToUseLqbCollections()
        {
            var loan = Substitute.For<IUladDataLayerMigrationLoanDataProvider>();
            loan.Apps.Returns(Enumerable.Empty<IUladDataLayerMigrationAppDataProvider>());
            var migrator = this.GetMigrator(loan);
            
            migrator.Migrate();
            
            Assert.AreEqual(E_sLqbCollectionT.UseLqbCollections, loan.sBorrowerApplicationCollectionT);
        }

        [Test]
        public void Migrate_WhenCalled_CallsContainerMethodToInitializeUladApps()
        {
            var container = Substitute.For<ILoanLqbCollectionContainer>();
            var migrator = this.GetMigrator(container: container);
            
            migrator.Migrate();
            
            container.Received().InitializeUladAppsToMatchLegacyApps();
        }

        [Test]
        public void Migrate_SingleApp_CallsMigrateForEachAppLevelCollection()
        {
            var fakeLoan = Substitute.For<IUladDataLayerMigrationLoanDataProvider>();
            fakeLoan.Apps.Returns(new[] { Substitute.For<IUladDataLayerMigrationAppDataProvider>() });
            var fakeContainer = Substitute.For<ILoanLqbCollectionContainer>();
            var testMigrator = new TestUladDataLayerMigrator(fakeLoan, fakeContainer);
            var fakeAssetMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.Asset>>();
            testMigrator.AssetMigrator = fakeAssetMigrator;
            var fakeBorrowerEmploymentRecordMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord>>();
            testMigrator.BorrowerEmploymentRecordMigrator = fakeBorrowerEmploymentRecordMigrator;
            var fakeCoborrowerEmploymentRecordMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord>>();
            testMigrator.CoborrowerEmploymentRecordMigrator = fakeCoborrowerEmploymentRecordMigrator;
            var fakeLiabilityMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.Liability>>();
            testMigrator.LiabilityMigrator = fakeLiabilityMigrator;
            var fakeRealPropertyMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.RealProperty>>();
            testMigrator.RealPropertyMigrator = fakeRealPropertyMigrator;
            var fakePublicRecordMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.PublicRecord>>();
            testMigrator.PublicRecordMigrator = fakePublicRecordMigrator;
            var fakeVaPreviousLoanMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.VaPreviousLoan>>();
            testMigrator.VaPreviousLoanMigrator = fakeVaPreviousLoanMigrator;

            testMigrator.Migrate();

            fakeAssetMigrator.Received(1).MigrateAppLevelRecordsToLoanLevel();
            fakeBorrowerEmploymentRecordMigrator.Received(1).MigrateAppLevelRecordsToLoanLevel();
            fakeCoborrowerEmploymentRecordMigrator.Received(1).MigrateAppLevelRecordsToLoanLevel();
            fakeLiabilityMigrator.Received(1).MigrateAppLevelRecordsToLoanLevel();
            fakeRealPropertyMigrator.Received(1).MigrateAppLevelRecordsToLoanLevel();
            fakePublicRecordMigrator.Received(1).MigrateAppLevelRecordsToLoanLevel();
            fakeVaPreviousLoanMigrator.Received(1).MigrateAppLevelRecordsToLoanLevel();
        }
        
        [Test]
        public void Migrate_ThreeApps_CallsMigrateForEachCollectionPerApp()
        {
            var fakeLoan = Substitute.For<IUladDataLayerMigrationLoanDataProvider>();
            fakeLoan.Apps.Returns(new[]
            {
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
            });
            var fakeContainer = Substitute.For<ILoanLqbCollectionContainer>();
            var testMigrator = new TestUladDataLayerMigrator(fakeLoan, fakeContainer);
            var fakeAssetMigrator = NSubstitute.Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.Asset>>();
            testMigrator.AssetMigrator = fakeAssetMigrator;
            var fakeBorrowerEmploymentRecordMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord>>();
            testMigrator.BorrowerEmploymentRecordMigrator = fakeBorrowerEmploymentRecordMigrator;
            var fakeCoborrowerEmploymentRecordMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord>>();
            testMigrator.CoborrowerEmploymentRecordMigrator = fakeCoborrowerEmploymentRecordMigrator;
            var fakeLiabilityMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.Liability>>();
            testMigrator.LiabilityMigrator = fakeLiabilityMigrator;
            var fakeRealPropertyMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.RealProperty>>();
            testMigrator.RealPropertyMigrator = fakeRealPropertyMigrator;
            var fakePublicRecordMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.PublicRecord>>();
            testMigrator.PublicRecordMigrator = fakePublicRecordMigrator;
            var fakeVaPreviousLoanMigrator = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.VaPreviousLoan>>();
            testMigrator.VaPreviousLoanMigrator = fakeVaPreviousLoanMigrator;
            
            testMigrator.Migrate();

            fakeAssetMigrator.Received(3).MigrateAppLevelRecordsToLoanLevel();
            fakeBorrowerEmploymentRecordMigrator.Received(3).MigrateAppLevelRecordsToLoanLevel();
            fakeCoborrowerEmploymentRecordMigrator.Received(3).MigrateAppLevelRecordsToLoanLevel();
            fakeLiabilityMigrator.Received(3).MigrateAppLevelRecordsToLoanLevel();
            fakeRealPropertyMigrator.Received(3).MigrateAppLevelRecordsToLoanLevel();
            fakePublicRecordMigrator.Received(3).MigrateAppLevelRecordsToLoanLevel();
            fakeVaPreviousLoanMigrator.Received(3).MigrateAppLevelRecordsToLoanLevel();
        }

        private UladDataLayerMigrator GetMigrator(
            IUladDataLayerMigrationLoanDataProvider loan = null,
            ILoanLqbCollectionContainer container = null)
        {
            if (loan == null)
            {
                loan = Substitute.For<IUladDataLayerMigrationLoanDataProvider>();
                loan.Apps.Returns(Enumerable.Empty<IUladDataLayerMigrationAppDataProvider>());
            }

            if (container == null)
            {
               container = Substitute.For<ILoanLqbCollectionContainer>();
            }
            
            return new UladDataLayerMigrator(
                loan,
                container);
        }
    }

    internal class TestUladDataLayerMigrator : UladDataLayerMigrator
    {
        public TestUladDataLayerMigrator(IUladDataLayerMigrationLoanDataProvider loan, ILoanLqbCollectionContainer collectionContainer)
            : base(loan, collectionContainer)
        {
        }

        public IAppLevelToLoanLevelMigrator<DataObjectKind.Asset> AssetMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.Asset>>();
        public IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord> BorrowerEmploymentRecordMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord>>();
        public IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord> CoborrowerEmploymentRecordMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord>>();
        public IAppLevelToLoanLevelMigrator<DataObjectKind.Liability> LiabilityMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.Liability>>();
        public IAppLevelToLoanLevelMigrator<DataObjectKind.RealProperty> RealPropertyMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.RealProperty>>();
        public IAppLevelToLoanLevelMigrator<DataObjectKind.PublicRecord> PublicRecordMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.PublicRecord>>();
        public IAppLevelToLoanLevelMigrator<DataObjectKind.VaPreviousLoan> VaPreviousLoanMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.VaPreviousLoan>>();
        public IAppLevelToLoanLevelMigrator<DataObjectKind.VorRecord> VorRecordMigrator { get; set; } = Substitute.For<IAppLevelToLoanLevelMigrator<DataObjectKind.VorRecord>>();

        protected override IAppLevelToLoanLevelMigrator<DataObjectKind.Asset> GetAssetMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return this.AssetMigrator;
        }

        protected override IAppLevelToLoanLevelMigrator<DataObjectKind.EmploymentRecord> GetEmploymentMigrator(
            E_BorrowerModeT borrowerMode,
            IUladDataLayerMigrationAppDataProvider app)
        {
            return borrowerMode == E_BorrowerModeT.Borrower
                ? this.BorrowerEmploymentRecordMigrator
                : this.CoborrowerEmploymentRecordMigrator;
        }

        protected override IAppLevelToLoanLevelMigrator<DataObjectKind.Liability> GetLiabilityMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return this.LiabilityMigrator;
        }

        protected override IAppLevelToLoanLevelMigrator<DataObjectKind.PublicRecord> GetPublicRecordMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return this.PublicRecordMigrator;
        }

        protected override IAppLevelToLoanLevelMigrator<DataObjectKind.RealProperty> GetReoMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return this.RealPropertyMigrator;
        }

        protected override IAppLevelToLoanLevelMigrator<DataObjectKind.VaPreviousLoan> GetVaPastLoanMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return this.VaPreviousLoanMigrator;
        }

        protected override IAppLevelToLoanLevelMigrator<DataObjectKind.VorRecord> GetVorRecordMigrator(IUladDataLayerMigrationAppDataProvider app)
        {
            return this.VorRecordMigrator;
        }
    }
}