﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Test.Developers.Fakes;
    using global::LendingQB.Test.Developers.Utils;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LoanLevelCollectionMethodsTest
    {
        [Test]
        public void AddPrimaryEmployment_WhenCalled_CallsAddPrimaryEmploymentOnContainer()
        {
            // This method was hand-rolled, make sure it is wired up correctly.
            var pageData = this.GetLoan();
            var pageBase = CPageBaseExtractor.Extract(pageData);
            var fakeCollectionContainer = Substitute.For<ILoanLqbCollectionContainer>();
            pageBase.loanLqbCollectionContainer = fakeCollectionContainer;
            this.InitSaveLoan(pageData);
            var primaryApp = pageData.GetAppData(0);
            var consumerId = primaryApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            var employment = new EmploymentRecord(pageBase.CreateEmploymentRecordDefaultsProvider());
            employment.IsPrimary = true;

            pageData.AddEmploymentRecord(consumerId, employment);

            fakeCollectionContainer.Received().Add(consumerId, employment);
        }

        [Test]
        public void AddAsset_WhenCalled_CallsAddOnContainer()
        {
            // This method was generated, by testing it I'm considering the 
            // equivalent generated methods tested.
            var pageData = this.GetLoan();
            var pageBase = CPageBaseExtractor.Extract(pageData);
            var fakeCollectionContainer = Substitute.For<ILoanLqbCollectionContainer>();
            pageBase.loanLqbCollectionContainer = fakeCollectionContainer;
            this.InitSaveLoan(pageData);
            var primaryApp = pageData.GetAppData(0);
            var appId = primaryApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>();
            var consumerId = primaryApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            var asset = new Asset();

            pageData.AddAsset(consumerId, asset);

            fakeCollectionContainer.Received().Add(consumerId, asset);
        }

        [Test]
        public void RemoveAsset_WhenCalled_CallsRemoveOnContainer()
        {
            // This method was generated, by testing it I'm considering the 
            // equivalent generated methods tested.
            var pageData = this.GetLoan();
            var pageBase = CPageBaseExtractor.Extract(pageData);
            var fakeCollectionContainer = Substitute.For<ILoanLqbCollectionContainer>();
            pageBase.loanLqbCollectionContainer = fakeCollectionContainer;
            this.InitSaveLoan(pageData);
            var assetId = Guid.NewGuid().ToIdentifier<DataObjectKind.Asset>();

            pageData.RemoveAsset(assetId);

            fakeCollectionContainer.Received().Remove(assetId);
        }

        [Test]
        public void ClearAssets_ThreeAssetsOnFile_CallsRemoveOnContainerForEach()
        {
            // This method was generated, by testing it I'm considering the 
            // equivalent generated methods tested.
            var pageData = this.GetLoan();
            var pageBase = CPageBaseExtractor.Extract(pageData);
            var fakeCollectionContainer = Substitute.For<ILoanLqbCollectionContainer>();
            var assetCollection = this.GetAssetCollection(3);
            fakeCollectionContainer.Assets.Returns(assetCollection);
            pageBase.loanLqbCollectionContainer = fakeCollectionContainer;
            this.InitSaveLoan(pageData);
            var assetId = Guid.NewGuid().ToIdentifier<DataObjectKind.Asset>();

            pageData.ClearAssets();

            fakeCollectionContainer.Received(3).Remove(Arg.Any<DataObjectIdentifier<DataObjectKind.Asset, Guid>>());
        }

        [Test]
        public void AddAssetOwnership_WhenCalled_CallsAddOwnershipOnContainer()
        {
            // This method was generated, by testing it I'm considering the 
            // equivalent generated methods tested.
            var pageData = this.GetLoan();
            var pageBase = CPageBaseExtractor.Extract(pageData);
            var fakeCollectionContainer = Substitute.For<ILoanLqbCollectionContainer>();
            pageBase.loanLqbCollectionContainer = fakeCollectionContainer;
            this.InitSaveLoan(pageData);
            var primaryApp = pageData.GetAppData(0);
            var appId = primaryApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>();
            var consumerId = primaryApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            var assetId = Guid.NewGuid().ToIdentifier<DataObjectKind.Asset>();

            pageData.AddAssetOwnership(consumerId, assetId);

            fakeCollectionContainer.Received().AddOwnership(consumerId, assetId);
        }

        [Test]
        public void SetAssetOwnership_WhenCalled_CallsSetOwnershipOnContainer()
        {
            // This method was generated, by testing it I'm considering the 
            // equivalent generated methods tested.
            var pageData = this.GetLoan();
            var pageBase = CPageBaseExtractor.Extract(pageData);
            var fakeCollectionContainer = Substitute.For<ILoanLqbCollectionContainer>();
            pageBase.loanLqbCollectionContainer = fakeCollectionContainer;
            this.InitSaveLoan(pageData);
            var primaryApp = pageData.GetAppData(0);
            var appId = primaryApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>();
            var consumerId = primaryApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            var assetId = Guid.NewGuid().ToIdentifier<DataObjectKind.Asset>();
            var additionalOwners = Enumerable.Empty<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>();

            pageData.SetAssetOwnership(assetId, consumerId, additionalOwners);

            fakeCollectionContainer.Received().SetOwnership(assetId, consumerId, additionalOwners);
        }

        private CPageData GetLoan()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                return FakePageData.Create();
            }
        }

        private void InitSaveLoan(CPageData loan)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                loan.InitSave();
            }
        }

        private IReadOnlyOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset> GetAssetCollection(int numAssets)
        {
            var name = Name.Create("Asset").Value;
            var collection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(name, new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            var ordering = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            var orderedCollection = OrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(collection, ordering);

            for (int i = 0; i < numAssets; ++i)
            {
                orderedCollection.Add(new Asset());
            }

            return orderedCollection;
        }
    }
}
