﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class PublicRecordMigratorTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = new Guid("11111111-1111-1111-1111-111111111111").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId = new Guid("22222222-2222-2222-2222-222222222222").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();

        #region Fake Data Points
        private static readonly List<CPublicRecordAuditItem> AuditTrailItems = new List<CPublicRecordAuditItem>
        {
            new CPublicRecordAuditItem("User", "Date", "Desc")
        };
        private const int BankruptcyLiabilitiesAmount = 1;
        private static readonly CDateTime BkFileD = CDateTime.Create(new DateTime(2018, 10, 1));
        private const string CourtName = "People's Court";
        private static readonly CDateTime DispositionD = CDateTime.InvalidWrapValue;
        private const E_CreditPublicRecordDispositionType DispositionT = E_CreditPublicRecordDispositionType.Canceled;
        private const string IdFromCreditReport = "Credit Report ID";
        private const bool IncludeInPricing = true;
        private static readonly CDateTime LastEffectiveD = CDateTime.Create(new DateTime(2018, 10, 2));
        private static readonly CDateTime ReportedD = CDateTime.Create(new DateTime(2018, 10, 3));
        private const E_CreditPublicRecordType Type = E_CreditPublicRecordType.WaterAndSewerLien;
        #endregion

        [Test]
        public void Migrate_BorrowerOwnedRecord_CreatesOwnershipAssociationForBorrower()
        {
            var appFake = this.GetAppFake(E_PublicRecordOwnerT.Borrower);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new PublicRecordMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerPublicRecords.Count);
            var association = container.ConsumerPublicRecords.Values.Single();
            Assert.AreEqual(container.PublicRecords.Keys.Single(), association.PublicRecordId);
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecord_CreatesOwnershipAssociationForCoborrower()
        {
            var appFake = this.GetAppFake(E_PublicRecordOwnerT.CoBorrower, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new PublicRecordMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerPublicRecords.Count);
            var association = container.ConsumerPublicRecords.Values.Single();
            Assert.AreEqual(container.PublicRecords.Keys.Single(), association.PublicRecordId);
            Assert.AreEqual(CoborrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_PublicRecordOwnerT.CoBorrower, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new PublicRecordMigrator(appFake, container);

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        [Test]
        public void Migrate_JointlyOwnedRecord_CreatesOwnershipAssociationForBothConsumers()
        {
            var appFake = this.GetAppFake(E_PublicRecordOwnerT.Joint, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new PublicRecordMigrator(appFake, container);

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(2, container.ConsumerPublicRecords.Count);
            Assert.AreEqual(1, container.ConsumerPublicRecords.Values.Count(a => a.ConsumerId == BorrowerId));
            Assert.AreEqual(1, container.ConsumerPublicRecords.Values.Count(a => a.ConsumerId == CoborrowerId));
            var borrowerAssociation = container.ConsumerPublicRecords.Values.Single(a => a.ConsumerId == BorrowerId);
            Assert.AreEqual(container.PublicRecords.Keys.Single(), borrowerAssociation.PublicRecordId);
            Assert.AreEqual(true, borrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, borrowerAssociation.AppId);

            var coborrowerAssociation = container.ConsumerPublicRecords.Values.Single(a => a.ConsumerId == CoborrowerId);
            Assert.AreEqual(container.PublicRecords.Keys.Single(), coborrowerAssociation.PublicRecordId);
            Assert.AreEqual(false, coborrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, coborrowerAssociation.AppId);
        }

        [Test]
        public void Migrate_JointlyOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_PublicRecordOwnerT.Joint, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new PublicRecordMigrator(appFake, container);

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        private IUladDataLayerMigrationAppDataProvider GetAppFake(E_PublicRecordOwnerT fakeOwnerType, bool hasCoborrower = false)
        {
            var appFake = Substitute.For<IUladDataLayerMigrationAppDataProvider>();
            appFake.aAppId.Returns(AppId.Value);
            appFake.aBConsumerId.Returns(BorrowerId.Value);
            appFake.aCConsumerId.Returns(CoborrowerId.Value);
            appFake.aHasCoborrower.Returns(hasCoborrower);

            var recordFake = Substitute.For<ICollectionItemBase2, IPublicRecord>();
            ((IPublicRecord)recordFake).OwnerT.Returns(fakeOwnerType);
            var collectionFake = Substitute.For<IRecordCollection, IPublicRecordCollection>();
            collectionFake.CountRegular.Returns(1);
            collectionFake.GetRegularRecordAt(Arg.Any<int>()).Returns(recordFake);
            appFake.aPublicRecordCollection.Returns(collectionFake);

            return appFake;
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(IUladDataLayerMigrationAppDataProvider app)
        {
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var appConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { app });
            loanData.LegacyApplicationConsumers.Returns(appConsumers);
            var container = new LoanLqbCollectionContainer(
                new LoanLqbEmptyCollectionProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null) as ILoanLqbCollectionContainer;
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.PublicRecords), nameof(ILoanLqbCollectionContainer.ConsumerPublicRecords) });
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());
            return container;
        }

        [Test]
        public void CreateEntity_FilledOutLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakePublicRecord();
            var testMigrator = new TestPublicRecordMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertDataCopiedOver(entity);
        }

        private IPublicRecord GetFakePublicRecord()
        {
            IPublicRecord legacy = Substitute.For<IPublicRecord>();
            legacy.AuditTrailItems.Returns(AuditTrailItems);
            legacy.BankruptcyLiabilitiesAmount.Returns(BankruptcyLiabilitiesAmount);
            legacy.BkFileD.Returns(BkFileD);
            legacy.CourtName.Returns(CourtName);
            legacy.DispositionD.Returns(DispositionD);
            legacy.DispositionT.Returns(DispositionT);
            legacy.IdFromCreditReport.Returns(IdFromCreditReport);
            legacy.IncludeInPricing.Returns(IncludeInPricing);
            legacy.LastEffectiveD.Returns(LastEffectiveD);
            legacy.ReportedD.Returns(ReportedD);
            legacy.Type.Returns(Type);
            return legacy;
        }

        private void AssertDataCopiedOver(PublicRecord entity)
        {
            Assert.AreEqual(AuditTrailItems, entity.AuditTrail);
            Assert.AreEqual(Money.Create(BankruptcyLiabilitiesAmount), entity.BankruptcyLiabilitiesAmount);
            Assert.AreEqual(BkFileD.DateTimeForComputation, entity.BkFileDate.Value.Date);
            Assert.AreEqual(CourtName, entity.CourtName.ToString());
            Assert.AreEqual(null, entity.DispositionDate);
            Assert.AreEqual(DispositionT, entity.DispositionType);
            Assert.AreEqual(IdFromCreditReport, entity.IdFromCreditReport.ToString());
            Assert.AreEqual(IncludeInPricing, entity.IncludeInPricing);
            Assert.AreEqual(LastEffectiveD.DateTimeForComputation, entity.LastEffectiveDate.Value.Date);
            Assert.AreEqual(ReportedD.DateTimeForComputation, entity.ReportedDate.Value.Date);
            Assert.AreEqual(Type, entity.Type);
        }
    }

    internal class TestPublicRecordMigrator : PublicRecordMigrator
    {
        public TestPublicRecordMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer container)
            : base(app, container)
        {
        }

        public new PublicRecord CreateEntity(IPublicRecord legacy)
        {
            return base.CreateEntity(legacy);
        }
    }
}
