﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityMigratorTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = new Guid("11111111-1111-1111-1111-111111111111").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId = new Guid("22222222-2222-2222-2222-222222222222").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();

        #region Fake Data Points
        private readonly bool NotUsedInRatio = false;
        private readonly List<LiabilityPmlAuditTrailEvent> PmlAuditTrail = new List<LiabilityPmlAuditTrailEvent>
        {
            new LiabilityPmlAuditTrailEvent("User", "Login", "Action", "Field", "Value", "Date")
        };
        private readonly decimal Pmt = 1;
        private readonly string RemainMons = "2 mon";
        private readonly bool WillBePdOff = true;
        private readonly string AccNm = "Name";
        private readonly Sensitive<string> AccNum = "Num";
        private readonly string Attention = "May I have your Attention";
        private readonly string AutoYearMake = "1989 Chevy";
        private readonly decimal Bal = 3;
        private readonly string ComAddr = "123 Test Cir.";
        private readonly string ComCity = "Make Believe";
        private readonly string ComFax = "1111111111";
        private readonly string ComNm = "Liability";
        private readonly string ComPhone = "2222222222";
        private readonly string ComState = "OR";
        private readonly string ComZip = "12345";
        private readonly E_DebtRegularT RegularDebtT = E_DebtRegularT.Revolving;
        private readonly string Desc = "Description";
        private readonly string Due = "A lot";
        private readonly bool ExcFromUnderwriting = true;
        private readonly decimal FullyIndexedPITIPayment = 5;
        private readonly bool IncInBankruptcy = false;
        private readonly bool IncInForeclosure = true;
        private readonly bool IncInReposession = false;
        private readonly bool IsForAuto = true;
        private readonly bool IsMortFHAInsured = false;
        private readonly bool IsPiggyBack = true;
        private readonly bool IsSeeAttachment = true;
        private readonly bool IsSubjectProperty1stMortgage = true;
        private readonly bool IsSubjectPropertyMortgage = true;
        private readonly string Late30 = "30";
        private readonly string Late60 = "60";
        private readonly string Late90Plus = "90+";
        private readonly decimal OrigDebtAmt = 6;
        private readonly string OrigTerm = "7";
        private readonly decimal PayoffAmt = 8;
        private readonly bool PayoffAmtLckd = true;
        private readonly E_Timing TestPayoffTiming = E_Timing.After_Closing;
        private readonly bool PayoffTimingLckd = true;
        private readonly CDateTime PrepD = CDateTime.Create(new DateTime(2000, 1, 1));
        private readonly E_LiabilityReconcileStatusT ReconcileStatusT = E_LiabilityReconcileStatusT.Matched;
        private readonly decimal R = 9;
        private readonly bool UsedInRatio = true;
        private readonly CDateTime VerifExpD = CDateTime.Create(new DateTime(2001, 1, 1));
        private readonly bool VerifHasSignature = true;
        private readonly CDateTime VerifRecvD = CDateTime.Create(new DateTime(2002, 1, 1));
        private readonly CDateTime VerifReorderedD = CDateTime.Create(new DateTime(2003, 1, 1));
        private readonly CDateTime VerifSentD = CDateTime.Create(new DateTime(2004, 1, 1));
        private readonly string VerifSignatureImgId = "44444444-4444-4444-4444-444444444444";
        private readonly Guid VerifSigningEmployeeId = new Guid("55555555-5555-5555-5555-555555555555");
        #endregion

        [Test]
        public void Migrate_BorrowerOwnedRecord_CreatesOwnershipAssociationForBorrower()
        {
            var appFake = this.GetAppFake(E_LiaOwnerT.Borrower);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new LiabilityMigrator(appFake, container, Substitute.For<ILiabilityDefaultsProvider>());

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerLiabilities.Count);
            var association = container.ConsumerLiabilities.Values.Single();
            Assert.AreEqual(container.Liabilities.Keys.Single(), association.LiabilityId);
            Assert.AreEqual(BorrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecord_CreatesOwnershipAssociationForCoborrower()
        {
            var appFake = this.GetAppFake(E_LiaOwnerT.CoBorrower, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new LiabilityMigrator(appFake, container, Substitute.For<ILiabilityDefaultsProvider>());

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.ConsumerLiabilities.Count);
            var association = container.ConsumerLiabilities.Values.Single();
            Assert.AreEqual(container.Liabilities.Keys.Single(), association.LiabilityId);
            Assert.AreEqual(CoborrowerId, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.AppId);
        }

        [Test]
        public void Migrate_CoborrowerOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_LiaOwnerT.CoBorrower, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new LiabilityMigrator(appFake, container, Substitute.For<ILiabilityDefaultsProvider>());

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        [Test]
        public void Migrate_JointlyOwnedRecord_CreatesOwnershipAssociationForBothConsumers()
        {
            var appFake = this.GetAppFake(E_LiaOwnerT.Joint, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new LiabilityMigrator(appFake, container, Substitute.For<ILiabilityDefaultsProvider>());

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(2, container.ConsumerLiabilities.Count);
            Assert.AreEqual(1, container.ConsumerLiabilities.Values.Count(a => a.ConsumerId == BorrowerId));
            Assert.AreEqual(1, container.ConsumerLiabilities.Values.Count(a => a.ConsumerId == CoborrowerId));
            var borrowerAssociation = container.ConsumerLiabilities.Values.Single(a => a.ConsumerId == BorrowerId);
            Assert.AreEqual(container.Liabilities.Keys.Single(), borrowerAssociation.LiabilityId);
            Assert.AreEqual(true, borrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, borrowerAssociation.AppId);

            var coborrowerAssociation = container.ConsumerLiabilities.Values.Single(a => a.ConsumerId == CoborrowerId);
            Assert.AreEqual(container.Liabilities.Keys.Single(), coborrowerAssociation.LiabilityId);
            Assert.AreEqual(false, coborrowerAssociation.IsPrimary);
            Assert.AreEqual(AppId, coborrowerAssociation.AppId);
        }

        [Test]
        public void Migrate_JointlyOwnedRecordButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_LiaOwnerT.Joint, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new LiabilityMigrator(appFake, container, Substitute.For<ILiabilityDefaultsProvider>());

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        private IUladDataLayerMigrationAppDataProvider GetAppFake(E_LiaOwnerT fakeOwnerType, bool hasCoborrower = false)
        {
            var appFake = Substitute.For<IUladDataLayerMigrationAppDataProvider>();
            appFake.aAppId.Returns(AppId.Value);
            appFake.aBConsumerId.Returns(BorrowerId.Value);
            appFake.aCConsumerId.Returns(CoborrowerId.Value);
            appFake.aHasCoborrower.Returns(hasCoborrower);

            var recordFake = Substitute.For<ICollectionItemBase2, ILiability>();
            ((ILiability)recordFake).OwnerT.Returns(fakeOwnerType);
            var collectionFake = Substitute.For<IRecordCollection, ILiaCollection>();
            collectionFake.CountRegular.Returns(1);
            collectionFake.GetRegularRecordAt(Arg.Any<int>()).Returns(recordFake);
            appFake.aLiaCollection.Returns(collectionFake);

            return appFake;
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(IUladDataLayerMigrationAppDataProvider app)
        {
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var appConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { app });
            loanData.LegacyApplicationConsumers.Returns(appConsumers);
            var container = new LoanLqbCollectionContainer(
                new LoanLqbEmptyCollectionProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null) as ILoanLqbCollectionContainer;
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.Liabilities), nameof(ILoanLqbCollectionContainer.ConsumerLiabilities) });
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());
            return container;
        }

        [Test]
        public void CreateEntity_FilledOutRegularLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeRegularLiability();
            var testMigrator = new TestLiabilityMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertRegularDataCopiedOver(entity);
            this.AssertSpecialDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutAlimonyLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeAlimonyLiability();
            var testMigrator = new TestLiabilityMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertAlimonyDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutChildSupportLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeChildSupportLiability();
            var testMigrator = new TestLiabilityMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertChildSupportDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutJobExpenseLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeJobExpenseLiability();
            var testMigrator = new TestLiabilityMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertJobExpenseDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_InvalidVerifSignatureImgIdInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<ILiabilityRegular>();
            fakeLegacyRecord.VerifHasSignature.Returns(true);
            fakeLegacyRecord.VerifSignatureImgId.Returns("Meow");
            var testMigrator = new TestLiabilityMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        [Test]
        public void CreateEntity_InvalidAuditTrailInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<ILiabilityRegular>();
            fakeLegacyRecord.PmlAuditTrailXmlContent = "This is not xml :)";
            var testMigrator = new TestLiabilityMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        private ILiabilityJobExpense GetFakeJobExpenseLiability()
        {
            var fake = Substitute.For<ILiabilityJobExpense>();
            this.FakeBaseLiabilityFields(fake);
            ((ILiability)fake).DebtT.Returns(E_DebtT.JobRelatedExpense);
            fake.DebtT.Returns(E_DebtSpecialT.JobRelatedExpense);
            fake.ExpenseDesc = "Cat food";
            return fake;
        }

        private void AssertJobExpenseDataCopiedOver(Liability entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual(E_DebtT.JobRelatedExpense, entity.DebtType);
            Assert.AreEqual("Cat food", entity.JobExpenseDesc.ToString());
        }

        private ILiabilityChildSupport GetFakeChildSupportLiability()
        {
            var fake = Substitute.For<ILiabilityChildSupport>();
            this.FakeBaseLiabilityFields(fake);
            ((ILiability)fake).DebtT.Returns(E_DebtT.ChildSupport);
            fake.DebtT.Returns(E_DebtSpecialT.ChildSupport);
            fake.OwedTo = "ME!!";
            return fake;
        }

        private void AssertChildSupportDataCopiedOver(Liability entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual(E_DebtT.ChildSupport, entity.DebtType);
            Assert.AreEqual("ME!!", entity.OwedTo.ToString());
        }

        private ILiabilityAlimony GetFakeAlimonyLiability()
        {
            var fake = Substitute.For<ILiabilityAlimony>();
            this.FakeBaseLiabilityFields(fake);
            ((ILiability)fake).DebtT.Returns(E_DebtT.Alimony);
            fake.DebtT.Returns(E_DebtSpecialT.Alimony);
            fake.OwedTo = "ME!!";
            return fake;
        }

        private void AssertAlimonyDataCopiedOver(Liability entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual(E_DebtT.Alimony, entity.DebtType);
            Assert.AreEqual("ME!!", entity.OwedTo.ToString());
        }

        private void AssertRegularDataCopiedOver(Liability entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual((E_DebtT)RegularDebtT, entity.DebtType);
            Assert.AreEqual(AccNm, entity.AccountName.ToString());
            Assert.AreEqual(AccNum.Value, entity.AccountNum.ToString());
            Assert.AreEqual(Attention, entity.Attention.ToString());
            Assert.AreEqual(AutoYearMake, entity.AutoYearMakeAsString.ToString());
            Assert.AreEqual(Bal, (decimal)entity.Bal);
            Assert.AreEqual(ComAddr, entity.CompanyAddress.ToString());
            Assert.AreEqual(ComCity, entity.CompanyCity.ToString());
            Assert.AreEqual(ComFax, entity.CompanyFax.ToString());
            Assert.AreEqual(ComNm, entity.CompanyName.ToString());
            Assert.AreEqual(ComPhone, entity.CompanyPhone.ToString());
            Assert.AreEqual(ComState, entity.CompanyState.ToString());
            Assert.AreEqual(ComZip, entity.CompanyZip.ToString());
            Assert.AreEqual(Desc, entity.Desc.ToString());
            Assert.AreEqual(Due, entity.DueInMonAsString.ToString());
            Assert.AreEqual(ExcFromUnderwriting, entity.ExcludeFromUw);
            Assert.AreEqual(FullyIndexedPITIPayment, (decimal)entity.FullyIndexedPitiPmt);
            Assert.AreEqual(IncInBankruptcy, entity.IncludeInBk);
            Assert.AreEqual(IncInForeclosure, entity.IncludeInFc);
            Assert.AreEqual(IncInReposession, entity.IncludeInReposession);
            Assert.AreEqual(IsForAuto, entity.IsForAuto);
            Assert.AreEqual(IsMortFHAInsured, entity.IsMtgFhaInsured);
            Assert.AreEqual(IsPiggyBack, entity.IsPiggyBack);
            Assert.AreEqual(IsSeeAttachment, entity.IsSeeAttachment);
            Assert.AreEqual(IsSubjectProperty1stMortgage, entity.IsSp1stMtgData);
            Assert.AreEqual(Late30, entity.Late30AsString.ToString());
            Assert.AreEqual(Late60, entity.Late60AsString.ToString());
            Assert.AreEqual(Late90Plus, entity.Late90PlusAsString.ToString());
            Assert.AreEqual(OrigDebtAmt, (decimal)entity.OriginalDebtAmt);
            Assert.AreEqual(OrigTerm, entity.OriginalTermAsString.ToString());
            Assert.AreEqual(PayoffAmt, (decimal)entity.PayoffAmtData);
            Assert.AreEqual(PayoffAmtLckd, entity.PayoffAmtLocked);
            Assert.AreEqual((PayoffTiming)TestPayoffTiming, entity.PayoffTimingData);
            Assert.AreEqual(PayoffTimingLckd, entity.PayoffTimingLockedData);
            Assert.AreEqual(PrepD.DateTimeForComputation, entity.PrepDate.Value.Date);
            Assert.AreEqual(R, (decimal)entity.Rate);
            Assert.AreEqual(ReconcileStatusT, entity.ReconcileStatusType);
            Assert.AreEqual(VerifExpD.DateTimeForComputation, entity.VerifExpiresDate.Value.Date);
            Assert.AreEqual(VerifRecvD.DateTimeForComputation, entity.VerifRecvDate.Value.Date);
            Assert.AreEqual(VerifReorderedD.DateTimeForComputation, entity.VerifReorderedDate.Value.Date);
            Assert.AreEqual(VerifSentD.DateTimeForComputation, entity.VerifSentDate.Value.Date);
            Assert.AreEqual(VerifSignatureImgId, entity.VerifSignatureImgId.ToString());
            Assert.AreEqual(VerifSigningEmployeeId, entity.VerifSigningEmployeeId.Value.Value);
        }

        private void AssertSpecialDataNull(Liability entity)
        {
            Assert.AreEqual(null, entity.OwedTo);
            Assert.AreEqual(null, entity.JobExpenseDesc);
        }

        private void AssertRegularDataNull(Liability entity)
        {
            Assert.AreEqual(null, entity.AccountName);
            Assert.AreEqual(null, entity.AccountNum);
            Assert.AreEqual(null, entity.Attention);
            Assert.AreEqual(null, entity.AutoYearMakeAsString);
            Assert.AreEqual(null, entity.Bal);
            Assert.AreEqual(null, entity.CompanyAddress);
            Assert.AreEqual(null, entity.CompanyCity);
            Assert.AreEqual(null, entity.CompanyFax);
            Assert.AreEqual(null, entity.CompanyName);
            Assert.AreEqual(null, entity.CompanyPhone);
            Assert.AreEqual(null, entity.CompanyState);
            Assert.AreEqual(null, entity.CompanyZip);
            Assert.AreEqual(null, entity.Desc);
            Assert.AreEqual(null, entity.DueInMonAsString);
            Assert.AreEqual(null, entity.ExcludeFromUw);
            Assert.AreEqual(null, entity.FullyIndexedPitiPmt);
            Assert.AreEqual(null, entity.IncludeInBk);
            Assert.AreEqual(null, entity.IncludeInFc);
            Assert.AreEqual(null, entity.IncludeInReposession);
            Assert.AreEqual(null, entity.IsForAuto);
            Assert.AreEqual(null, entity.IsMtgFhaInsured);
            Assert.AreEqual(null, entity.IsPiggyBack);
            Assert.AreEqual(null, entity.IsSeeAttachment);
            Assert.AreEqual(null, entity.IsSp1stMtgData);
            Assert.AreEqual(null, entity.Late30AsString);
            Assert.AreEqual(null, entity.Late60AsString);
            Assert.AreEqual(null, entity.Late90PlusAsString);
            Assert.AreEqual(null, entity.OriginalDebtAmt);
            Assert.AreEqual(null, entity.OriginalTermAsString);
            Assert.AreEqual(null, entity.PayoffAmtData);
            Assert.AreEqual(null, entity.PayoffAmtLocked);
            Assert.AreEqual(null, entity.PayoffTimingData);
            Assert.AreEqual(null, entity.PayoffTimingLockedData);
            Assert.AreEqual(null, entity.PrepDate);
            Assert.AreEqual(null, entity.Rate);
            Assert.AreEqual(null, entity.ReconcileStatusType);
            Assert.AreEqual(null, entity.VerifExpiresDate);
            Assert.AreEqual(null, entity.VerifRecvDate);
            Assert.AreEqual(null, entity.VerifReorderedDate);
            Assert.AreEqual(null, entity.VerifSentDate);
            Assert.AreEqual(null, entity.VerifSignatureImgId);
            Assert.AreEqual(null, entity.VerifSigningEmployeeId);
        }

        private ILiabilityRegular GetFakeRegularLiability()
        {
            var fake = Substitute.For<ILiabilityRegular>();
            this.FakeBaseLiabilityFields(fake);
            fake.AccNm = AccNm;
            fake.AccNum = AccNum;
            fake.Attention = Attention;
            fake.AutoYearMake = AutoYearMake;
            fake.Bal = Bal;
            fake.ComAddr = ComAddr;
            fake.ComCity = ComCity;
            fake.ComFax = ComFax;
            fake.ComNm = ComNm;
            fake.ComPhone = ComPhone;
            fake.ComState = ComState;
            fake.ComZip = ComZip;
            ((ILiability)fake).DebtT.Returns((E_DebtT)RegularDebtT);
            fake.DebtT = RegularDebtT;
            fake.Desc = Desc;
            fake.Due_rep = Due;
            fake.ExcFromUnderwriting = ExcFromUnderwriting;
            fake.FullyIndexedPITIPayment = FullyIndexedPITIPayment;
            fake.IncInBankruptcy = IncInBankruptcy;
            fake.IncInForeclosure = IncInForeclosure;
            fake.IncInReposession = IncInReposession;
            fake.IsForAuto = IsForAuto;
            fake.IsMortFHAInsured = IsMortFHAInsured;
            fake.IsPiggyBack = IsPiggyBack;
            fake.IsSeeAttachment = IsSeeAttachment;
            fake.IsSubjectProperty1stMortgage = IsSubjectProperty1stMortgage;
            fake.IsSubjectPropertyMortgage = IsSubjectPropertyMortgage;
            fake.Late30_rep = Late30;
            fake.Late60_rep = Late60;
            fake.Late90Plus_rep = Late90Plus;
            fake.OrigDebtAmt = OrigDebtAmt;
            fake.OrigTerm_rep = OrigTerm;
            fake.PayoffAmt = PayoffAmt;
            fake.PayoffAmtLckd = PayoffAmtLckd;
            fake.PayoffTiming = TestPayoffTiming;
            fake.PayoffTimingLckd = PayoffTimingLckd;
            fake.PrepD = PrepD;
            fake.ReconcileStatusT = ReconcileStatusT;
            fake.R = R;
            fake.UsedInRatio = UsedInRatio;
            fake.VerifExpD = VerifExpD;
            fake.VerifHasSignature.Returns(VerifHasSignature);
            fake.VerifRecvD = VerifRecvD;
            fake.VerifReorderedD = VerifReorderedD;
            fake.VerifSentD = VerifSentD;
            fake.VerifSignatureImgId.Returns(VerifSignatureImgId);
            fake.VerifSigningEmployeeId.Returns(VerifSigningEmployeeId);
            return fake;
        }

        private void FakeBaseLiabilityFields(ILiability fake)
        {
            fake.NotUsedInRatio = NotUsedInRatio;
            var customMapper = new global::LendingQB.Core.Mapping.LiabilityPmlAuditTrailMapper();
            fake.PmlAuditTrailXmlContent = customMapper.DatabaseRepresentation(PmlAuditTrail);
            fake.Pmt = Pmt;
            fake.RemainMons_rep = RemainMons;
            fake.WillBePdOff = WillBePdOff;
        }

        private void AssertBaseDataCopiedOver(Liability entity)
        {
            Assert.AreEqual(!NotUsedInRatio, entity.UsedInRatioData);
            Assert.AreEqual(1, entity.PmlAuditTrail.Count);
            Assert.AreEqual("User", entity.PmlAuditTrail.Single().UserName);
            Assert.AreEqual("Login", entity.PmlAuditTrail.Single().LoginId);
            Assert.AreEqual("Action", entity.PmlAuditTrail.Single().Action);
            Assert.AreEqual("Field", entity.PmlAuditTrail.Single().Field);
            Assert.AreEqual("Value", entity.PmlAuditTrail.Single().Value);
            Assert.AreEqual("Date", entity.PmlAuditTrail.Single().EventDate);
            Assert.AreEqual(Pmt, (decimal)entity.Pmt);
            Assert.AreEqual(RemainMons, entity.RemainMon.ToString());
            Assert.AreEqual(WillBePdOff, entity.WillBePdOff);
        }

        internal class TestLiabilityMigrator : LiabilityMigrator
        {
            public TestLiabilityMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer container)
                : base(app, container, Substitute.For<ILiabilityDefaultsProvider>())
            {
            }

            public new Liability CreateEntity(ILiability legacy)
            {
                return base.CreateEntity(legacy);
            }
        }
    }
}
