﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class PageDataUnitTest
    {
        [Test]
        public void NotifyCollectionLoad_FileNotMigrated_DoesNotRegister()
        {
            var loan = new TestPageData(E_DataState.Normal);
            loan.sBorrowerApplicationCollectionT = E_sLqbCollectionT.Legacy;

            loan.NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.Assets));

            Assert.IsFalse(loan.IsCollectionRegistered(nameof(ILoanLqbCollectionContainer.Assets)));
        }

        [Test]
        public void NotifyCollectionLoad_ForAllCollections_DoesNotThrowException()
        {
            var loan = new TestPageData(E_DataState.Normal);

            foreach (var collectionName in LoanLqbCollectionContainer.LoanCollections)
            {
                Assert.DoesNotThrow(() => loan.NotifyCollectionLoad(collectionName), collectionName);
            }
        }

        [Test]
        public void NotifyCollectionLoad_BeforeFileLoaded_DoesNotRegisterYet()
        {
            var loan = new TestPageData(E_DataState.Normal);

            loan.NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.Assets));

            Assert.IsFalse(loan.IsCollectionRegistered("CAssetCollection"));
        }

        [Test]
        public void RegisterCollectionsAfterLoad_AfterNotifiedOfAssetLoad_RegistersAssetCollection()
        {
            var loan = new TestPageData(E_DataState.Normal);
            loan.NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.Assets));

            loan.BeginTrackingCollectionsForLegacyWorkflow();

            Assert.IsTrue(loan.IsCollectionRegistered("CAssetCollection"));
        }

        [TestCase("Assets", "CAssetCollection")]
        [TestCase("ConsumerAssets", "CAssetCollection")]
        [TestCase("EmploymentRecords", "CEmpCollection")]
        [TestCase("ConsumerEmployments", "CEmpCollection")]
        [TestCase("Liabilities", "CLiaCollection")]
        [TestCase("ConsumerLiabilities", "CLiaCollection")]
        [TestCase("RealProperties", "CReCollection")]
        [TestCase("ConsumerRealProperties", "CReCollection")]
        [TestCase("VaPreviousLoans", "CVaPastLCollection")]
        [TestCase("ConsumerVaPreviousLoans", "CVaPastLCollection")]
        [TestCase("VorRecords", "CVorFields")]
        [TestCase("ConsumerVorRecords", "CVorFields")]
        [TestCase("RealPropertyLiabilities", "CReCollection", "CLiaCollection")]
        [TestCase("PublicRecords", new string[0])]
        [TestCase("ConsumerPublicRecords", new string[0])]
        [TestCase("UladApplications", new string[0])]
        [TestCase("UladApplicationConsumers", new string[0])]
        public void NotifyCollectionLoad_WhenCalledForLoadedFile_RegistersCollectionForTracking(string newDataLayerCollectionName, params string[] expectedRegistered)
        {
            var loan = new TestPageData(E_DataState.Normal);

            loan.NotifyCollectionLoad(newDataLayerCollectionName);

            loan.BeginTrackingCollectionsForLegacyWorkflow();

            Assert.AreEqual(expectedRegistered.Length, loan.NumRegisteredCollections);
            foreach (var expected in expectedRegistered)
            {
                Assert.IsTrue(loan.IsCollectionRegistered(expected));
            }
        }

        [Test]
        public void NotifyCollectionLoad_CalledWithTwoCollectionsThatRegisterSameLegacyCollection_RegistersLegacyCollectionOnce()
        {
            var loan = new TestPageData(E_DataState.Normal);

            loan.NotifyCollectionLoad("Assets");
            loan.NotifyCollectionLoad("ConsumerAssets");

            loan.BeginTrackingCollectionsForLegacyWorkflow();
            Assert.AreEqual(1, loan.NumRegisteredCollections);
        }

        [Test]
        public void NotifyCollectionLoad_LoanWithoutAccessControl_DoesNotRegisterAnyCollections()
        {
            var loan = new TestPageData(E_DataState.Normal)
            {
                EnforceAccessControl = false,
            };

            loan.NotifyCollectionLoad("Assets");
            loan.NotifyCollectionLoad("ConsumerAssets");

            loan.BeginTrackingCollectionsForLegacyWorkflow();
            Assert.AreEqual(0, loan.NumRegisteredCollections);
        }
    }

    internal class TestPageData : CPageData
    {
        public TestPageData(E_DataState dataState)
            : base(Guid.Empty)
        {
            this.DataState = dataState;
        }

        public new bool EnforceAccessControl { get; set; } = true;

        public override E_DataState DataState { get; }

        public int NumRegisteredCollections => this.originalCollectionSnapshotsForLegacyWorkflow.Count;

        public override E_sLqbCollectionT sBorrowerApplicationCollectionT { get; set; } = E_sLqbCollectionT.UseLqbCollections;

        public bool IsCollectionRegistered(string collectionName)
        {
            return this.originalCollectionSnapshotsForLegacyWorkflow.ContainsKey(collectionName);
        }

        new public void BeginTrackingCollectionsForLegacyWorkflow()
        {
            base.BeginTrackingCollectionsForLegacyWorkflow();
        }

        protected override bool m_enforceAccessControl => this.EnforceAccessControl;

        protected override DataSet GetLoanLevelDataSetForCollection(string collectionName)
        {
            return new DataSet();
        }
    }
}
