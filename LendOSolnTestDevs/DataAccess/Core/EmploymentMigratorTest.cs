﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class EmploymentMigratorTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> BorrowerId = new Guid("11111111-1111-1111-1111-111111111111").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> CoborrowerId = new Guid("22222222-2222-2222-2222-222222222222").ToIdentifier<DataObjectKind.Consumer>();
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = new Guid("33333333-3333-3333-3333-333333333333").ToIdentifier<DataObjectKind.LegacyApplication>();

        #region Fake Data Points
        private readonly List<EmploymentRecordVOEData> TestEmploymentRecordVOEData = new List<EmploymentRecordVOEData>
        {
            new EmploymentRecordVOEData
            {
                EmployeeIdVoe = "44444444-4444-4444-4444-444444444444",
                EmployerCodeVoe = "VOE Employer Code"
            }
        };
        private readonly string EmplrNm = "MeridianLink";
        private readonly string EmplrAddr = "1600 Sunflower";
        private readonly string EmplrCity = "Costa Mesa";
        private readonly string EmplrZip = "92626";
        private readonly string EmplrBusPhone = "1111111111";
        private readonly string EmplrFax = "2222222222";
        private readonly string JobTitle = "Free Food Consumer";
        private readonly CDateTime VerifReorderedD = CDateTime.Create(new DateTime(2018, 1, 1));
        private readonly CDateTime VerifExpD = CDateTime.Create(new DateTime(2018, 1, 2));
        private readonly CDateTime VerifRecvD = CDateTime.Create(new DateTime(2018, 1, 3));
        private readonly CDateTime VerifSentD = CDateTime.Create(new DateTime(2018, 1, 4));
        private readonly string EmplrState = "CA";
        private readonly E_EmplmtStat EmplmtStat = E_EmplmtStat.Current;
        private readonly string Attention = "Mr Boss Man";
        private readonly CDateTime PrepD = CDateTime.Create(new DateTime(2018, 1, 5));
        private readonly bool IsSeeAttachment = false;
        private readonly bool IsCurrent = true;
        private readonly Guid VerifSigningEmployeeId = new Guid("55555555-5555-5555-5555-555555555555");
        private readonly string VerifSignatureImgId = new Guid("66666666-6666-6666-6666-666666666666").ToString();
        private readonly bool VerifHasSignature = true;
        private bool IsSelfEmplmt = false;
        #endregion

        [Test]
        public void Migrate_BorrowerPrimaryEmployment_CreatesPrimaryRecordForBorrower()
        {
            var appFake = this.GetAppFake(E_BorrowerModeT.Borrower, primaryEmployment: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new EmploymentMigrator(E_BorrowerModeT.Borrower, appFake, container, Substitute.For<IEmploymentRecordDefaultsProvider>());

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            var entity = container.EmploymentRecords.Values.Single();
            Assert.AreEqual(true, entity.IsPrimary);
            Assert.AreEqual(BorrowerId, entity.ConsumerId);
            Assert.AreEqual(AppId, entity.LegacyAppId);
        }

        [Test]
        public void Migrate_BorrowerRegularEmployment_CreatesRecordForBorrower()
        {
            var appFake = this.GetAppFake(E_BorrowerModeT.Borrower, primaryEmployment: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new EmploymentMigrator(E_BorrowerModeT.Borrower, appFake, container, Substitute.For<IEmploymentRecordDefaultsProvider>());

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            var entity = container.EmploymentRecords.Values.Single();
            Assert.AreEqual(false, entity.IsPrimary);
            Assert.AreEqual(BorrowerId, entity.ConsumerId);
            Assert.AreEqual(AppId, entity.LegacyAppId);
        }

        [Test]
        public void Migrate_CoborrowerPrimaryEmployment_CreatesPrimaryRecordForCoborrower()
        {
            var appFake = this.GetAppFake(E_BorrowerModeT.Coborrower, primaryEmployment: true, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new EmploymentMigrator(E_BorrowerModeT.Coborrower, appFake, container, Substitute.For<IEmploymentRecordDefaultsProvider>());

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            var entity = container.EmploymentRecords.Values.Single();
            Assert.AreEqual(true, entity.IsPrimary);
            Assert.AreEqual(CoborrowerId, entity.ConsumerId);
            Assert.AreEqual(AppId, entity.LegacyAppId);
        }

        [Test]
        public void Migrate_CoborrowerRegularEmployment_CreatesRecordForCoborrower()
        {
            var appFake = this.GetAppFake(E_BorrowerModeT.Coborrower, primaryEmployment: false, hasCoborrower: true);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new EmploymentMigrator(E_BorrowerModeT.Coborrower, appFake, container, Substitute.For<IEmploymentRecordDefaultsProvider>());

            migrator.MigrateAppLevelRecordsToLoanLevel();

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            var entity = container.EmploymentRecords.Values.Single();
            Assert.AreEqual(false, entity.IsPrimary);
            Assert.AreEqual(CoborrowerId, entity.ConsumerId);
            Assert.AreEqual(AppId, entity.LegacyAppId);
        }

        [Test]
        public void Migrate_CoborrowerPrimaryEmploymentButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_BorrowerModeT.Coborrower, primaryEmployment: true, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new EmploymentMigrator(E_BorrowerModeT.Coborrower, appFake, container, Substitute.For<IEmploymentRecordDefaultsProvider>());

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        [Test]
        public void Migrate_CoborrowerRegularEmploymentButNoCoborrower_Throws()
        {
            var appFake = this.GetAppFake(E_BorrowerModeT.Coborrower, primaryEmployment: false, hasCoborrower: false);
            var container = this.GetCollectionContainer(appFake);
            var migrator = new EmploymentMigrator(E_BorrowerModeT.Coborrower, appFake, container, Substitute.For<IEmploymentRecordDefaultsProvider>());

            Assert.Catch<Exception>(() => migrator.MigrateAppLevelRecordsToLoanLevel());
        }

        private IUladDataLayerMigrationAppDataProvider GetAppFake(E_BorrowerModeT borrowerMode, bool primaryEmployment, bool hasCoborrower = false)
        {
            var appFake = Substitute.For<IUladDataLayerMigrationAppDataProvider>();
            appFake.aAppId.Returns(AppId.Value);
            appFake.aBConsumerId.Returns(BorrowerId.Value);
            appFake.aCConsumerId.Returns(CoborrowerId.Value);
            appFake.aHasCoborrower.Returns(hasCoborrower);

            var collectionFake = Substitute.For<IRecordCollection, IEmpCollection>();
            ICollectionItemBase2 recordFake;
            if (primaryEmployment)
            {
                recordFake = Substitute.For<ICollectionItemBase2, IEmploymentRecord, IPrimaryEmploymentRecord>();
                collectionFake.CountSpecial.Returns(1);
                collectionFake.GetSpecialRecordAt(Arg.Any<int>()).Returns(recordFake);
            }
            else
            {
                recordFake = Substitute.For<ICollectionItemBase2, IEmploymentRecord>();
                collectionFake.CountRegular.Returns(1);
                collectionFake.GetRegularRecordAt(Arg.Any<int>()).Returns(recordFake);
            }

            if (borrowerMode == E_BorrowerModeT.Borrower)
            {
                appFake.aBEmpCollection.Returns(collectionFake);
            }
            else
            {
                appFake.aCEmpCollection.Returns(collectionFake);
            }

            return appFake;
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(IUladDataLayerMigrationAppDataProvider app)
        {
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var appConsumers = Fakes.FakeLegacyApplicationConsumers.FromLegacyApplicationData(new[] { app });
            loanData.LegacyApplicationConsumers.Returns(appConsumers);
            ILoanLqbCollectionContainer container = new LoanLqbCollectionContainer(
                new LoanLqbEmptyCollectionProvider(),
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);
            container.RegisterCollections(new[] { nameof(ILoanLqbCollectionContainer.EmploymentRecords) });
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());
            return container;
        }

        [Test]
        public void CreateEntity_FilledOutRegularLegacyRecord_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = this.GetFakeRegularEmployment();
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertRegularDataCopiedOver(entity);
            this.AssertPrimaryDataNull(entity);
        }

        [Test]
        public void CreateEntity_FilledOutPrimaryEmployment_CopiesOverDataAsExpected()
        {
            IPrimaryEmploymentRecord fakeLegacyRecord = this.GetFakePrimaryEmployment();
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            this.AssertPrimaryDataCopiedOver(entity);
            this.AssertRegularDataNull(entity);
        }

        [Test]
        public void CreateEntity_InvalidVerifSignatureImgIdInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IRegularEmploymentRecord>();
            fakeLegacyRecord.VerifHasSignature.Returns(true);
            fakeLegacyRecord.VerifSignatureImgId.Returns("Meow");
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        [Test]
        public void CreateEntity_InvalidMonIInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IRegularEmploymentRecord>();
            fakeLegacyRecord.MonI_rep = "A whole lotta cashola.";
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        [Test]
        public void CreateEntity_MonIWithDollarSign_CopiesOverDataAsExpected()
        {
            var fakeLegacyRecord = Substitute.For<IRegularEmploymentRecord>();
            fakeLegacyRecord.MonI_rep = "$1,234.56";
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            var entity = testMigrator.CreateEntity(fakeLegacyRecord);

            Assert.AreEqual(1234.56M, (decimal)entity.MonthlyIncome);
        }

        [Test]
        public void CreateEntity_InvalidEmplmtStartDIInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IRegularEmploymentRecord>();
            fakeLegacyRecord.EmplmtStartD_rep = "June First Nineteen Hundred Ninety Five";
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        [Test]
        public void CreateEntity_InvalidEmplmtEndDIInLegacyRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IRegularEmploymentRecord>();
            fakeLegacyRecord.EmplmtEndD_rep = "2016";
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        [Test]
        public void CreateEntity_InvalidEmpltStartDIInLegacyPrimaryRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IPrimaryEmploymentRecord>();
            fakeLegacyRecord.EmpltStartD_rep = "June First Nineteen Hundred Ninety Nine";
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        [Test]
        public void CreateEntity_InvalidProfStartDIInLegacyPrimaryRecord_ThrowsException()
        {
            var fakeLegacyRecord = Substitute.For<IPrimaryEmploymentRecord>();
            fakeLegacyRecord.ProfStartD_rep = "The Ides of March 2018";
            var testMigrator = new TestEmploymentMigrator(
                Substitute.For<IUladDataLayerMigrationAppDataProvider>(),
                Substitute.For<ILoanLqbCollectionContainer>());

            Assert.Throws<CBaseException>(() => testMigrator.CreateEntity(fakeLegacyRecord));
        }

        private IRegularEmploymentRecord GetFakeRegularEmployment()
        {
            var legacy = Substitute.For<IRegularEmploymentRecord>();
            this.FakeBaseEmploymentFields(legacy);
            legacy.MonI_rep = "1";
            legacy.EmplmtStartD_rep = "01/01/2001";
            legacy.EmplmtEndD_rep = "01/01/2010";
            return legacy;
        }

        private void AssertRegularDataCopiedOver(EmploymentRecord entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual(1m, (decimal)entity.MonthlyIncome);
            Assert.AreEqual(new DateTime(2001, 1, 1), entity.EmploymentStartDate.Value.Date);
            Assert.AreEqual(new DateTime(2010, 1, 1), entity.EmploymentEndDate.Value.Date);
        }

        private void FakeBaseEmploymentFields(IEmploymentRecord legacy)
        {
            legacy.EmploymentRecordVOEData.Returns(TestEmploymentRecordVOEData);
            legacy.EmplrNm = EmplrNm;
            legacy.IsSelfEmplmt = IsSelfEmplmt;
            legacy.EmplrAddr = EmplrAddr;
            legacy.EmplrCity = EmplrCity;
            legacy.EmplrZip = EmplrZip;
            legacy.EmplrBusPhone = EmplrBusPhone;
            legacy.EmplrFax = EmplrFax;
            legacy.JobTitle = JobTitle;
            legacy.VerifReorderedD = VerifReorderedD;
            legacy.VerifExpD = VerifExpD;
            legacy.VerifRecvD = VerifRecvD;
            legacy.VerifSentD = VerifSentD;
            legacy.EmplrState = EmplrState;
            legacy.EmplmtStat = EmplmtStat;
            legacy.Attention = Attention;
            legacy.PrepD = PrepD;
            legacy.IsSeeAttachment = IsSeeAttachment;
            legacy.IsCurrent = IsCurrent;
            legacy.VerifSigningEmployeeId.Returns(VerifSigningEmployeeId);
            legacy.VerifSignatureImgId.Returns(VerifSignatureImgId);
            legacy.VerifHasSignature.Returns(VerifHasSignature);
        }

        private void AssertBaseDataCopiedOver(EmploymentRecord entity)
        {
            Assert.AreEqual(1, entity.EmploymentRecordVoeData.Count);
            Assert.AreEqual(TestEmploymentRecordVOEData.First().EmployeeIdVoe, entity.EmploymentRecordVoeData.First().EmployeeIdVoe);
            Assert.AreEqual(TestEmploymentRecordVOEData.First().EmployerCodeVoe, entity.EmploymentRecordVoeData.First().EmployerCodeVoe);
            Assert.AreEqual(EmplrNm, entity.EmployerName.ToString());
            Assert.AreEqual(IsSelfEmplmt, entity.IsSelfEmployed);
            Assert.AreEqual(EmplrAddr, entity.EmployerAddress.ToString());
            Assert.AreEqual(EmplrCity, entity.EmployerCity.ToString());
            Assert.AreEqual(EmplrZip, entity.EmployerZip.ToString());
            Assert.AreEqual(EmplrBusPhone, entity.EmployerPhone.ToString());
            Assert.AreEqual(EmplrFax, entity.EmployerFax.ToString());
            Assert.AreEqual(JobTitle, entity.JobTitle.ToString());
            Assert.AreEqual(VerifReorderedD.DateTimeForComputation, entity.VerifReorderedDate.Value.Date);
            Assert.AreEqual(VerifExpD.DateTimeForComputation, entity.VerifExpiresDate.Value.Date);
            Assert.AreEqual(VerifRecvD.DateTimeForComputation, entity.VerifRecvDate.Value.Date);
            Assert.AreEqual(VerifSentD.DateTimeForComputation, entity.VerifSentDate.Value.Date);
            Assert.AreEqual(EmplrState, entity.EmployerState.ToString());
            Assert.AreEqual(EmplmtStat, entity.EmploymentStatusType);
            Assert.AreEqual(Attention, entity.Attention.ToString());
            Assert.AreEqual(PrepD.DateTimeForComputation, entity.PrepDate.Value.Date);
            Assert.AreEqual(IsSeeAttachment, entity.IsSeeAttachment);
            Assert.AreEqual(IsCurrent, entity.IsCurrent);
            Assert.AreEqual(VerifSigningEmployeeId, entity.VerifSigningEmployeeId.Value.Value);
            Assert.AreEqual(VerifSignatureImgId, entity.VerifSignatureImgId.ToString());
        }

        private IPrimaryEmploymentRecord GetFakePrimaryEmployment()
        {
            var legacy = Substitute.For<IPrimaryEmploymentRecord>();
            this.FakeBaseEmploymentFields(legacy);
            legacy.ProfStartD_rep = "01/01/1999";
            legacy.EmpltStartD_rep = "01/01/2004";
            return legacy;
        }

        private void AssertPrimaryDataCopiedOver(EmploymentRecord entity)
        {
            this.AssertBaseDataCopiedOver(entity);
            Assert.AreEqual(new DateTime(1999, 01, 01), entity.ProfessionStartDate.Value.Date);
            Assert.AreEqual(new DateTime(2004, 01, 01), entity.PrimaryEmploymentStartDate.Value.Date);
        }

        private void AssertRegularDataNull(EmploymentRecord entity)
        {
            Assert.AreEqual(null, entity.MonthlyIncome);
            Assert.AreEqual(null, entity.EmploymentStartDate);
            Assert.AreEqual(null, entity.EmploymentEndDate);
        }

        private void AssertPrimaryDataNull(EmploymentRecord entity)
        {
            Assert.AreEqual(null, entity.ProfessionStartDate);
            Assert.AreEqual(null, entity.PrimaryEmploymentStartDate);
        }
    }

    internal class TestEmploymentMigrator : EmploymentMigrator
    {
        public TestEmploymentMigrator(IUladDataLayerMigrationAppDataProvider app, ILoanLqbCollectionContainer container)
            : base(E_BorrowerModeT.Borrower, app, container, Substitute.For<IEmploymentRecordDefaultsProvider>())
        {
        }

        public new EmploymentRecord CreateEntity(IEmploymentRecord legacy)
        {
            return base.CreateEntity(legacy);
        }
    }
}
