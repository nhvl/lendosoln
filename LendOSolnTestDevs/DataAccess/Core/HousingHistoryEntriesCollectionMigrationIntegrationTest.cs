﻿namespace DataAccess
{
    using System.Collections.Generic;
    using System.Linq;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    /// <summary>
    /// Tests for <see cref="HousingHistoryEntriesCollectionMigration"/>, via the use case in the loan migration.
    /// </summary>
    [TestFixture]
    public class HousingHistoryEntriesCollectionMigrationIntegrationTest
    {
        private LoanForIntegrationTest readOnlyLoanReference;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            try
            {
                this.readOnlyLoanReference = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false);

                Assert.That(E_sLqbCollectionT.IncomeSourceCollection < E_sLqbCollectionT.HousingHistoryEntriesCollection);
                CPageData loan = this.readOnlyLoanReference.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.EnsureMigratedToAtLeastUladCollectionVersion(E_sLqbCollectionT.IncomeSourceCollection);
                loan.Save();
                Assert.AreEqual(E_sLqbCollectionT.IncomeSourceCollection, loan.sBorrowerApplicationCollectionT);
            }
            catch
            {
                this.readOnlyLoanReference?.Dispose();
                this.readOnlyLoanReference = null;
                throw;
            }
        }

        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            this.readOnlyLoanReference?.Dispose();
        }

        [Test]
        public void MigrateLoan_BlankLoanFile_WorksAndProducesNoItems()
        {
            CPageData loan = this.readOnlyLoanReference.CreateNewPageDataWithBypass();
            loan.InitLoad();

            loan.MigrateToLatestUladCollectionVersion();

            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).True);
            CollectionAssert.IsEmpty(loan.HousingHistoryEntries);
        }

        [Test]
        public void MigrateLoan_InvalidYearsAtPresentAddress_Throws()
        {
            CPageData loan = this.readOnlyLoanReference.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBFirstNm = "Alice";
            app.aCFirstNm = "Bruce";
            app.aBAddrYrs = "2/3";

            CBaseException exception = Assert.Throws<CBaseException>(() => loan.MigrateToLatestUladCollectionVersion());
            StringAssert.Contains("Present Address", exception.UserMessage);
            StringAssert.Contains("Alice", exception.UserMessage);
            StringAssert.DoesNotContain("Bruce", exception.UserMessage);
            StringAssert.Contains("Years at Address", exception.UserMessage);
            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).False);

            app.aBAddrYrs = string.Empty;
            app.aCAddrYrs = "ponies";

            exception = Assert.Throws<CBaseException>(() => loan.MigrateToLatestUladCollectionVersion());
            StringAssert.Contains("Present Address", exception.UserMessage);
            StringAssert.Contains("Bruce", exception.UserMessage);
            StringAssert.DoesNotContain("Alice", exception.UserMessage);
            StringAssert.Contains("Years at Address", exception.UserMessage);
            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).False);
        }

        [Test]
        public void MigrateLoan_InvalidYearsAtFormerAddress1_Throws()
        {
            CPageData loan = this.readOnlyLoanReference.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBFirstNm = "Alice";
            app.aCFirstNm = "Bruce";
            app.aBPrev1AddrYrs = "2/3";

            CBaseException exception = Assert.Throws<CBaseException>(() => loan.MigrateToLatestUladCollectionVersion());
            StringAssert.Contains("Former Address", exception.UserMessage);
            StringAssert.Contains("Alice", exception.UserMessage);
            StringAssert.DoesNotContain("Bruce", exception.UserMessage);
            StringAssert.Contains("Years at Address", exception.UserMessage);
            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).False);

            app.aBPrev1AddrYrs = string.Empty;
            app.aCPrev1AddrYrs = "ponies";

            exception = Assert.Throws<CBaseException>(() => loan.MigrateToLatestUladCollectionVersion());
            StringAssert.Contains("Former Address", exception.UserMessage);
            StringAssert.Contains("Bruce", exception.UserMessage);
            StringAssert.DoesNotContain("Alice", exception.UserMessage);
            StringAssert.Contains("Years at Address", exception.UserMessage);
            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).False);
        }

        [Test]
        public void MigrateLoan_InvalidYearsAtFormerAddress2_Throws()
        {
            CPageData loan = this.readOnlyLoanReference.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBFirstNm = "Alice";
            app.aCFirstNm = "Bruce";
            app.aBPrev2AddrYrs = "2/3";

            CBaseException exception = Assert.Throws<CBaseException>(() => loan.MigrateToLatestUladCollectionVersion());
            StringAssert.Contains("Former Address", exception.UserMessage);
            StringAssert.Contains("Alice", exception.UserMessage);
            StringAssert.DoesNotContain("Bruce", exception.UserMessage);
            StringAssert.Contains("Years at Address", exception.UserMessage);
            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).False);

            app.aBPrev2AddrYrs = string.Empty;
            app.aCPrev2AddrYrs = "ponies";

            exception = Assert.Throws<CBaseException>(() => loan.MigrateToLatestUladCollectionVersion());
            StringAssert.Contains("Former Address", exception.UserMessage);
            StringAssert.Contains("Bruce", exception.UserMessage);
            StringAssert.DoesNotContain("Alice", exception.UserMessage);
            StringAssert.Contains("Years at Address", exception.UserMessage);
            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).False);
        }


        [Test]
        public void MigrateLoan_ReasonableLoanScenario_Works()
        {
            CPageData loan = this.readOnlyLoanReference.CreateNewPageDataWithBypass();
            loan.InitLoad();
            CAppData app = loan.GetAppData(0);
            app.aBFirstNm = "Alice";
            app.aCFirstNm = "Bruce";
            app.aBAddr = "1600 Sunflower Ave";
            app.aBCity = "Costa Mesa";
            app.aBState = "CA";
            app.aBZip = "92626";
            app.aBAddrT = E_aBAddrT.Own;
            app.aBAddrYrs = "2";
            app.aBPrev1Addr = "1506 SW Broadway Dr";
            app.aBPrev1City = "Portland";
            app.aBPrev1State = "OR";
            app.aBPrev1Zip = "97201";
            app.aBPrev1AddrT = E_aBPrev1AddrT.Rent;
            app.aBPrev1AddrYrs = "0.75";
            app.aBPrev2Addr = "72406 E Hwy 26";
            app.aBPrev2City = "Rhododendron";
            app.aBPrev2State = "OR";
            app.aBPrev2Zip = "97049";
            app.aBPrev2AddrT = E_aBPrev2AddrT.LivingRentFree;
            app.aBPrev2AddrYrs = "2.7";
            app.aCAddr = "1600 Sunflower Ave";
            app.aCCity = "Costa Mesa";
            app.aCState = "CA";
            app.aCZip = "92626";
            app.aCAddrT = E_aCAddrT.Own;
            app.aCAddrYrs = "2";
            app.aCPrev1Addr = "1506 SW Broadway Dr";
            app.aCPrev1City = "Portland";
            app.aCPrev1State = "OR";
            app.aCPrev1Zip = "97201";
            app.aCPrev1AddrT = E_aCPrev1AddrT.Rent;
            app.aCPrev1AddrYrs = "1.75";
            app.aCPrev2Addr = "501 Hillcrest Dr";
            app.aCPrev2City = "Longview";
            app.aCPrev2State = "WA";
            app.aCPrev2Zip = "98632";
            app.aCPrev2AddrT = E_aCPrev2AddrT.LeaveBlank;
            app.aCPrev2AddrYrs = "2.3";

            Assert.AreEqual(E_aAddrMailSourceT.PresentAddress, app.aBAddrMailSourceT);
            Assert.AreEqual(E_aAddrMailSourceT.PresentAddress, app.aCAddrMailSourceT);
            Assert.AreEqual(E_aAddrPostSourceT.MailingAddress, app.aBAddrPostSourceT);
            Assert.AreEqual(E_aAddrPostSourceT.MailingAddress, app.aCAddrPostSourceT);
            Assert.IsNull(app.aBMailingAddress);
            Assert.IsNull(app.aCMailingAddress);
            Assert.IsNull(app.aBPostClosingAddress);
            Assert.IsNull(app.aCPostClosingAddress);
            Assert.IsNull(app.aBMailingAddressData);
            Assert.IsNull(app.aCMailingAddressData);
            Assert.IsNull(app.aBPostClosingAddressData);
            Assert.IsNull(app.aCPostClosingAddressData);

            loan.MigrateToLatestUladCollectionVersion();

            Assert.That(loan, Has.Property(nameof(CPageData.sIsHousingHistoryEntriesCollectionEnabled)).True);
            Assert.AreEqual(6, loan.HousingHistoryEntries.Count);
            IEnumerable<HousingHistoryEntry> borrEntries = loan.HousingHistoryEntries.Values.Where(e => e.ConsumerId == app.aBConsumerIdentifier).ToList();
            IEnumerable<HousingHistoryEntry> coboEntries = loan.HousingHistoryEntries.Values.Where(e => e.ConsumerId == app.aCConsumerIdentifier).ToList();
            HousingHistoryEntry borrEntry1 = borrEntries.Single(e => e.Address.StreetAddress.ToString() == "1600 Sunflower Ave");
            HousingHistoryEntry coboEntry1 = coboEntries.Single(e => e.Address.StreetAddress.ToString() == "1600 Sunflower Ave");
            Assert.AreEqual(true, borrEntry1.IsPresentAddress);
            Assert.AreEqual("1600 Sunflower Ave", borrEntry1.Address?.StreetAddress?.ToString());
            Assert.AreEqual("Costa Mesa", borrEntry1.Address?.City?.ToString());
            Assert.AreEqual("CA", borrEntry1.Address?.State?.ToString());
            Assert.AreEqual("92626", borrEntry1.Address?.PostalCode?.ToString());
            Assert.AreEqual(CountryCodeIso3.UnitedStates, borrEntry1.Address?.CountryCode);
            Assert.AreEqual(ResidencyType.Own, borrEntry1.ResidencyType);
            Assert.AreEqual(24, borrEntry1.TimeAtAddress?.Value);
            Assert.AreEqual(true, coboEntry1.IsPresentAddress);
            Assert.AreEqual("1600 Sunflower Ave", coboEntry1.Address?.StreetAddress?.ToString());
            Assert.AreEqual("Costa Mesa", coboEntry1.Address?.City?.ToString());
            Assert.AreEqual("CA", coboEntry1.Address?.State?.ToString());
            Assert.AreEqual("92626", coboEntry1.Address?.PostalCode?.ToString());
            Assert.AreEqual(CountryCodeIso3.UnitedStates, coboEntry1.Address?.CountryCode);
            Assert.AreEqual(ResidencyType.Own, coboEntry1.ResidencyType);
            Assert.AreEqual(24, coboEntry1.TimeAtAddress?.Value);

            HousingHistoryEntry borrEntry2 = borrEntries.Single(e => e.Address.StreetAddress.ToString() == "1506 SW Broadway Dr");
            HousingHistoryEntry coboEntry2 = coboEntries.Single(e => e.Address.StreetAddress.ToString() == "1506 SW Broadway Dr");
            Assert.AreEqual(false, borrEntry2.IsPresentAddress);
            Assert.AreEqual("1506 SW Broadway Dr", borrEntry2.Address?.StreetAddress?.ToString());
            Assert.AreEqual("Portland", borrEntry2.Address?.City?.ToString());
            Assert.AreEqual("OR", borrEntry2.Address?.State?.ToString());
            Assert.AreEqual("97201", borrEntry2.Address?.PostalCode?.ToString());
            Assert.AreEqual(CountryCodeIso3.UnitedStates, borrEntry2.Address?.CountryCode);
            Assert.AreEqual(ResidencyType.Rent, borrEntry2.ResidencyType);
            Assert.AreEqual(9, borrEntry2.TimeAtAddress?.Value);
            Assert.AreEqual(false, coboEntry2.IsPresentAddress);
            Assert.AreEqual("1506 SW Broadway Dr", coboEntry2.Address?.StreetAddress?.ToString());
            Assert.AreEqual("Portland", coboEntry2.Address?.City?.ToString());
            Assert.AreEqual("OR", coboEntry2.Address?.State?.ToString());
            Assert.AreEqual("97201", coboEntry2.Address?.PostalCode?.ToString());
            Assert.AreEqual(CountryCodeIso3.UnitedStates, coboEntry2.Address?.CountryCode);
            Assert.AreEqual(ResidencyType.Rent, coboEntry2.ResidencyType);
            Assert.AreEqual(21, coboEntry2.TimeAtAddress?.Value);

            HousingHistoryEntry borrEntry3 = borrEntries.Single(e => e.Address.StreetAddress.ToString() == "72406 E Hwy 26");
            HousingHistoryEntry coboEntry3 = coboEntries.Single(e => e.Address.StreetAddress.ToString() == "501 Hillcrest Dr");
            Assert.AreEqual(false, borrEntry3.IsPresentAddress);
            Assert.AreEqual("72406 E Hwy 26", borrEntry3.Address?.StreetAddress?.ToString());
            Assert.AreEqual("Rhododendron", borrEntry3.Address?.City?.ToString());
            Assert.AreEqual("OR", borrEntry3.Address?.State?.ToString());
            Assert.AreEqual("97049", borrEntry3.Address?.PostalCode?.ToString());
            Assert.AreEqual(CountryCodeIso3.UnitedStates, borrEntry3.Address?.CountryCode);
            Assert.AreEqual(ResidencyType.LivingRentFree, borrEntry3.ResidencyType);
            Assert.AreEqual(32, borrEntry3.TimeAtAddress?.Value);
            Assert.AreEqual(false, coboEntry3.IsPresentAddress);
            Assert.AreEqual("501 Hillcrest Dr", coboEntry3.Address?.StreetAddress?.ToString());
            Assert.AreEqual("Longview", coboEntry3.Address?.City?.ToString());
            Assert.AreEqual("WA", coboEntry3.Address?.State?.ToString());
            Assert.AreEqual("98632", coboEntry3.Address?.PostalCode?.ToString());
            Assert.AreEqual(CountryCodeIso3.UnitedStates, coboEntry3.Address?.CountryCode);
            Assert.AreEqual(null, coboEntry3.ResidencyType);
            Assert.AreEqual(28, coboEntry3.TimeAtAddress?.Value);

            Assert.AreSame(borrEntry1.Address, app.aBMailingAddress);
            Assert.AreEqual(borrEntry1.Address?.StreetAddress, app.aBMailingAddressData.StreetAddress);
            Assert.AreEqual(borrEntry1.Address?.City, app.aBMailingAddressData.City);
            Assert.AreEqual(borrEntry1.Address?.State, app.aBMailingAddressData.State);
            Assert.AreEqual(borrEntry1.Address?.PostalCode, app.aBMailingAddressData.PostalCode);
            Assert.AreEqual(borrEntry1.Address?.CountryCode, app.aBMailingAddressData.CountryCode);

            Assert.AreSame(coboEntry1.Address, app.aCMailingAddress);
            Assert.AreEqual(coboEntry1.Address?.StreetAddress, app.aCMailingAddressData.StreetAddress);
            Assert.AreEqual(coboEntry1.Address?.City, app.aCMailingAddressData.City);
            Assert.AreEqual(coboEntry1.Address?.State, app.aCMailingAddressData.State);
            Assert.AreEqual(coboEntry1.Address?.PostalCode, app.aCMailingAddressData.PostalCode);
            Assert.AreEqual(coboEntry1.Address?.CountryCode, app.aCMailingAddressData.CountryCode);

            Assert.AreSame(borrEntry1.Address, app.aBPostClosingAddress);
            Assert.AreEqual(borrEntry1.Address?.StreetAddress, app.aBPostClosingAddressData.StreetAddress);
            Assert.AreEqual(borrEntry1.Address?.City, app.aBPostClosingAddressData.City);
            Assert.AreEqual(borrEntry1.Address?.State, app.aBPostClosingAddressData.State);
            Assert.AreEqual(borrEntry1.Address?.PostalCode, app.aBPostClosingAddressData.PostalCode);
            Assert.AreEqual(borrEntry1.Address?.CountryCode, app.aBPostClosingAddressData.CountryCode);

            Assert.AreSame(coboEntry1.Address, app.aCPostClosingAddress);
            Assert.AreEqual(coboEntry1.Address?.StreetAddress, app.aCPostClosingAddressData.StreetAddress);
            Assert.AreEqual(coboEntry1.Address?.City, app.aCPostClosingAddressData.City);
            Assert.AreEqual(coboEntry1.Address?.State, app.aCPostClosingAddressData.State);
            Assert.AreEqual(coboEntry1.Address?.PostalCode, app.aCPostClosingAddressData.PostalCode);
            Assert.AreEqual(coboEntry1.Address?.CountryCode, app.aCPostClosingAddressData.CountryCode);
        }
    }
}
