﻿namespace LendingQB.Test.Developers.DataAccess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using global::LendingQB.Test.Developers.FOOL;
    using global::LendingQB.Test.Developers.Utils;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LoanFileCreationCreateBlankUladFilesTest
    {
        private AbstractUserPrincipal principal;
        private CLoanFileCreator loanCreator;
        private Guid? loanId;

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.loanId = null;
            }
        }

        [TearDown]
        public void TearDown()
        {
            if (this.loanId.HasValue)
            {
                using (var helper = new FoolHelper())
                {
                    helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                    Tools.DeclareLoanFileInvalid(this.principal, this.loanId.Value, sendNotifications: false, generateLinkLoanMsgEvent: false);
                }
            }
        }

        [Test]
        public void CreateBlankUladLoanFile_Always_CreatesUladApplicationAndAssociation()
        {
            CPageData loan;
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = loanCreator.CreateBlankUladLoanFile();
                loan = CPageData.CreateUsingSmartDependency(this.loanId.Value, typeof(LoanFileCreationCreateBlankUladFilesTest));
                loan.InitLoad();
            }

            Assert.AreNotEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(1, loan.UladApplicationConsumers.Count);
            var expectedConsumerId = loan.GetAppData(0).aBConsumerId;
            Assert.AreEqual(expectedConsumerId, loan.UladApplicationConsumers.Values.Single().ConsumerId.Value);
        }

        [Test]
        public void CreateBlankUladLoanFile_DisabledByStageConfig_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                // Register fake stage config to disable it.
                var fakeValues = new Dictionary<string, Tuple<int, string>>()
                {
                    ["DisableBorrowerApplicationCollectionTUpdates"] = Tuple.Create(1, "")
                };
                var fakeStageConfig = new FakeStageConfigurationQuery(fakeValues);
                var fakeConfigFactory = new FakeConfigurationQueryFactory(Substitute.For<ISiteConfigurationQuery>(), fakeStageConfig);
                LqbApplication.Current.RegisterFactory<IConfigurationQueryFactory>(fakeConfigFactory);

                Assert.Throws<CBaseException>(() => loanCreator.CreateBlankUladLoanFile());
            }
        }

        [Test]
        public void CreateBlankUladLead_Always_CreatesUladApplicationAndAssociation()
        {
            CPageData loan;
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = loanCreator.CreateBlankUladLead();
                loan = CPageData.CreateUsingSmartDependency(this.loanId.Value, typeof(LoanFileCreationCreateBlankUladFilesTest));
                loan.InitLoad();
            }

            Assert.AreNotEqual(E_sLqbCollectionT.Legacy, loan.sBorrowerApplicationCollectionT);
            Assert.AreEqual(1, loan.UladApplications.Count);
            Assert.AreEqual(1, loan.UladApplicationConsumers.Count);
            var expectedConsumerId = loan.GetAppData(0).aBConsumerId;
            Assert.AreEqual(expectedConsumerId, loan.UladApplicationConsumers.Values.Single().ConsumerId.Value);
        }

        [Test]
        public void CreateBlankUladLead_DisabledByStageConfig_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                // Register fake stage config to disable it.
                var fakeValues = new Dictionary<string, Tuple<int, string>>()
                {
                    ["DisableBorrowerApplicationCollectionTUpdates"] = Tuple.Create(1, "")
                };
                var fakeStageConfig = new FakeStageConfigurationQuery(fakeValues);
                var fakeConfigFactory = new FakeConfigurationQueryFactory(Substitute.For<ISiteConfigurationQuery>(), fakeStageConfig);
                LqbApplication.Current.RegisterFactory<IConfigurationQueryFactory>(fakeConfigFactory);

                Assert.Throws<CBaseException>(() => loanCreator.CreateBlankUladLoanFile());
            }
        }
    }
}
