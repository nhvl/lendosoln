﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers;
    using LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LoanLqbCollectionDuplicatorIntegrationTest
    {
        private Lazy<AbstractUserPrincipal> LazyPrincipal { get; } = new Lazy<AbstractUserPrincipal>(() => LoginTools.LoginAs(TestAccountType.LoTest001));
        private AbstractUserPrincipal Principal => this.LazyPrincipal.Value;

        private Dictionary<E_sLqbCollectionT, Guid> sourceloanIdsByCollectionState = new Dictionary<E_sLqbCollectionT, Guid>();

        [TestFixtureSetUp]
        public void MakeLoans()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.MessageQueue);
                try
                {
                    foreach (E_sLqbCollectionT collectionState in Enum.GetValues(typeof(E_sLqbCollectionT)))
                    {
                        sourceloanIdsByCollectionState.Add(collectionState, CreateSourceLoanFile(collectionState));
                    }
                }
                catch
                {
                    foreach (Guid loanId in sourceloanIdsByCollectionState.Values)
                    {
                        Tools.SystemDeclareLoanFileInvalid(this.Principal, loanId, false, false);
                    }

                    throw;
                }
            }
        }

        [TestFixtureTearDown]
        public void ClearLoans()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.MessageQueue);
                foreach (Guid loanId in sourceloanIdsByCollectionState.Values)
                {
                    Tools.SystemDeclareLoanFileInvalid(this.Principal, loanId, false, false);
                }
            }
        }

        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.Legacy })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.Legacy })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.UseLqbCollections })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection })]
        public void DuplicateLqbCollectionData_SourceCollectionVersionGreaterThanOrEqualToTargetFile_TargetFileCollectionVersionMatchesSourceAfter(
            E_sLqbCollectionT sourceFileVersion,
            E_sLqbCollectionT targetFileVersion)
        {
            using (LoanForIntegrationTest tempLoan = CreateTempFile(targetFileVersion))
            {
                CPageData loan = tempLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                Assert.AreEqual(targetFileVersion, loan.sBorrowerApplicationCollectionT);
                CPageData sourceLoan = this.GetSourceLoan(sourceFileVersion);

                loan.DuplicateLqbCollectionDataFrom(sourceLoan);
                loan.Save();

                CPageData loanAfter = tempLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(sourceFileVersion, loanAfter.sBorrowerApplicationCollectionT);
                Assert.AreEqual(sourceFileVersion, sourceLoan.sBorrowerApplicationCollectionT);
            }
        }

        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.Legacy })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.Legacy })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.UseLqbCollections })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection })]
        public void DuplicateLqbCollectionEntityData_SourceCollectionVersionGreaterThanOrEqualToTargetFile_TargetFileCollectionVersionMatchesSourceAfter(
            E_sLqbCollectionT sourceFileVersion,
            E_sLqbCollectionT targetFileVersion)
        {
            using (LoanForIntegrationTest tempLoan = CreateTempFile(targetFileVersion))
            {
                CPageData loan = tempLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                Assert.AreEqual(targetFileVersion, loan.sBorrowerApplicationCollectionT);
                CPageData sourceLoan = this.GetSourceLoan(sourceFileVersion);

                loan.DuplicateLqbCollectionEntityDataFrom(sourceLoan);
                loan.Save();

                CPageData loanAfter = tempLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(sourceFileVersion, loanAfter.sBorrowerApplicationCollectionT);
                Assert.AreEqual(sourceFileVersion, sourceLoan.sBorrowerApplicationCollectionT);
            }
        }

        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.Legacy })]
        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.UseLqbCollections })]
        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.IncomeSourceCollection })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.IncomeSourceCollection })]
        public void DuplicateLqbCollectionData_SourceCollectionVersionLessThanTargetFile_Throws(E_sLqbCollectionT sourceFileVersion, E_sLqbCollectionT targetFileVersion)
        {
            using (LoanForIntegrationTest tempLoan = CreateTempFile(targetFileVersion))
            {
                CPageData loan = tempLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                Assert.AreEqual(targetFileVersion, loan.sBorrowerApplicationCollectionT);
                CPageData sourceLoan = this.GetSourceLoan(sourceFileVersion);

                Assert.Catch<CBaseException>(() => loan.DuplicateLqbCollectionDataFrom(sourceLoan));

                // Part 2: Entities only; doing this here to save on creating more loans.
                loan = tempLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                Assert.AreEqual(targetFileVersion, loan.sBorrowerApplicationCollectionT);
                sourceLoan = this.GetSourceLoan(sourceFileVersion);

                Assert.Catch<CBaseException>(() => loan.DuplicateLqbCollectionEntityDataFrom(sourceLoan));
            }
        }

        private CPageData GetSourceLoan(E_sLqbCollectionT collectionState)
        {
            CPageData sourceLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.sourceloanIdsByCollectionState[collectionState], this.GetType());
            sourceLoan.InitLoad();
            Assert.AreEqual(collectionState, sourceLoan.sBorrowerApplicationCollectionT);
            return sourceLoan;
        }

        private static LoanForIntegrationTest CreateTempFile(E_sLqbCollectionT loanCollectionState)
        {
            LoanForIntegrationTest tempLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false);
            try
            {
                if (loanCollectionState != E_sLqbCollectionT.Legacy)
                {
                    CPageData loan = tempLoan.CreateNewPageDataWithBypass();
                    loan.InitSave();
                    loan.EnsureMigratedToAtLeastUladCollectionVersion(loanCollectionState);
                    Assert.AreEqual(loanCollectionState, loan.sBorrowerApplicationCollectionT);
                    loan.Save();
                }

                return tempLoan;
            }
            catch
            {
                tempLoan?.Dispose();
                throw;
            }
        }

        private Guid CreateSourceLoanFile(E_sLqbCollectionT loanCollectionState)
        {
            var loanCreator = CLoanFileCreator.GetCreator(this.Principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            Guid loanId = loanCreator.CreateBlankLoanFile();
            try
            {
                CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, this.GetType());
                loan.InitSave();
                loan.EnsureMigratedToAtLeastUladCollectionVersion(loanCollectionState);
                Assert.AreEqual(loanCollectionState, loan.sBorrowerApplicationCollectionT);

                CAppData app = loan.GetAppData(0);
                var asset = app.aAssetCollection.AddRegularRecord();
                asset.AccNm = "AssetAccNm12345";
                var employment = app.aEmpCollection.AddRegularRecord();
                employment.EmplrNm = "Employer Nm";
                var liability = app.aLiaCollection.AddRegularRecord();
                liability.AccNm = "LiabilityAccNm54321";

                if (loan.sIsIncomeCollectionEnabled)
                {
                    loan.AddIncomeSource(
                        app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                        new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = 3000 });
                    loan.AddIncomeSource(
                        app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                        new IncomeSource { IncomeType = IncomeType.Overtime, MonthlyAmountData = 600 });
                }
                else
                {
                    app.aBBaseI = 3000;
                    app.aBOvertimeI = 600;
                }

                loan.Save();

                return loanId;
            }
            catch
            {
                Tools.SystemDeclareLoanFileInvalid(this.Principal, loanId, false, false);
                throw;
            }
        }
    }
}
