﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers.Utils;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendingQB.Test.Developers;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class Populate80To20Test
    {
        private AbstractUserPrincipal principal;
        private Guid secondLienId;

        [SetUp]
        public void SetUp()
        {
            this.secondLienId = Guid.Empty;
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.MessageQueue);

                if (this.secondLienId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(
                        this.principal,
                        this.secondLienId,
                        false,
                        false);
                }
            }
        }

        [Test]
        public void UpdateLinkedLoans_BothFilesOnLegacyIncomeFields_SetsIncomeFieldsToSubFile()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                this.principal = testFirstLien.Principal;
                var firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                Assert.AreEqual(false, firstLien.sIsIncomeCollectionEnabled);
                Assert.AreEqual(firstLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(firstLien.GetAppData(0).aCTotI, 0);
                var secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                Assert.AreEqual(secondLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(secondLien.GetAppData(0).aCTotI, 0);
                firstLien.InitSave();
                PopulateIncomeFields(firstLien.GetAppData(0));
                firstLien.Save();

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                populatorOfLiens.UpdateLinkedLoans();

                var secondLienAfter = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    secondLien.sLId,
                    typeof(Populate80To20Test));
                secondLienAfter.InitLoad();
                AssertLegacyFieldsCopiedOver(secondLienAfter.GetAppData(0));
            }
        }

        [Test]
        public void UpdateLinkedLoans_BothFilesOnIncomeSourceCollection_SetsIncomeSourcesToSubFile()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                this.principal = testFirstLien.Principal;
                CPageData firstLien = testFirstLien.CreateNewPageDataWithBypass();
                firstLien.InitSave();
                firstLien.AddCoborrowerToLegacyApplication(firstLien.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                firstLien.MigrateToUseLqbCollections();
                firstLien.MigrateToIncomeSourceCollection();
                firstLien.Save();
                Assert.AreEqual(true, firstLien.sIsIncomeCollectionEnabled);
                Assert.AreEqual(firstLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(firstLien.GetAppData(0).aCTotI, 0);
                firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                var secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                Assert.AreEqual(true, secondLien.sIsIncomeCollectionEnabled);
                Assert.AreEqual(secondLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(secondLien.GetAppData(0).aCTotI, 0);
                firstLien.InitSave();
                PopulateIncomeFields(firstLien.GetAppData(0));
                firstLien.Save();

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                populatorOfLiens.UpdateLinkedLoans();

                var secondLienAfter = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    secondLien.sLId,
                    typeof(Populate80To20Test));
                secondLienAfter.InitLoad();
                var secondLienAppAfter = secondLienAfter.GetAppData(0);
                var borrowerId = secondLienAppAfter.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coborrowerId = secondLienAppAfter.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();

                AssertLegacyFieldsCopiedOver(secondLienAppAfter);
                Assert.AreEqual(13, secondLienAfter.IncomeSources.Count);
                Assert.AreEqual(7, secondLienAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId == borrowerId));
                Assert.AreEqual(6, secondLienAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId == coborrowerId));
            }
        }

        [Test]
        public void UpdateLinkedLoans_ParentFileOnLegacyIncomeFieldsSubFileOnULAD_ThrowsAnException()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                this.principal = testFirstLien.Principal;
                CPageData firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                Assert.AreEqual(false, firstLien.sIsIncomeCollectionEnabled);
                Assert.AreEqual(firstLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(firstLien.GetAppData(0).aCTotI, 0);
                CPageData secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                secondLien = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.secondLienId, this.GetType());
                secondLien.InitSave();
                secondLien.MigrateToUseLqbCollections();
                secondLien.MigrateToIncomeSourceCollection();
                secondLien.AddCoborrowerToLegacyApplication(secondLien.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                secondLien.AddIncomeSource(secondLien.GetAppData(0).aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(), new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = 3000 });
                secondLien.AddIncomeSource(secondLien.GetAppData(0).aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(), new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = 2000 });
                Assert.AreEqual(secondLien.GetAppData(0).aBTotI, 3000);
                Assert.AreEqual(secondLien.GetAppData(0).aCTotI, 2000);
                Assert.AreEqual(true, secondLien.sIsIncomeCollectionEnabled);
                secondLien.Save();
                firstLien = testFirstLien.CreateNewPageDataWithBypass();
                firstLien.InitSave();
                PopulateIncomeFields(firstLien.GetAppData(0));
                firstLien.Save();
                var secondLienBefore = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.secondLienId, this.GetType());
                secondLienBefore.InitLoad();
                Assert.AreEqual(true, secondLienBefore.sIsIncomeCollectionEnabled);
                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, secondLienBefore.sBorrowerApplicationCollectionT);

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                Assert.Catch<CBaseException>(() => populatorOfLiens.UpdateLinkedLoans());
            }
        }

        [Test]
        public void UpdateLinkedLoans_ParentFileOnLegacyIncomeFieldsSubFileOnIncomeCollection_ThrowsAnException()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                this.principal = testFirstLien.Principal;
                CPageData firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                Assert.AreEqual(false, firstLien.sIsIncomeCollectionEnabled);
                Assert.AreEqual(firstLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(firstLien.GetAppData(0).aCTotI, 0);
                CPageData secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                secondLien = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.secondLienId, this.GetType());
                secondLien.InitSave();
                secondLien.MigrateToUseLqbCollections();
                secondLien.MigrateToIncomeSourceCollection();
                secondLien.AddCoborrowerToLegacyApplication(secondLien.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                secondLien.AddIncomeSource(secondLien.GetAppData(0).aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(), new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = 3000 });
                secondLien.AddIncomeSource(secondLien.GetAppData(0).aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(), new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = 2000 });
                Assert.AreEqual(secondLien.GetAppData(0).aBTotI, 3000);
                Assert.AreEqual(secondLien.GetAppData(0).aCTotI, 2000);
                Assert.AreEqual(true, secondLien.sIsIncomeCollectionEnabled);
                secondLien.Save();
                firstLien = testFirstLien.CreateNewPageDataWithBypass();
                firstLien.InitSave();
                PopulateIncomeFields(firstLien.GetAppData(0));
                firstLien.Save();
                var secondLienBefore = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.secondLienId, this.GetType());
                secondLienBefore.InitLoad();
                Assert.AreEqual(true, secondLienBefore.sIsIncomeCollectionEnabled);
                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, secondLienBefore.sBorrowerApplicationCollectionT);

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                Assert.Catch<CBaseException>(() => populatorOfLiens.UpdateLinkedLoans());
            }
        }

        [Test]
        public void UpdateLinkedLoans_ParentFileOnULADSubFileOnLegacyIncomeFields_SetsIncomeFieldsToSubFileAndSetsToULAD()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                this.principal = testFirstLien.Principal;
                CPageData firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                Assert.AreEqual(firstLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(firstLien.GetAppData(0).aCTotI, 0);
                var secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                secondLien.GetAppData(0).aBBaseI = 3000M;
                secondLien.GetAppData(0).aCBaseI = 2000M;
                secondLien.Save();
                firstLien = testFirstLien.CreateNewPageDataWithBypass();
                firstLien.InitSave();
                firstLien.MigrateToUseLqbCollections();
                firstLien.MigrateToIncomeSourceCollection();
                firstLien.AddCoborrowerToLegacyApplication(firstLien.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                PopulateIncomeFields(firstLien.GetAppData(0));
                firstLien.Save();
                var secondLienBefore = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.secondLienId, this.GetType());
                secondLienBefore.InitLoad();
                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, firstLien.sBorrowerApplicationCollectionT);
                Assert.AreEqual(true, firstLien.sIsIncomeCollectionEnabled);
                Assert.AreEqual(secondLienBefore.GetAppData(0).aBTotI, 3000);
                Assert.AreEqual(secondLienBefore.GetAppData(0).aCTotI, 2000);
                Assert.AreEqual(E_sLqbCollectionT.Legacy, secondLienBefore.sBorrowerApplicationCollectionT);
                Assert.AreEqual(false, secondLienBefore.sIsIncomeCollectionEnabled);

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                populatorOfLiens.UpdateLinkedLoans();

                var secondLienAfter = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    secondLien.sLId,
                    typeof(Populate80To20Test));
                secondLienAfter.InitLoad();
                AssertLegacyFieldsCopiedOver(secondLienAfter.GetAppData(0));
                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, secondLienAfter.sBorrowerApplicationCollectionT);
                Assert.AreEqual(true, secondLienAfter.sIsIncomeCollectionEnabled);
            }
        }

        [Test]
        public void UpdateLinkedLoans_ParentFileOnIncomeCollectionSubFileOnLegacyIncomeFields_SetsIncomeFieldsToSubFileAndSetsToIncomeCollection()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                this.principal = testFirstLien.Principal;
                CPageData firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                Assert.AreEqual(firstLien.GetAppData(0).aBTotI, 0);
                Assert.AreEqual(firstLien.GetAppData(0).aCTotI, 0);
                var secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                secondLien.GetAppData(0).aBBaseI = 3000M;
                secondLien.GetAppData(0).aCBaseI = 2000M;
                secondLien.Save();
                firstLien = testFirstLien.CreateNewPageDataWithBypass();
                firstLien.InitSave();
                firstLien.MigrateToUseLqbCollections();
                firstLien.MigrateToIncomeSourceCollection();
                firstLien.AddCoborrowerToLegacyApplication(firstLien.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                PopulateIncomeFields(firstLien.GetAppData(0));
                firstLien.Save();
                var secondLienBefore = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.secondLienId, this.GetType());
                secondLienBefore.InitLoad();
                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, firstLien.sBorrowerApplicationCollectionT);
                Assert.AreEqual(true, firstLien.sIsIncomeCollectionEnabled);
                Assert.AreEqual(secondLienBefore.GetAppData(0).aBTotI, 3000);
                Assert.AreEqual(secondLienBefore.GetAppData(0).aCTotI, 2000);
                Assert.AreEqual(E_sLqbCollectionT.Legacy, secondLienBefore.sBorrowerApplicationCollectionT);
                Assert.AreEqual(false, secondLienBefore.sIsIncomeCollectionEnabled);

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                populatorOfLiens.UpdateLinkedLoans();

                var secondLienAfter = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    secondLien.sLId,
                    typeof(Populate80To20Test));
                secondLienAfter.InitLoad();
                AssertLegacyFieldsCopiedOver(secondLienAfter.GetAppData(0));
                Assert.AreNotEqual(E_sLqbCollectionT.Legacy, secondLienAfter.sBorrowerApplicationCollectionT);
                Assert.AreEqual(true, secondLienAfter.sIsIncomeCollectionEnabled);
            }
        }

        [Test]
        public void UpdateLinkedLoans_LegacyFiles_CopiesHousingHistory()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestFileType.RegularLoan))
            {
                this.principal = testFirstLien.Principal;
                CPageData firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                var secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                secondLien.Save();

                firstLien = testFirstLien.CreateNewPageDataWithBypass();
                firstLien.InitSave();
                LeadConversionTest.PopulateLegacyHousingHistoryFields(firstLien.GetAppData(0));
                firstLien.Save();

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                populatorOfLiens.UpdateLinkedLoans();

                var secondLienAfter = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    secondLien.sLId,
                    typeof(Populate80To20Test));
                secondLienAfter.InitLoad();
                LeadConversionTest.AssertLegacyHousingHistoryFieldsCopiedOver(secondLienAfter.GetAppData(0));
            }
        }

        [Test]
        public void UpdateLinkedLoans_BothFilesUsingHousingHistoryCollection_CopiesHousingHistory()
        {
            using (var testFirstLien = LoanForIntegrationTest.Create(TestFileType.UladLoan))
            {
                this.principal = testFirstLien.Principal;
                CPageData firstLien = new CCreateSubfinancingLoanData(testFirstLien.Id);
                firstLien.InitLoad();
                var secondLien = firstLien.SpinOffLoanForSubsequent2ndLienLoanManually(testFirstLien.Principal);
                this.secondLienId = secondLien.sLId;
                secondLien.Save();

                firstLien = testFirstLien.CreateNewPageDataWithBypass();
                firstLien.InitSave();
                LeadConversionTest.PopulateLegacyHousingHistoryFields(firstLien.GetAppData(0));
                firstLien.Save();

                var populatorOfLiens = new CPopulate80To20(firstLien.BrokerDB, firstLien.sLId, secondLien.sLId);
                populatorOfLiens.UpdateLinkedLoans();

                var secondLienAfter = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    secondLien.sLId,
                    typeof(Populate80To20Test));
                secondLienAfter.InitLoad();
                Assert.AreEqual(6, secondLienAfter.HousingHistoryEntries.Count);
            }
        }

        private static void PopulateIncomeFields(CAppData app)
        {
            if (!app.LoanData.sIsIncomeCollectionEnabled)
            {
                app.aBBaseI = 1;
                app.aBBonusesI = 2;
                app.aBCommisionI = 3;
                app.aBDividendI = 4;
                app.aBOvertimeI = 5;
                app.aCBaseI = 6;
                app.aCBonusesI = 7;
                app.aCCommisionI = 8;
                app.aCDividendI = 9;
                app.aCOvertimeI = 10;
                app.aOtherIncomeList = new List<OtherIncome>()
                {
                    new OtherIncome
                    {
                        Desc = "Custom 1",
                        Amount = 1,
                        IsForCoBorrower = false
                    },
                    new OtherIncome
                    {
                        Desc = "Custom 2",
                        Amount = 2,
                        IsForCoBorrower = false
                    },
                    new OtherIncome
                    {
                        Desc = "Custom 3",
                        Amount = 3,
                        IsForCoBorrower = true
                    },
                };
            }
            else
            {
                app.LoanData.AddIncomeSource(
                    app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(1)
                    });
                app.LoanData.AddIncomeSource(
                    app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Bonuses,
                        MonthlyAmountData = Money.Create(2)
                    });
                app.LoanData.AddIncomeSource(
                    app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Commission,
                        MonthlyAmountData = Money.Create(3)
                    });
                app.LoanData.AddIncomeSource(
                    app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.DividendsOrInterest,
                        MonthlyAmountData = Money.Create(4)
                    });
                app.LoanData.AddIncomeSource(
                    app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Overtime,
                        MonthlyAmountData = Money.Create(5)
                    });
                app.LoanData.AddIncomeSource(
                    app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(6)
                    });
                app.LoanData.AddIncomeSource(
                    app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Bonuses,
                        MonthlyAmountData = Money.Create(7)
                    });
                app.LoanData.AddIncomeSource(
                    app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Commission,
                        MonthlyAmountData = Money.Create(8)
                    });
                app.LoanData.AddIncomeSource(
                    app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.DividendsOrInterest,
                        MonthlyAmountData = Money.Create(9)
                    });
                app.LoanData.AddIncomeSource(
                    app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Overtime,
                        MonthlyAmountData = Money.Create(10)
                    });
                app.LoanData.AddIncomeSource(
                    app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Other,
                        Description = DescriptionField.Create("Custom 1"),
                        MonthlyAmountData = Money.Create(1)
                    });
                app.LoanData.AddIncomeSource(
                    app.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Other,
                        Description = DescriptionField.Create("Custom 2"),
                        MonthlyAmountData = Money.Create(2)
                    });
                app.LoanData.AddIncomeSource(
                    app.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Other,
                        Description = DescriptionField.Create("Custom 3"),
                        MonthlyAmountData = Money.Create(3)
                    });
            }
        }

        private static void AssertLegacyFieldsCopiedOver(CAppData app)
        {
            Assert.AreEqual(1, app.aBBaseI);
            Assert.AreEqual(2, app.aBBonusesI);
            Assert.AreEqual(3, app.aBCommisionI);
            Assert.AreEqual(4, app.aBDividendI);
            Assert.AreEqual(5, app.aBOvertimeI);
            Assert.AreEqual(6, app.aCBaseI);
            Assert.AreEqual(7, app.aCBonusesI);
            Assert.AreEqual(8, app.aCCommisionI);
            Assert.AreEqual(9, app.aCDividendI);
            Assert.AreEqual(10, app.aCOvertimeI);
            Assert.AreEqual(1, app.aOtherIncomeList.Count(i => i.Desc == "Custom 1" && i.Amount == 1 && !i.IsForCoBorrower));
            Assert.AreEqual(1, app.aOtherIncomeList.Count(i => i.Desc == "Custom 2" && i.Amount == 2 && !i.IsForCoBorrower));
            Assert.AreEqual(1, app.aOtherIncomeList.Count(i => i.Desc == "Custom 3" && i.Amount == 3 && i.IsForCoBorrower));
        }
    }
}
