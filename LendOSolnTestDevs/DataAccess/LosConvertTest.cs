﻿namespace LendingQB.Test.Developers.DataAccess
{
    using System;
    using System.Collections;
    using global::DataAccess;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class LosConvertTest
    {
        [Test]
        public void DateTime_IsPartiallyRobust([ValueSource("FormatTargetEnumerable")] FormatTarget serializer, [ValueSource("FormatTargetEnumerable")] FormatTarget deserializer)
        {
            // Ideally a datetime stored somewhere (e.g., in XML or JSON) could be read in and parsed
            // independent of what version of LosConvert was used on both ends.
            // Unfortunately that is not the case, so this unit test can demonstrate exactly when the problems occur...

            if (IsProblematicSerializer(serializer) && CannotHandleProblematicSerializedFormat(deserializer)) return;

            var date = new DateTime(2000, 12, 25);
            var losOut = new LosConvert(serializer);
            var losRecover = new LosConvert(deserializer);

            string serialized = losOut.ToDateTimeVarCharString(date.ToShortDateString());
            Assert.IsFalse(string.IsNullOrEmpty(serialized));

            string recovered = losRecover.ToDateTimeVarCharString(serialized);
            if (serializer == deserializer)
            {
                Assert.AreEqual(serialized, recovered);
            }
            else
            {
                Assert.IsFalse(string.IsNullOrEmpty(recovered));
            }
        }

        private static bool IsProblematicSerializer(FormatTarget serializer)
        {
            // Each of the following serialize to yyyyMMdd format
            if (serializer == FormatTarget.FannieMaeMornetPlus) return true;
            if (serializer == FormatTarget.FreddieMacAUS) return true;
            if (serializer == FormatTarget.GinnieNet) return true;

            return false;
        }

        private static bool CannotHandleProblematicSerializedFormat(FormatTarget deserializer)
        {
            // Each of the following bails with string.Empty when the problematic serialized format cannot be parsed
            // NOTE: none of the formats can parse yyyyMMdd, but some formats return the input while these return string.Empty.

            if (deserializer == FormatTarget.FannieMaeMornetPlus) return true;
            if (deserializer == FormatTarget.FreddieMacAUS) return true;
            if (deserializer == FormatTarget.GinnieNet) return true;
            if (deserializer == FormatTarget.LON_Mismo) return true;
            if (deserializer == FormatTarget.MismoClosing) return true;
            if (deserializer == FormatTarget.XsltExport) return true;

            return false;
        }

        private IEnumerable FormatTargetEnumerable()
        {
            return RangeLimits.FormatTarget.Enumerable();
        }
    }
}
