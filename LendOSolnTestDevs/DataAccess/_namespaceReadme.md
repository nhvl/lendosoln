# LendOSolnTestDevs/DataAccess

This folder is for testing the DataAccess namespace in LendersOfficeLib.dll

If you try to prefix this namespace with LendingQB.Test, all other tests' using
statements for DataAccess inside of LendingQB.Test.Anything will suddenly refer
to LendingQB.Test.DataAccess instead of global::DataAccess as defined in
LendersOfficeLib.dll.  Thus, the intial test classes defined here are using the
actual DataAccess namespace they use.
