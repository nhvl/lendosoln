﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::LendingQB.Test.Developers.Utils;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendingQB.Test.Developers;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LeadConversionTest
    {
        private AbstractUserPrincipal principal;
        private Guid legacyIncomeTemplateId;
        private Guid incomeSourceCollectionTemplateId;
        private Guid useLqbCollectionsTemplateId;
        private Guid convertedLoanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.MessageQueue);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                this.legacyIncomeTemplateId = loanCreator.CreateBlankLoanTemplate();
                var legacyIncomeTemplate = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.legacyIncomeTemplateId,
                    typeof(LeadConversionTest));
                legacyIncomeTemplate.InitSave();
                legacyIncomeTemplate.AddCoborrowerToLegacyApplication(legacyIncomeTemplate.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                PopulateLegacyIncomeFields(legacyIncomeTemplate.GetAppData(0), isForTemplate: true);
                legacyIncomeTemplate.Save();
                Assert.AreEqual(false, legacyIncomeTemplate.sIsIncomeCollectionEnabled);
                Assert.AreEqual(E_sLqbCollectionT.Legacy, legacyIncomeTemplate.sBorrowerApplicationCollectionT);

                this.incomeSourceCollectionTemplateId = loanCreator.CreateBlankLoanTemplate();
                var incomeSourceCollectionTemplate = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.incomeSourceCollectionTemplateId,
                    typeof(LeadConversionTest));
                incomeSourceCollectionTemplate.InitSave();
                incomeSourceCollectionTemplate.AddCoborrowerToLegacyApplication(incomeSourceCollectionTemplate.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                PopulateLegacyIncomeFields(incomeSourceCollectionTemplate.GetAppData(0), isForTemplate: true);
                incomeSourceCollectionTemplate.MigrateToUseLqbCollections();
                incomeSourceCollectionTemplate.MigrateToIncomeSourceCollection();
                incomeSourceCollectionTemplate.Save();
                Assert.AreEqual(true, incomeSourceCollectionTemplate.sIsIncomeCollectionEnabled);
                Assert.AreEqual(E_sLqbCollectionT.IncomeSourceCollection, incomeSourceCollectionTemplate.sBorrowerApplicationCollectionT);

                this.useLqbCollectionsTemplateId = loanCreator.CreateBlankLoanTemplate();
                var uladTemplate = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.useLqbCollectionsTemplateId,
                    typeof(LeadConversionTest));
                uladTemplate.InitSave();
                uladTemplate.MigrateToUseLqbCollections();
                uladTemplate.Save();
                Assert.AreEqual(false, uladTemplate.sIsIncomeCollectionEnabled);
                Assert.AreEqual(E_sLqbCollectionT.UseLqbCollections, uladTemplate.sBorrowerApplicationCollectionT);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.MessageQueue);

                if (this.legacyIncomeTemplateId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(
                        this.principal,
                        this.legacyIncomeTemplateId,
                        false,
                        false);
                }

                if (this.incomeSourceCollectionTemplateId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(
                        this.principal,
                        this.incomeSourceCollectionTemplateId,
                        false,
                        false);
                }

                if (this.useLqbCollectionsTemplateId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(
                        this.principal,
                        this.useLqbCollectionsTemplateId,
                        false,
                        false);
                }
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.convertedLoanId = Guid.Empty;
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.MessageQueue);

                if (this.convertedLoanId != Guid.Empty)
                {
                    Tools.DeclareLoanFileInvalid(
                        this.principal,
                        this.convertedLoanId,
                        false,
                        false);
                }
            }
        }

        [Test]
        public void ConvertLqb_FromTemplateUsingLegacyIncomeFieldsLegacyIncome_SetsIncomeFieldsAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.sStatusLckd = true;
                loan.sStatusT = E_sStatusT.Lead_New;
                var app = loan.GetAppData(0);
                PopulateLegacyIncomeFields(app, isForTemplate: false);
                loan.Save();
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.legacyIncomeTemplateId,
                    LeadId = loan.sLId,
                    IsNonQm = false
                };

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Lqb(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                var convertedApp = convertedLoan.GetAppData(0);

                AssertLegacyIncomeFieldsCopiedOver(convertedApp);
            }
        }

        [Test]
        public void ConvertLqb_FromTemplateUsingIncomeSourceCollection_SetsIncomeSourcesAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.sStatusLckd = true;
                loan.sStatusT = E_sStatusT.Lead_New;
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                PopulateLegacyIncomeFields(loan.GetAppData(0), isForTemplate: false);
                loan.MigrateToUseLqbCollections();
                loan.MigrateToIncomeSourceCollection();
                loan.Save();
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.incomeSourceCollectionTemplateId,
                    LeadId = loan.sLId,
                    IsNonQm = false
                };

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Lqb(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                var convertedApp = convertedLoan.GetAppData(0);
                var borrowerId = convertedApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coborrowerId = convertedApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();

                AssertLegacyIncomeFieldsCopiedOver(convertedApp);
                Assert.AreEqual(13, convertedLoan.IncomeSources.Count);
                Assert.AreEqual(7, convertedLoan.ConsumerIncomeSources.Values.Count(a => a.ConsumerId == borrowerId));
                Assert.AreEqual(6, convertedLoan.ConsumerIncomeSources.Values.Count(a => a.ConsumerId == coborrowerId));
            }
        }

        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.Legacy, E_sLqbCollectionT.Legacy, })]
        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections, })]
        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.Legacy, E_sLqbCollectionT.UseLqbCollections, })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections, })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.Legacy, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, })]
        public void ConvertLqb_FromTemplate_UpconvertsResultingLoanToExpected(E_sLqbCollectionT lead, E_sLqbCollectionT template, E_sLqbCollectionT expected)
        {
            using (LoanForIntegrationTest testLoan = CreateTempLeadFile(lead))
            {
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.GetTemplateId(template),
                    LeadId = testLoan.Id,
                };

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Lqb(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                Assert.AreEqual(expected, convertedLoan.sBorrowerApplicationCollectionT);
            }
        }

        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.Legacy, E_sLqbCollectionT.Legacy, })]
        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections, })]
        [TestCase(new object[] { E_sLqbCollectionT.Legacy, E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.Legacy, E_sLqbCollectionT.UseLqbCollections, })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.UseLqbCollections, })]
        [TestCase(new object[] { E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.Legacy, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.UseLqbCollections, E_sLqbCollectionT.IncomeSourceCollection, })]
        [TestCase(new object[] { E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, E_sLqbCollectionT.IncomeSourceCollection, })]
        public void ConvertPml_FromTemplate_UpconvertsResultingLoanToExpected(E_sLqbCollectionT lead, E_sLqbCollectionT template, E_sLqbCollectionT expected)
        {
            using (LoanForIntegrationTest testLoan = CreateTempLeadFile(lead))
            {
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.GetTemplateId(template),
                    LeadId = testLoan.Id,
                };

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Pml(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                Assert.AreEqual(expected, convertedLoan.sBorrowerApplicationCollectionT);
            }
        }

        [Test]
        public void ConvertLqb_LegacyLeadFromTemplate_CopiesSourceHousingHistory()
        {
            using (LoanForIntegrationTest testLoan = CreateTempLeadFile(E_sLqbCollectionT.Legacy))
            {
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.GetTemplateId(E_sLqbCollectionT.Legacy),
                    LeadId = testLoan.Id,
                };

                var lead = testLoan.CreateNewPageDataWithBypass();
                lead.InitSave();
                PopulateLegacyHousingHistoryFields(lead.GetAppData(0));
                lead.Save();

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Lqb(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                AssertLegacyHousingHistoryFieldsCopiedOver(convertedLoan.GetAppData(0));
            }
        }

        [Test]
        public void ConvertPml_LegacyLeadFromTemplate_CopiesSourceHousingHistory()
        {
            using (LoanForIntegrationTest testLoan = CreateTempLeadFile(E_sLqbCollectionT.Legacy))
            {
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.GetTemplateId(E_sLqbCollectionT.Legacy),
                    LeadId = testLoan.Id,
                };

                var lead = testLoan.CreateNewPageDataWithBypass();
                lead.InitSave();
                PopulateLegacyHousingHistoryFields(lead.GetAppData(0));
                lead.Save();

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Pml(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                AssertLegacyHousingHistoryFieldsCopiedOver(convertedLoan.GetAppData(0));
            }
        }

        [Test]
        public void ConvertLqb_HousingHistoryCollectionLeadFromTemplate_CopiesSourceHousingHistory()
        {
            using (LoanForIntegrationTest testLoan = CreateTempLeadFile(E_sLqbCollectionT.HousingHistoryEntriesCollection))
            {
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.GetTemplateId(E_sLqbCollectionT.Legacy),
                    LeadId = testLoan.Id,
                };

                var lead = testLoan.CreateNewPageDataWithBypass();
                lead.InitSave();
                PopulateLegacyHousingHistoryFields(lead.GetAppData(0));
                lead.Save();

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Lqb(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                // Just make sure we created the correct number of records.
                Assert.AreEqual(6, convertedLoan.HousingHistoryEntries.Count);
            }
        }

        [Test]
        public void ConvertPml_HousingHistoryCollectionLeadFromTemplate_CopiesSourceHousingHistory()
        {
            using (LoanForIntegrationTest testLoan = CreateTempLeadFile(E_sLqbCollectionT.HousingHistoryEntriesCollection))
            {
                var parameters = new LeadConversionParameters()
                {
                    CreateFromTemplate = true,
                    TemplateId = this.GetTemplateId(E_sLqbCollectionT.Legacy),
                    LeadId = testLoan.Id,
                };

                var lead = testLoan.CreateNewPageDataWithBypass();
                lead.InitSave();
                PopulateLegacyHousingHistoryFields(lead.GetAppData(0));
                lead.Save();

                LeadConversionResults results = LeadConversion.ConvertLeadToLoan_Pml(testLoan.Principal, parameters);

                this.convertedLoanId = results.ConvertedLoanId;
                var convertedLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(convertedLoanId, typeof(LeadConversionTest));
                convertedLoan.InitLoad();
                // Just make sure we created the correct number of records.
                Assert.AreEqual(6, convertedLoan.HousingHistoryEntries.Count);
            }
        }
        private static LoanForIntegrationTest CreateTempLeadFile(E_sLqbCollectionT loanCollectionState)
        {
            LoanForIntegrationTest tempLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false);
            try
            {
                CPageData loan = tempLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.sStatusLckd = true;
                loan.sStatusT = E_sStatusT.Lead_New;
                loan.EnsureMigratedToAtLeastUladCollectionVersion(loanCollectionState);
                loan.Save();
                Assert.AreEqual(loanCollectionState, loan.sBorrowerApplicationCollectionT);
                return tempLoan;
            }
            catch
            {
                tempLoan?.Dispose();
                throw;
            }
        }

        private Guid GetTemplateId(E_sLqbCollectionT collectionVersion)
        {
            switch (collectionVersion)
            {
                case E_sLqbCollectionT.Legacy: return this.legacyIncomeTemplateId;
                case E_sLqbCollectionT.UseLqbCollections: return this.useLqbCollectionsTemplateId;
                case E_sLqbCollectionT.IncomeSourceCollection: return this.incomeSourceCollectionTemplateId;
                default: throw new UnhandledEnumException(collectionVersion);
            }
        }

        private static void PopulateLegacyIncomeFields(CAppData app, bool isForTemplate)
        {
            decimal multiplier = isForTemplate ? 100 : 1;

            app.aBBaseI = 1 * multiplier;
            app.aBBonusesI = 2 * multiplier;
            app.aBCommisionI = 3 * multiplier;
            app.aBDividendI = 4 * multiplier;
            app.aBOvertimeI = 5 * multiplier;
            app.aCBaseI = 6 * multiplier;
            app.aCBonusesI = 7 * multiplier;
            app.aCCommisionI = 8 * multiplier;
            app.aCDividendI = 9 * multiplier;
            app.aCOvertimeI = 10 * multiplier;
            app.aOtherIncomeList = new List<OtherIncome>()
            {
                new OtherIncome
                {
                    Desc = "Custom 1",
                    Amount = 1 * multiplier,
                    IsForCoBorrower = false
                },
                new OtherIncome
                {
                    Desc = "Custom 2",
                    Amount = 2 * multiplier,
                    IsForCoBorrower = false
                },
                new OtherIncome
                {
                    Desc = "Custom 3",
                    Amount = 3 * multiplier,
                    IsForCoBorrower = true
                },
            };
        }

        private static void AssertLegacyIncomeFieldsCopiedOver(CAppData app)
        {
            Assert.AreEqual(1, app.aBBaseI);
            Assert.AreEqual(2, app.aBBonusesI);
            Assert.AreEqual(3, app.aBCommisionI);
            Assert.AreEqual(4, app.aBDividendI);
            Assert.AreEqual(5, app.aBOvertimeI);
            Assert.AreEqual(6, app.aCBaseI);
            Assert.AreEqual(7, app.aCBonusesI);
            Assert.AreEqual(8, app.aCCommisionI);
            Assert.AreEqual(9, app.aCDividendI);
            Assert.AreEqual(10, app.aCOvertimeI);
            Assert.AreEqual(1, app.aOtherIncomeList.Count(i => i.Desc == "Custom 1" && i.Amount == 1 && !i.IsForCoBorrower));
            Assert.AreEqual(1, app.aOtherIncomeList.Count(i => i.Desc == "Custom 2" && i.Amount == 2 && !i.IsForCoBorrower));
            Assert.AreEqual(1, app.aOtherIncomeList.Count(i => i.Desc == "Custom 3" && i.Amount == 3 && i.IsForCoBorrower));
        }

        private const string aBAddr = "aBAddr";
        private const string aBCity = "aBCity";
        private const string aBState = "CA";
        private const string aBZip = "00001";
        private const E_aBAddrT aBAddrT = E_aBAddrT.Rent;
        private const string aBAddrYrs = "1";
        private const string aBPrev1Addr = "aBPrev1Addr";
        private const string aBPrev1City = "aBPrev1City";
        private const string aBPrev1State = "AZ";
        private const string aBPrev1Zip = "00002";
        private const E_aBPrev1AddrT aBPrev1AddrT = E_aBPrev1AddrT.LeaveBlank;
        private const string aBPrev1AddrYrs = "2";
        private const string aBPrev2Addr = "aBPrev2Addr";
        private const string aBPrev2City = "aBPrev2City";
        private const string aBPrev2State = "NV";
        private const string aBPrev2Zip = "00003";
        private const E_aBPrev2AddrT aBPrev2AddrT = E_aBPrev2AddrT.LivingRentFree;
        private const string aBPrev2AddrYrs = "3";

        private const string aCAddr = "aCAddr";
        private const string aCCity = "aCCity";
        private const string aCState = "OR";
        private const string aCZip = "00004";
        private const E_aCAddrT aCAddrT = E_aCAddrT.LivingRentFree;
        private const string aCAddrYrs = "4";
        private const string aCPrev1Addr = "aCPrev1Addr";
        private const string aCPrev1City = "aCPrev1City";
        private const string aCPrev1State = "WA";
        private const string aCPrev1Zip = "00005";
        private const E_aCPrev1AddrT aCPrev1AddrT = E_aCPrev1AddrT.Rent;
        private const string aCPrev1AddrYrs = "2";
        private const string aCPrev2Addr = "aCPrev2Addr";
        private const string aCPrev2City = "aCPrev2City";
        private const string aCPrev2State = "NM";
        private const string aCPrev2Zip = "00006";
        private const E_aCPrev2AddrT aCPrev2AddrT = E_aCPrev2AddrT.LeaveBlank;
        private const string aCPrev2AddrYrs = "3";

        private const E_aAddrMailSourceT aBAddrMailSourceT = E_aAddrMailSourceT.Other;
        private const string aBAddrMail = "aBAddrMail";
        private const string aBCityMail = "aBCityMail";
        private const string aBStateMail = "AL";
        private const string aBZipMail = "00007";
        private const E_aAddrMailSourceT aCAddrMailSourceT = E_aAddrMailSourceT.Other;
        private const string aCAddrMail = "aCAddrMail";
        private const string aCCityMail = "aCCityMail";
        private const string aCStateMail = "AK";
        private const string aCZipMail = "00008";

        private const bool aBAddrPostSourceTLckd = true;
        private const E_aAddrPostSourceT aBAddrPostSourceT = E_aAddrPostSourceT.Other;
        private const string aBAddrPost = "aBAddrPost";
        private const string aBCityPost = "aBCityPost";
        private const string aBStatePost = "AR";
        private const string aBZipPost = "00009";
        private const bool aCAddrPostSourceTLckd = true;
        private const E_aAddrPostSourceT aCAddrPostSourceT = E_aAddrPostSourceT.Other;
        private const string aCAddrPost = "aCAddrPost";
        private const string aCCityPost = "aCCityPost";
        private const string aCStatePost = "CO";
        private const string aCZipPost = "00010";

        public static void PopulateLegacyHousingHistoryFields(CAppData app)
        {
            app.aBAddr = aBAddr;
            app.aBCity = aBCity;
            app.aBState = aBState;
            app.aBZip = aBZip;
            app.aBAddrT = aBAddrT;
            app.aBAddrYrs = aBAddrYrs;
            app.aBPrev1Addr = aBPrev1Addr;
            app.aBPrev1City = aBPrev1City;
            app.aBPrev1State = aBPrev1State;
            app.aBPrev1Zip = aBPrev1Zip;
            app.aBPrev1AddrT = aBPrev1AddrT;
            app.aBPrev1AddrYrs = aBPrev1AddrYrs;
            app.aBPrev2Addr = aBPrev2Addr;
            app.aBPrev2City = aBPrev2City;
            app.aBPrev2State = aBPrev2State;
            app.aBPrev2Zip = aBPrev2Zip;
            app.aBPrev2AddrT = aBPrev2AddrT;
            app.aBPrev2AddrYrs = aBPrev2AddrYrs;

            app.aCAddr = aCAddr;
            app.aCCity = aCCity;
            app.aCState = aCState;
            app.aCZip = aCZip;
            app.aCAddrT = aCAddrT;
            app.aCAddrYrs = aCAddrYrs;
            app.aCPrev1Addr = aCPrev1Addr;
            app.aCPrev1City = aCPrev1City;
            app.aCPrev1State = aCPrev1State;
            app.aCPrev1Zip = aCPrev1Zip;
            app.aCPrev1AddrT = aCPrev1AddrT;
            app.aCPrev1AddrYrs = aCPrev1AddrYrs;
            app.aCPrev2Addr = aCPrev2Addr;
            app.aCPrev2City = aCPrev2City;
            app.aCPrev2State = aCPrev2State;
            app.aCPrev2Zip = aCPrev2Zip;
            app.aCPrev2AddrT = aCPrev2AddrT;
            app.aCPrev2AddrYrs = aCPrev2AddrYrs;

            app.aBAddrMailSourceT = aBAddrMailSourceT;
            app.aBAddrMail = aBAddrMail;
            app.aBCityMail = aBCityMail;
            app.aBStateMail = aBStateMail;
            app.aBZipMail = aBZipMail;
            app.aCAddrMailSourceT = aCAddrMailSourceT;
            app.aCAddrMail = aCAddrMail;
            app.aCCityMail = aCCityMail;
            app.aCStateMail = aCStateMail;
            app.aCZipMail = aCZipMail;

            app.aBAddrPostSourceTLckd = aBAddrPostSourceTLckd;
            app.aBAddrPostSourceT = aBAddrPostSourceT;
            app.aBAddrPost = aBAddrPost;
            app.aBCityPost = aBCityPost;
            app.aBStatePost = aBStatePost;
            app.aBZipPost = aBZipPost;
            app.aCAddrPostSourceTLckd = aCAddrPostSourceTLckd;
            app.aCAddrPostSourceT = aCAddrPostSourceT;
            app.aCAddrPost = aCAddrPost;
            app.aCCityPost = aCCityPost;
            app.aCStatePost = aCStatePost;
            app.aCZipPost = aCZipPost;
        }

        public static void AssertLegacyHousingHistoryFieldsCopiedOver(CAppData app)
        {
            Assert.AreEqual(aBAddr, app.aBAddr);
            Assert.AreEqual(aBCity, app.aBCity);
            Assert.AreEqual(aBState, app.aBState);
            Assert.AreEqual(aBZip, app.aBZip);
            Assert.AreEqual(aBAddrT, app.aBAddrT);
            Assert.AreEqual(aBAddrYrs, app.aBAddrYrs);
            Assert.AreEqual(aBPrev1Addr, app.aBPrev1Addr);
            Assert.AreEqual(aBPrev1City, app.aBPrev1City);
            Assert.AreEqual(aBPrev1State, app.aBPrev1State);
            Assert.AreEqual(aBPrev1Zip, app.aBPrev1Zip);
            Assert.AreEqual(aBPrev1AddrT, app.aBPrev1AddrT);
            Assert.AreEqual(aBPrev1AddrYrs, app.aBPrev1AddrYrs);
            Assert.AreEqual(aBPrev2Addr, app.aBPrev2Addr);
            Assert.AreEqual(aBPrev2City, app.aBPrev2City);
            Assert.AreEqual(aBPrev2State, app.aBPrev2State);
            Assert.AreEqual(aBPrev2Zip, app.aBPrev2Zip);
            Assert.AreEqual(aBPrev2AddrT, app.aBPrev2AddrT);
            Assert.AreEqual(aBPrev2AddrYrs, app.aBPrev2AddrYrs);

            Assert.AreEqual(aCAddr, app.aCAddr);
            Assert.AreEqual(aCCity, app.aCCity);
            Assert.AreEqual(aCState, app.aCState);
            Assert.AreEqual(aCZip, app.aCZip);
            Assert.AreEqual(aCAddrT, app.aCAddrT);
            Assert.AreEqual(aCAddrYrs, app.aCAddrYrs);
            Assert.AreEqual(aCPrev1Addr, app.aCPrev1Addr);
            Assert.AreEqual(aCPrev1City, app.aCPrev1City);
            Assert.AreEqual(aCPrev1State, app.aCPrev1State);
            Assert.AreEqual(aCPrev1Zip, app.aCPrev1Zip);
            Assert.AreEqual(aCPrev1AddrT, app.aCPrev1AddrT);
            Assert.AreEqual(aCPrev1AddrYrs, app.aCPrev1AddrYrs);
            Assert.AreEqual(aCPrev2Addr, app.aCPrev2Addr);
            Assert.AreEqual(aCPrev2City, app.aCPrev2City);
            Assert.AreEqual(aCPrev2State, app.aCPrev2State);
            Assert.AreEqual(aCPrev2Zip, app.aCPrev2Zip);
            Assert.AreEqual(aCPrev2AddrT, app.aCPrev2AddrT);
            Assert.AreEqual(aCPrev2AddrYrs, app.aCPrev2AddrYrs);

            Assert.AreEqual(aBAddrMailSourceT, app.aBAddrMailSourceT);
            Assert.AreEqual(aBAddrMail, app.aBAddrMail);
            Assert.AreEqual(aBCityMail, app.aBCityMail);
            Assert.AreEqual(aBStateMail, app.aBStateMail);
            Assert.AreEqual(aBZipMail, app.aBZipMail);
            Assert.AreEqual(aCAddrMailSourceT, app.aCAddrMailSourceT);
            Assert.AreEqual(aCAddrMail, app.aCAddrMail);
            Assert.AreEqual(aCCityMail, app.aCCityMail);
            Assert.AreEqual(aCStateMail, app.aCStateMail);
            Assert.AreEqual(aCZipMail, app.aCZipMail);

            Assert.AreEqual(aBAddrPostSourceTLckd, app.aBAddrPostSourceTLckd);
            Assert.AreEqual(aBAddrPostSourceT, app.aBAddrPostSourceT);
            Assert.AreEqual(aBAddrPost, app.aBAddrPost);
            Assert.AreEqual(aBCityPost, app.aBCityPost);
            Assert.AreEqual(aBStatePost, app.aBStatePost);
            Assert.AreEqual(aBZipPost, app.aBZipPost);
            Assert.AreEqual(aCAddrPostSourceTLckd, app.aCAddrPostSourceTLckd);
            Assert.AreEqual(aCAddrPostSourceT, app.aCAddrPostSourceT);
            Assert.AreEqual(aCAddrPost, app.aCAddrPost);
            Assert.AreEqual(aCCityPost, app.aCCityPost);
            Assert.AreEqual(aCStatePost, app.aCStatePost);
            Assert.AreEqual(aCZipPost, app.aCZipPost);
        }
    }
}
