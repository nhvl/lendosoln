﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers;
    using LendingQB.Test.Developers.Utils;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class EntityDependencyTest
    {
        [TestCase(typeof(TestIConsumer), nameof(TestIConsumerSmartDependencyFields))]
        [TestCase(typeof(TestILegacyApplication), nameof(TestILegacyApplicationSmartDependencyFields))]
        [TestCase(typeof(TestExplicitConsumerData), nameof(TestExplicitConsumerDataSmartDependencyFields))]
        [TestCase(typeof(TestExplicitLegacyApplicationData), nameof(TestExplicitLegacyApplicationDataSmartDependencyFields))]
        [TestCase(typeof(TestMixedFields), nameof(TestMixedFieldsSmartDependencyFields))]
        [TestCase(typeof(TestStringFormatterFields), nameof(TestStringFormatterFieldsSmartDependencyFields))]
        public void GetDependencyListForSmartDependency_ReturnsUsedFields(Type t, string expectedFieldName)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                string[] expected = this.GetFieldValue<string[]>(expectedFieldName);

                IReadOnlyCollection<string> actual = CPageData.GetCPageBaseAndCAppDataDependencyList(t);

                CollectionAssert.AreEquivalent(expected, actual);
            }
        }

        [TestCase(typeof(IConsumer), nameof(IConsumer.Name), new[] { nameof(CAppBase.aBNm), nameof(CAppBase.aCNm) })]
        [TestCase(typeof(IConsumer), nameof(IConsumer.FirstName), new[] { nameof(CAppBase.aBFirstNm), nameof(CAppBase.aCFirstNm), })]
        [TestCase(typeof(IConsumer), nameof(IConsumer.IsChinese), new[] { nameof(CAppBase.aBIsChinese), nameof(CAppBase.aCIsChinese), })]
        [TestCase(typeof(IConsumer), nameof(IConsumer.IsJapanese), new[] { nameof(CAppBase.aBIsJapanese), nameof(CAppBase.aCIsJapanese), })]
        [TestCase(typeof(ILegacyApplication), nameof(ILegacyApplication.CoborrIsDefined), new[] { nameof(CAppBase.aCIsDefined), })]
        [TestCase(typeof(ILegacyApplication), nameof(ILegacyApplication.InterviewerMethodType), new[] { nameof(CAppBase.aIntrvwrMethodT), })]
        [TestCase(typeof(ILegacyApplication), nameof(ILegacyApplication.FhaCreditRating), new[] { nameof(CAppBase.aFHACreditRating), })]
        public void ExpandDependencyList_ExpandsToExpectedFields(Type typeUnderTest, string inputField, string[] expectedEquivalentExpansion)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                // 2019-01 tj - It would be nice to have the expected fields list hardcoded in our test arguments.  However, this would mean the test would begin failing when
                // dependencies change, including pretty far downstream.  As such, I'm choosing to simply reference the next fields in the dependency chain.
                IEnumerable<string> expected = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, expectedEquivalentExpansion).FieldSet;

                IEnumerable<string> actual = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, new[] { inputField }).FieldSet;

                CollectionAssert.AreEquivalent(expected, actual);
            }
        }

        [Test]
        public void LoadData_FirstName_CanReadConsumersCollection()
        {
            using (var loanForTest = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                CPageData loan = CPageData.CreateUsingSmartDependency(loanForTest.Id, typeof(TestConsumerOnlyFirstNameField));
                loan.InitLoad();

                var c = new TestConsumerOnlyFirstNameField();
                Assert.DoesNotThrow(() => c.Foo(loan));
            }
        }

        private static void DoNothing(object input)
        {
        }

        private static readonly string[] TestIConsumerSmartDependencyFields = new[]
        {
            nameof(IConsumer.Name),
            nameof(IConsumer.FirstName),
            nameof(IConsumer.IsChinese),
            nameof(IConsumer.IsJapanese),
            nameof(IConsumer.IsOtherAsian),
        };

        private class TestIConsumer
        {
            public void Foo(IConsumer consumer)
            {
                DoNothing(consumer.Name);
                DoNothing(consumer.FirstName);
                DoNothing(consumer.IsChinese);
                DoNothing(consumer.IsJapanese);
                DoNothing(consumer.IsOtherAsian);
            }
        }

        private static readonly string[] TestILegacyApplicationSmartDependencyFields = new[]
        {
            nameof(ILegacyApplication.FhaCreditRating),
            nameof(ILegacyApplication.VaApplyOneTimeRestorationCityState),
        };

        private class TestILegacyApplication
        {
            public void Foo(ILegacyApplication legacyApplication)
            {
                DoNothing(legacyApplication.FhaCreditRating);
                DoNothing(legacyApplication.VaApplyOneTimeRestorationCityState);
            }
        }

        private static readonly string[] TestExplicitConsumerDataSmartDependencyFields = new[]
        {
            nameof(AppDataConsumer.EquifaxCreatedDate),
            nameof(AppDataConsumer.MiddleName),
            nameof(AppDataConsumer.PreferredName),
        };

        private class TestExplicitConsumerData
        {
            public void Foo(AppDataConsumer consumer)
            {
                DoNothing(consumer.EquifaxCreatedDate);
                DoNothing(consumer.MiddleName);
                DoNothing(consumer.PreferredName);
            }
        }

        private static readonly string[] TestExplicitLegacyApplicationDataSmartDependencyFields = new[]
        {
            nameof(AppDataLegacyApplication.IsPrimary),
            nameof(AppDataLegacyApplication.OccType),
            nameof(AppDataLegacyApplication.HasSpouse),
        };

        private class TestExplicitLegacyApplicationData
        {
            public void Foo(AppDataLegacyApplication legacyApplication)
            {
                DoNothing(legacyApplication.IsPrimary);
                DoNothing(legacyApplication.OccType);
                DoNothing(legacyApplication.HasSpouse);
            }
        }

        private static readonly object[] TestMixedFieldsSmartDependencyFields = new string[]
        {
            nameof(CAppBase.aCNoFurnish),
            nameof(CPageBase.LegacyApplications), // bah
            nameof(ILegacyApplication.AltName1),
            nameof(CPageBase.Consumers),
            nameof(IConsumer.EquifaxFactors),
            nameof(CPageBase.sTransNetCash),
            nameof(CPageBase.DuplicateLqbCollectionDataFrom),
            nameof(CAppBase.afDelMarriedCobor),

            // These are the black sheep: fields/Methods that don't have DependsOn, but smart dependency will still return
            // them.  The dependency graph understands to drop them during select statement generation.
            nameof(CPageBase.DataState),
            nameof(CAppBase.BorrowerModeT),
        };

        private class TestMixedFields
        {
            public void Foo(CPageData loan)
            {
                loan.InitLoad();
                if (loan.GetAppData(0).aCNoFurnish)
                {
                    DoNothing(loan.LegacyApplications.Values.First().AltName1);
                }
                else
                {
                    foreach (var consumerWithId in loan.Consumers)
                    {
                        DoNothing(consumerWithId.Value.EquifaxFactors);
                    }
                }

                DoNothing(loan.sTransNetCash);
                loan.DuplicateLqbCollectionDataFrom(null);
                loan.GetAppData(2).DelMarriedCobor();
                DoNothing(loan.DataState);
                DoNothing(loan.GetAppData(23).BorrowerModeT);
                loan.Save();
            }
        }

        private readonly object[] TestStringFormatterFieldsSmartDependencyFields = new string[]
        {
            nameof(IConsumerStringFormatter.IdOther),
            nameof(ILegacyApplicationStringFormatter.PresentHoaDues),
        };

        private class TestStringFormatterFields
        {
            public void Foo(IConsumerStringFormatter consumer) => DoNothing(consumer.IdOther);
            public void Bar(ILegacyApplicationStringFormatter legacyApplication) => DoNothing(legacyApplication.PresentHoaDues);
        }

        private class TestConsumerOnlyFirstNameField
        {
            public void Foo(CPageData loan)
            {
                DoNothing(string.Join(", ", loan.Consumers.Values.Select(c => c.FirstName)));
            }
        }
    }
}
