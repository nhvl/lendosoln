﻿using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Common;
using LendingQB.Core;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;

namespace LendingQB.Test.Developers.Fakes
{
    using LegacyApplicationConsumerIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>;

    /// <summary>
    /// A fake for creating values for <see cref="ILoanLqbCollectionContainer.LegacyApplicationConsumers"/>.
    /// </summary>
    internal class FakeLegacyApplicationConsumers :
        ReadOnlyLqbCollection<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation>,
        IReadOnlyLqbAssociationSet<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation>
    {
        public FakeLegacyApplicationConsumers(Dictionary<LegacyApplicationConsumerIdentifier, ILegacyApplicationConsumerAssociation> data)
            : base(
                Name.Create("LegacyApplicationConsumers").ForceValue(),
                new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                data)
        {
            this.Data = data;
        }

        public Dictionary<LegacyApplicationConsumerIdentifier, ILegacyApplicationConsumerAssociation> Data { get; }

        public void AddBorrower(DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId, DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId, bool isPrimary)
        {
            var associationFactory = new LegacyApplicationConsumerAssociationFactory();
            this.Data.Add(
                consumerId.Value.ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                associationFactory.Create(appId, consumerId, isPrimary));
        }

        public static FakeLegacyApplicationConsumers FromLegacyApplicationData(IEnumerable<IShimAppDataProvider> applications)
        {
            var associationFactory = new LegacyApplicationConsumerAssociationFactory();
            var data = new Dictionary<LegacyApplicationConsumerIdentifier, ILegacyApplicationConsumerAssociation>();
            foreach (var application in applications)
            {
                var appId = application.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>();
                var borrowerId = application.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var borrowerAssociationId = application.aBConsumerId.ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();

                var borrowerAssociation = associationFactory.Create(appId, borrowerId, isPrimary: true);
                data.Add(borrowerAssociationId, borrowerAssociation);

                var coborrowerId = application.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coborrowerAssociationId = application.aCConsumerId.ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();

                var coborrowerAssociation = associationFactory.Create(appId, coborrowerId, isPrimary: false);
                data.Add(coborrowerAssociationId, coborrowerAssociation);
            }

            return new FakeLegacyApplicationConsumers(data);
        }

        public static FakeLegacyApplicationConsumers FromLegacyApplicationData(IEnumerable<IUladDataLayerMigrationAppDataProvider> applications)
        {
            var associationFactory = new LegacyApplicationConsumerAssociationFactory();
            var data = new Dictionary<LegacyApplicationConsumerIdentifier, ILegacyApplicationConsumerAssociation>();
            foreach (var application in applications)
            {
                var appId = application.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>();
                var borrowerId = application.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var borrowerAssociationId = application.aBConsumerId.ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();

                var borrowerAssociation = associationFactory.Create(appId, borrowerId, isPrimary: true);
                data.Add(borrowerAssociationId, borrowerAssociation);
                if (application.aHasCoborrower)
                {
                    var coborrowerId = application.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                    var coborrowerAssociationId = application.aCConsumerId.ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();

                    var coborrowerAssociation = associationFactory.Create(appId, coborrowerId, isPrimary: false);
                    data.Add(coborrowerAssociationId, coborrowerAssociation);
                }
            }

            return new FakeLegacyApplicationConsumers(data);
        }

        public static FakeLegacyApplicationConsumers ForSingleLegacyApplication(
            Guid appId,
            Guid borrowerId,
            Guid? coborrowerId)
        {
            return ForSingleLegacyApplication(
                appId.ToIdentifier<DataObjectKind.LegacyApplication>(),
                borrowerId.ToIdentifier<DataObjectKind.Consumer>(),
                coborrowerId?.ToIdentifier<DataObjectKind.Consumer>());
        }

        public static FakeLegacyApplicationConsumers ForSingleLegacyApplication(
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId)
        {
            var associationFactory = new LegacyApplicationConsumerAssociationFactory();
            var data = new Dictionary<LegacyApplicationConsumerIdentifier, ILegacyApplicationConsumerAssociation>();
            data.Add(
                borrowerId.Value.ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                associationFactory.Create(appId, borrowerId, isPrimary: true));
            if (coborrowerId.HasValue)
            {
                data.Add(
                    coborrowerId.Value.Value.ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                    associationFactory.Create(appId, coborrowerId.Value, isPrimary: true));
            }

            return new FakeLegacyApplicationConsumers(data);
        }
    }
}
