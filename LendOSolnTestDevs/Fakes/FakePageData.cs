﻿/// <copyright file="FakePageData.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// Copied: 4/12/2018 from LendOSolnTest to LendOSolnTestDevs
/// </summary>
namespace LendingQB.Test.Developers.Fakes
{
    using System;
    using System.Data;
    using LendersOffice.Security;
    using Utils;
    using global::DataAccess;

    /// <summary>
    /// Provides a fake for the external interface of a loan.
    /// </summary>
    internal class FakePageData : CPageData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FakePageData"/> class.
        /// </summary>
        /// <param name="loanValues">
        /// The <see cref="DataRow"/> containing the values for the fake.
        /// </param>
        /// <param name="appCount">
        /// The number of applications that the fake should have.
        /// </param>
        /// <param name="bypassFieldSecurityCheck">
        /// Whether the loan should bypass field security checks on load 
        /// and save.
        /// </param>
        private FakePageData(DataRow loanValues, int appCount, bool bypassFieldSecurityCheck = true)
            : base(new FakePageBase(loanValues, appCount), new FakeSelectStatementProvider(), new FakeLoanFileFieldValidator())
        {
            this.ByPassFieldSecurityCheck = bypassFieldSecurityCheck;
        }

        /// <summary>
        /// Gets the wrapped <see cref="CPageBase"/> for this fake.
        /// </summary>
        public CPageBase PageBase => this.m_pageBase;

        /// <summary>
        /// Gets a value indicating whether access control is enforced for the fake.
        /// </summary>
        /// <value>
        /// True if access control is enforced for the fake, false otherwise.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Maintain naming of access control property between parent and derived class.")]
        protected override bool m_enforceAccessControl
        {
            get { return !this.ByPassFieldSecurityCheck; }
        }

        /// <summary>
        /// Creates a new instance of the <see cref="FakePageData"/> class. Optional
        /// parameters specify the number of applications for the fake and whether the
        /// fake will enforce access control.
        /// </summary>
        /// <param name="appCount">
        /// The number of applications for the fake.
        /// </param>
        /// <param name="bypassFieldSecurityCheck">
        /// Whether the loan should bypass field security checks on load 
        /// and save.
        /// </param>
        /// <returns>
        /// A new instance fake loan instance.
        /// </returns>
        public static FakePageData Create(int appCount = 1, bool bypassFieldSecurityCheck = true)
        {
            if (appCount < 1)
            {
                throw new ArgumentException("Fake must have at least one application.", nameof(appCount));
            }
            else if (appCount > FakePageDataUtilities.FakeApplicationCount)
            {
                var message = $"The current max for fake applications is {FakePageDataUtilities.FakeApplicationCount}. To " +
                    $"use additional fake applications, please change the value of {nameof(FakePageDataUtilities.FakeApplicationCount)} " +
                    $"in {nameof(FakePageDataUtilities)}.";

                throw new ArgumentException(message, nameof(appCount));
            }

            return new FakePageData(FakePageDataUtilities.Instance.GetLoanDataContainerCopy(), appCount, bypassFieldSecurityCheck);
        }

        /// <summary>
        /// Initializes loan data for the fake.
        /// </summary>
        public override void InitLoad()
        {
            this.InitLoad(PrincipalFactory.CurrentPrincipal ?? LoginTools.LoginAs(TestAccountType.LoTest001));
        }

        /// <summary>
        /// Initializes loan data for the fake. A parameter specifies
        /// the principal that is loading the loan.
        /// </summary>
        /// <param name="userP">
        /// The <see cref="AbstractUserPrincipal"/> that is loading the loan.
        /// </param>
        public override void InitLoad(AbstractUserPrincipal userP)
        {
            this.m_pageBase.InitLoad();
        }

        /// <summary>
        /// Prepares the loan for saving.
        /// </summary>
        /// <param name="expectedVersion">
        /// The expected version of the loan to save.
        /// </param>
        public override void InitSave(int expectedVersion)
        {
            this.m_pageBase.InitSave(expectedVersion);
        }

        /// <summary>
        /// Saves the loan data.
        /// </summary>
        public override void Save()
        {
            this.m_pageBase.Save();
        }
    }
}
